#!/bin/bash
INITIAL_PATCH_NUMBER=`cat build/patchNumber/initialPatchNumber.conf`
if [ -f build/patchNumber/patchNumberCounter ]
then
        CURRENT=`cat build/patchNumber/patchNumberCounter`
        NEXT=$((CURRENT+1))
        echo -n $NEXT > build/patchNumber/patchNumberCounter
else
    echo -n $INITIAL_PATCH_NUMBER > build/patchNumber/patchNumberCounter
fi
