#!/bin/bash
INITIAL_PATCH_NUMBER=`cat build/patchNumber/initialPatchNumber.conf`
if [ -f build/patchNumber/patchNumberCounter ]
then
 cat build/patchNumber/patchNumberCounter
else
    echo -n $INITIAL_PATCH_NUMBER > build/patchNumber/patchNumberCounter
    cat build/patchNumber/patchNumberCounter
fi
