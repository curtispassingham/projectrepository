#!/bin/bash

#Definig constants
readonly MANIFEST_FILE_IDX=0
readonly MANIFEST_VERSION_IDX=1
readonly MANIFEST_TYPE_IDX=2
readonly MANIFEST_UNITTEST_IDX=3
readonly MANIFEST_LOCALIZATION_IDX=4
readonly MANIFEST_PATCHNAME_IDX=5
readonly MANIFEST_CUSTOMIZED_IDX=6
readonly MANIFEST_FWPVERSION_IDX=7
readonly MANIFEST_FUTURE1_IDX=8
readonly MANIFEST_FUTURE2_IDX=9
readonly MANIFEST_FUTURE3_IDX=10
readonly MANIFEST_FUTURE4_IDX=11
readonly MANIFEST_ORIGFILE_IDX=12

readonly METADATA_OVERRIDES_FILE_IDX=0
readonly METADATA_OVERRIDES_TYPE_IDX=1


BUILD_VERSION=$1
GIT_HASH=$2
SOURCE_FILES_OVERRIDE=$3



echo "BuildVersion: ${BUILD_VERSION}"
echo "GitHash: ${GIT_HASH}"


#Defining the PATCH NAME
PATCH_NAME="ADEO_PATCH_$BUILD_VERSION"


#Defining working directories
BUILD_DIR="./build"
RELEASE_DIR="$BUILD_DIR/release"
ARTIFACTS_DIR="$BUILD_DIR/artifacts"
ARCHIVE_CUMULATIVE_MANIFEST_DIR=$ARTIFACTS_DIR/ARCHIVE_CUMULATIVE_MANIFEST
ARCHIVE_CUMULATIVE_PATCH_INFO_DIR=$ARTIFACTS_DIR/ARCHIVE_CUMULATIVE_PATCH_INFO
PATCH_DIR="$BUILD_DIR/patch"



preparingWorkingDirectories(){
  
  #Creating $ARTIFACTS_DIR if not present
  if [ ! -d $ARTIFACTS_DIR ]; then 
    mkdir $ARTIFACTS_DIR
  fi


  #Creating $RELEASE_DIR if not present
  if [ ! -d $RELEASE_DIR ]; then 
    mkdir $RELEASE_DIR
  fi

  
  #Creating $PATCH_DIR if not present
  if [ ! -d $PATCH_DIR ]; then 
    mkdir $PATCH_DIR
  fi
  
  #Creating $ARCHIVE_CUMULATIVE_MANIFEST_DIR if not present
  if [ ! -d $ARCHIVE_CUMULATIVE_MANIFEST_DIR ]; then 
    mkdir -p $ARCHIVE_CUMULATIVE_MANIFEST_DIR
  fi
  
  #Creating $ARCHIVE_CUMULATIVE_PATCH_INFO_DIR if not present
  if [ ! -d $ARCHIVE_CUMULATIVE_PATCH_INFO_DIR ]; then 
    mkdir -p $ARCHIVE_CUMULATIVE_PATCH_INFO_DIR
  fi

  #Deleting content of $RELEASE_DIR and $PATCH_DIR
  echo "Deleting $RELEASE_DIR content"
  rm -rf $RELEASE_DIR/*
  echo "Deleting $PATCH_DIR content"
  rm -rf $PATCH_DIR/*

}

getManifestFiles(){

  local STATIC_BUILD_FILE=$1
  local STATIC_BUILD_FILE_PATH 
  local MANIFEST_FILES

  if [ "$STATIC_BUILD_FILE" != "UseGit" ]; then
    STATIC_BUILD_FILE_PATH="${BUILD_DIR}/static_builds/${STATIC_BUILD_FILE}"
    if [ ! -f $STATIC_BUILD_FILE_PATH ]; then
    	echo "The static build file '${STATIC_BUILD_FILE_PATH}' does not exist" >&2
        exit 1
    fi

    MANIFEST_FILES=$(cat $STATIC_BUILD_FILE_PATH | sed 's/\r$//')
  else
    MANIFEST_FILES=$(git diff-tree --no-commit-id --name-only --diff-filter=ACMRd -m -r $GIT_HASH --first-parent | sort)

    if [ $? -gt 0 ]; then
      echo "Git failure - The git hash $GIT_HASH does not exist" >&2
      exit 1
    fi
  fi
  
  echo "${MANIFEST_FILES}"
}

addManifestEntry(){

  local NEW_ENTRY=("$@")

  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_FILE_IDX"]=${NEW_ENTRY[${MANIFEST_FILE_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_VERSION_IDX"]=${NEW_ENTRY[${MANIFEST_VERSION_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_TYPE_IDX"]=${NEW_ENTRY[${MANIFEST_TYPE_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_UNITTEST_IDX"]=${NEW_ENTRY[${MANIFEST_UNITTEST_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_LOCALIZATION_IDX"]=${NEW_ENTRY[${MANIFEST_LOCALIZATION_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_PATCHNAME_IDX"]=${NEW_ENTRY[${MANIFEST_PATCHNAME_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_CUSTOMIZED_IDX"]=${NEW_ENTRY[${MANIFEST_CUSTOMIZED_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_FWPVERSION_IDX"]=${NEW_ENTRY[${MANIFEST_FWPVERSION_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_FUTURE1_IDX"]=${NEW_ENTRY[${MANIFEST_FUTURE1_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_FUTURE2_IDX"]=${NEW_ENTRY[${MANIFEST_FUTURE2_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_FUTURE3_IDX"]=${NEW_ENTRY[${MANIFEST_FUTURE3_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_FUTURE4_IDX"]=${NEW_ENTRY[${MANIFEST_FUTURE4_IDX}]}
  NEW_MANIFEST_CONTENT["$NEW_MANIFEST_CONTENT_COUNT,$MANIFEST_ORIGFILE_IDX"]=${NEW_ENTRY[${MANIFEST_ORIGFILE_IDX}]}


  ((NEW_MANIFEST_CONTENT_COUNT++))

}

printManifestContent(){

  printf "FILE,VERSION,TYPE,UNITTEST,LOCALIZATION,PATCHNAME,CUSTOMIZED,FWPVERSION,FUTURE1,FUTURE2,FUTURE3,FUTURE4,ORIGFILE"

  for (( i=0; i<$NEW_MANIFEST_CONTENT_COUNT; i++ ))
  do
 
    printf "\n%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s"    "${NEW_MANIFEST_CONTENT["${i},${MANIFEST_FILE_IDX}"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_VERSION_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_TYPE_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_UNITTEST_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_LOCALIZATION_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_PATCHNAME_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_CUSTOMIZED_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FWPVERSION_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FUTURE1_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FUTURE2_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FUTURE3_IDX"]}"  "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FUTURE4_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_ORIGFILE_IDX"]}"

  
  done

}


#Calling preparingWorkingDirectories function
preparingWorkingDirectories

FILES_ADDED_UPDATED=$(getManifestFiles $SOURCE_FILES_OVERRIDE) || exit $?


declare -A NEW_MANIFEST_CONTENT
NEW_MANIFEST_CONTENT_COUNT=0

APPLIST=()
PATCH_FILES_TO_COPY=()

# Checking if oracle manifest file and patch info file exists

USE_ORACLE_MANIFEST=$([[ $(echo "${FILES_ADDED_UPDATED}" | grep oracle_manifest.csv |wc -l ) -gt 0 ]]  && echo "true" || echo "false")
USE_ORACLE_PATCH_INFO=$([[ $(echo "${FILES_ADDED_UPDATED}" | grep patch_info.cfg |wc -l) -gt 0 ]] && echo "true" || echo "false")


# Reading METADA OVERRIDE content
METADATA_OVERRIDE_FILE="$BUILD_DIR/metadata_override.csv"
if [ ! -f $METADATA_OVERRIDE_FILE ]; then
  echo "The file '$METADATA_OVERRIDE_FILE' does not exists" >&2
  exit 1
fi
METADATA_OVERRIDE_CONTENT=$(cat $METADATA_OVERRIDE_FILE | sed 's/\r$//')


echo "Using Oracle Manifest  : $USE_ORACLE_MANIFEST" 
echo "Using Oracle Patch Info: $USE_ORACLE_PATCH_INFO "

# Loading Oracle manifest file
if [ "$USE_ORACLE_MANIFEST" == "true" ]; then
  
  if [ ! -f $BUILD_DIR/oracle_manifest.csv ]; then
    echo "The file '$BUILD_DIR/oracle_manifest.csv' does not exists" >&2
    exit 1
  fi

  ORACLE_MANIFEST_CONTENT=$(cat $BUILD_DIR/oracle_manifest.csv | sed 's/\r$//')
fi


#For each file from the GIT commit or from the static build
for FILE in $FILES_ADDED_UPDATED
do

  FILE_PATH=$(dirname $FILE)
 
  #Handling only the files that have the metadata.csv file in its directory
  if [ -f $FILE_PATH/metadata.csv ] 
  then
   
    #Ignoring metadata.csv file 
    if [ $(basename $FILE) != "metadata.csv" ]
    then
       
      #Loading metadata.csv content and cheking if the content is valid
      METADATA_CONTENT=$(cat $FILE_PATH/metadata.csv | sed 's/\r$//')
      if [ $(echo "$METADATA_CONTENT" | wc -l) -lt 2 ]
      then
        echo "The metadata file '$FILE_PATH/metadata.csv is invalid'" >&2
        exit 1
      fi

      PATH_TO_REMOVE=$(echo "$METADATA_CONTENT" | tail -n  +2 |  awk  -F "," '//{print $2}')|| exit $? 

      #Cheking if PATH_TO_REMOVE ends with '/'. If not append a '/' to the end
      if [  $(echo "$PATH_TO_REMOVE" | grep /$|wc -l) -eq 0  ]
      then
        PATH_TO_REMOVE=$(echo "$PATH_TO_REMOVE/")
      fi

      #Escaping '/' characters
      PATH_TO_REMOVE=$(echo "$PATH_TO_REMOVE"| sed 's/\//\\\//g')

      
      #Removing the path
      TRIMED_FILE=$( echo "$FILE" |sed -E "s/$PATH_TO_REMOVE(.+)/\1/g" )



      METADATA_OVERRIDE_MATCH_FILE=$(echo "$METADATA_OVERRIDE_CONTENT" | awk -F ","  -v pattern="$FILE"   '$1 ~  pattern  {print $0}')
      
	  #If there is an entry on metadata_override.csv
	  METADATA_OVERRIDE_TYPE=""
	  if [ "$METADATA_OVERRIDE_MATCH_FILE" != "" ]
	  then
	    #splitting metadata_override entry by ','
	    OLDIFS="$IFS"
	    IFS=","
	    read -r -a METADATA_OVERRRIDE_ENTRY <<< "$METADATA_OVERRIDE_MATCH_FILE"
	    IFS="$OLDIFS"

	    METADATA_OVERRIDE_TYPE=${METADATA_OVERRRIDE_ENTRY[$METADATA_OVERRIDES_TYPE_IDX]}

	    echo "TYPE override found in metadata_override.csv for file '$FILE'"
	  fi
	  

      if [ "$USE_ORACLE_MANIFEST" == "true" ]
      then	
      	ORACLE_MANIFEST_MATCH_FILE=$(echo "$ORACLE_MANIFEST_CONTENT" | awk -F ","  -v pattern="$TRIMED_FILE"   '$1 ~  pattern  {print $0}')
      else
        ORACLE_MANIFEST_MATCH_FILE=""
      fi

      #If there is an entry on oracle_manifest.csv for the file
      if [ "$ORACLE_MANIFEST_MATCH_FILE" != "" ] 
      then
        echo "Using entry from oracle_manifest for file '$FILE'";
        

        #splitting oracle_manifest entry by ',' 
        OLDIFS="$IFS"
        IFS="," 
        read -r -a MANIFEST_ENTRY <<< "$ORACLE_MANIFEST_MATCH_FILE"
        IFS="$OLDIFS"
	
	    #Updating Type with the type from 'metadata_override.csv'
		if [ "$METADATA_OVERRIDE_TYPE" != ""]
		then
          MANIFEST_ENTRY[$MANIFEST_TYPE_IDX]=$METADATA_OVERRIDE_TYPE
		fi  
		
        MANIFEST_ENTRY[$MANIFEST_ORIGFILE_IDX]=${MANIFEST_ENTRY[$MANIFEST_FILE_IDX]}
   
        addManifestEntry  "${MANIFEST_ENTRY[@]}"
	  

      #if !$USE_ORACLE_MANIFEST or if there is no entry on oracle_manifest.csv
      else
	     
	     FUTURE1_FLAG=''
		 
		 #Updating Type with the type from 'metadata_override.csv'
		 if [ "$METADATA_OVERRIDE_TYPE" != "" ]
		 then
           METADATA_TYPE=$METADATA_OVERRIDE_TYPE
		 else
		   METADATA_TYPE=$(echo "$METADATA_CONTENT" | tail -n  +2 |  awk  -F "," '//{print $1}'|sed 's/ *$//')|| exit $?
		 fi
		 
		 if [ "$(echo "$METADATA_TYPE" | grep java_app)" != "" ]
		 then
		   FUTURE1_FLAG="Y"
		 fi
		 
		 MANIFEST_ENTRY[${MANIFEST_FILE_IDX}]=${TRIMED_FILE}
         MANIFEST_ENTRY[${MANIFEST_VERSION_IDX}]=${BUILD_VERSION}
         MANIFEST_ENTRY[${MANIFEST_TYPE_IDX}]=${METADATA_TYPE}
         MANIFEST_ENTRY[${MANIFEST_UNITTEST_IDX}]=""
         MANIFEST_ENTRY[${MANIFEST_LOCALIZATION_IDX}]=""
         MANIFEST_ENTRY[${MANIFEST_PATCHNAME_IDX}]=""
         MANIFEST_ENTRY[${MANIFEST_CUSTOMIZED_IDX}]="Y"
         MANIFEST_ENTRY[${MANIFEST_FWPVERSION_IDX}]=""
         MANIFEST_ENTRY[${MANIFEST_FUTURE1_IDX}]=${FUTURE1_FLAG}
         MANIFEST_ENTRY[${MANIFEST_FUTURE2_IDX}]=""
         MANIFEST_ENTRY[${MANIFEST_FUTURE3_IDX}]=""
         MANIFEST_ENTRY[${MANIFEST_FUTURE4_IDX}]=""
         MANIFEST_ENTRY[${MANIFEST_ORIGFILE_IDX}]=${FILE}
		 
		 addManifestEntry  "${MANIFEST_ENTRY[@]}"
	  
      fi #if [ "$ORACLE_MANIFEST_MATCH_FILE" != "" ]
	 
	 PATCH_FILES_TO_COPY+=( "${FILE}|${MANIFEST_ENTRY[$MANIFEST_FILE_IDX]}" )
	 
	 NEW_APP=$(echo "$METADATA_CONTENT" | tail -n  +2 |  awk  -F "," '//{print $3}')|| exit $? 
	 APPLIST+=("$NEW_APP")
	
	
	
	fi #if [ $(basename $FILE) != "metadata.csv" ]
  else
    echo  "Ignoring file '$FILE' - metadata.csv not found in path: '$FILE_PATH'" >&2
  fi
  
  
  if [  "$(echo "$FILE" | grep "^build/patch_include/")" != "" ]
  then
     $(cp $FILE $PATCH_DIR)
  fi

done # for FILE in $FILES_ADDED_UPDATED


if [ $NEW_MANIFEST_CONTENT_COUNT -eq 0 ]
then
	echo "No files to be patched" >&2
else
    # Writing manifest content
    printManifestContent > $PATCH_DIR/manifest.csv
	
	# Copying manifest.csv file to ARTIFACTS_DIR
	cp $PATCH_DIR/manifest.csv $ARTIFACTS_DIR/manifest.csv
fi 


# Updating cumulative manifest
if [ -f "$ARTIFACTS_DIR/cumulative_manifest.csv" ]
then
	echo "Cumulative manifest found"
	echo "Archiving cumulative_manifest.csv"
	TIMESTAMP=$(date "+%Y-%m-%d_%H.%M.%S")
	$(cp "${ARTIFACTS_DIR}/cumulative_manifest.csv" "${ARCHIVE_CUMULATIVE_MANIFEST_DIR}/cumulative_manifest_${TIMESTAMP}.csv")
	
	########################################################
	#### Loading cumulative Manifest                    ####           
	########################################################
	declare -A CUMULATIVE_MANIFEST_HASH
	declare -A CUMULATIVE_MANIFEST_HASH_BY_FILE
    CUMULATIVE_MANIFEST_HASH_COUNT=0
	
	CUMULATIVE_MANIFEST_CONTENT=$(cat "$ARTIFACTS_DIR/cumulative_manifest.csv" | tail -n  +2 | sed 's/\r$//')
	
	for line in $CUMULATIVE_MANIFEST_CONTENT
	do
	   
	   #splitting line by ','
	   OLDIFS="$IFS"
	   IFS=","
	   read -r -a LINE_SPLITTED <<< "$line"
	   IFS="$OLDIFS"
	   
	   CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_FILE_IDX"]=${LINE_SPLITTED[${MANIFEST_FILE_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_VERSION_IDX"]=${LINE_SPLITTED[${MANIFEST_VERSION_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_TYPE_IDX"]=${LINE_SPLITTED[${MANIFEST_TYPE_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_UNITTEST_IDX"]=${LINE_SPLITTED[${MANIFEST_UNITTEST_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_LOCALIZATION_IDX"]=${LINE_SPLITTED[${MANIFEST_LOCALIZATION_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_PATCHNAME_IDX"]=${LINE_SPLITTED[${MANIFEST_PATCHNAME_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_CUSTOMIZED_IDX"]=${LINE_SPLITTED[${MANIFEST_CUSTOMIZED_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_FWPVERSION_IDX"]=${LINE_SPLITTED[${MANIFEST_FWPVERSION_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_FUTURE1_IDX"]=${LINE_SPLITTED[${MANIFEST_FUTURE1_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_FUTURE2_IDX"]=${LINE_SPLITTED[${MANIFEST_FUTURE2_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_FUTURE3_IDX"]=${LINE_SPLITTED[${MANIFEST_FUTURE3_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_FUTURE4_IDX"]=${LINE_SPLITTED[${MANIFEST_FUTURE4_IDX}]}
       CUMULATIVE_MANIFEST_HASH["$CUMULATIVE_MANIFEST_HASH_COUNT,$MANIFEST_ORIGFILE_IDX"]=${LINE_SPLITTED[${MANIFEST_ORIGFILE_IDX}]}
	   
	   
	   CUMULATIVE_MANIFEST_HASH_BY_FILE[${LINE_SPLITTED[${MANIFEST_FILE_IDX}]}]=$CUMULATIVE_MANIFEST_HASH_COUNT
	   
	   ((CUMULATIVE_MANIFEST_HASH_COUNT++))
	done
	
	
	########################################################
	#### Updating cumulative Manifest                   ####           
	########################################################
	CUMULATIVE_MANIFEST_NEW_LINES=()
	for (( i=0; i<$NEW_MANIFEST_CONTENT_COUNT; i++ ))
	do
	  
	  if [[ ${CUMULATIVE_MANIFEST_HASH_BY_FILE[${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FILE_IDX"]}]} ]]
	  then
		
		ENTRY_POS=${CUMULATIVE_MANIFEST_HASH_BY_FILE[${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FILE_IDX"]}]}
		
		CUMULATIVE_MANIFEST_HASH["$ENTRY_POS,$MANIFEST_VERSION_IDX"]=${NEW_MANIFEST_CONTENT["$i,$MANIFEST_VERSION_IDX"]}
	  else
	  
        NEW_CUMULATIVE_LINE=$(printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s"    "${NEW_MANIFEST_CONTENT["${i},${MANIFEST_FILE_IDX}"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_VERSION_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_TYPE_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_UNITTEST_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_LOCALIZATION_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_PATCHNAME_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_CUSTOMIZED_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FWPVERSION_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FUTURE1_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FUTURE2_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FUTURE3_IDX"]}"  "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_FUTURE4_IDX"]}" "${NEW_MANIFEST_CONTENT["$i,$MANIFEST_ORIGFILE_IDX"]}"	)	
	
		
		CUMULATIVE_MANIFEST_NEW_LINES+=("$NEW_CUMULATIVE_LINE")
	  fi
	done
	
	########################################################
	#### Writing cumulative manifest                    ####           
	########################################################
	printf "FILE,VERSION,TYPE,UNITTEST,LOCALIZATION,PATCHNAME,CUSTOMIZED,FWPVERSION,FUTURE1,FUTURE2,FUTURE3,FUTURE4,ORIGFILE" > $ARTIFACTS_DIR/cumulative_manifest.csv
    for (( i=0; i<$CUMULATIVE_MANIFEST_HASH_COUNT; i++ ))
    do
      printf "\n%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s"    "${CUMULATIVE_MANIFEST_HASH["${i},${MANIFEST_FILE_IDX}"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_VERSION_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_TYPE_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_UNITTEST_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_LOCALIZATION_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_PATCHNAME_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_CUSTOMIZED_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_FWPVERSION_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_FUTURE1_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_FUTURE2_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_FUTURE3_IDX"]}"  "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_FUTURE4_IDX"]}" "${CUMULATIVE_MANIFEST_HASH["$i,$MANIFEST_ORIGFILE_IDX"]}" >> $ARTIFACTS_DIR/cumulative_manifest.csv
    done
	
	#printing the new lines
	for line in "${CUMULATIVE_MANIFEST_NEW_LINES[@]}"
	do
		printf "\n%s" "$line" >> $ARTIFACTS_DIR/cumulative_manifest.csv
	done

else
  echo "Cumulative manifest not found. Creating a new cumulative_manifest.csv"
  
  printManifestContent > $ARTIFACTS_DIR/cumulative_manifest.csv
fi


#Copy patch files to patch directory

for FILE_TO_COPY in "${PATCH_FILES_TO_COPY[@]}"
do

     #splitting line by '|'
	 OLDIFS="$IFS"
	 IFS="|"
	 read -r -a SPLITTED_FILE_TO_COPY <<< "$FILE_TO_COPY"
	 IFS="$OLDIFS"
   
   
     SOURCE_PATH=${SPLITTED_FILE_TO_COPY[0]}
     DEST_PATH=${SPLITTED_FILE_TO_COPY[1]}
	 
	 
	 COPY_DEST_PATH="$PATCH_DIR/$DEST_PATH"
	 COPY_DEST_PATH_DIR=$(dirname $COPY_DEST_PATH)
	 
	 
	 #Creating $COPY_DEST_PATH_DIR if not present
     if [ ! -d $COPY_DEST_PATH_DIR ]; then 
       mkdir -p $COPY_DEST_PATH_DIR
     fi
	 
	 cp $SOURCE_PATH $COPY_DEST_PATH


done


#Adding patch info
if [ "$USE_ORACLE_PATCH_INFO" == "true" ]
then
   cp "$BUILD_DIR/patch_info.cfg" "$PATCH_DIR/patch_info.cfg"
else
	
	PRODUCT_NAMES=$(echo "${APPLIST[@]}" | tr ' ' '\n' | sort -u | tr '\n' ','| sed 's/,$//' )
	
	OLDIFS="$IFS"
	IFS=$'\n'
	PATCH_INFO_TEMPLATE_CONTENT_ARR=($( cat "$BUILD_DIR/patch_info-template.cfg" | sed 's/\r$//'))
	IFS="$OLDIFS"
	
	
	# Replacing product name
	PATCH_INFO_TEMPLATE_CONTENT_ARR[0]=$( echo ${PATCH_INFO_TEMPLATE_CONTENT_ARR[0]} | sed 's/##Product-Names##/'"$PRODUCT_NAMES"'/')
	
	# Replacing patch name
	PATCH_INFO_TEMPLATE_CONTENT_ARR[2]=$( echo ${PATCH_INFO_TEMPLATE_CONTENT_ARR[2]} | sed 's/##Patch-Name##/'"$PATCH_NAME"'/')
	
	
	printf '%s\n' "${PATCH_INFO_TEMPLATE_CONTENT_ARR[@]}" > "$PATCH_DIR/patch_info.cfg"
fi

#copy patch info to artifacts
cp $PATCH_DIR/patch_info.cfg $ARTIFACTS_DIR/patch_info.cfg


#updating cumulative patch info
if [ -f "$ARTIFACTS_DIR/cumulative_patch_info.cfg" ]
then
	echo "cumulative_patch_info found! ... archiving cumulative_patch_info.cfg"
	TIMESTAMP=$(date "+%Y-%m-%d_%H.%M.%S")
	$(cp "${ARTIFACTS_DIR}/cumulative_patch_info.cfg" "${ARCHIVE_CUMULATIVE_PATCH_INFO_DIR}/cumulative_patch_info_${TIMESTAMP}.csv")
	
	echo "Updating with patch_info from this build"

	#Getting the apps from the cumulative_patch_info.cfg
	OLDIFS="$IFS"
	IFS=","  #splitting line by ','
	read -r -a CUMULATIVE_APPS <<< "$( cat $ARTIFACTS_DIR/cumulative_patch_info.cfg| sed 's/\r$//' | head -1| awk  -F "=" '//{print $2}')"
	IFS="$OLDIFS"
	
	#Getting the apps from the new build
	OLDIFS="$IFS"
	IFS=","  #splitting line by ','
	read -r -a BUILD_APPS <<< "$( cat $PATCH_DIR/patch_info.cfg| sed 's/\r$//' | head -1 | awk  -F "=" '//{print $2}')"
	IFS="$OLDIFS"
	
	NEW_CUMULATIVE_APPS=("${CUMULATIVE_APPS[@]}" "${BUILD_APPS[@]}")
	NEW_CUMULATIVE_DISTINCT_APPS=$(echo "${NEW_CUMULATIVE_APPS[@]}" | tr ' ' '\n' | sort -u | tr '\n' ','| sed 's/,$//' )
	
	
	#Reading and updating sub_pacth_lines
	OLDIFS="$IFS"
	IFS=$'\n'
	SUB_PATCH_LINES=($( cat "$ARTIFACTS_DIR/cumulative_patch_info.cfg" | tail -n  +5 | sed 's/\r$//'))
	IFS="$OLDIFS"
	
	NEW_SUBPATCH_INDEX=$((${#SUB_PATCH_LINES[@]} + 1))
	SUB_PATCH_LINES+=("SUBPATCH_NAME_${NEW_SUBPATCH_INDEX}=${PATCH_NAME}")
	
	
	
	OLDIFS="$IFS"
	IFS=$'\n'
	PATCH_INFO_TEMPLATE_CONTENT_ARR=($( cat "$BUILD_DIR/patch_info-template.cfg" | sed 's/\r$//'))
	IFS="$OLDIFS"
	
	
	# Replacing product name
	PATCH_INFO_TEMPLATE_CONTENT_ARR[0]=$( echo ${PATCH_INFO_TEMPLATE_CONTENT_ARR[0]} | sed 's/##Product-Names##/'"$NEW_CUMULATIVE_DISTINCT_APPS"'/')
	
	
	printf '%s\n' "${PATCH_INFO_TEMPLATE_CONTENT_ARR[@]}" > "$ARTIFACTS_DIR/cumulative_patch_info.cfg"
	
	printf 'MERGED_PATCH=Y' >> "$ARTIFACTS_DIR/cumulative_patch_info.cfg"
	
	
	for PATCH_LINE in "${SUB_PATCH_LINES[@]}"
	do
	  printf "\n%s" "$PATCH_LINE" >> "$ARTIFACTS_DIR/cumulative_patch_info.cfg"
	done
else
	echo "cumulative_patch_info.cfg not found! Creating a new cumulative_patch_info.cfg"

    OLDIFS="$IFS"
	IFS=$'\n'
	PATCH_INFO_TEMPLATE_CONTENT_ARR=($( cat "$BUILD_DIR/patch_info-template.cfg" | sed 's/\r$//'))
	IFS="$OLDIFS"
	
	
	# Replacing product name
	PATCH_INFO_TEMPLATE_CONTENT_ARR[0]=$( echo ${PATCH_INFO_TEMPLATE_CONTENT_ARR[0]} | sed 's/##Product-Names##/'"$PRODUCT_NAMES"'/')
	
	printf '%s\n' "${PATCH_INFO_TEMPLATE_CONTENT_ARR[@]}" > "$ARTIFACTS_DIR/cumulative_patch_info.cfg"
	
	printf 'MERGED_PATCH=Y' >> "$ARTIFACTS_DIR/cumulative_patch_info.cfg"
	
	printf "\nSUBPATCH_NAME_1=${PATCH_NAME}"  >> "$ARTIFACTS_DIR/cumulative_patch_info.cfg"
	

fi

#Generating patch zip
CURRENT_DIR=$(pwd)
(cd $PATCH_DIR; zip -r ../../$RELEASE_DIR/$PATCH_NAME.zip .)
$(cd $CURRENT_DIR)

