<?xml version="1.0" encoding="UTF-8"?>
<xs:schema elementFormDefault="qualified" version="1.0" targetNamespace="http://www.oracle.com/retail/integration/base/bo/ShlfAdjModVo/v1" xmlns="http://www.oracle.com/retail/integration/base/bo/ShlfAdjModVo/v1" xmlns:retailDoc="http://www.w3.org/2001/XMLSchema" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="ShlfAdjModVo">
        <retailDoc:annotation>
            <retailDoc:documentation>Detailed information about a created or modified shelf adjustment.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="adjustment_id" type="number15" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Shelf adjustment identifier. If left black, a new shelf adjustment will be created.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="store_id" type="number10">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Store identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
				<xs:element name="user_name" type="varchar2128">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The user name of the user requesting the changes.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="status" type="shlf_adj_status">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Status of the shelf adjustment. Valid values are NEW, COMPLETE, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="type" type="shlf_adj_type">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Type of the shelf adjustment. Valid values are SHOP_FLOOR_ADJUST, BACK_ROOM_ADJUST, DISPLAY_LIST, ADHOC_REPLENISH, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="ShlfAdjModItm" minOccurs="0" maxOccurs="999">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Collection of shelf adjustment line items.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="removed_line_id_col" type="number12" minOccurs="0" maxOccurs="5000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Collections of removed line items indicated by their unique identifer.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="ShlfAdjModItm">
        <retailDoc:annotation>
            <retailDoc:documentation>Detailed Information about a modified shelf adjustment line item.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="line_id" type="number12" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Line item unique identifier. If null, the line item will be created.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="item_id" type="varchar225">
                    <retailDoc:annotation>
                        <retailDoc:documentation>SKU number.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="requested_pick_amount" type="number20_4">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Quantity requested.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="case_size" type="number10_2">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Case size associated with the item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="shlf_adj_status">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="NEW" />
            <xs:enumeration value="COMPLETE" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="shlf_adj_type">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="SHOP_FLOOR_ADJUST" />
            <xs:enumeration value="BACK_ROOM_ADJUST" />
            <xs:enumeration value="DISPLAY_LIST" />
            <xs:enumeration value="ADHOC_REPLENISH" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar225">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 25 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="25" />
        </xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="varchar2128">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 128 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="128" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="10" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10_2">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10 and fraction digit count of 2.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:decimal">
            <xs:totalDigits value="10" />
            <xs:fractionDigits value="2" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number12">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 12.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="12" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number15">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 15.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="15" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number20_4">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 20 and fraction digit count of 4.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:decimal">
            <xs:totalDigits value="20" />
            <xs:fractionDigits value="4" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
