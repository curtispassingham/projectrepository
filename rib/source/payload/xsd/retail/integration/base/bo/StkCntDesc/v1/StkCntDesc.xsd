<?xml version="1.0" encoding="UTF-8"?>
<xs:schema elementFormDefault="qualified" version="1.0" targetNamespace="http://www.oracle.com/retail/integration/base/bo/StkCntDesc/v1" xmlns="http://www.oracle.com/retail/integration/base/bo/StkCntDesc/v1" xmlns:retailDoc="http://www.w3.org/2001/XMLSchema" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="StkCntDesc">
        <retailDoc:annotation>
            <retailDoc:documentation>Detailed information about a Stock Count.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="stock_count_id" type="number12">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique identifier for the stock count.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="store_id" type="number10">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The store where the stock is being counted.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="product_group_id" type="number10" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique identifier for the product group used for the stock count.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="schedule_id" type="number10" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique identifier for the product group schedule attached to the stock count..</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="count_type" type="stk_cnt_type">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The type of stock count being performed. Valid values are UNIT, UNIT_AMOUNT, PROBLEM_LINE, ADHOC, RMS_SYNC, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="status" type="stk_cnt_status">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The status of the stock count. Valid values are NEW, COUNT_SCHEDULED, COUNT_IN_PROGRESS, COUNT_COMPLETE, RECOUNT_SCHEDULED, RECOUNT_IN_PROGRESS, RECOUNT_COMPLETE, APPROVAL_SCHEDULED, APPROVAL_IN_PROGRESS, APPROVAL_PROCESSING, APPROVAL_AUTHORIZED, APPROVAL_COMPLETE, CANCELED, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="display_status" type="stk_cnt_display_status">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The display status of the stock count. Valid values are NONE, NEW, ACTIVE, IN_PROGRESS, PENDING, PROCESSING, AUTHORIZED, COMPLETED, CANCELED, VIEW_ONLY.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="counting_method" type="stk_cnt_method">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The counting method of the stock count. Valid values are GUIDED, UNGUIDED, THIRD_PARTY, AUTO, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="stock_count_time_frame" type="stk_cnt_time_frame">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Time frame indicator for whether snapshot of the stock count took place before or after the store opened. Valid values are BEFORE_STORE_OPEN, AFTER_STORE_CLOSE, NONE.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="group_type" type="stk_cnt_breakdown_type">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Indicates how the stock count items should be broken into stock count children.. Valid values are DEPARTMENT, CLASS, SUBCLASS, LOCATION, NONE.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="count_desc" type="varchar2255">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The description of the stock count.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="group_desc" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The description of the product group.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="schedule_date" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The date the stock count is scheduled for.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="count_start_time" type="number18" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The time in milliseconds that the count began.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="last_updated_time" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The date and time the stock count last had an update.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="export_user" type="varchar240" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The person or operator that exported the stock count.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="variance_count" type="number12" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The number of units in standard uom that will determine if an item on the count is to be considered discrepant.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="variance_percent" type="number12" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The percentage of units in standard uom that will determine if an item on the count is to be considered discrepant.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="variance_value" type="number12" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The cost value variance from expected that will determine if an item on the count is to be considered discrepant.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="recount_active" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True indicates the stock count items should be recounted.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="auto_authorize" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True indicates the stock count is auto authorized after counting.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="pbl_pick_less_suggested" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True indicates that items with action pick amount less than suggested amount are included.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="pbl_repl_less_suggested" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True indicates that items with replenishment amount less than suggested amount are included.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="pbl_repl_neg_available" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True indicates items with negative available inventory are included.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="pbl_repl_discrepant_uin" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True indicates items with serial number discrepancies are included.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="include_active_items" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True if the stock count includes active items.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="include_inactive_items" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True if the stock count includes inactive items.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="include_discontinued_items" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True if the stock count includes discontinued items.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="include_deleted_items" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True if the stock count includes deleted items.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="include_soh_zero" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>"True if the stock count includes items whose stock on hand is zero.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="include_soh_less_than_zero" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>"True if the stock count includes items whose stock on hand is less than zero.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="include_soh_greater_than_zero" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>"True if the stock count includes items whose stock on hand is greater than zero.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="total_on_count" type="number12">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The total number of items to be counted.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="left_to_count" type="number12">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The total remaining items to be counted.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="all_items" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True indicates the stock count is an all store items count.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="CountChild" minOccurs="0" maxOccurs="1000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>A collection of stock count children associated with the stock count.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="CountChild">
        <retailDoc:annotation>
            <retailDoc:documentation>Summary information about the stock count child.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="child_id" type="varchar225">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique identifier of the stock count child.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="description" type="varchar240">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The description of the stock count child based on department or location.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="status" type="stk_cnt_status">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Status of the stock count child which may be different from the parent count.. Valid values are NEW, COUNT_SCHEDULED, COUNT_IN_PROGRESS, COUNT_COMPLETE, RECOUNT_SCHEDULED, RECOUNT_IN_PROGRESS, RECOUNT_COMPLETE, APPROVAL_SCHEDULED, APPROVAL_IN_PROGRESS, APPROVAL_PROCESSING, APPROVAL_AUTHORIZED, APPROVAL_COMPLETE, CANCELED, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="display_status" type="stk_cnt_display_status">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Display status of the stock count child which may be different from the parent count.. Valid values are NONE, NEW, ACTIVE, IN_PROGRESS, PENDING, PROCESSING, AUTHORIZED, COMPLETED, CANCELED, VIEW_ONLY.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="left_to_count" type="number12">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The remaining items to be counted in the stock count child.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="stk_cnt_type">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="UNIT" />
            <xs:enumeration value="UNIT_AMOUNT" />
            <xs:enumeration value="PROBLEM_LINE" />
            <xs:enumeration value="ADHOC" />
            <xs:enumeration value="RMS_SYNC" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="stk_cnt_status">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="NEW" />
            <xs:enumeration value="COUNT_SCHEDULED" />
            <xs:enumeration value="COUNT_IN_PROGRESS" />
            <xs:enumeration value="COUNT_COMPLETE" />
            <xs:enumeration value="RECOUNT_SCHEDULED" />
            <xs:enumeration value="RECOUNT_IN_PROGRESS" />
            <xs:enumeration value="RECOUNT_COMPLETE" />
            <xs:enumeration value="APPROVAL_SCHEDULED" />
            <xs:enumeration value="APPROVAL_IN_PROGRESS" />
            <xs:enumeration value="APPROVAL_PROCESSING" />
            <xs:enumeration value="APPROVAL_AUTHORIZED" />
            <xs:enumeration value="APPROVAL_COMPLETE" />
            <xs:enumeration value="CANCELED" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="stk_cnt_display_status">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="NONE" />
            <xs:enumeration value="NEW" />
            <xs:enumeration value="ACTIVE" />
            <xs:enumeration value="IN_PROGRESS" />
            <xs:enumeration value="PENDING" />
            <xs:enumeration value="PROCESSING" />
            <xs:enumeration value="AUTHORIZED" />
            <xs:enumeration value="COMPLETED" />
            <xs:enumeration value="CANCELED" />
            <xs:enumeration value="VIEW_ONLY" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="stk_cnt_method">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="GUIDED" />
            <xs:enumeration value="UNGUIDED" />
            <xs:enumeration value="THIRD_PARTY" />
            <xs:enumeration value="AUTO" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="stk_cnt_time_frame">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="BEFORE_STORE_OPEN" />
            <xs:enumeration value="AFTER_STORE_CLOSE" />
            <xs:enumeration value="NONE" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="stk_cnt_breakdown_type">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="DEPARTMENT" />
            <xs:enumeration value="CLASS" />
            <xs:enumeration value="SUBCLASS" />
            <xs:enumeration value="LOCATION" />
            <xs:enumeration value="NONE" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar225">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 25 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="25" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar240">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 40 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="40" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar2128">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 128 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="128" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar2255">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 255 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="255" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="10" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number12">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 12.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="12" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number18">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 18.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="18" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
