<?xml version="1.0" encoding="UTF-8"?>
<xs:schema elementFormDefault="qualified" version="1.0" targetNamespace="http://www.oracle.com/retail/integration/base/bo/ShlfRplHdrDesc/v1" xmlns="http://www.oracle.com/retail/integration/base/bo/ShlfRplHdrDesc/v1" xmlns:retailDoc="http://www.w3.org/2001/XMLSchema" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="ShlfRplHdrDesc">
        <retailDoc:annotation>
            <retailDoc:documentation>Shelf replenishment header. This does not contain line item information.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="replenishment_id" type="number15">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Unique identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="store_id" type="number10">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Store identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="description" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Description/name of the shelf replenishment.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="user_name" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>User that created the shelf replenishment.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="create_date" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Date in GMT that the shelf replenishment was created.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="status" type="shlf_repl_status">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Status of the shelf replenishment. Valid values are NEW, IN_PROGRESS, COMPLETE, CANCELED, PENDING_ALTERED, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="type" type="shlf_repl_type">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Type of shelf replenishment. Valid values are CAPACITY, SALES, AD_HOC, DISPLAY, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="replenishment_mode" type="shlf_repl_mode">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Mode of the shelf replenishment. Valid values are END_OF_DAY, WITHIN_DAY, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="replenish_quantity" type="number20_4">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Quantity to be replenished.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="actual_quantity" type="number20_4">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Actual quantity picked.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="number_of_line_items" type="number10">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Number of line items on the shelf replenishment.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="shlf_repl_status">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="NEW" />
            <xs:enumeration value="IN_PROGRESS" />
            <xs:enumeration value="COMPLETE" />
            <xs:enumeration value="CANCELED" />
            <xs:enumeration value="PENDING_ALTERED" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="shlf_repl_type">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="CAPACITY" />
            <xs:enumeration value="SALES" />
            <xs:enumeration value="AD_HOC" />
            <xs:enumeration value="DISPLAY" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="shlf_repl_mode">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="END_OF_DAY" />
            <xs:enumeration value="WITHIN_DAY" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar2128">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 128 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="128" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="10" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number15">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 15.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="15" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number20_4">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 20 and fraction digit count of 4.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:decimal">
            <xs:totalDigits value="20" />
            <xs:fractionDigits value="4" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
