<?xml version="1.0" encoding="UTF-8"?>
<xs:schema elementFormDefault="qualified" version="1.0" targetNamespace="http://www.oracle.com/retail/integration/base/bo/TsfDelvCtnModVo/v1" xmlns="http://www.oracle.com/retail/integration/base/bo/TsfDelvCtnModVo/v1" xmlns:retailDoc="http://www.w3.org/2001/XMLSchema" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:StrInvExtAttDesc="http://www.oracle.com/retail/integration/base/bo/StrInvExtAttDesc/v1">
    <xs:import namespace="http://www.oracle.com/retail/integration/base/bo/StrInvExtAttDesc/v1" schemaLocation="../../StrInvExtAttDesc/v1/StrInvExtAttDesc.xsd">
        <retailDoc:annotation>
            <retailDoc:documentation>It's a referenced element. For detailed description, please refer referenced element doc.</retailDoc:documentation>
        </retailDoc:annotation>
    </xs:import>
    <xs:element name="TsfDelvCtnModVo">
        <retailDoc:annotation>
            <retailDoc:documentation>Detailed information for saving or updating the transfer delivery container.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="delivery_id" type="number15">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Transfer delivery unique identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="store_id" type="number10">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Store unique identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="carton_id" type="number15">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique identifier for the transfer delivery container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="external_id" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Container external identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="reference_id" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The reference identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="damaged_reason" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The reason for container damage.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="serial_code" type="number18" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Serial code.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="tracking_number" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>A tracking number for the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="damage_remaining" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Indicates all remaining quantities should be damaged on final receipt.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="receive_shopfloor" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Y indicates if the stock would be received at shop floor otherwise it is received in back room.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="TsfDelvCtnItmMod" minOccurs="0" maxOccurs="5000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Collection of items within the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="TsfDelvCtnModNote" minOccurs="0" maxOccurs="1000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Notes associated to the vendor delivery.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="TsfDelvCtnItmMod">
        <retailDoc:annotation>
            <retailDoc:documentation>Detailed information about an item on the container</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="line_id" type="number15" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Line item unique identifier. If left blank, the item will be added to the delivery container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="item_id" type="varchar225">
                    <retailDoc:annotation>
                        <retailDoc:documentation>SKU number.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="case_size" type="number10_2" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Case size associated with the item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="quantity_expected" type="number20_4" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The total number of units expected in this transfer delivery.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="quantity_received" type="number20_4" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The total number of units received in this transfer delivery.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="quantity_damaged" type="number20_4" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The total number of units that were damaged when the transfer delivery was received.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="document_type" type="tsf_delv_doc_type">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Transfer delivery document type. Valid values are TRANSFER, ALLOCATION, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="document_id" type="number15">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Reference to a transfer or allocation.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="document_date" type="xs:dateTime">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The date when document was created.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="TsfDelvUinMod" minOccurs="0" maxOccurs="5000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Collection of all the Unique Identification Numbers associated with the line item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="StrInvExtAttDesc:StrInvExtAttDesc" minOccurs="0" maxOccurs="1000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Collection of extended attributes associated with the line item, either added or updated external attributes.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="removed_ext_att_col" type="varchar2128" minOccurs="0" maxOccurs="1000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Collection of extended attributes associated with the line item to be removed.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="TsfDelvUinMod">
        <retailDoc:annotation>
            <retailDoc:documentation>Transfer delivery unique serial number detail.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="uin" type="varchar2128">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Unique serial number.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="shipped" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Indicates if the item is shipped.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="received" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Indicates if the item is received.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="damaged" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Indicates if the item is damaged.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="TsfDelvCtnModNote">
        <retailDoc:annotation>
            <retailDoc:documentation>Notes attached to a transfer delivery.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="note" type="varchar22000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Note associated to the transfer delivery.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="tsf_delv_doc_type">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="TRANSFER" />
            <xs:enumeration value="ALLOCATION" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar225">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 25 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="25" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar2128">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 128 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="128" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar22000">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 2000 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="2000" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="10" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10_2">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10 and fraction digit count of 2.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:decimal">
            <xs:totalDigits value="10" />
            <xs:fractionDigits value="2" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number15">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 15.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="15" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number18">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 18.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="18" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number20_4">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 20 and fraction digit count of 4.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:decimal">
            <xs:totalDigits value="20" />
            <xs:fractionDigits value="4" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
