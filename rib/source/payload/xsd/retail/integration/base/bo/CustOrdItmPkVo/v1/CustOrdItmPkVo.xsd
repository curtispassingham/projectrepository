<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:retailDoc="http://www.w3.org/2001/XMLSchema"
           xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns="http://www.oracle.com/retail/integration/base/bo/CustOrdItmPkVo/v1"
		   xmlns:dis="http://www.oracle.com/retail/integration/base/bo/DiscntLinePkColVo/v1"
           xmlns:tax="http://www.oracle.com/retail/integration/base/bo/TaxLinePkColVo/v1"
           xmlns:itm="http://www.oracle.com/retail/integration/base/bo/CustOrdItmColDesc/v1"
           elementFormDefault="qualified"
           targetNamespace="http://www.oracle.com/retail/integration/base/bo/CustOrdItmPkVo/v1"
           version="1.0">
 <xs:import namespace="http://www.oracle.com/retail/integration/base/bo/DiscntLinePkColVo/v1"
            schemaLocation="../../DiscntLinePkColVo/v1/DiscntLinePkColVo.xsd"/>
 <xs:import namespace="http://www.oracle.com/retail/integration/base/bo/TaxLinePkColVo/v1"
            schemaLocation="../../TaxLinePkColVo/v1/TaxLinePkColVo.xsd"/>
 <xs:import namespace="http://www.oracle.com/retail/integration/base/bo/CustOrdItmColDesc/v1"
            schemaLocation="../../CustOrdItmColDesc/v1/CustOrdItmColDesc.xsd"/>
 <xs:element name="CustOrdItmPkVo">
  <retailDoc:annotation>
   <retailDoc:documentation>This is the top level CustOrdItmPkVo</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:complexType>
   <xs:sequence>
    <xs:element name="line_item_no" type="number4">
     <retailDoc:annotation>
      <retailDoc:documentation>The line item no that uniquely identifies the
                               order item to pick up.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element name="fulfill_order_id" type="varchar248">
     <retailDoc:annotation>
      <retailDoc:documentation>The fulfillment order id of this order item.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element name="completed_quantity" type="number12_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The item quantities that are picked up in this
                               transaction. This is the additional, not the
                               total completed quantities of an order item.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element name="cancelled_quantity" type="number12_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The item quantities that are cancelled in this
                               transaction. This is the additional, not the
                               total cancelled quantities of an order item.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="completed_repriced_quantity" type="number12_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The item quantities that are picked up with new price 
                               in this transaction. This is the additional, not the
                               total picked up with new price quantities of an order item.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="unit_of_measure" type="varchar24">
     <retailDoc:annotation>
      <retailDoc:documentation>The unit of measure of the order item.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element name="currency_code" type="varchar23">
     <retailDoc:annotation>
      <retailDoc:documentation>the currency ISO code</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element name="completed_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The additional amount picked up for the item in this
                               transaction. It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element name="cancelled_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The additional amount cancelled for the item in this
                               transaction. It is always positive.</retailDoc:documentation>
		 </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="repriced_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The additional original amount repriced during pickup for the 
                               item in this transaction. It is always positive.</retailDoc:documentation>
		 </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="completed_new_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The additional amount picked up with new price for the item 
                               in this transaction. It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="completed_discount_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The discount amount applied to item units picked
                               up in this transaction for this order line item.
                               It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="cancelled_discount_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The discount amount applied to item units
                               cancelled in this transaction for this order line
                               item. It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="repriced_discount_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The original discount amount applied to item units 
                               repriced during pickup in this transaction for this 
                               order line item. It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="completed_new_discount_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The new discount amount applied to item units picked
                               up with new price in this transaction for this order 
                               line item. It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="completed_tax_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The tax amount applied to item units picked up in
                               this transaction for this order line item. It is
                               always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="cancelled_tax_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The tax amount applied to item units cancelled in
                               this transaction for this order line item. It is
                               always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="repriced_tax_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The original tax amount applied to item units repriced 
                               during pickup in this transaction for this order line item. 
                               It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="completed_new_tax_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The new tax amount applied to item units picked up with 
                               new price in this transaction for this order line item. It is
                               always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="completed_inc_tax_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The VAT tax amount applied to item units picked
                               up in this transaction for this order line item.
                               It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="cancelled_inc_tax_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The VAT tax amount applied to item units
                               cancelled in this transaction for this order line
                               item. It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="repriced_inc_tax_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The original VAT tax amount applied to item units repriced 
                               during pickup in this transaction for this order line item. 
                               It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="completed_new_inc_tax_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The new VAT tax amount applied to item units picked up with 
                               new price in this transaction for this order line item.
                               It is always positive.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="paid_amount" type="number20_4">
     <retailDoc:annotation>
      <retailDoc:documentation>The additional amount paid for the item in this
                               transaction. This amount can be negative if this
                               is a refund for a cancelled order item.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" name="serial_number" type="varchar240">
     <retailDoc:annotation>
      <retailDoc:documentation>The serial number of the order item entered in
                               pick up process.</retailDoc:documentation>
	 </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" ref="dis:DiscntLinePkColVo">
     <retailDoc:annotation>
      <retailDoc:documentation>A collection of discounts applied to item units
                               picked up or cancelled in this transaction for
                               this order line item.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element minOccurs="0" ref="tax:TaxLinePkColVo">
     <retailDoc:annotation>
      <retailDoc:documentation>A collection of tax applied to item units picked
                               up or cancelled in this transaction for this
                               order line item.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
    <xs:element  minOccurs="0" ref="NewPricePkItems">
     <retailDoc:annotation>
      <retailDoc:documentation>Picked up order items with new price.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
   </xs:sequence>
  </xs:complexType>
 </xs:element>
 <xs:element name="NewPricePkItems">
  <retailDoc:annotation>
   <retailDoc:documentation>Picked up order items with new price</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:complexType>
   <xs:sequence>
    <xs:element ref="itm:CustOrdItmColDesc">
     <retailDoc:annotation>
      <retailDoc:documentation>A collection picked up order items with new price.</retailDoc:documentation>
     </retailDoc:annotation>
    </xs:element>
   </xs:sequence>
  </xs:complexType>
 </xs:element>
 <xs:simpleType name="varchar23">
  <retailDoc:annotation>
   <retailDoc:documentation>This type can hold a string of max length of 3
                            characters.</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:restriction base="xs:string">
   <xs:maxLength value="3"/>
  </xs:restriction>
 </xs:simpleType>
 <xs:simpleType name="varchar24">
  <retailDoc:annotation>
   <retailDoc:documentation>This type can hold a string of max length of 4
                            characters.</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:restriction base="xs:string">
   <xs:maxLength value="4"/>
  </xs:restriction>
 </xs:simpleType>
 <xs:simpleType name="varchar240">
  <retailDoc:annotation>
   <retailDoc:documentation>This type can hold a string of max length of 40
                            characters.</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:restriction base="xs:string">
   <xs:maxLength value="40"/>
  </xs:restriction>
 </xs:simpleType>
 <xs:simpleType name="varchar248">
  <retailDoc:annotation>
   <retailDoc:documentation>This type can hold a string of max length of 48
                            characters.</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:restriction base="xs:string">
   <xs:maxLength value="48"/>
  </xs:restriction>
 </xs:simpleType>
 <xs:simpleType name="number4">
  <retailDoc:annotation>
   <retailDoc:documentation>This type can hold a number of digits count 4.</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:restriction base="xs:int">
   <xs:totalDigits value="4"/>
  </xs:restriction>
 </xs:simpleType>
 <xs:simpleType name="number12_4">
  <retailDoc:annotation>
   <retailDoc:documentation>This type can hold a number of digits count 12 and
                            fraction digit count of 4.</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:restriction base="xs:decimal">
   <xs:totalDigits value="12"/>
   <xs:fractionDigits value="4"/>
  </xs:restriction>
 </xs:simpleType>
 <xs:simpleType name="number20_4">
  <retailDoc:annotation>
   <retailDoc:documentation>This type can hold a number of digits count 20 and
                            fraction digit count of 4.</retailDoc:documentation>
  </retailDoc:annotation>
  <xs:restriction base="xs:decimal">
   <xs:totalDigits value="20"/>
   <xs:fractionDigits value="4"/>
  </xs:restriction>
 </xs:simpleType>
</xs:schema>