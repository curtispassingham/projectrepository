<?xml version="1.0" encoding="UTF-8"?>
<xs:schema elementFormDefault="qualified" version="1.0" targetNamespace="http://www.oracle.com/retail/integration/base/bo/ItPrHdrCriVo/v1" xmlns="http://www.oracle.com/retail/integration/base/bo/ItPrHdrCriVo/v1" xmlns:retailDoc="http://www.w3.org/2001/XMLSchema" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="ItPrHdrCriVo">
        <retailDoc:annotation>
            <retailDoc:documentation>Query criteria to find item price information.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="store_id" type="number10">
                    <retailDoc:annotation>
                        <retailDoc:documentation>SIM store internal identifier</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="from_effective_date" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include prices with an effective date equal to or after this date.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="to_effective_date" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include prices with an effective date equal to or before this date.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="from_end_date" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include prices with an end date equal to or after this end date.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="to_end_date" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include prices with an end date equal to or before this end date.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="department_id" type="number12" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include prices with items with this merchandise hierarchy department identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="class_id" type="number12" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include prices with items with this merchandise hierarchy class identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="subclass_id" type="number12" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include prices with items with this merchandise hierarchy subclass identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="item_id" type="varchar225" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include only prices for this particular sku.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="status" type="it_pr_status">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include only prices in this status. Valid values are APPROVED, PENDING, ACTIVE, COMPLETED, REJECTED, TICKET_LIST, NEW, DEFAULT, DELETED, NO_VALUE, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="price_type" type="it_pr_price_type">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include only prices of this type. Valid values are CLEARANCE, PROMOTIONAL, PERMANENT, NO_VALUE, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="promotion_id" type="number10" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Include only prices associated with this promotion.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="search_limit" type="number3">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Maximum number of items to return from the query.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="it_pr_status">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="APPROVED" />
            <xs:enumeration value="PENDING" />
            <xs:enumeration value="ACTIVE" />
            <xs:enumeration value="COMPLETED" />
            <xs:enumeration value="REJECTED" />
            <xs:enumeration value="TICKET_LIST" />
            <xs:enumeration value="NEW" />
            <xs:enumeration value="DEFAULT" />
            <xs:enumeration value="DELETED" />
            <xs:enumeration value="NO_VALUE" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="it_pr_price_type">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="CLEARANCE" />
            <xs:enumeration value="PROMOTIONAL" />
            <xs:enumeration value="PERMANENT" />
            <xs:enumeration value="NO_VALUE" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar225">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 25 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="25" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number3">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 3.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:int">
            <xs:totalDigits value="3" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="10" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number12">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 12.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="12" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
