<?xml version="1.0" encoding="UTF-8"?>
<xs:schema elementFormDefault="qualified" version="1.0" targetNamespace="http://www.oracle.com/retail/integration/base/bo/TsfShipCtnDesc/v1" xmlns="http://www.oracle.com/retail/integration/base/bo/TsfShipCtnDesc/v1" xmlns:retailDoc="http://www.w3.org/2001/XMLSchema" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:StrInvExtAttDesc="http://www.oracle.com/retail/integration/base/bo/StrInvExtAttDesc/v1">
    <xs:import namespace="http://www.oracle.com/retail/integration/base/bo/StrInvExtAttDesc/v1" schemaLocation="../../StrInvExtAttDesc/v1/StrInvExtAttDesc.xsd">
        <retailDoc:annotation>
            <retailDoc:documentation>It's a referenced element. For detailed description, please refer referenced element doc.</retailDoc:documentation>
        </retailDoc:annotation>
    </xs:import>
    <xs:element name="TsfShipCtnDesc">
        <retailDoc:annotation>
            <retailDoc:documentation>Detailed information about a transfer shipment container.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="tsf_ship_carton_id" type="number15">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique transfer shipment container identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="store_id" type="number10">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique store identifier from where shipment is created.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="external_id" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The external identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="tsf_ship_id" type="number15">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The transfer shipment identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="status" type="tsf_ship_ctn_status">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The current status of the container. Valid values are NEW, IN_PROGRESS, COMPLETED, SHIPPED, CANCELED, UNKNOWN, NO_VALUE.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="destination_type" type="tsf_ship_dest_type">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The destination type of the destination the transfer shipment is being shipped to. Valid values are FINISHER, STORE, WAREHOUSE, UNKNOWN.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="destination_name" type="varchar2128">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The destination name of the destination the transfer shipment is being shipped to.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="hierarchy_level" type="tsf_hierarchy_level">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The container restricted hierarchy level. Valid values are NONE, DEPT, CLASS, SUBCLASS.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="TsfShipCtnSize" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The container size.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="weight" type="number12_4" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The weight of the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="weight_uom" type="varchar24" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unit of measure of the weight of the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="tracking_number" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The tracking number for the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="create_user" type="varchar2128">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The user who created the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="update_user" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The user who last updated the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="approval_user" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The user who approved the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="create_date" type="xs:dateTime">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The date the transfer shipment container was created.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="update_date" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The most recent date when the transfer shipment container was updated.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="approval_date" type="xs:dateTime" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The date when the transfer shipment container was approved.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="use_available" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>True if container uses only available inventory.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="order_related" type="xs:boolean">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Indicates if any customer orders are associated with the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="TsfShipCtnItm" minOccurs="0" maxOccurs="5000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>A collection of items in the container.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="TsfShipCtnSize">
        <retailDoc:annotation>
            <retailDoc:documentation>Carton Dimension attributes</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="id" type="number15">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique carton size identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="store_id" type="number10" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique store identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="description" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The description of the dimensions.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="height" type="number20_4" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The height of the carton.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="width" type="number20_4" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The width of the carton.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="length" type="number20_4" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The length of the carton.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="unit_of_measure" type="varchar2128">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Unit of measure for the measurements.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="TsfShipCtnItm">
        <retailDoc:annotation>
            <retailDoc:documentation>Transfer Shipment Container Line Item.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="line_item_id" type="number15">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique line item identifier.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="item_id" type="varchar225">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The unique identifier of the sku.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="description" type="varchar2400" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Description of the item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="case_size" type="number10_2" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Case size of the item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="quantity" type="number10">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Quantity to be shipped.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="shipment_reason_id" type="number15" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The identifier of the reason for the item shipment.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="transfer_id" type="number12">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The document id associated with this item and quantity.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="cust_ord_ext_id" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The external identifier of a customer order if one is associated to the item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="ful_ord_ext_id" type="varchar2128" minOccurs="0">
                    <retailDoc:annotation>
                        <retailDoc:documentation>The external identifier of a fulfillment order if one is associated to the item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element name="uin_col" type="varchar2128" minOccurs="0" maxOccurs="5000">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Collection of all the unique identifiers/serial numbers associated with the line item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
                <xs:element ref="StrInvExtAttDesc:StrInvExtAttDesc" minOccurs="0" maxOccurs="999">
                    <retailDoc:annotation>
                        <retailDoc:documentation>Collection of extended attributes associated with the line item.</retailDoc:documentation>
                    </retailDoc:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="tsf_ship_ctn_status">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="NEW" />
            <xs:enumeration value="IN_PROGRESS" />
            <xs:enumeration value="COMPLETED" />
            <xs:enumeration value="SHIPPED" />
            <xs:enumeration value="CANCELED" />
            <xs:enumeration value="UNKNOWN" />
            <xs:enumeration value="NO_VALUE" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="tsf_ship_dest_type">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="FINISHER" />
            <xs:enumeration value="STORE" />
            <xs:enumeration value="WAREHOUSE" />
            <xs:enumeration value="UNKNOWN" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="tsf_hierarchy_level">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold an enumerated string of max length of 20 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="20" />
            <xs:enumeration value="NONE" />
            <xs:enumeration value="DEPT" />
            <xs:enumeration value="CLASS" />
            <xs:enumeration value="SUBCLASS" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar24">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 4 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="4" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar225">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 25 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="25" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar2128">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 128 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="128" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="varchar2400">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a string of max length of 400 characters.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:string">
            <xs:maxLength value="400" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="10" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number10_2">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 10 and fraction digit count of 2.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:decimal">
            <xs:totalDigits value="10" />
            <xs:fractionDigits value="2" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number12">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 12.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="12" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number12_4">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 12 and fraction digit count of 4.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:decimal">
            <xs:totalDigits value="12" />
            <xs:fractionDigits value="4" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number15">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 15.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:long">
            <xs:totalDigits value="15" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="number20_4">
        <retailDoc:annotation>
            <retailDoc:documentation>This type can hold a number of digits count 20 and fraction digit count of 4.</retailDoc:documentation>
        </retailDoc:annotation>
        <xs:restriction base="xs:decimal">
            <xs:totalDigits value="20" />
            <xs:fractionDigits value="4" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
