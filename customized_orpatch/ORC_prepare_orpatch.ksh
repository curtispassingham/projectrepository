#! /bin/ksh

#-------------------------------------------------------------------------------
#  File:  ORC_prepare_orpatch.ksh
#  Desc:  This shell script prepares "orpatch" to be able to install patches for ADEO database schemas.
#-------------------------------------------------------------------------------


echo "Temporarily setting TNS_ADMIN to $RETAIL_HOME/orpatch/rms_wallet...."
export TNS_ADMIN=$RETAIL_HOME/orpatch/rms_wallet
[ -z "$RETAIL_HOME" ] && echo "Need to set RETAIL_HOME environment variable, exiting... " && exit 1;
[ -z "$ORACLE_HOME" ] && echo "Need to set ORACLE_HOME environment variable, exiting... " && exit 1;
[ -z "$TNS_ADMIN" ] && echo "Need to set TNS_ADMIN environment variable, exiting... " && exit 1;

if [[ ! -d $RETAIL_HOME/orpatch ]]; then
        echo "Can't find orpatch installation, please make sure environment variables are set correctly."
        exit 1
fi

if [[ -f $RETAIL_HOME/orpatch/config/env_info.cfg ]]; then
        echo "Found orpatch installation ${RETAIL_HOME}/orpatch/config/env_info.cfg"

else

        echo "Can't find orpatch installation, please make sure environment variables are set correctly."
        exit 1

fi

RET_HOME=`cat $RETAIL_HOME/orpatch/config/env_info.cfg | grep "ACTION_DBSQL_RMS=N" | wc -l`

if [[ $RET_HOME -gt 0 ]]; then
        echo "Not a valid RETAIL_HOME for this installation, it must be executed on the database's RETAIL_HOME"
        echo "Current RETAIL_HOME: ${RETAIL_HOME}"
        exit 1
fi


function check_db {
  CONNECTION=$1

  RETVAL=`$ORADBHOME/bin/sqlplus -s $CONNECTION <<EOF
SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF
SELECT 'Alive' FROM dual;
EXIT;
EOF`

  if [ "$RETVAL" = "Alive" ]; then
    DB_OK=0
  else
    DB_OK=1
  fi
}

ORADBHOME=`cat $RETAIL_HOME/orpatch/config/env_info.cfg | grep DB_ORACLE_HOME | cut -d"=" -f2`


MK=`$ORADBHOME/bin/mkstore | grep "Oracle Secret Store Tool" | wc -l`

if [[ $MK -ne 1 ]]; then

        echo "mkstore command not available, please make sure environment variables are set correctly."
        exit 1

fi

TIMESTAMP=`date +%Y%m%d%H%M%S`
echo ""
echo "###################################################################################################################################"
echo "----- THIS SCRIPT SHOULD BE EXECUTED ON THE RMS DATABASE SERVER ONLY (DATABASE RETAIL HOME)-----"
echo "###################################################################################################################################"
echo ""
echo "The script will modify configuration under $RETAIL_HOME/orpatch/config directory, setup wallet files for ##### schemas"
echo "You will need:"
echo "-The schema username and password"
echo "-The application name"
echo "-The password for the wallet"
echo "Please do NOT proceed without these information"
echo "The script creates backup of any files modified during the installation"
echo "In case of any failure, please restore the following backup before restarting this script:"
echo "$RETAIL_HOME/orpatch/config/env_info.cfg_${TIMESTAMP}"
echo "TNS_ADMIN=$TNS_ADMIN"
echo "###################################################################################################################################"
echo "Would you like to continue with the installation? (Y/N)"

read ANSWER

if [[ $ANSWER != 'Y' ]]; then
        echo "Exiting..."
        exit 0
fi


echo "Please enter the schema name"
read DB_SCHEMA
echo "Please enter the password for ${DB_SCHEMA}:"
stty -echo
read -s DB_PASSWORD
stty echo


rmswallet=`cat $RETAIL_HOME/orpatch/config/env_info.cfg | grep DBSQL_RMS_WALLET | cut -d"=" -f2`
if [[ ! -d $RETAIL_HOME/orpatch/$rmswallet ]]; then
        echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Couldn't find the location of RMS wallet file"
        exit 1
fi


rmsds=`cat $RETAIL_HOME/orpatch/config/env_info.cfg | grep DBSQL_RMS_CONNECT | cut -d"=" -f2 | cut -d"@" -f2`
echo "----------------------------"
echo "RMSDS $rmsds"
echo "----------------------------"

if [[ ! -f $RETAIL_HOME/orpatch/$rmswallet/tnsnames.ora.$rmsds ]]; then
        echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Error, please make sure the DBSQL_RMS_CONNECT variable properly set in env_info.cfg"
        exit 1
fi



STR=`ls $RETAIL_HOME/orpatch/$rmswallet/tnsnames.ora.$rmsds | cut -d"." -f3`


check_db $DB_SCHEMA/$DB_PASSWORD@$STR

if [[ $DB_OK -ne 0 ]]; then
        echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Error while connecting to database, exiting..."
        exit 1
fi

DB_SCHEMA_LOWER_CASE=$(echo "$DB_SCHEMA"| awk '{print tolower($0)}')


echo "Please enter the application name:"
stty -echo
read -s APP_NAME
stty echo

echo "Please enter the password for the wallet file located under $RETAIL_HOME/orpatch/$rmswallet/:"
stty -echo
read -s WALLET_PASSWRD
stty echo



CHK=`cat $RETAIL_HOME/orpatch/config/env_info.cfg | grep ${DB_SCHEMA} | wc -l`

if [[ $CHK -ne 0 ]]; then
        echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: ${DB_SCHEMA} schema already in the configuration, please verify the installation manually."
        exit 1
fi

OUTPUT=`echo $WALLET_PASSWRD | $ORADBHOME/bin/mkstore -wrl $RETAIL_HOME/orpatch/$rmswallet/ -list`
CHK=`echo $OUTPUT | grep "incorrect password" | wc -l`

if [[ $CHK -eq 1 ]]; then
        echo    "#      [`date +\"%Y%m%d %H:%M:%S\"`]: Error while verifying wallet password."
        exit 1
fi

echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Backup \"env_info.cfg\" to \"env_info.cfg_${TIMESTAMP}\""

cp $RETAIL_HOME/orpatch/config/env_info.cfg $RETAIL_HOME/orpatch/config/env_info.cfg_${TIMESTAMP}

echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Appending custom entries to \"env_info.cfg\""

echo "" >> $RETAIL_HOME/orpatch/config/env_info.cfg
echo "#CUSTOM APP ${APP_NAME}" >> $RETAIL_HOME/orpatch/config/env_info.cfg
echo "PRODUCT_${APP_NAME}=Y" >> $RETAIL_HOME/orpatch/config/env_info.cfg
echo "DBSQL_${DB_SCHEMA}_CONNECT=/@${DB_SCHEMA_LOWER_CASE}" >> $RETAIL_HOME/orpatch/config/env_info.cfg
echo "DBSQL_${DB_SCHEMA}_WALLET=${DB_SCHEMA_LOWER_CASE}_wallet" >> $RETAIL_HOME/orpatch/config/env_info.cfg
echo "DBSQL_${DB_SCHEMA}_RUN_DBC=N" >> $RETAIL_HOME/orpatch/config/env_info.cfg
echo "ACTION_DBSQL_${DB_SCHEMA}=$HOSTNAME" >> $RETAIL_HOME/orpatch/config/env_info.cfg

echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Updated \"env_info.cfg\""

echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Creating waller form ${DB_SCHEMA}"



if [[ -d $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet ]]; then
        mv $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet_$TIMESTAMP
fi


mkdir $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet
RES=$?

if [[ $RES -ne 0 ]]; then
        echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: ERROR while creating wallet directory."
        exit 1
fi



cp $RETAIL_HOME/orpatch/$rmswallet/*wallet* $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/

echo "WALLET_LOCATION = (SOURCE = (METHOD = FILE) (METHOD_DATA = (DIRECTORY = ${RETAIL_HOME}/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet)))" > $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/sqlnet.ora
echo "SQLNET.WALLET_OVERRIDE=TRUE" >> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/sqlnet.ora
echo "SSL_CLIENT_AUTHENTICATION=TRUE" >> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/sqlnet.ora
echo "#SQLNET.AUTHENTICATION_SERVICES=(TCPS,NTS)" >> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/sqlnet.ora


echo "ifile=${RETAIL_HOME}/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/tnsnames.ora.${DB_SCHEMA_LOWER_CASE}" > $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/tnsnames.ora



echo "#!/bin/sh" > $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "" >> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "ORACLE_HOME=${ORADBHOME}">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "WALLET=${RETAIL_HOME}/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "ALIAS=${DB_SCHEMA_LOWER_CASE}">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}_rms.sh
echo "echo \"\"">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "echo \"Note: when prompted for your secret/Password, enter the NEW value you want to use.\"">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "echo \"\"">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "echo -n \"Enter the username to associate with \$ALIAS (this can be the existing username or a new username): \"">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "read USERNAME">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "echo \"\"">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "echo \"Running \$ORACLE_HOME/bin/mkstore -wrl \$WALLET -modifyCredential \$ALIAS \$USERNAME\"">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "echo \"\"">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "\$ORACLE_HOME/bin/mkstore -wrl \$WALLET -modifyCredential \$ALIAS \$USERNAME">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "exit \$?">> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh
echo "" >> $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh

chmod +x $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/update-${DB_SCHEMA_LOWER_CASE}.sh


#ALIAS=`cat $RETAIL_HOME/orpatch/config/env_info.cfg | grep DBSQL_RMS_CONNECT | cut -d"=" -f2 | cut -d"@" -f2`


cp $RETAIL_HOME/orpatch/$rmswallet/tnsnames.ora.$rmsds $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/tnsnames.ora.${DB_SCHEMA_LOWER_CASE}


sed -i "s/$rmsds/$DB_SCHEMA_LOWER_CASE/g" "$RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/tnsnames.ora.${DB_SCHEMA_LOWER_CASE}"




echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Creating wallet file for ${DB_SCHEMA_LOWER_CASE} schema"
echo $WALLET_PASSWRD | $ORADBHOME/bin/mkstore -wrl $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/ -modifyEntry oracle.security.client.connect_string1 $DB_SCHEMA_LOWER_CASE > /dev/null 2>&1
echo $WALLET_PASSWRD | $ORADBHOME/bin/mkstore -wrl $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/ -modifyEntry oracle.security.client.password1 $DB_PASSWORD > /dev/null 2>&1
echo $WALLET_PASSWRD | $ORADBHOME/bin/mkstore -wrl $RETAIL_HOME/orpatch/${DB_SCHEMA_LOWER_CASE}_wallet/ -modifyEntry oracle.security.client.username1 $DB_SCHEMA > /dev/null 2>&1



echo "# [`date +\"%Y%m%d %H:%M:%S\"`]: Finished updating wallet file"

echo "###################################################################################################################################"
echo "Installation completed."
echo "Please don't forget to install the ORC patch for orpatch before trying to patch ${DB_SCHEMA} schema"
echo ""
exit 0