set serveroutput on size unlimited
declare cursor c_invalids IS
	select object_type, object_name, 'compile' c
		from   user_objects
		where  status <> 'VALID'
		and    object_type = 'TYPE'
	union all
	select 'TYPE', object_name, 'compile body' c
		from   user_objects
		where  status <> 'VALID'
		and    object_type = 'TYPE BODY'
	union all
	select object_type, object_name, 'compile' c
		from   user_objects
		where  status <> 'VALID'
		and    object_type not in ( 'PACKAGE BODY', 'TYPE BODY', 'TYPE', 'PACKAGE' )
	union all
	select object_type, object_name, 'compile' c
		from   user_objects
		where  status <> 'VALID'
		and    object_type = 'PACKAGE'
	union all
	select 'PACKAGE', object_name, 'compile body' c
		from   user_objects
		where  status <> 'VALID'
		and    object_type = 'PACKAGE BODY';
	quoted_object	varchar2(130);
BEGIN
	for rec_invalids in c_invalids
	loop
	begin
		quoted_object:=sys.dbms_assert.enquote_name(rec_invalids.object_name,FALSE);
		execute immediate 'alter ' || rec_invalids.object_type || ' ' || quoted_object || ' ' || rec_invalids.c;
			dbms_output.put_line( 'info: recompiled ' || quoted_object );
		exception
			when others then
			dbms_output.put_line( 'error: recompile failed ' || quoted_object );
		end;
	end loop;
end;
/
