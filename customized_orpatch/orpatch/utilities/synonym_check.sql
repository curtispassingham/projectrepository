-- **************************************************************************
-- Copyright (C) 2013,2014, Oracle and/or its affiliates. All rights reserved.
--
-- This script will identify missing local synonyms or local objects for a target
-- schema in a user's schema.  User's target schema is calculated by checking
-- to see if over half of their synonyms are referencing the schema.
--
-- Once identified, script is generated to create local synonyms pointing to
-- the target schema objects for any missing object reference
--
-- **************************************************************************
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON size unlimited

DECLARE
	run_schema				varchar2(30);
	quoted_owner			varchar2(32);
    schema_user            	varchar2(30);
	quoted_schema_user		varchar2(32);
    missing_object         	varchar2(130);

    cursor c_get_username(owner_schema in varchar2) is
        select owner
        from   dba_synonyms s
        where  table_owner = owner_schema
		and    owner <> 'PUBLIC'
        having count(synonym_name) >
               (select 0.5 * count(synonym_name) from dba_synonyms where s.owner = owner)
        group by owner, table_owner;

    cursor c_get_missing_object(owner_schema in varchar2,schema_user in varchar2) is
        (select object_name
        from   dba_objects
        where  owner = owner_schema
            and object_type in ('TABLE', 'VIEW', 'CLUSTER', 'FUNCTION', 'PACKAGE', 'PROCEDURE', 'SEQUENCE', 'TYPE')
            and object_name not like 'DBC_%'
            and object_name not like 'BIN$%'
        union
        select synonym_name from dba_synonyms
        where
        owner = owner_schema
        and table_name in ('ARI_INTERFACE_SQL','RMS_NOTIFICATION_REC', 'AQ$RMS_NOTIFICATION_QTAB') and synonym_name in ('ARI_INTERFACE_SQL','RMS_NOTIFICATION_REC','AQ$RMS_NOTIFICATION_QTAB'))
        MINUS
        select object_name
        from   dba_objects
        where  owner = schema_user
        order by 1;
BEGIN
	run_schema:=sys.dbms_assert.schema_name('&_USER');
    dbms_output.put_line('Owning Schema: '||run_schema);
	quoted_owner:=sys.dbms_assert.enquote_name(run_schema,FALSE);
	
    -- Find users who belong to the owning_schema.  Users who have 50% of their
    -- synonyms pointing to the owning_schema.
    open c_get_username(run_schema);
    LOOP
        fetch c_get_username into schema_user;
        --When at end of list of usernames, exit
        if c_get_username%NOTFOUND then
            exit;
        end if;

        dbms_output.put_line('Checking username: '||schema_user);
		quoted_schema_user:=sys.dbms_assert.enquote_name(schema_user,FALSE);

        -- Generate create synonym statements for objects in the owning_schema where
        -- this user does not have a local synonym or local object with the same name.
        open c_get_missing_object(run_schema,schema_user);
        LOOP
            fetch c_get_missing_object into missing_object;
            --When at end of objects, exit
            if c_get_missing_object%NOTFOUND then
                exit;
            end if;
        
            BEGIN
				missing_object:=sys.dbms_assert.enquote_name(missing_object,FALSE);
				
                execute immediate 'CREATE SYNONYM '||quoted_schema_user||'.'||missing_object||' FOR '||quoted_owner||'.'||missing_object;
                dbms_output.put_line('Created synonym '||schema_user||'.'||missing_object||' pointing to '||run_schema);
            EXCEPTION
            WHEN OTHERS THEN
                dbms_output.put_line('Create synonym FAILED '||missing_object||' '||SQLCODE||' - '||SQLERRM);
            END;
        
        END LOOP;
        close c_get_missing_object;
    
    END LOOP;
    close c_get_username;
END;
/
