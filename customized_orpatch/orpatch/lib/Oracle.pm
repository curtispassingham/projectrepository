############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################

&_InitOracleVars();

#########################
# Sub:  SetOracleHome
# Desc: Setup our environment to use a particular ORACLE_HOME
# Args: oracle_home
#########################
sub SetOracleHome {
	my ($oh)=@_;
	
	#Prevent excessive growth of PATH, LD_LIBRARY_PATH, SHLIB_PATH and LIBPATH
	#if we are called over and over in the same session
	#
	#Note that this could cause unintended consequences if someone modifies these variables
	#after the first call to SetOracleHome
	#If you need to do that, call ResetOracleHome($oh) first to reset these variables
	$::ORIG_PATH=$ENV{PATH} if $::ORIG_PATH eq '';
	$::ORIG_LDL_PATH=$ENV{LD_LIBRARY_PATH} if $ORIG_LDL_PATH eq '';
	$::ORIG_SL_PATH=$ENV{SHLIB_PATH} if $ORIG_SL_PATH eq '';
	$::ORIG_LIB_PATH=$ENV{LIB_PATH} if $ORIG_LIB_PATH eq '';
	
	$ENV{ORACLE_HOME}=$oh;
	$ENV{PATH}="${oh}/bin:$::ORIG_PATH";
	$ENV{LD_LIBRARY_PATH}="${oh}/lib:$::ORIG_LDL_PATH";
	$ENV{SHLIB_PATH}="${oh}/lib:$::ORIG_SL_PATH";
	$ENV{LIBPATH}="${oh}/lib:$::ORIG_LIB_PATH";
	
	$::SQLPLUS="$oh/bin/sqlplus";
	return 1;
}

#########################
# Sub:  ResetOracleHome
# Desc: Remove an ORACLE_HOME from environment variables
# Args: oracle_home
#########################
sub ResetOracleHome {
	my ($oh)=@_;
	
	$ENV{PATH}=~s/^${oh}\/bin://;
	$::ORIG_PATH='';
	$ENV{LD_LIBRARY_PATH}=~s/^${oh}\/lib://;
	$::ORIG_LDL_PATH='';
	$ENV{SHLIB_PATH}=~s/^${oh}\/lib://;
	$::ORIG_SL_PATH='';
	$ENV{LIBPATH}=~s/^${oh}\/lib://;
	$::ORIG_LIB_PATH='';

	$::SQLPLUS="sqlplus";
	
	return 1;
}

#########################
# Sub:  ExecSQL
# Desc: Run SQL statements against a database
# Args: 
#   sql to run
#   error level - 0 to ignore, 1 to log, 2 to die, 3 debug on errors, 4 always debug
# Ret:  ref to list of output,ref to list of errors
#########################
sub ExecSQL {
    my ($user_string,$sql,$error_level)=@_;
    $error_level=1 if ($error_level eq '');
	
    my $input_file=&::GetTempName('sql');
	my $output_file=&::GetTempName('log');
    open(SQL_FILE,">$input_file") ||
        die "Unable to write to $input_file: $!";

    print SQL_FILE <<ENDSQL;
$user_string
set feedback off wrap off head off pagesize 0 line 9999 verify off echo off
$sql;
exit;
ENDSQL

    close(SQL_FILE) || &::LogMsg("Unable to close file $input_file: $!");

	(-f $::SQLPLUS) || die "SQLPLUS binary $::SQLPLUS does not exist!";
	
    my $command="$::SQLPLUS -S -L <$input_file >$output_file 2>&1";
    my @cmd_output=`$command`;
	my $status=$?;
	
	#if ($status!=0) {
	#	&::DebugOutput("cmd output",@cmd_output);
	#}
	
	open(OUTPUT_FILE,"<$output_file") || die "Unable to open file $output_file: $!";
	my @output=();
	while(<OUTPUT_FILE>) {
		chomp;
		push(@output,$_);
	}
	close(OUTPUT_FILE);
	unlink($output_file);
	unlink($input_file);

    &::DebugOutput('sqlplus',@output) if ($error_level==4);

    my @errors=grep(/ORA\-|SP2\-/,@output);

    if (scalar(@errors)!=0) {
        &::DebugOutput('sqlplus',@output) if ($error_level==3);

        if ($error_level>0) {
            &::LogMsg("Errors while running SQL $sql",@errors);

            if ($error_level==2) {
                die "Error while running SQL: $errors[0]";
            }
        }
    }

    if (wantarray) {
        return(\@output,\@errors);
    } else {
        return(scalar(@errors)==0);
    }
}

#Initialize our SQLPLUS variable
sub _InitOracleVars {
	$::SQLPLUS='sqlplus';
}


1;