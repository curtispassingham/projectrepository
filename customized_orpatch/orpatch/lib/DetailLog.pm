############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package DetailLog;

sub CreateLog {
	my ($category,$sub_category,$source_file,$detail_ref,$type)=@_;
	$type='log' if $type eq '';
	
	my $log_dir=&GetDetailLogDir($category,$sub_category);
	
	#Make sure source_file was given as a basename
	my ($sdir,$sfile)=&::GetFileParts($source_file,2);
	
	my $log_file=&::ConcatPathString($log_dir,"${sfile}.${type}");
	
	if (-f $log_file) {
		#&::RemoveFile($log_file);
		unlink($log_file);
	}
	
	#Now dump our output
	unless (open(DETAIL_LOG,">$log_file")) {
		&::LogMsg("Unable to open detail log $log_file: $!");
		return 0;
	}
	
	foreach my $line (@$detail_ref) {
		chomp($line);
		print DETAIL_LOG "$line\n";
	}
	close(DETAIL_LOG);
	
	return 1;
}

sub GetDetailLogDir {
	my ($category,$sub_category)=@_;
	
	#Determine the global log directory
	my $lfile=$::LOG->GetLogFile();
	my ($ldir)=&::GetFileParts($lfile,2);
	
	my $log_dir=&::ConcatPathString($ldir,'detail_logs',$category);
	if ($sub_category ne '') {
		$log_dir=&::ConcatPathString($log_dir,$sub_category);
	}
	
	&::CreateDirs('0755',$log_dir);
	
	return $log_dir;
}

sub CleanDetailLogDir {
	my ($category,$sub_category)=@_;
	
	my $log_dir=&GetDetailLogDir($category,$sub_category);

	#This is simply to silence the warning regarding otherwise only having
	#used the reference to $::LOG once
	my $dummy=$::LOG;
	
    unless (opendir(DIR,"$log_dir")) {
		&::LogMsg("Unable to open directory $log_dir: $!");
		return 0;
	}
    while(my $this_file=readdir(DIR)) {
		next if ($this_file eq '.' || $this_file eq '..');
		
        my $full_file=&::ConcatPathString($log_dir,$this_file);

        next unless -f $full_file;
        unlink($full_file);
    }
    closedir(DIR);
	return 1;
}

1;