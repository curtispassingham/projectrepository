#!/usr/bin/perl
############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

($ENV{RETAIL_HOME} eq '') && (die "ERROR: RETAIL_HOME must be defined!");
$::ORP_DIR="$ENV{RETAIL_HOME}/orpatch";
$::ORP_LIB_DIR="$::ORP_DIR/lib";

require "$::ORP_LIB_DIR/orpatch_include.pl";

$::ORP_CONFIG=new ConfigFile();

$::ORP_MY_NAME='orcustomreg';
$::ORP_REGISTER_MODE='REGISTER';
$::ORP_CUSTOMIZED=1;
@::ORP_REGISTER_LIST=();

&ValidateConfig();

@::ACTIONS=();
&::PopulateActions(\@::ACTIONS,$::ORP_CONFIG);
$::ORP_GLOBAL_STATE->SetActionList(@::ACTIONS);

my $exit_status=0;

if ($::ORP_REGISTER_MODE eq 'REGISTER') {
	$exit_status=&RegisterFiles(\@::ORP_REGISTER_LIST,\@::ACTIONS,$::ORP_CUSTOMIZED);
} elsif ($::ORP_REGISTER_MODE eq 'LIST') {
	$exit_status=&ListCustomFiles(\@::ACTIONS);
}

&::ExitORCustomReg($exit_status);

#########################
# Sub:  RegisterFiles
# Desc: Register (or unregister) a list of file
#########################
sub RegisterFiles {
	my ($reg_list_ref,$action_list_ref,$customized)=@_;
	
	my $dest_dir=$ENV{RETAIL_HOME};
	
	my %fq_file_list=();
	
	#all files must be relative to RETAIL_HOME, and must exist
	foreach my $file (@$reg_list_ref) {
		my $full_file='';
		if ($file=~m<^$dest_dir/>) {
			#Already contains the RETAIL_HOME
			$full_file=$file;
		} else {
			$full_file=&::ConcatPathString($dest_dir,$file);
		}
		
		if (-f $full_file) {
			if (exists($fq_file_list{$full_file})) {
				&::LogMsg("Ignoring duplicate file $full_file");
			} else {
				$fq_file_list{$full_file}=1;
			}
		} else {
			&::LogMsg("File does not exist: $full_file");
			return 1;
		}
	}
	
	foreach my $action (@$action_list_ref) {
		$action->SetORPatchGlobalState($::ORP_GLOBAL_STATE);
	}
	
	my $count=scalar(keys(%fq_file_list));
	my $custom_flag=(($customized==1) ? 'Y' : 'N');
	
	if ($::ORP_MY_NAME eq 'orcustomreg') {
		&::LogMsg("Registering $count files as CUSTOMIZED=$custom_flag");
	} else {
		if ($customized==1) {
			&::LogMsg("Registering $count files in DBManifest tables");
		} else {
			&::LogMsg("Removing $count files from DBManifest table");
		}
	}

	#Register each file
	foreach my $full_file (sort(keys(%fq_file_list))) {
		&::LogMsg("Processing $full_file");
		
		my ($full_dest,$dest_manifest,$action_handle)=&FindFullDest($full_file,$dest_dir,$::ORP_GLOBAL_STATE);
		
		if ($full_dest eq '' || $action_handle eq '') {
			return 1;
		}

		my $rel_file_name=&::GetRelativePathName($full_file,$full_dest);
		if ($::ORP_MY_NAME eq 'orcustomreg') {
			&::LogMsg("  Found owning manifest: $dest_manifest");
			if (&RegisterCustomizedFile($rel_file_name,$dest_manifest,$custom_flag)!=1) {
				return 1;
			}
		} else {
			&::LogMsg("  Found owning action: ".$action_handle->GetActionName());
			if (&UpdateDBManifest($rel_file_name,$dest_manifest,$action_handle,$customized)!=1) {
				return 1;
			}
		}
		&::LogMsg("Registration of $full_file successful");
	}
	
	return 0;
}

#########################
# Sub:  FindFullDest
# Desc: Find the full destination and destination manifest that tracks a specific file
#########################
sub FindFullDest {
	my ($full_file,$dest_dir,$global_state)=@_;
	
	my $manifest_list_ref=$global_state->GetEnvManifestList();
	foreach my $ref (@$manifest_list_ref) {
		my ($action_name,$full_dest,$dest_manifest)=@$ref;

		#if the full file path is inside the full destination path, then this manifest should contain the file
		if ($full_file=~m<^$full_dest/>) {
			my $action_handle=$global_state->GetActionHandle($action_name);
			if ($action_handle eq '') {
				&::LogMsg("ERROR: Unable to locate handle for $action_name for $full_file");
				return ('','','');
			}
			
			if ($::ORP_MY_NAME eq 'ordbmreg') {
				#Make sure the owning action is a DBSQL-based action
				if (!$action_handle->isa('ORPAction::DBSQL')) {
					&::LogMsg("ERROR: $full_file is not owned by a DBSQL action, dbmanifest table does not apply!");
					return ('','','');
				}
			}
		
			return ($full_dest,$dest_manifest,$action_handle);
		}
	}
	
	&::LogMsg("ERROR: Unable to locate env_manifest.csv file which contains $full_file");
	return ('','','');
}

#########################
# Sub:  RegisterCustomizedFile
# Desc: register a file with a destination manifest as customized
#########################
sub RegisterCustomizedFile {
	my ($rel_file,$dest_manifest,$custom_flag)=@_;
	
	#Read the destination manifest
	my @dest_info=&Manifest::ReadManifest($dest_manifest);

	#Find the row we are marking as customized
	foreach my $ref (@dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$custom,$fwp_rev,$future1,$future2,$future3,$future4)=&Manifest::DecodeDestLine($ref);
		if ($dfile eq $rel_file) {
			&::LogMsg("  Updating CUSTOMIZED=$custom_flag (previously $custom)");
			&Manifest::UpdateDestInfo($sfile,$ftype,$frevision,$dfile,$fdesc,$custom_flag,$fwp_rev,$future1,$future2,$future3,$future4,\@dest_info);
			if (&Manifest::SaveDestManifest($dest_manifest,[],\@dest_info,{},1)!=1) {
				#Failed to save/swap manifest
				&::LogMsg("ERROR: Unable to save updated manifest $dest_manifest!");
				return 0;
			}
			#Success!
			return 1;
		}
	}

	&::LogMsg("ERROR: Relative file name $rel_file does not exist in $dest_manifest!");
	
	return 0;
}

#########################
# Sub:  UpdateDBManifest
# Desc: register/unregister a file with the dbmanifest table
#########################
sub UpdateDBManifest {
	my ($rel_file,$dest_manifest,$action_handle,$dbm_insert)=@_;
	
	#Read the destination manifest
	my @dest_info=&Manifest::ReadManifest($dest_manifest);

	#Find the manifest row for the file we are registering
	foreach my $ref (@dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$patch_name,@discard)=&Manifest::DecodeDestLine($ref);
		if ($dfile eq $rel_file) {
			if ($dbm_insert==1) {
				&::LogMsg("  Inserting dbmanifest row for $dfile");
				if ($action_handle->CreateDBManifestTable()!=1) {
					&::LogMsg("Failed to create manifest table!");
					return 0;
				}
				if ($action_handle->UpdateDBManifestTable($dfile,$frevision,$ftype,$patch_name)!=1) {
					&::LogMsg("ERROR: Unable to update dbmanifest!");
					return 0;
				}
			} else {
				&::LogMsg("  Removing dbmanifest row for $dfile");
				if ($action_handle->RemoveFromDBManifestTable($rel_file,$ftype)!=1) {
					return 0;
				}
			}
			#Success!
			return 1;
		}
	}

	&::LogMsg("ERROR: Relative file name $rel_file does not exist in $dest_manifest!");
	
	return 0;
}

#########################
# Sub:  ListCustomFiles
# Desc: List all files that are registered as customized
#########################
sub ListCustomFiles {
	my ($action_list_ref)=@_;

	&::LogMsg("Listing customized files registered with orpatch");
	
	foreach my $action (@$action_list_ref) {
		$action->SetORPatchGlobalState($::ORP_GLOBAL_STATE);
	}
	
	my @custom=();
	my $mf_list_ref=$::ORP_GLOBAL_STATE->GetEnvManifestList();
	foreach my $ref (@$mf_list_ref) {
		my ($name,$full_dest,$dest_manifest)=@$ref;

		my $fcp_custom=&FindCustomFilesInManifest($dest_manifest,$full_dest);
		push(@custom,@$fcp_custom);
	}

	if (scalar(@custom)==0) {
		&::LogMsg("No custom files registered!");
	} else {
		&::LogMsg("Registered customized files:");
		foreach my $file (@custom) {
			&::LogMsg("  $file");
		}
	}
	
	return 1;
}

sub FindCustomFilesInManifest {
	my ($dest_manifest,$full_dest)=@_;
	
	#Read the destination manifest
	my @dest_info=&Manifest::ReadManifest($dest_manifest);

	my @custom=();
	#Find the row we are marking as customized
	foreach my $ref (@dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$custom,@discard)=&Manifest::DecodeDestLine($ref);
		if ($custom=~/^y$/i) {
			push(@custom,&::ConcatPathString($full_dest,$dfile));
		}
	}
	
	return \@custom;
}

#########################
# Sub:  ValidateConfig
# Desc: parse our command-line arguments and validate our configuration
#########################
sub ValidateConfig {
	while(my $arg=shift(@ARGV)) {
		$arg=~tr/a-z/A-Z/;
		if ($arg eq '-UNREGISTER') {
			$::ORP_CUSTOMIZED=0;
		} elsif ($arg eq '-REGISTER') {
			$::ORP_CUSTOMIZED=1;
		} elsif ($arg eq '-F') {
			my $file=shift(@ARGV);
			if ($file eq '') {
				&::LogMsg("-F requires an argument");
				&Usage();
			}
			push(@::ORP_REGISTER_LIST,$file);
		} elsif ($arg eq '-BULK') {
			my $bulk_file=shift(@ARGV);
			if ($bulk_file eq '') {
				&::LogMsg("-BULK requires an argument");
				&Usage();
			}
			if (&ReadBulkFile($bulk_file)!=1) {
				&Usage();
			}
		} elsif ($arg eq '-LIST') {
			$::ORP_REGISTER_MODE='LIST';
		} elsif ($arg eq '-HELP') {
			&Usage();
		} elsif ($arg eq '-DBM') {
			$::ORP_MY_NAME='ordbmreg';
			my $log_file=$::LOG->GetLogFile();
			$log_file=~s/orcustomreg/ordbmreg/;
			$::LOG=new Log($log_file,0,'',0);
		} else {
			&::LogMsg("Unknown argument: $arg");
			&Usage();
		}
	}

	if ($::ORP_REGISTER_MODE eq 'REGISTER' && scalar(@::ORP_REGISTER_LIST)==0) {
		&::LogMsg("No files to register, either -f or -bulk must be specified");
		&Usage();
	}
	
	if ($::ORP_REGISTER_MODE eq 'LIST' && $::ORP_MY_NAME ne 'orcustomreg') {
		&::LogMsg("Unknown argument: -LIST");
		&Usage();
	}
	
	my $restart_file="$::ORP_DIR/logs/${main::ORP_MY_NAME}_restart.state";
	$::ORP_GLOBAL_STATE=new ORPatchGlobalState($::ORP_DIR,$restart_file,$::ORP_CONFIG,$ENV{RETAIL_HOME},'RETAIL_HOME');
	
	unless ($::ORP_GLOBAL_STATE->ValidatePatchlessConfig()==1) {
		exit 1;
	}
	
	my $env_info_cfg="$::ORP_DIR/config/env_info.cfg";
	unless (-f $env_info_cfg) {
		&::LogMsg("Environment info file $env_info_cfg not found");
		exit 1;
	}
	
	if ($::ORP_CONFIG->LoadFile($env_info_cfg)!=1) {
		&::LogMsg("Unable to read env_info.cfg!");
		exit 1;
	}
	
	return 1;
}

sub ReadBulkFile {
	my ($bulk_file)=@_;
	
	&::LogMsg("Reading bulk file: $bulk_file");
	open(BULK,"<$bulk_file") || die "Unable to open $bulk_file: $!";
	my $files=0;
	while(<BULK>) {
		chomp;
		next if (/^#/);
		next if (/^\s*$/);
		push(@::ORP_REGISTER_LIST,$_);
		$files++;
	}
	close(BULK);
	
	&::LogMsg("Added $files files to register from bulk file");
	return 1;
}

sub ExitORCustomReg {
	my ($exit_status)=@_;
	
	#If we are exiting successfully, remove the restart state
	#if ($exit_status==0) {
		#We always remove the restart file as we never load it
		$::ORP_GLOBAL_STATE->ResetRestartState();
	#}
	&::LogMsg("$::ORP_MY_NAME session complete");
	exit $exit_status;
}

sub Usage {
	&::LogMsg("Usage:");
	
	if ($::ORP_MY_NAME eq 'orcustomreg') {
		&::LogMsg("Register, or unregister specified files as custom files:");
	} else {		
		&::LogMsg("Register, or unregister specified files in the dbmanifest:");
	}
	
	&::LogMsg("   $::ORP_MY_NAME [-unregister] [-f <file>] [-bulk <file containing filenames>]");
	
	if ($::ORP_MY_NAME eq 'orcustomreg') {
		&::LogMsg("");
		&::LogMsg("List all customized files in environment:");
		&::LogMsg("   $::ORP_MY_NAME -list");
	}

	exit 1;
}
