############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################

use strict;
package ORPRestorePoint;

use File::Copy;

sub GetBackupModeCreate {
	my $self=shift;
	return 'CREATE';
}

sub GetBackupModeUpdate {
	my $self=shift;
	return 'UPDATE';
}

sub GetBackupModeDelete {
	my $self=shift;
	return 'DELETE';
}

sub GetBackupModeOrphan {
	my $self=shift;
	return 'ORPHAN';
}

#########################
# Sub:  CreateBackupFile
# Desc: Create a backup file for future restores
# Args: file mode      - 'CREATE','UPDATE','DELETE','ORPHAN' (see GetBackupModeXXXXX)
#       perms          - the permissions the source/backup file should have
#       dest file      - full path to dest file
#       dest manifest  - full path to dest manifest
#       dest info      - ref to file's current row in destination manifest
# Ret:  1 on success, 0 on failure
#########################
sub CreateBackupFile {
	my $self=shift;
	my ($file_mode,$perms,$dest_file,$dest_manifest,$dest_info_row_ref)=@_;
	
	my $restore_dir=$self->GetRestoreDir();
	
	my $dest_dir=$self->_GetDestDir();
	
	my $base_file=&::GetRelativePathName($dest_file,$dest_dir);
	my $bkp_file='';
	
	#First make a copy of the existing file
	if ($file_mode eq $self->GetBackupModeCreate()) {
		#Nothing to backup for a new file
	} elsif ($file_mode eq $self->GetBackupModeOrphan()) {
		#Orphan files (listed in manifest but actual file doesn't exist can't be backed up)
		&::LogMsg("WARNING: Dest file $dest_file does not exist, unable to make a backup.");
	} else {
		$bkp_file=$self->_CalculateBackupFileName($restore_dir,$base_file);
		if ($bkp_file eq '') {
			return 0;
		}
		
		#Get the current file permissions
		my $type_perms=(stat($dest_file))[2];
		my $perms_oct=($type_perms & 07777);
		#convert the octal perms back into a string
		$perms=sprintf("%04o",$perms_oct);
	
		my ($bkp_path,$f)=&::GetFileParts($bkp_file,2);
		my $dir_perms='0755';
		&::CreateDirs($dir_perms,$bkp_path);

		$::LOG->LogMsgNoVerbose("Creating backup of $base_file to $bkp_file");
		if (&File::Copy::copy($dest_file,$bkp_file)) {
			#Successful
		} else {
			#Copy failed!
			&::LogMsg("Unable to copy $dest_file to $bkp_file: $!");
			return 0;
		}
		
		#Adjust permissions if desired
		my $files_changed=chmod($perms_oct,$bkp_file);
		if ($files_changed!=1) {
			&::LogMsg("WARNING: Unable to change permissions on $bkp_file: $!");
		}
	}
	
	#Now register the backup file with our map
	my ($proceed,$row_ref)=$self->_RegisterBackupFile($file_mode,$dest_file,$bkp_file,$perms,$dest_manifest,$dest_info_row_ref);
	if ($proceed!=1) {
		return 0;
	}
	
	#Save to our restore map file
	return $self->_AppendMapFile($row_ref);
}

#########################
# Sub:  StartRestorePoint
# Desc: Start a new restore point
# Args: backup dir - the directory name to store this restore point in, will be created relative to RETAIL_HOME/backups
#       patch_info_file - The full path to the source patch_info.cfg file
#       orpatch_restore - 1 if this is a restore for orpatch, 0 if not
# Ret:  1 on sucess, 0 on failure
#########################
sub StartRestorePoint {
	my $self=shift;
	my ($backup_dir,$patch_info_file,$orpatch_restore)=@_;
	
	my $restore_dir=$self->_SetBackupDir($backup_dir);
	
	if (-d $restore_dir) {
		&::LogMsg("Designated restore directory already exists: $restore_dir");
		return 0;
	}
	
	&::LogMsg("Creating backup directory $restore_dir");
	if (&::CreateDirs('0750',$restore_dir)!=1) {
		&::LogMsg("Unable to create directory $restore_dir");
		return 0;
	}
	
	if ($self->_InitMapFile()!=1) {
		return 0;
	}
	
	if (-f $patch_info_file) {
		my ($pidir,$pifile)=&::GetFileParts($patch_info_file,2);
		my $dest_patch_info=&::ConcatPathString($restore_dir,$pifile);
		if (&File::Copy::copy($patch_info_file,$dest_patch_info)) {
			#Successful
		} else {
			#Copy failed!
			&::LogMsg("Unable to copy $patch_info_file to $dest_patch_info: $!");
			return 0;
		}
	} else {
		&::LogMsg("Source patch info: $patch_info_file does not exist");
		return 0;
	}
	
	if ($orpatch_restore==1) {
		#touch the flag so that future restores will know this is an orpatch restore point
		my $orpatch_flag=&::ConcatPathString($restore_dir,$self->_GetORPatchFlag());
		unless(open(FLAG_FILE,">$orpatch_flag")) {
			&::LogMsg("Unable to open $orpatch_flag: $!");
			return 0;
		}
		print FLAG_FILE "#Do not restore\n";
		close(FLAG_FILE);
	}
	
	return 1;
}

#########################
# Sub:  ContinueRestorePoint
# Desc: Continue an in process restore point
# Args: restore dir - the directory where the restore point was created
# Ret:  1 on sucess, 0 on failure
#########################
sub ContinueRestorePoint {
	my $self=shift;
	my ($restore_dir)=@_;
	
	if (!-d $restore_dir) {
		&::LogMsg("Designated backup directory does not exist: $restore_dir");
		return 0;
	}
	$self->_SetRestoreDir($restore_dir);
	
	return $self->_LoadFile(0);
}

#########################
# Sub:  RestoreFiles
# Desc: Revert the environment based on the files from this restore point
# Args: restore dir - the directory where the restore point was created
# Ret:  1 on sucess, 0 on failure
#########################
sub RestoreFiles {
	my $self=shift;
	my ($restore_dir,$any_action,$global_state)=@_;
	
	if (!-d $restore_dir) {
		#Check if they gave us a directory inside $RETAIL_HOME/backups by mistake
		my $old_restore_dir=$restore_dir;
		$restore_dir=$self->_SetBackupDir($restore_dir);
		if (!-d $restore_dir) {
			&::LogMsg("Designated restore directory does not exist: $old_restore_dir ($restore_dir)");
			return 0;
		} else {
			&::LogMsg("Found $old_restore_dir as $restore_dir, using that");
		}
	}
	
	$self->_SetRestoreDir($restore_dir);
	
	my $orpatch_flag=&::ConcatPathString($restore_dir,$self->_GetORPatchFlag());
	if (-f $orpatch_flag) {
		&::LogMsg("Restore directory is for orpatch update, Unable to revert");
		&::LogMsg("To revert a patch, use the restore point directory for the patch, not orpatch");
		return 0;
	}
	
	&::LogMsg("Patch to revert:");
	
	my $patch_info_cfg=&::ConcatPathString($self->GetRestoreDir(),$global_state->GetPatchInfoShortName());
	my $patch_info=new ORPatchInfo($patch_info_cfg);
	
	if ($patch_info->ReadPatchInfo()!=1) {
		return 0;
	}
	
	&::LogMsg("Calculating restore actions");
	if ($self->_LoadFile(1)!=1) {
		return 0;
	}
	
	#Organize our restore operations by destination manifest
	my %manifest_map=();
	my $manifest_count=0;
	my $file_count=0;
	foreach my $ref (@{$self->_GetRestoreMap()}) {
		my ($file_mode,$dest_file,$bkp_file,$perms,$dest_manifest,@row)=$self->DecodeMapLine($ref);
		my $dm_ref=$manifest_map{$dest_manifest};
		if ($dm_ref eq '') {
			$dm_ref=[];
			$manifest_count++;
		}

		#We build the list of operations by unshifting so the list is in the reverse
		#order of how they were originally done
		unshift(@$dm_ref,$ref);
		
		$manifest_map{$dest_manifest}=$dm_ref;
		$file_count++;
	}
	
	if ($file_count==0) {
		&::LogMsg("No files found to revert");
		return 1;
	}
	
	&::LogMsg("$file_count file(s) to restore in $manifest_count manifest(s)");
	
	#Loop through each destination manifest, fixing the files related to each
	foreach my $dest_manifest (keys(%manifest_map)) {
		my $ref=$manifest_map{$dest_manifest};
		
		if ($self->_RestoreOneManifest($dest_manifest,$ref,$any_action)!=1) {
			return 0;
		}
	}
	
	&::LogMsg("Revert complete.");
	
	my $inventory=$global_state->GetPatchInventory();
	
	if ($inventory->RemovePatch($patch_info->GetPatchName())!=1) {
		&::LogMsg("WARNING: Unable to de-register patch from inventory");
	}
	
	return 1;
}

##############################################################################################


#########################
# Sub:  _LoadFile
# Desc: Load the metadata map file
# Args: None
# Ret:  ref to list of products
#########################
sub _LoadFile {
	my $self=shift;
	my ($require_map_file)=@_;

	my $map_file=$self->GetMapFileName();
	
	if (!-f $map_file) {
		if ($require_map_file==1) {
			&::LogMsg("Restore map file $map_file does not exist");
			return 0;
		} else {
			#Nothing to load
			return 1;
		}
	}
	
	$::LOG->LogMsgNoVerbose("Loading restore point map file");
	
	unless(open(MAP_FILE,"<$map_file")) {
		&::LogMsg("Unable to open $map_file: $!");
		return 0;
	}
	
	my %dup_hash=();
	my @info=();
	while(<MAP_FILE>) {
		chomp;
		next if (/^#/);
		my ($file_mode,$dest_file,$bkp_file,$perms,$dest_manifest,@manifest_info)=split(/\,/,$_,-1);
		if ($bkp_file ne '') {
			$dup_hash{$bkp_file}=1;
		}
		push(@info,[$file_mode,$dest_file,$bkp_file,$perms,$dest_manifest,@manifest_info]);
	}
	close(MAP_FILE);
	
	$self->{'RESTORE_MAP'}=\@info;
	$self->{'RESTORE_DUP_HASH'}=\%dup_hash;
	
	return 1;
}

#########################
# Sub:  DecodeMapLine
# Desc: Convert a ref to a Restore Map line into it's pieces
# Args: ref to convert
# Ret:  file mode,destination filename,backup filename,permissions,destination manifest,@destination info
#########################
sub DecodeMapLine {
	my $self=shift;
	my ($ref)=@_;
	
	my @row=@$ref;
	
	my $file_mode=shift(@row);
	my $dest_file=shift(@row);
	my $bkp_file=shift(@row);
	my $perms=shift(@row);
	my $dest_manifest=shift(@row);
	
	return ($file_mode,$dest_file,$bkp_file,$perms,$dest_manifest,@row);
}

#########################
# Sub:  _WriteMapFile
# Desc: write out our restore map file
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub _WriteMapFile {
	my $self=shift;

	my $map_file=$self->GetMapFileName();
	my $interim=$self->_GetInterimMapName($map_file);
	
	unless(open(MAP_FILE,">$interim")) {
		&::LogMsg("Unable to open $interim: $!");
		return 0;
	}
	
	my $info_ref=$self->_GetRestoreMap();
	
	#&::LogMsg("Saving restore map file");
	
	unless(print MAP_FILE "#File Mode,dest file,bkp file,dest manifest,dest manifest info\n") {
		&::LogMsg("Failed writing to $map_file: $!");
		return 0;
	}
	
	foreach my $ref (@$info_ref) {
		my ($mode,$dest_file,$bkp_file,$perms,$dest_manifest,@manifest_info)=$self->DecodeMapLine($ref);
		unless(print MAP_FILE join(',',$mode,$dest_file,$bkp_file,$perms,$dest_manifest,@manifest_info)."\n") {
			&::LogMsg("Failed writing to $map_file: $!");
			return 0;
		}
	}
	close(MAP_FILE);
	
	#Swap the interim map to the final name
	if (File::Copy::move($interim,$map_file)) {
		#Success
	} else {
		&::LogMsg("Unable to move $interim to $map_file: $!");
		return 0;
	}
	
	chmod(0640,$map_file);
	
	return 1;
}

#########################
# Sub:  _AppendMapFile
# Desc: Append a row to our restore map file
# Args: row_ref - ref to the row to append to the map file
# Ret:  1 on success, 0 on failure
#########################
sub _AppendMapFile {
	my $self=shift;
	my ($row_ref)=@_;

	my $map_file=$self->GetMapFileName();
	
	unless(open(MAP_FILE,">>$map_file")) {
		&::LogMsg("Unable to open $map_file: $!");
		return 0;
	}
	
	my ($mode,$dest_file,$bkp_file,$perms,$dest_manifest,@manifest_info)=$self->DecodeMapLine($row_ref);
	unless(print MAP_FILE join(',',$mode,$dest_file,$bkp_file,$perms,$dest_manifest,@manifest_info)."\n") {
		&::LogMsg("Failed writing to $map_file: $!");
		return 0;
	}
	
	close(MAP_FILE);
	
	return 1;
}

#########################
# Sub:  _InitMapFile
# Desc: Create a map file and write the header
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub _InitMapFile {
	my $self=shift;

	my $map_file=$self->GetMapFileName();
	
	unless(open(MAP_FILE,">$map_file")) {
		&::LogMsg("Unable to open $map_file: $!");
		return 0;
	}
	
	unless(print MAP_FILE "#File Mode,dest file,bkp file,dest manifest,dest manifest info\n") {
		&::LogMsg("Failed writing to $map_file: $!");
		return 0;
	}
	
	close(MAP_FILE);
	
	return 1;
}

#########################
# Sub:  _GetInterimMapName
# Desc: Construct an interim map name based on a real map name
# Args: final map name
# Ret:  interim map name
#########################
sub _GetInterimMapName {
	my ($real_manifest)=@_;
	
	return $real_manifest.".tmp";
}

#########################
# Sub:  _RegisterBackupFile
# Desc: Record a backup file in our in-memory structure of restore point actions
# Args: file_mode - CREATE,UPDATE, DELETE
#       dest_file - path to the file in the destination area
#       bkp_file  - file in the restore point area
#       perms     - permissions the file should have if restored
#       dest_manifest - the destination manifest tracking the dest file
#       dest_info_row_ref - ref to the destination info row for this file
# Ret:  proceed - 1 on success, 0 on failure
#       row_ref - ref to the row that was created
#########################
sub _RegisterBackupFile {
	my $self=shift;
	my ($file_mode,$dest_file,$bkp_file,$perms,$dest_manifest,$dest_info_row_ref)=@_;

	my $dest_dir=$self->_GetDestDir();
	my $rel_dest_file=$dest_file;
	my $rel_bkp_file=$bkp_file;
	
	#Check for duplicate backup files
	if ($rel_bkp_file ne '') {
		my $dup_hash_ref=$self->{'RESTORE_DUP_HASH'};
		if ($dup_hash_ref->{$rel_bkp_file} ne '') {
			&::LogMsg("Duplicate backup file generated: $bkp_file");
			return (0,undef);
		}
	
		$dup_hash_ref->{$rel_bkp_file}=1;
	}
	
	my $map_ref=$self->_GetRestoreMap();
	my @dest_info=();
	if (ref($dest_info_row_ref)) {
		@dest_info=@$dest_info_row_ref;
	}
	my $row_ref=[$file_mode,$rel_dest_file,$rel_bkp_file,$perms,$dest_manifest,@dest_info];
	push(@$map_ref,$row_ref);
	
	return (1,$row_ref);
}

#########################
# Sub:  _CalculateBackupFileName
# Desc: Determine a non-conflicting file name for a backup file
# Args: restore_dir - path to the restore area
#       base_file - the base file name
# Ret:  bkp_file - the backup file name or '' on errors
#########################
sub _CalculateBackupFileName {
	my $self=shift;
	my ($restore_dir,$base_file)=@_;
	
	my $dup_hash_ref=$self->{'RESTORE_DUP_HASH'};
	
	my $basic_file=&::ConcatPathString($restore_dir,$base_file);
	
	if ($dup_hash_ref->{$basic_file} eq '') {
		return $basic_file;
	}
	
	#Collision, construct a new name that doesn't collide
	for(my $i=0;$i<1000;$i++) {
		my $try_file=sprintf("%s_%06d",$basic_file,$i);
		if ($dup_hash_ref->{$try_file} eq '') {
			return $try_file;
		}
	}
	
	#1000 duplicate file names, abort
	&::LogMsg("Unable to determine non-conflicting backup file name for $basic_file!");
	return '';
}

#########################
# Sub:  _RestoreOneManifest
# Desc: Restore all the files related to a single destination manifest
# Args: dest_manifest - destination manifest that will be impacted
#       restore_ref - ref to an array of the rows from the restore map
#       any action - handle to any action that we can call GetDeletedManifestName on
# Ret:  1 on success, 0 on failure
#########################
sub _RestoreOneManifest {
	my $self=shift;
	my ($dest_manifest,$restore_ref,$any_action)=@_;

	&::LogMsg("Processing restores related to $dest_manifest");
	
	my @dest_info=&Manifest::ReadManifest($dest_manifest);
	my $del_manifest=$any_action->GetDeletedManifestName($dest_manifest);
	my @delete_dest_info=&Manifest::ReadManifest($del_manifest);
	
	my $orig_delete_count=scalar(@delete_dest_info);
	
	my $create=$self->GetBackupModeCreate();
	my $update=$self->GetBackupModeUpdate();
	my $delete=$self->GetBackupModeDelete();
	my $orphan=$self->GetBackupModeOrphan();
	
	my @remove_from_dest_mfst=();
	foreach my $ref (@$restore_ref) {
		my ($file_mode,$dest_file,$bkp_file,$perms,$this_manifest,@row)=$self->DecodeMapLine($ref);
		
		if ($file_mode=~/^$create$/i) {
			#Take care of new files created during the patch
			
			#Remove the file
			if (-f $dest_file) {
				&::RemoveFile($dest_file);
			} else {
				&::LogMsg("Unable to remove new file $dest_file created during patch: file does not exist");
			}
			#Remove the relative file from the destination manifest
			my ($dfile,$sfile,@discard)=&Manifest::DecodeDestLine(\@row);
			my ($keep_info,$remove_info)=&Manifest::DeleteDestInfo(\@dest_info,[$dfile]);
			@dest_info=@$keep_info;
		} elsif ($file_mode=~/^$update$/i) {
			#Take care of files updated during the patch
			
			#Copy the file back
			if ($self->_RestoreFile($bkp_file,$dest_file,$perms,\@dest_info,\@row)!=1) {
				return 0;
			}
		} elsif ($file_mode=~/^$delete$/i) {
			#Take care of files deleted during the patch
			
			#Copy the file back
			if ($self->_RestoreFile($bkp_file,$dest_file,$perms,\@dest_info,\@row)!=1) {
				return 0;
			}
			#Remove the file from the deleted manifest
			my ($dfile,$sfile,@discard)=&Manifest::DecodeDeletedDestLine(\@row);
			my ($keep_info,$remove_info)=&Manifest::DeleteDeletedDestInfo(\@delete_dest_info,[$dfile]);
			@delete_dest_info=@$keep_info;
		} elsif ($file_mode=~/^$orphan$/i) {
			#Take care of orphan files found during the patch (update manifest, no file copy)
			&::LogMsg("Restoring $dest_file to original orphaned state");
		
			#Remove the file
			if (-f $dest_file) {
				&::RemoveFile($dest_file);
			} else {
				&::LogMsg("Unable to remove replacement file $dest_file created during patch: file does not exist");
			}
			
			#Update the manifest 
			my ($dfile,$sfile,$ftype,$frevision,$fdesc,$custom,$fwp_rev,$future1,$future2,$future3,$future4)=&Manifest::DecodeDestLine(\@row);
			&Manifest::UpdateDestInfo($sfile,$ftype,$frevision,$dfile,$fdesc,$custom,$fwp_rev,$future1,$future2,$future3,$future4,\@dest_info);
		}
	}
	
	#Save the destination manifest
	&::LogMsg("Saving reverted manifest");
	if (&Manifest::SaveDestManifest($dest_manifest,[],\@dest_info,{},1)!=1) {
		return 0;
	}
	
	#If there are now delete manifest entries, or there were originally delete manifest entries, save that manifest
	if (scalar(@delete_dest_info)!=0 || $orig_delete_count!=0) {
		&::LogMsg("Saving reverted deleted dest manifest");
		if (&Manifest::SaveDestManifest($del_manifest,[],\@delete_dest_info,{},0)!=1) {
			return 0;
		}
	}
	
	#this manifest is completed
	return 1;
}

#########################
# Sub:  _RestoreFile
# Desc: Restore a backup file and update the manifest revision
# Args: backup file to restore
#       dest file to replace
#       perms - string of permissions ("0750" for example)
#       dest_info_ref - ref to the manifest info array
#       dest_info_row_ref - ref to the destination info row for the original file
# Ret:  1 on success, 0 on failure
#########################
sub _RestoreFile {
	my $self=shift;
	my ($bkp_file,$dest_file,$perms,$dest_info_ref,$dest_info_row_ref)=@_;
	
	my ($dfile,$sfile,$ftype,$frevision,$fdesc,$custom,$fwp_rev,$future1,$future2,$future3,$future4)=&Manifest::DecodeDestLine($dest_info_row_ref);
	
	&::LogMsg("Restoring $dest_file to revision $frevision");
	
	my ($dest_path,$f)=&::GetFileParts($dest_file,2);
	my $dir_perms='0755';
	&::CreateDirs($dir_perms,$dest_path);
	
	if (&File::Copy::copy($bkp_file,$dest_file)) {
		#Successful
	} else {
		#Copy failed!
		&::LogMsg("Unable to copy $bkp_file to $dest_file: $!");
		return 0;
	}
		
	#Adjust permissions if desired
	my $files_changed=chmod(oct($perms),$dest_file);
	if ($files_changed!=1) {
		&::LogMsg("WARNING: Unable to change permissions on $dest_file: $!");
	}
	
	#Update the revision in the manifest
	&Manifest::UpdateDestInfo($sfile,$ftype,$frevision,$dfile,$fdesc,$custom,$fwp_rev,$future1,$future2,$future3,$future4,$dest_info_ref); 
	
	return 1;
}
	

sub GetMapFileName {
	my $self=shift;
	return $self->{'MAP_FILE'};
}


sub _GetRestoreMap {
	my $self=shift;
	return $self->{'RESTORE_MAP'};
}

sub GetRestoreDir {
	my $self=shift;
	return $self->{'RESTORE_DIR'};
}

sub _SetRestoreMap {
	my $self=shift;
	my ($restore_map)=@_;
	$self->{'RESTORE_MAP'}=$restore_map;
}

sub _SetBackupDir {
	my $self=shift;
	my ($backup_dir)=@_;
	my $restore_dir=&::ConcatPathString($self->{'RESTORE_BASE_DIR'},$backup_dir);
	$self->_SetRestoreDir($restore_dir);
	return $restore_dir;
}

sub _SetRestoreDir {
	my $self=shift;
	my ($restore_dir)=@_;
	$self->{'RESTORE_DIR'}=$restore_dir;
	$self->_SetRestoreMapFile(&::ConcatPathString($restore_dir,'restore_actions.map'));
	return $restore_dir;
}

sub _SetRestoreMapFile {
	my $self=shift;
	my ($map_file)=@_;
	$self->{'MAP_FILE'}=$map_file;
}

sub _GetDestDir {
	my $self=shift;
	return $self->{'DEST_DIR'};
}

sub _GetORPatchFlag {
	my $self=shift;
	return 'orpatch_selfupdate.cfg';
}

#####################################################################
# Sub:  new
# Desc: A class encapsulating a restore directory for orpatch
# Args: restore_dir
#####################################################################
sub new {
    my $class=shift;
	my ($dest_dir)=@_;
    my $self={};
	
    bless($self,$class);

	$self->{'MAP_FILE'}='';
	$self->{'RESTORE_MAP'}=[];
	$self->{'RESTORE_DUP_HASH'}={};
	$self->{'DEST_DIR'}=$dest_dir;
	$self->{'RESTORE_BASE_DIR'}=&::ConcatPathString($dest_dir,'backups');
	$self->{'RESTORE_DIR'}='';
	
    return $self;
}

1;
