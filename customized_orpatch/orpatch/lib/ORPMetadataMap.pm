############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################

use strict;
package ORPMetadataMap;

#########################
# Sub:  LoadFile
# Desc: Load the metadata map file
# Args: None
# Ret:  ref to list of products
#########################
sub LoadFile {
	my $self=shift;

	my $map_file=$self->GetMapFileName();
	
	if (!-f $map_file) {
		&::LogMsg("Metadata map file $map_file does not exist");
		return 0;
	}
	
	unless(open(MAP_FILE,"<$map_file")) {
		&::LogMsg("Unable to open $map_file: $!");
		return 0;
	}
	
	my $export_dir=$self->GetExportDir();
	
	my @info=();
	while(<MAP_FILE>) {
		chomp;
		next if (/^#/);
		my ($action,$type,$file)=split(/\,/);
		push(@info,[$action,$type,&::ConcatPathString($export_dir,$file)]);
	}
	close(MAP_FILE);
	
	$self->{'META_MAP'}=\@info;
	
	return 1;
}

#########################
# Sub:  WriteFile
# Desc: write out our metadata map file
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub WriteFile {
	my $self=shift;
	my ($dest_dir)=@_;

	my $map_file=$self->GetMapFileName();
	
	unless(open(MAP_FILE,">$map_file")) {
		&::LogMsg("Unable to open $map_file: $!");
		return 0;
	}
	
	my $info_ref=$self->_GetMetaMap();
	
	&::LogMsg("Saving metadata map file");
	
	unless(print MAP_FILE "#Action,Type,File Name\n") {
		&::LogMsg("Failed writing to $map_file: $!");
		return 0;
	}
	
	foreach my $ref (@$info_ref) {
		my ($action,$type,$file)=$self->DecodeMapLine($ref);
		my $rel_file=&::GetRelativePathName($file,$dest_dir);
		unless(print MAP_FILE join(',',$action,$type,$rel_file)."\n") {
			&::LogMsg("Failed writing to $map_file: $!");
			return 0;
		}
	}
	close(MAP_FILE);
	chmod(0750,$map_file);
	
	return 1;
}

#########################
# Sub:  GetMetadataFileList
# Desc: Get a possibly filtered list of Metadata Files
# Args: action_filter,type_filter - if either or both filters are specified, only matching types or actions will be returned
# Ret:  ref to list keyed by full file names which match the criteria
#########################
sub GetMetadataFileList {
	my $self=shift;
	my ($action_filter,$type_filter)=@_;
	
	if ($action_filter ne '' && !ref($action_filter)) {
		$action_filter=[$action_filter];
	}
	
	my $info_ref=$self->_GetMetaMap();
	my @info=();
	foreach my $ref (@$info_ref) {
		my ($action,$type,$file)=$self->DecodeMapLine($ref);
	
		my $type_good=1;
		if ($type_filter ne '') {
			$type_good=0;
			if ($type_filter eq $type) {
				$type_good=1;
			}
		}
		
		my $action_good=1;
		if ($action_filter ne '') {
			$action_good=0;
			foreach my $af (@$action_filter) {
				if ($af eq $action) {
					$action_good=1;
					last;
				}
			}
		}
		
		if ($type_good==1 && $action_good==1) {
			push(@info,$file);
		}
	}
	
	return \@info;	
}

#########################
# Sub:  GetSingleFile
# Desc: Get a specific action+file type row, if multiple exist in the map, it is undefined which will be returned
# Args: type_filter,action_filter - both criteria must be specified
# Ret:  file name which matches or '' if none matches
#########################
sub GetSingleFile {
	my $self=shift;
	my ($action_filter,$type_filter)=@_;
	
	my $info_ref=$self->GetMetadataFileList($action_filter,$type_filter);
	if (scalar(@$info_ref)==0) {
		return '';
	}
	my $first_file=shift(@$info_ref);
	return $first_file;
}

#########################
# Sub:  DecodeMapLine
# Desc: Convert a ref to a Metadata Map line into it's pieces
# Args: ref to convert
# Ret:  action,type,file name
#########################
sub DecodeMapLine {
	my $self=shift;
	my ($ref)=@_;
	
	my $action=$$ref[0];
	my $type=$$ref[1];
	my $file=$$ref[2];

	return ($action,$type,$file);
}

sub RegisterInventory {
	my $self=shift;
	my ($inv_file)=@_;
	return $self->RegisterFile($self->_GetInventoryRegisterType(),$inv_file);
}

sub RegisterEnvInfo {
	my $self=shift;
	my ($env_info)=@_;
	return $self->RegisterFile($self->_GetEnvInfoRegisterType(),$env_info);
}

sub GetInventoryFile {
	my $self=shift;
	return $self->GetSingleFile($self->_GetInventoryRegisterType());
}

sub GetEnvInfoFile {
	my $self=shift;
	return $self->GetSingleFile($self->_GetEnvInfoRegisterType());
}

#########################
# Sub:  RegisterFile
# Desc: register a metadata file with our map
# Args: action, type, file
# Ret:  1
#########################
sub RegisterFile {
	my $self=shift;
	my ($action,$type,$file)=@_;
	
	my @row=($action,$type,$file);
	my $info_ref=$self->_GetMetaMap();
	push(@$info_ref,\@row);
	return 1;
}

sub GetMapFileName {
	my $self=shift;
	return $self->{'MAP_FILE'};
}

sub GetDBManifestType {
	return 'DBMANIFEST';
}

sub GetJarManifestType {
	return 'JARMANIFEST';
}

sub GetEnvManifestType {
	return 'ENVMANIFEST';
}

sub GetDelEnvManifestType {
	return 'DELENVMANIFEST';
}

sub _GetMetaMap {
	my $self=shift;
	return $self->{'META_MAP'};
}

#We have a specific action and type for env_info.cfg files for easy reference
sub _GetEnvInfoRegisterType {
	return('CONFIG','ENVINFO');
}

#We have a specific action and type for inventory files for easy reference
sub _GetInventoryRegisterType {
	return('CONFIG','INVENTORY');
}

sub GetExportDir {
	my $self=shift;
	return $self->{'EXPORT_DIR'};
}

#####################################################################
# Sub:  new
# Desc: A class encapsulating a map of exported metadata files
# Args: support_dir
#####################################################################
sub new {
    my $class=shift;
	my ($support_dir)=@_;
    my $self={};
	
    bless($self,$class);

	$self->{'MAP_FILE'}=&::ConcatPathString($support_dir,'metadata_map.csv');
	$self->{'META_MAP'}=[];
	$self->{'EXPORT_DIR'}=$support_dir;
	
    return $self;
}

1;
