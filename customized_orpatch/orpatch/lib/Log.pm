############################################################################
# Copyright © 2013,2014 Oracle and/or its affiliates. All rights reserved.
############################################################################
package Log;

###################
# Sub:   LogMsg
# Desc:  Print a message to the log
###################
sub LogMsg {
    my $this=shift;

	my $lprefix='';
    if ($this->{'LOG_TIME_FORMAT'} ne '') {
        $lprefix=&::GetTimeStamp($this->{'LOG_TIME_FORMAT'})." ";
    }

	if ($this->{'LOG_PREFIX'} ne '') {
        $lprefix.=$this->{'LOG_PREFIX'}." ";
    }

	my $lfile=$this->GetLogFile();
    open(LOG_FILE,">>$lfile") || die "Unable to open Log File $lfile: $!";
    foreach (@_) {
        my $line=$_;
        chomp($line);
		my $output_line="$lprefix$line\n";
        print LOG_FILE $output_line;
        print $output_line if $this->{'VERBOSE'};
    }

    close(LOG_FILE);
    return 1;
}

sub GetLogFile {
	my $this=shift;
	return $this->{'LOGFILE'};
}

sub LogMsgNoDate {
    my $this=shift;

    local $this->{'LOG_TIME_FORMAT'}='';
    return $this->LogMsg(@_);
}

sub LogMsgNoVerbose {
    my $this=shift;

    local $this->{'VERBOSE'}=0;
    return $this->LogMsg(@_);
}

###################
# Sub:   SetupLogFile
# Desc:  Setup our logging to a file
###################
sub SetupLogFile {
    my $this=shift;
    my ($log_file,$append,$log_dir,$timestamp_log)=@_;

    $append=1 if $append eq '';
    $log_dir||="$ENV{LOG_DIR}";
	$log_dir||="$ENV{RETAIL_HOME}/orpatch/logs";
	$timestamp_log=1 if $timestamp_log eq '';

	#If they didn't give us a log name, we use our program name
    if ($log_file eq '') {
        my ($path,$script,$ext)=&::GetFileParts($0,3);
        $log_file="$log_dir/${script}.log";
    }

    if ($timestamp_log==1) {
        $log_file=&::TimeStampFile($log_file);
    } else {
        if (-f $log_file) {
            if ($append==1) {
                if (-s $log_file>5000000) {
                    my $new_name="${log_file}.old";
                    unlink($new_name);
                    rename($log_file,$new_name);
                }
            } else {
               unlink($log_file);
            }
        }
    }

    $this->{'LOGFILE'}=$log_file;
    return 1;
}

sub SetPrefix {
    my $this=shift;
    my ($prefix)=@_;

    $this->{'LOG_PREFIX'}=$prefix;
    return 1;
}

sub SetVerbose {
    my $this=shift;
    my ($verbose)=@_;

    $this->{'VERBOSE'}=$verbose;
    return 1;
}

sub GetVerbose {
	my $this=shift;
	return $this->{'VERBOSE'};
}

# This must be called before any LogMsg
sub new {
    my $class=shift;
    my $self={};
    $self->{'LOGFILE'}='';
    $self->{'LOG_PREFIX'}='';
    $self->{'LOG_TIME_FORMAT'}='TIME_DATE';
    $self->{'VERBOSE'}=1;
    bless($self,$class);

    $self->SetupLogFile(@_);

    return $self;
}

1;