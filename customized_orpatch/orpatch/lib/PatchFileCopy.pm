############################################################################
# Copyright © 2013,2015, Oracle and/or its affiliates. All rights reserved.
############################################################################

package PatchFileCopy;
use strict;

###################
# Sub:   DoFileCopy
# Desc:  Do the actual copying of files from the patch area to their
#        destination area, optionally preserving directory structure
#        and optionally ignoring patterns of file names
# Args:  Patch Name - name for this patch
#        Source Dir - Where to copy from
#        src_info_ref - Ref to the source manifest info
#        src_type_lookup - Ref to the source manifest type lookup hash constructed by BuildSourceManifestTypeHash
#        Dest Dir - Where to copy to
#        Dest Manifest - The path of the resultant env manifest
#        restore_point - A ref to the restore point
#        fcp_ref - A ref to a FileCopyParams object for this copy
#        patch_info_ref - A handle to the patch info object
# Ret:   proceed - 1 if sucessful, 0 if not
#        files copied - the total number of files that were created or updated
####################
sub PatchFileCopy::DoFileCopy {
	my ($patch_name,$src_dir,$src_info_ref,$src_type_lookup,$dest_dir,$dest_mfst,$restore_point,$fcp_ref,$patch_info_ref)=@_;

	#Read the dest manifest
	my @dest_info=&Manifest::ReadManifest($dest_mfst);

	#Build the list of unknown files in the dest manifest
	my ($href,$di_ref)=&Manifest::RemoveUnknownBlocks($src_info_ref,\@dest_info);
	@dest_info=@{$di_ref};
	
	#Create a lookup hash to speed version comparisons
	my %dest_lookup_hash=();
	foreach my $ref (@dest_info) {
		my ($dfile,$sfile,@discard)=&Manifest::DecodeDestLine($ref);
		$dest_lookup_hash{$sfile}=$ref;
	}

	my $ignore_pat_ref=$fcp_ref->GetIgnorePatterns();
	my $types=$fcp_ref->GetFileTypes();
	
	my $filtered_list_ref=&Manifest::FilterSourceManifest($src_type_lookup,$types,$ignore_pat_ref);
	my $total=scalar(@$filtered_list_ref);
	
	&::LogMsg("Processing $total files from this patch...");
	
	#Copy away
	my $copied=0;
	foreach my $ref (@$filtered_list_ref) {   
		my ($fname,$frevision,$ftype,$fpatch_name,$unit_test,$localization,$fcustomized,$fwp_rev,$future1,$future2,$future3,$future4)=&Manifest::DecodeSourceLine($ref);
		
		#If this file came from a specific patch, use that name otherwise use the top-level patch name
		if ($fpatch_name eq '') {
			$fpatch_name=$patch_name;
		}
		
		#Check if this file is newer than the destination file
		my ($replace_mode,$replaced_di_ref)=&PatchFileIsNewer($fname,$frevision,\%dest_lookup_hash,$dest_dir,$fcp_ref,$ftype,$patch_info_ref);
		if ($replace_mode>0) {
			my $dfname=&CopyPatchFile($fname,$ftype,$src_dir,$dest_dir,$restore_point,$dest_mfst,$replaced_di_ref,$frevision,$fcp_ref);
			if ($dfname eq '') {
				#File Copy Failed
				&::LogMsg("Error while copying patch files, aborting");
				return 0;
			}
			$copied++;
			#Now update our intended destination manifest
			my $new_ref=&Manifest::UpdateDestInfo($fname,$ftype,$frevision,$dfname,$fpatch_name,$fcustomized,$fwp_rev,$future1,$future2,$future3,$future4,\@dest_info); 
			#Update our cross-reference lookup
			$dest_lookup_hash{$fname}=$new_ref;
		} else {
			#We only print this to the log
			#$::LOG->LogMsgNoVerbose("File $fname does not need update");
		}
	}
	
	if ($copied!=0) {
		&::LogMsg("  $copied/$total files had updates and were copied");
	}
	my $not_copied=$total-$copied;
	if ($not_copied!=0) {
		&::LogMsg("  $not_copied/$total files did not need to be copied");
	}

	if ($copied!=0) {
		#Only write the manifest if a file was copied
		if (&Manifest::SaveDestManifest($dest_mfst,$src_info_ref,\@dest_info,$href,1)!=1) {
			&::LogMsg("Unable to save destination manifest $dest_mfst");
			return 0;
		}
	}

	return (1,$copied);
}

###################
# Sub:   PatchFileIsNewer
# Desc:  Check if a file included in the patch is newer than what is
#        in our destination area
# Args:  fname - The source file name
#        frevision - The source file revision
#        di_ref - A ref to the destination manifest info array
#        $dest_dir - The destination directory where relative file names start
#		 $fcp_ref  - ref to the FileCopyParams controlling this copy
#        $ftype    - the file type of the file in question
#        patch_info_ref - A handle to the patch info object
# Ret:   (replace_mode,ref) - replace_mode is 1 if the file is newer, 0 if not, 2 if the file will be new, -1 if the environment file is newer
#                             Ref is ref to dest_info row if the file will replace something, undef if it will not
####################
sub PatchFileCopy::PatchFileIsNewer {
	my ($fname,$frevision,$di_ref,$dest_dir,$fcp_ref,$ftype,$patch_info_ref)=@_;
	
	my $update_crit=$fcp_ref->GetUpdateCriteria($ftype);
	$update_crit='IFNEW' if $update_crit eq '';
	
	#Find the matching source name line in destination info
	my $ref=$di_ref->{$fname};
	
	if ($ref eq '') {
		#Unable to find the file, treat it as new
		return (2,'');
	}
	
	my ($di_dfile,$di_sfile,$di_ftype,$di_revision,@discard)=&::Manifest::DecodeDestLine($ref);
		
	#Make sure the file listed in the destination manifest actually exists
	my $full_dest_file=&::ConcatPathString($dest_dir,$di_dfile);
	if (!-f $full_dest_file) {
		#If it is missing, we will always copy
		$::LOG->LogMsgNoVerbose("Missing file $di_dfile will be updated to revision $frevision");
		return (1,$ref);
	}
	
	#File exists, determine if it can be updated based on update criteria
	
	if ($update_crit eq 'ALWAYS') {
		#We ALWAYS copy the file, regardless of the version
		$::LOG->LogMsgNoVerbose("$di_dfile:$di_revision will be updated to revision $frevision");
		return (1,$ref);
	} elsif ($update_crit eq 'NEVER') {
		#We NEVER overwrite a file, regardless of the version
		$::LOG->LogMsgNoVerbose("$di_dfile:$di_revision exists and should not be overwritten");
		return (0,$ref);
	} else {
		#We copy the file only IFNEW, IFDIFF, or IFHIGHER
		my $compare_result=&RevisionCompare($frevision,$di_revision);
	
		#On a cumulative patch we treat IFNEW like IFDIFF as we want to update the file even if the revision looks older
		if ($update_crit eq 'IFNEW' && $patch_info_ref->IsPatchCummulative()) {
			$update_crit='IFDIFF';
		}
		
		#IFDIFF will update the file if it is not the same
		if ($compare_result!=0 && $update_crit eq 'IFDIFF') {
			$compare_result=1;
		}
		
		#If the file is newer, report it will be updated
		if ($compare_result==1) {
			$::LOG->LogMsgNoVerbose("$di_dfile:$di_revision will be updated to revision $frevision");
		}
		return ($compare_result,$ref);
	}
}

###################
# Sub:   CopyPatchFile
# Desc:  Copy a patch file into the destination area
#        Note: the destination manifest is NOT updated in this function
# Args:  fname - The source manifest's filename
#        ftype - The source file type
#        src_dir - The path to the source directory of this patch
#        dst_dir - The path ot the destination directory
#        restore_point - handle to restore point to use to create backup copies
#        dest_manifest - destination manifest associated with this file (used by restore point)
#        dest_info_row_ref - ref to destination info row for this file, or '' if it is a new file
#        frevision - revision of the new file
#        fcp_ref - A ref to the FileCopyParam object
# Ret:   1 if the file was successfully copied, 0 if not
####################
sub PatchFileCopy::CopyPatchFile {
	my ($fname,$ftype,$src_dir,$dst_dir,$restore_point,$dest_manifest,$dest_info_row_ref,$frevision,$fcp_ref)=@_;

	#Convert the relative source name into
	#the more specific file names we need
	my ($real_src_path,$real_dst_path,$dfname)=$fcp_ref->GenFileNames($fname,$ftype,$src_dir,$dst_dir);

	my $backup_mode=$restore_point->GetBackupModeUpdate();
	
	#We log this here as new files don't have the destination file during PatchFileIsNewer
	if ($dest_info_row_ref eq '') {
		$::LOG->LogMsgNoVerbose("$dfname will be created at revision $frevision");
		#Create enough of a dest line (relative dest file name) in order to find this file if we have to revert
		#Note that this is not the 'correct' dest line, that will be created during UpdateDestInfo
		#however in the event we have to revert a new file, all we need is the relative dest file name to remove
		#the correct line after removing the file.
		$dest_info_row_ref=&Manifest::AssembleDestLine($dfname,$fname,'',$frevision,'','','','','','','');
		$backup_mode=$restore_point->GetBackupModeCreate();
	} else {
		#Updating an existing file, make sure the dest file actually exists
		if (!-f $real_dst_path) {
			&::LogMsg("WARNING: Found orphaned manifest entry for $dfname, file cannot be backed up!");
			$backup_mode=$restore_point->GetBackupModeOrphan();
		}
	}
	
	my ($final_dir_string,$junk)=&::GetFileParts($real_dst_path);
	
	my $dir_perms='0755';
	my $perms=$fcp_ref->GetFilePerms($ftype);
	if ($perms ne '') {
		$dir_perms=$perms;
	}
	
	#Create our directory tree if necessary
	&::CreateDirs($dir_perms,$final_dir_string);

	if (! -f $real_src_path) {
		&::LogMsg("ERROR: Source File $real_src_path does not exist!");
		return '';
	}

	#First backup up the file
	if ($restore_point->CreateBackupFile($backup_mode,$perms,$real_dst_path,$dest_manifest,$dest_info_row_ref)!=1) {
		return '';
	}
	
	#Copy the file
	$::LOG->LogMsgNoVerbose("Copying $fname to $real_dst_path");
	my $method=$fcp_ref->GetFileCopyMethod($ftype,$fname);
	my $error_msg='';
	if ($method->DoCopy($real_src_path,$real_dst_path,\$error_msg)!=1) {
		&::LogMsg($error_msg);
		return '';
	}
	
	#Adjust permissions if desired
	if ($perms ne '') {
		my $files_changed=chmod(oct($perms),$real_dst_path);
		if ($files_changed!=1) {
			&::LogMsg("WARNING: Unable to change permissions on $real_dst_path: $!");
		}
	}
	  
	#We return the relative path where we copied the file for manifest update
	return $dfname;
}

###################
# Sub:   RevisionCompare
# Desc:  Custom comparison of revision numbers to accommodate huge decimal sizes if SVN
#        revisions are used like 14000100.12345678901234
# Args:  frevision - revision of the new file
#        di_revision - revision of the environment
# Ret:   1 if frevision is newer, 0 if the revisions are the same, -1 if the di_revision is newer
####################
sub PatchFileCopy::RevisionCompare {
	my ($frevision,$di_revision)=@_;
	
	#determine the type of comparisons this number requires
	if ($frevision!~/\./ && $di_revision!~/\./) {
		#integer version, so do a simple comparison
		if ($frevision>$di_revision) {
			return 1;
		} elsif ($frevision==$di_revision) {
			return 0;
		} else {
			return -1;
		}
	} else {
		#Handle floating point numbers with huge decimal places, and multi-level version numbers
		my @inc_rev=split(/\./,$frevision);
		my @env_rev=split(/\./,$di_revision);
		
		my $last_digit=-1;
		my $current_digit=0;
		
		#When we have a simple xxxx.yyyy number, we will compare the yyyy part differently
		#so we need to know when we get to yyyy
		if (scalar(@inc_rev)==2) {
			$last_digit=scalar(@inc_rev);
		}
		
		foreach my $inc_digit (@inc_rev) {
			my $env_digit=shift(@env_rev);
			
			$inc_digit=0 if $inc_digit eq '';
			$env_digit=0 if $env_digit eq '';
			
			$current_digit++;
			if ($current_digit==$last_digit) {
				#if this is the last digit, convert to 0.xxxx so they compare correctly
				$inc_digit="0.$inc_digit";
				$env_digit="0.$env_digit";
			}
			
			if ($inc_digit<$env_digit) {
				#existing env file is newer
				return -1;
			} elsif ($inc_digit>$env_digit) {
				#incoming file is newer
				return 1;
			}
			
			#digits are the same, continue
		}
		
		#All digits in the incoming revision were the same
		#but it is possible that incoming was 11.1.1.7 and the env has 11.1.1.7.1
		if (scalar(@env_rev)!=0) {
			#so if there are any digits left in the environment, it is newer
			return -1;
		}
		
		#Everything is exactly the same
		return 0;
	}
}

1;
