############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RASECDTDBSQL;
our @ISA = qw(ORPAction::SIABaseDBSQL);

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	my $self=$class->SUPER::new($config_ref,'DBSQL_RASECDT','rasecdt','','cdt_db');
    bless($self,$class);

	return $self;
}

1;
