############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
# This is the action for database scripts that run in the ALLOC user schema
# Normal alc_ tables in the RMS schema are run in AllocRMSDBSQL
use strict;

package ORPAction::AllocDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#If this is a full check, register our need for a working DBSQL_ALCRMS configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_ALCRMS');
	}
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;
	
	#Get the RMS schema owner name using the DBSQL_ALCRMS connection information
	my $rms_name=$self->GetCrossActionSchemaName('DBSQL_ALCRMS');
	return 0 if $rms_name eq '';
	
	#Create synonyms in our schema, pointing to RMS objects
	unless($self->CreateSynonymsForUser('',$rms_name)==1) {
		&::LogMsg("Failed to create synonyms to $rms_name schema");
		return 0;
	}
	
	#Compile Invalid Objects
	if ($self->CompileInvalids()!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadAllocInstallScripts
# Desc: a custom function to load AllocRMS install scripts
#########################
sub LoadAllocInstallScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	#Find the RMS owning schema
	my $alcrms_owner=$self->GetCrossActionSchemaName('DBSQL_ALCRMS');
	return 0 if $alcrms_owner eq '';
	
	my @filter=(
	);
	
	my $args="$alcrms_owner";
	
	return $self->RunDestFileListWithArgs($full_dest,$args,$filtered_dest_info,\@filter);
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='DBSQL_ALLOC';
	
	#Allow skipping all DBC scripts when DBSQL_ALLOC_RUN_DBC=N
	my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not was 0
	my @file_type_info=(
	['alc_db_ddl',				3,			'',1],
	['alc_db_change_scripts',	$dbc_mode,	'',1],
	['alc_db_install_scripts',	2,'LoadAllocInstallScripts',1],
	);
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	$self->SetCheckLockedObjects(0);
	
    return $self;
}
