####################################################################
# Copyright © 2016, Oracle and/or its affiliates. All rights reserved.
############################################################################

package ORPAction::RMSJavaApp;
use strict;
use File::Basename;
our @ISA = qw(ORPAction::JavaApp);


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
        my $class=shift;
        my ($config_ref)=@_;

        my $action_name='JAVAAPP_RMS';

    my $reports_subdir='reports';
    my $reports_type='rms_reports';
    my $reports_fcp=new FileCopyParams($reports_type,$reports_subdir,-2);

        my @file_type_info=(
        #Type               Archive copies
        ['rms_java_app',        1,      0,      undef],
        #Non-Archive copies
        #Type               Archive copies, Sign jars, FileCopyParam
        [$reports_type,         0,      0,      $reports_fcp],
        );

        my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);

    return $self;
}

sub PreAction {
	my $self=shift;
      
        if($self->processFilesWithName(["RmsApps.properties"]) !=1 ){
            return 0;
        }

        if ($self->SUPER::PreAction()!=1) {
                return 0;
        }
	
	return 1;
}



#####################################################################################
# Sub:  processFilesWithName
# Desc: This function will search for files with file type 'rms_java_app_custom'.
#       For the resulting files, the function will check if the filename is on
#       the list of files given by parameter. If the file exists on that list,
#       the content of the file is processed and all the placeholders @<env_variable>@
#       will be replaced by the respective environment variable content.
# Arg:  List of filenames to be processed
# Ret:  1 on success, 0 on failure
#####################################################################################
sub processFilesWithName{

        my $self=shift;
        my $fileNames=shift;

        my $global_state=$self->GetORPatchGlobalState();

        my $source_dir=$global_state->GetSourceDir();

		my ($src_info_ref,$type_lookup_href,$distinct_types)=$global_state->GetSourceManifestInfo();
        
        # Filtering manifest data by 'rms_java_app_custom'
        my $filtered_list_ref=&Manifest::FilterSourceManifest($type_lookup_href,['rms_java_app_custom'],[]);

	#Iterating over each file with type 'rms_java_app_custom'
        foreach my $ref (@$filtered_list_ref) {   
			my ($fname,$frevision,$ftype,$fpatch_name,$unit_test,$localization,$fcustomized,$fwp_rev,$future1,$future2,$future3,$future4)=&Manifest::DecodeSourceLine($ref);
           
            #getting file basename
			my $basename=basename($fname);

            if (grep { $_ eq $basename } @{$fileNames}) {

				#Replacing all placeholders @<env_variable>@
				if ($self->replaceVariables("$source_dir/$fname") !=1 ){
                    return 0;
                }

            }

        }

        return 0;

}


#####################################################################################
# Sub:  replaceVariables 
# Desc: This function receives as parameter the filename to be processed, and will
#       search on the file content by placeholders with the pattern @<env_variable>@.
#       Then it checks if all the referred env variables are defined on the system,
#       and if they are, it will start replacing all the placeholders by the respective
#       variable content
# Arg:  The file name to be processed
# Ret:  1 on success, 0 on failure
#####################################################################################
sub replaceVariables{

    my $self=shift;
    my $file_name=shift;
    
    my %env_variables=();
    my @matches;

    open(FILE, "<$file_name") || die "The File '$file_name' not found";
    my @lines = <FILE>;
    close(FILE);

   
    # Collecting all the environemnt variables present in the file 
    foreach(@lines){
       @matches = ( $_ =~ /@([A-Za-z0-9_]+)@/g );
      
       foreach(@matches){
          $env_variables{$_}=1;
       }
    }


    # Checking if all the environment variables are defined in the system
    foreach(keys %env_variables){
	if (!exists $ENV{$_}){
           &::LogMsg("Error: The environmant variable '$_' does not exist");
           return 0;
        }
    }
     

    # Replacing the variables by their content
    my @newlines;
    foreach(@lines) {
      $_ =~ s/@([A-Za-z0-9_]+)@/$ENV{$1}/g;
      push(@newlines,$_);
    }

    open(FILE, ">$file_name") || die "File not found";
    print FILE @newlines;
    close(FILE);


} 


sub PreAction {
        my $self=shift;

        if($self->processFilesWithName(["RmsApps.properties"]) !=1 ){
            return 0;
        }

        if ($self->SUPER::PreAction()!=1) {
                return 0;
        }

        return 1;
}



#####################################################################################
# Sub:  processFilesWithName
# Desc: This function will search for files with file type 'rms_java_app_custom'.
#       For the resulting files, the function will check if the filename is on
#       the list of files given by parameter. If the file exists on that list,
#       the content of the file is processed and all the placeholders @<env_variable>@
#       will be replaced by the respective environment variable content.
# Arg:  List of filenames to be processed
# Ret:  1 on success, 0 on failure
#####################################################################################
sub processFilesWithName{

        my $self=shift;
        my $fileNames=shift;

        my $global_state=$self->GetORPatchGlobalState();

        my $source_dir=$global_state->GetSourceDir();

        my ($src_info_ref,$type_lookup_href,$distinct_types)=$global_state->GetSourceManifestInfo();

        # Filtering manifest data by 'rms_java_app_custom'
        my $filtered_list_ref=&Manifest::FilterSourceManifest($type_lookup_href,['rms_java_app_custom'],[]);

        #Iterating over each file with type 'rms_java_app_custom'
        foreach my $ref (@$filtered_list_ref) {
                my ($fname,$frevision,$ftype,$fpatch_name,$unit_test,$localization,$fcustomized,$fwp_rev,$future1,$future2,$future3,$future4)=&Manifest::DecodeSourceLine($ref);

            #getting file basename
        my $basename=basename($fname);

        if (grep { $_ eq $basename } @{$fileNames}) {


        #Replacing all placeholders @<env_variable>@
        if ($self->replaceVariables("$source_dir/$fname") !=1 ){
        
		return 0;
                }

            }

        }

        return 1;

}


#####################################################################################
# Sub:  replaceVariables
# Desc: This function receives as parameter the filename to be processed, and will
#       search on the file content by placeholders with the pattern @<env_variable>@.
#       Then it checks if all the referred env variables are defined on the system,
#       and if they are, it will start replacing all the placeholders by the respective
#       variable content
# Arg:  The file name to be processed
# Ret:  1 on success, 0 on failure
#####################################################################################
sub replaceVariables{

    my $self=shift;
    my $file_name=shift;

    my %env_variables=();
    my @matches;

    open(FILE, "<$file_name") || die "The File '$file_name' not found";

    my @lines = <FILE>;
    close(FILE);


    # Collecting all the environemnt variables present in the file
    foreach(@lines){
       @matches = ( $_ =~ /@([A-Za-z0-9_]+)@/g );

       foreach(@matches){
          $env_variables{$_}=1;
       }
    }


    # Checking if all the environment variables are defined in the system
    foreach(keys %env_variables){
        if (!exists $ENV{$_}){
           &::LogMsg("Error: The environmant variable '$_' does not exist");
           return 0;
        }
    }


    # Replacing the variables by their content
    my @newlines;
    foreach(@lines) {
      $_ =~ s/@([A-Za-z0-9_]+)@/$ENV{$1}/g;
      push(@newlines,$_);
    }

    open(FILE, ">$file_name") || die "File not found";
    print FILE @newlines;
    close(FILE);

    return 1;

}


1;