############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::EDGEUIDBSQL;
our @ISA = qw(ORPAction::DBSQL);


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
        my $action_name='DBSQL_EDGEUI';

        #Allow skipping all DBC scripts when DBSQL_RAF_RUN_DBC=N
        my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);
              
        #There is no separate RAF product yet, so we don't set this
        my $product_name='';
         
        #This is a more detailed data structure then what is sent to the ORPAction constructor
        #It allows us to keep track of how to handle files in a particular file type category
        #For example, always run all files or never run files (because they are run some other way),etc
        #
        #[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase]
        #   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
        #   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
        #   std_run_catch_errors is 1 if we should catch errors, 0 if not
        #   patch_product
        #   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
        my @file_type_info=(
        ['ui_db_install_scripts',	3,'',				1,$product_name,'PATCHACTION']
        );
        
        my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
        bless($self,$class);
        
        return $self;
}

1;
