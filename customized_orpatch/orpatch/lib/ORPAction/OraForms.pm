############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
# This is the base Action class for actions which need to compile forms
# It contains helper functions related to setting up the environment, running frmcmp, etc
use strict;

package ORPAction::OraForms;
our @ISA = qw(ORPAction);

use File::Copy;
#import the WNOHANG symbol
use POSIX ":sys_wait_h";

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;
	
	my $name=$self->GetActionName();
	&::LogMsg("Pre-patch check for $name");
	&::LogMsg("Validating database configuration");
	
	#Try our best to make sure the Oracle environment looks OK
	my $user_string=$self->{'CONNECT_STRING'};
	my $wallet_dir=$self->{'TNS_ADMIN'};
	my $oh=$self->{'ORACLE_HOME'};
	my $oi=$self->{'ORACLE_INSTANCE'};
	
	if ($user_string eq '') {
		&::LogMsg("ERROR: Missing connection string for $name");
		&::LogMsg("Should be defined in config file as ${name}_CONNECT");
		return 0;
	}
	
	if (!-d $wallet_dir) {
		&::LogMsg("ERROR: Wallet directory $wallet_dir does not exist!");
		&::LogMsg("Should be defined in config file as ${name}_WALLET");
		return 0;
	}
	
	if (!-d $oh) {
		&::LogMsg("ERROR: ORACLE_HOME $oh does not exist!");
		&::LogMsg("Should be defined in config file as APP_ORACLE_HOME");
		return 0;
	}
	
	if (!-d $oi) {
		&::LogMsg("ERROR: ORACLE_INSTANCE $oi does not exist!");
		&::LogMsg("Should be defined in config file as APP_ORACLE_INSTANCE");
		return 0;
	}

	#Check our SQL connection
	if ($self->CheckDBStatus()!=1) {
		&::LogMsg("Unable to connect to DB, aborting");
		return 0;
	}
	
	if (exists($ENV{DISPLAY})) {
		my $cfg_display=$self->{'DISPLAY'};
		my $env_display=$ENV{'DISPLAY'};
		
		#If they have DISPLAY defined in the environment, use that instead
		&::LogMsg("Ignoring env_info.cfg setting for DISPLAY, using DISPLAY=$env_display");
		$self->{'DISPLAY'}=$env_display;
	} else {
		if ($self->{'DISPLAY'} eq '') {
			&::LogMsg("ERROR: DISPLAY must be set, either in env_info.cfg or in environment");
			return 0;
		}
		$ENV{DISPLAY}=$self->{'DISPLAY'};
		&::LogMsg("Using DISPLAY = ".$self->{'DISPLAY'});
	}

	my $fp=$self->{'FORMS_PROFILE'};
	if ($fp ne '' && ! -f $fp) {
		&::LogMsg("WARNING: specified forms profile $fp does not exist, ignoring");
	}
	
	if ($full_check==1) {
		if ($self->CheckFrmcmp()!=1) {
			&::LogMsg("Unable to run frmcmp help=y, aborting");
			return 0;
		}
	}
	
	return 1;
}

########################
# Sub:  PostAction
# Desc: Compile our Forms files based on file_type_info
# Ret:  1 if all files compiled successfully, 0 if not
########################
sub PostAction {
	my $self=shift;
	
	my $fcp_ref=$self->{'FILE_COPY_PARAMS'};
	my $patch_mode=$self->{'PATCH_MODE'};
	
	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my $dest_dir=$global_state->GetDestDir();
	my $restart_state=$global_state->GetRestartState();
	my $patch_name=$patch_info->GetPatchName();
	my @patch_list=$patch_info->GetPatchList();
	
	#Our action info is in the form dest_subdir:file_type
	my $restart_action_info=$restart_state->GetActionInfo();
	my ($restart_dest_subdir,$restart_file_type,$full_compile)=$self->DecodeRestartPoint($restart_action_info);
	
	if ($full_compile ne '') {
		$self->SetFullCompileFlag();
	}
	
	#If we are not doing a hotfix, always full compile everything
	unless ($patch_mode eq 'HOTFIX') {
		$self->SetFullCompileFlag();
	}
	
	my $fcp_index=0;
	if ($restart_dest_subdir ne '') {
		#Skip ahead to the appropriate file copy param
		&::LogMsg("Restarting with subdirectory: $restart_dest_subdir");
		$fcp_index=$self->FindFileCopyParamsIndex($restart_dest_subdir,$fcp_ref);
	}
				
	#Loop through each file destination
	for (;$fcp_index<scalar(@$fcp_ref);$fcp_index++) {
		my $ref=$$fcp_ref[$fcp_index];
		my ($ft_ref,$dest_subdir)=$ref->GetBasicParams();
		
		&::LogMsg("Processing $dest_subdir (".join(',',@$ft_ref).")");
		
		my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_subdir);
		
		my $bin_dir=$self->GetBinDir($full_dest);
	
		#Read the destination manifest
		my @dest_info=&Manifest::ReadManifest($dest_manifest);
		
		my $ft_index=0;
		if ($restart_file_type ne '') {
			#Skip ahead to the appropriate file type
			&::LogMsg("Restarting with file type $restart_file_type");
			$ft_index=$self->FindFTIndex($restart_file_type,$ft_ref);
		} 
		
		if ($restart_file_type eq '' || $ft_index==0) {
			#Tell forms compile processes that they should create a backup dir if appropriate
			$self->SetBackupDirNeededFlag(1);
		} else {
			$self->SetBackupDirNeededFlag(0);
		}
		
		#Loop through file types
		for(;$ft_index<scalar(@$ft_ref);$ft_index++) {
			my $type=$$ft_ref[$ft_index];
			my ($when_run,$process_func,$std_catch_errors,$keep_plx)=$self->GetFileTypeInfo($type);
	
			if ($when_run==0) {
				#File type we never run, go to the next type
				&::LogMsg("Skipping file type $type as run_type is 0");
				next;
			}
		
			$self->MarkRestartPoint($restart_state,$dest_subdir,$type);

			my $detail_dir=$self->GetActionName();
			$detail_dir=~tr/A-Z/a-z/;
			&DetailLog::CleanDetailLogDir($detail_dir,$type);
			
			#Filter dest manifest by type
			my $all_f_dest_info=&Manifest::FilterDestManifest(\@dest_info,$type,'');
			
			#Filter out any files in env_manifest.csv that don't actually exist in the environment
			my $orig_f_dest_info=&Manifest::FilterDestManifestNonExistentFiles($full_dest,$all_f_dest_info);
			
			#Ensure further filtering is done on a copy so custom actions can see what the 'pre-filtered' list looked like
			my $filtered_dest_info=$orig_f_dest_info;
			
			#Filter files by whether they were updated with this patch
			#If this is a file type where we only load updated files (i.e. when_run 1)
			#Or we would normally run all files but we are applying a hotfix and only loading updated files
			if ($when_run==1 || 
				($patch_mode eq 'HOTFIX' && ($when_run==2 || $when_run==3 || $when_run==4))) {
				$filtered_dest_info=&Manifest::FilterDestManifest($filtered_dest_info,'',$patch_name,\@patch_list);
			}
			
			if ($when_run==4 && scalar(@$filtered_dest_info)!=0) {
				#If any files were updated trigger a full recompile
				$self->SetFullCompileFlag(1);
			}
			
			if ($process_func eq '') {
				#Run the files in order using the standard function
				if ($self->CompileDestFileList($full_dest,$bin_dir,$filtered_dest_info,$orig_f_dest_info,$std_catch_errors,$keep_plx,$type)!=1) {
					return 0;
				}
			} else {
				#A non-standard function
				if ($self->$process_func($full_dest,$bin_dir,$filtered_dest_info,$orig_f_dest_info,$patch_name,$dest_manifest,$restart_state,\@patch_list,$type)!=1) {
					return 0;
				}
			}
			
			#If this is a toolset-style type, force a full recompile of everything subsequent to it if any files changed
			if ($when_run==3 && scalar(@$filtered_dest_info)!=0) {
				$self->SetFullCompileFlag(1);
			}
		}
		
		#Once we've finished all types for this file copy destination
		#Don't treat this as a restart anymore
		#This ensures that all file types for subsequent FCP's are processed
		$restart_file_type='';
	}
	
	return 1;
}

#########################
# Sub:  CheckDBStatus
# Desc: Check if we have enough of an environment to connect to the database
# Ret:  1 if successful, 0 if not
#########################
sub CheckDBStatus {
	my $self=shift;
	
	my $user_string=$self->{'CONNECT_STRING'};
	
	&::LogMsg("Checking database status");
	my $sql="select 'UP' from dual";
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	my $status=$$output_ref[0];
	chomp($status);
	
	if ($status ne 'UP') {
		&::LogMsg("Failed to connect to database as $user_string");
		&::DebugOutput("select 'UP' from dual",@$output_ref);
		return 0;
	} else {
		&::LogMsg("Database connection check successful");
		return 1;
	}
}
	

#########################################################################################
# Helper functions

#########################
# Sub:  CompileDestFileList
# Desc: Compile a list of forms files, creating the bin directory, copying the files
#       and then executing the compiles for each one
# Args: src_dir - the source directory holding the files
#       filtered_dest_info - the filtered destination manifest list of files to compile
#       orig_f_dest_info - original filtered manifest info (differs in case of hotfix)
#       catch_errors - 1 if we should consider any FRM- message a failure to compile, 0 if just creating a file is enough
#       keep_plx - 1 if we should retain plx and reference forms fmx files, 0 if they should be removed
#       detail_log_subcat - the subcategory for our detail logs
# Ret:  1 if successful, 0 if not
#########################
sub CompileDestFileList {
	my $self=shift;
	my ($src_dir,$bin_dir,$filtered_dest_info,$orig_f_dest_info,$catch_errors,$keep_plx,$detail_log_subcat)=@_;
	
	my $print_file_copy_details=1;
	
	#Check if any files included required all files to be recompiled (i.e. plls or reference forms)
	if ($self->IsFullCompileRequired($filtered_dest_info,$orig_f_dest_info)==1) {
		#Tell future compiles they must recompile everything
		$self->SetFullCompileFlag();

		#Process the unfiltered list rather than just files updated with this patch
		$filtered_dest_info=$orig_f_dest_info;
		$print_file_copy_details=0;
	}
	
	#If we have not yet created a backup directory
	if ($self->GetBackupDirNeededFlag()==1) {
		#Create one if we are doing a full compile
		if ($self->GetFullCompileFlag()==1) {
			if ($self->CreateBackupDir($bin_dir)!=1) {
				return 0;
			}
		}
		#whether we created a backup or not, we've satisfied the check
		$self->SetBackupDirNeededFlag(0);
	}

	#Copy files from src to bin
	if ($self->CopyFormsFiles($filtered_dest_info,$src_dir,$bin_dir,$print_file_copy_details)!=1) {
		return 0;
	}
	
	my ($single_thread_dest_info,$streams_ref)=$self->SplitFormsList($filtered_dest_info);
	
	#Compile the plls and reference forms one at a time
	my $overall_ret=1;
	my @overall_failed=();
	
	my ($ret,$failed_file_ref)=$self->CompileFormsFiles($single_thread_dest_info,$bin_dir,$catch_errors,$keep_plx,$::LOG,$detail_log_subcat);
	
	#If we failed, keep track of the failing files
	if ($ret!=1) {
		$overall_ret=0;
		foreach my $name (@$failed_file_ref) {
			push(@overall_failed,$name);
		}
	}
	
	if (scalar(@$streams_ref)!=0) {
		#Compile the forms and menus in parallel
		#This just forks one child process per stream, and calls CompileFormsFiles on each
		my ($pret,$pfailed_file_ref)=$self->CompileFormsFilesParallel($streams_ref,$bin_dir,$catch_errors,$keep_plx,$detail_log_subcat);
		if ($pret!=1) {
			$overall_ret=0;
			foreach my $name (@$pfailed_file_ref) {
				push(@overall_failed,$name);
			}
		}
	}
	
	if ($overall_ret!=1) {
		&::LogMsg("One or more forms files failed to compile!");
		&::DebugOutput('failed files',@overall_failed);
		if ($self->{'IGNORE_FAILED_FORMS'}==1) {
			&::LogMsg("Ignoring failed forms based on configuration");
			$overall_ret=1;
		}
	}

	return $overall_ret;
}

#########################
# Sub:  CompileFormsFiles
# Desc: Compile a list of forms files, with variable error checking
#       optionally removing the generated plx and reference form fmx files
# Args: filtered_dest_info - the filtered destination manifest list of files to compile
#       bin_dir - the directory where the files have been copied to for compiling
#       catch_errors - 1 if we should consider any FRM- message a failure to compile, 0 if just creating a file is enough
#       keep_plx - 1 if we should retain plx and reference forms fmx files, 0 if they should be removed
#       detail_log_subcat - the subcategory for detail logs
# Ret:  1 if successful, 0 if not
#########################
sub CompileFormsFiles {
	my $self=shift;
	my ($filtered_dest_info,$bin_dir,$catch_errors,$keep_plx,$log_obj,$detail_log_subcat)=@_;
	
	#Override our normal log object with the one we were passed
	local $::LOG=$log_obj;
	
	my $user_string=$self->{'CONNECT_STRING'};
	$self->SetFRMOracleHome();
	
	my $max_tries=$self->{'COMPILE_ATTEMPTS'};
	
	my $ret=1;
	my @failed_files=();
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		my $dest_file=&::ConcatPathString($bin_dir,$dfile);
		
		my $file_type=$self->GetFormsFileType($dest_file);
		next if $file_type eq '';
		
		my @detail_output=();
		my $output_file='';
		my $output_ref='';
		my $error_ref='';
		my $last_output='';
		
		#We try each compilation up to 5 times because parallel compiles to the
		#same display sometimes collide
		my $failed=0;
		for(my $i=0;$i<$max_tries;$i++) {
			$failed=0;
			&::LogMsg("Compilation attempt #".($i+1).": $dest_file");
			push(@detail_output,"\#Compilation attempt #".($i+1).": $dest_file");
			($output_file,$output_ref,$error_ref)=$self->ExecFrmcmp($dest_file,$user_string,$file_type,0);
			
			push(@detail_output,@$output_ref);
			$last_output=$output_ref;
			
			if ($catch_errors==1) {
				#If we are catching errors, consider a failure if there are any errors
				if (scalar(@$error_ref)!=0) {
					$failed=1;
				}
			} else {
				#otherwise as long as a file was created, good enough
				if (!-f $output_file) {
					$failed=1;
				}
			}

			if ($failed==1) {
				&::LogMsg("Output file not generated, retrying compilation");
				sleep 1;  #Wait a sec before retrying
			} else {
				#No more tries if successful
				last;
			}
		}
		
		my $detail_dir=$self->GetActionName();
		$detail_dir=~tr/A-Z/a-z/;
		&DetailLog::CreateLog($detail_dir,$detail_log_subcat,$dfile,\@detail_output,($failed==1) ? 'err' : 'log');
		
		if ($failed==0) {
			&::LogMsg("Successful: $dest_file");
			if ($file_type eq 'library') {
				#libraries we remove the plx
				if ($keep_plx!=1) {
					&::RemoveFile($output_file);
				}
			} elsif ($file_type eq 'menu' || $file_type eq 'form') {
				#Forms and menus, remove the fmb/mmb
				&::RemoveFile($dest_file);
			} else {
				#Reference forms remove fmx
				&::RemoveFile($output_file);
			}
		} else {
			#Do not change this LogMsg string without updating the search logic
			#in the parallel compile funcion
			&::LogMsg("Failed: $dest_file");
			&::DebugOutput("Last failed compile attempt",@$last_output);
			$ret=0;
			push(@failed_files,$dest_file);
		}
	}
	
	return ($ret,\@failed_files);	
}

#########################
# Sub:  CompileFormsFilesParallel
# Desc: Compile a list of streams in parallel
# Args: streams_ref - a ref to a list of filtered destination manifest list of files to compile
#       bin_dir - the directory where the files have been copied to for compiling
#       catch_errors - 1 if we should consider any FRM- message a failure to compile, 0 if just creating a file is enough
#       keep_plx - 1 if we should retain plx and reference forms fmx files, 0 if they should be removed
#       detail_log_subcat - the subcategory for detail logs
# Ret:  1 if successful, 0 if not
#########################
sub CompileFormsFilesParallel {
	my $self=shift;
	my ($streams_ref,$bin_dir,$catch_errors,$keep_plx,$detail_log_subcat)=@_;

	my $parallel_count=scalar(@$streams_ref);
	
	&Parallel::RemoveAllChildren();
	&Parallel::RegisterSignalHandler();
	
	my @failed_files=();
	
	my $lfile=$::LOG->GetLogFile();
	my ($ldir,$fname)=&::GetFileParts($lfile,2);
	
	#Spawn one child for each stream of files
	&::LogMsg("Spawning $parallel_count children for compilation");
	for(my $i=0;$i<$parallel_count;$i++) {
		my $ref=$$streams_ref[$i];
		my $child_log_file=&::ConcatPathString($ldir,"orpatch_forms_${$}_child_${i}.log");

		my $cpid=$self->SpawnCompileChild($ref,$child_log_file,$bin_dir,$catch_errors,$keep_plx,$detail_log_subcat);
		if (!defined($cpid)) {
			#child failed to spawn!
			&::LogMsg("Child #$i failed to spawn!");
			foreach my $dref (@$ref) {
				my ($dfile,@discard)=&Manifest::DecodeDestLine($dref);
				push(@failed_files,$dfile);
			}
		} else {
			&::LogMsg("Child #$i spawned as pid $cpid");
			&Parallel::RegisterChild($cpid,$child_log_file);
		}
	}
	
	my $overall_ret=1;
	
	#Children spawned, wait for all of them to finish
	my @log_files=();
	while (scalar(&Parallel::GetKids())>0) {
		while((my $kid=waitpid(-1,WNOHANG)) > 0) {
			my $child_ret=($?>>8);
			my $status_text=($child_ret==0) ? 'all files compiled' : 'some files failed compilation';
			&::LogMsg("Child process $kid completed with status $child_ret ($status_text)");
			push(@log_files,&Parallel::GetChildInfo($kid));
			&Parallel::RegisterChildExit($kid);
			if ($child_ret!=0) {
				$overall_ret=0;
			}
		}
	}

	#Now look through our child log files
	&::LogMsg("All children completed, appending log files");
	$::LOG->SetVerbose(0);
	foreach my $lfile (@log_files) {
		&::LogMsg("--------- Start child log output ---------");
		unless(open(CHILD_LOG,"<$lfile")) {
			&::LogMsg("Unable to open log file $lfile: $!");
			next;
		}
		while(<CHILD_LOG>) {
			chomp;
			my $line=$_;
			if ($line=~m/Failed: (.*)$/) {
				$overall_ret=0;
				&::LogMsg("Found failed file: $1");
				push(@failed_files,$1);
			}
			$::LOG->LogMsgNoDate($line);
		}
		close(CHILD_LOG);
		&::LogMsg("--------- End child log output ---------");
		&::RemoveFile($lfile);
	}
	$::LOG->SetVerbose(1);
	
	&Parallel::UnRegisterSignalHandler();
	
	return ($overall_ret,\@failed_files);
}

#########################
# Sub:  SpawnCompileChild
# Desc: Create a copy of ourselves to compile forms
# Ret:  process id of the child process, or undef if the child failed to spawn
#########################
sub SpawnCompileChild {
	my $self=shift;
	my ($dest_info_ref,$child_log_file,$bin_dir,$catch_errors,$keep_plx,$detail_log_subcat)=@_;
	
	#Fork ourselves, immediately return undef if the fork failed
	defined(my $kid=fork) or return undef;
	
	unless ($kid) {
		#Child process
		#open a log to a specific filename, no appending, no timestamp
		my $child_log=new Log($child_log_file,0,'',0);
		#Clean-up the children array in the child process as it does not have children
		&Parallel::RemoveAllChildren();
		my ($ret,$failed_file_ref)=$self->CompileFormsFiles($dest_info_ref,$bin_dir,$catch_errors,$keep_plx,$child_log,$detail_log_subcat);
		exit(($ret==1) ? 0 : 1);  #The child always exits
	}
	
	#parent process
	return $kid;
}

#########################
# Sub:  CopyFormsFiles
# Desc: Copy all the forms files from src to bin
# Ret:  1 if successful, 0 if not
#########################
sub CopyFormsFiles {
	my $self=shift;
	my ($filtered_dest_info,$src_dir,$bin_dir,$file_details)=@_;
	
	#Make sure the files end up writable after copying
	my $orig_umask=umask(0022);

	my $file_count=scalar(@$filtered_dest_info);
	if ($file_count!=0) {
		&::LogMsg("Copying forms files from src to $bin_dir");
	}
	
	my $ret=1;
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		my $src_file=&::ConcatPathString($src_dir,$dfile);
		my $dest_file=&::ConcatPathString($bin_dir,$dfile);
		
		if (&File::Copy::copy($src_file,$dest_file)!=1) {
			&::LogMsg("Failed to copy $src_file to $dest_file: $!");
			$ret=0;
			last;
		}
		#Ensure the resultant forms have execute bits
		chmod 0755,$dest_file;
		$::LOG->LogMsgNoVerbose("Forms file $dfile copied to bin dir") if $file_details==1;
	}
	
	if ($file_count!=0) {
		&::LogMsg("   $file_count file(s) copied");
	}
	
	umask($orig_umask);
	
	return $ret;
}

#########################
# Sub:  CheckFrmcmp
# Desc: Check if we have frmcmp.sh in $ORACLE_INSTANCE/bin
# Ret:  1 if successful, 0 if not
#########################
sub CheckFrmcmp {
	my $self=shift;
	
	my $user_string=$self->{'CONNECT_STRING'};
	$self->SetFRMOracleHome();
	
	my $command="$::FRMCMP help=y";
        my $source_profile=$self->{'FORMS_PROFILE'};
        if ($source_profile ne '' && -f $source_profile) {
                $command=". $source_profile ; $command";
        }
	my @cmd_output=`$command 2>&1`;
	my $ret=$?;
	
	my @errors=grep(/FRM\-|ORA\-/,@cmd_output);
	
	if (scalar(@errors)!=0 || $ret!=0) {
		&::LogMsg("Failed to run frmcmp.sh help=y");
		&::DebugOutput("$::FRMCMP help=y",@cmd_output);
		return 0;
	}
	return 1;
}

########################
# Sub:  CallExecSQL
# Desc: Setup our environment and call ExecSQL for a single statement
#       DoSQL is prefered for use but this is a convenience for running a simple query
# Args: SQL statement to run, without trailing ;
# Ret:  ref to the array of output, ref to the array of errors
########################
sub CallExecSQL {
	my $self=shift;
	my ($sql)=@_;
	
	my $user_string=$self->{'CONNECT_STRING'};
	$ENV{TNS_ADMIN}=$self->{'TNS_ADMIN'};
	&::SetOracleHome($self->{'ORACLE_HOME'});
	$ENV{MMHOME}=$ENV{RETAIL_HOME};
	
	my ($output_ref,$error_ref)=&::ExecSQL($user_string,$sql,0);
	
	return ($output_ref,$error_ref);
}


sub DecodeRestartPoint {
	my $self=shift;
	my ($restart_point)=@_;
	
	my ($dest_subdir,$file_type,$full_compile)=split(/\:/,$restart_point);
	return ($dest_subdir,$file_type,$full_compile);
}

sub MarkRestartPoint {
	my $self=shift;
	my ($restart_state,$dest_subdir,$type)=@_;
	
	my $full_compile='';
	if ($self->GetFullCompileFlag()==1) {
		$full_compile=1;
	}
	
	return $restart_state->SetAndSaveActionInfo("$dest_subdir:$type:$full_compile");
}

#########################
# Sub:  GetBinDir
# Desc: Take a src dir, and return the bin equivalent
# Args: full path to the src dir
# Ret:  full path to the bin dir
#########################
sub GetBinDir {
	my $self=shift;
	my ($src_dir)=@_;
	
	my @parts=split(/\//,$src_dir);
	my $src=pop(@parts);
	push(@parts,'bin');
	
	return join('/',@parts);
}
	
#########################
# Sub:  ExecFrmcmp
# Desc: Compile a form/menu/pll
# Args: 
#   file to compile
#   userid string for the database connection
#   file_type - library, form or menu
#   error level - 0 to ignore, 1 to log, 2 to die, 3 debug on errors, 4 always debug
# Ret:  ref to list of output,ref to list of errors
#########################
sub ExecFrmcmp {
	my $self=shift;
	my ($file,$userid,$file_type,$error_level)=@_;
	
	my $cmd_line_type=$file_type;
	if ($file_type eq 'ref_form') {
		$cmd_line_type='form';
	}
	
	my $command="$::FRMCMP module=$file userid=$userid module_type=$cmd_line_type ";
	if ($file_type eq 'library' || $file_type eq 'ref_form') {
		$command.="logon=yes script=no compile_all=yes";
	}

	my $output_file=$file;
	$output_file=~s/\.pll$/.plx/;
	$output_file=~s/\.fmb$/.fmx/;
	$output_file=~s/\.mmb$/.mmx/;
	
	if (-f $output_file) {
		&::RemoveFile($output_file);
	}
	
	#Ensure frmcmp is run the directory where the file exists, so the pll/reference forms
	#are generated back into the original file
	my ($src_dir)=&::GetFileParts($file,2);
	$command="cd $src_dir ; $command";
	
	my $source_profile=$self->{'FORMS_PROFILE'};	
	if ($source_profile ne '' && -f $source_profile) {
		$command=". $source_profile ; $command";
	}

	#This didn't seem to impact the frmcmp output file permissions mode
	#my $orig_umask=umask(0022);
	
    my @cmd_output=`$command 2>&1`;
	my $status=$?;
	
	#umask($orig_umask);
	
	&::DebugOutput('frmcmp',@cmd_output) if ($error_level==4);
	
	my @errors=grep(/ORA\-|SP2\-|FRM\-/,@cmd_output);
	
	if (!-f $output_file) {
		&::DebugOutput('frmcmp',@cmd_output) if ($error_level==3);
		
		if ($error_level>0) {
			my $message="Compilation of form $file did not generate $output_file";
            &::LogMsg($message,@errors);
			push(@errors,$message);

            if ($error_level==2) {
                die "Error while running frmcmp: $errors[0]";
            }
        }
	} else {
		#Ensure the fmx/mmx/plx has execute permissions
		#&::LogMsg("Updating permissions on $output_file");
		chmod 0755,$output_file;
	}
	
	if (wantarray) {
        return($output_file,\@cmd_output,\@errors);
    } else {
        return(scalar(@errors)==0);
    }
}

#########################
# Sub:  SetFRMOracleHome
# Desc: Setup our environment to use a particular ORACLE_HOME for frmcmp
# Args: oracle_home
#########################
sub SetFRMOracleHome {
	my $self=shift;
	
	$ENV{TNS_ADMIN}=$self->{'TNS_ADMIN'};
	my $oh=$self->{'ORACLE_HOME'};
	my $oi=$self->{'ORACLE_INSTANCE'};
	
	#Prevent excessive growth of PATH, LD_LIBRARY_PATH, SHLIB_PATH and LIBPATH
	#if we are called over and over in the same session
	#
	#Note that this could cause unintended consequences if someone modifies these variables
	#after the first call to SetFRMOracleHome
	#If you need to do that, call ResetFRMOracleHome($oh) first to reset these variables
	$::FRM_ORIG_PATH=$ENV{PATH} if $::FRM_ORIG_PATH eq '';
	$::FRM_ORIG_LDL_PATH=$ENV{LD_LIBRARY_PATH} if $::FRM_ORIG_LDL_PATH eq '';
	$::FRM_ORIG_SL_PATH=$ENV{SHLIB_PATH} if $::FRM_ORIG_SL_PATH eq '';
	$::FRM_ORIG_LIB_PATH=$ENV{LIB_PATH} if $::FRM_ORIG_LIB_PATH eq '';
	
	$ENV{ORACLE_HOME}=$oh;
	$ENV{ORACLE_INSTANCE}=$oi;
	$ENV{PATH}="${oh}/bin:${oi}/bin:$::FRM_ORIG_PATH";
	$ENV{LD_LIBRARY_PATH}="${oh}/lib:$::FRM_ORIG_LDL_PATH";
	$ENV{SHLIB_PATH}="${oh}/lib:$::FRM_ORIG_SL_PATH";
	$ENV{LIBPATH}="${oh}/lib:$::FRM_ORIG_LIB_PATH";
	
	$::FRMCMP="$oi/bin/frmcmp.sh";
	return 1;
}

#########################
# Sub:  ResetFrmOracleHome
# Desc: Remove an ORACLE_HOME from environment variables
# Args: oracle_home
#########################
sub ResetFRMOracleHome {
	my $self=shift;
	
	my $oh=$self->{'ORACLE_HOME'};
	my $oi=$self->{'ORACLE_INSTANCE'};
	
	$ENV{PATH}=~s/^${oh}\/bin://;
	$ENV{PATH}=~s/^${oi}\/bin://;
	$::FRM_ORIG_PATH='';
	$ENV{LD_LIBRARY_PATH}=~s/^${oh}\/lib://;
	$::FRM_ORIG_LDL_PATH='';
	$ENV{SHLIB_PATH}=~s/^${oh}\/lib://;
	$::FRM_ORIG_SL_PATH='';
	$ENV{LIBPATH}=~s/^${oh}\/lib://;
	$::FRM_ORIG_LIB_PATH='';

	$::FRMCMP="frmcmp.sh";
	
	return 1;
}

#########################
# Sub:  CreateBackupDir
# Desc: Rename the bin dir to a backup name and create a new one
# Args: bin_dir
#########################
sub CreateBackupDir {
	my $self=shift;
	my ($bin_dir)=@_;
	
	my $bkp_dir="${bin_dir}-".&::GetTimeStamp('DATE-TIME');
	
	if (-l $bin_dir) {
		#If the bin_dir is a symlink
		&::LogMsg("Unlinking $bin_dir");
		unlink($bin_dir);
	} elsif (-d $bin_dir) {
		&::LogMsg("Moving $bin_dir to $bkp_dir");
		if (&File::Copy::move($bin_dir,$bkp_dir)!=1) {
			&::LogMsg("Unable to rename $bin_dir to $bkp_dir: $!");
			return 0;
		}
	}
	
	if (&::CreateDirs('0755',$bin_dir)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  SplitFormsList
# Desc: Split a destination manifest list into two lists - files to single thread compile, files to parallelize
# Args: filtered_dest_info - destination manifest list of files
# Ret:  ref to single threaded file list, ref to a list of parallel file lists
#########################
sub SplitFormsList {
	my $self=shift;
	my ($filtered_dest_info)=@_;
	
	my $pd=$self->{'PARALLEL_DEGREE'};
	#If we are not doing parallel, return immediately
	return ($filtered_dest_info,[]) if ($pd==1);
	
	my @single_thread=();
	my @parallel=();
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $file_type=$self->GetFormsFileType($dfile);
		next if ($file_type eq '');
		
		if ($file_type eq 'library' || $file_type eq 'ref_form') {
			push(@single_thread,$ref);
		} else {
			push(@parallel,$ref);
		}
	}
	
	#Split the parallel list into a number of arrays equal to the PARALLEL_DEGREE setting
	
	#We never allow more children than files to process
	if ($pd>scalar(@parallel)) {
		$pd=scalar(@parallel);
	}
	
	#Initialize the streams list to empty arrays
	my @streams=();
	for(my $i=0;$i<$pd;$i++) {
		$streams[$i]=[];
	}
	
	#Round robin files into each stream
	my $index=0;
	foreach my $ref (@parallel) {
		push(@{$streams[$index]},$ref);
		$index++;
		if ($index==$pd) {
			$index=0;
		}
	}
	
	# #Verify we didn't lose anything
	# &::LogMsg("Original file list had: ".scalar(@$filtered_dest_info)." entries");
	# &::LogMsg("Single threaded: ".scalar(@single_thread));
	# for(my $i=0;$i<$pd;$i++) {
		# &::LogMsg("Stream $i: ".scalar(@{$streams[$i]}));
	# }
	
	return (\@single_thread,\@streams);
	
}

#########################
# Sub:  IsFullCompileRequired
# Desc: Compare a filtered and an unfiltered dest manifest list to see if we should force a full compile
# Arg:  filtered manifest info, unfiltered manifest info
# Ret:  1 if a full recompile is required, 0 if not
#########################
sub IsFullCompileRequired {
	my $self=shift;
	my ($filtered_info,$unfiltered_info)=@_;
	
	#If the full compile flag is set, a full compile is required
	if ($self->GetFullCompileFlag()==1) {
		return 1;
	}
	
	foreach my $ref (@$filtered_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $file_type=$self->GetFormsFileType($dfile);
		if ($file_type eq 'library' || $file_type eq 'ref_form') {
			#Full compile will be required if any library or reference form is included
			return 1;
		}
	}
	
	return 0;
}

#########################
# Sub:  GetFileTypeInfo
# Desc: Get the details on how to run a specific file type
# Arg:  file type
# Ret:  when_run,process_function,std_catch_errors,keep_plx
#########################
sub GetFileTypeInfo {
	my $self=shift;
	my ($file_type)=@_;
	
	my $def_when_run=1;
	my $def_process_func='';
	my $def_std_catch_errors=0;
	my $def_keep_plx=0;
	foreach my $ref (@{$self->{'FILE_TYPE_INFO'}}) {
		if ($$ref[0] eq $file_type) {
			my $when_run=$$ref[1];
			$when_run=$def_when_run if ($when_run eq '');
			my $process_func=$$ref[2];
			$process_func=$def_process_func if ($process_func eq '');
			my $std_catch_errors=$$ref[3];
			$std_catch_errors=$def_std_catch_errors if ($std_catch_errors eq '');
			my $keep_plx=$$ref[4];
			$keep_plx=$def_keep_plx if ($keep_plx eq '');
			return ($when_run,$process_func,$std_catch_errors,$keep_plx);
		}
	}

	return ($def_when_run,$def_process_func,$def_std_catch_errors,$def_keep_plx);
}

sub FindFTIndex {
	my $self=shift;
	my ($restart_file_type,$ft_ref)=@_;
	
	my $ft_index=0;
	for(;$ft_index<scalar(@$ft_ref);$ft_index++) {
		my $this_type=$$ft_ref[$ft_index];
		if ($this_type eq $restart_file_type) {
			return $ft_index;
		}
	}
	#Couldn't find it, start from beginning
	return 0;
}

sub SetFullCompileFlag {
	my $self=shift;
	
	$self->{'FORCE_FULL_COMPILE'}=1;
}

sub GetFullCompileFlag {
	my $self=shift;
	return $self->{'FORCE_FULL_COMPILE'};
}

sub SetBackupDirNeededFlag {
	my $self=shift;
	my ($flag)=@_;
	$self->{'BACKUP_DIR_NEEDED'}=$flag;
}

sub GetBackupDirNeededFlag {
	my $self=shift;
	return $self->{'BACKUP_DIR_NEEDED'};
}

#########################
# Sub:  GetFormsFileType
# Desc: Classify a forms file as library,menu,form or ref_form
# Arg:  file name
# Ret:  'library','menu','form' or 'ref_form'
#########################
sub GetFormsFileType {
	my $self=shift;
	my ($dest_file)=@_;
	
	my ($fpath,$fbase,$fext)=&::GetFileParts($dest_file,3);

	my $file_type='';
	if ($fext eq 'pll') {
		$file_type='library';
	} elsif ($fext eq 'mmb') {
		$file_type='menu';
	} elsif ($fext eq 'fmb') {
		if ($fbase=~/^fm\_/) {
			$file_type='ref_form';
		} else {
			$file_type='form';
		}
	} else {
		#Not a forms file
	}
	
	return $file_type;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($name,$file_copy_params,$file_type_info,$config_ref)=@_;
	
	my $dest_dir=$name;
	$dest_dir=~tr/A-Z/a-z/;
	
	my $self=$class->SUPER::new($name,$file_copy_params,$config_ref);
	
	$self->{'FILE_TYPE_INFO'}=$file_type_info;
	
	my $cs=$self->{'CONNECT_STRING'}=$config_ref->Get("${name}_CONNECT");
	my $ta=$self->{'TNS_ADMIN'}=$self->ResolveConfigPath($config_ref->Get("${name}_WALLET"));
	my $oh=$self->{'ORACLE_HOME'}=$config_ref->Get("APP_ORACLE_HOME");
	my $oi=$self->{'ORACLE_INSTANCE'}=$config_ref->Get('APP_ORACLE_INSTANCE');
	my $display=$self->{'DISPLAY'}=$config_ref->Get('DISPLAY');
	my $hfm=$self->{'PATCH_MODE'}=$config_ref->Get('PATCH_MODE');
	$self->{'FORMS_PROFILE'}=$self->ResolveConfigPath($config_ref->Get("${name}_PROFILE"));
	$self->{'PARALLEL_DEGREE'}=$config_ref->Get("${name}_PARALLEL_DEGREE");
	if ($self->{'PARALLEL_DEGREE'} eq '') {
		$self->{'PARALLEL_DEGREE'}=5;
	}
	$self->{'COMPILE_ATTEMPTS'}=$config_ref->Get("${name}_COMPILE_ATTEMPTS");
	if ($self->{'COMPILE_ATTEMPTS'} eq '') {
		$self->{'COMPILE_ATTEMPTS'}=5;
	}
	$self->{'FORCE_FULL_COMPILE'}='';
	$self->{'BACKUP_DIR_NEEDED'}=1;

	$self->SetTFFlagFromConfigVar($config_ref,'IGNORE_FAILED_FORMS','ORAFORMS',0);
	
    return $self;
}

1;
