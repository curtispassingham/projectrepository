############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RWMSADFDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#If this is a full check, register our need for a working DBSQL_RWMS configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_RWMS');
		$global_state->AddExtraPrePatchCheck('DBSQL_RWMSUSER');
	}
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;
	
	#Compile Invalid Objects
	if ($self->CompileInvalids()!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadRWMSADFInstallScripts
# Desc: a custom function to load the RWMS ADF Install Scripts
#########################
sub LoadRWMSADFInstallScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	my $install_script='xdomain_queues.sql';
	my $grant_script='grant_access_to_rwms_adf_user_objects.sql';
	
	my ($xd_run,$xd_ref)=$self->FindScriptHelper($filtered_dest_info,$orig_f_dest_info,$install_script);
	
	if ($xd_run==1 && $xd_ref eq '') {
		&::LogMsg("Unable to find RWMS ADF_USER xdomain script $install_script");
		return 0;
	}
	
	#If the xdomain script needs to be run
	if ($xd_run==1) {
		if ($self->RunDestFileList($full_dest,[$xd_ref],1)!=1) {
			return 0;
		}
	}
	
	#Find the RWMS schema owner
	my $rwms_owner=$self->GetCrossActionSchemaName('DBSQL_RWMS');
	return 0 if $rwms_owner eq '';
	
	my $rwms_user=$self->GetCrossActionSchemaName('DBSQL_RWMSUSER');
	return 0 if $rwms_user eq '';
	
	#Run the grants on our payload to RWMS
	
	#Note that we always search the original unfiltered manifest info, because if we recreate the type
	#the grant may need to be redone, even though the grant script didn't 'change'
	my $gref=$self->FindFileByShortName($orig_f_dest_info,$grant_script);
	if ($gref eq '') {
		&::LogMsg("Unable to find RWMS_ADF_USER grant script $grant_script");
		return 0;
	}
	
	foreach my $schema ($rwms_owner,$rwms_user) {	
		my $args="$schema";
		if ($self->RunDestFileListWithArgs($full_dest,$args,[$gref],[])!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  FindScriptHelper
# Desc: Look for a specific script and see if it was filtered for a legitimate reason
# Args: filtered info, original info, install script to find
# Rets: run_status (1 if it needs to be run, 0 if not), ref to the script
#       1 and a ref of '' indicates an error that is likely fatal
#########################
sub FindScriptHelper {
	my $self=shift;
	my ($filtered_dest_info,$orig_f_dest_info,$install_script)=@_;
	
	my $ref=$self->FindFileByShortName($filtered_dest_info,$install_script);
	my $in_dbmanifest=0;
	if ($ref eq '') {
		#It's not in the filtered list, was it in the original?
		my $oref=$self->FindFileByShortName($orig_f_dest_info,$install_script);
		if ($oref eq '') {
			#Unable to find the script and not in DB manifest
			return (1,'');
		} else {
			#Found script but it is in the dbmanifest
			return (0,'');
		}
	}
	
	#the script exists and needs to be run
	return (1,$ref);
}


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	my @file_type_info=(
	['rwmsadf_db_install_scripts',	3,'LoadRWMSADFInstallScripts',1],
	);
	
	my $self=$class->SUPER::new('DBSQL_RWMSADF',\@file_type_info,$config_ref);
	$self->SetCheckLockedObjects(0);
	
    bless($self,$class);

	return $self;
}

1;
