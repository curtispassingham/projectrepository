############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RASECoreDBSQL;
our @ISA = qw(ORPAction::SIABaseDBSQL);

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	my $self=$class->SUPER::new($config_ref,'DBSQL_RASECORE','rasecore','','core_db');
    bless($self,$class);
	
	$self->SetUniqueDBManifest(1);

	return $self;
}

1;
