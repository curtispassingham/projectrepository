############################################################################
# Copyright © 2013,20142018, Oracle and/or its affiliates. All rights reserved.
# ORC - Custom
############################################################################
use strict;

package ORPAction::XXADEO_RPASDBSQL;
our @ISA = qw(ORPAction::DBSQL);
use File::Basename;
########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
        my $self=shift;
        my ($full_check)=@_;

        #Run the default DBSQL check
        if ($self->SUPER::PrePatchCheck($full_check)!=1) {
                return 0;
        }
        #If this is a full check, register our need for a working DBSQL_RMS configuration
        if ($full_check==1) {
                        my $global_state=$self->GetORPatchGlobalState();
                        $global_state->AddExtraPrePatchCheck('DBSQL_RMS');
        }
        return 1;

}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
        my $self=shift;
        my ($source_dir,$source_manifest,$dest_dir,$patch_name)=@_;

        #Compile Invalid Objects
        if ($self->CompileInvalids()!=1) {
                return 0;
        }

        #Run our PostAction registered SQL scripts (grant scripts)
        if ($self->SUPER::PostAction()!=1) {
                return 0;
        }

        return 1;
}


sub CreateSynonymsForXXSchema {
        my $self=shift;
        my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
        my @filter=(
        );

         #Find our username
         my $schema_name=$self->GetSchemaName();
         return 0 if $schema_name eq '';

         #Run all the filtered destinations
        foreach my $ref (@$filtered_dest_info) {
                   my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);

                   my $full_file=&::ConcatPathString($full_dest,$dfile);


           #Find the target owner from file name
           my ($name,$path,$suffix) = fileparse($dfile,qr"\..[^.]*$");
                    my @fields = split /\./, $name;
                        my $target = $fields[1];
                #Get schema name from wallet
                  &::LogMsg("Trying to find schema name for $target, and dfile: $dfile");
                my $target_owner=$self->GetCrossActionSchemaName('DBSQL_'.$target);
                return 0 if $target_owner eq '';

                my $args="$schema_name $target_owner";

                   &::LogMsg("Running $dfile with args: $args");
                   if ($self->RunSQLFileWithArgs($full_file,$args,$sfile,$frevision,$ftype,$fdesc,\@filter,1)!=1) {
                        return 0;
                   }
            }

return 1;
}



sub CreateGrantsForXXSchema {
                my $self=shift;
        my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
       my @filter=(qr/ORA\-06512: at line|ORA\-04042|ORA\-00942: table or view does not exist/);

         #Find our username
         my $schema_name=$self->GetSchemaName();
         return 0 if $schema_name eq '';

         #Run all the filtered destinations
        foreach my $ref (@$filtered_dest_info) {
                   my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);

                   my $full_file=&::ConcatPathString($full_dest,$dfile);


           #Find the target owner from file name
            my ($name,$path,$suffix) = fileparse($dfile,qr"\..[^.]*$");
                    my @fields = split /\./, $name;
                        my $target = $fields[1];
                #Get schema name from wallet
                &::LogMsg("Trying to find schema name for $target, and dfile: $dfile");


                my $action_name='DBSQL_'.$target;

                my $target_owner=$self->GetCrossActionSchemaName($action_name);
                return 0 if $target_owner eq '';

                 #Get a handle to the DBSQL_$target action
                my $global_state=$self->GetORPatchGlobalState();
                my $handle=$global_state->GetActionHandle($action_name);

                my $args="$schema_name $target_owner";

                   &::LogMsg("Running $dfile with args: $args and with handler: $action_name");
                   if ($handle->RunSQLFileWithArgs($full_file,$args,$sfile,$frevision,$ftype,$fdesc,\@filter,1)!=1) {
                        return 0;
                   }
            }
return 1;
}
#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
        my $class=shift;
        my ($config_ref)=@_;

        my $action_name='DBSQL_XXADEO_RPAS';

        #We use this in the db_rib_top function, so we want to make sure the
        #name matches identically
        my $rib_subtype_name='test_build_db_rib';
        my $iscripts_subtype_name='test_build_db_installscripts';

        #Allow skipping all DBC scripts when DBSQL_XXADEO_RPAS_RUN_DBC=N
        my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);

        #This is a more detailed data structure then what is sent to the ORPAction constructor
        #It allows us to keep track of how to handle files in a particular file type category
        #For example, always run all files or never run files (because they are run some other way),etc
        #
        #[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase,del_func]
        #   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
        #   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
        #   std_run_catch_errors is 1 if we should catch errors, 0 if not
        #   patch_product
        #   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
        #   del_func - Optional function that should be run whenever a file of this type is deleted
        my @file_type_info=(

        # File_type                          when_run      process_function        sed_run_catch_errors   patch_product           run_phase        del_func
        ['xxadeo_rpas_db_synonyms',             2,          'CreateSynonymsForXXSchema',    1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_grants',               2,          'CreateGrantsForXXSchema',      1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_sequences',            2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_ddl',                  2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_types',                2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_procedures',           2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_packages',             2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_triggers',             2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_views',                2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_tables',               2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_control_scripts',      2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_change_scripts',       2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_functions',            2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        ['xxadeo_rpas_db_dml',                  2,          '',                             1,               'XXADEO_RPAS',         'PATCHACTION',       ''],
        );


        my $self=$class->SUPER::new('DBSQL_XXADEO_RPAS',\@file_type_info,$config_ref);
        $self->{'RIB_SUBTYPE_NAME'}=$rib_subtype_name;
        $self->{'ISCRIPT_SUBTYPE_NAME'}=$iscripts_subtype_name;

        return $self;
}

1;
