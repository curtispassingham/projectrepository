############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::ReIMDBSQL;
our @ISA = qw(ORPAction::DBSQL);

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;

	#Compile Invalid Objects
	if ($self->CompileInvalidsInSharedSchema('DBSQL_RMS')!=1) {
		return 0;
	}
	
	if ($self->CreateUserSynonyms('RMS')!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadReIMPackages
# Desc: a custom function to load ReIM pl/sql and skip the startall.sql file
#########################
sub LoadReIMPackages {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	my $filtered=[];
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		if ($dfile=~/startall.sql/) {
			#Skip the startall file
		} else {
			push(@$filtered,$ref);
		}
	}
	
	return $self->RunDestFileList($full_dest,$filtered,1);
}

#########################
# Sub:  LoadReIMInstallScripts
# Desc: a custom function to load the ReIM Install Scripts
#########################
sub LoadReIMInstallScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	my @install_scripts=('im_base_data.sql','run_im_demo_data.sql');
	
	my ($proceed,$found_scripts_ref,$missing_scrips_ref)=$self->FindFilteredFilesInOrder($filtered_dest_info,$orig_f_dest_info,@install_scripts);
	
	#If required files were missing, abort
	unless($proceed==1) {
		return 0;
	}

	if (scalar(@$found_scripts_ref)==0) {
		#No install scripts to run
		return 1;
	}
			
	#Run each install script by changing into the install scripts dir and executing it without a path
	return $self->RunDestFileListByBaseName($full_dest,$found_scripts_ref,[]);
}

#########################
# Sub:  GetCheckLockedObjectsSQL
# Desc: Override the default check locked object SQL statement so that we only look for ReIM objects if
#       only ReIM objects are being migrated
# Ret:  SQL statement to use when checking locked objects
#########################
sub GetCheckLockedObjectSQL {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my @product_list=$patch_info->GetProductList();
	
	my $SQL='';
	
	if (scalar(@product_list)==1 && $product_list[0] eq 'REIM') {
		#If there is only one product and it is REIM, only look for locked object names that belong to REIM
		$SQL=<<ENDSQL;
alter session set "_optimizer_cartesian_enabled"=false;
SELECT a.session_id||'|'||a.oracle_username||'|'||count(1)
FROM V\$LOCKED_OBJECT A,user_objects b
WHERE A.OBJECT_ID = B.OBJECT_ID
and ((b.object_name like 'IM_\%' or b.object_name like 'TABLE_IM_\%' or b.object_name like 'REIM_\%' or b.object_name like 'OBJ_REIM_\%'
      or b.object_name like 'V_IM_\%')
     or
	 (b.object_name in ('SHIPMENT_API_SQL','NUMBERLIST_TBL','OBJ_ITEM_TAX_BREAK_TBL','OBJ_ITEM_TAX_BREAK_REC','OBJ_ITEM_TAX_BREAKUP_TBL',
	                    'OBJ_ITEM_TAX_BREAKUP_REC','OBJ_ITEM_TAX_CALC_OVRD_TBL','OBJ_ITEM_TAX_CALC_OVRD_REC','OBJ_ITEM_TAX_CRITERIA_TBL',
						'OBJ_ITEM_TAX_CRITERIA_REC','OBJ_VENDOR_DATA_TBL','OBJ_VENDOR_DATA_REC','OBJ_VENDOR_TAX_DATA_TBL',
						'OBJ_VENDOR_TAX_DATA_REC','OBJ_DOC_ID_TBL','OBJ_DOC_ID_REC','SHIPMENT_IDS','SHIPMENT_MATCHED_QTY_ARRAY',
						'SHIPMENT_MATCHED_QTYS'))
	)
group by a.session_id,a.oracle_username
ENDSQL
	} else {
		#If multiple products are involved, use the standard SQL
		$SQL=$self->SUPER::GetCheckLockedObjectSQL();
	}

	return $SQL;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='DBSQL_REIM';
	
	#Allow skipping all DBC scripts when DBSQL_REIM_RUN_DBC=N
	my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   patch_product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	my @file_type_info=(
	['reim_db_ddl',				1,'',						1,'REIM','PATCHACTION'],
	['reim_db_types',			2,'LoadTypes',				1,'REIM','PATCHACTION'],
	['reim_db_change_scripts',	$dbc_mode,'',				1,'REIM','PATCHACTION'],
	['reim_db_packages',		2,'LoadReIMPackages',		1,'REIM','PATCHACTION'],
	['reim_db_triggers',		2,'',						1,'REIM','PATCHACTION'],
	['reim_db_control_scripts',	2,'',						1,'REIM','PATCHACTION'],
	['reim_db_install_scripts',	3,'LoadReIMInstallScripts',	1,'REIM','CLEANUPACTION'],
	);
	
	my $self=$class->SUPER::new('DBSQL_REIM',\@file_type_info,$config_ref);
	bless($self,$class);
	
	$self->SetUniqueDBManifest(0);
	
	#We override the connect information to use RMS' info
	my $name='DBSQL_RMS';
	$self->{'CONNECT_STRING'}=$config_ref->Get("${name}_CONNECT");
	$self->{'TNS_ADMIN'}=$self->ResolveConfigPath($config_ref->Get("${name}_WALLET"));
	
	return $self;
}

1;
