############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RASECoreBatch;
our @ISA = qw(ORPAction);

###############
# Sub:  PatchAction
# Desc: Put execute bits on new/updated shell scripts
###############
sub PatchAction {
	my $self=shift;
	
	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	my $dest_prefix=$self->GetDestPrefix();
	
	foreach my $dir ('data/infile','data/outfile','data/reject','data/log','scripts/log') {
		my $create_dir=&::ConcatPathString($dest_dir,$dest_prefix,$dir);
		if (!-d $create_dir) {
			&::CreateDirs('0755',$create_dir);
		}
	}
	
	#Setup i18n symlinks
	my $lang=$self->{'RASEBATCH_LANG_CODE'};
	$lang='en' if $lang eq '';

	if (&RASECoreBatch::SetupI18NLinks($dest_dir,$dest_prefix,$lang)!=1) {
		return 0;
	}

	return 1;
}				

sub GetDestPrefix {
	my $self=shift;
	return $self->{'DEST_PREFIX'};
}

#########################
# Sub:  SetupI18NLinks
# Desc: Create symlinks for seed_data files
# Arg:  lang - the language code of translated files to symlink
# Ret:  1 on success, 0 on failure
#########################
sub RASECoreBatch::SetupI18NLinks {
	my ($dest_dir,$dest_prefix,$lang)=@_;
	
	my $seed_data_prefix=&::ConcatPathString($dest_prefix,'data','seed_data');
	
	&::LogMsg("Creating I18N symlinks for $seed_data_prefix");
	
	my $full_path=&::ConcatPathString($dest_dir,$seed_data_prefix);
	
	my @en_files=();
	my @lang_files=();
	if (&::GetFilesInDir(&::ConcatPathString($full_path,'i18n','en'),\@en_files,0)!=1) {
		return 0;
	}

	if ($lang ne 'en') {
		my $lang_dir=&::ConcatPathString($full_path,'i18n',$lang);
		if (-d $lang_dir) {
			if (&::GetFilesInDir($lang_dir,\@lang_files,0)!=1) {
				return 0;
			}
		}
	}
	
	#Create all the I18N links
	if (&RASECoreBatch::CreateI18NLinks($full_path,\@lang_files,1)!=1) {
		return 0;
	}
	
	#Fill in any en links
	if (&RASECoreBatch::CreateI18NLinks($full_path,\@en_files,0)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  CreateI18NLinks
# Desc: Create symlinks for a list of source files
# Arg:  base_dir - path to the directory to create the symlinks in
#       file_list_ref - ref to a list of fully qualified source files
#       reset_existing - 1 to reset existing links, 0 to not
# Ret:  1 on success, 0 on failure
#########################
sub RASECoreBatch::CreateI18NLinks {
	my ($base_dir,$file_list_ref,$reset_existing)=@_;
	
	foreach my $file (@$file_list_ref) {
		my ($i18n_dir,$base_file)=&::GetFileParts($file,2);
		my $link_dest=&::ConcatPathString($base_dir,$base_file);
		
		if (-l $link_dest) {
			if ($reset_existing==1) {
				unless(unlink($link_dest)) {
					&::LogMsg("Unable to reset existing symlink $link_dest: $!");
					return 0;
				}
			}
		}
		next if (-e $link_dest);
		unless(symlink($file,$link_dest)) {
			&::LogMsg("Unable to create symlink from $file to $link_dest: $!");
			return 0;
		}
	}
	return 1;
}

#########################
# Sub:  FinalizeConfiguration
# Desc: Finalize our configuration, specifically the file copying for export files
# Arg:  None
#########################
sub FinalizeConfiguration {
	my $self=shift;

	#No need to finalize configuration if we are not enabled on this host
	if ($self->IsValidOnThisHost()!=1) {
		return 1;
	}
	
	my $global_state=$self->GetORPatchGlobalState();
	
	my $export_fcp=$self->{'EXPORT_FCP'};
	
	my %strip_hash=();
	foreach my $type (@{$export_fcp->GetFileTypes()}) {
		$strip_hash{$type}=$export_fcp->GetStripDirs($type);
	}
	
	#Now find all the SIABaseBatch objects that are valid on this host
	foreach my $action (@{$global_state->GetActionList()}) {
		if ($action->isa('ORPAction::SIABaseBatch')) {
			if ($action->IsValidOnThisHost()==1) {
				my @export_types=$action->GetCommonExportTypes();
				if (scalar(@export_types)!=0) {
					foreach my $et_ref (@export_types) {
						my ($type,$strip_level)=@$et_ref;
						$strip_hash{$type}=0-$strip_level;
						$export_fcp->AddFileTypes($type);
					}
				}
			}
		}
	}
	
	#Set the strip dirs level for each type
	$export_fcp->SetStripDirs(\%strip_hash);
	
	#Finally update the Subst FileCopy Method with our substitution property file
	my $dest_dir=$global_state->GetDestDir();
	my $property_file=&::ConcatPathString($dest_dir,'orpatch','config','rase_config.properties');
	
	my $subst_method=$self->{'SUBST_FILE_COPY'};
	$subst_method->SetSubstPropertyFile($property_file);
	#Prepare the substitution file copy method immediately so that we know if the property file is bad right away
	if ($subst_method->PrepareForCopies()!=1) {
		return 0;
	}
	
	return 1;
}


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;
	
	my $rse_home='rse-home';
	my $dest_prefix="$rse_home/common";
	my $type_prefix="rasecore";
	
	my $subst_method=new FileCopyMethodSubst([qr/rse\.env$/,qr/META-INF\/persistence.xml/],{},'%{','}%');
	
	my @file_copy_params=();
	
	#Copy core_db/db/seed_data -> common/data/seed_data and core_db/db/controlfiles -> common/data/controlfiles
	my $seed_fcp=new FileCopyParams(["${type_prefix}_batch_ctlfiles","${type_prefix}_batch_seedfiles"],"$dest_prefix/data",-2);
	push(@file_copy_params,$seed_fcp);
	
	#Copy core_db/scripts/lib -> common/scripts/lib, core_db/scripts/bin -> common/scripts/bin and core_db/scripts/batch -> common/scripts/batch
    my $scripts_fcp=new FileCopyParams(["${type_prefix}_batch_script_bin","${type_prefix}_batch_script_lib","${type_prefix}_batch_script_batch"],"$dest_prefix/scripts",-2);
	#Set 755 permissions on bin files
	$scripts_fcp->SetFilePerms({"${type_prefix}_batch_script_bin"=>'0755'});
	#Substitute values in the rse.env file
	$scripts_fcp->SetFileCopyMethod({"${type_prefix}_batch_script_lib"=>$subst_method});
	push(@file_copy_params,$scripts_fcp);
    
	#Copy common/JobProcessor/RseCoherenceSrv/* -> common/RseCoherenceServer
	my $rcs_fcp=new FileCopyParams(["${type_prefix}_rcs_cfg","${type_prefix}_rcs_meta_cfg","${type_prefix}_rcs_scripts"],"$dest_prefix/RseCoherenceServer",-3);
	#Set 755 permissions on bin files
	$rcs_fcp->SetFilePerms({"${type_prefix}_rcs_scripts"=>'0755'});
	#Substitute values in the persistence.xml file
	$rcs_fcp->SetFileCopyMethod({"${type_prefix}_rcs_meta_cfg"=>$subst_method});
	push(@file_copy_params,$rcs_fcp);
	
	#Copy common/ThirdPartyLibs/xxxx/* and common/dist/artifacts -> common/export
	#The full list of types and update criteria is finalized during SetORPatchGlobalState
	my $export_fcp=new FileCopyParams(["${type_prefix}_batch_export","${type_prefix}_batch_application"],"$dest_prefix/export",1);
	$export_fcp->SetStripDirs({"${type_prefix}_batch_export"=>-3,"${type_prefix}_batch_application"=>-4});
	push(@file_copy_params,$export_fcp);
	
	my $dataset_fcp=new FileCopyParams(["${type_prefix}_dataset"],"$rse_home/dataset",0);
	push(@file_copy_params,$dataset_fcp);
	
	my $self=$class->SUPER::new('RASECOREBATCH',\@file_copy_params,$config_ref);
	bless($self,$class);
	
	$self->{'DEST_PREFIX'}=$dest_prefix;
	$self->{'MODULE'}='core';
	$self->{'EXPORT_FCP'}=$export_fcp;
	$self->{'SUBST_FILE_COPY'}=$subst_method;
	$self->{'RASEBATCH_LANG_CODE'}=$config_ref->Get('RASEBATCH_LANG_CODE');
	
    return $self;
}

1;
