############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RASECDTBatch;
our @ISA = qw(ORPAction::SIABaseBatch);


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $self=$class->SUPER::new($config_ref,'RASECDTBATCH','cdt');
	bless($self,$class);
	
	return $self;
}

1;
