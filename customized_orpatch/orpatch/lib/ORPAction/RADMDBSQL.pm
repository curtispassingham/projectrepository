############################################################################
# Copyright � 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RADMDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#If this is a full check, register our need for a working DBSQL_RAFEDM configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_RAFEDM');
	}
	
		#If this is a full check, register our need for a working DBSQL_RABATCH configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_RABATCH');
	}
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name)=@_;
	
	#Compile Invalid Objects
	if ($self->CompileInvalids()!=1) {
		return 0;
	}

	return 1;
}

#########################
# Sub:  LoadRABatchGrant
# Desc: Load Grants from RADM to RABATCH user 
#       This is a custom function because we always run the grant script(s) even on a hotfix
#       to unrelated files, so runtype 2 won't work
#########################
sub LoadRABatchGrant {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	if (scalar(@$orig_f_dest_info)==0) {
		return 1;
	}
	
	
	#Find the RA BATCH owning schema
	my $rabatch_user=$self->GetCrossActionSchemaName('DBSQL_RABATCH');
	return 0 if $rabatch_user eq '';
	
	my @filter=(
	);
	
	my $args="$rabatch_user";
	
	return $self->RunDestFileListWithArgs($full_dest,$args,$filtered_dest_info,\@filter);
		
}

#########################
# Sub:  LoadRAGrants
# Desc: Grant permissions to ra_analyst and ra_batch and creates synonym to RA Batch
#       This is a custom function because we always run the grant script(s) even on a hotfix
#       to unrelated files, so runtype 2 won't work
#########################
sub LoadRAGrants {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	if (scalar(@$orig_f_dest_info)==0) {
		return 1;
	}
	
	#Run the original unfiltered list, always
	return $self->RunDestFileList($full_dest,$orig_f_dest_info,1);
}

#########################
# Sub:  CreateRABatchSynonym
# Desc: Create RA BATCH Synonym RADM to RABATCH user 
#       This is a custom function because we always run the grant script(s) even on a hotfix
#       to unrelated files, so runtype 2 won't work
#########################
sub CreateRABatchSynonym {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	if (scalar(@$orig_f_dest_info)==0) {
		return 1;
	}
	
	
	#Find the RA BATCH owning schema
	my $rabatch_user=$self->GetCrossActionSchemaName('DBSQL_RABATCH');
	return 0 if $rabatch_user eq '';
	
	my @filter=(
	);
	
	my $args="$rabatch_user";
	
	return $self->RunDestFileListWithArgs($full_dest,$args,$filtered_dest_info,\@filter);
		
}

#########################
# Sub:  CreateRAFEDMSynonym
# Desc: Create RA BATCH Synonym RADM to REFEDM user 
#       This is a custom function because we always run the grant script(s) even on a hotfix
#       to unrelated files, so runtype 2 won't work
#########################
sub CreateRAFEDMSynonym {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	if (scalar(@$orig_f_dest_info)==0) {
		return 1;
	}
	
	
	#Find the RAFEDM owning schema
	my $refedm_user=$self->GetCrossActionSchemaName('DBSQL_RAFEDM');
	return 0 if $refedm_user eq '';
	
	my @filter=(
	);
	
	my $args="$refedm_user";
	
	return $self->RunDestFileListWithArgs($full_dest,$args,$filtered_dest_info,\@filter);
		
}

#########################
# Sub:  RunGrantsToRSE
# Desc: Run the grants to the RSE Core schema from RADM.
#       Note: This function is only called as part of the SIABaseDBSQL actions, it is not run during RADM patches
#########################
sub RunGrantsToRSE {
	my $self=shift;
	my ($full_dest,$method_ref,$filtered_dest_info)=@_;
	
	if (scalar(@$filtered_dest_info)==0) {
		return 1;
	}
	
	#RASE installs to call rungrantstorse without a PatchAction
	if ($self->CreateDBManifestTable()!=1) {
		&::LogMsg("Failed to create manifest table!");
		return 0;
	}
	
	return $self->RunDestFileListWithSubstitutions($full_dest,$filtered_dest_info,[],$method_ref);
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
		
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest,
    #	            4 - on fresh install only register with dbmanifest, otherwise treat as run type 1, 5 - on a non fresh install only register with dbmanifest, otherwise treat as run type 5
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   patch_product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	my @file_type_info=(
	['radm_db_ddl',					3,'',						1,'RA','PATCHACTION'],
	['radm_db_change_scripts',		1,'',						1,'RA','PATCHACTION'],
	['radm_db_control_scripts',		2,'',						1,'RA','PATCHACTION'],
	['radm_db_packages',			2,'',						1,'RA','PATCHACTION'],
	['radm_db_languages',			2,'',						1,'RA','PATCHACTION'],
	['radm_db_role_grants',		    2,'LoadRAGrants',			1,'RA','PATCHACTION'],
	['radm_db_rabatch_grant',		2,'LoadRABatchGrant',		1,'RA','PATCHACTION'],
	['radm_db_rabatch_syn',			2,'CreateRABatchSynonym',	1,'RA','PATCHACTION'],
	['radm_db_rafe_syn',			2,'CreateRAFEDMSynonym',	1,'RA','PATCHACTION']
	);
	
	my $self=$class->SUPER::new('DBSQL_RADM',\@file_type_info,$config_ref);
	
	
	return $self;
}

1;
