############################################################################
# Copyright � 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RARMSBatchDBSQL;
our @ISA = qw(ORPAction::DBSQL);



########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#If this is a full check, register our need for a working DBSQL_RADM configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_RMS');
		}
	
	return 1;
}


#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
#sub PostAction {
#	my $self=shift;
#	my ($source_dir,$source_manifest,$dest_dir,$patch_name)=@_;
    
	#Compile Invalid Objects
	#if ($self->CompileInvalids()!=1) {
	#	return 0;
	#}
	## To include Ignore clause to ignore only the expected objects.
#	return 0;
#}

#########################
# Sub:  LoadGrantsToRARMSBATCH
# Desc: Run the grant script as RMS user
#########################
sub LoadGrantsToRARMSBATCH {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
	
	my $action_name='DBSQL_RMS';
	
	#Find our username
	my $schema_name=$self->GetSchemaName();
	return 0 if $schema_name eq '';
	
	#Get a handle to the DBSQL_RMS action	
	my $global_state=$self->GetORPatchGlobalState();
	my $rarmsbatch_handle=$global_state->GetActionHandle($action_name);
	
	if (!defined($rarmsbatch_handle)) {
		&::LogMsg("Unable to get handle to $action_name action");
		return 0;
	}

	return $rarmsbatch_handle->RunGrantsToRARMSBATCH($full_dest,$schema_name,$orig_f_dest_info);
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
		
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   patch_product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	my @file_type_info=(
	['rarmsbatch_db_rms_syn',			    2,'LoadGrantsToRARMSBATCH',	    1,'RA','PATCHACTION'],
	['rarmsbatch_db_rms_grant',			    2,'LoadGrantsToRARMSBATCH',	    1,'RA','PATCHACTION'],
	['rarmsbatch_db_ddl',					3,'',						    1,'RA','PATCHACTION'],
	['rarmsbatch_db_change_scripts',		1,'',						    1,'RA','PATCHACTION'],
	);
	
	my $self=$class->SUPER::new('DBSQL_RARMSBATCH',\@file_type_info,$config_ref);
	
	
	return $self;
}

1;
