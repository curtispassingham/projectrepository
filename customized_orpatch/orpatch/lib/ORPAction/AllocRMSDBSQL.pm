############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::AllocRMSDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#If this is a full check, register our need for a working DBSQL_ALLOC configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_ALLOC');
	}
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;

	#Compile Invalid Objects
	if ($self->CompileInvalidsInSharedSchema('DBSQL_RMS')!=1) {
		return 0;
	}
	
	if ($self->CreateUserSynonyms('RMS')!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadAllocRMSInstallScripts
# Desc: a custom function to load AllocRMS install scripts
#########################
sub LoadAllocRMSInstallScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	#Find the ALLOC user schema
	my $alloc_owner=$self->GetCrossActionSchemaName('DBSQL_ALLOC');
	return 0 if $alloc_owner eq '';
	
	my @filter=(
	);
	
	my $args="$alloc_owner";
	
	return $self->RunDestFileListWithArgs($full_dest,$args,$filtered_dest_info,\@filter);
}

#########################
# Sub:  GetCheckLockedObjectsSQL
# Desc: Override the default check locked object SQL statement so that we only look for Alloc objects if
#       only Alloc objects are being migrated
# Ret:  SQL statement to use when checking locked objects
#########################
sub GetCheckLockedObjectSQL {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my @product_list=$patch_info->GetProductList();
	
	my $SQL='';
	
	if (scalar(@product_list)==1 && $product_list[0] eq 'ALLOC') {
		#If there is only one product and it is ALLOC, only look for locked object names that belong to ALLOC
		$SQL=<<ENDSQL;
alter session set "_optimizer_cartesian_enabled"=false;
SELECT a.session_id||'|'||a.oracle_username||'|'||count(1)
FROM V\$LOCKED_OBJECT A,user_objects b
WHERE A.OBJECT_ID = B.OBJECT_ID
and ((b.object_name like 'ALC_\%' or b.object_name like 'RAF_\%' or b.object_name like 'RTC_\%' or b.object_name like 'PSA_\%'
      or b.object_name like 'V_ALC_\%')
     or
	 (b.object_name in ('DATE_INTERVAL_FORECAST_TBL','OBJ_DETAILS_UI_FORECAST_REC','OBJ_DETAILS_UI_FORECAST_TBL','OBJ_ITEMS_FORECAST_TBL',
	                    'OBJ_WEEK_SALES_FORECAST_TBL','OBJ_WEEK_SALES_FORECAST_REC','DATE_INTERVAL_TBL','OBJ_DETAILS_STORE_UI_TBL',
						'OBJ_DETAILS_STORE_UI_REC','OBJ_ITEMS_TBL','OBJ_WEEK_SALES_TBL','OBJ_WEEK_SALES_REC','OBJ_ALLOC_SCHEDULE_INFO_TBL',
						'OBJ_ALLOC_SCHEDULE_INFO_REC','OBJ_ALLOC_ITEM_PRICING_TBL','OBJ_ALLOC_ITEM_PRICING_REC','DISABLED_FACET_ATTRIBUTES_TBL',
						'DISABLED_FACET_ATTRIBUTE_REC','STRAGG_TYPE'))
	)
group by a.session_id,a.oracle_username
ENDSQL
	} else {
		#If multiple products are involved, use the standard SQL
		$SQL=$self->SUPER::GetCheckLockedObjectSQL();
	}

	return $SQL;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='DBSQL_ALCRMS';
	
	#Allow skipping all DBC scripts when DBSQL_ALCRMS_RUN_DBC=N
	my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);
		
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	my @file_type_info=(
	['alcrms_db_ddl',				1,			'',1],
	['alcrms_db_types',				2,			'LoadTypes',1],
	['alcrms_db_change_scripts',	$dbc_mode,	'',1],
	['alcrms_db_packages',			2,			'',1],
	['alcrms_db_triggers',          2,          '',1],
	['alcrms_db_seed_scripts',		3,			'',1],
	['alcrms_db_control_scripts',	2,			'',1],
	['alcrms_db_languages',			2,			'',1],
	['alcrms_db_install_scripts',	2,			'LoadAllocRMSInstallScripts',1],
	);
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	bless($self,$class);
	
	$self->SetUniqueDBManifest(0);
	
	#We override the connect information to use RMS' info
	my $name='DBSQL_RMS';
	$self->{'CONNECT_STRING'}=$config_ref->Get("${name}_CONNECT");
	$self->{'TNS_ADMIN'}=$self->ResolveConfigPath($config_ref->Get("${name}_WALLET"));
	
	return $self;
}

1;
