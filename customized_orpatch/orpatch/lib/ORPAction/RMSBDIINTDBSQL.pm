############################################################################
# Copyright © 2016, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RMSBDIINTDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}

	#If this is a full check, register our need for a working DBSQL_RMS configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_RMS');
	}

	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;

        #Run any SQL files associated with this step
        if ($self->SUPER::PostAction(@_)!=1) {
                        return 0;
        }

	#Compile Invalid Objects
	if ($self->CompileInvalids()!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  CreateBDISynonymsInRMS
# Desc: Create the synonyms from RMS schema to the BDI Integration schema objects
#       required for RMS BDI packages to compile
#########################
sub CreateBDISynonymsInRMS {
        my $self=shift;
        my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;

        my $action_name='DBSQL_RMS';

        #Find our username
        my $schema_name=$self->GetSchemaName();
        return 0 if $schema_name eq '';

        #Get a handle to the DBSQL_RMS action
        my $global_state=$self->GetORPatchGlobalState();
        my $rms_handle=$global_state->GetActionHandle($action_name);

        if (!defined($rms_handle)) {
                &::LogMsg("Unable to get handle to $action_name action");
                return 0;
        }

        return $rms_handle->CreateSynonymsToBDISchema();
}

########################
# Sub:  CreateBDIGrantsForRMS
# Desc: Create the required grants to the BDI Integration schema objects for the RMS schema
########################
sub CreateBDIGrantsForRMS {
        my $self=shift;
        my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;

        #Find the RMS schema owner
        my $rms_owner=$self->GetCrossActionSchemaName('DBSQL_RMS');
        return 0 if $rms_owner eq '';

        my $args="$rms_owner";
        if ($self->RunDestFileListWithArgs($full_dest,$args,$filtered_dest_info,[])!=1) {
                return 0;
        }

        return 1;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest, 
	#   6 - always run all files of this type as if it was a cumulative patch, even during an incremental patch  
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   product_name if a type should only be run with a patch for a particular product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	my @file_type_info=(
        ['rmsbdi_db_ddl_install',	3,'',				1,'RMS','PATCHACTION'],
        ['rmsbdi_db_ddl_rerun',		6,'',				1,'RMS','PATCHACTION'],
        ['rmsbdi_db_rtg_packages',	6,'',				1,'RMS','PATCHACTION'],
        ['rmsbdi_db_rms_synonyms',	6,'CreateBDISynonymsInRMS',	1,'RMS','PATCHACTION'],
        ['rmsbdi_db_grants', 	 	6,'CreateBDIGrantsForRMS',	1,'RMS','PATCHACTION'],
	);
	
	my $self=$class->SUPER::new('DBSQL_RMSBDIINT',\@file_type_info,$config_ref);
	$self->SetCheckLockedObjects(0);
	
        bless($self,$class);

	return $self;
}

1;
