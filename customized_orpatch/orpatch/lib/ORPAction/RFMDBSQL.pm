#############################################################################
# Copyright © 2014,2015 Oracle and/or its affiliates. All rights reserved.
#############################################################################
use strict;

package ORPAction::RFMDBSQL;
our @ISA = qw(ORPAction::DBSQL);

use Cwd;

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	
	my $name=$self->GetActionName();
	&::LogMsg("Pre-patch check for $name");

    # Check RFM_TAX_PROVIDER is set to TAXWEB or SYNCHRO, when RFM is enabled.	
	if ($self->{'TAX_PROVIDER'} eq 'INVALID') {
	    &::LogMsg("Invalid setting for RFM_TAX_PROVIDER.");		
		return 0;
		}	
	my $global_state=$self->GetORPatchGlobalState();

	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects and create any new synonyms for RMS users
#########################
sub PostAction {
	my $self=shift;
	
	#Compile Invalid Objects
	if ($self->CompileInvalidsInSharedSchema('DBSQL_RFM')!=1) {
		return 0;
	}
	
	#Create synonyms for users using our utilities/synonym_check.sql script
	#We do this as a postaction so that we only have to create synonyms once
	#after RMS/RPM/ReIM/Alloc have all had a chance to create their new objects
	if ($self->CreateUserSynonyms('RFM')!=1) {
		return 0;
	}
	
	return 1;
}


########################
# Sub:  CompileInvalidsOnce
# Desc: Override CompileInvalidsOnce so that we can do an alter system when compiling objects in a UTPLSQL env
# Ret:  1 if successful, 0 if not
########################
sub CompileInvalidsOnce {
	my $self=shift;
	my ($num)=@_;
	
	my @lines=('@@recompile_invalids.sql',"select 'COMPLETE - recompile_invalids' from dual");
	if ($self->{'UTPLSQL_ALTER_SESSION'} ne '') {
		unshift(@lines,$self->{'UTPLSQL_ALTER_SESSION'});
	}

	my $SQL=join("\n",@lines);
	
	&::LogMsg("Compiling $num invalid objects...");
	
	return $self->DoSQL('recompile_invalids.sql',$SQL,[],1);
}


#########################
# Sub:  LoadRFMInstallScripts
# Desc: a custom function to load the RFM Install Scripts
#########################
sub LoadRFMInstallScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
	
	my $lang_dir=&::ConcatPathString($full_dest,'Localization/languages');
	my $felm=&::ConcatPathString($full_dest,'Localization/control_scripts/form_menu_elements');
	my $cscripts=&::ConcatPathString($full_dest,'Localization/control_scripts/source');
	my $iscripts_dir=&::ConcatPathString($full_dest,'Localization/install_scripts');
	my $twscripts_dir=&::ConcatPathString($full_dest,'Localization/install_scripts/taxweb');
	my $syscripts_dir=&::ConcatPathString($full_dest,'Localization/install_scripts/synchro');
	local $ENV{SQLPATH}="$lang_dir:$iscripts_dir:$twscripts_dir:$syscripts_dir:$felm:$cscripts:$ENV{SQLPATH}";

	my @install_scripts=(
	'install.sql'
	);

	if ($self->{'TAX_PROVIDER'} eq 'TAXWEB') {
		push(@install_scripts,"rfm_install_taxweb.sql"); 
	}
	if ($self->{'TAX_PROVIDER'} eq 'SYNCHRO') {
		push(@install_scripts,"rfm_install_synchro.sql"); 
	}		
	my ($proceed,$found_scripts_ref,$missing_scrips_ref)=$self->FindFilteredFilesInOrder($filtered_dest_info,$orig_f_dest_info,@install_scripts);
	
	#If required files were missing, abort
	unless($proceed==1) {
		return 0;
	}

	if (scalar(@$found_scripts_ref)==0) {
		#No install scripts to run
		return 1;
	}
	
	#Run rfmseeddata from the install scripts directory
	return $self->RunDestFileListByBaseName($full_dest,$found_scripts_ref,[]);
}

#########################
# Sub:  LoadRFMLanguages
# Desc: Load the appropriate primary and secondary language scripts
#########################
sub LoadRFMLanguages {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;
	
	#Language scripts can take a long time to run, so we have an option to only run the ones that changed
	#note that we're not careful about whether a language changed only in the primary
	#and we have secondary installed or vice-versa.  If anything about the language changed
	#we rerun the appropriate type for that language based on what is installed
	
	my $only_upd_langs=$self->{'ONLY_UPDATED_LANGS'};
	
	#If this is a hotfix, then we always only run updated langs
	if ($self->{'PATCH_MODE'} eq 'HOTFIX') {
		$only_upd_langs=1;
	}
	
	#Build a list of the changed files
	my $f_dest_info;
	if ($only_upd_langs==1) {
		#If we are only running updated files, filter the complete dest manifest for things updated with this patch
		&::LogMsg("Filtering language scripts for only those updated in this patch");
		$f_dest_info=&Manifest::FilterDestManifest($orig_f_dest_info,'',$patch_name,$subpatch_ref);
	} else {
		#Otherwise pretend like all files were updated with this patch
		$f_dest_info=$orig_f_dest_info;
	}
	
	#Go through the changed files and determine what languages they apply to
	my %updated_langs=();
	foreach my $ref (@$f_dest_info) {
		my ($dfile,@discard)=&Manifest::DecodeDestLine($ref);
		
		#There various types of file names but they always end in _<lang code>.sql
		if ($dfile=~/\.sql$/) {
			if ($dfile=~/_([a-zA-Z]+)\.sql$/) {
				my $lang=$1;
				$lang=~tr/A-Z/a-z/;
				$updated_langs{$lang}=1;
			} else {
				&::LogMsg("Unable to identify language for file: $dfile");
				return 0;
			}
		} else {
			#Ignore non sql files
		}
	}
	
	if (scalar(keys(%updated_langs))==0) {
		&::LogMsg("No language scripts were updated");
	}
	
	#Get the primary language
	my $prim_lang=$self->GetPrimaryLanguage();
	return 0 if ($prim_lang eq '');
	
	my @lang_scripts=();
	
	#If the primary language is not English and the script was updated, run the primary script
	#English is always run during form elements scripts
	if ($prim_lang eq 'ptb') {
		if ($updated_langs{$prim_lang}==1) {
			if ($self->{'TAX_PROVIDER'} eq 'TAXWEB') {
				push(@lang_scripts,"orfm_primary_taxweb_${prim_lang}.sql"); 
			}
			if ($self->{'TAX_PROVIDER'} eq 'SYNCHRO') {
				push(@lang_scripts,"orfm_primary_synchro_${prim_lang}.sql"); 
			}				

		}
	}
	
	#Get any secondary languages that are installed
	my ($status,@sec_langs)=$self->GetSecondaryLanguages();
	return 0 if ($status!=1);
	#RFM runs only the PTB secondary scripts, if ptb is not a primary language.
    if ($prim_lang ne 'ptb') {	
	foreach my $lang (@sec_langs) {
		if ($updated_langs{$lang}==1) {
		    if ($lang eq 'ptb') {
				if ($self->{'TAX_PROVIDER'} eq 'TAXWEB') {
					push(@lang_scripts,"orfm_secondary_taxweb_${lang}.sql"); 
				}
				if ($self->{'TAX_PROVIDER'} eq 'SYNCHRO') {
					push(@lang_scripts,"orfm_secondary_synchro_${lang}.sql"); 
				}
            }
		}
	}
	}
	
	if (scalar(@lang_scripts)==0) {
		&::LogMsg("No installed languages were updated.");
		return 1;
	}

	#Find the language scripts we need to run, always looking in the unfiltered manifest
	my ($found_scripts_ref,$missing_scripts_ref)=$self->FindReqFilesInOrder($orig_f_dest_info,@lang_scripts);
		
	#It is possible that a customer has installed a custom language that we don't have scripts for
	#so don't abort if there are language scripts that we wanted to run but can't find.  Just log it.
	if (scalar(@$missing_scripts_ref)!=0) {
		foreach my $file (@$missing_scripts_ref) {
			&::LogMsg("Language Script $file not found, skipping");
		}
	}
	
	#Run the languages scripts from the language directory
	return $self->RunDestFileListByBaseName($full_dest,$found_scripts_ref,[]);
}


#########################
# Sub:  GetPrimaryLanguage
# Desc: Get the primary language in RFM
# Arg:  None
# Ret:  primary language iso code, or '' if errors
#########################
sub GetPrimaryLanguage {
	my $self=shift;
	
	&::LogMsg("Getting Primary Language");
	my $sql="select l.iso_code from system_options s, lang l where s.data_integration_lang = l.lang";
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to retrieve primary language!");
		&::DebugOutput("get primary language",@$output_ref);
		return '';
	}

	my $prim_lang=$$output_ref[0];
	chomp($prim_lang);
	$prim_lang=~tr/A-Z/a-z/;
	$prim_lang=~s/\s*$//g;
	return $prim_lang;
}

#########################
# Sub:  GetSecondaryLanguages
# Desc: Get the secondary languages in RFM
# Arg:  None
# Ret:  status (1 if success),array of secondary language iso codes
#########################
sub GetSecondaryLanguages {
	my $self=shift;
	
	&::LogMsg("Getting Secondary Languages");
	my $sql="select distinct l.iso_code from code_detail_tl f,lang l,system_options o where f.lang = l.lang and f.lang !=1 and l.lang != o.data_integration_lang";
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to retrieve secondary languages!");
		&::DebugOutput("get secondary languages",@$output_ref);
		return(0,'');
	}
	
	my @langs=();
	foreach my $line (@$output_ref) {
		chomp($line);
		$line=~tr/A-Z/a-z/;
		$line=~s/\s*$//g;
		push(@langs,$line);
	}
	
	return (1,@langs);
}

#########################
# Sub:  LoadDBUrlInvoker
# Desc: a custom function to load the UrlInvoker RFM DB Scripts
#########################
sub LoadDBUrlInvoker {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
	

	my $uiscripts_dir=&::ConcatPathString($full_dest,'Localization/rtil/db');
	local $ENV{SQLPATH}="$uiscripts_dir:$ENV{SQLPATH}";

	my @urlinvoker_scripts=(
	'PublishUrlInvoker.sql'
	);

	my ($proceed,$found_scripts_ref,$missing_scrips_ref)=$self->FindFilteredFilesInOrder($filtered_dest_info,$orig_f_dest_info,@urlinvoker_scripts);
	
	#If required files were missing, abort
	unless($proceed==1) {
		return 0;
	}

	if (scalar(@$found_scripts_ref)==0) {
		#No UrlInvoker scripts to run
		return 1;
	}
	
	#Run UrlInvoker from the rtil/db scripts directory
	return $self->RunDestFileListByBaseName($full_dest,$found_scripts_ref,[]);
}

#########################
# Sub:  GetCheckLockedObjectsSQL
# Desc: Override the default check locked object SQL statement so that we only look for RPM objects if
#       only RPM objects are being migrated
# Ret:  SQL statement to use when checking locked objects
#########################
sub GetCheckLockedObjectSQL {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my @product_list=$patch_info->GetProductList();
	
	my $SQL='';
	
	if (scalar(@product_list)==1 && $product_list[0] eq 'RPM') {
		#If there is only one product and it is RPM, only look for locked object names starting with RPM_
		$SQL=<<ENDSQL;
alter session set "_optimizer_cartesian_enabled"=false;
SELECT a.session_id||'|'||a.oracle_username||'|'||count(1)
FROM V\$LOCKED_OBJECT A,user_objects b
WHERE A.OBJECT_ID = B.OBJECT_ID
and (b.object_name like 'RPM_\%' or b.object_name like 'RSM_\%')
group by a.session_id,a.oracle_username
ENDSQL
	} else {
		#If multiple products are involved, use the standard SQL
		$SQL=$self->SUPER::GetCheckLockedObjectSQL();
	}

	return $SQL;
}


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='DBSQL_RFM';
	my $rfm_sy_dbcontrol_name='rfm_db_synchro_control_scripts';
	my $rfm_tw_dbcontrol_name='rfm_db_taxweb_control_scripts';
	my $rfm_sy_dbplsql_name='rfm_db_synchro_packages';
	my $rfm_tw_dbplsql_name='rfm_db_taxweb_packages';
	my $rfm_tw_urlinvoker_name='rfm_java_taxweb_urlinvoker';
	my $rfm_tw_dburlinvoker_name='rfm_db_taxweb_urlinvoker';
    my $rfm_ts_dpsql_name='';
	my $rfm_ts_dbcontrol_name='';	
	#Allow skipping all DBC scripts when DBSQL_RMS_RUN_DBC=N
	my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);

	# Validate RFM_TAX_PROVIDER
	my	$tax_provider=$config_ref->ValidateStringVar('RFM_TAX_PROVIDER','INVALID','SYNCHRO','TAXWEB');	
    # Checks TAXWEB or SYNCHRO enabled in the config and assigns the Control script and package of respective to the variables .ts->taxweb/synchro
	if ($tax_provider eq 'TAXWEB') {
		&::LogMsg("TAXWEB - Database Component  Enabled. ");
		$rfm_ts_dpsql_name=$rfm_tw_dbplsql_name;
		$rfm_ts_dbcontrol_name=$rfm_tw_dbcontrol_name;
	}	
	elsif ($tax_provider eq 'SYNCHRO') {
	    &::LogMsg("SYNCHRO - Database Component Enabled. ");
		$rfm_ts_dpsql_name=$rfm_sy_dbplsql_name;
		$rfm_ts_dbcontrol_name=$rfm_sy_dbcontrol_name;
	}			

		
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,product_name,run_phase,del_func]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   product_name if a type should only be run with a patch for a particular product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	#   del_func - Optional function that should be run whenever a file of this type is deleted
	

	# rfm_ts_dbcontrol_name and rfm_ts_dpsql_name - identifies the file type of taxweb/synchro based on the one enabled in the config.
	my @file_type_info=(
	['rfm_db_ddl',					3,'',						1,'RFM','PATCHACTION',''],
	['rfm_db_types',				2,'LoadTypes',				1,'RFM','PATCHACTION',''],
	['rfm_db_change_scripts',		$dbc_mode,'',				1,'RFM','PATCHACTION',''],
	['rfm_db_packages',				2,'',						1,'RFM','PATCHACTION',''],	
	[$rfm_ts_dpsql_name,			2,'',						1,'RFM','PATCHACTION',''],	
	['rfm_db_triggers',				2,'',						1,'RFM','PATCHACTION',''],	
	['rfm_db_form_menu_elements',	2,'',						1,'RFM','PATCHACTION',''],
# Commenting out for 16.0.x SE.  RFM team has removed the install scripts that were referenced and are currently coming up with a solution 
# ['rfm_db_installscripts',		3,'LoadRFMInstallScripts',	1,'RFM','PATCHACTION',''], 
	['rfm_db_control_scripts',		2,'',						1,'RFM','PATCHACTION',''],	
	[$rfm_ts_dbcontrol_name,		2,'',						1,'RFM','PATCHACTION',''],		
	['rfm_db_languages',		    2,'LoadRFMLanguages',		1,'RFM','PATCHACTION',''],	
	);

	if ($tax_provider eq 'TAXWEB') {
		#Include the URLInvoker File types
		my @urlinvoker_type_info=(
	['rfm_java_taxweb_urlinvoker',		2,'LoadDBJarsSingly',		1,'RFM','PATCHACTION','DropDBJar'],
	['rfm_db_taxweb_urlinvoker',		2,'LoadDBUrlInvoker',		1,'RFM','PATCHACTION',''],		
		);
		push(@file_type_info,@urlinvoker_type_info);
	}	
my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	#Allow languages to be only run when updated
	my $only_updated_langs=0;
	if ($config_ref->ValidateTFVar("${action_name}_RUN_ONLY_UPDATED_LANGS",0)==1) {
		$only_updated_langs=1;
	}
	$self->{'ONLY_UPDATED_LANGS'}=$only_updated_langs;
	# Validate RRFM_TAX_PROVIDER 
	$self->{'TAX_PROVIDER'}=$config_ref->ValidateStringVar('RFM_TAX_PROVIDER','INVALID','SYNCHRO','TAXWEB');	
	$self->SetRequiredTablespaces('RETAIL_DATA','RETAIL_INDEX','LOB_DATA','ENCRYPTED_RETAIL_DATA','ENCRYPTED_RETAIL_INDEX');
	#We override the connect information to use RMS' info
	my $name='DBSQL_RMS';
	$self->{'CONNECT_STRING'}=$config_ref->Get("${name}_CONNECT");
	$self->{'TNS_ADMIN'}=$self->ResolveConfigPath($config_ref->Get("${name}_WALLET"));
	
	return $self;
}

1;
