############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RETLScripts;
our @ISA = qw(ORPAction);

sub PatchAction {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	foreach my $dir_ref (['RETLforAIP','retl/Cross_Pillar/retl_scripts/aip'],['RETLforRPAS','retl/Cross_Pillar/retl_scripts/rpas'],['RETLforReSA','retl/Cross_Pillar/retl_scripts/resa'],['RETLforALLOC','retl/Cross_Pillar/retl_scripts/alloc']) {
		my $link_dir="$dest_dir/$$dir_ref[0]";
		my $src_dir="$dest_dir/$$dir_ref[1]";
		
		if (-l $link_dir) {
			#Link already exists
		} else {
			if (-f $link_dir || -d $link_dir) {
				&::LogMsg("WARNING: RETL director $$dir_ref[0] exists but is not a link!?");
			} else {
				&::LogMsg("Creating $$dir_ref[0] link to $$dir_ref[1]");
				if (symlink($src_dir,$link_dir)) {
					#Success
				} else {
					&::LogMsg("ERROR: Failed to link $src_dir to $link_dir: $!");
				}
			}
		}

		&::CreateDirs('0755',"$link_dir/data","$link_dir/error","$link_dir/log");
	}
	
	return 1;
}

###############
# Sub:  PostAction
# Desc: Put execute bits on new/updated shell scripts
###############
sub PostAction {
	my $self=shift;
	
	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my $patch_info=$global_state->GetPatchInfo();
	my $patch_name=$patch_info->GetPatchName();
	my @patch_list=$patch_info->GetPatchList();
	
	&::LogMsg("Adding execute permissions to any new RETL scripts");
	
	foreach my $ref ($self->GetFileCopyParams()) {
		my ($ftype_ref,$dest_sub_dir)=$ref->GetBasicParams();
		my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_sub_dir);
	
		#Read the destination manifest
		my @dest_info=&Manifest::ReadManifest($dest_manifest);

		foreach my $type (@$ftype_ref) {
			#Filter dest manifest by type and updated in this patch
			my $all_f_dest_info=&Manifest::FilterDestManifest(\@dest_info,$type,$patch_name,\@patch_list);
			
			foreach my $dest_line (@$all_f_dest_info) {
				my ($dfile,@discard)=&Manifest::DecodeDestLine($dest_line);
				
				#If this is a shell script
				if ($dfile=~/(\.ksh|\.sh)$/) {
					my $full_file=&::ConcatPathString($full_dest,$dfile);
					if (-f $full_file) {
						chmod 0755,$full_file;
					}
				}
			}
		}
	}
	
	return 1;
}				


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;
	
	my $r_scripts='rms_retl_scripts';
	my $rc_scripts='rms_retl_scripts_config';
	
	my $fcp=new FileCopyParams([$r_scripts,$rc_scripts],'retl',1);
	$fcp->SetUpdateCriteria({$r_scripts=>'IFNEW',$rc_scripts=>'NEVER'});
	my @file_copy_params=($fcp);
	
	my $self=$class->SUPER::new('RMSRETLSCRIPTS',\@file_copy_params,$config_ref);
    return $self;
}

1;
