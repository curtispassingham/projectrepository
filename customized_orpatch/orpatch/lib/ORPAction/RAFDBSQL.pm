############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RAFDBSQL;
our @ISA = qw(ORPAction::DBSQL);

sub PreCheck {
	my $self=shift;
	my ($full_check)=@_;
	
	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#Determine who our parent schema is
	foreach my $parent_action ('DBSQL_RMS','DBSQL_RAFTEST') {
		my $global_state=$self->GetORPatchGlobalState();
		my $rms_handle=$global_state->GetActionHandle($parent_action);
		if (!defined($rms_handle)) {
			&::LogMsg("Unable to get handle to $parent_action action");
			return 0;
		}
		if ($rms_handle->IsValidOnThisHost()==1) {
			$self->{'PARENT_ACTION'}=$parent_action;
			last;
		}
	}
	
	if ($self->{'PARENT_ACTION'} eq '') {
		&::LogMsg("Unable to determine parent schema for RAF tables");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;

	my $parent=$self->{'PARENT_ACTION'};
	if ($self->CompileInvalidsInSharedSchema($parent)!=1) {
		return 0;
	}
	
	if ($parent eq 'DBSQL_RMS') {
		#Create synonyms for users if they haven't already been
		if ($self->CreateUserSynonyms('RMS')!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  GetCheckLockedObjectsSQL
# Desc: Override the default check locked object SQL statement so that we only look for RAF objects if
#       only RAF objects are being migrated
# Ret:  SQL statement to use when checking locked objects
#########################
sub GetCheckLockedObjectSQL {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my @product_list=$patch_info->GetProductList();
	
	my $SQL='';
	
	if (scalar(@product_list)==1 && $product_list[0] eq 'RAF') {
		#If there is only one product and it is RAF, only look for locked object names that belong to RAF
		$SQL=<<ENDSQL;
alter session set "_optimizer_cartesian_enabled"=false;
set escape \\
SELECT a.session_id||'|'||a.oracle_username||'|'||count(1)
FROM V\$LOCKED_OBJECT A,user_objects b
WHERE A.OBJECT_ID = B.OBJECT_ID
and (b.object_name like 'RAF\\_\%' or b.object_name like 'RTC\\_\%')
group by a.session_id,a.oracle_username
ENDSQL
	} else {
		#If multiple products are involved, use the standard SQL
		$SQL=$self->SUPER::GetCheckLockedObjectSQL();
	}

	return $SQL;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='DBSQL_RAF';
	
	#Allow skipping all DBC scripts when DBSQL_RAF_RUN_DBC=N
	my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);
	
	#There is no separate RAF product yet, so we don't set this
	my $product_name='';
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   patch_product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	my @file_type_info=(
	['raf_db_ddl',				1,'',				1,$product_name,'PATCHACTION'],
	['raf_db_types',			2,'LoadTypes',		1,$product_name,'PATCHACTION'],
	['raf_db_change_scripts',	$dbc_mode,'',		1,$product_name,'PATCHACTION'],
	['raf_db_packages',			2,'',				1,$product_name,'PATCHACTION'],
	['raf_db_triggers',			2,'',				1,$product_name,'PATCHACTION'],
	['raf_db_install_scripts',	3,'',				1,$product_name,'PATCHACTION'],
	['raf_db_control_scripts',	2,'',				1,$product_name,'PATCHACTION'],
	);
	
	my $self=$class->SUPER::new('DBSQL_RAF',\@file_type_info,$config_ref);
	bless($self,$class);
	
	$self->SetUniqueDBManifest(0);
	
	return $self;
}

1;
