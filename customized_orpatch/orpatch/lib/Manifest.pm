############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;
package Manifest;
use File::Copy;

$Manifest::START_OF_FILE_HASH_KEY='START-OF-FILE';
$Manifest::END_OF_FILE_HASH_KEY='END-OF-FILE';

############################
# Sub:  UpdateDestInfo
# Desc: Update a row that is destined for the destination manifest
# Args: source file name,file type, file revision, destination file,patch name,customized,forward port revision,has_jar_manifest,future2,future3,future4,ref to the dest info array
# Ret:  a ref to the updated row
# Note: This has dependencies on the ordering of columns in the dest manifest
############################
sub Manifest::UpdateDestInfo {
   my ($fname,$ftype,$frevision,$dfname,$patch_name,$customized,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4,$di_ref)=@_;

   my $return_ref='';
   my $found=0;
   #Look through the manifest and update the row if we overwrote a file
   for(my $i=0;$i<scalar(@{$di_ref});$i++) {
      #Destination Manifest has
      #dest file name,source file name,source file type, source revision
      my $aref=$$di_ref[$i];

      if ($dfname eq $$aref[0]) {
          #Found the row
		  #&::LogMsg("Updating row for $fname to $ftype and $frevision");
          $found=1;
		  $$aref[1]=$fname;  #in case the source file changes?
          $$aref[2]=$ftype;  #just in case a file type changes?
          $$aref[3]=$frevision;
		  $$aref[4]=$patch_name;
		  $$aref[5]=$customized;
		  $$aref[6]=$fwp_rev;
		  $$aref[7]=$has_jar_manifest;
		  $$aref[8]=$future2;
		  $$aref[9]=$future3;
		  $$aref[10]=$future4;
		  $return_ref=$aref;
          last;
      }
   }

   if ($found==0) {
      #New File, append to end
	  my $ref=&Manifest::AssembleDestLine($dfname,$fname,$ftype,$frevision,$patch_name,$customized,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4);
      push(@{$di_ref},$ref);
	  $return_ref=$ref;
   }
   return $return_ref;
}

############################
# Sub:  UpdateDestWithLimitedInfo
# Desc: Update a row that is destined for the destination manifest, but only overwrite columns with non '' entries and only file type if there is not current file type
# Args: source file name,file type, file revision, destination file,patch name,customized,forward port revision,has_jar_manifest,future2,future3,future4,ref to the dest info array
#
# Note: This has dependencies on the ordering of columns in the dest manifest
############################
sub Manifest::UpdateDestWithLimitedInfo {
	my ($fname,$ftype,$frevision,$dfname,$patch_name,$customized,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4,$di_ref)=@_;

	my $found=0;
	#Look through the manifest and update the row if we overwrote a file
	for(my $i=0;$i<scalar(@{$di_ref});$i++) {
		#Destination Manifest has
		#dest file name,source file name,source file type, source revision
		my $aref=$$di_ref[$i];

		if ($dfname eq $$aref[0]) {
			#Found the row
			$found=1;
			
			$$aref[2]=$ftype if $$aref[2] eq '';
			
			#Update all the columns after dest name,source name,file type if they are not ''
			my $index=3;
			foreach my $value ($frevision,$patch_name,$customized,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4) {
				if ($value ne '') {
					$$aref[$index]=$value;
				}
				$index++;
			}
			last;
		}
	}

   if ($found==0) {
		#New File, append to end
		my $ref=&Manifest::AssembleDestLine($dfname,$fname,$ftype,$frevision,$patch_name,$customized,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4);
		push(@{$di_ref},$ref);
	}
}

############################
# Sub:  ReadManifest
# Desc: Read a manifest (csv) file
# Args: Full path to the manifest file
# Ret:  Array of manifest info, each entry a ref to an array of the line split on commas
############################
sub Manifest::ReadManifest {
	my ($manifest)=@_;

	my @info=();

	#it is OK for the destination manifest to not exist yet
	if (-f $manifest) {
		open(MANIFEST,"<$manifest") || die "Unable to open $manifest: $!";
		while(<MANIFEST>) {
			chomp;
			s/\r$//;
			next if (/^#/);
			my @parts=split(/\,/,$_,-1);
			push(@info,\@parts);
		}
		close(MANIFEST);
	}

	return @info;
}

############################
# Sub:  AssembleDestLine
# Desc: Return a ref to a row that could be written to the destination manifest
# Args: 
#  destination file relative to placement of envmanifest.csv
#  source file path
#  file type
#  file revision
#  description of patch the file came from
#  customized flag
#  forward port revision
#  Has jar manifest
#  future2
#  future3
#  future4
# Returns: ref to the row
# Note: This has dependencies on the ordering of columns in the dest manifest
############################
sub Manifest::AssembleDestLine {
	my ($dest_rel_file,$src_file,$file_type,$revision,$patch_name,$customized,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4)=@_;
	
	return [$dest_rel_file,$src_file,$file_type,$revision,$patch_name,$customized,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4];
}

############################
# Sub:  DecodeDestLine
# Desc: Return the elements in a manifest array in a specific order
# Args: dest array ref
# Returns: 
#  destination file relative to placement of envmanifest.csv
#  source file path
#  file type
#  file revision
#  description of patch the file came from
#  customized flag
#  forward port revision
#  has_jar_manifest
#  future2
#  future3
#  future4
# Note: This has dependencies on the ordering of columns in the dest manifest
############################
sub Manifest::DecodeDestLine {
	my ($ref)=@_;
	
	my $dfile=$$ref[0];
	my $sfile=$$ref[1];
	my $ftype=$$ref[2];
	my $frevision=$$ref[3];
	my $fdesc=$$ref[4];
	my $custom=$$ref[5];
	my $fwp_rev=$$ref[6];
	my $has_jar_manifest=$$ref[7];
	my $future2=$$ref[8];
	my $future3=$$ref[9];
	my $future4=$$ref[10];
	
	return ($dfile,$sfile,$ftype,$frevision,$fdesc,$custom,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4);
}

############################
# Sub:  DecodeDeletedDestLine
# Desc: Return the elements in a deleted env manifest array in a specific order
# Args: deleted dest array ref
# Returns: 
#  items from DecodeDestLine in same order
#  patch that removed the file
# Note: This has dependencies on the ordering of columns in the dest manifest
############################
sub Manifest::DecodeDeletedDestLine {
	my ($ref)=@_;
	
	my @info=&Manifest::DecodeDestLine($ref);
	my $remove_patch=$$ref[11];
	if ($remove_patch eq '' && scalar(@{$ref})==7) {
		#An old length deleteddestline
		$remove_patch=$$ref[6];
	}

	return (@info,$remove_patch);
}

############################
# Sub:  DecodeSourceLine
# Desc: Return the elements in a manifest array in a specific order
# Args: source array ref
# Returns: 
#  source file path
#  file revision
#  file type
#  patch_name
#  unit_test
#  localization
#  customized flag
#  forward port revision
#  future1
#  future2
#  future3
#  future4
# Note: This has dependencies on the ordering of columns in the dest manifest
############################
sub Manifest::DecodeSourceLine {
	my ($ref)=@_;
	
	my $sfile=$$ref[0];
	my $frevision=$$ref[1];
	my $ftype=$$ref[2];
	my $unit_test=$$ref[3];
	my $localization=$$ref[4];
	my $patch_name=$$ref[5];
	my $custom=$$ref[6];
	my $fwp_rev=$$ref[7];
	my $future1=$$ref[8];
	my $future2=$$ref[9];
	my $future3=$$ref[10];
	my $future4=$$ref[11];
	
	return ($sfile,$frevision,$ftype,$patch_name,$unit_test,$localization,$custom,$fwp_rev,$future1,$future2,$future3,$future4);
}

sub Manifest::RemoveUnknownBlocks {
	my ($si_ref,$di_ref)=@_;

	#We create a map of source files to lookup whether a file is 'unknown'
	my $so_ref=&Manifest::CreateSourceOrderMap($si_ref);
	my %source_order=%{$so_ref};
   
	#Our hash of 'unknown blocks'
	my %unknown_blocks=();
	#Our new dest_info array
	my @new_dest_info=();

	#Loop our dest info looking for unknown files
	my $in_unknown=0;
	my @u_block=();
	foreach my $ref (@{$di_ref}) {
		my ($dfile,$sfile,@junk)=&Manifest::DecodeDestLine($ref);
		#Check this source name against our source ordering
		if (exists($source_order{$sfile})) {
			#Known file
			push(@new_dest_info,$ref);
			if ($in_unknown==1) {
				#&::LogMsg("Recording block of unknown files for insert before $dfile");
				#We were in an unknown block, save it as 'before' this file
				$unknown_blocks{$dfile}=[@u_block];
				$in_unknown=0;
				@u_block=();
			}
		} else {
			#Unknown file
			#&::LogMsg("Saving unknown file in destination manifest: $dfile");

			#Mark that we are in an unknown block if we weren't already
			$in_unknown=1 if $in_unknown==0;

			#Append to the unknown block accumulator
			push(@u_block,$ref);
		}
	}
				
	#Handle an unknown block at the end of the file
	
	if ($in_unknown==1) {
		#&::LogMsg("Saving current unknown block to end-of-file");
		$unknown_blocks{$Manifest::END_OF_FILE_HASH_KEY}=[@u_block];
	}
	
	if (scalar(@new_dest_info)==0) {
		#If we had only a manifest with unknown blocks (i.e. the source manifest contains all new files)
		#ensure that the new files end up at the end of the manifest, not the start
		if (keys(%unknown_blocks)==1 && exists($unknown_blocks{$Manifest::END_OF_FILE_HASH_KEY})) {
			#Change the end of file block to a start-of-file block
			$unknown_blocks{$Manifest::START_OF_FILE_HASH_KEY}=$unknown_blocks{$Manifest::END_OF_FILE_HASH_KEY};
			delete($unknown_blocks{$Manifest::END_OF_FILE_HASH_KEY});
		}
	}
	
	return (\%unknown_blocks,\@new_dest_info); 
}

############################
# Sub:  UpdateSourceInfo
# Desc: Update a row that is destined for the source manifest
# Args: file name,file revision,file type,unittest, localization,patch name,customized,forward port revision,future1-4,ref to the source info array
#
# Note: This has dependencies on the ordering of columns in the source manifest
############################
sub Manifest::UpdateSourceInfo {
	my ($fname,$frevision,$ftype,$unit_test,$localization,$patch_name,$customized,$fwp_rev,$future1,$future2,$future3,$future4,$si_ref)=@_;

	my $found=0;
	#Look through the manifest and update the row if we overwrote a file
	for(my $i=0;$i<scalar(@{$si_ref});$i++) {
		#Source Manifest has
		#FILE,VERSION,TYPE,UNITTEST,LOCALIZATION,PATCHNAME,CUSTOMIZED

		my $aref=$$si_ref[$i];

		if ($fname eq $$aref[0]) {
			#Found the row
			#&::LogMsg("Updating row for $fname to $ftype and $frevision");
			$found=1;
			$$aref[1]=$frevision;
			$$aref[2]=$ftype;  #just in case a file type changes?
			$$aref[3]=$unit_test;
			$$aref[4]=$localization;
			$$aref[5]=$patch_name;
			$$aref[6]=$customized;
			$$aref[7]=$fwp_rev;
			$$aref[8]=$future1;
			$$aref[9]=$future2;
			$$aref[10]=$future3;
			$$aref[11]=$future4;
			last;
		}
	}

	if ($found==0) {
		#New File, append to end
		push(@{$si_ref},[$fname,$frevision,$ftype,$unit_test,$localization,$patch_name,$customized,$fwp_rev,$future1,$future2,$future3,$future4]);
	}
}

############################
# Sub:  FilterDestManifest
# Desc: Filter a destination manifest info array by file type, or patch_name
#       if both file type and patch name(s) are specified, a line must match BOTH to be kept
#       file type, patch_name and subpatch_ref are all optional, but if none are specified no filtering is done
# Args: dest info ref,file type,patch_name,ref to subpatch names
# Returns: 
#  ref to new filtered dest info array
############################
sub Manifest::FilterDestManifest {
	my ($dest_info,$file_type,$patch_name,$subpatch_ref)=@_;
	
	my @patch_names=();
	if ($patch_name ne '') {
		push(@patch_names,$patch_name);
	}
	
	if ($subpatch_ref ne '' && ref($subpatch_ref)) {
		push(@patch_names,@$subpatch_ref);
	}
	
	my $type_match=(($file_type eq '') ? 1 : 0);
	my $patch_match=((scalar(@patch_names)==0) ? 1 : 0);

	my @filtered_dest_info=();
	foreach my $ref (@$dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $t_matches=$type_match;
		my $p_matches=$patch_match;
		
		if ($file_type ne '') {
			if ($ftype eq $file_type) {
				$t_matches=1;
			}
		}
		
		if ($p_matches==0) {
			foreach my $pname (@patch_names) {
				if ($pname eq $fdesc) {
					$p_matches=1;
					last;
				}
			}
		}
		
		if ($t_matches==1 && $p_matches==1) {
			push(@filtered_dest_info,$ref);
		}
	}
	
	return \@filtered_dest_info;
}

############################
# Sub:  FilterDestManifestNonExistentFiles
# Desc: Filter a destination manifest info array to remove files which do not exist
# Args: full path to dest,dest info ref
# Returns: 
#  ref to new filtered dest info array
############################
sub Manifest::FilterDestManifestNonExistentFiles {
	my ($full_dest,$dest_info)=@_;

	my @filtered_dest_info=();
	foreach my $ref (@$dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $full_file=&::ConcatPathString($full_dest,$dfile);
	
		if (-f $full_file) {
			push(@filtered_dest_info,$ref);
		} else {
			#File does not exist!
			&::LogMsg("WARNING: Removing non-existent file $full_file from processing list!");
		}
	}
	
	return \@filtered_dest_info;
}

############################
# Sub:  BuildSourceManifestTypeHash
# Desc: Construct a hash to cross-ref source manifest lines to types
# Args: source info ref,type lookup hash ref
# Returns: distinct list of types in source manifest
############################
sub Manifest::BuildSourceManifestTypeHash {
	my ($source_info_ref,$type_lookup_href)=@_;
	
	my %distinct_types=();
	%$type_lookup_href=();
	
	foreach my $ref (@$source_info_ref) {
		my ($sfile,$frevision,$ftype,@discard)=&Manifest::DecodeSourceLine($ref);
		my $cross_ref=$type_lookup_href->{$ftype};
		if ($cross_ref eq '') {
			$cross_ref=[];
		}
		push(@$cross_ref,$ref);
		$type_lookup_href->{$ftype}=$cross_ref;
		$distinct_types{$ftype}=1;
	}
	
	return keys(%distinct_types);
}

############################
# Sub:  FilterSourceManifest
# Desc: Filter a source manifest info array by file type
# Args: ref to a type lookup hash created by BuildSourceManifestTypeHash,ref to list of file types to find, ref to list of ignore patterns
# Returns: 
#  ref to new filtered source info array
############################
sub Manifest::FilterSourceManifest {
	my ($type_lookup_href,$file_type_ref,$ignore_pat_ref)=@_;

	my @filtered_source_info=();
	foreach my $type (@$file_type_ref) {
		my $cross_ref=$type_lookup_href->{$type};
		
		#If there are lines for this type
		if ($cross_ref ne '') {
			foreach my $ref (@$cross_ref) {
				#Check if any need to be ignored
				my ($fname,@discard)=&Manifest::DecodeSourceLine($ref);
				
				if ($ignore_pat_ref ne '') {
					my $skip=0;
					foreach my $ir (@$ignore_pat_ref) {
						if ($fname=~$ir) {
							$skip=1;
							last;
						}
					}
					next if ($skip==1);
				}

				#A line that matches one of the types and does not match any ignore patterns
				push(@filtered_source_info,$ref);
			}
		}
	}
	
	return \@filtered_source_info;
}

############################
# Sub:  GetDistinctPatchesInDestManifestList
# Desc: Return the distinct patches from all dest manifests in a list
# Args: dest_manifest_ref - ref to a list of destination manifests
#       delete_dest_manifest_ref - ref to a list of deleted destination manifests
# Returns: 
#  proceed - 1 if successful, 0 if not
#  ref to the hash keyed by patch names
############################
sub Manifest::GetDistinctPatchesInDestManifestList {
	my ($dest_manifest_ref,$del_manifest_ref)=@_;

	&::LogMsg("Gathering list of patches still active in environment");
	
	my %patch_names=();
	
	#loop through both manifest lists, distinguishing between the dest manifest and the deleted dest manifests
	foreach my $info_ref ([$dest_manifest_ref,0],[$del_manifest_ref,1]) {
		my ($manifest_list,$del_manifest)=@$info_ref;
		
		#Loop through all manifests in this list
		foreach my $dest_manifest (@$manifest_list) {
			my @dest_info=&Manifest::ReadManifest($dest_manifest);
			
			#Go through the manifest contents and record the distinct patches that are responsible for lines in the file
			foreach my $ref (@dest_info) {
				my $this_patch='';
				if ($del_manifest==1) {
					my ($dfile,$sfile,$ftype,$frevision,$patch_name,$custom,$fwp_rev,$future1,$future2,$future3,$future4,$remove_patch)=&Manifest::DecodeDeletedDestLine($ref);
					$this_patch=$remove_patch;
				} else {
					my ($dfile,$sfile,$ftype,$frevision,$patch_name,$custom,@discard)=&Manifest::DecodeDestLine($ref);
					$this_patch=$patch_name;
				}
				$patch_names{$this_patch}++;
			}
		}
	}
	
	return (1,\%patch_names);
}

############################
# Sub:  DeleteDestInfo
# Desc: Remove row(s) from an in-memory destination manifest
# Args: ref to dest info, ref to list of dest relative file names to remove
# Ret:  ref to dest info to keep, ref to dest info of rows that were removed
# Note: This has dependencies on the ordering of columns in the dest manifest
############################
sub Manifest::DeleteDestInfo {
	my ($di_ref,$fn_ref)=@_;

	my @keep_info=();
	my @remove_info=();

	foreach my $ref (@$di_ref) {
		#Destination Manifest has
		#dest file name,source file name,source file type, source revision
		
		my $keep=1;
		foreach my $file_to_remove (@$fn_ref) {
			if ($file_to_remove eq $$ref[0]) {
				$keep=0;
				last;
			}
		}
		
		if ($keep==1) {
			push(@keep_info,$ref);
		} else {
			push(@remove_info,$ref);
		}
	}

	return (\@keep_info,\@remove_info);
}

############################
# Sub:  DeleteDeletedDestInfo
# Desc: Remove row(s) from an in-memory delete destination manifest
# Args: ref to dest info, ref to list of dest relative file names to remove
# Ret:  ref to dest info to keep, ref to dest info of rows that were removed
# Note: This has dependencies on the ordering of columns in the dest manifest
############################
sub Manifest::DeleteDeletedDestInfo {
	my ($di_ref,$fn_ref)=@_;
	
	#Destination manifests and deleted destination manifests have different formats
	#but the relative filename is in the same location so we can re-use DeleteDestInfo here
	return &Manifest::DeleteDestInfo($di_ref,$fn_ref);
}

############################
# Sub:  AppendDeletedDestManifest
# Desc: Append rows to a deleted destination manifest
# Args: path to deleted manifest, ref to removed rows, ref to map relative file names to the patch that removed them
# Ret:  1 if successful, 0 if not
#
# Note: This has dependencies on the ordering of columns in the dest delete manifest
############################
sub AppendDeletedDestManifest {
	my ($del_manifest,$ri_ref,$patch_lookup_ref)=@_;
	
	my @dest_info=&Manifest::ReadManifest($del_manifest);
	
	#Loop through all removed rows
	foreach my $rem_ref (@$ri_ref) {
		my ($rem_dfile,@discard1)=&Manifest::DecodeDestLine($rem_ref);
		
		my $patch_name=$patch_lookup_ref->{$rem_dfile};
		
		my $found=0;
		#Loop through the deleted manifest to see if this file has already been removed
		foreach my $di_ref (@dest_info) {
			my ($di_dfile,@discard2)=&Manifest::DecodeDestLine($di_ref);
			if ($rem_dfile eq $di_dfile) {
				#This row had already been removed previously, replace the information in the delete env manifest
				@$di_ref=@$rem_ref;
				#the remove patch always comes last as it does not exist as a column in the real dest manifest
				push(@$di_ref,$patch_name);
				$found=1;
				last;
			}
		}
		
		if ($found==0) {
			#Add our remove patch information
			push(@$rem_ref,$patch_name);
			#append to our destination delete manifest array
			push(@dest_info,$rem_ref);
		}
	}
	
	if (&Manifest::SaveDestManifest($del_manifest,[],\@dest_info,{},0)!=1) {
		#Failed to save/swap manifest
		return 0;
	}

	return 1;
}

############################
# Sub:  DeleteFromDeletedDestManifest
# Desc: Remove rows from a deleted destination manifest
# Args: path to deleted manifest, ref to relative file names to remove
# Ret:  1 if successful, 0 if not
#
# Note: This has dependencies on the ordering of columns in the dest delete manifest
############################
sub DeleteFromDeletedDestManifest {
	my ($del_manifest,$rfn_ref)=@_;
	
	my @dest_info=&Manifest::ReadManifest($del_manifest);
	
	my ($keep_ref,$remove_ref)=&Manifest::DeleteDeletedDestInfo(\@dest_info,$rfn_ref);
	
	if (&Manifest::SaveDestManifest($del_manifest,[],$keep_ref,{},0)!=1) {
		#Failed to save/swap manifest
		return 0;
	}

	return 1;
}

############################
# Sub:  SaveDestManifest
# Desc: Save our current destination manifest array out to a file
# Args: destination manifest name,source info ref, destination info ref,unknown block hash ref,dest manifest (1 if dest manifest, 0 if deleted dest manifest)
# Ret:  1 on success, 0 on failure
############################
sub Manifest::SaveDestManifest {
	my ($dst_mfst,$si_ref,$di_ref,$href,$is_dest_manifest)=@_;
	
	if (&Manifest::SaveInterimDestManifest($dst_mfst,$si_ref,$di_ref,$href,$is_dest_manifest)!=1) {
		#Failed writing the manifest
		return 0;
	}
	
	if (&Manifest::SwapInterimDestManifest($dst_mfst)!=1) {
		#Failed to swap manifest
		return 0;
	}

	return 1;
}

############################
# Sub:  WriteSourceManifest
# Desc: Get the header that should be used for a source manifest
# Args: none
# Ret:  the header string
############################
sub Manifest::WriteSourceManifest {
	my ($manifest_csv,$msi_ref)=@_;
	
	#Get the standard length of a source manifest line
	my @info=&DecodeSourceLine([]);
	my $length=$#info;
	
	unless(open(MANIFEST,">$manifest_csv")) {
		&::LogMsg("Unable to open $manifest_csv: $!");
		return 0;
	}
	print MANIFEST "#FILE,VERSION,TYPE,UNITTEST,LOCALIZATION,PATCHNAME,CUSTOMIZED,FWPVERSION,FUTURE1,FUTURE2,FUTURE3,FUTURE4\n";
	foreach my $ref (@$msi_ref) {
		#Make sure all rows are a minimum length
		if ($#{$ref}<$length) {
			$#{$ref}=$length;
		}
		print MANIFEST join(',',@$ref)."\n";
	}
	close(MANIFEST);
	return 1;
}

########################################################################################
# Helper functions

sub Manifest::GetInterimName {
	my ($real_manifest)=@_;
	
	return $real_manifest.".tmp";
}

############################
# Sub:  SaveInterimDestManifest
# Desc: Save our current destination manifest array out to a file
# Args: destination manifest name,source info ref, destination info ref,unknown block hash ref,is_dest_manifest
# Ret:  1 on success, 0 on failure
# Note: This has dependecies on the ordering of columns in the dest manifest
############################
sub Manifest::SaveInterimDestManifest {
	my ($dst_mfst,$si_ref,$di_ref,$href,$is_dest_manifest)=@_;
	
	my $interim_file=&Manifest::GetInterimName($dst_mfst);
	
	my @sample_info=();
	if ($is_dest_manifest==1) {
		@sample_info=&Manifest::DecodeDestLine([]);
	} else {
		@sample_info=&Manifest::DecodeDeletedDestLine([]);
	}
	my $line_length=$#sample_info;
	
	my @sorted_dest=();
	if (scalar(@$si_ref)!=0) {
		#create a map for filenames to their array position in preparation for sorting the dest array
		#We need the map to avoid iterating the source array for each file in dest info
		#Note that in the source info array, the filename is in column 1 (index 0)
		my $so_ref=&Manifest::CreateSourceOrderMap($si_ref);
		my %source_order=%{$so_ref};
	
		#Now sort the destination info
		#Note that in the dest info, the source filename is in column 2 (index 1)
		#We look up the source names in the source_ordering and then <=> to sort them numerically
		@sorted_dest=sort {$source_order{$$a[1]} <=> $source_order{$$b[1]}} @{$di_ref};
	} else {
		@sorted_dest=@{$di_ref};
	}
	
	#This just makes the syntax in the while look easier to read
	my %unknown_blocks=%{$href};

	unless(open(MANIFEST,">$interim_file")) {
		&::LogMsg("Unable to open $interim_file for writing: $!");
		return 0;
	}
	
	#Print any lines that are at the start of the file
	if (exists($unknown_blocks{$Manifest::START_OF_FILE_HASH_KEY})) {
		my $aref=$unknown_blocks{$Manifest::START_OF_FILE_HASH_KEY};
		foreach my $ukaref (@$aref) {
			#Make sure each line is a minimum length
			if ($#{$ukaref}<$line_length) {
				$#{$ukaref}=$line_length;
			}
			unless(print MANIFEST join(',',@$ukaref)."\n") {
				&::LogMsg("Failed writing to $interim_file: $!");
				return 0;
			}
		}
	}
	
	#Now print each row
	foreach my $ref (@sorted_dest) {
		my ($dfile,@junk)=&Manifest::DecodeDestLine($ref);
		#If there are unknown blocks 'before' this file, print them now
		if (exists($unknown_blocks{$dfile})) {
			#&::LogMsg("Printing unknown block before $$ref[0]");
			my $aref=$unknown_blocks{$dfile};
			foreach my $ukaref (@$aref) {
				#Make sure each line is a minimum length
				if ($#{$ukaref}<$line_length) {
					$#{$ukaref}=$line_length;
				}
				unless(print MANIFEST join(',',@$ukaref)."\n") {
					&::LogMsg("Failed writing to $interim_file: $!");
					return 0;
				}
			}
		}
		if ($#{$ref}<$line_length) {
			$#{$ref}=$line_length;
		}
		unless(print MANIFEST join(',',@$ref)."\n") {
			&::LogMsg("Failed writing to $interim_file: $!");
			return 0;
		}
	}
	
	#Print any lines that are at the end of the file
	if (exists($unknown_blocks{$Manifest::END_OF_FILE_HASH_KEY})) {
		#&::LogMsg("Appending end of file block");
		my $aref=$unknown_blocks{$Manifest::END_OF_FILE_HASH_KEY};
		foreach my $ukaref (@$aref) {
			#Make sure each line is a minimum length
			if ($#{$ukaref}<$line_length) {
				$#{$ukaref}=$line_length;
			}
			unless(print MANIFEST join(',',@$ukaref)."\n") {
				&::LogMsg("Failed writing to $interim_file: $!");
				return 0;
			}
		}
	}
	
	close(MANIFEST);
	
	return 1;
}

sub Manifest::SwapInterimDestManifest {
	my ($dst_mfst)=@_;
	
	my $interim=&Manifest::GetInterimName($dst_mfst);
	if (File::Copy::move($interim,$dst_mfst)) {
		#Success
	} else {
		&::LogMsg("Unable to move $interim to $dst_mfst: $!");
		return 0;
	}
	
	return 1;
}

############################
# Sub:  CreateSourceOrderMap
# Desc: A helper function to create a map of filenames to their array position
#       which will be used to sort the destination manifest array
#       We need the map to avoid iterating the source array for every file in the destination
# Args: Source Info ref - Ref to the source info
# Ret:  Reference to the hash array, key'd by source file name with a value of the position in the source array
############################
sub Manifest::CreateSourceOrderMap {
	my ($si_ref)=@_;
	
	my %source_order=();
	for(my $i=0;$i<scalar(@{$si_ref});$i++) {
		my $aref=$$si_ref[$i];
		my ($sfile,$frevision,$ftype,@discard)=&Manifest::DecodeSourceLine($aref);
		$source_order{$sfile}=$i;
	}
	
	return \%source_order;
}

1;