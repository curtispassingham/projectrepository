
#!/bin/sh

ORACLE_HOME=/u01/app/oracle/product/12.1.0/dbhome_1
WALLET=/u01/app/rms/orpatch/rms_wallet
ALIAS=dsDemoDataAlias

echo ""
echo "Note: when prompted for your secret/Password, enter the NEW value you want to use."
echo ""

echo -n "Enter the username to associate with $ALIAS (this can be the existing username or a new username): "
read USERNAME

echo ""
echo "Running $ORACLE_HOME/bin/mkstore -wrl $WALLET -modifyCredential $ALIAS $USERNAME"
echo ""
$ORACLE_HOME/bin/mkstore -wrl $WALLET -modifyCredential $ALIAS $USERNAME

exit $?
            