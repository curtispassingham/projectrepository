########################################################################
# Imported modules
########################################################################

import sys

import wl

########################################################################
# DatasourceCommands class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class DatasourceCommands:

   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   # print an error message
   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   # Utility method to check if a Data Source exists
   def checkDataSourceExists(self, dsName):
      """Check if a Data Source exists."""
      try:
        target=wl.lookup(dsName,"JDBCSystemResource")
        type=target.getType()
        print 'Data Source check: '+type+' '+dsName+' exists.'
        return type
      except:
        type=None
        print 'Data Source check: '+dsName+' does not exist.'

      return type

   def createDatasource(self, dsName, serverTarget, jndiName, databaseUrl, databaseUser, databasePassword, databaseClassName, wrapDataTypes, transactionOption):
      # If datasource does not exist, create it
      dsExists = "false"
      sources = wl.cmo.getJDBCSystemResources()
      for tmpDS in sources:
        if tmpDS.getName() == dsName:   
          dsExists = "true"
          break
 
      if dsExists == "true":  
        print 'Datasource '+dsName+'exists, it will be deleted and re-created...'
        self.deleteDatasource(dsName, serverTarget)

      print 'Creating data source with name '+dsName

      wl.edit()
      wl.startEdit(600000,600000,"true")
     
      try: 
        jdbcSR = wl.create(dsName,"JDBCSystemResource")
        theJDBCResource = jdbcSR.getJDBCResource()
        theJDBCResource.setName(dsName)
        connectionPoolParams = theJDBCResource.getJDBCConnectionPoolParams()
        connectionPoolParams.setConnectionReserveTimeoutSeconds(30)
        connectionPoolParams.setMaxCapacity(100)
        connectionPoolParams.setTestTableName("SQL SELECT 1 FROM DUAL")
        connectionPoolParams.setTestConnectionsOnReserve(wl.true)

        if wrapDataTypes == "false":
          print "Setting Wrap Data Types [false]"
          connectionPoolParams.setWrapTypes(0)
        elif wrapDataTypes == "true":
          print "Setting Wrap Data Types [true]"
          connectionPoolParams.setWrapTypes(1)

        dsParams = theJDBCResource.getJDBCDataSourceParams()

        print "Setting Transaction Option [" + transactionOption + "]"
        dsParams.setGlobalTransactionsProtocol(transactionOption)
     
        # Only add jndiName if it does not exist
        jndiNames=dsParams.getJNDINames()
        jndiExists="false"
        for jndi in jndiNames:
          if jndi == jndiName:
            jndiExists="true"
            break
     
        if jndiExists == "false":
          dsParams.addJNDIName(jndiName)
     
        driverParams = theJDBCResource.getJDBCDriverParams()
        driverParams.setUrl(databaseUrl)
        driverParams.setDriverName(databaseClassName)
        driverParams.setPassword(databasePassword)
        driverProperties = driverParams.getProperties()
     
        properties = driverProperties.getProperties()
        userPropertyExists="false"
        for property in properties:
          propertyName=property.getName()
          if propertyName == "user":
            userPropertyExists="true"
            proper=property
            break
     
        # Only create user property if it does not exist, other an error would be thrown
        if userPropertyExists == "false":
          proper = driverProperties.createProperty("user")
     
        proper.setValue(databaseUser)
        # This can be a server or a cluster.  Try server first, then cluster.  Otherwise an error will occur.
        target=wl.getMBean("/Servers/"+serverTarget)
        if target == None:
          target=wl.getMBean("/Clusters/"+serverTarget)
          if target == None:
            self.printError()
            print 'No server or cluster with name '+serverTarget+' was found, rolling back changes.'
            wl.cancelEdit('y')
            return 0
        jdbcSR.addTarget(target)
      except:
        self.printError()
        print 'Could not configure data source '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      try:
        wl.save()
      except:
        self.printError()
        print 'Could not save the addition of data source '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      try:
        wl.activate(600000,block="true")
        print 'Done configuring the data source '+dsName
      except:
        self.printError()
        print 'Could not activate datasource creation for '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      return 0

   def deleteDatasource(self, dsName, serverTarget):
      # get JDBC information
      deleteds = "false"
      sources = wl.cmo.getJDBCSystemResources()
      for jdbcSR in sources:
        if jdbcSR.getName() == dsName:
          deleteds = "true"
          wl.edit()
          wl.startEdit(600000,600000,"true")

          try: 
            print 'Deleting data source with name '+dsName
            wl.delete(dsName,"JDBCSystemResource")
          except:
            self.printError()
            print 'Could not remove data source '+dsName+', rolling back changes.'
            wl.cancelEdit('y')
            return 1
  
          try:
            wl.save()
          except:
            self.printError()
            print 'Could not save data source '+dsName+' deletion, rolling back changes.'
            wl.cancelEdit('y')
            return 1
            
          try:
            wl.activate(600000,block="true")
            print 'Done removing the data source '+dsName
            return 0 
          except:
            self.printError()
            print 'Could not activate data source '+dsName+' deletion, rolling back changes.'
            wl.cancelEdit('y')
            return 1

      if deleteds == "false":
        print 'Could not delete datasource ' +dsName+', because it does not exist.'
        return 0

   def setDataSourceXATransactionTimeout(self, dsName, transactionTimeout):

      dsCheck = self.checkDataSourceExists(dsName)
      if dsCheck == None:
        print "Error: Data Source " + dsName + " must exist in order to set its XA Transaction Timeout."
        return 1

      wl.edit()
      wl.startEdit(600000,600000,"true")
      print ""

      try:
        jdbcSR = wl.lookup(dsName,"JDBCSystemResource")
        theJDBCResource = jdbcSR.getJDBCResource()
        xaParams = theJDBCResource.getJDBCXAParams()

        transactionTimeoutInt=int(transactionTimeout)
        print "Data Source [" + dsName + "]: Setting XA Transaction Timeout [" + str(transactionTimeoutInt) + "]"
        xaParams.setXaSetTransactionTimeout(1)
        xaParams.setXaTransactionTimeout(transactionTimeoutInt)

      except:
        self.printError()
        wl.cancelEdit('y')
        print "Error configuring Data Source changes, rolling back to previous state."
        return 1

      try:
        wl.save()
        wl.activate(600000,block="true")
        print "Done setting XA Transaction Timeout to [" + str(transactionTimeoutInt) + "] for Data Source [" + dsName + "]"
      except:
        self.printError()
        wl.cancelEdit('y')
        print "Error saving and activating Data Source changes, rolling back to previous state."
        return 1

      return 0

