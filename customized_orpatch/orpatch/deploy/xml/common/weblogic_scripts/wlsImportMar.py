##########################################################################
# This script imports the metadata present in the Mar file into the server
##########################################################################
import wl

args=[sys.argv[0]]
for line in sys.stdin:
   args.append(line.rstrip())


user              = args[1]
password          = args[2]
url               = args[3]
appName           = args[4]
managedServerName = args[5]
marDir            = args[6]

#Conecting to the server	
connect(user, password, url)

#Test if we're deploying to a managed server or a cluster.
#In case it's a cluster, choose the first Managed Server.
try:
  targetName=wl.lookup(managedServerName,"Server")
  print 'Target for importMetadata is a server.'
  target=managedServerName
except:
  targetName=wl.lookup(managedServerName,"Cluster")
  print 'Target for importMetadata is a cluster. Will deploy to the first Managed Server...'
  target=targetName.getServers()[0].getName()


#Importing the Mar file content
importMetadata(appName, target, marDir, docs="/**")
