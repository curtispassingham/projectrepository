########################################################################
# Imported modules
########################################################################

import sys
import os

########################################################################
# GrantPermission class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################


#Reading the arguments passed
args=[sys.argv[0]]
for line in sys.stdin:
   args.append(line.rstrip())

username=args[1]
password=args[2]
adminUrl=args[3]
appStripe_value=None
codeBaseURL_value=args[5]
principalClass_value=None
principalName_value=None
permClass_value = args[8]
permTarget_value = args[9]
permActions_value = args[10]

#connecting to weblogic to grant permissions
connect(username, password, adminUrl)
try:
   revokePermission (appStripe=appStripe_value, codeBaseURL=codeBaseURL_value, principalClass=principalClass_value, principalName=principalName_value, permClass=permClass_value, permTarget=permTarget_value, permActions=permActions_value)
except:
   print 'Grants does not exists so adding grants' 

print 'Adding grants for'+codeBaseURL_value+' .'
grantPermission (appStripe=appStripe_value, codeBaseURL=codeBaseURL_value, principalClass=principalClass_value, principalName=principalName_value, permClass=permClass_value, permTarget=permTarget_value, permActions=permActions_value)
print 'Done adding grants for'+codeBaseURL_value+' .'
