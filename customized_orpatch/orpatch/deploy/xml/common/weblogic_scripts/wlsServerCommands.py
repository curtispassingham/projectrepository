########################################################################

import time

import java.lang.String as String
import java.lang.Integer as Integer
import java.io.File as File 

from xml.dom import minidom

import wl

########################################################################
# ServerCommands class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class ServerCommands:

   # Constructor
   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   # print an error message
   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   # Utility to get array of servers, either single server or all servers in cluster
   def getServers(self, targetServerName):
      targetServer = wl.getMBean("/Servers/" + targetServerName)
      if targetServer == None:
        targetServer = wl.getMBean("/Clusters/" + targetServerName)
        if targetServer == None:
          print 'Server target '+targetServerName+' does not exist.'
          return 1
        else:
          type="Cluster"
          cbean = wl.lookup(targetServerName,'Cluster')
          servers = cbean.getServers()
      else:
        type="Server"
        servers = [ targetServer ]

      return servers

   # Utility method to check if a server or cluster exists
   def checkServerOrClusterExists(self, serverTarget):
      """Check if a managed server exists."""
      try:
        target=wl.lookup(serverTarget,"Server")
        type=target.getType()
        print 'Server check: '+type+' '+serverTarget+' exists.'
        return type
      except:
        type=None
 
      try:
        target=wl.lookup(serverTarget,"Cluster")
        type=target.getType()
        print 'Server check: '+type+' '+serverTarget+' exists.'
      except:
        type=None
        print 'Server check: server or cluster '+serverTarget+' does not exist.'

      return type

   # method to get all servers in a cluster, meant to be called from wlsMain.py.  Store output in properties file.
   def getClusterServers(self, serverTarget):
      """Get all servers in a cluster."""
      servers = self.getServers(serverTarget)
      if servers == 1:
        return 1
      else:
        # concatenate array elements
        serverstring = ""
        for server in servers:
          name = server.getName()
          serverstring = serverstring + name + ','

        # write them to properties file
        finalserverstring = String(serverstring).substring(0, String(serverstring).length() - 1)
        print 'Found servers '+finalserverstring+' for target '+serverTarget+', writing to servers.txt...'
        output = open('/tmp/servers.txt','w')
        output.write('temp.servers='+finalserverstring+'\n')
        output.close()

        return 0
        
   # Method to start node manager for a domain if it's not already running
   def startNodeManager(self, wlsHome, listenAddress, listenPort):
      """start node manager"""
      nmHome=wlsHome+'/wlserver_10.3/common/nodemanager'
      try:
        wl.startNodeManager(verbose='true', NodeManagerHome=nmHome, ListenPort=listenPort, ListenAddress=listenAddress)
        return 0
      except:
        self.printError()
        print 'Could not start the node manager, exiting...'
        return 1

   # Method to set server start options (arguments and classpath)
   def setServerStartOptions(self, serverTarget, arguments, classpath):
      """Set the server start options for the server or all servers in a cluster"""
      wl.domainConfig()
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
        print serverTarget+' does not exist, server start options will not be set.'
        return 1

      wl.edit()
      wl.startEdit(600000,600000,"true")
      print 'Setting server start options for '+serverTarget+'...'
      try:
        arguments = String(arguments).trim()
        classpath = String(classpath).trim()
        if type == "Server":
          server = wl.lookup(serverTarget,'Server')
          options = server.getServerStart()
          if arguments != None and arguments != "":
            options.setArguments(arguments)
          if classpath != None and classpath != "":
            options.setClassPath(classpath)
        else:
          cbean = wl.lookup(serverTarget,'Cluster')
          servers = cbean.getServers()
          for server in servers:
            options = server.getServerStart()
            if arguments != None and arguments != "":
              options.setArguments(arguments)
            if classpath != None and classpath != "":
              options.setClassPath(classpath)
        wl.save()
        wl.activate(block="true")
        print 'Server start options have successfully been set.  The '+type+' '+serverTarget+' must be restarted to pick up the changes.'
        return 0
      except:
        self.printError()
        print 'Could not set server start options for '+serverTarget+', rolling back changes...'
        wl.cancelEdit('y')
        return 1          
   # Method to disable non-ssl port for server or cluster
   def DisableNonSSLPort(self, serverTarget):
      """Disable the non-SSL port for the server or all servers in a cluster"""
      wl.domainConfig()
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
        print serverTarget+' does not exist, server start options will not be set.'
        return 1

      wl.edit()
      wl.startEdit(600000,600000,"true")
      print 'Disabling Non-SSL port for '+serverTarget+'...'
      try:
        if type == "Server":
          server = wl.lookup(serverTarget,'Server')
          server.setListenPortEnabled(0)
        else:
          cbean = wl.lookup(serverTarget,'Cluster')
          servers = cbean.getServers()
          for server in servers:
            server.setListenPortEnabled(0)
        wl.save()
        wl.activate(block="true")
        print 'Non-SSL port has been successfully disabled.  The '+type+' '+serverTarget+' must be restarted to pick up the changes.'
        return 0
      except:
        self.printError()
        print 'Could not disable Non-SSL port for '+serverTarget+', rolling back changes...'
        wl.cancelEdit('y')
        return 1      

   # Formal method to check if target exists
   def checkTargetExists(self, target):
      """Check if a target exists."""
      wl.domainConfig()
      print 'Testing the existence of '+target+'...'
      type=self.checkServerOrClusterExists(target)
      if type == None:
        exists = "false"
        print 'Error: target '+target+' does not exist as expected.'
      else:
        exists = "true"
        print 'Target '+target+' exists as expected.'

      output = open('/tmp/target.txt','w')
      output.write('target.exists='+exists)
      output.close()
      return 0

   # Method to restart a server or cluster
   def restartServerOrCluster(self, serverTarget):
      """Restart a weblogic managed server or cluster"""
      self.stopServerOrCluster(serverTarget)
      self.startServerOrCluster(serverTarget)
      return 0

   # Method to get the primary server name of a cluster
   def getPrimaryServerName(self, serverTarget):
      """Get the server name or primary server name in a cluster"""
      wl.domainConfig()
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
        print serverTarget+' does not exist, cannot get primary name.'
        return 1

      print 'Getting primary name for '+serverTarget+'...' 
      if type == "Server":
        sbean = wl.lookup(serverTarget,'Server')
      else:
        cbean = wl.lookup(serverTarget,'Cluster')
        servers = cbean.getServers()
        sbean = wl.lookup(servers[0].getName(),'Server')

      primaryServerName = sbean.getName()

      print 'Retreived primary server name '+primaryServerName+' for target '+serverTarget+', writing to server.txt...'
      output = open('/tmp/server.txt','w')
      output.write('temp.primary.server.name='+primaryServerName)
      output.close()
      return 0

   # Method to get a port numver of a server
   def getPort(self, serverTarget, SSL):
      """Get the port number of the server or primary server in cluster"""
      wl.domainConfig()
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
        print serverTarget+' does not exist, cannot get port number.'
        return 1

      print 'Getting port for '+serverTarget+'...' 
      if type == "Server":
        sbean = wl.lookup(serverTarget,'Server')
      else:
        cbean = wl.lookup(serverTarget,'Cluster')
        servers = cbean.getServers()
        sbean = wl.lookup(servers[0].getName(),'Server')

      # Figure out which ports are enabled
      sslobject = sbean.getSSL()
      SSLEnabled = sslobject.isEnabled()
      nonSSLEnabled = sbean.isListenPortEnabled()

      # Get non-SSL or SSL port
      if SSL == "true":
        port = sslobject.getListenPort()
        if SSLEnabled == 1:
          print 'The SSL port is enabled as expected.  Returning the SSL port for '+serverTarget
          enabled = "true"
        else:
          if nonSSLEnabled == 1:
            print 'Error: The SSL port is disabled for '+serverTarget+', even though this function was told to return the SSL port.  Returning the SSL port anyway. This must be enabled for the applications deployed to this server to work properly.'
            enabled = "false"
          else:
            print 'Error: The SSL and non-SSL ports are disabled for '+serverTarget+', even though this function was told to return the SSL port.  Returning the SSL port anyway.  This must be enabled for the applications deployed to this server to work properly.'
            enabled = "false"
      else:
        port = sbean.getListenPort()
        if nonSSLEnabled == 1:
          print 'The non-SSL port enabled as expected.  Returning the non-SSL port for '+serverTarget
          enabled = "true"
        else:
          print 'Error: The non-SSL port is disabled for '+serverTarget+', even though this function was told to return the non-SSL port.  Returning the non-SSL port anyway.  This must be enabled for the applications deployed to this server to work properly.'
          enabled = "false"

      portstring = Integer.toString(port)
      print 'Retreived port '+portstring+' for target '+serverTarget+', writing to port.txt...'
      output = open('/tmp/port.txt','w')
      output.write('temp.port.number='+portstring+'\n')
      output.close()

      if enabled == "true":
        return 0 
      else:
        return 1

   # Method to stop a server or cluster
   def stopServerOrCluster(self, serverTarget):
      """Stop a weblogic managed server"""
      if serverTarget == "AdminServer":
        print "The AdminServer cannot be stopped via wlst in the installer.  Specify a managed server/cluster instead."
        return 1
   
      wl.domainConfig()
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
        print serverTarget+' does not exist, and cannot be stopped.'
        return 1

      print 'Begin stopping '+type+' '+serverTarget+'...' 
      shutdownserver = "false"
      print 'Getting state for '+serverTarget+'...'
      if type == "Server":
        wl.domainRuntime()
        slcBean = wl.cmo.lookupServerLifeCycleRuntime(serverTarget)
        status = slcBean.getState()
        print 'Status of target ' +serverTarget+' is '+status
        if status == "SHUTDOWN" or status == "FAILED_NOT_RESTARTABLE":
          shutdownserver = "true"
      else:
        cbean = wl.lookup(serverTarget,'Cluster')
        servers = cbean.getServers()
        wl.domainRuntime()
        for server in servers:
          sTarget = server.getName()
          slcBean = wl.cmo.lookupServerLifeCycleRuntime(sTarget)
          status = slcBean.getState()
          print 'Status of target ' +sTarget+' is '+status
          if status == "SHUTDOWN" or status == "FAILED_NOT_RESTARTABLE":
            shutdownserver = "true"

      if shutdownserver == "true":
        print type+' '+serverTarget+' is already shutdown, no attempt will be made to shut it down.'
      else:
        print 'Shutting down '+serverTarget+'...'
        wl.shutdown(serverTarget,type,'true',15,force='true')

        #check status to make sure it was shutdown
        checkCount = 0
        status = slcBean.getState()
        while status != "SHUTDOWN":
          checkCount += 1
          print 'Status of target ' +serverTarget+' is '+status+', waiting for SHUTDOWN'
          print "Checked %d times." % (checkCount, )
          time.sleep(5)
          status = slcBean.getState()
          if (checkCount > 12):
            print "Server shutdown for "+serverTarget+" failed!"
            return 1
           
        print serverTarget+' was shut down successfully.'
      return 0
  
   # Method to start a server or cluster 
   def startServerOrCluster(self, serverTarget):
      """Start a weblogic managed server"""
      if serverTarget == "AdminServer":
        print "The AdminServer cannot be started via wlst in the installer.  Specify a managed server/cluster instead."
        return 1

      wl.domainConfig()
      domainDir = wl.cmo.getRootDirectory()
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
        print serverTarget+' does not exist, and cannot be started.'
        return 1

      print 'Begin starting '+type+' '+serverTarget+'.' 
      startup = "false"
      print 'Getting state for '+serverTarget+'...'
      if type == "Server":
        wl.domainRuntime()

        slcBean = wl.cmo.lookupServerLifeCycleRuntime(serverTarget)
        status = slcBean.getState()
        print 'Status of target ' +serverTarget+' is '+status
        if status == "RUNNING":
          startup = "true"
        else:
          # remove all .lok files for all ms components
          file=File(String(domainDir +'/servers/'+ serverTarget +'/tmp/'+ serverTarget + '.lok'))
          file.delete()

      else:
        cbean = wl.lookup(serverTarget,'Cluster')
        servers = cbean.getServers()
        wl.domainRuntime()
        for server in servers:
          sTarget = server.getName()

          slcBean = wl.cmo.lookupServerLifeCycleRuntime(sTarget)
          status = slcBean.getState()
          print 'Status of target ' +sTarget+' is '+status
          if status == "RUNNING":
            startup = "true"
          else:
            # remove all .lok files for all ms components
            file=File(String(domainDir +'/servers/'+ sTarget +'/tmp/'+ sTarget + '.lok'))
            file.delete()

      if startup == "true":
        print type+' '+serverTarget+' is already running, no attempt will be made to start it up.'
      else:
        #check status to make sure it was shut down first
        status = slcBean.getState()
        if status == "FORCE_SHUTTING_DOWN":
          #check status to make sure it was shut down 
          checkCount = 0
          while status != "SHUTDOWN":
            checkCount += 1
            print 'Status of target ' +serverTarget+' is '+status+', waiting for SHUTDOWN'
            print "Checked %d times." % (checkCount, )
            time.sleep(5)
            status = slcBean.getState()
            if (checkCount > 12):
              print "Server "+serverTarget+" could not enter SHUTDOWN state prior to starting, command failed!"
              return 1

        print 'Starting '+serverTarget+'...'
        wl.start(serverTarget,type)

        #check status to make sure it was started
        checkCount = 0
        status = slcBean.getState()
        while status != "RUNNING":
          checkCount += 1
          print 'Status of target ' +serverTarget+' is '+status+', waiting for RUNNING'
          print "Checked %d times." % (checkCount, )
          time.sleep(5)
          status = slcBean.getState()
          if (checkCount > 12):
            print "Server start for "+serverTarget+" failed!"
            return 1
           
        print serverTarget+' was started successfully.'
      return 0

   # Internal method to check if a machine exists
   def checkMachineExists(self, machineName):
     try:
       tmpmachine=wl.lookup(machineName, 'Machine')
       if tmpmachine != None:
         print 'Machine check: machine '+machineName+' exists.'
         return "true"
     except:
       print 'Machine check: machine '+machineName+' does not exist.'
       return "false"

   # Method to check that node manager is running
   def checkNodeManager(self, username, password, serverTarget):
      # Check that target exists first
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
        print serverTarget+' does not exist, node manager status cannot be checked.'
        output = open('/tmp/nodemanager.txt','w')
        output.write('nodemanager.running=it cannot be determined')
        output.close()
        return 0

      # if it does get the primary server
      if type == "Server":
        sbean = wl.lookup(serverTarget,'Server')
      else:
        cbean = wl.lookup(serverTarget,'Cluster')
        servers = cbean.getServers()
        sbean = wl.lookup(servers[0].getName(),'Server')
      primaryServerName = sbean.getName()

      # get the machine for the server
      tmpmachine = sbean.getMachine()
      nodemanager=tmpmachine.getNodeManager()
      listenaddress = nodemanager.getListenAddress()
      listenport = nodemanager.getListenPort()
      machinetype = nodemanager.getNMType()

      # attempt node manager connection
      domaindir = wl.cmo.getRootDirectory()
      tmpDirs=String(domaindir).split("/")
      domainname=tmpDirs[len(tmpDirs)-1]
      print "Testing Node manager availability for address "+listenaddress+" and port "+Integer.toString(listenport)+"."
      try:
        wl.nmConnect(username,password,listenaddress,listenport,domainname,domaindir,machinetype)
        wl.nmDisconnect()
        print "Node manager at address "+listenaddress+" and port "+Integer.toString(listenport)+" was reached successfully."
        running="true"
      except:
        print "Error: node manager at address "+listenaddress+" and port "+Integer.toString(listenport)+" cannot be reached."
        running="false"

      # Write output to file
      output = open('/tmp/nodemanager.txt','w')
      output.write('nodemanager.running='+running)
      output.close()
      return 0

 # Method to get Machine Names in a cluster
   def getClusterMachines(self, serverTarget):
      servers = self.getServers(serverTarget)
      # concatenate array elements
      serverstring = ""
      for server in servers:
        tmpmachine = server.getMachine()
        nodemanager=tmpmachine.getNodeManager()
        listenaddress = nodemanager.getListenAddress()
        serverstring = serverstring + listenaddress + ','
      # write them to properties file
      finalserverstring = String(serverstring).substring(0, String(serverstring).length() - 1)
      print 'Found machines '+finalserverstring+' for target '+serverTarget+', writing to Machines.txt...'
      output = open('/tmp/Machines.txt','w')
      output.write('temp.Machines='+finalserverstring+'\n')
      output.close()
      return 0

   # Method to create a machine to associate a server to a node manager
   def createMachine(self, machineName, listenAddress, listenPort):
     machineExists=self.checkMachineExists(machineName)
     if machineExists == "false":
       print 'Starting creation of machine '+machineName+'...'
       wl.edit()
       wl.startEdit(600000,600000,"true")
       try:
         tmpmachine=wl.create(machineName,'Machine')
         nodemanager=tmpmachine.getNodeManager()
         nodemanager.setListenAddress(listenAddress)
         nodemanager.setListenPort(long(listenPort))
         wl.save()
         wl.activate(block="true")
         print 'Machine '+machineName+' has been created.'
         return 0
       except:
         self.printError()
         print 'Could not create machine '+machineName+', rolling back changes...'
         wl.cancelEdit('y')
         return 1
     else:
       print 'Machine '+machineName+' was not created, because it already exists.'
       return 1
     
   # Method to delete a machine
   def deleteMachine(self, machineName):
     machineExists=self.checkMachineExists(machineName)
     if machineExists == "true":
       print 'Starting deletion of machine '+machineName+'...'
       wl.edit()
       wl.startEdit(600000,600000,"true")
       try:
         # First get all servers and untarget the machines from them if they are this machine
         allservers=wl.cmo.getServers()
         for tmpserver in allservers:
           tmpmachine=tmpserver.getMachine()
           if machineName == tmpmachine:
             tmpserver.setMachine(None)
         wl.delete(machineName, 'Machine')
         wl.save()
         wl.activate(block="true")
         print 'Machine '+machineName+' has been deleted.'
         return 0
       except:
         self.printError()
         print 'Could not delete machine '+machineName+', rolling back changes...'
         wl.cancelEdit('y')
         return 1
     else:
       print 'Machine '+machineName+' was not deleted, because it does not exist.'
       return 1

   # Method to recursively create clusters/servers
   def createServerOrCluster(self, serverTarget, objecttype, machineName, serverList, docreate, SSL, port):
      """Create a weblogic managed server or cluster"""
      print 'Begin creating '+objecttype+' '+serverTarget+'.'
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
         """Enter edit tree"""
         wl.edit()
         wl.startEdit(600000,600000,"true")
         try:
            retval=self.doCreateServerOrCluster(serverTarget, objecttype, machineName, serverList, docreate, SSL, port)
            if retval > 0:
              print 'Rolling back changes...'
              wl.cancelEdit('y')
            else:
              wl.save()
              wl.activate(block="true")
              print objecttype+' '+serverTarget+' has been created.'
            return retval
         except:
            self.printError() 
            print objecttype+' '+serverTarget+' could not be created, rolling back unsaved changes.'
            wl.cancelEdit('y')
            return 1
      else:
          print objecttype+' '+serverTarget+' could not be created, because it already exists.'
          return 1
      return 0

   # Internal method to recursively create clusters/servers without try/except and edit/save commands 
   def doCreateServerOrCluster(self, serverTarget, objecttype, machineName, serverList, docreate, SSL, port):
      retval = 0
      print 'Creating '+objecttype+' '+serverTarget+'...'
      serverobject=wl.create(serverTarget, objecttype)
      if objecttype == "Server":
        if SSL == "true":
          sslobject = serverobject.getSSL()
          sslobject.setEnabled(1)
          sslobject.setListenPort(Integer.parseInt(port))
          serverobject.setListenPortEnabled(0)
        else:
          serverobject.setListenPortEnabled(1)
          serverobject.setListenPort(Integer.parseInt(port))
        if machineName != None:
          machineExists=self.checkMachineExists(machineName)
          if machineExists == "true":
            print 'Setting machine name for '+serverTarget+' to '+machineName+'...'
            mbean=wl.lookup(machineName, "Machine")
            serverobject.setMachine(mbean)
      else:
        serverobject.setClusterMessagingMode("Unicast") 
        serverList = String(serverList).replaceAll("[ ][ ]*", "")
        tmpservers = String(serverList).split(",")
        tmpport = port
        for tmpserver in tmpservers:
          tmptype=self.checkServerOrClusterExists(tmpserver)
          if tmptype == None:
            if docreate == "true":
              retval=self.doCreateServerOrCluster(tmpserver, "Server", machineName, None, None, SSL, tmpport)
              if retval == 0:
                sbean=wl.lookup(tmpserver, "Server")
                cbean=wl.lookup(serverTarget, "Cluster")
                print 'Assigning '+tmpserver+' to '+serverTarget+'...'
                sbean.setCluster(cbean)
                if machineName != None:
                  machineExists=self.checkMachineExists(machineName)
                  if machineExists == "true": 
                    print 'Setting machine name for '+tmpserver+' to '+machineName+'...'
                    mbean=wl.lookup(machineName, "Machine")
                    sbean.setMachine(mbean)
            else:
              print 'Error: server '+tmpserver+' cannot be added to the cluster, because it does not exist.'
              retval = 1
          else:
            print 'Error: Cannot create '+tmptype+' '+tmpserver+', because it already exists.'
            return 1
          tmpport = Integer.toString(Integer.parseInt(tmpport)+1) 

      return retval 

   # Method to recursively delete clusters/servers
   def deleteServerOrCluster(self, serverTarget, objecttype, dodelete):
      """Delete a weblogic managed server"""
      """First stop server if it's running"""
      print 'Begin deleting '+objecttype+' '+serverTarget+'.'
      type=self.checkServerOrClusterExists(serverTarget)
      if type == None:
        print objecttype+' '+serverTarget+' could not be deleted, because it does not exist.'
        return 1
      else:
        retval=self.stopServerOrCluster(serverTarget)
        if retval == 0:
          """Enter edit tree"""
          wl.edit()
          wl.startEdit(600000,600000,"true")
          try:
             retval=self.doDeleteServerOrCluster(serverTarget,objecttype,dodelete)
             if retval > 0:
               print 'Rolling back changes...'
               wl.cancelEdit('y')
             else:
               wl.save()
               wl.activate(block="true")
               print objecttype+' '+serverTarget+' has been deleted.'
             return retval
          except:
             self.printError()
             print objecttype+' '+serverTarget+' could not be deleted, rolling back unsaved changes.'
             wl.cancelEdit('y')
             return 1
        else:
          print 'Could not delete '+serverTarget+' because it failed to shut down.'
          return 1
      return 0

   # Internal method to recursively delete clusters/servers without try/except and edit/save commands 
   def doDeleteServerOrCluster(self, serverTarget, objecttype, dodelete):
      retval = 0
      if objecttype == "Cluster":
        cbean = wl.lookup(serverTarget, "Cluster")
        if cbean != None:
          serverList=cbean.getServers()
          for mserver in serverList:
            tmpserver = mserver.getName()
            tmpserver = String(tmpserver).replaceAll("[ ][ ]*", "")
            tmptype=self.checkServerOrClusterExists(tmpserver)
            if tmptype == "Server":
              if dodelete == "true":
                retval=self.doDeleteServerOrCluster(tmpserver, "Server", None)
              else:
                print 'Server '+tmpserver+' will not be unassigned from '+serverTarget+'.'
            else:
              print 'Error: cannot delete '+tmptype+' '+tmpserver+', because it does not exist.'
              retval = 1
        print 'Deleting '+objecttype+' '+serverTarget+'...'
        wl.delete(serverTarget,objecttype)
      else:
        print 'Deleting '+objecttype+' '+serverTarget+'...'
        sbean = wl.lookup(serverTarget, "Server")
        sbean.setCluster(None)
        sbean.setMachine(None)
        wl.delete(serverTarget,objecttype)
      return retval 
