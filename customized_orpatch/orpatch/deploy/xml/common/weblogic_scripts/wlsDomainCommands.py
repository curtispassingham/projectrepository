########################################################################

import sys
import os

import java.lang.String as String
import java.io.File as File

import wl

########################################################################
# ServerCommands class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class DomainCommands:

   # Constructor
   def __init__(self, username, password, listenaddress, nmport, domainDir):
     """Initialize a WebLogic MBean connection."""
     self.username = username
     self.password = password
     self.listenaddress = listenaddress 
     self.nmport = nmport
     self.domainDir = domainDir

   # print an error message
   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   # create a domain
   def makeDomain(self, adminport, mode):
     # Set variables 
     ORACLE_HOME=os.getenv('ORACLE_HOME')
     JAVA_HOME=os.getenv('JAVA_HOME')
     nmHome=ORACLE_HOME+'/wlserver_10.3/common/nodemanager'
     tmpDirs=String(self.domainDir).split("/")
     domainName=tmpDirs[len(tmpDirs)-1]

     domainDirFile=File(String(self.domainDir))
     if domainDirFile.exists():
       print 'Domain directory '+self.domainDir+' already exists, exiting domain creation.'
       return 0	   
	 
     print 'Creating domain directory '+self.domainDir+'.  This will take a few moments...'

     # Read domain template
     try:
       wl.readTemplate(ORACLE_HOME+'/wlserver_10.3/common/templates/domains/wls.jar')
     except:
       self.printError()
       return 1
        
     #Configure admin server
     try:
       wl.cd('Servers/AdminServer')
       wl.set('ListenAddress',self.listenaddress)
       wl.set('ListenPort',long(adminport))
       wl.create('AdminServer','SSL')
       wl.cd('SSL/AdminServer')
       wl.set('Enabled', 'True')
       wl.set('ListenPort', long(adminport)+1)
       wl.set('HostNameVerificationIgnored', 'True')
       wl.cd('../..')
       wl.create('AdminServer','ServerStart')
       wl.cd('ServerStart/AdminServer')
       wl.set('Arguments', '-XX:MaxPermSize=512m')
  
       # Set password
       wl.cd('/')
       wl.cd('Security/base_domain/User/weblogic')
       wl.set('Password', self.password)
  
       # Set domain options
       wl.setOption('CreateStartMenu', 'false')
       wl.setOption('ServerStartMode', mode)
       wl.setOption('JavaHome', JAVA_HOME)
       wl.setOption('OverwriteDomain', 'true')
     except:
       self.printError()
       return 1

     # Write domain
     try:
       wl.writeDomain(self.domainDir)
     except:
       self.printError()
       return 1

     # Connect to node manager, retart admin server, and enroll domain
     try:
       wl.closeTemplate()
       wl.startNodeManager(verbose='true', NodeManagerHome=nmHome, ListenPort=self.nmport, ListenAddress=self.listenaddress)
       wl.nmConnect(self.username,self.password,self.listenaddress,self.nmport,domainName,self.domainDir)
     except:
       self.printError()
       print 'Could not get a handle for the node manager, rolling back domain creation...'
       self.removeDomain()
       return 1

     try:
       wl.nmKill('AdminServer')
     except:
       print 'AdminServer is not running, no attempt will be made to shut it down.'

     try:
       wl.nmStart('AdminServer', self.domainDir)
       wl.nmDisconnect()
     except:
       self.printError()
       print 'Could not start AdminServer, rolling back domain creation...'
       self.removeDomain()
       return 1

     # Test connection to new admin server
     try:
       print 'Testing connection to the AdminServer for the new domain '+domainName+'...'
       wl.connect(self.username, self.password, 't3://'+self.listenaddress+':'+adminport)
       wl.nmEnroll(self.domainDir, nmHome)
       wl.disconnect()
     except:
       self.printError()
       print 'Could not connect to the admin server for domain '+domainName+', rolling back domain creation...'
       self.removeDomain()
       return 1

     print 'Domain '+domainName+' was successfully created.' 

     return 0

   # Wrapper for removing a domain via spawn command
   def runRemoveDomain(self):
     os.spawnlp(os.P_WAIT, os.getenv(JAVA_HOME)+'/bin/java', 'java', 'weblogic.WLST', 'wlsMain.py', self.username, self.password, self.listenaddress, 'DeleteDomain', self.nmport, self.domainDir)

   # delete a domain - this is done by shutting down admin server and deleting the domain directory :)
   def removeDomain(self):
     # Set variables 
     ORACLE_HOME=os.getenv('ORACLE_HOME')
     JAVA_HOME=os.getenv('JAVA_HOME')
     nmHome=ORACLE_HOME+'/wlserver_10.3/common/nodemanager'
     tmpDirs=String(self.domainDir).split("/")
     domainName=tmpDirs[len(tmpDirs)-1]

     print 'Deleting domain directory '+self.domainDir+'.  This will take a few moments...'

     # Get a list of all the server in this domain
     try:
       wl.readDomain(self.domainDir)
       allservers=wl.get('Servers')
       wl.closeDomain()
     except:
	   self.printError()
	   print 'Could not delete domain '+domainName+' because it does not exist.'
	   return 0

     # Connect to node manager, and kill managed servers and admin server 
     try:
       wl.startNodeManager(verbose='true', NodeManagerHome=nmHome, ListenPort=self.nmport, ListenAddress=self.listenaddress)
     except:
       print 'Node manager is already running, no attempt will be made to start it.'
     try:
       wl.nmConnect(self.username,self.password,self.listenaddress,self.nmport,domainName,self.domainDir)
     except:
       self.printError()
       print 'Could not delete domain '+domainName+': could not connect to the node manager.'
       return 1
     for tmpserver in allservers:
       servername=tmpserver.getName()
       try:
         wl.nmKill(servername)
       except:
         'Server '+servername+' is not running, no attempt will be made to shut it down.'
     try:
       wl.nmKill('AdminServer')
     except:
       print 'AdminServer is not running, no attempt will be made to shut it down.'
       
     # Delete domain directory
     try:
       wl.nmDisconnect()
       self.deleteDir(self.domainDir)
     except:
       self.printError()
       print 'Domain directory for domain '+domainName+' could not be removed...'
       return 1

     print 'Domain '+domainName+' was successfully deleted.'

     return 0
 
   # Recursively delete a directory
   def deleteDir(self, dirname):
     files=File(String(dirname)).listFiles()
     for f in files:
       if f.isDirectory():
         self.deleteDir(dirname+'/'+f.getName())
       else: 
         f.delete()
     File(String(dirname)).delete()
