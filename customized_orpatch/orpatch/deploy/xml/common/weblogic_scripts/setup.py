########################################################################
# Imported modules
########################################################################

import sys

########################################################################
# MAIN SCRIPT
#
# File: setup.py
#
# Description: create wl.py (or value passed in as first argument) which
#              is imported by the other wlst modules, such as wlsMain.py.
#              If this module does not exist, the wlst commands will fail.
#
# Usage: java weblogic.WLST setup.py <full directory path to wl.py file to be generated>
#
# Updates: NAME        DATE        DESCRIPTION
#          ====        ====        ===========
#          damaas      2011/02/10  Added documentation
#
########################################################################

def usage():
   print "Usage: setup.py <full directory path to wl.py file to be generated>"
   exit(exitcode=2)

# Get the command-line arguments
if len(sys.argv) < 1:
   usage()

genFile = sys.argv[1]+"/wl.py"

print "Generating wl.py file: "+genFile

# Call built-in writeIniFile method to generate wl.py in the appropriate path
writeIniFile(genFile)

exit(exitcode=0)
