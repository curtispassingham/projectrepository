
#Reading the arguments passed
args=[sys.argv[0]]
for line in sys.stdin:
   args.append(line.rstrip())

adminUser=args[1]   
adminPassword=args[2]  
adminUrl=args[3]
appName=args[4]
connectionName=args[5]
context=args[6]
host=args[7]
isStaticResourcesLocationAutomatic=args[8]
port=args[9]
protocol=args[10]
shouldPerformImpersonation=args[11]
staticResourcesLocation=args[12]
username=args[13]
password=args[14]
wsdlContext=args[15]

def adf_isNotEmpty(arg):
    arg=str(arg)
    if arg and arg.strip():
        # arg is not None AND arg is not empty or blank
        return True
    # arg is None OR arg is empty or blank
    return False

def adf_stringToBoolean(arg):
    arg=str(arg)
    if arg=='true' or arg=='True':
        return True
    return False

# BIConnection args buildup
def adf_buildBIConnectionArgs(context,host,isStaticResourcesLocationAutomatic,port,protocol,shouldPerformImpersonation,staticResourcesLocation,username,password,wsdlContext):
    args={}
    if adf_isNotEmpty(context):
    	args['Context']=context
    else:
		args['Context']='analytics'
    args['Host']=host
    if adf_isNotEmpty(isStaticResourcesLocationAutomatic):
		args['IsStaticResourcesLocationAutomatic']=adf_stringToBoolean(isStaticResourcesLocationAutomatic)
    else:
		args['IsStaticResourcesLocationAutomatic']=True
    args['Port']=port
    if adf_isNotEmpty(protocol):
    	args['Protocol']=protocol
    else:
		args['Protocol']='http'
    if adf_isNotEmpty(shouldPerformImpersonation):
		args['ShouldPerformImpersonation']=adf_stringToBoolean(shouldPerformImpersonation)
    else:
		args['ShouldPerformImpersonation']=True
    args['StaticResourcesLocation']=staticResourcesLocation
    args['Username']=username
    args['Password']=password
    if adf_isNotEmpty(wsdlContext):
    	args['WSDLContext']=wsdlContext	
    else:
    	args['WSDLContext']='analytics-ws'
    return args

#Create or Update BI Connection
def adf_createOrUpdateBIConnection(adminUser,adminPassword,adminUrl,appName,connectionName,context,host,isStaticResourcesLocationAutomatic,port,protocol,shouldPerformImpersonation,staticResourcesLocation,username,password,wsdlContext):
	connectionType='BISoapConnection'
    #connecting to WebLogic domain
	connect(adminUser,adminPassword,adminUrl) 
	#change to domainRuntime
	origDirectory = adf_Initialize()
	if origDirectory is None:
		return None
	#build the userArg dictionary
	userArg=adf_buildBIConnectionArgs(context,host,isStaticResourcesLocationAutomatic,port,protocol,shouldPerformImpersonation,staticResourcesLocation,username,password,wsdlContext)
	#create the BI Connection or if the connection already exists update the same connection
	adf_customCreateConnection(appName,connectionName,connectionType,userArg)
	#save the connection
	adf_saveConnections(appName)
	adf_Restore(origDirectory)
	print 'CreateOrUpdateBIConnection Completed.'
	return

#Create/Update the ADF BI connection
adf_createOrUpdateBIConnection(adminUser,adminPassword,adminUrl,appName,connectionName,context,host,isStaticResourcesLocationAutomatic,port,protocol,shouldPerformImpersonation,staticResourcesLocation,username,password,wsdlContext)