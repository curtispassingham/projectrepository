#!/bin/ksh
# Copyright © 2014 Oracle and/or its affiliates. All rights reserved.

if [ -z "$RETAIL_HOME" ]; then
	if [ ! -z "$MMHOME" ]; then
		RETAIL_HOME=$MMHOME
		export RETAIL_HOME
	else
		echo "RETAIL_HOME environment variable must be defined"
		exit 1
	fi
fi

function usage
{
   echo "Usage: $0 <action_xml> <deploy_config> <action_phase>"
   exit 1
}

function kill_child
{
   if [ ! -z "$CHILD_PID" ]; then
      kill -TERM $CHILD_PID
   fi
   exit 1
}

trap 'kill_child' TERM INT

#First argument is ant xml file
ACTION_XML=$1

if [ -z "$ACTION_XML" ]; then
   echo "ERROR: Required argument action_xml missing"
   usage
fi

FULL_ACTION_XML=$RETAIL_HOME/orpatch/deploy/xml/$ACTION_XML

if [ ! -f "$FULL_ACTION_XML" ]; then
   echo "ERROR: Specified action xml $FULL_ACTION_XML does not exist"
   usage
fi

#Second argument is path to deploy config directory
DEPLOY_CONFIG=$2

if [ -z "$DEPLOY_CONFIG" ]; then
   echo "ERROR: Required argument deploy_config missing"
   usage
fi

if [ ! -d "$DEPLOY_CONFIG" ]; then
   echo "ERROR: Deploy config directory $DEPLOY_CONFIG does not exist!"
   usage
fi

PROPERTY_FILE=$DEPLOY_CONFIG/ant.deploy.properties

if [ ! -f "$PROPERTY_FILE" ]; then
   echo "ERROR: Unable to find deploy property file: $PROPERTY_FILE"
   usage
fi

#Third argument is the action phase 
ACTION_PHASE=$3

if [ -z "$ACTION_PHASE" ]; then
   echo "ERROR: Required argument action_phase missing"
   usage
fi

if [ "$ACTION_PHASE" = "precheck" ]; then
   ANT_TARGET="precheck"
elif [ "$ACTION_PHASE" = "deploy" ]; then
   ANT_TARGET="deploy"
else
   echo "ERROR: Unknown action phase: $ACTION_PHASE"
   exit 1
fi

#Setup ant execution environment
if [ -z "$JAVA_HOME" ]; then
   echo "JAVA_HOME must be defined"
   exit 1
fi

PATH=$JAVA_HOME/bin:$PATH
export PATH

ORPDEPLOY_HOME=$RETAIL_HOME/orpatch/deploy
ANT_HOME=$ORPDEPLOY_HOME/ant
export ANT_HOME
ANT_EXT_HOME=$ORPDEPLOY_HOME/ant-ext
RPS_LIB_HOME=$ORPDEPLOY_HOME/retail-public-security-api/lib

# Ant jars
CLASSPATH=$ANT_EXT_HOME/ant-contrib.jar

#ANT_LIB_JARS=`ls $ANT_HOME/lib/*.jar $ANT_EXT_HOME/*.jar $RPS_LIB_HOME/*.jar 2>/dev/null`
ANT_LIB_JARS=`ls $ANT_EXT_HOME/*.jar $RPS_LIB_HOME/*.jar 2>/dev/null`
for j in  $ANT_LIB_JARS; do
    CLASSPATH=$CLASSPATH:$j
done
export CLASSPATH

if [ ! -x "$ANT_HOME/bin/ant" ]; then
   echo "ERROR: $ANT_HOME/bin/ant is not executable"
   exit 1
fi

$ANT_HOME/bin/ant -f "$FULL_ACTION_XML" -propertyfile $PROPERTY_FILE -Dinput.retail.home=$RETAIL_HOME -Dinput.orpatch.deploy.config=$DEPLOY_CONFIG $ANT_TARGET &
CHILD_PID=$!
wait $CHILD_PID
RET=$?

exit $RET

