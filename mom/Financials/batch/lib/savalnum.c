/* sa_valid_all_numeric(char *is_p, int ii_len) */
/* returns 0 if good, 1 if bad */

#include <ctype.h>

#define SPACES_OK_IN_NUMBER

int sa_valid_all_numeric(char *is_p, int ii_len)
{
#ifdef SPACES_OK_IN_NUMBER
   for( ; ii_len > 0; ii_len--, is_p++)
   {
      if(! isspace(*is_p))
      {
         break;
      } /* end if */
   } /* end for */
#endif

   /* search for non-digit */
   for( ; ii_len > 0; ii_len--, is_p++)
   {
      if( ! isdigit(*is_p))
      {
         break;
      } /* end if */
   } /* end for */

#ifdef SPACES_OK_IN_NUMBER
   for( ; ii_len > 0; ii_len--, is_p++)
   {
      if( ! isspace(*is_p))
      {
         break;
      } /* end if */
   } /* end for */
#endif

   if(ii_len == 0)
   {
      return(0);
   } /* end if */
   else
   {
      return(1);
   } /* end else */
}


