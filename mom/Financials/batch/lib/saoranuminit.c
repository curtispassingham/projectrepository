/* oranuminit.c */

#include <oracle.h>
#include "saoranum.h"

#define MAX_NUM_LEN 50

OCIEnv *oeh;
OCIError *err;

text errbuf[512];  /* size is taken from example in OCI Manual */
sb4 errcode;

int OraNumInit()
{
   char *function = "OraNumInit";
   short li_retcode;

   if ((li_retcode = SQLEnvGet(SQL_SINGLE_RCTX, &oeh)) != OCI_SUCCESS)
   {
      sprintf(err_data, "SQLEnvGet failed with return code %d", li_retcode);
      write_oci_error(function,err_data);
      return(-1);
   }

   if ((li_retcode = OCIHandleAlloc((dvoid *)oeh, (dvoid**)&err,
                                    (ub4)OCI_HTYPE_ERROR, (ub4)0, (dvoid **)0))
       != OCI_SUCCESS)
   {
      sprintf(err_data, "OCIHandleAlloc failed with return code %d", li_retcode);
      write_oci_error(function,err_data);
      return(-1);
   }

   return(0);
}

int OraNumFromString(OCINumber *o_ora_num, char *is_string)
{
   char *function = "OraNumFromString";

   char  ls_fmt_string[MAX_NUM_LEN];
   short li_retcode;
   ub4 l_string_len = strlen(is_string);
   short start = 0; /* this is set to 1 if first character is the sign. */

   /* Check first character of input string. If it 
      is a '+' or a '-', then put S at the beginning
      of the format */
   if ((is_string[0] == '+') || (is_string[0] == '-'))
   {
      start = 1;
      ls_fmt_string[0] = 'S';
   }

   /* Initialize format string to a series of '9'; 
      it has the same length as the input string */
   memset(ls_fmt_string + start,'9',(size_t)l_string_len);
   ls_fmt_string[start + l_string_len] = '\0';

   /* Convert */
   if ((li_retcode = OCINumberFromText(err,
                                       (text *)is_string,
                                       l_string_len,
                                       (text *)ls_fmt_string,
                                       l_string_len,
                                       (text *)0,   /* use default nls_params */
                                       (ub4)0,      /* use default nls_params */
                                       o_ora_num)) != OCI_SUCCESS)
   {
      sprintf(err_data,
              "OCINumberFromText failed with return code %d",
              li_retcode);
      write_oci_error(function,err_data);
      return(-1);
   }

   return(0);
}

int OraNumToString(OCINumber *i_ora_num, char *is_string, ub4 *io_string_len)
{
   char *function = "OraNumToString";
   char ls_fmt_string[MAX_NUM_LEN];
   short li_retcode;
   ub4 l_fmt_len = (*io_string_len) - 2; /* remove null terminator and sign */

   (*io_string_len)--; /* remove space for null terminator */

   /* Initialize format string to a series of '9';
      it is the same length as the output string */
   /* FM: format to remove leading blanks */
   memset(ls_fmt_string,'9',(size_t)l_fmt_len);
   ls_fmt_string[0] = '0';
   ls_fmt_string[l_fmt_len] = '\0';

   /* Convert */
   if ((li_retcode = OCINumberToText(err,
                                     i_ora_num,
                                     (text *)ls_fmt_string,
                                     l_fmt_len,
                                     (text *)0,   /* use default nls_params */
                                     (ub4)0,      /* use default nls_params */
                                     io_string_len,
                                     (text *)is_string)) != OCI_SUCCESS)
   {
      sprintf(err_data,
              "OCINumberToText failed with return code %d",
              li_retcode);
      write_oci_error(function,err_data);
      return(-1);
   }

   return(0);
}

/* Assumption: ios_error_string is large enough */
void write_oci_error(char *is_function, char *ios_error_string)
{
   short li_retcode;

   if ((li_retcode = OCIErrorGet(err, 
                                 (ub4)1, 
                                 (text *)NULL, 
                                 &errcode, 
                                 errbuf, 
                                 sizeof(errbuf), 
                                 (ub4)OCI_HTYPE_ERROR)) != OCI_SUCCESS) 
   {
      sprintf(err_data, "OCIErrorGet failed with return code %d", li_retcode);
      WRITE_ERROR(RET_FUNCTION_ERR,is_function,"",err_data);
   }
   else
   {
      strcat(ios_error_string, ": ");
      strcat(ios_error_string, (char *)errbuf);
      WRITE_ERROR(errcode,is_function,"",ios_error_string);
   }
}


