SET DEFINE OFF;
SET PAGESIZE 32000;
SET LINESIZE 1000;
SET HEADING OFF;
SET FEEDBACK OFF;
SET TERMOUT OFF;
SET TRIMOUT ON;
SET TRIMSPOOL ON;
SET PAUSE OFF;

BREAK ON code_type SKIP 1 ON s SKIP 1;
COLUMN code_type NOPRINT;
COLUMN code_seq NOPRINT;
COLUMN s NOPRINT;
COLUMN np NOPRINT;

SPOOL newh;

SELECT 1 np, '/* This is an automatically generated file. DO NOT EDIT. */'
  FROM DUAL
UNION
SELECT 2 np, '/* To make changes, update the code_head and code_detail tables. */'
  FROM DUAL
UNION
SELECT 3 np, '/* Then run: make -f retek.mk sacodes.c sacodes.h */'
  FROM DUAL
ORDER BY 1;

SELECT 1 np, '#ifndef _CODE_TYPES_H'
  FROM DUAL
UNION
SELECT 2 np, '#define _CODE_TYPES_H'
  FROM DUAL
ORDER BY 1;

SELECT '#include "sastdlen.h"'
  FROM DUAL;

SELECT code_type, -2 code_seq, 1 s, '/* ' || code_type || ' = ' || code_type_desc || ' */'
  FROM code_head
 WHERE code_type NOT IN ('ZZ#P', 'ZZ$P', 'ZZ%P')
UNION
SELECT code_type, -1 code_seq, 1 s, 'extern const char ' || code_type || '[NULL_CODE];'
  FROM code_head
 WHERE code_type NOT IN ('ZZ#P', 'ZZ$P', 'ZZ%P')
UNION
SELECT code_type, code_seq, 2 s, 'extern const char ' || code_type || '_' ||
                                 RPAD( DECODE( code, '-1',     'NEG1',
                                                     '+',      'PLUS',
                                                     '-',      'MINUS',
                                                     '=',      'EQ',
                                                     '<',      'LT',
                                                     '>',      'GT',
                                                     '<=',     'LE',
                                                     '>=',     'GE',
                                                     '!=',     'NE',
                                                     '!NULL',  'NOTNUL',
                                                     '!TOL',   'NOTTOL',
                                                     'is not', 'ISNOT',
                                                     'NOT IN', 'NOTIN',
                                                     'N/B',    'NONBAS',
                                                     'C&I',    'CI',
                                                     'UPC-A',  'UPC_A',
                                                     'UPC-E',  'UPC_E',
                                                     'UPC-AS', 'UPC_AS',
                                                     'UPC-ES', 'UPC_ES', 
                                                     '250+',   'RICH',
                                                     REGEXP_REPLACE(LTRIM(RTRIM(code)),'([^[:alnum:]_])',TO_CHAR(code_seq))), 6, ' ') ||
                                 '[NULL_CODE];   /* ' || code_desc || ' */'
  FROM code_detail
 WHERE code_type NOT IN ('ZZ#P', 'ZZ$P', 'ZZ%P')
UNION
SELECT tender_type_group, tender_type_id, 1 s, 'extern const char ' || tender_type_group || '_' ||
                                 RPAD( tender_type_id, 6, ' ') ||
                                 '[NULL_CODE];   /* ' || tender_type_desc || ' */'
  FROM pos_tender_type_head
UNION
SELECT code_type, code_seq, 3 s, '#define ' || code_type || 'TT_' ||
                                 RPAD( DECODE( code, '-1',     'NEG1',
                                                     '+',      'PLUS',
                                                     '-',      'MINUS',
                                                     '=',      'EQ',
                                                     '<',      'LT',
                                                     '>',      'GT',
                                                     '<=',     'LE',
                                                     '>=',     'GE',
                                                     '!=',     'NE',
                                                     '!NULL',  'NOTNUL',
                                                     '!TOL',   'NOTTOL',
                                                     'is not', 'ISNOT',
                                                     'NOT IN', 'NOTIN',
                                                     'N/B',    'NONBAS',
                                                     'C&I',    'CI',
                                                     'UPC-A',  'UPC_A',
                                                     'UPC-E',  'UPC_E',
                                                     'UPC-AS', 'UPC_AS',
                                                     'UPC-ES', 'UPC_ES', 
                                                     '250+',   'RICH',
                                                     REGEXP_REPLACE(LTRIM(RTRIM(code)),'([^[:alnum:]_])',TO_CHAR(code_seq))), 6, ' ') ||
                                 ' ' || LPAD( code_seq, 3, ' ') || '   /* ' || code_desc || ' */'
  FROM code_detail
 WHERE code_type NOT IN ('ZZ#P', 'ZZ$P', 'ZZ%P')
UNION
SELECT code_type, 0, 4 s, '#define _' || code_type || 'TT_MAX ' ||
                                 MAX( code_seq)
  FROM code_detail
 WHERE code_type NOT IN ('ZZ#P', 'ZZ$P', 'ZZ%P')
GROUP BY code_type
ORDER BY 1, 3, 2;

SELECT '#endif'
  FROM DUAL;

SPOOL OFF;

SPOOL newc;

SELECT 1 np, '/* This is an automatically generated file. DO NOT EDIT. */'
  FROM DUAL
UNION
SELECT 2 np, '/* To make changes, update the code_head and code_detail tables. */'
  FROM DUAL
UNION
SELECT 3 np, '/* Then run: make -f retek.mk sacodes.c sacodes.h */'
  FROM DUAL
ORDER BY 1;

SELECT '#include "sastdlen.h"'
  FROM DUAL;

SELECT code_type, -2 code_seq, 1 s, '/* ' || code_type || ' = ' || code_type_desc || ' */'
  FROM code_head
 WHERE code_type NOT IN ('ZZ#P', 'ZZ$P', 'ZZ%P')
UNION
SELECT code_type, -1 code_seq, 1 s, 'const char ' || code_type || '[NULL_CODE] = "' ||
                                    RPAD( code_type , 4, ' ') || '";'
  FROM code_head
 WHERE code_type NOT IN ('ZZ#P', 'ZZ$P', 'ZZ%P')
UNION
SELECT code_type, code_seq, 2 s, 'const char ' || code_type || '_' ||
                                 RPAD( DECODE( code, '-1',     'NEG1',
                                                     '+',      'PLUS',
                                                     '-',      'MINUS',
                                                     '=',      'EQ',
                                                     '<',      'LT',
                                                     '>',      'GT',
                                                     '<=',     'LE',
                                                     '>=',     'GE',
                                                     '!=',     'NE',
                                                     '!NULL',  'NOTNUL',
                                                     '!TOL',   'NOTTOL',
                                                     'is not', 'ISNOT',
                                                     'NOT IN', 'NOTIN',
                                                     'N/B',    'NONBAS',
                                                     'C&I',    'CI',
                                                     'UPC-A',  'UPC_A',
                                                     'UPC-E',  'UPC_E',
                                                     'UPC-AS', 'UPC_AS',
                                                     'UPC-ES', 'UPC_ES', 
                                                     '250+',   'RICH',
                                                     REGEXP_REPLACE(LTRIM(RTRIM(code)),'([^[:alnum:]_])',TO_CHAR(code_seq))), 6, ' ') ||
                                 '[NULL_CODE] = ' || '"' || code || '";   /* ' || code_desc || ' */'
  FROM code_detail
 WHERE code_type NOT IN ('ZZ#P', 'ZZ$P', 'ZZ%P')
UNION
SELECT tender_type_group, tender_type_id, 1 s, 'const char ' || tender_type_group || '_' ||
                                 RPAD( tender_type_id, 6, ' ') ||
                                 '[NULL_CODE] = ' || '"' || tender_type_id || '";   /* ' || tender_type_desc || ' */'
  FROM pos_tender_type_head
ORDER BY 1, 3, 2;

SPOOL OFF;
QUIT;


