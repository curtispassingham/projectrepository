/*=========================================================================

   samerchcode.c: merchcode load and lookup

=========================================================================*/

#include "sagetref.h"

static MERCHCODEDAT *pa_merchcodetable = NULL;  /* memory-mapped merchcode table */
static size_t pl_merchcodetablelen = 0;   /* number of items in merchcodetable */

/* prototypes */
MERCHCODEDAT *merchcode_lookup(char *is_key);
static int merchcodecmp(const void *is_s1, const void *is_s2);
int merchcode_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
merchcode_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
MERCHCODEDAT *merchcode_lookup(char *is_key)
{
   if (pl_merchcodetablelen == 0)
   {
      return(NULL);
   }

   return((MERCHCODEDAT *)bsearch(
          (const void *)is_key, (const void *)pa_merchcodetable,
           pl_merchcodetablelen, sizeof(MERCHCODEDAT), merchcodecmp));
} /* end merchcode_lookup() */


/*-------------------------------------------------------------------------
merchcodecmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int merchcodecmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_CODE));
} /* end merchcodecmp() */


/*-------------------------------------------------------------------------
merchcode_loadfile() loads the merchcode data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int merchcode_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_merchcodetable = (MERCHCODEDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                                MAP_SHARED, li_datafile, 0 );
            if(pa_merchcodetable != (MERCHCODEDAT *)MAP_FAILED)
            {
               pl_merchcodetablelen = lr_statbuf.st_size / sizeof(MERCHCODEDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end merchcode_loadfile() */


