
/*=========================================================================

   sawh.c: pysical wh load and lookup

=========================================================================*/

#include "sagetref.h"

static WHDAT *pa_whtable = NULL;  /* memory-mapped wh table */
static size_t pl_whtablelen = 0;   /* number of warehouses in whtable */

/* prototypes */
WHDAT *wh_lookup(char *is_key);
static int whcmp(const void *is_s1, const void *is_s2);
int wh_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
wh_lookup() checks the memory-mapped file for the wh and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
WHDAT *wh_lookup(char *is_key)
{
   char ls_key[NULL_LOC];

   if (pl_whtablelen == 0)
   {
      return(NULL);
   }

   sprintf(ls_key,"%-*.*s",LEN_LOC,LEN_LOC,is_key);

   return((WHDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_whtable,
           pl_whtablelen, sizeof(WHDAT), whcmp));
} /* end wh_lookup() */


/*-------------------------------------------------------------------------
whcmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int whcmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_LOC));
} /* end whcmp() */


/*-------------------------------------------------------------------------
wh_loadfile() loads the wh data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int wh_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_whtable = (WHDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                        MAP_SHARED, li_datafile, 0 );
            if(pa_whtable != (WHDAT *)MAP_FAILED)
            {
               pl_whtablelen = lr_statbuf.st_size / sizeof(WHDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end wh_loadfile() */


