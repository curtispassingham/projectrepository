
#ifndef _SAREF_H
#define _SAREF_H

#include <stdlib.h>
#include <strings.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "std_len.h"
#include "sastdlen.h"

#ifndef TRUE
  #define TRUE  1
#endif
#ifndef FALSE
  #define FALSE 0
#endif

/* Define map stuff if not found */
/* Supposed to be in /usr/include/sys/mman.h, but systems vary */
#ifndef MAP_SHARED
   #define MAP_SHARED     1
#endif
#ifndef MAP_FAILED
   #define MAP_FAILED     ((void *)-1)
#endif

/* variable upc field sizes */
#define VUPC_INITDIGITS_SIZE     2    /* initial digits */
#define VUPC_ITEMFIELD_SIZE      5    /* item id */
#define VUPC_VARFIELD_SIZE       5    /* variable data */
#define VUPC_CHKDIGITS_SIZE      1

typedef struct {
   char item[ LEN_ITEM ];
   char dept[ LEN_DEPT ];
   char class[ LEN_CLASS ];
   char subclass[ LEN_SUBCLASS ];
   char standard_uom[ LEN_UOM ];
   char catchweight_ind[ LEN_CATCHWEIGHT_IND ];
   char class_vat_ind[ LEN_IND ];
   char inventory_ind[ LEN_IND ];
   char newline[ LEN_NEWLINE ];
} ITEMDAT;

typedef struct {
   char item[ LEN_ITEM ];
   char waste_type[ LEN_CODE ];
   char waste_pct[ LEN_PCT ];
   char newline[ LEN_NEWLINE ];
} WASTEDAT;

/* REFITEMDAT structure */
typedef struct {
   char refitem[ LEN_ITEM ];
   char item[ LEN_ITEM ];
   char newline[ LEN_NEWLINE ];
} REFITEMDAT;

/* PRIMVARDAT structure */
typedef struct {
   char location[ LEN_LOC ];
   char item[ LEN_ITEM ];
   char primary_variant[ LEN_ITEM ];
   char newline[ LEN_NEWLINE ];
} PRIMVARDAT;

typedef struct {       /* VUPCDAT variable UPC info structure */
   char format_id[ LEN_FORMAT_ID ];
   char format_desc[ LEN_FORMAT_DESC ];
   char prefix_length[ LEN_PREFIX_LEN ];
   char begin_item_digit[ LEN_DIGIT ];
   char begin_var_digit[ LEN_DIGIT ];
   char check_digit[ LEN_DIGIT ];
   char default_prefix[ LEN_PREFIX ];
   char prefix[ LEN_PREFIX ];
   char newline[ LEN_NEWLINE ];
} VUPCDAT;

typedef struct {
   char store[ LEN_STORE ];
   char business_date[ LEN_DATEONLY ];
   char store_day_seq_no[ LEN_BIG_SEQ_NO ];
   char day[ LEN_DAY ];
   char tran_no_generated[ LEN_CODE ];
   char pos_data_expected[ LEN_IND ];
   char currency_rtl_dec[ LEN_CURRENCY_RTL_DEC ];
   char currency_code[ LEN_CURRENCY_CODE ];
   char country_id[ LEN_COUNTRY_ID ];
   char vat_include_ind[ LEN_IND ];
   char tax_level[LEN_CODE];
   char newline[ LEN_NEWLINE ];
} STOREDAYDAT;

typedef struct {
   char promotion[ LEN_PROMOTION ];
   char component[ LEN_PROMO_COMP ];
   char newline[ LEN_NEWLINE ];
} PROMDAT;

typedef struct {
   char code_type[ LEN_CODE_TYPE ];
   char code[ LEN_CODE ];
   char code_seq[ LEN_LITTLE_SEQ_NO ];
   char newline[ LEN_NEWLINE ];
} CODEDAT;

typedef struct {
   char error_code[ LEN_ERROR_CODE ];
   char system_code[ LEN_CODE ];
   char error_desc[ LEN_ERROR_TEXT ];
   char rec_solution[ LEN_ERROR_TEXT ];
   char newline[ LEN_NEWLINE ];
} ERRORDAT;

typedef struct {
   char store[ LEN_STORE ];
   char pos_type[ POST_SIZE ];
   char start_tran_no[ LEN_TRAN_NO ];
   char end_tran_no[ LEN_TRAN_NO ];
   char newline[ LEN_NEWLINE ];
} STOREPOSDAT;

typedef struct {
   char tender_type_group[ TENT_SIZE ];
   char tender_type_id[ LEN_TENDER_TYPE_ID ];
   char tender_type_desc[ LEN_TENDER_TYPE_DESC ];
   char newline[ LEN_NEWLINE ];
} TENDERTYPEDAT;

typedef struct {
   char non_merch_code[ LEN_CODE ];
   char newline[ LEN_NEWLINE ];
} MERCHCODEDAT;

typedef struct {
   char partner_type[ LEN_PARTNER_TYPE ];
   char partner_id[ LEN_PARTNER_ID ];
   char newline[ LEN_NEWLINE ];
} PARTNERDAT;

typedef struct {
   char supplier[ LEN_SUPPLIER ];
   char sup_status[ LEN_STATUS ];
   char newline[ LEN_NEWLINE ];
} SUPPLIERDAT;

typedef struct {
   char store[ LEN_STORE ];
   char pos_id[ LEN_EMP_ID ];
   char emp_id[ LEN_EMP_ID ];
   char newline[ LEN_NEWLINE ];
} EMPLOYEEDAT;

typedef struct {
   char store[ LEN_STORE ];
   char banner_id[ LEN_BANNER_ID ];
   char newline[ LEN_NEWLINE ];
} BANNERDAT;

/* CURRENCYDAT currency code info structure */
typedef struct {
   char currency_code[ LEN_CURRENCY_CODE ];
   char newline[ LEN_NEWLINE ];
} CURRENCYDAT;

/* WHDAT physical warehouse info structure */
typedef struct {
   char wh[ LEN_LOC ];
   char newline[ LEN_NEWLINE ];
} WHDAT;

/* INVSTATUSDAT inv status structure */
typedef struct {
   char inv_status[ LEN_RETURN_DISP ];
   char newline[ LEN_NEWLINE ];
} INVSTATUSDAT;


/* saitem.c */
ITEMDAT *item_lookup( char *key );
int item_loadfile( char *filename );

/* sawaste.c */
WASTEDAT *waste_lookup( char *key );
int waste_loadfile( char *filename );

/* sarefitem.c */
REFITEMDAT *ref_item_lookup( char *key );
int ref_item_loadfile( char *filename );

/* saprimvariant.c */
PRIMVARDAT *prim_variant_lookup( char *_location_, char *_item_ );
int primvariant_loadfile( char *filename );

/* savupc.c */
VUPCDAT *vupc_lookup( char *key );
int vupc_loadfile( char *filename );

/* sastoreday.c */
STOREDAYDAT *storeday_lookup( char *_store_, char *_business_date_ );
int storeday_loadfile( char *filename );

/* saprom.c */
PROMDAT *prom_lookup( char *key );
PROMDAT *prom_comp_lookup( char *promotion, char *promo_comp );
int prom_loadfile( char *filename );

/* sacode.c */
int code_get_seq( const char *code_type, char *code, int len );
CODEDAT *code_lookup( const char *code_type, char *code, int len );
int code_loadfile( char *filename );

/* saerror.c */
ERRORDAT *error_lookup( const char *key );
int error_loadfile( char *filename );

/* sastorepos.c */
STOREPOSDAT *storepos_lookup( char *key );
int storepos_loadfile( char *filename );

/* satendertype.c */
TENDERTYPEDAT *tendertype_lookup( char *group, char *id );
int tendertype_loadfile( char *filename );

/* samerchcode.c */
MERCHCODEDAT *merchcode_lookup( char *key );
int merchcode_loadfile( char *filename );

/* sapartner.c */
PARTNERDAT *partner_lookup( char *key );
int partner_loadfile( char *filename );

/* sasupplier.c */
SUPPLIERDAT *supplier_lookup( char *key );
int supplier_loadfile( char *filename );

/* saemployee.c */
EMPLOYEEDAT *employee_lookup( char *store, char *pos_id );
int employee_loadfile( char *filename );

/* sabanner.c */
BANNERDAT *banner_lookup( char *store, char *banner_id );
BANNERDAT *banner_store_lookup( char *store );
int banner_loadfile( char *filename );

/* sacurrency.c */
CURRENCYDAT *currency_lookup( char *key );
int currency_loadfile( char *filename );

/* sawh.c */
WHDAT *wh_lookup( char *key );
int wh_loadfile( char *filename );

/* sainvstatus.c */
INVSTATUSDAT *invstatus_lookup( char *key );
int invstatus_loadfile( char *filename );

#endif


