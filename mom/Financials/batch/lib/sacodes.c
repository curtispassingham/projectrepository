
/* This is an automatically generated file. DO NOT EDIT. */
/* To make changes, update the code_head and code_detail tables. */
/* Then run: make -f retek.mk sacodes.c sacodes.h */

#include "sastdlen.h"

/* AAIL = Admin API Action List */
const char AAIL[NULL_CODE] = "AAIL";

const char AAIL_D     [NULL_CODE] = "D";   /* Download */
const char AAIL_U     [NULL_CODE] = "U";   /* Upload */


/* AALC = Indicates when the deal component should */
const char AALC[NULL_CODE] = "AALC";

const char AALC_O     [NULL_CODE] = "O";   /* PO approval */
const char AALC_R     [NULL_CODE] = "R";   /* Receiving */


/* AATC = Admin API Template Category */
const char AATC[NULL_CODE] = "AATC";

const char AATC_RSAT  [NULL_CODE] = "RSAT";   /* ReSA */
const char AATC_RMST  [NULL_CODE] = "RMST";   /* RMS */


/* ACRJ = Status (Accepted, Rejected) */
const char ACRJ[NULL_CODE] = "ACRJ";

const char ACRJ_A     [NULL_CODE] = "A";   /* Accepted */
const char ACRJ_R     [NULL_CODE] = "R";   /* Rejected */


/* ACT2 = Mode with view all */
const char ACT2[NULL_CODE] = "ACT2";

const char ACT2_NEW   [NULL_CODE] = "NEW";   /* New */
const char ACT2_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char ACT2_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char ACT2_V_AL  [NULL_CODE] = "V_AL";   /* View All */


/* ACT3 = Item List actions */
const char ACT3[NULL_CODE] = "ACT3";

const char ACT3_NEW   [NULL_CODE] = "NEW";   /* New */
const char ACT3_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char ACT3_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char ACT3_NEWE  [NULL_CODE] = "NEWE";   /* Create from Existing */
const char ACT3_USE   [NULL_CODE] = "USE";   /* Use */


/* ACT4 = Stock Count Actions */
const char ACT4[NULL_CODE] = "ACT4";

const char ACT4_NEW   [NULL_CODE] = "NEW";   /* New */
const char ACT4_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char ACT4_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char ACT4_UVA   [NULL_CODE] = "UVA";   /* Unit Variance and Adjustment */
const char ACT4_DV    [NULL_CODE] = "DV";   /* Dollars View */
const char ACT4_DCLA  [NULL_CODE] = "DCLA";   /* Dollars Change/Ledger Adjust */
const char ACT4_VWHD  [NULL_CODE] = "VWHD";   /* Virtual Warehouse Distribution */


/* ACTN = Mode */
const char ACTN[NULL_CODE] = "ACTN";

const char ACTN_NEW   [NULL_CODE] = "NEW";   /* New */
const char ACTN_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char ACTN_EDIT  [NULL_CODE] = "EDIT";   /* Edit */


/* ACTP = Activity Cost Type */
const char ACTP[NULL_CODE] = "ACTP";

const char ACTP_U     [NULL_CODE] = "U";   /* Update Inventory Cost */
const char ACTP_P     [NULL_CODE] = "P";   /* Post To Financials */


/* ACTT = Action Type */
const char ACTT[NULL_CODE] = "ACTT";

const char ACTT_A     [NULL_CODE] = "A";   /* Add */
const char ACTT_D     [NULL_CODE] = "D";   /* Delete */


/* ACTY = Stock Count Action Type */
const char ACTY[NULL_CODE] = "ACTY";

const char ACTY_R     [NULL_CODE] = "R";   /* Request */
const char ACTY_C     [NULL_CODE] = "C";   /* Count Quantity */


/* ADJT = Receipt Type */
const char ADJT[NULL_CODE] = "ADJT";

const char ADJT_R     [NULL_CODE] = "R";   /* Receipt */
const char ADJT_A     [NULL_CODE] = "A";   /* Adjustment */


/* ADTP = Price Adjustment Type */
const char ADTP[NULL_CODE] = "ADTP";

const char ADTP_PP    [NULL_CODE] = "PP";   /* Price Point */
const char ADTP_RO    [NULL_CODE] = "RO";   /* Round */
const char ADTP_EI    [NULL_CODE] = "EI";   /* Ends In */
const char ADTP_NO    [NULL_CODE] = "NO";   /* None */


/* AGS = OI Report Option Area/Grade/Store */
const char AGS[NULL_CODE] = "AGS ";

const char AGS_A     [NULL_CODE] = "A";   /* Area */
const char AGS_S     [NULL_CODE] = "S";   /* Store */
const char AGS_G     [NULL_CODE] = "G";   /* Grade */


/* AIPT = AIP Case Types */
const char AIPT[NULL_CODE] = "AIPT";

const char AIPT_F     [NULL_CODE] = "F";   /* Formal */
const char AIPT_I     [NULL_CODE] = "I";   /* Informal */


/* AJTY = Type of Adjustment */
const char AJTY[NULL_CODE] = "AJTY";

const char AJTY_P     [NULL_CODE] = "P";   /* Purchase Cost */
const char AJTY_U     [NULL_CODE] = "U";   /* Unit Cost */
const char AJTY_R     [NULL_CODE] = "R";   /* Retail Price */
const char AJTY_M     [NULL_CODE] = "M";   /* Multi-Unit Retail */
const char AJTY_S     [NULL_CODE] = "S";   /* Single Unit Markup % of Retail */


/* ALCG = ALC Status General */
const char ALCG[NULL_CODE] = "ALCG";

const char ALCG_P     [NULL_CODE] = "P";   /* Pending */
const char ALCG_R     [NULL_CODE] = "R";   /* Processed */


/* ALCO = ALC STATUS ALCORDFN */
const char ALCO[NULL_CODE] = "ALCO";

const char ALCO_E     [NULL_CODE] = "E";   /* Estimated */
const char ALCO_N     [NULL_CODE] = "N";   /* No Finalization */
const char ALCO_P     [NULL_CODE] = "P";   /* Pending */
const char ALCO_F     [NULL_CODE] = "F";   /* Processed */
const char ALCO_M     [NULL_CODE] = "M";   /* Processed - Records Pending */


/* ALCS = ALC Status */
const char ALCS[NULL_CODE] = "ALCS";

const char ALCS_E     [NULL_CODE] = "E";   /* Estimated */
const char ALCS_N     [NULL_CODE] = "N";   /* No Finalization */
const char ALCS_P     [NULL_CODE] = "P";   /* Pending */
const char ALCS_PW    [NULL_CODE] = "PW";   /* Processed */


/* ALLL = All stores */
const char ALLL[NULL_CODE] = "ALLL";

const char ALLL_S     [NULL_CODE] = "S";   /* All Stores */


/* ALMT = Allocation Method */
const char ALMT[NULL_CODE] = "ALMT";

const char ALMT_A     [NULL_CODE] = "A";   /* Allocation Quantity */
const char ALMT_P     [NULL_CODE] = "P";   /* Prorate */
const char ALMT_C     [NULL_CODE] = "C";   /* Custom */


/* ALST = Allocation Status */
const char ALST[NULL_CODE] = "ALST";

const char ALST_W     [NULL_CODE] = "W";   /* Worksheet */
const char ALST_A     [NULL_CODE] = "A";   /* Approved */
const char ALST_E     [NULL_CODE] = "E";   /* Extracted */
const char ALST_C     [NULL_CODE] = "C";   /* Closed */
const char ALST_R     [NULL_CODE] = "R";   /* Reserved */


/* ALTY = Allocation Type */
const char ALTY[NULL_CODE] = "ALTY";

const char ALTY_0     [NULL_CODE] = "0";   /* Replenishment */
const char ALTY_1     [NULL_CODE] = "1";   /* Allocation */


/* AMTH = Apply Methods */
const char AMTH[NULL_CODE] = "AMTH";

const char AMTH_A     [NULL_CODE] = "A";   /* Single Assessment */
const char AMTH_H     [NULL_CODE] = "H";   /* All HTS Components */


/* AMTY = Amount Type */
const char AMTY[NULL_CODE] = "AMTY";

const char AMTY_G     [NULL_CODE] = "G";   /* Gross */
const char AMTY_N     [NULL_CODE] = "N";   /* Net */


/* ANST = Analytics (Store or All Stores) list box */
const char ANST[NULL_CODE] = "ANST";

const char ANST_1     [NULL_CODE] = "1";   /* Store */
const char ANST_2     [NULL_CODE] = "2";   /* All Stores */


/* APCC = A&P Constraint Code */
const char APCC[NULL_CODE] = "APCC";

const char APCC_TBD   [NULL_CODE] = "TBD";   /* TBD */


/* APOR = Apromsku Order Indicator */
const char APOR[NULL_CODE] = "APOR";

const char APOR_1     [NULL_CODE] = "1";   /* Best to Worst */
const char APOR_NEG1  [NULL_CODE] = "-1";   /* Worst to Best */


/* APPA = Allowance Accounts payable */
const char APPA[NULL_CODE] = "APPA";

const char APPA_OI    [NULL_CODE] = "OI";   /* Off Invoice */
const char APPA_DM    [NULL_CODE] = "DM";   /* Debit Memo */
const char APPA_IV    [NULL_CODE] = "IV";   /* Invoice */
const char APPA_SP    [NULL_CODE] = "SP";   /* Supplier Auto-Pay */


/* APPB = Bracket Accounts payable */
const char APPB[NULL_CODE] = "APPB";

const char APPB_BB    [NULL_CODE] = "BB";   /* Bill Back */
const char APPB_OI    [NULL_CODE] = "OI";   /* Off Invoice */


/* APPC = All Accounts payable */
const char APPC[NULL_CODE] = "APPC";

const char APPC_BB    [NULL_CODE] = "BB";   /* Bill Back */
const char APPC_OI    [NULL_CODE] = "OI";   /* Off Invoice */
const char APPC_DM    [NULL_CODE] = "DM";   /* Debit Memo */
const char APPC_IV    [NULL_CODE] = "IV";   /* Invoice */
const char APPC_SP    [NULL_CODE] = "SP";   /* Supplier Auto-Pay */


/* APST = A&P Supplier Type */
const char APST[NULL_CODE] = "APST";

const char APST_W     [NULL_CODE] = "W";   /* Warehouse */
const char APST_DSD   [NULL_CODE] = "DSD";   /* Direct Store */
const char APST_VMI   [NULL_CODE] = "VMI";   /* VMI */


/* APTS = Appointment Status */
const char APTS[NULL_CODE] = "APTS";

const char APTS_SC    [NULL_CODE] = "SC";   /* Scheduled */
const char APTS_MS    [NULL_CODE] = "MS";   /* Modified */
const char APTS_AR    [NULL_CODE] = "AR";   /* Arrived */
const char APTS_AC    [NULL_CODE] = "AC";   /* Closed */


/* APVS = A&P VMI Status */
const char APVS[NULL_CODE] = "APVS";

const char APVS_T     [NULL_CODE] = "T";   /* Test */
const char APVS_P     [NULL_CODE] = "P";   /* Production */


/* ARCC = Accepted Rejected Cost Change */
const char ARCC[NULL_CODE] = "ARCC";

const char ARCC_Y     [NULL_CODE] = "Y";   /* Accepted */
const char ARCC_N     [NULL_CODE] = "N";   /* Rejected */


/* ARST = Auto Receive Store */
const char ARST[NULL_CODE] = "ARST";

const char ARST_Y     [NULL_CODE] = "Y";   /* Yes */
const char ARST_N     [NULL_CODE] = "N";   /* No */
const char ARST_D     [NULL_CODE] = "D";   /* System Default */


/* ARUG = ARI User Group Labels */
const char ARUG[NULL_CODE] = "ARUG";

const char ARUG_PMMUG [NULL_CODE] = "PMMUG";   /* Multiple Users/Groups */
const char ARUG_PMMG  [NULL_CODE] = "PMMG";   /* Multiple Groups */
const char ARUG_LKU   [NULL_CODE] = "LKU";   /* Users */
const char ARUG_LKG   [NULL_CODE] = "LKG";   /* Groups */
const char ARUG_LKUG  [NULL_CODE] = "LKUG";   /* Users and Groups */
const char ARUG_LKAG  [NULL_CODE] = "LKAG";   /* Available Groups */
const char ARUG_LKAUG [NULL_CODE] = "LKAUG";   /* Available Users and Groups */
const char ARUG_LKARGU[NULL_CODE] = "LKARGU";   /* Add/Remove Groups to/from User/Group */
const char ARUG_LKARUG[NULL_CODE] = "LKARUG";   /* Add/Remove Users/Groups to/from Group */
const char ARUG_LKGNAM[NULL_CODE] = "LKGNAM";   /* Group Name */
const char ARUG_LKGPTN[NULL_CODE] = "LKGPTN";   /* Parameter Type Name */
const char ARUG_USUID [NULL_CODE] = "USUID";   /* User ID */
const char ARUG_USUNAM[NULL_CODE] = "USUNAM";   /* User Name */
const char ARUG_GPGNAM[NULL_CODE] = "GPGNAM";   /* Group Name */
const char ARUG_GPGTYP[NULL_CODE] = "GPGTYP";   /* Group Type */
const char ARUG_GPPTN [NULL_CODE] = "GPPTN";   /* Parameter Type Name */
const char ARUG_GPEVTG[NULL_CODE] = "GPEVTG";   /* Event Target */


/* ATYP = Activity Type */
const char ATYP[NULL_CODE] = "ATYP";

const char ATYP_A     [NULL_CODE] = "A";   /* Activity */
const char ATYP_T     [NULL_CODE] = "T";   /* Template */


/* BCTP = Barcode type for coupon in POS config */
const char BCTP[NULL_CODE] = "BCTP";

const char BCTP_E     [NULL_CODE] = "E";   /* EAN13 */
const char BCTP_F     [NULL_CODE] = "F";   /* Free Text */


/* BKHL = Code type for backhaul allowances */
const char BKHL[NULL_CODE] = "BKHL";

const char BKHL_C     [NULL_CODE] = "C";   /* Calculated */
const char BKHL_F     [NULL_CODE] = "F";   /* Flat Fee */


/* BLAC = Bill of Lading Action List Box */
const char BLAC[NULL_CODE] = "BLAC";

const char BLAC_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char BLAC_EDIT  [NULL_CODE] = "EDIT";   /* Edit */


/* BLMU = Multiple BOL/AWB */
const char BLMU[NULL_CODE] = "BLMU";

const char BLMU_MULTI [NULL_CODE] = "MULTI";   /* Multiple BOL/AWB */


/* BLST = Bill of Lading Status */
const char BLST[NULL_CODE] = "BLST";

const char BLST_0     [NULL_CODE] = "0";   /* Open */
const char BLST_1     [NULL_CODE] = "1";   /* Closed */


/* BNTP = Button Type */
const char BNTP[NULL_CODE] = "BNTP";

const char BNTP_U     [NULL_CODE] = "U";   /* Multi Selection */
const char BNTP_S     [NULL_CODE] = "S";   /* Single Selection */
const char BNTP_I     [NULL_CODE] = "I";   /* Single Item */
const char BNTP_M     [NULL_CODE] = "M";   /* Major Category */
const char BNTP_N     [NULL_CODE] = "N";   /* Minor Category */
const char BNTP_X     [NULL_CODE] = "X";   /* No Button */


/* BOLB = Blanket Order Date Labels */
const char BOLB[NULL_CODE] = "BOLB";

const char BOLB_SD    [NULL_CODE] = "SD";   /* Start Date */
const char BOLB_SM    [NULL_CODE] = "SM";   /* Start Month */
const char BOLB_ED    [NULL_CODE] = "ED";   /* End Date */
const char BOLB_EM    [NULL_CODE] = "EM";   /* End Month */


/* BOLF = Bill of Lading From Location */
const char BOLF[NULL_CODE] = "BOLF";

const char BOLF_S     [NULL_CODE] = "S";   /* From Store */
const char BOLF_W     [NULL_CODE] = "W";   /* From Warehouse */


/* BOLT = Bill of Lading To Location */
const char BOLT[NULL_CODE] = "BOLT";

const char BOLT_S     [NULL_CODE] = "S";   /* To Store */
const char BOLT_W     [NULL_CODE] = "W";   /* To Warehouse */


/* BOTP = Bond Types */
const char BOTP[NULL_CODE] = "BOTP";

const char BOTP_0     [NULL_CODE] = "0";   /* No bond required */
const char BOTP_8     [NULL_CODE] = "8";   /* Continuous bond */
const char BOTP_9     [NULL_CODE] = "9";   /* Single transaction bond */


/* BTLT = Bill To Loc Type */
const char BTLT[NULL_CODE] = "BTLT";

const char BTLT_M     [NULL_CODE] = "M";   /* Importer */
const char BTLT_X     [NULL_CODE] = "X";   /* Exporter */
const char BTLT_S     [NULL_CODE] = "S";   /* Store */
const char BTLT_W     [NULL_CODE] = "W";   /* Warehouse */
const char BTLT_F     [NULL_CODE] = "F";   /* @SUH4@ */
const char BTLT_C     [NULL_CODE] = "C";   /* Costing Location */


/* CACT = POS Coupon Action Listbox */
const char CACT[NULL_CODE] = "CACT";

const char CACT_NEW   [NULL_CODE] = "NEW";   /* New */
const char CACT_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char CACT_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char CACT_NEWE  [NULL_CODE] = "NEWE";   /* Create from Existing */
const char CACT_RSEQ  [NULL_CODE] = "RSEQ";   /* Resequence Coupons */


/* CALC = Normal/454 */
const char CALC[NULL_CODE] = "CALC";

const char CALC_4     [NULL_CODE] = "4";   /* 454 */
const char CALC_C     [NULL_CODE] = "C";   /* Normal */


const char CASH_1000  [NULL_CODE] = "1000";   /* Cash - primary currency */
const char CASH_1010  [NULL_CODE] = "1010";   /* Cash Alternate Currency */
const char CASH_1020  [NULL_CODE] = "1020";   /* Rounding Tender */


/* CASN = Case and Equivalent Names */
const char CASN[NULL_CODE] = "CASN";

const char CASN_CS    [NULL_CODE] = "CS";   /* Case */
const char CASN_BBL   [NULL_CODE] = "BBL";   /* Barrel */
const char CASN_PO    [NULL_CODE] = "PO";   /* Pot */
const char CASN_CT    [NULL_CODE] = "CT";   /* Carton */
const char CASN_CR    [NULL_CODE] = "CR";   /* Crate */
const char CASN_CON   [NULL_CODE] = "CON";   /* Container */
const char CASN_CA    [NULL_CODE] = "CA";   /* Can */
const char CASN_BX    [NULL_CODE] = "BX";   /* Box */
const char CASN_BK    [NULL_CODE] = "BK";   /* Basket */
const char CASN_BA    [NULL_CODE] = "BA";   /* Barrel */
const char CASN_BE    [NULL_CODE] = "BE";   /* Bundle */
const char CASN_BG    [NULL_CODE] = "BG";   /* Bag */
const char CASN_BJ    [NULL_CODE] = "BJ";   /* Bucket */
const char CASN_BI    [NULL_CODE] = "BI";   /* Bin */
const char CASN_PACK  [NULL_CODE] = "PACK";   /* Pack */


/* CBAS = Calculation Basis */
const char CBAS[NULL_CODE] = "CBAS";

const char CBAS_V     [NULL_CODE] = "V";   /* Value */
const char CBAS_S     [NULL_CODE] = "S";   /* Specific */


const char CCARD_3000  [NULL_CODE] = "3000";   /* Visa */
const char CCARD_3010  [NULL_CODE] = "3010";   /* Mastercard */
const char CCARD_3020  [NULL_CODE] = "3020";   /* American Express */
const char CCARD_3030  [NULL_CODE] = "3030";   /* Discover */
const char CCARD_3040  [NULL_CODE] = "3040";   /* Diners Club - N. America */
const char CCARD_3045  [NULL_CODE] = "3045";   /* Diners Club - Non-N. America */
const char CCARD_3049  [NULL_CODE] = "3049";   /* Diners Club - Ancillary */
const char CCARD_3050  [NULL_CODE] = "3050";   /* WEX */
const char CCARD_3060  [NULL_CODE] = "3060";   /* Voyageur */
const char CCARD_3070  [NULL_CODE] = "3070";   /* Unocal */
const char CCARD_3080  [NULL_CODE] = "3080";   /* enRoute */
const char CCARD_3090  [NULL_CODE] = "3090";   /* Japanese Credit Bureau */
const char CCARD_3100  [NULL_CODE] = "3100";   /* Australian Bank Card */
const char CCARD_3110  [NULL_CODE] = "3110";   /* Carte Blanche - N. America */
const char CCARD_3115  [NULL_CODE] = "3115";   /* Carte Blanche - Non-N. America */
const char CCARD_3120  [NULL_CODE] = "3120";   /* House Card */


/* CCAS = Credit Card Authorization Source */
const char CCAS[NULL_CODE] = "CCAS";

const char CCAS_ERR   [NULL_CODE] = "ERR";   /* Error */
const char CCAS_E     [NULL_CODE] = "E";   /* Electronic */
const char CCAS_M     [NULL_CODE] = "M";   /* Manual */


/* CCEL = Cost Component Exception Log */
const char CCEL[NULL_CODE] = "CCEL";

const char CCEL_ORDS  [NULL_CODE] = "ORDS";   /* Order Shipped */
const char CCEL_ALLOCS[NULL_CODE] = "ALLOCS";   /* Allocation Shipped */
const char CCEL_CEVF  [NULL_CODE] = "CEVF";   /* CE Validations failed */
const char CCEL_ORDU  [NULL_CODE] = "ORDU";   /* Order Updated */
const char CCEL_ALLOCU[NULL_CODE] = "ALLOCU";   /* Allocation Updated */


/* CCEM = Credit Card Entry Mode */
const char CCEM[NULL_CODE] = "CCEM";

const char CCEM_ERR   [NULL_CODE] = "ERR";   /* Error */
const char CCEM_T     [NULL_CODE] = "T";   /* Terminal Used */
const char CCEM_M1R   [NULL_CODE] = "M1R";   /* Magnetic Strip One Read */
const char CCEM_M1T   [NULL_CODE] = "M1T";   /* Magnetic Strip One Transmitted */
const char CCEM_M2R   [NULL_CODE] = "M2R";   /* Magnetic Strip Two Read */
const char CCEM_M2T   [NULL_CODE] = "M2T";   /* Magnetic Strip Two Transmitted */
const char CCEM_MSR   [NULL_CODE] = "MSR";   /* Magnetic Strip Read */


/* CCOR = Cost Change Origin */
const char CCOR[NULL_CODE] = "CCOR";

const char CCOR_ITEM  [NULL_CODE] = "ITEM";   /* By Item */
const char CCOR_SUP   [NULL_CODE] = "SUP";   /* By Supplier */


/* CCSC = Credit Card Special Condition */
const char CCSC[NULL_CODE] = "CCSC";

const char CCSC_ERR   [NULL_CODE] = "ERR";   /* Error */
const char CCSC_M     [NULL_CODE] = "M";   /* Mail */
const char CCSC_P     [NULL_CODE] = "P";   /* Phone */
const char CCSC_E     [NULL_CODE] = "E";   /* Electronic-secured */
const char CCSC_N     [NULL_CODE] = "N";   /* Electronic-non-secured */


/* CCSL = Credit Card Security Level */
const char CCSL[NULL_CODE] = "CCSL";

const char CCSL_N     [NULL_CODE] = "N";   /* None */
const char CCSL_R     [NULL_CODE] = "R";   /* Restricted Access */
const char CCSL_B     [NULL_CODE] = "B";   /* Blocked */


/* CCTP = Cost Component Type */
const char CCTP[NULL_CODE] = "CCTP";


/* CCVF = Credit Card Verification */
const char CCVF[NULL_CODE] = "CCVF";

const char CCVF_ERR   [NULL_CODE] = "ERR";   /* Error */
const char CCVF_S     [NULL_CODE] = "S";   /* Signature Verified */
const char CCVF_C     [NULL_CODE] = "C";   /* Card Shown */
const char CCVF_P     [NULL_CODE] = "P";   /* PIN Entered */
const char CCVF_M     [NULL_CODE] = "M";   /* Mail Order/Phone */
const char CCVF_E     [NULL_CODE] = "E";   /* e-commerce */


/* CCVT = Credit Card Validation Type */
const char CCVT[NULL_CODE] = "CCVT";

const char CCVT_MOD10 [NULL_CODE] = "MOD10";   /* Modulus 10 */
const char CCVT_NONE  [NULL_CODE] = "NONE";   /* No validation */


/* CDIP = Complex Deal Invoice Processing */
const char CDIP[NULL_CODE] = "CDIP";

const char CDIP_AA    [NULL_CODE] = "AA";   /* Automatic All Values */
const char CDIP_MA    [NULL_CODE] = "MA";   /* Manual All Values */
const char CDIP_AP    [NULL_CODE] = "AP";   /* Automatic Positive Values Only */
const char CDIP_MP    [NULL_CODE] = "MP";   /* Manual Positive Values Only */
const char CDIP_NO    [NULL_CODE] = "NO";   /* No Invoice Processing */


/* CEAS = Customs Entry ALC Status */
const char CEAS[NULL_CODE] = "CEAS";

const char CEAS_P     [NULL_CODE] = "P";   /* Pending */
const char CEAS_A     [NULL_CODE] = "A";   /* Allocated */
const char CEAS_R     [NULL_CODE] = "R";   /* Processed */


/* CEPT = Customs Entry Partners */
const char CEPT[NULL_CODE] = "CEPT";

const char CEPT_IA    [NULL_CODE] = "IA";   /* Import Authority */
const char CEPT_BR    [NULL_CODE] = "BR";   /* Broker */


/* CES = Cost Event Status */
const char CES[NULL_CODE] = "CES ";

const char CES_N     [NULL_CODE] = "N";   /* New */
const char CES_C     [NULL_CODE] = "C";   /* Completed */
const char CES_E     [NULL_CODE] = "E";   /* Error */
const char CES_R     [NULL_CODE] = "R";   /* Reprocess */


/* CEST = CE Ref. ID Status */
const char CEST[NULL_CODE] = "CEST";

const char CEST_W     [NULL_CODE] = "W";   /* Worksheet */
const char CEST_S     [NULL_CODE] = "S";   /* Send */
const char CEST_D     [NULL_CODE] = "D";   /* Downloaded */
const char CEST_U     [NULL_CODE] = "U";   /* Confirmed */


/* CFRG = CFAS Record Group Types */
const char CFRG[NULL_CODE] = "CFRG";

const char CFRG_S     [NULL_CODE] = "S";   /* Simple */
const char CFRG_C     [NULL_CODE] = "C";   /* Complex */


/* CFWH = CFAS Where Clause Operators */
const char CFWH[NULL_CODE] = "CFWH";

const char CFWH_EQ    [NULL_CODE] = "=";   /* = */
const char CFWH_NE    [NULL_CODE] = "!=";   /* != */
const char CFWH_GT    [NULL_CODE] = ">";   /* > */
const char CFWH_LT    [NULL_CODE] = "<";   /* < */
const char CFWH_GE    [NULL_CODE] = ">=";   /* >= */
const char CFWH_LE    [NULL_CODE] = "<=";   /* <= */
const char CFWH_NULL  [NULL_CODE] = "NULL";   /* is NULL */
const char CFWH_NOTNUL[NULL_CODE] = "!NULL";   /* is not NULL */


/* CGS = WF Organizational Level */
const char CGS[NULL_CODE] = "CGS ";

const char CGS_A     [NULL_CODE] = "A";   /* @OH3@ */
const char CGS_R     [NULL_CODE] = "R";   /* @OH4@ */
const char CGS_D     [NULL_CODE] = "D";   /* @OH5@ */
const char CGS_LL    [NULL_CODE] = "LL";   /* Location List */
const char CGS_C     [NULL_CODE] = "C";   /* Customer */
const char CGS_G     [NULL_CODE] = "G";   /* Customer Group */
const char CGS_S     [NULL_CODE] = "S";   /* Store */


const char CHECK_2000  [NULL_CODE] = "2000";   /* Personal Check */
const char CHECK_2010  [NULL_CODE] = "2010";   /* Cashier Check */
const char CHECK_2020  [NULL_CODE] = "2020";   /* Traveler Check */
const char CHECK_2030  [NULL_CODE] = "2030";   /* Electronic Check */
const char CHECK_2040  [NULL_CODE] = "2040";   /* Mail Bank Check */
const char CHECK_2050  [NULL_CODE] = "2050";   /* Personal Check Alternate Currency */
const char CHECK_2060  [NULL_CODE] = "2060";   /* Travelers Check Alternate Currency */


/* CHTY = Channel Types */
const char CHTY[NULL_CODE] = "CHTY";

const char CHTY_WEBSTR[NULL_CODE] = "WEBSTR";   /* Webstore */
const char CHTY_BANDM [NULL_CODE] = "BANDM";   /* Brick and Mortar */
const char CHTY_CAT   [NULL_CODE] = "CAT";   /* Catalog */


/* CIDT = Sales Audit Customer ID Types */
const char CIDT[NULL_CODE] = "CIDT";

const char CIDT_ERR   [NULL_CODE] = "ERR";   /* Error */
const char CIDT_TERM  [NULL_CODE] = "TERM";   /* Termination Record */
const char CIDT_PHONE [NULL_CODE] = "PHONE";   /* Phone */
const char CIDT_DL    [NULL_CODE] = "DL";   /* Drivers License */
const char CIDT_BNKACT[NULL_CODE] = "BNKACT";   /* Bank Account */
const char CIDT_SSN   [NULL_CODE] = "SSN";   /* Social Security */
const char CIDT_HSCARD[NULL_CODE] = "HSCARD";   /* House Card */
const char CIDT_VICARD[NULL_CODE] = "VICARD";   /* Visa Card */
const char CIDT_MCCARD[NULL_CODE] = "MCCARD";   /* Mastercard */
const char CIDT_AXCARD[NULL_CODE] = "AXCARD";   /* American Express Card */
const char CIDT_DSCARD[NULL_CODE] = "DSCARD";   /* Discover Card */
const char CIDT_DCCARD[NULL_CODE] = "DCCARD";   /* Diners Club Card */
const char CIDT_ZIP   [NULL_CODE] = "ZIP";   /* Zip Code */
const char CIDT_IDCODE[NULL_CODE] = "IDCODE";   /* Store-defined id code */
const char CIDT_CUSTID[NULL_CODE] = "CUSTID";   /* Customer ID */


/* CIND = Cost Indicator */
const char CIND[NULL_CODE] = "CIND";

const char CIND_BC    [NULL_CODE] = "BC";   /* Base Cost */
const char CIND_NIC   [NULL_CODE] = "NIC";   /* Negotiated Item Cost */


/* CLAS = Class Store */
const char CLAS[NULL_CODE] = "CLAS";

const char CLAS_C     [NULL_CODE] = "C";   /* Class Stores */


/* CLBL = Sales Audit Canvas Labels */
const char CLBL[NULL_CODE] = "CLBL";

const char CLBL_R     [NULL_CODE] = "R";   /* Realms */
const char CLBL_J     [NULL_CODE] = "J";   /* Joins */
const char CLBL_P     [NULL_CODE] = "P";   /* Parameters */
const char CLBL_RE    [NULL_CODE] = "RE";   /* Restrictions */
const char CLBL_LT    [NULL_CODE] = "LT";   /* Location Traits */
const char CLBL_CTD   [NULL_CODE] = "CTD";   /* Combined Total Details */
const char CLBL_RU    [NULL_CODE] = "RU";   /* Roll Ups */
const char CLBL_U     [NULL_CODE] = "U";   /* Usages */
const char CLBL_TO    [NULL_CODE] = "TO";   /* Total Overview */
const char CLBL_TC    [NULL_CODE] = "TC";   /* Total Characteristics */
const char CLBL_TCC   [NULL_CODE] = "TCC";   /* Total Characteristics (cont.) */
const char CLBL_RO    [NULL_CODE] = "RO";   /* Rule Overview */
const char CLBL_RC    [NULL_CODE] = "RC";   /* Rule Characteristics */


/* CLIL = Clearance Item List */
const char CLIL[NULL_CODE] = "CLIL";

const char CLIL_I     [NULL_CODE] = "I";   /* Item */
const char CLIL_IL    [NULL_CODE] = "IL";   /* Item List */


/* CLMD = Calculation Method */
const char CLMD[NULL_CODE] = "CLMD";

const char CLMD_C     [NULL_CODE] = "C";   /* Constant */
const char CLMD_F     [NULL_CODE] = "F";   /* Floating */
const char CLMD_M     [NULL_CODE] = "M";   /* Minimum/Maximum */


/* CLS1 = Clearance Status 1 */
const char CLS1[NULL_CODE] = "CLS1";

const char CLS1_W     [NULL_CODE] = "W";   /* Worksheet */
const char CLS1_S     [NULL_CODE] = "S";   /* Submitted */
const char CLS1_A     [NULL_CODE] = "A";   /* Approved */
const char CLS1_R     [NULL_CODE] = "R";   /* Rejected */
const char CLS1_O     [NULL_CODE] = "O";   /* Complete */
const char CLS1_C     [NULL_CODE] = "C";   /* Cancelled */
const char CLS1_D     [NULL_CODE] = "D";   /* Deleted */


/* CLSF = Item Classification */
const char CLSF[NULL_CODE] = "CLSF";

const char CLSF_A     [NULL_CODE] = "A";   /* A */
const char CLSF_B     [NULL_CODE] = "B";   /* B */
const char CLSF_C     [NULL_CODE] = "C";   /* C */
const char CLSF_D     [NULL_CODE] = "D";   /* D */
const char CLSF_E     [NULL_CODE] = "E";   /* E */


/* CLST = Clearance Event Status */
const char CLST[NULL_CODE] = "CLST";

const char CLST_W     [NULL_CODE] = "W";   /* Worksheet */
const char CLST_S     [NULL_CODE] = "S";   /* Submitted */
const char CLST_A     [NULL_CODE] = "A";   /* Approved */
const char CLST_R     [NULL_CODE] = "R";   /* Rejected */
const char CLST_E     [NULL_CODE] = "E";   /* Extracted */
const char CLST_O     [NULL_CODE] = "O";   /* Completed */
const char CLST_C     [NULL_CODE] = "C";   /* Cancelled */
const char CLST_D     [NULL_CODE] = "D";   /* Deleted */


/* CMPL = Component Type Labels */
const char CMPL[NULL_CODE] = "CMPL";

const char CMPL_E     [NULL_CODE] = "E";   /* Expense Type */
const char CMPL_A     [NULL_CODE] = "A";   /* Assessment Type */


/* CNAL = Contract Find Action List Box */
const char CNAL[NULL_CODE] = "CNAL";

const char CNAL_NEW   [NULL_CODE] = "NEW";   /* New */
const char CNAL_CREATE[NULL_CODE] = "CREATE";   /* Create from Existing */
const char CNAL_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char CNAL_EDIT  [NULL_CODE] = "EDIT";   /* Edit */


/* CNFQ = Consign/Concession POs & invoice level */
const char CNFQ[NULL_CODE] = "CNFQ";

const char CNFQ_I     [NULL_CODE] = "I";   /* Item/Supplier/Location */
const char CNFQ_S     [NULL_CODE] = "S";   /* Supplier/@MH4@ */
const char CNFQ_L     [NULL_CODE] = "L";   /* Supplier/@MH4@/Location */


/* CNIV = Consign/Concession POs & invoices */
const char CNIV[NULL_CODE] = "CNIV";

const char CNIV_M     [NULL_CODE] = "M";   /* Monthly */
const char CNIV_W     [NULL_CODE] = "W";   /* Weekly */
const char CNIV_P     [NULL_CODE] = "P";   /* Multiple */
const char CNIV_D     [NULL_CODE] = "D";   /* Daily */


/* CNLB = CNTRSKU from tables */
const char CNLB[NULL_CODE] = "CNLB";

const char CNLB_C     [NULL_CODE] = "C";   /* Contract Qty */
const char CNLB_QR    [NULL_CODE] = "QR";   /* Qty Recvd */
const char CNLB_QO    [NULL_CODE] = "QO";   /* Qty Ordered */
const char CNLB_U     [NULL_CODE] = "U";   /* Unit Cost */
const char CNLB_P     [NULL_CODE] = "P";   /* Packaging Cost */
const char CNLB_D     [NULL_CODE] = "D";   /* Duty Code */


/* CNLC = CNTRLOCS locations type list box */
const char CNLC[NULL_CODE] = "CNLC";

const char CNLC_C     [NULL_CODE] = "C";   /* Store Class */
const char CNLC_D     [NULL_CODE] = "D";   /* @OH5@ */
const char CNLC_R     [NULL_CODE] = "R";   /* @OH4@ */
const char CNLC_S     [NULL_CODE] = "S";   /* Store */
const char CNLC_W     [NULL_CODE] = "W";   /* Warehouse */
const char CNLC_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char CNLC_L     [NULL_CODE] = "L";   /* Location Trait */
const char CNLC_AS    [NULL_CODE] = "AS";   /* All Stores */


/* CNTP = Contract Type */
const char CNTP[NULL_CODE] = "CNTP";

const char CNTP_A     [NULL_CODE] = "A";   /* Plan / Availability */
const char CNTP_B     [NULL_CODE] = "B";   /* Plan / No Availability */
const char CNTP_C     [NULL_CODE] = "C";   /* No Plan / No Availability */
const char CNTP_D     [NULL_CODE] = "D";   /* No Plan / Availability */


/* CNTX = Context type */
const char CNTX[NULL_CODE] = "CNTX";

const char CNTX_PROM  [NULL_CODE] = "PROM";   /* Promotion */
const char CNTX_WED   [NULL_CODE] = "WED";   /* Customer Transfer */
const char CNTX_STORE [NULL_CODE] = "STORE";   /* Store Requisition */
const char CNTX_REPAIR[NULL_CODE] = "REPAIR";   /* Repairing */


/* COCS = Contract Order Create Status */
const char COCS[NULL_CODE] = "COCS";

const char COCS_B     [NULL_CODE] = "B";   /* Pending */
const char COCS_C     [NULL_CODE] = "C";   /* Created */
const char COCS_G     [NULL_CODE] = "G";   /* Generated */


/* COLR = Color Labels */
const char COLR[NULL_CODE] = "COLR";

const char COLR_C     [NULL_CODE] = "C";   /* Color */
const char COLR_CR    [NULL_CODE] = "CR";   /* Color Range */


/* COLT = Collect Type */
const char COLT[NULL_CODE] = "COLT";

const char COLT_D     [NULL_CODE] = "D";   /* Dates */
const char COLT_M     [NULL_CODE] = "M";   /* Month */
const char COLT_Q     [NULL_CODE] = "Q";   /* Qtr */
const char COLT_A     [NULL_CODE] = "A";   /* Annual */
const char COLT_P     [NULL_CODE] = "P";   /* Perf */


/* COND = Condition Type */
const char COND[NULL_CODE] = "COND";

const char COND_BC    [NULL_CODE] = "BC";   /* Base Cost */
const char COND_EBC   [NULL_CODE] = "EBC";   /* Extended Base Cost */
const char COND_NIC   [NULL_CODE] = "NIC";   /* Negotiated Item Cost */
const char COND_IC    [NULL_CODE] = "IC";   /* Cost Inclusive of All Taxes */


/* CONS = Construction Conditions */
const char CONS[NULL_CODE] = "CONS";

const char CONS_REM   [NULL_CODE] = "REM";   /* Store Remodel */
const char CONS_MAI   [NULL_CODE] = "MAI";   /* Store Maintenance */
const char CONS_PAR   [NULL_CODE] = "PAR";   /* Parking Lot */
const char CONS_SID   [NULL_CODE] = "SID";   /* Sidewalk */
const char CONS_ROA   [NULL_CODE] = "ROA";   /* Connecting Road */
const char CONS_STR   [NULL_CODE] = "STR";   /* Strike */
const char CONS_LAB   [NULL_CODE] = "LAB";   /* Labor Disturbance */


/* CORT = Cost/Retail Flag */
const char CORT[NULL_CODE] = "CORT";

const char CORT_C     [NULL_CODE] = "C";   /* Cost */
const char CORT_R     [NULL_CODE] = "R";   /* Retail */


/* COST = Cost Change Status (plus Extracted) */
const char COST[NULL_CODE] = "COST";

const char COST_W     [NULL_CODE] = "W";   /* Worksheet */
const char COST_S     [NULL_CODE] = "S";   /* Submitted */
const char COST_A     [NULL_CODE] = "A";   /* Approved */
const char COST_R     [NULL_CODE] = "R";   /* Rejected */
const char COST_E     [NULL_CODE] = "E";   /* Extracted */
const char COST_C     [NULL_CODE] = "C";   /* Cancelled */
const char COST_D     [NULL_CODE] = "D";   /* Deleted */


const char COUPON_5000  [NULL_CODE] = "5000";   /* Manufacturers Coupons */


/* CPCT = Coupon Percentage Indicator */
const char CPCT[NULL_CODE] = "CPCT";

const char CPCT_Y     [NULL_CODE] = "Y";   /* Percentage */
const char CPCT_N     [NULL_CODE] = "N";   /* Amount */


/* CPTM = Capture Time */
const char CPTM[NULL_CODE] = "CPTM";

const char CPTM_S     [NULL_CODE] = "S";   /* Sale */
const char CPTM_SR    [NULL_CODE] = "SR";   /* Store Receiving */


/* CPTP = Coupon Report Type */
const char CPTP[NULL_CODE] = "CPTP";

const char CPTP_C     [NULL_CODE] = "C";   /* Coupons and Merchandise Criteria */
const char CPTP_S     [NULL_CODE] = "S";   /* Coupons and Associated Stores */
const char CPTP_I     [NULL_CODE] = "I";   /* Coupons and Associated Items */


/* CRLB = Currency Form Labels */
const char CRLB[NULL_CODE] = "CRLB";

const char CRLB_C     [NULL_CODE] = "C";   /* Consolidation */
const char CRLB_CR    [NULL_CODE] = "CR";   /* Consolidation Rate */
const char CRLB_R     [NULL_CODE] = "R";   /* Rate */


/* CRLL = Cost Relationship Loc List */
const char CRLL[NULL_CODE] = "CRLL";

const char CRLL_A     [NULL_CODE] = "A";   /* @OH3@ */
const char CRLL_R     [NULL_CODE] = "R";   /* @OH4@ */
const char CRLL_D     [NULL_CODE] = "D";   /* @OH5@ */
const char CRLL_C     [NULL_CODE] = "C";   /* Cost Zone Group */
const char CRLL_S     [NULL_CODE] = "S";   /* Store */
const char CRLL_L     [NULL_CODE] = "L";   /* Location List Stores */
const char CRLL_AS    [NULL_CODE] = "AS";   /* All Stores */


/* CRML = Credit Memo Level */
const char CRML[NULL_CODE] = "CRML";

const char CRML_L     [NULL_CODE] = "L";   /* Location */
const char CRML_T     [NULL_CODE] = "T";   /* Transfer Entity */
const char CRML_B     [NULL_CODE] = "B";   /* Set of Books */
const char CRML_D     [NULL_CODE] = "D";   /* Deal */


/* CRSS = Contract Status */
const char CRSS[NULL_CODE] = "CRSS";

const char CRSS_W     [NULL_CODE] = "W";   /* Worksheet */
const char CRSS_S     [NULL_CODE] = "S";   /* Submitted */
const char CRSS_A     [NULL_CODE] = "A";   /* Approved */
const char CRSS_R     [NULL_CODE] = "R";   /* Reviewed */
const char CRSS_C     [NULL_CODE] = "C";   /* Complete */
const char CRSS_X     [NULL_CODE] = "X";   /* Cancelled */


/* CS9T = Cost Change Template Types */
const char CS9T[NULL_CODE] = "CS9T";

const char CS9T_IS9C  [NULL_CODE] = "IS9C";   /* Cost Changes */


/* CSLB = Costsku Form Labels */
const char CSLB[NULL_CODE] = "CSLB";

const char CSLB_R     [NULL_CODE] = "R";   /* (% of Retail) */
const char CSLB_C     [NULL_CODE] = "C";   /* (% of Cost) */


/* CSMT = Contract Shipment Method */
const char CSMT[NULL_CODE] = "CSMT";

const char CSMT_AR    [NULL_CODE] = "AR";   /* Air */
const char CSMT_RD    [NULL_CODE] = "RD";   /* Road */
const char CSMT_SA    [NULL_CODE] = "SA";   /* Sea */
const char CSMT_RL    [NULL_CODE] = "RL";   /* Rail */


/* CST2 = Cost Change Status */
const char CST2[NULL_CODE] = "CST2";

const char CST2_W     [NULL_CODE] = "W";   /* Worksheet */
const char CST2_S     [NULL_CODE] = "S";   /* Submitted */
const char CST2_A     [NULL_CODE] = "A";   /* Approved */
const char CST2_R     [NULL_CODE] = "R";   /* Rejected */
const char CST2_C     [NULL_CODE] = "C";   /* Cancelled */
const char CST2_D     [NULL_CODE] = "D";   /* Deleted */


/* CST3 = Cost Change Status (plus Temp) */
const char CST3[NULL_CODE] = "CST3";

const char CST3_T     [NULL_CODE] = "T";   /* Temporary */
const char CST3_W     [NULL_CODE] = "W";   /* Worksheet */
const char CST3_S     [NULL_CODE] = "S";   /* Submitted */
const char CST3_A     [NULL_CODE] = "A";   /* Approved */
const char CST3_R     [NULL_CODE] = "R";   /* Rejected */
const char CST3_E     [NULL_CODE] = "E";   /* Extracted */
const char CST3_C     [NULL_CODE] = "C";   /* Cancelled */
const char CST3_D     [NULL_CODE] = "D";   /* Deleted */


/* CSTR = Class Stores */
const char CSTR[NULL_CODE] = "CSTR";

const char CSTR_A     [NULL_CODE] = "A";   /* Class Stores A */
const char CSTR_B     [NULL_CODE] = "B";   /* Class Stores B */
const char CSTR_C     [NULL_CODE] = "C";   /* Class Stores C */
const char CSTR_D     [NULL_CODE] = "D";   /* Class Stores D */
const char CSTR_E     [NULL_CODE] = "E";   /* Class Stores E */
const char CSTR_X     [NULL_CODE] = "X";   /* Class Stores X */


/* CSVC = Carrier Service Code */
const char CSVC[NULL_CODE] = "CSVC";

const char CSVC_F     [NULL_CODE] = "F";   /* Free 2 Day Shipping */
const char CSVC_D     [NULL_CODE] = "D";   /* Discounted 4 Day Shipping */
const char CSVC_O     [NULL_CODE] = "O";   /* Overnight Shipping */
const char CSVC_P     [NULL_CODE] = "P";   /* Premium Next Business Day Delivery */
const char CSVC_S     [NULL_CODE] = "S";   /* Standard Shipping */


/* CTYP = Component Type */
const char CTYP[NULL_CODE] = "CTYP";

const char CTYP_E     [NULL_CODE] = "E";   /* Expense */
const char CTYP_A     [NULL_CODE] = "A";   /* Assessment */
const char CTYP_U     [NULL_CODE] = "U";   /* Up Charge */


/* CULB = Currpend form labels */
const char CULB[NULL_CODE] = "CULB";

const char CULB_C     [NULL_CODE] = "C";   /* Consolidation Rate */
const char CULB_E     [NULL_CODE] = "E";   /* Exchange Type */


/* CULT = Customer Location Type */
const char CULT[NULL_CODE] = "CULT";

const char CULT_C     [NULL_CODE] = "C";   /* @OH1@ */
const char CULT_F     [NULL_CODE] = "F";   /* Franchise */


/* CURR = Currency Labels */
const char CURR[NULL_CODE] = "CURR";

const char CURR_PR    [NULL_CODE] = "PR";   /* Prmry Retail */
const char CURR_LC    [NULL_CODE] = "LC";   /* Local Curr */
const char CURR_LR    [NULL_CODE] = "LR";   /* Local Retail */
const char CURR_PC    [NULL_CODE] = "PC";   /* Prmry Cur */


/* CUST = Customer Pickup */
const char CUST[NULL_CODE] = "CUST";

const char CUST_P     [NULL_CODE] = "P";   /* Cust Ord For:  */


/* CUTP = Currency Type */
const char CUTP[NULL_CODE] = "CUTP";

const char CUTP_P     [NULL_CODE] = "P";   /* Primary */
const char CUTP_L     [NULL_CODE] = "L";   /* Local */


/* CVAL = Condition Value */
const char CVAL[NULL_CODE] = "CVAL";

const char CVAL_COVALU[NULL_CODE] = "COVALU";   /* Condition Value */


/* CXTP = Currency Exchange Rate Type */
const char CXTP[NULL_CODE] = "CXTP";

const char CXTP_O     [NULL_CODE] = "O";   /* Operational */
const char CXTP_C     [NULL_CODE] = "C";   /* Consolidation */
const char CXTP_P     [NULL_CODE] = "P";   /* Oracle code type */


/* CYFQ = Cycle Frequency */
const char CYFQ[NULL_CODE] = "CYFQ";

const char CYFQ_O     [NULL_CODE] = "O";   /* Once */
const char CYFQ_R     [NULL_CODE] = "R";   /* Repeating */


/* CZGP = Cost Zone Group Level */
const char CZGP[NULL_CODE] = "CZGP";

const char CZGP_C     [NULL_CODE] = "C";   /* Corporate */
const char CZGP_L     [NULL_CODE] = "L";   /* Location */
const char CZGP_Z     [NULL_CODE] = "Z";   /* Zone */


/* DACJ = Date Conjuctions */
const char DACJ[NULL_CODE] = "DACJ";

const char DACJ_B     [NULL_CODE] = "B";   /* Back To */
const char DACJ_PL    [NULL_CODE] = "PL";   /* Plus Last */
const char DACJ_T     [NULL_CODE] = "T";   /* To */
const char DACJ_P     [NULL_CODE] = "P";   /* Plus */


/* DACO = Damage Code */
const char DACO[NULL_CODE] = "DACO";

const char DACO_S     [NULL_CODE] = "S";   /* Spoilage */
const char DACO_E     [NULL_CODE] = "E";   /* External Damage */
const char DACO_C     [NULL_CODE] = "C";   /* Concealed Damage */


/* DACT = Deal Attribute Code Type */
const char DACT[NULL_CODE] = "DACT";

const char DACT_ACCESS[NULL_CODE] = "ACCESS";   /* Accessorial Costs - Monetary */
const char DACT_HAND  [NULL_CODE] = "HAND";   /* Handling Codes */
const char DACT_ALLOW [NULL_CODE] = "ALLOW";   /* Allowances - Monetary */
const char DACT_OTHER [NULL_CODE] = "OTHER";   /* Other - Monetary */


/* DATY = Store Ship Schedule Date Types */
const char DATY[NULL_CODE] = "DATY";

const char DATY_S     [NULL_CODE] = "S";   /* Specific Date */
const char DATY_E     [NULL_CODE] = "E";   /* Every */
const char DATY_T     [NULL_CODE] = "T";   /* Every Second */
const char DATY_H     [NULL_CODE] = "H";   /* Every Third */
const char DATY_F     [NULL_CODE] = "F";   /* Every Fourth */
const char DATY_A     [NULL_CODE] = "A";   /* All Dates in Range */


/* DAY2 = Date type 2 */
const char DAY2[NULL_CODE] = "DAY2";

const char DAY2_M     [NULL_CODE] = "M";   /* Months */
const char DAY2_W     [NULL_CODE] = "W";   /* Weeks */
const char DAY2_D     [NULL_CODE] = "D";   /* Days */


/* DAYS = Days of the Week */
const char DAYS[NULL_CODE] = "DAYS";

const char DAYS_1     [NULL_CODE] = "1";   /* Sunday */
const char DAYS_2     [NULL_CODE] = "2";   /* Monday */
const char DAYS_3     [NULL_CODE] = "3";   /* Tuesday */
const char DAYS_4     [NULL_CODE] = "4";   /* Wednesday */
const char DAYS_5     [NULL_CODE] = "5";   /* Thursday */
const char DAYS_6     [NULL_CODE] = "6";   /* Friday */
const char DAYS_7     [NULL_CODE] = "7";   /* Saturday */


const char DCARD_8000  [NULL_CODE] = "8000";   /* Debit Card */


/* DCMP = Duty Computation Code */
const char DCMP[NULL_CODE] = "DCMP";

const char DCMP_0     [NULL_CODE] = "0";   /* 0.00 */
const char DCMP_1     [NULL_CODE] = "1";   /* P1*Q1 */
const char DCMP_2     [NULL_CODE] = "2";   /* P1*Q2 */
const char DCMP_3     [NULL_CODE] = "3";   /* (P1*Q1)+(P3*Q2) */
const char DCMP_4     [NULL_CODE] = "4";   /* (P1*Q1)+(P2*Value) */
const char DCMP_5     [NULL_CODE] = "5";   /* (P1*Q2)+(P2*Value) */
const char DCMP_6     [NULL_CODE] = "6";   /* (P1*Q2)+(P3*Q2)+(P2*Value) */
const char DCMP_7     [NULL_CODE] = "7";   /* P2*Value */
const char DCMP_9     [NULL_CODE] = "9";   /* P2*Derived Duty */
const char DCMP_A     [NULL_CODE] = "A";   /* (P2+P3*Q3)*Value */
const char DCMP_B     [NULL_CODE] = "B";   /* (P1*Q2)+(P2+P3*Q3)*Value */
const char DCMP_C     [NULL_CODE] = "C";   /* (P1*Q1) or (P2*Q1) */
const char DCMP_D     [NULL_CODE] = "D";   /* (P1*Q3)+(P2*Value) */
const char DCMP_E     [NULL_CODE] = "E";   /* (P1*Q3)+(P2*Value) */
const char DCMP_F     [NULL_CODE] = "F";   /* Q1*(P1+P3*Q2) */
const char DCMP_J     [NULL_CODE] = "J";   /* Greater of Q2*(P1-P2*(100-Q3)) or P3*Q2 */
const char DCMP_K     [NULL_CODE] = "K";   /* Greater of Q1*(P1-P2*(100-Q2)) or P3*Q1 */
const char DCMP_X     [NULL_CODE] = "X";   /* No computation formula available */


/* DCTX = Document Text */
const char DCTX[NULL_CODE] = "DCTX";


/* DEFT = Nautilus Default Values */
const char DEFT[NULL_CODE] = "DEFT";

const char DEFT_DCC   [NULL_CODE] = "DCC";   /* Default Carrier Code */
const char DEFT_DSC   [NULL_CODE] = "DSC";   /* Default Service Code */
const char DEFT_DR    [NULL_CODE] = "DR";   /* Default Route Code */
const char DEFT_DATE  [NULL_CODE] = "DATE";   /* 30 */


/* DELB = Delete in Nightly Batch Message */
const char DELB[NULL_CODE] = "DELB";

const char DELB_PC    [NULL_CODE] = "PC";   /* Price Change */


/* DELT = Customer Delivery Type */
const char DELT[NULL_CODE] = "DELT";

const char DELT_S     [NULL_CODE] = "S";   /* Ship Direct */
const char DELT_C     [NULL_CODE] = "C";   /* Customer Pickup */


/* DEMG = Sales Audit Demographic */
const char DEMG[NULL_CODE] = "DEMG";

const char DEMG_FC    [NULL_CODE] = "FC";   /* Flower Child */
const char DEMG_SM    [NULL_CODE] = "SM";   /* Soccer Mom */
const char DEMG_BB    [NULL_CODE] = "BB";   /* Baby Boomer */
const char DEMG_000   [NULL_CODE] = "000";   /* Error */


/* DEPO = Deposit Code */
const char DEPO[NULL_CODE] = "DEPO";

const char DEPO_NONE  [NULL_CODE] = "NONE";   /* None */
const char DEPO_MA    [NULL_CODE] = "MA";   /* Massachusetts - 0.05 */
const char DEPO_ME1   [NULL_CODE] = "ME1";   /* Maine - 0.05 */
const char DEPO_ME2   [NULL_CODE] = "ME2";   /* Maine - 0.10 */


/* DEST = Reports Destination */
const char DEST[NULL_CODE] = "DEST";

const char DEST_V     [NULL_CODE] = "V";   /* Preview */
const char DEST_S     [NULL_CODE] = "S";   /* Screen */
const char DEST_F     [NULL_CODE] = "F";   /* File */
const char DEST_P     [NULL_CODE] = "P";   /* Printer */
const char DEST_M     [NULL_CODE] = "M";   /* Mail */


/* DETP = Delete Type */
const char DETP[NULL_CODE] = "DETP";

const char DETP_D     [NULL_CODE] = "D";   /* Delete */
const char DETP_C     [NULL_CODE] = "C";   /* Cancel */


/* DFDL = Active Date and Close Date labels */
const char DFDL[NULL_CODE] = "DFDL";

const char DFDL_AND   [NULL_CODE] = "AND";   /* and */
const char DFDL_SDBET [NULL_CODE] = "SDBET";   /* Start Date Between */
const char DFDL_EDBET [NULL_CODE] = "EDBET";   /* End Date Between */
const char DFDL_SD    [NULL_CODE] = "SD";   /* Start Date */
const char DFDL_ED    [NULL_CODE] = "ED";   /* End Date */


/* DFTY = Diff Apply Types */
const char DFTY[NULL_CODE] = "DFTY";

const char DFTY_D     [NULL_CODE] = "D";   /* Diff ID */
const char DFTY_R     [NULL_CODE] = "R";   /* Diff Range */
const char DFTY_T     [NULL_CODE] = "T";   /* Diff Template */


/* DIFF = Diff Types */
const char DIFF[NULL_CODE] = "DIFF";

const char DIFF_S     [NULL_CODE] = "S";   /* Size */
const char DIFF_C     [NULL_CODE] = "C";   /* Color */
const char DIFF_F     [NULL_CODE] = "F";   /* Flavor */
const char DIFF_E     [NULL_CODE] = "E";   /* Scent */
const char DIFF_P     [NULL_CODE] = "P";   /* Pattern */


/* DIFT = Differential Group/ID Types */
const char DIFT[NULL_CODE] = "DIFT";

const char DIFT_ID    [NULL_CODE] = "ID";   /* ID */
const char DIFT_GROUP [NULL_CODE] = "GROUP";   /* Group */


/* DIML = Deal Component Merchandise Level */
const char DIML[NULL_CODE] = "DIML";

const char DIML_1     [NULL_CODE] = "1";   /* @OH1@ */
const char DIML_2     [NULL_CODE] = "2";   /* @MH2@ */
const char DIML_3     [NULL_CODE] = "3";   /* @MH3@ */
const char DIML_4     [NULL_CODE] = "4";   /* @MH4@ */
const char DIML_5     [NULL_CODE] = "5";   /* @MH5@ */
const char DIML_6     [NULL_CODE] = "6";   /* @MH6@ */
const char DIML_7     [NULL_CODE] = "7";   /* Item Parent */
const char DIML_8     [NULL_CODE] = "8";   /* Item Parent - Diff 1 */
const char DIML_9     [NULL_CODE] = "9";   /* Item Parent - Diff 2 */
const char DIML_10    [NULL_CODE] = "10";   /* Item Parent - Diff 3 */
const char DIML_11    [NULL_CODE] = "11";   /* Item Parent - Diff 4 */
const char DIML_12    [NULL_CODE] = "12";   /* Item */


/* DIMO = Dimmension Object */
const char DIMO[NULL_CODE] = "DIMO";

const char DIMO_EA    [NULL_CODE] = "EA";   /* Each */
const char DIMO_IN    [NULL_CODE] = "IN";   /* Inner */
const char DIMO_CA    [NULL_CODE] = "CA";   /* Case */
const char DIMO_PA    [NULL_CODE] = "PA";   /* Pallet */


/* DIMS = Dimension Object-Simple Pack Setup */
const char DIMS[NULL_CODE] = "DIMS";

const char DIMS_CA    [NULL_CODE] = "CA";   /* Case */
const char DIMS_PA    [NULL_CODE] = "PA";   /* Pallet */


/* DIOL = Deal Component Org. Level */
const char DIOL[NULL_CODE] = "DIOL";

const char DIOL_1     [NULL_CODE] = "1";   /* @OH2@ */
const char DIOL_2     [NULL_CODE] = "2";   /* @OH3@ */
const char DIOL_3     [NULL_CODE] = "3";   /* @OH4@ */
const char DIOL_4     [NULL_CODE] = "4";   /* @OH5@ */
const char DIOL_5     [NULL_CODE] = "5";   /* Location */


/* DISC = Discrepancies */
const char DISC[NULL_CODE] = "DISC";

const char DISC_O     [NULL_CODE] = "O";   /* Overage */
const char DISC_S     [NULL_CODE] = "S";   /* Shortage */


/* DISD = Discrepancy Display */
const char DISD[NULL_CODE] = "DISD";

const char DISD_DESC  [NULL_CODE] = "DESC";   /* Descriptive */
const char DISD_PCNT  [NULL_CODE] = "PCNT";   /* Percentage */
const char DISD_ABSO  [NULL_CODE] = "ABSO";   /* Absolute */


/* DIST = Discount Type */
const char DIST[NULL_CODE] = "DIST";

const char DIST_P     [NULL_CODE] = "P";   /* % */
const char DIST_A     [NULL_CODE] = "A";   /* Amt */
const char DIST_U     [NULL_CODE] = "U";   /* Unit */


/* DITM = Deposit Price Per UOM */
const char DITM[NULL_CODE] = "DITM";

const char DITM_I     [NULL_CODE] = "I";   /* Inclusive of Deposit Amount */
const char DITM_E     [NULL_CODE] = "E";   /* Exclusive of Deposit Amount */


/* DLAP = Deal Age Priority Levels */
const char DLAP[NULL_CODE] = "DLAP";

const char DLAP_O     [NULL_CODE] = "O";   /* Oldest */
const char DLAP_N     [NULL_CODE] = "N";   /* Newest */


/* DLAT = Identifies whether thresholds have been */
const char DLAT[NULL_CODE] = "DLAT";

const char DLAT_I     [NULL_CODE] = "I";   /* Insert */
const char DLAT_U     [NULL_CODE] = "U";   /* Update */
const char DLAT_D     [NULL_CODE] = "D";   /* Delete */


/* DLBM = Deal Billing Method */
const char DLBM[NULL_CODE] = "DLBM";

const char DLBM_C     [NULL_CODE] = "C";   /* Credit Note */
const char DLBM_D     [NULL_CODE] = "D";   /* Debit Note */


/* DLBP = Code that identifies the bill-back perio */
const char DLBP[NULL_CODE] = "DLBP";

const char DLBP_W     [NULL_CODE] = "W";   /* Week */
const char DLBP_M     [NULL_CODE] = "M";   /* Month */
const char DLBP_Q     [NULL_CODE] = "Q";   /* Quarter */
const char DLBP_H     [NULL_CODE] = "H";   /* Half Year */
const char DLBP_A     [NULL_CODE] = "A";   /* Annual */


/* DLBT = Billing type of the deal component. */
const char DLBT[NULL_CODE] = "DLBT";

const char DLBT_OI    [NULL_CODE] = "OI";   /* Off-invoice */
const char DLBT_BB    [NULL_CODE] = "BB";   /* Bill Back */
const char DLBT_VFP   [NULL_CODE] = "VFP";   /* Vendor Funded Promotion */
const char DLBT_VFM   [NULL_CODE] = "VFM";   /* Vendor Funded Markdown */
const char DLBT_BBR   [NULL_CODE] = "BBR";   /* Bill Back Rebate */


/* DLCA = Deal Component Cost Application Buckets */
const char DLCA[NULL_CODE] = "DLCA";

const char DLCA_N     [NULL_CODE] = "N";   /* Net Cost */
const char DLCA_NN    [NULL_CODE] = "NN";   /* Net Net Cost */
const char DLCA_DNN   [NULL_CODE] = "DNN";   /* Dead Net Cost */


/* DLCL = Deal Component Classes */
const char DLCL[NULL_CODE] = "DLCL";

const char DLCL_CU    [NULL_CODE] = "CU";   /* Cumulative - discs summed then applied */
const char DLCL_CS    [NULL_CODE] = "CS";   /* Cascade - discs taken one at a time */
const char DLCL_EX    [NULL_CODE] = "EX";   /* Exclusive - overrides other discounts */


/* DLCT = Indicates if the rebate should be calcul */
const char DLCT[NULL_CODE] = "DLCT";

const char DLCT_L     [NULL_CODE] = "L";   /* Linear */
const char DLCT_S     [NULL_CODE] = "S";   /* Scalar */


/* DLH2 = Deal Types */
const char DLH2[NULL_CODE] = "DLH2";

const char DLH2_A     [NULL_CODE] = "A";   /* Annual */
const char DLH2_P     [NULL_CODE] = "P";   /* Promotional */


/* DLHC = Deal Types for Create from existing */
const char DLHC[NULL_CODE] = "DLHC";

const char DLHC_A     [NULL_CODE] = "A";   /* Annual */
const char DLHC_P     [NULL_CODE] = "P";   /* Promotional */
const char DLHC_M     [NULL_CODE] = "M";   /* Vendor funded markdown */


/* DLHS = Deal Statuses */
const char DLHS[NULL_CODE] = "DLHS";

const char DLHS_W     [NULL_CODE] = "W";   /* Worksheet */
const char DLHS_S     [NULL_CODE] = "S";   /* Submitted */
const char DLHS_A     [NULL_CODE] = "A";   /* Approved */
const char DLHS_R     [NULL_CODE] = "R";   /* Rejected */
const char DLHS_C     [NULL_CODE] = "C";   /* Closed */


/* DLHT = Deal Types */
const char DLHT[NULL_CODE] = "DLHT";

const char DLHT_A     [NULL_CODE] = "A";   /* Annual */
const char DLHT_P     [NULL_CODE] = "P";   /* Promotional */
const char DLHT_O     [NULL_CODE] = "O";   /* PO-Specific */
const char DLHT_M     [NULL_CODE] = "M";   /* Vendor funded markdown */


/* DLIC = Deal Income Calculation */
const char DLIC[NULL_CODE] = "DLIC";

const char DLIC_A     [NULL_CODE] = "A";   /* Actuals Earned to date */
const char DLIC_P     [NULL_CODE] = "P";   /* Pro-rated using forecast */


/* DLIP = Deal Invoice Processing */
const char DLIP[NULL_CODE] = "DLIP";

const char DLIP_AA    [NULL_CODE] = "AA";   /* Automatic */
const char DLIP_MA    [NULL_CODE] = "MA";   /* Manual */
const char DLIP_NO    [NULL_CODE] = "NO";   /* No Invoice Processing */


/* DLL2 = Deal Component Value Types */
const char DLL2[NULL_CODE] = "DLL2";

const char DLL2_Q     [NULL_CODE] = "Q";   /* Buy/Get Free or Disc Items */
const char DLL2_A     [NULL_CODE] = "A";   /* Amount off */
const char DLL2_P     [NULL_CODE] = "P";   /* Percent off per unit */
const char DLL2_F     [NULL_CODE] = "F";   /* Get units for fixed price */


/* DLL3 = Tran Disc Deal Component Value Types */
const char DLL3[NULL_CODE] = "DLL3";

const char DLL3_A     [NULL_CODE] = "A";   /* Amount off per order */
const char DLL3_P     [NULL_CODE] = "P";   /* Percent off per order */


/* DLLD = Tran Disc Deal Component Thresh Limits */
const char DLLD[NULL_CODE] = "DLLD";

const char DLLD_A     [NULL_CODE] = "A";   /* Amount - total cost of orders */


/* DLLT = Deal Component Threshold Limits */
const char DLLT[NULL_CODE] = "DLLT";

const char DLLT_Q     [NULL_CODE] = "Q";   /* Quantity - number of Units */
const char DLLT_A     [NULL_CODE] = "A";   /* Amount - total value of units */


/* DLPB = Deal Performance Basis Actuals Indicator */
const char DLPB[NULL_CODE] = "DLPB";

const char DLPB_A     [NULL_CODE] = "A";   /* Yes */
const char DLPB_F     [NULL_CODE] = "F";   /* No */


/* DLPT = Deal Type Priority Levels */
const char DLPT[NULL_CODE] = "DLPT";

const char DLPT_A     [NULL_CODE] = "A";   /* Annual */
const char DLPT_P     [NULL_CODE] = "P";   /* Promotional */


/* DLRL = Deal Reporting Level */
const char DLRL[NULL_CODE] = "DLRL";

const char DLRL_W     [NULL_CODE] = "W";   /* Week */
const char DLRL_M     [NULL_CODE] = "M";   /* Month */
const char DLRL_Q     [NULL_CODE] = "Q";   /* Quarter */


/* DLRP = Rebate Application Types */
const char DLRP[NULL_CODE] = "DLRP";

const char DLRP_P     [NULL_CODE] = "P";   /* Purchases */
const char DLRP_S     [NULL_CODE] = "S";   /* Sales */


/* DLTP = Deal Types */
const char DLTP[NULL_CODE] = "DLTP";

const char DLTP_DEAL  [NULL_CODE] = "DEAL";   /* Deal */
const char DLTP_SKU   [NULL_CODE] = "SKU";   /* Item Bracket */
const char DLTP_SUPP  [NULL_CODE] = "SUPP";   /* Supplier Bracket */
const char DLTP_SKUA  [NULL_CODE] = "SKUA";   /* Item Allowance */
const char DLTP_SUPA  [NULL_CODE] = "SUPA";   /* Supplier Allowance */
const char DLTP_MCHA  [NULL_CODE] = "MCHA";   /* Hierarchy Allowance */


/* DLTT = Values for Deal Total Indicator */
const char DLTT[NULL_CODE] = "DLTT";

const char DLTT_Y     [NULL_CODE] = "Y";   /* Total */
const char DLTT_N     [NULL_CODE] = "N";   /* Unit */


/* DLVY = Delivery Policy */
const char DLVY[NULL_CODE] = "DLVY";

const char DLVY_NEXT  [NULL_CODE] = "NEXT";   /* Next Day */
const char DLVY_NDD   [NULL_CODE] = "NDD";   /* Next Valid Delivery Day */


/* DOCT = Document Type */
const char DOCT[NULL_CODE] = "DOCT";

const char DOCT_REQ   [NULL_CODE] = "REQ";   /* Required Documents */
const char DOCT_CS    [NULL_CODE] = "CS";   /* Charges */
const char DOCT_BI    [NULL_CODE] = "BI";   /* Bank Instructions */
const char DOCT_SI    [NULL_CODE] = "SI";   /* Sender Instructions */
const char DOCT_AI    [NULL_CODE] = "AI";   /* Additional Instructions */


/* DOHT = Distribution On Hand Type */
const char DOHT[NULL_CODE] = "DOHT";

const char DOHT_NOLKUP[NULL_CODE] = "NOLKUP";   /* No Look Up - Use Supplied Value */
const char DOHT_LKSOH [NULL_CODE] = "LKSOH";   /* Look Up Stock On Hand */
const char DOHT_LKNOH [NULL_CODE] = "LKNOH";   /* Look Up Net Stock On Hand */


/* DOL = Due Order Level */
const char DOL[NULL_CODE] = "DOL ";

const char DOL_I     [NULL_CODE] = "I";   /* Item Level */
const char DOL_O     [NULL_CODE] = "O";   /* Purchase Order Level */


/* DOOR = Default Door Type Indicator */
const char DOOR[NULL_CODE] = "DOOR";

const char DOOR_A     [NULL_CODE] = "A";   /* Default Door Type Indicator */


/* DOSB = Due Order Service Basis */
const char DOSB[NULL_CODE] = "DOSB";

const char DOSB_C     [NULL_CODE] = "C";   /* Cost */
const char DOSB_U     [NULL_CODE] = "U";   /* Units */
const char DOSB_P     [NULL_CODE] = "P";   /* Profit */


/* DQGT = Deal Quantity Thresh. Get Types */
const char DQGT[NULL_CODE] = "DQGT";

const char DQGT_X     [NULL_CODE] = "X";   /* Free - get units free */
const char DQGT_P     [NULL_CODE] = "P";   /* Percent Off - get units at % off cost */
const char DQGT_A     [NULL_CODE] = "A";   /* Amount - get units at amount off cost */
const char DQGT_F     [NULL_CODE] = "F";   /* Fixed Amount - get units for fixed price */


/* DQT2 = Deal Quantity Threshold Get Types */
const char DQT2[NULL_CODE] = "DQT2";

const char DQT2_P     [NULL_CODE] = "P";   /* percent off */
const char DQT2_A     [NULL_CODE] = "A";   /* amount */
const char DQT2_F     [NULL_CODE] = "F";   /* fixed amount */


const char DRIVEO_9000  [NULL_CODE] = "9000";   /* Fuel Drive Off */


/* DRUL = Distribution Rules */
const char DRUL[NULL_CODE] = "DRUL";

const char DRUL_PRORAT[NULL_CODE] = "PRORAT";   /* Proration */
const char DRUL_MN2MX [NULL_CODE] = "MN2MX";   /* Minimum to maximum */
const char DRUL_MX2MN [NULL_CODE] = "MX2MN";   /* Maximum to minimum */


/* DSTK = Distribution Keys */
const char DSTK[NULL_CODE] = "DSTK";

const char DSTK_ON    [NULL_CODE] = "ON";   /* Order No */
const char DSTK_PTI   [NULL_CODE] = "PTI";   /* Pack Template ID */
const char DSTK_ST    [NULL_CODE] = "ST";   /* Item Parent */
const char DSTK_CN    [NULL_CODE] = "CN";   /* Contract No */


/* DSTR = Distro type */
const char DSTR[NULL_CODE] = "DSTR";

const char DSTR_T     [NULL_CODE] = "T";   /* Transfer */
const char DSTR_A     [NULL_CODE] = "A";   /* Allocation */


/* DSTT = Distribution Types */
const char DSTT[NULL_CODE] = "DSTT";

const char DSTT_DP    [NULL_CODE] = "DP";   /* Distribution % */
const char DSTT_QT    [NULL_CODE] = "QT";   /* Quantity */
const char DSTT_RA    [NULL_CODE] = "RA";   /* Ratio */
const char DSTT_TL    [NULL_CODE] = "TL";   /* Total */
const char DSTT_QS    [NULL_CODE] = "QS";   /* Qty. per Store */


/* DTAX = Default Tax Types */
const char DTAX[NULL_CODE] = "DTAX";

const char DTAX_SVAT  [NULL_CODE] = "SVAT";   /* Simple VAT */
const char DTAX_SALES [NULL_CODE] = "SALES";   /* Sales and Use Tax */
const char DTAX_GTAX  [NULL_CODE] = "GTAX";   /* Global Tax */


/* DTTP = Sales Audit Data Types */
const char DTTP[NULL_CODE] = "DTTP";

const char DTTP_C     [NULL_CODE] = "C";   /* Character */
const char DTTP_N     [NULL_CODE] = "N";   /* Number */
const char DTTP_D     [NULL_CODE] = "D";   /* Date */


/* DTYP = Distribution Type */
const char DTYP[NULL_CODE] = "DTYP";

const char DTYP_ININV [NULL_CODE] = "ININV";   /* Into Inventory */
const char DTYP_OTINV [NULL_CODE] = "OTINV";   /* Out of Inventory */


/* DTYS = Duty Status */
const char DTYS[NULL_CODE] = "DTYS";

const char DTYS_W     [NULL_CODE] = "W";   /* Worksheet */
const char DTYS_A     [NULL_CODE] = "A";   /* Approved */


/* EALT = Expense */
const char EALT[NULL_CODE] = "EALT";

const char EALT_A     [NULL_CODE] = "A";   /* Amount */
const char EALT_U     [NULL_CODE] = "U";   /* Unit of Measure */


/* ECCR = EDI Cost Change Reason */
const char ECCR[NULL_CODE] = "ECCR";

const char ECCR_A     [NULL_CODE] = "A";   /* Accepted */
const char ECCR_R     [NULL_CODE] = "R";   /* Rejected */
const char ECCR_N     [NULL_CODE] = "N";   /* Not Reviewed */
const char ECCR_F     [NULL_CODE] = "F";   /* Failed */


/* EDBG = EDI Message Types */
const char EDBG[NULL_CODE] = "EDBG";

const char EDBG_F     [NULL_CODE] = "F";   /* Failure */
const char EDBG_W     [NULL_CODE] = "W";   /* Warning */


/* EDCT = EDI Code Type */
const char EDCT[NULL_CODE] = "EDCT";

const char EDCT_AI    [NULL_CODE] = "AI";   /* Add Item(s) */
const char EDCT_DI    [NULL_CODE] = "DI";   /* Delete Item(s) */
const char EDCT_PC    [NULL_CODE] = "PC";   /* Price Change */
const char EDCT_QD    [NULL_CODE] = "QD";   /* Quantity Decrease */
const char EDCT_QI    [NULL_CODE] = "QI";   /* Quantity Increase */


/* EDIO = EDI Order Indicator */
const char EDIO[NULL_CODE] = "EDIO";

const char EDIO_Y     [NULL_CODE] = "Y";   /* Submit via EDI */
const char EDIO_N     [NULL_CODE] = "N";   /* Do not use EDI */
const char EDIO_S     [NULL_CODE] = "S";   /* Order has already been sent via EDI */


/* EDST = EDI Status */
const char EDST[NULL_CODE] = "EDST";

const char EDST_C     [NULL_CODE] = "C";   /* Changed */
const char EDST_N     [NULL_CODE] = "N";   /* Not Changed */
const char EDST_D     [NULL_CODE] = "D";   /* Deleted */


/* EHAC = Event History Action Codes */
const char EHAC[NULL_CODE] = "EHAC";

const char EHAC_URE   [NULL_CODE] = "URE";   /* User Requested Evaluation */
const char EHAC_PREA  [NULL_CODE] = "PREA";   /* Pre-Action Evaluation */
const char EHAC_PSTA  [NULL_CODE] = "PSTA";   /* Post-Action Evaluation */
const char EHAC_AAE   [NULL_CODE] = "AAE";   /* Auto-Action Executed */
const char EHAC_MAE   [NULL_CODE] = "MAE";   /* Manual Action Executed */
const char EHAC_SE    [NULL_CODE] = "SE";   /* Scheduled Re-evaluation */
const char EHAC_XCRE  [NULL_CODE] = "XCRE";   /* Exception Creation Evaluation */
const char EHAC_ADDXE [NULL_CODE] = "ADDXE";   /* Additional Exception Re-evaluation */


/* EMPT = Employee Type */
const char EMPT[NULL_CODE] = "EMPT";

const char EMPT_S     [NULL_CODE] = "S";   /* Store */
const char EMPT_H     [NULL_CODE] = "H";   /* Headquarters */


/* ESDF = EDI Sales Download Frequency */
const char ESDF[NULL_CODE] = "ESDF";

const char ESDF_D     [NULL_CODE] = "D";   /* Daily */
const char ESDF_W     [NULL_CODE] = "W";   /* Weekly */


/* ESLV = Escheatment Level */
const char ESLV[NULL_CODE] = "ESLV";

const char ESLV_C     [NULL_CODE] = "C";   /* Country */
const char ESLV_S     [NULL_CODE] = "S";   /* State */


/* EVIM = Event Informational Messages */
const char EVIM[NULL_CODE] = "EVIM";

const char EVIM_U     [NULL_CODE] = "U";   /* Unable to refresh parameters */
const char EVIM_L     [NULL_CODE] = "L";   /* Event Reevaluation Process Loop Exceeded */
const char EVIM_A     [NULL_CODE] = "A";   /* Auto Action Failed Execution Failed */
const char EVIM_E     [NULL_CODE] = "E";   /* Unexpected Error In Reevaluation Process */
const char EVIM_G     [NULL_CODE] = "G";   /* Empty Group Resolution-No User Assigned */
const char EVIM_R     [NULL_CODE] = "R";   /* Empty Group Resolution-No User Assigned */


/* EVST = Event Instance Status */
const char EVST[NULL_CODE] = "EVST";

const char EVST_A     [NULL_CODE] = "A";   /* Active - OK */
const char EVST_D     [NULL_CODE] = "D";   /* Disabled */
const char EVST_U     [NULL_CODE] = "U";   /* Unable to Refresh */
const char EVST_C     [NULL_CODE] = "C";   /* Closed */
const char EVST_E     [NULL_CODE] = "E";   /* Error */


/* EXCB = Expense Cost Basis */
const char EXCB[NULL_CODE] = "EXCB";

const char EXCB_S     [NULL_CODE] = "S";   /* Supplier */
const char EXCB_O     [NULL_CODE] = "O";   /* Order */


/* EXDS = External Data Sources */
const char EXDS[NULL_CODE] = "EXDS";

const char EXDS_VR    [NULL_CODE] = "VR";   /* Vertex */
const char EXDS_VI    [NULL_CODE] = "VI";   /* Vialink */
const char EXDS_NI    [NULL_CODE] = "NI";   /* Nielsen */
const char EXDS_IRI   [NULL_CODE] = "IRI";   /* SymphonyIRI */


/* EXPC = Expense Category */
const char EXPC[NULL_CODE] = "EXPC";

const char EXPC_A     [NULL_CODE] = "A";   /* Admin */
const char EXPC_M     [NULL_CODE] = "M";   /* Misc */
const char EXPC_B     [NULL_CODE] = "B";   /* Backhaul Allowance */
const char EXPC_F     [NULL_CODE] = "F";   /* Freight */
const char EXPC_I     [NULL_CODE] = "I";   /* Insurance */


/* EXPT = Expense Type */
const char EXPT[NULL_CODE] = "EXPT";

const char EXPT_Z     [NULL_CODE] = "Z";   /* Zone */
const char EXPT_C     [NULL_CODE] = "C";   /* Country */


/* EXRT = Exchange Rate Types */
const char EXRT[NULL_CODE] = "EXRT";

const char EXRT_E     [NULL_CODE] = "E";   /* Existing */
const char EXRT_M     [NULL_CODE] = "M";   /* Manual */


/* EXSH = EXT Shipment Codes */
const char EXSH[NULL_CODE] = "EXSH";

const char EXSH_AS    [NULL_CODE] = "AS";   /* ASN No. */
const char EXSH_ES    [NULL_CODE] = "ES";   /* Ext. Ship */


/* EXST = Sales Audit Export Sub-Type */
const char EXST[NULL_CODE] = "EXST";

const char EXST_G     [NULL_CODE] = "G";   /* Gas */
const char EXST_O     [NULL_CODE] = "O";   /* Other */


/* EXTP = Exchange Types */
const char EXTP[NULL_CODE] = "EXTP";

const char EXTP_O     [NULL_CODE] = "O";   /* Operational */
const char EXTP_C     [NULL_CODE] = "C";   /* Consolidation */
const char EXTP_L     [NULL_CODE] = "L";   /* LC/Bank */
const char EXTP_P     [NULL_CODE] = "P";   /* Purchase Order */
const char EXTP_U     [NULL_CODE] = "U";   /* Customs Entry */
const char EXTP_T     [NULL_CODE] = "T";   /* Transportation */


/* FAST = Sales Type */
const char FAST[NULL_CODE] = "FAST";

const char FAST_R     [NULL_CODE] = "R";   /* Regular */
const char FAST_P     [NULL_CODE] = "P";   /* Promotional */
const char FAST_C     [NULL_CODE] = "C";   /* Clearance */


/* FDY4 = Day Frequency 454 Calendar */
const char FDY4[NULL_CODE] = "FDY4";

const char FDY4_0     [NULL_CODE] = "0";   /* Every */
const char FDY4_1     [NULL_CODE] = "1";   /* First */
const char FDY4_2     [NULL_CODE] = "2";   /* 2nd */
const char FDY4_3     [NULL_CODE] = "3";   /* 3rd */
const char FDY4_4     [NULL_CODE] = "4";   /* 4th */
const char FDY4_5     [NULL_CODE] = "5";   /* 5th */
const char FDY4_6     [NULL_CODE] = "6";   /* 6th */
const char FDY4_7     [NULL_CODE] = "7";   /* 7th */
const char FDY4_8     [NULL_CODE] = "8";   /* 8th */
const char FDY4_9     [NULL_CODE] = "9";   /* 9th */
const char FDY4_10    [NULL_CODE] = "10";   /* 10th */
const char FDY4_11    [NULL_CODE] = "11";   /* 11th */
const char FDY4_12    [NULL_CODE] = "12";   /* 12th */
const char FDY4_13    [NULL_CODE] = "13";   /* 13th */
const char FDY4_14    [NULL_CODE] = "14";   /* 14th */
const char FDY4_15    [NULL_CODE] = "15";   /* 15th */
const char FDY4_16    [NULL_CODE] = "16";   /* 16th */
const char FDY4_17    [NULL_CODE] = "17";   /* 17th */
const char FDY4_18    [NULL_CODE] = "18";   /* 18th */
const char FDY4_19    [NULL_CODE] = "19";   /* 19th */
const char FDY4_20    [NULL_CODE] = "20";   /* 20th */
const char FDY4_21    [NULL_CODE] = "21";   /* 21st */
const char FDY4_22    [NULL_CODE] = "22";   /* 22nd */
const char FDY4_23    [NULL_CODE] = "23";   /* 23rd */
const char FDY4_24    [NULL_CODE] = "24";   /* 24th */
const char FDY4_25    [NULL_CODE] = "25";   /* 25th */
const char FDY4_26    [NULL_CODE] = "26";   /* 26th */
const char FDY4_27    [NULL_CODE] = "27";   /* 27th */
const char FDY4_28    [NULL_CODE] = "28";   /* 28th */
const char FDY4_29    [NULL_CODE] = "29";   /* 29th */
const char FDY4_30    [NULL_CODE] = "30";   /* 30th */
const char FDY4_31    [NULL_CODE] = "31";   /* 31st */
const char FDY4_32    [NULL_CODE] = "32";   /* 32nd */
const char FDY4_33    [NULL_CODE] = "33";   /* 33rd */
const char FDY4_34    [NULL_CODE] = "34";   /* 34th */
const char FDY4_35    [NULL_CODE] = "35";   /* 35th */
const char FDY4_36    [NULL_CODE] = "36";   /* 36th */
const char FDY4_37    [NULL_CODE] = "37";   /* 37th */
const char FDY4_38    [NULL_CODE] = "38";   /* 38th */
const char FDY4_39    [NULL_CODE] = "39";   /* 39th */
const char FDY4_40    [NULL_CODE] = "40";   /* 40th */
const char FDY4_41    [NULL_CODE] = "41";   /* 41st */
const char FDY4_42    [NULL_CODE] = "42";   /* 42nd */
const char FDY4_43    [NULL_CODE] = "43";   /* 43rd */
const char FDY4_44    [NULL_CODE] = "44";   /* 44th */
const char FDY4_45    [NULL_CODE] = "45";   /* 45th */
const char FDY4_46    [NULL_CODE] = "46";   /* 46th */
const char FDY4_47    [NULL_CODE] = "47";   /* 47th */
const char FDY4_48    [NULL_CODE] = "48";   /* 48th */
const char FDY4_49    [NULL_CODE] = "49";   /* 49th */
const char FDY4_50    [NULL_CODE] = "50";   /* 50th */
const char FDY4_51    [NULL_CODE] = "51";   /* 51st */
const char FDY4_52    [NULL_CODE] = "52";   /* 52nd */
const char FDY4_53    [NULL_CODE] = "53";   /* 53rd */
const char FDY4_54    [NULL_CODE] = "54";   /* 54th */
const char FDY4_55    [NULL_CODE] = "55";   /* 55th */
const char FDY4_56    [NULL_CODE] = "56";   /* 56th */
const char FDY4_57    [NULL_CODE] = "57";   /* 57th */
const char FDY4_58    [NULL_CODE] = "58";   /* 58th */
const char FDY4_59    [NULL_CODE] = "59";   /* 59th */
const char FDY4_60    [NULL_CODE] = "60";   /* 60th */
const char FDY4_61    [NULL_CODE] = "61";   /* 61st */
const char FDY4_62    [NULL_CODE] = "62";   /* 62nd */
const char FDY4_63    [NULL_CODE] = "63";   /* 63rd */
const char FDY4_64    [NULL_CODE] = "64";   /* 64th */
const char FDY4_65    [NULL_CODE] = "65";   /* 65th */
const char FDY4_66    [NULL_CODE] = "66";   /* 66th */
const char FDY4_67    [NULL_CODE] = "67";   /* 67th */
const char FDY4_68    [NULL_CODE] = "68";   /* 68th */
const char FDY4_69    [NULL_CODE] = "69";   /* 69th */
const char FDY4_70    [NULL_CODE] = "70";   /* 70th */
const char FDY4_71    [NULL_CODE] = "71";   /* 71st */
const char FDY4_72    [NULL_CODE] = "72";   /* 72nd */
const char FDY4_73    [NULL_CODE] = "73";   /* 73rd */
const char FDY4_74    [NULL_CODE] = "74";   /* 74th */
const char FDY4_75    [NULL_CODE] = "75";   /* 75th */
const char FDY4_76    [NULL_CODE] = "76";   /* 76th */
const char FDY4_77    [NULL_CODE] = "77";   /* 77th */
const char FDY4_78    [NULL_CODE] = "78";   /* 78th */
const char FDY4_79    [NULL_CODE] = "79";   /* 79th */
const char FDY4_80    [NULL_CODE] = "80";   /* 80th */
const char FDY4_81    [NULL_CODE] = "81";   /* 81st */
const char FDY4_82    [NULL_CODE] = "82";   /* 82nd */
const char FDY4_83    [NULL_CODE] = "83";   /* 83rd */
const char FDY4_84    [NULL_CODE] = "84";   /* 84th */
const char FDY4_85    [NULL_CODE] = "85";   /* 85th */
const char FDY4_86    [NULL_CODE] = "86";   /* 86th */
const char FDY4_87    [NULL_CODE] = "87";   /* 87th */
const char FDY4_88    [NULL_CODE] = "88";   /* 88th */
const char FDY4_89    [NULL_CODE] = "89";   /* 89th */
const char FDY4_90    [NULL_CODE] = "90";   /* 90th */
const char FDY4_91    [NULL_CODE] = "91";   /* 91st */
const char FDY4_92    [NULL_CODE] = "92";   /* 92nd */
const char FDY4_93    [NULL_CODE] = "93";   /* 93rd */
const char FDY4_94    [NULL_CODE] = "94";   /* 94th */
const char FDY4_95    [NULL_CODE] = "95";   /* 95th */
const char FDY4_96    [NULL_CODE] = "96";   /* 96th */
const char FDY4_97    [NULL_CODE] = "97";   /* 97th */
const char FDY4_98    [NULL_CODE] = "98";   /* 98th */
const char FDY4_99    [NULL_CODE] = "99";   /* 99th */
const char FDY4_100   [NULL_CODE] = "100";   /* 100th */
const char FDY4_101   [NULL_CODE] = "101";   /* 101st */
const char FDY4_102   [NULL_CODE] = "102";   /* 102nd */
const char FDY4_103   [NULL_CODE] = "103";   /* 103rd */
const char FDY4_104   [NULL_CODE] = "104";   /* 104th */
const char FDY4_105   [NULL_CODE] = "105";   /* 105th */
const char FDY4_106   [NULL_CODE] = "106";   /* 106th */
const char FDY4_107   [NULL_CODE] = "107";   /* 107th */
const char FDY4_108   [NULL_CODE] = "108";   /* 108th */
const char FDY4_109   [NULL_CODE] = "109";   /* 109th */
const char FDY4_110   [NULL_CODE] = "110";   /* 110th */
const char FDY4_111   [NULL_CODE] = "111";   /* 111th */
const char FDY4_112   [NULL_CODE] = "112";   /* 112th */
const char FDY4_113   [NULL_CODE] = "113";   /* 113th */
const char FDY4_114   [NULL_CODE] = "114";   /* 114th */
const char FDY4_115   [NULL_CODE] = "115";   /* 115th */
const char FDY4_116   [NULL_CODE] = "116";   /* 116th */
const char FDY4_117   [NULL_CODE] = "117";   /* 117th */
const char FDY4_118   [NULL_CODE] = "118";   /* 118th */
const char FDY4_119   [NULL_CODE] = "119";   /* 119th */
const char FDY4_120   [NULL_CODE] = "120";   /* 120th */
const char FDY4_121   [NULL_CODE] = "121";   /* 121st */
const char FDY4_122   [NULL_CODE] = "122";   /* 122nd */
const char FDY4_123   [NULL_CODE] = "123";   /* 123rd */
const char FDY4_124   [NULL_CODE] = "124";   /* 124th */
const char FDY4_125   [NULL_CODE] = "125";   /* 125th */
const char FDY4_126   [NULL_CODE] = "126";   /* 126th */
const char FDY4_127   [NULL_CODE] = "127";   /* 127th */
const char FDY4_128   [NULL_CODE] = "128";   /* 128th */
const char FDY4_129   [NULL_CODE] = "129";   /* 129th */
const char FDY4_130   [NULL_CODE] = "130";   /* 130th */
const char FDY4_131   [NULL_CODE] = "131";   /* 131st */
const char FDY4_132   [NULL_CODE] = "132";   /* 132nd */
const char FDY4_133   [NULL_CODE] = "133";   /* 133rd */
const char FDY4_134   [NULL_CODE] = "134";   /* 134th */
const char FDY4_135   [NULL_CODE] = "135";   /* 135th */
const char FDY4_136   [NULL_CODE] = "136";   /* 136th */
const char FDY4_137   [NULL_CODE] = "137";   /* 137th */
const char FDY4_138   [NULL_CODE] = "138";   /* 138th */
const char FDY4_139   [NULL_CODE] = "139";   /* 139th */
const char FDY4_140   [NULL_CODE] = "140";   /* 140th */
const char FDY4_141   [NULL_CODE] = "141";   /* 141st */
const char FDY4_142   [NULL_CODE] = "142";   /* 142nd */
const char FDY4_143   [NULL_CODE] = "143";   /* 143rd */
const char FDY4_144   [NULL_CODE] = "144";   /* 144th */
const char FDY4_145   [NULL_CODE] = "145";   /* 145th */
const char FDY4_146   [NULL_CODE] = "146";   /* 146th */
const char FDY4_147   [NULL_CODE] = "147";   /* 147th */
const char FDY4_148   [NULL_CODE] = "148";   /* 148th */
const char FDY4_149   [NULL_CODE] = "149";   /* 149th */
const char FDY4_150   [NULL_CODE] = "150";   /* 150th */
const char FDY4_151   [NULL_CODE] = "151";   /* 151st */
const char FDY4_152   [NULL_CODE] = "152";   /* 152nd */
const char FDY4_153   [NULL_CODE] = "153";   /* 153rd */
const char FDY4_154   [NULL_CODE] = "154";   /* 154th */
const char FDY4_155   [NULL_CODE] = "155";   /* 155th */
const char FDY4_156   [NULL_CODE] = "156";   /* 156th */
const char FDY4_157   [NULL_CODE] = "157";   /* 157th */
const char FDY4_158   [NULL_CODE] = "158";   /* 158th */
const char FDY4_159   [NULL_CODE] = "159";   /* 159th */
const char FDY4_160   [NULL_CODE] = "160";   /* 160th */
const char FDY4_161   [NULL_CODE] = "161";   /* 161st */
const char FDY4_162   [NULL_CODE] = "162";   /* 162nd */
const char FDY4_163   [NULL_CODE] = "163";   /* 163rd */
const char FDY4_164   [NULL_CODE] = "164";   /* 164th */
const char FDY4_165   [NULL_CODE] = "165";   /* 165th */
const char FDY4_166   [NULL_CODE] = "166";   /* 166th */
const char FDY4_167   [NULL_CODE] = "167";   /* 167th */
const char FDY4_168   [NULL_CODE] = "168";   /* 168th */
const char FDY4_169   [NULL_CODE] = "169";   /* 169th */
const char FDY4_170   [NULL_CODE] = "170";   /* 170th */
const char FDY4_171   [NULL_CODE] = "171";   /* 171st */
const char FDY4_172   [NULL_CODE] = "172";   /* 172nd */
const char FDY4_173   [NULL_CODE] = "173";   /* 173rd */
const char FDY4_174   [NULL_CODE] = "174";   /* 174th */
const char FDY4_175   [NULL_CODE] = "175";   /* 175th */
const char FDY4_176   [NULL_CODE] = "176";   /* 176th */
const char FDY4_177   [NULL_CODE] = "177";   /* 177th */
const char FDY4_178   [NULL_CODE] = "178";   /* 178th */
const char FDY4_179   [NULL_CODE] = "179";   /* 179th */
const char FDY4_180   [NULL_CODE] = "180";   /* 180th */
const char FDY4_181   [NULL_CODE] = "181";   /* 181st */
const char FDY4_182   [NULL_CODE] = "182";   /* 182nd */
const char FDY4_183   [NULL_CODE] = "183";   /* 183rd */
const char FDY4_184   [NULL_CODE] = "184";   /* 184th */
const char FDY4_185   [NULL_CODE] = "185";   /* 185th */
const char FDY4_186   [NULL_CODE] = "186";   /* 186th */
const char FDY4_187   [NULL_CODE] = "187";   /* 187th */
const char FDY4_188   [NULL_CODE] = "188";   /* 188th */
const char FDY4_189   [NULL_CODE] = "189";   /* 189th */
const char FDY4_999   [NULL_CODE] = "999";   /* Last */


/* FDYC = Day Frequency Normal Calendar */
const char FDYC[NULL_CODE] = "FDYC";

const char FDYC_0     [NULL_CODE] = "0";   /* Every */
const char FDYC_1     [NULL_CODE] = "1";   /* First */
const char FDYC_2     [NULL_CODE] = "2";   /* 2nd */
const char FDYC_3     [NULL_CODE] = "3";   /* 3rd */
const char FDYC_4     [NULL_CODE] = "4";   /* 4th */
const char FDYC_5     [NULL_CODE] = "5";   /* 5th */
const char FDYC_6     [NULL_CODE] = "6";   /* 6th */
const char FDYC_7     [NULL_CODE] = "7";   /* 7th */
const char FDYC_8     [NULL_CODE] = "8";   /* 8th */
const char FDYC_9     [NULL_CODE] = "9";   /* 9th */
const char FDYC_10    [NULL_CODE] = "10";   /* 10th */
const char FDYC_11    [NULL_CODE] = "11";   /* 11th */
const char FDYC_12    [NULL_CODE] = "12";   /* 12th */
const char FDYC_13    [NULL_CODE] = "13";   /* 13th */
const char FDYC_14    [NULL_CODE] = "14";   /* 14th */
const char FDYC_15    [NULL_CODE] = "15";   /* 15th */
const char FDYC_16    [NULL_CODE] = "16";   /* 16th */
const char FDYC_17    [NULL_CODE] = "17";   /* 17th */
const char FDYC_18    [NULL_CODE] = "18";   /* 18th */
const char FDYC_19    [NULL_CODE] = "19";   /* 19th */
const char FDYC_20    [NULL_CODE] = "20";   /* 20th */
const char FDYC_21    [NULL_CODE] = "21";   /* 21st */
const char FDYC_22    [NULL_CODE] = "22";   /* 22nd */
const char FDYC_23    [NULL_CODE] = "23";   /* 23rd */
const char FDYC_24    [NULL_CODE] = "24";   /* 24th */
const char FDYC_25    [NULL_CODE] = "25";   /* 25th */
const char FDYC_26    [NULL_CODE] = "26";   /* 26th */
const char FDYC_27    [NULL_CODE] = "27";   /* 27th */
const char FDYC_28    [NULL_CODE] = "28";   /* 28th */
const char FDYC_29    [NULL_CODE] = "29";   /* 29th */
const char FDYC_30    [NULL_CODE] = "30";   /* 30th */
const char FDYC_31    [NULL_CODE] = "31";   /* 31st */
const char FDYC_32    [NULL_CODE] = "32";   /* 32nd */
const char FDYC_33    [NULL_CODE] = "33";   /* 33rd */
const char FDYC_34    [NULL_CODE] = "34";   /* 34th */
const char FDYC_35    [NULL_CODE] = "35";   /* 35th */
const char FDYC_36    [NULL_CODE] = "36";   /* 36th */
const char FDYC_37    [NULL_CODE] = "37";   /* 37th */
const char FDYC_38    [NULL_CODE] = "38";   /* 38th */
const char FDYC_39    [NULL_CODE] = "39";   /* 39th */
const char FDYC_40    [NULL_CODE] = "40";   /* 40th */
const char FDYC_41    [NULL_CODE] = "41";   /* 41st */
const char FDYC_42    [NULL_CODE] = "42";   /* 42nd */
const char FDYC_43    [NULL_CODE] = "43";   /* 43rd */
const char FDYC_44    [NULL_CODE] = "44";   /* 44th */
const char FDYC_45    [NULL_CODE] = "45";   /* 45th */
const char FDYC_46    [NULL_CODE] = "46";   /* 46th */
const char FDYC_47    [NULL_CODE] = "47";   /* 47th */
const char FDYC_48    [NULL_CODE] = "48";   /* 48th */
const char FDYC_49    [NULL_CODE] = "49";   /* 49th */
const char FDYC_50    [NULL_CODE] = "50";   /* 50th */
const char FDYC_51    [NULL_CODE] = "51";   /* 51st */
const char FDYC_52    [NULL_CODE] = "52";   /* 52nd */
const char FDYC_53    [NULL_CODE] = "53";   /* 53rd */
const char FDYC_54    [NULL_CODE] = "54";   /* 54th */
const char FDYC_55    [NULL_CODE] = "55";   /* 55th */
const char FDYC_56    [NULL_CODE] = "56";   /* 56th */
const char FDYC_57    [NULL_CODE] = "57";   /* 57th */
const char FDYC_58    [NULL_CODE] = "58";   /* 58th */
const char FDYC_59    [NULL_CODE] = "59";   /* 59th */
const char FDYC_60    [NULL_CODE] = "60";   /* 60th */
const char FDYC_61    [NULL_CODE] = "61";   /* 61st */
const char FDYC_62    [NULL_CODE] = "62";   /* 62nd */
const char FDYC_63    [NULL_CODE] = "63";   /* 63rd */
const char FDYC_64    [NULL_CODE] = "64";   /* 64th */
const char FDYC_65    [NULL_CODE] = "65";   /* 65th */
const char FDYC_66    [NULL_CODE] = "66";   /* 66th */
const char FDYC_67    [NULL_CODE] = "67";   /* 67th */
const char FDYC_68    [NULL_CODE] = "68";   /* 68th */
const char FDYC_69    [NULL_CODE] = "69";   /* 69th */
const char FDYC_70    [NULL_CODE] = "70";   /* 70th */
const char FDYC_71    [NULL_CODE] = "71";   /* 71st */
const char FDYC_72    [NULL_CODE] = "72";   /* 72nd */
const char FDYC_73    [NULL_CODE] = "73";   /* 73rd */
const char FDYC_74    [NULL_CODE] = "74";   /* 74th */
const char FDYC_75    [NULL_CODE] = "75";   /* 75th */
const char FDYC_76    [NULL_CODE] = "76";   /* 76th */
const char FDYC_77    [NULL_CODE] = "77";   /* 77th */
const char FDYC_78    [NULL_CODE] = "78";   /* 78th */
const char FDYC_79    [NULL_CODE] = "79";   /* 79th */
const char FDYC_80    [NULL_CODE] = "80";   /* 80th */
const char FDYC_81    [NULL_CODE] = "81";   /* 81st */
const char FDYC_82    [NULL_CODE] = "82";   /* 82nd */
const char FDYC_83    [NULL_CODE] = "83";   /* 83rd */
const char FDYC_84    [NULL_CODE] = "84";   /* 84th */
const char FDYC_85    [NULL_CODE] = "85";   /* 85th */
const char FDYC_86    [NULL_CODE] = "86";   /* 86th */
const char FDYC_87    [NULL_CODE] = "87";   /* 87th */
const char FDYC_88    [NULL_CODE] = "88";   /* 88th */
const char FDYC_89    [NULL_CODE] = "89";   /* 89th */
const char FDYC_90    [NULL_CODE] = "90";   /* 90th */
const char FDYC_91    [NULL_CODE] = "91";   /* 91st */
const char FDYC_92    [NULL_CODE] = "92";   /* 92nd */
const char FDYC_93    [NULL_CODE] = "93";   /* 93rd */
const char FDYC_94    [NULL_CODE] = "94";   /* 94th */
const char FDYC_95    [NULL_CODE] = "95";   /* 95th */
const char FDYC_96    [NULL_CODE] = "96";   /* 96th */
const char FDYC_97    [NULL_CODE] = "97";   /* 97th */
const char FDYC_98    [NULL_CODE] = "98";   /* 98th */
const char FDYC_99    [NULL_CODE] = "99";   /* 99th */
const char FDYC_100   [NULL_CODE] = "100";   /* 100th */
const char FDYC_101   [NULL_CODE] = "101";   /* 101st */
const char FDYC_102   [NULL_CODE] = "102";   /* 102nd */
const char FDYC_103   [NULL_CODE] = "103";   /* 103rd */
const char FDYC_104   [NULL_CODE] = "104";   /* 104th */
const char FDYC_105   [NULL_CODE] = "105";   /* 105th */
const char FDYC_106   [NULL_CODE] = "106";   /* 106th */
const char FDYC_107   [NULL_CODE] = "107";   /* 107th */
const char FDYC_108   [NULL_CODE] = "108";   /* 108th */
const char FDYC_109   [NULL_CODE] = "109";   /* 109th */
const char FDYC_110   [NULL_CODE] = "110";   /* 110th */
const char FDYC_111   [NULL_CODE] = "111";   /* 111th */
const char FDYC_112   [NULL_CODE] = "112";   /* 112th */
const char FDYC_113   [NULL_CODE] = "113";   /* 113th */
const char FDYC_114   [NULL_CODE] = "114";   /* 114th */
const char FDYC_115   [NULL_CODE] = "115";   /* 115th */
const char FDYC_116   [NULL_CODE] = "116";   /* 116th */
const char FDYC_117   [NULL_CODE] = "117";   /* 117th */
const char FDYC_118   [NULL_CODE] = "118";   /* 118th */
const char FDYC_119   [NULL_CODE] = "119";   /* 119th */
const char FDYC_120   [NULL_CODE] = "120";   /* 120th */
const char FDYC_121   [NULL_CODE] = "121";   /* 121st */
const char FDYC_122   [NULL_CODE] = "122";   /* 122nd */
const char FDYC_123   [NULL_CODE] = "123";   /* 123rd */
const char FDYC_124   [NULL_CODE] = "124";   /* 124th */
const char FDYC_125   [NULL_CODE] = "125";   /* 125th */
const char FDYC_126   [NULL_CODE] = "126";   /* 126th */
const char FDYC_127   [NULL_CODE] = "127";   /* 127th */
const char FDYC_128   [NULL_CODE] = "128";   /* 128th */
const char FDYC_129   [NULL_CODE] = "129";   /* 129th */
const char FDYC_130   [NULL_CODE] = "130";   /* 130th */
const char FDYC_131   [NULL_CODE] = "131";   /* 131st */
const char FDYC_132   [NULL_CODE] = "132";   /* 132nd */
const char FDYC_133   [NULL_CODE] = "133";   /* 133rd */
const char FDYC_134   [NULL_CODE] = "134";   /* 134th */
const char FDYC_135   [NULL_CODE] = "135";   /* 135th */
const char FDYC_136   [NULL_CODE] = "136";   /* 136th */
const char FDYC_137   [NULL_CODE] = "137";   /* 137th */
const char FDYC_138   [NULL_CODE] = "138";   /* 138th */
const char FDYC_139   [NULL_CODE] = "139";   /* 139th */
const char FDYC_140   [NULL_CODE] = "140";   /* 140th */
const char FDYC_141   [NULL_CODE] = "141";   /* 141st */
const char FDYC_142   [NULL_CODE] = "142";   /* 142nd */
const char FDYC_143   [NULL_CODE] = "143";   /* 143rd */
const char FDYC_144   [NULL_CODE] = "144";   /* 144th */
const char FDYC_145   [NULL_CODE] = "145";   /* 145th */
const char FDYC_146   [NULL_CODE] = "146";   /* 146th */
const char FDYC_147   [NULL_CODE] = "147";   /* 147th */
const char FDYC_148   [NULL_CODE] = "148";   /* 148th */
const char FDYC_149   [NULL_CODE] = "149";   /* 149th */
const char FDYC_150   [NULL_CODE] = "150";   /* 150th */
const char FDYC_151   [NULL_CODE] = "151";   /* 151st */
const char FDYC_152   [NULL_CODE] = "152";   /* 152nd */
const char FDYC_153   [NULL_CODE] = "153";   /* 153rd */
const char FDYC_154   [NULL_CODE] = "154";   /* 154th */
const char FDYC_155   [NULL_CODE] = "155";   /* 155th */
const char FDYC_156   [NULL_CODE] = "156";   /* 156th */
const char FDYC_157   [NULL_CODE] = "157";   /* 157th */
const char FDYC_158   [NULL_CODE] = "158";   /* 158th */
const char FDYC_159   [NULL_CODE] = "159";   /* 159th */
const char FDYC_160   [NULL_CODE] = "160";   /* 160th */
const char FDYC_161   [NULL_CODE] = "161";   /* 161st */
const char FDYC_162   [NULL_CODE] = "162";   /* 162nd */
const char FDYC_163   [NULL_CODE] = "163";   /* 163rd */
const char FDYC_164   [NULL_CODE] = "164";   /* 164th */
const char FDYC_165   [NULL_CODE] = "165";   /* 165th */
const char FDYC_166   [NULL_CODE] = "166";   /* 166th */
const char FDYC_167   [NULL_CODE] = "167";   /* 167th */
const char FDYC_168   [NULL_CODE] = "168";   /* 168th */
const char FDYC_169   [NULL_CODE] = "169";   /* 169th */
const char FDYC_170   [NULL_CODE] = "170";   /* 170th */
const char FDYC_171   [NULL_CODE] = "171";   /* 171st */
const char FDYC_172   [NULL_CODE] = "172";   /* 172nd */
const char FDYC_173   [NULL_CODE] = "173";   /* 173rd */
const char FDYC_174   [NULL_CODE] = "174";   /* 174th */
const char FDYC_175   [NULL_CODE] = "175";   /* 175th */
const char FDYC_176   [NULL_CODE] = "176";   /* 176th */
const char FDYC_177   [NULL_CODE] = "177";   /* 177th */
const char FDYC_178   [NULL_CODE] = "178";   /* 178th */
const char FDYC_179   [NULL_CODE] = "179";   /* 179th */
const char FDYC_180   [NULL_CODE] = "180";   /* 180th */
const char FDYC_181   [NULL_CODE] = "181";   /* 181st */
const char FDYC_182   [NULL_CODE] = "182";   /* 182nd */
const char FDYC_183   [NULL_CODE] = "183";   /* 183rd */
const char FDYC_184   [NULL_CODE] = "184";   /* 184th */
const char FDYC_185   [NULL_CODE] = "185";   /* 185th */
const char FDYC_186   [NULL_CODE] = "186";   /* 186th */
const char FDYC_187   [NULL_CODE] = "187";   /* 187th */
const char FDYC_188   [NULL_CODE] = "188";   /* 188th */
const char FDYC_189   [NULL_CODE] = "189";   /* 189th */
const char FDYC_999   [NULL_CODE] = "999";   /* Last */


/* FFUI = Flex Fields UI Widget Types */
const char FFUI[NULL_CODE] = "FFUI";

const char FFUI_TI    [NULL_CODE] = "TI";   /* Text Item */
const char FFUI_RG    [NULL_CODE] = "RG";   /* List of Values */
const char FFUI_LI    [NULL_CODE] = "LI";   /* List Item */
const char FFUI_CB    [NULL_CODE] = "CB";   /* Check Box */
const char FFUI_DT    [NULL_CODE] = "DT";   /* Date */


/* FINT = From Inventory Types */
const char FINT[NULL_CODE] = "FINT";

const char FINT_I     [NULL_CODE] = "I";   /* All Inventory */
const char FINT_U     [NULL_CODE] = "U";   /* All Unavailable */
const char FINT_A     [NULL_CODE] = "A";   /* Available */
const char FINT_S     [NULL_CODE] = "S";   /* Specific Unavailable */


/* FIPR = Item Fill Priority */
const char FIPR[NULL_CODE] = "FIPR";

const char FIPR_M     [NULL_CODE] = "M";   /* Main */
const char FIPR_S     [NULL_CODE] = "S";   /* Substitute */


/* FLDA = Sales Audit Field Access */
const char FLDA[NULL_CODE] = "FLDA";

const char FLDA_ESO   [NULL_CODE] = "ESO";   /* Error List Store Override */
const char FLDA_EHO   [NULL_CODE] = "EHO";   /* Error List HQ Override */
const char FLDA_OSS   [NULL_CODE] = "OSS";   /* O/S Store Reported */
const char FLDA_OSH   [NULL_CODE] = "OSH";   /* O/S HQ Reported */
const char FLDA_OSA   [NULL_CODE] = "OSA";   /* O/S Actual O/S Value */
const char FLDA_OST   [NULL_CODE] = "OST";   /* O/S Trial O/S Value */
const char FLDA_MS    [NULL_CODE] = "MS";   /* Misc. Store Reported */
const char FLDA_MH    [NULL_CODE] = "MH";   /* Misc. HQ Reported */
const char FLDA_BOS   [NULL_CODE] = "BOS";   /* Balance Level Over/Short Value */


/* FLOC = From Location */
const char FLOC[NULL_CODE] = "FLOC";

const char FLOC_S     [NULL_CODE] = "S";   /* From St */
const char FLOC_W     [NULL_CODE] = "W";   /* From Wh */
const char FLOC_L     [NULL_CODE] = "L";   /* From Loc */


/* FLOW = Filter Org Hier including WH, Finishers */
const char FLOW[NULL_CODE] = "FLOW";

const char FLOW_C     [NULL_CODE] = "C";   /* @OH2@ */
const char FLOW_A     [NULL_CODE] = "A";   /* @OH3@ */
const char FLOW_R     [NULL_CODE] = "R";   /* @OH4@ */
const char FLOW_D     [NULL_CODE] = "D";   /* @OH5@ */
const char FLOW_W     [NULL_CODE] = "W";   /* Warehouse */
const char FLOW_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char FLOW_E     [NULL_CODE] = "E";   /* External Finisher */
const char FLOW_T     [NULL_CODE] = "T";   /* Transfer Entity */
const char FLOW_O     [NULL_CODE] = "O";   /* Org Unit */


/* FLQC = Label Failed QC */
const char FLQC[NULL_CODE] = "FLQC";

const char FLQC_f     [NULL_CODE] = "f";   /* Failed QC */


/* FLTM = Filter Merchandise Hierarchy Levels */
const char FLTM[NULL_CODE] = "FLTM";

const char FLTM_D     [NULL_CODE] = "D";   /* @MH2@ */
const char FLTM_G     [NULL_CODE] = "G";   /* @MH3@ */
const char FLTM_P     [NULL_CODE] = "P";   /* @MH4@ */
const char FLTM_C     [NULL_CODE] = "C";   /* @MH5@ */
const char FLTM_S     [NULL_CODE] = "S";   /* @MH6@ */


/* FLTO = Filter Organization Hierarchy Levels */
const char FLTO[NULL_CODE] = "FLTO";

const char FLTO_C     [NULL_CODE] = "C";   /* @OH2@ */
const char FLTO_A     [NULL_CODE] = "A";   /* @OH3@ */
const char FLTO_R     [NULL_CODE] = "R";   /* @OH4@ */
const char FLTO_D     [NULL_CODE] = "D";   /* @OH5@ */


/* FMTP = Form Type */
const char FMTP[NULL_CODE] = "FMTP";

const char FMTP_F     [NULL_CODE] = "F";   /* Form */
const char FMTP_M     [NULL_CODE] = "M";   /* Menu */


/* FNAP = Financial Integration Application */
const char FNAP[NULL_CODE] = "FNAP";

const char FNAP_O     [NULL_CODE] = "O";   /* Point-To-Point Oracle E-Business Suite */
const char FNAP_A     [NULL_CODE] = "A";   /* Oracle Retail Financial Integration */


/* FOBT = Location Transportation Responsibility */
const char FOBT[NULL_CODE] = "FOBT";

const char FOBT_AC    [NULL_CODE] = "AC";   /* City and State */
const char FOBT_CA    [NULL_CODE] = "CA";   /* Country Of Sourcing */
const char FOBT_CC    [NULL_CODE] = "CC";   /* Country */
const char FOBT_CI    [NULL_CODE] = "CI";   /* City */
const char FOBT_CO    [NULL_CODE] = "CO";   /* County/Parish and State */
const char FOBT_CS    [NULL_CODE] = "CS";   /* Canadian SPLC */
const char FOBT_CY    [NULL_CODE] = "CY";   /* County/Parish */
const char FOBT_DE    [NULL_CODE] = "DE";   /* Destination (Shipping) */
const char FOBT_FA    [NULL_CODE] = "FA";   /* Factory */
const char FOBT_FE    [NULL_CODE] = "FE";   /* Freight Equalization Point */
const char FOBT_FF    [NULL_CODE] = "FF";   /* Foreign Freight Forwarder Location */
const char FOBT_MI    [NULL_CODE] = "MI";   /* Mill */
const char FOBT_NS    [NULL_CODE] = "NS";   /* City/State from Points */
const char FOBT_OA    [NULL_CODE] = "OA";   /* Origin (After loading on Equipment) */
const char FOBT_OR    [NULL_CODE] = "OR";   /* Origin (Shipping Point) */
const char FOBT_OV    [NULL_CODE] = "OV";   /* On Vessel (Free on Board Point) */
const char FOBT_SP    [NULL_CODE] = "SP";   /* State/Province */
const char FOBT_TL    [NULL_CODE] = "TL";   /* Terminal Cargo Location */
const char FOBT_WH    [NULL_CODE] = "WH";   /* Warehouse */


/* FORM = Sales Audit Forms */
const char FORM[NULL_CODE] = "FORM";

const char FORM_TR    [NULL_CODE] = "TR";   /* Transaction Detail */
const char FORM_O     [NULL_CODE] = "O";   /* Over/Short Totals */
const char FORM_T     [NULL_CODE] = "T";   /* Misc. Totals */
const char FORM_M     [NULL_CODE] = "M";   /* Missing Trans. */
const char FORM_NT    [NULL_CODE] = "NT";   /* Non Transactional */


/* FOST = @SUH4@ Order source types */
const char FOST[NULL_CODE] = "FOST";

const char FOST_ST    [NULL_CODE] = "ST";   /* Store */
const char FOST_SU    [NULL_CODE] = "SU";   /* Supplier */
const char FOST_WH    [NULL_CODE] = "WH";   /* Warehouse */


/* FOTP = Customs Entry Form Type */
const char FOTP[NULL_CODE] = "FOTP";

const char FOTP_1     [NULL_CODE] = "1";   /* Form Type 1 */
const char FOTP_2     [NULL_CODE] = "2";   /* Form Type 2 */
const char FOTP_3     [NULL_CODE] = "3";   /* Form Type 3 */


/* FQHF = Half Frequency */
const char FQHF[NULL_CODE] = "FQHF";

const char FQHF_0     [NULL_CODE] = "0";   /* Every */
const char FQHF_1     [NULL_CODE] = "1";   /* First */
const char FQHF_2     [NULL_CODE] = "2";   /* Second */


/* FQHR = Hour Frequency */
const char FQHR[NULL_CODE] = "FQHR";

const char FQHR_0     [NULL_CODE] = "0";   /* Once a Day */
const char FQHR_1     [NULL_CODE] = "1";   /* Every Hour */


/* FQMN = Month Frequency */
const char FQMN[NULL_CODE] = "FQMN";

const char FQMN_0     [NULL_CODE] = "0";   /* Every */
const char FQMN_1     [NULL_CODE] = "1";   /* First */
const char FQMN_2     [NULL_CODE] = "2";   /* 2nd */
const char FQMN_3     [NULL_CODE] = "3";   /* 3rd */
const char FQMN_4     [NULL_CODE] = "4";   /* 4th */
const char FQMN_5     [NULL_CODE] = "5";   /* 5th */
const char FQMN_6     [NULL_CODE] = "6";   /* 6th */
const char FQMN_7     [NULL_CODE] = "7";   /* 7th */
const char FQMN_8     [NULL_CODE] = "8";   /* 8th */
const char FQMN_9     [NULL_CODE] = "9";   /* 9th */
const char FQMN_10    [NULL_CODE] = "10";   /* 10th */
const char FQMN_11    [NULL_CODE] = "11";   /* 11th */
const char FQMN_12    [NULL_CODE] = "12";   /* 12th */


/* FQWK = Week Frequency */
const char FQWK[NULL_CODE] = "FQWK";

const char FQWK_0     [NULL_CODE] = "0";   /* Every */
const char FQWK_1     [NULL_CODE] = "1";   /* First */
const char FQWK_2     [NULL_CODE] = "2";   /* 2nd */
const char FQWK_3     [NULL_CODE] = "3";   /* 3rd */
const char FQWK_4     [NULL_CODE] = "4";   /* 4th */
const char FQWK_5     [NULL_CODE] = "5";   /* 5th */
const char FQWK_6     [NULL_CODE] = "6";   /* 6th */
const char FQWK_7     [NULL_CODE] = "7";   /* 7th */
const char FQWK_8     [NULL_CODE] = "8";   /* 8th */
const char FQWK_9     [NULL_CODE] = "9";   /* 9th */
const char FQWK_10    [NULL_CODE] = "10";   /* 10th */
const char FQWK_11    [NULL_CODE] = "11";   /* 11th */
const char FQWK_12    [NULL_CODE] = "12";   /* 12th */
const char FQWK_13    [NULL_CODE] = "13";   /* 13th */
const char FQWK_14    [NULL_CODE] = "14";   /* 14th */
const char FQWK_15    [NULL_CODE] = "15";   /* 15th */
const char FQWK_16    [NULL_CODE] = "16";   /* 16th */
const char FQWK_17    [NULL_CODE] = "17";   /* 17th */
const char FQWK_18    [NULL_CODE] = "18";   /* 18th */
const char FQWK_19    [NULL_CODE] = "19";   /* 19th */
const char FQWK_20    [NULL_CODE] = "20";   /* 20th */
const char FQWK_21    [NULL_CODE] = "21";   /* 21st */
const char FQWK_22    [NULL_CODE] = "22";   /* 22nd */
const char FQWK_23    [NULL_CODE] = "23";   /* 23rd */
const char FQWK_24    [NULL_CODE] = "24";   /* 24th */
const char FQWK_25    [NULL_CODE] = "25";   /* 25th */
const char FQWK_26    [NULL_CODE] = "26";   /* 26th */
const char FQWK_27    [NULL_CODE] = "27";   /* 27th */
const char FQWK_28    [NULL_CODE] = "28";   /* 28th */
const char FQWK_29    [NULL_CODE] = "29";   /* 29th */
const char FQWK_30    [NULL_CODE] = "30";   /* 30th */
const char FQWK_31    [NULL_CODE] = "31";   /* 31st */
const char FQWK_32    [NULL_CODE] = "32";   /* 32nd */
const char FQWK_33    [NULL_CODE] = "33";   /* 33rd */
const char FQWK_34    [NULL_CODE] = "34";   /* 34th */
const char FQWK_35    [NULL_CODE] = "35";   /* 35th */
const char FQWK_36    [NULL_CODE] = "36";   /* 36th */
const char FQWK_37    [NULL_CODE] = "37";   /* 37th */
const char FQWK_38    [NULL_CODE] = "38";   /* 38th */
const char FQWK_39    [NULL_CODE] = "39";   /* 39th */
const char FQWK_40    [NULL_CODE] = "40";   /* 40th */
const char FQWK_41    [NULL_CODE] = "41";   /* 41st */
const char FQWK_42    [NULL_CODE] = "42";   /* 42nd */
const char FQWK_43    [NULL_CODE] = "43";   /* 43rd */
const char FQWK_44    [NULL_CODE] = "44";   /* 44th */
const char FQWK_45    [NULL_CODE] = "45";   /* 45th */
const char FQWK_46    [NULL_CODE] = "46";   /* 46th */
const char FQWK_47    [NULL_CODE] = "47";   /* 47th */
const char FQWK_48    [NULL_CODE] = "48";   /* 48th */
const char FQWK_49    [NULL_CODE] = "49";   /* 49th */
const char FQWK_50    [NULL_CODE] = "50";   /* 50th */
const char FQWK_51    [NULL_CODE] = "51";   /* 51st */
const char FQWK_52    [NULL_CODE] = "52";   /* 52nd */
const char FQWK_99    [NULL_CODE] = "99";   /* Last */


/* FTYP = Finisher Types */
const char FTYP[NULL_CODE] = "FTYP";

const char FTYP_I     [NULL_CODE] = "I";   /* Internal */
const char FTYP_E     [NULL_CODE] = "E";   /* External */


/* FXCL = Fixed Deal Collect By Type */
const char FXCL[NULL_CODE] = "FXCL";

const char FXCL_D     [NULL_CODE] = "D";   /* Date */
const char FXCL_M     [NULL_CODE] = "M";   /* Monthly */
const char FXCL_Q     [NULL_CODE] = "Q";   /* Quarterly */
const char FXCL_A     [NULL_CODE] = "A";   /* Annually */


/* FXDT = Fixed Deal Types */
const char FXDT[NULL_CODE] = "FXDT";

const char FXDT_CA    [NULL_CODE] = "CA";   /* Cancel Allowance */
const char FXDT_ANP   [NULL_CODE] = "ANP";   /* Allowance Non Performance */
const char FXDT_DA    [NULL_CODE] = "DA";   /* Display Allowance */
const char FXDT_EBA   [NULL_CODE] = "EBA";   /* Early Buy Allowance */
const char FXDT_ND    [NULL_CODE] = "ND";   /* New Discount */
const char FXDT_NW    [NULL_CODE] = "NW";   /* New Warehouse */
const char FXDT_LUMP  [NULL_CODE] = "LUMP";   /* Lump Sum */
const char FXDT_NI    [NULL_CODE] = "NI";   /* New Item Allowance */
const char FXDT_SA    [NULL_CODE] = "SA";   /* Slotting Allowance */
const char FXDT_NDA   [NULL_CODE] = "NDA";   /* New Distribution Allowance */
const char FXDT_AD    [NULL_CODE] = "AD";   /* Advertising Allowance */
const char FXDT_OTHER [NULL_CODE] = "OTHER";   /* Other Allowance */
const char FXDT_SCAN  [NULL_CODE] = "SCAN";   /* Scan Based Allowance */


/* FXMC = Labels for merchandise and Locations */
const char FXMC[NULL_CODE] = "FXMC";

const char FXMC_MERCH [NULL_CODE] = "MERCH";   /* Merchandise */
const char FXMC_LOC   [NULL_CODE] = "LOC";   /* Locations */


/* FXST = Fixed Deal Status */
const char FXST[NULL_CODE] = "FXST";

const char FXST_A     [NULL_CODE] = "A";   /* Active */
const char FXST_I     [NULL_CODE] = "I";   /* Inactive */


/* GAIT = GAP Packing Item Types */
const char GAIT[NULL_CODE] = "GAIT";

const char GAIT_F     [NULL_CODE] = "F";   /* From */
const char GAIT_R     [NULL_CODE] = "R";   /* To */


/* GALC = GAP Stock Reconcile Location List */
const char GALC[NULL_CODE] = "GALC";

const char GALC_S     [NULL_CODE] = "S";   /* Store */
const char GALC_W     [NULL_CODE] = "W";   /* Warehouse */
const char GALC_PW    [NULL_CODE] = "PW";   /* Physical Warehouse */
const char GALC_C     [NULL_CODE] = "C";   /* Store Class */
const char GALC_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GALC_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GALC_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GALC_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GALC_L     [NULL_CODE] = "L";   /* Location Trait */
const char GALC_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GALC_LLS   [NULL_CODE] = "LLS";   /* Loc-List Store */
const char GALC_LLW   [NULL_CODE] = "LLW";   /* Loc-List Warehouse */


/* GART = GAP Stock Reconcile Action Types */
const char GART[NULL_CODE] = "GART";

const char GART_SL    [NULL_CODE] = "SL";   /* Shipping Location */
const char GART_RL    [NULL_CODE] = "RL";   /* Receiving Location */
const char GART_FR    [NULL_CODE] = "FR";   /* Force Receipt */
const char GART_FC    [NULL_CODE] = "FC";   /* Freight Claim */
const char GART_RE    [NULL_CODE] = "RE";   /* Received Elsewhere - See Comments */


/* GLMT = General Ledger Monthly Transactions */
const char GLMT[NULL_CODE] = "GLMT";

const char GLMT_50R   [NULL_CODE] = "50R";   /* OPN STK Retail */
const char GLMT_50C   [NULL_CODE] = "50C";   /* OPN STK Cost */
const char GLMT_22R   [NULL_CODE] = "22R";   /* Stock Adj Retail */
const char GLMT_22C   [NULL_CODE] = "22C";   /* Stock Adj Cost */
const char GLMT_20R   [NULL_CODE] = "20R";   /* Purch Retail */
const char GLMT_20C   [NULL_CODE] = "20C";   /* Purch Cost */
const char GLMT_24R   [NULL_CODE] = "24R";   /* RTV Retail */
const char GLMT_24C   [NULL_CODE] = "24C";   /* RTV Cost */
const char GLMT_26C   [NULL_CODE] = "26C";   /* Freight Cost */
const char GLMT_30R   [NULL_CODE] = "30R";   /* TSF in Retail */
const char GLMT_30C   [NULL_CODE] = "30C";   /* TSF in Cost */
const char GLMT_32R   [NULL_CODE] = "32R";   /* TSF Out Retail */
const char GLMT_32C   [NULL_CODE] = "32C";   /* TSF Out Cost */
const char GLMT_01R   [NULL_CODE] = "01R";   /* Net Sales Retail */
const char GLMT_02R   [NULL_CODE] = "02R";   /* Net Sales Retail ex VAT */
const char GLMT_01C   [NULL_CODE] = "01C";   /* Net Sales Cost */
const char GLMT_04R   [NULL_CODE] = "04R";   /* Returns Retail */
const char GLMT_04C   [NULL_CODE] = "04C";   /* Returns Cost */
const char GLMT_11R   [NULL_CODE] = "11R";   /* Markup Retail */
const char GLMT_12R   [NULL_CODE] = "12R";   /* Markup Cancel Retail  */
const char GLMT_16R   [NULL_CODE] = "16R";   /* Clear Markdown Retail */
const char GLMT_13R   [NULL_CODE] = "13R";   /* Perm Markup Retail  */
const char GLMT_15R   [NULL_CODE] = "15R";   /* Prom Markdown Retail */
const char GLMT_14R   [NULL_CODE] = "14R";   /* Markdown Cancel Retail */
const char GLMT_51C   [NULL_CODE] = "51C";   /* Shrinkage Cost */
const char GLMT_51R   [NULL_CODE] = "51R";   /* Shrinkage Retail */
const char GLMT_60R   [NULL_CODE] = "60R";   /* EMPL discount Retail */
const char GLMT_80C   [NULL_CODE] = "80C";   /* Workroom Amount */
const char GLMT_81R   [NULL_CODE] = "81R";   /* Cash discount Amount */
const char GLMT_52R   [NULL_CODE] = "52R";   /* CLS STK Retail */
const char GLMT_52C   [NULL_CODE] = "52C";   /* CLS STK Cost */
const char GLMT_53R   [NULL_CODE] = "53R";   /* Gross Margin Amount */
const char GLMT_70C   [NULL_CODE] = "70C";   /* Cost Variance Amount */
const char GLMT_54R   [NULL_CODE] = "54R";   /* HTD GAFS Retail */
const char GLMT_54C   [NULL_CODE] = "54C";   /* HTD GAFS Cost */
const char GLMT_55C   [NULL_CODE] = "55C";   /* Inter stocktake sales amount */
const char GLMT_56C   [NULL_CODE] = "56C";   /* Inter stocktake shrinkage amount */
const char GLMT_57C   [NULL_CODE] = "57C";   /* Stocktake MTD sales amount */
const char GLMT_58C   [NULL_CODE] = "58C";   /* Stocktake MTD shrink amount */
const char GLMT_59R   [NULL_CODE] = "59R";   /* Stocktake BOOKSTK Retail */
const char GLMT_59C   [NULL_CODE] = "59C";   /* Stocktake BOOKSTK Cost */
const char GLMT_61R   [NULL_CODE] = "61R";   /* Stocktake ACTSTK Retail */
const char GLMT_61C   [NULL_CODE] = "61C";   /* Stocktake ACTSTK Cost */
const char GLMT_34C   [NULL_CODE] = "34C";   /* Reclass in Cost */
const char GLMT_34R   [NULL_CODE] = "34R";   /* Reclass in Retail */
const char GLMT_36C   [NULL_CODE] = "36C";   /* Reclass out Cost */
const char GLMT_36R   [NULL_CODE] = "36R";   /* Reclass out Retail */
const char GLMT_62C   [NULL_CODE] = "62C";   /* Freight Claim Cost */
const char GLMT_62R   [NULL_CODE] = "62R";   /* Freight Claim Retail */
const char GLMT_23C   [NULL_CODE] = "23C";   /* Stock Adj Cogs Cost */
const char GLMT_23R   [NULL_CODE] = "23R";   /* Stock Adj Cogs Retail */
const char GLMT_17R   [NULL_CODE] = "17R";   /* Intercompany Markup Retail */
const char GLMT_18R   [NULL_CODE] = "18R";   /* Intercompany Markdown Retail */
const char GLMT_37C   [NULL_CODE] = "37C";   /* Intercompany in Cost */
const char GLMT_37R   [NULL_CODE] = "37R";   /* Intercompany in Retail */
const char GLMT_38C   [NULL_CODE] = "38C";   /* Intercompany out Cost */
const char GLMT_38R   [NULL_CODE] = "38R";   /* Intercompany out Retail */
const char GLMT_63C   [NULL_CODE] = "63C";   /* WO activity UPD INV Cost */
const char GLMT_64C   [NULL_CODE] = "64C";   /* WO activity POST FIN Cost */
const char GLMT_39R   [NULL_CODE] = "39R";   /* Intercompany Margin */
const char GLMT_82R   [NULL_CODE] = "82R";   /* @SUH4@ Sales Retail */
const char GLMT_82C   [NULL_CODE] = "82C";   /* @SUH4@ Sales Cost */
const char GLMT_83R   [NULL_CODE] = "83R";   /* @SUH4@ Returns Retail */
const char GLMT_83C   [NULL_CODE] = "83C";   /* @SUH4@ Returns Cost */
const char GLMT_84R   [NULL_CODE] = "84R";   /* @SUH4@ Markups */
const char GLMT_85R   [NULL_CODE] = "85R";   /* @SUH4@ Markdowns */
const char GLMT_86C   [NULL_CODE] = "86C";   /* @SUH4@ Restocking Fee */
const char GLMT_87C   [NULL_CODE] = "87C";   /* Vat In Cost */
const char GLMT_88R   [NULL_CODE] = "88R";   /* Vat Out Retail */
const char GLMT_10R   [NULL_CODE] = "10R";   /* Weight Variance Retail */
const char GLMT_74C   [NULL_CODE] = "74C";   /* Recoverable Tax at destination */
const char GLMT_75C   [NULL_CODE] = "75C";   /* Recoverable Tax at source */
const char GLMT_03R   [NULL_CODE] = "03R";   /* Net Sales Non Inv Retail */
const char GLMT_05R   [NULL_CODE] = "05R";   /* Net Sales Non Inv Rtl ex VAT */
const char GLMT_03C   [NULL_CODE] = "03C";   /* Net Sales Non Inv Cost */
const char GLMT_73C   [NULL_CODE] = "73C";   /* Rec Cost Adjust Variance FIFO */
const char GLMT_29C   [NULL_CODE] = "29C";   /* GL Interface - Expense Up Charge (Cost) */
const char GLMT_28C   [NULL_CODE] = "28C";   /* GL Interface - Profit Up Charge (Cost) */
const char GLMT_31C   [NULL_CODE] = "31C";   /* GL Interface - Book Transfers In (Cost) */
const char GLMT_33C   [NULL_CODE] = "33C";   /* GL Interface - Book Transfers Out (Cost) */
const char GLMT_31R   [NULL_CODE] = "31R";   /* GL Interface - Book Tsf In (Retail) */
const char GLMT_33R   [NULL_CODE] = "33R";   /* GL Interface - Book Tsf Out (Retail) */


/* GLRL = General Ledger Roll Up */
const char GLRL[NULL_CODE] = "GLRL";

const char GLRL_D     [NULL_CODE] = "D";   /* @MH4@ */
const char GLRL_C     [NULL_CODE] = "C";   /* @MH5@ */
const char GLRL_S     [NULL_CODE] = "S";   /* @MH6@ */


/* GLRT = General Ledger Rolled Transactions */
const char GLRT[NULL_CODE] = "GLRT";

const char GLRT_01C   [NULL_CODE] = "01C";   /* GL Interface - Net Sales (Cost) */
const char GLRT_04R   [NULL_CODE] = "04R";   /* GL Interface - Markup (Retail) */
const char GLRT_15R   [NULL_CODE] = "15R";   /* GL Interface - Promo Markdown (Retail) */
const char GLRT_17R   [NULL_CODE] = "17R";   /* GL Interface - Intercompany Markup */
const char GLRT_18R   [NULL_CODE] = "18R";   /* GL Interface - Intercompany Markdown */
const char GLRT_20C   [NULL_CODE] = "20C";   /* GL Interface - Purchases (Cost) */
const char GLRT_22C   [NULL_CODE] = "22C";   /* GL Interface - Stock Adj Non-COGS (Cost) */
const char GLRT_23C   [NULL_CODE] = "23C";   /* GL Interface - Stock Adj COGS (Cost) */
const char GLRT_28C   [NULL_CODE] = "28C";   /* GL Interface - Profit Up Charge (Cost) */
const char GLRT_29C   [NULL_CODE] = "29C";   /* GL Interface - Expense Up Charge (Cost) */
const char GLRT_30C   [NULL_CODE] = "30C";   /* GL Interface - Transfers In (Cost) */
const char GLRT_31C   [NULL_CODE] = "31C";   /* GL Interface - Book Transfers In (Cost) */
const char GLRT_32C   [NULL_CODE] = "32C";   /* GL Interface - Transfers Out (Cost) */
const char GLRT_33C   [NULL_CODE] = "33C";   /* GL Interface - Book Transfers Out (Cost) */
const char GLRT_37C   [NULL_CODE] = "37C";   /* GL Interface - Intercompany In (Cost) */
const char GLRT_37R   [NULL_CODE] = "37R";   /* GL Interface - Intercompany In (Retail) */
const char GLRT_38C   [NULL_CODE] = "38C";   /* GL Interface - Intercompany Out (Cost) */
const char GLRT_38R   [NULL_CODE] = "38R";   /* GL Interface - Intercompany Out (Retail) */
const char GLRT_62C   [NULL_CODE] = "62C";   /* GL Interface - Freight Claim (Cost) */
const char GLRT_63C   [NULL_CODE] = "63C";   /* GL Interface - WO Activity - Update Inv */
const char GLRT_64C   [NULL_CODE] = "64C";   /* GL Interface - WO Activity - Post to Fin */
const char GLRT_70C   [NULL_CODE] = "70C";   /* GL Interface - Cost Variance (Cost) */
const char GLRT_01R   [NULL_CODE] = "01R";   /* GL Intf - Net Sales (Retail) */
const char GLRT_02R   [NULL_CODE] = "02R";   /* GL Intf - Net Sales ex VAT (Retail) */
const char GLRT_03C   [NULL_CODE] = "03C";   /* GL Intf - Non-Inv Net Sales (Cost) */
const char GLRT_03R   [NULL_CODE] = "03R";   /* GL Intf - Non-Inv Net Sales (Retail) */
const char GLRT_04C   [NULL_CODE] = "04C";   /* GL Intf - Returns (Cost) */
const char GLRT_05C   [NULL_CODE] = "05C";   /* GL Intf - Non-Inv Net Sales ex VAT (C) */
const char GLRT_05R   [NULL_CODE] = "05R";   /* GL Intf - Non-Inv Net Sales ex VAT (R) */
const char GLRT_06R   [NULL_CODE] = "06R";   /* GL Intf - Deal Income (Sales) */
const char GLRT_07C   [NULL_CODE] = "07C";   /* GL Intf - Deal Income (Purchase) */
const char GLRT_08R   [NULL_CODE] = "08R";   /* GL Intf - Fixed Income Accrual */
const char GLRT_10R   [NULL_CODE] = "10R";   /* GL Intf - Weight Variance (Retail) */
const char GLRT_11R   [NULL_CODE] = "11R";   /* GL Intf - Markup (Retail) */
const char GLRT_12R   [NULL_CODE] = "12R";   /* GL Intf - Markup Cancel Retail  */
const char GLRT_13R   [NULL_CODE] = "13R";   /* GL Intf - Perm Markup Retail  */
const char GLRT_14R   [NULL_CODE] = "14R";   /* GL Intf - Markdown Cancel (Retail) */
const char GLRT_16R   [NULL_CODE] = "16R";   /* GL Intf - Clear Markdown (Retail) */
const char GLRT_20R   [NULL_CODE] = "20R";   /* GL Intf - Purch (Retail) */
const char GLRT_22R   [NULL_CODE] = "22R";   /* GL Intf - Stock Adj (Retail) */
const char GLRT_23R   [NULL_CODE] = "23R";   /* GL Intf - Stock Adj Cogs (Retail) */
const char GLRT_24C   [NULL_CODE] = "24C";   /* GL Intf - RTV (Cost) */
const char GLRT_24R   [NULL_CODE] = "24R";   /* GL Intf - RTV (Retail) */
const char GLRT_26C   [NULL_CODE] = "26C";   /* GL Intf - Freight (Cost) */
const char GLRT_28R   [NULL_CODE] = "28R";   /* GL Intf - Profit Up Charge- Rec loc */
const char GLRT_29R   [NULL_CODE] = "29R";   /* GL Intf - Expense Up Charges -Rec loc */
const char GLRT_30R   [NULL_CODE] = "30R";   /* GL Intf - TSF in (Retail) */
const char GLRT_32R   [NULL_CODE] = "32R";   /* GL Intf - TSF Out (Retail) */
const char GLRT_34C   [NULL_CODE] = "34C";   /* GL Intf - Reclass in (Cost) */
const char GLRT_34R   [NULL_CODE] = "34R";   /* GL Intf - Reclass in (Retail) */
const char GLRT_36C   [NULL_CODE] = "36C";   /* GL Intf - Reclass out (Cost) */
const char GLRT_36R   [NULL_CODE] = "36R";   /* GL Intf - Reclass out (Retail) */
const char GLRT_39R   [NULL_CODE] = "39R";   /* GL Intf - Intercompany Margin */
const char GLRT_60R   [NULL_CODE] = "60R";   /* GL Intf - EMPL discount (Retail) */
const char GLRT_61C   [NULL_CODE] = "61C";   /* GL Intf - Stocktake ACTSTK (Cost) */
const char GLRT_61R   [NULL_CODE] = "61R";   /* GL Intf - Stocktake ACTSTK (Retail) */
const char GLRT_62R   [NULL_CODE] = "62R";   /* GL Intf - Freight Claim (Retail) */
const char GLRT_65C   [NULL_CODE] = "65C";   /* GL Intf - Restocking Fee */
const char GLRT_71C   [NULL_CODE] = "71C";   /* GL Intf - Cost Var Amt- Cost Mthd */
const char GLRT_72R   [NULL_CODE] = "72R";   /* GL Intf - Cost Var Amt- Retail Mthd */
const char GLRT_73C   [NULL_CODE] = "73C";   /* GL Intf - Cost Var Amt- Cost Mthd-FIFO */
const char GLRT_80C   [NULL_CODE] = "80C";   /* GL Intf - Workroom Amount */
const char GLRT_81R   [NULL_CODE] = "81R";   /* GL Intf - Cash discount Amount */
const char GLRT_82C   [NULL_CODE] = "82C";   /* GL Intf - @SUH4@ Sales (Cost) */
const char GLRT_82R   [NULL_CODE] = "82R";   /* GL Intf - @SUH4@ Sales (Retail) */
const char GLRT_83C   [NULL_CODE] = "83C";   /* GL Intf - @SUH4@ Returns (Cost) */
const char GLRT_83R   [NULL_CODE] = "83R";   /* GL Intf - @SUH4@ Returns (Retail) */
const char GLRT_84R   [NULL_CODE] = "84R";   /* GL Intf - @SUH4@ Markups */
const char GLRT_85R   [NULL_CODE] = "85R";   /* GL Intf - @SUH4@ Markdowns */
const char GLRT_86C   [NULL_CODE] = "86C";   /* GL Intf - @SUH4@ Restocking Fee */
const char GLRT_87C   [NULL_CODE] = "87C";   /* GL Intf - Vat In (Cost) */
const char GLRT_88R   [NULL_CODE] = "88R";   /* GL Intf - Vat Out (Retail) */
const char GLRT_74C   [NULL_CODE] = "74C";   /* Recoverable Tax at destination */
const char GLRT_75C   [NULL_CODE] = "75C";   /* Recoverable Tax at source */


/* GLST = General Ledger Single Transactions */
const char GLST[NULL_CODE] = "GLST";

const char GLST_01C   [NULL_CODE] = "01C";   /* GL Interface - Net Sales (Cost) */
const char GLST_04R   [NULL_CODE] = "04R";   /* GL Interface - Markup (Retail) */
const char GLST_15R   [NULL_CODE] = "15R";   /* GL Interface - Promo Markdown (Retail) */
const char GLST_17R   [NULL_CODE] = "17R";   /* GL Interface - Intercompany Markup */
const char GLST_18R   [NULL_CODE] = "18R";   /* GL Interface - Intercompany Markdown */
const char GLST_20C   [NULL_CODE] = "20C";   /* GL Interface - Purchases (Cost) */
const char GLST_22C   [NULL_CODE] = "22C";   /* GL Interface - Stock Adj Non-COGS (Cost) */
const char GLST_23C   [NULL_CODE] = "23C";   /* GL Interface - Stock Adj COGS (Cost) */
const char GLST_28C   [NULL_CODE] = "28C";   /* GL Interface - Profit Up Charge (Cost) */
const char GLST_29C   [NULL_CODE] = "29C";   /* GL Interface - Expense Up Charge (Cost) */
const char GLST_30C   [NULL_CODE] = "30C";   /* GL Interface - Transfers In (Cost) */
const char GLST_31C   [NULL_CODE] = "31C";   /* GL Interface - Book Transfers In (Cost) */
const char GLST_32C   [NULL_CODE] = "32C";   /* GL Interface - Transfers Out (Cost) */
const char GLST_33C   [NULL_CODE] = "33C";   /* GL Interface - Book Transfers Out (Cost) */
const char GLST_37C   [NULL_CODE] = "37C";   /* GL Interface - Intercompany In (Cost) */
const char GLST_37R   [NULL_CODE] = "37R";   /* GL Interface - Intercompany In (Retail) */
const char GLST_38C   [NULL_CODE] = "38C";   /* GL Interface - Intercompany Out (Cost) */
const char GLST_38R   [NULL_CODE] = "38R";   /* GL Interface - Intercompany Out (Retail) */
const char GLST_62C   [NULL_CODE] = "62C";   /* GL Interface - Freight Claim (Cost) */
const char GLST_63C   [NULL_CODE] = "63C";   /* GL Interface - WO Activity - Update Inv */
const char GLST_64C   [NULL_CODE] = "64C";   /* GL Interface - WO Activity - Post to Fin */
const char GLST_70C   [NULL_CODE] = "70C";   /* GL Interface - Cost Variance (Cost) */
const char GLST_01R   [NULL_CODE] = "01R";   /* GL Intf - Net Sales (Retail) */
const char GLST_02R   [NULL_CODE] = "02R";   /* GL Intf - Net Sales Retail ex VAT */
const char GLST_03C   [NULL_CODE] = "03C";   /* GL Intf - Non-Inv Net Sales (Cost) */
const char GLST_03R   [NULL_CODE] = "03R";   /* GL Intf - Non-Inv Net Sales (Retail) */
const char GLST_04C   [NULL_CODE] = "04C";   /* GL Intf - Returns (Cost) */
const char GLST_05C   [NULL_CODE] = "05C";   /* GL Intf - Non-Inv Net Sales ex VAT (C) */
const char GLST_05R   [NULL_CODE] = "05R";   /* GL Intf - Non-Inv Net Sales ex VAT (R) */
const char GLST_06R   [NULL_CODE] = "06R";   /* GL Intf - Deal Income (Sales) */
const char GLST_07C   [NULL_CODE] = "07C";   /* GL Intf - Deal Income (Purchase) */
const char GLST_08R   [NULL_CODE] = "08R";   /* GL Intf - Fixed Income Accrual */
const char GLST_10R   [NULL_CODE] = "10R";   /* GL Intf - Weight Variance (Retail) */
const char GLST_11R   [NULL_CODE] = "11R";   /* GL Intf - Markup (Retail) */
const char GLST_12R   [NULL_CODE] = "12R";   /* GL Intf - Markup Cancel Retail  */
const char GLST_13R   [NULL_CODE] = "13R";   /* GL Intf - Perm Markup Retail  */
const char GLST_14R   [NULL_CODE] = "14R";   /* GL Intf - Markdown Cancel (Retail) */
const char GLST_16R   [NULL_CODE] = "16R";   /* GL Intf - Clear Markdown (Retail) */
const char GLST_20R   [NULL_CODE] = "20R";   /* GL Intf - Purch (Retail) */
const char GLST_22R   [NULL_CODE] = "22R";   /* GL Intf - Stock Adj (Retail) */
const char GLST_23R   [NULL_CODE] = "23R";   /* GL Intf - Stock Adj Cogs (Retail) */
const char GLST_24C   [NULL_CODE] = "24C";   /* GL Intf - RTV (Cost) */
const char GLST_24R   [NULL_CODE] = "24R";   /* GL Intf - RTV (Retail) */
const char GLST_26C   [NULL_CODE] = "26C";   /* GL Intf - Freight (Cost) */
const char GLST_28R   [NULL_CODE] = "28R";   /* GL Intf - Profit Up Charge - Rec loc */
const char GLST_29R   [NULL_CODE] = "29R";   /* GL Intf - Expense Up Charges - Rec loc */
const char GLST_30R   [NULL_CODE] = "30R";   /* GL Intf - TSF in (Retail) */
const char GLST_32R   [NULL_CODE] = "32R";   /* GL Intf - TSF Out (Retail) */
const char GLST_34C   [NULL_CODE] = "34C";   /* GL Intf - Reclass in (Cost) */
const char GLST_34R   [NULL_CODE] = "34R";   /* GL Intf - Reclass in (Retail) */
const char GLST_36C   [NULL_CODE] = "36C";   /* GL Intf - Reclass out (Cost) */
const char GLST_36R   [NULL_CODE] = "36R";   /* GL Intf - Reclass out (Retail) */
const char GLST_39R   [NULL_CODE] = "39R";   /* GL Intf - Intercompany Margin */
const char GLST_60R   [NULL_CODE] = "60R";   /* GL Intf - EMPL discount (Retail) */
const char GLST_61C   [NULL_CODE] = "61C";   /* GL Intf - Stocktake ACTSTK (Cost) */
const char GLST_61R   [NULL_CODE] = "61R";   /* GL Intf - Stocktake ACTSTK (Retail) */
const char GLST_62R   [NULL_CODE] = "62R";   /* GL Intf - Freight Claim (Retail) */
const char GLST_65C   [NULL_CODE] = "65C";   /* GL Intf - Restocking Fee */
const char GLST_71C   [NULL_CODE] = "71C";   /* GL Intf - Cost Var Amt- Cost Mthd */
const char GLST_72R   [NULL_CODE] = "72R";   /* GL Intf - Cost Var Amt- Retail Mthd */
const char GLST_73C   [NULL_CODE] = "73C";   /* GL Intf - Cost Var Amt- Cost Mthd-FIFO */
const char GLST_80C   [NULL_CODE] = "80C";   /* GL Intf - Workroom Amount */
const char GLST_81R   [NULL_CODE] = "81R";   /* GL Intf - Cash discount Amount */
const char GLST_82C   [NULL_CODE] = "82C";   /* GL Intf - @SUH4@ Sales (Cost) */
const char GLST_82R   [NULL_CODE] = "82R";   /* GL Intf - @SUH4@ Sales (Retail) */
const char GLST_83C   [NULL_CODE] = "83C";   /* GL Intf - @SUH4@ Returns (Cost) */
const char GLST_83R   [NULL_CODE] = "83R";   /* GL Intf - @SUH4@ Returns (Retail) */
const char GLST_84R   [NULL_CODE] = "84R";   /* GL Intf - @SUH4@ Markups */
const char GLST_85R   [NULL_CODE] = "85R";   /* GL Intf - @SUH4@ Markdowns */
const char GLST_86C   [NULL_CODE] = "86C";   /* GL Intf - @SUH4@ Restocking Fee */
const char GLST_87C   [NULL_CODE] = "87C";   /* GL Intf - Vat In (Cost) */
const char GLST_88R   [NULL_CODE] = "88R";   /* GL Intf - Vat Out (Retail) */
const char GLST_74C   [NULL_CODE] = "74C";   /* Recoverable Tax at destination */
const char GLST_75C   [NULL_CODE] = "75C";   /* Recoverable Tax at source */


/* GRLB = Group form labels */
const char GRLB[NULL_CODE] = "GRLB";

const char GRLB_B     [NULL_CODE] = "B";   /* Buyer */
const char GRLB_M     [NULL_CODE] = "M";   /* Merchandiser */


/* GRSW = Repl Attr Group Type - ST/WH */
const char GRSW[NULL_CODE] = "GRSW";

const char GRSW_S     [NULL_CODE] = "S";   /* Store */
const char GRSW_W     [NULL_CODE] = "W";   /* Warehouse */


/* GRT1 = Group Type 1 */
const char GRT1[NULL_CODE] = "GRT1";

const char GRT1_AL    [NULL_CODE] = "AL";   /* All Locations */
const char GRT1_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GRT1_S     [NULL_CODE] = "S";   /* Store */
const char GRT1_C     [NULL_CODE] = "C";   /* Store Class */
const char GRT1_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRT1_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRT1_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GRT1_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRT1_L     [NULL_CODE] = "L";   /* Location Trait */
const char GRT1_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GRT1_LLS   [NULL_CODE] = "LLS";   /* Location List-Store */
const char GRT1_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GRT1_W     [NULL_CODE] = "W";   /* Warehouse */
const char GRT1_LLW   [NULL_CODE] = "LLW";   /* Location List-Warehouse */
const char GRT1_E     [NULL_CODE] = "E";   /* External Finisher */
const char GRT1_AE    [NULL_CODE] = "AE";   /* All External Finishers */


/* GRT2 = Group Type Short List */
const char GRT2[NULL_CODE] = "GRT2";

const char GRT2_S     [NULL_CODE] = "S";   /* Store */
const char GRT2_C     [NULL_CODE] = "C";   /* Store Class */
const char GRT2_W     [NULL_CODE] = "W";   /* Warehouse */
const char GRT2_A     [NULL_CODE] = "A";   /* All Stores */
const char GRT2_H     [NULL_CODE] = "H";   /* All Warehouses */
const char GRT2_LLS   [NULL_CODE] = "LLS";   /* Location List - Store */
const char GRT2_LLW   [NULL_CODE] = "LLW";   /* Location List - Warehouse */


/* GRT3 = Group type 3 */
const char GRT3[NULL_CODE] = "GRT3";

const char GRT3_S     [NULL_CODE] = "S";   /* Store */
const char GRT3_C     [NULL_CODE] = "C";   /* Store Class */
const char GRT3_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRT3_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRT3_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRT3_L     [NULL_CODE] = "L";   /* Location Trait */
const char GRT3_DW    [NULL_CODE] = "DW";   /* Default Wh */
const char GRT3_A     [NULL_CODE] = "A";   /* All Stores */


/* GRT4 = Group Type 4 */
const char GRT4[NULL_CODE] = "GRT4";

const char GRT4_AL    [NULL_CODE] = "AL";   /* All Locations */
const char GRT4_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GRT4_S     [NULL_CODE] = "S";   /* Store */
const char GRT4_C     [NULL_CODE] = "C";   /* Store Class */
const char GRT4_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRT4_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRT4_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GRT4_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRT4_L     [NULL_CODE] = "L";   /* Location Trait */
const char GRT4_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GRT4_W     [NULL_CODE] = "W";   /* Warehouse */
const char GRT4_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GRT4_LLS   [NULL_CODE] = "LLS";   /* Location List-Store */
const char GRT4_LLW   [NULL_CODE] = "LLW";   /* Location List-Warehouse */


/* GRT5 = Group Type 5 */
const char GRT5[NULL_CODE] = "GRT5";

const char GRT5_S     [NULL_CODE] = "S";   /* Store */
const char GRT5_C     [NULL_CODE] = "C";   /* Store Class */
const char GRT5_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRT5_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRT5_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRT5_L     [NULL_CODE] = "L";   /* Location Trait */
const char GRT5_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GRT5_A     [NULL_CODE] = "A";   /* All Stores */


/* GRT6 = Group Type 6 */
const char GRT6[NULL_CODE] = "GRT6";

const char GRT6_S     [NULL_CODE] = "S";   /* Store */
const char GRT6_C     [NULL_CODE] = "C";   /* Store Class */
const char GRT6_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRT6_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRT6_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRT6_L     [NULL_CODE] = "L";   /* Location Trait */
const char GRT6_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GRT6_A     [NULL_CODE] = "A";   /* All Stores */
const char GRT6_LL    [NULL_CODE] = "LL";   /* Location List */


/* GRT7 = Group Type Store Add */
const char GRT7[NULL_CODE] = "GRT7";

const char GRT7_S     [NULL_CODE] = "S";   /* Store */
const char GRT7_C     [NULL_CODE] = "C";   /* Store Class */
const char GRT7_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRT7_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRT7_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRT7_L     [NULL_CODE] = "L";   /* Location Traits */
const char GRT7_A     [NULL_CODE] = "A";   /* All Stores */
const char GRT7_LL    [NULL_CODE] = "LL";   /* Location List */


/* GRT8 = Repl Attribute Group Type - Stores */
const char GRT8[NULL_CODE] = "GRT8";

const char GRT8_S     [NULL_CODE] = "S";   /* Store */
const char GRT8_C     [NULL_CODE] = "C";   /* Store Class */
const char GRT8_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRT8_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRT8_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRT8_L     [NULL_CODE] = "L";   /* Location Trait */
const char GRT8_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GRT8_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GRT8_LLS   [NULL_CODE] = "LLS";   /* Location List-Store */


/* GRT9 = Repl Attribute Group Type - Warehouse */
const char GRT9[NULL_CODE] = "GRT9";

const char GRT9_W     [NULL_CODE] = "W";   /* Warehouse */
const char GRT9_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GRT9_LLW   [NULL_CODE] = "LLW";   /* Location List-Warehouse */


/* GRTA = Group Type 1 without Finishers */
const char GRTA[NULL_CODE] = "GRTA";

const char GRTA_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GRTA_S     [NULL_CODE] = "S";   /* Store */
const char GRTA_C     [NULL_CODE] = "C";   /* Store Class */
const char GRTA_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GRTA_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GRTA_W     [NULL_CODE] = "W";   /* Warehouse */
const char GRTA_LLS   [NULL_CODE] = "LLS";   /* Location List - Store */
const char GRTA_LLW   [NULL_CODE] = "LLW";   /* Location List - Warehouse */
const char GRTA_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRTA_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRTA_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GRTA_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRTA_L     [NULL_CODE] = "L";   /* Location Trait */


/* GRTC = COST CHANGE BY LOCATION - LOC TYPE */
const char GRTC[NULL_CODE] = "GRTC";

const char GRTC_AL    [NULL_CODE] = "AL";   /* All Locations */
const char GRTC_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GRTC_S     [NULL_CODE] = "S";   /* Store */
const char GRTC_C     [NULL_CODE] = "C";   /* Store Class */
const char GRTC_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRTC_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GRTC_L     [NULL_CODE] = "L";   /* Location Trait */
const char GRTC_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GRTC_LLS   [NULL_CODE] = "LLS";   /* Location List-Store */
const char GRTC_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GRTC_W     [NULL_CODE] = "W";   /* Warehouse */
const char GRTC_LLW   [NULL_CODE] = "LLW";   /* Location List-Warehouse */


/* GRTP = Group Types - Price History and Margin */
const char GRTP[NULL_CODE] = "GRTP";

const char GRTP_A     [NULL_CODE] = "A";   /* All Locations */
const char GRTP_S     [NULL_CODE] = "S";   /* Single Location */
const char GRTP_L     [NULL_CODE] = "L";   /* Location List */


/* GRTV = Group Type Virtual */
const char GRTV[NULL_CODE] = "GRTV";

const char GRTV_AL    [NULL_CODE] = "AL";   /* All Locations */
const char GRTV_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GRTV_S     [NULL_CODE] = "S";   /* Store */
const char GRTV_C     [NULL_CODE] = "C";   /* Store Class */
const char GRTV_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRTV_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRTV_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GRTV_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRTV_L     [NULL_CODE] = "L";   /* Location Trait */
const char GRTV_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GRTV_LLS   [NULL_CODE] = "LLS";   /* Location List - Store */
const char GRTV_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GRTV_W     [NULL_CODE] = "W";   /* Warehouse */
const char GRTV_PW    [NULL_CODE] = "PW";   /* Physical Warehouse */
const char GRTV_LLW   [NULL_CODE] = "LLW";   /* Location List - Warehouse */


/* GRTY = Group Type */
const char GRTY[NULL_CODE] = "GRTY";

const char GRTY_S     [NULL_CODE] = "S";   /* Store */
const char GRTY_C     [NULL_CODE] = "C";   /* Store Class */
const char GRTY_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GRTY_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GRTY_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GRTY_L     [NULL_CODE] = "L";   /* Location Traits */
const char GRTY_A     [NULL_CODE] = "A";   /* All Stores */
const char GRTY_LL    [NULL_CODE] = "LL";   /* Location List */


/* GT1E = Group Type 1 + External Finishers */
const char GT1E[NULL_CODE] = "GT1E";

const char GT1E_AL    [NULL_CODE] = "AL";   /* All Locations */
const char GT1E_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GT1E_S     [NULL_CODE] = "S";   /* Store */
const char GT1E_C     [NULL_CODE] = "C";   /* Store Class */
const char GT1E_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GT1E_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GT1E_W     [NULL_CODE] = "W";   /* Warehouse */
const char GT1E_AE    [NULL_CODE] = "AE";   /* All External Finishers */
const char GT1E_E     [NULL_CODE] = "E";   /* External Finisher */
const char GT1E_L     [NULL_CODE] = "L";   /* Location Trait */
const char GT1E_LLS   [NULL_CODE] = "LLS";   /* Location List - Store */
const char GT1E_LLW   [NULL_CODE] = "LLW";   /* Location List - Warehouse */
const char GT1E_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GT1E_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GT1E_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GT1E_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GT1E_ULOU  [NULL_CODE] = "ULOU";   /* Unmatched Location Org Units */


/* GT3B = Group Type 3 B */
const char GT3B[NULL_CODE] = "GT3B";

const char GT3B_S     [NULL_CODE] = "S";   /* Store */
const char GT3B_C     [NULL_CODE] = "C";   /* Store Class */
const char GT3B_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GT3B_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GT3B_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GT3B_L     [NULL_CODE] = "L";   /* Location Trait */
const char GT3B_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GT3B_A     [NULL_CODE] = "A";   /* All Stores */
const char GT3B_LL    [NULL_CODE] = "LL";   /* Location List */


/* GT6I = Group Type 6 + Internal Finisher */
const char GT6I[NULL_CODE] = "GT6I";


/* GTMR = Group Type - MRTs */
const char GTMR[NULL_CODE] = "GTMR";

const char GTMR_S     [NULL_CODE] = "S";   /* Store */
const char GTMR_W     [NULL_CODE] = "W";   /* Warehouse */
const char GTMR_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GTMR_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GTMR_LLS   [NULL_CODE] = "LLS";   /* Location List - Store */
const char GTMR_LLW   [NULL_CODE] = "LLW";   /* Location List - Warehouse */
const char GTMR_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GTMR_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GTMR_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GTMR_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GTMR_L     [NULL_CODE] = "L";   /* Location Trait */
const char GTMR_C     [NULL_CODE] = "C";   /* Store Class */


/* GTSE = Group Type Short List + Ext Finishers */
const char GTSE[NULL_CODE] = "GTSE";

const char GTSE_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GTSE_S     [NULL_CODE] = "S";   /* Store */
const char GTSE_C     [NULL_CODE] = "C";   /* Store Class */
const char GTSE_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GTSE_W     [NULL_CODE] = "W";   /* Warehouse */
const char GTSE_AE    [NULL_CODE] = "AE";   /* All External Finishers */
const char GTSE_E     [NULL_CODE] = "E";   /* External Finisher */
const char GTSE_LLS   [NULL_CODE] = "LLS";   /* Location List - Store */
const char GTSE_LLW   [NULL_CODE] = "LLW";   /* Location List - Warehouse */


/* GTVF = Group Type Virtual + Finishers */
const char GTVF[NULL_CODE] = "GTVF";

const char GTVF_AL    [NULL_CODE] = "AL";   /* All Locations */
const char GTVF_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GTVF_S     [NULL_CODE] = "S";   /* Store */
const char GTVF_C     [NULL_CODE] = "C";   /* Store Class */
const char GTVF_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GTVF_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GTVF_W     [NULL_CODE] = "W";   /* Warehouse */
const char GTVF_PW    [NULL_CODE] = "PW";   /* Physical Warehouse */
const char GTVF_AI    [NULL_CODE] = "AI";   /* All Internal Finishers */
const char GTVF_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char GTVF_AE    [NULL_CODE] = "AE";   /* All External Finishers */
const char GTVF_E     [NULL_CODE] = "E";   /* External Finisher */
const char GTVF_LLS   [NULL_CODE] = "LLS";   /* Location List - Store */
const char GTVF_LLW   [NULL_CODE] = "LLW";   /* Location List - Warehouse */
const char GTVF_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GTVF_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GTVF_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GTVF_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GTVF_L     [NULL_CODE] = "L";   /* Location Trait */
const char GTVF_ULOU  [NULL_CODE] = "ULOU";   /* Unmatched Location Org Units */


/* GTVI = Group Type with Internal Finishers */
const char GTVI[NULL_CODE] = "GTVI";

const char GTVI_AL    [NULL_CODE] = "AL";   /* All Locations */
const char GTVI_AS    [NULL_CODE] = "AS";   /* All Stores */
const char GTVI_S     [NULL_CODE] = "S";   /* Store */
const char GTVI_C     [NULL_CODE] = "C";   /* Store Class */
const char GTVI_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GTVI_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char GTVI_W     [NULL_CODE] = "W";   /* Warehouse */
const char GTVI_PW    [NULL_CODE] = "PW";   /* Physical Warehouse */
const char GTVI_AI    [NULL_CODE] = "AI";   /* All Internal Finishers */
const char GTVI_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char GTVI_LLS   [NULL_CODE] = "LLS";   /* Location List - Store */
const char GTVI_LLW   [NULL_CODE] = "LLW";   /* Location List - Warehouse */
const char GTVI_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GTVI_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GTVI_A     [NULL_CODE] = "A";   /* @OH3@ */
const char GTVI_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GTVI_L     [NULL_CODE] = "L";   /* Location Trait */


/* GTY4 = Group Type 4 */
const char GTY4[NULL_CODE] = "GTY4";

const char GTY4_S     [NULL_CODE] = "S";   /* Store */
const char GTY4_C     [NULL_CODE] = "C";   /* Store Class */
const char GTY4_D     [NULL_CODE] = "D";   /* @OH5@ */
const char GTY4_R     [NULL_CODE] = "R";   /* @OH4@ */
const char GTY4_MA    [NULL_CODE] = "MA";   /* Min Area */
const char GTY4_MX    [NULL_CODE] = "MX";   /* Max Area */
const char GTY4_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char GTY4_L     [NULL_CODE] = "L";   /* Location Trait */
const char GTY4_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char GTY4_A     [NULL_CODE] = "A";   /* All Stores */


/* HSEN = Handling Sensitivity */
const char HSEN[NULL_CODE] = "HSEN";

const char HSEN_FRAG  [NULL_CODE] = "FRAG";   /* Fragile */
const char HSEN_EXPLOD[NULL_CODE] = "EXPLOD";   /* Explosive */
const char HSEN_COMPUS[NULL_CODE] = "COMPUS";   /* Combustable */
const char HSEN_AERO  [NULL_CODE] = "AERO";   /* Aerosol Container - flammable */
const char HSEN_TOXIC [NULL_CODE] = "TOXIC";   /* Toxic */


/* HTDS = HTS Description */
const char HTDS[NULL_CODE] = "HTDS";


/* HTMP = Handling Temperature */
const char HTMP[NULL_CODE] = "HTMP";

const char HTMP_ROOM  [NULL_CODE] = "ROOM";   /* Keep at Room Temperature */
const char HTMP_FROZEN[NULL_CODE] = "FROZEN";   /* Keep Frozen */
const char HTMP_FRIDGE[NULL_CODE] = "FRIDGE";   /* Keep Refrigerated */
const char HTMP_HOT   [NULL_CODE] = "HOT";   /* Keep Hot */


/* HTSF = HTS Fee or Tax */
const char HTSF[NULL_CODE] = "HTSF";

const char HTSF_F     [NULL_CODE] = "F";   /* Fee */
const char HTSF_T     [NULL_CODE] = "T";   /* Tax */


/* HTSL = HTS Tracking Level */
const char HTSL[NULL_CODE] = "HTSL";

const char HTSL_S     [NULL_CODE] = "S";   /* Country of Sourcing */
const char HTSL_M     [NULL_CODE] = "M";   /* @COM@ */


/* IACS = Itemfind Action List Box (Select option) */
const char IACS[NULL_CODE] = "IACS";

const char IACS_SELE  [NULL_CODE] = "SELE";   /* Select an Item */


/* IACT = Itemfind Action List Box */
const char IACT[NULL_CODE] = "IACT";

const char IACT_NEW   [NULL_CODE] = "NEW";   /* New Item */
const char IACT_VIEW  [NULL_CODE] = "VIEW";   /* View Item */
const char IACT_EDIT  [NULL_CODE] = "EDIT";   /* Edit Item */
const char IACT_NEWE  [NULL_CODE] = "NEWE";   /* New From Existing */
const char IACT_RETL  [NULL_CODE] = "RETL";   /* New Retail Change */
const char IACT_COST  [NULL_CODE] = "COST";   /* New Cost Change */


/* ICTS = Intercompany Transfer Location Security */
const char ICTS[NULL_CODE] = "ICTS";

const char ICTS_LTXFTO[NULL_CODE] = "LTXFTO";   /* Transfers to */
const char ICTS_LTXFRM[NULL_CODE] = "LTXFRM";   /* Transfers from */


/* IDBT = Induction Download Blank Template */
const char IDBT[NULL_CODE] = "IDBT";

const char IDBT_PODT  [NULL_CODE] = "PODT";   /* Purchase Orders */
const char IDBT_IS9M  [NULL_CODE] = "IS9M";   /* Items */
const char IDBT_IS9C  [NULL_CODE] = "IS9C";   /* Cost Changes */


/* IDFL = Item Default Level */
const char IDFL[NULL_CODE] = "IDFL";

const char IDFL_T     [NULL_CODE] = "T";   /* Transaction Level */
const char IDFL_A     [NULL_CODE] = "A";   /* Above Transaction Level */


/* IDIL = Item-@MH4@-Item List */
const char IDIL[NULL_CODE] = "IDIL";

const char IDIL_I     [NULL_CODE] = "I";   /* Item */
const char IDIL_D     [NULL_CODE] = "D";   /* @MH4@ */
const char IDIL_L     [NULL_CODE] = "L";   /* Item List */


/* IDMH = Identification Method */
const char IDMH[NULL_CODE] = "IDMH";

const char IDMH_DRVRLC[NULL_CODE] = "DRVRLC";   /* Driver's License */
const char IDMH_PASSPT[NULL_CODE] = "PASSPT";   /* Passport */
const char IDMH_MILTID[NULL_CODE] = "MILTID";   /* Military ID */
const char IDMH_STRGID[NULL_CODE] = "STRGID";   /* State or Region ID */
const char IDMH_STUDID[NULL_CODE] = "STUDID";   /* Student ID */
const char IDMH_RSALID[NULL_CODE] = "RSALID";   /* Resident Alien ID */


/* IFLT = Financial Interface Line Types */
const char IFLT[NULL_CODE] = "IFLT";

const char IFLT_I     [NULL_CODE] = "I";   /* Item */
const char IFLT_T     [NULL_CODE] = "T";   /* Tax */
const char IFLT_M     [NULL_CODE] = "M";   /* Misc. */
const char IFLT_F     [NULL_CODE] = "F";   /* Freight */


/* IFWN = Itemfind Canvas Names */
const char IFWN[NULL_CODE] = "IFWN";

const char IFWN_MAIN  [NULL_CODE] = "MAIN";   /* Item Search Window         (itemfind) */
const char IFWN_LIKE  [NULL_CODE] = "LIKE";   /* Create Like Item              (itemfind) */


/* IGLB = Image Form Labels */
const char IGLB[NULL_CODE] = "IGLB";

const char IGLB_DOC   [NULL_CODE] = "DOC";   /* DOC */
const char IGLB_TXT   [NULL_CODE] = "TXT";   /* TXT */
const char IGLB_XLS   [NULL_CODE] = "XLS";   /* XLS */
const char IGLB_BMP   [NULL_CODE] = "BMP";   /* BMP */
const char IGLB_CALS  [NULL_CODE] = "CALS";   /* CALS */
const char IGLB_GIF   [NULL_CODE] = "GIF";   /* GIF */
const char IGLB_JFIF  [NULL_CODE] = "JFIF";   /* JFIF */
const char IGLB_PICT  [NULL_CODE] = "PICT";   /* PICT */
const char IGLB_RAS   [NULL_CODE] = "RAS";   /* RAS */
const char IGLB_TIFF  [NULL_CODE] = "TIFF";   /* TIFF */


/* IIAL = Item Induction - List of actions */
const char IIAL[NULL_CODE] = "IIAL";

const char IIAL_U     [NULL_CODE] = "U";   /* Upload */
const char IIAL_D     [NULL_CODE] = "D";   /* Download */
const char IIAL_DT    [NULL_CODE] = "DT";   /* Download Blank Template */
const char IIAL_DF    [NULL_CODE] = "DF";   /* Diff Finalization */


/* IILN = Item Induction - Level Names */
const char IILN[NULL_CODE] = "IILN";

const char IILN_1     [NULL_CODE] = "1";   /* Line */
const char IILN_2     [NULL_CODE] = "2";   /* Line Extension */
const char IILN_3     [NULL_CODE] = "3";   /* Variant */


/* IIPS = Item Induction - List of Statuses */
const char IIPS[NULL_CODE] = "IIPS";

const char IIPS_N     [NULL_CODE] = "N";   /* New */
const char IIPS_Q     [NULL_CODE] = "Q";   /* In Queue */
const char IIPS_PR    [NULL_CODE] = "PR";   /* Processing */
const char IIPS_PS    [NULL_CODE] = "PS";   /* Processed Successfully */
const char IIPS_PE    [NULL_CODE] = "PE";   /* Processed with errors */
const char IIPS_PW    [NULL_CODE] = "PW";   /* Processed with warnings */


/* IISL = Item Induction - List of sources */
const char IISL[NULL_CODE] = "IISL";

const char IISL_S9T   [NULL_CODE] = "S9T";   /* Spreadsheet */
const char IISL_RMS   [NULL_CODE] = "RMS";   /* RMS Tables */
const char IISL_STG   [NULL_CODE] = "STG";   /* Staging Tables */
const char IISL_EXT   [NULL_CODE] = "EXT";   /* External System */
const char IISL_RESA  [NULL_CODE] = "RESA";   /* Resa Tables */


/* IITD = Item Image Type Description */
const char IITD[NULL_CODE] = "IITD";

const char IITD_T     [NULL_CODE] = "T";   /* Thumbnail */
const char IITD_H     [NULL_CODE] = "H";   /* High */
const char IITD_M     [NULL_CODE] = "M";   /* Medium */
const char IITD_L     [NULL_CODE] = "L";   /* Low */


/* ILLB = Item List Labels */
const char ILLB[NULL_CODE] = "ILLB";

const char ILLB_VAL   [NULL_CODE] = "VAL";   /* UDA Value */
const char ILLB_MIN   [NULL_CODE] = "MIN";   /* Min Date */
const char ILLB_MAX   [NULL_CODE] = "MAX";   /* Max Date */
const char ILLB_DESC  [NULL_CODE] = "DESC";   /* Copied From: */


/* ILSD = Item List Static vs Dynamic */
const char ILSD[NULL_CODE] = "ILSD";

const char ILSD_N     [NULL_CODE] = "N";   /* Dynamic */
const char ILSD_Y     [NULL_CODE] = "Y";   /* Static */


/* ILVL = Single item, item list, or parent */
const char ILVL[NULL_CODE] = "ILVL";

const char ILVL_I     [NULL_CODE] = "I";   /* Item */
const char ILVL_ST    [NULL_CODE] = "ST";   /* Item Parent */
const char ILVL_STC   [NULL_CODE] = "STC";   /* Item Parent/Diff */
const char ILVL_IL    [NULL_CODE] = "IL";   /* Item List */


/* ILVS = Transaction Level Descriptions */
const char ILVS[NULL_CODE] = "ILVS";

const char ILVS_1     [NULL_CODE] = "1";   /* Level 1 */
const char ILVS_2     [NULL_CODE] = "2";   /* Level 2 */
const char ILVS_3     [NULL_CODE] = "3";   /* Level 3 */


/* IMD2 = Invoice Matching Discrep Type Initials */
const char IMD2[NULL_CODE] = "IMD2";

const char IMD2_Q     [NULL_CODE] = "Q";   /* Q */
const char IMD2_C     [NULL_CODE] = "C";   /* C */
const char IMD2_V     [NULL_CODE] = "V";   /* V */
const char IMD2_A     [NULL_CODE] = "A";   /* A */


/* IMDB = Invoice Matching Debit/Credit Write */
const char IMDB[NULL_CODE] = "IMDB";

const char IMDB_C     [NULL_CODE] = "C";   /* Write Crdt Req */
const char IMDB_D     [NULL_CODE] = "D";   /* Write Debit */


/* IMDC = Supplier Debit Memo Code */
const char IMDC[NULL_CODE] = "IMDC";

const char IMDC_Y     [NULL_CODE] = "Y";   /* Always */
const char IMDC_N     [NULL_CODE] = "N";   /* Never */


/* IMDS = Invoice Line Item Status */
const char IMDS[NULL_CODE] = "IMDS";

const char IMDS_U     [NULL_CODE] = "U";   /* Unmatched */
const char IMDS_M     [NULL_CODE] = "M";   /* Matched */


/* IMDT = Invoice Matching Discrepancy Types */
const char IMDT[NULL_CODE] = "IMDT";

const char IMDT_C     [NULL_CODE] = "C";   /* Cost */
const char IMDT_Q     [NULL_CODE] = "Q";   /* Qty */
const char IMDT_V     [NULL_CODE] = "V";   /* VAT */
const char IMDT_A     [NULL_CODE] = "A";   /* Any */


/* IMEX = Sales Audit Import Export */
const char IMEX[NULL_CODE] = "IMEX";

const char IMEX_I     [NULL_CODE] = "I";   /* Import */
const char IMEX_E     [NULL_CODE] = "E";   /* Export */


/* IMIT = Invoice Type */
const char IMIT[NULL_CODE] = "IMIT";

const char IMIT_I     [NULL_CODE] = "I";   /* Merchandise Invoice */
const char IMIT_C     [NULL_CODE] = "C";   /* Credit Note */
const char IMIT_D     [NULL_CODE] = "D";   /* Debit Memo */
const char IMIT_M     [NULL_CODE] = "M";   /* Credit Memo */
const char IMIT_R     [NULL_CODE] = "R";   /* Credit Note Request */
const char IMIT_N     [NULL_CODE] = "N";   /* Non-Merchandise Invoice */
const char IMIT_O     [NULL_CODE] = "O";   /* Consignment Invoice */
const char IMIT_P     [NULL_CODE] = "P";   /* Debit Memo-Consignment Returns */


/* IMLV = Item Merchandise Level (Item, Parent) */
const char IMLV[NULL_CODE] = "IMLV";

const char IMLV_STY   [NULL_CODE] = "STY";   /* Item Parent */
const char IMLV_SKU   [NULL_CODE] = "SKU";   /* Item */


/* IMPD = Import HTS Date */
const char IMPD[NULL_CODE] = "IMPD";

const char IMPD_W     [NULL_CODE] = "W";   /* Written Date */
const char IMPD_N     [NULL_CODE] = "N";   /* Not After Date */


/* IMPL = Invoice Pay/Rcv Location */
const char IMPL[NULL_CODE] = "IMPL";

const char IMPL_S     [NULL_CODE] = "S";   /* Store */
const char IMPL_C     [NULL_CODE] = "C";   /* Central */


/* IMRF = Invoice Matching Reference Type */
const char IMRF[NULL_CODE] = "IMRF";

const char IMRF_I     [NULL_CODE] = "I";   /* Invoice */
const char IMRF_R     [NULL_CODE] = "R";   /* RTV */
const char IMRF_N     [NULL_CODE] = "N";   /* Invoice */


/* IMRR = Invoice Matching Credit/Debit Reasons */
const char IMRR[NULL_CODE] = "IMRR";

const char IMRR_Q     [NULL_CODE] = "Q";   /* Qty. Discrepancy */
const char IMRR_C     [NULL_CODE] = "C";   /* Cost Discrepancy */
const char IMRR_D     [NULL_CODE] = "D";   /* Discount Discrepancy */
const char IMRR_V     [NULL_CODE] = "V";   /* VAT Discrepancy */
const char IMRR_R     [NULL_CODE] = "R";   /* RTV */
const char IMRR_B     [NULL_CODE] = "B";   /* Qty and Cost Discrepancies */
const char IMRR_VFM   [NULL_CODE] = "VFM";   /* Vendor Funded Markdown */


/* IMSS = Shipment Invoice Match Status */
const char IMSS[NULL_CODE] = "IMSS";

const char IMSS_U     [NULL_CODE] = "U";   /* Unmatched */
const char IMSS_P     [NULL_CODE] = "P";   /* Partially Matched */
const char IMSS_M     [NULL_CODE] = "M";   /* Matched */
const char IMSS_C     [NULL_CODE] = "C";   /* Closed */


/* IMST = Invoice Header Status Types */
const char IMST[NULL_CODE] = "IMST";

const char IMST_U     [NULL_CODE] = "U";   /* Unmatched */
const char IMST_R     [NULL_CODE] = "R";   /* Partially Matched */
const char IMST_M     [NULL_CODE] = "M";   /* Matched */
const char IMST_A     [NULL_CODE] = "A";   /* Approved */
const char IMST_P     [NULL_CODE] = "P";   /* Posted */


/* IMT2 = Invoice Matching EDIT/VIEW Types */
const char IMT2[NULL_CODE] = "IMT2";

const char IMT2_I     [NULL_CODE] = "I";   /* Merchandise Invoice */
const char IMT2_C     [NULL_CODE] = "C";   /* Credit Note */
const char IMT2_D     [NULL_CODE] = "D";   /* Debit Memo */
const char IMT2_M     [NULL_CODE] = "M";   /* Credit Memo */
const char IMT2_R     [NULL_CODE] = "R";   /* Credit Note Request */
const char IMT2_N     [NULL_CODE] = "N";   /* Non-Merchandise Invoice */


/* INDT = Invoice Discount Type */
const char INDT[NULL_CODE] = "INDT";

const char INDT_P     [NULL_CODE] = "P";   /* Percent */
const char INDT_A     [NULL_CODE] = "A";   /* Amount */


/* INGN = Invoice Cost Type */
const char INGN[NULL_CODE] = "INGN";

const char INGN_G     [NULL_CODE] = "G";   /* Gross */
const char INGN_N     [NULL_CODE] = "N";   /* Net */


/* INRF = Reference types for invoice credit notes */
const char INRF[NULL_CODE] = "INRF";

const char INRF_INVC  [NULL_CODE] = "INVC";   /* Invoice */
const char INRF_RTV   [NULL_CODE] = "RTV";   /* RTV */
const char INRF_VFMD  [NULL_CODE] = "VFMD";   /* Vendor-funded Markdown */
const char INRF_SDC   [NULL_CODE] = "SDC";   /* Stand Alone Debit/Credit */


/* INRN = Inner and equivalent names */
const char INRN[NULL_CODE] = "INRN";

const char INRN_INR   [NULL_CODE] = "INR";   /* Inner */
const char INRN_SCS   [NULL_CODE] = "SCS";   /* Sub-Case */
const char INRN_SPACK [NULL_CODE] = "SPACK";   /* Sub-Pack */
const char INRN_EA    [NULL_CODE] = "EA";   /* Eaches */


/* INST = Inventory Status */
const char INST[NULL_CODE] = "INST";

const char INST_0     [NULL_CODE] = "0";   /* Total Stock on Hand */


/* INTS = Inv. Adj. Total Stock on Hand List Box */
const char INTS[NULL_CODE] = "INTS";

const char INTS_0     [NULL_CODE] = "0";   /* Total Stock on Hand */


/* INVH = Inventory History Level */
const char INVH[NULL_CODE] = "INVH";

const char INVH_A     [NULL_CODE] = "A";   /* All Items */
const char INVH_I     [NULL_CODE] = "I";   /* Items Sold Only */
const char INVH_N     [NULL_CODE] = "N";   /* No History */


/* INVM = Inventory Movement Indicators */
const char INVM[NULL_CODE] = "INVM";

const char INVM_U     [NULL_CODE] = "U";   /* Unavailable */
const char INVM_A     [NULL_CODE] = "A";   /* Available */


/* INVS = Label Inventory Status */
const char INVS[NULL_CODE] = "INVS";

const char INVS_I     [NULL_CODE] = "I";   /* Inventory Status */


/* INVX = Invoice Matching X-ref Types */
const char INVX[NULL_CODE] = "INVX";

const char INVX_O     [NULL_CODE] = "O";   /* Order No. */
const char INVX_A     [NULL_CODE] = "A";   /* ASN No. */


/* IREL = Item Relationship Types */
const char IREL[NULL_CODE] = "IREL";

const char IREL_CRSL  [NULL_CODE] = "CRSL";   /* Cross Sell */
const char IREL_UPSL  [NULL_CODE] = "UPSL";   /* Up-Sell */
const char IREL_SUBS  [NULL_CODE] = "SUBS";   /* Substitution */


/* IS9T = Item Template Types */
const char IS9T[NULL_CODE] = "IS9T";

const char IS9T_IS9M  [NULL_CODE] = "IS9M";   /* Items */
const char IS9T_IS9X  [NULL_CODE] = "IS9X";   /* XItem Data */


/* ISLV = Item Service Level */
const char ISLV[NULL_CODE] = "ISLV";

const char ISLV_GRND  [NULL_CODE] = "GRND";   /* Ground */
const char ISLV_2DAY  [NULL_CODE] = "2DAY";   /* Second Day */
const char ISLV_OVRNT [NULL_CODE] = "OVRNT";   /* Overnight */
const char ISLV_POVRNT[NULL_CODE] = "POVRNT";   /* Priority Overnight */


/* ISST = Issue Types */
const char ISST[NULL_CODE] = "ISST";

const char ISST_E     [NULL_CODE] = "E";   /* Error */
const char ISST_W     [NULL_CODE] = "W";   /* Warning */


/* ITDP = Item-@MH4@ */
const char ITDP[NULL_CODE] = "ITDP";

const char ITDP_I     [NULL_CODE] = "I";   /* Item */
const char ITDP_D     [NULL_CODE] = "D";   /* @MH4@ */


/* ITED = Item Editor Titles */
const char ITED[NULL_CODE] = "ITED";

const char ITED_IDESC [NULL_CODE] = "IDESC";   /* Item Description */
const char ITED_PDESC [NULL_CODE] = "PDESC";   /* Item Parent Description */
const char ITED_GPDESC[NULL_CODE] = "GPDESC";   /* Item Grandparent Description */
const char ITED_RIDESC[NULL_CODE] = "RIDESC";   /* Ref Item Description */
const char ITED_VPNDSC[NULL_CODE] = "VPNDSC";   /* VPN Item Description */
const char ITED_SDESC [NULL_CODE] = "SDESC";   /* Item Short Description */
const char ITED_SIDESC[NULL_CODE] = "SIDESC";   /* Secondary Item Description */


/* ITLS = Item Level */
const char ITLS[NULL_CODE] = "ITLS";

const char ITLS_1     [NULL_CODE] = "1";   /* Item */
const char ITLS_2     [NULL_CODE] = "2";   /* Item Parent/Differentiator 1 */
const char ITLS_3     [NULL_CODE] = "3";   /* Item Parent/Differentiator 2 */
const char ITLS_4     [NULL_CODE] = "4";   /* Item Parent */
const char ITLS_5     [NULL_CODE] = "5";   /* Item List */


/* ITLV = Item Level Descriptions */
const char ITLV[NULL_CODE] = "ITLV";

const char ITLV_1     [NULL_CODE] = "1";   /* 1 */
const char ITLV_2     [NULL_CODE] = "2";   /* 2 */
const char ITLV_3     [NULL_CODE] = "3";   /* 3 */


/* ITMT = Item Type */
const char ITMT[NULL_CODE] = "ITMT";

const char ITMT_R     [NULL_CODE] = "R";   /* Regular Item */
const char ITMT_S     [NULL_CODE] = "S";   /* Simple Pack */
const char ITMT_C     [NULL_CODE] = "C";   /* Complex Pack */
const char ITMT_E     [NULL_CODE] = "E";   /* Deposit Contents */
const char ITMT_A     [NULL_CODE] = "A";   /* Deposit Container */
const char ITMT_Z     [NULL_CODE] = "Z";   /* Deposit Crate */
const char ITMT_T     [NULL_CODE] = "T";   /* Deposit Returned Item */
const char ITMT_I     [NULL_CODE] = "I";   /* Consignment/Concession */
const char ITMT_O     [NULL_CODE] = "O";   /* Transformed Orderable */
const char ITMT_L     [NULL_CODE] = "L";   /* Transformed Sellable */
const char ITMT_P     [NULL_CODE] = "P";   /* Deposit Complex Pack */


/* ITNG = Non-Grocery Item Type */
const char ITNG[NULL_CODE] = "ITNG";

const char ITNG_R     [NULL_CODE] = "R";   /* Regular Item */
const char ITNG_S     [NULL_CODE] = "S";   /* Simple Pack */
const char ITNG_C     [NULL_CODE] = "C";   /* Complex Pack */
const char ITNG_I     [NULL_CODE] = "I";   /* Consignment/Concession */


/* ITOS = Item or Item Parent */
const char ITOS[NULL_CODE] = "ITOS";

const char ITOS_I     [NULL_CODE] = "I";   /* Item */
const char ITOS_S     [NULL_CODE] = "S";   /* Item Parent */


/* ITPK = Itemfind Pack Types */
const char ITPK[NULL_CODE] = "ITPK";

const char ITPK_C     [NULL_CODE] = "C";   /* Complex */
const char ITPK_S     [NULL_CODE] = "S";   /* Simple */
const char ITPK_N     [NULL_CODE] = "N";   /* No Pack */


/* ITRC = Item Transformation Codes */
const char ITRC[NULL_CODE] = "ITRC";

const char ITRC_1     [NULL_CODE] = "1";   /* 9999 */


/* ITST = Item Status */
const char ITST[NULL_CODE] = "ITST";

const char ITST_W     [NULL_CODE] = "W";   /* Worksheet */
const char ITST_S     [NULL_CODE] = "S";   /* Submitted */
const char ITST_A     [NULL_CODE] = "A";   /* Approved */
const char ITST_D     [NULL_CODE] = "D";   /* Delete Pending */


/* ITTY = Item Types */
const char ITTY[NULL_CODE] = "ITTY";

const char ITTY_SS    [NULL_CODE] = "SS";   /* Single Item */
const char ITTY_IL    [NULL_CODE] = "IL";   /* Item List */


/* IVAC = Invfind Action List Box */
const char IVAC[NULL_CODE] = "IVAC";

const char IVAC_NEW   [NULL_CODE] = "NEW";   /* New */
const char IVAC_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char IVAC_MATC  [NULL_CODE] = "MATC";   /* Matching */


/* IVLB = Invenq form labels */
const char IVLB[NULL_CODE] = "IVLB";

const char IVLB_D     [NULL_CODE] = "D";   /* @MH4@ */
const char IVLB_S     [NULL_CODE] = "S";   /* Supplier */
const char IVLB_I     [NULL_CODE] = "I";   /* Invoice */
const char IVLB_C     [NULL_CODE] = "C";   /* Claim */
const char IVLB_G     [NULL_CODE] = "G";   /* G.W.I. */


/* IVMH = Invmatch form labels */
const char IVMH[NULL_CODE] = "IVMH";

const char IVMH_E     [NULL_CODE] = "E";   /* Extended VAT Cost */
const char IVMH_A     [NULL_CODE] = "A";   /* Actual VAT Cost */


/* IVTP = Invoice Type */
const char IVTP[NULL_CODE] = "IVTP";

const char IVTP_I     [NULL_CODE] = "I";   /* Invoice */
const char IVTP_C     [NULL_CODE] = "C";   /* Claim */


/* JALT = Job Audit Logs - List of Log Types */
const char JALT[NULL_CODE] = "JALT";

const char JALT_E     [NULL_CODE] = "E";   /* Error */
const char JALT_I     [NULL_CODE] = "I";   /* Info */
const char JALT_W     [NULL_CODE] = "W";   /* Warning */


/* JAST = Job Audit Logs - List of Statuses */
const char JAST[NULL_CODE] = "JAST";

const char JAST_S     [NULL_CODE] = "S";   /* Started */
const char JAST_F     [NULL_CODE] = "F";   /* Failure */
const char JAST_C     [NULL_CODE] = "C";   /* Completed */
const char JAST_I     [NULL_CODE] = "I";   /* In Progress */


/* JBST = Statuses of Asynchronous Jobs */
const char JBST[NULL_CODE] = "JBST";

const char JBST_N     [NULL_CODE] = "N";   /* New */
const char JBST_E     [NULL_CODE] = "E";   /* Completed with Error */
const char JBST_S     [NULL_CODE] = "S";   /* Completed Successfully */
const char JBST_R     [NULL_CODE] = "R";   /* Retry Job */
const char JBST_I     [NULL_CODE] = "I";   /* In Progress */


/* LABL = Label */
const char LABL[NULL_CODE] = "LABL";

const char LABL_Z     [NULL_CODE] = "Z";   /* New Zone Group */
const char LABL_VAT   [NULL_CODE] = "VAT";   /* VAT Region */
const char LABL_POS   [NULL_CODE] = "POS";   /* POS Includes VAT? */
const char LABL_D     [NULL_CODE] = "D";   /* Dollar */
const char LABL_UD    [NULL_CODE] = "UD";   /* Unit and Dollar */
const char LABL_U     [NULL_CODE] = "U";   /* Unit */
const char LABL_PC    [NULL_CODE] = "PC";   /* Price Change */
const char LABL_CE    [NULL_CODE] = "CE";   /* Clearance Event */
const char LABL_ST    [NULL_CODE] = "ST";   /* Store */
const char LABL_NA    [NULL_CODE] = "NA";   /* Name */
const char LABL_DESC  [NULL_CODE] = "DESC";   /* Description */
const char LABL_ZONE  [NULL_CODE] = "ZONE";   /* Zone */
const char LABL_PSC   [NULL_CODE] = "PSC";   /* Price Store Change */
const char LABL_AS    [NULL_CODE] = "AS";   /* Add Stores */
const char LABL_PZC   [NULL_CODE] = "PZC";   /* Price Zone Change */
const char LABL_AZ    [NULL_CODE] = "AZ";   /* Add Zones */
const char LABL_C     [NULL_CODE] = "C";   /* Clear */
const char LABL_REG   [NULL_CODE] = "REG";   /* Regular */
const char LABL_TRAN  [NULL_CODE] = "TRAN";   /* Transportation ID */
const char LABL_ADD   [NULL_CODE] = "ADD";   /* Add */
const char LABL_CA    [NULL_CODE] = "CA";   /* Case/ */
const char LABL_PP    [NULL_CODE] = "PP";   /* Pre-Pack */
const char LABL_PPD   [NULL_CODE] = "PPD";   /* Pre-Pack Detail */
const char LABL_PSCW  [NULL_CODE] = "PSCW";   /* Price Store Change (pcstore) */
const char LABL_PZCW  [NULL_CODE] = "PZCW";   /* Price Zone Change             (pcstore) */
const char LABL_CD    [NULL_CODE] = "CD";   /* Case Detail */
const char LABL_VSKU  [NULL_CODE] = "VSKU";   /* View Item */
const char LABL_VUPC  [NULL_CODE] = "VUPC";   /* View Child Item */
const char LABL_CEDT  [NULL_CODE] = "CEDT";   /* Customs Detail */
const char LABL_VESS  [NULL_CODE] = "VESS";   /* Vessel */
const char LABL_VOY   [NULL_CODE] = "VOY";   /* Voyage/Flight */
const char LABL_ETD   [NULL_CODE] = "ETD";   /* Est. Depart Date */
const char LABL_OBL   [NULL_CODE] = "OBL";   /* Obligation */
const char LABL_EM    [NULL_CODE] = "EM";   /* Edit Matrix */
const char LABL_EQ    [NULL_CODE] = "EQ";   /* Edit Quantity */
const char LABL_ER    [NULL_CODE] = "ER";   /* Edit Ratio */
const char LABL_Q     [NULL_CODE] = "Q";   /* Quantity */
const char LABL_RAT   [NULL_CODE] = "RAT";   /* Ratio */
const char LABL_COUR  [NULL_CODE] = "COUR";   /* Courier */
const char LABL_SHDT  [NULL_CODE] = "SHDT";   /* Shipment Date */
const char LABL_MH4NAM[NULL_CODE] = "MH4NAM";   /* @MH4@ Name */
const char LABL_TXCD  [NULL_CODE] = "TXCD";   /* Tax Code */
const char LABL_ATXCD [NULL_CODE] = "ATXCD";   /* All Tax Codes */
const char LABL_FI    [NULL_CODE] = "FI";   /*   Find Item   */
const char LABL_FU    [NULL_CODE] = "FU";   /*   Find Child Item   */
const char LABL_FV    [NULL_CODE] = "FV";   /*   Find VPN   */
const char LABL_SD    [NULL_CODE] = "SD";   /* Store Detail */
const char LABL_ZD    [NULL_CODE] = "ZD";   /* Zone Detail */
const char LABL_AV    [NULL_CODE] = "AV";   /* Availability */
const char LABL_PA    [NULL_CODE] = "PA";   /* Pack */
const char LABL_LC    [NULL_CODE] = "LC";   /* Landed Cost Per Unit */
const char LABL_SUPP  [NULL_CODE] = "SUPP";   /* Supp. */
const char LABL_CART  [NULL_CODE] = "CART";   /* Carton */
const char LABL_MON   [NULL_CODE] = "MON";   /* Monetary */
const char LABL_SKU   [NULL_CODE] = "SKU";   /* Item */
const char LABL_DEPT  [NULL_CODE] = "DEPT";   /* @MHP4@ */
const char LABL_ITEM  [NULL_CODE] = "ITEM";   /* Items */
const char LABL_UPC   [NULL_CODE] = "UPC";   /* UPC */
const char LABL_VPN   [NULL_CODE] = "VPN";   /* VPN */
const char LABL_UDA   [NULL_CODE] = "UDA";   /* UDA */
const char LABL_UDAD  [NULL_CODE] = "UDAD";   /* UDA Date */
const char LABL_UDAT  [NULL_CODE] = "UDAT";   /* UDA Text */
const char LABL_UDAV  [NULL_CODE] = "UDAV";   /* UDA Value */
const char LABL_ILST  [NULL_CODE] = "ILST";   /* Item List */
const char LABL_STY   [NULL_CODE] = "STY";   /* Item Parent */
const char LABL_SIZE1 [NULL_CODE] = "SIZE1";   /* Size 1 */
const char LABL_SIZE2 [NULL_CODE] = "SIZE2";   /* Size 2 */
const char LABL_COLOR [NULL_CODE] = "COLOR";   /* Color */
const char LABL_CUCL  [NULL_CODE] = "CUCL";   /* Currently on Clearance */
const char LABL_BCK2  [NULL_CODE] = "BCK2";   /* Back To */
const char LABL_PLUS  [NULL_CODE] = "PLUS";   /* Plus Last */
const char LABL_LSTW  [NULL_CODE] = "LSTW";   /* Weeks */
const char LABL_MH5NAM[NULL_CODE] = "MH5NAM";   /* @MH5@ Name */
const char LABL_CI    [NULL_CODE] = "CI";   /* Cancel Item */
const char LABL_DI    [NULL_CODE] = "DI";   /* Delete Item */
const char LABL_DEP   [NULL_CODE] = "DEP";   /* @MH4@ */
const char LABL_ITM   [NULL_CODE] = "ITM";   /* Item */
const char LABL_PLC   [NULL_CODE] = "PLC";   /* Total Pack Landed Cost */
const char LABL_NULL  [NULL_CODE] = "NULL";   /* Blank */
const char LABL_NEWS  [NULL_CODE] = "NEWS";   /* New Item Parent */
const char LABL_NEWI  [NULL_CODE] = "NEWI";   /* New Item */
const char LABL_AI    [NULL_CODE] = "AI";   /* Apply Item */
const char LABL_AL    [NULL_CODE] = "AL";   /* Apply Item List */
const char LABL_AU    [NULL_CODE] = "AU";   /* Apply Child Item */
const char LABL_TA    [NULL_CODE] = "TA";   /* Threshold Amount */
const char LABL_TU    [NULL_CODE] = "TU";   /* Threshold Unit */
const char LABL_DCD   [NULL_CODE] = "DCD";   /* Document Details */
const char LABL_ETDT  [NULL_CODE] = "ETDT";   /* Entity Details */
const char LABL_TXBR  [NULL_CODE] = "TXBR";   /* Tax Breakup */
const char LABL_MH6NAM[NULL_CODE] = "MH6NAM";   /* @MH6@ Name */
const char LABL_LTCR  [NULL_CODE] = "LTCR";   /* Letter of Credit */
const char LABL_PO    [NULL_CODE] = "PO";   /* Order No. */
const char LABL_CNTR  [NULL_CODE] = "CNTR";   /* Contract No. */
const char LABL_CTRY  [NULL_CODE] = "CTRY";   /* Country */
const char LABL_PYMT  [NULL_CODE] = "PYMT";   /* Letter of Credit */
const char LABL_SUP   [NULL_CODE] = "SUP";   /* Supplier */
const char LABL_BANK  [NULL_CODE] = "BANK";   /* Bank */
const char LABL_PTNR  [NULL_CODE] = "PTNR";   /* Partner */
const char LABL_PTYP  [NULL_CODE] = "PTYP";   /* Partner Type */
const char LABL_CU    [NULL_CODE] = "CU";   /* Entry No. */
const char LABL_LO    [NULL_CODE] = "LO";   /* Logistics No. */
const char LABL_LOBL  [NULL_CODE] = "LOBL";   /* BOL/AWB */
const char LABL_LOCO  [NULL_CODE] = "LOCO";   /* Container */
const char LABL_LOCI  [NULL_CODE] = "LOCI";   /* Invoice No. */
const char LABL_HTSC  [NULL_CODE] = "HTSC";   /* HTS Heading */
const char LABL_CB    [NULL_CODE] = "CB";   /* Order Cost Basis */
const char LABL_EC    [NULL_CODE] = "EC";   /* Expense Category */
const char LABL_OC    [NULL_CODE] = "OC";   /* Country Of Sourcing */
const char LABL_TMBL  [NULL_CODE] = "TMBL";   /* Timeline Base */
const char LABL_DPD   [NULL_CODE] = "DPD";   /* Discharge Port Description */
const char LABL_LPD   [NULL_CODE] = "LPD";   /* Lading port Description */
const char LABL_ZND   [NULL_CODE] = "ZND";   /* Zone Description */
const char LABL_IC    [NULL_CODE] = "IC";   /* Import Country */
const char LABL_EXRTAB[NULL_CODE] = "EXRTAB";   /* Exch. Rt. */
const char LABL_EURXAB[NULL_CODE] = "EURXAB";   /* Exch. Rt.(Euro) */
const char LABL_RDLB  [NULL_CODE] = "RDLB";   /* Required Documents */
const char LABL_IHLB  [NULL_CODE] = "IHLB";   /* HTS Assessment */
const char LABL_PARD  [NULL_CODE] = "PARD";   /* Partner/Required Documents */
const char LABL_ITHT  [NULL_CODE] = "ITHT";   /* Item/HTS */
const char LABL_ITIM  [NULL_CODE] = "ITIM";   /* Item/Import Attributes */
const char LABL_ITRD  [NULL_CODE] = "ITRD";   /* Item/Required Documents */
const char LABL_ITTM  [NULL_CODE] = "ITTM";   /* Item/Timelines */
const char LABL_ITEL  [NULL_CODE] = "ITEL";   /* Eligible Tariff Treatments */
const char LABL_IDLB  [NULL_CODE] = "IDLB";   /* Import Desc. */
const char LABL_CTRD  [NULL_CODE] = "CTRD";   /* Country Required Documents */
const char LABL_CTTT  [NULL_CODE] = "CTTT";   /* Country Tariff Treatments */
const char LABL_CZG   [NULL_CODE] = "CZG";   /* Cost Zone Group */
const char LABL_CZ    [NULL_CODE] = "CZ";   /* Cost Zone */
const char LABL_BP    [NULL_CODE] = "BP";   /* Base Profile */
const char LABL_PROF  [NULL_CODE] = "PROF";   /* Profile Type */
const char LABL_IT    [NULL_CODE] = "IT";   /* Item */
const char LABL_LOG   [NULL_CODE] = "LOG";   /* Logistics */
const char LABL_CUST  [NULL_CODE] = "CUST";   /* Customs */
const char LABL_CNT   [NULL_CODE] = "CNT";   /* Contract */
const char LABL_PORD  [NULL_CODE] = "PORD";   /* Purchase Order */
const char LABL_POIT  [NULL_CODE] = "POIT";   /* Order/Item */
const char LABL_LIST  [NULL_CODE] = "LIST";   /* List of */
const char LABL_TRTW  [NULL_CODE] = "TRTW";   /* Associated with Trait */
const char LABL_MAXL  [NULL_CODE] = "MAXL";   /* Maximum Length */
const char LABL_LEVEL [NULL_CODE] = "LEVEL";   /* Level */
const char LABL_COM   [NULL_CODE] = "COM";   /* Comments */
const char LABL_TEXT  [NULL_CODE] = "TEXT";   /* Text */
const char LABL_TERM  [NULL_CODE] = "TERM";   /* Terms */
const char LABL_IMPD  [NULL_CODE] = "IMPD";   /* Import Description */
const char LABL_DCTX  [NULL_CODE] = "DCTX";   /* Document Text */
const char LABL_MWIN  [NULL_CODE] = "MWIN";   /* Maintenance Window */
const char LABL_HTDS  [NULL_CODE] = "HTDS";   /* HTS Description */
const char LABL_SQF   [NULL_CODE] = "SQF";   /* Square Footage Area */
const char LABL_DEPS  [NULL_CODE] = "DEPS";   /* *All @MHP4@ */
const char LABL_CLASS [NULL_CODE] = "CLASS";   /* *All @MHP5@ */
const char LABL_SCLASS[NULL_CODE] = "SCLASS";   /* *All @MHP6@ */
const char LABL_LOCS  [NULL_CODE] = "LOCS";   /* *All Locations */
const char LABL_TRCODE[NULL_CODE] = "TRCODE";   /* *All Tran Codes */
const char LABL_UOT   [NULL_CODE] = "UOT";   /* Unit of Transfer */
const char LABL_UOP   [NULL_CODE] = "UOP";   /* Unit of Purchase */
const char LABL_UOA   [NULL_CODE] = "UOA";   /* Unit of Allocation */
const char LABL_CLS   [NULL_CODE] = "CLS";   /* @MH5@ */
const char LABL_DIV   [NULL_CODE] = "DIV";   /* @MH2@ */
const char LABL_GROUP [NULL_CODE] = "GROUP";   /* @MH3@ */
const char LABL_SCLS  [NULL_CODE] = "SCLS";   /* @MH6@ */
const char LABL_COMP  [NULL_CODE] = "COMP";   /* @OH1@ */
const char LABL_DIS   [NULL_CODE] = "DIS";   /* @OH5@ */
const char LABL_ORC   [NULL_CODE] = "ORC";   /* Orig. Country */
const char LABL_ORGC  [NULL_CODE] = "ORGC";   /* Orig. Ctry */
const char LABL_ALL   [NULL_CODE] = "ALL";   /* *All */
const char LABL_CLRS  [NULL_CODE] = "CLRS";   /* Clearance Defaults */
const char LABL_EWIN  [NULL_CODE] = "EWIN";   /* Entry Window */
const char LABL_PROM  [NULL_CODE] = "PROM";   /* Promotion */
const char LABL_EVDS  [NULL_CODE] = "EVDS";   /* Event Description */
const char LABL_PRDS  [NULL_CODE] = "PRDS";   /* Promotion Description */
const char LABL_KEY   [NULL_CODE] = "KEY";   /* Key Value */
const char LABL_TRANS [NULL_CODE] = "TRANS";   /* Translated Value */
const char LABL_UOM   [NULL_CODE] = "UOM";   /* UOM */
const char LABL_MERCH [NULL_CODE] = "MERCH";   /* Merchandise Description */
const char LABL_ERCODE[NULL_CODE] = "ERCODE";   /* Merchandise Description */
const char LABL_ACT   [NULL_CODE] = "ACT";   /* Actual */
const char LABL_PROJ  [NULL_CODE] = "PROJ";   /* Projected */
const char LABL_ADF   [NULL_CODE] = "ADF";   /* Apply Defaults For */
const char LABL_WKE   [NULL_CODE] = "WKE";   /* Week */
const char LABL_TR    [NULL_CODE] = "TR";   /* Transportation */
const char LABL_TRCO  [NULL_CODE] = "TRCO";   /* Container */
const char LABL_TRBL  [NULL_CODE] = "TRBL";   /* BOL/AWB */
const char LABL_TRCI  [NULL_CODE] = "TRCI";   /* Commercial Invoice */
const char LABL_NEXT  [NULL_CODE] = "NEXT";   /* Next */
const char LABL_CASE  [NULL_CODE] = "CASE";   /* Case */
const char LABL_SUOM  [NULL_CODE] = "SUOM";   /* SUOM */
const char LABL_PAL   [NULL_CODE] = "PAL";   /* Pallet */
const char LABL_SPPACK[NULL_CODE] = "SPPACK";   /* Supp Pack */
const char LABL_EUROEX[NULL_CODE] = "EUROEX";   /* Exchange Rate(Euro) */
const char LABL_EXRATE[NULL_CODE] = "EXRATE";   /* Exchange Rate */
const char LABL_ORDEX [NULL_CODE] = "ORDEX";   /* Order Exchange Rate */
const char LABL_ISSUE [NULL_CODE] = "ISSUE";   /* Form Issue */
const char LABL_GVN   [NULL_CODE] = "GVN";   /* Gift Certificate Number */
const char LABL_SQST  [NULL_CODE] = "SQST";   /* SQL Statement */
const char LABL_FUEL  [NULL_CODE] = "FUEL";   /* Fuel  */
const char LABL_DEFA  [NULL_CODE] = "DEFA";   /* Default  */
const char LABL_DEPIMP[NULL_CODE] = "DEPIMP";   /* @MH4@ Impact */
const char LABL_TRAW  [NULL_CODE] = "TRAW";   /* Traits associated with  */
const char LABL_SLCM  [NULL_CODE] = "SLCM";   /* Cost Maintenance          (stkldgmn) */
const char LABL_SLRM  [NULL_CODE] = "SLRM";   /* Retail Maintenance          (stkldgmn) */
const char LABL_VCRM  [NULL_CODE] = "VCRM";   /* Rates Maintenance          (vatcode) */
const char LABL_SCRT  [NULL_CODE] = "SCRT";   /* Search Criteria */
const char LABL_SRLT  [NULL_CODE] = "SRLT";   /* Search Results */
const char LABL_LYR1  [NULL_CODE] = "LYR1";   /* First Layer */
const char LABL_LYR2  [NULL_CODE] = "LYR2";   /* Second Layer */
const char LABL_ITBN  [NULL_CODE] = "ITBN";   /* Item/Button */
const char LABL_INCL  [NULL_CODE] = "INCL";   /* Include */
const char LABL_EXCL  [NULL_CODE] = "EXCL";   /* Exclude */
const char LABL_CRIT  [NULL_CODE] = "CRIT";   /* Criteria */
const char LABL_COUP  [NULL_CODE] = "COUP";   /* Coupon */
const char LABL_PRES  [NULL_CODE] = "PRES";   /* Product Restriction */
const char LABL_CCVL  [NULL_CODE] = "CCVL";   /* Credit Card Validation */
const char LABL_APLY  [NULL_CODE] = "APLY";   /* Apply */
const char LABL_DAY   [NULL_CODE] = "DAY";   /* Day */
const char LABL_TIME  [NULL_CODE] = "TIME";   /* Time */
const char LABL_DATE  [NULL_CODE] = "DATE";   /* Date */
const char LABL_TXCDE [NULL_CODE] = "TXCDE";   /* Tax Codes */
const char LABL_TXRT  [NULL_CODE] = "TXRT";   /* Tax Rates */
const char LABL_WAST  [NULL_CODE] = "WAST";   /* Wastage Attributes */
const char LABL_TICK  [NULL_CODE] = "TICK";   /* Ticket Attributes */
const char LABL_COPY  [NULL_CODE] = "COPY";   /* Copy tax codes from item */
const char LABL_DIP   [NULL_CODE] = "DIP";   /* Dis. Port */
const char LABL_DIST  [NULL_CODE] = "DIST";   /* Distribute By */
const char LABL_ALLTLS[NULL_CODE] = "ALLTLS";   /* All Totals */
const char LABL_SZRANG[NULL_CODE] = "SZRANG";   /* Size Range */
const char LABL_SZTEMP[NULL_CODE] = "SZTEMP";   /* Size Template */
const char LABL_NULL1 [NULL_CODE] = "NULL1";   /* NULL */
const char LABL_ALLOC [NULL_CODE] = "ALLOC";   /* Allocate to Location */
const char LABL_ALWAR [NULL_CODE] = "ALWAR";   /* Allocate from Warehouse */
const char LABL_SUMM  [NULL_CODE] = "SUMM";   /* Summary */
const char LABL_SPDP  [NULL_CODE] = "SPDP";   /* Supplier/@MH4@ */
const char LABL_G     [NULL_CODE] = "G";   /* Group */
const char LABL_VAL   [NULL_CODE] = "VAL";   /* Value */
const char LABL_GRP   [NULL_CODE] = "GRP";   /* Group */
const char LABL_A     [NULL_CODE] = "A";   /* Available */
const char LABL_IR    [NULL_CODE] = "IR";   /* In Range */
const char LABL_DP    [NULL_CODE] = "DP";   /* Delete Pending */
const char LABL_RFIT  [NULL_CODE] = "RFIT";   /* Ref Item */
const char LABL_PARENT[NULL_CODE] = "PARENT";   /* Item Parent/Grandparent */
const char LABL_DIFF1 [NULL_CODE] = "DIFF1";   /* Diff 1 */
const char LABL_DIFF2 [NULL_CODE] = "DIFF2";   /* Diff 2 */
const char LABL_FT    [NULL_CODE] = "FT";   /* Feet */
const char LABL_M     [NULL_CODE] = "M";   /* Meters */
const char LABL_LCID  [NULL_CODE] = "LCID";   /* Letter of Credit */
const char LABL_UCHRG [NULL_CODE] = "UCHRG";   /* Up Charges */
const char LABL_MLOC  [NULL_CODE] = "MLOC";   /* Multiple Locations */
const char LABL_SUBPO [NULL_CODE] = "SUBPO";   /* Submitted PO */
const char LABL_POTO1 [NULL_CODE] = "POTO1";   /* 1st Order Total */
const char LABL_POTO2 [NULL_CODE] = "POTO2";   /* 2nd Order Total */
const char LABL_SCIS  [NULL_CODE] = "SCIS";   /* Scaling Issues */
const char LABL_TRIS  [NULL_CODE] = "TRIS";   /* Truck Splitting Issues */
const char LABL_P     [NULL_CODE] = "P";   /* Physical */
const char LABL_V     [NULL_CODE] = "V";   /* Virtual */
const char LABL_UNKNWN[NULL_CODE] = "UNKNWN";   /* Unknown */
const char LABL_ORD   [NULL_CODE] = "ORD";   /* Order */
const char LABL_TSF   [NULL_CODE] = "TSF";   /* Transfer */
const char LABL_ALLC  [NULL_CODE] = "ALLC";   /* Allocation */
const char LABL_DOCTYP[NULL_CODE] = "DOCTYP";   /* Document Type */
const char LABL_LOCTYP[NULL_CODE] = "LOCTYP";   /* Location Type */
const char LABL_UCST  [NULL_CODE] = "UCST";   /* Unit Cost */
const char LABL_UELC  [NULL_CODE] = "UELC";   /* Unit ELC */
const char LABL_LD    [NULL_CODE] = "LD";   /* Launch Date */
const char LABL_QO    [NULL_CODE] = "QO";   /* Qty/Key Options */
const char LABL_MP    [NULL_CODE] = "MP";   /* Manual Price Entry */
const char LABL_DC    [NULL_CODE] = "DC";   /* Deposit Code */
const char LABL_FS    [NULL_CODE] = "FS";   /* Food Stamp Ind */
const char LABL_WI    [NULL_CODE] = "WI";   /* WIC Ind */
const char LABL_PT    [NULL_CODE] = "PT";   /* Proportional Tare Pct */
const char LABL_FX    [NULL_CODE] = "FX";   /* Fixed Tare Value */
const char LABL_FM    [NULL_CODE] = "FM";   /* Fixed Tare UOM */
const char LABL_RE    [NULL_CODE] = "RE";   /* Reward Eligible Ind */
const char LABL_NB    [NULL_CODE] = "NB";   /* National Brand Comparison Item */
const char LABL_RP    [NULL_CODE] = "RP";   /* Return Policy */
const char LABL_SS    [NULL_CODE] = "SS";   /* Stop Sale Ind */
const char LABL_EL    [NULL_CODE] = "EL";   /* Electronic Market Clubs */
const char LABL_RC    [NULL_CODE] = "RC";   /* Report Code */
const char LABL_SL    [NULL_CODE] = "SL";   /* Shelf Life on Select */
const char LABL_SR    [NULL_CODE] = "SR";   /* Shelf Life on Rcpt */
const char LABL_IB    [NULL_CODE] = "IB";   /* Investment Buy Shelf Life */
const char LABL_SI    [NULL_CODE] = "SI";   /* Store Reorderable Ind */
const char LABL_RS    [NULL_CODE] = "RS";   /* Rack Size */
const char LABL_FP    [NULL_CODE] = "FP";   /* Full Pallet Item */
const char LABL_IM    [NULL_CODE] = "IM";   /* In Store Market Basket */
const char LABL_SC    [NULL_CODE] = "SC";   /* Storage Location */
const char LABL_AT    [NULL_CODE] = "AT";   /* Alt Storage Location */
const char LABL_RI    [NULL_CODE] = "RI";   /* Returnable Ind */
const char LABL_RF    [NULL_CODE] = "RF";   /* Refundable Ind */
const char LABL_BI    [NULL_CODE] = "BI";   /* Back Orderable Ind */
const char LABL_DIFF3 [NULL_CODE] = "DIFF3";   /* Diff 3 */
const char LABL_DIFF4 [NULL_CODE] = "DIFF4";   /* Diff 4 */
const char LABL_DIFFT [NULL_CODE] = "DIFFT";   /* Diff Type */
const char LABL_IDAPPL[NULL_CODE] = "IDAPPL";   /* ID Applied */
const char LABL_ACDS  [NULL_CODE] = "ACDS";   /* Activity Description */
const char LABL_AACT  [NULL_CODE] = "AACT";   /* All Activities */
const char LABL_SPLU  [NULL_CODE] = "SPLU";   /* Stores */
const char LABL_WPLU  [NULL_CODE] = "WPLU";   /* Warehouses */
const char LABL_IPLU  [NULL_CODE] = "IPLU";   /* Internal Finishers */
const char LABL_EPLU  [NULL_CODE] = "EPLU";   /* External Finishers */
const char LABL_WH    [NULL_CODE] = "WH";   /* Warehouse */
const char LABL_NOEXST[NULL_CODE] = "NOEXST";   /* No longer exists in pricing system */
const char LABL_OWCUD [NULL_CODE] = "OWCUD";   /* Worksheet PO Clean Up Delay */
const char LABL_OPRCD [NULL_CODE] = "OPRCD";   /* Partially Received PO Close Delay */
const char LABL_OACD  [NULL_CODE] = "OACD";   /* Approved PO Close Delay */
const char LABL_DIYRS [NULL_CODE] = "DIYRS";   /* Years */
const char LABL_RSUD  [NULL_CODE] = "RSUD";   /* Retention of Scheduled Updates Days */
const char LABL_MRD   [NULL_CODE] = "MRD";   /* Retention for MRT Transfers */
const char LABL_RRTI  [NULL_CODE] = "RRTI";   /* RTV/RAC Transfer */
const char LABL_MSTSSR[NULL_CODE] = "MSTSSR";   /* Store-to-Store */
const char LABL_MWTSSR[NULL_CODE] = "MWTSSR";   /* Whs-to-Store */
const char LABL_MSTWSR[NULL_CODE] = "MSTWSR";   /* Store-to-Whs */
const char LABL_MWTWSR[NULL_CODE] = "MWTWSR";   /* Whs-to-Whs */
const char LABL_RCAT  [NULL_CODE] = "RCAT";   /* Receiver Cost Adjustment */
const char LABL_CIF   [NULL_CODE] = "CIF";   /* Frequency */
const char LABL_CIISLI[NULL_CODE] = "CIISLI";   /* Level */
const char LABL_AIPI  [NULL_CODE] = "AIPI";   /* Advance Inventory Planning */
const char LABL_SPAGI [NULL_CODE] = "SPAGI";   /* Automatically Generate ID */
const char LABL_PIUI  [NULL_CODE] = "PIUI";   /* Partner ID Uniq Across All Partner Types */
const char LABL_CNCR  [NULL_CODE] = "CNCR";   /* Concession Rate */
const char LABL_CNSR  [NULL_CODE] = "CNSR";   /* Consignment Rate */
const char LABL_FRMLOC[NULL_CODE] = "FRMLOC";   /* From Loc */
const char LABL_FINSHR[NULL_CODE] = "FINSHR";   /* Finisher */
const char LABL_TSFPRC[NULL_CODE] = "TSFPRC";   /* Transfer
 Price */

const char LABL_TSFPAT[NULL_CODE] = "TSFPAT";   /* Transfer Price Adjustment Type */
const char LABL_TSFCST[NULL_CODE] = "TSFCST";   /* Transfer
 Cost */

const char LABL_TSFCAT[NULL_CODE] = "TSFCAT";   /* Transfer Cost
 Adjustment Type */

const char LABL_UL    [NULL_CODE] = "UL";   /* Upper Limit */
const char LABL_LL    [NULL_CODE] = "LL";   /* Lower Limit */
const char LABL_LSTWHS[NULL_CODE] = "LSTWHS";   /* List of Warehouses */
const char LABL_LSTSTS[NULL_CODE] = "LSTSTS";   /* List of Stores */
const char LABL_VIEWBY[NULL_CODE] = "VIEWBY";   /* View By */
const char LABL_DATEEN[NULL_CODE] = "DATEEN";   /* Date Entry     (date) */
const char LABL_UDA1  [NULL_CODE] = "UDA1";   /* UDA - List of Values */
const char LABL_UDA2  [NULL_CODE] = "UDA2";   /* UDA - Dates */
const char LABL_UDA3  [NULL_CODE] = "UDA3";   /* UDA - Free Form Text */
const char LABL_TOTALW[NULL_CODE] = "TOTALW";   /* Total Def. Wizard  (satotal) */
const char LABL_RULEW [NULL_CODE] = "RULEW";   /* Rules  Def. Wizard  (sarule) */
const char LABL_SHOWDT[NULL_CODE] = "SHOWDT";   /* Show Detail */
const char LABL_CONTOF[NULL_CODE] = "CONTOF";   /* Contents of */
const char LABL_REF1DE[NULL_CODE] = "REF1DE";   /* Reference No. 1 */
const char LABL_REF2DE[NULL_CODE] = "REF2DE";   /* Reference No. 2 */
const char LABL_USEOWD[NULL_CODE] = "USEOWD";   /* Unit Sales by End of Week Date */
const char LABL_UIEOWD[NULL_CODE] = "UIEOWD";   /* Unit Issues by End of Week Date */
const char LABL_AREA  [NULL_CODE] = "AREA";   /* @OH3@ */
const char LABL_CHAIN [NULL_CODE] = "CHAIN";   /* @OH2@ */
const char LABL_DISTRI[NULL_CODE] = "DISTRI";   /* @OH5@ */
const char LABL_VIEWNM[NULL_CODE] = "VIEWNM";   /* View Name */
const char LABL_REALMI[NULL_CODE] = "REALMI";   /* Realm Id */
const char LABL_REALMA[NULL_CODE] = "REALMA";   /* Realm Alias */
const char LABL_PARAM [NULL_CODE] = "PARAM";   /* Parameter */
const char LABL_PARAMA[NULL_CODE] = "PARAMA";   /* Parameter Alias */
const char LABL_MODE  [NULL_CODE] = "MODE";   /* Mode */
const char LABL_BLOCK [NULL_CODE] = "BLOCK";   /* Block */
const char LABL_RECGRP[NULL_CODE] = "RECGRP";   /* Record Group Column */
const char LABL_SUBMIT[NULL_CODE] = "SUBMIT";   /* submitted */
const char LABL_APPRVE[NULL_CODE] = "APPRVE";   /* approved */
const char LABL_PICKUP[NULL_CODE] = "PICKUP";   /* Pickup Date */
const char LABL_NOTBEF[NULL_CODE] = "NOTBEF";   /* Not Before Date */
const char LABL_NOTAFT[NULL_CODE] = "NOTAFT";   /* Not After Date */
const char LABL_REGION[NULL_CODE] = "REGION";   /* @OH4@ */
const char LABL_SIZE  [NULL_CODE] = "SIZE";   /* Size */
const char LABL_INFO  [NULL_CODE] = "INFO";   /* Information */
const char LABL_COST  [NULL_CODE] = "COST";   /* Cost */
const char LABL_NEEDDT[NULL_CODE] = "NEEDDT";   /* Need Date */
const char LABL_VDATE [NULL_CODE] = "VDATE";   /* Business Date */
const char LABL_ASN   [NULL_CODE] = "ASN";   /* ASN */
const char LABL_FLSOB [NULL_CODE] = "FLSOB";   /* From Set of Books */
const char LABL_TLSOB [NULL_CODE] = "TLSOB";   /* To Set of Books */
const char LABL_FNSOB [NULL_CODE] = "FNSOB";   /* Finisher Set of Books */
const char LABL_CUSDLY[NULL_CODE] = "CUSDLY";   /* Customer Delivery Date */
const char LABL_CUSDLT[NULL_CODE] = "CUSDLT";   /* Customer Delivery Time */
const char LABL_BINFO [NULL_CODE] = "BINFO";   /* Billing Information */
const char LABL_DINFO [NULL_CODE] = "DINFO";   /* Delivery Information */
const char LABL_NOMF1 [NULL_CODE] = "NOMF1";   /* Flag 1 */
const char LABL_NOMF2 [NULL_CODE] = "NOMF2";   /* In Duty */
const char LABL_NOMF3 [NULL_CODE] = "NOMF3";   /* Flag 3 */
const char LABL_NOMF4 [NULL_CODE] = "NOMF4";   /* In Exp */
const char LABL_NOMF5 [NULL_CODE] = "NOMF5";   /* In ALC */
const char LABL_R     [NULL_CODE] = "R";   /* Supplier Revision */
const char LABL_RV    [NULL_CODE] = "RV";   /* Retailer Version */
const char LABL_MARGIN[NULL_CODE] = "MARGIN";   /* Margin */
const char LABL_RETAIL[NULL_CODE] = "RETAIL";   /* Retail */
const char LABL_MARGPT[NULL_CODE] = "MARGPT";   /* Margin Percent */
const char LABL_RETLPT[NULL_CODE] = "RETLPT";   /* Retail Percent */
const char LABL_FCOST [NULL_CODE] = "FCOST";   /* Fixed Cost */
const char LABL_CRET  [NULL_CODE] = "CRET";   /* Completed Reclassification Event */
const char LABL_OAL   [NULL_CODE] = "OAL";   /* Order Appr. Limit */
const char LABL_TCC   [NULL_CODE] = "TCC";   /* Total Customer Cost */
const char LABL_TP    [NULL_CODE] = "TP";   /* Transfer Price */
const char LABL_ADJRC [NULL_CODE] = "ADJRC";   /* Adjustment Reason Code */
const char LABL_FRMLC [NULL_CODE] = "FRMLC";   /* From Location */
const char LABL_TOLOC [NULL_CODE] = "TOLOC";   /* To Location */
const char LABL_ACTID [NULL_CODE] = "ACTID";   /* Activity ID */
const char LABL_TRFNO [NULL_CODE] = "TRFNO";   /* Tran Ref No */
const char LABL_TCAR  [NULL_CODE] = "TCAR";   /* Adjustment Reason Code */
const char LABL_TCFL  [NULL_CODE] = "TCFL";   /* From Location */
const char LABL_TCTL  [NULL_CODE] = "TCTL";   /* To Location */
const char LABL_TCTR  [NULL_CODE] = "TCTR";   /* Tran. Ref. No. */
const char LABL_SP    [NULL_CODE] = "SP";   /* space */
const char LABL_CTMZ  [NULL_CODE] = "CTMZ";   /* Customize */
const char LABL_BLANKC[NULL_CODE] = "BLANKC";   /* Blank Color */
const char LABL_BLANKF[NULL_CODE] = "BLANKF";   /* Blank Flavor */
const char LABL_BLANKP[NULL_CODE] = "BLANKP";   /* Blank Pattern */
const char LABL_BLANKE[NULL_CODE] = "BLANKE";   /* Blank Scent */
const char LABL_BLANKS[NULL_CODE] = "BLANKS";   /* Blank Size */
const char LABL_ASATW [NULL_CODE] = "ASATW";   /* @OHP3@ Associated with Trait */
const char LABL_TSAWR [NULL_CODE] = "TSAWR";   /* Traits Associated with @OHP4@ */
const char LABL_RSAWT [NULL_CODE] = "RSAWT";   /* @OHP4@ Associated with Trait */
const char LABL_SDSFA [NULL_CODE] = "SDSFA";   /* Store Department Square Footage Area */
const char LABL_ISIZE [NULL_CODE] = "ISIZE";   /* Inner Size */
const char LABL_CSIZE [NULL_CODE] = "CSIZE";   /* Case Size */
const char LABL_CNTACT[NULL_CODE] = "CNTACT";   /* Contact */
const char LABL_TO    [NULL_CODE] = "TO";   /* To */
const char LABL_SEARCH[NULL_CODE] = "SEARCH";   /* Search */
const char LABL_APPT  [NULL_CODE] = "APPT";   /* Appointment */
const char LABL_APPTD [NULL_CODE] = "APPTD";   /* Appointment Details */
const char LABL_FILCRT[NULL_CODE] = "FILCRT";   /* Filter Criteria */
const char LABL_SCALCO[NULL_CODE] = "SCALCO";   /* Scaling Constraints */
const char LABL_COMPIN[NULL_CODE] = "COMPIN";   /* Competitor Info */
const char LABL_MCUC  [NULL_CODE] = "MCUC";   /* Mass Change Unit Cost */
const char LABL_BLIT  [NULL_CODE] = "BLIT";   /* Blank Item */
const char LABL_COMPTO[NULL_CODE] = "COMPTO";   /* Compared To */
const char LABL_FILBY [NULL_CODE] = "FILBY";   /* Filter By */
const char LABL_ENTAS [NULL_CODE] = "ENTAS";   /* Enter As: */
const char LABL_TRANL [NULL_CODE] = "TRANL";   /* Tran Level: */
const char LABL_SUNAB [NULL_CODE] = "SUNAB";   /* Sun */
const char LABL_MONAB [NULL_CODE] = "MONAB";   /* Mon */
const char LABL_FRIAB [NULL_CODE] = "FRIAB";   /* Fri */
const char LABL_TUEAB [NULL_CODE] = "TUEAB";   /* Tue */
const char LABL_WEDAB [NULL_CODE] = "WEDAB";   /* Wed */
const char LABL_SATAB [NULL_CODE] = "SATAB";   /* Sat */
const char LABL_THUAB [NULL_CODE] = "THUAB";   /* Thu */
const char LABL_NOTBET[NULL_CODE] = "NOTBET";   /* Not Before Datetime */
const char LABL_DEALH [NULL_CODE] = "DEALH";   /* Deal Head */
const char LABL_DEALD [NULL_CODE] = "DEALD";   /* Deal Head */
const char LABL_GET   [NULL_CODE] = "GET";   /* Get */
const char LABL_BUY   [NULL_CODE] = "BUY";   /* Buy */
const char LABL_LOCS1 [NULL_CODE] = "LOCS1";   /* Locations */
const char LABL_MUCTYP[NULL_CODE] = "MUCTYP";   /* Markup Calculation Type */
const char LABL_PCTYP [NULL_CODE] = "PCTYP";   /* Profit Calculation Type */
const char LABL_OTBTYP[NULL_CODE] = "OTBTYP";   /* OTB Calculation Type */
const char LABL_PURTYP[NULL_CODE] = "PURTYP";   /* Purchase Type */
const char LABL_BTPER [NULL_CODE] = "BTPER";   /* Build Time Period */
const char LABL_GENBY [NULL_CODE] = "GENBY";   /* Generated By */
const char LABL_SALTYP[NULL_CODE] = "SALTYP";   /* Sales Type */
const char LABL_FILTER[NULL_CODE] = "FILTER";   /* Filter */
const char LABL_NIUI  [NULL_CODE] = "NIUI";   /* New Items/Unavailable Items */
const char LABL_DIFFS [NULL_CODE] = "DIFFS";   /* Diffs */
const char LABL_OVRRID[NULL_CODE] = "OVRRID";   /* Override */
const char LABL_GMLVL [NULL_CODE] = "GMLVL";   /* @MH3@/Merchandise Level */
const char LABL_GOLVL [NULL_CODE] = "GOLVL";   /* @MH3@/Organization Level */
const char LABL_TOTALS[NULL_CODE] = "TOTALS";   /* Totals */
const char LABL_DIFFER[NULL_CODE] = "DIFFER";   /* Differentiators */
const char LABL_X     [NULL_CODE] = "X";   /* X */
const char LABL_GENINF[NULL_CODE] = "GENINF";   /* General Info */
const char LABL_DEMINS[NULL_CODE] = "DEMINS";   /* Dimensions */
const char LABL_WEIGHT[NULL_CODE] = "WEIGHT";   /* Weight */
const char LABL_PACKS [NULL_CODE] = "PACKS";   /* Packs */
const char LABL_PRMSRC[NULL_CODE] = "PRMSRC";   /* Primary Sourcing */
const char LABL_CPKINF[NULL_CODE] = "CPKINF";   /* Case Pack Information */
const char LABL_PAR   [NULL_CODE] = "PAR";   /* Parent */
const char LABL_LTIHI [NULL_CODE] = "LTIHI";   /* Location Ti - Hi */
const char LABL_RCOM  [NULL_CODE] = "RCOM";   /* Customer Order Specific Attributes */
const char LABL_DEPITM[NULL_CODE] = "DEPITM";   /* Deposit Item */
const char LABL_CTCHWT[NULL_CODE] = "CTCHWT";   /* Catch Weight */
const char LABL_ACNTIM[NULL_CODE] = "ACNTIM";   /* Associated Contents Items */
const char LABL_ATTRS [NULL_CODE] = "ATTRS";   /* Attributes */
const char LABL_PRMSUP[NULL_CODE] = "PRMSUP";   /* Primary Supplier */
const char LABL_PRMCTY[NULL_CODE] = "PRMCTY";   /* Primary Country */
const char LABL_POINFO[NULL_CODE] = "POINFO";   /* Ordering Information */
const char LABL_SPTIHI[NULL_CODE] = "SPTIHI";   /* Supplier/Inbound Ti - Hi */
const char LABL_RNDTHR[NULL_CODE] = "RNDTHR";   /* Rounding Thresholds */
const char LABL_TOLTYP[NULL_CODE] = "TOLTYP";   /* Tolerance Type */
const char LABL_OCDETL[NULL_CODE] = "OCDETL";   /* Country Of Sourcing Details */
const char LABL_LOCDIN[NULL_CODE] = "LOCDIN";   /* Location Default Information */
const char LABL_PRMARY[NULL_CODE] = "PRMARY";   /* Primary */
const char LABL_SECOND[NULL_CODE] = "SECOND";   /* Secondary */
const char LABL_STSALE[NULL_CODE] = "STSALE";   /* Store Sales */
const char LABL_SOH   [NULL_CODE] = "SOH";   /* Stock On Hand */
const char LABL_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char LABL_COPYFR[NULL_CODE] = "COPYFR";   /* Copy From */
const char LABL_RTVDTL[NULL_CODE] = "RTVDTL";   /* RTV Details */
const char LABL_MRTDTL[NULL_CODE] = "MRTDTL";   /* MRT Details */
const char LABL_ADITMS[NULL_CODE] = "ADITMS";   /* Add Items */
const char LABL_ITMDTL[NULL_CODE] = "ITMDTL";   /* Item Details */
const char LABL_LOCDTL[NULL_CODE] = "LOCDTL";   /* Location Details */
const char LABL_APPDEL[NULL_CODE] = "APPDEL";   /* Apply or Delete To: */
const char LABL_ALTCP [NULL_CODE] = "ALTCP";   /* Apply Locations and Transfer Cost/Price */
const char LABL_CURNAM[NULL_CODE] = "CURNAM";   /* Current Name */
const char LABL_NEWNAM[NULL_CODE] = "NEWNAM";   /* New Name */
const char LABL_DEFNAM[NULL_CODE] = "DEFNAM";   /* Default Name */
const char LABL_PHNCON[NULL_CODE] = "PHNCON";   /* Phone Numbers/Contact Name */
const char LABL_OINFO [NULL_CODE] = "OINFO";   /* Other Information */
const char LABL_MIN1  [NULL_CODE] = "MIN1";   /* Minimum 1 */
const char LABL_MIN2  [NULL_CODE] = "MIN2";   /* Minimum 2 */
const char LABL_MINCON[NULL_CODE] = "MINCON";   /* Minimum Constraints */
const char LABL_TRSCON[NULL_CODE] = "TRSCON";   /* Truck Splitting Constraints */
const char LABL_SUPPOC[NULL_CODE] = "SUPPOC";   /* Supplier Order Costs */
const char LABL_QTYS  [NULL_CODE] = "QTYS";   /* Quantities */
const char LABL_PODUES[NULL_CODE] = "PODUES";   /* Order Due Status */
const char LABL_CON2  [NULL_CODE] = "CON2";   /* Constraint 2 */
const char LABL_CON1  [NULL_CODE] = "CON1";   /* Constraint 1 */
const char LABL_ISSUES[NULL_CODE] = "ISSUES";   /* Issues */
const char LABL_SUPPOL[NULL_CODE] = "SUPPOL";   /* Supplier Pooling */
const char LABL_POCOST[NULL_CODE] = "POCOST";   /* Order Costs */
const char LABL_POQTYS[NULL_CODE] = "POQTYS";   /* Order Quantities */
const char LABL_ALLOCD[NULL_CODE] = "ALLOCD";   /* Allocation Details */
const char LABL_PODETL[NULL_CODE] = "PODETL";   /* Order Details */
const char LABL_LOCATS[NULL_CODE] = "LOCATS";   /* Order Details */
const char LABL_DIFMTX[NULL_CODE] = "DIFMTX";   /* Diff Matrix */
const char LABL_ENTRAS[NULL_CODE] = "ENTRAS";   /* Enter As */
const char LABL_OHDCHG[NULL_CODE] = "OHDCHG";   /* Order Header Date Changes */
const char LABL_TYPE  [NULL_CODE] = "TYPE";   /* Type */
const char LABL_CQTYIL[NULL_CODE] = "CQTYIL";   /* Cost and Qty Changes By Item/Location */
const char LABL_MODLST[NULL_CODE] = "MODLST";   /* Modify List */
const char LABL_PNDPRC[NULL_CODE] = "PNDPRC";   /* Pending Price Changes */
const char LABL_LOCSEL[NULL_CODE] = "LOCSEL";   /* Location Selection */
const char LABL_PRCDEF[NULL_CODE] = "PRCDEF";   /* Price Change Default */
const char LABL_DMTXBY[NULL_CODE] = "DMTXBY";   /*  Distribute Matrix  By */
const char LABL_DSTZAB[NULL_CODE] = "DSTZAB";   /* Distribute Z Axis By */
const char LABL_AXDGRP[NULL_CODE] = "AXDGRP";   /* Axes Diff Groups */
const char LABL_DRLMT [NULL_CODE] = "DRLMT";   /* Diff Range Limiting */
const char LABL_ZAXIS [NULL_CODE] = "ZAXIS";   /* Z Axis */
const char LABL_DFFRT [NULL_CODE] = "DFFRT";   /* Diff Ratio */
const char LABL_XTOTAL[NULL_CODE] = "XTOTAL";   /* X Total */
const char LABL_XYDIST[NULL_CODE] = "XYDIST";   /* X-Y Distribution */
const char LABL_DISTY [NULL_CODE] = "DISTY";   /* Distribute Y By */
const char LABL_XDIST [NULL_CODE] = "XDIST";   /* X Distribution */
const char LABL_DISTX [NULL_CODE] = "DISTX";   /* Distribute X By */
const char LABL_PRFFUL[NULL_CODE] = "PRFFUL";   /* Proof of Performance Fulfillment */
const char LABL_PRFDEF[NULL_CODE] = "PRFDEF";   /* Proof of Performance Definitions */
const char LABL_MOVE  [NULL_CODE] = "MOVE";   /* Move */
const char LABL_PREPLP[NULL_CODE] = "PREPLP";   /* Primary Replenishment Pack */
const char LABL_STKVAL[NULL_CODE] = "STKVAL";   /* Stock Values */
const char LABL_LDTIME[NULL_CODE] = "LDTIME";   /* Lead Times */
const char LABL_RPLCYL[NULL_CODE] = "RPLCYL";   /* Replenishment Cycle */
const char LABL_REPLDT[NULL_CODE] = "REPLDT";   /* Replenishment Dates */
const char LABL_RPLMA [NULL_CODE] = "RPLMA";   /* Replenishment Method Attributes */
const char LABL_RPLAMA[NULL_CODE] = "RPLAMA";   /* Repl Attribute Maintenance Action */
const char LABL_UINFO [NULL_CODE] = "UINFO";   /* User Information */
const char LABL_SCHADT[NULL_CODE] = "SCHADT";   /* Scheduled Active Date */
const char LABL_MADMIN[NULL_CODE] = "MADMIN";   /* Master Administration */
const char LABL_AOCTOL[NULL_CODE] = "AOCTOL";   /* Allowable Order Change Tolerances */
const char LABL_SCALE [NULL_CODE] = "SCALE";   /* Scaling */
const char LABL_RECALC[NULL_CODE] = "RECALC";   /* Recalculation */
const char LABL_ORDREV[NULL_CODE] = "ORDREV";   /* Order Review */
const char LABL_DOINFO[NULL_CODE] = "DOINFO";   /* Due Order Information */
const char LABL_CPOSTS[NULL_CODE] = "CPOSTS";   /* Current Order Status */
const char LABL_REPLC [NULL_CODE] = "REPLC";   /* Replenishment Costs */
const char LABL_GENFOR[NULL_CODE] = "GENFOR";   /* Generated Forecast Results */
const char LABL_LOCINV[NULL_CODE] = "LOCINV";   /* Location Inventory */
const char LABL_PACKSZ[NULL_CODE] = "PACKSZ";   /* Pack Sizes */
const char LABL_REPLRS[NULL_CODE] = "REPLRS";   /* Replenishment Results */
const char LABL_OITEM [NULL_CODE] = "OITEM";   /* Original Item */
const char LABL_CTITEM[NULL_CODE] = "CTITEM";   /* Change To Item */
const char LABL_RELDTS[NULL_CODE] = "RELDTS";   /* Item Relationship Details */
const char LABL_RELIMS[NULL_CODE] = "RELIMS";   /* Related Items */
const char LABL_PRESAB[NULL_CODE] = "PRESAB";   /* Presence or absence creates error? */
const char LABL_EXGRP [NULL_CODE] = "EXGRP";   /* Execute Group */
const char LABL_EXGRPD[NULL_CODE] = "EXGRPD";   /* Execute Group Details */
const char LABL_WIZRUL[NULL_CODE] = "WIZRUL";   /* Will you use the wizard to write a rule? */
const char LABL_BDRANG[NULL_CODE] = "BDRANG";   /* Business Day Range? */
const char LABL_STORES[NULL_CODE] = "STORES";   /* Stores */
const char LABL_STATUS[NULL_CODE] = "STATUS";   /* Status */
const char LABL_STSTAT[NULL_CODE] = "STSTAT";   /* Store Status */
const char LABL_HQSTAT[NULL_CODE] = "HQSTAT";   /* Headquarter Status */
const char LABL_TINFO [NULL_CODE] = "TINFO";   /* Transaction Information */
const char LABL_FMERCH[NULL_CODE] = "FMERCH";   /* Do you have Fuel Merchandise? */
const char LABL_SVALCO[NULL_CODE] = "SVALCO";   /* System Validation Constraints */
const char LABL_CSTCRT[NULL_CODE] = "CSTCRT";   /* Compare Store Criteria */
const char LABL_ESCIND[NULL_CODE] = "ESCIND";   /* Escheatment Indicator */
const char LABL_TVWOPT[NULL_CODE] = "TVWOPT";   /* Total View Option */
const char LABL_ACHINF[NULL_CODE] = "ACHINF";   /* ACH Information */
const char LABL_RINADJ[NULL_CODE] = "RINADJ";   /* Retailer Details for Income Adjustment */
const char LABL_TNDARG[NULL_CODE] = "TNDARG";   /* Tender Amount Range */
const char LABL_AUDTRD[NULL_CODE] = "AUDTRD";   /* Audit Trail Details */
const char LABL_BSDYRG[NULL_CODE] = "BSDYRG";   /* Business Day Range */
const char LABL_UPDTRG[NULL_CODE] = "UPDTRG";   /* Update Date/Time Range */
const char LABL_TOTDTL[NULL_CODE] = "TOTDTL";   /* Totaling Details */
const char LABL_TRDTRG[NULL_CODE] = "TRDTRG";   /* Tran. Date/Time Range */
const char LABL_POSTRG[NULL_CODE] = "POSTRG";   /* POS Tran. No. Range */
const char LABL_STUOMC[NULL_CODE] = "STUOMC";   /* Standard Unit of Measure Conversions */
const char LABL_SELUOM[NULL_CODE] = "SELUOM";   /* Selling Unit of Measure */
const char LABL_STDUOM[NULL_CODE] = "STDUOM";   /* Standard Unit of Measure */
const char LABL_DSCUOM[NULL_CODE] = "DSCUOM";   /* Discount Unit of Measure */
const char LABL_TLCOA [NULL_CODE] = "TLCOA";   /* Transaction Level Customer Order Attribs */
const char LABL_ITMREF[NULL_CODE] = "ITMREF";   /* Item Reference */
const char LABL_TNDREF[NULL_CODE] = "TNDREF";   /* Tender Reference */
const char LABL_TXNREF[NULL_CODE] = "TXNREF";   /* Transaction Reference */
const char LABL_ISSSLD[NULL_CODE] = "ISSSLD";   /* Issued/Sold */
const char LABL_ILCOA [NULL_CODE] = "ILCOA";   /* Item Level Customer Order Attributes */
const char LABL_REDEEM[NULL_CODE] = "REDEEM";   /* Redeemed */
const char LABL_ASSIGN[NULL_CODE] = "ASSIGN";   /* Assigned */
const char LABL_TAXREF[NULL_CODE] = "TAXREF";   /* Tax Reference */
const char LABL_SHDTS [NULL_CODE] = "SHDTS";   /* Shipment Dates */
const char LABL_RECDAT[NULL_CODE] = "RECDAT";   /* Shipment Dates */
const char LABL_FROM  [NULL_CODE] = "FROM";   /* From */
const char LABL_MERCHH[NULL_CODE] = "MERCHH";   /* Merchandise Hierarchy */
const char LABL_ORGH  [NULL_CODE] = "ORGH";   /* Organizational Hierarchy */
const char LABL_DETLS [NULL_CODE] = "DETLS";   /* Details */
const char LABL_DETLIN[NULL_CODE] = "DETLIN";   /* Detail Information */
const char LABL_HEADIN[NULL_CODE] = "HEADIN";   /* Header Information */
const char LABL_COUNT [NULL_CODE] = "COUNT";   /* Count */
const char LABL_LIST2 [NULL_CODE] = "LIST2";   /* List */
const char LABL_CINFCI[NULL_CODE] = "CINFCI";   /* Copy Info From Component Item */
const char LABL_DIMINF[NULL_CODE] = "DIMINF";   /* Dimension Info */
const char LABL_APKINF[NULL_CODE] = "APKINF";   /* Apply Pack Info */
const char LABL_TOLS  [NULL_CODE] = "TOLS";   /* Tolerances */
const char LABL_CUR   [NULL_CODE] = "CUR";   /* Currency */
const char LABL_SITMBY[NULL_CODE] = "SITMBY";   /* Select Item By: */
const char LABL_DAYCNT[NULL_CODE] = "DAYCNT";   /* Days of Count */
const char LABL_REPL  [NULL_CODE] = "REPL";   /* Replenishment */
const char LABL_INVBUY[NULL_CODE] = "INVBUY";   /* Investment Buy */
const char LABL_LOC   [NULL_CODE] = "LOC";   /* Location */
const char LABL_DATERG[NULL_CODE] = "DATERG";   /* Date Range */
const char LABL_APPLYB[NULL_CODE] = "APPLYB";   /* Apply Block */
const char LABL_FILPRI[NULL_CODE] = "FILPRI";   /* Fill Priority */
const char LABL_DOPROC[NULL_CODE] = "DOPROC";   /* Due Order Processing */
const char LABL_SCALAT[NULL_CODE] = "SCALAT";   /* Scaling Attributes */
const char LABL_INVBAT[NULL_CODE] = "INVBAT";   /* Investment Buy Attributes */
const char LABL_DFSCLT[NULL_CODE] = "DFSCLT";   /* Default From Scaling Constraints to: */
const char LABL_TRSATT[NULL_CODE] = "TRSATT";   /* Truck Splitting Attributes */
const char LABL_RNDATT[NULL_CODE] = "RNDATT";   /* Rounding Attributes */
const char LABL_SMNATT[NULL_CODE] = "SMNATT";   /* Supplier Minimum Attributes */
const char LABL_OR    [NULL_CODE] = "OR";   /* Or */
const char LABL_SUPEDI[NULL_CODE] = "SUPEDI";   /* EDI Trans. Supported By the Supplier */
const char LABL_EDICCV[NULL_CODE] = "EDICCV";   /* EDI Cost Change Approval Variance */
const char LABL_INDS  [NULL_CODE] = "INDS";   /* Indicators */
const char LABL_HQCTRY[NULL_CODE] = "HQCTRY";   /* Corporate HQ Country */
const char LABL_INVENT[NULL_CODE] = "INVENT";   /* Inventory */
const char LABL_PRMLNG[NULL_CODE] = "PRMLNG";   /* Primary Language */
const char LABL_CHANNL[NULL_CODE] = "CHANNL";   /* Channel */
const char LABL_TSFS  [NULL_CODE] = "TSFS";   /* Transfers */
const char LABL_MONTHS[NULL_CODE] = "MONTHS";   /* Months */
const char LABL_MLOCRD[NULL_CODE] = "MLOCRD";   /* Markdown Location For Retail Difference */
const char LABL_HTS   [NULL_CODE] = "HTS";   /* HTS */
const char LABL_RIMOPT[NULL_CODE] = "RIMOPT";   /* Invoice Matching Options */
const char LABL_DAYS  [NULL_CODE] = "DAYS";   /* Days */
const char LABL_DEFUOP[NULL_CODE] = "DEFUOP";   /* Default Unit of Purchase */
const char LABL_ITMOPT[NULL_CODE] = "ITMOPT";   /* Item Options */
const char LABL_DEFUOM[NULL_CODE] = "DEFUOM";   /* Default UOM */
const char LABL_SOB   [NULL_CODE] = "SOB";   /* Set of Books */
const char LABL_DISTRO[NULL_CODE] = "DISTRO";   /* Distribution */
const char LABL_PACKNG[NULL_CODE] = "PACKNG";   /* Packing */
const char LABL_TCKLBL[NULL_CODE] = "TCKLBL";   /* Tickets/Labels */
const char LABL_TIMPRD[NULL_CODE] = "TIMPRD";   /* Time Period Detail */
const char LABL_SYSREC[NULL_CODE] = "SYSREC";   /* System of Record */
const char LABL_STOREC[NULL_CODE] = "STOREC";   /* Stock Order Reconciliation */
const char LABL_LOVFIL[NULL_CODE] = "LOVFIL";   /* LOV Filtering */
const char LABL_OUTSTR[NULL_CODE] = "OUTSTR";   /* Outside Storage */
const char LABL_WHSTR [NULL_CODE] = "WHSTR";   /* Warehouse Storage */
const char LABL_DEALS [NULL_CODE] = "DEALS";   /* Deals */
const char LABL_LTRCRT[NULL_CODE] = "LTRCRT";   /* Letter of Credit */
const char LABL_ORDERS[NULL_CODE] = "ORDERS";   /* Orders */
const char LABL_EDITXN[NULL_CODE] = "EDITXN";   /* EDI Transactions */
const char LABL_CNTRTS[NULL_CODE] = "CNTRTS";   /* Contracts */
const char LABL_NWPPRC[NULL_CODE] = "NWPPRC";   /* LVP Process Controls */
const char LABL_HISTRY[NULL_CODE] = "HISTRY";   /* History */
const char LABL_BILLAD[NULL_CODE] = "BILLAD";   /* Bill of Lading */
const char LABL_STKCNT[NULL_CODE] = "STKCNT";   /* Stock Count */
const char LABL_COSTMD[NULL_CODE] = "COSTMD";   /* Cost Method */
const char LABL_CHKDIG[NULL_CODE] = "CHKDIG";   /* Check Digit */
const char LABL_DCSTYP[NULL_CODE] = "DCSTYP";   /* Discount Type */
const char LABL_APPLTP[NULL_CODE] = "APPLTP";   /* Apply Type */
const char LABL_CNTXTD[NULL_CODE] = "CNTXTD";   /* Context Details */
const char LABL_WLKTST[NULL_CODE] = "WLKTST";   /* Walk Through Store */
const char LABL_CSTLOC[NULL_CODE] = "CSTLOC";   /* Cost Location */
const char LABL_RLMS  [NULL_CODE] = "RLMS";   /* Realms */
const char LABL_SELRLM[NULL_CODE] = "SELRLM";   /* Selected Realm */
const char LABL_RRBTV [NULL_CODE] = "RRBTV";   /* Restrict Results By Table Values */
const char LABL_RRBCV [NULL_CODE] = "RRBCV";   /* Restrict Results By Constant Values */
const char LABL_PCTSG [NULL_CODE] = "PCTSG";   /* Pct */
const char LABL_MRKUPR[NULL_CODE] = "MRKUPR";   /* Markup Pct Retail */
const char LABL_ROUNDP[NULL_CODE] = "ROUNDP";   /* Rounding Pct */
const char LABL_LOCSQ [NULL_CODE] = "LOCSQ";   /* locations? */
const char LABL_STDAYQ[NULL_CODE] = "STDAYQ";   /* store/day? */
const char LABL_EVALQ [NULL_CODE] = "EVALQ";   /* evaluation? */
const char LABL_DTATIM[NULL_CODE] = "DTATIM";   /* Date And Time */
const char LABL_WHISS [NULL_CODE] = "WHISS";   /* Warehouse Issues */
const char LABL_RPLATT[NULL_CODE] = "RPLATT";   /* Replenishment Attributes */
const char LABL_BRKTCO[NULL_CODE] = "BRKTCO";   /* Bracket Constraints */
const char LABL_OTHRAT[NULL_CODE] = "OTHRAT";   /* Other Attributes */
const char LABL_STKLGR[NULL_CODE] = "STKLGR";   /* Stock Ledger */
const char LABL_RECV  [NULL_CODE] = "RECV";   /* Receiving */
const char LABL_OPENTB[NULL_CODE] = "OPENTB";   /* Open to Buy */
const char LABL_VATT  [NULL_CODE] = "VATT";   /* VAT */
const char LABL_EXTSYS[NULL_CODE] = "EXTSYS";   /* External Systems */
const char LABL_CNSPOI[NULL_CODE] = "CNSPOI";   /* Consignment POs and Invoices */
const char LABL_SUPPAR[NULL_CODE] = "SUPPAR";   /* Supplier-Partner */
const char LABL_GLROLU[NULL_CODE] = "GLROLU";   /* Finance */
const char LABL_PCCL  [NULL_CODE] = "PCCL";   /* Price Change / Clearance */
const char LABL_AA2BC [NULL_CODE] = "AA2BC";   /* Attributes Available to be Copied */
const char LABL_ACINFO[NULL_CODE] = "ACINFO";   /* Accounting Information */
const char LABL_ACOPNS[NULL_CODE] = "ACOPNS";   /* At Cost OR % of Net Sales */
const char LABL_ACTION[NULL_CODE] = "ACTION";   /* Action */
const char LABL_ADDINF[NULL_CODE] = "ADDINF";   /* Address Info */
const char LABL_ADJAMT[NULL_CODE] = "ADJAMT";   /* Adjustment Amount */
const char LABL_AEDTLS[NULL_CODE] = "AEDTLS";   /* Apply Entry Details */
const char LABL_ALCDET[NULL_CODE] = "ALCDET";   /* View ALC details for: */
const char LABL_ALVDTS[NULL_CODE] = "ALVDTS";   /* Apply License/Visa Details */
const char LABL_AND   [NULL_CODE] = "AND";   /* and */
const char LABL_AORITD[NULL_CODE] = "AORITD";   /* Apply Order/Item Details */
const char LABL_APDTE [NULL_CODE] = "APDTE";   /* Approval Date: */
const char LABL_APLHTS[NULL_CODE] = "APLHTS";   /* Apply HTS */
const char LABL_APPDT [NULL_CODE] = "APPDT";   /* Application Date: */
const char LABL_APPLY [NULL_CODE] = "APPLY";   /* Apply */
const char LABL_APPTTL[NULL_CODE] = "APPTTL";   /* Apply Totals */
const char LABL_APSHPD[NULL_CODE] = "APSHPD";   /* Apply Shipment Details */
const char LABL_APYCRT[NULL_CODE] = "APYCRT";   /* Apply Criteria */
const char LABL_ASDTLS[NULL_CODE] = "ASDTLS";   /* Assessment Details */
const char LABL_ASNVAL[NULL_CODE] = "ASNVAL";   /* Assign Values */
const char LABL_ATCOST[NULL_CODE] = "ATCOST";   /* At Cost */
const char LABL_ATRETL[NULL_CODE] = "ATRETL";   /* At Retail */
const char LABL_BANKS [NULL_CODE] = "BANKS";   /* Banks */
const char LABL_BATSTA[NULL_CODE] = "BATSTA";   /* Batch Status */
const char LABL_BENATT[NULL_CODE] = "BENATT";   /* Beneficiary Attributes */
const char LABL_BKINFO[NULL_CODE] = "BKINFO";   /* Banking Information */
const char LABL_BREAK2[NULL_CODE] = "BREAK2";   /* Break to */
const char LABL_CALCDB[NULL_CODE] = "CALCDB";   /* Calculate Dates Based On */
const char LABL_CCAM  [NULL_CODE] = "CCAM";   /* Cost Change Approval Method */
const char LABL_CGVAL2[NULL_CODE] = "CGVAL2";   /* Change Value To */
const char LABL_CLSD4 [NULL_CODE] = "CLSD4";   /* Closed For */
const char LABL_CNFLCT[NULL_CODE] = "CNFLCT";   /* Conflicts */
const char LABL_COLLDA[NULL_CODE] = "COLLDA";   /* Collect Dates */
const char LABL_COMNOM[NULL_CODE] = "COMNOM";   /* Component Nomination */
const char LABL_CONDIT[NULL_CODE] = "CONDIT";   /* Conditions */
const char LABL_CONDT [NULL_CODE] = "CONDT";   /* Confirmation Date: */
const char LABL_CONINF[NULL_CODE] = "CONINF";   /* Contact Info */
const char LABL_CTGTTH[NULL_CODE] = "CTGTTH";   /* Component Target Thresholds */
const char LABL_CURFMT[NULL_CODE] = "CURFMT";   /* Currency Formats */
const char LABL_CZCHG [NULL_CODE] = "CZCHG";   /* Cost Zone Change */
const char LABL_DATES [NULL_CODE] = "DATES";   /* Dates */
const char LABL_DCOMIN[NULL_CODE] = "DCOMIN";   /* Deal Component Information */
const char LABL_DECPLC[NULL_CODE] = "DECPLC";   /* Decimal Places */
const char LABL_DG    [NULL_CODE] = "DG";   /* Display Group */
const char LABL_DGD   [NULL_CODE] = "DGD";   /* Display Group Details */
const char LABL_DL2DAT[NULL_CODE] = "DL2DAT";   /* Deal to Date */
const char LABL_DLCDAT[NULL_CODE] = "DLCDAT";   /* Deal Component to Date */
const char LABL_DLTTL [NULL_CODE] = "DLTTL";   /* Deal Total */
const char LABL_ENDTLS[NULL_CODE] = "ENDTLS";   /* Entry Details */
const char LABL_ENTOBL[NULL_CODE] = "ENTOBL";   /* Entries / Obligations */
const char LABL_EPDTE [NULL_CODE] = "EPDTE";   /* Effective Date: */
const char LABL_ESHPDT[NULL_CODE] = "ESHPDT";   /* Earliest Ship Date: */
const char LABL_EXDTLS[NULL_CODE] = "EXDTLS";   /* Expense Details */
const char LABL_EXPDAT[NULL_CODE] = "EXPDAT";   /* Expiration Date: */
const char LABL_EXPHDR[NULL_CODE] = "EXPHDR";   /* Expense Header */
const char LABL_EXPRD [NULL_CODE] = "EXPRD";   /* Expense Profile Detail */
const char LABL_EXPRH [NULL_CODE] = "EXPRH";   /* Expense Profile Header */
const char LABL_FIXED [NULL_CODE] = "FIXED";   /* Fixed */
const char LABL_HTSINF[NULL_CODE] = "HTSINF";   /* HTS Information */
const char LABL_IMPATT[NULL_CODE] = "IMPATT";   /* Import Attributes */
const char LABL_INCAMT[NULL_CODE] = "INCAMT";   /* Income Amount */
const char LABL_INUMTY[NULL_CODE] = "INUMTY";   /* Item Number Type */
const char LABL_ITCOLN[NULL_CODE] = "ITCOLN";   /* Item Type: */
const char LABL_ITEMS [NULL_CODE] = "ITEMS";   /* Items */
const char LABL_ITMSEL[NULL_CODE] = "ITMSEL";   /* Item Selection */
const char LABL_ITMTYP[NULL_CODE] = "ITMTYP";   /* Item Type */
const char LABL_LCAMTS[NULL_CODE] = "LCAMTS";   /* LC Amounts */
const char LABL_LEASED[NULL_CODE] = "LEASED";   /* Leased */
const char LABL_LETCC [NULL_CODE] = "LETCC";   /* Location Exceptions to @OH1@ Close */
const char LABL_LOCATT[NULL_CODE] = "LOCATT";   /* Location Attributes */
const char LABL_LOCINF[NULL_CODE] = "LOCINF";   /* Location Information */
const char LABL_LOCLSD[NULL_CODE] = "LOCLSD";   /* Location Closed */
const char LABL_LOCLST[NULL_CODE] = "LOCLST";   /* Location List */
const char LABL_LSHPDT[NULL_CODE] = "LSHPDT";   /* Latest Ship Date: */
const char LABL_LVLCOL[NULL_CODE] = "LVLCOL";   /* Level: */
const char LABL_MONTH1[NULL_CODE] = "MONTH1";   /* Month 1 */
const char LABL_MONTH2[NULL_CODE] = "MONTH2";   /* Month 2 */
const char LABL_MONTH3[NULL_CODE] = "MONTH3";   /* Month 3 */
const char LABL_MONTH4[NULL_CODE] = "MONTH4";   /* Month 4 */
const char LABL_MONTH5[NULL_CODE] = "MONTH5";   /* Month 5 */
const char LABL_MONTH6[NULL_CODE] = "MONTH6";   /* Month 6 */
const char LABL_NETEFT[NULL_CODE] = "NETEFT";   /* Net Effect */
const char LABL_NEW   [NULL_CODE] = "NEW";   /* New */
const char LABL_NEWVAL[NULL_CODE] = "NEWVAL";   /* New Value */
const char LABL_OBLDTL[NULL_CODE] = "OBLDTL";   /* Obligation Detail */
const char LABL_OLD   [NULL_CODE] = "OLD";   /* Old */
const char LABL_OPEN4 [NULL_CODE] = "OPEN4";   /* Open For */
const char LABL_ORVAL [NULL_CODE] = "ORVAL";   /* Original Value */
const char LABL_OVRSHR[NULL_CODE] = "OVRSHR";   /* Over/Short */
const char LABL_PREVAL[NULL_CODE] = "PREVAL";   /* Previous Value */
const char LABL_PTNRS [NULL_CODE] = "PTNRS";   /* Partners */
const char LABL_REQDFT[NULL_CODE] = "REQDFT";   /* Request Default */
const char LABL_RTNINV[NULL_CODE] = "RTNINV";   /* Return From Inventory */
const char LABL_RXINFO[NULL_CODE] = "RXINFO";   /* Pharmacy Information */
const char LABL_SCAMT [NULL_CODE] = "SCAMT";   /* Stock Count Amount */
const char LABL_SCRIT [NULL_CODE] = "SCRIT";   /* Selection Criteria */
const char LABL_SHIPS [NULL_CODE] = "SHIPS";   /* Shipments */
const char LABL_SOHCLN[NULL_CODE] = "SOHCLN";   /* Stock on Hand: */
const char LABL_SPCATT[NULL_CODE] = "SPCATT";   /* Specific Component Attribute */
const char LABL_STATTR[NULL_CODE] = "STATTR";   /* Store Attributes */
const char LABL_STDFLT[NULL_CODE] = "STDFLT";   /* Store Default */
const char LABL_SUPHIE[NULL_CODE] = "SUPHIE";   /* Supplier Hierarchy */
const char LABL_TGTBGR[NULL_CODE] = "TGTBGR";   /* Target Baseline Growth % */
const char LABL_TRNDET[NULL_CODE] = "TRNDET";   /* Transportation Details */
const char LABL_TRNFS [NULL_CODE] = "TRNFS";   /* Transportation Finalize Selection */
const char LABL_TSTEPS[NULL_CODE] = "TSTEPS";   /* Timeline Steps */
const char LABL_TTLBGR[NULL_CODE] = "TTLBGR";   /* Total Baseline Growth % */
const char LABL_TVRAMT[NULL_CODE] = "TVRAMT";   /* Turnover Amount */
const char LABL_TVRUNS[NULL_CODE] = "TVRUNS";   /* Turnover Units */
const char LABL_UDAVAL[NULL_CODE] = "UDAVAL";   /* UDA Values */
const char LABL_UPDTE [NULL_CODE] = "UPDTE";   /* Update */
const char LABL_VWCRIT[NULL_CODE] = "VWCRIT";   /* View Criteria */
const char LABL_WHINFO[NULL_CODE] = "WHINFO";   /* Warehouse Information */
const char LABL_FIND  [NULL_CODE] = "FIND";   /* Find */
const char LABL_DIMENS[NULL_CODE] = "DIMENS";   /* Dimensions */
const char LABL_SHPRCV[NULL_CODE] = "SHPRCV";   /* Shipping and Receiving */
const char LABL_RTMM  [NULL_CODE] = "RTMM";   /* RTM */
const char LABL_ADITDI[NULL_CODE] = "ADITDI";   /* Add Items from Distros */
const char LABL_SRCH  [NULL_CODE] = "SRCH";   /* Search */
const char LABL_JIDS  [NULL_CODE] = "JIDS";   /* Job Id Status */
const char LABL_JIRL  [NULL_CODE] = "JIRL";   /* Job Id Retry Log */
const char LABL_FUNC  [NULL_CODE] = "FUNC";   /* Functions */
const char LABL_OTBEOW[NULL_CODE] = "OTBEOW";   /* OTB EOW Date */
const char LABL_RECDTE[NULL_CODE] = "RECDTE";   /* Receipt Date */
const char LABL_SHPDTE[NULL_CODE] = "SHPDTE";   /* Ship Date */
const char LABL_TRNDTL[NULL_CODE] = "TRNDTL";   /* Transaction Details */
const char LABL_DAY1  [NULL_CODE] = "DAY1";   /* Day 1 */
const char LABL_DAY2  [NULL_CODE] = "DAY2";   /* Day 2 */
const char LABL_DAY3  [NULL_CODE] = "DAY3";   /* Day 3 */
const char LABL_DAY4  [NULL_CODE] = "DAY4";   /* Day 4 */
const char LABL_DAY5  [NULL_CODE] = "DAY5";   /* Day 5 */
const char LABL_DAY6  [NULL_CODE] = "DAY6";   /* Day 6 */
const char LABL_DAY7  [NULL_CODE] = "DAY7";   /* Day 7 */
const char LABL_TOTAL [NULL_CODE] = "TOTAL";   /* Total */
const char LABL_WEEK1 [NULL_CODE] = "WEEK1";   /* Week 1 */
const char LABL_WEEK2 [NULL_CODE] = "WEEK2";   /* Week 2 */
const char LABL_WEEK3 [NULL_CODE] = "WEEK3";   /* Week 3 */
const char LABL_WEEK4 [NULL_CODE] = "WEEK4";   /* Week 4 */
const char LABL_WEEK5 [NULL_CODE] = "WEEK5";   /* Week 5 */
const char LABL_TLEMP [NULL_CODE] = "TLEMP";   /* Transaction Level Employees */
const char LABL_ILEMP [NULL_CODE] = "ILEMP";   /* Item Level Employees */
const char LABL_WFRTY [NULL_CODE] = "WFRTY";   /* Return Type */
const char LABL_WFRM  [NULL_CODE] = "WFRM";   /* Return Method */
const char LABL_ELC   [NULL_CODE] = "ELC";   /* ELC Component */
const char LABL_ML    [NULL_CODE] = "ML";   /* Manual */
const char LABL_EXTTRG[NULL_CODE] = "EXTTRG";   /* External System Tran. No. Range */
const char LABL_FORW  [NULL_CODE] = "FORW";   /* Transfers for @SUH4@ Returns          (wfordrettsf) */
const char LABL_FORL  [NULL_CODE] = "FORL";   /* @SUH4@ Return */
const char LABL_FOOL  [NULL_CODE] = "FOOL";   /* @SUH4@ Order No. */
const char LABL_OKFIN [NULL_CODE] = "OKFIN";   /* OK / Finish */
const char LABL_FINISH[NULL_CODE] = "FINISH";   /* Finish */
const char LABL_WFOPT [NULL_CODE] = "WFOPT";   /* @SUH4@ Options */
const char LABL_WFST  [NULL_CODE] = "WFST";   /* Store */
const char LABL_CREL  [NULL_CODE] = "CREL";   /* Cost Relationship */
const char LABL_AREL  [NULL_CODE] = "AREL";   /* Add Relationship */
const char LABL_MRANGE[NULL_CODE] = "MRANGE";   /* Margin Range: */
const char LABL_OBLS  [NULL_CODE] = "OBLS";   /* Obligations */
const char LABL_INTLNG[NULL_CODE] = "INTLNG";   /* Data Integration Language */
const char LABL_RNDF  [NULL_CODE] = "RNDF";   /* Rounding (Default) */
const char LABL_PCKGNG[NULL_CODE] = "PCKGNG";   /* Packaging */
const char LABL_NOMFLG[NULL_CODE] = "NOMFLG";   /* Nomination Flag */
const char LABL_SHPMNT[NULL_CODE] = "SHPMNT";   /* Shipment */
const char LABL_TSFRTV[NULL_CODE] = "TSFRTV";   /* Transfers and RTVs */
const char LABL_RMS   [NULL_CODE] = "RMS";   /* Retail Merchandising System */
const char LABL_PORDS [NULL_CODE] = "PORDS";   /* Purchase Orders */
const char LABL_CADJ  [NULL_CODE] = "CADJ";   /* Cost Adjustments */
const char LABL_VRSN  [NULL_CODE] = "VRSN";   /* Version */
const char LABL_RPM   [NULL_CODE] = "RPM";   /* Retail Price Management */
const char LABL_ERR   [NULL_CODE] = "ERR";   /* Errors */
const char LABL_DLDFIL[NULL_CODE] = "DLDFIL";   /* Download File */
const char LABL_DLDSTG[NULL_CODE] = "DLDSTG";   /* Download Staged */
const char LABL_TSFDCT[NULL_CODE] = "TSFDCT";   /* Transfer Cost Adjustment */
const char LABL_TSFDPT[NULL_CODE] = "TSFDPT";   /* Transfer Price Adjustment */
const char LABL_REDDAT[NULL_CODE] = "REDDAT";   /* Receive Dates */
const char LABL_ORET  [NULL_CODE] = "ORET";   /* % Off Retail */
const char LABL_CSTM  [NULL_CODE] = "CSTM";   /* Cost */
const char LABL_TEMT  [NULL_CODE] = "TEMT";   /* Template Type */
const char LABL_REBATS[NULL_CODE] = "REBATS";   /* Rebates */
const char LABL_BGTHIN[NULL_CODE] = "BGTHIN";   /* Buy / Get Item Threshold Information */
const char LABL_HISTPD[NULL_CODE] = "HISTPD";   /* Historical Period */
const char LABL_READAT[NULL_CODE] = "READAT";   /* Ready By Date */
const char LABL_ACLS  [NULL_CODE] = "ACLS";   /* Stock Order Auto Close */
const char LABL_TAX   [NULL_CODE] = "TAX";   /* Tax */
const char LABL_FSLT  [NULL_CODE] = "FSLT";   /* Source Loc Type */
const char LABL_SLID  [NULL_CODE] = "SLID";   /* Source
Loc ID */

const char LABL_FRLT  [NULL_CODE] = "FRLT";   /* Return Loc Type */
const char LABL_RLID  [NULL_CODE] = "RLID";   /* Return Loc ID */
const char LABL_WFOT  [NULL_CODE] = "WFOT";   /* Order Type */
const char LABL_VEHINF[NULL_CODE] = "VEHINF";   /* Vehicle Information */
const char LABL_SUPOOL[NULL_CODE] = "SUPOOL";   /* Supplier Pooling */


/* LACO = Standard/Average Cost */
const char LACO[NULL_CODE] = "LACO";

const char LACO_A     [NULL_CODE] = "A";   /* Average Cost */
const char LACO_S     [NULL_CODE] = "S";   /* Standard Cost */


/* LANG = List of non-primary languages for maintainence of translation records through spreadsheet API and UI. */
const char LANG[NULL_CODE] = "LANG";

const char LANG_1     [NULL_CODE] = "1";   /* English */
const char LANG_2     [NULL_CODE] = "2";   /* German */
const char LANG_3     [NULL_CODE] = "3";   /* French */
const char LANG_4     [NULL_CODE] = "4";   /* Spanish */
const char LANG_5     [NULL_CODE] = "5";   /* Japanese */
const char LANG_6     [NULL_CODE] = "6";   /* Korean */
const char LANG_7     [NULL_CODE] = "7";   /* Russian */
const char LANG_8     [NULL_CODE] = "8";   /* Chinese-Simplified */
const char LANG_9     [NULL_CODE] = "9";   /* Turkish */
const char LANG_10    [NULL_CODE] = "10";   /* Hungarian */
const char LANG_11    [NULL_CODE] = "11";   /* Chinese-Traditional */
const char LANG_12    [NULL_CODE] = "12";   /* Portuguese-Brazil */
const char LANG_15    [NULL_CODE] = "15";   /* Croatian */
const char LANG_18    [NULL_CODE] = "18";   /* Dutch */
const char LANG_20    [NULL_CODE] = "20";   /* Greek */
const char LANG_22    [NULL_CODE] = "22";   /* Italian */
const char LANG_26    [NULL_CODE] = "26";   /* Polish */
const char LANG_31    [NULL_CODE] = "31";   /* Swedish */


/* LCAC = Letter of Credit Find From Action */
const char LCAC[NULL_CODE] = "LCAC";

const char LCAC_NEW   [NULL_CODE] = "NEW";   /* New */
const char LCAC_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char LCAC_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char LCAC_A     [NULL_CODE] = "A";   /* Download Application */
const char LCAC_M     [NULL_CODE] = "M";   /* Download Amendment */


/* LCAD = Letter of Credit Advice Method */
const char LCAD[NULL_CODE] = "LCAD";

const char LCAD_F     [NULL_CODE] = "F";   /* Full Wire */
const char LCAD_M     [NULL_CODE] = "M";   /* Mail */
const char LCAD_O     [NULL_CODE] = "O";   /* Overnight */


/* LCAF = Letter of Credit Amendment Field */
const char LCAF[NULL_CODE] = "LCAF";

const char LCAF_LSD   [NULL_CODE] = "LSD";   /* Latest Ship Date */
const char LCAF_ESD   [NULL_CODE] = "ESD";   /* Earliest Ship Date */
const char LCAF_OC    [NULL_CODE] = "OC";   /* Country Of Sourcing */
const char LCAF_TSF   [NULL_CODE] = "TSF";   /* Transshipment Flag */
const char LCAF_TFF   [NULL_CODE] = "TFF";   /* Transferable Flag */
const char LCAF_PSF   [NULL_CODE] = "PSF";   /* Partial Ship Flag */
const char LCAF_C     [NULL_CODE] = "C";   /* Cost */
const char LCAF_OQ    [NULL_CODE] = "OQ";   /* Order Quantity */
const char LCAF_NA    [NULL_CODE] = "NA";   /* Net Amount */
const char LCAF_ED    [NULL_CODE] = "ED";   /* Expiration Date */
const char LCAF_PE    [NULL_CODE] = "PE";   /* Place of Expiry */
const char LCAF_PRT   [NULL_CODE] = "PRT";   /* Presentation Terms */
const char LCAF_ND    [NULL_CODE] = "ND";   /* Negotiation Days */
const char LCAF_RO    [NULL_CODE] = "RO";   /* Remove PO */
const char LCAF_AO    [NULL_CODE] = "AO";   /* Add PO */
const char LCAF_ARQD  [NULL_CODE] = "ARQD";   /* Add Required Doc. */
const char LCAF_RRQD  [NULL_CODE] = "RRQD";   /* Remove Required Doc. */
const char LCAF_AI    [NULL_CODE] = "AI";   /* Add Item */
const char LCAF_RI    [NULL_CODE] = "RI";   /* Remove Item */
const char LCAF_TRT   [NULL_CODE] = "TRT";   /* Transport To */
const char LCAF_NC    [NULL_CODE] = "NC";   /* New Comments */


/* LCAM = Cost Component Amount Type */
const char LCAM[NULL_CODE] = "LCAM";

const char LCAM_C     [NULL_CODE] = "C";   /* Currency */
const char LCAM_D     [NULL_CODE] = "D";   /* Duty */
const char LCAM_P     [NULL_CODE] = "P";   /* Percent */


/* LCAS = Letter of Credit Amendment Status */
const char LCAS[NULL_CODE] = "LCAS";

const char LCAS_N     [NULL_CODE] = "N";   /* New */
const char LCAS_A     [NULL_CODE] = "A";   /* Accept */
const char LCAS_H     [NULL_CODE] = "H";   /* Hold */
const char LCAS_D     [NULL_CODE] = "D";   /* Downloaded */


/* LCAT = Letter of Credit Amount Type */
const char LCAT[NULL_CODE] = "LCAT";

const char LCAT_E     [NULL_CODE] = "E";   /* Exact */
const char LCAT_A     [NULL_CODE] = "A";   /* Approximately */


/* LCDA = Letter of Credit Drafts At */
const char LCDA[NULL_CODE] = "LCDA";

const char LCDA_01    [NULL_CODE] = "01";   /* At Sight */
const char LCDA_02    [NULL_CODE] = "02";   /* 30 Days */
const char LCDA_03    [NULL_CODE] = "03";   /* 60 Days */


/* LCDS = Letter of Credit Download Selection */
const char LCDS[NULL_CODE] = "LCDS";


/* LCFT = Letter of Credit Issuance */
const char LCFT[NULL_CODE] = "LCFT";

const char LCFT_S     [NULL_CODE] = "S";   /* Short */
const char LCFT_L     [NULL_CODE] = "L";   /* Long */


/* LCIL = Letter of Credit Item Levels */
const char LCIL[NULL_CODE] = "LCIL";

const char LCIL_S     [NULL_CODE] = "S";   /* Item Parent */
const char LCIL_SC    [NULL_CODE] = "SC";   /* Item Parent/Differentiator 1 */
const char LCIL_SS    [NULL_CODE] = "SS";   /* Item Parent/Differentiator 2 */
const char LCIL_SKU   [NULL_CODE] = "SKU";   /* Item */


/* LCIS = Letter of Credit Issuance */
const char LCIS[NULL_CODE] = "LCIS";

const char LCIS_C     [NULL_CODE] = "C";   /* Cable */
const char LCIS_T     [NULL_CODE] = "T";   /* Telex */


/* LCLS = Location Classification Method */
const char LCLS[NULL_CODE] = "LCLS";

const char LCLS_S     [NULL_CODE] = "S";   /* Sales History */
const char LCLS_H     [NULL_CODE] = "H";   /* Shrinkage History */
const char LCLS_R     [NULL_CODE] = "R";   /* Retail Price */


/* LCPE = Letter of Credit Place of Expiry */
const char LCPE[NULL_CODE] = "LCPE";

const char LCPE_01    [NULL_CODE] = "01";   /* Issuing Bank */
const char LCPE_02    [NULL_CODE] = "02";   /* Advising Bank */
const char LCPE_03    [NULL_CODE] = "03";   /* Miami */
const char LCPE_04    [NULL_CODE] = "04";   /* New York */
const char LCPE_05    [NULL_CODE] = "05";   /* Los Angeles */


/* LCPT = Letter of Credit Presentation Terms */
const char LCPT[NULL_CODE] = "LCPT";

const char LCPT_P     [NULL_CODE] = "P";   /* By Payment */
const char LCPT_A     [NULL_CODE] = "A";   /* By Acceptance */
const char LCPT_N     [NULL_CODE] = "N";   /* By Negotiation */


/* LCRL = LC Bank Applicable Rules */
const char LCRL[NULL_CODE] = "LCRL";

const char LCRL_EUCP  [NULL_CODE] = "EUCP";   /* EUCP Latest Version */
const char LCRL_EURR  [NULL_CODE] = "EURR";   /* EUCPURR Latest Version */
const char LCRL_ISP   [NULL_CODE] = "ISP";   /* ISP Latest Version */
const char LCRL_OTHR  [NULL_CODE] = "OTHR";   /* OTHR */
const char LCRL_UCP   [NULL_CODE] = "UCP";   /* UCP Latest Version */
const char LCRL_UCPURR[NULL_CODE] = "UCPURR";   /* UCPURR Latest Version */


/* LCSP = Letter of Credit Specification */
const char LCSP[NULL_CODE] = "LCSP";

const char LCSP_M     [NULL_CODE] = "M";   /* Maximum */
const char LCSP_U     [NULL_CODE] = "U";   /* Up TO */
const char LCSP_N     [NULL_CODE] = "N";   /* Not Exceeding */


/* LCST = Letter of Credit Status */
const char LCST[NULL_CODE] = "LCST";

const char LCST_W     [NULL_CODE] = "W";   /* Worksheet */
const char LCST_S     [NULL_CODE] = "S";   /* Submitted */
const char LCST_A     [NULL_CODE] = "A";   /* Approved */
const char LCST_E     [NULL_CODE] = "E";   /* Extracted */
const char LCST_C     [NULL_CODE] = "C";   /* Confirmed */
const char LCST_L     [NULL_CODE] = "L";   /* Closed */


/* LCTC = Letter of Credit Transaction Code */
const char LCTC[NULL_CODE] = "LCTC";

const char LCTC_A     [NULL_CODE] = "A";   /* Amendment */
const char LCTC_B     [NULL_CODE] = "B";   /* Bank Charge */
const char LCTC_D     [NULL_CODE] = "D";   /* Drawdown */
const char LCTC_L     [NULL_CODE] = "L";   /* Letter of Credit */


/* LCTP = Letter of Credit Type */
const char LCTP[NULL_CODE] = "LCTP";

const char LCTP_N     [NULL_CODE] = "N";   /* Normal */
const char LCTP_M     [NULL_CODE] = "M";   /* Master */
const char LCTP_R     [NULL_CODE] = "R";   /* Revolving */
const char LCTP_O     [NULL_CODE] = "O";   /* Open */


/* LCTY = Letter of Credit Type */
const char LCTY[NULL_CODE] = "LCTY";

const char LCTY_N     [NULL_CODE] = "N";   /* Normal */
const char LCTY_M     [NULL_CODE] = "M";   /* Master */
const char LCTY_R     [NULL_CODE] = "R";   /* Revolving */
const char LCTY_O     [NULL_CODE] = "O";   /* Open */


/* LGRP = Item Retail Group Type */
const char LGRP[NULL_CODE] = "LGRP";

const char LGRP_AS    [NULL_CODE] = "AS";   /* All Stores */
const char LGRP_Cl    [NULL_CODE] = "Cl";   /* Store Class */
const char LGRP_D     [NULL_CODE] = "D";   /* @OH5@ */
const char LGRP_DW    [NULL_CODE] = "DW";   /* Default WH */
const char LGRP_LL    [NULL_CODE] = "LL";   /* Location List */
const char LGRP_R     [NULL_CODE] = "R";   /* @OH4@ */
const char LGRP_S     [NULL_CODE] = "S ";   /* Store */
const char LGRP_St    [NULL_CODE] = "St";   /* State */
const char LGRP_T     [NULL_CODE] = "T";   /* Trait */
const char LGRP_TZ    [NULL_CODE] = "TZ";   /* Transfer Zone */


/* LIVI = License/Visa Indicator */
const char LIVI[NULL_CODE] = "LIVI";

const char LIVI_L     [NULL_CODE] = "L";   /* License */
const char LIVI_V     [NULL_CODE] = "V";   /* Visa */


/* LLAB = Language Action */
const char LLAB[NULL_CODE] = "LLAB";

const char LLAB_NEW   [NULL_CODE] = "NEW";   /* New */
const char LLAB_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char LLAB_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char LLAB_ED_ALL[NULL_CODE] = "ED_ALL";   /* Edit All */


/* LLBR = Batch Rebuild */
const char LLBR[NULL_CODE] = "LLBR";

const char LLBR_Y     [NULL_CODE] = "Y";   /* Yes */
const char LLBR_N     [NULL_CODE] = "N";   /* No */


/* LLCM = Location List Comparison */
const char LLCM[NULL_CODE] = "LLCM";

const char LLCM_EQ    [NULL_CODE] = "EQ";   /* = */
const char LLCM_NE    [NULL_CODE] = "NE";   /* != */
const char LLCM_GT    [NULL_CODE] = "GT";   /* > */
const char LLCM_LT    [NULL_CODE] = "LT";   /* < */
const char LLCM_GE    [NULL_CODE] = "GE";   /* >= */
const char LLCM_LE    [NULL_CODE] = "LE";   /* <= */


/* LLDA = Location List Detail Actions */
const char LLDA[NULL_CODE] = "LLDA";

const char LLDA_A     [NULL_CODE] = "A";   /* Add */
const char LLDA_D     [NULL_CODE] = "D";   /* Delete */


/* LLLB = Location List Labels */
const char LLLB[NULL_CODE] = "LLLB";

const char LLLB_DESC  [NULL_CODE] = "DESC";   /* Copied From: */


/* LLLO = Location List Logic Operation */
const char LLLO[NULL_CODE] = "LLLO";

const char LLLO_AND   [NULL_CODE] = "AND";   /* AND */
const char LLLO_OR    [NULL_CODE] = "OR";   /* OR */


/* LLSC = Location List Source Field */
const char LLSC[NULL_CODE] = "LLSC";

const char LLSC_RDF   [NULL_CODE] = "RDF";   /* Demand Forecasting */
const char LLSC_RA    [NULL_CODE] = "RA";   /* Retail Analytics */
const char LLSC_RMS   [NULL_CODE] = "RMS";   /* Merchandising System */


/* LLSD = Static\Dynamic */
const char LLSD[NULL_CODE] = "LLSD";

const char LLSD_Y     [NULL_CODE] = "Y";   /* Static */
const char LLSD_N     [NULL_CODE] = "N";   /* Dynamic */


/* LLST = Location List Store Criteria */
const char LLST[NULL_CODE] = "LLST";

const char LLST_DT    [NULL_CODE] = "DT";   /* @OH5@ */
const char LLST_CZ    [NULL_CODE] = "CZ";   /* Cost Zone */
const char LLST_CTY   [NULL_CODE] = "CTY";   /* Country ID */
const char LLST_CUR   [NULL_CODE] = "CUR";   /* Currency */
const char LLST_DW    [NULL_CODE] = "DW";   /* Default WH */
const char LLST_LNG   [NULL_CODE] = "LNG";   /* Language */
const char LLST_LT    [NULL_CODE] = "LT";   /* Location Traits */
const char LLST_SA    [NULL_CODE] = "SA";   /* Selling Area */
const char LLST_ST    [NULL_CODE] = "ST";   /* State */
const char LLST_AD    [NULL_CODE] = "AD";   /* Store Acquired Date */
const char LLST_SC    [NULL_CODE] = "SC";   /* Store Class */
const char LLST_FL    [NULL_CODE] = "FL";   /* Store Forecast Level for Week */
const char LLST_SF    [NULL_CODE] = "SF";   /* Store Format */
const char LLST_SG    [NULL_CODE] = "SG";   /* Store Grade */
const char LLST_SN    [NULL_CODE] = "SN";   /* Store Number */
const char LLST_OD    [NULL_CODE] = "OD";   /* Store Open Date */
const char LLST_RD    [NULL_CODE] = "RD";   /* Store Remodel Date */
const char LLST_SL    [NULL_CODE] = "SL";   /* Store Sales Level for Week */
const char LLST_TA    [NULL_CODE] = "TA";   /* Total Area */
const char LLST_TE    [NULL_CODE] = "TE";   /* Transfer Entity ID */
const char LLST_TSF   [NULL_CODE] = "TSF";   /* Transfer Zone */
const char LLST_VR    [NULL_CODE] = "VR";   /* VAT Region */
const char LLST_ZC    [NULL_CODE] = "ZC";   /* Zip Code */
const char LLST_WFC   [NULL_CODE] = "WFC";   /* @SUH4@ Customer ID */


/* LLWH = Location List Warehouse Criteria */
const char LLWH[NULL_CODE] = "LLWH";

const char LLWH_CTY   [NULL_CODE] = "CTY";   /* Country ID */
const char LLWH_CUR   [NULL_CODE] = "CUR";   /* Currency */
const char LLWH_PWN   [NULL_CODE] = "PWN";   /* Physical WH Number */
const char LLWH_ST    [NULL_CODE] = "ST";   /* State */
const char LLWH_TE    [NULL_CODE] = "TE";   /* Transfer Entity ID */
const char LLWH_VR    [NULL_CODE] = "VR";   /* VAT Region */
const char LLWH_WN    [NULL_CODE] = "WN";   /* WH Number */
const char LLWH_ZC    [NULL_CODE] = "ZC";   /* Zip Code */


/* LOC = Location */
const char LOC[NULL_CODE] = "LOC ";

const char LOC_S     [NULL_CODE] = "S";   /* Store */
const char LOC_W     [NULL_CODE] = "W";   /* Warehouse */
const char LOC_A     [NULL_CODE] = "A";   /* All Locations */


/* LOC2 = Locations (All) */
const char LOC2[NULL_CODE] = "LOC2";

const char LOC2_S     [NULL_CODE] = "S";   /* Store */
const char LOC2_W     [NULL_CODE] = "W";   /* Warehouse */
const char LOC2_A     [NULL_CODE] = "A";   /* All Stores */
const char LOC2_L     [NULL_CODE] = "L";   /* All Warehouses */


/* LOC3 = Location Types */
const char LOC3[NULL_CODE] = "LOC3";

const char LOC3_S     [NULL_CODE] = "S";   /* Store */
const char LOC3_W     [NULL_CODE] = "W";   /* Warehouse */
const char LOC3_L     [NULL_CODE] = "L";   /* Location List */


/* LOC4 = Location Types - Store & Phys Warehouse */
const char LOC4[NULL_CODE] = "LOC4";

const char LOC4_S     [NULL_CODE] = "S";   /* Store */
const char LOC4_W     [NULL_CODE] = "W";   /* Physical Warehouse */


/* LOC9 = Locations (ST/WH) */
const char LOC9[NULL_CODE] = "LOC9";

const char LOC9_S     [NULL_CODE] = "S";   /* Store */
const char LOC9_W     [NULL_CODE] = "W";   /* Warehouse */


/* LOCS = Locations - store, wh, All stores. */
const char LOCS[NULL_CODE] = "LOCS";

const char LOCS_S     [NULL_CODE] = "S";   /* Store */
const char LOCS_W     [NULL_CODE] = "W";   /* Warehouse */
const char LOCS_A     [NULL_CODE] = "A";   /* All Stores */


/* LOCT = Location Type */
const char LOCT[NULL_CODE] = "LOCT";

const char LOCT_DP    [NULL_CODE] = "DP";   /* Discharge Port */
const char LOCT_LP    [NULL_CODE] = "LP";   /* Lading Port */
const char LOCT_EP    [NULL_CODE] = "EP";   /* Entry Port */
const char LOCT_RC    [NULL_CODE] = "RC";   /* Return Center */
const char LOCT_BT    [NULL_CODE] = "BT";   /* Bill To Location */
const char LOCT_LL    [NULL_CODE] = "LL";   /* LC To Location */
const char LOCT_RL    [NULL_CODE] = "RL";   /* Routing Location */
const char LOCT_CZ    [NULL_CODE] = "CZ";   /* Clearing Zone */


/* LOFT = Template Type */
const char LOFT[NULL_CODE] = "LOFT";

const char LOFT_M     [NULL_CODE] = "M";   /* Margin then Up Charges */
const char LOFT_U     [NULL_CODE] = "U";   /* Up Charges then Margin */
const char LOFT_R     [NULL_CODE] = "R";   /* Percent Off Retail */
const char LOFT_C     [NULL_CODE] = "C";   /* Fixed Cost */


/* LOHR = Location Hierarchy */
const char LOHR[NULL_CODE] = "LOHR";

const char LOHR_C     [NULL_CODE] = "C";   /* @OH2@ */
const char LOHR_A     [NULL_CODE] = "A";   /* @OH3@ */
const char LOHR_R     [NULL_CODE] = "R";   /* @OH4@ */
const char LOHR_D     [NULL_CODE] = "D";   /* @OH5@ */
const char LOHR_S     [NULL_CODE] = "S";   /* Store */
const char LOHR_W     [NULL_CODE] = "W";   /* Warehouse */
const char LOHR_L     [NULL_CODE] = "L";   /* Location List */


/* LORM = Return Method Types */
const char LORM[NULL_CODE] = "LORM";

const char LORM_R     [NULL_CODE] = "R";   /* Return to Location */
const char LORM_D     [NULL_CODE] = "D";   /* Destroy on Site */


/* LOTP = Location Type */
const char LOTP[NULL_CODE] = "LOTP";

const char LOTP_S     [NULL_CODE] = "S";   /* Store */
const char LOTP_W     [NULL_CODE] = "W";   /* Warehouse */


/* LPOS = Transportation Load Position */
const char LPOS[NULL_CODE] = "LPOS";

const char LPOS_M     [NULL_CODE] = "M";   /* Middle of the Container */
const char LPOS_N     [NULL_CODE] = "N";   /* Nose of the Container */
const char LPOS_R     [NULL_CODE] = "R";   /* Rear of the Container */
const char LPOS_T     [NULL_CODE] = "T";   /* Tail of the Container */


/* LSEC = Location Security Areas */
const char LSEC[NULL_CODE] = "LSEC";

const char LSEC_LALL  [NULL_CODE] = "LALL";   /* All */
const char LSEC_LPRM  [NULL_CODE] = "LPRM";   /* Promotion */
const char LSEC_LTXFTO[NULL_CODE] = "LTXFTO";   /* Transfers to */
const char LSEC_LTXFRM[NULL_CODE] = "LTXFRM";   /* Transfers from */
const char LSEC_LALCTO[NULL_CODE] = "LALCTO";   /* Allocations to */
const char LSEC_LALCFR[NULL_CODE] = "LALCFR";   /* Allocations from */
const char LSEC_LSHP  [NULL_CODE] = "LSHP";   /* Shipments */
const char LSEC_LPO   [NULL_CODE] = "LPO";   /* Orders */
const char LSEC_LSTK  [NULL_CODE] = "LSTK";   /* Stock Counts */
const char LSEC_LTRQ  [NULL_CODE] = "LTRQ";   /* Ticket Requests */
const char LSEC_LIVJ  [NULL_CODE] = "LIVJ";   /* Inventory Adjustments */
const char LSEC_LRTV  [NULL_CODE] = "LRTV";   /* RTVs */
const char LSEC_LSTR  [NULL_CODE] = "LSTR";   /* Store */
const char LSEC_LSTDY [NULL_CODE] = "LSTDY";   /* Sales Audit Store Day */
const char LSEC_LSTACH[NULL_CODE] = "LSTACH";   /* Sales Audit Store ACH */
const char LSEC_LSFM  [NULL_CODE] = "LSFM";   /* Site Fuel Management */


/* LSUC = Lead Time Sales Calc Method for Prom Imp */
const char LSUC[NULL_CODE] = "LSUC";

const char LSUC_T     [NULL_CODE] = "T";   /* Turns */
const char LSUC_S     [NULL_CODE] = "S";   /* Sell-through */
const char LSUC_F     [NULL_CODE] = "F";   /* Forecasted Sales */
const char LSUC_P     [NULL_CODE] = "P";   /* Percent of Current SOH */


/* LTLA = Store, WH, All, Int. and Ext. Fins */
const char LTLA[NULL_CODE] = "LTLA";

const char LTLA_AS    [NULL_CODE] = "AS";   /* All Stores */
const char LTLA_S     [NULL_CODE] = "S";   /* Store */
const char LTLA_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char LTLA_W     [NULL_CODE] = "W";   /* Warehouse */
const char LTLA_AI    [NULL_CODE] = "AI";   /* All Internal Finishers */
const char LTLA_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char LTLA_AE    [NULL_CODE] = "AE";   /* All External Finishers */
const char LTLA_E     [NULL_CODE] = "E";   /* External Finisher */


/* LTLE = Store, WH, Loc List and Ext. Finishers */
const char LTLE[NULL_CODE] = "LTLE";

const char LTLE_S     [NULL_CODE] = "S";   /* Store */
const char LTLE_W     [NULL_CODE] = "W";   /* Warehouse */
const char LTLE_E     [NULL_CODE] = "E";   /* External Finisher */
const char LTLE_L     [NULL_CODE] = "L";   /* Location List */


/* LTLF = Store, WH, Loc List, Int. and Ext. Fins */
const char LTLF[NULL_CODE] = "LTLF";

const char LTLF_S     [NULL_CODE] = "S";   /* Store */
const char LTLF_W     [NULL_CODE] = "W";   /* Warehouse */
const char LTLF_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char LTLF_E     [NULL_CODE] = "E";   /* External Finisher */
const char LTLF_LL    [NULL_CODE] = "LL";   /* Location List */


/* LTPE = Store, WH and External Finishers */
const char LTPE[NULL_CODE] = "LTPE";

const char LTPE_S     [NULL_CODE] = "S";   /* Store */
const char LTPE_W     [NULL_CODE] = "W";   /* Warehouse */
const char LTPE_E     [NULL_CODE] = "E";   /* External Finisher */


/* LTPF = Store, WH and Int. and Ext. Finishers */
const char LTPF[NULL_CODE] = "LTPF";

const char LTPF_S     [NULL_CODE] = "S";   /* Store */
const char LTPF_W     [NULL_CODE] = "W";   /* Warehouse */
const char LTPF_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char LTPF_E     [NULL_CODE] = "E";   /* External Finisher */


/* LTPI = Store, WH, Int. Fins */
const char LTPI[NULL_CODE] = "LTPI";

const char LTPI_S     [NULL_CODE] = "S";   /* Store */
const char LTPI_W     [NULL_CODE] = "W";   /* Warehouse */
const char LTPI_I     [NULL_CODE] = "I";   /* Internal Finisher */


/* LTPM = Store, WH , External Finishers and SOB */
const char LTPM[NULL_CODE] = "LTPM";

const char LTPM_S     [NULL_CODE] = "S";   /* Store */
const char LTPM_W     [NULL_CODE] = "W";   /* Warehouse */
const char LTPM_E     [NULL_CODE] = "E";   /* External Finisher */
const char LTPM_M     [NULL_CODE] = "M";   /* Set of Books */


/* LTSE = Store, WH, all stores, Ext Finishers */
const char LTSE[NULL_CODE] = "LTSE";


/* LTSF = Store, WH, all Stores, Ext & Int Finish */
const char LTSF[NULL_CODE] = "LTSF";


/* MALD = Mall Editor Titles */
const char MALD[NULL_CODE] = "MALD";

const char MALD_MADESC[NULL_CODE] = "MADESC";   /* Mall Description */


/* MCAC = MCLOCN type list box */
const char MCAC[NULL_CODE] = "MCAC";

const char MCAC_S     [NULL_CODE] = "S";   /* Store */
const char MCAC_C     [NULL_CODE] = "C";   /* Store Class */
const char MCAC_D     [NULL_CODE] = "D";   /* @OH5@ */
const char MCAC_R     [NULL_CODE] = "R";   /* @OH4@ */
const char MCAC_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char MCAC_L     [NULL_CODE] = "L";   /* Location Trait */
const char MCAC_Z     [NULL_CODE] = "Z";   /* Zone Group */
const char MCAC_V     [NULL_CODE] = "V";   /* Active Stores */
const char MCAC_LL    [NULL_CODE] = "LL";   /* Location List */


/* MCLB = MCLOCN form labels */
const char MCLB[NULL_CODE] = "MCLB";

const char MCLB_I     [NULL_CODE] = "I";   /* Item List */
const char MCLB_S     [NULL_CODE] = "S";   /* Store */
const char MCLB_SKU   [NULL_CODE] = "SKU";   /* Item */
const char MCLB_SL    [NULL_CODE] = "SL";   /* Store List */
const char MCLB_SS    [NULL_CODE] = "SS";   /* Store Selection */
const char MCLB_ST    [NULL_CODE] = "ST";   /* Item Parent */
const char MCLB_W     [NULL_CODE] = "W";   /* Wh */
const char MCLB_WL    [NULL_CODE] = "WL";   /* Warehouse List */
const char MCLB_WS    [NULL_CODE] = "WS";   /* Warehouse Selection */


/* MCLT = Multiple Channel Location Type */
const char MCLT[NULL_CODE] = "MCLT";

const char MCLT_S     [NULL_CODE] = "S";   /* Store */
const char MCLT_W     [NULL_CODE] = "W";   /* Warehouse */
const char MCLT_PW    [NULL_CODE] = "PW";   /* Physical Warehouse */


/* MCTP = Rejection Change Type */
const char MCTP[NULL_CODE] = "MCTP";

const char MCTP_I     [NULL_CODE] = "I";   /* Item Indicator */
const char MCTP_L     [NULL_CODE] = "L";   /* Location-level Attributes */
const char MCTP_M     [NULL_CODE] = "M";   /* Merchandise Hierarchy */
const char MCTP_P     [NULL_CODE] = "P";   /* Seasons/Phases */
const char MCTP_R     [NULL_CODE] = "R";   /* Replenishment */
const char MCTP_S     [NULL_CODE] = "S";   /* Substitute Items */
const char MCTP_U     [NULL_CODE] = "U";   /* User-defined Attributes */
const char MCTP_V     [NULL_CODE] = "V";   /* Vat Indicator */


/* MDLB = Mkdvars - Form Labels */
const char MDLB[NULL_CODE] = "MDLB";

const char MDLB_MC    [NULL_CODE] = "MC";   /* Markup % Cost */
const char MDLB_MR    [NULL_CODE] = "MR";   /* Markup % Retail */


/* MDTP = Markdown Type */
const char MDTP[NULL_CODE] = "MDTP";

const char MDTP_11    [NULL_CODE] = "11";   /* Markup */
const char MDTP_12    [NULL_CODE] = "12";   /* Markup Cancel */
const char MDTP_13    [NULL_CODE] = "13";   /* Permanent Markdown */
const char MDTP_14    [NULL_CODE] = "14";   /* Markdown Cancel */
const char MDTP_70    [NULL_CODE] = "70";   /* Cost Change */


/* MEAS = Measurment Type */
const char MEAS[NULL_CODE] = "MEAS";

const char MEAS_M     [NULL_CODE] = "M";   /* Metric */
const char MEAS_I     [NULL_CODE] = "I";   /* Imperial */


/* MELB = Merchandise Hierarchy Labels */
const char MELB[NULL_CODE] = "MELB";

const char MELB_C     [NULL_CODE] = "C";   /* @OH1@ */
const char MELB_DI    [NULL_CODE] = "DI";   /* @MH2@ */
const char MELB_D     [NULL_CODE] = "D";   /* @MH4@ */
const char MELB_CL    [NULL_CODE] = "CL";   /* @MH5@ */
const char MELB_S     [NULL_CODE] = "S";   /* @MH6@ */
const char MELB_ASIT  [NULL_CODE] = "ASIT";   /* All @MHP6@ in the @MH5@ */
const char MELB_ACIT  [NULL_CODE] = "ACIT";   /* All @MHP5@ in the @MH4@ */
const char MELB_ADPI  [NULL_CODE] = "ADPI";   /* All @MHP4@ in the @MH3@ */
const char MELB_AGIT  [NULL_CODE] = "AGIT";   /* All @MHP3@ in the @MH2@ */
const char MELB_ADIT  [NULL_CODE] = "ADIT";   /* All @MHP2@ in the @OH1@ */
const char MELB_ADP   [NULL_CODE] = "ADP";   /* All @MHP4@ */
const char MELB_AG    [NULL_CODE] = "AG";   /* All @MHP3@ */
const char MELB_AD    [NULL_CODE] = "AD";   /* All @MHP2@ */
const char MELB_ENDS  [NULL_CODE] = "ENDS";   /* Enter @MH4@ and @MH5@ */
const char MELB_ENGP  [NULL_CODE] = "ENGP";   /* Enter @MH3@ */
const char MELB_ENDP  [NULL_CODE] = "ENDP";   /* Enter @MH4@ */
const char MELB_AC    [NULL_CODE] = "AC";   /* All @OHP1@ */
const char MELB_G     [NULL_CODE] = "G";   /* @MH3@ */
const char MELB_M     [NULL_CODE] = "M";   /* Maintenance */
const char MELB_LVC   [NULL_CODE] = "LVC";   /* List of @OHP1@ */
const char MELB_LVDI  [NULL_CODE] = "LVDI";   /* List of @MHP2@ */
const char MELB_LVG   [NULL_CODE] = "LVG";   /* List of @MHP3@ */
const char MELB_LVD   [NULL_CODE] = "LVD";   /* List of @MHP4@ */
const char MELB_LVCL  [NULL_CODE] = "LVCL";   /* List of @MHP5@ */
const char MELB_LVS   [NULL_CODE] = "LVS";   /* List of @MHP6@ */


/* MER1 = Merchandise Level */
const char MER1[NULL_CODE] = "MER1";

const char MER1_D     [NULL_CODE] = "D";   /* @MH4@ */
const char MER1_C     [NULL_CODE] = "C";   /* @MH5@ */
const char MER1_S     [NULL_CODE] = "S";   /* @MH6@ */


/* MFIN = ALC Method of Finalization */
const char MFIN[NULL_CODE] = "MFIN";

const char MFIN_N     [NULL_CODE] = "N";   /* No Finalization */
const char MFIN_W     [NULL_CODE] = "W";   /* Finalization/WAC */


/* MGUC = Cost Template Margin/Upcharge */
const char MGUC[NULL_CODE] = "MGUC";

const char MGUC_M     [NULL_CODE] = "M";   /* Margin */
const char MGUC_U     [NULL_CODE] = "U";   /* Upcharge */


/* MHDD = Merch Hier Default Dialog */
const char MHDD[NULL_CODE] = "MHDD";

const char MHDD_ITEM  [NULL_CODE] = "ITEM";   /* Item Master */


/* MHDI = Merch Hier Default Info */
const char MHDI[NULL_CODE] = "MHDI";

const char MHDI_LOC   [NULL_CODE] = "LOC";   /* Locations */
const char MHDI_SEAS  [NULL_CODE] = "SEAS";   /* Seasons */
const char MHDI_IMPA  [NULL_CODE] = "IMPA";   /* Import Attributes */
const char MHDI_DOCS  [NULL_CODE] = "DOCS";   /* Documents */
const char MHDI_HTS   [NULL_CODE] = "HTS";   /* HTS */
const char MHDI_ETT   [NULL_CODE] = "ETT";   /* Eligible Tariff Treatments */
const char MHDI_EXP   [NULL_CODE] = "EXP";   /* Expenses */
const char MHDI_TIMEL [NULL_CODE] = "TIMEL";   /* Timelines */
const char MHDI_TICKET[NULL_CODE] = "TICKET";   /* Tickets */
const char MHDI_IMAGE [NULL_CODE] = "IMAGE";   /* Image */
const char MHDI_SUBTRN[NULL_CODE] = "SUBTRN";   /* Sub-Tran Level Items */
const char MHDI_DIMO  [NULL_CODE] = "DIMO";   /* Case Dimensions */
const char MHDI_DIFF  [NULL_CODE] = "DIFF";   /* Diffs */
const char MHDI_RECRET[NULL_CODE] = "RECRET";   /* Suggested Retail */
const char MHDI_PCKSZE[NULL_CODE] = "PCKSZE";   /* Package Size Info */
const char MHDI_RETLBL[NULL_CODE] = "RETLBL";   /* Retail Label */
const char MHDI_HSEN  [NULL_CODE] = "HSEN";   /* Handling Sensitivity */
const char MHDI_HTEMP [NULL_CODE] = "HTEMP";   /* Handling Temperature */
const char MHDI_WASTE [NULL_CODE] = "WASTE";   /* Wastage */
const char MHDI_COM   [NULL_CODE] = "COM";   /* Comments */


/* MIKI = Distance Unit of Measure */
const char MIKI[NULL_CODE] = "MIKI";

const char MIKI_1     [NULL_CODE] = "1";   /* Miles */
const char MIKI_2     [NULL_CODE] = "2";   /* Kilometers */


/* MKDV = Mkdvars - Store Cost Change Types */
const char MKDV[NULL_CODE] = "MKDV";

const char MKDV_S     [NULL_CODE] = "S";   /* Store */
const char MKDV_C     [NULL_CODE] = "C";   /* Store Class */
const char MKDV_D     [NULL_CODE] = "D";   /* @OH5@ */
const char MKDV_R     [NULL_CODE] = "R";   /* @OH4@ */
const char MKDV_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char MKDV_L     [NULL_CODE] = "L";   /* Location Traits */
const char MKDV_A     [NULL_CODE] = "A";   /* All Stores */


/* MKTC = Electronic Marketing Clubs */
const char MKTC[NULL_CODE] = "MKTC";

const char MKTC_B     [NULL_CODE] = "B";   /* Baby Club */
const char MKTC_F     [NULL_CODE] = "F";   /* Family Club */
const char MKTC_BF    [NULL_CODE] = "BF";   /* Baby/Family Clubs */
const char MKTC_FR    [NULL_CODE] = "FR";   /* Frozen Foods Club */
const char MKTC_D     [NULL_CODE] = "D";   /* Deli Club */
const char MKTC_P     [NULL_CODE] = "P";   /* Produce Club */
const char MKTC_HL    [NULL_CODE] = "HL";   /* Healthy Living Club */
const char MKTC_NF    [NULL_CODE] = "NF";   /* Natural Foods Club */
const char MKTC_PHL   [NULL_CODE] = "PHL";   /* Produce/Healthy Living Clubs */
const char MKTC_PHLNF [NULL_CODE] = "PHLNF";   /* Produce/Healthy Living/Natural Clubs */


/* MLVL = Filter Merchandise Levels */
const char MLVL[NULL_CODE] = "MLVL";

const char MLVL_D     [NULL_CODE] = "D";   /* @MH2@ */
const char MLVL_G     [NULL_CODE] = "G";   /* Groups */
const char MLVL_P     [NULL_CODE] = "P";   /* @MH4@ */
const char MLVL_C     [NULL_CODE] = "C";   /* @MH5@ */
const char MLVL_S     [NULL_CODE] = "S";   /* @MH6@ */


/* MNCJ = Minimum Order Conjunction */
const char MNCJ[NULL_CODE] = "MNCJ";

const char MNCJ_A     [NULL_CODE] = "A";   /* AND */
const char MNCJ_O     [NULL_CODE] = "O";   /* OR */


/* MNLV = Supplier Order Min Level */
const char MNLV[NULL_CODE] = "MNLV";

const char MNLV_O     [NULL_CODE] = "O";   /* Total Order */
const char MNLV_L     [NULL_CODE] = "L";   /* Location */
const char MNLV_N     [NULL_CODE] = "N";   /* None Required */


/* MNTH = Month Abbreviations */
const char MNTH[NULL_CODE] = "MNTH";

const char MNTH_1     [NULL_CODE] = "1";   /* JAN */
const char MNTH_2     [NULL_CODE] = "2";   /* FEB */
const char MNTH_3     [NULL_CODE] = "3";   /* MAR */
const char MNTH_4     [NULL_CODE] = "4";   /* APR */
const char MNTH_5     [NULL_CODE] = "5";   /* MAY */
const char MNTH_6     [NULL_CODE] = "6";   /* JUN */
const char MNTH_7     [NULL_CODE] = "7";   /* JUL */
const char MNTH_8     [NULL_CODE] = "8";   /* AUG */
const char MNTH_9     [NULL_CODE] = "9";   /* SEP */
const char MNTH_10    [NULL_CODE] = "10";   /* OCT */
const char MNTH_11    [NULL_CODE] = "11";   /* NOV */
const char MNTH_12    [NULL_CODE] = "12";   /* DEC */


/* MNTP = Supplier Order Min Type */
const char MNTP[NULL_CODE] = "MNTP";

const char MNTP_VOL   [NULL_CODE] = "VOL";   /* Volume */
const char MNTP_MASS  [NULL_CODE] = "MASS";   /* Mass */
const char MNTP_PACK  [NULL_CODE] = "PACK";   /* Pack */
const char MNTP_QTY   [NULL_CODE] = "QTY";   /* Quantity */
const char MNTP_AMT   [NULL_CODE] = "AMT";   /* Amount */


/* MONT = Months of the Year */
const char MONT[NULL_CODE] = "MONT";

const char MONT_1     [NULL_CODE] = "1";   /* January */
const char MONT_2     [NULL_CODE] = "2";   /* February */
const char MONT_3     [NULL_CODE] = "3";   /* March */
const char MONT_4     [NULL_CODE] = "4";   /* April */
const char MONT_5     [NULL_CODE] = "5";   /* May */
const char MONT_6     [NULL_CODE] = "6";   /* June */
const char MONT_7     [NULL_CODE] = "7";   /* July */
const char MONT_8     [NULL_CODE] = "8";   /* August */
const char MONT_9     [NULL_CODE] = "9";   /* September */
const char MONT_10    [NULL_CODE] = "10";   /* October */
const char MONT_11    [NULL_CODE] = "11";   /* November */
const char MONT_12    [NULL_CODE] = "12";   /* December */


const char MORDER_6000  [NULL_CODE] = "6000";   /* Money Orders */


/* MRIT = MRT apply Item Types */
const char MRIT[NULL_CODE] = "MRIT";

const char MRIT_I     [NULL_CODE] = "I";   /* Item */
const char MRIT_IP    [NULL_CODE] = "IP";   /* Item Parent */
const char MRIT_IG    [NULL_CODE] = "IG";   /* Item Grandparent */
const char MRIT_IL    [NULL_CODE] = "IL";   /* Item List */


/* MRLV = Merchandise Levels */
const char MRLV[NULL_CODE] = "MRLV";

const char MRLV_DEP   [NULL_CODE] = "DEP";   /* @MH4@ */
const char MRLV_CLA   [NULL_CODE] = "CLA";   /* @MH5@ */
const char MRLV_SUB   [NULL_CODE] = "SUB";   /* @MH6@ */
const char MRLV_STY   [NULL_CODE] = "STY";   /* Item Parent */
const char MRLV_SKU   [NULL_CODE] = "SKU";   /* Item */
const char MRLV_LST   [NULL_CODE] = "LST";   /* Item List */


/* MRTS = MRT Status */
const char MRTS[NULL_CODE] = "MRTS";

const char MRTS_I     [NULL_CODE] = "I";   /* Input */
const char MRTS_S     [NULL_CODE] = "S";   /* Submitted */
const char MRTS_A     [NULL_CODE] = "A";   /* Approved */
const char MRTS_R     [NULL_CODE] = "R";   /* RTV Created */
const char MRTS_C     [NULL_CODE] = "C";   /* Closed */


/* MSCR = Miscellaneous Report Type */
const char MSCR[NULL_CODE] = "MSCR";

const char MSCR_M     [NULL_CODE] = "M";   /* Money Orders */
const char MSCR_P     [NULL_CODE] = "P";   /* Pay In/Out */
const char MSCR_B     [NULL_CODE] = "B";   /* Button Configurations */
const char MSCR_T     [NULL_CODE] = "T";   /* Tender Types */
const char MSCR_S     [NULL_CODE] = "S";   /* Supplier Payment Methods */


/* MSRP = Miscellaneous Sales Tax Reports */
const char MSRP[NULL_CODE] = "MSRP";

const char MSRP_W     [NULL_CODE] = "W";   /* Items With No Tax Codes */
const char MSRP_L     [NULL_CODE] = "L";   /* Tax Rates by Location */
const char MSRP_I     [NULL_CODE] = "I";   /* Tax Rates by Item/@MH4@ */


/* MTCH = Ship Match Criteria */
const char MTCH[NULL_CODE] = "MTCH";

const char MTCH_P     [NULL_CODE] = "P";   /* Purchase Order */
const char MTCH_S     [NULL_CODE] = "S";   /* Supplier */
const char MTCH_A     [NULL_CODE] = "A";   /* All Shipments */


/* MUFR = Mass Update Failure Reason */
const char MUFR[NULL_CODE] = "MUFR";

const char MUFR_F     [NULL_CODE] = "F";   /* Failed */
const char MUFR_N     [NULL_CODE] = "N";   /* Not Reviewed */


/* MUTP = Markup Calculation Type */
const char MUTP[NULL_CODE] = "MUTP";

const char MUTP_C     [NULL_CODE] = "C";   /* Cost */
const char MUTP_R     [NULL_CODE] = "R";   /* Retail */


/* MUTY = Markup Type */
const char MUTY[NULL_CODE] = "MUTY";

const char MUTY_C     [NULL_CODE] = "C";   /* Markup % Cost */
const char MUTY_M     [NULL_CODE] = "M";   /* Markup % */
const char MUTY_R     [NULL_CODE] = "R";   /* Markup % Retail */
const char MUTY_S     [NULL_CODE] = "S";   /*        Markup % */


/* NOMF = Nomination Flags */
const char NOMF[NULL_CODE] = "NOMF";

const char NOMF_N     [NULL_CODE] = "N";   /* N/A */
const char NOMF_PLUS  [NULL_CODE] = "+";   /* + */
const char NOMF_MINUS [NULL_CODE] = "-";   /* - */


/* OACE = Ordfind Action List with Contract */
const char OACE[NULL_CODE] = "OACE";

const char OACE_NEW   [NULL_CODE] = "NEW";   /* New Order */
const char OACE_S     [NULL_CODE] = "S";   /* New Order with Contract */
const char OACE_EXIST [NULL_CODE] = "EXIST";   /* Create Order from Existing */
const char OACE_PRE   [NULL_CODE] = "PRE";   /* Create Order with Pre-Issued Number */
const char OACE_VIEW  [NULL_CODE] = "VIEW";   /* View Order */
const char OACE_EDIT  [NULL_CODE] = "EDIT";   /* Edit Order */
const char OACE_NEWC  [NULL_CODE] = "NEWC";   /* Create from Contract */
const char OACE_DSDORD[NULL_CODE] = "DSDORD";   /* DSD Order Entry */


/* OAIT = Order Apply Item Types */
const char OAIT[NULL_CODE] = "OAIT";

const char OAIT_I     [NULL_CODE] = "I";   /* Item */
const char OAIT_P     [NULL_CODE] = "P";   /* Item Parent */
const char OAIT_G     [NULL_CODE] = "G";   /* Item Grandparent */
const char OAIT_A     [NULL_CODE] = "A";   /* All Items */


/* OANC = Ordfind Action List with No Contract */
const char OANC[NULL_CODE] = "OANC";

const char OANC_NEW   [NULL_CODE] = "NEW";   /* New Order */
const char OANC_EXIST [NULL_CODE] = "EXIST";   /* Create Order from Existing */
const char OANC_PRE   [NULL_CODE] = "PRE";   /* Create Order with Pre-Issued Number */
const char OANC_VIEW  [NULL_CODE] = "VIEW";   /* View Order */
const char OANC_EDIT  [NULL_CODE] = "EDIT";   /* Edit Order */
const char OANC_DSDORD[NULL_CODE] = "DSDORD";   /* DSD Order Entry */


/* OARR = Order Rejection Reasons */
const char OARR[NULL_CODE] = "OARR";

const char OARR_VM    [NULL_CODE] = "VM";   /* Vendor minimum not met */
const char OARR_NC    [NULL_CODE] = "NC";   /* Negative cost associated on an item */
const char OARR_UOM   [NULL_CODE] = "UOM";   /* UOM convert error due to incomplete data */
const char OARR_LC    [NULL_CODE] = "LC";   /* Letter of Credit information required */
const char OARR_HTS   [NULL_CODE] = "HTS";   /* HTS information incomplete */
const char OARR_SO    [NULL_CODE] = "SO";   /* Order Scaling errors */
const char OARR_UR    [NULL_CODE] = "UR";   /* User role approval amounts exceeded */
const char OARR_OTB   [NULL_CODE] = "OTB";   /* OTB Check failed */
const char OARR_PT    [NULL_CODE] = "PT";   /* Sup.bracket costed,purchase type reqd */
const char OARR_LTL   [NULL_CODE] = "LTL";   /* Cannot approve LTL orders */
const char OARR_SNA   [NULL_CODE] = "SNA";   /* Supplier on order currently isn't active */
const char OARR_VMI   [NULL_CODE] = "VMI";   /* Vendor Managed Inventory */
const char OARR_NASA  [NULL_CODE] = "NASA";   /* Not Adequate Supplier Availability */
const char OARR_CCF   [NULL_CODE] = "CCF";   /* Credit Check failed */
const char OARR_CUST  [NULL_CODE] = "CUST";   /* Custom approval failed */


/* OBLG = Obligation Levels */
const char OBLG[NULL_CODE] = "OBLG";

const char OBLG_TRCO  [NULL_CODE] = "TRCO";   /* Trans. Container */
const char OBLG_TRCPO [NULL_CODE] = "TRCPO";   /* Trans. Container PO */
const char OBLG_TRCP  [NULL_CODE] = "TRCP";   /* Trans. Container PO/Item */
const char OBLG_TRBL  [NULL_CODE] = "TRBL";   /* Trans. BOL/AWB */
const char OBLG_TRBLP [NULL_CODE] = "TRBLP";   /* Trans. BOL/AWB PO */
const char OBLG_TRBP  [NULL_CODE] = "TRBP";   /* Trans. BOL/AWB PO/Item */
const char OBLG_TRVV  [NULL_CODE] = "TRVV";   /* Trans. Vessel/Voyage/ETD */
const char OBLG_TRVVEP[NULL_CODE] = "TRVVEP";   /* Trans. Vessel/Voyage/ETD PO */
const char OBLG_TRVP  [NULL_CODE] = "TRVP";   /* Trans. Vessel/Voyage/ETD PO/Item */
const char OBLG_PO    [NULL_CODE] = "PO";   /* Purchase Order Header */
const char OBLG_POIT  [NULL_CODE] = "POIT";   /* Purchase Order/Item */
const char OBLG_CUST  [NULL_CODE] = "CUST";   /* Customs Entry Header */
const char OBLG_POT   [NULL_CODE] = "POT";   /* PO Trans. Vessel/Voyage/ETD */
const char OBLG_ASN   [NULL_CODE] = "ASN";   /* ASN */
const char OBLG_ASNP  [NULL_CODE] = "ASNP";   /* ASN PO */
const char OBLG_ASNC  [NULL_CODE] = "ASNC";   /* ASN Carton */


/* OBST = Obligation Status */
const char OBST[NULL_CODE] = "OBST";

const char OBST_P     [NULL_CODE] = "P";   /* Pending */
const char OBST_A     [NULL_CODE] = "A";   /* Approved */


/* ODCS = Orddtl Create status list box */
const char ODCS[NULL_CODE] = "ODCS";

const char ODCS_B     [NULL_CODE] = "B";   /* Built */
const char ODCS_G     [NULL_CODE] = "G";   /* Generated */


/* ODIT = Order Distribution Item Types */
const char ODIT[NULL_CODE] = "ODIT";

const char ODIT_I     [NULL_CODE] = "I";   /* Item */
const char ODIT_VPN   [NULL_CODE] = "VPN";   /* Reference Item */
const char ODIT_IL    [NULL_CODE] = "IL";   /* Item List */
const char ODIT_PT    [NULL_CODE] = "PT";   /* Pack Template */
const char ODIT_P     [NULL_CODE] = "P";   /* Prepack */


/* ODLB = ORDDTL form labels */
const char ODLB[NULL_CODE] = "ODLB";

const char ODLB_C     [NULL_CODE] = "C";   /* Contract */
const char ODLB_CC    [NULL_CODE] = "CC";   /* Currency Code */
const char ODLB_D     [NULL_CODE] = "D";   /* Description */
const char ODLB_S     [NULL_CODE] = "S";   /* Supplier */
const char ODLB_T     [NULL_CODE] = "T";   /* Total Cost */


/* OEIE = The error messages for the OI Inventory Analysis Order Item Errors Report. */
const char OEIE[NULL_CODE] = "OEIE";

const char OEIE_HTS   [NULL_CODE] = "HTS";   /* Approved HTS codes do not exist for this item on the order */
const char OEIE_MARG  [NULL_CODE] = "MARG";   /* Margin percentage has not been meet for this item on the order */
const char OEIE_REFIT [NULL_CODE] = "REFIT";   /* Primary Ref Item is either not defined or not specified on the Order */


/* OEOE = The error messages for the OI Inventory Analysis Order Errors Report. */
const char OEOE[NULL_CODE] = "OEOE";

const char OEOE_LAD   [NULL_CODE] = "LAD";   /* Lading port is missing for this order */
const char OEOE_DISC  [NULL_CODE] = "DISC";   /* Discharge port is missing for this order */
const char OEOE_FACT  [NULL_CODE] = "FACT";   /* Factory is missing for this order */


/* OET = Organization Entity Type */
const char OET[NULL_CODE] = "OET ";

const char OET_R     [NULL_CODE] = "R";   /* Regular Warehouse */
const char OET_M     [NULL_CODE] = "M";   /* Importer */
const char OET_X     [NULL_CODE] = "X";   /* Exporter */


/* OFTP = Offer Type */
const char OFTP[NULL_CODE] = "OFTP";

const char OFTP_1     [NULL_CODE] = "1";   /* Coupon */
const char OFTP_2     [NULL_CODE] = "2";   /* Mailer */
const char OFTP_3     [NULL_CODE] = "3";   /* Bonus Card */
const char OFTP_4     [NULL_CODE] = "4";   /* Bonus Card - End of Aisle */
const char OFTP_5     [NULL_CODE] = "5";   /* BOGO - Buy One Get One */
const char OFTP_6     [NULL_CODE] = "6";   /* BOGO - End of Aisle */
const char OFTP_7     [NULL_CODE] = "7";   /* Cents Off */
const char OFTP_8     [NULL_CODE] = "8";   /* Regular */
const char OFTP_9     [NULL_CODE] = "9";   /* Pre-priced */


/* OHTP = Organizational Hierarchy Types */
const char OHTP[NULL_CODE] = "OHTP";

const char OHTP_1     [NULL_CODE] = "1";   /* @OH1@ */
const char OHTP_10    [NULL_CODE] = "10";   /* @OH2@ */
const char OHTP_20    [NULL_CODE] = "20";   /* @OH3@ */
const char OHTP_30    [NULL_CODE] = "30";   /* @OH4@ */
const char OHTP_40    [NULL_CODE] = "40";   /* @OH5@ */
const char OHTP_50    [NULL_CODE] = "50";   /* Store */


/* OIBI = Operation Insight early or late shipments issue list */
const char OIBI[NULL_CODE] = "OIBI";

const char OIBI_ESHIP [NULL_CODE] = "ESHIP";   /* Early Shipment */
const char OIBI_LSHIP [NULL_CODE] = "LSHIP";   /* Late Shipment */
const char OIBI_ASNNR [NULL_CODE] = "ASNNR";   /* ASN Not Received */
const char OIBI_OTBIN [NULL_CODE] = "OTBIN";   /* OTB Shift In */
const char OIBI_OTBOUT[NULL_CODE] = "OTBOUT";   /* OTB Shift Out */


/* OICS = Order/Item Cost Source */
const char OICS[NULL_CODE] = "OICS";

const char OICS_NORM  [NULL_CODE] = "NORM";   /* Supplier */
const char OICS_BRKT  [NULL_CODE] = "BRKT";   /* Bracket */
const char OICS_DEAL  [NULL_CODE] = "DEAL";   /* Deal */
const char OICS_CONT  [NULL_CODE] = "CONT";   /* Contract */
const char OICS_MANL  [NULL_CODE] = "MANL";   /* Manual */
const char OICS_MULT  [NULL_CODE] = "MULT";   /* Multiple */


/* OLVL = Filter Organization Levels */
const char OLVL[NULL_CODE] = "OLVL";

const char OLVL_C     [NULL_CODE] = "C";   /* @OH2@ */
const char OLVL_A     [NULL_CODE] = "A";   /* Area Retail Merchandising System */
const char OLVL_R     [NULL_CODE] = "R";   /* @OH4@ */
const char OLVL_D     [NULL_CODE] = "D";   /* @OH5@ */
const char OLVL_W     [NULL_CODE] = "W";   /* Warehouse */
const char OLVL_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char OLVL_E     [NULL_CODE] = "E";   /* External Finisher */
const char OLVL_T     [NULL_CODE] = "T";   /* Transfer Entity */
const char OLVL_O     [NULL_CODE] = "O";   /* Org Unit */


/* OPER = Combination Operations */
const char OPER[NULL_CODE] = "OPER";

const char OPER_PLUS  [NULL_CODE] = "+";   /* + */
const char OPER_MINUS [NULL_CODE] = "-";   /* - */


/* OPYM = Obligation Payment Method */
const char OPYM[NULL_CODE] = "OPYM";

const char OPYM_C     [NULL_CODE] = "C";   /* Check */
const char OPYM_W     [NULL_CODE] = "W";   /* Wired Transfer */
const char OPYM_A     [NULL_CODE] = "A";   /* ACH */
const char OPYM_L     [NULL_CODE] = "L";   /* Letter of Credit */


/* ORCA = Ordhead Cancel List Box */
const char ORCA[NULL_CODE] = "ORCA";

const char ORCA_B     [NULL_CODE] = "B";   /* Buyer Cancelled */
const char ORCA_V     [NULL_CODE] = "V";   /* Vendor Cancelled */
const char ORCA_A     [NULL_CODE] = "A";   /* Automatic */


/* ORCS = Order Cost Source */
const char ORCS[NULL_CODE] = "ORCS";

const char ORCS_NORM  [NULL_CODE] = "NORM";   /* Supplier */
const char ORCS_BRKT  [NULL_CODE] = "BRKT";   /* Bracket */
const char ORCS_DEAL  [NULL_CODE] = "DEAL";   /* Deal */
const char ORCS_CONT  [NULL_CODE] = "CONT";   /* Contract */
const char ORCS_MANL  [NULL_CODE] = "MANL";   /* Manual */


/* ORDB = Ordfind Form Labels */
const char ORDB[NULL_CODE] = "ORDB";

const char ORDB_C     [NULL_CODE] = "C";   /* Contract */
const char ORDB_CN    [NULL_CODE] = "CN";   /* Contract No. */


/* ORDL = Ordadd Form Labels */
const char ORDL[NULL_CODE] = "ORDL";

const char ORDL_N     [NULL_CODE] = "N";   /* No orders address on file. */
const char ORDL_M     [NULL_CODE] = "M";   /* Markup % */
const char ORDL_MR    [NULL_CODE] = "MR";   /* Markup % Retail */
const char ORDL_MC    [NULL_CODE] = "MC";   /* Markup % Cost */


/* ORDM = Order Methods */
const char ORDM[NULL_CODE] = "ORDM";

const char ORDM_I     [NULL_CODE] = "I";   /* Invoice Entry */
const char ORDM_U     [NULL_CODE] = "U";   /* Unit Extension */


/* ORDO = Ordadd Order Type List Box (Abreviated) */
const char ORDO[NULL_CODE] = "ORDO";

const char ORDO_NONBAS[NULL_CODE] = "N/B";   /* N/B */
const char ORDO_ARB   [NULL_CODE] = "ARB";   /* ARB */
const char ORDO_BRB   [NULL_CODE] = "BRB";   /* BRB */
const char ORDO_DSD   [NULL_CODE] = "DSD";   /* DSD */
const char ORDO_CO    [NULL_CODE] = "CO";   /* Customer Order */


/* ORDR = Order Revision Labels */
const char ORDR[NULL_CODE] = "ORDR";

const char ORDR_R     [NULL_CODE] = "R";   /* Revision */
const char ORDR_V     [NULL_CODE] = "V";   /* Version */


/* ORDS = Ordsumm form labels */
const char ORDS[NULL_CODE] = "ORDS";

const char ORDS_L     [NULL_CODE] = "L";   /*  Location Totals */
const char ORDS_S     [NULL_CODE] = "S";   /*  Item */
const char ORDS_O     [NULL_CODE] = "O";   /*  Order Totals */


/* ORDT = Order Type */
const char ORDT[NULL_CODE] = "ORDT";

const char ORDT_F     [NULL_CODE] = "F";   /* Fixed Weight */
const char ORDT_V     [NULL_CODE] = "V";   /* Variable Weight */


/* ORG1 = Organizational Hierarchy */
const char ORG1[NULL_CODE] = "ORG1";

const char ORG1_CO    [NULL_CODE] = "CO";   /* @OH1@ */
const char ORG1_CH    [NULL_CODE] = "CH";   /* @OH2@ */
const char ORG1_AR    [NULL_CODE] = "AR";   /* @OH3@ */
const char ORG1_RE    [NULL_CODE] = "RE";   /* @OH4@ */
const char ORG1_DI    [NULL_CODE] = "DI";   /* @OH5@ */
const char ORG1_ST    [NULL_CODE] = "ST";   /* Store */
const char ORG1_WH    [NULL_CODE] = "WH";   /* Warehouse */


/* ORGH = Organizational Hierarchy */
const char ORGH[NULL_CODE] = "ORGH";

const char ORGH_R     [NULL_CODE] = "R";   /* @OH4@ */
const char ORGH_D     [NULL_CODE] = "D";   /* @OH5@ */
const char ORGH_S     [NULL_CODE] = "S";   /* Store */
const char ORGH_W     [NULL_CODE] = "W";   /* Warehouse */


/* ORL = Order Rounding Level */
const char ORL[NULL_CODE] = "ORL ";

const char ORL_C     [NULL_CODE] = "C";   /* Case */
const char ORL_L     [NULL_CODE] = "L";   /* Layer */
const char ORL_P     [NULL_CODE] = "P";   /* Pallet */
const char ORL_CL    [NULL_CODE] = "CL";   /* Case/Layer */
const char ORL_LP    [NULL_CODE] = "LP";   /* Layer/Pallet */
const char ORL_CLP   [NULL_CODE] = "CLP";   /* Case/Layer/Pallet */


/* ORLB = Organizational Hierarchy Labels */
const char ORLB[NULL_CODE] = "ORLB";

const char ORLB_AC    [NULL_CODE] = "AC";   /* All @OHP1@ */
const char ORLB_ACH   [NULL_CODE] = "ACH";   /* All @OHP2@ */
const char ORLB_AR    [NULL_CODE] = "AR";   /* All @OHP4@ */
const char ORLB_AA    [NULL_CODE] = "AA";   /* All @OHP3@ */
const char ORLB_D     [NULL_CODE] = "D";   /* @OH5@ */
const char ORLB_R     [NULL_CODE] = "R";   /* @OH4@ */
const char ORLB_A     [NULL_CODE] = "A";   /* @OH3@ */
const char ORLB_CH    [NULL_CODE] = "CH";   /* @OH2@ */
const char ORLB_C     [NULL_CODE] = "C";   /* @OH1@ */
const char ORLB_ASIT  [NULL_CODE] = "ASIT";   /* All Stores in the @OH5@ */
const char ORLB_ADIT  [NULL_CODE] = "ADIT";   /* All @OHP5@ in the @OH4@ */
const char ORLB_ARIT  [NULL_CODE] = "ARIT";   /* All @OHP4@ in the @OH3@ */
const char ORLB_W     [NULL_CODE] = "W";   /* Warehouse */
const char ORLB_S     [NULL_CODE] = "S";   /* Store */
const char ORLB_AAIT  [NULL_CODE] = "AAIT";   /* All @OHP3@ in the @OH2@ */
const char ORLB_ACIT  [NULL_CODE] = "ACIT";   /* All @OHP2@ in the @OH1@ */
const char ORLB_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char ORLB_AS    [NULL_CODE] = "AS";   /* All Stores */
const char ORLB_AD    [NULL_CODE] = "AD";   /* All @OHP5@ */
const char ORLB_PHWH  [NULL_CODE] = "PHWH";   /* Physical Warehouses */
const char ORLB_LVA   [NULL_CODE] = "LVA";   /* List of @OHP3@ */
const char ORLB_LVR   [NULL_CODE] = "LVR";   /* List of @OHP4@ */
const char ORLB_LVD   [NULL_CODE] = "LVD";   /* List of @OHP5@ */


/* ORM2 = Merchandise Order */
const char ORM2[NULL_CODE] = "ORM2";

const char ORM2_C     [NULL_CODE] = "C";   /* Case Pack */
const char ORM2_I     [NULL_CODE] = "I";   /* Inner Pack */
const char ORM2_E     [NULL_CODE] = "E";   /* Eaches */


/* ORML = Merchandise Order Multiples */
const char ORML[NULL_CODE] = "ORML";

const char ORML_E     [NULL_CODE] = "E";   /* Eaches */
const char ORML_I     [NULL_CODE] = "I";   /* Inner */
const char ORML_C     [NULL_CODE] = "C";   /* Cases */


/* OROR = Order Origin */
const char OROR[NULL_CODE] = "OROR";

const char OROR_0     [NULL_CODE] = "0";   /* Current System Generated */
const char OROR_1     [NULL_CODE] = "1";   /* Past System Generated */
const char OROR_2     [NULL_CODE] = "2";   /* Manual */
const char OROR_3     [NULL_CODE] = "3";   /* Buyer Worksheet */
const char OROR_4     [NULL_CODE] = "4";   /* Consignment */
const char OROR_5     [NULL_CODE] = "5";   /* Vendor Generated */
const char OROR_6     [NULL_CODE] = "6";   /* AIP Generated PO */
const char OROR_7     [NULL_CODE] = "7";   /* SIM Generated PO */
const char OROR_8     [NULL_CODE] = "8";   /* Allocation Generated PO */


/* ORRC = Sales Audit Override Reason Codes */
const char ORRC[NULL_CODE] = "ORRC";

const char ORRC_ERR   [NULL_CODE] = "ERR";   /* Error */
const char ORRC_D     [NULL_CODE] = "D";   /* Damaged Goods */
const char ORRC_T     [NULL_CODE] = "T";   /* Incorrect Tag */
const char ORRC_S     [NULL_CODE] = "S";   /* Incorrect Signage */
const char ORRC_CP    [NULL_CODE] = "CP";   /* Competition Price */
const char ORRC_AP    [NULL_CODE] = "AP";   /* Ad Price */
const char ORRC_MS    [NULL_CODE] = "MS";   /* Managers Special */
const char ORRC_PROMPT[NULL_CODE] = "PROMPT";   /* Price Prompt in POS */
const char ORRC_BSPRC [NULL_CODE] = "BSPRC";   /* Base Price Rule */
const char ORRC_AUTHMT[NULL_CODE] = "AUTHMT";   /* Authorized Amount */
const char ORRC_ARPR_1[NULL_CODE] = "ARPR_1";   /* Insufficient Funds (House Account Reversal) */
const char ORRC_ARPR_2[NULL_CODE] = "ARPR_2";   /* Wrong Account (House Account Reversal) */
const char ORRC_ARPR_3[NULL_CODE] = "ARPR_3";   /* Wrong Amount (House Account Reversal) */
const char ORRC_ARPR_4[NULL_CODE] = "ARPR_4";   /* Other - Enter Comment (House Account Reversal) */


/* ORSI = PO Induction Order Status */
const char ORSI[NULL_CODE] = "ORSI";

const char ORSI_W     [NULL_CODE] = "W";   /* Worksheet */
const char ORSI_S     [NULL_CODE] = "S";   /* Submitted */
const char ORSI_A     [NULL_CODE] = "A";   /* Approved */
const char ORSI_C     [NULL_CODE] = "C";   /* Closed */
const char ORSI_D     [NULL_CODE] = "D";   /* Deleted */


/* ORSS = Ordskus form labels */
const char ORSS[NULL_CODE] = "ORSS";

const char ORSS_SK    [NULL_CODE] = "SK";   /* Item Description */
const char ORSS_U     [NULL_CODE] = "U";   /* Child Item */
const char ORSS_S     [NULL_CODE] = "S";   /* Supplement */
const char ORSS_D     [NULL_CODE] = "D";   /* Description */
const char ORSS_V     [NULL_CODE] = "V";   /* VPN */
const char ORSS_P     [NULL_CODE] = "P";   /* Pre-Pack */


/* ORST = Order Status */
const char ORST[NULL_CODE] = "ORST";

const char ORST_W     [NULL_CODE] = "W";   /* Worksheet */
const char ORST_S     [NULL_CODE] = "S";   /* Submitted */
const char ORST_A     [NULL_CODE] = "A";   /* Approved */
const char ORST_C     [NULL_CODE] = "C";   /* Closed */


/* ORTP = Order Type */
const char ORTP[NULL_CODE] = "ORTP";

const char ORTP_NONBAS[NULL_CODE] = "N/B";   /* Non-Basic */
const char ORTP_ARB   [NULL_CODE] = "ARB";   /* Automatic Reorder of Basic */
const char ORTP_BRB   [NULL_CODE] = "BRB";   /* Buyer Reorder of Basic */


/* OS9T = Purchase Order Template Types */
const char OS9T[NULL_CODE] = "OS9T";

const char OS9T_PODT  [NULL_CODE] = "PODT";   /* Purchase Orders */


/* OSGP = Sales Audit Over/Short Groups */
const char OSGP[NULL_CODE] = "OSGP";

const char OSGP_B     [NULL_CODE] = "B";   /* Accountable For */
const char OSGP_D     [NULL_CODE] = "D";   /* Accounted For */


/* OTBC = OTB Calculation Type */
const char OTBC[NULL_CODE] = "OTBC";

const char OTBC_C     [NULL_CODE] = "C";   /* Direct Cost */
const char OTBC_R     [NULL_CODE] = "R";   /* Retail Inventory */


/* OTBR = Open to Buy Report Options */
const char OTBR[NULL_CODE] = "OTBR";

const char OTBR_D     [NULL_CODE] = "D";   /* @MH4@ */
const char OTBR_AD    [NULL_CODE] = "AD";   /* All @MHP4@ */
const char OTBR_C     [NULL_CODE] = "C";   /* @MH5@ */
const char OTBR_AC    [NULL_CODE] = "AC";   /* All @MHP5@ */
const char OTBR_S     [NULL_CODE] = "S";   /* @MH6@ */
const char OTBR_AS    [NULL_CODE] = "AS";   /* All @MHP6@ */


/* OXTP = Exchange Types */
const char OXTP[NULL_CODE] = "OXTP";

const char OXTP_O     [NULL_CODE] = "O";   /* Operational */
const char OXTP_L     [NULL_CODE] = "L";   /* LC/Bank */
const char OXTP_P     [NULL_CODE] = "P";   /* Purchase Order */
const char OXTP_U     [NULL_CODE] = "U";   /* Customs Entry */
const char OXTP_T     [NULL_CODE] = "T";   /* Transportation */


/* PACT = POS Action Listbox */
const char PACT[NULL_CODE] = "PACT";

const char PACT_NEW   [NULL_CODE] = "NEW";   /* New */
const char PACT_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char PACT_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char PACT_NEWE  [NULL_CODE] = "NEWE";   /* Create From Existing */


/* PALN = Pallet and Equivalent Types */
const char PALN[NULL_CODE] = "PALN";

const char PALN_PAL   [NULL_CODE] = "PAL";   /* Pallet */
const char PALN_FLA   [NULL_CODE] = "FLA";   /* Flat */


/* PARC = Pack Receive Indicator */
const char PARC[NULL_CODE] = "PARC";

const char PARC_E     [NULL_CODE] = "E";   /* Eaches */
const char PARC_P     [NULL_CODE] = "P";   /* Pack */


/* PART = Partner Types */
const char PART[NULL_CODE] = "PART";

const char PART_BK    [NULL_CODE] = "BK";   /* Bank */
const char PART_AG    [NULL_CODE] = "AG";   /* Agent */
const char PART_FF    [NULL_CODE] = "FF";   /* Freight Forwarder */
const char PART_IM    [NULL_CODE] = "IM";   /* Importer */
const char PART_BR    [NULL_CODE] = "BR";   /* Broker */
const char PART_FA    [NULL_CODE] = "FA";   /* Factory */
const char PART_AP    [NULL_CODE] = "AP";   /* Applicant */
const char PART_CO    [NULL_CODE] = "CO";   /* Consolidator */
const char PART_CN    [NULL_CODE] = "CN";   /* Consignee */


/* PATH = Phone Authorization Type */
const char PATH[NULL_CODE] = "PATH";

const char PATH_SEARS [NULL_CODE] = "SEARS";   /* Sears */
const char PATH_NPC   [NULL_CODE] = "NPC";   /* NPC */
const char PATH_NONE  [NULL_CODE] = "NONE";   /* None */


/* PATP = Pack Type */
const char PATP[NULL_CODE] = "PATP";

const char PATP_B     [NULL_CODE] = "B";   /* Buyer Pack */
const char PATP_V     [NULL_CODE] = "V";   /* Vendor Pack */


/* PAYM = Payment Method */
const char PAYM[NULL_CODE] = "PAYM";

const char PAYM_LC    [NULL_CODE] = "LC";   /* Letter of Credit */
const char PAYM_OA    [NULL_CODE] = "OA";   /* Open Account */
const char PAYM_WT    [NULL_CODE] = "WT";   /* Wire Transfer */


/* PAYT = CASH,MORD,INVC list box */
const char PAYT[NULL_CODE] = "PAYT";

const char PAYT_CASH  [NULL_CODE] = "CASH";   /* Cash */
const char PAYT_INVC  [NULL_CODE] = "INVC";   /* Invoice */


/* PCAC = Price Change Action List */
const char PCAC[NULL_CODE] = "PCAC";

const char PCAC_A     [NULL_CODE] = "A";   /* New */
const char PCAC_B     [NULL_CODE] = "B";   /* View */
const char PCAC_C     [NULL_CODE] = "C";   /* Edit */


/* PCCH = Overlap Types */
const char PCCH[NULL_CODE] = "PCCH";

const char PCCH_PC    [NULL_CODE] = "PC";   /* Price Change */
const char PCCH_PR    [NULL_CODE] = "PR";   /* Promotion */
const char PCCH_CL    [NULL_CODE] = "CL";   /* Clearance */
const char PCCH_ZC    [NULL_CODE] = "ZC";   /* Zone Type */


/* PCET = Price change event type */
const char PCET[NULL_CODE] = "PCET";

const char PCET_BASERT[NULL_CODE] = "BASERT";   /* Base retail */
const char PCET_PROMS [NULL_CODE] = "PROMS";   /* Promotion Start */
const char PCET_PROME [NULL_CODE] = "PROME";   /* Promotion End */
const char PCET_REG   [NULL_CODE] = "REG";   /* Regular */
const char PCET_CLRS  [NULL_CODE] = "CLRS";   /* Clearance Start */
const char PCET_CLRE  [NULL_CODE] = "CLRE";   /* Clearance End */


/* PCIT = Price Change Item */
const char PCIT[NULL_CODE] = "PCIT";

const char PCIT_A     [NULL_CODE] = "A";   /* Single Item */
const char PCIT_B     [NULL_CODE] = "B";   /* Item List */
const char PCIT_C     [NULL_CODE] = "C";   /* Item Parent/Diff */


/* PCKT = Packaging Type */
const char PCKT[NULL_CODE] = "PCKT";

const char PCKT_JHOOK [NULL_CODE] = "JHOOK";   /* Peggable Packaging */
const char PCKT_STACK [NULL_CODE] = "STACK";   /* Stackable Packaging */


/* PCLA = Product Classification */
const char PCLA[NULL_CODE] = "PCLA";

const char PCLA_ELC   [NULL_CODE] = "ELC";   /* Electronics */
const char PCLA_GRC   [NULL_CODE] = "GRC";   /* Grocery */
const char PCLA_APP   [NULL_CODE] = "APP";   /* Apparel */


/* PCLB = Price Change Label */
const char PCLB[NULL_CODE] = "PCLB";

const char PCLB_R     [NULL_CODE] = "R";   /* Regular Price Change */
const char PCLB_S     [NULL_CODE] = "S";   /* Item Zone Group Changed */


/* PCLO = Price Change Location List */
const char PCLO[NULL_CODE] = "PCLO";

const char PCLO_C     [NULL_CODE] = "C";   /* Store Class */
const char PCLO_D     [NULL_CODE] = "D";   /* @OH5@ */
const char PCLO_R     [NULL_CODE] = "R";   /* @OH4@ */
const char PCLO_S     [NULL_CODE] = "S";   /* Store */
const char PCLO_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char PCLO_L     [NULL_CODE] = "L";   /* Location Traits */
const char PCLO_A     [NULL_CODE] = "A";   /* All Stores */
const char PCLO_LL    [NULL_CODE] = "LL";   /* Location List */


/* PCLR = POS Color */
const char PCLR[NULL_CODE] = "PCLR";

const char PCLR_BLACK [NULL_CODE] = "BLACK";   /* Black */
const char PCLR_BLUE  [NULL_CODE] = "BLUE";   /* Blue */
const char PCLR_CYAN  [NULL_CODE] = "CYAN";   /* Cyan */
const char PCLR_GRAY  [NULL_CODE] = "GRAY";   /* Gray */
const char PCLR_GREEN [NULL_CODE] = "GREEN";   /* Green */
const char PCLR_KHAKI [NULL_CODE] = "KHAKI";   /* Khaki */
const char PCLR_LBLUE [NULL_CODE] = "LBLUE";   /* Light Blue */
const char PCLR_LGREEN[NULL_CODE] = "LGREEN";   /* Light Green */
const char PCLR_LPURPL[NULL_CODE] = "LPURPL";   /* Light Purple */
const char PCLR_MAGNTA[NULL_CODE] = "MAGNTA";   /* Magenta */
const char PCLR_ORANGE[NULL_CODE] = "ORANGE";   /* Orange */
const char PCLR_PINK  [NULL_CODE] = "PINK";   /* Pink */
const char PCLR_PURPLE[NULL_CODE] = "PURPLE";   /* Purple */
const char PCLR_RED   [NULL_CODE] = "RED";   /* Red */
const char PCLR_WHITE [NULL_CODE] = "WHITE";   /* White */
const char PCLR_YELLOW[NULL_CODE] = "YELLOW";   /* Yellow */


/* PCO1 = Price Change Organization (zones) */
const char PCO1[NULL_CODE] = "PCO1";

const char PCO1_1     [NULL_CODE] = "1";   /* Store */
const char PCO1_2     [NULL_CODE] = "2";   /* Store Class */
const char PCO1_3     [NULL_CODE] = "3";   /* @OH5@ */
const char PCO1_4     [NULL_CODE] = "4";   /* Transfer Zone */
const char PCO1_5     [NULL_CODE] = "5";   /* @OH4@ */
const char PCO1_6     [NULL_CODE] = "6";   /* Pricing Zone */


/* PCON = POS Configurations */
const char PCON[NULL_CODE] = "PCON";

const char PCON_ALL   [NULL_CODE] = "ALL";   /* All */
const char PCON_PRES  [NULL_CODE] = "PRES";   /* Product Restrictions */
const char PCON_COUP  [NULL_CODE] = "COUP";   /* Coupons */


/* PCOR = Price Change Find Origin */
const char PCOR[NULL_CODE] = "PCOR";

const char PCOR_I     [NULL_CODE] = "I";   /* Retail */
const char PCOR_C     [NULL_CODE] = "C";   /* Cost */


/* PCOS = Price Change Overlap Status */
const char PCOS[NULL_CODE] = "PCOS";

const char PCOS_W     [NULL_CODE] = "W";   /* Worksheet */
const char PCOS_S     [NULL_CODE] = "S";   /* Submitted */
const char PCOS_A     [NULL_CODE] = "A";   /* Approved */
const char PCOS_E     [NULL_CODE] = "E";   /* Extracted */
const char PCOS_R     [NULL_CODE] = "R";   /* Rejected */
const char PCOS_C     [NULL_CODE] = "C";   /* Cancelled */
const char PCOS_D     [NULL_CODE] = "D";   /* Deleted */
const char PCOS_T     [NULL_CODE] = "T";   /* Immediate */


/* PCOV = Price Change Overlap (abbrev) */
const char PCOV[NULL_CODE] = "PCOV";

const char PCOV_A     [NULL_CODE] = "A";   /* Apprv */
const char PCOV_E     [NULL_CODE] = "E";   /* Extr */
const char PCOV_S     [NULL_CODE] = "S";   /* Submt */
const char PCOV_R     [NULL_CODE] = "R";   /* Rejct */
const char PCOV_D     [NULL_CODE] = "D";   /* Del */
const char PCOV_C     [NULL_CODE] = "C";   /* Canc */
const char PCOV_T     [NULL_CODE] = "T";   /* Immdt */
const char PCOV_W     [NULL_CODE] = "W";   /* Work */


/* PCPD = Price Change Pending */
const char PCPD[NULL_CODE] = "PCPD";

const char PCPD_A     [NULL_CODE] = "A";   /* All */
const char PCPD_NP    [NULL_CODE] = "NP";   /* No Price Change Pending */
const char PCPD_PP    [NULL_CODE] = "PP";   /* Price Change Pending */


/* PCSC = Price Change Style/Color */
const char PCSC[NULL_CODE] = "PCSC";

const char PCSC_A     [NULL_CODE] = "A";   /* Single Item */
const char PCSC_B     [NULL_CODE] = "B";   /* Item List */
const char PCSC_C     [NULL_CODE] = "C";   /* Item Parent/Diff */


/* PCST = Price Change Status */
const char PCST[NULL_CODE] = "PCST";

const char PCST_0     [NULL_CODE] = "0";   /* New Item Added */
const char PCST_2     [NULL_CODE] = "2";   /* Unit Cost Changed */
const char PCST_4     [NULL_CODE] = "4";   /* Unit Retail Changed */
const char PCST_8     [NULL_CODE] = "8";   /* Unit Retail Changed/Clearance */
const char PCST_9     [NULL_CODE] = "9";   /* Unit Retail Changed/Promotions */
const char PCST_10    [NULL_CODE] = "10";   /* Multi-Unit Retail Changed */
const char PCST_11    [NULL_CODE] = "11";   /* Multi/Single - U Retail Changed */
const char PCST_99    [NULL_CODE] = "99";   /* Multi/Single - U Retail Changed */
const char PCST_R     [NULL_CODE] = "R";   /* All Retail Changes */


/* PCT1 = Price Change Tran Type (abbrev) */
const char PCT1[NULL_CODE] = "PCT1";

const char PCT1_R     [NULL_CODE] = "R";   /* Regular */
const char PCT1_S     [NULL_CODE] = "S";   /* Zone Grp. */


/* PCT2 = Price Change List (condensed) */
const char PCT2[NULL_CODE] = "PCT2";

const char PCT2_MA    [NULL_CODE] = "MA";   /* Change Price By Amount */
const char PCT2_PC    [NULL_CODE] = "PC";   /* Change Price by % */
const char PCT2_PB    [NULL_CODE] = "PB";   /* Set Price to % of Base */
const char PCT2_NP    [NULL_CODE] = "NP";   /* Set Price to Set Amount */
const char PCT2_RP    [NULL_CODE] = "RP";   /* Reset POS Price */


/* PCT3 = Price Change List (clearance) */
const char PCT3[NULL_CODE] = "PCT3";

const char PCT3_MA    [NULL_CODE] = "MA";   /* Reduce Price By Amount */
const char PCT3_PC    [NULL_CODE] = "PC";   /* Reduce Price by % */
const char PCT3_PB    [NULL_CODE] = "PB";   /* Set Price to % of Base */
const char PCT3_NP    [NULL_CODE] = "NP";   /* Set Price to a Set Amount */


/* PCT4 = Price Change Type (pcstbysk) */
const char PCT4[NULL_CODE] = "PCT4";

const char PCT4_MA    [NULL_CODE] = "MA";   /* Change Price by Amount */
const char PCT4_PC    [NULL_CODE] = "PC";   /* Change Price by % */
const char PCT4_PB    [NULL_CODE] = "PB";   /* Set Price to % of Base */
const char PCT4_NP    [NULL_CODE] = "NP";   /* Set Price to Set Amount */
const char PCT4_OS    [NULL_CODE] = "OS";   /* Reset to Original Store Price */
const char PCT4_SB    [NULL_CODE] = "SB";   /* Reset Store */


/* PCT5 = Price Change Type (pcstbysk1) */
const char PCT5[NULL_CODE] = "PCT5";

const char PCT5_MA    [NULL_CODE] = "MA";   /* Change Price by Amount */
const char PCT5_PC    [NULL_CODE] = "PC";   /* Change Price by % */
const char PCT5_PB    [NULL_CODE] = "PB";   /* Set Price to % of Base */
const char PCT5_NP    [NULL_CODE] = "NP";   /* Set Price to Set Amount */
const char PCT5_SB    [NULL_CODE] = "SB";   /* Reset Store */


/* PCTL = Price Change Type List */
const char PCTL[NULL_CODE] = "PCTL";

const char PCTL_MA    [NULL_CODE] = "MA";   /* Change Price By Amount */
const char PCTL_PC    [NULL_CODE] = "PC";   /* Change Price by % */
const char PCTL_PB    [NULL_CODE] = "PB";   /* Set Price to % of Base */
const char PCTL_NP    [NULL_CODE] = "NP";   /* Set Price to a Set Amount */
const char PCTL_RP    [NULL_CODE] = "RP";   /* Reset POS Price */
const char PCTL_OS    [NULL_CODE] = "OS";   /* Reset to Original Store Price */
const char PCTL_SB    [NULL_CODE] = "SB";   /* Reset Store */


/* PCTP = Price Change Exception Type */
const char PCTP[NULL_CODE] = "PCTP";

const char PCTP_MA    [NULL_CODE] = "MA";   /* Change Current Price by Amount */
const char PCTP_NC    [NULL_CODE] = "NC";   /* No Change */
const char PCTP_NP    [NULL_CODE] = "NP";   /* Set Price to an Amount */
const char PCTP_OS    [NULL_CODE] = "OS";   /* Reset to Original Store Price */
const char PCTP_PB    [NULL_CODE] = "PB";   /* Set Price to Percentage of Base */
const char PCTP_PC    [NULL_CODE] = "PC";   /* Change Current Price by Percentage */
const char PCTP_RP    [NULL_CODE] = "RP";   /* Reset POS Price */
const char PCTP_SB    [NULL_CODE] = "SB";   /* Reset Store */


/* PCTT = Price Change Transaction Type */
const char PCTT[NULL_CODE] = "PCTT";

const char PCTT_R     [NULL_CODE] = "R";   /* Regular Price Change */
const char PCTT_S     [NULL_CODE] = "S";   /* Item Changed Zone Group */


/* PCTY = Price Type */
const char PCTY[NULL_CODE] = "PCTY";

const char PCTY_R     [NULL_CODE] = "R";   /* Regular */
const char PCTY_P     [NULL_CODE] = "P";   /* Promotion */
const char PCTY_C     [NULL_CODE] = "C";   /* Clearance */


/* PERT = Period Type */
const char PERT[NULL_CODE] = "PERT";

const char PERT_H     [NULL_CODE] = "H";   /* Half */
const char PERT_M     [NULL_CODE] = "M";   /* Month Ending */
const char PERT_W     [NULL_CODE] = "W";   /* Week Ending */


/* PEST = Promotion Extract Status */
const char PEST[NULL_CODE] = "PEST";

const char PEST_E     [NULL_CODE] = "E";   /* Extracted */
const char PEST_M     [NULL_CODE] = "M";   /* Promotion Complete */


/* PETP = Item Price Event Type */
const char PETP[NULL_CODE] = "PETP";

const char PETP_REG   [NULL_CODE] = "REG";   /* Regular Price Change */
const char PETP_CLEAR [NULL_CODE] = "CLEAR";   /* Clearance Event */
const char PETP_COST  [NULL_CODE] = "COST";   /* Cost Change */


/* PEZT = Zone Price Event Type */
const char PEZT[NULL_CODE] = "PEZT";

const char PEZT_REG   [NULL_CODE] = "REG";   /* Regular Price Change */
const char PEZT_CLEAR [NULL_CODE] = "CLEAR";   /* Clearance Event */


/* PFTP = Profit Calculation Type */
const char PFTP[NULL_CODE] = "PFTP";

const char PFTP_1     [NULL_CODE] = "1";   /* Direct Cost */
const char PFTP_2     [NULL_CODE] = "2";   /* Retail Inventory */


/* PITL = Pack Item Level Descriptions */
const char PITL[NULL_CODE] = "PITL";

const char PITL_1     [NULL_CODE] = "1";   /* Pack */
const char PITL_2     [NULL_CODE] = "2";   /* Reference Item */


/* PKMT = Packing Method */
const char PKMT[NULL_CODE] = "PKMT";

const char PKMT_HANG  [NULL_CODE] = "HANG";   /* Hanging */
const char PKMT_FLAT  [NULL_CODE] = "FLAT";   /* Flat */


/* PLS1 = Pricing Level Select Type 1 */
const char PLS1[NULL_CODE] = "PLS1";

const char PLS1_S     [NULL_CODE] = "S";   /* Store */
const char PLS1_O     [NULL_CODE] = "O";   /* All Stores */


/* PLST = Pricing Level Select Type */
const char PLST[NULL_CODE] = "PLST";

const char PLST_Z     [NULL_CODE] = "Z";   /* Zone */
const char PLST_A     [NULL_CODE] = "A";   /* All Zone */


/* PMBT = Promotion Mix Match Buy Type */
const char PMBT[NULL_CODE] = "PMBT";

const char PMBT_A     [NULL_CODE] = "A";   /* All */
const char PMBT_U     [NULL_CODE] = "U";   /* Any */


/* PMGT = Promotion Mix Match Get Type */
const char PMGT[NULL_CODE] = "PMGT";

const char PMGT_P     [NULL_CODE] = "P";   /* Percent Off */
const char PMGT_A     [NULL_CODE] = "A";   /* Amount Off */
const char PMGT_F     [NULL_CODE] = "F";   /* Fixed Amount */


/* POPS = Proof of Performance Search */
const char POPS[NULL_CODE] = "POPS";

const char POPS_D     [NULL_CODE] = "D";   /* Deal */
const char POPS_DD    [NULL_CODE] = "DD";   /* Deal Detail/Component */
const char POPS_DIL   [NULL_CODE] = "DIL";   /* Deal Item/Location */
const char POPS_I     [NULL_CODE] = "I";   /* Item */
const char POPS_L     [NULL_CODE] = "L";   /* Location */


/* POPT = PO Print Type */
const char POPT[NULL_CODE] = "POPT";

const char POPT_A     [NULL_CODE] = "A";   /* On Approval */
const char POPT_R     [NULL_CODE] = "R";   /* On Receipt */


/* POSC = POS Configuration Types */
const char POSC[NULL_CODE] = "POSC";

const char POSC_COUP  [NULL_CODE] = "COUP";   /* Coupon */
const char POSC_PRES  [NULL_CODE] = "PRES";   /* Product Restriction */


/* POSL = POS Locations Types */
const char POSL[NULL_CODE] = "POSL";

const char POSL_A     [NULL_CODE] = "A";   /* @OH3@ */
const char POSL_R     [NULL_CODE] = "R";   /* @OH4@ */
const char POSL_D     [NULL_CODE] = "D";   /* @OH5@ */
const char POSL_S     [NULL_CODE] = "S";   /* Store */
const char POSL_AS    [NULL_CODE] = "AS";   /* All Stores */
const char POSL_LL    [NULL_CODE] = "LL";   /* Location List */


/* POSM = POS Upload Transaction Types */
const char POSM[NULL_CODE] = "POSM";

const char POSM_1     [NULL_CODE] = "1";   /* New Item Added */
const char POSM_2     [NULL_CODE] = "2";   /* Added Child to Item */
const char POSM_10    [NULL_CODE] = "10";   /* Change Short Desc. of Existing Item */
const char POSM_11    [NULL_CODE] = "11";   /* Change Price of an Existing Item */
const char POSM_12    [NULL_CODE] = "12";   /* Change Description of an Existing Item */
const char POSM_13    [NULL_CODE] = "13";   /* Change @MH4@/@MH5@/@MH6@ of Existing Item */
const char POSM_16    [NULL_CODE] = "16";   /* Put Item on Clearance */
const char POSM_17    [NULL_CODE] = "17";   /* Change Existing Item Clearance Price */
const char POSM_18    [NULL_CODE] = "18";   /* Remove Item from Clearance and Reset */
const char POSM_21    [NULL_CODE] = "21";   /* Delete Existing Item */
const char POSM_22    [NULL_CODE] = "22";   /* Delete Child from Item */
const char POSM_25    [NULL_CODE] = "25";   /* Change Item Status */
const char POSM_26    [NULL_CODE] = "26";   /* Change Item Taxable Indicator */
const char POSM_31    [NULL_CODE] = "31";   /* Promotional Item - Start Maintenance */
const char POSM_32    [NULL_CODE] = "32";   /* Promotional Item - End Maintenance */


/* POST = Sales Audit POS types */
const char POST[NULL_CODE] = "POST";

const char POST_1     [NULL_CODE] = "1";   /* POS type 1 */
const char POST_2     [NULL_CODE] = "2";   /* POS type 2 */


/* PPSS = Promotion Status for Item/Store */
const char PPSS[NULL_CODE] = "PPSS";

const char PPSS_PC    [NULL_CODE] = "PC";   /* Price Change */
const char PPSS_AI    [NULL_CODE] = "AI";   /* Added Item */
const char PPSS_DI    [NULL_CODE] = "DI";   /* Deleted Item */
const char PPSS_DP    [NULL_CODE] = "DP";   /* Delete Processed */


/* PPT = Proof of Performance Types */
const char PPT[NULL_CODE] = "PPT ";

const char PPT_ECD   [NULL_CODE] = "ECD";   /* End Cap Display */
const char PPT_NAD   [NULL_CODE] = "NAD";   /* Newspaper Ad */
const char PPT_CAD   [NULL_CODE] = "CAD";   /* In Store Circular Ad */
const char PPT_RAD   [NULL_CODE] = "RAD";   /* Radio Ad */
const char PPT_TASTE [NULL_CODE] = "TASTE";   /* Tasting/In Store Demonstration */
const char PPT_PROMO [NULL_CODE] = "PROMO";   /* Promotion */
const char PPT_COUP  [NULL_CODE] = "COUP";   /* Coupon */
const char PPT_OTHER [NULL_CODE] = "OTHER";   /* Other */


/* PPTP = Prepack Type */
const char PPTP[NULL_CODE] = "PPTP";

const char PPTP_S     [NULL_CODE] = "S";   /* Standard */
const char PPTP_N     [NULL_CODE] = "N";   /* Non-Standard */


/* PPVT = Proof of Performance Type Values */
const char PPVT[NULL_CODE] = "PPVT";

const char PPVT_DAYS  [NULL_CODE] = "DAYS";   /* Day(s) */
const char PPVT_WEEKS [NULL_CODE] = "WEEKS";   /* Week(s) */
const char PPVT_MONTH [NULL_CODE] = "MONTH";   /* Month(s) */
const char PPVT_INST  [NULL_CODE] = "INST";   /* Instances */


/* PRAT = Promotion Adjust Types */
const char PRAT[NULL_CODE] = "PRAT";

const char PRAT_RO    [NULL_CODE] = "RO";   /* Round */
const char PRAT_EI    [NULL_CODE] = "EI";   /* Ends In */
const char PRAT_PP    [NULL_CODE] = "PP";   /* Price Point */
const char PRAT_NO    [NULL_CODE] = "NO";   /* None */


/* PRCO = Customs Entry Protest Codes */
const char PRCO[NULL_CODE] = "PRCO";

const char PRCO_1     [NULL_CODE] = "1";   /* Protest Code 1 */
const char PRCO_2     [NULL_CODE] = "2";   /* Protest Code 2 */
const char PRCO_3     [NULL_CODE] = "3";   /* Protest Code 3 */


/* PREF = Prefix of Variable Weight PLU Item Number */
const char PREF[NULL_CODE] = "PREF";

const char PREF_2     [NULL_CODE] = "2";   /* 2 */
const char PREF_20    [NULL_CODE] = "20";   /* 20 */
const char PREF_21    [NULL_CODE] = "21";   /* 21 */
const char PREF_22    [NULL_CODE] = "22";   /* 22 */
const char PREF_23    [NULL_CODE] = "23";   /* 23 */
const char PREF_24    [NULL_CODE] = "24";   /* 24 */
const char PREF_25    [NULL_CODE] = "25";   /* 25 */
const char PREF_26    [NULL_CODE] = "26";   /* 26 */
const char PREF_27    [NULL_CODE] = "27";   /* 27 */
const char PREF_28    [NULL_CODE] = "28";   /* 28 */
const char PREF_29    [NULL_CODE] = "29";   /* 29 */


/* PREM = Promotion Item Type */
const char PREM[NULL_CODE] = "PREM";

const char PREM_R     [NULL_CODE] = "R";   /* Regular Price Items */
const char PREM_C     [NULL_CODE] = "C";   /* Clearance Price Items */
const char PREM_B     [NULL_CODE] = "B";   /* Reg and Clear Price Items */


/* PRES = Product Restrictions */
const char PRES[NULL_CODE] = "PRES";

const char PRES_STMP  [NULL_CODE] = "STMP";   /* Food Stamp */
const char PRES_MNAG  [NULL_CODE] = "MNAG";   /* Minimum Age */
const char PRES_CNDP  [NULL_CODE] = "CNDP";   /* Container Deposit */
const char PRES_CNVL  [NULL_CODE] = "CNVL";   /* Container Redemption Value */
const char PRES_DTDR  [NULL_CODE] = "DTDR";   /* Day/Time/Date */
const char PRES_TENT  [NULL_CODE] = "TENT";   /* Tender Type */
const char PRES_NDSC  [NULL_CODE] = "NDSC";   /* Non-Discountable */
const char PRES_RTRN  [NULL_CODE] = "RTRN";   /* Returnable */
const char PRES_QLMT  [NULL_CODE] = "QLMT";   /* Quantity Limit */


/* PRET = Product Restriction Report Type */
const char PRET[NULL_CODE] = "PRET";

const char PRET_C     [NULL_CODE] = "C";   /* Merchandise Criteria */
const char PRET_S     [NULL_CODE] = "S";   /* Associated Stores */
const char PRET_I     [NULL_CODE] = "I";   /* Associated Items */


/* PRFT = Profile Types */
const char PRFT[NULL_CODE] = "PRFT";

const char PRFT_Z     [NULL_CODE] = "Z";   /* Zone */
const char PRFT_C     [NULL_CODE] = "C";   /* Country */


/* PRMI = Item types for promotion */
const char PRMI[NULL_CODE] = "PRMI";

const char PRMI_R     [NULL_CODE] = "R";   /* Regular */
const char PRMI_C     [NULL_CODE] = "C";   /* Clearance */
const char PRMI_B     [NULL_CODE] = "B";   /* Both Regular and Clearance */


/* PRMP = Promotion Parameters */
const char PRMP[NULL_CODE] = "PRMP";

const char PRMP_LIST  [NULL_CODE] = "LIST";   /* LIST */


/* PRMT = Promotion Types */
const char PRMT[NULL_CODE] = "PRMT";

const char PRMT_9999  [NULL_CODE] = "9999";   /* RPM Promotion */
const char PRMT_1004  [NULL_CODE] = "1004";   /* In Store Discount */
const char PRMT_1005  [NULL_CODE] = "1005";   /* Employee Discount */
const char PRMT_2000  [NULL_CODE] = "2000";   /* Non-RPM Promotion */


/* PROM = Promotion Types */
const char PROM[NULL_CODE] = "PROM";

const char PROM_SK    [NULL_CODE] = "SK";   /* Item Level */
const char PROM_DP    [NULL_CODE] = "DP";   /* @MH4@ Level */
const char PROM_TH    [NULL_CODE] = "TH";   /* Threshold */
const char PROM_MB    [NULL_CODE] = "MB";   /* Mix/Match Buy */
const char PROM_MG    [NULL_CODE] = "MG";   /* Mix/Match Get */
const char PROM_MU    [NULL_CODE] = "MU";   /* Multi multi-units */


/* PRPG = Promotions Price Change Type */
const char PRPG[NULL_CODE] = "PRPG";

const char PRPG_PC    [NULL_CODE] = "PC";   /* Percent Off */
const char PRPG_MA    [NULL_CODE] = "MA";   /* Amount Off */
const char PRPG_NP    [NULL_CODE] = "NP";   /* Fixed Price */
const char PRPG_NC    [NULL_CODE] = "NC";   /* No Change */
const char PRPG_EX    [NULL_CODE] = "EX";   /* Exclude */


/* PRST = Promotion Status */
const char PRST[NULL_CODE] = "PRST";

const char PRST_W     [NULL_CODE] = "W";   /* Worksheet */
const char PRST_S     [NULL_CODE] = "S";   /* Submitted */
const char PRST_A     [NULL_CODE] = "A";   /* Approved */
const char PRST_R     [NULL_CODE] = "R";   /* Rejected */
const char PRST_E     [NULL_CODE] = "E";   /* Extracted */
const char PRST_M     [NULL_CODE] = "M";   /* Completed */
const char PRST_C     [NULL_CODE] = "C";   /* Cancelled */
const char PRST_D     [NULL_CODE] = "D";   /* Deleted */
const char PRST_I     [NULL_CODE] = "I";   /* Submit In Progress */
const char PRST_P     [NULL_CODE] = "P";   /* Approve In Progress */


/* PRTP = Consignment Purchase Indicator */
const char PRTP[NULL_CODE] = "PRTP";

const char PRTP_0     [NULL_CODE] = "0";   /* Normal Merchandise */
const char PRTP_1     [NULL_CODE] = "1";   /* Consignment Merchandise */
const char PRTP_2     [NULL_CODE] = "2";   /* Concession Stock */


/* PRTY = Promotion Group Type */
const char PRTY[NULL_CODE] = "PRTY";

const char PRTY_C     [NULL_CODE] = "C";   /* Store Class */
const char PRTY_D     [NULL_CODE] = "D";   /* @OH5@ */
const char PRTY_R     [NULL_CODE] = "R";   /* @OH4@ */
const char PRTY_S     [NULL_CODE] = "S";   /* Store */
const char PRTY_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char PRTY_L     [NULL_CODE] = "L";   /* Location Traits */
const char PRTY_A     [NULL_CODE] = "A";   /* All Stores */
const char PRTY_Y     [NULL_CODE] = "Y";   /* Country */
const char PRTY_LL    [NULL_CODE] = "LL";   /* Location List */


/* PRUM = Purchase Unit of Measure */
const char PRUM[NULL_CODE] = "PRUM";

const char PRUM_EA    [NULL_CODE] = "EA";   /* Eaches */
const char PRUM_PA    [NULL_CODE] = "PA";   /* Pairs */


/* PSAD = Promsku Adjust Type */
const char PSAD[NULL_CODE] = "PSAD";

const char PSAD_NO    [NULL_CODE] = "NO";   /* None */
const char PSAD_EI    [NULL_CODE] = "EI";   /* Ends In */


/* PSEC = Product Security Areas */
const char PSEC[NULL_CODE] = "PSEC";

const char PSEC_PALL  [NULL_CODE] = "PALL";   /* All */
const char PSEC_PPRC  [NULL_CODE] = "PPRC";   /* Pricing */
const char PSEC_PCST  [NULL_CODE] = "PCST";   /* Costing */
const char PSEC_PPRM  [NULL_CODE] = "PPRM";   /* Promotion */
const char PSEC_PCLR  [NULL_CODE] = "PCLR";   /* Clearance */
const char PSEC_PTXF  [NULL_CODE] = "PTXF";   /* Transfer */
const char PSEC_PALCFR[NULL_CODE] = "PALCFR";   /* Allocations */
const char PSEC_PPO   [NULL_CODE] = "PPO";   /* Orders */
const char PSEC_PSTK  [NULL_CODE] = "PSTK";   /* Stock Counts */


/* PSPG = Promsku Price Change Type */
const char PSPG[NULL_CODE] = "PSPG";

const char PSPG_PC    [NULL_CODE] = "PC";   /* Percent Off */
const char PSPG_MA    [NULL_CODE] = "MA";   /* Amount Off */
const char PSPG_NP    [NULL_CODE] = "NP";   /* Set Price to an Amount */


/* PSUC = Projected Promotional Sales Calc Method */
const char PSUC[NULL_CODE] = "PSUC";

const char PSUC_T     [NULL_CODE] = "T";   /* Turns */
const char PSUC_S     [NULL_CODE] = "S";   /* Sell-through */
const char PSUC_F     [NULL_CODE] = "F";   /* Forecasted Sales */
const char PSUC_P     [NULL_CODE] = "P";   /* Percent of Projected SOH */


/* PTAL = All Partner Types */
const char PTAL[NULL_CODE] = "PTAL";

const char PTAL_BK    [NULL_CODE] = "BK";   /* Bank */
const char PTAL_AG    [NULL_CODE] = "AG";   /* Agent */
const char PTAL_FF    [NULL_CODE] = "FF";   /* Freight Forwarder */
const char PTAL_IM    [NULL_CODE] = "IM";   /* Importer */
const char PTAL_BR    [NULL_CODE] = "BR";   /* Broker */
const char PTAL_FA    [NULL_CODE] = "FA";   /* Factory */
const char PTAL_CO    [NULL_CODE] = "CO";   /* Consolidator */
const char PTAL_AP    [NULL_CODE] = "AP";   /* Applicant */
const char PTAL_CN    [NULL_CODE] = "CN";   /* Consignee */
const char PTAL_S1    [NULL_CODE] = "S1";   /* @SUH1@ */
const char PTAL_S2    [NULL_CODE] = "S2";   /* @SUH2@ */
const char PTAL_S3    [NULL_CODE] = "S3";   /* @SUH3@ */
const char PTAL_EV    [NULL_CODE] = "EV";   /* Expense Vendor */
const char PTAL_IA    [NULL_CODE] = "IA";   /* Import Authority */
const char PTAL_ES    [NULL_CODE] = "ES";   /* Escheat - State */
const char PTAL_EC    [NULL_CODE] = "EC";   /* Escheat - Country */
const char PTAL_E     [NULL_CODE] = "E";   /* External Finisher */
const char PTAL_S     [NULL_CODE] = "S";   /* Supplier */


/* PTAT = Promotion Threshold Apply To */
const char PTAT[NULL_CODE] = "PTAT";

const char PTAT_C     [NULL_CODE] = "C";   /* Compound */
const char PTAT_E     [NULL_CODE] = "E";   /* Exclusive */


/* PTDT = Promotion Threshold Discount Type */
const char PTDT[NULL_CODE] = "PTDT";

const char PTDT_P     [NULL_CODE] = "P";   /* Percent Off */
const char PTDT_A     [NULL_CODE] = "A";   /* Amount Off */
const char PTDT_F     [NULL_CODE] = "F";   /* Fixed Amount */


/* PTNR = Partner Types */
const char PTNR[NULL_CODE] = "PTNR";

const char PTNR_AG    [NULL_CODE] = "AG";   /* Agent */
const char PTNR_AP    [NULL_CODE] = "AP";   /* Applicant */
const char PTNR_BK    [NULL_CODE] = "BK";   /* Bank */
const char PTNR_BR    [NULL_CODE] = "BR";   /* Broker */
const char PTNR_CN    [NULL_CODE] = "CN";   /* Consignee */
const char PTNR_CO    [NULL_CODE] = "CO";   /* Consolidator */
const char PTNR_E     [NULL_CODE] = "E";   /* External Finisher */
const char PTNR_EC    [NULL_CODE] = "EC";   /* Escheat-Country */
const char PTNR_ES    [NULL_CODE] = "ES";   /* Escheat-State */
const char PTNR_EV    [NULL_CODE] = "EV";   /* EV */
const char PTNR_FA    [NULL_CODE] = "FA";   /* Factory */
const char PTNR_FF    [NULL_CODE] = "FF";   /* Freight Forwarder */
const char PTNR_IA    [NULL_CODE] = "IA";   /* IA */
const char PTNR_IM    [NULL_CODE] = "IM";   /* Importer */
const char PTNR_S1    [NULL_CODE] = "S1";   /* @SUH1@ */
const char PTNR_S2    [NULL_CODE] = "S2";   /* @SUH2@ */
const char PTNR_S3    [NULL_CODE] = "S3";   /* @SUH3@ */


/* PTPE = Processing Type */
const char PTPE[NULL_CODE] = "PTPE";

const char PTPE_X     [NULL_CODE] = "X";   /* Cross Dock */
const char PTPE_C     [NULL_CODE] = "C";   /* Consolidation */


/* PTTC = Process Tracker Template Category. */
const char PTTC[NULL_CODE] = "PTTC";

const char PTTC_IS9M  [NULL_CODE] = "IS9M";   /* Items */
const char PTTC_IS9C  [NULL_CODE] = "IS9C";   /* Cost Changes */
const char PTTC_PODT  [NULL_CODE] = "PODT";   /* Purchase Orders */
const char PTTC_OTHR  [NULL_CODE] = "OTHR";   /* Foundation */


/* PTTY = Promotion Threshold Type */
const char PTTY[NULL_CODE] = "PTTY";

const char PTTY_A     [NULL_CODE] = "A";   /* Amount */
const char PTTY_U     [NULL_CODE] = "U";   /* Unit */


/* PTYP = Processor Type */
const char PTYP[NULL_CODE] = "PTYP";

const char PTYP_SEARS [NULL_CODE] = "SEARS";   /* Sears */
const char PTYP_NPC   [NULL_CODE] = "NPC";   /* NPC */
const char PTYP_NONE  [NULL_CODE] = "NONE";   /* None */


/* PURT = Purchase Types */
const char PURT[NULL_CODE] = "PURT";

const char PURT_DLV   [NULL_CODE] = "DLV";   /* Delivered */
const char PURT_FOB   [NULL_CODE] = "FOB";   /* Free on Board */
const char PURT_BACK  [NULL_CODE] = "BACK";   /* Backhaul */
const char PURT_PUP   [NULL_CODE] = "PUP";   /* Pick-up */


/* PXS1 = Price Change Status (no reject) */
const char PXS1[NULL_CODE] = "PXS1";

const char PXS1_W     [NULL_CODE] = "W";   /* Worksheet */
const char PXS1_S     [NULL_CODE] = "S";   /* Submitted */
const char PXS1_A     [NULL_CODE] = "A";   /* Approved */
const char PXS1_E     [NULL_CODE] = "E";   /* Extracted */
const char PXS1_C     [NULL_CODE] = "C";   /* Cancelled */
const char PXS1_D     [NULL_CODE] = "D";   /* Deleted */


/* PXST = Price Change Status */
const char PXST[NULL_CODE] = "PXST";

const char PXST_W     [NULL_CODE] = "W";   /* Worksheet */
const char PXST_S     [NULL_CODE] = "S";   /* Submitted */
const char PXST_A     [NULL_CODE] = "A";   /* Approved */
const char PXST_R     [NULL_CODE] = "R";   /* Rejected */
const char PXST_E     [NULL_CODE] = "E";   /* Extracted */
const char PXST_C     [NULL_CODE] = "C";   /* Cancelled */
const char PXST_D     [NULL_CODE] = "D";   /* Deleted */


/* PYMT = Payment Method */
const char PYMT[NULL_CODE] = "PYMT";

const char PYMT_LC    [NULL_CODE] = "LC";   /* Letter of Credit */
const char PYMT_OA    [NULL_CODE] = "OA";   /* Open Account */
const char PYMT_WT    [NULL_CODE] = "WT";   /* Wire Transfer */


/* PYTP = Payment Method */
const char PYTP[NULL_CODE] = "PYTP";

const char PYTP_1     [NULL_CODE] = "1";   /* Cash */
const char PYTP_2     [NULL_CODE] = "2";   /* Check */
const char PYTP_3     [NULL_CODE] = "3";   /* Credit */


/* PZG = Pricing Zone Group Label */
const char PZG[NULL_CODE] = "PZG ";

const char PZG_PZG   [NULL_CODE] = "PZG";   /* Pricing Zone Group */


/* PZLB = Pricing Zone Labels */
const char PZLB[NULL_CODE] = "PZLB";

const char PZLB_S     [NULL_CODE] = "S";   /* Store */
const char PZLB_Z     [NULL_CODE] = "Z";   /* Zone */
const char PZLB_C     [NULL_CODE] = "C";   /* Corp. */


/* PZP1 = Pricing Zone Group Level 1 */
const char PZP1[NULL_CODE] = "PZP1";

const char PZP1_A     [NULL_CODE] = "A";   /* All Levels */


/* PZPL = Zone Group Pricing Level */
const char PZPL[NULL_CODE] = "PZPL";

const char PZPL_C     [NULL_CODE] = "C";   /* Corporate */
const char PZPL_Z     [NULL_CODE] = "Z";   /* Zone */
const char PZPL_S     [NULL_CODE] = "S";   /* Store */


/* QCIR = Quality Inspection Result */
const char QCIR[NULL_CODE] = "QCIR";

const char QCIR_P     [NULL_CODE] = "P";   /* Pass */
const char QCIR_F     [NULL_CODE] = "F";   /* Fail */


/* QCPC = QC Percentage Amount */
const char QCPC[NULL_CODE] = "QCPC";

const char QCPC_P     [NULL_CODE] = "P";   /* 10 */


/* QCST = Quality Control Status */
const char QCST[NULL_CODE] = "QCST";

const char QCST_R     [NULL_CODE] = "R";   /* Required */
const char QCST_P     [NULL_CODE] = "P";   /* Passed */
const char QCST_F     [NULL_CODE] = "F";   /* Failed */


/* QLBL = Quantity Labels */
const char QLBL[NULL_CODE] = "QLBL";

const char QLBL_P     [NULL_CODE] = "P";   /* Pct. */
const char QLBL_R     [NULL_CODE] = "R";   /* Ratio */


/* QTYP = MRT Quantity Type */
const char QTYP[NULL_CODE] = "QTYP";

const char QTYP_A     [NULL_CODE] = "A";   /* All Inventory */
const char QTYP_M     [NULL_CODE] = "M";   /* Manual */


/* QUAN = Label Quantity */
const char QUAN[NULL_CODE] = "QUAN";

const char QUAN_Q     [NULL_CODE] = "Q";   /* Quantity */


/* QYTP = Transfer Quantity Type (Manual, SOH) */
const char QYTP[NULL_CODE] = "QYTP";

const char QYTP_M     [NULL_CODE] = "M";   /* Manual */
const char QYTP_SOH   [NULL_CODE] = "SOH";   /* Total Stock on Hand */


/* RACK = Rack Sizes */
const char RACK[NULL_CODE] = "RACK";

const char RACK_S     [NULL_CODE] = "S";   /* Small */
const char RACK_M     [NULL_CODE] = "M";   /* Medium */
const char RACK_L     [NULL_CODE] = "L";   /* Large */


/* RAS = OI Report Option Region/Area/Store */
const char RAS[NULL_CODE] = "RAS ";

const char RAS_30    [NULL_CODE] = "30";   /* Region */
const char RAS_40    [NULL_CODE] = "40";   /* Area */
const char RAS_50    [NULL_CODE] = "50";   /* Store */


/* RASK = Ragskadd Labels */
const char RASK[NULL_CODE] = "RASK";

const char RASK_ON    [NULL_CODE] = "ON";   /* Order Number */
const char RASK_D     [NULL_CODE] = "D";   /* Distribution */
const char RASK_E     [NULL_CODE] = "E";   /* End Item Parent */
const char RASK_C     [NULL_CODE] = "C";   /* Contract Number */
const char RASK_OK    [NULL_CODE] = "OK";   /* OK */
const char RASK_CAN   [NULL_CODE] = "CAN";   /* Cancel */


/* RCAD = RCA Cost Change Description */
const char RCAD[NULL_CODE] = "RCAD";

const char RCAD_RCAA  [NULL_CODE] = "RCAA";   /* Receiver Cost Adjustment Cost Change */


/* RCCQ = Reclass and Cost Change Queue Code Types */
const char RCCQ[NULL_CODE] = "RCCQ";

const char RCCQ_C     [NULL_CODE] = "C";   /* Cost Change */
const char RCCQ_R     [NULL_CODE] = "R";   /* Reclassification */
const char RCCQ_N     [NULL_CODE] = "N";   /* New item location */


/* RCER = Tran Data Adjustment */
const char RCER[NULL_CODE] = "RCER";

const char RCER_U     [NULL_CODE] = "U";   /* Units */
const char RCER_C     [NULL_CODE] = "C";   /* Cost */
const char RCER_R     [NULL_CODE] = "R";   /* Retail */
const char RCER_A     [NULL_CODE] = "A";   /* ALC/adjustments */


/* RCLA = Reclass Maintenance Codes */
const char RCLA[NULL_CODE] = "RCLA";

const char RCLA_A     [NULL_CODE] = "A";   /* Addition */
const char RCLA_M     [NULL_CODE] = "M";   /* Modification */


/* RCLH = Reclass Hierarchy Codes */
const char RCLH[NULL_CODE] = "RCLH";

const char RCLH_M     [NULL_CODE] = "M";   /* @OH1@ */
const char RCLH_V     [NULL_CODE] = "V";   /* @MH2@ */
const char RCLH_G     [NULL_CODE] = "G";   /* @MH3@ */
const char RCLH_D     [NULL_CODE] = "D";   /* @MH4@ */
const char RCLH_C     [NULL_CODE] = "C";   /* @MH5@ */
const char RCLH_S     [NULL_CODE] = "S";   /* @MH6@ */


/* RCST = Receiving Item Type */
const char RCST[NULL_CODE] = "RCST";

const char RCST_S     [NULL_CODE] = "S";   /* Item */
const char RCST_U     [NULL_CODE] = "U";   /* Child Item */
const char RCST_V     [NULL_CODE] = "V";   /* VPN */
const char RCST_R     [NULL_CODE] = "R";   /* Ref Item */


/* RDTP = Appointment Document Types */
const char RDTP[NULL_CODE] = "RDTP";

const char RDTP_P     [NULL_CODE] = "P";   /* Purchase Order */
const char RDTP_T     [NULL_CODE] = "T";   /* Transfer */
const char RDTP_A     [NULL_CODE] = "A";   /* Allocation */


/* REAC = Sales Audit Reason Codes */
const char REAC[NULL_CODE] = "REAC";

const char REAC_ERR   [NULL_CODE] = "ERR";   /* Error */
const char REAC_ACCT  [NULL_CODE] = "ACCT";   /* Payment on Account */
const char REAC_INC   [NULL_CODE] = "INC";   /* Incident Payout */
const char REAC_NSF   [NULL_CODE] = "NSF";   /* NSF Check Payment */
const char REAC_PAYRL [NULL_CODE] = "PAYRL";   /* Payroll Payout */
const char REAC_LOTWIN[NULL_CODE] = "LOTWIN";   /* Lottery Winnings */
const char REAC_TPIVMR[NULL_CODE] = "TPIVMR";   /* Till Pay In Vending Machine Revenue */
const char REAC_TPIMSC[NULL_CODE] = "TPIMSC";   /* Till Pay In Miscellaneous */
const char REAC_TPOP  [NULL_CODE] = "TPOP";   /* Till Pay Out Postage */
const char REAC_TPOS  [NULL_CODE] = "TPOS";   /* Till Pay Out Supplies */
const char REAC_TPOE  [NULL_CODE] = "TPOE";   /* Till Pay Out Entertainment */
const char REAC_NSCC  [NULL_CODE] = "NSCC";   /* No Sale Customer Change */
const char REAC_NSCFR [NULL_CODE] = "NSCFR";   /* No Sale Change For Register */
const char REAC_PVIP  [NULL_CODE] = "PVIP";   /* Post Void Incorrect Price */
const char REAC_PVDI  [NULL_CODE] = "PVDI";   /* Post Void Discount Incorrect */
const char REAC_PVCCM [NULL_CODE] = "PVCCM";   /* Post Void Customer Changed Mind */
const char REAC_PVAE  [NULL_CODE] = "PVAE";   /* Post Void Associate Error */
const char REAC_PVOFP [NULL_CODE] = "PVOFP";   /* Post Void Other Form Payment */
const char REAC_PVO   [NULL_CODE] = "PVO ";   /* Post Void Other */
const char REAC_HOUSE [NULL_CODE] = "HOUSE";   /* House Payment */
const char REAC_LAYINT[NULL_CODE] = "LAYINT";   /* Layaway Initiate */
const char REAC_LAYPAY[NULL_CODE] = "LAYPAY";   /* Layaway Payment */
const char REAC_ORDINT[NULL_CODE] = "ORDINT";   /* Order Initiate */
const char REAC_LAYDEL[NULL_CODE] = "LAYDEL";   /* Layaway Delete */
const char REAC_ORDCAN[NULL_CODE] = "ORDCAN";   /* Order Cancel */
const char REAC_COUPN [NULL_CODE] = "COUPN";   /* Coupon */
const char REAC_FONCT [NULL_CODE] = "FONCT";   /* Fonacot */
const char REAC_BLPYMT[NULL_CODE] = "BLPYMT";   /* Telephone - Mobile Bill Payment */
const char REAC_PV1   [NULL_CODE] = "PV1";   /* Cashier Error */
const char REAC_PV2   [NULL_CODE] = "PV2";   /* Supervisors Discretion */
const char REAC_PV3   [NULL_CODE] = "PV3";   /* Customer Satisfaction */
const char REAC_NS1   [NULL_CODE] = "NS1";   /* Making Change */
const char REAC_NS2   [NULL_CODE] = "NS2";   /* Employee Check Cashed */
const char REAC_NS3   [NULL_CODE] = "NS3";   /* Petty Cash In */
const char REAC_NS4   [NULL_CODE] = "NS4";   /* Petty Cash Out */
const char REAC_NS5   [NULL_CODE] = "NS5";   /* Spiff/Bonus Out 1 */
const char REAC_CF1   [NULL_CODE] = "CF1";   /* Register Down 1 */
const char REAC_CF2   [NULL_CODE] = "CF2";   /* Holiday Adjustment */
const char REAC_PI1   [NULL_CODE] = "PI1";   /* Change from Paid Out */
const char REAC_PI2   [NULL_CODE] = "PI2";   /* Found Money */
const char REAC_PI3   [NULL_CODE] = "PI3";   /* Drawer Loan 1 */
const char REAC_PO1   [NULL_CODE] = "PO1";   /* Stocks */
const char REAC_P02   [NULL_CODE] = "P02  ";   /* Delivery */
const char REAC_PO3   [NULL_CODE] = "PO3";   /* Postage */
const char REAC_PO4   [NULL_CODE] = "PO4";   /* Contractor Services */
const char REAC_PO5   [NULL_CODE] = "PO5";   /* Store Incentives */
const char REAC_TENDEX[NULL_CODE] = "TENDEX";   /* Tender Exchange */


/* RECL = Reclassification Privilege */
const char RECL[NULL_CODE] = "RECL";

const char RECL_1001  [NULL_CODE] = "1001";   /* Friemann */
const char RECL_4401  [NULL_CODE] = "4401";   /* Mike's Super Group */
const char RECL_2200  [NULL_CODE] = "2200";   /* eph */
const char RECL_4700  [NULL_CODE] = "4700";   /* eph */


/* REFL = Sales Audit Reference Labels */
const char REFL[NULL_CODE] = "REFL";

const char REFL_1     [NULL_CODE] = "1";   /* Reference No. 1 */
const char REFL_2     [NULL_CODE] = "2";   /* Reference No. 2 */
const char REFL_3     [NULL_CODE] = "3";   /* Reference No. 3 */
const char REFL_4     [NULL_CODE] = "4";   /* Reference No. 4 */
const char REFL_5     [NULL_CODE] = "5";   /* Reference No. 5 */
const char REFL_6     [NULL_CODE] = "6";   /* Reference No. 6 */
const char REFL_7     [NULL_CODE] = "7";   /* Reference No. 7 */
const char REFL_8     [NULL_CODE] = "8";   /* Reference No. 8 */
const char REFL_9     [NULL_CODE] = "9";   /* Reference No. 9 */
const char REFL_10    [NULL_CODE] = "10";   /* Reference No. 10 */
const char REFL_11    [NULL_CODE] = "11";   /* Reference No. 11 */
const char REFL_12    [NULL_CODE] = "12";   /* Reference No. 12 */
const char REFL_13    [NULL_CODE] = "13";   /* Reference No. 13 */
const char REFL_14    [NULL_CODE] = "14";   /* Reference No. 14 */
const char REFL_15    [NULL_CODE] = "15";   /* Reference No. 15 */
const char REFL_16    [NULL_CODE] = "16";   /* Reference No. 16 */
const char REFL_S     [NULL_CODE] = "S";   /* Supplier */
const char REFL_M     [NULL_CODE] = "M";   /* Money Order No. */
const char REFL_C     [NULL_CODE] = "C";   /* Check No. */
const char REFL_SP    [NULL_CODE] = "SP";   /* Special Order No. */
const char REFL_G     [NULL_CODE] = "G";   /* Gift Cert. Serial No. */
const char REFL_P     [NULL_CODE] = "P";   /* Personal ID No. */
const char REFL_I     [NULL_CODE] = "I";   /* Incident No. */
const char REFL_IT    [NULL_CODE] = "IT";   /* Incident Type */
const char REFL_GT    [NULL_CODE] = "GT";   /* Lottery Game */
const char REFL_L     [NULL_CODE] = "L";   /* Lottery Ticket No. */
const char REFL_IV    [NULL_CODE] = "IV";   /* Invoice No. */
const char REFL_CC    [NULL_CODE] = "CC";   /* Credit Card No. */
const char REFL_D     [NULL_CODE] = "D";   /* Driver's License No. */
const char REFL_A     [NULL_CODE] = "A";   /* Account No. */
const char REFL_R     [NULL_CODE] = "R";   /* Routing No. */
const char REFL_CH    [NULL_CODE] = "CH";   /* Charity Org. No. */
const char REFL_CM    [NULL_CODE] = "CM";   /* Coin Mach. Supplier No. */
const char REFL_T     [NULL_CODE] = "T";   /* Traffic */
const char REFL_TM    [NULL_CODE] = "TM";   /* Temperature */
const char REFL_W     [NULL_CODE] = "W";   /* Weather */
const char REFL_RF    [NULL_CODE] = "RF";   /* Refund Ref. No. */
const char REFL_CD    [NULL_CODE] = "CD";   /* Coupon Discount */
const char REFL_CMR   [NULL_CODE] = "CMR";   /* Close Meter Reading */
const char REFL_CON   [NULL_CODE] = "CON";   /* Construction */
const char REFL_CT    [NULL_CODE] = "CT";   /* Coupon Tender */
const char REFL_DHJ   [NULL_CODE] = "DHJ";   /* Dip Height Major */
const char REFL_DHN   [NULL_CODE] = "DHN";   /* Dip Height Minor */
const char REFL_DT    [NULL_CODE] = "DT";   /* Dip Type */
const char REFL_E     [NULL_CODE] = "E";   /* Employee No. */
const char REFL_OMR   [NULL_CODE] = "OMR";   /* Open Meter Reading */
const char REFL_RI    [NULL_CODE] = "RI";   /* Receipt Indicator */
const char REFL_RT    [NULL_CODE] = "RT";   /* Reading Type */
const char REFL_SS    [NULL_CODE] = "SS";   /* Speed Sale No. */
const char REFL_TK    [NULL_CODE] = "TK";   /* Tank */
const char REFL_CMV   [NULL_CODE] = "CMV";   /* Close Meter Value */
const char REFL_OMV   [NULL_CODE] = "OMV";   /* Open Meter Value */
const char REFL_ED    [NULL_CODE] = "ED";   /* Escheat Date */
const char REFL_RN    [NULL_CODE] = "RN";   /* Recipient Name */
const char REFL_RS    [NULL_CODE] = "RS";   /* Recipient State */
const char REFL_RC    [NULL_CODE] = "RC";   /* Recipient Country */
const char REFL_17    [NULL_CODE] = "17";   /* Reference No. 17 */
const char REFL_18    [NULL_CODE] = "18";   /* Reference No. 18 */
const char REFL_19    [NULL_CODE] = "19";   /* Reference No. 19 */
const char REFL_20    [NULL_CODE] = "20";   /* Reference No. 20 */
const char REFL_TOTAL [NULL_CODE] = "TOTAL";   /* Total ID */
const char REFL_IFEID [NULL_CODE] = "IFEID";   /* Instituto Federal Electoral ID */
const char REFL_PRD   [NULL_CODE] = "PRD";   /* Provider Name */
const char REFL_FON   [NULL_CODE] = "FON";   /* Fonacot Number */
const char REFL_CKDATE[NULL_CODE] = "CKDATE";   /* Check Date */
const char REFL_CKDTFL[NULL_CODE] = "CKDTFL";   /* Check post dated flag */
const char REFL_21    [NULL_CODE] = "21";   /* Reference No. 21 */
const char REFL_22    [NULL_CODE] = "22";   /* Reference No. 22 */
const char REFL_23    [NULL_CODE] = "23";   /* Reference No. 23 */
const char REFL_24    [NULL_CODE] = "24";   /* Reference No. 24 */
const char REFL_25    [NULL_CODE] = "25";   /* Reference No. 25 */
const char REFL_26    [NULL_CODE] = "26";   /* Reference No. 26 */
const char REFL_27    [NULL_CODE] = "27";   /* Reference No. 27 */
const char REFL_CI    [NULL_CODE] = "CI";   /* Component Item */
const char REFL_CID   [NULL_CODE] = "CID";   /* Component Item Description */
const char REFL_FNN   [NULL_CODE] = "FNN";   /* Fiscal Note Number */
const char REFL_CACN  [NULL_CODE] = "CACN";   /* Customer Account Number */
const char REFL_CN    [NULL_CODE] = "CN";   /* Customer Name */


/* REFN = Sales Audit Reference Field Numbers */
const char REFN[NULL_CODE] = "REFN";

const char REFN_1     [NULL_CODE] = "1";   /* 1 */
const char REFN_2     [NULL_CODE] = "2";   /* 2 */
const char REFN_3     [NULL_CODE] = "3";   /* 3 */
const char REFN_4     [NULL_CODE] = "4";   /* 4 */
const char REFN_5     [NULL_CODE] = "5";   /* 5 */
const char REFN_6     [NULL_CODE] = "6";   /* 6 */
const char REFN_7     [NULL_CODE] = "7";   /* 7 */
const char REFN_8     [NULL_CODE] = "8";   /* 8 */
const char REFN_9     [NULL_CODE] = "9";   /* 9 */
const char REFN_10    [NULL_CODE] = "10";   /* 10 */
const char REFN_11    [NULL_CODE] = "11";   /* 11 */
const char REFN_12    [NULL_CODE] = "12";   /* 12 */
const char REFN_13    [NULL_CODE] = "13";   /* 13 */
const char REFN_14    [NULL_CODE] = "14";   /* 14 */
const char REFN_15    [NULL_CODE] = "15";   /* 15 */
const char REFN_16    [NULL_CODE] = "16";   /* 16 */
const char REFN_17    [NULL_CODE] = "17";   /* 17 */
const char REFN_18    [NULL_CODE] = "18";   /* 18 */
const char REFN_19    [NULL_CODE] = "19";   /* 19 */
const char REFN_20    [NULL_CODE] = "20";   /* 20 */
const char REFN_21    [NULL_CODE] = "21";   /* 21 */
const char REFN_22    [NULL_CODE] = "22";   /* 22 */
const char REFN_23    [NULL_CODE] = "23";   /* 23 */
const char REFN_24    [NULL_CODE] = "24";   /* 24 */
const char REFN_25    [NULL_CODE] = "25";   /* 25 */
const char REFN_26    [NULL_CODE] = "26";   /* 26 */
const char REFN_27    [NULL_CODE] = "27";   /* 27 */


/* RELM = SA Realm Addition Indicator */
const char RELM[NULL_CODE] = "RELM";

const char RELM_Y     [NULL_CODE] = "Y";   /* Available */
const char RELM_N     [NULL_CODE] = "N";   /* Not Available */


/* REOP = Relational Operators */
const char REOP[NULL_CODE] = "REOP";

const char REOP_EQ    [NULL_CODE] = "=";   /* = */
const char REOP_LT    [NULL_CODE] = "<";   /* < */
const char REOP_GT    [NULL_CODE] = ">";   /* > */
const char REOP_LE    [NULL_CODE] = "<=";   /* <= */
const char REOP_GE    [NULL_CODE] = ">=";   /* >= */


/* REPC = Report Code */
const char REPC[NULL_CODE] = "REPC";

const char REPC_ALL   [NULL_CODE] = "ALL";   /* All Locations Reports */
const char REPC_NO    [NULL_CODE] = "NO";   /* No Location Report */


/* RETP = Return Policy */
const char RETP[NULL_CODE] = "RETP";

const char RETP_NONE  [NULL_CODE] = "NONE";   /* Returns not allowed */
const char RETP_RCPT  [NULL_CODE] = "RCPT";   /* Receipt needed for return */
const char RETP_ALL   [NULL_CODE] = "ALL";   /* All returns accepted */
const char RETP_MNGR  [NULL_CODE] = "MNGR";   /* Manager approval needed for return */
const char RETP_ID    [NULL_CODE] = "ID";   /* Customer ID needed for return */
const char RETP_NOSTCK[NULL_CODE] = "NOSTCK";   /* Accept - do not return item to stock */


/* RETY = Error Types */
const char RETY[NULL_CODE] = "RETY";

const char RETY_BL    [NULL_CODE] = "BL";   /* Business Logic */
const char RETY_OR    [NULL_CODE] = "OR";   /* API procedure call out of sequence */
const char RETY_LK    [NULL_CODE] = "LK";   /* Record Locked */
const char RETY_OE    [NULL_CODE] = "OE";   /* Oracle Error */
const char RETY_FE    [NULL_CODE] = "FE";   /* Fatal Error */


/* RLCT = Sales Audit Rule Category */
const char RLCT[NULL_CODE] = "RLCT";

const char RLCT_ALL   [NULL_CODE] = "ALL";   /* All */
const char RLCT_POS   [NULL_CODE] = "POS";   /* POS rule */
const char RLCT_NON   [NULL_CODE] = "NON";   /* Non-POS rule */


/* RLPT = Sales Audit Rule Prompt */
const char RLPT[NULL_CODE] = "RLPT";

const char RLPT_T     [NULL_CODE] = "T";   /* Tolerance */
const char RLPT_C     [NULL_CODE] = "C";   /* Column Name */
const char RLPT_P     [NULL_CODE] = "P";   /* Parameter Name */


/* RLTP = Sales Audit Rule Types */
const char RLTP[NULL_CODE] = "RLTP";

const char RLTP_TAB   [NULL_CODE] = "TAB";   /* Table */
const char RLTP_TRN   [NULL_CODE] = "TRN";   /* Transaction */
const char RLTP_TOT   [NULL_CODE] = "TOT";   /* Totals */


/* RLVL = Warehouse Store Assignment Type */
const char RLVL[NULL_CODE] = "RLVL";


/* RM = Replenishment Methods */
const char RM[NULL_CODE] = "RM  ";

const char RM_C     [NULL_CODE] = "C";   /* Constant */
const char RM_M     [NULL_CODE] = "M";   /* Min / Max */
const char RM_F     [NULL_CODE] = "F";   /* Floating Point */
const char RM_T     [NULL_CODE] = "T";   /* Time Supply */
const char RM_E     [NULL_CODE] = "E";   /* Time Supply - Seasonal */
const char RM_TI    [NULL_CODE] = "TI";   /* Time Supply - Issues */
const char RM_D     [NULL_CODE] = "D";   /* Dynamic */
const char RM_S     [NULL_CODE] = "S";   /* Dynamic - Seasonal */
const char RM_DI    [NULL_CODE] = "DI";   /* Dynamic - Issues */
const char RM_SO    [NULL_CODE] = "SO";   /* Store Orders */


/* RMIL = Replenishment Methods Item Labels */
const char RMIL[NULL_CODE] = "RMIL";

const char RMIL_S     [NULL_CODE] = "S";   /* Item */
const char RMIL_F     [NULL_CODE] = "F";   /* Item Parent */
const char RMIL_f     [NULL_CODE] = "f";   /* Item */
const char RMIL_I     [NULL_CODE] = "I";   /* Item List */


/* RMLB = Replenishment Method Attribute Labels */
const char RMLB[NULL_CODE] = "RMLB";

const char RMLB_MAXSTK[NULL_CODE] = "MAXSTK";   /* Maximum Stock */
const char RMLB_MINSTK[NULL_CODE] = "MINSTK";   /* Minimum Stock */
const char RMLB_INCPCT[NULL_CODE] = "INCPCT";   /* Increment Percent */
const char RMLB_MINSUP[NULL_CODE] = "MINSUP";   /* Minimum Time Supply */
const char RMLB_MAXSUP[NULL_CODE] = "MAXSUP";   /* Maximum Time Supply */
const char RMLB_INVSEL[NULL_CODE] = "INVSEL";   /* Inventory Selling Days */
const char RMLB_TMSUPH[NULL_CODE] = "TMSUPH";   /* Time Supply Horizon */
const char RMLB_LSTSLS[NULL_CODE] = "LSTSLS";   /* Lost Sales Factor */
const char RMLB_SERLVL[NULL_CODE] = "SERLVL";   /* Service Level */
const char RMLB_PERCNT[NULL_CODE] = "PERCNT";   /* % */
const char RMLB_DAYS  [NULL_CODE] = "DAYS";   /* Days */
const char RMLB_SSNIND[NULL_CODE] = "SSNIND";   /* Seasonal Ind. */
const char RMLB_TERMNL[NULL_CODE] = "TERMNL";   /* Terminal */
const char RMLB_STOCK [NULL_CODE] = "STOCK";   /* Stock */
const char RMLB_SSNID [NULL_CODE] = "SSNID";   /* Season ID */
const char RMLB_PHASID[NULL_CODE] = "PHASID";   /* Phase ID */


/* RMST = RMS Admin API Templates */
const char RMST[NULL_CODE] = "RMST";

const char RMST_RMSADM[NULL_CODE] = "RMSADM";   /* Administration */
const char RMST_RMSSEC[NULL_CODE] = "RMSSEC";   /* Security */
const char RMST_RMSFCO[NULL_CODE] = "RMSFCO";   /* Financial Control */
const char RMST_RMSBUD[NULL_CODE] = "RMSBUD";   /* Budgets */
const char RMST_RMSFIN[NULL_CODE] = "RMSFIN";   /* Finance Administration */
const char RMST_RMSFRN[NULL_CODE] = "RMSFRN";   /* Franchise */
const char RMST_RMSFND[NULL_CODE] = "RMSFND";   /* Foundation */
const char RMST_RMSINV[NULL_CODE] = "RMSINV";   /* Inventory */
const char RMST_RMSPCO[NULL_CODE] = "RMSPCO";   /* Price and Cost */
const char RMST_RMSITM[NULL_CODE] = "RMSITM";   /* Items */
const char RMST_RMSIMP[NULL_CODE] = "RMSIMP";   /* Import */


/* ROCT = Replenishment Order Control */
const char ROCT[NULL_CODE] = "ROCT";

const char ROCT_M     [NULL_CODE] = "M";   /* Manual */
const char ROCT_B     [NULL_CODE] = "B";   /* Buyer Worksheet */
const char ROCT_S     [NULL_CODE] = "S";   /* Semi-Automatic */
const char ROCT_A     [NULL_CODE] = "A";   /* Automatic */


/* ROLE = Regionality Roles */
const char ROLE[NULL_CODE] = "ROLE";

const char ROLE_BUYER [NULL_CODE] = "BUYER";   /* Buyer */
const char ROLE_ACCT  [NULL_CODE] = "ACCT";   /* Accounting Clerk */
const char ROLE_CATG  [NULL_CODE] = "CATG";   /* Category Manager */
const char ROLE_REGRL1[NULL_CODE] = "REGRL1";   /* Regionality Role #1 */
const char ROLE_REGRL2[NULL_CODE] = "REGRL2";   /* Regionality Role #2 */


/* RON = OI Report Option Req/Opt/No */
const char RON[NULL_CODE] = "RON ";

const char RON_R     [NULL_CODE] = "R";   /* Required */
const char RON_O     [NULL_CODE] = "O";   /* Optional */
const char RON_N     [NULL_CODE] = "N";   /* No */


/* RPE = RPM Pricing Event */
const char RPE[NULL_CODE] = "RPE ";

const char RPE_NI    [NULL_CODE] = "NI";   /* New Item */
const char RPE_NL    [NULL_CODE] = "NL";   /* New Item/Loc Association */
const char RPE_CC    [NULL_CODE] = "CC";   /* Cost Change */
const char RPE_PS    [NULL_CODE] = "PS";   /* Primary Store in a Zone Change */
const char RPE_CR    [NULL_CODE] = "CR";   /* Competitor Retail Change */
const char RPE_PC    [NULL_CODE] = "PC";   /* Primary Competitor Change */


/* RPO = Req/Prohib/Optional */
const char RPO[NULL_CODE] = "RPO ";

const char RPO_R     [NULL_CODE] = "R";   /* Required */
const char RPO_P     [NULL_CODE] = "P";   /* Prohibited */
const char RPO_O     [NULL_CODE] = "O";   /* Optional */


/* RPPM = Report Print Mode */
const char RPPM[NULL_CODE] = "RPPM";

const char RPPM_A     [NULL_CODE] = "A";   /* Asynchronous */
const char RPPM_S     [NULL_CODE] = "S";   /* Synchronous */


/* RPRC = Replenishment Review Cycle */
const char RPRC[NULL_CODE] = "RPRC";

const char RPRC_1     [NULL_CODE] = "1";   /* Every Day */
const char RPRC_0     [NULL_CODE] = "0";   /* Every Week */
const char RPRC_2     [NULL_CODE] = "2";   /* Every 2 Weeks */
const char RPRC_3     [NULL_CODE] = "3";   /* Every 3 Weeks */
const char RPRC_4     [NULL_CODE] = "4";   /* Every 4 Weeks */
const char RPRC_5     [NULL_CODE] = "5";   /* Every 5 Weeks */
const char RPRC_6     [NULL_CODE] = "6";   /* Every 6 Weeks */
const char RPRC_7     [NULL_CODE] = "7";   /* Every 7 Weeks */
const char RPRC_8     [NULL_CODE] = "8";   /* Every 8 Weeks */
const char RPRC_9     [NULL_CODE] = "9";   /* Every 9 Weeks */
const char RPRC_10    [NULL_CODE] = "10";   /* Every 10 Weeks */
const char RPRC_11    [NULL_CODE] = "11";   /* Every 11 Weeks */
const char RPRC_12    [NULL_CODE] = "12";   /* Every 12 Weeks */
const char RPRC_13    [NULL_CODE] = "13";   /* Every 13 Weeks */
const char RPRC_14    [NULL_CODE] = "14";   /* Every 14 Weeks */


/* RQAC = Reqfind action list box */
const char RQAC[NULL_CODE] = "RQAC";

const char RQAC_NEW   [NULL_CODE] = "NEW";   /* New */
const char RQAC_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char RQAC_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char RQAC_PRINT [NULL_CODE] = "PRINT";   /* Print */
const char RQAC_FEED  [NULL_CODE] = "FEED";   /* Feedback */


/* RQFD = Requisition Feedback Indicator */
const char RQFD[NULL_CODE] = "RQFD";

const char RQFD_0     [NULL_CODE] = "0";   /* Not Fed Back */
const char RQFD_1     [NULL_CODE] = "1";   /* Completed */


/* RQOR = Requisition Origin */
const char RQOR[NULL_CODE] = "RQOR";

const char RQOR_1     [NULL_CODE] = "1";   /* System Generated */
const char RQOR_2     [NULL_CODE] = "2";   /* Manually Input */


/* RQPI = Requistion Print Indicator */
const char RQPI[NULL_CODE] = "RQPI";

const char RQPI_0     [NULL_CODE] = "0";   /* Not Printed */
const char RQPI_1     [NULL_CODE] = "1";   /* Printed */


/* RSAT = ReSA Admin API Templates */
const char RSAT[NULL_CODE] = "RSAT";

const char RSAT_RSAEC [NULL_CODE] = "RSAEC";   /* Error Codes */
const char RSAT_RSARF [NULL_CODE] = "RSARF";   /* Reference Field */
const char RSAT_RSACR [NULL_CODE] = "RSACR";   /* Currency Rounding Rules */
const char RSAT_RSAGLC[NULL_CODE] = "RSAGLC";   /* GL Cross Reference */
const char RSAT_RSASD [NULL_CODE] = "RSASD";   /* Store Data */
const char RSAT_RSATT [NULL_CODE] = "RSATT";   /* Tender Types */
const char RSAT_RSACD [NULL_CODE] = "RSACD";   /* Constants */


/* RSLT = Replenishment Service Level Types */
const char RSLT[NULL_CODE] = "RSLT";

const char RSLT_S     [NULL_CODE] = "S";   /* Standard */
const char RSLT_SS    [NULL_CODE] = "SS";   /* Simple Sales */


/* RSUF = Restraint Suffix */
const char RSUF[NULL_CODE] = "RSUF";

const char RSUF_SU    [NULL_CODE] = "SU";   /* Suffix1 */
const char RSUF_SU2   [NULL_CODE] = "SU2";   /* Suffix2 */
const char RSUF_SU3   [NULL_CODE] = "SU3";   /* Suffix3 */


/* RSYS = RTLog Originating System */
const char RSYS[NULL_CODE] = "RSYS";

const char RSYS_OMS   [NULL_CODE] = "OMS";   /* OMS */
const char RSYS_POS   [NULL_CODE] = "POS";   /* POS */


/* RTLT = Retail Label Type */
const char RTLT[NULL_CODE] = "RTLT";

const char RTLT_PREP  [NULL_CODE] = "PREP";   /* Pre-Priced */
const char RTLT_COFF  [NULL_CODE] = "COFF";   /* Cents Off */
const char RTLT_EXTRA [NULL_CODE] = "EXTRA";   /* Extra Product */


/* RTSF = RTV/RAC Transfer */
const char RTSF[NULL_CODE] = "RTSF";

const char RTSF_E     [NULL_CODE] = "E";   /* Intercompany */
const char RTSF_A     [NULL_CODE] = "A";   /* Intra-Company */


/* RTV = Label RTV */
const char RTV[NULL_CODE] = "RTV ";

const char RTV_R     [NULL_CODE] = "R";   /* RTV */


/* RTVR = Return to Vendor Reasons */
const char RTVR[NULL_CODE] = "RTVR";

const char RTVR_U     [NULL_CODE] = "U";   /* Unavailable Inventory */
const char RTVR_O     [NULL_CODE] = "O";   /* Overstock */
const char RTVR_W     [NULL_CODE] = "W";   /* Externally Initiated RTV */


/* RTVS = MRT RTV Create Status */
const char RTVS[NULL_CODE] = "RTVS";

const char RTVS_I     [NULL_CODE] = "I";   /* Input */
const char RTVS_A     [NULL_CODE] = "A";   /* Approved */
const char RTVS_M     [NULL_CODE] = "M";   /* Manual */


/* RTYP = Restraint Type */
const char RTYP[NULL_CODE] = "RTYP";

const char RTYP_TR    [NULL_CODE] = "TR";   /* Test Restraint1 */
const char RTYP_TR2   [NULL_CODE] = "TR2";   /* Test Restraint2 */
const char RTYP_TR3   [NULL_CODE] = "TR3";   /* Test Restraint3 */


/* RUAE = Receiver Unit Adjustment Errors */
const char RUAE[NULL_CODE] = "RUAE";

const char RUAE_RUAL  [NULL_CODE] = "RUAL";   /* Record Locked */
const char RUAE_RUAU  [NULL_CODE] = "RUAU";   /* Available Qty less than Adjusted Qty */


/* RUCA =  Receiver Cost Adjustment */
const char RUCA[NULL_CODE] = "RUCA";

const char RUCA_F     [NULL_CODE] = "F";   /* FIFO */
const char RUCA_S     [NULL_CODE] = "S";   /* Standard */


/* RUCI = List of RTV Unit Cost Values */
const char RUCI[NULL_CODE] = "RUCI";

const char RUCI_A     [NULL_CODE] = "A";   /* Average Cost */
const char RUCI_S     [NULL_CODE] = "S";   /* Standard Cost */
const char RUCI_R     [NULL_CODE] = "R";   /* Last Receipt Cost */


/* RVST = Return to Vendor Status */
const char RVST[NULL_CODE] = "RVST";

const char RVST_5     [NULL_CODE] = "5";   /* Input */
const char RVST_10    [NULL_CODE] = "10";   /* Approved */
const char RVST_12    [NULL_CODE] = "12";   /* In Progress */
const char RVST_15    [NULL_CODE] = "15";   /* Shipped */
const char RVST_20    [NULL_CODE] = "20";   /* Cancelled */


/* RVTP = Origin of Item Returned to Vendor */
const char RVTP[NULL_CODE] = "RVTP";

const char RVTP_I     [NULL_CODE] = "I";   /* Inventory */
const char RVTP_Q     [NULL_CODE] = "Q";   /* Quality Control */


/* S9A1 = Spreadsheet action types - Only NEW */
const char S9A1[NULL_CODE] = "S9A1";

const char S9A1_NEW   [NULL_CODE] = "NEW";   /* Create */


/* S9A2 = Spreadsheet action types - Only MOD */
const char S9A2[NULL_CODE] = "S9A2";

const char S9A2_MOD   [NULL_CODE] = "MOD";   /* Update */


/* S9A3 = Spreadsheet action types - Only Create and Delete */
const char S9A3[NULL_CODE] = "S9A3";

const char S9A3_NEW   [NULL_CODE] = "NEW";   /* Create */
const char S9A3_DEL   [NULL_CODE] = "DEL";   /* Delete */


/* S9A4 = Spreadsheet action types - Only Create and Update */
const char S9A4[NULL_CODE] = "S9A4";

const char S9A4_NEW   [NULL_CODE] = "NEW";   /* Create */
const char S9A4_MOD   [NULL_CODE] = "MOD";   /* Update */


/* S9A5 = Spreadsheet action types - Only Update and Delete */
const char S9A5[NULL_CODE] = "S9A5";

const char S9A5_MOD   [NULL_CODE] = "MOD";   /* Update */
const char S9A5_DEL   [NULL_CODE] = "DEL";   /* Delete */


/* S9AT = Spreadsheet action types */
const char S9AT[NULL_CODE] = "S9AT";

const char S9AT_NEW   [NULL_CODE] = "NEW";   /* Create */
const char S9AT_MOD   [NULL_CODE] = "MOD";   /* Update */
const char S9AT_DEL   [NULL_CODE] = "DEL";   /* Delete */


/* S9LV = Spreadsheet List of Values */
const char S9LV[NULL_CODE] = "S9LV";

const char S9LV_IS9T  [NULL_CODE] = "IS9T";   /* Item Related Templates */
const char S9LV_IS9TT [NULL_CODE] = "IS9TT";   /* Item API Templates */
const char S9LV_POS9T [NULL_CODE] = "POS9T";   /* Purchase Order API Templates */
const char S9LV_RMSADM[NULL_CODE] = "RMSADM";   /* Administration */
const char S9LV_RMSFIN[NULL_CODE] = "RMSFIN";   /* Finance Administration */
const char S9LV_RMSFND[NULL_CODE] = "RMSFND";   /* Foundation */
const char S9LV_RMSFRN[NULL_CODE] = "RMSFRN";   /* Franchise */
const char S9LV_RMSIMP[NULL_CODE] = "RMSIMP";   /* Import */
const char S9LV_RMSINV[NULL_CODE] = "RMSINV";   /* Inventory */
const char S9LV_RMSITM[NULL_CODE] = "RMSITM";   /* Items */
const char S9LV_RMSPCO[NULL_CODE] = "RMSPCO";   /* Price and Cost */
const char S9LV_RSAEC [NULL_CODE] = "RSAEC";   /* Error Codes */
const char S9LV_RSARF [NULL_CODE] = "RSARF";   /* Reference Field */
const char S9LV_RSACR [NULL_CODE] = "RSACR";   /* Currency Rounding Rules */
const char S9LV_RSAGLC[NULL_CODE] = "RSAGLC";   /* GL Cross Reference */
const char S9LV_RSASD [NULL_CODE] = "RSASD";   /* Store Data */
const char S9LV_RSATT [NULL_CODE] = "RSATT";   /* Tender Types */
const char S9LV_RSACD [NULL_CODE] = "RSACD";   /* Constants */


/* S9TC = Spreadsheet Template Categories */
const char S9TC[NULL_CODE] = "S9TC";

const char S9TC_IS9T  [NULL_CODE] = "IS9T";   /* Item Related Templates */
const char S9TC_RSARF [NULL_CODE] = "RSARF";   /* Reference Field Template */


/* SAAC = Sales Audit Action Codes */
const char SAAC[NULL_CODE] = "SAAC";

const char SAAC_NEW   [NULL_CODE] = "NEW";   /* New */
const char SAAC_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char SAAC_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char SAAC_PV    [NULL_CODE] = "PV";   /* Post Void */


/* SAAS = Sales Audit Audit Status */
const char SAAS[NULL_CODE] = "SAAS";

const char SAAS_U     [NULL_CODE] = "U";   /* Unaudited */
const char SAAS_H     [NULL_CODE] = "H";   /* Errors Pending */
const char SAAS_R     [NULL_CODE] = "R";   /* Re-Totaling/Auditing Required */
const char SAAS_T     [NULL_CODE] = "T";   /* Totaled */
const char SAAS_A     [NULL_CODE] = "A";   /* Audited */


/* SABA = Sales Audit Bank Account Types */
const char SABA[NULL_CODE] = "SABA";

const char SABA_C     [NULL_CODE] = "C";   /* Checking */
const char SABA_S     [NULL_CODE] = "S";   /* Savings */


/* SABL = Sales Audit Balancing Level */
const char SABL[NULL_CODE] = "SABL";

const char SABL_S     [NULL_CODE] = "S";   /* Store */
const char SABL_R     [NULL_CODE] = "R";   /* Register Summary */
const char SABL_C     [NULL_CODE] = "C";   /* Cashier Summary */
const char SABL_CS    [NULL_CODE] = "CS";   /* Cashier/Register Summary */


/* SACA = Sales Audit Customer Attribute Types */
const char SACA[NULL_CODE] = "SACA";

const char SACA_ERR   [NULL_CODE] = "ERR";   /* Error */
const char SACA_TERM  [NULL_CODE] = "TERM";   /* Termination Record marker */
const char SACA_SAGC  [NULL_CODE] = "SAGC";   /* Sales Audit Gender Codes */
const char SACA_SAIN  [NULL_CODE] = "SAIN";   /* Sales Audit Income Codes */
const char SACA_SARD  [NULL_CODE] = "SARD";   /* Sales Audit Retailer Defined Codes */


/* SACD = Sales Audit Comp Store Date */
const char SACD[NULL_CODE] = "SACD";

const char SACD_R     [NULL_CODE] = "R";   /* Remodel Date */
const char SACD_S     [NULL_CODE] = "S";   /* Store Open Date */
const char SACD_A     [NULL_CODE] = "A";   /* Acquire Date */


/* SACI = Sales Audit Combined Total Ind */
const char SACI[NULL_CODE] = "SACI";

const char SACI_Y     [NULL_CODE] = "Y";   /* Combined Total */
const char SACI_N     [NULL_CODE] = "N";   /* Raw Data */


/* SACM = Sales Audit Comment Type */
const char SACM[NULL_CODE] = "SACM";

const char SACM_SATR  [NULL_CODE] = "SATR";   /* ReSA Transaction No */
const char SACM_SATL  [NULL_CODE] = "SATL";   /* Total */
const char SACM_SABG  [NULL_CODE] = "SABG";   /* Balance Group */
const char SACM_SASD  [NULL_CODE] = "SASD";   /* Store/Day */


/* SACO = Sales Audit Comparison Operators */
const char SACO[NULL_CODE] = "SACO";

const char SACO_EQ    [NULL_CODE] = "=";   /* = */
const char SACO_NE    [NULL_CODE] = "!=";   /* != */
const char SACO_GT    [NULL_CODE] = ">";   /* > */
const char SACO_LT    [NULL_CODE] = "<";   /* < */
const char SACO_GE    [NULL_CODE] = ">=";   /* >= */
const char SACO_LE    [NULL_CODE] = "<=";   /* <= */
const char SACO_IN    [NULL_CODE] = "IN";   /* in */
const char SACO_NOTIN [NULL_CODE] = "NOT IN";   /* not in */
const char SACO_NULL  [NULL_CODE] = "NULL";   /* is NULL */
const char SACO_NOTNUL[NULL_CODE] = "!NULL";   /* is not NULL */
const char SACO_B     [NULL_CODE] = "B";   /* between */
const char SACO_NB    [NULL_CODE] = "NB";   /* not between */
const char SACO_TOL   [NULL_CODE] = "TOL";   /* within tolerance */
const char SACO_NOTTOL[NULL_CODE] = "!TOL";   /* not within tolerance */


/* SACS = Sales Audit Count/Sum */
const char SACS[NULL_CODE] = "SACS";

const char SACS_C     [NULL_CODE] = "C";   /* Count */
const char SACS_S     [NULL_CODE] = "S";   /* Sum */


/* SACT = Shipment Actions */
const char SACT[NULL_CODE] = "SACT";

const char SACT_ITEM  [NULL_CODE] = "ITEM";   /* View Shipments by Item */
const char SACT_ORDR  [NULL_CODE] = "ORDR";   /* View Shipments by Order */
const char SACT_MTCH  [NULL_CODE] = "MTCH";   /* Match Shipments */


/* SADS = Sales Audit Data Status */
const char SADS[NULL_CODE] = "SADS";

const char SADS_R     [NULL_CODE] = "R";   /* Ready for Import */
const char SADS_L     [NULL_CODE] = "L";   /* Loading */
const char SADS_P     [NULL_CODE] = "P";   /* Partially Loaded */
const char SADS_F     [NULL_CODE] = "F";   /* Fully Loaded */
const char SADS_G     [NULL_CODE] = "G";   /* Purged */


/* SADT = Sales Audit Discount Types */
const char SADT[NULL_CODE] = "SADT";

const char SADT_ERR   [NULL_CODE] = "ERR";   /* Error */
const char SADT_TERM  [NULL_CODE] = "TERM";   /* Termination Record marker */
const char SADT_SCOUP [NULL_CODE] = "SCOUP";   /* Store Coupon */
const char SADT_CSTMER[NULL_CODE] = "CSTMER";   /* Merchandise Accommodation */
const char SADT_CSTSH [NULL_CODE] = "CSTSH";   /* Shipping and Handling Accommodation */
const char SADT_CSTVAS[NULL_CODE] = "CSTVAS";   /* Value Added Service Accommodation */
const char SADT_SATSPL[NULL_CODE] = "SATSPL";   /* Saturday Morning Special */
const char SADT_SENCIT[NULL_CODE] = "SENCIT";   /* Senior Citizen */
const char SADT_CMPSPL[NULL_CODE] = "CMPSPL";   /* Competition Special */
const char SADT_EMPDSC[NULL_CODE] = "EMPDSC";   /* Employee Discount */
const char SADT_S     [NULL_CODE] = "S";   /* Incorrect Signage */
const char SADT_MS    [NULL_CODE] = "MS";   /* Manager Discretion */
const char SADT_CP    [NULL_CODE] = "CP";   /* Price Guarantee */
const char SADT_D     [NULL_CODE] = "D";   /* Damage Adjustment */
const char SADT_NEWPRC[NULL_CODE] = "NEWPRC";   /* New Price Rule */
const char SADT_DOC   [NULL_CODE] = "DOC";   /* Document */
const char SADT_MCOUP [NULL_CODE] = "MCOUP";   /* @SUH1@ Coupon */
const char SADT_REFUND[NULL_CODE] = "REFUND";   /* Refund Proration */
const char SADT_CALWAR[NULL_CODE] = "CALWAR";   /* Warranty Price */


/* SAES = Sales Audit Export Status */
const char SAES[NULL_CODE] = "SAES";

const char SAES_R     [NULL_CODE] = "R";   /* Ready for Export */
const char SAES_P     [NULL_CODE] = "P";   /* Processed */
const char SAES_E     [NULL_CODE] = "E";   /* Exported */


/* SAET = Sales Audit Excheat Type */
const char SAET[NULL_CODE] = "SAET";

const char SAET_E     [NULL_CODE] = "E";   /* Escheatment */
const char SAET_N     [NULL_CODE] = "N";   /* Income Adjustment */


/* SAFD = Sales Audit Full Disclosure Indicator */
const char SAFD[NULL_CODE] = "SAFD";

const char SAFD_P     [NULL_CODE] = "P";   /* Positive Transaction/Total */
const char SAFD_N     [NULL_CODE] = "N";   /* Negative (Reversal) Transaction/Total */


/* SAGC = Sales Audit Gender Codes */
const char SAGC[NULL_CODE] = "SAGC";

const char SAGC_M     [NULL_CODE] = "M";   /* Male */
const char SAGC_F     [NULL_CODE] = "F";   /* Female */
const char SAGC_U     [NULL_CODE] = "U";   /* Unknown */
const char SAGC_ERR   [NULL_CODE] = "ERR";   /* Error */


/* SAIN = Sales Audit Income Codes */
const char SAIN[NULL_CODE] = "SAIN";

const char SAIN_0     [NULL_CODE] = "0";   /* 0 - 9,999 */
const char SAIN_10    [NULL_CODE] = "10";   /* 10,000 - 14,999 */
const char SAIN_15    [NULL_CODE] = "15";   /* 15,000 - 19,999 */
const char SAIN_20    [NULL_CODE] = "20";   /* 20,000 - 24,999 */
const char SAIN_25    [NULL_CODE] = "25";   /* 25,000 - 29,999 */
const char SAIN_30    [NULL_CODE] = "30";   /* F,000 - 34,999 */
const char SAIN_35    [NULL_CODE] = "35";   /* 35,000 - 39,999 */
const char SAIN_40    [NULL_CODE] = "40";   /* 40,000 - 44,999 */
const char SAIN_45    [NULL_CODE] = "45";   /* 45,000 - 49,999 */
const char SAIN_50    [NULL_CODE] = "50";   /* 50,000 - 54,999 */
const char SAIN_55    [NULL_CODE] = "55";   /* 55,000 - 59,999 */
const char SAIN_60    [NULL_CODE] = "60";   /* 60,000 - 64,999 */
const char SAIN_65    [NULL_CODE] = "65";   /* 65,000 - 69,999 */
const char SAIN_70    [NULL_CODE] = "70";   /* 70,000 - 74,999 */
const char SAIN_75    [NULL_CODE] = "75";   /* 75,000 - 79,999 */
const char SAIN_80    [NULL_CODE] = "80";   /* 80,000 - 84,999 */
const char SAIN_85    [NULL_CODE] = "85";   /* 85,000 - 89,999 */
const char SAIN_90    [NULL_CODE] = "90";   /* 90,000 - 94,999 */
const char SAIN_95    [NULL_CODE] = "95";   /* 95,000 - 99,999 */
const char SAIN_100   [NULL_CODE] = "100";   /* 100,000 - 149,999 */
const char SAIN_150   [NULL_CODE] = "150";   /* 150,000 - 199,999 */
const char SAIN_200   [NULL_CODE] = "200";   /* 200,000 - 249,999 */
const char SAIN_RICH  [NULL_CODE] = "250+";   /* Over 250,000 */
const char SAIN_ERR   [NULL_CODE] = "ERR";   /* Erroneous Value */


/* SAIS = Sales Audit Import Status */
const char SAIS[NULL_CODE] = "SAIS";

const char SAIS_R     [NULL_CODE] = "R";   /* Ready for Import */
const char SAIS_L     [NULL_CODE] = "L";   /* Loaded into Database */
const char SAIS_U     [NULL_CODE] = "U";   /* Unexpected Import */


/* SAIT = Sales Audit Item Types */
const char SAIT[NULL_CODE] = "SAIT";

const char SAIT_ERR   [NULL_CODE] = "ERR";   /* Error */
const char SAIT_TERM  [NULL_CODE] = "TERM";   /* Termination Record Marker */
const char SAIT_ITEM  [NULL_CODE] = "ITEM";   /* Item */
const char SAIT_REF   [NULL_CODE] = "REF";   /* Reference Item */
const char SAIT_GCN   [NULL_CODE] = "GCN";   /* Voucher Number */
const char SAIT_NMITEM[NULL_CODE] = "NMITEM";   /* Non Merchandise Item */


/* SALT = Sales Audit Lock Types */
const char SALT[NULL_CODE] = "SALT";

const char SALT_R     [NULL_CODE] = "R";   /* Read Only Lock */
const char SALT_W     [NULL_CODE] = "W";   /* Write Lock */


/* SAMS = Sales Audit Missing Status */
const char SAMS[NULL_CODE] = "SAMS";

const char SAMS_A     [NULL_CODE] = "A";   /* Added */
const char SAMS_D     [NULL_CODE] = "D";   /* Deleted */
const char SAMS_M     [NULL_CODE] = "M";   /* Missing */


/* SAND = Sales Audit Not Defined */
const char SAND[NULL_CODE] = "SAND";

const char SAND_N     [NULL_CODE] = "N";   /* Not Defined */
const char SAND_I     [NULL_CODE] = "I";   /* Invalid */
const char SAND_ERR   [NULL_CODE] = "ERR";   /* Record in Error */


/* SAO = Sales Audit Order */
const char SAO[NULL_CODE] = "SAO ";

const char SAO_1     [NULL_CODE] = "1";   /* 1 */
const char SAO_2     [NULL_CODE] = "2";   /* 2 */
const char SAO_3     [NULL_CODE] = "3";   /* 3 */
const char SAO_4     [NULL_CODE] = "4";   /* 4 */
const char SAO_5     [NULL_CODE] = "5";   /* 5 */
const char SAO_6     [NULL_CODE] = "6";   /* 6 */
const char SAO_7     [NULL_CODE] = "7";   /* 7 */
const char SAO_8     [NULL_CODE] = "8";   /* 8 */
const char SAO_9     [NULL_CODE] = "9";   /* 9 */
const char SAO_10    [NULL_CODE] = "10";   /* 10 */
const char SAO_11    [NULL_CODE] = "11";   /* 11 */
const char SAO_12    [NULL_CODE] = "12";   /* 12 */
const char SAO_13    [NULL_CODE] = "13";   /* 13 */
const char SAO_14    [NULL_CODE] = "14";   /* 14 */
const char SAO_15    [NULL_CODE] = "15";   /* 15 */
const char SAO_16    [NULL_CODE] = "16";   /* 16 */
const char SAO_17    [NULL_CODE] = "17";   /* 17 */
const char SAO_18    [NULL_CODE] = "18";   /* 18 */
const char SAO_19    [NULL_CODE] = "19";   /* 19 */
const char SAO_20    [NULL_CODE] = "20";   /* 20 */


/* SAPA = Sales Audit Presence or Absence Indicator */
const char SAPA[NULL_CODE] = "SAPA";

const char SAPA_P     [NULL_CODE] = "P";   /* Presence */
const char SAPA_A     [NULL_CODE] = "A";   /* Absence */


/* SARD = Sales Audit Retailer Defined Codes */
const char SARD[NULL_CODE] = "SARD";

const char SARD_RD1   [NULL_CODE] = "RD1";   /* Retailer Defined 1 */
const char SARD_RD2   [NULL_CODE] = "RD2";   /* Retailer Defined 2 */
const char SARD_RD3   [NULL_CODE] = "RD3";   /* Retailer Defined 3 */
const char SARD_ERR   [NULL_CODE] = "ERR";   /* Error */


/* SARL = Sales Audit Rule Definition Labels */
const char SARL[NULL_CODE] = "SARL";

const char SARL_RO    [NULL_CODE] = "RO";   /* Rule Overview */
const char SARL_RC    [NULL_CODE] = "RC";   /* Rule Characteristics */
const char SARL_R     [NULL_CODE] = "R";   /* Realms */
const char SARL_J     [NULL_CODE] = "J";   /* Joins */
const char SARL_P     [NULL_CODE] = "P";   /* Parameters */
const char SARL_RE    [NULL_CODE] = "RE";   /* Restrictions */
const char SARL_LT    [NULL_CODE] = "LT";   /* Location Traits */


/* SARR = Return Reason Types */
const char SARR[NULL_CODE] = "SARR";

const char SARR_RET1  [NULL_CODE] = "RET1";   /* Did not like */
const char SARR_RET2  [NULL_CODE] = "RET2";   /* Better Price somewhere else. */
const char SARR_RET3  [NULL_CODE] = "RET3";   /* Did not fit */
const char SARR_RET4  [NULL_CODE] = "RET4";   /* Damaged */
const char SARR_RET5  [NULL_CODE] = "RET5";   /* Exchange */
const char SARR_RET6  [NULL_CODE] = "RET6";   /* Poor Quality */
const char SARR_RET41 [NULL_CODE] = "RET41";   /* Open Box */
const char SARR_RET42 [NULL_CODE] = "RET42";   /* Unusable */
const char SARR_RET43 [NULL_CODE] = "RET43";   /* Repairable */
const char SARR_ERR   [NULL_CODE] = "ERR";   /* Error */


/* SARS = Sales Audit Rounding Rules Status */
const char SARS[NULL_CODE] = "SARS";

const char SARS_A     [NULL_CODE] = "A";   /* Active */
const char SARS_I     [NULL_CODE] = "I";   /* Inactive */


/* SART = Sales Audit Record Types */
const char SART[NULL_CODE] = "SART";

const char SART_THEAD [NULL_CODE] = "THEAD";   /* Header Level Error */
const char SART_TITEM [NULL_CODE] = "TITEM";   /* Item Level Error */
const char SART_TTEND [NULL_CODE] = "TTEND";   /* Tender Level Error */
const char SART_TTAX  [NULL_CODE] = "TTAX";   /* Tax Level Error */
const char SART_IDISC [NULL_CODE] = "IDISC";   /* Item Discount Level Error */
const char SART_TTAIL [NULL_CODE] = "TTAIL";   /* Tail Error */
const char SART_TCUST [NULL_CODE] = "TCUST";   /* Customer Level Error */
const char SART_ERROR [NULL_CODE] = "ERROR";   /* Error */
const char SART_TINFO [NULL_CODE] = "TINFO";   /* Informational Error */
const char SART_FHEAD [NULL_CODE] = "FHEAD";   /* Header Level Error */
const char SART_FTAIL [NULL_CODE] = "FTAIL";   /* Tail Error */
const char SART_TOTAL [NULL_CODE] = "TOTAL";   /* Total Error */
const char SART_STORE [NULL_CODE] = "STORE";   /* Store Day Error */
const char SART_BALG  [NULL_CODE] = "BALG";   /* Balance Group Error */
const char SART_TCTYPE[NULL_CODE] = "TCTYPE";   /* Invalid Customer ID Type */
const char SART_TCUSTA[NULL_CODE] = "TCUSTA";   /* Customer Attribute Level Error */
const char SART_THBALC[NULL_CODE] = "THBALC";   /* Transaction out of Balance */
const char SART_THPOSI[NULL_CODE] = "THPOSI";   /* Invalid POS Transaction Indicator */
const char SART_THREAC[NULL_CODE] = "THREAC";   /* Invalid Reason Code */
const char SART_THSUBT[NULL_CODE] = "THSUBT";   /* Invalid Sub-Transaction Type */
const char SART_THTRAT[NULL_CODE] = "THTRAT";   /* Invalid Transaction Type */
const char SART_THDATE[NULL_CODE] = "THDATE";   /* Invalid Transaction Date */
const char SART_CATT  [NULL_CODE] = "CATT";   /* Customer Attribute Type */
const char SART_THTRAN[NULL_CODE] = "THTRAN";   /* Invalid Transaction Number */
const char SART_THOREG[NULL_CODE] = "THOREG";   /* Invalid Original Register Number */
const char SART_THOTRT[NULL_CODE] = "THOTRT";   /* Invalid Original Transaction Type */
const char SART_THOTRN[NULL_CODE] = "THOTRN";   /* Invalid Original Transaction Number */
const char SART_THVALU[NULL_CODE] = "THVALU";   /* Invalid Value */
const char SART_RULERR[NULL_CODE] = "RULERR";   /* Rule function did not execute */
const char SART_TOTERR[NULL_CODE] = "TOTERR";   /* Total function did not execute */
const char SART_THTEND[NULL_CODE] = "THTEND";   /* Invalid Tender Ending */
const char SART_THTTSA[NULL_CODE] = "THTTSA";   /* No rounding rule for Sale Total */
const char SART_IGTAX [NULL_CODE] = "IGTAX";   /* Item Level Tax Error */
const char SART_TPYMT [NULL_CODE] = "TPYMT";   /* Payment Level Error */


/* SASC = Sales Audit Sum/Count Wizard Total */
const char SASC[NULL_CODE] = "SASC";

const char SASC_SUM   [NULL_CODE] = "SUM";   /* Parameter to be Totaled */
const char SASC_COUNT [NULL_CODE] = "COUNT";   /* Parameter to be Counted */


/* SASD = Sales Audit Discount Types */
const char SASD[NULL_CODE] = "SASD";

const char SASD_RMS   [NULL_CODE] = "RMS";   /* RMS Discount */
const char SASD_COUPON[NULL_CODE] = "COUPON";   /* Coupon Type 1 */


/* SASI = Sales Audit Item Status */
const char SASI[NULL_CODE] = "SASI";

const char SASI_ERR   [NULL_CODE] = "ERR";   /* Error */
const char SASI_V     [NULL_CODE] = "V";   /* Voided */
const char SASI_S     [NULL_CODE] = "S";   /* Sale */
const char SASI_R     [NULL_CODE] = "R";   /* Return */
const char SASI_O     [NULL_CODE] = "O";   /* Other */
const char SASI_ORI   [NULL_CODE] = "ORI";   /* Ord Ini */
const char SASI_ORC   [NULL_CODE] = "ORC";   /* Ord Can */
const char SASI_ORD   [NULL_CODE] = "ORD";   /* Ord Com */
const char SASI_LIN   [NULL_CODE] = "LIN";   /* Lay Ini */
const char SASI_LCA   [NULL_CODE] = "LCA";   /* Lay Can */
const char SASI_LCO   [NULL_CODE] = "LCO";   /* Lay Com */


/* SASR = Sales Audit Supported Realms */
const char SASR[NULL_CODE] = "SASR";

const char SASR_T     [NULL_CODE] = "T";   /* Table */
const char SASR_V     [NULL_CODE] = "V";   /* View */
const char SASR_P     [NULL_CODE] = "P";   /* PL/SQL */


/* SASS = Sales Audit Store Status */
const char SASS[NULL_CODE] = "SASS";

const char SASS_W     [NULL_CODE] = "W";   /* Worksheet */
const char SASS_F     [NULL_CODE] = "F";   /* Fuel Closed */
const char SASS_C     [NULL_CODE] = "C";   /* Closed */


/* SAST = Sales Audit Status Codes */
const char SAST[NULL_CODE] = "SAST";

const char SAST_D     [NULL_CODE] = "D";   /* Delete */
const char SAST_P     [NULL_CODE] = "P";   /* Present */
const char SAST_V     [NULL_CODE] = "V";   /* Post-Voided */


/* SASV = Sales Audit Status Values */
const char SASV[NULL_CODE] = "SASV";

const char SASV_W     [NULL_CODE] = "W";   /* Worksheet */
const char SASV_S     [NULL_CODE] = "S";   /* Submitted */
const char SASV_A     [NULL_CODE] = "A";   /* Approved */
const char SASV_DI    [NULL_CODE] = "DI";   /* Disabled */
const char SASV_DE    [NULL_CODE] = "DE";   /* Deleted */
const char SASV_B     [NULL_CODE] = "B";   /* All */


/* SASY = Sales Audit Sales Type */
const char SASY[NULL_CODE] = "SASY";

const char SASY_R     [NULL_CODE] = "R";   /* Regular Sales */
const char SASY_I     [NULL_CODE] = "I";   /* In-Store Customer Order */
const char SASY_E     [NULL_CODE] = "E";   /* External Customer Order */


/* SATC = Sales Audit Totals Category */
const char SATC[NULL_CODE] = "SATC";

const char SATC_S     [NULL_CODE] = "S";   /* Sales */
const char SATC_TAX   [NULL_CODE] = "TAX";   /* Tax */
const char SATC_OS    [NULL_CODE] = "OS";   /* Over/Short */
const char SATC_TT    [NULL_CODE] = "TT";   /* Transaction Type */
const char SATC_TD    [NULL_CODE] = "TD";   /* Tender Type */
const char SATC_PT    [NULL_CODE] = "PT";   /* Payment Total */


/* SATL = Sales Audit Total Definition Labels */
const char SATL[NULL_CODE] = "SATL";

const char SATL_TO    [NULL_CODE] = "TO";   /* Total Overview */
const char SATL_TC    [NULL_CODE] = "TC";   /* Total Characteristics */
const char SATL_TCC   [NULL_CODE] = "TCC";   /* Total Characteristics Cont. */
const char SATL_R     [NULL_CODE] = "R";   /* Realms */
const char SATL_J     [NULL_CODE] = "J";   /* Joins */
const char SATL_P     [NULL_CODE] = "P";   /* Parameters */
const char SATL_RU    [NULL_CODE] = "RU";   /* Roll-Ups */
const char SATL_RE    [NULL_CODE] = "RE";   /* Restrictions */
const char SATL_CTD   [NULL_CODE] = "CTD";   /* Combined Total Details */
const char SATL_LT    [NULL_CODE] = "LT";   /* Location Traits */
const char SATL_U     [NULL_CODE] = "U";   /* Usages */


/* SATO = Sales Audit Total Transaction Indicator */
const char SATO[NULL_CODE] = "SATO";

const char SATO_TO    [NULL_CODE] = "TO";   /* Total */
const char SATO_TR    [NULL_CODE] = "TR";   /* Transaction */
const char SATO_BG    [NULL_CODE] = "BG";   /* Balance Group Level Error */


/* SATP = Pack Sales Type */
const char SATP[NULL_CODE] = "SATP";

const char SATP_R     [NULL_CODE] = "R";   /* Regular */
const char SATP_P     [NULL_CODE] = "P";   /* Promotional */


/* SATT = Sales Audit Tolerance Types */
const char SATT[NULL_CODE] = "SATT";

const char SATT_A     [NULL_CODE] = "A";   /* Amount */
const char SATT_P     [NULL_CODE] = "P";   /* Percent */


/* SATY = Sales Audit Total Types */
const char SATY[NULL_CODE] = "SATY";

const char SATY_O     [NULL_CODE] = "O";   /* Over/Short */
const char SATY_M     [NULL_CODE] = "M";   /* Miscellaneous */


/* SAUT = Sales Audit Usage Types */
const char SAUT[NULL_CODE] = "SAUT";

const char SAUT_FLR   [NULL_CODE] = "FLR";   /* Flash Totals Reporting */
const char SAUT_SFMS  [NULL_CODE] = "SFMS";   /* Site Fuels Management Sales */
const char SAUT_SFMD  [NULL_CODE] = "SFMD";   /* Site Fuels Management Drive Offs */
const char SAUT_SFMP  [NULL_CODE] = "SFMP";   /* Site Fuels Management Pump Tests */
const char SAUT_UARBDP[NULL_CODE] = "UARBDP";   /* UAR Bank Deposits */
const char SAUT_UARMON[NULL_CODE] = "UARMON";   /* UAR Money Orders */
const char SAUT_UARLOT[NULL_CODE] = "UARLOT";   /* UAR Lottery */
const char SAUT_UARCRD[NULL_CODE] = "UARCRD";   /* UAR Credit Card */
const char SAUT_ACH   [NULL_CODE] = "ACH";   /* Automated Clearing House */
const char SAUT_RA    [NULL_CODE] = "RA";   /* RA Export */
const char SAUT_GL    [NULL_CODE] = "GL";   /* General Ledger */
const char SAUT_IM    [NULL_CODE] = "IM";   /* IM Export */


/* SAVS = Sales Audit Voucher Status */
const char SAVS[NULL_CODE] = "SAVS";

const char SAVS_A     [NULL_CODE] = "A";   /* Assigned from HQ to a Store */
const char SAVS_I     [NULL_CODE] = "I";   /* Issued from a Store to a Customer */
const char SAVS_R     [NULL_CODE] = "R";   /* Redeemed by a Customer */
const char SAVS_E     [NULL_CODE] = "E";   /* Escheated to the State/Country */
const char SAVS_N     [NULL_CODE] = "N";   /* Income Adjustment to Retailer */


/* SBCA = Supplier Bracket Category Abbreviations */
const char SBCA[NULL_CODE] = "SBCA";

const char SBCA_AMNT  [NULL_CODE] = "AMNT";   /* Amount */
const char SBCA_CUBE  [NULL_CODE] = "CUBE";   /* Cube */
const char SBCA_WGHT  [NULL_CODE] = "WGHT";   /* Weight */


/* SCDS = Description for stkschedexpl.pc */
const char SCDS[NULL_CODE] = "SCDS";

const char SCDS_1     [NULL_CODE] = "1";   /* Scheduled Count for %s */


/* SCL = Scaling Constraint Type */
const char SCL[NULL_CODE] = "SCL ";

const char SCL_O     [NULL_CODE] = "O";   /* Order Level */
const char SCL_L     [NULL_CODE] = "L";   /* Location Level */


/* SCLN = Scale Order */
const char SCLN[NULL_CODE] = "SCLN";

const char SCLN_S     [NULL_CODE] = "S";   /* Scale Order */
const char SCLN_U     [NULL_CODE] = "U";   /* Undo Scaling */


/* SCNT = Stock Count */
const char SCNT[NULL_CODE] = "SCNT";

const char SCNT_Y     [NULL_CODE] = "Y";   /* For Units Only */
const char SCNT_N     [NULL_CODE] = "N";   /* Never */
const char SCNT_S     [NULL_CODE] = "S";   /* For Unit and Value */


/* SCO = Scaling Constraint Objective */
const char SCO[NULL_CODE] = "SCO ";

const char SCO_M     [NULL_CODE] = "M";   /* Minimum */
const char SCO_X     [NULL_CODE] = "X";   /* Maximum */


/* SCRT = Stock Count Run Type */
const char SCRT[NULL_CODE] = "SCRT";

const char SCRT_A     [NULL_CODE] = "A";   /* Actual */
const char SCRT_T     [NULL_CODE] = "T";   /* Trial Run */


/* SCSK = Cycle Count Item List */
const char SCSK[NULL_CODE] = "SCSK";

const char SCSK_P     [NULL_CODE] = "P";   /* Product */
const char SCSK_I     [NULL_CODE] = "I";   /* Item List */


/* SCST = Stock Categories - Store */
const char SCST[NULL_CODE] = "SCST";

const char SCST_D     [NULL_CODE] = "D";   /* Direct To Store */
const char SCST_C     [NULL_CODE] = "C";   /* Cross-Docked */
const char SCST_W     [NULL_CODE] = "W";   /* Warehouse Stocked */
const char SCST_L     [NULL_CODE] = "L";   /* WH/Cross Link */


/* SCT = Scaling Constraint Type */
const char SCT[NULL_CODE] = "SCT ";

const char SCT_A     [NULL_CODE] = "A";   /* Amount */
const char SCT_M     [NULL_CODE] = "M";   /* Mass */
const char SCT_V     [NULL_CODE] = "V";   /* Volume */
const char SCT_P     [NULL_CODE] = "P";   /* Pallet */
const char SCT_C     [NULL_CODE] = "C";   /* Case */
const char SCT_E     [NULL_CODE] = "E";   /* Each */
const char SCT_S     [NULL_CODE] = "S";   /* Stat Case */


/* SCTL = Invoice Matching Cost Tolerance Level */
const char SCTL[NULL_CODE] = "SCTL";

const char SCTL_TC    [NULL_CODE] = "TC";   /* Summary Cost */
const char SCTL_LC    [NULL_CODE] = "LC";   /* Line Item Cost */
const char SCTL_LQ    [NULL_CODE] = "LQ";   /* Line Item Qty. */


/* SCTY = Stock Count Type */
const char SCTY[NULL_CODE] = "SCTY";

const char SCTY_U     [NULL_CODE] = "U";   /* Unit */
const char SCTY_B     [NULL_CODE] = "B";   /* Unit & Value */


/* SCWH = Stock Categories - Warehouse */
const char SCWH[NULL_CODE] = "SCWH";

const char SCWH_W     [NULL_CODE] = "W";   /* Warehouse Stocked */


/* SDST = Sales Audit Store Status */
const char SDST[NULL_CODE] = "SDST";

const char SDST_I     [NULL_CODE] = "I";   /* Sales Audit in Progress */
const char SDST_A     [NULL_CODE] = "A";   /* Sales Audit Complete */


/* SEPC = Season/Phase maintenance codes */
const char SEPC[NULL_CODE] = "SEPC";

const char SEPC_A     [NULL_CODE] = "A";   /* Add */
const char SEPC_C     [NULL_CODE] = "C";   /* Change */
const char SEPC_D     [NULL_CODE] = "D";   /* Delete */


/* SEPS = Season Phase Style/color Maintenance */
const char SEPS[NULL_CODE] = "SEPS";

const char SEPS_A     [NULL_CODE] = "A";   /* Add */
const char SEPS_O     [NULL_CODE] = "O";   /* Overwrite */


/* SESF = Sales Error Status Filter */
const char SESF[NULL_CODE] = "SESF";

const char SESF_A     [NULL_CODE] = "A";   /* Approved Rules  */
const char SESF_W     [NULL_CODE] = "W";   /* Non-Approved Rules  */
const char SESF_B     [NULL_CODE] = "B";   /* All Rules */


/* SHDL = Shipment Discrepancy Labels */
const char SHDL[NULL_CODE] = "SHDL";

const char SHDL_IS    [NULL_CODE] = "IS";   /* Item Status */
const char SHDL_MQ    [NULL_CODE] = "MQ";   /* Match Qty. */
const char SHDL_QE    [NULL_CODE] = "QE";   /* Qty. Exp'd */
const char SHDL_QA    [NULL_CODE] = "QA";   /* ASN Qty. */


/* SHMD = Schedule Modes */
const char SHMD[NULL_CODE] = "SHMD";

const char SHMD_S     [NULL_CODE] = "S";   /* Signal Driven */
const char SHMD_T     [NULL_CODE] = "T";   /* Time Driven */


/* SHMT = Shipment Payment Terms */
const char SHMT[NULL_CODE] = "SHMT";

const char SHMT_CC    [NULL_CODE] = "CC";   /* Collect */
const char SHMT_CF    [NULL_CODE] = "CF";   /* Collected Freight Credited Back to Cust */
const char SHMT_DF    [NULL_CODE] = "DF";   /* Defined by Buyer and Seller */
const char SHMT_MX    [NULL_CODE] = "MX";   /* Mixed */
const char SHMT_PC    [NULL_CODE] = "PC";   /* Prepaid but Charged to Customer */
const char SHMT_PO    [NULL_CODE] = "PO";   /* Prepaid Only */
const char SHMT_PP    [NULL_CODE] = "PP";   /* Prepaid by Seller */


/* SHOR = Shipment Origin */
const char SHOR[NULL_CODE] = "SHOR";

const char SHOR_0     [NULL_CODE] = "0";   /* ASN Shipments */
const char SHOR_1     [NULL_CODE] = "1";   /* Manual Shipment */
const char SHOR_2     [NULL_CODE] = "2";   /* Autoship */
const char SHOR_3     [NULL_CODE] = "3";   /* System */
const char SHOR_4     [NULL_CODE] = "4";   /* System ASN */
const char SHOR_5     [NULL_CODE] = "5";   /* System UCC-128 */
const char SHOR_6     [NULL_CODE] = "6";   /* ASN UCC-128 */


/* SHPM = Ship Method */
const char SHPM[NULL_CODE] = "SHPM";

const char SHPM_10    [NULL_CODE] = "10";   /* Vessel, Non-container */
const char SHPM_11    [NULL_CODE] = "11";   /* Vessel, Container */
const char SHPM_12    [NULL_CODE] = "12";   /* Border Water-borne (Only Mex. and Can.) */
const char SHPM_20    [NULL_CODE] = "20";   /* Rail, Non-container */
const char SHPM_21    [NULL_CODE] = "21";   /* Rail, Container */
const char SHPM_30    [NULL_CODE] = "30";   /* Truck, Non-container */
const char SHPM_31    [NULL_CODE] = "31";   /* Truck, Container */
const char SHPM_32    [NULL_CODE] = "32";   /* Auto */
const char SHPM_33    [NULL_CODE] = "33";   /* Pedestrian */
const char SHPM_34    [NULL_CODE] = "34";   /* Road, other, incl. foot and animal borne */
const char SHPM_40    [NULL_CODE] = "40";   /* Air, Non-container */
const char SHPM_41    [NULL_CODE] = "41";   /* Air, container */
const char SHPM_50    [NULL_CODE] = "50";   /* Mail */
const char SHPM_60    [NULL_CODE] = "60";   /* Passenger, Hand carried */
const char SHPM_70    [NULL_CODE] = "70";   /* Fixed Transportation Installation */
const char SHPM_80    [NULL_CODE] = "80";   /* Not used at this time */


/* SHPO = ASN Shipment */
const char SHPO[NULL_CODE] = "SHPO";

const char SHPO_0     [NULL_CODE] = "0";   /* ASN Shipment */
const char SHPO_1     [NULL_CODE] = "1";   /* Manual Shipment */
const char SHPO_2     [NULL_CODE] = "2";   /* Autoship */
const char SHPO_3     [NULL_CODE] = "3";   /* System */
const char SHPO_4     [NULL_CODE] = "4";   /* System ASN */
const char SHPO_5     [NULL_CODE] = "5";   /* System UCC-128 */
const char SHPO_6     [NULL_CODE] = "6";   /* ASN UCC-128 */


/* SHPS = Shipment Status */
const char SHPS[NULL_CODE] = "SHPS";

const char SHPS_U     [NULL_CODE] = "U";   /* Unmatched */
const char SHPS_R     [NULL_CODE] = "R";   /* Received */
const char SHPS_I     [NULL_CODE] = "I";   /* Input */
const char SHPS_E     [NULL_CODE] = "E";   /* Extracted */
const char SHPS_P     [NULL_CODE] = "P";   /* Dispatched */
const char SHPS_T     [NULL_CODE] = "T";   /* Tickets Printed */
const char SHPS_V     [NULL_CODE] = "V";   /* Invoice Entered */


/* SHST = Shipment Status Code */
const char SHST[NULL_CODE] = "SHST";

const char SHST_I     [NULL_CODE] = "I";   /* Input */
const char SHST_T     [NULL_CODE] = "T";   /* Tickets Printed */
const char SHST_V     [NULL_CODE] = "V";   /* Invoice Entered */
const char SHST_E     [NULL_CODE] = "E";   /* Extracted */
const char SHST_P     [NULL_CODE] = "P";   /* Dispatched */
const char SHST_R     [NULL_CODE] = "R";   /* Received */
const char SHST_C     [NULL_CODE] = "C";   /* Cancelled */
const char SHST_D     [NULL_CODE] = "D";   /* Delete at POS */
const char SHST_U     [NULL_CODE] = "U";   /* Unmatched */


/* SHTO = Shipment Types */
const char SHTO[NULL_CODE] = "SHTO";

const char SHTO_O     [NULL_CODE] = "O";   /* Order */
const char SHTO_T     [NULL_CODE] = "T";   /* Transfer */
const char SHTO_C     [NULL_CODE] = "C";   /* Customer Order */


/* SIGN = Positive/Negative number indicator */
const char SIGN[NULL_CODE] = "SIGN";

const char SIGN_P     [NULL_CODE] = "P";   /* Positive */
const char SIGN_N     [NULL_CODE] = "N";   /* Negative */


/* SIOP = Simple Relational Operators */
const char SIOP[NULL_CODE] = "SIOP";

const char SIOP_EQ    [NULL_CODE] = "=";   /* = */
const char SIOP_GT    [NULL_CODE] = ">";   /* > */
const char SIOP_LT    [NULL_CODE] = "<";   /* < */


/* SIST = Sales Type For Store/Sales Issues */
const char SIST[NULL_CODE] = "SIST";

const char SIST_R     [NULL_CODE] = "R";   /* Regular */
const char SIST_P     [NULL_CODE] = "P";   /* Promotional */
const char SIST_C     [NULL_CODE] = "C";   /* Clearance */
const char SIST_I     [NULL_CODE] = "I";   /* Issues From Warehouses to Stores */


/* SIZE = Size Labels */
const char SIZE[NULL_CODE] = "SIZE";

const char SIZE_1     [NULL_CODE] = "1";   /* Size 1 */
const char SIZE_2     [NULL_CODE] = "2";   /* Size 2 */


/* SKLB = Skusupp form labels */
const char SKLB[NULL_CODE] = "SKLB";

const char SKLB_I     [NULL_CODE] = "I";   /* Item */
const char SKLB_P     [NULL_CODE] = "P";   /* Primary Supplier */
const char SKLB_S     [NULL_CODE] = "S";   /* Supplier */
const char SKLB_SL    [NULL_CODE] = "SL";   /* Supplier Label */
const char SKLB_SS    [NULL_CODE] = "SS";   /* Supplier Style Code */
const char SKLB_U     [NULL_CODE] = "U";   /* Child Item */
const char SKLB_V     [NULL_CODE] = "V";   /* VPN */


/* SKLL = Stock Ledger Location Level */
const char SKLL[NULL_CODE] = "SKLL";

const char SKLL_S     [NULL_CODE] = "S";   /* Each Location */
const char SKLL_T     [NULL_CODE] = "T";   /* Total Locations */


/* SKPL = Stock Ledger Product Level */
const char SKPL[NULL_CODE] = "SKPL";

const char SKPL_D     [NULL_CODE] = "D";   /* @MH4@ */
const char SKPL_C     [NULL_CODE] = "C";   /* @MH5@ */
const char SKPL_S     [NULL_CODE] = "S";   /* @MH6@ */
const char SKPL_K     [NULL_CODE] = "K";   /* Item */


/* SKTL = Stock Ledger Time Level */
const char SKTL[NULL_CODE] = "SKTL";

const char SKTL_M     [NULL_CODE] = "M";   /* Month */
const char SKTL_W     [NULL_CODE] = "W";   /* Week */


/* SLTP = Sales Type (plus temp) */
const char SLTP[NULL_CODE] = "SLTP";

const char SLTP_R     [NULL_CODE] = "R";   /* Regular */
const char SLTP_P     [NULL_CODE] = "P";   /* Promotional */
const char SLTP_C     [NULL_CODE] = "C";   /* Clearance */
const char SLTP_T     [NULL_CODE] = "T";   /* Temporary */


/* SLVL = Stock Level */
const char SLVL[NULL_CODE] = "SLVL";

const char SLVL_L     [NULL_CODE] = "L";   /* 85 */
const char SLVL_H     [NULL_CODE] = "H";   /* 98 */


/* SOAT = Stock Order Reconcile Adjust Type */
const char SOAT[NULL_CODE] = "SOAT";

const char SOAT_SL    [NULL_CODE] = "SL";   /* Shipping Location */
const char SOAT_RL    [NULL_CODE] = "RL";   /* Receiving Location */
const char SOAT_FC    [NULL_CODE] = "FC";   /* Freight Claim */
const char SOAT_FR    [NULL_CODE] = "FR";   /* Forced Receipt */
const char SOAT_RE    [NULL_CODE] = "RE";   /* Received Elsewhere */


const char SOCASS_7000  [NULL_CODE] = "7000";   /* Food Stamps */
const char SOCASS_7010  [NULL_CODE] = "7010";   /* Electronic Benefits System -EBS */


/* SOLT = Stock Order Reconcile Location Types */
const char SOLT[NULL_CODE] = "SOLT";

const char SOLT_S     [NULL_CODE] = "S";   /* Store */
const char SOLT_W     [NULL_CODE] = "W";   /* Warehouse */
const char SOLT_PW    [NULL_CODE] = "PW";   /* Physical Warehouse */
const char SOLT_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char SOLT_E     [NULL_CODE] = "E";   /* External Finisher */
const char SOLT_C     [NULL_CODE] = "C";   /* Store Class */
const char SOLT_R     [NULL_CODE] = "R";   /* @OH4@ */
const char SOLT_A     [NULL_CODE] = "A";   /* @OH3@ */
const char SOLT_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char SOLT_L     [NULL_CODE] = "L";   /* Location Trait */
const char SOLT_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char SOLT_LLS   [NULL_CODE] = "LLS";   /* Loc List Store */
const char SOLT_LLW   [NULL_CODE] = "LLW";   /* Loc List Warehouse */


/* SOMD = Sourcing Methods for Item-Stores */
const char SOMD[NULL_CODE] = "SOMD";

const char SOMD_S     [NULL_CODE] = "S";   /* Supplier */
const char SOMD_W     [NULL_CODE] = "W";   /* Warehouse */


/* SORC = Source types for delivery schedules */
const char SORC[NULL_CODE] = "SORC";

const char SORC_SUP   [NULL_CODE] = "SUP";   /* Supplier */
const char SORC_W     [NULL_CODE] = "W";   /* Warehouse */


/* SPCS = Item Supplier Primary Case Sizes */
const char SPCS[NULL_CODE] = "SPCS";

const char SPCS_S     [NULL_CODE] = "S";   /* Standard Unit Of Measure */
const char SPCS_I     [NULL_CODE] = "I";   /* Inner */
const char SPCS_C     [NULL_CODE] = "C";   /* Case */
const char SPCS_P     [NULL_CODE] = "P";   /* Pallet */


/* SPQL = Supplier Quantity Level */
const char SPQL[NULL_CODE] = "SPQL";

const char SPQL_CA    [NULL_CODE] = "CA";   /* Cases */
const char SPQL_EA    [NULL_CODE] = "EA";   /* Eaches */


/* SPSC = Supplier Settlement Codes */
const char SPSC[NULL_CODE] = "SPSC";

const char SPSC_E     [NULL_CODE] = "E";   /* Evaluated Receipts Settlement */
const char SPSC_N     [NULL_CODE] = "N";   /* N/A */


/* SPST = Supplier Status */
const char SPST[NULL_CODE] = "SPST";

const char SPST_A     [NULL_CODE] = "A";   /* Active */
const char SPST_I     [NULL_CODE] = "I";   /* Inactive */


/* SQST = Shipment Item Quality Status */
const char SQST[NULL_CODE] = "SQST";

const char SQST_N     [NULL_CODE] = "N";   /* Not Required */
const char SQST_R     [NULL_CODE] = "R";   /* Required */
const char SQST_P     [NULL_CODE] = "P";   /* Passed */
const char SQST_F     [NULL_CODE] = "F";   /* Failed */
const char SQST_V     [NULL_CODE] = "V";   /* Returned to Vendor */


/* SRAT = Size Ratio Apply Type */
const char SRAT[NULL_CODE] = "SRAT";

const char SRAT_ST    [NULL_CODE] = "ST";   /* Store */
const char SRAT_SU    [NULL_CODE] = "SU";   /* @MH6@ */


/* SRCT = Source Types */
const char SRCT[NULL_CODE] = "SRCT";

const char SRCT_R     [NULL_CODE] = "R";   /* Replenishment */
const char SRCT_I     [NULL_CODE] = "I";   /* Investment Buy */
const char SRCT_M     [NULL_CODE] = "M";   /* Manual */


/* SREC = Sales Audit Record Type Group */
const char SREC[NULL_CODE] = "SREC";

const char SREC_THEAD [NULL_CODE] = "THEAD";   /* Transaction Header Level Error */
const char SREC_TITEM [NULL_CODE] = "TITEM";   /* Transaction Item Level Error */
const char SREC_TTEND [NULL_CODE] = "TTEND";   /* Transaction Tender Level Error */
const char SREC_TTAX  [NULL_CODE] = "TTAX";   /* Transaction Tax Level Error */
const char SREC_IDISC [NULL_CODE] = "IDISC";   /* Item Discount Level Error */
const char SREC_TCUST [NULL_CODE] = "TCUST";   /* Transaction Customer Level Error */
const char SREC_TOTAL [NULL_CODE] = "TOTAL";   /* Total Level Error */
const char SREC_BALG  [NULL_CODE] = "BALG";   /* Balance Group Level Error */
const char SREC_STORE [NULL_CODE] = "STORE";   /* Store Day Level Error */
const char SREC_IGTAX [NULL_CODE] = "IGTAX";   /* Item Level Tax Error */
const char SREC_TPYMT [NULL_CODE] = "TPYMT";   /* Transaction Payment Level Error */


/* SRLB = Store Form Labels */
const char SRLB[NULL_CODE] = "SRLB";

const char SRLB_SF    [NULL_CODE] = "SF";   /* Sq Ft */
const char SRLB_SM    [NULL_CODE] = "SM";   /* Sq M */


/* SRPA = Sales Audit Rule execution based on presence or absence of the rule */
const char SRPA[NULL_CODE] = "SRPA";

const char SRPA_P     [NULL_CODE] = "P";   /* Presence */
const char SRPA_A     [NULL_CODE] = "A";   /* Absence */


/* SRPT = Size Ratio Period Types */
const char SRPT[NULL_CODE] = "SRPT";

const char SRPT_D     [NULL_CODE] = "D";   /* Date to Date */
const char SRPT_L     [NULL_CODE] = "L";   /* Last Weeks */
const char SRPT_W     [NULL_CODE] = "W";   /* Weeks to Date */


/* SRRT = Sales Audit Rule Realm Types */
const char SRRT[NULL_CODE] = "SRRT";

const char SRRT_A     [NULL_CODE] = "A";   /* All */
const char SRRT_T     [NULL_CODE] = "T";   /* Table */
const char SRRT_V     [NULL_CODE] = "V";   /* View */
const char SRRT_TOTAL [NULL_CODE] = "TOTAL";   /* Total */


/* SRTP = Storage Type */
const char SRTP[NULL_CODE] = "SRTP";

const char SRTP_W     [NULL_CODE] = "W";   /* Warehouse */
const char SRTP_O     [NULL_CODE] = "O";   /* Outside */


/* SRTY = Stock Request Store Types */
const char SRTY[NULL_CODE] = "SRTY";

const char SRTY_S     [NULL_CODE] = "S";   /* Store */
const char SRTY_C     [NULL_CODE] = "C";   /* Store Class */
const char SRTY_A     [NULL_CODE] = "A";   /* All Stores */


/* SSBK = Supplier Item Bracket */
const char SSBK[NULL_CODE] = "SSBK";

const char SSBK_AMNT  [NULL_CODE] = "AMNT";   /* Amount */
const char SSBK_CUBE  [NULL_CODE] = "CUBE";   /* Cube */
const char SSBK_WGHT  [NULL_CODE] = "WGHT";   /* Weight */
const char SSBK_QTY   [NULL_CODE] = "QTY";   /* Quantity */


/* SSEF = Supplier, Site and External Finisher */
const char SSEF[NULL_CODE] = "SSEF";

const char SSEF_S     [NULL_CODE] = "S";   /* Supplier */
const char SSEF_U     [NULL_CODE] = "U";   /* Supplier Site */
const char SSEF_E     [NULL_CODE] = "E";   /* External Finisher */


/* SSSA = Accept, Reject, and Hold */
const char SSSA[NULL_CODE] = "SSSA";

const char SSSA_A     [NULL_CODE] = "A";   /* Accept */
const char SSSA_R     [NULL_CODE] = "R";   /* Reject */
const char SSSA_H     [NULL_CODE] = "H";   /* Hold */


/* SSSB = Accept, Reject or Hold */
const char SSSB[NULL_CODE] = "SSSB";

const char SSSB_A     [NULL_CODE] = "A";   /* Accept */
const char SSSB_R     [NULL_CODE] = "R";   /* Reject */
const char SSSB_H     [NULL_CODE] = "H";   /* Hold */
const char SSSB_X     [NULL_CODE] = "X";   /* All */


/* SSST = Shipment Item Status */
const char SSST[NULL_CODE] = "SSST";

const char SSST_C     [NULL_CODE] = "C";   /* Carton Received */
const char SSST_A     [NULL_CODE] = "A";   /* Accept */
const char SSST_R     [NULL_CODE] = "R";   /* Reject */
const char SSST_H     [NULL_CODE] = "H";   /* Hold */


/* SSTP = Store Add Group Type--Store priced Item */
const char SSTP[NULL_CODE] = "SSTP";

const char SSTP_S     [NULL_CODE] = "S";   /* Store */
const char SSTP_C     [NULL_CODE] = "C";   /* Store Class */
const char SSTP_D     [NULL_CODE] = "D";   /* @OH5@ */
const char SSTP_R     [NULL_CODE] = "R";   /* @OH4@ */
const char SSTP_MA    [NULL_CODE] = "MA";   /* Min Area */
const char SSTP_MX    [NULL_CODE] = "MX";   /* Max Area */
const char SSTP_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char SSTP_L     [NULL_CODE] = "L";   /* Location Trait */
const char SSTP_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char SSTP_A     [NULL_CODE] = "A";   /* All Stores */


/* SSTY = Supplier Site Code */
const char SSTY[NULL_CODE] = "SSTY";

const char SSTY_CCOSS [NULL_CODE] = "CCOSS";   /* Case Cost
(Sup. Site) */

const char SSTY_CURSS [NULL_CODE] = "CURSS";   /* Currency
(Sup. Site) */

const char SSTY_PLSS  [NULL_CODE] = "PLSS";   /* Pool Supplier Site */
const char SSTY_PLSSN [NULL_CODE] = "PLSSN";   /* Pool Supplier Site Name */
const char SSTY_POLSS [NULL_CODE] = "POLSS";   /* Pooled
Supplier Site */

const char SSTY_PSLS  [NULL_CODE] = "PSLS";   /* Primary Supplier Site */
const char SSTY_PSSOC [NULL_CODE] = "PSSOC";   /* Prescaled Supplier Site
Order Cost */

const char SSTY_PTSS  [NULL_CODE] = "PTSS";   /* Partner/
Supplier Site */

const char SSTY_SS    [NULL_CODE] = "SS";   /* Supplier Site */
const char SSTY_SSA   [NULL_CODE] = "SSA";   /* Supplier Site Availability */
const char SSTY_SSCU  [NULL_CODE] = "SSCU";   /* Sup. Site Cost UOP */
const char SSTY_SSCUP1[NULL_CODE] = "SSCUP1";   /* Supplier Site Cost
UOP Primary */

const char SSTY_SSD   [NULL_CODE] = "SSD";   /* Supplier Site Description */
const char SSTY_SSDIF [NULL_CODE] = "SSDIF";   /* Sup. Site Diff */
const char SSTY_SSDIF1[NULL_CODE] = "SSDIF1";   /* Sup. Site
Diff */

const char SSTY_SSITES[NULL_CODE] = "SSITES";   /* Supplier Sites */
const char SSTY_SSL   [NULL_CODE] = "SSL";   /* Supplier Site Label */
const char SSTY_SSLDTM[NULL_CODE] = "SSLDTM";   /* Sup. Site Lead Time */
const char SSTY_SSN   [NULL_CODE] = "SSN";   /* Supplier Site Name */
const char SSTY_SSOC1 [NULL_CODE] = "SSOC1";   /* Supplier Site Order Cost */
const char SSTY_SSPD  [NULL_CODE] = "SSPD";   /* Supplier Site Pack Desc */
const char SSTY_SSPS1 [NULL_CODE] = "SSPS1";   /* Supplier Site Pack Size */
const char SSTY_SSPT  [NULL_CODE] = "SSPT";   /* Sup. Site Payment Type */
const char SSTY_SSUC  [NULL_CODE] = "SSUC";   /* Supplier Site
Unit Cost */

const char SSTY_SUPP  [NULL_CODE] = "SUPP";   /* Supplier */
const char SSTY_SUPST [NULL_CODE] = "SUPST";   /* Sup. Site */
const char SSTY_UCOSS [NULL_CODE] = "UCOSS";   /* Unit Cost
(Sup. Site) */

const char SSTY_UCSS1 [NULL_CODE] = "UCSS1";   /* Unit Cost
(Supplier Site) */

const char SSTY_SSCA  [NULL_CODE] = "SSCA";   /* Supplier Site Cost Adjustment */
const char SSTY_SSN1  [NULL_CODE] = "SSN1";   /* Sup. Site Name */
const char SSTY_SSO   [NULL_CODE] = "SSO";   /* Supplier Site - Order */
const char SSTY_SD    [NULL_CODE] = "SD";   /* Supplier Site - Delivery */


/* STAT = RMS Standard Statuses */
const char STAT[NULL_CODE] = "STAT";

const char STAT_W     [NULL_CODE] = "W";   /* Worksheet */
const char STAT_S     [NULL_CODE] = "S";   /* Submitted */
const char STAT_A     [NULL_CODE] = "A";   /* Approved */
const char STAT_R     [NULL_CODE] = "R";   /* Rejected */
const char STAT_D     [NULL_CODE] = "D";   /* Deleted */


/* STC2 = Item Stock Category - Store */
const char STC2[NULL_CODE] = "STC2";

const char STC2_D     [NULL_CODE] = "D";   /* Direct To Store Merchandise */


/* STCA = Sales Audit Store/Cashier */
const char STCA[NULL_CODE] = "STCA";

const char STCA_S     [NULL_CODE] = "S";   /* Store */
const char STCA_C     [NULL_CODE] = "C";   /* Cashier */


/* STFV = Supplier Invoice Match Tolerance Favor */
const char STFV[NULL_CODE] = "STFV";

const char STFV_S     [NULL_CODE] = "S";   /* Supplier */
const char STFV_R     [NULL_CODE] = "R";   /* Retailer */


/* STGR = Sales Audit Store Groupings */
const char STGR[NULL_CODE] = "STGR";

const char STGR_A     [NULL_CODE] = "A";   /* All Stores */
const char STGR_S     [NULL_CODE] = "S";   /* Assigned Stores */
const char STGR_I     [NULL_CODE] = "I";   /* Individual Store */


/* STKL = Stock Count Level */
const char STKL[NULL_CODE] = "STKL";

const char STKL_A     [NULL_CODE] = "A";   /* All @MHP4@ */
const char STKL_D     [NULL_CODE] = "D";   /* @MH4@ */
const char STKL_C     [NULL_CODE] = "C";   /* @MH5@ */
const char STKL_S     [NULL_CODE] = "S";   /* @MH6@ */
const char STKL_N     [NULL_CODE] = "N";   /* Non-Specific */


/* STKT = Stock Ledger Download Templates */
const char STKT[NULL_CODE] = "STKT";

const char STKT_CTMPL [NULL_CODE] = "CTMPL";   /* Cost Template */
const char STKT_RTMPL [NULL_CODE] = "RTMPL";   /* Retail Template */
const char STKT_CRTMPL[NULL_CODE] = "CRTMPL";   /* Cost-Retail Template */


/* STLB = Inventory Stock Label */
const char STLB[NULL_CODE] = "STLB";

const char STLB_0     [NULL_CODE] = "0";   /* Total Stock(SUOM) */
const char STLB_1     [NULL_CODE] = "1";   /* Unavailable Stock(SUOM) */


/* STLL = Supivmgmt Loc Type */
const char STLL[NULL_CODE] = "STLL";

const char STLL_ST    [NULL_CODE] = "ST";   /* Store */
const char STLL_WH    [NULL_CODE] = "WH";   /* Warehouse */
const char STLL_SL    [NULL_CODE] = "SL";   /* Location List */


/* STLV = Stock Level */
const char STLV[NULL_CODE] = "STLV";

const char STLV_H     [NULL_CODE] = "H";   /* High */
const char STLV_L     [NULL_CODE] = "L";   /* Low */


/* STMB = In Store Market Basket */
const char STMB[NULL_CODE] = "STMB";

const char STMB_A     [NULL_CODE] = "A";   /* A item at this loc */
const char STMB_B     [NULL_CODE] = "B";   /* B item at this loc */
const char STMB_C     [NULL_CODE] = "C";   /* C item at this loc */


/* STMO = Status Modification Messages */
const char STMO[NULL_CODE] = "STMO";

const char STMO_CSWS  [NULL_CODE] = "CSWS";   /* Change status Worksheet to Submitted: */
const char STMO_CSSW  [NULL_CODE] = "CSSW";   /* Change status Submitted to Worksheet: */
const char STMO_CSSA  [NULL_CODE] = "CSSA";   /* Change status Submitted to Approved: */


/* STNA = Store Editor Titles */
const char STNA[NULL_CODE] = "STNA";

const char STNA_SSTNA [NULL_CODE] = "SSTNA";   /* Secondary Store Name */


/* STRC = Store Receive Type */
const char STRC[NULL_CODE] = "STRC";

const char STRC_A     [NULL_CODE] = "A";   /* Auto Receive */
const char STRC_B     [NULL_CODE] = "B";   /* Receive by BOL */
const char STRC_C     [NULL_CODE] = "C";   /* Receive by Carton */


/* STRG = Sales Audit Store/Register */
const char STRG[NULL_CODE] = "STRG";

const char STRG_S     [NULL_CODE] = "S";   /* Store */
const char STRG_R     [NULL_CODE] = "R";   /* Register */


/* STRT = Start Element Types */
const char STRT[NULL_CODE] = "STRT";

const char STRT_F     [NULL_CODE] = "F";   /* Form */
const char STRT_W     [NULL_CODE] = "W";   /* Web Page */
const char STRT_V     [NULL_CODE] = "V";   /* Desktop View */
const char STRT_S     [NULL_CODE] = "S";   /* Internal Item */
const char STRT_A     [NULL_CODE] = "A";   /* User Application */
const char STRT_R     [NULL_CODE] = "R";   /* Oracle Report */


/* STS3 = Item Status (no delete) */
const char STS3[NULL_CODE] = "STS3";

const char STS3_A     [NULL_CODE] = "A";   /* Active */
const char STS3_I     [NULL_CODE] = "I";   /* Inactive */
const char STS3_C     [NULL_CODE] = "C";   /* Discontinued */


/* STSF = Sales Total Status Filter */
const char STSF[NULL_CODE] = "STSF";

const char STSF_A     [NULL_CODE] = "A";   /* Approved Totals */
const char STSF_W     [NULL_CODE] = "W";   /* Non-Approved Totals */
const char STSF_B     [NULL_CODE] = "B";   /* All Totals */


/* STSS = Supplier Site Source Types for delivery schedules */
const char STSS[NULL_CODE] = "STSS";

const char STSS_SUP   [NULL_CODE] = "SUP";   /* Supplier Site */
const char STSS_W     [NULL_CODE] = "W";   /* Warehouse */


/* STST = Item Status */
const char STST[NULL_CODE] = "STST";

const char STST_A     [NULL_CODE] = "A";   /* Active */
const char STST_I     [NULL_CODE] = "I";   /* Inactive */
const char STST_C     [NULL_CODE] = "C";   /* Discontinued */
const char STST_D     [NULL_CODE] = "D";   /* Delete */


/* STTA = Store Task Type (Abbreviated) */
const char STTA[NULL_CODE] = "STTA";

const char STTA_REC   [NULL_CODE] = "REC";   /* Shipment */
const char STTA_TSF   [NULL_CODE] = "TSF";   /* Transfer */
const char STTA_RPC   [NULL_CODE] = "RPC";   /* Price Chg. */
const char STTA_RTV   [NULL_CODE] = "RTV";   /* RTV */
const char STTA_CNT   [NULL_CODE] = "CNT";   /* Stock Cnt. */


/* STTL = Stocktake Level */
const char STTL[NULL_CODE] = "STTL";

const char STTL_D     [NULL_CODE] = "D";   /* @MH4@ */
const char STTL_C     [NULL_CODE] = "C";   /* @MH5@ */
const char STTL_S     [NULL_CODE] = "S";   /* @MH6@ */
const char STTL_N     [NULL_CODE] = "N";   /* Non-Specific */


/* STTP = Stocktake Type */
const char STTP[NULL_CODE] = "STTP";

const char STTP_B     [NULL_CODE] = "B";   /* Book Count */
const char STTP_U     [NULL_CODE] = "U";   /* Cycle Count */


/* STTT = Supplier Invoice Match Tolerance Type */
const char STTT[NULL_CODE] = "STTT";

const char STTT_A     [NULL_CODE] = "A";   /* Amount */
const char STTT_P     [NULL_CODE] = "P";   /* Percent */


/* STYP = Sale Type */
const char STYP[NULL_CODE] = "STYP";

const char STYP_L     [NULL_CODE] = "L";   /* Loose Weight */
const char STYP_V     [NULL_CODE] = "V";   /* Variable Weight Each */


/* SUAP = Submit/Approve */
const char SUAP[NULL_CODE] = "SUAP";

const char SUAP_S     [NULL_CODE] = "S";   /* Submit */
const char SUAP_A     [NULL_CODE] = "A";   /* Approve */


/* SUBR = Substitution reason codes */
const char SUBR[NULL_CODE] = "SUBR";

const char SUBR_P     [NULL_CODE] = "P";   /* Promotional */
const char SUBR_T     [NULL_CODE] = "T";   /* Transitional */


/* SUCO = Surety Codes */
const char SUCO[NULL_CODE] = "SUCO";

const char SUCO_1     [NULL_CODE] = "1";   /* Surety Code 1 */
const char SUCO_2     [NULL_CODE] = "2";   /* Surety Code 2 */
const char SUCO_3     [NULL_CODE] = "3";   /* Surety Code 3 */


/* SUH2 = Deal Partner Types */
const char SUH2[NULL_CODE] = "SUH2";

const char SUH2_S1    [NULL_CODE] = "S1";   /* @SUH1@ */
const char SUH2_S2    [NULL_CODE] = "S2";   /* @SUH2@ */
const char SUH2_S3    [NULL_CODE] = "S3";   /* @SUH3@ */
const char SUH2_S     [NULL_CODE] = "S";   /* Supplier */


/* SUHL = Supplier Hierarchy Levels */
const char SUHL[NULL_CODE] = "SUHL";

const char SUHL_S1    [NULL_CODE] = "S1";   /* @SUH1@ */
const char SUHL_S2    [NULL_CODE] = "S2";   /* @SUH2@ */
const char SUHL_S3    [NULL_CODE] = "S3";   /* @SUH3@ */
const char SUHL_S     [NULL_CODE] = "S";   /* Supplier */


/* SUPC = Supplier Contact Defaults */
const char SUPC[NULL_CODE] = "SUPC";

const char SUPC_NAME  [NULL_CODE] = "NAME";   /* Contact name not available. */
const char SUPC_PHONE [NULL_CODE] = "PHONE";   /* Contact phone not available. */


/* SUPD = Split Replenishment Orders Indicator */
const char SUPD[NULL_CODE] = "SUPD";

const char SUPD_S     [NULL_CODE] = "S";   /* Supplier */
const char SUPD_D     [NULL_CODE] = "D";   /* Supp/@MH4@ */
const char SUPD_L     [NULL_CODE] = "L";   /* Supp/Loc */
const char SUPD_A     [NULL_CODE] = "A";   /* Supp/@MH4@/Loc */


/* SUPT = Supplier Types */
const char SUPT[NULL_CODE] = "SUPT";

const char SUPT_S     [NULL_CODE] = "S";   /* Supplier */
const char SUPT_P     [NULL_CODE] = "P";   /* Pool Supplier */


/* SUPV = Supplier Violations */
const char SUPV[NULL_CODE] = "SUPV";

const char SUPV_N     [NULL_CODE] = "N";   /* Not as expected */
const char SUPV_C     [NULL_CODE] = "C";   /* Carton labels missing */
const char SUPV_S     [NULL_CODE] = "S";   /* Shipped to wrong location */


/* SUTL = Supplier Invoice Match Tolerance Level */
const char SUTL[NULL_CODE] = "SUTL";

const char SUTL_TC    [NULL_CODE] = "TC";   /* Summary Cost */
const char SUTL_TQ    [NULL_CODE] = "TQ";   /* Summary Qty. */
const char SUTL_LC    [NULL_CODE] = "LC";   /* Line Item Cost */
const char SUTL_LQ    [NULL_CODE] = "LQ";   /* Line Item Qty. */


/* SUTT = Supplier Transaction Type */
const char SUTT[NULL_CODE] = "SUTT";

const char SUTT_1     [NULL_CODE] = "1";   /* Purchases at Cost */
const char SUTT_2     [NULL_CODE] = "2";   /* Purchases at Retail */
const char SUTT_3     [NULL_CODE] = "3";   /* Claims at Cost */
const char SUTT_10    [NULL_CODE] = "10";   /* Markdowns at Retail */
const char SUTT_20    [NULL_CODE] = "20";   /* Cancellations at Cost */
const char SUTT_30    [NULL_CODE] = "30";   /* Sales at Retail */
const char SUTT_40    [NULL_CODE] = "40";   /* Quantity Failed */
const char SUTT_70    [NULL_CODE] = "70";   /* Markdowns at Cost */


/* SUTY = Supplier Type */
const char SUTY[NULL_CODE] = "SUTY";

const char SUTY_SUP   [NULL_CODE] = "SUP";   /* Merchandise Vendor */
const char SUTY_EV    [NULL_CODE] = "EV";   /* Expense Vendor */
const char SUTY_BK    [NULL_CODE] = "BK";   /* Bank */
const char SUTY_AG    [NULL_CODE] = "AG";   /* Agent */
const char SUTY_FF    [NULL_CODE] = "FF";   /* Freight Forwarder */
const char SUTY_IM    [NULL_CODE] = "IM";   /* Importer */
const char SUTY_IA    [NULL_CODE] = "IA";   /* Import Authority */
const char SUTY_BR    [NULL_CODE] = "BR";   /* Broker */
const char SUTY_FA    [NULL_CODE] = "FA";   /* Factory */
const char SUTY_CO    [NULL_CODE] = "CO";   /* Consolidator */
const char SUTY_AP    [NULL_CODE] = "AP";   /* Applicant */
const char SUTY_CN    [NULL_CODE] = "CN";   /* Consignee */
const char SUTY_S1    [NULL_CODE] = "S1";   /* @SUH1@ */
const char SUTY_S2    [NULL_CODE] = "S2";   /* @SUH2@ */
const char SUTY_S3    [NULL_CODE] = "S3";   /* @SUH3@ */


/* SWDP = Store, Warehouse, Discharge Port */
const char SWDP[NULL_CODE] = "SWDP";

const char SWDP_S     [NULL_CODE] = "S";   /* Store */
const char SWDP_W     [NULL_CODE] = "W";   /* Warehouse */
const char SWDP_DP    [NULL_CODE] = "DP";   /* Discharge Port */


/* SWFT = SWIFT Tags */
const char SWFT[NULL_CODE] = "SWFT";

const char SWFT_46B   [NULL_CODE] = "46B";   /* Required Document SWIFT Tag */
const char SWFT_71B   [NULL_CODE] = "71B";   /* Charges SWIFT Tag */
const char SWFT_78    [NULL_CODE] = "78";   /* Bank Instructions SWIFT Tag */
const char SWFT_72    [NULL_CODE] = "72";   /* Sender Instructions SWIFT Tag */
const char SWFT_47B   [NULL_CODE] = "47B";   /* Additional Instructions SWIFT Tag */


/* SYSE = Sales Audit System Export Interfaces */
const char SYSE[NULL_CODE] = "SYSE";

const char SYSE_RMS   [NULL_CODE] = "RMS";   /* RMS Export */
const char SYSE_RA    [NULL_CODE] = "RA";   /* RA Export */
const char SYSE_GL    [NULL_CODE] = "GL";   /* GL Export */
const char SYSE_SFM   [NULL_CODE] = "SFM";   /* SFM Export */
const char SYSE_ACH   [NULL_CODE] = "ACH";   /* ACH Export */
const char SYSE_UAR   [NULL_CODE] = "UAR";   /* UAR Export */
const char SYSE_IM    [NULL_CODE] = "IM";   /* IM Export */
const char SYSE_ORIN  [NULL_CODE] = "ORIN";   /* ORIN Export */
const char SYSE_SIM   [NULL_CODE] = "SIM";   /* SIM Export */


/* SYSG = System Generated */
const char SYSG[NULL_CODE] = "SYSG";

const char SYSG_S     [NULL_CODE] = "S";   /* System */
const char SYSG_M     [NULL_CODE] = "M";   /* Manual */


/* SYSI = Sales Audit System Import Interfaces */
const char SYSI[NULL_CODE] = "SYSI";

const char SYSI_POS   [NULL_CODE] = "POS";   /* Sales Import */
const char SYSI_SFM   [NULL_CODE] = "SFM";   /* SFM Import */
const char SYSI_UAR   [NULL_CODE] = "UAR";   /* UAR Import */
const char SYSI_IGTAX [NULL_CODE] = "IGTAX";   /* Item Level Tax */


/* SZTP = Store Add Group Type--Zone priced SKU */
const char SZTP[NULL_CODE] = "SZTP";

const char SZTP_S     [NULL_CODE] = "S";   /* Store */
const char SZTP_C     [NULL_CODE] = "C";   /* Store Class */
const char SZTP_D     [NULL_CODE] = "D";   /* @OH5@ */
const char SZTP_R     [NULL_CODE] = "R";   /* @OH4@ */
const char SZTP_MA    [NULL_CODE] = "MA";   /* Min Area */
const char SZTP_MX    [NULL_CODE] = "MX";   /* Max Area */
const char SZTP_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char SZTP_L     [NULL_CODE] = "L";   /* Location Trait */
const char SZTP_DW    [NULL_CODE] = "DW";   /* Default Warehouse */
const char SZTP_A     [NULL_CODE] = "A";   /* All Stores */


/* SZTT = Size Template Type */
const char SZTT[NULL_CODE] = "SZTT";

const char SZTT_P     [NULL_CODE] = "P";   /* Percentage */
const char SZTT_R     [NULL_CODE] = "R";   /* Ratio */


/* T1IT = Tran Level 1 Item Level Descriptions */
const char T1IT[NULL_CODE] = "T1IT";

const char T1IT_1     [NULL_CODE] = "1";   /* SKU */
const char T1IT_2     [NULL_CODE] = "2";   /* Reference Item */


/* T2IT = Tran Level 2 Item Level Descriptions */
const char T2IT[NULL_CODE] = "T2IT";

const char T2IT_1     [NULL_CODE] = "1";   /* Style */
const char T2IT_2     [NULL_CODE] = "2";   /* SKU */
const char T2IT_3     [NULL_CODE] = "3";   /* Reference Item */


/* T3IT = Tran Level 3 Item Level Descriptions */
const char T3IT[NULL_CODE] = "T3IT";

const char T3IT_1     [NULL_CODE] = "1";   /* Style */
const char T3IT_2     [NULL_CODE] = "2";   /* SKU */
const char T3IT_3     [NULL_CODE] = "3";   /* Reference Item */


/* TABS = Sales Audit Form Tabs */
const char TABS[NULL_CODE] = "TABS";

const char TABS_I     [NULL_CODE] = "I";   /* Item */
const char TABS_T     [NULL_CODE] = "T";   /* Tender */
const char TABS_E     [NULL_CODE] = "E";   /* Exported */
const char TABS_TX    [NULL_CODE] = "TX";   /* Tax */
const char TABS_C     [NULL_CODE] = "C";   /* Customer */
const char TABS_TA    [NULL_CODE] = "TA";   /* Transaction Attributes */
const char TABS_P     [NULL_CODE] = "P";   /* Paid Out Details */


/* TABT = RMS Notifications Tab Title. */
const char TABT[NULL_CODE] = "TABT";

const char TABT_ITEM  [NULL_CODE] = "ITEM";   /* Item */
const char TABT_PO    [NULL_CODE] = "PO";   /* Order */
const char TABT_TSF   [NULL_CODE] = "TSF";   /* Transfer */
const char TABT_ASYNC [NULL_CODE] = "ASYNC";   /* Asynchronous Job Log */
const char TABT_LOAD  [NULL_CODE] = "LOAD";   /* Data Loading Status */


/* TARE = Tare Weight Type */
const char TARE[NULL_CODE] = "TARE";

const char TARE_W     [NULL_CODE] = "W";   /* Wet */
const char TARE_D     [NULL_CODE] = "D";   /* Dry */


/* TAXC = Sales Audit Tax Codes */
const char TAXC[NULL_CODE] = "TAXC";

const char TAXC_ERR   [NULL_CODE] = "ERR";   /* Error */
const char TAXC_TERM  [NULL_CODE] = "TERM";   /* Termination Record */
const char TAXC_STATE [NULL_CODE] = "STATE";   /* State Tax */
const char TAXC_CITY  [NULL_CODE] = "CITY";   /* City Tax */
const char TAXC_TOTTAX[NULL_CODE] = "TOTTAX";   /* Aggregate total of tax excluding VAT */


/* TCAT = Template Category */
const char TCAT[NULL_CODE] = "TCAT";

const char TCAT_IS9T  [NULL_CODE] = "IS9T";   /* Item API Templates */
const char TCAT_POS9T [NULL_CODE] = "POS9T";   /* Purchase Order API Templates */


/* TCKT = Ticket Type Items */
const char TCKT[NULL_CODE] = "TCKT";

const char TCKT_ITEM  [NULL_CODE] = "ITEM";   /* Item */
const char TCKT_ITDS  [NULL_CODE] = "ITDS";   /* Item Description */
const char TCKT_ITSD  [NULL_CODE] = "ITSD";   /* Item Short Description */
const char TCKT_VAR   [NULL_CODE] = "VAR";   /* Primary Variant */
const char TCKT_DIF1  [NULL_CODE] = "DIF1";   /* Diff 1 */
const char TCKT_DIF2  [NULL_CODE] = "DIF2";   /* Diff 2 */
const char TCKT_WGHT  [NULL_CODE] = "WGHT";   /* Weight */
const char TCKT_DEPT  [NULL_CODE] = "DEPT";   /* @MH4@ */
const char TCKT_CLSS  [NULL_CODE] = "CLSS";   /* @MH5@ */
const char TCKT_SBCL  [NULL_CODE] = "SBCL";   /* @MH6@ */
const char TCKT_RTPC  [NULL_CODE] = "RTPC";   /* Retail Price */
const char TCKT_SRTP  [NULL_CODE] = "SRTP";   /* Suggested Retail Price */
const char TCKT_MUPC  [NULL_CODE] = "MUPC";   /* Multi-unit Price */
const char TCKT_SUPR  [NULL_CODE] = "SUPR";   /* Supplier */
const char TCKT_SUP1  [NULL_CODE] = "SUP1";   /* Supplier Diff 1 */
const char TCKT_SUP2  [NULL_CODE] = "SUP2";   /* Supplier Diff 2 */
const char TCKT_STRE  [NULL_CODE] = "STRE";   /* Store */
const char TCKT_WHSE  [NULL_CODE] = "WHSE";   /* Warehouse */
const char TCKT_COOG  [NULL_CODE] = "COOG";   /* Country Of Sourcing */
const char TCKT_UOM   [NULL_CODE] = "UOM";   /* Price Per Unit of Measure */
const char TCKT_ITPR  [NULL_CODE] = "ITPR";   /* Item Parent ID */
const char TCKT_IPDS  [NULL_CODE] = "IPDS";   /* Item Parent Description */
const char TCKT_EURO  [NULL_CODE] = "EURO";   /* Euro/Original Currency */
const char TCKT_DIF3  [NULL_CODE] = "DIF3";   /* Diff 3 */
const char TCKT_DIF4  [NULL_CODE] = "DIF4";   /* Diff 4 */
const char TCKT_SUP3  [NULL_CODE] = "SUP3";   /* Supplier Diff 3 */
const char TCKT_SUP4  [NULL_CODE] = "SUP4";   /* Supplier Diff 4 */
const char TCKT_NETV  [NULL_CODE] = "NETV";   /* Unit Retail of VAT */
const char TCKT_DTOT  [NULL_CODE] = "DTOT";   /* Total deposit item unit retail */
const char TCKT_DPST  [NULL_CODE] = "DPST";   /* Contents Item container deposit amount */


/* TENT = Sales Audit Tender Type Groups */
const char TENT[NULL_CODE] = "TENT";

const char TENT_ERR   [NULL_CODE] = "ERR";   /* Error */
const char TENT_TERM  [NULL_CODE] = "TERM";   /* Termination Record */
const char TENT_CASH  [NULL_CODE] = "CASH";   /* Cash */
const char TENT_CHECK [NULL_CODE] = "CHECK";   /* Personal Check */
const char TENT_CCARD [NULL_CODE] = "CCARD";   /* Credit Card */
const char TENT_VOUCH [NULL_CODE] = "VOUCH";   /* Voucher (gift cert. or credit) */
const char TENT_COUPON[NULL_CODE] = "COUPON";   /* Coupon */
const char TENT_MORDER[NULL_CODE] = "MORDER";   /* Money Order */
const char TENT_DCARD [NULL_CODE] = "DCARD";   /* Debit Card */
const char TENT_DRIVEO[NULL_CODE] = "DRIVEO";   /* Drive Off */
const char TENT_SOCASS[NULL_CODE] = "SOCASS";   /* Social Assistance */
const char TENT_FONCOT[NULL_CODE] = "FONCOT";   /* Fonacot */
const char TENT_PAYPAL[NULL_CODE] = "PAYPAL";   /* Paypal */
const char TENT_OTHERS[NULL_CODE] = "OTHERS";   /* Others */


/* TERM = Sales Audit Record Termination Marker */
const char TERM[NULL_CODE] = "TERM";

const char TERM_TERM  [NULL_CODE] = "TERM";   /* Termination Record marker */


/* TFML = Transfer Markdown Location */
const char TFML[NULL_CODE] = "TFML";

const char TFML_S     [NULL_CODE] = "S";   /* Sending Location */
const char TFML_R     [NULL_CODE] = "R";   /* Receiving Location */


/* TIME = Before Time */
const char TIME[NULL_CODE] = "TIME";

const char TIME_1     [NULL_CODE] = "1";   /* 1:00 */
const char TIME_2     [NULL_CODE] = "2";   /* 2:00 */
const char TIME_3     [NULL_CODE] = "3";   /* 3:00 */
const char TIME_4     [NULL_CODE] = "4";   /* 4:00 */
const char TIME_5     [NULL_CODE] = "5";   /* 5:00 */
const char TIME_6     [NULL_CODE] = "6";   /* 6:00 */
const char TIME_7     [NULL_CODE] = "7";   /* 7:00 */
const char TIME_8     [NULL_CODE] = "8";   /* 8:00 */
const char TIME_9     [NULL_CODE] = "9";   /* 9:00 */
const char TIME_10    [NULL_CODE] = "10";   /* 10:00 */
const char TIME_11    [NULL_CODE] = "11";   /* 11:00 */
const char TIME_12    [NULL_CODE] = "12";   /* 12:00 */
const char TIME_13    [NULL_CODE] = "13";   /* 13:00 */
const char TIME_14    [NULL_CODE] = "14";   /* 14:00 */
const char TIME_15    [NULL_CODE] = "15";   /* 15:00 */
const char TIME_16    [NULL_CODE] = "16";   /* 16:00 */
const char TIME_17    [NULL_CODE] = "17";   /* 17:00 */
const char TIME_18    [NULL_CODE] = "18";   /* 18:00 */
const char TIME_19    [NULL_CODE] = "19";   /* 19:00 */
const char TIME_20    [NULL_CODE] = "20";   /* 20:00 */
const char TIME_21    [NULL_CODE] = "21";   /* 21:00 */
const char TIME_22    [NULL_CODE] = "22";   /* 22:00 */
const char TIME_23    [NULL_CODE] = "23";   /* 23:00 */
const char TIME_24    [NULL_CODE] = "24";   /* 24:00 */


/* TINT = To Inventory Types */
const char TINT[NULL_CODE] = "TINT";

const char TINT_A     [NULL_CODE] = "A";   /* Available */
const char TINT_S     [NULL_CODE] = "S";   /* Specific Unavailable */


/* TIRC = Timeline Reason Code */
const char TIRC[NULL_CODE] = "TIRC";

const char TIRC_1     [NULL_CODE] = "1";   /* Shipment Arrived Early */
const char TIRC_2     [NULL_CODE] = "2";   /* Shipment Arrived Late */


/* TKIT = Ticket Print Items */
const char TKIT[NULL_CODE] = "TKIT";

const char TKIT_I     [NULL_CODE] = "I";   /* Single Item */
const char TKIT_L     [NULL_CODE] = "L";   /* Item List */
const char TKIT_P     [NULL_CODE] = "P";   /* Purchase Order */


/* TKTA = Ticket Attribute Type */
const char TKTA[NULL_CODE] = "TKTA";

const char TKTA_A     [NULL_CODE] = "A";   /* Attribute */
const char TKTA_U     [NULL_CODE] = "U";   /* User Defined Attribute */


/* TLBC = Timeline Base Code */
const char TLBC[NULL_CODE] = "TLBC";

const char TLBC_APD   [NULL_CODE] = "APD";   /* Approval Date */
const char TLBC_NBD   [NULL_CODE] = "NBD";   /* Not Before Date */
const char TLBC_NAD   [NULL_CODE] = "NAD";   /* Not After Date */
const char TLBC_WRD   [NULL_CODE] = "WRD";   /* Written Date */


/* TLLB = Tools Labels */
const char TLLB[NULL_CODE] = "TLLB";

const char TLLB_SAVING[NULL_CODE] = "SAVING";   /* Please Wait, saving... */
const char TLLB_SETNUM[NULL_CODE] = "SETNUM";   /* Set Number */


/* TLLC = Translation code for default UI label to be used for the field if the label is not available in the UI layer. */
const char TLLC[NULL_CODE] = "TLLC";

const char TLLC_ADDKEY[NULL_CODE] = "ADDKEY";   /* Address Id */
const char TLLC_ADD1  [NULL_CODE] = "ADD1";   /* Line 1 */
const char TLLC_ADD2  [NULL_CODE] = "ADD2";   /* Line 2 */
const char TLLC_ADD3  [NULL_CODE] = "ADD3";   /* Line 3 */
const char TLLC_ACITY [NULL_CODE] = "ACITY";   /* City */
const char TLLC_ACNTC [NULL_CODE] = "ACNTC";   /* Contact Name */
const char TLLC_ACNTY [NULL_CODE] = "ACNTY";   /* County */
const char TLLC_AREA  [NULL_CODE] = "AREA";   /* @OH3@ */
const char TLLC_ARNM  [NULL_CODE] = "ARNM";   /* Name */
const char TLLC_CHAIN [NULL_CODE] = "CHAIN";   /* @OH2@ */
const char TLLC_CHNM  [NULL_CODE] = "CHNM";   /* Name */
const char TLLC_CLDEPT[NULL_CODE] = "CLDEPT";   /* Dept */
const char TLLC_CLASS [NULL_CODE] = "CLASS";   /* @MH5@ */
const char TLLC_CLNM  [NULL_CODE] = "CLNM";   /* Name */
const char TLLC_CZZGI [NULL_CODE] = "CZZGI";   /* Zone Group */
const char TLLC_CZZID [NULL_CODE] = "CZZID";   /* Zone */
const char TLLC_COSTZN[NULL_CODE] = "COSTZN";   /* Description */
const char TLLC_CZGZGI[NULL_CODE] = "CZGZGI";   /* Zone Group */
const char TLLC_CZGDES[NULL_CODE] = "CZGDES";   /* Description */
const char TLLC_DEPT  [NULL_CODE] = "DEPT";   /* Dept */
const char TLLC_DEPTNM[NULL_CODE] = "DEPTNM";   /* Name */
const char TLLC_DIFGID[NULL_CODE] = "DIFGID";   /* Diff Group */
const char TLLC_DIFGRP[NULL_CODE] = "DIFGRP";   /* Description */
const char TLLC_RANGID[NULL_CODE] = "RANGID";   /* Diff Range */
const char TLLC_RANG  [NULL_CODE] = "RANG";   /* Description */
const char TLLC_RATID [NULL_CODE] = "RATID";   /* Diff Ratio */
const char TLLC_RATDES[NULL_CODE] = "RATDES";   /* Description */
const char TLLC_DISTNM[NULL_CODE] = "DISTNM";   /* Name */
const char TLLC_DIST  [NULL_CODE] = "DIST";   /* @OH5@ */
const char TLLC_DIV   [NULL_CODE] = "DIV";   /* @MH2@ */
const char TLLC_DIVNM [NULL_CODE] = "DIVNM";   /* Name */
const char TLLC_GRPNO [NULL_CODE] = "GRPNO";   /* Group */
const char TLLC_GRPNM [NULL_CODE] = "GRPNM";   /* Name */
const char TLLC_IMGITM[NULL_CODE] = "IMGITM";   /* Item */
const char TLLC_IMGNM [NULL_CODE] = "IMGNM";   /* Image Name */
const char TLLC_IMG   [NULL_CODE] = "IMG";   /* Description */
const char TLLC_ITDESC[NULL_CODE] = "ITDESC";   /* Description */
const char TLLC_ITEM  [NULL_CODE] = "ITEM";   /* Item */
const char TLLC_IT2DES[NULL_CODE] = "IT2DES";   /* Secondary Description */
const char TLLC_ITSDES[NULL_CODE] = "ITSDES";   /* Short Description */
const char TLLC_ISITEM[NULL_CODE] = "ISITEM";   /* Item */
const char TLLC_ISSUPP[NULL_CODE] = "ISSUPP";   /* Supplier */
const char TLLC_ISLABL[NULL_CODE] = "ISLABL";   /* Supplier Label */
const char TLLC_ISDIF1[NULL_CODE] = "ISDIF1";   /* Diff 1 */
const char TLLC_ISDIF2[NULL_CODE] = "ISDIF2";   /* Diff 2 */
const char TLLC_ISDIF3[NULL_CODE] = "ISDIF3";   /* Diff 3 */
const char TLLC_ISDIF4[NULL_CODE] = "ISDIF4";   /* Diff 4 */
const char TLLC_ITXFID[NULL_CODE] = "ITXFID";   /* Item Transform Id */
const char TLLC_ITXFRM[NULL_CODE] = "ITXFRM";   /* Description */
const char TLLC_LLSTID[NULL_CODE] = "LLSTID";   /* Location List */
const char TLLC_LLIST [NULL_CODE] = "LLIST";   /* Description */
const char TLLC_PACKTI[NULL_CODE] = "PACKTI";   /* Pack Template Id */
const char TLLC_PACKTD[NULL_CODE] = "PACKTD";   /* Description */
const char TLLC_PTNR2N[NULL_CODE] = "PTNR2N";   /* Secondary Name */
const char TLLC_PTNRNM[NULL_CODE] = "PTNRNM";   /* Name */
const char TLLC_PTNRID[NULL_CODE] = "PTNRID";   /* Partner */
const char TLLC_PTNRTY[NULL_CODE] = "PTNRTY";   /* Type */
const char TLLC_PMHNM [NULL_CODE] = "PMHNM";   /* Name */
const char TLLC_PMHGP [NULL_CODE] = "PMHGP";   /* Grandparent Id */
const char TLLC_PMHID [NULL_CODE] = "PMHID";   /* Pending Merch Id */
const char TLLC_PMHTYP[NULL_CODE] = "PMHTYP";   /* Hierarchy Type */
const char TLLC_PMHPID[NULL_CODE] = "PMHPID";   /* Parent Id */
const char TLLC_CPNDES[NULL_CODE] = "CPNDES";   /* Description */
const char TLLC_CPNID [NULL_CODE] = "CPNID";   /* Coupon */
const char TLLC_PGRPID[NULL_CODE] = "PGRPID";   /* Priority Group */
const char TLLC_PGRP  [NULL_CODE] = "PGRP";   /* Description */
const char TLLC_RCLS  [NULL_CODE] = "RCLS";   /* Description */
const char TLLC_RCLSNO[NULL_CODE] = "RCLSNO";   /* Reclass number */
const char TLLC_REG   [NULL_CODE] = "REG";   /* @OH4@ */
const char TLLC_REGNM [NULL_CODE] = "REGNM";   /* Name */
const char TLLC_RLTNID[NULL_CODE] = "RLTNID";   /* Relationship Id */
const char TLLC_RLTNNM[NULL_CODE] = "RLTNNM";   /* Description */
const char TLLC_SLSTD [NULL_CODE] = "SLSTD";   /* Description */
const char TLLC_SLST  [NULL_CODE] = "SLST";   /* Item List Id */
const char TLLC_STNM  [NULL_CODE] = "STNM";   /* Name */
const char TLLC_ST2NM [NULL_CODE] = "ST2NM";   /* Secondary Name */
const char TLLC_ST    [NULL_CODE] = "ST";   /* Store */
const char TLLC_STAD2N[NULL_CODE] = "STAD2N";   /* Secondary Name */
const char TLLC_STADST[NULL_CODE] = "STADST";   /* Store */
const char TLLC_STADNM[NULL_CODE] = "STADNM";   /* Name */
const char TLLC_SCDEPT[NULL_CODE] = "SCDEPT";   /* Dept */
const char TLLC_SCNM  [NULL_CODE] = "SCNM";   /* Name */
const char TLLC_SUBCLS[NULL_CODE] = "SUBCLS";   /* @MH6@ */
const char TLLC_SCCLS [NULL_CODE] = "SCCLS";   /* @MH5@ */
const char TLLC_SUP2NM[NULL_CODE] = "SUP2NM";   /* Secondary Name */
const char TLLC_SUPNM [NULL_CODE] = "SUPNM";   /* Name */
const char TLLC_SUPP  [NULL_CODE] = "SUPP";   /* Supplier */
const char TLLC_SUPCNM[NULL_CODE] = "SUPCNM";   /* Contact Name */
const char TLLC_SPTSUP[NULL_CODE] = "SPTSUP";   /* Supplier */
const char TLLC_SPTID [NULL_CODE] = "SPTID";   /* Pack Template Id */
const char TLLC_SPTDES[NULL_CODE] = "SPTDES";   /* Description */
const char TLLC_UDAID [NULL_CODE] = "UDAID";   /* UDA Id */
const char TLLC_UDATXT[NULL_CODE] = "UDATXT";   /* Text */
const char TLLC_UDAITM[NULL_CODE] = "UDAITM";   /* Item */
const char TLLC_WCBDCI[NULL_CODE] = "WCBDCI";   /* Cost Component */
const char TLLC_WCBDTI[NULL_CODE] = "WCBDTI";   /* Template Id */
const char TLLC_WCBD  [NULL_CODE] = "WCBD";   /* Description */
const char TLLC_WCBHTI[NULL_CODE] = "WCBHTI";   /* Template Id */
const char TLLC_WCBH  [NULL_CODE] = "WCBH";   /* Description */
const char TLLC_WH    [NULL_CODE] = "WH";   /* Warehouse */
const char TLLC_WHNM  [NULL_CODE] = "WHNM";   /* Name */
const char TLLC_WH2NM [NULL_CODE] = "WH2NM";   /* Secondary Name */
const char TLLC_UDATXD[NULL_CODE] = "UDATXD";   /* Description */


/* TLLT = Translation code for default UI label to be used for base system of record RMS table for the entity. */
const char TLLT[NULL_CODE] = "TLLT";

const char TLLT_ADDR  [NULL_CODE] = "ADDR";   /* Addresses */
const char TLLT_AREA  [NULL_CODE] = "AREA";   /* @OH3@ */
const char TLLT_CHAIN [NULL_CODE] = "CHAIN";   /* @OH2@ */
const char TLLT_CLASS [NULL_CODE] = "CLASS";   /* @MH5@ */
const char TLLT_CZ    [NULL_CODE] = "CZ";   /* Cost Zone */
const char TLLT_CZG   [NULL_CODE] = "CZG";   /* Cost Zone Group */
const char TLLT_DEPS  [NULL_CODE] = "DEPS";   /* @MH4@ */
const char TLLT_DGH   [NULL_CODE] = "DGH";   /* Diff Group */
const char TLLT_DRGH  [NULL_CODE] = "DRGH";   /* Diff Range */
const char TLLT_DRTH  [NULL_CODE] = "DRTH";   /* Diff Ratio */
const char TLLT_DIST  [NULL_CODE] = "DIST";   /* @OH5@ */
const char TLLT_DIV   [NULL_CODE] = "DIV";   /* @MH2@ */
const char TLLT_GROUPS[NULL_CODE] = "GROUPS";   /* Groups */
const char TLLT_ITIMG [NULL_CODE] = "ITIMG";   /* Item Image */
const char TLLT_IM    [NULL_CODE] = "IM";   /* Item Master */
const char TLLT_IS    [NULL_CODE] = "IS";   /* Item Supplier */
const char TLLT_IXFROM[NULL_CODE] = "IXFROM";   /* Item Transform */
const char TLLT_LLH   [NULL_CODE] = "LLH";   /* Location List */
const char TLLT_PTH   [NULL_CODE] = "PTH";   /* Pack Template */
const char TLLT_PRTNR [NULL_CODE] = "PRTNR";   /* Partner */
const char TLLT_PENDMH[NULL_CODE] = "PENDMH";   /* Pending Merchandise Hierarchy */
const char TLLT_POSCPN[NULL_CODE] = "POSCPN";   /* POS Coupon */
const char TLLT_PRIGRP[NULL_CODE] = "PRIGRP";   /* Priority Group */
const char TLLT_RECLS [NULL_CODE] = "RECLS";   /* Reclass */
const char TLLT_REGION[NULL_CODE] = "REGION";   /* @OH4@ */
const char TLLT_RIH   [NULL_CODE] = "RIH";   /* Related Item */
const char TLLT_SKULST[NULL_CODE] = "SKULST";   /* Item List */
const char TLLT_STORE [NULL_CODE] = "STORE";   /* Store */
const char TLLT_STRADD[NULL_CODE] = "STRADD";   /* New Store */
const char TLLT_SBCLS [NULL_CODE] = "SBCLS";   /* @MH6@ */
const char TLLT_SUPS  [NULL_CODE] = "SUPS";   /* Supplier */
const char TLLT_SPACKT[NULL_CODE] = "SPACKT";   /* Supplier Pack Template */
const char TLLT_UDAITF[NULL_CODE] = "UDAITF";   /* UDA Free Form */
const char TLLT_CSTMPD[NULL_CODE] = "CSTMPD";   /* Cost Buildup Template Detail */
const char TLLT_CSTMPH[NULL_CODE] = "CSTMPH";   /* Cost Buildup Template Header */
const char TLLT_WH    [NULL_CODE] = "WH";   /* Warehouse */


/* TLOC = To Location */
const char TLOC[NULL_CODE] = "TLOC";

const char TLOC_S     [NULL_CODE] = "S";   /* To St */
const char TLOC_W     [NULL_CODE] = "W";   /* To Wh */
const char TLOC_L     [NULL_CODE] = "L";   /* To Loc */


/* TLVL = Transaction Level Descriptions */
const char TLVL[NULL_CODE] = "TLVL";

const char TLVL_1     [NULL_CODE] = "1";   /* Level 1 */
const char TLVL_2     [NULL_CODE] = "2";   /* Level 2 */
const char TLVL_3     [NULL_CODE] = "3";   /* Level 3 */


/* TMLB = Timeline Base */
const char TMLB[NULL_CODE] = "TMLB";


/* TMLN = Timeline Type Code */
const char TMLN[NULL_CODE] = "TMLN";

const char TMLN_IT    [NULL_CODE] = "IT";   /* Item */
const char TMLN_PO    [NULL_CODE] = "PO";   /* Purchase Order */
const char TMLN_POIT  [NULL_CODE] = "POIT";   /* Order/Item */
const char TMLN_CE    [NULL_CODE] = "CE";   /* Customs */
const char TMLN_TR    [NULL_CODE] = "TR";   /* Transportation */
const char TMLN_TRPI  [NULL_CODE] = "TRPI";   /* Transportation PO/Item */
const char TMLN_TRCO  [NULL_CODE] = "TRCO";   /* Container */
const char TMLN_TRBL  [NULL_CODE] = "TRBL";   /* BOL/AWB */
const char TMLN_TRCI  [NULL_CODE] = "TRCI";   /* Commercial Invoice */
const char TMLN_TRPOBL[NULL_CODE] = "TRPOBL";   /* PO-BOL/AWB */


/* TMPL = Fashion Template Labels */
const char TMPL[NULL_CODE] = "TMPL";

const char TMPL_S     [NULL_CODE] = "S";   /* Seq No */
const char TMPL_T     [NULL_CODE] = "T";   /* Pack Tmpl ID */


/* TMPT = Transformation Types */
const char TMPT[NULL_CODE] = "TMPT";

const char TMPT_K     [NULL_CODE] = "K";   /* Break to Sell */
const char TMPT_O     [NULL_CODE] = "O";   /* Orderable */
const char TMPT_S     [NULL_CODE] = "S";   /* Sellable */


/* TOLE = Tolerance Type */
const char TOLE[NULL_CODE] = "TOLE";

const char TOLE_A     [NULL_CODE] = "A";   /* Actual */
const char TOLE_P     [NULL_CODE] = "P";   /* Percentage */


/* TOLT = Sales Audit Tolerance Types */
const char TOLT[NULL_CODE] = "TOLT";

const char TOLT_OS    [NULL_CODE] = "OS";   /* Over/Short Tolerance */
const char TOLT_DT    [NULL_CODE] = "DT";   /* Date Range Tolerance */
const char TOLT_DV    [NULL_CODE] = "DV";   /* Discount Value Tolerance */


/* TOOP = Sales Audit Totals Operation */
const char TOOP[NULL_CODE] = "TOOP";

const char TOOP_PLUS  [NULL_CODE] = "+";   /* + */
const char TOOP_MINUS [NULL_CODE] = "-";   /* - */


/* TOSC = Sales Audit Totals Sources */
const char TOSC[NULL_CODE] = "TOSC";

const char TOSC_TRN   [NULL_CODE] = "TRN";   /* Transaction details */
const char TOSC_TOT   [NULL_CODE] = "TOT";   /* Total details */


/* TPA1 = Transfer Price Adjustment Types 1 */
const char TPA1[NULL_CODE] = "TPA1";

const char TPA1_DP    [NULL_CODE] = "DP";   /* Decrease by Percent */
const char TPA1_DA    [NULL_CODE] = "DA";   /* Decrease by Amount */
const char TPA1_IP    [NULL_CODE] = "IP";   /* Increase by Percent */
const char TPA1_IA    [NULL_CODE] = "IA";   /* Increase by Amount */
const char TPA1_S     [NULL_CODE] = "S";   /* Set Price */


/* TPA2 = Transfer Price Adjustment Types 2 */
const char TPA2[NULL_CODE] = "TPA2";

const char TPA2_DP    [NULL_CODE] = "DP";   /* Decrease by Percent */
const char TPA2_DA    [NULL_CODE] = "DA";   /* Decrease by Amount */
const char TPA2_S     [NULL_CODE] = "S";   /* Set Price */


/* TR1E = Tsf Type MTE Off MultiChan Off Edit */
const char TR1E[NULL_CODE] = "TR1E";

const char TR1E_SR    [NULL_CODE] = "SR";   /* Store Requisition */
const char TR1E_CO    [NULL_CODE] = "CO";   /* Customer Order */
const char TR1E_RV    [NULL_CODE] = "RV";   /* Return To Vendor */
const char TR1E_CF    [NULL_CODE] = "CF";   /* Confirmation */
const char TR1E_AD    [NULL_CODE] = "AD";   /* Administrative */
const char TR1E_MR    [NULL_CODE] = "MR";   /* Manual Requisition */
const char TR1E_PL    [NULL_CODE] = "PL";   /* PO-Linked Transfer */
const char TR1E_EG    [NULL_CODE] = "EG";   /* Externally Generated */
const char TR1E_RAC   [NULL_CODE] = "RAC";   /* Reallocation Transfer */
const char TR1E_SIM   [NULL_CODE] = "SIM";   /* SIM Generated Transfer */
const char TR1E_AIP   [NULL_CODE] = "AIP";   /* AIP Generated Transfer */
const char TR1E_SG    [NULL_CODE] = "SG";   /* System Generated Transfer */


/* TR1N = Tsf Type MTE Off MultiChan Off New */
const char TR1N[NULL_CODE] = "TR1N";

const char TR1N_CO    [NULL_CODE] = "CO";   /* Customer Order */
const char TR1N_RV    [NULL_CODE] = "RV";   /* Return To Vendor */
const char TR1N_CF    [NULL_CODE] = "CF";   /* Confirmation */
const char TR1N_AD    [NULL_CODE] = "AD";   /* Administrative */
const char TR1N_MR    [NULL_CODE] = "MR";   /* Manual Requisition */
const char TR1N_RAC   [NULL_CODE] = "RAC";   /* Reallocation Transfer */


/* TR2E = Tsf Type MTE Off MultiChan On Edit */
const char TR2E[NULL_CODE] = "TR2E";

const char TR2E_AD    [NULL_CODE] = "AD";   /* Administrative */
const char TR2E_AIP   [NULL_CODE] = "AIP";   /* AIP Generated Transfer */
const char TR2E_BT    [NULL_CODE] = "BT";   /* Book Transfer */
const char TR2E_CF    [NULL_CODE] = "CF";   /* Confirmation */
const char TR2E_CO    [NULL_CODE] = "CO";   /* Customer Order */
const char TR2E_EG    [NULL_CODE] = "EG";   /* Externally Generated */
const char TR2E_FO    [NULL_CODE] = "FO";   /* @SUH4@ Order */
const char TR2E_FR    [NULL_CODE] = "FR";   /* @SUH4@ Return */
const char TR2E_MR    [NULL_CODE] = "MR";   /* Manual Requisition */
const char TR2E_PL    [NULL_CODE] = "PL";   /* PO-Linked Transfer */
const char TR2E_RAC   [NULL_CODE] = "RAC";   /* Reallocation Transfer */
const char TR2E_RV    [NULL_CODE] = "RV";   /* Return To Vendor */
const char TR2E_SG    [NULL_CODE] = "SG";   /* System Generated Transfer */
const char TR2E_SIM   [NULL_CODE] = "SIM";   /* SIM Generated Transfer */
const char TR2E_SR    [NULL_CODE] = "SR";   /* Store Requisition */


/* TR2N = Tsf Type MTE Off MultiChan On New */
const char TR2N[NULL_CODE] = "TR2N";

const char TR2N_AD    [NULL_CODE] = "AD";   /* Administrative */
const char TR2N_BT    [NULL_CODE] = "BT";   /* Book Transfer */
const char TR2N_CF    [NULL_CODE] = "CF";   /* Confirmation */
const char TR2N_CO    [NULL_CODE] = "CO";   /* Customer Order */
const char TR2N_FO    [NULL_CODE] = "FO";   /* @SUH4@ Order */
const char TR2N_FR    [NULL_CODE] = "FR";   /* @SUH4@ Return */
const char TR2N_MR    [NULL_CODE] = "MR";   /* Manual Requisition */
const char TR2N_RAC   [NULL_CODE] = "RAC";   /* Reallocation Transfer */
const char TR2N_RV    [NULL_CODE] = "RV";   /* Return To Vendor */


/* TR3E = Tsf Type MTE On MultiChan Off Edit */
const char TR3E[NULL_CODE] = "TR3E";

const char TR3E_SR    [NULL_CODE] = "SR";   /* Store Requisition */
const char TR3E_CO    [NULL_CODE] = "CO";   /* Customer Order */
const char TR3E_RV    [NULL_CODE] = "RV";   /* Return To Vendor */
const char TR3E_CF    [NULL_CODE] = "CF";   /* Confirmation */
const char TR3E_AD    [NULL_CODE] = "AD";   /* Administrative */
const char TR3E_MR    [NULL_CODE] = "MR";   /* Manual Requisition */
const char TR3E_IC    [NULL_CODE] = "IC";   /* Intercompany */
const char TR3E_PL    [NULL_CODE] = "PL";   /* PO-Linked Transfer */
const char TR3E_EG    [NULL_CODE] = "EG";   /* Externally Generated */
const char TR3E_RAC   [NULL_CODE] = "RAC";   /* Reallocation Transfer */
const char TR3E_SIM   [NULL_CODE] = "SIM";   /* SIM Generated Transfer */
const char TR3E_AIP   [NULL_CODE] = "AIP";   /* AIP Generated Transfer */
const char TR3E_SG    [NULL_CODE] = "SG";   /* System Generated Transfer */


/* TR3N = Tsf Type MTE On MultiChan Off New */
const char TR3N[NULL_CODE] = "TR3N";

const char TR3N_CO    [NULL_CODE] = "CO";   /* Customer Order */
const char TR3N_RV    [NULL_CODE] = "RV";   /* Return To Vendor */
const char TR3N_CF    [NULL_CODE] = "CF";   /* Confirmation */
const char TR3N_AD    [NULL_CODE] = "AD";   /* Administrative */
const char TR3N_MR    [NULL_CODE] = "MR";   /* Manual Requisition */
const char TR3N_IC    [NULL_CODE] = "IC";   /* Intercompany */
const char TR3N_RAC   [NULL_CODE] = "RAC";   /* Reallocation Transfer */


/* TR4E = Tsf Type MTE On MultiChan On Edit */
const char TR4E[NULL_CODE] = "TR4E";

const char TR4E_AD    [NULL_CODE] = "AD";   /* Administrative */
const char TR4E_AIP   [NULL_CODE] = "AIP";   /* AIP Generated Transfer */
const char TR4E_BT    [NULL_CODE] = "BT";   /* Book Transfer */
const char TR4E_CF    [NULL_CODE] = "CF";   /* Confirmation */
const char TR4E_CO    [NULL_CODE] = "CO";   /* Customer Order */
const char TR4E_EG    [NULL_CODE] = "EG";   /* Externally Generated */
const char TR4E_FO    [NULL_CODE] = "FO";   /* @SUH4@ Order */
const char TR4E_FR    [NULL_CODE] = "FR";   /* @SUH4@ Return */
const char TR4E_IC    [NULL_CODE] = "IC";   /* Intercompany */
const char TR4E_MR    [NULL_CODE] = "MR";   /* Manual Requisition */
const char TR4E_PL    [NULL_CODE] = "PL";   /* PO-Linked Transfer */
const char TR4E_RAC   [NULL_CODE] = "RAC";   /* Reallocation Transfer */
const char TR4E_RV    [NULL_CODE] = "RV";   /* Return To Vendor */
const char TR4E_SG    [NULL_CODE] = "SG";   /* System Generated Transfer */
const char TR4E_SIM   [NULL_CODE] = "SIM";   /* SIM Generated Transfer */
const char TR4E_SR    [NULL_CODE] = "SR";   /* Store Requisition */


/* TR4N = Tsf Type MTE On MultiChan On New */
const char TR4N[NULL_CODE] = "TR4N";

const char TR4N_AD    [NULL_CODE] = "AD";   /* Administrative */
const char TR4N_BT    [NULL_CODE] = "BT";   /* Book Transfer */
const char TR4N_CF    [NULL_CODE] = "CF";   /* Confirmation */
const char TR4N_CO    [NULL_CODE] = "CO";   /* Customer Order */
const char TR4N_FO    [NULL_CODE] = "FO";   /* @SUH4@ Order */
const char TR4N_FR    [NULL_CODE] = "FR";   /* @SUH4@ Return */
const char TR4N_IC    [NULL_CODE] = "IC";   /* Intercompany */
const char TR4N_MR    [NULL_CODE] = "MR";   /* Manual Requisition */
const char TR4N_RAC   [NULL_CODE] = "RAC";   /* Reallocation Transfer */
const char TR4N_RV    [NULL_CODE] = "RV";   /* Return To Vendor */


/* TRA1 = Transfer Allocation Types */
const char TRA1[NULL_CODE] = "TRA1";

const char TRA1_SR    [NULL_CODE] = "SR";   /* Stock Requisition */
const char TRA1_PO    [NULL_CODE] = "PO";   /* Cross-Dock PO */
const char TRA1_A     [NULL_CODE] = "A";   /* All */


/* TRA2 = Transfer Allocation Types (incl. All) */
const char TRA2[NULL_CODE] = "TRA2";

const char TRA2_SR    [NULL_CODE] = "SR";   /* Stock Requisition */
const char TRA2_PO    [NULL_CODE] = "PO";   /* Cross-Dock PO */


/* TRAC = Transfer Action List */
const char TRAC[NULL_CODE] = "TRAC";

const char TRAC_NEW   [NULL_CODE] = "NEW";   /* New */
const char TRAC_NEWE  [NULL_CODE] = "NEWE";   /* New from Existing */
const char TRAC_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char TRAC_EDIT  [NULL_CODE] = "EDIT";   /* Edit */


/* TRAF = Traffic Conditions */
const char TRAF[NULL_CODE] = "TRAF";

const char TRAF_HIG   [NULL_CODE] = "HIG";   /* High */
const char TRAF_MED   [NULL_CODE] = "MED";   /* Medium */
const char TRAF_LOW   [NULL_CODE] = "LOW";   /* Low */


/* TRAI = Sales Audit Sub-transaction Indicator */
const char TRAI[NULL_CODE] = "TRAI";

const char TRAI_ACC   [NULL_CODE] = "ACC";   /* Accomodation */
const char TRAI_DIS   [NULL_CODE] = "DIS";   /* Disposed */


/* TRAS = Sales Audit Sub-transaction Types */
const char TRAS[NULL_CODE] = "TRAS";

const char TRAS_ERR   [NULL_CODE] = "ERR";   /* Error */
const char TRAS_EMP   [NULL_CODE] = "EMP";   /* Employee */
const char TRAS_DRIVEO[NULL_CODE] = "DRIVEO";   /* Fuel Drive Off */
const char TRAS_MV    [NULL_CODE] = "MV";   /* Merchandise Vendor Payout */
const char TRAS_EV    [NULL_CODE] = "EV";   /* Expense Vendor Payout */
const char TRAS_EXCH  [NULL_CODE] = "EXCH";   /* Exchange in Item Sale/Return trans */
const char TRAS_CACCOM[NULL_CODE] = "CACCOM";   /* Credit Accommodation */
const char TRAS_CSTGEN[NULL_CODE] = "CSTGEN";   /* General Accommodation */
const char TRAS_CSTTAX[NULL_CODE] = "CSTTAX";   /* Tax Accommodation */
const char TRAS_DRET  [NULL_CODE] = "DRET";   /* Disposed Return */
const char TRAS_DEXCHI[NULL_CODE] = "DEXCHI";   /* Disposed Exchange */
const char TRAS_SALE  [NULL_CODE] = "SALE";   /* Sale */
const char TRAS_RETURN[NULL_CODE] = "RETURN";   /* Return */
const char TRAS_VOID  [NULL_CODE] = "VOID";   /* Void */
const char TRAS_NOSALE[NULL_CODE] = "NOSALE";   /* No Sale */
const char TRAS_OSTORE[NULL_CODE] = "OSTORE";   /* Open Store */
const char TRAS_CSTORE[NULL_CODE] = "CSTORE";   /* Close Store */
const char TRAS_DSTORE[NULL_CODE] = "DSTORE";   /* Close Store */
const char TRAS_OREG  [NULL_CODE] = "OREG";   /* Open Register */
const char TRAS_CREG  [NULL_CODE] = "CREG";   /* Close Register */
const char TRAS_CRGRC [NULL_CODE] = "CRGRC";   /* Close Register */
const char TRAS_OTILL [NULL_CODE] = "OTILL";   /* Open Till */
const char TRAS_CTILL [NULL_CODE] = "CTILL";   /* Close Till */
const char TRAS_CTILLT[NULL_CODE] = "CTILLT";   /* Close Till (Total) */
const char TRAS_LOTILL[NULL_CODE] = "LOTILL";   /* Loan Till */
const char TRAS_PUTILL[NULL_CODE] = "PUTILL";   /* Pickup Till */
const char TRAS_STILL [NULL_CODE] = "STILL";   /* Suspend Till */
const char TRAS_RTILL [NULL_CODE] = "RTILL";   /* Resume Till */
const char TRAS_PITILL[NULL_CODE] = "PITILL";   /* Payin Till */
const char TRAS_POTILL[NULL_CODE] = "POTILL";   /* Payout Till */
const char TRAS_HOUSE [NULL_CODE] = "HOUSE";   /* House Payment */
const char TRAS_LAYINT[NULL_CODE] = "LAYINT";   /* Initiate Layaway */
const char TRAS_LAYCMP[NULL_CODE] = "LAYCMP";   /* Layaway Complete */
const char TRAS_LAYPAY[NULL_CODE] = "LAYPAY";   /* Layaway Payment */
const char TRAS_LAYDEL[NULL_CODE] = "LAYDEL";   /* Layaway Delete */
const char TRAS_ORDINT[NULL_CODE] = "ORDINT";   /* Initiate Order */
const char TRAS_ORDCMP[NULL_CODE] = "ORDCMP";   /* Order Complete */
const char TRAS_ORDCAN[NULL_CODE] = "ORDCAN";   /* Order Cancel */
const char TRAS_ORDPAR[NULL_CODE] = "ORDPAR";   /* Order Partial */
const char TRAS_BANK  [NULL_CODE] = "BANK";   /* Bank Deposit Store */
const char TRAS_INSCRE[NULL_CODE] = "INSCRE";   /* Instant Credit Enrollment */
const char TRAS_REDEEM[NULL_CODE] = "REDEEM";   /* Redeem */
const char TRAS_ETRAIN[NULL_CODE] = "ETRAIN";   /* Enter Training Mode */
const char TRAS_XTRAIN[NULL_CODE] = "XTRAIN";   /* Exit Training Mode */
const char TRAS_SEND  [NULL_CODE] = "SEND";   /* Send */
const char TRAS_PAYOUT[NULL_CODE] = "PAYOUT";   /* Payroll Payout */
const char TRAS_NTRENT[NULL_CODE] = "NTRENT";   /* Enter Transaction Reentry */
const char TRAS_XTRENT[NULL_CODE] = "XTRENT";   /* Exit Transaction Reentry */
const char TRAS_CANCEL[NULL_CODE] = "CANCEL";   /* Canceled Transaction */
const char TRAS_TRAIN [NULL_CODE] = "TRAIN";   /* Training Mode Transaction */
const char TRAS_SUSPND[NULL_CODE] = "SUSPND";   /* Suspend Transaction */
const char TRAS_SSPNDR[NULL_CODE] = "SSPNDR";   /* Resume Suspended Transaction */
const char TRAS_SSPNDC[NULL_CODE] = "SSPNDC";   /* Cancelled Suspended Transaction */
const char TRAS_BILLPY[NULL_CODE] = "BILLPY";   /* Telephone - Mobile Bill Payment */
const char TRAS_STCHG [NULL_CODE] = "STCHG";   /* Transaction Status Change */
const char TRAS_AUDIT [NULL_CODE] = "AUDIT";   /* Till Audit */
const char TRAS_GRGRC [NULL_CODE] = "GRGRC";   /* Workstation Close */
const char TRAS_OTHER [NULL_CODE] = "OTHER";   /* Miscellaneous */
const char TRAS_STRBNK[NULL_CODE] = "STRBNK";   /* Currency Deposit Into Store Bank */
const char TRAS_OTILLT[NULL_CODE] = "OTILLT";   /* Open Till (Total) */


/* TRAT = Sales Audit Transaction Types */
const char TRAT[NULL_CODE] = "TRAT";

const char TRAT_ERR   [NULL_CODE] = "ERR";   /* Error */
const char TRAT_TERM  [NULL_CODE] = "TERM";   /* Termination Record */
const char TRAT_DCLOSE[NULL_CODE] = "DCLOSE";   /* Day has been closed at the store */
const char TRAT_OPEN  [NULL_CODE] = "OPEN";   /* Open */
const char TRAT_NOSALE[NULL_CODE] = "NOSALE";   /* No Sale */
const char TRAT_VOID  [NULL_CODE] = "VOID";   /* Void */
const char TRAT_PVOID [NULL_CODE] = "PVOID";   /* Post Void */
const char TRAT_SALE  [NULL_CODE] = "SALE";   /* Sale */
const char TRAT_SPLORD[NULL_CODE] = "SPLORD";   /* Special Order */
const char TRAT_RETURN[NULL_CODE] = "RETURN";   /* Return */
const char TRAT_PAIDIN[NULL_CODE] = "PAIDIN";   /* Paid In */
const char TRAT_PAIDOU[NULL_CODE] = "PAIDOU";   /* Paid Out */
const char TRAT_PULL  [NULL_CODE] = "PULL";   /* Pull */
const char TRAT_LOAN  [NULL_CODE] = "LOAN";   /* Loan */
const char TRAT_COND  [NULL_CODE] = "COND";   /* Daily Store Conditions */
const char TRAT_CLOSE [NULL_CODE] = "CLOSE";   /* Close */
const char TRAT_REFUND[NULL_CODE] = "REFUND";   /* Return of customer's original check */
const char TRAT_TOTAL [NULL_CODE] = "TOTAL";   /* RTLOG declared totals */
const char TRAT_METER [NULL_CODE] = "METER";   /* Meter Reading for Fuel */
const char TRAT_PUMPT [NULL_CODE] = "PUMPT";   /* Pump Test for Fuel */
const char TRAT_TANKDP[NULL_CODE] = "TANKDP";   /* Tank Dip */
const char TRAT_EEXCH [NULL_CODE] = "EEXCH";   /* Even Exchange */
const char TRAT_OTHER [NULL_CODE] = "OTHER";   /* Other */


/* TRCO = Transportation Conditions */
const char TRCO[NULL_CODE] = "TRCO";

const char TRCO_D     [NULL_CODE] = "D";   /* Delivered */
const char TRCO_L     [NULL_CODE] = "L";   /* Loaded */
const char TRCO_S     [NULL_CODE] = "S";   /* Sailed */
const char TRCO_F     [NULL_CODE] = "F";   /* Finalized */


/* TRFC = Freight Type */
const char TRFC[NULL_CODE] = "TRFC";

const char TRFC_N     [NULL_CODE] = "N";   /* Normal */
const char TRFC_E     [NULL_CODE] = "E";   /* Expedite */
const char TRFC_H     [NULL_CODE] = "H";   /* Hold */


/* TRFL = Totals Reference Labels */
const char TRFL[NULL_CODE] = "TRFL";

const char TRFL_1     [NULL_CODE] = "1";   /* Ref. No. 1 */
const char TRFL_2     [NULL_CODE] = "2";   /* Ref. No. 2 */
const char TRFL_3     [NULL_CODE] = "3";   /* Ref. No. 3 */
const char TRFL_D     [NULL_CODE] = "D";   /* Dept. */
const char TRFL_P     [NULL_CODE] = "P";   /* Pump */


/* TRLB = Transfer Label */
const char TRLB[NULL_CODE] = "TRLB";

const char TRLB_T     [NULL_CODE] = "T";   /* Transfer */
const char TRLB_A     [NULL_CODE] = "A";   /* Allocation */


/* TRMF = Transportation Modes (Find Form) */
const char TRMF[NULL_CODE] = "TRMF";

const char TRMF_NEW   [NULL_CODE] = "NEW";   /* New */
const char TRMF_NEWE  [NULL_CODE] = "NEWE";   /* Create From Existing */
const char TRMF_EDIT  [NULL_CODE] = "EDIT";   /* Edit */
const char TRMF_VIEW  [NULL_CODE] = "VIEW";   /* View */
const char TRMF_L     [NULL_CODE] = "L";   /* License */
const char TRMF_V     [NULL_CODE] = "V";   /* Visa */
const char TRMF_VVE   [NULL_CODE] = "VVE";   /* Vessel/Voyage/ETD */
const char TRMF_BL    [NULL_CODE] = "BL";   /* BOL/AWB */
const char TRMF_CI    [NULL_CODE] = "CI";   /* Invoice No. */
const char TRMF_CO    [NULL_CODE] = "CO";   /* Container */
const char TRMF_POIT  [NULL_CODE] = "POIT";   /* Order No./Item */


/* TRMO = Transportation Mode */
const char TRMO[NULL_CODE] = "TRMO";

const char TRMO_10    [NULL_CODE] = "10";   /* Vessel, Non-container */
const char TRMO_11    [NULL_CODE] = "11";   /* Vessel, Container */
const char TRMO_12    [NULL_CODE] = "12";   /* Border Water-borne (Mexico and Canada) */
const char TRMO_20    [NULL_CODE] = "20";   /* Rail, Non-container */
const char TRMO_21    [NULL_CODE] = "21";   /* Rail, Container */
const char TRMO_30    [NULL_CODE] = "30";   /* TRUCK, Non-container */
const char TRMO_31    [NULL_CODE] = "31";   /* TRUCK, Container */
const char TRMO_32    [NULL_CODE] = "32";   /* Auto */
const char TRMO_33    [NULL_CODE] = "33";   /* Pedestrian */
const char TRMO_34    [NULL_CODE] = "34";   /* Road,other,Includes foot and animalborne */
const char TRMO_40    [NULL_CODE] = "40";   /* Air, Non-container */
const char TRMO_41    [NULL_CODE] = "41";   /* Air, Container */
const char TRMO_50    [NULL_CODE] = "50";   /* Mail */
const char TRMO_60    [NULL_CODE] = "60";   /* Passenger, hand-carried */
const char TRMO_70    [NULL_CODE] = "70";   /* Fixed Tran Installation */
const char TRMO_80    [NULL_CODE] = "80";   /* Not used at this time */


/* TRNV = Transaction Maintenance Navigation Type */
const char TRNV[NULL_CODE] = "TRNV";

const char TRNV_R     [NULL_CODE] = "R";   /* Next Transaction Sequence Number */
const char TRNV_P     [NULL_CODE] = "P";   /* Register Transaction */


/* TRRC = Routing Type */
const char TRRC[NULL_CODE] = "TRRC";

const char TRRC_1     [NULL_CODE] = "1";   /* Federal Express */
const char TRRC_2     [NULL_CODE] = "2";   /* UPS */
const char TRRC_3     [NULL_CODE] = "3";   /* Mail */


/* TRS1 = MLE Transfer Overall Status */
const char TRS1[NULL_CODE] = "TRS1";

const char TRS1_I     [NULL_CODE] = "I";   /* Input */
const char TRS1_B     [NULL_CODE] = "B";   /* Submitted */
const char TRS1_A     [NULL_CODE] = "A";   /* Approved */
const char TRS1_C     [NULL_CODE] = "C";   /* Closed */
const char TRS1_D     [NULL_CODE] = "D";   /* Deleted */
const char TRS1_N     [NULL_CODE] = "N";   /* In Progress */


/* TRS2 = MLE Transfer Leg Status */
const char TRS2[NULL_CODE] = "TRS2";

const char TRS2_I     [NULL_CODE] = "I";   /* Input */
const char TRS2_B     [NULL_CODE] = "B";   /* Submitted */
const char TRS2_A     [NULL_CODE] = "A";   /* Approved */
const char TRS2_S     [NULL_CODE] = "S";   /* Shipped */
const char TRS2_C     [NULL_CODE] = "C";   /* Closed */
const char TRS2_D     [NULL_CODE] = "D";   /* Deleted */
const char TRS2_P     [NULL_CODE] = "P";   /* Picked */
const char TRS2_L     [NULL_CODE] = "L";   /* Selected */


/* TRSP = Truck Splitting Method */
const char TRSP[NULL_CODE] = "TRSP";

const char TRSP_B     [NULL_CODE] = "B";   /* Balanced Assortment */
const char TRSP_I     [NULL_CODE] = "I";   /* Item Sequence */


/* TRST = Transfer Status */
const char TRST[NULL_CODE] = "TRST";

const char TRST_I     [NULL_CODE] = "I";   /* Input */
const char TRST_B     [NULL_CODE] = "B";   /* Submitted */
const char TRST_A     [NULL_CODE] = "A";   /* Approved */
const char TRST_S     [NULL_CODE] = "S";   /* Shipped */
const char TRST_C     [NULL_CODE] = "C";   /* Closed */
const char TRST_D     [NULL_CODE] = "D";   /* Deleted */
const char TRST_P     [NULL_CODE] = "P";   /* Picked */
const char TRST_N     [NULL_CODE] = "N";   /* In Progress */


/* TRTO = Transportation Types */
const char TRTO[NULL_CODE] = "TRTO";

const char TRTO_VVE   [NULL_CODE] = "VVE";   /* Vessel/Voyage/ETD */
const char TRTO_BL    [NULL_CODE] = "BL";   /* BOL/AWB */
const char TRTO_CI    [NULL_CODE] = "CI";   /* Invoice No. */
const char TRTO_CO    [NULL_CODE] = "CO";   /* Container */
const char TRTO_POIT  [NULL_CODE] = "POIT";   /* Order No./Item */


/* TRTP = Tax Rate Type */
const char TRTP[NULL_CODE] = "TRTP";

const char TRTP_P     [NULL_CODE] = "P";   /* Percentage */
const char TRTP_S     [NULL_CODE] = "S";   /* Specific */
const char TRTP_V     [NULL_CODE] = "V";   /* Value */
const char TRTP_A     [NULL_CODE] = "A";   /* Amount */


/* TRTS = Transfer Receipt Treatments */
const char TRTS[NULL_CODE] = "TRTS";

const char TRTS_BL    [NULL_CODE] = "BL";   /* Both Locations */
const char TRTS_NL    [NULL_CODE] = "NL";   /* No Loss */
const char TRTS_RL    [NULL_CODE] = "RL";   /* Receiving Location */
const char TRTS_SL    [NULL_CODE] = "SL";   /* Sending Location */


/* TRTY = Transportation Shipment Types */
const char TRTY[NULL_CODE] = "TRTY";

const char TRTY_DESH  [NULL_CODE] = "DESH";   /* Deconsolidated Shipment */
const char TRTY_DISH  [NULL_CODE] = "DISH";   /* Direct Shipment */


/* TSFB = Transfer Basis */
const char TSFB[NULL_CODE] = "TSFB";

const char TSFB_T     [NULL_CODE] = "T";   /* Transfer Entity */
const char TSFB_B     [NULL_CODE] = "B";   /* Set of Books */


/* TSFC = Transfer Codes */
const char TSFC[NULL_CODE] = "TSFC";

const char TSFC_C     [NULL_CODE] = "C";   /* Carrier */
const char TSFC_RT    [NULL_CODE] = "RT";   /* Routing Type */


/* TSFL = Tsf Leg */
const char TSFL[NULL_CODE] = "TSFL";

const char TSFL_F     [NULL_CODE] = "F";   /* 1st Leg */
const char TSFL_S     [NULL_CODE] = "S";   /* 2nd Leg */


/* TSFO = Shipment Type (Transfer or Order) */
const char TSFO[NULL_CODE] = "TSFO";

const char TSFO_T     [NULL_CODE] = "T";   /* Transfer */
const char TSFO_O     [NULL_CODE] = "O";   /* Order */
const char TSFO_OS    [NULL_CODE] = "OS";   /* Ord */
const char TSFO_TS    [NULL_CODE] = "TS";   /* Tsf */


/* TSFS = Transfer Security */
const char TSFS[NULL_CODE] = "TSFS";

const char TSFS_PIXF  [NULL_CODE] = "PIXF";   /* Intercompany Transfer Privilege */
const char TSFS_PAXF  [NULL_CODE] = "PAXF";   /* Approve Transfer Privilege */
const char TSFS_PSXF  [NULL_CODE] = "PSXF";   /* Submit Transfer Privilege */
const char TSFS_PFIN  [NULL_CODE] = "PFIN";   /* Allow Finishing Privilege */
const char TSFS_PITX  [NULL_CODE] = "PITX";   /* Item Xform Packing */


/* TSFW = TSF WO Items */
const char TSFW[NULL_CODE] = "TSFW";

const char TSFW_I     [NULL_CODE] = "I";   /* Item */
const char TSFW_IP    [NULL_CODE] = "IP";   /* Item Parent */
const char TSFW_IPD   [NULL_CODE] = "IPD";   /* Item Parent/Diff */
const char TSFW_AI    [NULL_CODE] = "AI";   /* All Items */


/* TSIT = Transfer Inventory Types */
const char TSIT[NULL_CODE] = "TSIT";

const char TSIT_A     [NULL_CODE] = "A";   /* Available */
const char TSIT_U     [NULL_CODE] = "U";   /* Unavailable */


/* TSLC = Transfer Locations */
const char TSLC[NULL_CODE] = "TSLC";

const char TSLC_FS    [NULL_CODE] = "FS";   /* From Store */
const char TSLC_FW    [NULL_CODE] = "FW";   /* From WH */
const char TSLC_TS    [NULL_CODE] = "TS";   /* To Store */
const char TSLC_TW    [NULL_CODE] = "TW";   /* To WH */
const char TSLC_FF    [NULL_CODE] = "FF";   /* From Finisher */
const char TSLC_TF    [NULL_CODE] = "TF";   /* To Finisher */


/* TSLG = Transfer Legs */
const char TSLG[NULL_CODE] = "TSLG";

const char TSLG_1     [NULL_CODE] = "1";   /* 1st Leg */
const char TSLG_2     [NULL_CODE] = "2";   /* 2nd Leg */


/* TSYS = Transaction Processing System */
const char TSYS[NULL_CODE] = "TSYS";

const char TSYS_OMS   [NULL_CODE] = "OMS";   /* OMS */
const char TSYS_POS   [NULL_CODE] = "POS";   /* POS */
const char TSYS_SIM   [NULL_CODE] = "SIM";   /* SIM */


/* TTDT = Transit Times Origin Type */
const char TTDT[NULL_CODE] = "TTDT";

const char TTDT_ST    [NULL_CODE] = "ST";   /* Store */
const char TTDT_WH    [NULL_CODE] = "WH";   /* Warehouse */


/* TTOT = Transit Times Origin Type */
const char TTOT[NULL_CODE] = "TTOT";

const char TTOT_SU    [NULL_CODE] = "SU";   /* Supplier */
const char TTOT_ST    [NULL_CODE] = "ST";   /* Store */
const char TTOT_LLS   [NULL_CODE] = "LLS";   /* Location List-Store */
const char TTOT_WH    [NULL_CODE] = "WH";   /* Warehouse */
const char TTOT_LLW   [NULL_CODE] = "LLW";   /* Location List-Warehouse */


/* TTST = Transit Times Origin Type */
const char TTST[NULL_CODE] = "TTST";

const char TTST_SU    [NULL_CODE] = "SU";   /* Supplier Site */
const char TTST_ST    [NULL_CODE] = "ST";   /* Store */
const char TTST_LLS   [NULL_CODE] = "LLS";   /* Location List-Store */
const char TTST_WH    [NULL_CODE] = "WH";   /* Warehouse */
const char TTST_LLW   [NULL_CODE] = "LLW";   /* Location List-Warehouse */


/* TXCL = Tax Class */
const char TXCL[NULL_CODE] = "TXCL";

const char TXCL_TAX   [NULL_CODE] = "TAX";   /* Taxable */
const char TXCL_NON   [NULL_CODE] = "NON";   /* Non */
const char TXCL_FULL  [NULL_CODE] = "FULL";   /* Full */
const char TXCL_ALT   [NULL_CODE] = "ALT";   /* Alternate */


/* TXHD = Tax Header */
const char TXHD[NULL_CODE] = "TXHD";

const char TXHD_I     [NULL_CODE] = "I";   /* Item */
const char TXHD_D     [NULL_CODE] = "D";   /* @MH4@ */
const char TXHD_L     [NULL_CODE] = "L";   /* Item List */


/* TXTY = Tax Types */
const char TXTY[NULL_CODE] = "TXTY";

const char TXTY_SALES [NULL_CODE] = "SALES";   /* Sales */
const char TXTY_HWY   [NULL_CODE] = "HWY";   /* Highway */
const char TXTY_TRST  [NULL_CODE] = "TRST";   /* Transit */
const char TXTY_HOSP  [NULL_CODE] = "HOSP";   /* Hospital */
const char TXTY_STAD  [NULL_CODE] = "STAD";   /* Stadium */


/* TZON = Time Zone */
const char TZON[NULL_CODE] = "TZON";

const char TZON_E     [NULL_CODE] = "E";   /* Eastern */
const char TZON_C     [NULL_CODE] = "C";   /* Central */
const char TZON_M     [NULL_CODE] = "M";   /* Mountain */
const char TZON_P     [NULL_CODE] = "P";   /* Pacific */


/* UCHG = Up Charge Group */
const char UCHG[NULL_CODE] = "UCHG";

const char UCHG_F     [NULL_CODE] = "F";   /* Freight */
const char UCHG_M     [NULL_CODE] = "M";   /* Miscellaneous */
const char UCHG_A     [NULL_CODE] = "A";   /* Admin. Fee */
const char UCHG_T     [NULL_CODE] = "T";   /* Taxes */
const char UCHG_K     [NULL_CODE] = "K";   /* Special K Fees */
const char UCHG_W     [NULL_CODE] = "W";   /* @SUH4@ */


/* UDAC = UDA Maintenance Types */
const char UDAC[NULL_CODE] = "UDAC";

const char UDAC_ADD   [NULL_CODE] = "ADD";   /* Add */
const char UDAC_DELETE[NULL_CODE] = "DELETE";   /* Delete */
const char UDAC_CHANGE[NULL_CODE] = "CHANGE";   /* Change */
const char UDAC_CREATE[NULL_CODE] = "CREATE";   /* Create */


/* UDAM = UDA Dialog Values */
const char UDAM[NULL_CODE] = "UDAM";

const char UDAM_ITEM  [NULL_CODE] = "ITEM";   /* Item */


/* UDDT = UDA Data Types */
const char UDDT[NULL_CODE] = "UDDT";

const char UDDT_NUM   [NULL_CODE] = "NUM";   /* Number */
const char UDDT_ALPHA [NULL_CODE] = "ALPHA";   /* Alphanumeric */
const char UDDT_DATE  [NULL_CODE] = "DATE";   /* Date */


/* UDIS = UDA Display Types */
const char UDIS[NULL_CODE] = "UDIS";

const char UDIS_LV    [NULL_CODE] = "LV";   /* List of Values */
const char UDIS_FF    [NULL_CODE] = "FF";   /* Free Form Text */
const char UDIS_DT    [NULL_CODE] = "DT";   /* Date */


/* UINT = UIN Type */
const char UINT[NULL_CODE] = "UINT";

const char UINT_SN    [NULL_CODE] = "SN";   /* Serial Number */
const char UINT_AGSN  [NULL_CODE] = "AGSN";   /* Auto Generate SN */


/* ULBL = UIN Label */
const char ULBL[NULL_CODE] = "ULBL";

const char ULBL_SN    [NULL_CODE] = "SN";   /* Serial Number */
const char ULBL_LN    [NULL_CODE] = "LN";   /* License Number */
const char ULBL_PN    [NULL_CODE] = "PN";   /* Part Number */
const char ULBL_IMEI  [NULL_CODE] = "IMEI";   /* International Mobile Equipment Identity */
const char ULBL_SIN   [NULL_CODE] = "SIN";   /* Serial Identification Number */


/* UMOP = UOM Operator */
const char UMOP[NULL_CODE] = "UMOP";

const char UMOP_D     [NULL_CODE] = "D";   /* Divide */
const char UMOP_M     [NULL_CODE] = "M";   /* Multiply */


/* UNIT = Multi-Unit/Single Unit Labels */
const char UNIT[NULL_CODE] = "UNIT";

const char UNIT_S     [NULL_CODE] = "S";   /* Single-Unit */
const char UNIT_M     [NULL_CODE] = "M";   /* Multi-Unit */


/* UOMC = UOM Class Description */
const char UOMC[NULL_CODE] = "UOMC";

const char UOMC_AREA  [NULL_CODE] = "AREA";   /* Area */
const char UOMC_DIMEN [NULL_CODE] = "DIMEN";   /* Dimension */
const char UOMC_MASS  [NULL_CODE] = "MASS";   /* Mass */
const char UOMC_MISC  [NULL_CODE] = "MISC";   /* Miscellaneous */
const char UOMC_PACK  [NULL_CODE] = "PACK";   /* Pack */
const char UOMC_QTY   [NULL_CODE] = "QTY";   /* Quantity */
const char UOMC_VOL   [NULL_CODE] = "VOL";   /* Volume */
const char UOMC_LVOL  [NULL_CODE] = "LVOL";   /* Liquid Volume */


/* UORG = Up Charge Organizational Hierarchy */
const char UORG[NULL_CODE] = "UORG";

const char UORG_AL    [NULL_CODE] = "AL";   /* All Locations */
const char UORG_AS    [NULL_CODE] = "AS";   /* All Stores */
const char UORG_S     [NULL_CODE] = "S";   /* Store */
const char UORG_AW    [NULL_CODE] = "AW";   /* All Warehouses */
const char UORG_W     [NULL_CODE] = "W";   /* Warehouse */
const char UORG_PW    [NULL_CODE] = "PW";   /* Physical Warehouse */
const char UORG_AI    [NULL_CODE] = "AI";   /* All Internal Finishers */
const char UORG_I     [NULL_CODE] = "I";   /* Internal Finisher */
const char UORG_AE    [NULL_CODE] = "AE";   /* All External Finishers */
const char UORG_E     [NULL_CODE] = "E";   /* External Finisher */
const char UORG_LL    [NULL_CODE] = "LL";   /* Location List */
const char UORG_A     [NULL_CODE] = "A";   /* @OH3@ */
const char UORG_R     [NULL_CODE] = "R";   /* @OH4@ */
const char UORG_D     [NULL_CODE] = "D";   /* @OH5@ */
const char UORG_T     [NULL_CODE] = "T";   /* Transfer Zone */
const char UORG_CZ    [NULL_CODE] = "CZ";   /* Cost Zone */
const char UORG_M     [NULL_CODE] = "M";   /* Importer */
const char UORG_X     [NULL_CODE] = "X";   /* Exporter */


/* UOWK = Sales Audit Unit of Work */
const char UOWK[NULL_CODE] = "UOWK";

const char UOWK_T     [NULL_CODE] = "T";   /* Transaction */
const char UOWK_S     [NULL_CODE] = "S";   /* Store Day */


/* UPCM = Multiple UPC Indicator */
const char UPCM[NULL_CODE] = "UPCM";

const char UPCM_M     [NULL_CODE] = "M";   /* Multiple */


/* UPCT = UPC Type */
const char UPCT[NULL_CODE] = "UPCT";

const char UPCT_ITEM  [NULL_CODE] = "ITEM";   /* Oracle Retail Item Number */
const char UPCT_UPC_A [NULL_CODE] = "UPC-A";   /* UCC12 */
const char UPCT_UPC_AS[NULL_CODE] = "UPC-AS";   /* UCC12 with Supplement */
const char UPCT_UPC_E [NULL_CODE] = "UPC-E";   /* UCC8 */
const char UPCT_UPC_ES[NULL_CODE] = "UPC-ES";   /* UCC8 with Supplement */
const char UPCT_EAN8  [NULL_CODE] = "EAN8";   /* EAN/UCC-8 */
const char UPCT_EAN13 [NULL_CODE] = "EAN13";   /* EAN/UCC-13 */
const char UPCT_EAN13S[NULL_CODE] = "EAN13S";   /* EAN/UCC-13 with Supplement */
const char UPCT_ISBN10[NULL_CODE] = "ISBN10";   /* ISBN-10 */
const char UPCT_ISBN13[NULL_CODE] = "ISBN13";   /* ISBN-13 */
const char UPCT_NDC   [NULL_CODE] = "NDC";   /* NDC/NHRIC - National Drug Code */
const char UPCT_PLU   [NULL_CODE] = "PLU";   /* PLU */
const char UPCT_VPLU  [NULL_CODE] = "VPLU";   /* Variable Weight PLU */
const char UPCT_SSCC  [NULL_CODE] = "SSCC";   /* SSCC Shipper Carton */
const char UPCT_UCC14 [NULL_CODE] = "UCC14";   /* EAN/UCC-14 */
const char UPCT_MANL  [NULL_CODE] = "MANL";   /* Manual */


/* USOL = Unsatisfied Order Location */
const char USOL[NULL_CODE] = "USOL";

const char USOL_1     [NULL_CODE] = "1";   /* Store */
const char USOL_0     [NULL_CODE] = "0";   /* Warehouse */


/* UTYP = Up Charge Types */
const char UTYP[NULL_CODE] = "UTYP";

const char UTYP_E     [NULL_CODE] = "E";   /* Expense */
const char UTYP_P     [NULL_CODE] = "P";   /* Profit */


/* VALT = Validation Type */
const char VALT[NULL_CODE] = "VALT";


/* VATC = RMS Vat Codes */
const char VATC[NULL_CODE] = "VATC";


/* VATL = VAT Labels */
const char VATL[NULL_CODE] = "VATL";

const char VATL_EX    [NULL_CODE] = "EX";   /* (Excl. VAT) */
const char VATL_IN    [NULL_CODE] = "IN";   /* (Incl. VAT) */
const char VATL_TV    [NULL_CODE] = "TV";   /* Total VAT */


/* VATR = VAT Region */
const char VATR[NULL_CODE] = "VATR";

const char VATR_VR    [NULL_CODE] = "VR";   /* VAT Region */


/* VEND = Vendor Types */
const char VEND[NULL_CODE] = "VEND";

const char VEND_AG    [NULL_CODE] = "AG";   /* Agent */
const char VEND_AP    [NULL_CODE] = "AP";   /* Applicant */
const char VEND_BK    [NULL_CODE] = "BK";   /* Bank */
const char VEND_BR    [NULL_CODE] = "BR";   /* Broker */
const char VEND_CN    [NULL_CODE] = "CN";   /* Consignee */
const char VEND_CO    [NULL_CODE] = "CO";   /* Consolidator */
const char VEND_FA    [NULL_CODE] = "FA";   /* Factory */
const char VEND_FF    [NULL_CODE] = "FF";   /* Freight Forwarder */
const char VEND_IM    [NULL_CODE] = "IM";   /* Importer */
const char VEND_SU    [NULL_CODE] = "SU";   /* Supplier */


const char VOUCH_4000  [NULL_CODE] = "4000";   /* Credit Voucher */
const char VOUCH_4010  [NULL_CODE] = "4010";   /* Manual Credit */
const char VOUCH_4020  [NULL_CODE] = "4020";   /* Manual Imprint */
const char VOUCH_4030  [NULL_CODE] = "4030";   /* Gift Certificate */
const char VOUCH_4040  [NULL_CODE] = "4040";   /* Gift Card */
const char VOUCH_4050  [NULL_CODE] = "4050";   /* Store Credit */
const char VOUCH_4060  [NULL_CODE] = "4060";   /* Mail Certificate */
const char VOUCH_4070  [NULL_CODE] = "4070";   /* Purchase Order */
const char VOUCH_4080  [NULL_CODE] = "4080";   /* PrePaid */
const char VOUCH_4090  [NULL_CODE] = "4090";   /* Store Credit Alternate Currency */
const char VOUCH_4100  [NULL_CODE] = "4100";   /* Gift Certificate Alternate Currency */
const char VOUCH_4110  [NULL_CODE] = "4110";   /* Customer Credit */


/* VOUT = Voucher Types */
const char VOUT[NULL_CODE] = "VOUT";

const char VOUT_GIC   [NULL_CODE] = "GIC";   /* Gift Certificate */
const char VOUT_VOU   [NULL_CODE] = "VOU";   /* Voucher */


/* VRCT = VAT Region Calc Type */
const char VRCT[NULL_CODE] = "VRCT";

const char VRCT_S     [NULL_CODE] = "S";   /* Simple */
const char VRCT_E     [NULL_CODE] = "E";   /* Exempt */
const char VRCT_C     [NULL_CODE] = "C";   /* Custom */


/* VRRT = Virtual Realm Types */
const char VRRT[NULL_CODE] = "VRRT";

const char VRRT_PKTI  [NULL_CODE] = "PKTI";   /* Table(INT) PK or Single Row */
const char VRRT_PKV   [NULL_CODE] = "PKV";   /* View(INT/EXT) PK or Single Row */
const char VRRT_TOTAL [NULL_CODE] = "TOTAL";   /* View based on SA_TOTAL */


/* VRTY = VAT Region Type */
const char VRTY[NULL_CODE] = "VRTY";

const char VRTY_N     [NULL_CODE] = "N";   /* Non-Member */
const char VRTY_M     [NULL_CODE] = "M";   /* EU Member */
const char VRTY_E     [NULL_CODE] = "E";   /* Base EU Region */


/* VSCT = Sales Audit View System Calculated Total */
const char VSCT[NULL_CODE] = "VSCT";

const char VSCT_A     [NULL_CODE] = "A";   /* ALL */
const char VSCT_HQ    [NULL_CODE] = "HQ";   /* HQ only */
const char VSCT_HSM   [NULL_CODE] = "HSM";   /* HQ & Store Manager only */
const char VSCT_N     [NULL_CODE] = "N";   /* None */


/* VTTP = VAT Type */
const char VTTP[NULL_CODE] = "VTTP";

const char VTTP_B     [NULL_CODE] = "B";   /* Both */
const char VTTP_C     [NULL_CODE] = "C";   /* Cost */
const char VTTP_R     [NULL_CODE] = "R";   /* Retail */


/* WEAT = Sales Audit Weather Conditions */
const char WEAT[NULL_CODE] = "WEAT";

const char WEAT_SNO   [NULL_CODE] = "SNO";   /* Snow */
const char WEAT_RAI   [NULL_CODE] = "RAI";   /* Rain */
const char WEAT_SUN   [NULL_CODE] = "SUN";   /* Sunny */
const char WEAT_CLO   [NULL_CODE] = "CLO";   /* Cloudy */
const char WEAT_HAZ   [NULL_CODE] = "HAZ";   /* Hazy */
const char WEAT_HUM   [NULL_CODE] = "HUM";   /* Humid */
const char WEAT_DAM   [NULL_CODE] = "DAM";   /* Damp */
const char WEAT_FLO   [NULL_CODE] = "FLO";   /* Flood */
const char WEAT_HUR   [NULL_CODE] = "HUR";   /* Hurricane */
const char WEAT_TOR   [NULL_CODE] = "TOR";   /* Tornado */


/* WEEK = Label "Weeks" */
const char WEEK[NULL_CODE] = "WEEK";

const char WEEK_W     [NULL_CODE] = "W";   /* weeks */


/* WFCH = @SUH4@ Customer Hierarchy */
const char WFCH[NULL_CODE] = "WFCH";

const char WFCH_W     [NULL_CODE] = "W";   /* @SUH4@ */
const char WFCH_F     [NULL_CODE] = "F";   /* @SUH4@ */


/* WFCO = @SUH4@ Orders Cancel Reason */
const char WFCO[NULL_CODE] = "WFCO";

const char WFCO_PD    [NULL_CODE] = "PD";   /* Past Due Date */
const char WFCO_AS    [NULL_CODE] = "AS";   /* Alternate Supplier */
const char WFCO_NS    [NULL_CODE] = "NS";   /* Unknown (not specified) */
const char WFCO_SI    [NULL_CODE] = "SI";   /* Substitute Item */
const char WFCO_NR    [NULL_CODE] = "NR";   /* Item cannot be replenished */
const char WFCO_ED    [NULL_CODE] = "ED";   /* Externally Deleted */
const char WFCO_BC    [NULL_CODE] = "BC";   /* Bad Credit Customer */


/* WFCR = @SUH4@ Returns Cancel Reason */
const char WFCR[NULL_CODE] = "WFCR";

const char WFCR_II    [NULL_CODE] = "II";   /* Insufficient Inventory */
const char WFCR_DS    [NULL_CODE] = "DS";   /* Damaged Stock */
const char WFCR_OR    [NULL_CODE] = "OR";   /* Overdue Return */
const char WFCR_ED    [NULL_CODE] = "ED";   /* Externally Deleted */


/* WFIT = WF Item Types */
const char WFIT[NULL_CODE] = "WFIT";

const char WFIT_SS    [NULL_CODE] = "SS";   /* Item */
const char WFIT_IL    [NULL_CODE] = "IL";   /* Item List */
const char WFIT_IP    [NULL_CODE] = "IP";   /* Item Parent */
const char WFIT_RI    [NULL_CODE] = "RI";   /* Reference Item */


/* WFOH = @SUH4@ Organization Hierarchies */
const char WFOH[NULL_CODE] = "WFOH";

const char WFOH_C     [NULL_CODE] = "C";   /* @OH1@ */
const char WFOH_F     [NULL_CODE] = "F";   /* @SUH4@ */


/* WFOR = WF Order Type */
const char WFOR[NULL_CODE] = "WFOR";

const char WFOR_FO    [NULL_CODE] = "FO";   /* @SUH4@ Order */
const char WFOR_FR    [NULL_CODE] = "FR";   /* @SUH4@ Return */


/* WFOS = WF Order Status */
const char WFOS[NULL_CODE] = "WFOS";

const char WFOS_I     [NULL_CODE] = "I";   /* Input */
const char WFOS_A     [NULL_CODE] = "A";   /* Approved */
const char WFOS_P     [NULL_CODE] = "P";   /* In Progress */
const char WFOS_C     [NULL_CODE] = "C";   /* Cancelled */
const char WFOS_D     [NULL_CODE] = "D";   /* Closed */
const char WFOS_R     [NULL_CODE] = "R";   /* Pending Credit Approval */


/* WFOT = WF Order Types */
const char WFOT[NULL_CODE] = "WFOT";

const char WFOT_M     [NULL_CODE] = "M";   /* Manual */
const char WFOT_E     [NULL_CODE] = "E";   /* EDI */
const char WFOT_A     [NULL_CODE] = "A";   /* Auto */
const char WFOT_X     [NULL_CODE] = "X";   /* External */


/* WFRE = WF Return Types */
const char WFRE[NULL_CODE] = "WFRE";

const char WFRE_M     [NULL_CODE] = "M";   /* Manual */
const char WFRE_E     [NULL_CODE] = "E";   /* EDI */
const char WFRE_A     [NULL_CODE] = "A";   /* Auto */
const char WFRE_X     [NULL_CODE] = "X";   /* External */


/* WFRF = WF Return Method */
const char WFRF[NULL_CODE] = "WFRF";

const char WFRF_R     [NULL_CODE] = "R";   /* Return To warehouse */
const char WFRF_D     [NULL_CODE] = "D";   /* Destroy on site */


/* WFRS = Status of WF Returns */
const char WFRS[NULL_CODE] = "WFRS";

const char WFRS_I     [NULL_CODE] = "I";   /* Input */
const char WFRS_A     [NULL_CODE] = "A";   /* Approved */
const char WFRS_P     [NULL_CODE] = "P";   /* In Progress */
const char WFRS_C     [NULL_CODE] = "C";   /* Cancelled */
const char WFRS_D     [NULL_CODE] = "D";   /* Closed */


/* WFRT = WF Restocking Type */
const char WFRT[NULL_CODE] = "WFRT";

const char WFRT_V     [NULL_CODE] = "V";   /* Percent */
const char WFRT_S     [NULL_CODE] = "S";   /* Specific Amount */


/* WFSC = @SUH4@ Stores */
const char WFSC[NULL_CODE] = "WFSC";

const char WFSC_X     [NULL_CODE] = "X";   /* Class Stores X */


/* WHLL = Warehouse Location List */
const char WHLL[NULL_CODE] = "WHLL";

const char WHLL_W     [NULL_CODE] = "W";   /* Warehouse */
const char WHLL_A     [NULL_CODE] = "A";   /* All Warehouses */
const char WHLL_LL    [NULL_CODE] = "LL";   /* Location List */


/* WHSE = Warehouse */
const char WHSE[NULL_CODE] = "WHSE";

const char WHSE_W     [NULL_CODE] = "W";   /* Warehouse */


/* WHTP = Number of Warehouses */
const char WHTP[NULL_CODE] = "WHTP";

const char WHTP_W     [NULL_CODE] = "W";   /* Warehouse */
const char WHTP_A     [NULL_CODE] = "A";   /* All Warehouses */
const char WHTP_LL    [NULL_CODE] = "LL";   /* Location List */


/* WHTY = Virtual Warehouse Types */
const char WHTY[NULL_CODE] = "WHTY";

const char WHTY_XD_RG [NULL_CODE] = "XD_RG";   /* Cross Dock */
const char WHTY_CS_RG [NULL_CODE] = "CS_RG";   /* CSC */
const char WHTY_XD_GS [NULL_CODE] = "XD_GS";   /* Global Sourcing */
const char WHTY_CS_NT [NULL_CODE] = "CS_NT";   /* Non-traditional */


/* WIPP = WIP code for kitted items */
const char WIPP[NULL_CODE] = "WIPP";

const char WIPP_WIP   [NULL_CODE] = "WIP";   /* Pack kitting */


/* WOLT = Customer Location Type */
const char WOLT[NULL_CODE] = "WOLT";

const char WOLT_W     [NULL_CODE] = "W";   /* @SUH4@ */
const char WOLT_F     [NULL_CODE] = "F";   /* @SUH4@ */
const char WOLT_C     [NULL_CODE] = "C";   /* @OH1@ */


/* WSAT = Warehouse Store Assignment Type */
const char WSAT[NULL_CODE] = "WSAT";

const char WSAT_W     [NULL_CODE] = "W";   /* Warehouse Stocked */
const char WSAT_C     [NULL_CODE] = "C";   /* Cross-docked */
const char WSAT_L     [NULL_CODE] = "L";   /* WH/Cross Link */
const char WSAT_A     [NULL_CODE] = "A";   /* All */


/* WSIS = Warehouse Store Issue Labels */
const char WSIS[NULL_CODE] = "WSIS";

const char WSIS_SUS   [NULL_CODE] = "SUS";   /* Total Unit Sales */
const char WSIS_SUW   [NULL_CODE] = "SUW";   /* Unit Sales By End Of Week Date */
const char WSIS_IUI   [NULL_CODE] = "IUI";   /* Total Issues */
const char WSIS_IUW   [NULL_CODE] = "IUW";   /* Unit Issues By End Of Week Date */


/* WSTG = Wastage Type */
const char WSTG[NULL_CODE] = "WSTG";

const char WSTG_SL    [NULL_CODE] = "SL";   /* Sales Wastage */
const char WSTG_SP    [NULL_CODE] = "SP";   /* Spoilage Wastage */


/* WWIP = Work Order WIP codes */
const char WWIP[NULL_CODE] = "WWIP";

const char WWIP_QC    [NULL_CODE] = "QC";   /* Quality Control */
const char WWIP_KIT   [NULL_CODE] = "KIT";   /* Kitting */
const char WWIP_TICKET[NULL_CODE] = "TICKET";   /* Ticket Item */


/* YSNO = Yes or No Indicator */
const char YSNO[NULL_CODE] = "YSNO";

const char YSNO_Y     [NULL_CODE] = "Y";   /* Yes */
const char YSNO_N     [NULL_CODE] = "N";   /* No */


/* ZLVL = Zone Levels */
const char ZLVL[NULL_CODE] = "ZLVL";

const char ZLVL_A     [NULL_CODE] = "A";   /* All Zones */
const char ZLVL_Z     [NULL_CODE] = "Z";   /* Zone */


/* ZONE = Zones */
const char ZONE[NULL_CODE] = "ZONE";

const char ZONE_Z     [NULL_CODE] = "Z";   /* Zone */
const char ZONE_O     [NULL_CODE] = "O";   /* All Zones */


/* ZSEC = Zone Security Areas */
const char ZSEC[NULL_CODE] = "ZSEC";

const char ZSEC_ZALL  [NULL_CODE] = "ZALL";   /* All */
const char ZSEC_ZPRC  [NULL_CODE] = "ZPRC";   /* Pricing */
const char ZSEC_ZCLR  [NULL_CODE] = "ZCLR";   /* Clearances */


/* ZZAC = Action Type Category */
const char ZZAC[NULL_CODE] = "ZZAC";

const char ZZAC_F     [NULL_CODE] = "F";   /* Open Form */
const char ZZAC_P     [NULL_CODE] = "P";   /* Procedure */
const char ZZAC_W     [NULL_CODE] = "W";   /* Windows Call */
const char ZZAC_S     [NULL_CODE] = "S";   /* System */
const char ZZAC_N     [NULL_CODE] = "N";   /* Function */


/* ZZAD = Action / drill indicator */
const char ZZAD[NULL_CODE] = "ZZAD";

const char ZZAD_A     [NULL_CODE] = "A";   /* Action */
const char ZZAD_D     [NULL_CODE] = "D";   /* Drill */
const char ZZAD_N     [NULL_CODE] = "N";   /* Neither */


/* ZZAL = Active Only or All */
const char ZZAL[NULL_CODE] = "ZZAL";

const char ZZAL_A     [NULL_CODE] = "A";   /* Only Active and Future */
const char ZZAL_L     [NULL_CODE] = "L";   /* All */


/* ZZAN = Action Category Syntax */
const char ZZAN[NULL_CODE] = "ZZAN";

const char ZZAN_F     [NULL_CODE] = "F";   /* Form File Name */
const char ZZAN_P     [NULL_CODE] = "P";   /* Procedure Syntax */
const char ZZAN_N     [NULL_CODE] = "N";   /* Function Syntax */
const char ZZAN_W     [NULL_CODE] = "W";   /* Command Line */


/* ZZAS = Action Type Category (no system) */
const char ZZAS[NULL_CODE] = "ZZAS";

const char ZZAS_F     [NULL_CODE] = "F";   /* Open Form */
const char ZZAS_P     [NULL_CODE] = "P";   /* Procedure */
const char ZZAS_W     [NULL_CODE] = "W";   /* Windows Call */
const char ZZAS_N     [NULL_CODE] = "N";   /* Function */


/* ZZAT = Alternative Notification Type */
const char ZZAT[NULL_CODE] = "ZZAT";

const char ZZAT_EMAIL [NULL_CODE] = "EMAIL";   /* E-mail */
const char ZZAT_FAX   [NULL_CODE] = "FAX";   /* FAX */
const char ZZAT_PAGE  [NULL_CODE] = "PAGE";   /* Page */


/* ZZAU = Alternative Notification Use */
const char ZZAU[NULL_CODE] = "ZZAU";

const char ZZAU_HO    [NULL_CODE] = "HO";   /* High Priority Only */
const char ZZAU_ALL   [NULL_CODE] = "ALL";   /* All Events */
const char ZZAU_NO    [NULL_CODE] = "NO";   /* None */


/* ZZCF = Schedule Cycle Frequency */
const char ZZCF[NULL_CODE] = "ZZCF";

const char ZZCF_O     [NULL_CODE] = "O";   /* Once */
const char ZZCF_R     [NULL_CODE] = "R";   /* Repeat */


/* ZZCT = Comment Window Titles */
const char ZZCT[NULL_CODE] = "ZZCT";

const char ZZCT_SCHDSC[NULL_CODE] = "SCHDSC";   /* Schedule Description */
const char ZZCT_EVTDSC[NULL_CODE] = "EVTDSC";   /* Event Type Description */
const char ZZCT_EXTDSC[NULL_CODE] = "EXTDSC";   /* Exception Type Description */
const char ZZCT_EGLDSC[NULL_CODE] = "EGLDSC";   /* Event Group List Description */
const char ZZCT_ECRDSC[NULL_CODE] = "ECRDSC";   /* Event Type Closure Rule Description */


/* ZZDE = Deferred Indicator */
const char ZZDE[NULL_CODE] = "ZZDE";

const char ZZDE_Y     [NULL_CODE] = "Y";   /* Deferred */
const char ZZDE_N     [NULL_CODE] = "N";   /* Not Deferred */


/* ZZEP = Event Type Parameter Details */
const char ZZEP[NULL_CODE] = "ZZEP";

const char ZZEP_P     [NULL_CODE] = "P";   /* Parameter Types */
const char ZZEP_A     [NULL_CODE] = "A";   /* All Available */
const char ZZEP_S     [NULL_CODE] = "S";   /* System Only */
const char ZZEP_X     [NULL_CODE] = "X";   /* Exception Context */


/* ZZES = Exception Type Status */
const char ZZES[NULL_CODE] = "ZZES";

const char ZZES_A     [NULL_CODE] = "A";   /* Active */
const char ZZES_D     [NULL_CODE] = "D";   /* Disabled */
const char ZZES_E     [NULL_CODE] = "E";   /* Expired */
const char ZZES_P     [NULL_CODE] = "P";   /* Paused */


/* ZZEX = External Realm Types */
const char ZZEX[NULL_CODE] = "ZZEX";

const char ZZEX_RA    [NULL_CODE] = "RA";   /* Retail Analytics */


/* ZZGT = ARI Group Types */
const char ZZGT[NULL_CODE] = "ZZGT";

const char ZZGT_S     [NULL_CODE] = "S";   /* Simple */
const char ZZGT_P     [NULL_CODE] = "P";   /* Parameter Driven */
const char ZZGT_U     [NULL_CODE] = "U";   /* User Driven */


/* ZZLB = ARI Labels */
const char ZZLB[NULL_CODE] = "ZZLB";

const char ZZLB_EDESC [NULL_CODE] = "EDESC";   /* Event Description */
const char ZZLB_ADESC [NULL_CODE] = "ADESC";   /* Action Description */
const char ZZLB_DDESC [NULL_CODE] = "DDESC";   /* Drill Description */
const char ZZLB_DF    [NULL_CODE] = "DF";   /* Default */
const char ZZLB_XDESC [NULL_CODE] = "XDESC";   /* Exception Description */
const char ZZLB_IMAGE [NULL_CODE] = "IMAGE";   /* Image */
const char ZZLB_TEXT  [NULL_CODE] = "TEXT";   /* Text */
const char ZZLB_WORK  [NULL_CODE] = "WORK";   /* Revalidating events . . . */
const char ZZLB_EVACTS[NULL_CODE] = "EVACTS";   /* Event Actions */
const char ZZLB_DRACTS[NULL_CODE] = "DRACTS";   /* Event Drills */
const char ZZLB_DRILLS[NULL_CODE] = "DRILLS";   /* Drills */
const char ZZLB_ACTION[NULL_CODE] = "ACTION";   /* Actions */
const char ZZLB_AVAILA[NULL_CODE] = "AVAILA";   /* Available Actions */
const char ZZLB_AVAILD[NULL_CODE] = "AVAILD";   /* Available Drills */
const char ZZLB_ACTPRM[NULL_CODE] = "ACTPRM";   /* Action Parameter */
const char ZZLB_DRLPRM[NULL_CODE] = "DRLPRM";   /* Drill Parameter */
const char ZZLB_EAPM  [NULL_CODE] = "EAPM";   /* Event Action Link Parameter Mapping */
const char ZZLB_EDPM  [NULL_CODE] = "EDPM";   /* Event Drill Link Parameter Mapping */
const char ZZLB_EDITBL[NULL_CODE] = "EDITBL";   /* Editable */
const char ZZLB_ACTNM [NULL_CODE] = "ACTNM";   /* Action Name */
const char ZZLB_DRLNM [NULL_CODE] = "DRLNM";   /* Drill Name */
const char ZZLB_ACTPNM[NULL_CODE] = "ACTPNM";   /* Action Parameter Name */
const char ZZLB_DRLPNM[NULL_CODE] = "DRLPNM";   /* Drill Parameter Name */
const char ZZLB_REQUIR[NULL_CODE] = "REQUIR";   /* Required */
const char ZZLB_WHERE [NULL_CODE] = "WHERE";   /* Where Clause */
const char ZZLB_FROM  [NULL_CODE] = "FROM";   /* From Clause */
const char ZZLB_RDESC [NULL_CODE] = "RDESC";   /* Realm Description */
const char ZZLB_XRULES[NULL_CODE] = "XRULES";   /* Exception Rules */
const char ZZLB_ERULES[NULL_CODE] = "ERULES";   /* Event Rules */
const char ZZLB_CHANGE[NULL_CODE] = "CHANGE";   /* Changed */
const char ZZLB_HEADP [NULL_CODE] = "HEADP";   /* Headline Parameter Description */
const char ZZLB_AR    [NULL_CODE] = "AR";   /* Show Related Events */
const char ZZLB_NOAR  [NULL_CODE] = "NOAR";   /* Related Events Search */
const char ZZLB_ABOR  [NULL_CODE] = "ABOR";   /* Abort */
const char ZZLB_AUTO  [NULL_CODE] = "AUTO";   /* Auto Action */
const char ZZLB_MVALUE[NULL_CODE] = "MVALUE";   /* <Multiple Values> */
const char ZZLB_WORKAC[NULL_CODE] = "WORKAC";   /* Performing actions .  .  . */
const char ZZLB_FOWRD [NULL_CODE] = "FOWRD";   /* This is a forwarded event. */
const char ZZLB_REIN  [NULL_CODE] = "REIN";   /* This is a reinstated event. */


/* ZZMT = Exception Type Version Monitor Types */
const char ZZMT[NULL_CODE] = "ZZMT";

const char ZZMT_R     [NULL_CODE] = "R";   /* Real Time */
const char ZZMT_B     [NULL_CODE] = "B";   /* Batch Scan */
const char ZZMT_T     [NULL_CODE] = "T";   /* Trickle */


/* ZZRM = Routing Method */
const char ZZRM[NULL_CODE] = "ZZRM";

const char ZZRM_A     [NULL_CODE] = "A";   /* All */
const char ZZRM_B     [NULL_CODE] = "B";   /* Balanced */
const char ZZRM_R     [NULL_CODE] = "R";   /* Random */
const char ZZRM_E     [NULL_CODE] = "E";   /* Event Balanced */


/* ZZRO = Routing Methods */
const char ZZRO[NULL_CODE] = "ZZRO";

const char ZZRO_ALL   [NULL_CODE] = "ALL";   /* All */
const char ZZRO_RRA   [NULL_CODE] = "RRA";   /* Round Robin All */
const char ZZRO_RRE   [NULL_CODE] = "RRE";   /* Round Robin Event */
const char ZZRO_WL    [NULL_CODE] = "WL";   /* Workload */


/* ZZRT = Realm Types */
const char ZZRT[NULL_CODE] = "ZZRT";

const char ZZRT_L     [NULL_CODE] = "L";   /* Lookup */
const char ZZRT_V     [NULL_CODE] = "V";   /* View */
const char ZZRT_T     [NULL_CODE] = "T";   /* Table */
const char ZZRT_R     [NULL_CODE] = "R";   /* Report */
const char ZZRT_X     [NULL_CODE] = "X";   /* External */
const char ZZRT_P     [NULL_CODE] = "P";   /* PL/SQL */
const char ZZRT_F     [NULL_CODE] = "F";   /* Forms */
const char ZZRT_K     [NULL_CODE] = "K";   /* WaveLink */
const char ZZRT_S     [NULL_CODE] = "S";   /* Special */


/* ZZSB = Sort Item Conversions */
const char ZZSB[NULL_CODE] = "ZZSB";

const char ZZSB_P     [NULL_CODE] = "P";   /* Event Priority */
const char ZZSB_N     [NULL_CODE] = "N";   /* Event Name */
const char ZZSB_S     [NULL_CODE] = "S";   /* State Name */
const char ZZSB_C     [NULL_CODE] = "C";   /* Event Count */


/* ZZSC = Event Parameter Sort Category */
const char ZZSC[NULL_CODE] = "ZZSC";

const char ZZSC_H     [NULL_CODE] = "H";   /* Header */
const char ZZSC_D     [NULL_CODE] = "D";   /* Detail */


/* ZZSM = Schedule Modes */
const char ZZSM[NULL_CODE] = "ZZSM";

const char ZZSM_S     [NULL_CODE] = "S";   /* Signal */
const char ZZSM_T     [NULL_CODE] = "T";   /* Time */


/* ZZST = Schedule Type Description */
const char ZZST[NULL_CODE] = "ZZST";

const char ZZST_S     [NULL_CODE] = "S";   /* Signal Based */
const char ZZST_T     [NULL_CODE] = "T";   /* Time Based */


/* ZZTF = Boolean Values */
const char ZZTF[NULL_CODE] = "ZZTF";

const char ZZTF_T     [NULL_CODE] = "T";   /* True */
const char ZZTF_F     [NULL_CODE] = "F";   /* False */


/* ZZXP = Exception Parameter Details */
const char ZZXP[NULL_CODE] = "ZZXP";

const char ZZXP_S     [NULL_CODE] = "S";   /* System Only */
const char ZZXP_A     [NULL_CODE] = "A";   /* All */
const char ZZXP_M     [NULL_CODE] = "M";   /* Monitored */


