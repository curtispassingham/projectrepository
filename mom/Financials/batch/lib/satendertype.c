/*=========================================================================

   satendertype.c: tendertype load and lookup

=========================================================================*/

#include "sagetref.h"

static TENDERTYPEDAT *pa_tendertypetable = NULL;  /* memory-mapped tendertype table */
static size_t pl_tendertypetablelen = 0;   /* number of items in tendertypetable */

/* prototypes */
TENDERTYPEDAT *tendertype_lookup(char *is_group, char *is_id);
static int tendertypecmp(const void *is_s1, const void *is_s2);
int tendertype_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
tendertype_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
TENDERTYPEDAT *tendertype_lookup(char *is_group, char *is_id)
{
   char ls_key[TENT_SIZE + LEN_TENDER_TYPE_ID];

   if(pl_tendertypetablelen == 0)
   {
      return( NULL);
   } /* end if */

   memcpy(ls_key, is_group, TENT_SIZE);
   memcpy(ls_key + TENT_SIZE, is_id, LEN_TENDER_TYPE_ID);

   return((TENDERTYPEDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_tendertypetable,
           pl_tendertypetablelen, sizeof(TENDERTYPEDAT), tendertypecmp));

} /* end tendertype_lookup() */


/*-------------------------------------------------------------------------
tendertypecmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int tendertypecmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, TENT_SIZE + LEN_TENDER_TYPE_ID));
} /* end tendertypecmp() */


/*-------------------------------------------------------------------------
tendertype_loadfile() loads the tendertype data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int tendertype_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_tendertypetable = (TENDERTYPEDAT *)mmap( NULL, lr_statbuf.st_size, PROT_READ,
                               MAP_SHARED, li_datafile, 0);
            if(pa_tendertypetable != (TENDERTYPEDAT *)MAP_FAILED)
            {
               pl_tendertypetablelen = lr_statbuf.st_size / sizeof(TENDERTYPEDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
	 } /* end else */
      } /* end if */

      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end tendertype_loadfile() */


