/*=========================================================================

   saupc.c: ref_item load and lookup

=========================================================================*/

#include "sagetref.h"

#ifdef VUPC_DEBUG
#define VUPC_VERBOSE(a1) printf(a1)
#define VUPC_VERBOSE2(a1,a2) printf(a1,a2)
#define VUPC_VERBOSE3(a1,a2,a3) printf(a1,a2,a3)
#define VUPC_VERBOSE4(a1,a2,a3,a4) printf(a1,a2,a3,a4)
#else
#define VUPC_VERBOSE(a1)
#define VUPC_VERBOSE2(a1,a2)
#define VUPC_VERBOSE3(a1,a2,a3)
#define VUPC_VERBOSE4(a1,a2,a3,a4)
#endif

static REFITEMDAT *pa_refitemtable = NULL;  /* memory-mapped ref_item table */
static size_t pl_refitemtablelen = 0;   /* number of items in refitemtable */

/* prototypes */
REFITEMDAT *ref_item_lookup(char *is_key);
static int refitemcmp(const void *is_s1, const void *is_s2);
int ref_item_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
ref_item_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
REFITEMDAT *ref_item_lookup(char *is_key)
{
   char ls_key[NULL_ITEM];

   VUPC_VERBOSE4( "ref_item_lookup <%-*.*>\n", LEN_ITEM, LEN_ITEM, is_key );

   if(pl_refitemtablelen == 0)
   {
      return(NULL);
   } /* end if */

   /* copy ref_item key to local one */
   sprintf(ls_key, "%-*.*s", LEN_ITEM, LEN_ITEM, is_key);

   /* perform ref_item search */
   return((REFITEMDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_refitemtable,
           pl_refitemtablelen, sizeof(REFITEMDAT), refitemcmp));
} /* end ref_item_lookup() */


/*-------------------------------------------------------------------------
refitemcmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int refitemcmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_ITEM));
} /* end refitemcmp() */


/*-------------------------------------------------------------------------
ref_item_loadfile() loads the ref_item data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int ref_item_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_refitemtable = (REFITEMDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ, MAP_SHARED, 
                        li_datafile, 0);
            if(pa_refitemtable != (REFITEMDAT *)MAP_FAILED)
            {
               pl_refitemtablelen = lr_statbuf.st_size / sizeof(REFITEMDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
	 } /* end else */
      } /* end if */

      close(li_datafile);
   }

   return(li_rv);
} /* end ref_item_loadfile() */


