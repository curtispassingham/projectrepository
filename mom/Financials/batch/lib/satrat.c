/* transaction types support module */

#include <retek.h>
#include "salib.h"

typedef struct {
   const char *s_structstring;
   int i_token;
} TOKENMAP;

static TOKENMAP pa_tokentable[] = {
   { TRAT_OPEN,   TRATTT_OPEN   },
   { TRAT_NOSALE, TRATTT_NOSALE },
   { TRAT_VOID,   TRATTT_VOID   },
   { TRAT_PVOID,  TRATTT_PVOID  },
   { TRAT_SALE,   TRATTT_SALE   },
   { TRAT_RETURN, TRATTT_RETURN },
   { TRAT_PAIDIN, TRATTT_PAIDIN },
   { TRAT_PAIDOU, TRATTT_PAIDOU },
   { TRAT_PULL,   TRATTT_PULL   },
   { TRAT_LOAN,   TRATTT_LOAN   },
   { TRAT_COND,   TRATTT_COND   },
   { TRAT_CLOSE,  TRATTT_CLOSE  },
   { TRAT_REFUND, TRATTT_REFUND },
   { TRAT_TOTAL,  TRATTT_TOTAL  },
   { TRAT_ERR,    TRATTT_ERR    },
   { TRAT_TERM,   TRATTT_TERM   },
   { TRAT_METER,  TRATTT_METER  },
   { TRAT_PUMPT,  TRATTT_PUMPT  },
   { TRAT_TANKDP, TRATTT_TANKDP },
   { TRAT_DCLOSE, TRATTT_DCLOSE },
   { NULL,        0 }
};


/*-------------------------------------------------------------------------
trat_lookup() looks up the code and returns its token, or 0 if not found.
-------------------------------------------------------------------------*/
int trat_lookup(char *is_string)
{
   TOKENMAP *lr_p;

   for(lr_p = pa_tokentable; lr_p->s_structstring; ++lr_p)
   {
      if(strncmp(is_string, lr_p->s_structstring, TRAT_SIZE) == 0)
      {
         return(lr_p->i_token);
      } /* end if */
   } /* end for */

   return(0);

} /* end trat_lookup() */




