/* sa_valid_time(char *is_p, int ii_len) - validate time field */
/* returns 0 if good, 1 if bad */

#include <retek.h>
#include "salib.h"

int sa_valid_time(char *is_p, int ii_len)
{
     int li_hour, li_min, li_sec;

     li_hour = atoin(&is_p[0], 2);
     if(li_hour < 0 || li_hour > 23)
     {
        return(1);
     } /* end if */

     li_min = atoin(&is_p[2], 2);
     if(li_min < 0 || li_min > 59)
     {
        return(1);
     } /* end if */

     li_sec = atoin(&is_p[4], 2);
     if(li_sec < 0 || li_sec > 59)
     {
        return(1);
     } /* end if */

     /* must be good */
     return(0);
}


