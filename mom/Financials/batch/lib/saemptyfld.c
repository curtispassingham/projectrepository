/* empty_field() - check for empty field, return 1 if empty, 0 if not */

int empty_field(char *is_string, int ii_length )
{
     for( ; ii_length--; ++is_string)
     {
        if(*is_string != ' ')
	{
           return( 0 );
        } /* end if */
     } /* end for */

     return(1);
}


