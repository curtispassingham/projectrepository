/* datetime.c */

#include <time.h>

/*-------------------------------------------------------------------------
getDateTime() gets the current date/time and puts it in the callers buffer
as "YYYYMMDDHHMMSS".
-------------------------------------------------------------------------*/
void getDateTime(char *os_date_time)
{
     time_t lr_t;
     struct tm *lr_ts;

     lr_t = time(NULL);
     lr_ts = localtime(&lr_t);
     strftime(os_date_time, 16, "%Y%m%d%H%M%S", lr_ts);
}


