/* atoin() - atoi() functionality for fixed length fields */

#include <ctype.h>

int atoin(char *is_s, int ii_length)
{
   int li_result;

   for (li_result = 0; ii_length--; ++is_s)
   {
       if (isdigit(*is_s))	 
       {
           li_result = (li_result * 10) + (*is_s - '0');
       } /* end if */
       else
       {
           break;
       } /* end else */
   } /* end for */

   return(li_result);
} /* end atoin() */


