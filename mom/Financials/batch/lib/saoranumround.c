/* oranumround.c */

#include "saoranum.h"

int OraNumRound(char *is_num,
                char *is_decplace,
                char *os_result,
                long il_result)
{
   char *function = "OraNumRound";
   char err_data[1379];

   short li_retcode;
   OCINumber l_ocinumber_num1, l_ocinumber_num3;
   int li_dec = is_decplace[0]-'0'-4;
   ub4 ll_size = (ub4)il_result;


   if (is_decplace[0] !='4')
   {

      if (OraNumFromString(&l_ocinumber_num1, is_num) < 0) return(-1);

      if ((li_retcode = OCINumberRound(err,
                                       &l_ocinumber_num1,
                                       li_dec,
                                       &l_ocinumber_num3)) != OCI_SUCCESS)
      {
         sprintf(err_data,
                 "OCINumberRound failed with return code %d",
                 li_retcode);
         write_oci_error(function,err_data);
         return(-1);
      }

      if (OraNumToString(&l_ocinumber_num3, os_result, &ll_size) < 0)
         return(-1);
   }
   else
      strcpy(os_result, is_num);

   return(0);
}
