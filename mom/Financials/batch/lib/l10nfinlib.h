#ifndef LIBL10NFIN_H
   #define LIBL10NFIN_H

   /* This objeact contains the objects that will be used by a localized batch. */
   typedef struct wasteadj_param_struct
   {
      char item[NULL_ITEM];
      char adj_qty[NULL_QTY];
      char loc[NULL_LOC];
      char inv_adj_reason[NULL_REASON];
   }wasteadj_param;

#endif
