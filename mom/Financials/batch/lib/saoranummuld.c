/* oranummuld.c */

#include "saoranum.h"

int OraNumMuld(char *is_num1, double is_num2, char *os_num3, long il_size)
{
   char *function = "OraNumMuld";
   char err_data[1379];
   short li_retcode;
   OCINumber l_ocinumber_num1, l_ocinumber_num2, l_ocinumber_num3;
   ub4 ll_size = (ub4)il_size;

   if (OraNumFromString(&l_ocinumber_num1, is_num1) < 0) return(-1);
   OCINumberFromReal(err, &is_num2, sizeof(double), &l_ocinumber_num2);
   if ((li_retcode = OCINumberMul(err,
                                  &l_ocinumber_num1,
                                  &l_ocinumber_num2,
                                  &l_ocinumber_num3)) != OCI_SUCCESS)
   {
      sprintf(err_data, "OCINumberMuld failed with return code %d", li_retcode);
      write_oci_error(function,err_data);
      return(-1);
   }

   if (OraNumToString(&l_ocinumber_num3, os_num3, &ll_size) < 0) return(-1);

   return(0);
}
