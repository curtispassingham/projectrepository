
/*=========================================================================

   sasku.c: item load and lookup

=========================================================================*/

#include "sagetref.h"

static ITEMDAT *pa_itemtable = NULL;  /* memory-mapped item table */
static size_t pl_itemtablelen = 0;   /* number of items in itemtable */

/* prototypes */
ITEMDAT *item_lookup(char *is_key);
static int itemcmp(const void *is_s1, const void *is_s2);
int item_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
item_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
ITEMDAT *item_lookup(char *is_key)
{
   char ls_key[NULL_ITEM];

   if (pl_itemtablelen == 0)
   {
      return(NULL);
   }

   sprintf(ls_key,"%-*.*s",LEN_ITEM,LEN_ITEM,is_key);

   return((ITEMDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_itemtable,
           pl_itemtablelen, sizeof(ITEMDAT), itemcmp));
} /* end item_lookup() */


/*-------------------------------------------------------------------------
itemcmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int itemcmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_ITEM));
} /* end itemcmp() */


/*-------------------------------------------------------------------------
item_loadfile() loads the item data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int item_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_itemtable = (ITEMDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                        MAP_SHARED, li_datafile, 0 );
            if(pa_itemtable != (ITEMDAT *)MAP_FAILED)
            {
               pl_itemtablelen = lr_statbuf.st_size / sizeof(ITEMDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end item_loadfile() */


