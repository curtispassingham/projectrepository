/* fldcpy.c */

#include <retek.h>
#include "salib.h"


/*-------------------------------------------------------------------------
fldcpy(os_field_d, ii_dlen, is_field_s, ii_slen, ii_fldtype) copies one 
field to another. ii_ldtype defines what happens if ii_dlen != ii_slen, so 
far as right/left justification and/or padding. Note: Assumes that the dest 
field was space-filled before calling.
-------------------------------------------------------------------------*/
void fldcpy(char *os_field_d, int ii_dlen, const char *is_field_s, int ii_slen, int ii_fldtype)
{
     int li_diff;

     if (ii_dlen == ii_slen)
     {
          memcpy(os_field_d, is_field_s, ii_dlen);
     }
     else if (ii_dlen > ii_slen)
     {
          li_diff = ii_dlen - ii_slen;
          switch(ii_fldtype)
          {
               case FT_NONE:
               case FT_VARCHAR:
               case FT_DATE:
                    memcpy(os_field_d, is_field_s, ii_slen);
                    memset(os_field_d + ii_slen, ' ', li_diff);
                    break;
               case FT_NUMBER:
                    memcpy(os_field_d + li_diff, is_field_s, ii_slen);
                    memset(os_field_d, '0', li_diff);

                    /* if signed number, move sign to front */
                    if ((is_field_s[0] == '+') || (is_field_s[0] == '-'))
                    {
                       os_field_d[0] = is_field_s[0];
                       os_field_d[li_diff] = '0';
                    }
                    break;
               default:
                    memcpy(os_field_d, is_field_s, ii_slen);
                    break;
          }
     }
     else /* ii_dlen < ii_slen */
     {
          li_diff = ii_slen - ii_dlen;
          switch(ii_fldtype)
          {
               case FT_NONE:
               case FT_VARCHAR:
               case FT_DATE:
                    memcpy(os_field_d, is_field_s, ii_dlen);
                    break;
               case FT_NUMBER:
                    memcpy(os_field_d, is_field_s + li_diff, ii_dlen);
                    /* if signed number, copy sign to front */
                    if ((is_field_s[0] == '+') || (is_field_s[0] == '-'))
                    {
                       os_field_d[0] = is_field_s[0];
                    }
                    break;
               default:
                    memcpy(os_field_d, is_field_s, ii_dlen);
                    break;
          }
     }
}


