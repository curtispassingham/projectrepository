/*=========================================================================

   saprom.c: promotion load and lookup

=========================================================================*/

#include "sagetref.h"

static PROMDAT *pa_promtable = NULL;  /* memory-mapped prom table */
static size_t pl_promtablelen = 0;   /* number of items in promtable */

/* prototypes */
PROMDAT *prom_lookup(char *is_key);
static int promcmp(const void *is_s1, const void *is_s2);
int prom_loadfile(char *is_filename);
PROMDAT *prom_comp_lookup(char *is_promotion, char *is_promo_comp);
static int promcompcmp(const void *is_s1, const void *is_s2);


/*-------------------------------------------------------------------------
prom_lookup() checks the memory-mapped file for the promotion and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
PROMDAT *prom_lookup(char *is_key)
{
   if(pl_promtablelen == 0)
   {
      return(NULL);
   }

   return((PROMDAT *)bsearch(
          (const void *)is_key, (const void *)pa_promtable,
           pl_promtablelen, sizeof(PROMDAT), promcmp));
}


/*-------------------------------------------------------------------------
promcmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int promcmp(const void *is_s1, const void *is_s2)
{
   int promnum1=0,promnum2=0;

   promnum1 = atoin(is_s1,LEN_PROMOTION);
   promnum2 = atoin(is_s2,LEN_PROMOTION);
   return(promnum1-promnum2);
}

/*-------------------------------------------------------------------------
prom_comp_lookup() checks the memory-mapped file for the promotion and
promotion component and returns a pointer to the matching data structure,
or NULL if not found.
-------------------------------------------------------------------------*/
PROMDAT *prom_comp_lookup(char *is_promotion, char *is_promo_comp)
{
   char ls_key[LEN_PROMOTION + LEN_PROMO_COMP + 1];

   if (pl_promtablelen == 0)
   {
      return(NULL);
   }

   memcpy(ls_key, is_promotion,  LEN_PROMOTION);
   memcpy(ls_key + LEN_PROMOTION, is_promo_comp, LEN_PROMO_COMP);

   return((PROMDAT *)bsearch(
          (const void *)&ls_key, (const void *)pa_promtable,
           pl_promtablelen, sizeof(PROMDAT), promcompcmp));

}

/*-------------------------------------------------------------------------
promcompcmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int promcompcmp(const void *is_s1, const void *is_s2)
{
   int promcomp1=0,promcomp2=0,output=0;
   char promstr[LEN_PROMOTION]="",promcompstr[LEN_PROMO_COMP]="",promstr2[LEN_PROMOTION]="",promcompstr2[LEN_PROMO_COMP]="";

   strncpy(promstr,(char *)is_s1+0,LEN_PROMOTION);
   strncpy(promcompstr,(char *)is_s1+LEN_PROMOTION,LEN_PROMO_COMP);

   strncpy(promstr2,(char *)is_s2+0,LEN_PROMOTION);
   strncpy(promcompstr2,(char *)is_s2+LEN_PROMOTION,LEN_PROMO_COMP);

   output=promcmp(promstr,promstr2);

   if (output==0)
   {
      promcomp1=atoin(promcompstr,LEN_PROMO_COMP);
      promcomp2=atoin(promcompstr2,LEN_PROMO_COMP);

      return(promcomp1-promcomp2);
   }
   else
      return output;
}


/*-------------------------------------------------------------------------
prom_loadfile() loads the prom data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int prom_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_promtable = (PROMDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                                        MAP_SHARED, li_datafile, 0);
            if(pa_promtable != (PROMDAT *)MAP_FAILED)
            {
               pl_promtablelen = lr_statbuf.st_size / sizeof(PROMDAT);
               li_rv = TRUE;
            }
         }
         else
	 {
            li_rv = TRUE;
	 } /* end else */
      } /* end if */

      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end prom_loadfile */


