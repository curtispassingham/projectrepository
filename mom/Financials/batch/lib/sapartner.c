/*=========================================================================

   sapartner.c: partner load and lookup

=========================================================================*/

#include "sagetref.h"

static PARTNERDAT *pa_partnertable = NULL;  /* memory-mapped partner table */
static size_t pl_partnertablelen = 0;   /* number of items in partnertable */

/* prototypes */
PARTNERDAT *partner_lookup(char *is_key);
static int partnercmp(const void *is_s1, const void *is_s2);
int partner_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
partner_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
PARTNERDAT *partner_lookup(char *is_key)
{
   PARTNERDAT lr_partner_key = {0};

   if (pl_partnertablelen == 0)
   {
      return(NULL);
   }

   /* Create a key with only partner_id */
   strncpy(lr_partner_key.partner_id, is_key, LEN_PARTNER_ID);

   return((PARTNERDAT *)bsearch(
          (const void *)&lr_partner_key, (const void *)pa_partnertable,
           pl_partnertablelen, sizeof(PARTNERDAT), partnercmp));
} /* end partner_lookup() */


/*-------------------------------------------------------------------------
partnercmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int partnercmp(const void *is_s1, const void *is_s2)
{
   return(strncmp(((PARTNERDAT *)is_s1)->partner_id, 
                  ((PARTNERDAT *)is_s2)->partner_id, 
                  LEN_PARTNER_ID));
} /* end partnercmp() */


/*-------------------------------------------------------------------------
partner_loadfile() loads the partner data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int partner_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_partnertable = (PARTNERDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                        MAP_SHARED, li_datafile, 0 );
            if(pa_partnertable != (PARTNERDAT *)MAP_FAILED)
            {
               pl_partnertablelen = lr_statbuf.st_size / sizeof(PARTNERDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end partner_loadfile() */


