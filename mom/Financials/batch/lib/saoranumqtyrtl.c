
#include "saoranum.h"

int OraNumQtyRtl(char *is_num1,
                 char *is_num2,
                 char *os_num3,
                 long il_size,
                 char *is_decplace,
                 char *is_multiplier)
{
   char *function = "OraNumQtyRtl";
   char err_data[1379];
   short li_retcode;

   OCINumber l_ocinumber_num1, l_ocinumber_num2, l_ocinumber_num3,
             l_ocinumber_num4, l_ocinumber_num5, l_ocinumber_mult;
   ub4 ll_size = (ub4)il_size;
   int li_dec = is_decplace[0]-'0'-4;

   if (OraNumFromString(&l_ocinumber_num1, is_num1) < 0) return(-1);
   if (OraNumFromString(&l_ocinumber_num2, is_num2) < 0) return(-1);
   if (OraNumFromString(&l_ocinumber_mult, is_multiplier) < 0) return(-1);

   if ((li_retcode = OCINumberMul(err,
                                  &l_ocinumber_num1,
                                  &l_ocinumber_num2,
                                  &l_ocinumber_num3)) != OCI_SUCCESS)
   {
      sprintf(err_data, "OCINumberMul failed");
      write_oci_error(function,err_data);
      return(-1);
   }

   if ((li_retcode = OCINumberDiv(err,
                                  &l_ocinumber_num3,
                                  &l_ocinumber_mult,
                                  &l_ocinumber_num4)) != OCI_SUCCESS)
   {
      sprintf(err_data, "OCINumberDiv failed");
      write_oci_error(function,err_data);
      return(-1);
   }

   if (is_decplace[0] !='4')
   {
      if ((li_retcode = OCINumberRound(err,
                                       &l_ocinumber_num4,
                                       li_dec,
                                       &l_ocinumber_num5)) != OCI_SUCCESS)
      {
         sprintf(err_data, "OCINumberRound failed");
         write_oci_error(function,err_data);
         return(-1);
      }
      if (OraNumToString(&l_ocinumber_num5, os_num3, &ll_size) < 0) return(-1);
   }
   else if (OraNumToString(&l_ocinumber_num4, os_num3, &ll_size) < 0)
      return(-1);

   return(0);
}
