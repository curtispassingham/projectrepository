/* atoln() - atol() functionality for fixed length fields */

#include <ctype.h>

long atoln( char *is_s, int ii_length )
{
     long ll_result;

     for(ll_result = 0; ii_length--; ++is_s)
     {
        if (isdigit(*is_s))
	{
           ll_result = (ll_result * 10) + (*is_s - '0');
        } /* end if */
        else
        {
           break;
        } /* end else */
     } /* end for */

     return(ll_result);
} /* end atoln() */


