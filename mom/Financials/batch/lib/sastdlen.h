/* HEADER  : SASTDLEN
   Date    : 01-OCT-99 */

#ifndef _SASTDLEN_H
#define _SASTDLEN_H

#define LEN_ACCT_NO             16
#define LEN_ACCT_PERIOD         15
#define LEN_BIG_SEQ_NO          20
#define LEN_CASHIER             10
#define LEN_CC_AUTH_NO          16
#define LEN_CC_LENGTH            2
#define LEN_CC_NO               40
#define LEN_CC_MAIN_NO          16
#define LEN_CC_PREFIX            6
#define LEN_CC_SUFIX             4
#define LEN_CHECK_ACCT_NO       30  /* check account number length */
#define LEN_CHECK_NO            10  /* check number length */
#define LEN_CITY               120  /* NLS */
#define LEN_CLOSE_DESC          50
#define LEN_CODE                 6
#define LEN_CODE_DESC           40
#define LEN_CODE_TYPE            4
#define LEN_COMMENTS          2000  /* NLS */
#define LEN_CONSTANT_VALUE      14
#define LEN_COUNTRY              3
#define LEN_COUPON_NO           40
#define LEN_COUPON_REF_NO       16
#define LEN_CURRENCY_RTL_DEC     1
#define LEN_CUST_ADDR          240  /* NLS */
#define LEN_CUST_ID             16
#define LEN_CUST_NAME          120  /* NLS */
#define LEN_DAY                  3
#define LEN_DATEONLY             8   /* YYYYMMDD field */
#define LEN_DATETIME            14   /* YYYYMMDDHHMMSS field */
#define LEN_DEC_AMT            (LEN_AMT + 1)
#define LEN_DEC_QTY            (LEN_QTY + 1)
#define LEN_EMAIL              100
#define LEN_EMP_ID              10
#define LEN_ERROR_CODE          25
#define LEN_ERROR_MESSAGE      1765
#define LEN_ERROR_TEXT         LEN_ERROR_MESSAGE
#define LEN_EXPORT_D            10
#define LEN_FILE_LINE_NO        10
#define LEN_FILE_TYPE_DEF        4
#define LEN_FORMAT_ID            1
#define LEN_FORMAT_DESC         20
#define LEN_FREC_DESC            5
#define LEN_IDENTI_ID           40  /* identification id number length */
#define LEN_IND                  1
#define LEN_ITEM_SEQ_NO         20
#define LEN_ITEM_NUMBER_TY       6
#define LEN_LITTLE_SEQ_NO        4
#define LEN_MACHINE            255
#define LEN_NEWLINE              1
#define LEN_NUMBER_OF_COUPONS   12
#define LEN_ORIG_VALUE          70
#define LEN_PARTNER_ID          10
#define LEN_PARTNER_TYPE         6
#define LEN_PAYMENT_REF_NO      16
#define LEN_PCT                 12
#define LEN_PHONE               20
#define LEN_POSTAL_CODE         30  /* NLS */
#define LEN_PREFIX_LEN           1
#define LEN_PROCESS_ID           5
#define LEN_PROCESS_NAME        30
#define LEN_PROOF_DELIVERY_NO   30
#define LEN_PUMP                 8
#define LEN_REF_NO              30
#define LEN_REGISTER             5
#define LEN_RETURN_DISP         10
#define LEN_ROUTING_NO          16
#define LEN_SA_REV_NO            3
#define LEN_SALES_SIGN           1
#define LEN_SIGNED_AMT         (LEN_AMT + 1)
#define LEN_SIGNED_QTY         (LEN_QTY + 1)
#define LEN_SIGNED_DEC_AMT     (LEN_AMT + 2)
#define LEN_SIGNED_DEC_QTY     (LEN_QTY + 2)
#define LEN_STAGED_COUNTER       3
#define LEN_STATE                3
#define LEN_STATUS               1
#define LEN_TAX                  5
#define LEN_TEMPERATURE          3
#define LEN_TENDER_TYPE_ID       6
#define LEN_TENDER_TYPE_DESC   120  /* NLS */
#define LEN_TERM_ID              5
#define LEN_THREAD              10
#define LEN_TIME                 6   /* HHMMSS field */
#define LEN_TOTAL_ID            10
#define LEN_TOTAL_VALUE         20
#define LEN_TRAN_NO             10
#define LEN_TRAN_SIGN            1
#define LEN_EXP_TRAN_TYPE        1
#define LEN_UPDATE_ID           30
#define LEN_VALUE_REV_NO         3
#define LEN_VAR_TYPE             1
#define LEN_VENDOR_INVC_NO      30
#define LEN_VENDOR_NO           10
#define LEN_VOUCHER_AGE          5
#define LEN_VOUCHER_NO          25
#define LEN_VOUCHER_TYPE         6
#define LEN_BANNER_ID            4
#define LEN_CATCHWEIGHT_IND      1
#define LEN_MEDIA_ID            10
#define LEN_CUST_ORD_CREATE_DATE 8
#define LEN_CUST_ORD_HEAD_ID    48
#define LEN_CUST_ORD_HEAD_DATE  14
#define LEN_UOM_QTY             12
#define LEN_SELLING_ITEM        25
#define LEN_CUST_ORD_LINE_ID    30
#define LEN_CUST_ORD_LINE_NO     6
#define LEN_SUBTRANS_TYPE_IND    1
#define LEN_PROMO_COMP          10
#define LEN_WORKSTATION_ID       3
#define LEN_SET_OF_BOOKS_ID     15
#define LEN_LAST_UPDATE_ID      15
#define LEN_PERIOD_NAME         15
#define LEN_CREDIT_PROMOTION_ID 10
#define LEN_TAX_AUTHORITY       10
#define LEN_IGTAX_CODE           6
#define LEN_IGTAX_RATE          20
#define LEN_IGTAX_AMT           21
#define LEN_DEC_IGTAX_RATE      21
#define LEN_UNIQUE_ID          128

#define NULL_ACCT_NO              (LEN_ACCT_NO + 1)
#define NULL_ACCT_PERIOD          (LEN_ACCT_PERIOD + 1)
#define NULL_BIG_SEQ_NO           (LEN_BIG_SEQ_NO + 1)
#define NULL_CASHIER              (LEN_CASHIER + 1)
#define NULL_CC_AUTH_NO           (LEN_CC_AUTH_NO + 1)
#define NULL_CC_LENGTH            (LEN_CC_LENGTH + 1)
#define NULL_CC_NO                (LEN_CC_NO + 1)
#define NULL_CC_MAIN_NO           (LEN_CC_MAIN_NO + 1)
#define NULL_CC_PREFIX            (LEN_CC_PREFIX + 1)
#define NULL_CC_SUFIX             (LEN_CC_SUFIX + 1)
#define NULL_CHECK_ACCT_NO        (LEN_CHECK_ACCT_NO + 1)
#define NULL_CHECK_NO             (LEN_CHECK_NO + 1)
#define NULL_CITY                 (LEN_CITY + 1)
#define NULL_CLOSE_DESC           (LEN_CLOSE_DESC + 1)
#define NULL_CODE                 (LEN_CODE + 1)
#define NULL_CODE_DESC            (LEN_CODE_DESC + 1)
#define NULL_CODE_TYPE            (LEN_CODE_TYPE + 1)
#define NULL_COMMENTS             (LEN_COMMENTS + 1)
#define NULL_CONSTANT_VALUE       (LEN_CONSTANT_VALUE + 1)
#define NULL_COUNTRY              (LEN_COUNTRY + 1)
#define NULL_COUPON_NO            (LEN_COUPON_NO + 1)
#define NULL_COUPON_REF_NO        (LEN_COUPON_REF_NO + 1)
#define NULL_CURRENCY_RTL_DEC     (LEN_CURRENCY_RTL_DEC + 1)
#define NULL_CUST_ADDR            (LEN_CUST_ADDR + 1)
#define NULL_CUST_ID              (LEN_CUST_ID + 1)
#define NULL_CUST_NAME            (LEN_CUST_NAME + 1)
#define NULL_DAY                  (LEN_DAY + 1)
#define NULL_DATEONLY             (LEN_DATEONLY + 1)
#define NULL_DATETIME             (LEN_DATETIME + 1)
#define NULL_DEC_AMT              (LEN_DEC_AMT + 1)
#define NULL_DEC_QTY              (LEN_DEC_QTY + 1)
#define NULL_EMAIL                (LEN_EMAIL + 1)
#define NULL_EMP_ID               (LEN_EMP_ID + 1)
#define NULL_ERROR_CODE           (LEN_ERROR_CODE + 1)
#define NULL_ERROR_TEXT           (LEN_ERROR_TEXT + 1)
#define NULL_EXPORT_D             (LEN_EXPORT_D + 1)
#define NULL_FILE_LINE_NO         (LEN_FILE_LINE_NO + 1)
#define NULL_FILE_TYPE_DEF        (LEN_FILE_TYPE_DEF + 1)
#define NULL_FORMAT_ID            (LEN_FORMAT_ID + 1)
#define NULL_FORMAT_DESC          (LEN_FORMAT_DESC + 1)
#define NULL_FREC_DESC            (LEN_FREC_DESC + 1)
#define NULL_IDENTI_ID            (LEN_IDENTI_ID + 1)
#define NULL_IND                  (LEN_IND + 1)
#define NULL_ITEM_SEQ_NO          (LEN_ITEM_SEQ_NO + 1)
#define NULL_ITEM_NUMBER_TY       (LEN_ITEM_NUMBER_TY + 1)
#define NULL_LITTLE_SEQ_NO        (LEN_LITTLE_SEQ_NO + 1)
#define NULL_MACHINE              (LEN_MACHINE + 1)
#define NULL_ORIG_VALUE           (LEN_ORIG_VALUE + 1)
#define NULL_PARTNER_ID           (LEN_PARTNER_ID + 1)
#define NULL_PARTNER_TYPE         (LEN_PARTNER_TYPE + 1)
#define NULL_PAYMENT_REF_NO       (LEN_PAYMENT_REF_NO + 1)
#define NULL_PCT                  (LEN_PCT + 1)
#define NULL_PHONE                (LEN_PHONE + 1)
#define NULL_POSTAL_CODE          (LEN_POSTAL_CODE + 1)
#define NULL_PREFIX_LEN           (LEN_PREFIX_LEN + 1)
#define NULL_PROCESS_ID           (LEN_PROCESS_ID + 1)
#define NULL_PROCESS_NAME         (LEN_PROCESS_NAME + 1)
#define NULL_PROOF_DELIVERY_NO    (LEN_PROOF_DELIVERY_NO + 1)
#define NULL_PUMP                 (LEN_PUMP + 1)
#define NULL_REF_NO               (LEN_REF_NO + 1)
#define NULL_REGISTER             (LEN_REGISTER + 1)
#define NULL_RETURN_DISP          (LEN_RETURN_DISP + 1)
#define NULL_ROUTING_NO           (LEN_ROUTING_NO + 1)
#define NULL_SA_REV_NO            (LEN_SA_REV_NO + 1)
#define NULL_SALES_SIGN           (LEN_SALES_SIGN + 1)
#define NULL_SIGNED_AMT           (LEN_SIGNED_AMT + 1)
#define NULL_SIGNED_QTY           (LEN_SIGNED_QTY + 1)
#define NULL_SIGNED_DEC_AMT       (LEN_SIGNED_DEC_AMT + 1)
#define NULL_SIGNED_DEC_QTY       (LEN_SIGNED_DEC_QTY + 1)
#define NULL_STATE                (LEN_STATE + 1)
#define NULL_STATUS               (LEN_STATUS + 1)
#define NULL_TEMPERATURE          (LEN_TEMPERATURE + 1)
#define NULL_TAX                  (LEN_TAX + 1)
#define NULL_TENDER_TYPE_ID       (LEN_TENDER_TYPE_ID + 1)
#define NULL_TENDER_TYPE_DESC     (LEN_TENDER_TYPE_DESC + 1)
#define NULL_TERM_ID              (LEN_TERM_ID + 1)
#define NULL_TIME                 (LEN_TIME + 1)
#define NULL_THREAD               (LEN_THREAD + 1)
#define NULL_TOTAL_ID             (LEN_TOTAL_ID + 1)
#define NULL_TOTAL_VALUE          (LEN_TOTAL_VALUE + 1)
#define NULL_TRAN_NO              (LEN_TRAN_NO + 1)
#define NULL_TRAN_SIGN            (LEN_TRAN_SIGN + 1)
#define NULL_UPDATE_ID            (LEN_UPDATE_ID + 1)
#define NULL_VALUE_REV_NO         (LEN_VALUE_REV_NO + 1)
#define NULL_VAR_TYPE             (LEN_VAR_TYPE + 1)
#define NULL_VENDOR_INVC_NO       (LEN_VENDOR_INVC_NO + 1)
#define NULL_VENDOR_NO            (LEN_VENDOR_NO + 1)
#define NULL_VOUCHER_AGE          (LEN_VOUCHER_AGE + 1)
#define NULL_VOUCHER_NO           (LEN_VOUCHER_NO + 1)
#define NULL_VOUCHER_TYPE         (LEN_VOUCHER_TYPE + 1)
#define NULL_BANNER_ID            (LEN_BANNER_ID + 1)
#define NULL_CATCHWEIGHT_IND      (LEN_CATCHWEIGHT_IND + 1)
#define NULL_MEDIA_ID             (LEN_MEDIA_ID + 1)
#define NULL_CUST_ORD_CREATE_DATE (LEN_CUST_ORD_CREATE_DATE + 1)
#define NULL_CUST_ORD_HEAD_ID     (LEN_CUST_ORD_HEAD_ID + 1)
#define NULL_CUST_ORD_HEAD_DATE   (LEN_CUST_ORD_HEAD_DATE + 1)
#define NULL_UOM_QTY              (LEN_UOM_QTY + 1)
#define NULL_SELLING_ITEM         (LEN_SELLING_ITEM + 1)
#define NULL_CUST_ORD_LINE_ID     (LEN_CUST_ORD_LINE_ID + 1)
#define NULL_CUST_ORD_LINE_NO     (LEN_CUST_ORD_LINE_NO + 1)
#define NULL_SUBTRANS_TYPE_IND    (LEN_SUBTRANS_TYPE_IND + 1)
#define NULL_PROMO_COMP           (LEN_PROMO_COMP + 1)
#define NULL_SET_OF_BOOKS_ID      (LEN_SET_OF_BOOKS_ID + 1)
#define NULL_LAST_UPDATE_ID       (LEN_LAST_UPDATE_ID + 1)
#define NULL_PERIOD_NAME          (LEN_PERIOD_NAME + 1)
#define NULL_WORKSTATION_ID       (LEN_WORKSTATION_ID + 1)
#define NULL_CREDIT_PROMOTION_ID  (LEN_CREDIT_PROMOTION_ID + 1)
#define NULL_TAX_AUTHORITY        (LEN_TAX_AUTHORITY + 1)
#define NULL_IGTAX_CODE           (LEN_IGTAX_CODE + 1)
#define NULL_IGTAX_RATE           (LEN_IGTAX_RATE + 1)
#define NULL_IGTAX_AMT            (LEN_IGTAX_AMT + 1)
#define NULL_DEC_IGTAX_RATE       (LEN_DEC_IGTAX_RATE + 1)
#define NULL_UNIQUE_ID            (LEN_UNIQUE_ID + 1)

/*-------------------------------------------------------------------------
common field sizes -- carry overs from salib.h
-------------------------------------------------------------------------*/

#define AMT_DECPLACES   4
#define CCEM_SIZE       LEN_CODE
#define CCAS_SIZE       LEN_CODE
#define CCVF_SIZE       LEN_CODE
#define CCVT_SIZE       LEN_CODE
#define CIDT_SIZE       LEN_CODE
#define CCSC_SIZE       LEN_CODE
#define DEMG_SIZE       LEN_CODE
#define DIST_SIZE       LEN_CODE
#define DRN_SIZE        4
#define FAILCODE_SIZE   LEN_CODE
#define ORRC_SIZE       LEN_CODE
#define POST_SIZE       LEN_CODE
#define PRMT_SIZE       LEN_CODE
#define QTY_DECPLACES   4
#define REAC_SIZE       LEN_CODE
#define SACA_SIZE       LEN_CODE
#define SADT_SIZE       LEN_CODE
#define SAIT_SIZE       LEN_CODE
#define SAMS_SIZE       LEN_CODE
#define SARR_SIZE       LEN_CODE
#define SART_SIZE       LEN_CODE
#define SASI_SIZE       LEN_CODE
#define SAST_SIZE       LEN_CODE
#define SATC_SIZE       LEN_CODE
#define STRG_SIZE       LEN_CODE
#define STRT_SIZE       LEN_CODE
#define SYSE_SIZE       LEN_CODE
#define TAXC_SIZE       LEN_CODE
#define TENS_SIZE       LEN_CODE
#define TENT_SIZE       LEN_CODE
#define TRAS_SIZE       LEN_CODE
#define TRAT_SIZE       LEN_CODE
#define USTY_SIZE       LEN_CODE
#define WEAT_SIZE       LEN_CODE
#define WSTG_SIZE       LEN_CODE
#define YSNO_SIZE       LEN_CODE
#define IDMH_SIZE       LEN_CODE /* identification method number length */

/*-------------------------------------------------------------------------
common field sizes -- carry overs from salib.h -- obsolete
-------------------------------------------------------------------------*/

#define AMT_SIZE        LEN_AMT
#define CASHIER_SIZE    LEN_EMP_ID
#define DATESIZE        LEN_DATEONLY
#define DATETIMESIZE    LEN_DATETIME
#define LOC_SIZE        LEN_LOC
#define POSREG_SIZE     LEN_REGISTER
#define QTY_SIZE        LEN_QTY
#define TIMESIZE        LEN_TIME
#define TRANNO_SIZE     LEN_TRAN_NO

/*-------------------------------------------------------------------------
common field sizes -- carry overs from saexplib.h -- obsolete
-------------------------------------------------------------------------*/

#define LEN_CUST_ID_TYPE      CIDT_SIZE
#define LEN_DISCOUNT_REF_NO   LEN_PROMOTION
#define LEN_DISCOUNT_TYPE     1
#define LEN_REASON_CODE       REAS_SIZE
#define LEN_REVERSAL_NO       LEN_SA_REV_NO
#define LEN_SALESPERSON       LEN_EMP_ID
#define LEN_SEQ_NO            LEN_BIG_SEQ_NO
#define LEN_SHORTDATE         LEN_DATEONLY
#define LEN_STORE_DAY_SEQ_NO  LEN_BIG_SEQ_NO
#define LEN_TENDER_TYPE       TENT_SIZE
#define LEN_TRAN_SEQ_NO       LEN_BIG_SEQ_NO
#define LEN_TRAN_TYPE         TRAT_SIZE
#define LEN_TOTAL_SEQ_NO      LEN_BIG_SEQ_NO
#define LEN_USAGE_TYPE        USTY_SIZE
#define LEN_WASTE_PCT         LEN_PCT

#define NULL_DISCOUNT_REF_NO  NULL_PROMOTION
#define NULL_DISCOUNT_TYPE    (LEN_DISCOUNT_TYPE + 1)
#define NULL_REASON_CODE      NULL_CODE
#define NULL_SALESPERSON      NULL_EMP_ID
#define NULL_SEQ_NO           NULL_BIG_SEQ_NO
#define NULL_SHORTDATE        NULL_DATEONLY
#define NULL_STORE_DAY_SEQ_NO NULL_BIG_SEQ_NO
#define NULL_TENDER_TYPE      NULL_CODE
#define NULL_TRAN_SEQ_NO      NULL_BIG_SEQ_NO
#define NULL_TOTAL_SEQ_NO     NULL_BIG_SEQ_NO
#define NULL_TRAN_TYPE        NULL_CODE
#define NULL_USAGE_TYPE       NULL_CODE
#define NULL_WASTE_PCT        NULL_PCT

/*-------------------------------------------------------------------------
Irina's definitions
-------------------------------------------------------------------------*/

#define LEN_PREFIX             2
#define LEN_VAR_ORIGIN         1
#define LEN_VAR_TYPE           1
#define LEN_DIGIT              2
#define LEN_CHECK_POS          2

#define NULL_PREFIX            (LEN_PREFIX + 1)
#define NULL_VAR_ORIGIN        (LEN_VAR_ORIGIN + 1)
#define NULL_VAR_TYPE          (LEN_VAR_TYPE + 1)
#define NULL_DIGIT             (LEN_DIGIT + 1)
#define NULL_CHECK_POS         (LEN_CHECK_POS + 1)

/*-------------------------------------------------------------------------
Irina's definitions -- obsolete
-------------------------------------------------------------------------*/

#define LEN_STORE_DAY_SEQ_NO   LEN_BIG_SEQ_NO
#define LEN_INT_POS_IND        LEN_IND
#define LEN_WASTE_TYPE         WSTG_SIZE

#define NULL_INT_POS_IND       NULL_IND
#define NULL_WASTE_TYPE        NULL_CODE

#endif


