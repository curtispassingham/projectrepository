/*=========================================================================

   sastoreday.c: storeday load and lookup

=========================================================================*/

#include "sagetref.h"

static STOREDAYDAT *pa_storedaytable = NULL;  /* memory-mapped storeday table */
static size_t pl_storedaytablelen = 0;   /* number of items in storedaytable */

/* prototypes */
STOREDAYDAT *storeday_lookup(char *is_store, char *is_business_date);
static int storedaycmp(const void *is_s1, const void *is_s2);
int storeday_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
storeday_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
STOREDAYDAT *storeday_lookup(char *is_store, char *is_business_date)
{
   char ls_key[LEN_STORE + LEN_DATEONLY];
   char *li_null;
   int li_len = 0;

   if (pl_storedaytablelen == 0)
      return( NULL);

   if((li_null = memchr(is_store, '\0', LEN_STORE)) == NULL)
   {
      memcpy(ls_key, is_store, LEN_STORE);
   }
   else
   {
      li_len = li_null - is_store;
      memcpy(ls_key, is_store, li_len);
      memset(ls_key + li_len, ' ', LEN_STORE - li_len);
   }

   memcpy(ls_key + LEN_STORE, is_business_date, LEN_DATEONLY);
   return((STOREDAYDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_storedaytable,
           pl_storedaytablelen, sizeof(STOREDAYDAT), storedaycmp));
} /* end storeday_lookup() */

/*-------------------------------------------------------------------------
storedaycmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int storedaycmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_STORE + LEN_DATEONLY));
} /* end storedaycmp */


/*-------------------------------------------------------------------------
storeday_loadfile() loads the storeday data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int storeday_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_storedaytable = (STOREDAYDAT *)mmap( NULL, lr_statbuf.st_size, PROT_READ, MAP_SHARED, li_datafile, 0 );
            if(pa_storedaytable != (STOREDAYDAT *)MAP_FAILED)
            {
               pl_storedaytablelen = lr_statbuf.st_size / sizeof(STOREDAYDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
	 } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end storeday_loadfile() */


