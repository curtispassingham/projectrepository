/* oranumdiv.c */

#include "saoranum.h"

int OraNumDiv(char *is_num1, char *is_num2, char *os_num3, long il_size)
{
   char *function = "OraNumDiv";
   char err_data[1379];

   short li_retcode;
   OCINumber l_ocinumber_num1, l_ocinumber_num2, l_ocinumber_num3;
   ub4 ll_size = (ub4)il_size;

   if (OraNumFromString(&l_ocinumber_num1, is_num1) < 0) return(-1);
   if (OraNumFromString(&l_ocinumber_num2, is_num2) < 0) return(-1);

   if ((li_retcode = OCINumberDiv(err,
                                  &l_ocinumber_num1,
                                  &l_ocinumber_num2,
                                  &l_ocinumber_num3)) != OCI_SUCCESS)
   {
      sprintf(err_data, "OCINumberDiv failed with return code %d", li_retcode);
      write_oci_error(function,err_data);
      return(-1);
   }

   if (OraNumToString(&l_ocinumber_num3, os_num3, &ll_size) < 0) return(-1);

   return(0);
}


