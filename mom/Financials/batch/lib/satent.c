/* tender types support module */

#include <retek.h>
#include "salib.h"

typedef struct {
   const char *s_structstring;
   int i_token;
} TOKENMAP;

static TOKENMAP pa_tenttokentable[] = {
   { TENT_ERR,    TENTTT_ERR    },
   { TENT_CASH,   TENTTT_CASH   },
   { TENT_CHECK,  TENTTT_CHECK  },
   { TENT_CCARD,  TENTTT_CCARD  },
   { TENT_VOUCH,  TENTTT_VOUCH  },
   { TENT_COUPON, TENTTT_COUPON },
   { TENT_MORDER, TENTTT_MORDER },
   { TENT_DCARD,  TENTTT_DCARD  },
   { TENT_DRIVEO, TENTTT_DRIVEO },
   { TENT_TERM,   TENTTT_TERM   },
   { TENT_SOCASS, TENTTT_SOCASS },
   { NULL,        0             }
};

/*-------------------------------------------------------------------------
tent_lookup() looks up the code and returns its token, or 0 if not found.
-------------------------------------------------------------------------*/
int tent_lookup(char *is_string)
{
   TOKENMAP *lr_p;

   for(lr_p = pa_tenttokentable; lr_p->s_structstring; ++lr_p)
   {
      if(strncmp(is_string, lr_p->s_structstring, TENT_SIZE) == 0)
      {
         return(lr_p->i_token);
      } /* end if */
   } /* end for */

   return(0);
} /* end tent_lookup() */



