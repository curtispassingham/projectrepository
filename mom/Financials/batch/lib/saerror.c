/*=========================================================================

   saerror.c: error load and lookup

=========================================================================*/

#include "sagetref.h"

static ERRORDAT *pa_errortable = NULL;  /* memory-mapped error table */
static size_t pl_errortablelen = 0;   /* number of items in errortable */

static ERRORDAT *pa_errortableidx = NULL;
static long pl_errortableidxlen = 0;

/* prototypes */
ERRORDAT *error_lookup(const char *is_key);
static int errorcmp(const void *is_s1, const void *is_s2);
int error_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
error_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
ERRORDAT *error_lookup(const char *is_key)
{
   if(pl_errortablelen == 0)
   {
      return(NULL);
   } /* end if */

   return((ERRORDAT *)bsearch(
          (const void *)is_key, (const void *)pa_errortable,
          pl_errortablelen, sizeof(ERRORDAT), errorcmp));
} /* end error_lookup() */


/*-------------------------------------------------------------------------
errorcmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int errorcmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_ERROR_CODE));
} /* end errorcmp() */


/*-------------------------------------------------------------------------
error_loadfile() loads the error data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int error_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_errortable = (ERRORDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ, MAP_SHARED, li_datafile, 0);
            if(pa_errortable != (ERRORDAT *)MAP_FAILED)
            {
               pl_errortablelen = lr_statbuf.st_size / sizeof(ERRORDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end error_loadfile() */


