/*=========================================================================

   sacode.c: code load and lookup

=========================================================================*/

#include "sagetref.h"
#include "salib.h"

static CODEDAT *pa_codetable = NULL;  /* memory-mapped code table */
static size_t pl_codetablelen = 0;   /* number of items in codetable */

/* function prototypes */
int code_get_seq(const char *is_code_type, char *is_code, int ii_len);
CODEDAT *code_lookup(const char *is_code_type, char *is_code, int ii_len);
static int codecmp(const void *is_s1, const void *is_s2);
int code_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
code_get_seq() checks the memory-mapped file for the code_type & code and
returns the sequence number, or 0 if not found.
-------------------------------------------------------------------------*/
int code_get_seq(const char *is_code_type, char *is_code, int ii_len)
{
   CODEDAT *lr_c;

   lr_c = code_lookup(is_code_type, is_code, ii_len);
   if(lr_c != NULL)
   {
      return(atoin(lr_c->code_seq, sizeof(lr_c->code_seq)));
   } /* end if */

   return(0);
} /* end code_get_seq() */

/*-------------------------------------------------------------------------
code_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
CODEDAT *code_lookup( const char *is_code_type, char *is_code, int ii_length)
{
   char la_key[LEN_CODE_TYPE + LEN_CODE];

   if (pl_codetablelen == 0)
   {
      return(NULL);
   }

   memcpy(la_key, is_code_type, LEN_CODE_TYPE);
   memcpy(la_key + LEN_CODE_TYPE, is_code, ii_length);
   memset(la_key + LEN_CODE_TYPE + ii_length, ' ', LEN_CODE - ii_length);

   return((CODEDAT *)bsearch(
          (const void *)la_key, (const void *)pa_codetable,
          pl_codetablelen, sizeof(CODEDAT), codecmp));
}


/*-------------------------------------------------------------------------
codecmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int codecmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_CODE_TYPE + LEN_CODE));
} /* end codecmp() */


/*-------------------------------------------------------------------------
code_loadfile() loads the code data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int code_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if ((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if (fstat(li_datafile, &lr_statbuf) == 0 )
      {
         if (lr_statbuf.st_size != 0)
         {
            pa_codetable = (CODEDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ, MAP_SHARED, li_datafile, 0);
            if (pa_codetable != (CODEDAT *)MAP_FAILED)
            {
               pl_codetablelen = lr_statbuf.st_size / sizeof(CODEDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */

      close(li_datafile);
   } /* end if */

   return(li_rv);
}


