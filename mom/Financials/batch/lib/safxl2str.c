/* convert fixed length field with decimal places to string */
/* i.e. [00000000000001234500] to "123.4500" */

#include <ctype.h>
#define EAT_TRAILING_ZEROS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef SELFTEST

int main(int argc, char *argv[])
{
     int li_len, li_dp;
     char ls_string[256];

     if (argc == 4)
     {
          li_len = atoi(argv[ 2 ]);
          li_dp  = atoi(argv[ 3 ]);
          fxl2str(ls_string, argv[ 1 ], li_len, li_dp);
          printf("<%s>\n", ls_string);
     } /* end if */
     else 
     {
          printf( "Usage: FXL2STR fxl len decplaces\n" );
     } /* end else */
} /* end main() */
#endif

void fxl2str(char *os_str, char *is_fxl, int ii_len, int ii_dp)
{
     char *ls_stop;

     /* find first significant digit */
     ls_stop = is_fxl + ((ii_len - ii_dp) - 1);
     for( ; is_fxl < ls_stop; ++is_fxl)
     {
         if(isdigit(*is_fxl))
	 { 
             if(*is_fxl != '0')
	     {
                 break;
             } /* end if */
         } /* end if */
     } /* end for */

     /* copy integer portion */
     while(is_fxl <= ls_stop)
     {
         *os_str++ = *is_fxl++;
     } /* end while */

     /* if fractional part to do... */
     if(ii_dp > 0)
     {
          /* make decimal point */
          *os_str++ = '.';

          /* copy fractional portion */
          while(ii_dp--)
               *os_str++ = *is_fxl++;

#ifdef EAT_TRAILING_ZEROS
          /* truncate all but one trailing .0 */
          while ( *--os_str == '0' )
	  {
              *os_str = '\0';
	  } /* end while */

          if ( *os_str++ == '.' )
	  {
              *os_str++ = '0';
	  } /* end if */
#endif
     } /* end if */

     /* terminate */
     *os_str = '\0';
} /* end fxl2str() */




