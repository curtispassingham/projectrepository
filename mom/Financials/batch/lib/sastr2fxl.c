/* convert string to fixed length field with decimal places */
/* i.e. "123.45" to [00000000000001234500] */

#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef SELFTEST

int main(int argc, char *argv[])
{
   int li_len, li_dp;
   char ls_string[256];

   if (argc == 4)
   {
      li_len = atoi(argv[2]);
      li_dp  = atoi(argv[3]);
      str2fxl(ls_string, argv[1], li_len, li_dp);
      ls_string[len] = '\0';
      printf("<%s>\n", ls_string);
   }
   else printf("Usage: STR2FXL string len decplaces\n");
}
#endif


void str2fxl(char *os_fxl, char *is_str, int ii_len, int ii_dp)
{
   char *ls_p1, *ls_p2;

   /* set destination field to 0000000... */
   memset(os_fxl, '0', ii_len);

   /* find first non-blank char */
   for(ls_p1 = is_str; *ls_p1; ++ls_p1)
   {
      if(!isspace(*ls_p1))
      {
         /* find fractional portion and split off */
         if((ls_p2 = strchr(ls_p1, '.')) != NULL)
	 {
            *ls_p2++ = '\0';
	 } /* end if */

         /* Check to see if the number is negative. */
         if (*ls_p1 == '-')
         {
            /* Make the first character of the */
            /* fixed-length field a negative sign. */
            *os_fxl = '-';

            continue;
         } /* end if */

         /* output integer portion */
         ii_len -= ii_dp + strlen(ls_p1);
         os_fxl += ii_len;

         while (*ls_p1 && ii_len--)
	 {
            *os_fxl++ = *ls_p1++;
	 } /* end while */

         /* output fractional portion */
         if (ls_p2)
	 {
            while (*ls_p2 && ii_dp--)
	    {
               *os_fxl++ = *ls_p2++;
	    } /* end while */
	 } /* end if */

         /* done, break out of for loop */
         break;

      } /* end if */
   } /* end for */

} /* end str2fxl() */




