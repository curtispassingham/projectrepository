/* putrec.c */

#include <retek.h>
#include "salib.h"


/*---------------------------------------------------------------------------
putrec(char *is_point, FILE *if_f) writes the record at is_point to FILE if_f.
Returns TRUE if ok or FALSE if not.
---------------------------------------------------------------------------*/
int putrec(char *is_point, FILE *if_f)
{
    int li_rv;

    li_rv = TRUE;
    for( ; *is_point; ++is_point)
    {
       if(putc(*is_point, if_f) == -1)
       {
           li_rv = FALSE;
       } /* end if */
       if(*is_point == '\n')
       {
          break;
       } /* end if */
    } /* end for */

    return(li_rv);
} /* end putrec() */


