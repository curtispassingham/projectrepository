/* -------------------------- */
/*                            */
/* getTotals.pc                */
/*                            */
/* retrieves totals to an     */
/* internal array.            */
/*                            */
/* Assumption: the usage_type */
/* is the system_code with a  */
/* '%' concatenated at the    */
/* end.                       */
/*                            */
/* -------------------------- */

#include <retek_2.h>
#include "salib.h"

EXEC SQL INCLUDE SQLCA.H;
long SQLCODE;

/* Variables used as start string (to limit query of driving cursor) */
char ps_restart_total_seq_no[NULL_BIG_SEQ_NO];

static int   pi_pos_cursor_open = 0;
static long  pl_pos_records_remaining = 0;
static long  pl_pos_total_records = 0;
static long  pl_pos_records_to_process = 0;
static int   pi_pos_cursor_empty = 0;

static int   pi_neg_cursor_open = 0;
static long  pl_neg_records_remaining = 0;
static long  pl_neg_total_records = 0;
static long  pl_neg_records_to_process = 0;
static int   pi_neg_cursor_empty = 0;

static struct totals {
   char ( *total_seq_no)[NULL_BIG_SEQ_NO];
   char ( *value)[NULL_SIGNED_AMT];
   char ( *value_rev_no)[NULL_VALUE_REV_NO];
   char ( *total_type)[NULL_TOTAL_TYPE];
   char ( *total_id)[NULL_TOTAL_ID];
   char ( *ref_no1)[NULL_REF_NO];
   char ( *ref_no2)[NULL_REF_NO];
   char ( *ref_no3)[NULL_REF_NO];
   char ( *status)[NULL_CODE];
   char ( *usage_type)[NULL_CODE];
} la_pos_totals, la_neg_totals;


static int processPosCursor(
   char is_store_day_seq_no[NULL_BIG_SEQ_NO],
   char is_store[NULL_STORE],
   char is_day[NULL_DAY],
   char ios_usage_type[NULL_CODE],
   char os_total_seq_no[NULL_BIG_SEQ_NO],
   char os_total_value[NULL_SIGNED_AMT],
   char os_rev_no[NULL_VALUE_REV_NO],
   char os_total_type[NULL_TOTAL_TYPE],
   char os_total_id[NULL_TOTAL_ID],
   char os_ref_no1[NULL_REF_NO],
   char os_ref_no2[NULL_REF_NO],
   char os_ref_no3[NULL_REF_NO],
   char os_status[NULL_CODE],
   char os_sign[NULL_CODE],
   unsigned int ii_max_counter,
   long il_multiplier)
{
   char* function = "processPosCursor";

   EXEC SQL DECLARE c_pos_totals CURSOR FOR
   SELECT vsgt.total_seq_no, 
          vsgt.value * :il_multiplier, 
          vsgt.value_rev_no,
          DECODE(vsgt.precedence, 
                 1, 'HQ',
                 2, 'STORE', 
                 3, 'SYS', 
                 4, 'POS', 
                 'ERROR'),
          vsgt.total_id, 
          NVL(vsgt.ref_no1, ' '), 
          NVL(vsgt.ref_no2, ' '), 
          NVL(vsgt.ref_no3, ' '),
          vsgt.status,
          tu.usage_type
     FROM v_sa_get_total vsgt,
          sa_total_usage tu
     WHERE vsgt.store_day_seq_no = TO_NUMBER(:is_store_day_seq_no)
      AND vsgt.store = TO_NUMBER(:is_store)
      AND vsgt.day = TO_NUMBER(:is_day)
      AND vsgt.status = :SAST_P
      AND vsgt.total_id = tu.total_id
      AND vsgt.total_rev_no = tu.total_rev_no
      AND tu.usage_type LIKE :ios_usage_type
      AND vsgt.precedence = 
          (SELECT min(vsgt2.precedence) 
             FROM v_sa_get_total vsgt2
            WHERE vsgt2.total_seq_no = vsgt.total_seq_no
              AND vsgt2.store = vsgt.store
              AND vsgt2.day = vsgt.day)
      AND vsgt.value_rev_no = 
          (SELECT max(vsgt2.value_rev_no) 
             FROM v_sa_get_total vsgt2
            WHERE vsgt2.total_seq_no = vsgt.total_seq_no 
              AND vsgt2.store = vsgt.store
              AND vsgt2.day = vsgt.day
              AND vsgt2.precedence = 
                  (SELECT min(vsgt3.precedence)
                     FROM v_sa_get_total vsgt3
                    WHERE vsgt3.total_seq_no = vsgt.total_seq_no
                      AND vsgt3.store = vsgt.store
                      AND vsgt3.day = vsgt.day))
      AND NOT EXISTS                    /* latest revision has not been exported */
          (SELECT 1
             FROM sa_exported er
            WHERE er.total_seq_no = vsgt.total_seq_no
              AND er.store = vsgt.store
              AND er.day = vsgt.day
              AND er.system_code = tu.usage_type)
      AND NOT EXISTS                    /* and no errors for the total. */
           (SELECT 1
              FROM sa_error e, 
                   sa_error_impact ei
             WHERE vsgt.total_seq_no = e.total_seq_no
               AND vsgt.store = e.store
               AND vsgt.day = e.day
               AND e.error_code = ei.error_code
               AND ei.system_code LIKE :ios_usage_type
               AND e.hq_override_ind != :YSNO_Y);

   if (la_pos_totals.total_seq_no == NULL)
   {
      if((la_pos_totals.total_seq_no = (char(*)[NULL_BIG_SEQ_NO])
          calloc(ii_max_counter, NULL_BIG_SEQ_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.total_seq_no.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.value == NULL)
   {
      if((la_pos_totals.value = (char(*)[NULL_SIGNED_AMT])
          calloc(ii_max_counter, NULL_SIGNED_AMT))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.value.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.value_rev_no == NULL)
   {
      if((la_pos_totals.value_rev_no = (char(*)[NULL_VALUE_REV_NO])
          calloc(ii_max_counter, NULL_VALUE_REV_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.value_rev_no.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.total_type == NULL)
   {
      if((la_pos_totals.total_type = (char(*)[NULL_TOTAL_TYPE])
          calloc(ii_max_counter, NULL_TOTAL_TYPE))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.total_type.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.total_id == NULL)
   {
      if((la_pos_totals.total_id = (char(*)[NULL_TOTAL_ID])
          calloc(ii_max_counter, NULL_TOTAL_ID))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.total_id.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.ref_no1 == NULL)
   {
      if((la_pos_totals.ref_no1 = (char(*)[NULL_REF_NO])
          calloc(ii_max_counter, NULL_REF_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.ref_no1.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.ref_no2 == NULL)
   {
      if((la_pos_totals.ref_no2 = (char(*)[NULL_REF_NO])
          calloc(ii_max_counter, NULL_REF_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.ref_no2.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.ref_no3 == NULL)
   {
      if((la_pos_totals.ref_no3 = (char(*)[NULL_REF_NO])
          calloc(ii_max_counter, NULL_REF_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.ref_no3.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.status == NULL)
   {
      if((la_pos_totals.status = (char(*)[NULL_CODE])
          calloc(ii_max_counter, NULL_CODE))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.status.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_pos_totals.usage_type == NULL)
   {
      if((la_pos_totals.usage_type = (char(*)[NULL_CODE])
          calloc(ii_max_counter, NULL_CODE))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_pos_totals.usage_type.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (!pi_pos_cursor_open)
   {
      EXEC SQL OPEN c_pos_totals;
      if (SQL_ERROR_FOUND)
      {
         strcpy(err_data, "open c_pos_totals");
         strcpy(table, "v_sa_get_total, sa_total_usage, sa_exported, sa_exported_rev, sa_error, sa_error_impact");
         WRITE_ERROR(SQLCODE, function, table, err_data);
         return(-1);
      }
      pi_pos_cursor_open = TRUE;
   }

   if (pl_pos_records_to_process >= pl_pos_records_remaining)
   {
      if (!pi_pos_cursor_empty)
      {
         EXEC SQL FOR :ii_max_counter
         FETCH c_pos_totals INTO :la_pos_totals.total_seq_no,
                                 :la_pos_totals.value,
                                 :la_pos_totals.value_rev_no,
                                 :la_pos_totals.total_type,
                                 :la_pos_totals.total_id,
                                 :la_pos_totals.ref_no1,
                                 :la_pos_totals.ref_no2,
                                 :la_pos_totals.ref_no3,
                                 :la_pos_totals.status,
                                 :la_pos_totals.usage_type;
         if (SQL_ERROR_FOUND)
         {
            strcpy(err_data, "fetch c_pos_totals");
            strcpy(table, "v_sa_get_total, sa_total_usage, sa_exported, sa_exported_rev, sa_error, sa_error_impact");
            WRITE_ERROR(SQLCODE, function, table, err_data);
            return(-1);
         }

         if (NO_DATA_FOUND)
            pi_pos_cursor_empty = TRUE;

         pl_pos_records_to_process = 0;
         pl_pos_records_remaining = NUM_RECORDS_PROCESSED - pl_pos_total_records;
         pl_pos_total_records = NUM_RECORDS_PROCESSED;

         if (pl_pos_records_remaining == 0)
         {
            pi_pos_cursor_empty = TRUE;
            return(1); 
         }
      }   
      else
      {
         pi_pos_cursor_empty = TRUE;
         return(1);
      }
   }

   /* load the output variables and return to calling app */
   strcpy(ios_usage_type,  la_pos_totals.usage_type  [pl_pos_records_to_process]);
   strcpy(os_total_seq_no, la_pos_totals.total_seq_no[pl_pos_records_to_process]);
   strcpy(os_total_value,  la_pos_totals.value       [pl_pos_records_to_process]);
   strcpy(os_rev_no,       la_pos_totals.value_rev_no[pl_pos_records_to_process]);
   strcpy(os_total_type,   la_pos_totals.total_type  [pl_pos_records_to_process]);
   strcpy(os_total_id,     la_pos_totals.total_id    [pl_pos_records_to_process]);
   strcpy(os_ref_no1,      la_pos_totals.ref_no1     [pl_pos_records_to_process]);
   strcpy(os_ref_no2,      la_pos_totals.ref_no2     [pl_pos_records_to_process]);
   strcpy(os_ref_no3,      la_pos_totals.ref_no3     [pl_pos_records_to_process]);
   strcpy(os_status,       la_pos_totals.status      [pl_pos_records_to_process]);
   strcpy(os_sign, SAFD_P);

   pl_pos_records_to_process++;
   return(0);
}

static int processNegCursor(
   char is_store_day_seq_no[NULL_BIG_SEQ_NO],
   char is_store[NULL_STORE],
   char is_day[NULL_DAY],
   char ios_usage_type[NULL_CODE],
   char os_total_seq_no[NULL_BIG_SEQ_NO],
   char os_total_value[NULL_SIGNED_AMT],
   char os_rev_no[NULL_VALUE_REV_NO],
   char os_total_type[NULL_TOTAL_TYPE],
   char os_total_id[NULL_TOTAL_ID],
   char os_ref_no1[NULL_REF_NO],
   char os_ref_no2[NULL_REF_NO],
   char os_ref_no3[NULL_REF_NO],
   char os_status[NULL_CODE],
   char os_sign[NULL_CODE],
   unsigned int ii_max_counter,
   long il_multiplier)
{
   char* function = "processNegCursor";

   EXEC SQL DECLARE c_neg_totals CURSOR FOR
   /* total that has been exported, but not deleted */
   SELECT vsgt.total_seq_no, 
          vsgt.value * :il_multiplier, 
          vsgt.value_rev_no,
          DECODE(vsgt.precedence, 
                 1, 'HQ',
                 2, 'STORE', 
                 3, 'SYS', 
                 4, 'POS', 
                 'ERROR'),
          vsgt.total_id, 
          NVL(vsgt.ref_no1, ' '), 
          NVL(vsgt.ref_no2, ' '), 
          NVL(vsgt.ref_no3, ' '),
          vsgt.status,
          tu.usage_type
     FROM v_sa_get_total vsgt,
          sa_total_usage tu
    WHERE vsgt.store_day_seq_no = TO_NUMBER(:is_store_day_seq_no)
      AND vsgt.store = TO_NUMBER(:is_store)
      AND vsgt.day = TO_NUMBER(:is_day)
      AND vsgt.status = :SAST_P
      AND vsgt.total_id = tu.total_id
      AND vsgt.total_rev_no = tu.total_rev_no
      AND tu.usage_type LIKE :ios_usage_type
      AND vsgt.value_rev_no = 
          (SELECT rev_no 
             FROM sa_exported_rev erv
            WHERE erv.total_seq_no = vsgt.total_seq_no
              AND erv.store = vsgt.store
              AND erv.day = vsgt.day
              AND erv.system_code  = tu.usage_type
              AND erv.rev_no = 
                  (SELECT MAX(rev_no)
                     FROM sa_exported_rev erv2
                    WHERE erv2.total_seq_no = vsgt.total_seq_no
                      AND erv2.store = vsgt.store
                      AND erv2.day = vsgt.day
                      AND erv2.system_code  = tu.usage_type))
      AND NOT EXISTS                    /* latest revision has not been exported */
          (SELECT 1
             FROM sa_exported er
            WHERE er.total_seq_no = vsgt.total_seq_no
              AND er.store = vsgt.store
              AND er.day = vsgt.day
              AND er.system_code = tu.usage_type)
      AND EXISTS                        /* but a previous revision has been exported */
          (SELECT 1
             FROM sa_exported_rev erv
            WHERE erv.total_seq_no = vsgt.total_seq_no
              AND erv.store = vsgt.store
              AND erv.day = vsgt.day
              AND erv.rev_no = vsgt.value_rev_no
              AND erv.system_code = tu.usage_type)
      AND NOT EXISTS                    /* and no errors for the total. */
          (SELECT 1
             FROM sa_error e, 
                  sa_error_impact ei
            WHERE vsgt.total_seq_no = e.total_seq_no
              AND vsgt.store = e.store
              AND vsgt.day = e.day
              AND e.error_code = ei.error_code
              AND ei.system_code LIKE :ios_usage_type
              AND e.hq_override_ind != :YSNO_Y)
   UNION
   /* total has been deleted and latest revision has been exported */
   SELECT vsgt.total_seq_no, 
          vsgt.value * :il_multiplier, 
          vsgt.value_rev_no,
          DECODE(vsgt.precedence, 
                 1, 'HQ',
                 2, 'STORE', 
                 3, 'SYS', 
                 4, 'POS', 
                 'ERROR'),
          vsgt.total_id, 
          NVL(vsgt.ref_no1, ' '), 
          NVL(vsgt.ref_no2, ' '), 
          NVL(vsgt.ref_no3, ' '),
          vsgt.status,
          tu.usage_type
     FROM v_sa_get_total vsgt,
          sa_total_usage tu
    WHERE vsgt.store_day_seq_no = TO_NUMBER(:is_store_day_seq_no)
      AND vsgt.store = TO_NUMBER(:is_store)
      AND vsgt.day = TO_NUMBER(:is_day)
      AND vsgt.status != :SAST_P
      AND vsgt.total_id = tu.total_id
      AND vsgt.total_rev_no = tu.total_rev_no
      AND tu.usage_type LIKE :ios_usage_type
      AND vsgt.precedence = 
          (SELECT min(vsgt2.precedence) 
             FROM v_sa_get_total vsgt2
            WHERE vsgt2.total_seq_no = vsgt.total_seq_no
              AND vsgt2.store = vsgt.store
              AND vsgt2.day = vsgt.day)
      AND vsgt.value_rev_no = 
          (SELECT max(vsgt2.value_rev_no) 
             FROM v_sa_get_total vsgt2
            WHERE vsgt2.total_seq_no = vsgt.total_seq_no 
              AND vsgt2.store = vsgt.store
              AND vsgt2.day = vsgt.day
              AND vsgt2.precedence = 
                  (SELECT min(vsgt3.precedence)
                     FROM v_sa_get_total vsgt3
                    WHERE vsgt3.total_seq_no = vsgt.total_seq_no
                      AND vsgt3.store = vsgt.store
                      AND vsgt3.day = vsgt.day))
      AND EXISTS                        /* latest revision has been exported */
          (SELECT 1
             FROM sa_exported er
            WHERE er.total_seq_no = vsgt.total_seq_no
              AND er.store = vsgt.store
              AND er.day = vsgt.day
              AND er.system_code = tu.usage_type
              AND er.status = :SAST_P)
      AND NOT EXISTS                    /* and no errors for the total. */
          (SELECT 1
             FROM sa_error e, 
                  sa_error_impact ei
            WHERE vsgt.total_seq_no = e.total_seq_no
              AND vsgt.store = e.store
              AND vsgt.day = e.day
              AND e.error_code = ei.error_code
              AND ei.system_code LIKE :ios_usage_type
              AND e.hq_override_ind != :YSNO_Y)
   UNION
   /* total has been deleted and a previous revision has been exported */
   SELECT vsgt.total_seq_no, 
          vsgt.value * :il_multiplier, 
          vsgt.value_rev_no,
          DECODE(vsgt.precedence, 
                 1, 'HQ',
                 2, 'STORE', 
                 3, 'SYS', 
                 4, 'POS', 
                 'ERROR'),
          vsgt.total_id, 
          NVL(vsgt.ref_no1, ' '), 
          NVL(vsgt.ref_no2, ' '), 
          NVL(vsgt.ref_no3, ' '),
          vsgt.status,
          tu.usage_type
     FROM v_sa_get_total vsgt,
          sa_total_usage tu
    WHERE vsgt.store_day_seq_no = TO_NUMBER(:is_store_day_seq_no)
      AND vsgt.store = TO_NUMBER(:is_store)
      AND vsgt.day = TO_NUMBER(:is_day)
      AND vsgt.status != :SAST_P
      AND vsgt.total_id = tu.total_id
      AND vsgt.total_rev_no = tu.total_rev_no
      AND tu.usage_type LIKE :ios_usage_type
      AND vsgt.value_rev_no = 
          (SELECT rev_no 
             FROM sa_exported_rev erv
            WHERE erv.total_seq_no = vsgt.total_seq_no
              AND erv.store = vsgt.store
              AND erv.day = vsgt.day
              AND erv.system_code  = tu.usage_type
              AND erv.rev_no = 
                  (SELECT MAX(rev_no)
                     FROM sa_exported_rev erv2
                    WHERE erv2.total_seq_no = vsgt.total_seq_no
                      AND erv2.store = vsgt.store
                      AND erv2.day = vsgt.day
                      AND erv2.system_code  = tu.usage_type))
      AND NOT EXISTS                    /* latest revision has not been exported */
          (SELECT 1
             FROM sa_exported er
            WHERE er.total_seq_no = vsgt.total_seq_no
              AND er.store = vsgt.store
              AND er.day = vsgt.day
              AND er.system_code = tu.usage_type)
      AND EXISTS                        /* a previous revision has been exported */
          (SELECT 1
             FROM sa_exported_rev erv
            WHERE erv.total_seq_no = vsgt.total_seq_no
              AND erv.store = vsgt.store
              AND erv.day = vsgt.day
              AND erv.rev_no = vsgt.value_rev_no
              AND erv.system_code = tu.usage_type)
      AND NOT EXISTS                    /* and no errors for the total. */
          (SELECT 1
             FROM sa_error e, 
                  sa_error_impact ei
            WHERE vsgt.total_seq_no = e.total_seq_no
              AND vsgt.store = e.store
              AND vsgt.day = e.day
              AND e.error_code = ei.error_code
              AND ei.system_code LIKE :ios_usage_type
              AND e.hq_override_ind != :YSNO_Y);

   if (la_neg_totals.total_seq_no == NULL)
   {
      if((la_neg_totals.total_seq_no = (char(*)[NULL_BIG_SEQ_NO])
          calloc(ii_max_counter, NULL_BIG_SEQ_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.total_seq_no.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.value == NULL)
   {
      if((la_neg_totals.value = (char(*)[NULL_SIGNED_AMT])
          calloc(ii_max_counter, NULL_SIGNED_AMT))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.value.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.value_rev_no == NULL)
   {
      if((la_neg_totals.value_rev_no = (char(*)[NULL_VALUE_REV_NO])
          calloc(ii_max_counter, NULL_VALUE_REV_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.value_rev_no.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.total_type == NULL)
   {
      if((la_neg_totals.total_type = (char(*)[NULL_TOTAL_TYPE])
          calloc(ii_max_counter, NULL_TOTAL_TYPE))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.total_type.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.total_id == NULL)
   {
      if((la_neg_totals.total_id = (char(*)[NULL_TOTAL_ID])
          calloc(ii_max_counter, NULL_TOTAL_ID))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.total_id.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.ref_no1 == NULL)
   {
      if((la_neg_totals.ref_no1 = (char(*)[NULL_REF_NO])
          calloc(ii_max_counter, NULL_REF_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.ref_no1.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.ref_no2 == NULL)
   {
      if((la_neg_totals.ref_no2 = (char(*)[NULL_REF_NO])
          calloc(ii_max_counter, NULL_REF_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.ref_no2.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.ref_no3 == NULL)
   {
      if((la_neg_totals.ref_no3 = (char(*)[NULL_REF_NO])
          calloc(ii_max_counter, NULL_REF_NO))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.ref_no3.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.status == NULL)
   {
      if((la_neg_totals.status = (char(*)[NULL_CODE])
          calloc(ii_max_counter, NULL_CODE))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.status.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   if (la_neg_totals.usage_type == NULL)
   {
      if((la_neg_totals.usage_type = (char(*)[NULL_CODE])
          calloc(ii_max_counter, NULL_CODE))==NULL)
      {
         sprintf(err_data, "Unable to allocate memory for la_neg_totals.usage_type.");
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data);
         return(-1);
      }
   }

   /* Begin processing the negative cursor. */
   if (!pi_neg_cursor_open)
   {
      EXEC SQL OPEN c_neg_totals;
      if (SQL_ERROR_FOUND)
      {
         strcpy(err_data, "open c_neg_totals");
         strcpy(table, "v_sa_get_total, sa_total_usage, sa_exported, sa_exported_rev, sa_error, sa_error_impact");
         WRITE_ERROR(SQLCODE, function, table, err_data);
         return(-1);
      }
      pi_neg_cursor_open = TRUE;
   }

   if (pl_neg_records_to_process >= pl_neg_records_remaining)
   {
      if (!pi_neg_cursor_empty)
      {
         EXEC SQL FOR :ii_max_counter
         FETCH c_neg_totals INTO :la_neg_totals.total_seq_no,
                                 :la_neg_totals.value,
                                 :la_neg_totals.value_rev_no,
                                 :la_neg_totals.total_type,
                                 :la_neg_totals.total_id,
                                 :la_neg_totals.ref_no1,
                                 :la_neg_totals.ref_no2,
                                 :la_neg_totals.ref_no3,
                                 :la_neg_totals.status,
                                 :la_neg_totals.usage_type;
         if (SQL_ERROR_FOUND)
         {
            strcpy(err_data, "fetch c_neg_totals");
            strcpy(table, "v_sa_get_total, sa_total_usage, sa_exported, sa_exported_rev, sa_error, sa_error_impact");
            WRITE_ERROR(SQLCODE, function, table, err_data);
            return(-1);
         }

         if (NO_DATA_FOUND)
            pi_neg_cursor_empty = TRUE;

         pl_neg_records_to_process = 0;
         pl_neg_records_remaining = NUM_RECORDS_PROCESSED - pl_neg_total_records;
         pl_neg_total_records = NUM_RECORDS_PROCESSED;

         if (pl_neg_records_remaining == 0)
         {
            pi_neg_cursor_empty = TRUE;
            return(1);
         }
      }
      else
      {
         pi_neg_cursor_empty = TRUE;
         return(1);
      }
   }

   /* load the output variables and return to calling app */
   strcpy(ios_usage_type,  la_neg_totals.usage_type  [pl_neg_records_to_process]);
   strcpy(os_total_seq_no, la_neg_totals.total_seq_no[pl_neg_records_to_process]);
   strcpy(os_total_value,  la_neg_totals.value       [pl_neg_records_to_process]);
   strcpy(os_rev_no,       la_neg_totals.value_rev_no[pl_neg_records_to_process]);
   strcpy(os_total_type,   la_neg_totals.total_type  [pl_neg_records_to_process]);
   strcpy(os_total_id,     la_neg_totals.total_id    [pl_neg_records_to_process]);
   strcpy(os_ref_no1,      la_neg_totals.ref_no1     [pl_neg_records_to_process]);
   strcpy(os_ref_no2,      la_neg_totals.ref_no2     [pl_neg_records_to_process]);
   strcpy(os_ref_no3,      la_neg_totals.ref_no3     [pl_neg_records_to_process]);
   strcpy(os_status,       la_neg_totals.status      [pl_neg_records_to_process]);
   strcpy(os_sign, SAFD_N);

   pl_neg_records_to_process++;
   return(0);
}

static int resetGetTotals()
{
   pi_pos_cursor_open = 0;
   pl_pos_records_remaining = 0;
   pl_pos_total_records = 0;
   pl_pos_records_to_process = 0;
   pi_pos_cursor_empty = 0;

   pi_neg_cursor_open = 0;
   pl_neg_records_remaining = 0;
   pl_neg_total_records = 0;
   pl_neg_records_to_process = 0;
   pi_neg_cursor_empty = 0;

   FREE(la_pos_totals.total_seq_no);
   FREE(la_pos_totals.value);
   FREE(la_pos_totals.value_rev_no);
   FREE(la_pos_totals.total_type);
   FREE(la_pos_totals.total_id);
   FREE(la_pos_totals.ref_no1);
   FREE(la_pos_totals.ref_no2);
   FREE(la_pos_totals.ref_no3);
   FREE(la_pos_totals.status);
   FREE(la_pos_totals.usage_type);

   FREE(la_neg_totals.total_seq_no);
   FREE(la_neg_totals.value);
   FREE(la_neg_totals.value_rev_no);
   FREE(la_neg_totals.total_type);
   FREE(la_neg_totals.total_id);
   FREE(la_neg_totals.ref_no1);
   FREE(la_neg_totals.ref_no2);
   FREE(la_neg_totals.ref_no3);
   FREE(la_neg_totals.status);
   FREE(la_neg_totals.usage_type);

   return(0);
}

int getTotalLike(
   char is_store_day_seq_no[NULL_BIG_SEQ_NO],
   char is_store[NULL_STORE],
   char is_day[NULL_DAY],
   char ios_usage_type[NULL_CODE],
   char os_total_seq_no[NULL_BIG_SEQ_NO],
   char os_total_value[NULL_SIGNED_AMT],
   char os_rev_no[NULL_VALUE_REV_NO],
   char os_total_type[NULL_TOTAL_TYPE],
   char os_total_id[NULL_TOTAL_ID],
   char os_ref_no1[NULL_REF_NO],
   char os_ref_no2[NULL_REF_NO],
   char os_ref_no3[NULL_REF_NO],
   char os_status[NULL_CODE],
   char os_sign[NULL_CODE],
   unsigned int ii_max_counter,
   long il_multiplier)
{
   char* function = "getTotalLike";
   int   li_result = 0;
   static char ls_last_store_day_seq_no[NULL_BIG_SEQ_NO] = "";

   if (strcmp(ls_last_store_day_seq_no, is_store_day_seq_no) != 0)
   {
      strcpy(ls_last_store_day_seq_no, is_store_day_seq_no);
      resetGetTotals();          
   }
 
   li_result = processNegCursor(is_store_day_seq_no, 
                                is_store,
                                is_day,
                                ios_usage_type, 
                                os_total_seq_no,
                                os_total_value, 
                                os_rev_no, 
                                os_total_type, 
                                os_total_id, 
                                os_ref_no1, 
                                os_ref_no2, 
                                os_ref_no3, 
                                os_status, 
                                os_sign, 
                                ii_max_counter,
                                il_multiplier);
   if (li_result == 1)
   {
      li_result = processPosCursor(is_store_day_seq_no, 
                                   is_store,
                                   is_day,
                                   ios_usage_type, 
                                   os_total_seq_no,
                                   os_total_value, 
                                   os_rev_no, 
                                   os_total_type, 
                                   os_total_id, 
                                   os_ref_no1, 
                                   os_ref_no2, 
                                   os_ref_no3, 
                                   os_status, 
                                   os_sign, 
                                   ii_max_counter,
                                   il_multiplier);
      if (li_result == 1)
         resetGetTotals();
   }

   return(li_result);
}
