/* -------------------------------------------------- *\
| fetchStoreDay.pc                                     |
\* -------------------------------------------------- */

#include <retek_2.h>
#include "salib.h"

EXEC SQL INCLUDE SQLCA.H;
long SQLCODE;

/* Description of function                            */
/* -------------------------------------------------- */

int fetchStoreDay(
   char *is_store_no,
   char *is_business_date,
   char *os_store_day_seq_no)
{
   EXEC SQL VAR is_store_no IS STRING (NULL_STORE);
   EXEC SQL VAR is_business_date IS STRING (NULL_DATEONLY);

   char ls_store_day_seq_no[NULL_STORE_DAY_SEQ_NO];
   char *function="fetchStoreDay";

   EXEC SQL DECLARE c_store_day CURSOR FOR
      SELECT store_day_seq_no
      FROM   sa_store_day
      WHERE  store = :is_store_no
      AND    business_date = TO_DATE( :is_business_date, 'YYYYMMDD');

   EXEC SQL OPEN c_store_day;

   if (SQL_ERROR_FOUND)
   {

      sprintf(err_data, "open c_store_day");
      strcpy(table, "sa_store_day");
      WRITE_ERROR(SQLCODE, function, table, err_data);
      return(-1);
   
   }

   EXEC SQL FETCH c_store_day INTO :ls_store_day_seq_no;

   if (SQL_ERROR_FOUND)
   {

      sprintf(err_data, "Fetch c_store_day");
      strcpy(table, "sa_store_day");
      WRITE_ERROR(SQLCODE, function, table, err_data);
      return(-1);

   }

   if (NO_DATA_FOUND)
   {

      return(1);

   }

   strcpy(os_store_day_seq_no, ls_store_day_seq_no);
   return(0);

}

int fetchStoreDayInfo(
   char *is_store_day_seq_no,
   char *os_store_no,
   char *os_business_date,
   char *os_currency_code)
{
   EXEC SQL VAR is_store_day_seq_no IS STRING (NULL_BIG_SEQ_NO);

   char *function="fetchStoreDayInfo";
   char ls_store_no[NULL_STORE];
   char ls_business_date[NULL_DATEONLY];
   char ls_currency_code[NULL_CURRENCY_CODE];

   EXEC SQL VAR ls_currency_code IS STRING (NULL_CURRENCY_CODE);

   EXEC SQL DECLARE c_store_day_info CURSOR FOR
      SELECT sa.store, TO_CHAR(sa.business_date, 'YYYYMMDD'), s.currency_code
      FROM   sa_store_day sa, store s
      WHERE  store_day_seq_no = :is_store_day_seq_no
      AND    sa.store = s.store;

   EXEC SQL OPEN c_store_day_info;

   if (SQL_ERROR_FOUND)
   {

      sprintf(err_data, "open c_store_day_info");
      strcpy(table, "sa_store_day");
      WRITE_ERROR(SQLCODE, function, table, err_data);
      return(-1);
   
   }

   EXEC SQL FETCH c_store_day_info INTO :ls_store_no,
                                        :ls_business_date,
                                        :ls_currency_code;

   if (SQL_ERROR_FOUND)
   {

      sprintf(err_data, "Fetch c_store_day_info");
      strcpy(table, "sa_store_day");
      WRITE_ERROR(SQLCODE, function, table, err_data);
      return(-1);

   }

   if (NO_DATA_FOUND)
   {

      return(1);

   }

   strcpy(os_store_no, ls_store_no);
   strcpy(os_business_date, ls_business_date);
   strcpy(os_currency_code, ls_currency_code);

   return(0);

}
