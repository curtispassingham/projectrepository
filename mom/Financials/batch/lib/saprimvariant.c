
#include "sagetref.h"

static PRIMVARDAT *pa_primvarianttable = NULL; /* memory mapped prim_variant table */
static size_t pl_primvarianttablelen = 0;    /* number of items in prim_variant table */

/* prototypes */
PRIMVARDAT *prim_variant_lookup( char *is_store, char *is_item );
static int primvariantcmp( const void *is_s1, const void *is_s2 );
int primvariant_loadfile( char *is_filename );

/*----------------------------------------------------------
prim_variant_lookup() checks the memory-mapped file for the
location/item and returns a pointer to the matching
data structure, or NULL if not found.
----------------------------------------------------------*/
PRIMVARDAT *prim_variant_lookup( char *is_store, char *is_item )
{
   char ls_key[ LEN_LOC + LEN_ITEM + 1];

   if (pl_primvarianttablelen == 0)
      return(NULL);

   sprintf(ls_key, "%-*.*s%-*.*s",
           LEN_LOC,  LEN_LOC,  is_store,
           LEN_ITEM, LEN_ITEM, is_item);

   return((PRIMVARDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_primvarianttable,
          pl_primvarianttablelen, sizeof(PRIMVARDAT), primvariantcmp));
} /* end prim_variant_lookup */

/*----------------------------------------------------------
primvariantcmp() is a compare routine for bsearch.
----------------------------------------------------------*/
static int primvariantcmp( const void *is_s1, const void *is_s2 )
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_LOC + LEN_ITEM));
}

/*----------------------------------------------------------
primvariant_loadfile() loads only the primary_variant
file into memory.  Returns TRUE if success or FALSE if not.
----------------------------------------------------------*/
int primvariant_loadfile( char *is_filename )
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if ((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if (fstat(li_datafile, &lr_statbuf) == 0)
      {
         if (lr_statbuf.st_size != 0)
         {
            pa_primvarianttable = (PRIMVARDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ, MAP_SHARED, li_datafile, 0);
            if (pa_primvarianttable != (PRIMVARDAT *)MAP_FAILED)
            {
               pl_primvarianttablelen = lr_statbuf.st_size / sizeof(PRIMVARDAT);
               li_rv = TRUE;
            }
         }
         else
            li_rv = TRUE;
      }

      close(li_datafile);
   }

   return(li_rv);
} /* end primvariant_loadfile */
