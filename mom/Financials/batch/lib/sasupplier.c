/*=========================================================================

   sasupplier.c: supplier load and lookup

=========================================================================*/

#include "sagetref.h"

static SUPPLIERDAT *pa_suppliertable = NULL;  /* memory-mapped supplier table */
static size_t pl_suppliertablelen = 0;   /* number of items in suppliertable */

/* prototypes */
SUPPLIERDAT *supplier_lookup(char *is_key);
static int suppliercmp(const void *is_s1, const void *is_s2);
int supplier_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
supplier_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
SUPPLIERDAT *supplier_lookup(char *is_key)
{
   if (pl_suppliertablelen == 0)
   {
      return(NULL);
   }

   return((SUPPLIERDAT *)bsearch(
          (const void *)is_key, (const void *)pa_suppliertable,
           pl_suppliertablelen, sizeof(SUPPLIERDAT), suppliercmp));
} /* end supplier_lookup() */


/*-------------------------------------------------------------------------
suppliercmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int suppliercmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_SUPPLIER));
} /* end suppliercmp() */


/*-------------------------------------------------------------------------
supplier_loadfile() loads the supplier data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int supplier_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_suppliertable = (SUPPLIERDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                               MAP_SHARED, li_datafile, 0 );
            if(pa_suppliertable != (SUPPLIERDAT *)MAP_FAILED)
            {
               pl_suppliertablelen = lr_statbuf.st_size / sizeof(SUPPLIERDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end supplier_loadfile() */


