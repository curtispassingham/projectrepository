
/* This is an automatically generated file. DO NOT EDIT. */
/* To make changes, update the  sa_error_codes table. */
/* Than run: make -f retek.mk saerrcodes.h */

#ifndef _ERROR_CODES_H
#define _ERROR_CODES_H

/* Problem: The Cashier ID is required because of the balancing level. */
/* Solution: Enter the correct Cashier ID. */
#define CASHIER_ID_REQ_BAL_LEVEL "CASHIER_ID_REQ_BAL_LEVEL"

/* Problem: CATT record File Line Identifier - Non-numeric character in numeric field or field is empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define CATT_FIL_STIN            "CATT_FIL_STIN"

/* Problem: Partial Transaction. CATT record in illegal position. */
/* Solution: Input file is corrupt. A CATT record must be after a TCUST record. Either edit the file or repoll the store. */
#define CATT_IN_ILLEGAL_POS      "CATT_IN_ILLEGAL_POS"

/* Problem: The Credit Card Number (ID Number) is required. */
/* Solution: Enter the correct credit card number. */
#define CC_NO_REQ                "CC_NO_REQ"

/* Problem: Close transaction should not have a Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define CLOSE_NO_CATT            "CLOSE_NO_CATT"

/* Problem: Close Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define CLOSE_NO_CUST            "CLOSE_NO_CUST"

/* Problem: Close transaction should not have a Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define CLOSE_NO_DISC            "CLOSE_NO_DISC"

/* Problem: Close Transactions should not have Item records.  */
/* Solution: Delete the Item record from this transaction. */
#define CLOSE_NO_ITEM            "CLOSE_NO_ITEM"

/* Problem: Close Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define CLOSE_NO_TAX             "CLOSE_NO_TAX"

/* Problem: Close transactions should not have Tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define CLOSE_NO_TEND            "CLOSE_NO_TEND"

/* Problem: Store Conditions transaction should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define COND_NO_CATT             "COND_NO_CATT"

/* Problem: Store Conditions transactions should not have a Customer record. */
/* Solution: Delete the Customer record from this transaction. */
#define COND_NO_CUST             "COND_NO_CUST"

/* Problem: Store Conditions transaction should not have a Discount record. */
/* Solution: Delete the Discount record from this transaction. */
#define COND_NO_DISC             "COND_NO_DISC"

/* Problem: Store Conditions transactions should not have Item records.  */
/* Solution: Delete the Item record from this transaction. */
#define COND_NO_ITEM             "COND_NO_ITEM"

/* Problem: Store Conditions transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define COND_NO_TAX              "COND_NO_TAX"

/* Problem: Store Conditions transactions should not have tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define COND_NO_TEND             "COND_NO_TEND"

/* Problem: A Customer Identification Number required. */
/* Solution: Enter the correct Customer Identification Number. */
#define CUST_ID_REQ              "CUST_ID_REQ"

/* Problem: Data is not expected for this store/day. The store/day does not exist. */
/* Solution: Invalid location (store) or business date. Check the Store, Company Close, Company Close Exception and Store Close forms. */
#define DATAUNEXPECTEDSTOREDAY   "DATAUNEXPECTEDSTOREDAY"

/* Problem: Day Close transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define DCLOSE_NO_CATT           "DCLOSE_NO_CATT"

/* Problem: Day Close transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define DCLOSE_NO_CUST           "DCLOSE_NO_CUST"

/* Problem: Day Close transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define DCLOSE_NO_DISC           "DCLOSE_NO_DISC"

/* Problem: Day Close transactions should not have Item records. */
/* Solution: Delete the Item record from this transaction. */
#define DCLOSE_NO_ITEM           "DCLOSE_NO_ITEM"

/* Problem: Day Close transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define DCLOSE_NO_TAX            "DCLOSE_NO_TAX"

/* Problem: Day Close transactions should not have Tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define DCLOSE_NO_TEND           "DCLOSE_NO_TEND"

/* Problem: Duplicate Transaction Number. The transaction has been sent to the reject file. */
/* Solution: Input file is corrupt. Either edit the file or repoll the store. */
#define DUP_TRAN                 "DUP_TRAN"

/* Problem: Exchange transactions are required to have at least two Item records. */
/* Solution: Add one or more Item records to this transaction. */
#define EEXCH_ITEM_REQ           "EEXCH_ITEM_REQ"

/* Problem: FHEAD File Line Identifier- Non-numeric character in numeric field or field is empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define FHEAD_FIL_STIN           "FHEAD_FIL_STIN"

/* Problem: FHEAD is not the first record in the file. */
/* Solution: Input file is corrupt. Either edit the file or repoll the store. */
#define FHEAD_NOT_FIRST          "FHEAD_NOT_FIRST"

/* Problem: Error has occured during the last file operation */
/* Solution:  */
#define FILE_ERROR               "FILE_ERROR"

/* Problem: FTAIL File Line Identifier - Non-numeric character in numeric field or field is empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define FTAIL_FIL_STIN           "FTAIL_FIL_STIN"

/* Problem: FTAIL Record Counter - Non-numeric character in numeric field or field is empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define FTAIL_FRC_STIN           "FTAIL_FRC_STIN"

/* Problem: Partial Transaction. There is a FTAIL record in a transaction. */
/* Solution: The input file is corrupt. Either edit the file or repoll the store. */
#define FTAIL_IN_TRAN            "FTAIL_IN_TRAN"

/* Problem: FTAIL is not the last record in the file. */
/* Solution: Input file does not have an FTAIL record.  Insert an FTAIL record into input file. */
#define FTAIL_NOT_LAST           "FTAIL_NOT_LAST"

/* Problem: Embedded garbage in record. */
/* Solution: Input file is corrupt. There is an embedded NUL, TAB, CR or LF character in the record. Either edit the file or repoll the store. */
#define GARBAGE_IN_RECORD        "GARBAGE_IN_RECORD"

/* Problem: Coupon Number cannot be empty when Discount Type is Store Coupon. */
/* Solution: Enter a Coupon Number. */
#define IDISC_COUPON_NO_REQ      "IDISC_COUPON_NO_REQ"

/* Problem: Invalid Discount Promotion Number. */
/* Solution: Enter a valid Discount Promotion Number. */
#define IDISC_DRN_STIN           "IDISC_DRN_STIN"

/* Problem: IDISC File Line Identifier Non-numeric character in numeric field or field is empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define IDISC_FIL_STIN           "IDISC_FIL_STIN"

/* Problem: Partial Transaction. IDISC record in illegal position. */
/* Solution: Input file is corrupt. An IDISC record must be after a TITEM record. Either edit the file or repoll the store. */
#define IDISC_IN_ILLEGAL_POS     "IDISC_IN_ILLEGAL_POS"

/* Problem: Invalid Discount Promotion Component */
/* Solution: Enter a valid discount promotion component */
#define IDISC_PRC_STIN           "IDISC_PRC_STIN"

/* Problem: Discount Quantity - Non-numeric character in numeric field or field is empty. */
/* Solution: Enter a valid Quantity. */
#define IDISC_QTY_STIN           "IDISC_QTY_STIN"

/* Problem: Discount Catch Weight Indicator - The Catch Weight Indicator was not correctly imported */
/* Solution: Enter the correct Catch Weight Indicator */
#define IDISC_SET_CW             "IDISC_SET_CW"

/* Problem: Discount Amount - Non-numeric character in numeric field or field is empty. */
/* Solution: Enter a valid Discount Amount. */
#define IDISC_UDA_STIN           "IDISC_UDA_STIN"

/* Problem: Discount UOM Quantity - Non-numeric character in numeric field */
/* Solution: Enter the correct UOM Quantity */
#define IDISC_UOM_QTY_STIN       "IDISC_UOM_QTY_STIN"

/* Problem: Invalid banner ID */
/* Solution: Choose a Banner ID from the list on the Customer Attributes screen */
#define INVLD_BANNER_ID          "INVLD_BANNER_ID"

/* Problem: Invalid Birthdate. */
/* Solution: Correct the Birthdate. The format expected is YYYYMMDD. */
#define INVLD_BIRTHDATE          "INVLD_BIRTHDATE"

/* Problem: Invalid Business Date or date is empty. */
/* Solution: Correct the Business Date. The format expected is YYYYMMDD. */
#define INVLD_BUSINESS_DATE      "INVLD_BUSINESS_DATE"

/* Problem: Invalid checking account number card mask value. */
/* Solution: Enter the correct checking account number mask value. */
#define INVLD_CAN_MASK             "INVLD_CAN_MASK"

/* Problem: The Cashier POS ID does not have a corresponding Employee ID. */
/* Solution: Enter the Employee ID and POS ID in the Employee Maintenance Form. */
#define INVLD_CASHIER_ID         "INVLD_CASHIER_ID"

/* Problem: Invalid Credit Card Authorization Source. */
/* Solution: Choose a Credit Card Authorization Source from the list. */
#define INVLD_CCAS               "INVLD_CCAS"

/* Problem: Invalid Credit Card Entry Mode. */
/* Solution: Choose a Credit Card Entry Mode from the list. */
#define INVLD_CCEM               "INVLD_CCEM"

/* Problem: Invalid Credit Card Special Condition. */
/* Solution: Choose a Credit Card Special Condition from the list. */
#define INVLD_CCSC               "INVLD_CCSC"

/* Problem: Invalid Credit Card Holder Verification. */
/* Solution: Choose a Credit Card Holder Verification from the list. */
#define INVLD_CCVF               "INVLD_CCVF"

/* Problem: Invalid Credit Card Number check digit. This card number is invalid. */
/* Solution: Enter the correct Credit Card Number. */
#define INVLD_CC_CHECKSUM        "INVLD_CC_CHECKSUM"

/* Problem: This credit card is expired. */
/* Solution: Correct the expiration date or if the card is expired, than educate the cashier about checking the date. */
#define INVLD_CC_EXPIRED         "INVLD_CC_EXPIRED"

/* Problem: Invalid Credit Card Expiration Date. */
/* Solution: Correct the Expiration Date. The format expected is YYYYMMDD. */
#define INVLD_CC_EXP_DATE        "INVLD_CC_EXP_DATE"

/* Problem: Invalid credit card mask value. */
/* Solution: Enter the correct credit card mask value. */
#define INVLD_CC_MASK             "INVLD_CC_MASK"

/* Problem: Invalid credit card length */
/* Solution: Enter the correct credit card number. */
#define INVLD_CC_LEN             "INVLD_CC_LEN"

/* Problem: The numeric prefix of this Credit Card is not within the specified range. */
/* Solution: Enter the correct Credit Card number. */
#define INVLD_CC_PREFIX          "INVLD_CC_PREFIX"

/* Problem: Invalid Customer Identification Type. */
/* Solution: Choose a Customer Identification Type from the list. */
#define INVLD_CIDT               "INVLD_CIDT"

/* Problem: Invalid Customer Order Date */
/* Solution: Correct the Customer Order Date via the Customer Attributes screen. The format expected is YYYYMMDD */
#define INVLD_CUST_ORD_DATE      "INVLD_CUST_ORD_DATE"

/* Problem: The discount unit of measure is not valid for the item. */
/* Solution: Select a different discount unit of measure that is compatible with the standard unit of measure, or define the conversion between the current discount unit of measure and the standard unit of measure. */
#define INVLD_DISCOUNT_UOM       "INVLD_DISCOUNT_UOM"

/* Problem: A Gift Certificate/Voucher Number is required. */
/* Solution: Enter the Gift Certificate/Vouch Number. */
#define INVLD_DOCUMENT           "INVLD_DOCUMENT"

/* Problem: Invalid Expiration Date for a Voucher. */
/* Solution: Enter a valid Expiration Date. The format expected is YYYMMDD. */
#define INVLD_EXP_DATE           "INVLD_EXP_DATE"

/* Problem: FTAIL record is in the incorrect position. */
/* Solution: The input file is corrupt. Either edit the file or repoll the store. */
#define INVLD_FTAIL_POS          "INVLD_FTAIL_POS"

/* Problem: FTAIL Transaction Number Count is incorrect. */
/* Solution: The input file is corrupt. Either edit the file or repoll the store. */
#define INVLD_FTAIL_TRAN_CNT     "INVLD_FTAIL_TRAN_CNT"

/* Problem: Item Number is empty. */
/* Solution: Choose an Item Number from the list. */
#define INVLD_ITEM_NO            "INVLD_ITEM_NO"

/* Problem: Invalid Item Swiped Indicator. The value has been defaulted to 'Y'. */
/* Solution: Click the Item Swiped check box to set the correct value. */
#define INVLD_ITEM_SWIPED_IND    "INVLD_ITEM_SWIPED_IND"

/* Problem: Reason Code for Expense Vendor Paid Out or Merchandise Vendor Paid Out must come from non_merch_code_head table. */
/* Solution: Use the correct Reason Code. */
#define INVLD_NON_MERCH_CODE     "INVLD_NON_MERCH_CODE"

/* Problem: Invalid price Override Reason. */
/* Solution: Choose a price Override Reason from the list. */
#define INVLD_ORRC               "INVLD_ORRC"

/* Problem: Invalid RMS Promotion Type. */
/* Solution: Choose a RMS Promotion Type from the list. */
#define INVLD_PRMT               "INVLD_PRMT"

/* Problem: Invalid Reason Code. */
/* Solution: Choose a Reason Code from the list. */
#define INVLD_REAC               "INVLD_REAC"

/* Problem: Invalid Customer Attribute Type. */
/* Solution: Choose a Customer Attribute Type from the list. */
#define INVLD_SACA               "INVLD_SACA"

/* Problem: Invalid Discount Type. */
/* Solution: Choose a Discount Type from the list. */
#define INVLD_SADT               "INVLD_SADT"

/* Problem: Invalid Item Type. */
/* Solution: Choose an Item Type from the list. */
#define INVLD_SAIT               "INVLD_SAIT"

/* Problem: The Salesperson POS ID does not have a corresponding Employee ID. */
/* Solution: Enter the Employee ID and POS ID in the Employee Maintenance Form or correct the POS ID on the Employees Tab. */
#define INVLD_SALESPERSON_ID     "INVLD_SALESPERSON_ID"

/* Problem: Invalid Return Reason. */
/* Solution: Choose a Return Reason from the list. */
#define INVLD_SARR               "INVLD_SARR"

/* Problem: Invalid Item Status. */
/* Solution: Choose an Item Status from the list. */
#define INVLD_SASI               "INVLD_SASI"

/* Problem: The selling unit of measure is not valid for the item. */
/* Solution: Select a different selling unit of measure that is compatible with the standard unit of measure, or define the conversion between the current selling unit of measure and the standard unit of measure. */
#define INVLD_SELLING_UOM        "INVLD_SELLING_UOM"

/* Problem: Invalid Customer Attribute Value. */
/* Solution: Choose a Customer Attribute Value from the list. */
#define INVLD_SUB_SACA           "INVLD_SUB_SACA"

/* Problem: Invalid System Indicator. */
/* Solution: Choose a System Indicator from the list. */
#define INVLD_SYSTEM_IND         "INVLD_SYSTEM_IND"

/* Problem: Invalid Tax Type. */
/* Solution: Choose a Tax Type from the list. */
#define INVLD_TAXC               "INVLD_TAXC"

/* Problem: Invalid Tax Indicator. The value has been defaulted to 'Y'. */
/* Solution: Click the Tax Indicator check box to set the correct value. */
#define INVLD_TAX_IND            "INVLD_TAX_IND"

/* Problem: Invalid Tender Type Group. */
/* Solution: Choose a Tender Type Group from the list. */
#define INVLD_TENT               "INVLD_TENT"

/* Problem: Invalid Transaction Date/Time. */
/* Solution: Enter a valid Transaction Date/Time. */
#define INVLD_TRAN_DATETIME      "INVLD_TRAN_DATETIME"

/* Problem: Invalid Sub-Transaction Type. */
/* Solution: Choose a Sub-Transaction Type from the list. */
#define INVLD_TRAS               "INVLD_TRAS"

/* Problem: Invalid Transaction Type. */
/* Solution: Choose a Transaction Type from the list. */
#define INVLD_TRAT               "INVLD_TRAT"

/* Problem: Invalid Merchandise Vendor Number. */
/* Solution: Use a vendor from the supplier table for Merchandise Vendors. */
#define INVLD_VENDOR_NO_MV          "INVLD_VENDOR_NO_MV"

/* Problem: Invalid Expense Vendor Number. */
/* Solution: Use a vendor from the partner table for Expense Vendors. */
#define INVLD_VENDOR_NO_EV          "INVLD_VENDOR_NO_EV"

/* Problem: This rule that was defined in the Rule Definition Wizard did not calculate successfully for the store/day. */
/* Solution: Review the rule definition in the Rule Definition Wizard. */
#define INV_RULE_DEF             "INV_RULE_DEF"

/* Problem: This total that was defined in the Totals Calculation Wizard did not calculate successfully for the store/day. */
/* Solution: Review the total definition in the Totals Calculation Wizard. */
#define INV_TOTAL_DEF            "INV_TOTAL_DEF"

/* Problem: Item Status cannot be empty. */
/* Solution: Choose an Item Status from the list. */
#define ITEM_STATUS_REQ          "ITEM_STATUS_REQ"

/* Problem: Loan transactions should not have a Customer Attribute record. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define LOAN_NO_CATT             "LOAN_NO_CATT"

/* Problem: Loan transactions should not have customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define LOAN_NO_CUST             "LOAN_NO_CUST"

/* Problem: Loan transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define LOAN_NO_DISC             "LOAN_NO_DISC"

/* Problem: Loan Transactions should not have a Item record.  */
/* Solution: Delete the Item record from this transaction. */
#define LOAN_NO_ITEM             "LOAN_NO_ITEM"

/* Problem: Loan transactions should not have a Tax record. */
/* Solution: Delete the Tax record from this transaction. */
#define LOAN_NO_TAX              "LOAN_NO_TAX"

/* Problem: Loan Transactions are required to have at least one Tender record. */
/* Solution: Add a Tender Record to this transaction. */
#define LOAN_TENDER_REQ          "LOAN_TENDER_REQ"

/* Problem: Meter Transactions are required to have at least one Item record. */
/* Solution: Add an Item Record to this transaction. */
#define METER_ITEM_REQ           "METER_ITEM_REQ"

/* Problem: Meter transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define METER_NO_CATT            "METER_NO_CATT"

/* Problem: Meter Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define METER_NO_CUST            "METER_NO_CUST"

/* Problem: Meter Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define METER_NO_TAX             "METER_NO_TAX"

/* Problem: Meter Transactions should not have Tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define METER_NO_TEND            "METER_NO_TEND"

/* Problem: Missing THEAD record. */
/* Solution: Input file is corrupt. Every transaction must have a THEAD and TTAIL record. Either edit the file or repoll the store. */
#define MISSING_THEAD            "MISSING_THEAD"

/* Problem: A large number of transactions are missing. */
/* Solution: The input file may be corrupt. Try repolling the store. */
#define MISSING_TRAN_BIG_GAP     "MISSING_TRAN_BIG_GAP"

/* Problem: Missing TTAIL record. */
/* Solution: Input file is corrupt. Every transaction must have a THEAD and TTAIL record. Either edit the file or repoll the store. */
#define MISSING_TTAIL            "MISSING_TTAIL"

/* Problem: A non-merchandise item number is required for this item record. */
/* Solution: Specify the correct item number that is involved in the transaction. */
#define NON_MERCH_ITEM_NO_REQ    "NON_MERCH_ITEM_NO_REQ"

/* Problem: No Sale transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define NOSALE_NO_CATT           "NOSALE_NO_CATT"

/* Problem: No sale Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define NOSALE_NO_CUST           "NOSALE_NO_CUST"

/* Problem: No sale transaction should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define NOSALE_NO_DISC           "NOSALE_NO_DISC"

/* Problem: No sale Transactions should not have Item records. */
/* Solution: Delete the Item record from this transaction. */
#define NOSALE_NO_ITEM           "NOSALE_NO_ITEM"

/* Problem: No sale Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define NOSALE_NO_TAX            "NOSALE_NO_TAX"

/* Problem: No sale Transactions should not have Tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define NOSALE_NO_TEND           "NOSALE_NO_TEND"

/* Problem: Open transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define OPEN_NO_CATT             "OPEN_NO_CATT"

/* Problem: Open Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define OPEN_NO_CUST             "OPEN_NO_CUST"

/* Problem: Open transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define OPEN_NO_DISC             "OPEN_NO_DISC"

/* Problem: Open Transactions should not have Item records. */
/* Solution: Delete the Item record from this transaction. */
#define OPEN_NO_ITEM             "OPEN_NO_ITEM"

/* Problem: Open Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define OPEN_NO_TAX              "OPEN_NO_TAX"

/* Problem: Open Transactions should not have Tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define OPEN_NO_TEND             "OPEN_NO_TEND"

/* Problem: Paid In transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define PAIDIN_NO_CATT           "PAIDIN_NO_CATT"

/* Problem: Paid In Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define PAIDIN_NO_CUST           "PAIDIN_NO_CUST"

/* Problem: Paid In transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define PAIDIN_NO_DISC           "PAIDIN_NO_DISC"

/* Problem: Paid In Transactions should not have Item records.  */
/* Solution: Delete the Item record from this transaction. */
#define PAIDIN_NO_ITEM           "PAIDIN_NO_ITEM"

/* Problem: Paid In Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define PAIDIN_NO_TAX            "PAIDIN_NO_TAX"

/* Problem: Paid In Transactions are required to have a Reason Code. */
/* Solution: Choose a Reason Code from the list. */
#define PAIDIN_REASON_CODE_REQ   "PAIDIN_REASON_CODE_REQ"

/* Problem: Paid In Transactions are required to have at least one Tender record. */
/* Solution: Add a Tender record to this transaction. */
#define PAIDIN_TENDER_REQ        "PAIDIN_TENDER_REQ"

/* Problem: Paid Out transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define PAIDOU_NO_CATT           "PAIDOU_NO_CATT"

/* Problem: Paid Out Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define PAIDOU_NO_CUST           "PAIDOU_NO_CUST"

/* Problem: Paid Out transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define PAIDOU_NO_DISC           "PAIDOU_NO_DISC"

/* Problem: Paid Out Transactions should not have Item records. */
/* Solution: Delete the Item record from this transaction. */
#define PAIDOU_NO_ITEM           "PAIDOU_NO_ITEM"

/* Problem: Paid Out transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define PAIDOU_NO_TAX            "PAIDOU_NO_TAX"

/* Problem: Paid Out Transactions are required to have a Reason Code. */
/* Solution: Choose a Reason Code from the list. */
#define PAIDOU_REASON_CODE_REQ   "PAIDOU_REASON_CODE_REQ"

/* Problem: Paid Out Transactions are required to have at least one tender record. */
/* Solution: Add a Tender record to this transaction. */
#define PAIDOU_TENDER_REQ        "PAIDOU_TENDER_REQ"

/* Problem: POS data is too old to be loaded into the system. */
/* Solution: Check the Days Post Sale on the Sales Audit System Options form. This field contains the number of days back from today that POS data can be loaded into the system. */
#define POSDATATOOOLD            "POSDATATOOOLD"

/* Problem: POS data is for a business date greater than the current date. */
/* Solution: It is not possible to import data for a business date in the future. */
#define POSFUTUREDATA            "POSFUTUREDATA"

/* Problem: POS data is not expected for this store/day. This store is not expected to be open. */
/* Solution: The location (store) and business date are correct. However, POS data for this store/day are not expected. Check the Company Close, Company Close Exception and Store Close forms. */
#define POSUNEXPECTEDSTOREDAY    "POSUNEXPECTEDSTOREDAY"

/* Problem: Pull transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define PULL_NO_CATT             "PULL_NO_CATT"

/* Problem: Pull Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define PULL_NO_CUST             "PULL_NO_CUST"

/* Problem: Pull transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define PULL_NO_DISC             "PULL_NO_DISC"

/* Problem: Pull Transactions should not have Item records.  */
/* Solution: Delete the Item record from this transaction. */
#define PULL_NO_ITEM             "PULL_NO_ITEM"

/* Problem: Pull Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define PULL_NO_TAX              "PULL_NO_TAX"

/* Problem: Pull Transactions are required to have at least one Tender record. */
/* Solution: Add a Tender record to this transaction. */
#define PULL_TENDER_REQ          "PULL_TENDER_REQ"

/* Problem: Pump Test transactions are required to have at least one Item record.  */
/* Solution: Add an Item Record to this transaction. */
#define PUMPT_ITEM_REQ           "PUMPT_ITEM_REQ"

/* Problem: Pump Test transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define PUMPT_NO_CATT            "PUMPT_NO_CATT"

/* Problem: Pump Test transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define PUMPT_NO_CUST            "PUMPT_NO_CUST"

/* Problem: Pump Test transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define PUMPT_NO_DISC            "PUMPT_NO_DISC"

/* Problem: Pump Test transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define PUMPT_NO_TAX             "PUMPT_NO_TAX"

/* Problem: Pump Test transactions should not have tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define PUMPT_NO_TEND            "PUMPT_NO_TEND"

/* Problem: Post Void transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define PVOID_NO_CATT            "PVOID_NO_CATT"

/* Problem: Post Void Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define PVOID_NO_CUST            "PVOID_NO_CUST"

/* Problem: Post Void transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define PVOID_NO_DISC            "PVOID_NO_DISC"

/* Problem: Post Void Transactions should not have Item records. */
/* Solution: Delete the Item record from this transaction. */
#define PVOID_NO_ITEM            "PVOID_NO_ITEM"

/* Problem: Post Void Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define PVOID_NO_TAX             "PVOID_NO_TAX"

/* Problem: Post Void Transactions should not have Tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define PVOID_NO_TEND            "PVOID_NO_TEND"

/* Problem: Records have been rejected during the loading of this store/day. */
/* Solution: This input file is corrupt and can not be fully loaded. Either edit the file or repoll the store. */
#define RECORDS_REJECTED         "RECORDS_REJECTED"

/* Problem: Refund transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define REFUND_NO_DISC           "REFUND_NO_DISC"

/* Problem: Refund transactions should not have Item records. */
/* Solution: Delete the Item record from this transaction. */
#define REFUND_NO_ITEM           "REFUND_NO_ITEM"

/* Problem: Refund Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define REFUND_NO_TAX            "REFUND_NO_TAX"

/* Problem: Refund Transactions are required to have at least one tender record. */
/* Solution: Add a Tender Record to this transaction. */
#define REFUND_TENDER_REQ        "REFUND_TENDER_REQ"

/* Problem: The Register ID is required because transactions are generated at the register level. */
/* Solution: Enter the correct Register ID. */
#define REGISTER_ID_REQ          "REGISTER_ID_REQ"

/* Problem: The Register ID is required because of the balancing level. */
/* Solution: Enter the correct Register ID. */
#define REGISTER_ID_REQ_BAL_LEVEL"REGISTER_ID_REQ_BAL_LEVEL"

/* Problem: Return Transactions are required to have at least one Item record. */
/* Solution: Add an Item record to this transaction. */
#define RETURN_ITEM_REQ          "RETURN_ITEM_REQ"

/* Problem: Return Transactions should have at least one Tender record. */
/* Solution: Add a Tender Record to this transaction. */
#define RETURN_TENDER_REQ        "RETURN_TENDER_REQ"

/* Problem: Sale Transactions are required to have at least one Item record. */
/* Solution: Add an Item Record to this transaction. */
#define SALE_ITEM_REQ            "SALE_ITEM_REQ"

/* Problem: Sale Transactions are required to have at least one Tender record. */
/* Solution: Add a Tender Record to this transaction. */
#define SALE_TENDER_REQ          "SALE_TENDER_REQ"

/* Problem: The drop shipment indicator was not correctly imported for this item. */
/* Solution: In the item detail section, verify the drop shipment indicator. */
#define SET_DROP_SHIP            "SET_DROP_SHIP"

/* Problem: This SKU does not exist in the RMS. */
/* Solution: Choose a SKU from the list. */
#define SKU_NOT_FOUND            "SKU_NOT_FOUND"

/* Problem: This store/day is not in the status of Ready to Load. */
/* Solution: The data may already be loaded or trickle polled data may have been loaded out of order. */
#define STOREDAYNOTREADYTOBELOAD "STOREDAYNOTREADYTOBELOAD"

/* Problem: Can not get a write lock for a store/day. Either a locking record has not been loaded or the store/day is locked by another process. */
/* Solution: Try again after the lock has been released. */
#define STOREDAY_LOCK_FAILED     "STOREDAY_LOCK_FAILED"

/* Problem: Tank Dip transactions are required to have at least one Item record. */
/* Solution: Add an Item Record to this transaction. */
#define TANKDP_ITEM_REQ          "TANKDP_ITEM_REQ"

/* Problem: Tank Dip transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define TANKDP_NO_CATT           "TANKDP_NO_CATT"

/* Problem: Tank Dip Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define TANKDP_NO_CUST           "TANKDP_NO_CUST"

/* Problem: Tank Dip Transactions should not have Discount records. */
/* Solution: Delete the Discount record from this transaction. */
#define TANKDP_NO_DISC           "TANKDP_NO_DISC"

/* Problem: Tank Dip Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define TANKDP_NO_TAX            "TANKDP_NO_TAX"

/* Problem: Tank Dip Transactions should not have Tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define TANKDP_NO_TEND           "TANKDP_NO_TEND"

/* Problem: TCUST File Line Identifier - Non-numeric character in numeric field or the field my be empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define TCUST_FIL_STIN           "TCUST_FIL_STIN"

/* Problem: Partial Transaction. TCUST in illegal position. */
/* Solution: Input file is corrupt. A TCUST record must be between a THEAD and a TTAIL record. Either edit the file or repoll the store. */
#define TCUST_IN_ILLEGAL_POS     "TCUST_IN_ILLEGAL_POS"

/* Problem: This error is used to check that all data is loaded. */
/* Solution: Load all data and run saimptlogfin. */
#define TERM_MARKER_NO_ERROR     "TERM_MARKER_NO_ERROR"

/* Problem: THEAD File Line Identifier - Non-numeric character in numeric field or the field may be empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define THEAD_FIL_STIN           "THEAD_FIL_STIN"

/* Problem: Partial Transaction. THEAD in illegal position. */
/* Solution: The input file is corrupt. Either edit the file or repoll the store. */
#define THEAD_IN_ILLEGAL_POS     "THEAD_IN_ILLEGAL_POS"

/* Problem: Partial Transaction. THEAD in transaction. */
/* Solution: The input file is corrupt. Either edit the file or repoll the store. */
#define THEAD_IN_TRAN            "THEAD_IN_TRAN"

/* Problem: Location had no Banner ID */
/* Solution: The store has no associated banner ID */
#define THEAD_LOC_NO_BANN        "THEAD_LOC_NO_BANN"

/* Problem: Original POS Tran No - Non-numeric character in numeric field or the field may be empty. */
/* Solution: This value was set to -1 to facilitate data loading. Enter the original POS Tran No that is being Post Voided. */
#define THEAD_OTN_STIN           "THEAD_OTN_STIN"

/* Problem: THEAD Transaction Number - Non-numeric character in numeric field or the field may be empty. */
/* Solution: This value was set to -1 to facilitate data loading. Enter the POS Tran No for this transaction. */
#define THEAD_TRN_STIN           "THEAD_TRN_STIN"

/* Problem: Value - Non-numeric character in numeric field. */
/* Solution: Enter the correct value. */
#define THEAD_VAL_STIN           "THEAD_VAL_STIN"

/* Problem: SKU Class - Non-numeric character in numeric field. */
/* Solution: Enter the correct SKU Class. */
#define TITEM_CLS_STIN           "TITEM_CLS_STIN"

/* Problem: Customer Order Line Number - Non-numeric character in numeric field */
/* Solution: Enter the correct Customer Order Line Number via the Customer Attributes screen */
#define TITEM_CUST_ORD_STIN      "TITEM_CUST_ORD_STIN"

/* Problem: SKU Department - Non-numeric character in numeric field. */
/* Solution: Enter the correct SKU Department. */
#define TITEM_DEP_STIN           "TITEM_DEP_STIN"

/* Problem: TITEM File Line Identifier - Non-numeric character in numeric field or the field is empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define TITEM_FIL_STIN           "TITEM_FIL_STIN"

/* Problem: Item Catch Weight Indicator - The Catch Weight Indicator is not set for a weight item. */
/* Solution: In the item detail section, verify the catch weight indicator */
#define TITEM_INVALID_CW         "TITEM_INVALID_CW"

/* Problem: Partial Transaction. TITEM in illegal position. */
/* Solution: Input file is corrupt. A TITEM record must be between a THEAD and a TTAIL record. Either edit the file or repoll the store. */
#define TITEM_IN_ILLEGAL_POS     "TITEM_IN_ILLEGAL_POS"

/* Problem: Media ID - Non-numeric character in numeric field */
/* Solution: Enter the correct Media ID via the Customer Attributes screen */
#define TITEM_MID_STIN           "TITEM_MID_STIN"

/* Problem: Original Unit Retail - Non-numeric character in numeric field. */
/* Solution: Enter the correct Original Until Retail. */
#define TITEM_OUR_STIN           "TITEM_OUR_STIN"

/* Problem: Item records with an Item Status of Sale should have a positive Quantity, those with Item Status of Return should have a negative Quantity. If the Item Status is Void, the sign should be opposite that of the voided item. */
/* Solution: Modify the sign of the Item Quantity. */
#define TITEM_QTY_SIGN           "TITEM_QTY_SIGN"

/* Problem: Item Quantity - Non-numeric character in numeric field or the field is empty. */
/* Solution: Enter the correct Item Quantity. Item records with an Item Status of Sale should have a positive Quantity, those with Item Status of Return should have a negative Quantity. If the Item Status is Void, the sign should be opposite that of the voided item. */
#define TITEM_QTY_STIN           "TITEM_QTY_STIN"

/* Problem: SKU Subclass - Non-numeric character in numeric field. */
/* Solution: Enter the correct SKU Subclass. */
#define TITEM_SBC_STIN           "TITEM_SBC_STIN"

/* Problem: Item Catch Weight Indicator - The Catch Weight Indicator was not correctly imported for this item */
/* Solution: In the item detail section, verify the catch weight indicator */
#define TITEM_SET_CW             "TITEM_SET_CW"

/* Problem: UPC Supplement - Non-numeric character in numeric field. */
/* Solution: Enter the correct UPC Supplement.. */
#define TITEM_SUP_STIN           "TITEM_SUP_STIN"

/* Problem: Item UOM Quantity - Non-numeric character in numeric field */
/* Solution: Enter the correct UOM Quantity */
#define TITEM_UOM_QTY_STIN       "TITEM_UOM_QTY_STIN"

/* Problem: Unit Retail - Non-numeric character in numeric field. */
/* Solution: Enter the correct Unit Retail. */
#define TITEM_URT_STIN           "TITEM_URT_STIN"

/* Problem: Additional FHEAD record encountered. */
/* Solution: Input file is corrupt. Either edit the file or repoll the store. */
#define TOO_MANY_FHEADS          "TOO_MANY_FHEADS"

/* Problem: Total transactions should not have Customer Attribute records. */
/* Solution: Delete the Customer Attribute record from this transaction. */
#define TOTAL_NO_CATT            "TOTAL_NO_CATT"

/* Problem: Total Transactions should not have Customer records. */
/* Solution: Delete the Customer record from this transaction. */
#define TOTAL_NO_CUST            "TOTAL_NO_CUST"

/* Problem: Total transactions should not have a Discount records. */
/* Solution: Delete the Total record from this transaction. */
#define TOTAL_NO_DISC            "TOTAL_NO_DISC"

/* Problem: Total Transactions should not have Item records.  */
/* Solution: Delete the Item record from this transaction. */
#define TOTAL_NO_ITEM            "TOTAL_NO_ITEM"

/* Problem: Total Transactions should not have Tax records. */
/* Solution: Delete the Tax record from this transaction. */
#define TOTAL_NO_TAX             "TOTAL_NO_TAX"

/* Problem: Total transactions should not have an Tender records. */
/* Solution: Delete the Tender record from this transaction. */
#define TOTAL_NO_TEND            "TOTAL_NO_TEND"

/* Problem: Total Transactions are required to have the Total ID in the Reference Number 1 field. */
/* Solution: Enter the Total ID in the Reference Number 1 field. */
#define TOTAL_REF_NO_1_REQ       "TOTAL_REF_NO_1_REQ"

/* Problem: Total Transactions are required to have a Total Value. */
/* Solution: Enter the Total Value. */
#define TOTAL_VALUE_REQ          "TOTAL_VALUE_REQ"

/* Problem: Transaction is missing subrecords. */
/* Solution: The input file is corrupt. Either edit the file or repoll the store. */
#define TRAN_MISS_REC            "TRAN_MISS_REC"

/* Problem: Transaction Type is not recognized. */
/* Solution: Input file is corrupt. Check the TLOG to RTLOG conversion process or repoll the store. */
#define TRAN_NOT_RECOGNIZED      "TRAN_NOT_RECOGNIZED"

/* Problem: The Transaction Number is required. */
/* Solution: Enter the correct Transaction Number. */
#define TRAN_NO_REQ              "TRAN_NO_REQ"

/* Problem: Transaction is out of balance. The total of the items does not equal the total of the tenders. */
/* Solution: Adjust the Items or Tender as required. */
#define TRAN_OUT_BAL             "TRAN_OUT_BAL"

/* Problem: A transaction may only have one Customer record. */
/* Solution: Delete the extra Customer record from this transaction. */
#define TRAN_XTRA_CUST           "TRAN_XTRA_CUST"

/* Problem: Unknown record type found. */
/* Solution: Input file is corrupt. The valid record types are FHEAD, THEAD, TCUST, CATT, TITEM, IDISC, TTEND, TTAX, TTAIL and FTAIL. Either edit the file or repoll the store. */
#define TRAN_XTRA_REC            "TRAN_XTRA_REC"

/* Problem: TTAIL File Line Identifier - Non-numeric character in numeric field or the field may be empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define TTAIL_FIL_STIN           "TTAIL_FIL_STIN"

/* Problem: Partial Transaction. TTAIL in illegal position. */
/* Solution: Input file is corrupt. Either edit the file or repoll the store. */
#define TTAIL_IN_ILLEGAL_POS     "TTAIL_IN_ILLEGAL_POS"

/* Problem: TTAIL Transaction Record Counter - Non-numeric character in numeric field or the field may be empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define TTAIL_TRC_STIN           "TTAIL_TRC_STIN"

/* Problem: Partial Transaction. TTAIL outside of transaction */
/* Solution: Input file is corrupt. Either edit the file or repoll the store. */
#define TTAIL_WITHOUT_THEAD      "TTAIL_WITHOUT_THEAD"

/* Problem: Tax Amount - Non-numeric character in numeric field. */
/* Solution: Enter the correct Tax Amount. */
#define TTAX_AMT_STIN            "TTAX_AMT_STIN"

/* Problem: TTAX File Line Identifier - Non-numeric character in numeric field or the field may be empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define TTAX_FIL_STIN            "TTAX_FIL_STIN"

/* Problem: Partial Transaction. TTAX in illegal position. */
/* Solution: Input file is corrupt. A TTAX record must be between a THEAD and a TTAIL record. Either edit the file or repoll the store. */
#define TTAX_IN_ILLEGAL_POS      "TTAX_IN_ILLEGAL_POS"

/* Problem: Credit or Debit Card Number - Non-numeric character in numeric field. */
/* Solution: Enter the correct Credit or Debit Card Number (ID Number). */
#define TTEND_CCN_STIN           "TTEND_CCN_STIN"

/* Problem: Coupon Number cannot be empty when Tender Type Group is Coupon. */
/* Solution: Enter a coupon. */
#define TTEND_COUPON_NO_REQ      "TTEND_COUPON_NO_REQ"

/* Problem: TTEND File Line Identifier - Non-numeric character in numeric field or the field may be empty. */
/* Solution: This input file is corrupt and can not be loaded. Either edit the file or repoll the store. */
#define TTEND_FIL_STIN           "TTEND_FIL_STIN"

/* Problem: Partial Transaction. TTEND in illegal position. */
/* Solution: Input file is corrupt. A TTEND record must be between a THEAD and a TTAIL record. Either edit the file or repoll the store. */
#define TTEND_IN_ILLEGAL_POS     "TTEND_IN_ILLEGAL_POS"

/* Problem: Tender Amount - Non-numeric character in numeric field or the field may be empty. */
/* Solution: Enter the correct Tender Amount. */
#define TTEND_TAM_STIN           "TTEND_TAM_STIN"

/* Problem: Tender Type ID is not part of the Tender Type Group or it is empty. */
/* Solution: Choose a Tender Type from the list. */
#define TTEND_TTI_STIN           "TTEND_TTI_STIN"

/* Problem: This UPC does not exist in the RMS. */
/* Solution: Choose a UPC from the list. */
#define UPC_NOT_FOUND            "UPC_NOT_FOUND"

/* Problem: Data for a Vendor required - At least one of the following fields must have a value: Vendor Invoice Number, Payment Reference Number and Proof of Delivery Number. */
/* Solution: Enter at least one of the following values: Vendor Invoice Number, Payment Reference Number and Proof of Delivery Number. */
#define VENDOR_DATA_REQ          "VENDOR_DATA_REQ"

/* Problem: Vendor Number on THEAD is a required field for a Merchandise Vendor Paid Out or Expense Vendor Paid Out. */
/* Solution: Choose a Vendor Number from list. */
#define VENDOR_NO_REQ            "VENDOR_NO_REQ"


#endif
