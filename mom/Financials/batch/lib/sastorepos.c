/*=========================================================================

   sastorepos.c: storepos load and lookup

=========================================================================*/

#include "sagetref.h"

static STOREPOSDAT *pa_storepostable = NULL;  /* memory-mapped storepos table */
static size_t pl_storepostablelen = 0;   /* number of items in storepostable */

/* prototypes */
STOREPOSDAT *storepos_lookup(char *is_key);
static int storeposcmp(const void *is_s1, const void *is_s2 );
int storepos_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
storepos_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
STOREPOSDAT *storepos_lookup(char *is_key)
{
   if(pl_storepostablelen == 0)
   {
      return(NULL);
   } /* end if */

   return((STOREPOSDAT *)bsearch(
          (const void *)is_key, (const void *)pa_storepostable,
           pl_storepostablelen, sizeof(STOREPOSDAT), storeposcmp));
} /* end storepos_lookup() */


/*-------------------------------------------------------------------------
storeposcmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int storeposcmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_STORE));
} /* end storeposcmp() */


/*-------------------------------------------------------------------------
storepos_loadfile() loads the storepos data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int storepos_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_storepostable = (STOREPOSDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ, 
                             MAP_SHARED, li_datafile, 0);
            if(pa_storepostable != (STOREPOSDAT *)MAP_FAILED)
            {
               pl_storepostablelen = lr_statbuf.st_size / sizeof(STOREPOSDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end storepos_loadfile() */


