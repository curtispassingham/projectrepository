include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS =
PRODUCT_LDFLAGS =
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/lib/src/resalib_include.mk

#--- Standard targets.

all: resalib-all FORCE

libs: resalib-libs

includes: resalib-includes

clobber: resalib-clobber FORCE

clean: resalib-clean FORCE

install: resalib-install FORCE

lint: resalib-lint FORCE

depend: resalib-depend

#--- resalib targets

resalib-all: resalib-libs $(RESALIB_EXECUTABLES)

resalib-libs: resalib-includes $(RESALIB_LIBS)

resalib-includes: $(RESALIB_INCLUDES)

resalib-clean: FORCE
	rm -f $(RESALIB_C_SRCS:.c=.o)
	rm -f $(RESALIB_C_SRCS:.c=.ln)
	rm -f $(RESALIB_PC_SRCS:.pc=.o)
	rm -f $(RESALIB_PC_SRCS:.pc=.c)
	rm -f $(RESALIB_PC_SRCS:.pc=.lis)
	rm -f $(RESALIB_PC_SRCS:.pc=.ln)
	for f in $(RESALIB_LIBS); \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.lint; \
		rm -f $$n; \
	done

resalib-clobber: resalib-clean FORCE
	rm -f $(RESALIB_EXECUTABLES)
	rm -f $(RESALIB_LIBS)
	rm -f sacodes.h	sacodes.c
	rm -f saerrcodes.h
	for f in $(RESALIB_LIBS); \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

resalib-install: FORCE
	-@RESALIB_INCLUDES="$(RESALIB_INCLUDES)"; \
	for f in $$RESALIB_INCLUDES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_SRC)/$$f ] || [ $$f -nt $(RETEK_LIB_SRC)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_SRC)/$$f"; \
				cp $$f $(RETEK_LIB_SRC)/$$f; \
				chmod $(MASK) $(RETEK_LIB_SRC)/$$f; \
			fi; \
		fi; \
	done; \
	RESALIB_EXECUTABLES="$(RESALIB_EXECUTABLES)"; \
	for f in $$RESALIB_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$f ] || [ $$f -nt $(RETEK_LIB_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_BIN)/$$f"; \
				cp $$f $(RETEK_LIB_BIN)/$$f; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$f; \
			fi; \
		fi; \
	done; \
	for f in $(RESALIB_LIBS); \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$f ] || [ $$f -nt $(RETEK_LIB_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_BIN)/$$f"; \
				mv $$f $(RETEK_LIB_BIN)/$$f; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$f; \
			fi; \
		fi; \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		if [ -x $$n ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$n ] || [ $$f -nt $(RETEK_LIB_BIN)/$$n ]; \
			then \
				echo "Copying $$n to $(RETEK_LIB_BIN)/$$n"; \
				cp $$n $(RETEK_LIB_BIN)/$$n; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$n; \
			fi; \
		fi; \
	done;

resalib-lint: FORCE
	-@for l in $(RESALIB_LIBS); \
	do \
		n=`expr $$l : 'lib\(.*\)\.a' \| $$l : 'lib\(.*\)\.$(SHARED_SUFFIX)'`; \
		$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk llib-l$$n.ln; \
	done

resalib-depend: resalib.d

resalib.d: $(RESALIB_C_SRCS) $(RESALIB_PC_SRCS) ${MMHOME}/oracle/lib/src/oracle.mk
	@echo "Making dependencies resalib.d..."
	@$(MAKEDEPEND) $(RESALIB_C_SRCS) $(RESALIB_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

${MMHOME}/oracle/lib/src/oracle.mk: ${MMHOME}/oracle/lib/src/oramake FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/retek.mk $@

FORCE:

#--- Executable targets.

#--- Executable lint targets.

#--- Library targets by product.

resa: sacodes.h saerrcodes.h libresa.a

#--- Library targets.

libresa.a: $(RESALIB_C_OBJS) $(RESALIB_PC_OBJS)
	$(AR) $(ARFLAGS) `basename $@` `echo $(RESALIB_C_OBJS) $(RESALIB_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

#--- Library lint targets.

llib-lresa.ln: $(RESALIB_C_OBJS:.o=.c) $(RESALIB_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o resa `echo $(RESALIB_C_OBJS:.o=.c) $(RESALIB_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

#--- Include targets.

sacodes.h sacodes.c: sacodes.sql FORCE
	@echo 'Generating `basename $@`...'
	@sqlplus ${UP} @sacodes.sql
	grep '^ORA-[0-9][0-9]*: ' newh.lst newc.lst && exit 1 || exit 0
	cmp -s newh.lst `basename sacodes.h` && rm newh.lst || mv newh.lst `basename sacodes.h`
	cmp -s newc.lst `basename sacodes.c` && rm newc.lst || mv newc.lst `basename sacodes.c`
	chmod $(MASK) `basename sacodes.h` `basename sacodes.c`

saerrcodes.h: saerrcodes.sql FORCE
	@echo 'Generating `basename $@`...'
	@sqlplus ${UP} @saerrcodes.sql
	grep '^ORA-[0-9][0-9]*: ' newh.lst && exit 1 || exit 0
	cmp -s newh.lst `basename saerrcodes.h` && rm newh.lst || mv newh.lst `basename saerrcodes.h`
	chmod $(MASK) `basename saerrcodes.h`

#--- Object dependencies.

include resalib.d

