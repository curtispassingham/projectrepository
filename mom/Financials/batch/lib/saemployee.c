/*=========================================================================

   saemployee.c: employee load and lookup

=========================================================================*/

#include "sagetref.h"

static EMPLOYEEDAT *pa_employeetable = NULL;  /* memory-mapped employee table */
static size_t pl_employeetablelen = 0;   /* number of items in employeetable */

/* prototypes */
EMPLOYEEDAT *employee_lookup(char *is_store, char *is_pos_id);
static int employeecmp(const void *is_s1, const void *is_s2);
int employee_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
employee_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
EMPLOYEEDAT *employee_lookup(char *is_store, char *is_pos_id)
{
   char ls_key[LEN_STORE + LEN_EMP_ID + 1];

   if (pl_employeetablelen == 0)
   {
      return(NULL);
   }

   memset(ls_key, ' ', LEN_STORE + LEN_EMP_ID + 1);
   memcpy(ls_key, is_store,  strlen(is_store));
   memcpy(ls_key + LEN_STORE, is_pos_id, LEN_EMP_ID);

   return((EMPLOYEEDAT *)bsearch(
          (const void *)&ls_key, (const void *)pa_employeetable,
           pl_employeetablelen, sizeof(EMPLOYEEDAT), employeecmp));
} /* end employee_lookup() */


/*-------------------------------------------------------------------------
employeecmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int employeecmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_STORE + LEN_EMP_ID));
} /* end employeecmp() */


/*-------------------------------------------------------------------------
employee_loadfile() loads the employee data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int employee_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_employeetable = (EMPLOYEEDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                        MAP_SHARED, li_datafile, 0 );
            if(pa_employeetable != (EMPLOYEEDAT *)MAP_FAILED)
            {
               pl_employeetablelen = lr_statbuf.st_size / sizeof(EMPLOYEEDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end employee_loadfile() */


