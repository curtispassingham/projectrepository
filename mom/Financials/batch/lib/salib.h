
/* note: this file uses some #defines from std_len.h, which is included by */
/* retek.h, so you should #include <retek.h> before including this file. */

#ifndef SALIB_H
#define SALIB_H

#include <stdlib.h>
#include <stdio.h>
#include "intrface.h"
#include "sastdlen.h"
#include "sacodes.h"

#define LEN_ORIN_EXP_TRAN_TYPE 6

#ifndef TRUE
  #define TRUE  1
#endif
#ifndef FALSE
  #define FALSE 0
#endif


/*-------------------------------------------------------------------------
misc general defines
-------------------------------------------------------------------------*/
/* input record related values */
#define MAXRECS                    256       /* max records per transaction */
#define MAXRECSIZE                 512       /* max input record size */

/* overall read and write modes are controlled from here, */
/* making it more convenient to deal with the CRLF/LF thing */
#define SA_READMODE                "r"
#define SA_WRITEMODE               "wb"
#define SA_APPENDMODE              "ab"

/* headno system defines */
#define HEADNO_SIZE           20        /* headno field size */
#define HEADNO_FORMAT         "%020s"   /* format for sprintf */

/* arbitrary string sizes for scratch use */
#define LARGE_SCRATCH_AREA    256
#define MEDIUM_SCRATCH_AREA   32
#define SMALL_SCRATCH_AREA    16
#define LEN_FILE_NAME   30
#define NULL_FILE_NAME  (LEN_FILE_NAME + 1)
#define LEN_TOTAL_TYPE  5
#define NULL_TOTAL_TYPE (LEN_TOTAL_TYPE + 1)

/* item_type length expected by RMS */
#define LEN_ITEM_TYPE         3

/* cross channel attribute macros */
#define LEN_RTLOG_ORIG_SYS     3
#define LEN_CUST_ORDER_NO      48
#define LEN_TRAN_PROCESS_SYS   3

#define NULL_RTLOG_ORIG_SYS    (LEN_RTLOG_ORIG_SYS + 1)
#define NULL_CUST_ORDER_NO     (LEN_CUST_ORDER_NO + 1)
#define NULL_TRAN_PROCESS_SYS  (LEN_TRAN_PROCESS_SYS + 1)

/*-------------------------------------------------------------------------
library function related defines and structures
-------------------------------------------------------------------------*/
/* ResetTran() parameters */
#define COPY_RECORD                TRUE
#define DONT_COPY_RECORD           FALSE

/* CLR2*() macros */
#define CLRTO000(field)            strncpy((field),"000   ",LEN_CODE)
#define CLRTONULL(field)           memset((field),' ',sizeof(field))
#define CLRTOSPACES(field)         memset((field),' ',sizeof(field))
#define CLRTOZEROS(field)          memset((field),'0',sizeof(field))
#define CLRTOERR(field)            strncpy((field),"ERR   ",LEN_CODE)

/* fldcpy() defines and macros */
/* field types */
#define FT_NONE                    0
#define FT_VARCHAR                 1
#define FT_DATE                    2
#define FT_NUMBER                  3
/* copy fixed-length field to fixed-length field of equal size */
#define CPYFXL(a,b,c)           fldcpy((a),sizeof(a),(b),sizeof(a),(c))
/* copy fixed-length field to fixed-length field */
#define CPYFXLFXL(a,b,c)        fldcpy((a),sizeof(a),(b),sizeof(b),(c))
/* copy string to fixed-length field */
#define CPYSTRFXL(a,b,c)        fldcpy((a),sizeof(a),(b),strlen(b),(c))

/* Free dynamically allocated memory and set pointer to NULL */
#define FREE(p) \
   do { \
       free(p); \
       p = NULL; \
       /* CONST COND */ \
      } while (0)

/*-------------------------------------------------------------------------
main data file structures
-------------------------------------------------------------------------*/

typedef struct pt_sa_system_options{
   long l_days_before_purge;
   long l_day_post_sale;
   char s_balance_level_ind[NULL_IND];
   long l_max_days_compare_dups;
   char s_comp_base_date[NULL_DATE];
   long l_comp_no_days;
   char s_check_dup_miss_tran[NULL_IND];
   char s_unit_of_work[NULL_CODE];
   char s_audit_after_imp_ind[NULL_IND];
   char s_fuel_dept[NULL_DEPT];
   char s_default_chain[NULL_CODE];
   char s_close_in_order[NULL_IND];
   char s_escheat_ind[NULL_IND];
   char s_partner_type[NULL_PARTNER_TYPE];
   char s_partner_id[NULL_PARTNER_ID];
   char s_auto_validate_tran_employee_id[NULL_IND];
   char s_table_owner[NULL_TABLE_OWNER];
   char s_class_level_vat_ind[NULL_IND];
   char s_multichannel_ind[NULL_IND];
   char s_default_tax_type[NULL_CODE];          /* System Level Tax Type at default */
   char s_wkstation_tran_append_ind[NULL_IND];
   char s_inv_resv_from_store_ind[NULL_IND];   /* Inventory reservation from Store */
   char s_inv_resv_layaway_ind[NULL_IND];      /* Inventory reservation for Layaway */
   char s_cc_no_mask_char[NULL_IND];           /* Masked character for Tender CC/DC */
   char s_rpm_ind[NULL_IND];                   /* System Level Rpm Ind */
} pt_sa_system_options;

typedef struct pt_store_day{
   char (*store_day_seq_no)[NULL_BIG_SEQ_NO];
   char (*business_date)[NULL_DATE];
   char (*store)[NULL_STORE];
} pt_store_day;

/* Retek (enhanced) TLOG output file record structures */

#define RTL_MAXRECSIZE   1000

#define RTL_FRECDESC_SIZE  5

/* record type tokens */
#define RTLRT_END        0    /* signifies "stop here for this transaction" */
#define RTLRT_FHEAD      1
#define RTLRT_FTAIL      2
#define RTLRT_THEAD      3
#define RTLRT_TTAIL      4
#define RTLRT_TCUST      5
#define RTLRT_CATT       6
#define RTLRT_TITEM      7
#define RTLRT_IDISC      8
#define RTLRT_IGTAX      9
#define RTLRT_TTAX       10
#define RTLRT_TPYMT      11
#define RTLRT_TTEND      12
#define RTLRT_TINFO      13
#define RTLRT_UNKNOWN    14

typedef struct { /* standard item uom, vat info */
   char standard_uom[ LEN_UOM ];
   char standard_qty[ LEN_QTY ];
   char standard_unit_retail[ LEN_AMT ];
   char standard_orig_unit_retail[ LEN_AMT ];
   char conversion_error[ LEN_IND ];
   char class_vat_ind[ LEN_IND ];
   char total_igtax_amt[ LEN_AMT ];
} pt_item_uom;

typedef struct { /* standard disc uom info */
   char standard_qty[ LEN_QTY ];
   char standard_unit_disc_amt[ LEN_AMT ];
   char conversion_error[ LEN_IND ];
} pt_disc_uom;

typedef struct { /* igtax total igtax amt info */
   char TotalIgtaxAmt[LEN_AMT];
} pt_igtax_tia;

/* file header */
#define RTL_FHEAD_FRECDESC "FHEAD"
typedef struct {    /* RTL_FHEAD struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_type_definition[ LEN_FILE_TYPE_DEF ];
   char file_create_date[ LEN_DATETIME ];
   char business_date[ LEN_DATEONLY ];
   char location[ LEN_LOC ];
   char ref_no[ LEN_REF_NO ];
   char rtlog_orig_sys[ LEN_RTLOG_ORIG_SYS ];
   char newline[ LEN_NEWLINE ];
} RTL_FHEAD;

/* error record */
/* This record type does not occur in a customer-supplied RTLOG file. It */
/* appears when a transaction fails in such a manner that it is written */
/* out in RTLOG format to the bad transaction file. When this happens, an */
/* RTL_ERROR record is included to state the type of failure encountered. */
#define RTL_ERROR_FRECDESC "ERROR"
typedef struct {    /* RTL_ERROR struct */
   char frecdesc[ LEN_FREC_DESC ];
   char status[ LEN_STATUS ];
   char code[ FAILCODE_SIZE ];
   char seq_no1[ LEN_LITTLE_SEQ_NO ];
   char seq_no2[ LEN_LITTLE_SEQ_NO ];
   char rec_type[ LEN_CODE ];
   char desc[ 50 ];
   char newline[ LEN_NEWLINE ];
} RTL_ERROR;

/* transaction info */
#define RTL_TINFO_FRECDESC "TINFO"
typedef struct {    /* RTL_TINFO struct */
   char frecdesc[ LEN_FREC_DESC ];
   char status[ LEN_STATUS ];
   char tran_sign[ LEN_TRAN_SIGN ];
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char rev_no[ LEN_SA_REV_NO ];
   char newline[ LEN_NEWLINE ];
} RTL_TINFO;

/* transaction header */
#define RTL_THEAD_FRECDESC "THEAD"
typedef struct {    /* RTL_THEAD struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char pos_register[ LEN_REGISTER ];
   char tran_datetime[ LEN_DATETIME ];
   char tran_no[ LEN_TRAN_NO ];
   char cashier[ LEN_EMP_ID ];
   char salesperson[ LEN_EMP_ID ];
   char tran_type[ TRAT_SIZE ];
   char sub_tran_type[ TRAS_SIZE ];
   char orig_tran_no[ LEN_TRAN_NO ];
   char orig_reg_no[ LEN_REGISTER ];
   char reason_code[ REAC_SIZE ];
   char vendor_no[ LEN_VENDOR_NO ];
   char vendor_invc_no[ LEN_VENDOR_INVC_NO ];
   char payment_ref_no[ LEN_PAYMENT_REF_NO ];
   char proof_of_delivery_no[ LEN_PROOF_DELIVERY_NO ];
   char ref_no1[ LEN_REF_NO ];
   char ref_no2[ LEN_REF_NO ];
   char ref_no3[ LEN_REF_NO ];
   char ref_no4[ LEN_REF_NO ];
   char value_sign[ LEN_SALES_SIGN ];
   char value[ LEN_AMT ];
   char banner_id[ LEN_BANNER_ID ];
   char rounded_amt_sign[ LEN_SALES_SIGN ];
   char rounded_amt[ LEN_AMT ];
   char rounded_off_amt_sign[ LEN_SALES_SIGN ];
   char rounded_off_amt[ LEN_AMT ];
   char credit_promotion_id[ LEN_CREDIT_PROMOTION_ID ];
   char ref_no25[ LEN_REF_NO ];
   char ref_no26[ LEN_REF_NO ];
   char ref_no27[ LEN_REF_NO ];
   char tran_process_sys[ LEN_TRAN_PROCESS_SYS ];
   char newline[ LEN_NEWLINE ];
} RTL_THEAD;

/* transaction customer */
#define RTL_TCUST_FRECDESC "TCUST"
typedef struct {    /* RTL_TCUST struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char cust_id[ LEN_CUST_ID ];
   char cust_id_type[ CIDT_SIZE ];
   char cust_name[ LEN_CUST_NAME ];
   char addr1[ LEN_CUST_ADDR ];
   char addr2[ LEN_CUST_ADDR ];
   char city[ LEN_CITY ];
   char state[ LEN_STATE ];
   char postalcode[ LEN_POSTAL_CODE ];
   char country[ LEN_COUNTRY ];
   char home_phone[ LEN_PHONE ];
   char work_phone[ LEN_PHONE ];
   char email[ LEN_EMAIL ];
   char birthdate[ LEN_DATEONLY ];
   char newline[ LEN_NEWLINE ];
} RTL_TCUST;

/* transaction customer attribute */
#define RTL_CATT_FRECDESC "CATT "
typedef struct { /* RTL_CATT struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char attrib_type[ SACA_SIZE ];
   char attrib_value[ LEN_CODE ];
   char newline[ LEN_NEWLINE ];
} RTL_CATT;

/* transaction item */
#define RTL_TITEM_FRECDESC "TITEM"
typedef struct {    /* RTL_TITEM struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char item_status[ SASI_SIZE ];
   char item_type[ LEN_CODE ];
   char item_number_type[ LEN_CODE ];
   char format_id[ LEN_IND ];
   char item[ LEN_ITEM ];
   char ref_item[ LEN_ITEM ];
   char non_merch_item[ LEN_ITEM ];
   char voucher_no[ LEN_VOUCHER_NO ];
   char dept[ LEN_DEPT ];
   char class[ LEN_CLASS ];
   char subclass[ LEN_SUBCLASS ];
   char qty_sign[ LEN_SALES_SIGN ];
   char qty[ LEN_QTY ];
   char selling_uom[ LEN_UOM ];
   char unit_retail[ LEN_AMT ];
   char override_reason[ ORRC_SIZE ];
   char orig_unit_retail[ LEN_AMT ];
   char tax_ind[ LEN_IND ];
   char pump[ LEN_PUMP ];
   char ref_no5[ LEN_REF_NO ];
   char ref_no6[ LEN_REF_NO ];
   char ref_no7[ LEN_REF_NO ];
   char ref_no8[ LEN_REF_NO ];
   char item_swiped_ind[ LEN_IND ];
   char return_reason_code[ SARR_SIZE ];
   char salesperson[ LEN_EMP_ID ];
   char expiration_date[ LEN_DATEONLY ];
   char drop_ship_ind[ LEN_IND ];
   char uom_qty[ LEN_UOM_QTY ];
   char catchweight_ind[ LEN_CATCHWEIGHT_IND ];
   char selling_item[ LEN_SELLING_ITEM ];
   char cust_ord_line_no[ LEN_CUST_ORD_LINE_NO ];
   char media_id[ LEN_MEDIA_ID ];
   char total_igtax_amt[ LEN_IGTAX_AMT ];
   char unique_id[LEN_UNIQUE_ID];
   char cust_order_no[ LEN_CUST_ORDER_NO ];
   char cust_order_date[ LEN_CUST_ORD_HEAD_DATE ];
   char fulfill_order_no[ LEN_CUST_ORDER_NO ];
   char no_inv_ret_ind[ LEN_IND ];
   char sales_type[ LEN_IND ];
   char return_wh[ LEN_WH ];
   char return_disp[ LEN_RETURN_DISP ];
   char newline[ LEN_NEWLINE ];
} RTL_TITEM;

#define RTL_IDISC_FRECDESC "IDISC"
typedef struct {    /* RTL_IDISC struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char rms_promo_type[ PRMT_SIZE ];
   char disc_ref_no[ LEN_PROMOTION ];
   char disc_type[ SADT_SIZE ];
   char coupon_no[ LEN_COUPON_NO ];
   char coupon_ref_no[ LEN_COUPON_REF_NO ];
   char qty_sign[ LEN_SALES_SIGN ];
   char qty[ LEN_QTY ];
   char unit_disc_amt[ LEN_AMT ];
   char ref_no13[ LEN_REF_NO ];
   char ref_no14[ LEN_REF_NO ];
   char ref_no15[ LEN_REF_NO ];
   char ref_no16[ LEN_REF_NO ];
   char uom_qty[ LEN_UOM_QTY ];
   char catchweight_ind[ LEN_CATCHWEIGHT_IND ];
   char promo_comp[ LEN_PROMO_COMP ];
   char newline[ LEN_NEWLINE ];
} RTL_IDISC;

/* item tax */
#define RTL_IGTAX_FRECDESC "IGTAX"
typedef struct {    /* RTL_IGTAX struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tax_authority[ LEN_TAX_AUTHORITY ];
   char igtax_code[ LEN_IGTAX_CODE ];
   char igtax_rate[ LEN_IGTAX_RATE ];
   char igtax_amt_sign[ LEN_SALES_SIGN ];
   char total_igtax_amt[ LEN_IGTAX_AMT ];
   char ref_no21[ LEN_REF_NO ];
   char ref_no22[ LEN_REF_NO ];
   char ref_no23[ LEN_REF_NO ];
   char ref_no24[ LEN_REF_NO ];
   char newline[ LEN_NEWLINE ];
} RTL_IGTAX;

/* transaction tax */
#define RTL_TTAX_FRECDESC  "TTAX "
typedef struct {    /* RTL_TTAX struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tax_code[ TAXC_SIZE ];
   char tax_amt_sign[ LEN_SALES_SIGN ];
   char tax_amt[ LEN_AMT ];
   char ref_no17[ LEN_REF_NO ];
   char ref_no18[ LEN_REF_NO ];
   char ref_no19[ LEN_REF_NO ];
   char ref_no20[ LEN_REF_NO ];
   char newline[ LEN_NEWLINE ];
} RTL_TTAX;

/* transaction payment */
#define RTL_TPYMT_FRECDESC  "TPYMT"
typedef struct {    /* RTL_TPYMT struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char payment_amt_sign[ LEN_SALES_SIGN ];
   char payment_amt[ LEN_AMT ];
   char newline[ LEN_NEWLINE ];
} RTL_TPYMT;

/* transaction tender */
#define RTL_TTEND_FRECDESC "TTEND"
typedef struct {
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tender_type_group[ TENT_SIZE ];
   char tender_type_id[ TENS_SIZE ];
   char tender_amt_sign[ LEN_SALES_SIGN ];
   char tender_amt[ LEN_AMT ];
   char cc_no[ LEN_CC_NO ];
   char cc_auth_no[ LEN_CC_AUTH_NO ];
   char cc_auth_src[ CCAS_SIZE ];
   char cc_cardholder_verf[ CCVF_SIZE ];
   char cc_exp_date[ LEN_DATEONLY ];
   char cc_entry_mode[ CCEM_SIZE ];
   char cc_terminal_id[ LEN_TERM_ID ];
   char cc_special_cond[ CCSC_SIZE ];
   char voucher_no[ LEN_VOUCHER_NO ];
   char coupon_no[ LEN_COUPON_NO ];
   char coupon_ref_no[ LEN_COUPON_REF_NO ];
   char check_acct_no[ LEN_CHECK_ACCT_NO ]; /*Check account no.for check tender type */
   char check_no[ LEN_CHECK_NO ];           /*Check no. for check tender type */
   char identi_method[ IDMH_SIZE ]; /*Identification method for check tender type */
   char identi_id[ LEN_IDENTI_ID ];         /*Identification id for check tender type */
   char orig_currency[ LEN_CURRENCY_CODE ];
   char orig_curr_amt[ LEN_AMT ];
   char ref_no9[ LEN_REF_NO ];
   char ref_no10[ LEN_REF_NO ];
   char ref_no11[ LEN_REF_NO ];
   char ref_no12[ LEN_REF_NO ];
   char newline[ LEN_NEWLINE ];
} RTL_TTEND;

/* transaction trailer */
#define RTL_TTAIL_FRECDESC "TTAIL"
typedef struct {    /* RTL_TTAIL struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_rec_counter[ LEN_FILE_LINE_NO ];
   char newline[ LEN_NEWLINE ];
} RTL_TTAIL;

/* file trailer */
#define RTL_FTAIL_FRECDESC "FTAIL"
typedef struct {    /* RTL_FTAIL struct */
   char frecdesc[ LEN_FREC_DESC ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_rec_counter[ LEN_FILE_LINE_NO ];
   char newline[ LEN_NEWLINE ];
} RTL_FTAIL;

/* rms output file record structures */

#define RMS_FRECDESC_SIZE          5

/* file header */
#define RMS_FHEAD_FRECDESC "FHEAD"
typedef struct {    /* RMS_FHEAD struct */
   char frecdesc[ RMS_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_type_definition[ LEN_FILE_TYPE_DEF ];
   char file_create_date[ LEN_DATETIME ];
   char location[ LEN_LOC ];
   char vat_include_ind[ LEN_IND ];
   char vat_region[ LEN_VAT_REGION ];
   char currency_code[ LEN_CURRENCY_CODE ];
   char currency_rtl_dec[ LEN_CURRENCY_RTL_DEC ];
   char newline[ LEN_NEWLINE ];
} RMS_FHEAD;

/* transaction header */
#define RMS_THEAD_FRECDESC "THEAD"
typedef struct {    /* RMS_THEAD struct */
   char frecdesc[ RMS_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_date[ LEN_DATETIME ];
   char item_type[ LEN_ITEM_TYPE ];
   char item[ LEN_ITEM ];
   char dept[ LEN_DEPT ];
   char class[ LEN_CLASS ];
   char subclass[ LEN_SUBCLASS ];
   char pack_ind[ LEN_IND ];
   char item_level[ LEN_IND ];
   char tran_level[ LEN_IND ];
   char waste_type[ WSTG_SIZE ];
   char waste_pct[ LEN_PCT ];
   char tran_type[ LEN_EXP_TRAN_TYPE ]; /* note: for "S" or "R", *not* TRAT_SIZE */
   char drop_ship_ind[ LEN_IND ];
   char total_standard_sales_qty[ LEN_QTY ];
   char selling_uom[ LEN_UOM ];
   char sales_sign[ LEN_SALES_SIGN ];
   char total_sales_value[ LEN_AMT ];
   char last_time_modified[ LEN_DATETIME ];
   char catchweight_ind[ LEN_CATCHWEIGHT_IND ];
   char total_weight[ LEN_UOM_QTY ];
   char subtrans_type_ind[ LEN_SUBTRANS_TYPE_IND ];
   char total_igtax_value[ LEN_AMT ];
   char sales_type[ LEN_IND ];
   char no_inv_ret_ind[ LEN_IND];
   char return_disp[ LEN_RETURN_DISP ];
   char return_wh[ LEN_WH ];
   char newline[ LEN_NEWLINE ];
} RMS_THEAD;

/* transaction tax */
#define RMS_TTAX_FRECDESC "TTAX "
typedef struct {    /* RMS_TTAX struct */
   char frecdesc[ RMS_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tax_code[ LEN_VAT_CODE_ID ];
   char tax_rate[ LEN_AMT ];
   char total_tax_amt[ LEN_AMT ];
   char newline[ LEN_NEWLINE ];
} RMS_TTAX;

/* transaction detail */
#define RMS_TDETL_FRECDESC "TDETL"
typedef struct {    /* RMS_TDETL struct */
   char frecdesc[ RMS_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char promo_tran_type[ PRMT_SIZE ];
   char promo_no[ LEN_PROMOTION ];
   char sales_qty[ LEN_QTY ];
   char sales_value[ LEN_AMT ];
   char disc_value[ LEN_AMT ];
   char promo_comp[LEN_PROMO_COMP ];
   char newline[ LEN_NEWLINE ];
} RMS_TDETL;

/* transaction trailer */
#define RMS_TTAIL_FRECDESC "TTAIL"
typedef struct {    /* RMS_TTAIL struct */
   char frecdesc[ RMS_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_rec_counter[ LEN_DTL_LINE_CNT ];
   char newline[ LEN_NEWLINE ];
} RMS_TTAIL;

/* file trailer */
#define RMS_FTAIL_FRECDESC "FTAIL"
typedef struct {    /* RMS_FTAIL struct */
   char frecdesc[ RMS_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_rec_counter[ LEN_FILE_LINE_NO ];
   char newline[ LEN_NEWLINE ];
} RMS_FTAIL;


/* rdw output file record structures */

#define RDW_FRECDESC_SIZE      5

/* file header */
#define RDW_FHEAD_FRECDESC "FHEAD"
typedef struct {    /* RDW_FHEAD struct */
   char frecdesc[ RDW_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_type_definition[ LEN_FILE_TYPE_DEF ];
   char file_create_date[ LEN_DATETIME ];
   char newline[ LEN_NEWLINE ];
} RDW_FHEAD;

/* transaction header */
#define RDW_THEAD_FRECDESC "THEAD"
typedef struct {    /* RDW_THEAD struct */
   char frecdesc[ RDW_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char business_date[ LEN_DATEONLY ];
   char tran_datetime[ LEN_DATETIME ];
   char location[ LEN_LOC ];
   char register_id[ LEN_REGISTER ];
   char banner_id[ LEN_BANNER_ID ];
   char media_id[ LEN_MEDIA_ID ];
   char selling_item[ LEN_SELLING_ITEM ];
   char customer_order_no[ LEN_CUST_ORDER_NO ];
   char customer_order_line_no[ LEN_CUST_ORD_LINE_ID ];
   char customer_order_create_date[ LEN_CUST_ORD_CREATE_DATE ];
   char cashier_id[ LEN_EMP_ID ];
   char salesperson_id[ LEN_EMP_ID ];
   char cust_id_type[ CIDT_SIZE ];
   char cust_id_number[ LEN_CUST_ID ];
   char tran_no[ LEN_TRAN_NO ];
   char orig_reg_no[ LEN_REGISTER ];
   char orig_tran_no[ LEN_TRAN_NO ];
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char rev_no[ LEN_SA_REV_NO ];
   char tran_sign[ LEN_IND ]; /* N = reversal, P = positive */
   char tran_type[ TRAT_SIZE ];
   char sub_tran_type[ TRAS_SIZE ];
   char retail_type[ LEN_IND ];
   char item_seq_no[ LEN_LITTLE_SEQ_NO ];
   char emp_cashier_no[ LEN_EMP_ID ];
   char receipt_ind[ LEN_IND ];
   char reason_code[ LEN_CODE ];
   char vendor_no[ LEN_VENDOR_NO ];
   char item_type[ LEN_CODE ];
   char item[ LEN_ITEM ];
   char ref_item[ LEN_ITEM ];
   char tax_ind[ LEN_IND ];
   char item_swiped_ind[ LEN_CODE ];
   char dept[ LEN_DEPT ];
   char class[ LEN_CLASS ];
   char subclass[ LEN_SUBCLASS ];
   char total_sales_qty[ LEN_QTY ];
   char total_sales_value[ LEN_AMT ];
   char override_reason[ LEN_CODE ];
   char return_reason_code[ LEN_CODE ];
   char total_orig_sign[ LEN_IND ];
   char total_orig_value[ LEN_AMT ];
   char weather[ LEN_CODE ];
   char temperature[ LEN_CODE ];
   char traffic[ LEN_CODE ];
   char construction[ LEN_CODE ];
   char drop_ship_ind[ LEN_IND ];
   char item_status[LEN_CODE];
   char tran_process_sys[LEN_TRAN_PROCESS_SYS];
   char return_wh[LEN_WH];
   char fulfill_order_no[ LEN_CUST_ORDER_NO ];
   char no_inv_ret_ind[ LEN_IND ];
   char sales_type[ LEN_IND ];
   char return_disp[ LEN_RETURN_DISP ];
   char newline[ LEN_NEWLINE ];
} RDW_THEAD;

/* transaction detail */
#define RDW_TDETL_FRECDESC "TDETL"
typedef struct {    /* RDW_TDETL struct */
   char frecdesc[ RDW_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char discount_type[ LEN_CODE ];
   char promo_tran_type[ LEN_CODE ];
   char promo_no[ LEN_PROMOTION ];
   char promo_comp[ LEN_PROMO_COMP ];
   char coupon_no[ LEN_COUPON_NO ];
   char coupon_ref_no[ LEN_COUPON_REF_NO ];
   char sales_qty[ LEN_QTY ];
   char sales_sign[ LEN_IND ];
   char sales_value[ LEN_AMT ];
   char disc_value[ LEN_AMT ];
   char newline[ LEN_NEWLINE ];
} RDW_TDETL;

/* transaction detail for RDWS output file */
#define RDW_FDETL_FRECDESC "FDETL"
typedef struct {    /* RDWS_TDETL struct */
   char frecdesc[ RDW_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_date[ LEN_DATEONLY ];
   char location[ LEN_LOC ];
   char sales_sign[ LEN_SALES_SIGN ];
   char total_id[ LEN_TOTAL_ID ];
   char ref_no1[ LEN_REF_NO ];
   char ref_no2[ LEN_REF_NO ];
   char ref_no3[ LEN_REF_NO ];
   char total_sign[ LEN_SALES_SIGN ];
   char total_amount[ LEN_AMT ];
   char newline[ LEN_NEWLINE ];
} RDWS_TDETL;

/* transaction detail for RDWC output file */
#define RDW_FDETL_FRECDESC "FDETL"
typedef struct {    /* RDWC_TDETL struct */
   char frecdesc[ RDW_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_date[ LEN_DATEONLY ];
   char location[ LEN_LOC ];
   char cashier_id[ LEN_EMP_ID ];
   char register_id[ LEN_REGISTER ];
   char sales_sign[ LEN_SALES_SIGN ];
   char total_id[ LEN_TOTAL_ID ];
   char ref_no1[ LEN_REF_NO ];
   char ref_no2[ LEN_REF_NO ];
   char ref_no3[ LEN_REF_NO ];
   char total_sign[ LEN_SALES_SIGN ];
   char total_amount[ LEN_AMT ];
   char newline[ LEN_NEWLINE ];
} RDWC_TDETL;

/* transaction tender */
#define RDW_FDETL_FRECDESC "FDETL"
typedef struct {
   char frecdesc[ RDW_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char business_date[ LEN_DATEONLY ];
   char tran_datetime[ LEN_DATETIME ];
   char location[ LEN_LOC ];
   char cashier_id[ LEN_EMP_ID ];
   char register_id[ LEN_REGISTER ];
   char tran_sign[ LEN_IND ];
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char rev_no[ LEN_SA_REV_NO ];
   char tran_type[ TRAT_SIZE ];
   char tender_type_group[ TENT_SIZE ];
   char tender_type_id[ TENS_SIZE ];
   char tender_amt[ LEN_AMT ];
   char cc_entry_mode[ CCEM_SIZE ];
   char voucher_no[ LEN_VOUCHER_NO ];
   char voucher_age[ LEN_VOUCHER_AGE ];
   char escheat_date[ LEN_DATEONLY ];
   char coupon_no[ LEN_COUPON_NO ];
   char coupon_ref_no[ LEN_COUPON_REF_NO ];
   char newline[ LEN_NEWLINE ];
} RDW_TTEND;

/* transaction trailer */
#define RDW_TTAIL_FRECDESC "TTAIL"
typedef struct {    /* RDW_TTAIL struct */
   char frecdesc[ RDW_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_rec_counter[ LEN_DTL_LINE_CNT ];
   char newline[ LEN_NEWLINE ];
} RDW_TTAIL;

/* file trailer */
#define RDW_FTAIL_FRECDESC "FTAIL"
typedef struct {    /* RDW_FTAIL struct */
   char frecdesc[ RDW_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_rec_counter[ LEN_FILE_LINE_NO ];
   char newline[ LEN_NEWLINE ];
} RDW_FTAIL;


/* credit card file */

#define CCD_FRECDESC_SIZE      5

/* file header */
#define CCD_FHEAD_FRECDESC "FHEAD"
typedef struct {    /* CCD_FHEAD struct */
   char frecdesc[ CCD_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_type_definition[ LEN_FILE_TYPE_DEF ];
   char file_create_date[ LEN_DATETIME ];
   char newline[ LEN_NEWLINE ];
} CCD_FHEAD;

/* file detail */
#define CCD_FDETL_FRECDESC "FDETL"
typedef struct {    /* CCD_FDETL struct */
   char frecdesc[ CCD_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_seq_no[ HEADNO_SIZE ];
   char tran_date[ LEN_DATETIME ];
   char location[ LEN_LOC ];
   char tran_type[ TRAT_SIZE ];
   char tender_type_group[ TENT_SIZE ];
   char tender_type_id[ TENS_SIZE ];
   char tender_sign[ LEN_SALES_SIGN ];
   char tender_amt[ LEN_AMT ];
   char cc_no[ LEN_CC_NO ];
   char cc_auth_no[ LEN_CC_AUTH_NO ];
   char cc_exp_date[ LEN_DATETIME ];
   char staged_to_bank[ LEN_DATETIME ];
   char staged_bank_counter[ LEN_STAGED_COUNTER ];
   char newline[ LEN_NEWLINE ];
} CCD_FDETL;

/* file trailer */
#define CCD_FTAIL_FRECDESC "FTAIL"
typedef struct {    /* CCD_FTAIL struct */
   char frecdesc[ CCD_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_rec_counter[ LEN_FILE_LINE_NO ];
   char newline[ LEN_NEWLINE ];
} CCD_FTAIL;


/* orin output file record structures */

#define ORIN_FRECDESC_SIZE      5

/* file header */
#define ORIN_FHEAD_FRECDESC "FHEAD"
typedef struct {    /* ORIN_FHEAD struct */
   char frecdesc[ ORIN_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_type_definition[ LEN_FILE_TYPE_DEF ];
   char file_create_date[ LEN_DATETIME ];
   char file_location[ LEN_LOC ];
   char newline[ LEN_NEWLINE ];
}ORIN_FHEAD;

/* transaction header */
#define ORIN_THEAD_FRECDESC "THEAD"
typedef struct {    /* ORIN_THEAD struct */
   char frecdesc[ ORIN_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_date[ LEN_DATETIME ];
   char tran_type[ LEN_ORIN_EXP_TRAN_TYPE ]; /* note: for "S" or "R", *not* TRAT_SIZE */
   char newline[ LEN_NEWLINE ];
} ORIN_THEAD;


/* transaction detail */
#define ORIN_TDETL_FRECDESC "TDETL"
typedef struct {    /* ORIN_TDETL struct */
   char frecdesc[ ORIN_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char item_type [ LEN_ITEM_TYPE  ];
   char item[ LEN_ITEM ];
   char item_status[ SASI_SIZE ];
   char dept[ LEN_DEPT ];
   char class[ LEN_CLASS ];
   char subclass[ LEN_SUBCLASS ];
   char pack_ind[ LEN_IND ];
   char qty_sign[ LEN_IND ];
   char sales_qty[ LEN_QTY ];
   char selling_uom[ LEN_UOM ];
   char catchweight_ind[ LEN_CATCHWEIGHT_IND ];
   char customer_order_no[ LEN_CUST_ORD_HEAD_ID ];
   char newline[ LEN_NEWLINE ];
} ORIN_TDETL;

#define ORIN_TTAIL_FRECDESC "TTAIL"
typedef struct {    /* ORIN_TTAIL struct */
   char frecdesc[ ORIN_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_rec_counter[ LEN_DTL_LINE_CNT ];
   char newline[ LEN_NEWLINE ];
} ORIN_TTAIL;

/* file trailer */
#define ORIN_FTAIL_FRECDESC "FTAIL"
typedef struct {    /* ORIN_FTAIL struct */
   char frecdesc[ ORIN_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_rec_counter[ LEN_FILE_LINE_NO ];
   char newline[ LEN_NEWLINE ];
} ORIN_FTAIL;

/* sim output file record structures */

#define SIM_FRECDESC_SIZE          5

/* file header */
#define SIM_FHEAD_FRECDESC "FHEAD"
typedef struct {    /* SIM_FHEAD struct */
   char frecdesc[ SIM_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_type_definition[ LEN_FILE_TYPE_DEF ];
   char location[ LEN_LOC ];
   char business_date [ LEN_DATEONLY ];
   char file_create_date[ LEN_DATETIME ];
   char newline[ LEN_NEWLINE ];
} SIM_FHEAD;

/* transaction header */
#define SIM_THEAD_FRECDESC "THEAD"
typedef struct {    /* SIM_THEAD struct */
   char frecdesc[ SIM_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_no[ LEN_TRAN_NO ];
   char pos_register[ LEN_REGISTER ];
   char rev_no [LEN_SA_REV_NO];
   char tran_date[ LEN_DATETIME ];
   char tran_type[ TRAT_SIZE ];
   char pos_tran_ind[ LEN_IND ];
   char newline[ LEN_NEWLINE ];
} SIM_THEAD;

/* transaction detail */
#define SIM_TDETL_FRECDESC "TDETL"
typedef struct {    /* SIM_TDETL struct */
   char frecdesc[ SIM_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char item_seq_no[ LEN_LITTLE_SEQ_NO ];
   char item[ LEN_ITEM ];
   char item_type[ LEN_CODE ];
   char item_status[ SASI_SIZE ];
   char unique_id[LEN_UNIQUE_ID]; /* serial number */
   char pack_ind[ LEN_IND ];
   char catchweight_ind[ LEN_CATCHWEIGHT_IND ];
   char qty_sign[ LEN_IND ];
   char qty_value[ LEN_QTY ];
   char standard_uom[ LEN_UOM ];
   char selling_uom[ LEN_UOM ];
   char waste_type[ WSTG_SIZE ];
   char waste_pct[ LEN_PCT ];
   char drop_ship_ind[ LEN_IND ];
   char actual_weight[ LEN_UOM_QTY ];
   char actual_weight_sign[ LEN_IND ];
   char reason_code[ LEN_CODE ]; /* for returns and void */
   char sales_value[ LEN_AMT ];
   char sales_sign[ LEN_IND ];
   char unit_retail[ LEN_AMT ];
   char sales_type[ LEN_IND ];
   char customer_order_no[ LEN_CUST_ORDER_NO ];
   char customer_order_type[ LEN_CODE ];
   char fulfill_order_no[ LEN_CUST_ORDER_NO ];
   char newline[ LEN_NEWLINE ];
} SIM_TDETL;

/* transaction trailer */
#define SIM_TTAIL_FRECDESC "TTAIL"
typedef struct {    /* SIM_TTAIL struct */
   char frecdesc[ SIM_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char tran_rec_counter[ LEN_DTL_LINE_CNT ];
   char newline[ LEN_NEWLINE ];
} SIM_TTAIL;

/* file trailer */
#define SIM_FTAIL_FRECDESC "FTAIL"
typedef struct {    /* SIM_FTAIL struct */
   char frecdesc[ SIM_FRECDESC_SIZE ];
   char flineid[ LEN_FILE_LINE_NO ];
   char file_rec_counter[ LEN_FILE_LINE_NO ];
   char newline[ LEN_NEWLINE ];
} SIM_FTAIL;

/*-------------------------------------------------------------------------
salib function prototypes
-------------------------------------------------------------------------*/
/* saatoin.c */
int atoin(char *is_s, int ii_len);
#define ATOIN( s) atoin( s, sizeof(s))

/* saatoln.c */
long atoln(char *is_s, int ii_len);
#define ATOLN( s) atoln( s, sizeof(s))

/* sadatetime.c */
void getDateTime(char *os_date_time);

/* saemptyfld.c */
int empty_field(char *is_s, int ii_len);
int zero_field(char *is_s, int ii_len);
#define EMPTY_FIELD( s) empty_field( s, sizeof( s))
#define ZERO_FIELD( s) zero_field( s, sizeof( s))

/* safldcpy.c */
void fldcpy(char *os_field_d, int ii_dlen, const char *is_s, int ii_slen, int ii_fldtype);

/* safxl2str.c */
void fxl2str(char *os_str, char *is_fxl, int ii_len, int ii_dp );

/* sagetrec.c */
int getrec(char *os_p, FILE *if_f);

/* saisleap.c */
int isleapyear(int ii_year);

/* saputrec.c */
int putrec(char *is_point, FILE *if_f);

/* sastr2fxl.c */
void str2fxl(char *os_fxl, char *is_str, int ii_len, int ii_dp);

/* sastrsig.c */
int strsig(char *is_s);

/* satrat.c */
int trat_lookup(char *is_string);

/* savaldate.c */
int sa_valid_date(char *is_p, int ii_len);
#define SA_VALID_DATE( p) sa_valid_date( p, sizeof( p))

/* savalnum.c */
int sa_valid_all_numeric(char *is_p, int ii_len);
#define SA_VALID_ALL_NUMERIC( p) sa_valid_all_numeric( p, sizeof( p))

/* savaltime.c */
int sa_valid_time(char *is_p, int ii_len);
#define SA_VALID_TIME( p) sa_valid_time( p, sizeof( p))

/*-------------------------------------------------------------------------*/
/* convertCurrency                                                         */
/*-------------------------------------------------------------------------*/

int convertCurrency(
   char *is_business_date,
   char *is_local_currency_code,
   char *is_convert_to_currency_code,
   char *is_input_amount,
   char *os_converted_amount,
   char *os_error_message);

/*-------------------------------------------------------------------------*/
/* val_increment                                                           */
/*-------------------------------------------------------------------------*/

int val_increment(
   char *value,
   int   size);

/*-------------------------------------------------------------------------*/
/* getConversion                                                           */
/*-------------------------------------------------------------------------*/

int getConversion(
   char *is_store_day_seq_no,
   char *is_amount,
   char *os_converted_amount,
   char *os_error_message);

/*-------------------------------------------------------------------------*/
/* getLocks                                                                */
/*-------------------------------------------------------------------------*/

int get_lock(
   char       *is_store_day_seq_no,
   const char *is_system_code,
   const char *is_lock_type_ind,
   char       *os_error_message);

int release_lock(
   char       *is_store_day_seq_no,
   const char *is_system_code,
   const char *is_lock_type_ind,
   char       *os_error_message);

/*-------------------------------------------------------------------------*/
/* fetchStoreDay                                                           */
/*-------------------------------------------------------------------------*/

int fetchStoreDay(
   char *is_store_no,
   char *is_business_date,
   char *os_store_day_seq_no);

int fetchStoreDayInfo(
   char *is_store_day_seq_no,
   char *os_store_no,
   char *os_business_date,
   char *os_currency_code);

/*-------------------------------------------------------------------------*/
/* getTotals                                                               */
/*-------------------------------------------------------------------------*/

/* Assumption: The usage_type is the same as the system_code               */
int getTotal(
   char       is_store_day_seq_no[NULL_BIG_SEQ_NO],
   char       is_store[NULL_STORE],
   char       is_day[NULL_DAY],
   const char is_usage_type[NULL_CODE],
   char       os_total_seq_no[NULL_BIG_SEQ_NO],
   char       os_total_value[NULL_SIGNED_AMT],
   char       os_rev_no[NULL_VALUE_REV_NO],
   char       os_total_type[NULL_TOTAL_TYPE],
   char       os_total_id[NULL_TOTAL_ID],
   char       os_ref_no1[NULL_REF_NO],
   char       os_ref_no2[NULL_REF_NO],
   char       os_ref_no3[NULL_REF_NO],
   char       os_status[NULL_CODE],
   char       os_sign[NULL_CODE],
   unsigned int ii_max_counter,
   long       il_multiplier);

/* Assumption: The usage_type is the system_code with a '%' appended to the end */
int getTotalLike(
   char       is_store_day_seq_no[NULL_BIG_SEQ_NO],
   char       is_store[NULL_STORE],
   char       is_day[NULL_DAY],
   char       ios_usage_type[NULL_CODE],
   char       os_total_seq_no[NULL_BIG_SEQ_NO],
   char       os_total_value[NULL_SIGNED_AMT],
   char       os_rev_no[NULL_VALUE_REV_NO],
   char       os_total_type[NULL_TOTAL_TYPE],
   char       os_total_id[NULL_TOTAL_ID],
   char       os_ref_no1[NULL_REF_NO],
   char       os_ref_no2[NULL_REF_NO],
   char       os_ref_no3[NULL_REF_NO],
   char       os_status[NULL_CODE],
   char       os_sign[NULL_CODE],
   unsigned int ii_max_counter,
   long       il_multiplier);

/* Assumption: The usage_type is the same as the system_code               */
int getBalTotal(
   char       is_store_day_seq_no[NULL_BIG_SEQ_NO],
   char       is_store[NULL_STORE],
   char       is_day[NULL_DAY],
   const char is_usage_type[NULL_CODE],
   char       os_total_seq_no[NULL_BIG_SEQ_NO],
   char       os_register_id[NULL_REGISTER],
   char       os_cashier_id[NULL_EMP_ID],
   char       os_employee_id[NULL_EMP_ID],
   char       os_total_value[NULL_SIGNED_AMT],
   char       os_rev_no[NULL_VALUE_REV_NO],
   char       os_total_type[NULL_TOTAL_TYPE],
   char       os_total_id[NULL_TOTAL_ID],
   char       os_ref_no1[NULL_REF_NO],
   char       os_ref_no2[NULL_REF_NO],
   char       os_ref_no3[NULL_REF_NO],
   char       os_status[NULL_CODE],
   char       os_sign[NULL_CODE],
   unsigned int ii_max_counter,
   long       il_multiplier);

/*-------------------------------------------------------------------------*/
/* fetchSaSystemOptions                                                    */
/*-------------------------------------------------------------------------*/

int fetchSaSystemOptions(
   pt_sa_system_options *system_options);

/*-------------------------------------------------------------------------*/
/* fetchStoreDayToBeExported                                               */
/*-------------------------------------------------------------------------*/

int fetchStoreDayToBeExported(
   const char *system_code,
   char       *store_day_seq_no,
   char       *store_no,
   char       *day,
   char       *seq_no,
   char       *business_date,
   char       *rowid,
   unsigned int ii_max_counter,
   char       *num_threads,
   char       *thread_val);

/*-------------------------------------------------------------------------*/
/* fetchStoreDayToBeExportedGL                                             */
/*-------------------------------------------------------------------------*/

   int fetchStoreDayToBeExportedGL(
   const char *system_code,
   char       *store_day_seq_no,
   char       *seq_no,
   char       *day,
   char       *store_no,
   char       *business_date,
   char       *set_of_books_id,
   char       *period_name,
   char       *last_update_id,
   char       *rowid,
   unsigned int ii_max_counter,
   char       *num_threads,
   char       *thread_val);

/*-------------------------------------------------------------------------*/
/* fetchStoreDayToBeExportedLike                                           */
/*-------------------------------------------------------------------------*/

int fetchStoreDayToBeExportedLike(
   const char *system_code,
   char       *store_day_seq_no,
   char       *store_no,
   char       *os_day,
   char       *seq_no,
   char       *business_date,
   char       *rowid,
   unsigned int ii_max_counter,
   char       *num_threads,
   char       *thread_val);

/*-------------------------------------------------------------------------*/
/* fetchStoreDayErrorCount                                                    */
/*-------------------------------------------------------------------------*/

int fetchStoreDayErrorCount(
   char       *store_day_seq_no,
   char       *store,
   char       *day,
   const char *system_code,
   long       *count);

/*-------------------------------------------------------------------------*/
/* totalsAvailable                                                         */
/*-------------------------------------------------------------------------*/

int totalsAvailable(
   char *store_day_seq_no);

/*-------------------------------------------------------------------------*/
/* markStoreDayExported                                                    */
/*-------------------------------------------------------------------------*/

int markStoreDayExported(
   char       *store_day_seq_no,
   char       *store,
   char       *day,
   char       *seq_no,
   const char *system_code,
   char       *date_time,
   long        il_max_counter);

/*-------------------------------------------------------------------------*/
/* markTransactionExported                                                 */
/*-------------------------------------------------------------------------*/

int markTransactionExportedInit(
   long il_commit_max_counter);

int markTransactionExported(
   char       *transaction_sign,
   char       *store_day_seq_no,
   char       *store,
   char       *day,
   const char *system_code,
   char       *tran_seq_no,
   char       *rev_no,
   char       *date_time,
   char       *acct_period,
   char       *status,
   char       *exp_date);

int markTransactionExportedFlush(void);

/*-------------------------------------------------------------------------*/
/* markTotalExported                                                       */
/*-------------------------------------------------------------------------*/

int markTotalExportedInit(
   long       il_commit_max_counter);

int markTotalExported(
   char       *transaction_sign,
   char       *store_day_seq_no,
   char       *store,
   char       *day,
   const char *system_code,
   char       *total_seq_no,
   char       *rev_no,
   char       *date_time,
   char       *acct_period,
   char       *status);

int markTotalExportedFlush(void);

/*-------------------------------------------------------------------------*/
/* fetchSysdate                                                            */
/*-------------------------------------------------------------------------*/

int fetchSysdate(
   char *sys_date );

/*-------------------------------------------------------------------------*/
/* fetchVdate                                                              */
/*-------------------------------------------------------------------------*/

int fetchVdate(
   char *vdate );

/*-------------------------------------------------------------------------*/
/* get_retail_type                                                         */
/*-------------------------------------------------------------------------*/
int get_retail_type(
   char is_item[NULL_ITEM],
   char is_store[NULL_STORE],
   char is_vdate[NULL_DATE],
   char os_retail_type[NULL_IND]);

/*-------------------------------------------------------------------------*/
/* fetchvatcalctype                                                         */
/*-------------------------------------------------------------------------*/   

int fetchvatcalctype(
     char loc[ NULL_LOC ],
     char O_vat_calc_type[ 2 ]);
     
     
#endif

/* end */
