
/*=========================================================================

   sabanner.c: banner load and lookup

=========================================================================*/

#include "sagetref.h"

static BANNERDAT *pa_bannertable = NULL; /* memory-mapped banner table */
static size_t pl_bannertablelen = 0;     /* number of items in bannertable */

/* prototypes */
BANNERDAT *banner_lookup(char *is_store, char *is_banner_id);
BANNERDAT *banner_store_lookup(char *is_store);
static int bannercmp(const void *is_s1, const void *is_s2);
static int bannerstorecmp(const void *is_s1, const void *is_s2);
int banner_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
banner_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
BANNERDAT *banner_lookup(char *is_store, char *is_banner_id)
{
   char ls_key[LEN_STORE + LEN_BANNER_ID + 1];
   char ls_fill[NULL_STORE];
   int  li_store_len; /* actual length of store */
   int  li_fill_len; /* number of spaces to be filled */


   if (pl_bannertablelen == 0)
   {
      return(NULL);
   }

   li_store_len = strlen(is_store);

   memcpy(ls_key, is_store,  LEN_STORE);

   if (li_store_len < LEN_STORE)
   {
      li_fill_len = LEN_STORE - li_store_len;
      memset(ls_fill, ' ',li_fill_len);
      memcpy(ls_key + li_store_len, ls_fill, li_fill_len);
   }

   memcpy(ls_key + LEN_STORE, is_banner_id, LEN_BANNER_ID);

   return((BANNERDAT *)bsearch(
          (const void *)&ls_key, (const void *)pa_bannertable,
           pl_bannertablelen, sizeof(BANNERDAT), bannercmp));
} /* end banner_lookup() */

/*-------------------------------------------------------------------------
banner_store_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
Searches by store only.
-------------------------------------------------------------------------*/
BANNERDAT *banner_store_lookup(char *is_store)
{
   BANNERDAT lr_banner_store_key = {0};
   char ls_fill[NULL_STORE];
   int  li_store_len;         /* actual length of store */
   int  li_fill_len;          /* number of spaces to be filled */

   if (pl_bannertablelen == 0)
   {
      return(NULL);
   }

   li_store_len = strlen(is_store);

   /* Create a key with only store */
   strncpy(lr_banner_store_key.store, is_store, LEN_STORE);
   if (li_store_len < LEN_STORE)
   {
      li_fill_len = LEN_STORE - li_store_len;
      memset(ls_fill, ' ',li_fill_len);
      memcpy(lr_banner_store_key.store + li_store_len, ls_fill, li_fill_len);
   }

   return((BANNERDAT *)bsearch(
          (const void *)&lr_banner_store_key, (const void *)pa_bannertable,
           pl_bannertablelen, sizeof(BANNERDAT), bannerstorecmp));
} /* end banner_store_lookup() */

/*-------------------------------------------------------------------------
bannercmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int bannercmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_STORE + LEN_BANNER_ID));
} /* end bannercmp() */

/*-------------------------------------------------------------------------
bannerstorecmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int bannerstorecmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_STORE));
} /* end bannerstorecmp() */

/*-------------------------------------------------------------------------
banner_loadfile() loads the banner data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int banner_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_bannertable = (BANNERDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                        MAP_SHARED, li_datafile, 0 );
            if(pa_bannertable != (BANNERDAT *)MAP_FAILED)
            {
               pl_bannertablelen = lr_statbuf.st_size / sizeof(BANNERDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	     {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end banner_loadfile() */


