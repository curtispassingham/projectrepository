/*=========================================================================

   sacurrency.c: currency load and lookup

=========================================================================*/

#include "sagetref.h"

static CURRENCYDAT *pa_currencytable = NULL;  /* memory-mapped currency table */
static size_t pl_currencytablelen = 0;   /* number of items in currencytable */

/* prototypes */
CURRENCYDAT *currency_lookup(char *is_key);
static int currencycmp(const void *is_s1, const void *is_s2);
int currency_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
currency_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
CURRENCYDAT *currency_lookup(char *is_key)
{
   if (pl_currencytablelen == 0)
   {
      return(NULL);
   }

   return((CURRENCYDAT *)bsearch(
          (const void *)is_key, (const void *)pa_currencytable,
           pl_currencytablelen, sizeof(CURRENCYDAT), currencycmp));
} /* end currency_lookup() */


/*-------------------------------------------------------------------------
currencycmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int currencycmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_CURRENCY_CODE));
} /* end currencycmp() */


/*-------------------------------------------------------------------------
currency_loadfile() loads the currency data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int currency_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_currencytable = (CURRENCYDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                                MAP_SHARED, li_datafile, 0 );
            if(pa_currencytable != (CURRENCYDAT *)MAP_FAILED)
            {
               pl_currencytablelen = lr_statbuf.st_size / sizeof(CURRENCYDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
    {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end currency_loadfile() */


