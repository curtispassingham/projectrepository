/* sa_valid_date( char *p ) */
/* returns 0 if good, 1 if bad */

#include <retek.h>
#include "salib.h"

#define MIN_YEAR    1900      /* years less than this invalid */
#define MAX_YEAR    2100      /* years greater than this invalid */

int sa_valid_date(char *is_p, int ii_len)
{
     int la_monlen[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
     int li_year, li_mon, li_day, li_hour, li_min, li_sec;

     if(ii_len != DATESIZE && ii_len != DATETIMESIZE)
     {
        return(1);
     } /* end if */

     li_year = atoin(&is_p[0], 4);
     if(li_year < MIN_YEAR || li_year > MAX_YEAR)
     {
        return(1);
     } /* end if */

     li_mon = atoin(&is_p[4], 2);
     if(li_mon < 1 || li_mon > 12)
     {
        return(1);
     } /* end if */

     if(isleapyear(li_year))
     {
        la_monlen[2] = 29;
     } /* end if */
     else 
     {
        la_monlen[2] = 28;
     } /* end else */

     li_day = atoin(&is_p[6], 2);
     if(li_day < 1 || li_day > la_monlen[li_mon])
     {
        return(1);
     } /* end if */

     /* see if time is present */
     if(ii_len == DATETIMESIZE)
     {
        li_hour = atoin(&is_p[DATESIZE + 0], 2);
        if(li_hour < 0 || li_hour > 23)
	{
           return(1);
	} /* end if */

        li_min = atoin(&is_p[DATESIZE + 2], 2);
        if(li_min < 0 || li_min > 59)
	{
           return(1);
	} /* end if */

        li_sec = atoin(&is_p[DATESIZE + 4], 2);
        if(li_sec < 0 || li_sec > 59)
	{
           return(1);
	} /* end if */
     } /* end if */

     /* must be good */
     return(0);
}


