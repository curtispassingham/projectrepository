
/* This is an automatically generated file. DO NOT EDIT. */
/* To make changes, update the code_head and code_detail tables. */
/* Then run: make -f retek.mk sacodes.c sacodes.h */

#ifndef _CODE_TYPES_H
#define _CODE_TYPES_H

#include "sastdlen.h"

/* AAIL = Admin API Action List */
extern const char AAIL[NULL_CODE];

extern const char AAIL_D     [NULL_CODE];   /* Download */
extern const char AAIL_U     [NULL_CODE];   /* Upload */

#define AAILTT_D        1   /* Download */
#define AAILTT_U        2   /* Upload */

#define _AAILTT_MAX 2


/* AALC = Indicates when the deal component should */
extern const char AALC[NULL_CODE];

extern const char AALC_O     [NULL_CODE];   /* PO approval */
extern const char AALC_R     [NULL_CODE];   /* Receiving */

#define AALCTT_O        1   /* PO approval */
#define AALCTT_R        2   /* Receiving */

#define _AALCTT_MAX 2


/* AATC = Admin API Template Category */
extern const char AATC[NULL_CODE];

extern const char AATC_RSAT  [NULL_CODE];   /* ReSA */
extern const char AATC_RMST  [NULL_CODE];   /* RMS */

#define AATCTT_RSAT     1   /* ReSA */
#define AATCTT_RMST     2   /* RMS */

#define _AATCTT_MAX 2


/* ACRJ = Status (Accepted, Rejected) */
extern const char ACRJ[NULL_CODE];

extern const char ACRJ_A     [NULL_CODE];   /* Accepted */
extern const char ACRJ_R     [NULL_CODE];   /* Rejected */

#define ACRJTT_A        1   /* Accepted */
#define ACRJTT_R        2   /* Rejected */

#define _ACRJTT_MAX 2


/* ACT2 = Mode with view all */
extern const char ACT2[NULL_CODE];

extern const char ACT2_NEW   [NULL_CODE];   /* New */
extern const char ACT2_VIEW  [NULL_CODE];   /* View */
extern const char ACT2_EDIT  [NULL_CODE];   /* Edit */
extern const char ACT2_V_AL  [NULL_CODE];   /* View All */

#define ACT2TT_NEW      1   /* New */
#define ACT2TT_VIEW     2   /* View */
#define ACT2TT_EDIT     3   /* Edit */
#define ACT2TT_V_AL     4   /* View All */

#define _ACT2TT_MAX 4


/* ACT3 = Item List actions */
extern const char ACT3[NULL_CODE];

extern const char ACT3_NEW   [NULL_CODE];   /* New */
extern const char ACT3_VIEW  [NULL_CODE];   /* View */
extern const char ACT3_EDIT  [NULL_CODE];   /* Edit */
extern const char ACT3_NEWE  [NULL_CODE];   /* Create from Existing */
extern const char ACT3_USE   [NULL_CODE];   /* Use */

#define ACT3TT_NEW      1   /* New */
#define ACT3TT_VIEW     2   /* View */
#define ACT3TT_EDIT     3   /* Edit */
#define ACT3TT_NEWE     4   /* Create from Existing */
#define ACT3TT_USE      5   /* Use */

#define _ACT3TT_MAX 5


/* ACT4 = Stock Count Actions */
extern const char ACT4[NULL_CODE];

extern const char ACT4_NEW   [NULL_CODE];   /* New */
extern const char ACT4_VIEW  [NULL_CODE];   /* View */
extern const char ACT4_EDIT  [NULL_CODE];   /* Edit */
extern const char ACT4_UVA   [NULL_CODE];   /* Unit Variance and Adjustment */
extern const char ACT4_DV    [NULL_CODE];   /* Dollars View */
extern const char ACT4_DCLA  [NULL_CODE];   /* Dollars Change/Ledger Adjust */
extern const char ACT4_VWHD  [NULL_CODE];   /* Virtual Warehouse Distribution */

#define ACT4TT_NEW      1   /* New */
#define ACT4TT_VIEW     2   /* View */
#define ACT4TT_EDIT     3   /* Edit */
#define ACT4TT_UVA      4   /* Unit Variance and Adjustment */
#define ACT4TT_DV       5   /* Dollars View */
#define ACT4TT_DCLA     6   /* Dollars Change/Ledger Adjust */
#define ACT4TT_VWHD     7   /* Virtual Warehouse Distribution */

#define _ACT4TT_MAX 7


/* ACTN = Mode */
extern const char ACTN[NULL_CODE];

extern const char ACTN_NEW   [NULL_CODE];   /* New */
extern const char ACTN_VIEW  [NULL_CODE];   /* View */
extern const char ACTN_EDIT  [NULL_CODE];   /* Edit */

#define ACTNTT_NEW      1   /* New */
#define ACTNTT_VIEW     2   /* View */
#define ACTNTT_EDIT     3   /* Edit */

#define _ACTNTT_MAX 3


/* ACTP = Activity Cost Type */
extern const char ACTP[NULL_CODE];

extern const char ACTP_U     [NULL_CODE];   /* Update Inventory Cost */
extern const char ACTP_P     [NULL_CODE];   /* Post To Financials */

#define ACTPTT_U        1   /* Update Inventory Cost */
#define ACTPTT_P        2   /* Post To Financials */

#define _ACTPTT_MAX 2


/* ACTT = Action Type */
extern const char ACTT[NULL_CODE];

extern const char ACTT_A     [NULL_CODE];   /* Add */
extern const char ACTT_D     [NULL_CODE];   /* Delete */

#define ACTTTT_A        1   /* Add */
#define ACTTTT_D        2   /* Delete */

#define _ACTTTT_MAX 2


/* ACTY = Stock Count Action Type */
extern const char ACTY[NULL_CODE];

extern const char ACTY_R     [NULL_CODE];   /* Request */
extern const char ACTY_C     [NULL_CODE];   /* Count Quantity */

#define ACTYTT_R        1   /* Request */
#define ACTYTT_C        2   /* Count Quantity */

#define _ACTYTT_MAX 2


/* ADJT = Receipt Type */
extern const char ADJT[NULL_CODE];

extern const char ADJT_R     [NULL_CODE];   /* Receipt */
extern const char ADJT_A     [NULL_CODE];   /* Adjustment */

#define ADJTTT_R        1   /* Receipt */
#define ADJTTT_A        2   /* Adjustment */

#define _ADJTTT_MAX 2


/* ADTP = Price Adjustment Type */
extern const char ADTP[NULL_CODE];

extern const char ADTP_PP    [NULL_CODE];   /* Price Point */
extern const char ADTP_RO    [NULL_CODE];   /* Round */
extern const char ADTP_EI    [NULL_CODE];   /* Ends In */
extern const char ADTP_NO    [NULL_CODE];   /* None */

#define ADTPTT_PP       1   /* Price Point */
#define ADTPTT_RO       2   /* Round */
#define ADTPTT_EI       3   /* Ends In */
#define ADTPTT_NO       4   /* None */

#define _ADTPTT_MAX 4


/* AGS = OI Report Option Area/Grade/Store */
extern const char AGS[NULL_CODE];

extern const char AGS_A     [NULL_CODE];   /* Area */
extern const char AGS_S     [NULL_CODE];   /* Store */
extern const char AGS_G     [NULL_CODE];   /* Grade */

#define AGSTT_A        1   /* Area */
#define AGSTT_S        2   /* Store */
#define AGSTT_G        3   /* Grade */

#define _AGSTT_MAX 3


/* AIPT = AIP Case Types */
extern const char AIPT[NULL_CODE];

extern const char AIPT_F     [NULL_CODE];   /* Formal */
extern const char AIPT_I     [NULL_CODE];   /* Informal */

#define AIPTTT_F        1   /* Formal */
#define AIPTTT_I        2   /* Informal */

#define _AIPTTT_MAX 2


/* AJTY = Type of Adjustment */
extern const char AJTY[NULL_CODE];

extern const char AJTY_P     [NULL_CODE];   /* Purchase Cost */
extern const char AJTY_U     [NULL_CODE];   /* Unit Cost */
extern const char AJTY_R     [NULL_CODE];   /* Retail Price */
extern const char AJTY_M     [NULL_CODE];   /* Multi-Unit Retail */
extern const char AJTY_S     [NULL_CODE];   /* Single Unit Markup % of Retail */

#define AJTYTT_P        1   /* Purchase Cost */
#define AJTYTT_U        2   /* Unit Cost */
#define AJTYTT_R        3   /* Retail Price */
#define AJTYTT_M        4   /* Multi-Unit Retail */
#define AJTYTT_S        5   /* Single Unit Markup % of Retail */

#define _AJTYTT_MAX 5


/* ALCG = ALC Status General */
extern const char ALCG[NULL_CODE];

extern const char ALCG_P     [NULL_CODE];   /* Pending */
extern const char ALCG_R     [NULL_CODE];   /* Processed */

#define ALCGTT_P        1   /* Pending */
#define ALCGTT_R        2   /* Processed */

#define _ALCGTT_MAX 2


/* ALCO = ALC STATUS ALCORDFN */
extern const char ALCO[NULL_CODE];

extern const char ALCO_E     [NULL_CODE];   /* Estimated */
extern const char ALCO_N     [NULL_CODE];   /* No Finalization */
extern const char ALCO_P     [NULL_CODE];   /* Pending */
extern const char ALCO_F     [NULL_CODE];   /* Processed */
extern const char ALCO_M     [NULL_CODE];   /* Processed - Records Pending */

#define ALCOTT_E        1   /* Estimated */
#define ALCOTT_N        2   /* No Finalization */
#define ALCOTT_P        3   /* Pending */
#define ALCOTT_F        4   /* Processed */
#define ALCOTT_M        5   /* Processed - Records Pending */

#define _ALCOTT_MAX 5


/* ALCS = ALC Status */
extern const char ALCS[NULL_CODE];

extern const char ALCS_E     [NULL_CODE];   /* Estimated */
extern const char ALCS_N     [NULL_CODE];   /* No Finalization */
extern const char ALCS_P     [NULL_CODE];   /* Pending */
extern const char ALCS_PW    [NULL_CODE];   /* Processed */

#define ALCSTT_E        1   /* Estimated */
#define ALCSTT_N        2   /* No Finalization */
#define ALCSTT_P        3   /* Pending */
#define ALCSTT_PW       4   /* Processed */

#define _ALCSTT_MAX 4


/* ALLL = All stores */
extern const char ALLL[NULL_CODE];

extern const char ALLL_S     [NULL_CODE];   /* All Stores */

#define ALLLTT_S        1   /* All Stores */

#define _ALLLTT_MAX 1


/* ALMT = Allocation Method */
extern const char ALMT[NULL_CODE];

extern const char ALMT_A     [NULL_CODE];   /* Allocation Quantity */
extern const char ALMT_P     [NULL_CODE];   /* Prorate */
extern const char ALMT_C     [NULL_CODE];   /* Custom */

#define ALMTTT_A        1   /* Allocation Quantity */
#define ALMTTT_P        2   /* Prorate */
#define ALMTTT_C        3   /* Custom */

#define _ALMTTT_MAX 3


/* ALST = Allocation Status */
extern const char ALST[NULL_CODE];

extern const char ALST_W     [NULL_CODE];   /* Worksheet */
extern const char ALST_A     [NULL_CODE];   /* Approved */
extern const char ALST_E     [NULL_CODE];   /* Extracted */
extern const char ALST_C     [NULL_CODE];   /* Closed */
extern const char ALST_R     [NULL_CODE];   /* Reserved */

#define ALSTTT_W        1   /* Worksheet */
#define ALSTTT_A        2   /* Approved */
#define ALSTTT_E        3   /* Extracted */
#define ALSTTT_C        4   /* Closed */
#define ALSTTT_R        5   /* Reserved */

#define _ALSTTT_MAX 5


/* ALTY = Allocation Type */
extern const char ALTY[NULL_CODE];

extern const char ALTY_0     [NULL_CODE];   /* Replenishment */
extern const char ALTY_1     [NULL_CODE];   /* Allocation */

#define ALTYTT_0        1   /* Replenishment */
#define ALTYTT_1        2   /* Allocation */

#define _ALTYTT_MAX 2


/* AMTH = Apply Methods */
extern const char AMTH[NULL_CODE];

extern const char AMTH_A     [NULL_CODE];   /* Single Assessment */
extern const char AMTH_H     [NULL_CODE];   /* All HTS Components */

#define AMTHTT_A        1   /* Single Assessment */
#define AMTHTT_H        2   /* All HTS Components */

#define _AMTHTT_MAX 2


/* AMTY = Amount Type */
extern const char AMTY[NULL_CODE];

extern const char AMTY_G     [NULL_CODE];   /* Gross */
extern const char AMTY_N     [NULL_CODE];   /* Net */

#define AMTYTT_G        1   /* Gross */
#define AMTYTT_N        2   /* Net */

#define _AMTYTT_MAX 2


/* ANST = Analytics (Store or All Stores) list box */
extern const char ANST[NULL_CODE];

extern const char ANST_1     [NULL_CODE];   /* Store */
extern const char ANST_2     [NULL_CODE];   /* All Stores */

#define ANSTTT_1        1   /* Store */
#define ANSTTT_2        2   /* All Stores */

#define _ANSTTT_MAX 2


/* APCC = A&P Constraint Code */
extern const char APCC[NULL_CODE];

extern const char APCC_TBD   [NULL_CODE];   /* TBD */

#define APCCTT_TBD      1   /* TBD */

#define _APCCTT_MAX 1


/* APOR = Apromsku Order Indicator */
extern const char APOR[NULL_CODE];

extern const char APOR_1     [NULL_CODE];   /* Best to Worst */
extern const char APOR_NEG1  [NULL_CODE];   /* Worst to Best */

#define APORTT_1        1   /* Best to Worst */
#define APORTT_NEG1     2   /* Worst to Best */

#define _APORTT_MAX 2


/* APPA = Allowance Accounts payable */
extern const char APPA[NULL_CODE];

extern const char APPA_OI    [NULL_CODE];   /* Off Invoice */
extern const char APPA_DM    [NULL_CODE];   /* Debit Memo */
extern const char APPA_IV    [NULL_CODE];   /* Invoice */
extern const char APPA_SP    [NULL_CODE];   /* Supplier Auto-Pay */

#define APPATT_OI       1   /* Off Invoice */
#define APPATT_DM       2   /* Debit Memo */
#define APPATT_IV       3   /* Invoice */
#define APPATT_SP       4   /* Supplier Auto-Pay */

#define _APPATT_MAX 4


/* APPB = Bracket Accounts payable */
extern const char APPB[NULL_CODE];

extern const char APPB_BB    [NULL_CODE];   /* Bill Back */
extern const char APPB_OI    [NULL_CODE];   /* Off Invoice */

#define APPBTT_BB       1   /* Bill Back */
#define APPBTT_OI       2   /* Off Invoice */

#define _APPBTT_MAX 2


/* APPC = All Accounts payable */
extern const char APPC[NULL_CODE];

extern const char APPC_BB    [NULL_CODE];   /* Bill Back */
extern const char APPC_OI    [NULL_CODE];   /* Off Invoice */
extern const char APPC_DM    [NULL_CODE];   /* Debit Memo */
extern const char APPC_IV    [NULL_CODE];   /* Invoice */
extern const char APPC_SP    [NULL_CODE];   /* Supplier Auto-Pay */

#define APPCTT_BB       1   /* Bill Back */
#define APPCTT_OI       2   /* Off Invoice */
#define APPCTT_DM       3   /* Debit Memo */
#define APPCTT_IV       4   /* Invoice */
#define APPCTT_SP       5   /* Supplier Auto-Pay */

#define _APPCTT_MAX 5


/* APST = A&P Supplier Type */
extern const char APST[NULL_CODE];

extern const char APST_W     [NULL_CODE];   /* Warehouse */
extern const char APST_DSD   [NULL_CODE];   /* Direct Store */
extern const char APST_VMI   [NULL_CODE];   /* VMI */

#define APSTTT_W        1   /* Warehouse */
#define APSTTT_DSD      2   /* Direct Store */
#define APSTTT_VMI      3   /* VMI */

#define _APSTTT_MAX 3


/* APTS = Appointment Status */
extern const char APTS[NULL_CODE];

extern const char APTS_SC    [NULL_CODE];   /* Scheduled */
extern const char APTS_MS    [NULL_CODE];   /* Modified */
extern const char APTS_AR    [NULL_CODE];   /* Arrived */
extern const char APTS_AC    [NULL_CODE];   /* Closed */

#define APTSTT_SC       1   /* Scheduled */
#define APTSTT_MS       2   /* Modified */
#define APTSTT_AR       3   /* Arrived */
#define APTSTT_AC       4   /* Closed */

#define _APTSTT_MAX 4


/* APVS = A&P VMI Status */
extern const char APVS[NULL_CODE];

extern const char APVS_T     [NULL_CODE];   /* Test */
extern const char APVS_P     [NULL_CODE];   /* Production */

#define APVSTT_T        1   /* Test */
#define APVSTT_P        2   /* Production */

#define _APVSTT_MAX 2


/* ARCC = Accepted Rejected Cost Change */
extern const char ARCC[NULL_CODE];

extern const char ARCC_Y     [NULL_CODE];   /* Accepted */
extern const char ARCC_N     [NULL_CODE];   /* Rejected */

#define ARCCTT_Y        1   /* Accepted */
#define ARCCTT_N        2   /* Rejected */

#define _ARCCTT_MAX 2


/* ARST = Auto Receive Store */
extern const char ARST[NULL_CODE];

extern const char ARST_Y     [NULL_CODE];   /* Yes */
extern const char ARST_N     [NULL_CODE];   /* No */
extern const char ARST_D     [NULL_CODE];   /* System Default */

#define ARSTTT_Y        1   /* Yes */
#define ARSTTT_N        2   /* No */
#define ARSTTT_D        3   /* System Default */

#define _ARSTTT_MAX 3


/* ARUG = ARI User Group Labels */
extern const char ARUG[NULL_CODE];

extern const char ARUG_PMMUG [NULL_CODE];   /* Multiple Users/Groups */
extern const char ARUG_PMMG  [NULL_CODE];   /* Multiple Groups */
extern const char ARUG_LKU   [NULL_CODE];   /* Users */
extern const char ARUG_LKG   [NULL_CODE];   /* Groups */
extern const char ARUG_LKUG  [NULL_CODE];   /* Users and Groups */
extern const char ARUG_LKAG  [NULL_CODE];   /* Available Groups */
extern const char ARUG_LKAUG [NULL_CODE];   /* Available Users and Groups */
extern const char ARUG_LKARGU[NULL_CODE];   /* Add/Remove Groups to/from User/Group */
extern const char ARUG_LKARUG[NULL_CODE];   /* Add/Remove Users/Groups to/from Group */
extern const char ARUG_LKGNAM[NULL_CODE];   /* Group Name */
extern const char ARUG_LKGPTN[NULL_CODE];   /* Parameter Type Name */
extern const char ARUG_USUID [NULL_CODE];   /* User ID */
extern const char ARUG_USUNAM[NULL_CODE];   /* User Name */
extern const char ARUG_GPGNAM[NULL_CODE];   /* Group Name */
extern const char ARUG_GPGTYP[NULL_CODE];   /* Group Type */
extern const char ARUG_GPPTN [NULL_CODE];   /* Parameter Type Name */
extern const char ARUG_GPEVTG[NULL_CODE];   /* Event Target */

#define ARUGTT_PMMUG    1   /* Multiple Users/Groups */
#define ARUGTT_PMMG     2   /* Multiple Groups */
#define ARUGTT_LKU      3   /* Users */
#define ARUGTT_LKG      4   /* Groups */
#define ARUGTT_LKUG     5   /* Users and Groups */
#define ARUGTT_LKAG     6   /* Available Groups */
#define ARUGTT_LKAUG    7   /* Available Users and Groups */
#define ARUGTT_LKARGU   8   /* Add/Remove Groups to/from User/Group */
#define ARUGTT_LKARUG   9   /* Add/Remove Users/Groups to/from Group */
#define ARUGTT_LKGNAM  10   /* Group Name */
#define ARUGTT_LKGPTN  11   /* Parameter Type Name */
#define ARUGTT_USUID   12   /* User ID */
#define ARUGTT_USUNAM  13   /* User Name */
#define ARUGTT_GPGNAM  14   /* Group Name */
#define ARUGTT_GPGTYP  15   /* Group Type */
#define ARUGTT_GPPTN   16   /* Parameter Type Name */
#define ARUGTT_GPEVTG  17   /* Event Target */

#define _ARUGTT_MAX 17


/* ATYP = Activity Type */
extern const char ATYP[NULL_CODE];

extern const char ATYP_A     [NULL_CODE];   /* Activity */
extern const char ATYP_T     [NULL_CODE];   /* Template */

#define ATYPTT_A        1   /* Activity */
#define ATYPTT_T        2   /* Template */

#define _ATYPTT_MAX 2


/* BCTP = Barcode type for coupon in POS config */
extern const char BCTP[NULL_CODE];

extern const char BCTP_E     [NULL_CODE];   /* EAN13 */
extern const char BCTP_F     [NULL_CODE];   /* Free Text */

#define BCTPTT_E        1   /* EAN13 */
#define BCTPTT_F        2   /* Free Text */

#define _BCTPTT_MAX 2


/* BKHL = Code type for backhaul allowances */
extern const char BKHL[NULL_CODE];

extern const char BKHL_C     [NULL_CODE];   /* Calculated */
extern const char BKHL_F     [NULL_CODE];   /* Flat Fee */

#define BKHLTT_C        1   /* Calculated */
#define BKHLTT_F        2   /* Flat Fee */

#define _BKHLTT_MAX 2


/* BLAC = Bill of Lading Action List Box */
extern const char BLAC[NULL_CODE];

extern const char BLAC_VIEW  [NULL_CODE];   /* View */
extern const char BLAC_EDIT  [NULL_CODE];   /* Edit */

#define BLACTT_VIEW     1   /* View */
#define BLACTT_EDIT     2   /* Edit */

#define _BLACTT_MAX 2


/* BLMU = Multiple BOL/AWB */
extern const char BLMU[NULL_CODE];

extern const char BLMU_MULTI [NULL_CODE];   /* Multiple BOL/AWB */

#define BLMUTT_MULTI    1   /* Multiple BOL/AWB */

#define _BLMUTT_MAX 1


/* BLST = Bill of Lading Status */
extern const char BLST[NULL_CODE];

extern const char BLST_0     [NULL_CODE];   /* Open */
extern const char BLST_1     [NULL_CODE];   /* Closed */

#define BLSTTT_0        1   /* Open */
#define BLSTTT_1        2   /* Closed */

#define _BLSTTT_MAX 2


/* BNTP = Button Type */
extern const char BNTP[NULL_CODE];

extern const char BNTP_U     [NULL_CODE];   /* Multi Selection */
extern const char BNTP_S     [NULL_CODE];   /* Single Selection */
extern const char BNTP_I     [NULL_CODE];   /* Single Item */
extern const char BNTP_M     [NULL_CODE];   /* Major Category */
extern const char BNTP_N     [NULL_CODE];   /* Minor Category */
extern const char BNTP_X     [NULL_CODE];   /* No Button */

#define BNTPTT_U        1   /* Multi Selection */
#define BNTPTT_S        2   /* Single Selection */
#define BNTPTT_I        3   /* Single Item */
#define BNTPTT_M        4   /* Major Category */
#define BNTPTT_N        5   /* Minor Category */
#define BNTPTT_X        6   /* No Button */

#define _BNTPTT_MAX 6


/* BOLB = Blanket Order Date Labels */
extern const char BOLB[NULL_CODE];

extern const char BOLB_SD    [NULL_CODE];   /* Start Date */
extern const char BOLB_SM    [NULL_CODE];   /* Start Month */
extern const char BOLB_ED    [NULL_CODE];   /* End Date */
extern const char BOLB_EM    [NULL_CODE];   /* End Month */

#define BOLBTT_SD       1   /* Start Date */
#define BOLBTT_SM       2   /* Start Month */
#define BOLBTT_ED       3   /* End Date */
#define BOLBTT_EM       4   /* End Month */

#define _BOLBTT_MAX 4


/* BOLF = Bill of Lading From Location */
extern const char BOLF[NULL_CODE];

extern const char BOLF_S     [NULL_CODE];   /* From Store */
extern const char BOLF_W     [NULL_CODE];   /* From Warehouse */

#define BOLFTT_S        1   /* From Store */
#define BOLFTT_W        2   /* From Warehouse */

#define _BOLFTT_MAX 2


/* BOLT = Bill of Lading To Location */
extern const char BOLT[NULL_CODE];

extern const char BOLT_S     [NULL_CODE];   /* To Store */
extern const char BOLT_W     [NULL_CODE];   /* To Warehouse */

#define BOLTTT_S        1   /* To Store */
#define BOLTTT_W        2   /* To Warehouse */

#define _BOLTTT_MAX 2


/* BOTP = Bond Types */
extern const char BOTP[NULL_CODE];

extern const char BOTP_0     [NULL_CODE];   /* No bond required */
extern const char BOTP_8     [NULL_CODE];   /* Continuous bond */
extern const char BOTP_9     [NULL_CODE];   /* Single transaction bond */

#define BOTPTT_0        1   /* No bond required */
#define BOTPTT_8        2   /* Continuous bond */
#define BOTPTT_9        3   /* Single transaction bond */

#define _BOTPTT_MAX 3


/* BTLT = Bill To Loc Type */
extern const char BTLT[NULL_CODE];

extern const char BTLT_M     [NULL_CODE];   /* Importer */
extern const char BTLT_X     [NULL_CODE];   /* Exporter */
extern const char BTLT_S     [NULL_CODE];   /* Store */
extern const char BTLT_W     [NULL_CODE];   /* Warehouse */
extern const char BTLT_F     [NULL_CODE];   /* @SUH4@ */
extern const char BTLT_C     [NULL_CODE];   /* Costing Location */

#define BTLTTT_M        1   /* Importer */
#define BTLTTT_X        2   /* Exporter */
#define BTLTTT_S        3   /* Store */
#define BTLTTT_W        4   /* Warehouse */
#define BTLTTT_F        5   /* @SUH4@ */
#define BTLTTT_C        6   /* Costing Location */

#define _BTLTTT_MAX 6


/* CACT = POS Coupon Action Listbox */
extern const char CACT[NULL_CODE];

extern const char CACT_NEW   [NULL_CODE];   /* New */
extern const char CACT_VIEW  [NULL_CODE];   /* View */
extern const char CACT_EDIT  [NULL_CODE];   /* Edit */
extern const char CACT_NEWE  [NULL_CODE];   /* Create from Existing */
extern const char CACT_RSEQ  [NULL_CODE];   /* Resequence Coupons */

#define CACTTT_NEW      1   /* New */
#define CACTTT_VIEW     2   /* View */
#define CACTTT_EDIT     3   /* Edit */
#define CACTTT_NEWE     4   /* Create from Existing */
#define CACTTT_RSEQ     5   /* Resequence Coupons */

#define _CACTTT_MAX 5


/* CALC = Normal/454 */
extern const char CALC[NULL_CODE];

extern const char CALC_4     [NULL_CODE];   /* 454 */
extern const char CALC_C     [NULL_CODE];   /* Normal */

#define CALCTT_4        1   /* 454 */
#define CALCTT_C        2   /* Normal */

#define _CALCTT_MAX 2


extern const char CASH_1000  [NULL_CODE];   /* Cash - primary currency */
extern const char CASH_1010  [NULL_CODE];   /* Cash Alternate Currency */
extern const char CASH_1020  [NULL_CODE];   /* Rounding Tender */


/* CASN = Case and Equivalent Names */
extern const char CASN[NULL_CODE];

extern const char CASN_CS    [NULL_CODE];   /* Case */
extern const char CASN_BBL   [NULL_CODE];   /* Barrel */
extern const char CASN_PO    [NULL_CODE];   /* Pot */
extern const char CASN_CT    [NULL_CODE];   /* Carton */
extern const char CASN_CR    [NULL_CODE];   /* Crate */
extern const char CASN_CON   [NULL_CODE];   /* Container */
extern const char CASN_CA    [NULL_CODE];   /* Can */
extern const char CASN_BX    [NULL_CODE];   /* Box */
extern const char CASN_BK    [NULL_CODE];   /* Basket */
extern const char CASN_BA    [NULL_CODE];   /* Barrel */
extern const char CASN_BE    [NULL_CODE];   /* Bundle */
extern const char CASN_BG    [NULL_CODE];   /* Bag */
extern const char CASN_BJ    [NULL_CODE];   /* Bucket */
extern const char CASN_BI    [NULL_CODE];   /* Bin */
extern const char CASN_PACK  [NULL_CODE];   /* Pack */

#define CASNTT_CS       1   /* Case */
#define CASNTT_BBL      2   /* Barrel */
#define CASNTT_PO       3   /* Pot */
#define CASNTT_CT       4   /* Carton */
#define CASNTT_CR       5   /* Crate */
#define CASNTT_CON      6   /* Container */
#define CASNTT_CA       7   /* Can */
#define CASNTT_BX       8   /* Box */
#define CASNTT_BK       9   /* Basket */
#define CASNTT_BA      10   /* Barrel */
#define CASNTT_BE      11   /* Bundle */
#define CASNTT_BG      12   /* Bag */
#define CASNTT_BJ      13   /* Bucket */
#define CASNTT_BI      14   /* Bin */
#define CASNTT_PACK    15   /* Pack */

#define _CASNTT_MAX 15


/* CBAS = Calculation Basis */
extern const char CBAS[NULL_CODE];

extern const char CBAS_V     [NULL_CODE];   /* Value */
extern const char CBAS_S     [NULL_CODE];   /* Specific */

#define CBASTT_V        1   /* Value */
#define CBASTT_S        2   /* Specific */

#define _CBASTT_MAX 2


extern const char CCARD_3000  [NULL_CODE];   /* Visa */
extern const char CCARD_3010  [NULL_CODE];   /* Mastercard */
extern const char CCARD_3020  [NULL_CODE];   /* American Express */
extern const char CCARD_3030  [NULL_CODE];   /* Discover */
extern const char CCARD_3040  [NULL_CODE];   /* Diners Club - N. America */
extern const char CCARD_3045  [NULL_CODE];   /* Diners Club - Non-N. America */
extern const char CCARD_3049  [NULL_CODE];   /* Diners Club - Ancillary */
extern const char CCARD_3050  [NULL_CODE];   /* WEX */
extern const char CCARD_3060  [NULL_CODE];   /* Voyageur */
extern const char CCARD_3070  [NULL_CODE];   /* Unocal */
extern const char CCARD_3080  [NULL_CODE];   /* enRoute */
extern const char CCARD_3090  [NULL_CODE];   /* Japanese Credit Bureau */
extern const char CCARD_3100  [NULL_CODE];   /* Australian Bank Card */
extern const char CCARD_3110  [NULL_CODE];   /* Carte Blanche - N. America */
extern const char CCARD_3115  [NULL_CODE];   /* Carte Blanche - Non-N. America */
extern const char CCARD_3120  [NULL_CODE];   /* House Card */


/* CCAS = Credit Card Authorization Source */
extern const char CCAS[NULL_CODE];

extern const char CCAS_ERR   [NULL_CODE];   /* Error */
extern const char CCAS_E     [NULL_CODE];   /* Electronic */
extern const char CCAS_M     [NULL_CODE];   /* Manual */

#define CCASTT_ERR      1   /* Error */
#define CCASTT_E        2   /* Electronic */
#define CCASTT_M        3   /* Manual */

#define _CCASTT_MAX 3


/* CCEL = Cost Component Exception Log */
extern const char CCEL[NULL_CODE];

extern const char CCEL_ORDS  [NULL_CODE];   /* Order Shipped */
extern const char CCEL_ALLOCS[NULL_CODE];   /* Allocation Shipped */
extern const char CCEL_CEVF  [NULL_CODE];   /* CE Validations failed */
extern const char CCEL_ORDU  [NULL_CODE];   /* Order Updated */
extern const char CCEL_ALLOCU[NULL_CODE];   /* Allocation Updated */

#define CCELTT_ORDS     1   /* Order Shipped */
#define CCELTT_ALLOCS   2   /* Allocation Shipped */
#define CCELTT_CEVF     3   /* CE Validations failed */
#define CCELTT_ORDU     4   /* Order Updated */
#define CCELTT_ALLOCU   5   /* Allocation Updated */

#define _CCELTT_MAX 5


/* CCEM = Credit Card Entry Mode */
extern const char CCEM[NULL_CODE];

extern const char CCEM_ERR   [NULL_CODE];   /* Error */
extern const char CCEM_T     [NULL_CODE];   /* Terminal Used */
extern const char CCEM_M1R   [NULL_CODE];   /* Magnetic Strip One Read */
extern const char CCEM_M1T   [NULL_CODE];   /* Magnetic Strip One Transmitted */
extern const char CCEM_M2R   [NULL_CODE];   /* Magnetic Strip Two Read */
extern const char CCEM_M2T   [NULL_CODE];   /* Magnetic Strip Two Transmitted */
extern const char CCEM_MSR   [NULL_CODE];   /* Magnetic Strip Read */

#define CCEMTT_ERR      1   /* Error */
#define CCEMTT_T        2   /* Terminal Used */
#define CCEMTT_M1R      3   /* Magnetic Strip One Read */
#define CCEMTT_M1T      4   /* Magnetic Strip One Transmitted */
#define CCEMTT_M2R      5   /* Magnetic Strip Two Read */
#define CCEMTT_M2T      6   /* Magnetic Strip Two Transmitted */
#define CCEMTT_MSR      7   /* Magnetic Strip Read */

#define _CCEMTT_MAX 7


/* CCOR = Cost Change Origin */
extern const char CCOR[NULL_CODE];

extern const char CCOR_ITEM  [NULL_CODE];   /* By Item */
extern const char CCOR_SUP   [NULL_CODE];   /* By Supplier */

#define CCORTT_ITEM     1   /* By Item */
#define CCORTT_SUP      2   /* By Supplier */

#define _CCORTT_MAX 2


/* CCSC = Credit Card Special Condition */
extern const char CCSC[NULL_CODE];

extern const char CCSC_ERR   [NULL_CODE];   /* Error */
extern const char CCSC_M     [NULL_CODE];   /* Mail */
extern const char CCSC_P     [NULL_CODE];   /* Phone */
extern const char CCSC_E     [NULL_CODE];   /* Electronic-secured */
extern const char CCSC_N     [NULL_CODE];   /* Electronic-non-secured */

#define CCSCTT_ERR      1   /* Error */
#define CCSCTT_M        2   /* Mail */
#define CCSCTT_P        3   /* Phone */
#define CCSCTT_E        4   /* Electronic-secured */
#define CCSCTT_N        5   /* Electronic-non-secured */

#define _CCSCTT_MAX 5


/* CCSL = Credit Card Security Level */
extern const char CCSL[NULL_CODE];

extern const char CCSL_N     [NULL_CODE];   /* None */
extern const char CCSL_R     [NULL_CODE];   /* Restricted Access */
extern const char CCSL_B     [NULL_CODE];   /* Blocked */

#define CCSLTT_N        1   /* None */
#define CCSLTT_R        2   /* Restricted Access */
#define CCSLTT_B        3   /* Blocked */

#define _CCSLTT_MAX 3


/* CCTP = Cost Component Type */
extern const char CCTP[NULL_CODE];


/* CCVF = Credit Card Verification */
extern const char CCVF[NULL_CODE];

extern const char CCVF_ERR   [NULL_CODE];   /* Error */
extern const char CCVF_S     [NULL_CODE];   /* Signature Verified */
extern const char CCVF_C     [NULL_CODE];   /* Card Shown */
extern const char CCVF_P     [NULL_CODE];   /* PIN Entered */
extern const char CCVF_M     [NULL_CODE];   /* Mail Order/Phone */
extern const char CCVF_E     [NULL_CODE];   /* e-commerce */

#define CCVFTT_ERR      1   /* Error */
#define CCVFTT_S        2   /* Signature Verified */
#define CCVFTT_C        3   /* Card Shown */
#define CCVFTT_P        4   /* PIN Entered */
#define CCVFTT_M        5   /* Mail Order/Phone */
#define CCVFTT_E        6   /* e-commerce */

#define _CCVFTT_MAX 6


/* CCVT = Credit Card Validation Type */
extern const char CCVT[NULL_CODE];

extern const char CCVT_MOD10 [NULL_CODE];   /* Modulus 10 */
extern const char CCVT_NONE  [NULL_CODE];   /* No validation */

#define CCVTTT_MOD10    1   /* Modulus 10 */
#define CCVTTT_NONE     2   /* No validation */

#define _CCVTTT_MAX 2


/* CDIP = Complex Deal Invoice Processing */
extern const char CDIP[NULL_CODE];

extern const char CDIP_AA    [NULL_CODE];   /* Automatic All Values */
extern const char CDIP_MA    [NULL_CODE];   /* Manual All Values */
extern const char CDIP_AP    [NULL_CODE];   /* Automatic Positive Values Only */
extern const char CDIP_MP    [NULL_CODE];   /* Manual Positive Values Only */
extern const char CDIP_NO    [NULL_CODE];   /* No Invoice Processing */

#define CDIPTT_AA       1   /* Automatic All Values */
#define CDIPTT_MA       2   /* Manual All Values */
#define CDIPTT_AP       3   /* Automatic Positive Values Only */
#define CDIPTT_MP       4   /* Manual Positive Values Only */
#define CDIPTT_NO       5   /* No Invoice Processing */

#define _CDIPTT_MAX 5


/* CEAS = Customs Entry ALC Status */
extern const char CEAS[NULL_CODE];

extern const char CEAS_P     [NULL_CODE];   /* Pending */
extern const char CEAS_A     [NULL_CODE];   /* Allocated */
extern const char CEAS_R     [NULL_CODE];   /* Processed */

#define CEASTT_P        1   /* Pending */
#define CEASTT_A        2   /* Allocated */
#define CEASTT_R        3   /* Processed */

#define _CEASTT_MAX 3


/* CEPT = Customs Entry Partners */
extern const char CEPT[NULL_CODE];

extern const char CEPT_IA    [NULL_CODE];   /* Import Authority */
extern const char CEPT_BR    [NULL_CODE];   /* Broker */

#define CEPTTT_IA       1   /* Import Authority */
#define CEPTTT_BR       2   /* Broker */

#define _CEPTTT_MAX 2


/* CES = Cost Event Status */
extern const char CES[NULL_CODE];

extern const char CES_N     [NULL_CODE];   /* New */
extern const char CES_C     [NULL_CODE];   /* Completed */
extern const char CES_E     [NULL_CODE];   /* Error */
extern const char CES_R     [NULL_CODE];   /* Reprocess */

#define CESTT_N        1   /* New */
#define CESTT_C        2   /* Completed */
#define CESTT_E        3   /* Error */
#define CESTT_R        4   /* Reprocess */

#define _CESTT_MAX 4


/* CEST = CE Ref. ID Status */
extern const char CEST[NULL_CODE];

extern const char CEST_W     [NULL_CODE];   /* Worksheet */
extern const char CEST_S     [NULL_CODE];   /* Send */
extern const char CEST_D     [NULL_CODE];   /* Downloaded */
extern const char CEST_U     [NULL_CODE];   /* Confirmed */

#define CESTTT_W        1   /* Worksheet */
#define CESTTT_S        2   /* Send */
#define CESTTT_D        3   /* Downloaded */
#define CESTTT_U        4   /* Confirmed */

#define _CESTTT_MAX 4


/* CFRG = CFAS Record Group Types */
extern const char CFRG[NULL_CODE];

extern const char CFRG_S     [NULL_CODE];   /* Simple */
extern const char CFRG_C     [NULL_CODE];   /* Complex */

#define CFRGTT_S        1   /* Simple */
#define CFRGTT_C        2   /* Complex */

#define _CFRGTT_MAX 2


/* CFWH = CFAS Where Clause Operators */
extern const char CFWH[NULL_CODE];

extern const char CFWH_EQ    [NULL_CODE];   /* = */
extern const char CFWH_NE    [NULL_CODE];   /* != */
extern const char CFWH_GT    [NULL_CODE];   /* > */
extern const char CFWH_LT    [NULL_CODE];   /* < */
extern const char CFWH_GE    [NULL_CODE];   /* >= */
extern const char CFWH_LE    [NULL_CODE];   /* <= */
extern const char CFWH_NULL  [NULL_CODE];   /* is NULL */
extern const char CFWH_NOTNUL[NULL_CODE];   /* is not NULL */

#define CFWHTT_EQ       1   /* = */
#define CFWHTT_NE       2   /* != */
#define CFWHTT_GT       3   /* > */
#define CFWHTT_LT       4   /* < */
#define CFWHTT_GE       5   /* >= */
#define CFWHTT_LE       6   /* <= */
#define CFWHTT_NULL     7   /* is NULL */
#define CFWHTT_NOTNUL   8   /* is not NULL */

#define _CFWHTT_MAX 8


/* CGS = WF Organizational Level */
extern const char CGS[NULL_CODE];

extern const char CGS_A     [NULL_CODE];   /* @OH3@ */
extern const char CGS_R     [NULL_CODE];   /* @OH4@ */
extern const char CGS_D     [NULL_CODE];   /* @OH5@ */
extern const char CGS_LL    [NULL_CODE];   /* Location List */
extern const char CGS_C     [NULL_CODE];   /* Customer */
extern const char CGS_G     [NULL_CODE];   /* Customer Group */
extern const char CGS_S     [NULL_CODE];   /* Store */

#define CGSTT_A        1   /* @OH3@ */
#define CGSTT_R        2   /* @OH4@ */
#define CGSTT_D        3   /* @OH5@ */
#define CGSTT_LL       4   /* Location List */
#define CGSTT_C        5   /* Customer */
#define CGSTT_G        6   /* Customer Group */
#define CGSTT_S        7   /* Store */

#define _CGSTT_MAX 7


extern const char CHECK_2000  [NULL_CODE];   /* Personal Check */
extern const char CHECK_2010  [NULL_CODE];   /* Cashier Check */
extern const char CHECK_2020  [NULL_CODE];   /* Traveler Check */
extern const char CHECK_2030  [NULL_CODE];   /* Electronic Check */
extern const char CHECK_2040  [NULL_CODE];   /* Mail Bank Check */
extern const char CHECK_2050  [NULL_CODE];   /* Personal Check Alternate Currency */
extern const char CHECK_2060  [NULL_CODE];   /* Travelers Check Alternate Currency */


/* CHTY = Channel Types */
extern const char CHTY[NULL_CODE];

extern const char CHTY_WEBSTR[NULL_CODE];   /* Webstore */
extern const char CHTY_BANDM [NULL_CODE];   /* Brick and Mortar */
extern const char CHTY_CAT   [NULL_CODE];   /* Catalog */

#define CHTYTT_WEBSTR   1   /* Webstore */
#define CHTYTT_BANDM    2   /* Brick and Mortar */
#define CHTYTT_CAT      3   /* Catalog */

#define _CHTYTT_MAX 3


/* CIDT = Sales Audit Customer ID Types */
extern const char CIDT[NULL_CODE];

extern const char CIDT_ERR   [NULL_CODE];   /* Error */
extern const char CIDT_TERM  [NULL_CODE];   /* Termination Record */
extern const char CIDT_PHONE [NULL_CODE];   /* Phone */
extern const char CIDT_DL    [NULL_CODE];   /* Drivers License */
extern const char CIDT_BNKACT[NULL_CODE];   /* Bank Account */
extern const char CIDT_SSN   [NULL_CODE];   /* Social Security */
extern const char CIDT_HSCARD[NULL_CODE];   /* House Card */
extern const char CIDT_VICARD[NULL_CODE];   /* Visa Card */
extern const char CIDT_MCCARD[NULL_CODE];   /* Mastercard */
extern const char CIDT_AXCARD[NULL_CODE];   /* American Express Card */
extern const char CIDT_DSCARD[NULL_CODE];   /* Discover Card */
extern const char CIDT_DCCARD[NULL_CODE];   /* Diners Club Card */
extern const char CIDT_ZIP   [NULL_CODE];   /* Zip Code */
extern const char CIDT_IDCODE[NULL_CODE];   /* Store-defined id code */
extern const char CIDT_CUSTID[NULL_CODE];   /* Customer ID */

#define CIDTTT_ERR      1   /* Error */
#define CIDTTT_TERM     2   /* Termination Record */
#define CIDTTT_PHONE    3   /* Phone */
#define CIDTTT_DL       4   /* Drivers License */
#define CIDTTT_BNKACT   5   /* Bank Account */
#define CIDTTT_SSN      6   /* Social Security */
#define CIDTTT_HSCARD   7   /* House Card */
#define CIDTTT_VICARD   8   /* Visa Card */
#define CIDTTT_MCCARD   9   /* Mastercard */
#define CIDTTT_AXCARD  10   /* American Express Card */
#define CIDTTT_DSCARD  11   /* Discover Card */
#define CIDTTT_DCCARD  12   /* Diners Club Card */
#define CIDTTT_ZIP     13   /* Zip Code */
#define CIDTTT_IDCODE  14   /* Store-defined id code */
#define CIDTTT_CUSTID  15   /* Customer ID */

#define _CIDTTT_MAX 15


/* CIND = Cost Indicator */
extern const char CIND[NULL_CODE];

extern const char CIND_BC    [NULL_CODE];   /* Base Cost */
extern const char CIND_NIC   [NULL_CODE];   /* Negotiated Item Cost */

#define CINDTT_BC       1   /* Base Cost */
#define CINDTT_NIC      2   /* Negotiated Item Cost */

#define _CINDTT_MAX 2


/* CLAS = Class Store */
extern const char CLAS[NULL_CODE];

extern const char CLAS_C     [NULL_CODE];   /* Class Stores */

#define CLASTT_C        1   /* Class Stores */

#define _CLASTT_MAX 1


/* CLBL = Sales Audit Canvas Labels */
extern const char CLBL[NULL_CODE];

extern const char CLBL_R     [NULL_CODE];   /* Realms */
extern const char CLBL_J     [NULL_CODE];   /* Joins */
extern const char CLBL_P     [NULL_CODE];   /* Parameters */
extern const char CLBL_RE    [NULL_CODE];   /* Restrictions */
extern const char CLBL_LT    [NULL_CODE];   /* Location Traits */
extern const char CLBL_CTD   [NULL_CODE];   /* Combined Total Details */
extern const char CLBL_RU    [NULL_CODE];   /* Roll Ups */
extern const char CLBL_U     [NULL_CODE];   /* Usages */
extern const char CLBL_TO    [NULL_CODE];   /* Total Overview */
extern const char CLBL_TC    [NULL_CODE];   /* Total Characteristics */
extern const char CLBL_TCC   [NULL_CODE];   /* Total Characteristics (cont.) */
extern const char CLBL_RO    [NULL_CODE];   /* Rule Overview */
extern const char CLBL_RC    [NULL_CODE];   /* Rule Characteristics */

#define CLBLTT_R        1   /* Realms */
#define CLBLTT_J        2   /* Joins */
#define CLBLTT_P        3   /* Parameters */
#define CLBLTT_RE       4   /* Restrictions */
#define CLBLTT_LT       5   /* Location Traits */
#define CLBLTT_CTD      6   /* Combined Total Details */
#define CLBLTT_RU       7   /* Roll Ups */
#define CLBLTT_U        8   /* Usages */
#define CLBLTT_TO       9   /* Total Overview */
#define CLBLTT_TC      10   /* Total Characteristics */
#define CLBLTT_TCC     11   /* Total Characteristics (cont.) */
#define CLBLTT_RO      12   /* Rule Overview */
#define CLBLTT_RC      13   /* Rule Characteristics */

#define _CLBLTT_MAX 13


/* CLIL = Clearance Item List */
extern const char CLIL[NULL_CODE];

extern const char CLIL_I     [NULL_CODE];   /* Item */
extern const char CLIL_IL    [NULL_CODE];   /* Item List */

#define CLILTT_I        1   /* Item */
#define CLILTT_IL       2   /* Item List */

#define _CLILTT_MAX 2


/* CLMD = Calculation Method */
extern const char CLMD[NULL_CODE];

extern const char CLMD_C     [NULL_CODE];   /* Constant */
extern const char CLMD_F     [NULL_CODE];   /* Floating */
extern const char CLMD_M     [NULL_CODE];   /* Minimum/Maximum */

#define CLMDTT_C        1   /* Constant */
#define CLMDTT_F        2   /* Floating */
#define CLMDTT_M        3   /* Minimum/Maximum */

#define _CLMDTT_MAX 3


/* CLS1 = Clearance Status 1 */
extern const char CLS1[NULL_CODE];

extern const char CLS1_W     [NULL_CODE];   /* Worksheet */
extern const char CLS1_S     [NULL_CODE];   /* Submitted */
extern const char CLS1_A     [NULL_CODE];   /* Approved */
extern const char CLS1_R     [NULL_CODE];   /* Rejected */
extern const char CLS1_O     [NULL_CODE];   /* Complete */
extern const char CLS1_C     [NULL_CODE];   /* Cancelled */
extern const char CLS1_D     [NULL_CODE];   /* Deleted */

#define CLS1TT_W        1   /* Worksheet */
#define CLS1TT_S        2   /* Submitted */
#define CLS1TT_A        3   /* Approved */
#define CLS1TT_R        4   /* Rejected */
#define CLS1TT_O        5   /* Complete */
#define CLS1TT_C        6   /* Cancelled */
#define CLS1TT_D        7   /* Deleted */

#define _CLS1TT_MAX 7


/* CLSF = Item Classification */
extern const char CLSF[NULL_CODE];

extern const char CLSF_A     [NULL_CODE];   /* A */
extern const char CLSF_B     [NULL_CODE];   /* B */
extern const char CLSF_C     [NULL_CODE];   /* C */
extern const char CLSF_D     [NULL_CODE];   /* D */
extern const char CLSF_E     [NULL_CODE];   /* E */

#define CLSFTT_A        1   /* A */
#define CLSFTT_B        2   /* B */
#define CLSFTT_C        3   /* C */
#define CLSFTT_D        4   /* D */
#define CLSFTT_E        5   /* E */

#define _CLSFTT_MAX 5


/* CLST = Clearance Event Status */
extern const char CLST[NULL_CODE];

extern const char CLST_W     [NULL_CODE];   /* Worksheet */
extern const char CLST_S     [NULL_CODE];   /* Submitted */
extern const char CLST_A     [NULL_CODE];   /* Approved */
extern const char CLST_R     [NULL_CODE];   /* Rejected */
extern const char CLST_E     [NULL_CODE];   /* Extracted */
extern const char CLST_O     [NULL_CODE];   /* Completed */
extern const char CLST_C     [NULL_CODE];   /* Cancelled */
extern const char CLST_D     [NULL_CODE];   /* Deleted */

#define CLSTTT_W        1   /* Worksheet */
#define CLSTTT_S        2   /* Submitted */
#define CLSTTT_A        3   /* Approved */
#define CLSTTT_R        4   /* Rejected */
#define CLSTTT_E        5   /* Extracted */
#define CLSTTT_O        6   /* Completed */
#define CLSTTT_C        7   /* Cancelled */
#define CLSTTT_D        8   /* Deleted */

#define _CLSTTT_MAX 8


/* CMPL = Component Type Labels */
extern const char CMPL[NULL_CODE];

extern const char CMPL_E     [NULL_CODE];   /* Expense Type */
extern const char CMPL_A     [NULL_CODE];   /* Assessment Type */

#define CMPLTT_E        1   /* Expense Type */
#define CMPLTT_A        2   /* Assessment Type */

#define _CMPLTT_MAX 2


/* CNAL = Contract Find Action List Box */
extern const char CNAL[NULL_CODE];

extern const char CNAL_NEW   [NULL_CODE];   /* New */
extern const char CNAL_CREATE[NULL_CODE];   /* Create from Existing */
extern const char CNAL_VIEW  [NULL_CODE];   /* View */
extern const char CNAL_EDIT  [NULL_CODE];   /* Edit */

#define CNALTT_NEW      1   /* New */
#define CNALTT_CREATE   2   /* Create from Existing */
#define CNALTT_VIEW     3   /* View */
#define CNALTT_EDIT     4   /* Edit */

#define _CNALTT_MAX 4


/* CNFQ = Consign/Concession POs & invoice level */
extern const char CNFQ[NULL_CODE];

extern const char CNFQ_I     [NULL_CODE];   /* Item/Supplier/Location */
extern const char CNFQ_S     [NULL_CODE];   /* Supplier/@MH4@ */
extern const char CNFQ_L     [NULL_CODE];   /* Supplier/@MH4@/Location */

#define CNFQTT_I        1   /* Item/Supplier/Location */
#define CNFQTT_S        2   /* Supplier/@MH4@ */
#define CNFQTT_L        3   /* Supplier/@MH4@/Location */

#define _CNFQTT_MAX 3


/* CNIV = Consign/Concession POs & invoices */
extern const char CNIV[NULL_CODE];

extern const char CNIV_M     [NULL_CODE];   /* Monthly */
extern const char CNIV_W     [NULL_CODE];   /* Weekly */
extern const char CNIV_P     [NULL_CODE];   /* Multiple */
extern const char CNIV_D     [NULL_CODE];   /* Daily */

#define CNIVTT_M        1   /* Monthly */
#define CNIVTT_W        2   /* Weekly */
#define CNIVTT_P        3   /* Multiple */
#define CNIVTT_D        4   /* Daily */

#define _CNIVTT_MAX 4


/* CNLB = CNTRSKU from tables */
extern const char CNLB[NULL_CODE];

extern const char CNLB_C     [NULL_CODE];   /* Contract Qty */
extern const char CNLB_QR    [NULL_CODE];   /* Qty Recvd */
extern const char CNLB_QO    [NULL_CODE];   /* Qty Ordered */
extern const char CNLB_U     [NULL_CODE];   /* Unit Cost */
extern const char CNLB_P     [NULL_CODE];   /* Packaging Cost */
extern const char CNLB_D     [NULL_CODE];   /* Duty Code */

#define CNLBTT_C        1   /* Contract Qty */
#define CNLBTT_QR       2   /* Qty Recvd */
#define CNLBTT_QO       3   /* Qty Ordered */
#define CNLBTT_U        4   /* Unit Cost */
#define CNLBTT_P        5   /* Packaging Cost */
#define CNLBTT_D        6   /* Duty Code */

#define _CNLBTT_MAX 6


/* CNLC = CNTRLOCS locations type list box */
extern const char CNLC[NULL_CODE];

extern const char CNLC_C     [NULL_CODE];   /* Store Class */
extern const char CNLC_D     [NULL_CODE];   /* @OH5@ */
extern const char CNLC_R     [NULL_CODE];   /* @OH4@ */
extern const char CNLC_S     [NULL_CODE];   /* Store */
extern const char CNLC_W     [NULL_CODE];   /* Warehouse */
extern const char CNLC_T     [NULL_CODE];   /* Transfer Zone */
extern const char CNLC_L     [NULL_CODE];   /* Location Trait */
extern const char CNLC_AS    [NULL_CODE];   /* All Stores */

#define CNLCTT_C        1   /* Store Class */
#define CNLCTT_D        2   /* @OH5@ */
#define CNLCTT_R        4   /* @OH4@ */
#define CNLCTT_S        5   /* Store */
#define CNLCTT_W        6   /* Warehouse */
#define CNLCTT_T        7   /* Transfer Zone */
#define CNLCTT_L        8   /* Location Trait */
#define CNLCTT_AS       9   /* All Stores */

#define _CNLCTT_MAX 9


/* CNTP = Contract Type */
extern const char CNTP[NULL_CODE];

extern const char CNTP_A     [NULL_CODE];   /* Plan / Availability */
extern const char CNTP_B     [NULL_CODE];   /* Plan / No Availability */
extern const char CNTP_C     [NULL_CODE];   /* No Plan / No Availability */
extern const char CNTP_D     [NULL_CODE];   /* No Plan / Availability */

#define CNTPTT_A        1   /* Plan / Availability */
#define CNTPTT_B        2   /* Plan / No Availability */
#define CNTPTT_C        3   /* No Plan / No Availability */
#define CNTPTT_D        4   /* No Plan / Availability */

#define _CNTPTT_MAX 4


/* CNTX = Context type */
extern const char CNTX[NULL_CODE];

extern const char CNTX_PROM  [NULL_CODE];   /* Promotion */
extern const char CNTX_WED   [NULL_CODE];   /* Customer Transfer */
extern const char CNTX_STORE [NULL_CODE];   /* Store Requisition */
extern const char CNTX_REPAIR[NULL_CODE];   /* Repairing */

#define CNTXTT_PROM     1   /* Promotion */
#define CNTXTT_WED      2   /* Customer Transfer */
#define CNTXTT_STORE    3   /* Store Requisition */
#define CNTXTT_REPAIR   4   /* Repairing */

#define _CNTXTT_MAX 4


/* COCS = Contract Order Create Status */
extern const char COCS[NULL_CODE];

extern const char COCS_B     [NULL_CODE];   /* Pending */
extern const char COCS_C     [NULL_CODE];   /* Created */
extern const char COCS_G     [NULL_CODE];   /* Generated */

#define COCSTT_B        1   /* Pending */
#define COCSTT_C        2   /* Created */
#define COCSTT_G        3   /* Generated */

#define _COCSTT_MAX 3


/* COLR = Color Labels */
extern const char COLR[NULL_CODE];

extern const char COLR_C     [NULL_CODE];   /* Color */
extern const char COLR_CR    [NULL_CODE];   /* Color Range */

#define COLRTT_C        1   /* Color */
#define COLRTT_CR       2   /* Color Range */

#define _COLRTT_MAX 2


/* COLT = Collect Type */
extern const char COLT[NULL_CODE];

extern const char COLT_D     [NULL_CODE];   /* Dates */
extern const char COLT_M     [NULL_CODE];   /* Month */
extern const char COLT_Q     [NULL_CODE];   /* Qtr */
extern const char COLT_A     [NULL_CODE];   /* Annual */
extern const char COLT_P     [NULL_CODE];   /* Perf */

#define COLTTT_D        1   /* Dates */
#define COLTTT_M        2   /* Month */
#define COLTTT_Q        3   /* Qtr */
#define COLTTT_A        4   /* Annual */
#define COLTTT_P        5   /* Perf */

#define _COLTTT_MAX 5


/* COND = Condition Type */
extern const char COND[NULL_CODE];

extern const char COND_BC    [NULL_CODE];   /* Base Cost */
extern const char COND_EBC   [NULL_CODE];   /* Extended Base Cost */
extern const char COND_NIC   [NULL_CODE];   /* Negotiated Item Cost */
extern const char COND_IC    [NULL_CODE];   /* Cost Inclusive of All Taxes */

#define CONDTT_BC       1   /* Base Cost */
#define CONDTT_EBC      2   /* Extended Base Cost */
#define CONDTT_NIC      3   /* Negotiated Item Cost */
#define CONDTT_IC       4   /* Cost Inclusive of All Taxes */

#define _CONDTT_MAX 4


/* CONS = Construction Conditions */
extern const char CONS[NULL_CODE];

extern const char CONS_REM   [NULL_CODE];   /* Store Remodel */
extern const char CONS_MAI   [NULL_CODE];   /* Store Maintenance */
extern const char CONS_PAR   [NULL_CODE];   /* Parking Lot */
extern const char CONS_SID   [NULL_CODE];   /* Sidewalk */
extern const char CONS_ROA   [NULL_CODE];   /* Connecting Road */
extern const char CONS_STR   [NULL_CODE];   /* Strike */
extern const char CONS_LAB   [NULL_CODE];   /* Labor Disturbance */

#define CONSTT_REM      1   /* Store Remodel */
#define CONSTT_MAI      2   /* Store Maintenance */
#define CONSTT_PAR      3   /* Parking Lot */
#define CONSTT_SID      4   /* Sidewalk */
#define CONSTT_ROA      5   /* Connecting Road */
#define CONSTT_STR      6   /* Strike */
#define CONSTT_LAB      7   /* Labor Disturbance */

#define _CONSTT_MAX 7


/* CORT = Cost/Retail Flag */
extern const char CORT[NULL_CODE];

extern const char CORT_C     [NULL_CODE];   /* Cost */
extern const char CORT_R     [NULL_CODE];   /* Retail */

#define CORTTT_C        1   /* Cost */
#define CORTTT_R        2   /* Retail */

#define _CORTTT_MAX 2


/* COST = Cost Change Status (plus Extracted) */
extern const char COST[NULL_CODE];

extern const char COST_W     [NULL_CODE];   /* Worksheet */
extern const char COST_S     [NULL_CODE];   /* Submitted */
extern const char COST_A     [NULL_CODE];   /* Approved */
extern const char COST_R     [NULL_CODE];   /* Rejected */
extern const char COST_E     [NULL_CODE];   /* Extracted */
extern const char COST_C     [NULL_CODE];   /* Cancelled */
extern const char COST_D     [NULL_CODE];   /* Deleted */

#define COSTTT_W        1   /* Worksheet */
#define COSTTT_S        2   /* Submitted */
#define COSTTT_A        3   /* Approved */
#define COSTTT_R        4   /* Rejected */
#define COSTTT_E        5   /* Extracted */
#define COSTTT_C        6   /* Cancelled */
#define COSTTT_D        7   /* Deleted */

#define _COSTTT_MAX 7


extern const char COUPON_5000  [NULL_CODE];   /* Manufacturers Coupons */


/* CPCT = Coupon Percentage Indicator */
extern const char CPCT[NULL_CODE];

extern const char CPCT_Y     [NULL_CODE];   /* Percentage */
extern const char CPCT_N     [NULL_CODE];   /* Amount */

#define CPCTTT_Y        1   /* Percentage */
#define CPCTTT_N        2   /* Amount */

#define _CPCTTT_MAX 2


/* CPTM = Capture Time */
extern const char CPTM[NULL_CODE];

extern const char CPTM_S     [NULL_CODE];   /* Sale */
extern const char CPTM_SR    [NULL_CODE];   /* Store Receiving */

#define CPTMTT_S        1   /* Sale */
#define CPTMTT_SR       2   /* Store Receiving */

#define _CPTMTT_MAX 2


/* CPTP = Coupon Report Type */
extern const char CPTP[NULL_CODE];

extern const char CPTP_C     [NULL_CODE];   /* Coupons and Merchandise Criteria */
extern const char CPTP_S     [NULL_CODE];   /* Coupons and Associated Stores */
extern const char CPTP_I     [NULL_CODE];   /* Coupons and Associated Items */

#define CPTPTT_C        1   /* Coupons and Merchandise Criteria */
#define CPTPTT_S        2   /* Coupons and Associated Stores */
#define CPTPTT_I        3   /* Coupons and Associated Items */

#define _CPTPTT_MAX 3


/* CRLB = Currency Form Labels */
extern const char CRLB[NULL_CODE];

extern const char CRLB_C     [NULL_CODE];   /* Consolidation */
extern const char CRLB_CR    [NULL_CODE];   /* Consolidation Rate */
extern const char CRLB_R     [NULL_CODE];   /* Rate */

#define CRLBTT_C        1   /* Consolidation */
#define CRLBTT_CR       2   /* Consolidation Rate */
#define CRLBTT_R        3   /* Rate */

#define _CRLBTT_MAX 3


/* CRLL = Cost Relationship Loc List */
extern const char CRLL[NULL_CODE];

extern const char CRLL_A     [NULL_CODE];   /* @OH3@ */
extern const char CRLL_R     [NULL_CODE];   /* @OH4@ */
extern const char CRLL_D     [NULL_CODE];   /* @OH5@ */
extern const char CRLL_C     [NULL_CODE];   /* Cost Zone Group */
extern const char CRLL_S     [NULL_CODE];   /* Store */
extern const char CRLL_L     [NULL_CODE];   /* Location List Stores */
extern const char CRLL_AS    [NULL_CODE];   /* All Stores */

#define CRLLTT_A        1   /* @OH3@ */
#define CRLLTT_R        2   /* @OH4@ */
#define CRLLTT_D        3   /* @OH5@ */
#define CRLLTT_C        4   /* Cost Zone Group */
#define CRLLTT_S        5   /* Store */
#define CRLLTT_L        6   /* Location List Stores */
#define CRLLTT_AS       7   /* All Stores */

#define _CRLLTT_MAX 7


/* CRML = Credit Memo Level */
extern const char CRML[NULL_CODE];

extern const char CRML_L     [NULL_CODE];   /* Location */
extern const char CRML_T     [NULL_CODE];   /* Transfer Entity */
extern const char CRML_B     [NULL_CODE];   /* Set of Books */
extern const char CRML_D     [NULL_CODE];   /* Deal */

#define CRMLTT_L        1   /* Location */
#define CRMLTT_T        2   /* Transfer Entity */
#define CRMLTT_B        3   /* Set of Books */
#define CRMLTT_D        4   /* Deal */

#define _CRMLTT_MAX 4


/* CRSS = Contract Status */
extern const char CRSS[NULL_CODE];

extern const char CRSS_W     [NULL_CODE];   /* Worksheet */
extern const char CRSS_S     [NULL_CODE];   /* Submitted */
extern const char CRSS_A     [NULL_CODE];   /* Approved */
extern const char CRSS_R     [NULL_CODE];   /* Reviewed */
extern const char CRSS_C     [NULL_CODE];   /* Complete */
extern const char CRSS_X     [NULL_CODE];   /* Cancelled */

#define CRSSTT_W        1   /* Worksheet */
#define CRSSTT_S        2   /* Submitted */
#define CRSSTT_A        3   /* Approved */
#define CRSSTT_R        4   /* Reviewed */
#define CRSSTT_C        5   /* Complete */
#define CRSSTT_X        6   /* Cancelled */

#define _CRSSTT_MAX 6


/* CS9T = Cost Change Template Types */
extern const char CS9T[NULL_CODE];

extern const char CS9T_IS9C  [NULL_CODE];   /* Cost Changes */

#define CS9TTT_IS9C     1   /* Cost Changes */

#define _CS9TTT_MAX 1


/* CSLB = Costsku Form Labels */
extern const char CSLB[NULL_CODE];

extern const char CSLB_R     [NULL_CODE];   /* (% of Retail) */
extern const char CSLB_C     [NULL_CODE];   /* (% of Cost) */

#define CSLBTT_R        1   /* (% of Retail) */
#define CSLBTT_C        2   /* (% of Cost) */

#define _CSLBTT_MAX 2


/* CSMT = Contract Shipment Method */
extern const char CSMT[NULL_CODE];

extern const char CSMT_AR    [NULL_CODE];   /* Air */
extern const char CSMT_RD    [NULL_CODE];   /* Road */
extern const char CSMT_SA    [NULL_CODE];   /* Sea */
extern const char CSMT_RL    [NULL_CODE];   /* Rail */

#define CSMTTT_AR       1   /* Air */
#define CSMTTT_RD       2   /* Road */
#define CSMTTT_SA       3   /* Sea */
#define CSMTTT_RL       4   /* Rail */

#define _CSMTTT_MAX 4


/* CST2 = Cost Change Status */
extern const char CST2[NULL_CODE];

extern const char CST2_W     [NULL_CODE];   /* Worksheet */
extern const char CST2_S     [NULL_CODE];   /* Submitted */
extern const char CST2_A     [NULL_CODE];   /* Approved */
extern const char CST2_R     [NULL_CODE];   /* Rejected */
extern const char CST2_C     [NULL_CODE];   /* Cancelled */
extern const char CST2_D     [NULL_CODE];   /* Deleted */

#define CST2TT_W        1   /* Worksheet */
#define CST2TT_S        2   /* Submitted */
#define CST2TT_A        3   /* Approved */
#define CST2TT_R        4   /* Rejected */
#define CST2TT_C        5   /* Cancelled */
#define CST2TT_D        6   /* Deleted */

#define _CST2TT_MAX 6


/* CST3 = Cost Change Status (plus Temp) */
extern const char CST3[NULL_CODE];

extern const char CST3_T     [NULL_CODE];   /* Temporary */
extern const char CST3_W     [NULL_CODE];   /* Worksheet */
extern const char CST3_S     [NULL_CODE];   /* Submitted */
extern const char CST3_A     [NULL_CODE];   /* Approved */
extern const char CST3_R     [NULL_CODE];   /* Rejected */
extern const char CST3_E     [NULL_CODE];   /* Extracted */
extern const char CST3_C     [NULL_CODE];   /* Cancelled */
extern const char CST3_D     [NULL_CODE];   /* Deleted */

#define CST3TT_T        1   /* Temporary */
#define CST3TT_W        2   /* Worksheet */
#define CST3TT_S        3   /* Submitted */
#define CST3TT_A        4   /* Approved */
#define CST3TT_R        5   /* Rejected */
#define CST3TT_E        6   /* Extracted */
#define CST3TT_C        7   /* Cancelled */
#define CST3TT_D        8   /* Deleted */

#define _CST3TT_MAX 8


/* CSTR = Class Stores */
extern const char CSTR[NULL_CODE];

extern const char CSTR_A     [NULL_CODE];   /* Class Stores A */
extern const char CSTR_B     [NULL_CODE];   /* Class Stores B */
extern const char CSTR_C     [NULL_CODE];   /* Class Stores C */
extern const char CSTR_D     [NULL_CODE];   /* Class Stores D */
extern const char CSTR_E     [NULL_CODE];   /* Class Stores E */
extern const char CSTR_X     [NULL_CODE];   /* Class Stores X */

#define CSTRTT_A        1   /* Class Stores A */
#define CSTRTT_B        2   /* Class Stores B */
#define CSTRTT_C        3   /* Class Stores C */
#define CSTRTT_D        4   /* Class Stores D */
#define CSTRTT_E        5   /* Class Stores E */
#define CSTRTT_X        6   /* Class Stores X */

#define _CSTRTT_MAX 6


/* CSVC = Carrier Service Code */
extern const char CSVC[NULL_CODE];

extern const char CSVC_F     [NULL_CODE];   /* Free 2 Day Shipping */
extern const char CSVC_D     [NULL_CODE];   /* Discounted 4 Day Shipping */
extern const char CSVC_O     [NULL_CODE];   /* Overnight Shipping */
extern const char CSVC_P     [NULL_CODE];   /* Premium Next Business Day Delivery */
extern const char CSVC_S     [NULL_CODE];   /* Standard Shipping */

#define CSVCTT_F        1   /* Free 2 Day Shipping */
#define CSVCTT_D        2   /* Discounted 4 Day Shipping */
#define CSVCTT_O        3   /* Overnight Shipping */
#define CSVCTT_P        4   /* Premium Next Business Day Delivery */
#define CSVCTT_S        5   /* Standard Shipping */

#define _CSVCTT_MAX 5


/* CTYP = Component Type */
extern const char CTYP[NULL_CODE];

extern const char CTYP_E     [NULL_CODE];   /* Expense */
extern const char CTYP_A     [NULL_CODE];   /* Assessment */
extern const char CTYP_U     [NULL_CODE];   /* Up Charge */

#define CTYPTT_E        1   /* Expense */
#define CTYPTT_A        2   /* Assessment */
#define CTYPTT_U        3   /* Up Charge */

#define _CTYPTT_MAX 3


/* CULB = Currpend form labels */
extern const char CULB[NULL_CODE];

extern const char CULB_C     [NULL_CODE];   /* Consolidation Rate */
extern const char CULB_E     [NULL_CODE];   /* Exchange Type */

#define CULBTT_C        1   /* Consolidation Rate */
#define CULBTT_E        2   /* Exchange Type */

#define _CULBTT_MAX 2


/* CULT = Customer Location Type */
extern const char CULT[NULL_CODE];

extern const char CULT_C     [NULL_CODE];   /* @OH1@ */
extern const char CULT_F     [NULL_CODE];   /* Franchise */

#define CULTTT_C        1   /* @OH1@ */
#define CULTTT_F        2   /* Franchise */

#define _CULTTT_MAX 2


/* CURR = Currency Labels */
extern const char CURR[NULL_CODE];

extern const char CURR_PR    [NULL_CODE];   /* Prmry Retail */
extern const char CURR_LC    [NULL_CODE];   /* Local Curr */
extern const char CURR_LR    [NULL_CODE];   /* Local Retail */
extern const char CURR_PC    [NULL_CODE];   /* Prmry Cur */

#define CURRTT_PR       1   /* Prmry Retail */
#define CURRTT_LC       2   /* Local Curr */
#define CURRTT_LR       3   /* Local Retail */
#define CURRTT_PC       4   /* Prmry Cur */

#define _CURRTT_MAX 4


/* CUST = Customer Pickup */
extern const char CUST[NULL_CODE];

extern const char CUST_P     [NULL_CODE];   /* Cust Ord For:  */

#define CUSTTT_P        1   /* Cust Ord For:  */

#define _CUSTTT_MAX 1


/* CUTP = Currency Type */
extern const char CUTP[NULL_CODE];

extern const char CUTP_P     [NULL_CODE];   /* Primary */
extern const char CUTP_L     [NULL_CODE];   /* Local */

#define CUTPTT_P        1   /* Primary */
#define CUTPTT_L        2   /* Local */

#define _CUTPTT_MAX 2


/* CVAL = Condition Value */
extern const char CVAL[NULL_CODE];

extern const char CVAL_COVALU[NULL_CODE];   /* Condition Value */

#define CVALTT_COVALU   1   /* Condition Value */

#define _CVALTT_MAX 1


/* CXTP = Currency Exchange Rate Type */
extern const char CXTP[NULL_CODE];

extern const char CXTP_O     [NULL_CODE];   /* Operational */
extern const char CXTP_C     [NULL_CODE];   /* Consolidation */
extern const char CXTP_P     [NULL_CODE];   /* Oracle code type */

#define CXTPTT_O        1   /* Operational */
#define CXTPTT_C        2   /* Consolidation */
#define CXTPTT_P        3   /* Oracle code type */

#define _CXTPTT_MAX 3


/* CYFQ = Cycle Frequency */
extern const char CYFQ[NULL_CODE];

extern const char CYFQ_O     [NULL_CODE];   /* Once */
extern const char CYFQ_R     [NULL_CODE];   /* Repeating */

#define CYFQTT_O        1   /* Once */
#define CYFQTT_R        2   /* Repeating */

#define _CYFQTT_MAX 2


/* CZGP = Cost Zone Group Level */
extern const char CZGP[NULL_CODE];

extern const char CZGP_C     [NULL_CODE];   /* Corporate */
extern const char CZGP_L     [NULL_CODE];   /* Location */
extern const char CZGP_Z     [NULL_CODE];   /* Zone */

#define CZGPTT_C        1   /* Corporate */
#define CZGPTT_L        2   /* Location */
#define CZGPTT_Z        3   /* Zone */

#define _CZGPTT_MAX 3


/* DACJ = Date Conjuctions */
extern const char DACJ[NULL_CODE];

extern const char DACJ_B     [NULL_CODE];   /* Back To */
extern const char DACJ_PL    [NULL_CODE];   /* Plus Last */
extern const char DACJ_T     [NULL_CODE];   /* To */
extern const char DACJ_P     [NULL_CODE];   /* Plus */

#define DACJTT_B        1   /* Back To */
#define DACJTT_PL       2   /* Plus Last */
#define DACJTT_T        3   /* To */
#define DACJTT_P        4   /* Plus */

#define _DACJTT_MAX 4


/* DACO = Damage Code */
extern const char DACO[NULL_CODE];

extern const char DACO_S     [NULL_CODE];   /* Spoilage */
extern const char DACO_E     [NULL_CODE];   /* External Damage */
extern const char DACO_C     [NULL_CODE];   /* Concealed Damage */

#define DACOTT_S        1   /* Spoilage */
#define DACOTT_E        2   /* External Damage */
#define DACOTT_C        3   /* Concealed Damage */

#define _DACOTT_MAX 3


/* DACT = Deal Attribute Code Type */
extern const char DACT[NULL_CODE];

extern const char DACT_ACCESS[NULL_CODE];   /* Accessorial Costs - Monetary */
extern const char DACT_HAND  [NULL_CODE];   /* Handling Codes */
extern const char DACT_ALLOW [NULL_CODE];   /* Allowances - Monetary */
extern const char DACT_OTHER [NULL_CODE];   /* Other - Monetary */

#define DACTTT_ACCESS   1   /* Accessorial Costs - Monetary */
#define DACTTT_HAND     2   /* Handling Codes */
#define DACTTT_ALLOW    3   /* Allowances - Monetary */
#define DACTTT_OTHER    4   /* Other - Monetary */

#define _DACTTT_MAX 4


/* DATY = Store Ship Schedule Date Types */
extern const char DATY[NULL_CODE];

extern const char DATY_S     [NULL_CODE];   /* Specific Date */
extern const char DATY_E     [NULL_CODE];   /* Every */
extern const char DATY_T     [NULL_CODE];   /* Every Second */
extern const char DATY_H     [NULL_CODE];   /* Every Third */
extern const char DATY_F     [NULL_CODE];   /* Every Fourth */
extern const char DATY_A     [NULL_CODE];   /* All Dates in Range */

#define DATYTT_S        1   /* Specific Date */
#define DATYTT_E        2   /* Every */
#define DATYTT_T        3   /* Every Second */
#define DATYTT_H        4   /* Every Third */
#define DATYTT_F        5   /* Every Fourth */
#define DATYTT_A        6   /* All Dates in Range */

#define _DATYTT_MAX 6


/* DAY2 = Date type 2 */
extern const char DAY2[NULL_CODE];

extern const char DAY2_M     [NULL_CODE];   /* Months */
extern const char DAY2_W     [NULL_CODE];   /* Weeks */
extern const char DAY2_D     [NULL_CODE];   /* Days */

#define DAY2TT_M        1   /* Months */
#define DAY2TT_W        2   /* Weeks */
#define DAY2TT_D        3   /* Days */

#define _DAY2TT_MAX 3


/* DAYS = Days of the Week */
extern const char DAYS[NULL_CODE];

extern const char DAYS_1     [NULL_CODE];   /* Sunday */
extern const char DAYS_2     [NULL_CODE];   /* Monday */
extern const char DAYS_3     [NULL_CODE];   /* Tuesday */
extern const char DAYS_4     [NULL_CODE];   /* Wednesday */
extern const char DAYS_5     [NULL_CODE];   /* Thursday */
extern const char DAYS_6     [NULL_CODE];   /* Friday */
extern const char DAYS_7     [NULL_CODE];   /* Saturday */

#define DAYSTT_1        1   /* Sunday */
#define DAYSTT_2        2   /* Monday */
#define DAYSTT_3        3   /* Tuesday */
#define DAYSTT_4        4   /* Wednesday */
#define DAYSTT_5        5   /* Thursday */
#define DAYSTT_6        6   /* Friday */
#define DAYSTT_7        7   /* Saturday */

#define _DAYSTT_MAX 7


extern const char DCARD_8000  [NULL_CODE];   /* Debit Card */


/* DCMP = Duty Computation Code */
extern const char DCMP[NULL_CODE];

extern const char DCMP_0     [NULL_CODE];   /* 0.00 */
extern const char DCMP_1     [NULL_CODE];   /* P1*Q1 */
extern const char DCMP_2     [NULL_CODE];   /* P1*Q2 */
extern const char DCMP_3     [NULL_CODE];   /* (P1*Q1)+(P3*Q2) */
extern const char DCMP_4     [NULL_CODE];   /* (P1*Q1)+(P2*Value) */
extern const char DCMP_5     [NULL_CODE];   /* (P1*Q2)+(P2*Value) */
extern const char DCMP_6     [NULL_CODE];   /* (P1*Q2)+(P3*Q2)+(P2*Value) */
extern const char DCMP_7     [NULL_CODE];   /* P2*Value */
extern const char DCMP_9     [NULL_CODE];   /* P2*Derived Duty */
extern const char DCMP_A     [NULL_CODE];   /* (P2+P3*Q3)*Value */
extern const char DCMP_B     [NULL_CODE];   /* (P1*Q2)+(P2+P3*Q3)*Value */
extern const char DCMP_C     [NULL_CODE];   /* (P1*Q1) or (P2*Q1) */
extern const char DCMP_D     [NULL_CODE];   /* (P1*Q3)+(P2*Value) */
extern const char DCMP_E     [NULL_CODE];   /* (P1*Q3)+(P2*Value) */
extern const char DCMP_F     [NULL_CODE];   /* Q1*(P1+P3*Q2) */
extern const char DCMP_J     [NULL_CODE];   /* Greater of Q2*(P1-P2*(100-Q3)) or P3*Q2 */
extern const char DCMP_K     [NULL_CODE];   /* Greater of Q1*(P1-P2*(100-Q2)) or P3*Q1 */
extern const char DCMP_X     [NULL_CODE];   /* No computation formula available */

#define DCMPTT_0        1   /* 0.00 */
#define DCMPTT_1        2   /* P1*Q1 */
#define DCMPTT_2        3   /* P1*Q2 */
#define DCMPTT_3        4   /* (P1*Q1)+(P3*Q2) */
#define DCMPTT_4        5   /* (P1*Q1)+(P2*Value) */
#define DCMPTT_5        6   /* (P1*Q2)+(P2*Value) */
#define DCMPTT_6        7   /* (P1*Q2)+(P3*Q2)+(P2*Value) */
#define DCMPTT_7        8   /* P2*Value */
#define DCMPTT_9        9   /* P2*Derived Duty */
#define DCMPTT_A       10   /* (P2+P3*Q3)*Value */
#define DCMPTT_B       11   /* (P1*Q2)+(P2+P3*Q3)*Value */
#define DCMPTT_C       12   /* (P1*Q1) or (P2*Q1) */
#define DCMPTT_D       13   /* (P1*Q3)+(P2*Value) */
#define DCMPTT_E       14   /* (P1*Q3)+(P2*Value) */
#define DCMPTT_F       15   /* Q1*(P1+P3*Q2) */
#define DCMPTT_J       16   /* Greater of Q2*(P1-P2*(100-Q3)) or P3*Q2 */
#define DCMPTT_K       17   /* Greater of Q1*(P1-P2*(100-Q2)) or P3*Q1 */
#define DCMPTT_X       18   /* No computation formula available */

#define _DCMPTT_MAX 18


/* DCTX = Document Text */
extern const char DCTX[NULL_CODE];


/* DEFT = Nautilus Default Values */
extern const char DEFT[NULL_CODE];

extern const char DEFT_DCC   [NULL_CODE];   /* Default Carrier Code */
extern const char DEFT_DSC   [NULL_CODE];   /* Default Service Code */
extern const char DEFT_DR    [NULL_CODE];   /* Default Route Code */
extern const char DEFT_DATE  [NULL_CODE];   /* 30 */

#define DEFTTT_DCC      1   /* Default Carrier Code */
#define DEFTTT_DSC      2   /* Default Service Code */
#define DEFTTT_DR       3   /* Default Route Code */
#define DEFTTT_DATE     4   /* 30 */

#define _DEFTTT_MAX 4


/* DELB = Delete in Nightly Batch Message */
extern const char DELB[NULL_CODE];

extern const char DELB_PC    [NULL_CODE];   /* Price Change */

#define DELBTT_PC       1   /* Price Change */

#define _DELBTT_MAX 1


/* DELT = Customer Delivery Type */
extern const char DELT[NULL_CODE];

extern const char DELT_S     [NULL_CODE];   /* Ship Direct */
extern const char DELT_C     [NULL_CODE];   /* Customer Pickup */

#define DELTTT_S        1   /* Ship Direct */
#define DELTTT_C        2   /* Customer Pickup */

#define _DELTTT_MAX 2


/* DEMG = Sales Audit Demographic */
extern const char DEMG[NULL_CODE];

extern const char DEMG_FC    [NULL_CODE];   /* Flower Child */
extern const char DEMG_SM    [NULL_CODE];   /* Soccer Mom */
extern const char DEMG_BB    [NULL_CODE];   /* Baby Boomer */
extern const char DEMG_000   [NULL_CODE];   /* Error */

#define DEMGTT_FC       1   /* Flower Child */
#define DEMGTT_SM       2   /* Soccer Mom */
#define DEMGTT_BB       3   /* Baby Boomer */
#define DEMGTT_000      4   /* Error */

#define _DEMGTT_MAX 4


/* DEPO = Deposit Code */
extern const char DEPO[NULL_CODE];

extern const char DEPO_NONE  [NULL_CODE];   /* None */
extern const char DEPO_MA    [NULL_CODE];   /* Massachusetts - 0.05 */
extern const char DEPO_ME1   [NULL_CODE];   /* Maine - 0.05 */
extern const char DEPO_ME2   [NULL_CODE];   /* Maine - 0.10 */

#define DEPOTT_NONE     1   /* None */
#define DEPOTT_MA       2   /* Massachusetts - 0.05 */
#define DEPOTT_ME1      3   /* Maine - 0.05 */
#define DEPOTT_ME2      4   /* Maine - 0.10 */

#define _DEPOTT_MAX 4


/* DEST = Reports Destination */
extern const char DEST[NULL_CODE];

extern const char DEST_V     [NULL_CODE];   /* Preview */
extern const char DEST_S     [NULL_CODE];   /* Screen */
extern const char DEST_F     [NULL_CODE];   /* File */
extern const char DEST_P     [NULL_CODE];   /* Printer */
extern const char DEST_M     [NULL_CODE];   /* Mail */

#define DESTTT_V        1   /* Preview */
#define DESTTT_S        2   /* Screen */
#define DESTTT_F        3   /* File */
#define DESTTT_P        4   /* Printer */
#define DESTTT_M        5   /* Mail */

#define _DESTTT_MAX 5


/* DETP = Delete Type */
extern const char DETP[NULL_CODE];

extern const char DETP_D     [NULL_CODE];   /* Delete */
extern const char DETP_C     [NULL_CODE];   /* Cancel */

#define DETPTT_D        1   /* Delete */
#define DETPTT_C        2   /* Cancel */

#define _DETPTT_MAX 2


/* DFDL = Active Date and Close Date labels */
extern const char DFDL[NULL_CODE];

extern const char DFDL_AND   [NULL_CODE];   /* and */
extern const char DFDL_SDBET [NULL_CODE];   /* Start Date Between */
extern const char DFDL_EDBET [NULL_CODE];   /* End Date Between */
extern const char DFDL_SD    [NULL_CODE];   /* Start Date */
extern const char DFDL_ED    [NULL_CODE];   /* End Date */

#define DFDLTT_AND      1   /* and */
#define DFDLTT_SDBET    2   /* Start Date Between */
#define DFDLTT_EDBET    3   /* End Date Between */
#define DFDLTT_SD       4   /* Start Date */
#define DFDLTT_ED       5   /* End Date */

#define _DFDLTT_MAX 5


/* DFTY = Diff Apply Types */
extern const char DFTY[NULL_CODE];

extern const char DFTY_D     [NULL_CODE];   /* Diff ID */
extern const char DFTY_R     [NULL_CODE];   /* Diff Range */
extern const char DFTY_T     [NULL_CODE];   /* Diff Template */

#define DFTYTT_D        1   /* Diff ID */
#define DFTYTT_R        2   /* Diff Range */
#define DFTYTT_T        3   /* Diff Template */

#define _DFTYTT_MAX 3


/* DIFF = Diff Types */
extern const char DIFF[NULL_CODE];

extern const char DIFF_S     [NULL_CODE];   /* Size */
extern const char DIFF_C     [NULL_CODE];   /* Color */
extern const char DIFF_F     [NULL_CODE];   /* Flavor */
extern const char DIFF_E     [NULL_CODE];   /* Scent */
extern const char DIFF_P     [NULL_CODE];   /* Pattern */

#define DIFFTT_S        1   /* Size */
#define DIFFTT_C        2   /* Color */
#define DIFFTT_F        3   /* Flavor */
#define DIFFTT_E        4   /* Scent */
#define DIFFTT_P        5   /* Pattern */

#define _DIFFTT_MAX 5


/* DIFT = Differential Group/ID Types */
extern const char DIFT[NULL_CODE];

extern const char DIFT_ID    [NULL_CODE];   /* ID */
extern const char DIFT_GROUP [NULL_CODE];   /* Group */

#define DIFTTT_ID       1   /* ID */
#define DIFTTT_GROUP    2   /* Group */

#define _DIFTTT_MAX 2


/* DIML = Deal Component Merchandise Level */
extern const char DIML[NULL_CODE];

extern const char DIML_1     [NULL_CODE];   /* @OH1@ */
extern const char DIML_2     [NULL_CODE];   /* @MH2@ */
extern const char DIML_3     [NULL_CODE];   /* @MH3@ */
extern const char DIML_4     [NULL_CODE];   /* @MH4@ */
extern const char DIML_5     [NULL_CODE];   /* @MH5@ */
extern const char DIML_6     [NULL_CODE];   /* @MH6@ */
extern const char DIML_7     [NULL_CODE];   /* Item Parent */
extern const char DIML_8     [NULL_CODE];   /* Item Parent - Diff 1 */
extern const char DIML_9     [NULL_CODE];   /* Item Parent - Diff 2 */
extern const char DIML_10    [NULL_CODE];   /* Item Parent - Diff 3 */
extern const char DIML_11    [NULL_CODE];   /* Item Parent - Diff 4 */
extern const char DIML_12    [NULL_CODE];   /* Item */

#define DIMLTT_1        1   /* @OH1@ */
#define DIMLTT_2        2   /* @MH2@ */
#define DIMLTT_3        3   /* @MH3@ */
#define DIMLTT_4        4   /* @MH4@ */
#define DIMLTT_5        5   /* @MH5@ */
#define DIMLTT_6        6   /* @MH6@ */
#define DIMLTT_7        7   /* Item Parent */
#define DIMLTT_8        8   /* Item Parent - Diff 1 */
#define DIMLTT_9        9   /* Item Parent - Diff 2 */
#define DIMLTT_10      10   /* Item Parent - Diff 3 */
#define DIMLTT_11      11   /* Item Parent - Diff 4 */
#define DIMLTT_12      12   /* Item */

#define _DIMLTT_MAX 12


/* DIMO = Dimmension Object */
extern const char DIMO[NULL_CODE];

extern const char DIMO_EA    [NULL_CODE];   /* Each */
extern const char DIMO_IN    [NULL_CODE];   /* Inner */
extern const char DIMO_CA    [NULL_CODE];   /* Case */
extern const char DIMO_PA    [NULL_CODE];   /* Pallet */

#define DIMOTT_EA       1   /* Each */
#define DIMOTT_IN       2   /* Inner */
#define DIMOTT_CA       3   /* Case */
#define DIMOTT_PA       4   /* Pallet */

#define _DIMOTT_MAX 4


/* DIMS = Dimension Object-Simple Pack Setup */
extern const char DIMS[NULL_CODE];

extern const char DIMS_CA    [NULL_CODE];   /* Case */
extern const char DIMS_PA    [NULL_CODE];   /* Pallet */

#define DIMSTT_CA       1   /* Case */
#define DIMSTT_PA       2   /* Pallet */

#define _DIMSTT_MAX 2


/* DIOL = Deal Component Org. Level */
extern const char DIOL[NULL_CODE];

extern const char DIOL_1     [NULL_CODE];   /* @OH2@ */
extern const char DIOL_2     [NULL_CODE];   /* @OH3@ */
extern const char DIOL_3     [NULL_CODE];   /* @OH4@ */
extern const char DIOL_4     [NULL_CODE];   /* @OH5@ */
extern const char DIOL_5     [NULL_CODE];   /* Location */

#define DIOLTT_1        1   /* @OH2@ */
#define DIOLTT_2        2   /* @OH3@ */
#define DIOLTT_3        3   /* @OH4@ */
#define DIOLTT_4        4   /* @OH5@ */
#define DIOLTT_5        5   /* Location */

#define _DIOLTT_MAX 5


/* DISC = Discrepancies */
extern const char DISC[NULL_CODE];

extern const char DISC_O     [NULL_CODE];   /* Overage */
extern const char DISC_S     [NULL_CODE];   /* Shortage */

#define DISCTT_O        1   /* Overage */
#define DISCTT_S        2   /* Shortage */

#define _DISCTT_MAX 2


/* DISD = Discrepancy Display */
extern const char DISD[NULL_CODE];

extern const char DISD_DESC  [NULL_CODE];   /* Descriptive */
extern const char DISD_PCNT  [NULL_CODE];   /* Percentage */
extern const char DISD_ABSO  [NULL_CODE];   /* Absolute */

#define DISDTT_DESC     1   /* Descriptive */
#define DISDTT_PCNT     2   /* Percentage */
#define DISDTT_ABSO     3   /* Absolute */

#define _DISDTT_MAX 3


/* DIST = Discount Type */
extern const char DIST[NULL_CODE];

extern const char DIST_P     [NULL_CODE];   /* % */
extern const char DIST_A     [NULL_CODE];   /* Amt */
extern const char DIST_U     [NULL_CODE];   /* Unit */

#define DISTTT_P        1   /* % */
#define DISTTT_A        2   /* Amt */
#define DISTTT_U        3   /* Unit */

#define _DISTTT_MAX 3


/* DITM = Deposit Price Per UOM */
extern const char DITM[NULL_CODE];

extern const char DITM_I     [NULL_CODE];   /* Inclusive of Deposit Amount */
extern const char DITM_E     [NULL_CODE];   /* Exclusive of Deposit Amount */

#define DITMTT_I        1   /* Inclusive of Deposit Amount */
#define DITMTT_E        2   /* Exclusive of Deposit Amount */

#define _DITMTT_MAX 2


/* DLAP = Deal Age Priority Levels */
extern const char DLAP[NULL_CODE];

extern const char DLAP_O     [NULL_CODE];   /* Oldest */
extern const char DLAP_N     [NULL_CODE];   /* Newest */

#define DLAPTT_O        1   /* Oldest */
#define DLAPTT_N        2   /* Newest */

#define _DLAPTT_MAX 2


/* DLAT = Identifies whether thresholds have been */
extern const char DLAT[NULL_CODE];

extern const char DLAT_I     [NULL_CODE];   /* Insert */
extern const char DLAT_U     [NULL_CODE];   /* Update */
extern const char DLAT_D     [NULL_CODE];   /* Delete */

#define DLATTT_I        1   /* Insert */
#define DLATTT_U        2   /* Update */
#define DLATTT_D        3   /* Delete */

#define _DLATTT_MAX 3


/* DLBM = Deal Billing Method */
extern const char DLBM[NULL_CODE];

extern const char DLBM_C     [NULL_CODE];   /* Credit Note */
extern const char DLBM_D     [NULL_CODE];   /* Debit Note */

#define DLBMTT_C        1   /* Credit Note */
#define DLBMTT_D        2   /* Debit Note */

#define _DLBMTT_MAX 2


/* DLBP = Code that identifies the bill-back perio */
extern const char DLBP[NULL_CODE];

extern const char DLBP_W     [NULL_CODE];   /* Week */
extern const char DLBP_M     [NULL_CODE];   /* Month */
extern const char DLBP_Q     [NULL_CODE];   /* Quarter */
extern const char DLBP_H     [NULL_CODE];   /* Half Year */
extern const char DLBP_A     [NULL_CODE];   /* Annual */

#define DLBPTT_W        1   /* Week */
#define DLBPTT_M        2   /* Month */
#define DLBPTT_Q        3   /* Quarter */
#define DLBPTT_H        4   /* Half Year */
#define DLBPTT_A        5   /* Annual */

#define _DLBPTT_MAX 5


/* DLBT = Billing type of the deal component. */
extern const char DLBT[NULL_CODE];

extern const char DLBT_OI    [NULL_CODE];   /* Off-invoice */
extern const char DLBT_BB    [NULL_CODE];   /* Bill Back */
extern const char DLBT_VFP   [NULL_CODE];   /* Vendor Funded Promotion */
extern const char DLBT_VFM   [NULL_CODE];   /* Vendor Funded Markdown */
extern const char DLBT_BBR   [NULL_CODE];   /* Bill Back Rebate */

#define DLBTTT_OI       1   /* Off-invoice */
#define DLBTTT_BB       2   /* Bill Back */
#define DLBTTT_VFP      3   /* Vendor Funded Promotion */
#define DLBTTT_VFM      4   /* Vendor Funded Markdown */
#define DLBTTT_BBR      5   /* Bill Back Rebate */

#define _DLBTTT_MAX 5


/* DLCA = Deal Component Cost Application Buckets */
extern const char DLCA[NULL_CODE];

extern const char DLCA_N     [NULL_CODE];   /* Net Cost */
extern const char DLCA_NN    [NULL_CODE];   /* Net Net Cost */
extern const char DLCA_DNN   [NULL_CODE];   /* Dead Net Cost */

#define DLCATT_N        1   /* Net Cost */
#define DLCATT_NN       2   /* Net Net Cost */
#define DLCATT_DNN      3   /* Dead Net Cost */

#define _DLCATT_MAX 3


/* DLCL = Deal Component Classes */
extern const char DLCL[NULL_CODE];

extern const char DLCL_CU    [NULL_CODE];   /* Cumulative - discs summed then applied */
extern const char DLCL_CS    [NULL_CODE];   /* Cascade - discs taken one at a time */
extern const char DLCL_EX    [NULL_CODE];   /* Exclusive - overrides other discounts */

#define DLCLTT_CU       1   /* Cumulative - discs summed then applied */
#define DLCLTT_CS       2   /* Cascade - discs taken one at a time */
#define DLCLTT_EX       3   /* Exclusive - overrides other discounts */

#define _DLCLTT_MAX 3


/* DLCT = Indicates if the rebate should be calcul */
extern const char DLCT[NULL_CODE];

extern const char DLCT_L     [NULL_CODE];   /* Linear */
extern const char DLCT_S     [NULL_CODE];   /* Scalar */

#define DLCTTT_L        1   /* Linear */
#define DLCTTT_S        2   /* Scalar */

#define _DLCTTT_MAX 2


/* DLH2 = Deal Types */
extern const char DLH2[NULL_CODE];

extern const char DLH2_A     [NULL_CODE];   /* Annual */
extern const char DLH2_P     [NULL_CODE];   /* Promotional */

#define DLH2TT_A        1   /* Annual */
#define DLH2TT_P        2   /* Promotional */

#define _DLH2TT_MAX 2


/* DLHC = Deal Types for Create from existing */
extern const char DLHC[NULL_CODE];

extern const char DLHC_A     [NULL_CODE];   /* Annual */
extern const char DLHC_P     [NULL_CODE];   /* Promotional */
extern const char DLHC_M     [NULL_CODE];   /* Vendor funded markdown */

#define DLHCTT_A        1   /* Annual */
#define DLHCTT_P        2   /* Promotional */
#define DLHCTT_M        3   /* Vendor funded markdown */

#define _DLHCTT_MAX 3


/* DLHS = Deal Statuses */
extern const char DLHS[NULL_CODE];

extern const char DLHS_W     [NULL_CODE];   /* Worksheet */
extern const char DLHS_S     [NULL_CODE];   /* Submitted */
extern const char DLHS_A     [NULL_CODE];   /* Approved */
extern const char DLHS_R     [NULL_CODE];   /* Rejected */
extern const char DLHS_C     [NULL_CODE];   /* Closed */

#define DLHSTT_W        1   /* Worksheet */
#define DLHSTT_S        2   /* Submitted */
#define DLHSTT_A        3   /* Approved */
#define DLHSTT_R        4   /* Rejected */
#define DLHSTT_C        5   /* Closed */

#define _DLHSTT_MAX 5


/* DLHT = Deal Types */
extern const char DLHT[NULL_CODE];

extern const char DLHT_A     [NULL_CODE];   /* Annual */
extern const char DLHT_P     [NULL_CODE];   /* Promotional */
extern const char DLHT_O     [NULL_CODE];   /* PO-Specific */
extern const char DLHT_M     [NULL_CODE];   /* Vendor funded markdown */

#define DLHTTT_A        1   /* Annual */
#define DLHTTT_P        2   /* Promotional */
#define DLHTTT_O        3   /* PO-Specific */
#define DLHTTT_M        4   /* Vendor funded markdown */

#define _DLHTTT_MAX 4


/* DLIC = Deal Income Calculation */
extern const char DLIC[NULL_CODE];

extern const char DLIC_A     [NULL_CODE];   /* Actuals Earned to date */
extern const char DLIC_P     [NULL_CODE];   /* Pro-rated using forecast */

#define DLICTT_A        1   /* Actuals Earned to date */
#define DLICTT_P        2   /* Pro-rated using forecast */

#define _DLICTT_MAX 2


/* DLIP = Deal Invoice Processing */
extern const char DLIP[NULL_CODE];

extern const char DLIP_AA    [NULL_CODE];   /* Automatic */
extern const char DLIP_MA    [NULL_CODE];   /* Manual */
extern const char DLIP_NO    [NULL_CODE];   /* No Invoice Processing */

#define DLIPTT_AA       1   /* Automatic */
#define DLIPTT_MA       2   /* Manual */
#define DLIPTT_NO       5   /* No Invoice Processing */

#define _DLIPTT_MAX 5


/* DLL2 = Deal Component Value Types */
extern const char DLL2[NULL_CODE];

extern const char DLL2_Q     [NULL_CODE];   /* Buy/Get Free or Disc Items */
extern const char DLL2_A     [NULL_CODE];   /* Amount off */
extern const char DLL2_P     [NULL_CODE];   /* Percent off per unit */
extern const char DLL2_F     [NULL_CODE];   /* Get units for fixed price */

#define DLL2TT_Q        1   /* Buy/Get Free or Disc Items */
#define DLL2TT_A        2   /* Amount off */
#define DLL2TT_P        3   /* Percent off per unit */
#define DLL2TT_F        4   /* Get units for fixed price */

#define _DLL2TT_MAX 4


/* DLL3 = Tran Disc Deal Component Value Types */
extern const char DLL3[NULL_CODE];

extern const char DLL3_A     [NULL_CODE];   /* Amount off per order */
extern const char DLL3_P     [NULL_CODE];   /* Percent off per order */

#define DLL3TT_A        1   /* Amount off per order */
#define DLL3TT_P        2   /* Percent off per order */

#define _DLL3TT_MAX 2


/* DLLD = Tran Disc Deal Component Thresh Limits */
extern const char DLLD[NULL_CODE];

extern const char DLLD_A     [NULL_CODE];   /* Amount - total cost of orders */

#define DLLDTT_A        1   /* Amount - total cost of orders */

#define _DLLDTT_MAX 1


/* DLLT = Deal Component Threshold Limits */
extern const char DLLT[NULL_CODE];

extern const char DLLT_Q     [NULL_CODE];   /* Quantity - number of Units */
extern const char DLLT_A     [NULL_CODE];   /* Amount - total value of units */

#define DLLTTT_Q        1   /* Quantity - number of Units */
#define DLLTTT_A        2   /* Amount - total value of units */

#define _DLLTTT_MAX 2


/* DLPB = Deal Performance Basis Actuals Indicator */
extern const char DLPB[NULL_CODE];

extern const char DLPB_A     [NULL_CODE];   /* Yes */
extern const char DLPB_F     [NULL_CODE];   /* No */

#define DLPBTT_A        1   /* Yes */
#define DLPBTT_F        2   /* No */

#define _DLPBTT_MAX 2


/* DLPT = Deal Type Priority Levels */
extern const char DLPT[NULL_CODE];

extern const char DLPT_A     [NULL_CODE];   /* Annual */
extern const char DLPT_P     [NULL_CODE];   /* Promotional */

#define DLPTTT_A        1   /* Annual */
#define DLPTTT_P        2   /* Promotional */

#define _DLPTTT_MAX 2


/* DLRL = Deal Reporting Level */
extern const char DLRL[NULL_CODE];

extern const char DLRL_W     [NULL_CODE];   /* Week */
extern const char DLRL_M     [NULL_CODE];   /* Month */
extern const char DLRL_Q     [NULL_CODE];   /* Quarter */

#define DLRLTT_W        1   /* Week */
#define DLRLTT_M        2   /* Month */
#define DLRLTT_Q        3   /* Quarter */

#define _DLRLTT_MAX 3


/* DLRP = Rebate Application Types */
extern const char DLRP[NULL_CODE];

extern const char DLRP_P     [NULL_CODE];   /* Purchases */
extern const char DLRP_S     [NULL_CODE];   /* Sales */

#define DLRPTT_P        1   /* Purchases */
#define DLRPTT_S        2   /* Sales */

#define _DLRPTT_MAX 2


/* DLTP = Deal Types */
extern const char DLTP[NULL_CODE];

extern const char DLTP_DEAL  [NULL_CODE];   /* Deal */
extern const char DLTP_SKU   [NULL_CODE];   /* Item Bracket */
extern const char DLTP_SUPP  [NULL_CODE];   /* Supplier Bracket */
extern const char DLTP_SKUA  [NULL_CODE];   /* Item Allowance */
extern const char DLTP_SUPA  [NULL_CODE];   /* Supplier Allowance */
extern const char DLTP_MCHA  [NULL_CODE];   /* Hierarchy Allowance */

#define DLTPTT_DEAL     1   /* Deal */
#define DLTPTT_SKU      2   /* Item Bracket */
#define DLTPTT_SUPP     3   /* Supplier Bracket */
#define DLTPTT_SKUA     4   /* Item Allowance */
#define DLTPTT_SUPA     5   /* Supplier Allowance */
#define DLTPTT_MCHA     6   /* Hierarchy Allowance */

#define _DLTPTT_MAX 6


/* DLTT = Values for Deal Total Indicator */
extern const char DLTT[NULL_CODE];

extern const char DLTT_Y     [NULL_CODE];   /* Total */
extern const char DLTT_N     [NULL_CODE];   /* Unit */

#define DLTTTT_Y        1   /* Total */
#define DLTTTT_N        2   /* Unit */

#define _DLTTTT_MAX 2


/* DLVY = Delivery Policy */
extern const char DLVY[NULL_CODE];

extern const char DLVY_NEXT  [NULL_CODE];   /* Next Day */
extern const char DLVY_NDD   [NULL_CODE];   /* Next Valid Delivery Day */

#define DLVYTT_NEXT     1   /* Next Day */
#define DLVYTT_NDD      2   /* Next Valid Delivery Day */

#define _DLVYTT_MAX 2


/* DOCT = Document Type */
extern const char DOCT[NULL_CODE];

extern const char DOCT_REQ   [NULL_CODE];   /* Required Documents */
extern const char DOCT_CS    [NULL_CODE];   /* Charges */
extern const char DOCT_BI    [NULL_CODE];   /* Bank Instructions */
extern const char DOCT_SI    [NULL_CODE];   /* Sender Instructions */
extern const char DOCT_AI    [NULL_CODE];   /* Additional Instructions */

#define DOCTTT_REQ      1   /* Required Documents */
#define DOCTTT_CS       2   /* Charges */
#define DOCTTT_BI       3   /* Bank Instructions */
#define DOCTTT_SI       4   /* Sender Instructions */
#define DOCTTT_AI       5   /* Additional Instructions */

#define _DOCTTT_MAX 5


/* DOHT = Distribution On Hand Type */
extern const char DOHT[NULL_CODE];

extern const char DOHT_NOLKUP[NULL_CODE];   /* No Look Up - Use Supplied Value */
extern const char DOHT_LKSOH [NULL_CODE];   /* Look Up Stock On Hand */
extern const char DOHT_LKNOH [NULL_CODE];   /* Look Up Net Stock On Hand */

#define DOHTTT_NOLKUP   1   /* No Look Up - Use Supplied Value */
#define DOHTTT_LKSOH    2   /* Look Up Stock On Hand */
#define DOHTTT_LKNOH    3   /* Look Up Net Stock On Hand */

#define _DOHTTT_MAX 3


/* DOL = Due Order Level */
extern const char DOL[NULL_CODE];

extern const char DOL_I     [NULL_CODE];   /* Item Level */
extern const char DOL_O     [NULL_CODE];   /* Purchase Order Level */

#define DOLTT_I        1   /* Item Level */
#define DOLTT_O        2   /* Purchase Order Level */

#define _DOLTT_MAX 2


/* DOOR = Default Door Type Indicator */
extern const char DOOR[NULL_CODE];

extern const char DOOR_A     [NULL_CODE];   /* Default Door Type Indicator */

#define DOORTT_A        1   /* Default Door Type Indicator */

#define _DOORTT_MAX 1


/* DOSB = Due Order Service Basis */
extern const char DOSB[NULL_CODE];

extern const char DOSB_C     [NULL_CODE];   /* Cost */
extern const char DOSB_U     [NULL_CODE];   /* Units */
extern const char DOSB_P     [NULL_CODE];   /* Profit */

#define DOSBTT_C        1   /* Cost */
#define DOSBTT_U        2   /* Units */
#define DOSBTT_P        3   /* Profit */

#define _DOSBTT_MAX 3


/* DQGT = Deal Quantity Thresh. Get Types */
extern const char DQGT[NULL_CODE];

extern const char DQGT_X     [NULL_CODE];   /* Free - get units free */
extern const char DQGT_P     [NULL_CODE];   /* Percent Off - get units at % off cost */
extern const char DQGT_A     [NULL_CODE];   /* Amount - get units at amount off cost */
extern const char DQGT_F     [NULL_CODE];   /* Fixed Amount - get units for fixed price */

#define DQGTTT_X        1   /* Free - get units free */
#define DQGTTT_P        2   /* Percent Off - get units at % off cost */
#define DQGTTT_A        3   /* Amount - get units at amount off cost */
#define DQGTTT_F        4   /* Fixed Amount - get units for fixed price */

#define _DQGTTT_MAX 4


/* DQT2 = Deal Quantity Threshold Get Types */
extern const char DQT2[NULL_CODE];

extern const char DQT2_P     [NULL_CODE];   /* percent off */
extern const char DQT2_A     [NULL_CODE];   /* amount */
extern const char DQT2_F     [NULL_CODE];   /* fixed amount */

#define DQT2TT_P        1   /* percent off */
#define DQT2TT_A        2   /* amount */
#define DQT2TT_F        3   /* fixed amount */

#define _DQT2TT_MAX 3


extern const char DRIVEO_9000  [NULL_CODE];   /* Fuel Drive Off */


/* DRUL = Distribution Rules */
extern const char DRUL[NULL_CODE];

extern const char DRUL_PRORAT[NULL_CODE];   /* Proration */
extern const char DRUL_MN2MX [NULL_CODE];   /* Minimum to maximum */
extern const char DRUL_MX2MN [NULL_CODE];   /* Maximum to minimum */

#define DRULTT_PRORAT   1   /* Proration */
#define DRULTT_MN2MX    2   /* Minimum to maximum */
#define DRULTT_MX2MN    3   /* Maximum to minimum */

#define _DRULTT_MAX 3


/* DSTK = Distribution Keys */
extern const char DSTK[NULL_CODE];

extern const char DSTK_ON    [NULL_CODE];   /* Order No */
extern const char DSTK_PTI   [NULL_CODE];   /* Pack Template ID */
extern const char DSTK_ST    [NULL_CODE];   /* Item Parent */
extern const char DSTK_CN    [NULL_CODE];   /* Contract No */

#define DSTKTT_ON       1   /* Order No */
#define DSTKTT_PTI      2   /* Pack Template ID */
#define DSTKTT_ST       3   /* Item Parent */
#define DSTKTT_CN       4   /* Contract No */

#define _DSTKTT_MAX 4


/* DSTR = Distro type */
extern const char DSTR[NULL_CODE];

extern const char DSTR_T     [NULL_CODE];   /* Transfer */
extern const char DSTR_A     [NULL_CODE];   /* Allocation */

#define DSTRTT_T        1   /* Transfer */
#define DSTRTT_A        2   /* Allocation */

#define _DSTRTT_MAX 2


/* DSTT = Distribution Types */
extern const char DSTT[NULL_CODE];

extern const char DSTT_DP    [NULL_CODE];   /* Distribution % */
extern const char DSTT_QT    [NULL_CODE];   /* Quantity */
extern const char DSTT_RA    [NULL_CODE];   /* Ratio */
extern const char DSTT_TL    [NULL_CODE];   /* Total */
extern const char DSTT_QS    [NULL_CODE];   /* Qty. per Store */

#define DSTTTT_DP       1   /* Distribution % */
#define DSTTTT_QT       2   /* Quantity */
#define DSTTTT_RA       3   /* Ratio */
#define DSTTTT_TL       4   /* Total */
#define DSTTTT_QS       5   /* Qty. per Store */

#define _DSTTTT_MAX 5


/* DTAX = Default Tax Types */
extern const char DTAX[NULL_CODE];

extern const char DTAX_SVAT  [NULL_CODE];   /* Simple VAT */
extern const char DTAX_SALES [NULL_CODE];   /* Sales and Use Tax */
extern const char DTAX_GTAX  [NULL_CODE];   /* Global Tax */

#define DTAXTT_SVAT     1   /* Simple VAT */
#define DTAXTT_SALES    2   /* Sales and Use Tax */
#define DTAXTT_GTAX     3   /* Global Tax */

#define _DTAXTT_MAX 3


/* DTTP = Sales Audit Data Types */
extern const char DTTP[NULL_CODE];

extern const char DTTP_C     [NULL_CODE];   /* Character */
extern const char DTTP_N     [NULL_CODE];   /* Number */
extern const char DTTP_D     [NULL_CODE];   /* Date */

#define DTTPTT_C        1   /* Character */
#define DTTPTT_N        2   /* Number */
#define DTTPTT_D        3   /* Date */

#define _DTTPTT_MAX 3


/* DTYP = Distribution Type */
extern const char DTYP[NULL_CODE];

extern const char DTYP_ININV [NULL_CODE];   /* Into Inventory */
extern const char DTYP_OTINV [NULL_CODE];   /* Out of Inventory */

#define DTYPTT_ININV    1   /* Into Inventory */
#define DTYPTT_OTINV    2   /* Out of Inventory */

#define _DTYPTT_MAX 2


/* DTYS = Duty Status */
extern const char DTYS[NULL_CODE];

extern const char DTYS_W     [NULL_CODE];   /* Worksheet */
extern const char DTYS_A     [NULL_CODE];   /* Approved */

#define DTYSTT_W        1   /* Worksheet */
#define DTYSTT_A        2   /* Approved */

#define _DTYSTT_MAX 2


/* EALT = Expense */
extern const char EALT[NULL_CODE];

extern const char EALT_A     [NULL_CODE];   /* Amount */
extern const char EALT_U     [NULL_CODE];   /* Unit of Measure */

#define EALTTT_A        1   /* Amount */
#define EALTTT_U        2   /* Unit of Measure */

#define _EALTTT_MAX 2


/* ECCR = EDI Cost Change Reason */
extern const char ECCR[NULL_CODE];

extern const char ECCR_A     [NULL_CODE];   /* Accepted */
extern const char ECCR_R     [NULL_CODE];   /* Rejected */
extern const char ECCR_N     [NULL_CODE];   /* Not Reviewed */
extern const char ECCR_F     [NULL_CODE];   /* Failed */

#define ECCRTT_A        1   /* Accepted */
#define ECCRTT_R        2   /* Rejected */
#define ECCRTT_N        3   /* Not Reviewed */
#define ECCRTT_F        4   /* Failed */

#define _ECCRTT_MAX 4


/* EDBG = EDI Message Types */
extern const char EDBG[NULL_CODE];

extern const char EDBG_F     [NULL_CODE];   /* Failure */
extern const char EDBG_W     [NULL_CODE];   /* Warning */

#define EDBGTT_F        1   /* Failure */
#define EDBGTT_W        2   /* Warning */

#define _EDBGTT_MAX 2


/* EDCT = EDI Code Type */
extern const char EDCT[NULL_CODE];

extern const char EDCT_AI    [NULL_CODE];   /* Add Item(s) */
extern const char EDCT_DI    [NULL_CODE];   /* Delete Item(s) */
extern const char EDCT_PC    [NULL_CODE];   /* Price Change */
extern const char EDCT_QD    [NULL_CODE];   /* Quantity Decrease */
extern const char EDCT_QI    [NULL_CODE];   /* Quantity Increase */

#define EDCTTT_AI       1   /* Add Item(s) */
#define EDCTTT_DI       2   /* Delete Item(s) */
#define EDCTTT_PC       3   /* Price Change */
#define EDCTTT_QD       4   /* Quantity Decrease */
#define EDCTTT_QI       5   /* Quantity Increase */

#define _EDCTTT_MAX 5


/* EDIO = EDI Order Indicator */
extern const char EDIO[NULL_CODE];

extern const char EDIO_Y     [NULL_CODE];   /* Submit via EDI */
extern const char EDIO_N     [NULL_CODE];   /* Do not use EDI */
extern const char EDIO_S     [NULL_CODE];   /* Order has already been sent via EDI */

#define EDIOTT_Y        1   /* Submit via EDI */
#define EDIOTT_N        2   /* Do not use EDI */
#define EDIOTT_S        3   /* Order has already been sent via EDI */

#define _EDIOTT_MAX 3


/* EDST = EDI Status */
extern const char EDST[NULL_CODE];

extern const char EDST_C     [NULL_CODE];   /* Changed */
extern const char EDST_N     [NULL_CODE];   /* Not Changed */
extern const char EDST_D     [NULL_CODE];   /* Deleted */

#define EDSTTT_C        1   /* Changed */
#define EDSTTT_N        2   /* Not Changed */
#define EDSTTT_D        3   /* Deleted */

#define _EDSTTT_MAX 3


/* EHAC = Event History Action Codes */
extern const char EHAC[NULL_CODE];

extern const char EHAC_URE   [NULL_CODE];   /* User Requested Evaluation */
extern const char EHAC_PREA  [NULL_CODE];   /* Pre-Action Evaluation */
extern const char EHAC_PSTA  [NULL_CODE];   /* Post-Action Evaluation */
extern const char EHAC_AAE   [NULL_CODE];   /* Auto-Action Executed */
extern const char EHAC_MAE   [NULL_CODE];   /* Manual Action Executed */
extern const char EHAC_SE    [NULL_CODE];   /* Scheduled Re-evaluation */
extern const char EHAC_XCRE  [NULL_CODE];   /* Exception Creation Evaluation */
extern const char EHAC_ADDXE [NULL_CODE];   /* Additional Exception Re-evaluation */

#define EHACTT_URE      1   /* User Requested Evaluation */
#define EHACTT_PREA     2   /* Pre-Action Evaluation */
#define EHACTT_PSTA     3   /* Post-Action Evaluation */
#define EHACTT_AAE      4   /* Auto-Action Executed */
#define EHACTT_MAE      5   /* Manual Action Executed */
#define EHACTT_SE       6   /* Scheduled Re-evaluation */
#define EHACTT_XCRE     7   /* Exception Creation Evaluation */
#define EHACTT_ADDXE    8   /* Additional Exception Re-evaluation */

#define _EHACTT_MAX 8


/* EMPT = Employee Type */
extern const char EMPT[NULL_CODE];

extern const char EMPT_S     [NULL_CODE];   /* Store */
extern const char EMPT_H     [NULL_CODE];   /* Headquarters */

#define EMPTTT_S        1   /* Store */
#define EMPTTT_H        2   /* Headquarters */

#define _EMPTTT_MAX 2


/* ESDF = EDI Sales Download Frequency */
extern const char ESDF[NULL_CODE];

extern const char ESDF_D     [NULL_CODE];   /* Daily */
extern const char ESDF_W     [NULL_CODE];   /* Weekly */

#define ESDFTT_D        1   /* Daily */
#define ESDFTT_W        2   /* Weekly */

#define _ESDFTT_MAX 2


/* ESLV = Escheatment Level */
extern const char ESLV[NULL_CODE];

extern const char ESLV_C     [NULL_CODE];   /* Country */
extern const char ESLV_S     [NULL_CODE];   /* State */

#define ESLVTT_C        1   /* Country */
#define ESLVTT_S        2   /* State */

#define _ESLVTT_MAX 2


/* EVIM = Event Informational Messages */
extern const char EVIM[NULL_CODE];

extern const char EVIM_U     [NULL_CODE];   /* Unable to refresh parameters */
extern const char EVIM_L     [NULL_CODE];   /* Event Reevaluation Process Loop Exceeded */
extern const char EVIM_A     [NULL_CODE];   /* Auto Action Failed Execution Failed */
extern const char EVIM_E     [NULL_CODE];   /* Unexpected Error In Reevaluation Process */
extern const char EVIM_G     [NULL_CODE];   /* Empty Group Resolution-No User Assigned */
extern const char EVIM_R     [NULL_CODE];   /* Empty Group Resolution-No User Assigned */

#define EVIMTT_U        1   /* Unable to refresh parameters */
#define EVIMTT_L        2   /* Event Reevaluation Process Loop Exceeded */
#define EVIMTT_A        3   /* Auto Action Failed Execution Failed */
#define EVIMTT_E        4   /* Unexpected Error In Reevaluation Process */
#define EVIMTT_G        5   /* Empty Group Resolution-No User Assigned */
#define EVIMTT_R        6   /* Empty Group Resolution-No User Assigned */

#define _EVIMTT_MAX 6


/* EVST = Event Instance Status */
extern const char EVST[NULL_CODE];

extern const char EVST_A     [NULL_CODE];   /* Active - OK */
extern const char EVST_D     [NULL_CODE];   /* Disabled */
extern const char EVST_U     [NULL_CODE];   /* Unable to Refresh */
extern const char EVST_C     [NULL_CODE];   /* Closed */
extern const char EVST_E     [NULL_CODE];   /* Error */

#define EVSTTT_A        1   /* Active - OK */
#define EVSTTT_D        2   /* Disabled */
#define EVSTTT_U        3   /* Unable to Refresh */
#define EVSTTT_C        4   /* Closed */
#define EVSTTT_E        5   /* Error */

#define _EVSTTT_MAX 5


/* EXCB = Expense Cost Basis */
extern const char EXCB[NULL_CODE];

extern const char EXCB_S     [NULL_CODE];   /* Supplier */
extern const char EXCB_O     [NULL_CODE];   /* Order */

#define EXCBTT_S        1   /* Supplier */
#define EXCBTT_O        2   /* Order */

#define _EXCBTT_MAX 2


/* EXDS = External Data Sources */
extern const char EXDS[NULL_CODE];

extern const char EXDS_VR    [NULL_CODE];   /* Vertex */
extern const char EXDS_VI    [NULL_CODE];   /* Vialink */
extern const char EXDS_NI    [NULL_CODE];   /* Nielsen */
extern const char EXDS_IRI   [NULL_CODE];   /* SymphonyIRI */

#define EXDSTT_VR       1   /* Vertex */
#define EXDSTT_VI       2   /* Vialink */
#define EXDSTT_NI       3   /* Nielsen */
#define EXDSTT_IRI      4   /* SymphonyIRI */

#define _EXDSTT_MAX 4


/* EXPC = Expense Category */
extern const char EXPC[NULL_CODE];

extern const char EXPC_A     [NULL_CODE];   /* Admin */
extern const char EXPC_M     [NULL_CODE];   /* Misc */
extern const char EXPC_B     [NULL_CODE];   /* Backhaul Allowance */
extern const char EXPC_F     [NULL_CODE];   /* Freight */
extern const char EXPC_I     [NULL_CODE];   /* Insurance */

#define EXPCTT_A        1   /* Admin */
#define EXPCTT_M        2   /* Misc */
#define EXPCTT_B        3   /* Backhaul Allowance */
#define EXPCTT_F        4   /* Freight */
#define EXPCTT_I        5   /* Insurance */

#define _EXPCTT_MAX 5


/* EXPT = Expense Type */
extern const char EXPT[NULL_CODE];

extern const char EXPT_Z     [NULL_CODE];   /* Zone */
extern const char EXPT_C     [NULL_CODE];   /* Country */

#define EXPTTT_Z        1   /* Zone */
#define EXPTTT_C        2   /* Country */

#define _EXPTTT_MAX 2


/* EXRT = Exchange Rate Types */
extern const char EXRT[NULL_CODE];

extern const char EXRT_E     [NULL_CODE];   /* Existing */
extern const char EXRT_M     [NULL_CODE];   /* Manual */

#define EXRTTT_E        1   /* Existing */
#define EXRTTT_M        2   /* Manual */

#define _EXRTTT_MAX 2


/* EXSH = EXT Shipment Codes */
extern const char EXSH[NULL_CODE];

extern const char EXSH_AS    [NULL_CODE];   /* ASN No. */
extern const char EXSH_ES    [NULL_CODE];   /* Ext. Ship */

#define EXSHTT_AS       1   /* ASN No. */
#define EXSHTT_ES       2   /* Ext. Ship */

#define _EXSHTT_MAX 2


/* EXST = Sales Audit Export Sub-Type */
extern const char EXST[NULL_CODE];

extern const char EXST_G     [NULL_CODE];   /* Gas */
extern const char EXST_O     [NULL_CODE];   /* Other */

#define EXSTTT_G        1   /* Gas */
#define EXSTTT_O        2   /* Other */

#define _EXSTTT_MAX 2


/* EXTP = Exchange Types */
extern const char EXTP[NULL_CODE];

extern const char EXTP_O     [NULL_CODE];   /* Operational */
extern const char EXTP_C     [NULL_CODE];   /* Consolidation */
extern const char EXTP_L     [NULL_CODE];   /* LC/Bank */
extern const char EXTP_P     [NULL_CODE];   /* Purchase Order */
extern const char EXTP_U     [NULL_CODE];   /* Customs Entry */
extern const char EXTP_T     [NULL_CODE];   /* Transportation */

#define EXTPTT_O        1   /* Operational */
#define EXTPTT_C        2   /* Consolidation */
#define EXTPTT_L        3   /* LC/Bank */
#define EXTPTT_P        4   /* Purchase Order */
#define EXTPTT_U        5   /* Customs Entry */
#define EXTPTT_T        6   /* Transportation */

#define _EXTPTT_MAX 6


/* FAST = Sales Type */
extern const char FAST[NULL_CODE];

extern const char FAST_R     [NULL_CODE];   /* Regular */
extern const char FAST_P     [NULL_CODE];   /* Promotional */
extern const char FAST_C     [NULL_CODE];   /* Clearance */

#define FASTTT_R        1   /* Regular */
#define FASTTT_P        2   /* Promotional */
#define FASTTT_C        3   /* Clearance */

#define _FASTTT_MAX 3


/* FDY4 = Day Frequency 454 Calendar */
extern const char FDY4[NULL_CODE];

extern const char FDY4_0     [NULL_CODE];   /* Every */
extern const char FDY4_1     [NULL_CODE];   /* First */
extern const char FDY4_2     [NULL_CODE];   /* 2nd */
extern const char FDY4_3     [NULL_CODE];   /* 3rd */
extern const char FDY4_4     [NULL_CODE];   /* 4th */
extern const char FDY4_5     [NULL_CODE];   /* 5th */
extern const char FDY4_6     [NULL_CODE];   /* 6th */
extern const char FDY4_7     [NULL_CODE];   /* 7th */
extern const char FDY4_8     [NULL_CODE];   /* 8th */
extern const char FDY4_9     [NULL_CODE];   /* 9th */
extern const char FDY4_10    [NULL_CODE];   /* 10th */
extern const char FDY4_11    [NULL_CODE];   /* 11th */
extern const char FDY4_12    [NULL_CODE];   /* 12th */
extern const char FDY4_13    [NULL_CODE];   /* 13th */
extern const char FDY4_14    [NULL_CODE];   /* 14th */
extern const char FDY4_15    [NULL_CODE];   /* 15th */
extern const char FDY4_16    [NULL_CODE];   /* 16th */
extern const char FDY4_17    [NULL_CODE];   /* 17th */
extern const char FDY4_18    [NULL_CODE];   /* 18th */
extern const char FDY4_19    [NULL_CODE];   /* 19th */
extern const char FDY4_20    [NULL_CODE];   /* 20th */
extern const char FDY4_21    [NULL_CODE];   /* 21st */
extern const char FDY4_22    [NULL_CODE];   /* 22nd */
extern const char FDY4_23    [NULL_CODE];   /* 23rd */
extern const char FDY4_24    [NULL_CODE];   /* 24th */
extern const char FDY4_25    [NULL_CODE];   /* 25th */
extern const char FDY4_26    [NULL_CODE];   /* 26th */
extern const char FDY4_27    [NULL_CODE];   /* 27th */
extern const char FDY4_28    [NULL_CODE];   /* 28th */
extern const char FDY4_29    [NULL_CODE];   /* 29th */
extern const char FDY4_30    [NULL_CODE];   /* 30th */
extern const char FDY4_31    [NULL_CODE];   /* 31st */
extern const char FDY4_32    [NULL_CODE];   /* 32nd */
extern const char FDY4_33    [NULL_CODE];   /* 33rd */
extern const char FDY4_34    [NULL_CODE];   /* 34th */
extern const char FDY4_35    [NULL_CODE];   /* 35th */
extern const char FDY4_36    [NULL_CODE];   /* 36th */
extern const char FDY4_37    [NULL_CODE];   /* 37th */
extern const char FDY4_38    [NULL_CODE];   /* 38th */
extern const char FDY4_39    [NULL_CODE];   /* 39th */
extern const char FDY4_40    [NULL_CODE];   /* 40th */
extern const char FDY4_41    [NULL_CODE];   /* 41st */
extern const char FDY4_42    [NULL_CODE];   /* 42nd */
extern const char FDY4_43    [NULL_CODE];   /* 43rd */
extern const char FDY4_44    [NULL_CODE];   /* 44th */
extern const char FDY4_45    [NULL_CODE];   /* 45th */
extern const char FDY4_46    [NULL_CODE];   /* 46th */
extern const char FDY4_47    [NULL_CODE];   /* 47th */
extern const char FDY4_48    [NULL_CODE];   /* 48th */
extern const char FDY4_49    [NULL_CODE];   /* 49th */
extern const char FDY4_50    [NULL_CODE];   /* 50th */
extern const char FDY4_51    [NULL_CODE];   /* 51st */
extern const char FDY4_52    [NULL_CODE];   /* 52nd */
extern const char FDY4_53    [NULL_CODE];   /* 53rd */
extern const char FDY4_54    [NULL_CODE];   /* 54th */
extern const char FDY4_55    [NULL_CODE];   /* 55th */
extern const char FDY4_56    [NULL_CODE];   /* 56th */
extern const char FDY4_57    [NULL_CODE];   /* 57th */
extern const char FDY4_58    [NULL_CODE];   /* 58th */
extern const char FDY4_59    [NULL_CODE];   /* 59th */
extern const char FDY4_60    [NULL_CODE];   /* 60th */
extern const char FDY4_61    [NULL_CODE];   /* 61st */
extern const char FDY4_62    [NULL_CODE];   /* 62nd */
extern const char FDY4_63    [NULL_CODE];   /* 63rd */
extern const char FDY4_64    [NULL_CODE];   /* 64th */
extern const char FDY4_65    [NULL_CODE];   /* 65th */
extern const char FDY4_66    [NULL_CODE];   /* 66th */
extern const char FDY4_67    [NULL_CODE];   /* 67th */
extern const char FDY4_68    [NULL_CODE];   /* 68th */
extern const char FDY4_69    [NULL_CODE];   /* 69th */
extern const char FDY4_70    [NULL_CODE];   /* 70th */
extern const char FDY4_71    [NULL_CODE];   /* 71st */
extern const char FDY4_72    [NULL_CODE];   /* 72nd */
extern const char FDY4_73    [NULL_CODE];   /* 73rd */
extern const char FDY4_74    [NULL_CODE];   /* 74th */
extern const char FDY4_75    [NULL_CODE];   /* 75th */
extern const char FDY4_76    [NULL_CODE];   /* 76th */
extern const char FDY4_77    [NULL_CODE];   /* 77th */
extern const char FDY4_78    [NULL_CODE];   /* 78th */
extern const char FDY4_79    [NULL_CODE];   /* 79th */
extern const char FDY4_80    [NULL_CODE];   /* 80th */
extern const char FDY4_81    [NULL_CODE];   /* 81st */
extern const char FDY4_82    [NULL_CODE];   /* 82nd */
extern const char FDY4_83    [NULL_CODE];   /* 83rd */
extern const char FDY4_84    [NULL_CODE];   /* 84th */
extern const char FDY4_85    [NULL_CODE];   /* 85th */
extern const char FDY4_86    [NULL_CODE];   /* 86th */
extern const char FDY4_87    [NULL_CODE];   /* 87th */
extern const char FDY4_88    [NULL_CODE];   /* 88th */
extern const char FDY4_89    [NULL_CODE];   /* 89th */
extern const char FDY4_90    [NULL_CODE];   /* 90th */
extern const char FDY4_91    [NULL_CODE];   /* 91st */
extern const char FDY4_92    [NULL_CODE];   /* 92nd */
extern const char FDY4_93    [NULL_CODE];   /* 93rd */
extern const char FDY4_94    [NULL_CODE];   /* 94th */
extern const char FDY4_95    [NULL_CODE];   /* 95th */
extern const char FDY4_96    [NULL_CODE];   /* 96th */
extern const char FDY4_97    [NULL_CODE];   /* 97th */
extern const char FDY4_98    [NULL_CODE];   /* 98th */
extern const char FDY4_99    [NULL_CODE];   /* 99th */
extern const char FDY4_100   [NULL_CODE];   /* 100th */
extern const char FDY4_101   [NULL_CODE];   /* 101st */
extern const char FDY4_102   [NULL_CODE];   /* 102nd */
extern const char FDY4_103   [NULL_CODE];   /* 103rd */
extern const char FDY4_104   [NULL_CODE];   /* 104th */
extern const char FDY4_105   [NULL_CODE];   /* 105th */
extern const char FDY4_106   [NULL_CODE];   /* 106th */
extern const char FDY4_107   [NULL_CODE];   /* 107th */
extern const char FDY4_108   [NULL_CODE];   /* 108th */
extern const char FDY4_109   [NULL_CODE];   /* 109th */
extern const char FDY4_110   [NULL_CODE];   /* 110th */
extern const char FDY4_111   [NULL_CODE];   /* 111th */
extern const char FDY4_112   [NULL_CODE];   /* 112th */
extern const char FDY4_113   [NULL_CODE];   /* 113th */
extern const char FDY4_114   [NULL_CODE];   /* 114th */
extern const char FDY4_115   [NULL_CODE];   /* 115th */
extern const char FDY4_116   [NULL_CODE];   /* 116th */
extern const char FDY4_117   [NULL_CODE];   /* 117th */
extern const char FDY4_118   [NULL_CODE];   /* 118th */
extern const char FDY4_119   [NULL_CODE];   /* 119th */
extern const char FDY4_120   [NULL_CODE];   /* 120th */
extern const char FDY4_121   [NULL_CODE];   /* 121st */
extern const char FDY4_122   [NULL_CODE];   /* 122nd */
extern const char FDY4_123   [NULL_CODE];   /* 123rd */
extern const char FDY4_124   [NULL_CODE];   /* 124th */
extern const char FDY4_125   [NULL_CODE];   /* 125th */
extern const char FDY4_126   [NULL_CODE];   /* 126th */
extern const char FDY4_127   [NULL_CODE];   /* 127th */
extern const char FDY4_128   [NULL_CODE];   /* 128th */
extern const char FDY4_129   [NULL_CODE];   /* 129th */
extern const char FDY4_130   [NULL_CODE];   /* 130th */
extern const char FDY4_131   [NULL_CODE];   /* 131st */
extern const char FDY4_132   [NULL_CODE];   /* 132nd */
extern const char FDY4_133   [NULL_CODE];   /* 133rd */
extern const char FDY4_134   [NULL_CODE];   /* 134th */
extern const char FDY4_135   [NULL_CODE];   /* 135th */
extern const char FDY4_136   [NULL_CODE];   /* 136th */
extern const char FDY4_137   [NULL_CODE];   /* 137th */
extern const char FDY4_138   [NULL_CODE];   /* 138th */
extern const char FDY4_139   [NULL_CODE];   /* 139th */
extern const char FDY4_140   [NULL_CODE];   /* 140th */
extern const char FDY4_141   [NULL_CODE];   /* 141st */
extern const char FDY4_142   [NULL_CODE];   /* 142nd */
extern const char FDY4_143   [NULL_CODE];   /* 143rd */
extern const char FDY4_144   [NULL_CODE];   /* 144th */
extern const char FDY4_145   [NULL_CODE];   /* 145th */
extern const char FDY4_146   [NULL_CODE];   /* 146th */
extern const char FDY4_147   [NULL_CODE];   /* 147th */
extern const char FDY4_148   [NULL_CODE];   /* 148th */
extern const char FDY4_149   [NULL_CODE];   /* 149th */
extern const char FDY4_150   [NULL_CODE];   /* 150th */
extern const char FDY4_151   [NULL_CODE];   /* 151st */
extern const char FDY4_152   [NULL_CODE];   /* 152nd */
extern const char FDY4_153   [NULL_CODE];   /* 153rd */
extern const char FDY4_154   [NULL_CODE];   /* 154th */
extern const char FDY4_155   [NULL_CODE];   /* 155th */
extern const char FDY4_156   [NULL_CODE];   /* 156th */
extern const char FDY4_157   [NULL_CODE];   /* 157th */
extern const char FDY4_158   [NULL_CODE];   /* 158th */
extern const char FDY4_159   [NULL_CODE];   /* 159th */
extern const char FDY4_160   [NULL_CODE];   /* 160th */
extern const char FDY4_161   [NULL_CODE];   /* 161st */
extern const char FDY4_162   [NULL_CODE];   /* 162nd */
extern const char FDY4_163   [NULL_CODE];   /* 163rd */
extern const char FDY4_164   [NULL_CODE];   /* 164th */
extern const char FDY4_165   [NULL_CODE];   /* 165th */
extern const char FDY4_166   [NULL_CODE];   /* 166th */
extern const char FDY4_167   [NULL_CODE];   /* 167th */
extern const char FDY4_168   [NULL_CODE];   /* 168th */
extern const char FDY4_169   [NULL_CODE];   /* 169th */
extern const char FDY4_170   [NULL_CODE];   /* 170th */
extern const char FDY4_171   [NULL_CODE];   /* 171st */
extern const char FDY4_172   [NULL_CODE];   /* 172nd */
extern const char FDY4_173   [NULL_CODE];   /* 173rd */
extern const char FDY4_174   [NULL_CODE];   /* 174th */
extern const char FDY4_175   [NULL_CODE];   /* 175th */
extern const char FDY4_176   [NULL_CODE];   /* 176th */
extern const char FDY4_177   [NULL_CODE];   /* 177th */
extern const char FDY4_178   [NULL_CODE];   /* 178th */
extern const char FDY4_179   [NULL_CODE];   /* 179th */
extern const char FDY4_180   [NULL_CODE];   /* 180th */
extern const char FDY4_181   [NULL_CODE];   /* 181st */
extern const char FDY4_182   [NULL_CODE];   /* 182nd */
extern const char FDY4_183   [NULL_CODE];   /* 183rd */
extern const char FDY4_184   [NULL_CODE];   /* 184th */
extern const char FDY4_185   [NULL_CODE];   /* 185th */
extern const char FDY4_186   [NULL_CODE];   /* 186th */
extern const char FDY4_187   [NULL_CODE];   /* 187th */
extern const char FDY4_188   [NULL_CODE];   /* 188th */
extern const char FDY4_189   [NULL_CODE];   /* 189th */
extern const char FDY4_999   [NULL_CODE];   /* Last */

#define FDY4TT_0        1   /* Every */
#define FDY4TT_1        2   /* First */
#define FDY4TT_2        3   /* 2nd */
#define FDY4TT_3        4   /* 3rd */
#define FDY4TT_4        5   /* 4th */
#define FDY4TT_5        6   /* 5th */
#define FDY4TT_6        7   /* 6th */
#define FDY4TT_7        8   /* 7th */
#define FDY4TT_8        9   /* 8th */
#define FDY4TT_9       10   /* 9th */
#define FDY4TT_10      11   /* 10th */
#define FDY4TT_11      12   /* 11th */
#define FDY4TT_12      13   /* 12th */
#define FDY4TT_13      14   /* 13th */
#define FDY4TT_14      15   /* 14th */
#define FDY4TT_15      16   /* 15th */
#define FDY4TT_16      17   /* 16th */
#define FDY4TT_17      18   /* 17th */
#define FDY4TT_18      19   /* 18th */
#define FDY4TT_19      20   /* 19th */
#define FDY4TT_20      21   /* 20th */
#define FDY4TT_21      22   /* 21st */
#define FDY4TT_22      23   /* 22nd */
#define FDY4TT_23      24   /* 23rd */
#define FDY4TT_24      25   /* 24th */
#define FDY4TT_25      26   /* 25th */
#define FDY4TT_26      27   /* 26th */
#define FDY4TT_27      28   /* 27th */
#define FDY4TT_28      29   /* 28th */
#define FDY4TT_29      30   /* 29th */
#define FDY4TT_30      31   /* 30th */
#define FDY4TT_31      32   /* 31st */
#define FDY4TT_32      33   /* 32nd */
#define FDY4TT_33      34   /* 33rd */
#define FDY4TT_34      35   /* 34th */
#define FDY4TT_35      36   /* 35th */
#define FDY4TT_36      37   /* 36th */
#define FDY4TT_37      38   /* 37th */
#define FDY4TT_38      39   /* 38th */
#define FDY4TT_39      40   /* 39th */
#define FDY4TT_40      41   /* 40th */
#define FDY4TT_41      42   /* 41st */
#define FDY4TT_42      43   /* 42nd */
#define FDY4TT_43      44   /* 43rd */
#define FDY4TT_44      45   /* 44th */
#define FDY4TT_45      46   /* 45th */
#define FDY4TT_46      47   /* 46th */
#define FDY4TT_47      48   /* 47th */
#define FDY4TT_48      49   /* 48th */
#define FDY4TT_49      50   /* 49th */
#define FDY4TT_50      51   /* 50th */
#define FDY4TT_51      52   /* 51st */
#define FDY4TT_52      53   /* 52nd */
#define FDY4TT_53      54   /* 53rd */
#define FDY4TT_54      55   /* 54th */
#define FDY4TT_55      56   /* 55th */
#define FDY4TT_56      57   /* 56th */
#define FDY4TT_57      58   /* 57th */
#define FDY4TT_58      59   /* 58th */
#define FDY4TT_59      60   /* 59th */
#define FDY4TT_60      61   /* 60th */
#define FDY4TT_61      62   /* 61st */
#define FDY4TT_62      63   /* 62nd */
#define FDY4TT_63      64   /* 63rd */
#define FDY4TT_64      65   /* 64th */
#define FDY4TT_65      66   /* 65th */
#define FDY4TT_66      67   /* 66th */
#define FDY4TT_67      68   /* 67th */
#define FDY4TT_68      69   /* 68th */
#define FDY4TT_69      70   /* 69th */
#define FDY4TT_70      71   /* 70th */
#define FDY4TT_71      72   /* 71st */
#define FDY4TT_72      73   /* 72nd */
#define FDY4TT_73      74   /* 73rd */
#define FDY4TT_74      75   /* 74th */
#define FDY4TT_75      76   /* 75th */
#define FDY4TT_76      77   /* 76th */
#define FDY4TT_77      78   /* 77th */
#define FDY4TT_78      79   /* 78th */
#define FDY4TT_79      80   /* 79th */
#define FDY4TT_80      81   /* 80th */
#define FDY4TT_81      82   /* 81st */
#define FDY4TT_82      83   /* 82nd */
#define FDY4TT_83      84   /* 83rd */
#define FDY4TT_84      85   /* 84th */
#define FDY4TT_85      86   /* 85th */
#define FDY4TT_86      87   /* 86th */
#define FDY4TT_87      88   /* 87th */
#define FDY4TT_88      89   /* 88th */
#define FDY4TT_89      90   /* 89th */
#define FDY4TT_90      91   /* 90th */
#define FDY4TT_91      92   /* 91st */
#define FDY4TT_92      93   /* 92nd */
#define FDY4TT_93      94   /* 93rd */
#define FDY4TT_94      95   /* 94th */
#define FDY4TT_95      96   /* 95th */
#define FDY4TT_96      97   /* 96th */
#define FDY4TT_97      98   /* 97th */
#define FDY4TT_98      99   /* 98th */
#define FDY4TT_99     100   /* 99th */
#define FDY4TT_100    101   /* 100th */
#define FDY4TT_101    102   /* 101st */
#define FDY4TT_102    103   /* 102nd */
#define FDY4TT_103    104   /* 103rd */
#define FDY4TT_104    105   /* 104th */
#define FDY4TT_105    106   /* 105th */
#define FDY4TT_106    107   /* 106th */
#define FDY4TT_107    108   /* 107th */
#define FDY4TT_108    109   /* 108th */
#define FDY4TT_109    110   /* 109th */
#define FDY4TT_110    111   /* 110th */
#define FDY4TT_111    112   /* 111th */
#define FDY4TT_112    113   /* 112th */
#define FDY4TT_113    114   /* 113th */
#define FDY4TT_114    115   /* 114th */
#define FDY4TT_115    116   /* 115th */
#define FDY4TT_116    117   /* 116th */
#define FDY4TT_117    118   /* 117th */
#define FDY4TT_118    119   /* 118th */
#define FDY4TT_119    120   /* 119th */
#define FDY4TT_120    121   /* 120th */
#define FDY4TT_121    122   /* 121st */
#define FDY4TT_122    123   /* 122nd */
#define FDY4TT_123    124   /* 123rd */
#define FDY4TT_124    125   /* 124th */
#define FDY4TT_125    126   /* 125th */
#define FDY4TT_126    127   /* 126th */
#define FDY4TT_127    128   /* 127th */
#define FDY4TT_128    129   /* 128th */
#define FDY4TT_129    130   /* 129th */
#define FDY4TT_130    131   /* 130th */
#define FDY4TT_131    132   /* 131st */
#define FDY4TT_132    133   /* 132nd */
#define FDY4TT_133    134   /* 133rd */
#define FDY4TT_134    135   /* 134th */
#define FDY4TT_135    136   /* 135th */
#define FDY4TT_136    137   /* 136th */
#define FDY4TT_137    138   /* 137th */
#define FDY4TT_138    139   /* 138th */
#define FDY4TT_139    140   /* 139th */
#define FDY4TT_140    141   /* 140th */
#define FDY4TT_141    142   /* 141st */
#define FDY4TT_142    143   /* 142nd */
#define FDY4TT_143    144   /* 143rd */
#define FDY4TT_144    145   /* 144th */
#define FDY4TT_145    146   /* 145th */
#define FDY4TT_146    147   /* 146th */
#define FDY4TT_147    148   /* 147th */
#define FDY4TT_148    149   /* 148th */
#define FDY4TT_149    150   /* 149th */
#define FDY4TT_150    151   /* 150th */
#define FDY4TT_151    152   /* 151st */
#define FDY4TT_152    153   /* 152nd */
#define FDY4TT_153    154   /* 153rd */
#define FDY4TT_154    155   /* 154th */
#define FDY4TT_155    156   /* 155th */
#define FDY4TT_156    157   /* 156th */
#define FDY4TT_157    158   /* 157th */
#define FDY4TT_158    159   /* 158th */
#define FDY4TT_159    160   /* 159th */
#define FDY4TT_160    161   /* 160th */
#define FDY4TT_161    162   /* 161st */
#define FDY4TT_162    163   /* 162nd */
#define FDY4TT_163    164   /* 163rd */
#define FDY4TT_164    165   /* 164th */
#define FDY4TT_165    166   /* 165th */
#define FDY4TT_166    167   /* 166th */
#define FDY4TT_167    168   /* 167th */
#define FDY4TT_168    169   /* 168th */
#define FDY4TT_169    170   /* 169th */
#define FDY4TT_170    171   /* 170th */
#define FDY4TT_171    172   /* 171st */
#define FDY4TT_172    173   /* 172nd */
#define FDY4TT_173    174   /* 173rd */
#define FDY4TT_174    175   /* 174th */
#define FDY4TT_175    176   /* 175th */
#define FDY4TT_176    177   /* 176th */
#define FDY4TT_177    178   /* 177th */
#define FDY4TT_178    179   /* 178th */
#define FDY4TT_179    180   /* 179th */
#define FDY4TT_180    181   /* 180th */
#define FDY4TT_181    182   /* 181st */
#define FDY4TT_182    183   /* 182nd */
#define FDY4TT_183    184   /* 183rd */
#define FDY4TT_184    185   /* 184th */
#define FDY4TT_185    186   /* 185th */
#define FDY4TT_186    187   /* 186th */
#define FDY4TT_187    188   /* 187th */
#define FDY4TT_188    189   /* 188th */
#define FDY4TT_189    190   /* 189th */
#define FDY4TT_999    191   /* Last */

#define _FDY4TT_MAX 191


/* FDYC = Day Frequency Normal Calendar */
extern const char FDYC[NULL_CODE];

extern const char FDYC_0     [NULL_CODE];   /* Every */
extern const char FDYC_1     [NULL_CODE];   /* First */
extern const char FDYC_2     [NULL_CODE];   /* 2nd */
extern const char FDYC_3     [NULL_CODE];   /* 3rd */
extern const char FDYC_4     [NULL_CODE];   /* 4th */
extern const char FDYC_5     [NULL_CODE];   /* 5th */
extern const char FDYC_6     [NULL_CODE];   /* 6th */
extern const char FDYC_7     [NULL_CODE];   /* 7th */
extern const char FDYC_8     [NULL_CODE];   /* 8th */
extern const char FDYC_9     [NULL_CODE];   /* 9th */
extern const char FDYC_10    [NULL_CODE];   /* 10th */
extern const char FDYC_11    [NULL_CODE];   /* 11th */
extern const char FDYC_12    [NULL_CODE];   /* 12th */
extern const char FDYC_13    [NULL_CODE];   /* 13th */
extern const char FDYC_14    [NULL_CODE];   /* 14th */
extern const char FDYC_15    [NULL_CODE];   /* 15th */
extern const char FDYC_16    [NULL_CODE];   /* 16th */
extern const char FDYC_17    [NULL_CODE];   /* 17th */
extern const char FDYC_18    [NULL_CODE];   /* 18th */
extern const char FDYC_19    [NULL_CODE];   /* 19th */
extern const char FDYC_20    [NULL_CODE];   /* 20th */
extern const char FDYC_21    [NULL_CODE];   /* 21st */
extern const char FDYC_22    [NULL_CODE];   /* 22nd */
extern const char FDYC_23    [NULL_CODE];   /* 23rd */
extern const char FDYC_24    [NULL_CODE];   /* 24th */
extern const char FDYC_25    [NULL_CODE];   /* 25th */
extern const char FDYC_26    [NULL_CODE];   /* 26th */
extern const char FDYC_27    [NULL_CODE];   /* 27th */
extern const char FDYC_28    [NULL_CODE];   /* 28th */
extern const char FDYC_29    [NULL_CODE];   /* 29th */
extern const char FDYC_30    [NULL_CODE];   /* 30th */
extern const char FDYC_31    [NULL_CODE];   /* 31st */
extern const char FDYC_32    [NULL_CODE];   /* 32nd */
extern const char FDYC_33    [NULL_CODE];   /* 33rd */
extern const char FDYC_34    [NULL_CODE];   /* 34th */
extern const char FDYC_35    [NULL_CODE];   /* 35th */
extern const char FDYC_36    [NULL_CODE];   /* 36th */
extern const char FDYC_37    [NULL_CODE];   /* 37th */
extern const char FDYC_38    [NULL_CODE];   /* 38th */
extern const char FDYC_39    [NULL_CODE];   /* 39th */
extern const char FDYC_40    [NULL_CODE];   /* 40th */
extern const char FDYC_41    [NULL_CODE];   /* 41st */
extern const char FDYC_42    [NULL_CODE];   /* 42nd */
extern const char FDYC_43    [NULL_CODE];   /* 43rd */
extern const char FDYC_44    [NULL_CODE];   /* 44th */
extern const char FDYC_45    [NULL_CODE];   /* 45th */
extern const char FDYC_46    [NULL_CODE];   /* 46th */
extern const char FDYC_47    [NULL_CODE];   /* 47th */
extern const char FDYC_48    [NULL_CODE];   /* 48th */
extern const char FDYC_49    [NULL_CODE];   /* 49th */
extern const char FDYC_50    [NULL_CODE];   /* 50th */
extern const char FDYC_51    [NULL_CODE];   /* 51st */
extern const char FDYC_52    [NULL_CODE];   /* 52nd */
extern const char FDYC_53    [NULL_CODE];   /* 53rd */
extern const char FDYC_54    [NULL_CODE];   /* 54th */
extern const char FDYC_55    [NULL_CODE];   /* 55th */
extern const char FDYC_56    [NULL_CODE];   /* 56th */
extern const char FDYC_57    [NULL_CODE];   /* 57th */
extern const char FDYC_58    [NULL_CODE];   /* 58th */
extern const char FDYC_59    [NULL_CODE];   /* 59th */
extern const char FDYC_60    [NULL_CODE];   /* 60th */
extern const char FDYC_61    [NULL_CODE];   /* 61st */
extern const char FDYC_62    [NULL_CODE];   /* 62nd */
extern const char FDYC_63    [NULL_CODE];   /* 63rd */
extern const char FDYC_64    [NULL_CODE];   /* 64th */
extern const char FDYC_65    [NULL_CODE];   /* 65th */
extern const char FDYC_66    [NULL_CODE];   /* 66th */
extern const char FDYC_67    [NULL_CODE];   /* 67th */
extern const char FDYC_68    [NULL_CODE];   /* 68th */
extern const char FDYC_69    [NULL_CODE];   /* 69th */
extern const char FDYC_70    [NULL_CODE];   /* 70th */
extern const char FDYC_71    [NULL_CODE];   /* 71st */
extern const char FDYC_72    [NULL_CODE];   /* 72nd */
extern const char FDYC_73    [NULL_CODE];   /* 73rd */
extern const char FDYC_74    [NULL_CODE];   /* 74th */
extern const char FDYC_75    [NULL_CODE];   /* 75th */
extern const char FDYC_76    [NULL_CODE];   /* 76th */
extern const char FDYC_77    [NULL_CODE];   /* 77th */
extern const char FDYC_78    [NULL_CODE];   /* 78th */
extern const char FDYC_79    [NULL_CODE];   /* 79th */
extern const char FDYC_80    [NULL_CODE];   /* 80th */
extern const char FDYC_81    [NULL_CODE];   /* 81st */
extern const char FDYC_82    [NULL_CODE];   /* 82nd */
extern const char FDYC_83    [NULL_CODE];   /* 83rd */
extern const char FDYC_84    [NULL_CODE];   /* 84th */
extern const char FDYC_85    [NULL_CODE];   /* 85th */
extern const char FDYC_86    [NULL_CODE];   /* 86th */
extern const char FDYC_87    [NULL_CODE];   /* 87th */
extern const char FDYC_88    [NULL_CODE];   /* 88th */
extern const char FDYC_89    [NULL_CODE];   /* 89th */
extern const char FDYC_90    [NULL_CODE];   /* 90th */
extern const char FDYC_91    [NULL_CODE];   /* 91st */
extern const char FDYC_92    [NULL_CODE];   /* 92nd */
extern const char FDYC_93    [NULL_CODE];   /* 93rd */
extern const char FDYC_94    [NULL_CODE];   /* 94th */
extern const char FDYC_95    [NULL_CODE];   /* 95th */
extern const char FDYC_96    [NULL_CODE];   /* 96th */
extern const char FDYC_97    [NULL_CODE];   /* 97th */
extern const char FDYC_98    [NULL_CODE];   /* 98th */
extern const char FDYC_99    [NULL_CODE];   /* 99th */
extern const char FDYC_100   [NULL_CODE];   /* 100th */
extern const char FDYC_101   [NULL_CODE];   /* 101st */
extern const char FDYC_102   [NULL_CODE];   /* 102nd */
extern const char FDYC_103   [NULL_CODE];   /* 103rd */
extern const char FDYC_104   [NULL_CODE];   /* 104th */
extern const char FDYC_105   [NULL_CODE];   /* 105th */
extern const char FDYC_106   [NULL_CODE];   /* 106th */
extern const char FDYC_107   [NULL_CODE];   /* 107th */
extern const char FDYC_108   [NULL_CODE];   /* 108th */
extern const char FDYC_109   [NULL_CODE];   /* 109th */
extern const char FDYC_110   [NULL_CODE];   /* 110th */
extern const char FDYC_111   [NULL_CODE];   /* 111th */
extern const char FDYC_112   [NULL_CODE];   /* 112th */
extern const char FDYC_113   [NULL_CODE];   /* 113th */
extern const char FDYC_114   [NULL_CODE];   /* 114th */
extern const char FDYC_115   [NULL_CODE];   /* 115th */
extern const char FDYC_116   [NULL_CODE];   /* 116th */
extern const char FDYC_117   [NULL_CODE];   /* 117th */
extern const char FDYC_118   [NULL_CODE];   /* 118th */
extern const char FDYC_119   [NULL_CODE];   /* 119th */
extern const char FDYC_120   [NULL_CODE];   /* 120th */
extern const char FDYC_121   [NULL_CODE];   /* 121st */
extern const char FDYC_122   [NULL_CODE];   /* 122nd */
extern const char FDYC_123   [NULL_CODE];   /* 123rd */
extern const char FDYC_124   [NULL_CODE];   /* 124th */
extern const char FDYC_125   [NULL_CODE];   /* 125th */
extern const char FDYC_126   [NULL_CODE];   /* 126th */
extern const char FDYC_127   [NULL_CODE];   /* 127th */
extern const char FDYC_128   [NULL_CODE];   /* 128th */
extern const char FDYC_129   [NULL_CODE];   /* 129th */
extern const char FDYC_130   [NULL_CODE];   /* 130th */
extern const char FDYC_131   [NULL_CODE];   /* 131st */
extern const char FDYC_132   [NULL_CODE];   /* 132nd */
extern const char FDYC_133   [NULL_CODE];   /* 133rd */
extern const char FDYC_134   [NULL_CODE];   /* 134th */
extern const char FDYC_135   [NULL_CODE];   /* 135th */
extern const char FDYC_136   [NULL_CODE];   /* 136th */
extern const char FDYC_137   [NULL_CODE];   /* 137th */
extern const char FDYC_138   [NULL_CODE];   /* 138th */
extern const char FDYC_139   [NULL_CODE];   /* 139th */
extern const char FDYC_140   [NULL_CODE];   /* 140th */
extern const char FDYC_141   [NULL_CODE];   /* 141st */
extern const char FDYC_142   [NULL_CODE];   /* 142nd */
extern const char FDYC_143   [NULL_CODE];   /* 143rd */
extern const char FDYC_144   [NULL_CODE];   /* 144th */
extern const char FDYC_145   [NULL_CODE];   /* 145th */
extern const char FDYC_146   [NULL_CODE];   /* 146th */
extern const char FDYC_147   [NULL_CODE];   /* 147th */
extern const char FDYC_148   [NULL_CODE];   /* 148th */
extern const char FDYC_149   [NULL_CODE];   /* 149th */
extern const char FDYC_150   [NULL_CODE];   /* 150th */
extern const char FDYC_151   [NULL_CODE];   /* 151st */
extern const char FDYC_152   [NULL_CODE];   /* 152nd */
extern const char FDYC_153   [NULL_CODE];   /* 153rd */
extern const char FDYC_154   [NULL_CODE];   /* 154th */
extern const char FDYC_155   [NULL_CODE];   /* 155th */
extern const char FDYC_156   [NULL_CODE];   /* 156th */
extern const char FDYC_157   [NULL_CODE];   /* 157th */
extern const char FDYC_158   [NULL_CODE];   /* 158th */
extern const char FDYC_159   [NULL_CODE];   /* 159th */
extern const char FDYC_160   [NULL_CODE];   /* 160th */
extern const char FDYC_161   [NULL_CODE];   /* 161st */
extern const char FDYC_162   [NULL_CODE];   /* 162nd */
extern const char FDYC_163   [NULL_CODE];   /* 163rd */
extern const char FDYC_164   [NULL_CODE];   /* 164th */
extern const char FDYC_165   [NULL_CODE];   /* 165th */
extern const char FDYC_166   [NULL_CODE];   /* 166th */
extern const char FDYC_167   [NULL_CODE];   /* 167th */
extern const char FDYC_168   [NULL_CODE];   /* 168th */
extern const char FDYC_169   [NULL_CODE];   /* 169th */
extern const char FDYC_170   [NULL_CODE];   /* 170th */
extern const char FDYC_171   [NULL_CODE];   /* 171st */
extern const char FDYC_172   [NULL_CODE];   /* 172nd */
extern const char FDYC_173   [NULL_CODE];   /* 173rd */
extern const char FDYC_174   [NULL_CODE];   /* 174th */
extern const char FDYC_175   [NULL_CODE];   /* 175th */
extern const char FDYC_176   [NULL_CODE];   /* 176th */
extern const char FDYC_177   [NULL_CODE];   /* 177th */
extern const char FDYC_178   [NULL_CODE];   /* 178th */
extern const char FDYC_179   [NULL_CODE];   /* 179th */
extern const char FDYC_180   [NULL_CODE];   /* 180th */
extern const char FDYC_181   [NULL_CODE];   /* 181st */
extern const char FDYC_182   [NULL_CODE];   /* 182nd */
extern const char FDYC_183   [NULL_CODE];   /* 183rd */
extern const char FDYC_184   [NULL_CODE];   /* 184th */
extern const char FDYC_185   [NULL_CODE];   /* 185th */
extern const char FDYC_186   [NULL_CODE];   /* 186th */
extern const char FDYC_187   [NULL_CODE];   /* 187th */
extern const char FDYC_188   [NULL_CODE];   /* 188th */
extern const char FDYC_189   [NULL_CODE];   /* 189th */
extern const char FDYC_999   [NULL_CODE];   /* Last */

#define FDYCTT_0        1   /* Every */
#define FDYCTT_1        9   /* First */
#define FDYCTT_2       10   /* 2nd */
#define FDYCTT_3       11   /* 3rd */
#define FDYCTT_4       12   /* 4th */
#define FDYCTT_5       13   /* 5th */
#define FDYCTT_6       14   /* 6th */
#define FDYCTT_7       15   /* 7th */
#define FDYCTT_8       16   /* 8th */
#define FDYCTT_9       17   /* 9th */
#define FDYCTT_10      18   /* 10th */
#define FDYCTT_11      19   /* 11th */
#define FDYCTT_12      20   /* 12th */
#define FDYCTT_13      21   /* 13th */
#define FDYCTT_14      22   /* 14th */
#define FDYCTT_15      23   /* 15th */
#define FDYCTT_16      24   /* 16th */
#define FDYCTT_17      25   /* 17th */
#define FDYCTT_18      26   /* 18th */
#define FDYCTT_19      27   /* 19th */
#define FDYCTT_20      28   /* 20th */
#define FDYCTT_21      29   /* 21st */
#define FDYCTT_22      30   /* 22nd */
#define FDYCTT_23      31   /* 23rd */
#define FDYCTT_24      32   /* 24th */
#define FDYCTT_25      33   /* 25th */
#define FDYCTT_26      34   /* 26th */
#define FDYCTT_27      35   /* 27th */
#define FDYCTT_28      36   /* 28th */
#define FDYCTT_29      37   /* 29th */
#define FDYCTT_30      38   /* 30th */
#define FDYCTT_31      39   /* 31st */
#define FDYCTT_32      40   /* 32nd */
#define FDYCTT_33      41   /* 33rd */
#define FDYCTT_34      42   /* 34th */
#define FDYCTT_35      43   /* 35th */
#define FDYCTT_36      44   /* 36th */
#define FDYCTT_37      45   /* 37th */
#define FDYCTT_38      46   /* 38th */
#define FDYCTT_39      47   /* 39th */
#define FDYCTT_40      48   /* 40th */
#define FDYCTT_41      49   /* 41st */
#define FDYCTT_42      50   /* 42nd */
#define FDYCTT_43      51   /* 43rd */
#define FDYCTT_44      52   /* 44th */
#define FDYCTT_45      53   /* 45th */
#define FDYCTT_46      54   /* 46th */
#define FDYCTT_47      55   /* 47th */
#define FDYCTT_48      56   /* 48th */
#define FDYCTT_49      57   /* 49th */
#define FDYCTT_50      58   /* 50th */
#define FDYCTT_51      59   /* 51st */
#define FDYCTT_52      60   /* 52nd */
#define FDYCTT_53      61   /* 53rd */
#define FDYCTT_54      62   /* 54th */
#define FDYCTT_55      63   /* 55th */
#define FDYCTT_56      64   /* 56th */
#define FDYCTT_57      65   /* 57th */
#define FDYCTT_58      66   /* 58th */
#define FDYCTT_59      67   /* 59th */
#define FDYCTT_60      68   /* 60th */
#define FDYCTT_61      69   /* 61st */
#define FDYCTT_62      70   /* 62nd */
#define FDYCTT_63      71   /* 63rd */
#define FDYCTT_64      72   /* 64th */
#define FDYCTT_65      73   /* 65th */
#define FDYCTT_66      74   /* 66th */
#define FDYCTT_67      75   /* 67th */
#define FDYCTT_68      76   /* 68th */
#define FDYCTT_69      77   /* 69th */
#define FDYCTT_70      78   /* 70th */
#define FDYCTT_71      79   /* 71st */
#define FDYCTT_72      80   /* 72nd */
#define FDYCTT_73      81   /* 73rd */
#define FDYCTT_74      82   /* 74th */
#define FDYCTT_75      83   /* 75th */
#define FDYCTT_76      84   /* 76th */
#define FDYCTT_77      85   /* 77th */
#define FDYCTT_78      86   /* 78th */
#define FDYCTT_79      87   /* 79th */
#define FDYCTT_80      88   /* 80th */
#define FDYCTT_81      89   /* 81st */
#define FDYCTT_82      90   /* 82nd */
#define FDYCTT_83      91   /* 83rd */
#define FDYCTT_84      92   /* 84th */
#define FDYCTT_85      93   /* 85th */
#define FDYCTT_86      94   /* 86th */
#define FDYCTT_87      95   /* 87th */
#define FDYCTT_88      96   /* 88th */
#define FDYCTT_89      97   /* 89th */
#define FDYCTT_90      98   /* 90th */
#define FDYCTT_91      99   /* 91st */
#define FDYCTT_92     100   /* 92nd */
#define FDYCTT_93     101   /* 93rd */
#define FDYCTT_94     102   /* 94th */
#define FDYCTT_95     103   /* 95th */
#define FDYCTT_96     104   /* 96th */
#define FDYCTT_97     105   /* 97th */
#define FDYCTT_98     106   /* 98th */
#define FDYCTT_99     107   /* 99th */
#define FDYCTT_100    108   /* 100th */
#define FDYCTT_101    109   /* 101st */
#define FDYCTT_102    110   /* 102nd */
#define FDYCTT_103    111   /* 103rd */
#define FDYCTT_104    112   /* 104th */
#define FDYCTT_105    113   /* 105th */
#define FDYCTT_106    114   /* 106th */
#define FDYCTT_107    115   /* 107th */
#define FDYCTT_108    116   /* 108th */
#define FDYCTT_109    117   /* 109th */
#define FDYCTT_110    118   /* 110th */
#define FDYCTT_111    119   /* 111th */
#define FDYCTT_112    120   /* 112th */
#define FDYCTT_113    121   /* 113th */
#define FDYCTT_114    122   /* 114th */
#define FDYCTT_115    123   /* 115th */
#define FDYCTT_116    124   /* 116th */
#define FDYCTT_117    125   /* 117th */
#define FDYCTT_118    126   /* 118th */
#define FDYCTT_119    127   /* 119th */
#define FDYCTT_120    128   /* 120th */
#define FDYCTT_121    129   /* 121st */
#define FDYCTT_122    130   /* 122nd */
#define FDYCTT_123    131   /* 123rd */
#define FDYCTT_124    132   /* 124th */
#define FDYCTT_125    133   /* 125th */
#define FDYCTT_126    134   /* 126th */
#define FDYCTT_127    135   /* 127th */
#define FDYCTT_128    136   /* 128th */
#define FDYCTT_129    137   /* 129th */
#define FDYCTT_130    138   /* 130th */
#define FDYCTT_131    139   /* 131st */
#define FDYCTT_132    140   /* 132nd */
#define FDYCTT_133    141   /* 133rd */
#define FDYCTT_134    142   /* 134th */
#define FDYCTT_135    143   /* 135th */
#define FDYCTT_136    144   /* 136th */
#define FDYCTT_137    145   /* 137th */
#define FDYCTT_138    146   /* 138th */
#define FDYCTT_139    147   /* 139th */
#define FDYCTT_140    148   /* 140th */
#define FDYCTT_141    149   /* 141st */
#define FDYCTT_142    150   /* 142nd */
#define FDYCTT_143    151   /* 143rd */
#define FDYCTT_144    152   /* 144th */
#define FDYCTT_145    153   /* 145th */
#define FDYCTT_146    154   /* 146th */
#define FDYCTT_147    155   /* 147th */
#define FDYCTT_148    156   /* 148th */
#define FDYCTT_149    157   /* 149th */
#define FDYCTT_150    158   /* 150th */
#define FDYCTT_151    159   /* 151st */
#define FDYCTT_152    160   /* 152nd */
#define FDYCTT_153    161   /* 153rd */
#define FDYCTT_154    162   /* 154th */
#define FDYCTT_155    163   /* 155th */
#define FDYCTT_156    164   /* 156th */
#define FDYCTT_157    165   /* 157th */
#define FDYCTT_158    166   /* 158th */
#define FDYCTT_159    167   /* 159th */
#define FDYCTT_160    168   /* 160th */
#define FDYCTT_161    169   /* 161st */
#define FDYCTT_162    170   /* 162nd */
#define FDYCTT_163    171   /* 163rd */
#define FDYCTT_164    172   /* 164th */
#define FDYCTT_165    173   /* 165th */
#define FDYCTT_166    174   /* 166th */
#define FDYCTT_167    175   /* 167th */
#define FDYCTT_168    176   /* 168th */
#define FDYCTT_169    177   /* 169th */
#define FDYCTT_170    178   /* 170th */
#define FDYCTT_171    179   /* 171st */
#define FDYCTT_172    180   /* 172nd */
#define FDYCTT_173    181   /* 173rd */
#define FDYCTT_174    182   /* 174th */
#define FDYCTT_175    183   /* 175th */
#define FDYCTT_176    184   /* 176th */
#define FDYCTT_177    185   /* 177th */
#define FDYCTT_178    186   /* 178th */
#define FDYCTT_179    187   /* 179th */
#define FDYCTT_180    188   /* 180th */
#define FDYCTT_181    189   /* 181st */
#define FDYCTT_182    190   /* 182nd */
#define FDYCTT_183    191   /* 183rd */
#define FDYCTT_184    192   /* 184th */
#define FDYCTT_185    193   /* 185th */
#define FDYCTT_186    194   /* 186th */
#define FDYCTT_187    195   /* 187th */
#define FDYCTT_188    196   /* 188th */
#define FDYCTT_189    197   /* 189th */
#define FDYCTT_999    198   /* Last */

#define _FDYCTT_MAX 198


/* FFUI = Flex Fields UI Widget Types */
extern const char FFUI[NULL_CODE];

extern const char FFUI_TI    [NULL_CODE];   /* Text Item */
extern const char FFUI_RG    [NULL_CODE];   /* List of Values */
extern const char FFUI_LI    [NULL_CODE];   /* List Item */
extern const char FFUI_CB    [NULL_CODE];   /* Check Box */
extern const char FFUI_DT    [NULL_CODE];   /* Date */

#define FFUITT_TI       1   /* Text Item */
#define FFUITT_RG       2   /* List of Values */
#define FFUITT_LI       3   /* List Item */
#define FFUITT_CB       4   /* Check Box */
#define FFUITT_DT       5   /* Date */

#define _FFUITT_MAX 5


/* FINT = From Inventory Types */
extern const char FINT[NULL_CODE];

extern const char FINT_I     [NULL_CODE];   /* All Inventory */
extern const char FINT_U     [NULL_CODE];   /* All Unavailable */
extern const char FINT_A     [NULL_CODE];   /* Available */
extern const char FINT_S     [NULL_CODE];   /* Specific Unavailable */

#define FINTTT_I        1   /* All Inventory */
#define FINTTT_U        2   /* All Unavailable */
#define FINTTT_A        3   /* Available */
#define FINTTT_S        4   /* Specific Unavailable */

#define _FINTTT_MAX 4


/* FIPR = Item Fill Priority */
extern const char FIPR[NULL_CODE];

extern const char FIPR_M     [NULL_CODE];   /* Main */
extern const char FIPR_S     [NULL_CODE];   /* Substitute */

#define FIPRTT_M        1   /* Main */
#define FIPRTT_S        2   /* Substitute */

#define _FIPRTT_MAX 2


/* FLDA = Sales Audit Field Access */
extern const char FLDA[NULL_CODE];

extern const char FLDA_ESO   [NULL_CODE];   /* Error List Store Override */
extern const char FLDA_EHO   [NULL_CODE];   /* Error List HQ Override */
extern const char FLDA_OSS   [NULL_CODE];   /* O/S Store Reported */
extern const char FLDA_OSH   [NULL_CODE];   /* O/S HQ Reported */
extern const char FLDA_OSA   [NULL_CODE];   /* O/S Actual O/S Value */
extern const char FLDA_OST   [NULL_CODE];   /* O/S Trial O/S Value */
extern const char FLDA_MS    [NULL_CODE];   /* Misc. Store Reported */
extern const char FLDA_MH    [NULL_CODE];   /* Misc. HQ Reported */
extern const char FLDA_BOS   [NULL_CODE];   /* Balance Level Over/Short Value */

#define FLDATT_ESO      1   /* Error List Store Override */
#define FLDATT_EHO      2   /* Error List HQ Override */
#define FLDATT_OSS      3   /* O/S Store Reported */
#define FLDATT_OSH      4   /* O/S HQ Reported */
#define FLDATT_OSA      5   /* O/S Actual O/S Value */
#define FLDATT_OST      6   /* O/S Trial O/S Value */
#define FLDATT_MS       7   /* Misc. Store Reported */
#define FLDATT_MH       8   /* Misc. HQ Reported */
#define FLDATT_BOS      9   /* Balance Level Over/Short Value */

#define _FLDATT_MAX 9


/* FLOC = From Location */
extern const char FLOC[NULL_CODE];

extern const char FLOC_S     [NULL_CODE];   /* From St */
extern const char FLOC_W     [NULL_CODE];   /* From Wh */
extern const char FLOC_L     [NULL_CODE];   /* From Loc */

#define FLOCTT_S        1   /* From St */
#define FLOCTT_W        2   /* From Wh */
#define FLOCTT_L        3   /* From Loc */

#define _FLOCTT_MAX 3


/* FLOW = Filter Org Hier including WH, Finishers */
extern const char FLOW[NULL_CODE];

extern const char FLOW_C     [NULL_CODE];   /* @OH2@ */
extern const char FLOW_A     [NULL_CODE];   /* @OH3@ */
extern const char FLOW_R     [NULL_CODE];   /* @OH4@ */
extern const char FLOW_D     [NULL_CODE];   /* @OH5@ */
extern const char FLOW_W     [NULL_CODE];   /* Warehouse */
extern const char FLOW_I     [NULL_CODE];   /* Internal Finisher */
extern const char FLOW_E     [NULL_CODE];   /* External Finisher */
extern const char FLOW_T     [NULL_CODE];   /* Transfer Entity */
extern const char FLOW_O     [NULL_CODE];   /* Org Unit */

#define FLOWTT_C        1   /* @OH2@ */
#define FLOWTT_A        2   /* @OH3@ */
#define FLOWTT_R        3   /* @OH4@ */
#define FLOWTT_D        4   /* @OH5@ */
#define FLOWTT_W        5   /* Warehouse */
#define FLOWTT_I        6   /* Internal Finisher */
#define FLOWTT_E        7   /* External Finisher */
#define FLOWTT_T        8   /* Transfer Entity */
#define FLOWTT_O        9   /* Org Unit */

#define _FLOWTT_MAX 9


/* FLQC = Label Failed QC */
extern const char FLQC[NULL_CODE];

extern const char FLQC_f     [NULL_CODE];   /* Failed QC */

#define FLQCTT_f        1   /* Failed QC */

#define _FLQCTT_MAX 1


/* FLTM = Filter Merchandise Hierarchy Levels */
extern const char FLTM[NULL_CODE];

extern const char FLTM_D     [NULL_CODE];   /* @MH2@ */
extern const char FLTM_G     [NULL_CODE];   /* @MH3@ */
extern const char FLTM_P     [NULL_CODE];   /* @MH4@ */
extern const char FLTM_C     [NULL_CODE];   /* @MH5@ */
extern const char FLTM_S     [NULL_CODE];   /* @MH6@ */

#define FLTMTT_D        1   /* @MH2@ */
#define FLTMTT_G        2   /* @MH3@ */
#define FLTMTT_P        3   /* @MH4@ */
#define FLTMTT_C        4   /* @MH5@ */
#define FLTMTT_S        5   /* @MH6@ */

#define _FLTMTT_MAX 5


/* FLTO = Filter Organization Hierarchy Levels */
extern const char FLTO[NULL_CODE];

extern const char FLTO_C     [NULL_CODE];   /* @OH2@ */
extern const char FLTO_A     [NULL_CODE];   /* @OH3@ */
extern const char FLTO_R     [NULL_CODE];   /* @OH4@ */
extern const char FLTO_D     [NULL_CODE];   /* @OH5@ */

#define FLTOTT_C        1   /* @OH2@ */
#define FLTOTT_A        2   /* @OH3@ */
#define FLTOTT_R        3   /* @OH4@ */
#define FLTOTT_D        4   /* @OH5@ */

#define _FLTOTT_MAX 4


/* FMTP = Form Type */
extern const char FMTP[NULL_CODE];

extern const char FMTP_F     [NULL_CODE];   /* Form */
extern const char FMTP_M     [NULL_CODE];   /* Menu */

#define FMTPTT_F        1   /* Form */
#define FMTPTT_M        2   /* Menu */

#define _FMTPTT_MAX 2


/* FNAP = Financial Integration Application */
extern const char FNAP[NULL_CODE];

extern const char FNAP_O     [NULL_CODE];   /* Point-To-Point Oracle E-Business Suite */
extern const char FNAP_A     [NULL_CODE];   /* Oracle Retail Financial Integration */

#define FNAPTT_O        1   /* Point-To-Point Oracle E-Business Suite */
#define FNAPTT_A        2   /* Oracle Retail Financial Integration */

#define _FNAPTT_MAX 2


/* FOBT = Location Transportation Responsibility */
extern const char FOBT[NULL_CODE];

extern const char FOBT_AC    [NULL_CODE];   /* City and State */
extern const char FOBT_CA    [NULL_CODE];   /* Country Of Sourcing */
extern const char FOBT_CC    [NULL_CODE];   /* Country */
extern const char FOBT_CI    [NULL_CODE];   /* City */
extern const char FOBT_CO    [NULL_CODE];   /* County/Parish and State */
extern const char FOBT_CS    [NULL_CODE];   /* Canadian SPLC */
extern const char FOBT_CY    [NULL_CODE];   /* County/Parish */
extern const char FOBT_DE    [NULL_CODE];   /* Destination (Shipping) */
extern const char FOBT_FA    [NULL_CODE];   /* Factory */
extern const char FOBT_FE    [NULL_CODE];   /* Freight Equalization Point */
extern const char FOBT_FF    [NULL_CODE];   /* Foreign Freight Forwarder Location */
extern const char FOBT_MI    [NULL_CODE];   /* Mill */
extern const char FOBT_NS    [NULL_CODE];   /* City/State from Points */
extern const char FOBT_OA    [NULL_CODE];   /* Origin (After loading on Equipment) */
extern const char FOBT_OR    [NULL_CODE];   /* Origin (Shipping Point) */
extern const char FOBT_OV    [NULL_CODE];   /* On Vessel (Free on Board Point) */
extern const char FOBT_SP    [NULL_CODE];   /* State/Province */
extern const char FOBT_TL    [NULL_CODE];   /* Terminal Cargo Location */
extern const char FOBT_WH    [NULL_CODE];   /* Warehouse */

#define FOBTTT_AC       1   /* City and State */
#define FOBTTT_CA       2   /* Country Of Sourcing */
#define FOBTTT_CC       3   /* Country */
#define FOBTTT_CI       4   /* City */
#define FOBTTT_CO       5   /* County/Parish and State */
#define FOBTTT_CS       6   /* Canadian SPLC */
#define FOBTTT_CY       7   /* County/Parish */
#define FOBTTT_DE       8   /* Destination (Shipping) */
#define FOBTTT_FA       9   /* Factory */
#define FOBTTT_FE      10   /* Freight Equalization Point */
#define FOBTTT_FF      11   /* Foreign Freight Forwarder Location */
#define FOBTTT_MI      12   /* Mill */
#define FOBTTT_NS      13   /* City/State from Points */
#define FOBTTT_OA      14   /* Origin (After loading on Equipment) */
#define FOBTTT_OR      15   /* Origin (Shipping Point) */
#define FOBTTT_OV      16   /* On Vessel (Free on Board Point) */
#define FOBTTT_SP      17   /* State/Province */
#define FOBTTT_TL      18   /* Terminal Cargo Location */
#define FOBTTT_WH      19   /* Warehouse */

#define _FOBTTT_MAX 19


/* FORM = Sales Audit Forms */
extern const char FORM[NULL_CODE];

extern const char FORM_TR    [NULL_CODE];   /* Transaction Detail */
extern const char FORM_O     [NULL_CODE];   /* Over/Short Totals */
extern const char FORM_T     [NULL_CODE];   /* Misc. Totals */
extern const char FORM_M     [NULL_CODE];   /* Missing Trans. */
extern const char FORM_NT    [NULL_CODE];   /* Non Transactional */

#define FORMTT_TR       1   /* Transaction Detail */
#define FORMTT_O        2   /* Over/Short Totals */
#define FORMTT_T        3   /* Misc. Totals */
#define FORMTT_M        4   /* Missing Trans. */
#define FORMTT_NT       5   /* Non Transactional */

#define _FORMTT_MAX 5


/* FOST = @SUH4@ Order source types */
extern const char FOST[NULL_CODE];

extern const char FOST_ST    [NULL_CODE];   /* Store */
extern const char FOST_SU    [NULL_CODE];   /* Supplier */
extern const char FOST_WH    [NULL_CODE];   /* Warehouse */

#define FOSTTT_ST       1   /* Store */
#define FOSTTT_SU       2   /* Supplier */
#define FOSTTT_WH       3   /* Warehouse */

#define _FOSTTT_MAX 3


/* FOTP = Customs Entry Form Type */
extern const char FOTP[NULL_CODE];

extern const char FOTP_1     [NULL_CODE];   /* Form Type 1 */
extern const char FOTP_2     [NULL_CODE];   /* Form Type 2 */
extern const char FOTP_3     [NULL_CODE];   /* Form Type 3 */

#define FOTPTT_1        1   /* Form Type 1 */
#define FOTPTT_2        2   /* Form Type 2 */
#define FOTPTT_3        3   /* Form Type 3 */

#define _FOTPTT_MAX 3


/* FQHF = Half Frequency */
extern const char FQHF[NULL_CODE];

extern const char FQHF_0     [NULL_CODE];   /* Every */
extern const char FQHF_1     [NULL_CODE];   /* First */
extern const char FQHF_2     [NULL_CODE];   /* Second */

#define FQHFTT_0        1   /* Every */
#define FQHFTT_1        2   /* First */
#define FQHFTT_2        3   /* Second */

#define _FQHFTT_MAX 3


/* FQHR = Hour Frequency */
extern const char FQHR[NULL_CODE];

extern const char FQHR_0     [NULL_CODE];   /* Once a Day */
extern const char FQHR_1     [NULL_CODE];   /* Every Hour */

#define FQHRTT_0        1   /* Once a Day */
#define FQHRTT_1        2   /* Every Hour */

#define _FQHRTT_MAX 2


/* FQMN = Month Frequency */
extern const char FQMN[NULL_CODE];

extern const char FQMN_0     [NULL_CODE];   /* Every */
extern const char FQMN_1     [NULL_CODE];   /* First */
extern const char FQMN_2     [NULL_CODE];   /* 2nd */
extern const char FQMN_3     [NULL_CODE];   /* 3rd */
extern const char FQMN_4     [NULL_CODE];   /* 4th */
extern const char FQMN_5     [NULL_CODE];   /* 5th */
extern const char FQMN_6     [NULL_CODE];   /* 6th */
extern const char FQMN_7     [NULL_CODE];   /* 7th */
extern const char FQMN_8     [NULL_CODE];   /* 8th */
extern const char FQMN_9     [NULL_CODE];   /* 9th */
extern const char FQMN_10    [NULL_CODE];   /* 10th */
extern const char FQMN_11    [NULL_CODE];   /* 11th */
extern const char FQMN_12    [NULL_CODE];   /* 12th */

#define FQMNTT_0        1   /* Every */
#define FQMNTT_1        2   /* First */
#define FQMNTT_2        3   /* 2nd */
#define FQMNTT_3        4   /* 3rd */
#define FQMNTT_4        5   /* 4th */
#define FQMNTT_5        6   /* 5th */
#define FQMNTT_6        7   /* 6th */
#define FQMNTT_7        8   /* 7th */
#define FQMNTT_8        9   /* 8th */
#define FQMNTT_9       10   /* 9th */
#define FQMNTT_10      11   /* 10th */
#define FQMNTT_11      12   /* 11th */
#define FQMNTT_12      13   /* 12th */

#define _FQMNTT_MAX 13


/* FQWK = Week Frequency */
extern const char FQWK[NULL_CODE];

extern const char FQWK_0     [NULL_CODE];   /* Every */
extern const char FQWK_1     [NULL_CODE];   /* First */
extern const char FQWK_2     [NULL_CODE];   /* 2nd */
extern const char FQWK_3     [NULL_CODE];   /* 3rd */
extern const char FQWK_4     [NULL_CODE];   /* 4th */
extern const char FQWK_5     [NULL_CODE];   /* 5th */
extern const char FQWK_6     [NULL_CODE];   /* 6th */
extern const char FQWK_7     [NULL_CODE];   /* 7th */
extern const char FQWK_8     [NULL_CODE];   /* 8th */
extern const char FQWK_9     [NULL_CODE];   /* 9th */
extern const char FQWK_10    [NULL_CODE];   /* 10th */
extern const char FQWK_11    [NULL_CODE];   /* 11th */
extern const char FQWK_12    [NULL_CODE];   /* 12th */
extern const char FQWK_13    [NULL_CODE];   /* 13th */
extern const char FQWK_14    [NULL_CODE];   /* 14th */
extern const char FQWK_15    [NULL_CODE];   /* 15th */
extern const char FQWK_16    [NULL_CODE];   /* 16th */
extern const char FQWK_17    [NULL_CODE];   /* 17th */
extern const char FQWK_18    [NULL_CODE];   /* 18th */
extern const char FQWK_19    [NULL_CODE];   /* 19th */
extern const char FQWK_20    [NULL_CODE];   /* 20th */
extern const char FQWK_21    [NULL_CODE];   /* 21st */
extern const char FQWK_22    [NULL_CODE];   /* 22nd */
extern const char FQWK_23    [NULL_CODE];   /* 23rd */
extern const char FQWK_24    [NULL_CODE];   /* 24th */
extern const char FQWK_25    [NULL_CODE];   /* 25th */
extern const char FQWK_26    [NULL_CODE];   /* 26th */
extern const char FQWK_27    [NULL_CODE];   /* 27th */
extern const char FQWK_28    [NULL_CODE];   /* 28th */
extern const char FQWK_29    [NULL_CODE];   /* 29th */
extern const char FQWK_30    [NULL_CODE];   /* 30th */
extern const char FQWK_31    [NULL_CODE];   /* 31st */
extern const char FQWK_32    [NULL_CODE];   /* 32nd */
extern const char FQWK_33    [NULL_CODE];   /* 33rd */
extern const char FQWK_34    [NULL_CODE];   /* 34th */
extern const char FQWK_35    [NULL_CODE];   /* 35th */
extern const char FQWK_36    [NULL_CODE];   /* 36th */
extern const char FQWK_37    [NULL_CODE];   /* 37th */
extern const char FQWK_38    [NULL_CODE];   /* 38th */
extern const char FQWK_39    [NULL_CODE];   /* 39th */
extern const char FQWK_40    [NULL_CODE];   /* 40th */
extern const char FQWK_41    [NULL_CODE];   /* 41st */
extern const char FQWK_42    [NULL_CODE];   /* 42nd */
extern const char FQWK_43    [NULL_CODE];   /* 43rd */
extern const char FQWK_44    [NULL_CODE];   /* 44th */
extern const char FQWK_45    [NULL_CODE];   /* 45th */
extern const char FQWK_46    [NULL_CODE];   /* 46th */
extern const char FQWK_47    [NULL_CODE];   /* 47th */
extern const char FQWK_48    [NULL_CODE];   /* 48th */
extern const char FQWK_49    [NULL_CODE];   /* 49th */
extern const char FQWK_50    [NULL_CODE];   /* 50th */
extern const char FQWK_51    [NULL_CODE];   /* 51st */
extern const char FQWK_52    [NULL_CODE];   /* 52nd */
extern const char FQWK_99    [NULL_CODE];   /* Last */

#define FQWKTT_0        1   /* Every */
#define FQWKTT_1        2   /* First */
#define FQWKTT_2        3   /* 2nd */
#define FQWKTT_3        4   /* 3rd */
#define FQWKTT_4        5   /* 4th */
#define FQWKTT_5        6   /* 5th */
#define FQWKTT_6        7   /* 6th */
#define FQWKTT_7        8   /* 7th */
#define FQWKTT_8        9   /* 8th */
#define FQWKTT_9       10   /* 9th */
#define FQWKTT_10      11   /* 10th */
#define FQWKTT_11      12   /* 11th */
#define FQWKTT_12      13   /* 12th */
#define FQWKTT_13      14   /* 13th */
#define FQWKTT_14      15   /* 14th */
#define FQWKTT_15      16   /* 15th */
#define FQWKTT_16      17   /* 16th */
#define FQWKTT_17      18   /* 17th */
#define FQWKTT_18      19   /* 18th */
#define FQWKTT_19      20   /* 19th */
#define FQWKTT_20      21   /* 20th */
#define FQWKTT_21      22   /* 21st */
#define FQWKTT_22      23   /* 22nd */
#define FQWKTT_23      24   /* 23rd */
#define FQWKTT_24      25   /* 24th */
#define FQWKTT_25      26   /* 25th */
#define FQWKTT_26      27   /* 26th */
#define FQWKTT_27      28   /* 27th */
#define FQWKTT_28      29   /* 28th */
#define FQWKTT_29      30   /* 29th */
#define FQWKTT_30      31   /* 30th */
#define FQWKTT_31      32   /* 31st */
#define FQWKTT_32      33   /* 32nd */
#define FQWKTT_33      34   /* 33rd */
#define FQWKTT_34      35   /* 34th */
#define FQWKTT_35      36   /* 35th */
#define FQWKTT_36      37   /* 36th */
#define FQWKTT_37      38   /* 37th */
#define FQWKTT_38      39   /* 38th */
#define FQWKTT_39      40   /* 39th */
#define FQWKTT_40      41   /* 40th */
#define FQWKTT_41      42   /* 41st */
#define FQWKTT_42      43   /* 42nd */
#define FQWKTT_43      44   /* 43rd */
#define FQWKTT_44      45   /* 44th */
#define FQWKTT_45      46   /* 45th */
#define FQWKTT_46      47   /* 46th */
#define FQWKTT_47      48   /* 47th */
#define FQWKTT_48      49   /* 48th */
#define FQWKTT_49      50   /* 49th */
#define FQWKTT_50      51   /* 50th */
#define FQWKTT_51      52   /* 51st */
#define FQWKTT_52      53   /* 52nd */
#define FQWKTT_99      54   /* Last */

#define _FQWKTT_MAX 54


/* FTYP = Finisher Types */
extern const char FTYP[NULL_CODE];

extern const char FTYP_I     [NULL_CODE];   /* Internal */
extern const char FTYP_E     [NULL_CODE];   /* External */

#define FTYPTT_I        1   /* Internal */
#define FTYPTT_E        2   /* External */

#define _FTYPTT_MAX 2


/* FXCL = Fixed Deal Collect By Type */
extern const char FXCL[NULL_CODE];

extern const char FXCL_D     [NULL_CODE];   /* Date */
extern const char FXCL_M     [NULL_CODE];   /* Monthly */
extern const char FXCL_Q     [NULL_CODE];   /* Quarterly */
extern const char FXCL_A     [NULL_CODE];   /* Annually */

#define FXCLTT_D        1   /* Date */
#define FXCLTT_M        2   /* Monthly */
#define FXCLTT_Q        3   /* Quarterly */
#define FXCLTT_A        4   /* Annually */

#define _FXCLTT_MAX 4


/* FXDT = Fixed Deal Types */
extern const char FXDT[NULL_CODE];

extern const char FXDT_CA    [NULL_CODE];   /* Cancel Allowance */
extern const char FXDT_ANP   [NULL_CODE];   /* Allowance Non Performance */
extern const char FXDT_DA    [NULL_CODE];   /* Display Allowance */
extern const char FXDT_EBA   [NULL_CODE];   /* Early Buy Allowance */
extern const char FXDT_ND    [NULL_CODE];   /* New Discount */
extern const char FXDT_NW    [NULL_CODE];   /* New Warehouse */
extern const char FXDT_LUMP  [NULL_CODE];   /* Lump Sum */
extern const char FXDT_NI    [NULL_CODE];   /* New Item Allowance */
extern const char FXDT_SA    [NULL_CODE];   /* Slotting Allowance */
extern const char FXDT_NDA   [NULL_CODE];   /* New Distribution Allowance */
extern const char FXDT_AD    [NULL_CODE];   /* Advertising Allowance */
extern const char FXDT_OTHER [NULL_CODE];   /* Other Allowance */
extern const char FXDT_SCAN  [NULL_CODE];   /* Scan Based Allowance */

#define FXDTTT_CA       1   /* Cancel Allowance */
#define FXDTTT_ANP      2   /* Allowance Non Performance */
#define FXDTTT_DA       3   /* Display Allowance */
#define FXDTTT_EBA      4   /* Early Buy Allowance */
#define FXDTTT_ND       5   /* New Discount */
#define FXDTTT_NW       6   /* New Warehouse */
#define FXDTTT_LUMP     7   /* Lump Sum */
#define FXDTTT_NI       8   /* New Item Allowance */
#define FXDTTT_SA       9   /* Slotting Allowance */
#define FXDTTT_NDA     10   /* New Distribution Allowance */
#define FXDTTT_AD      11   /* Advertising Allowance */
#define FXDTTT_OTHER   12   /* Other Allowance */
#define FXDTTT_SCAN    13   /* Scan Based Allowance */

#define _FXDTTT_MAX 13


/* FXMC = Labels for merchandise and Locations */
extern const char FXMC[NULL_CODE];

extern const char FXMC_MERCH [NULL_CODE];   /* Merchandise */
extern const char FXMC_LOC   [NULL_CODE];   /* Locations */

#define FXMCTT_MERCH    1   /* Merchandise */
#define FXMCTT_LOC      2   /* Locations */

#define _FXMCTT_MAX 2


/* FXST = Fixed Deal Status */
extern const char FXST[NULL_CODE];

extern const char FXST_A     [NULL_CODE];   /* Active */
extern const char FXST_I     [NULL_CODE];   /* Inactive */

#define FXSTTT_A        1   /* Active */
#define FXSTTT_I        2   /* Inactive */

#define _FXSTTT_MAX 2


/* GAIT = GAP Packing Item Types */
extern const char GAIT[NULL_CODE];

extern const char GAIT_F     [NULL_CODE];   /* From */
extern const char GAIT_R     [NULL_CODE];   /* To */

#define GAITTT_F        1   /* From */
#define GAITTT_R        2   /* To */

#define _GAITTT_MAX 2


/* GALC = GAP Stock Reconcile Location List */
extern const char GALC[NULL_CODE];

extern const char GALC_S     [NULL_CODE];   /* Store */
extern const char GALC_W     [NULL_CODE];   /* Warehouse */
extern const char GALC_PW    [NULL_CODE];   /* Physical Warehouse */
extern const char GALC_C     [NULL_CODE];   /* Store Class */
extern const char GALC_D     [NULL_CODE];   /* @OH5@ */
extern const char GALC_R     [NULL_CODE];   /* @OH4@ */
extern const char GALC_A     [NULL_CODE];   /* @OH3@ */
extern const char GALC_T     [NULL_CODE];   /* Transfer Zone */
extern const char GALC_L     [NULL_CODE];   /* Location Trait */
extern const char GALC_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GALC_LLS   [NULL_CODE];   /* Loc-List Store */
extern const char GALC_LLW   [NULL_CODE];   /* Loc-List Warehouse */

#define GALCTT_S        1   /* Store */
#define GALCTT_W        2   /* Warehouse */
#define GALCTT_PW       3   /* Physical Warehouse */
#define GALCTT_C        4   /* Store Class */
#define GALCTT_D        5   /* @OH5@ */
#define GALCTT_R        6   /* @OH4@ */
#define GALCTT_A        7   /* @OH3@ */
#define GALCTT_T        9   /* Transfer Zone */
#define GALCTT_L       11   /* Location Trait */
#define GALCTT_DW      12   /* Default Warehouse */
#define GALCTT_LLS     13   /* Loc-List Store */
#define GALCTT_LLW     14   /* Loc-List Warehouse */

#define _GALCTT_MAX 14


/* GART = GAP Stock Reconcile Action Types */
extern const char GART[NULL_CODE];

extern const char GART_SL    [NULL_CODE];   /* Shipping Location */
extern const char GART_RL    [NULL_CODE];   /* Receiving Location */
extern const char GART_FR    [NULL_CODE];   /* Force Receipt */
extern const char GART_FC    [NULL_CODE];   /* Freight Claim */
extern const char GART_RE    [NULL_CODE];   /* Received Elsewhere - See Comments */

#define GARTTT_SL       1   /* Shipping Location */
#define GARTTT_RL       2   /* Receiving Location */
#define GARTTT_FR       3   /* Force Receipt */
#define GARTTT_FC       4   /* Freight Claim */
#define GARTTT_RE       5   /* Received Elsewhere - See Comments */

#define _GARTTT_MAX 5


/* GLMT = General Ledger Monthly Transactions */
extern const char GLMT[NULL_CODE];

extern const char GLMT_50R   [NULL_CODE];   /* OPN STK Retail */
extern const char GLMT_50C   [NULL_CODE];   /* OPN STK Cost */
extern const char GLMT_22R   [NULL_CODE];   /* Stock Adj Retail */
extern const char GLMT_22C   [NULL_CODE];   /* Stock Adj Cost */
extern const char GLMT_20R   [NULL_CODE];   /* Purch Retail */
extern const char GLMT_20C   [NULL_CODE];   /* Purch Cost */
extern const char GLMT_24R   [NULL_CODE];   /* RTV Retail */
extern const char GLMT_24C   [NULL_CODE];   /* RTV Cost */
extern const char GLMT_26C   [NULL_CODE];   /* Freight Cost */
extern const char GLMT_30R   [NULL_CODE];   /* TSF in Retail */
extern const char GLMT_30C   [NULL_CODE];   /* TSF in Cost */
extern const char GLMT_32R   [NULL_CODE];   /* TSF Out Retail */
extern const char GLMT_32C   [NULL_CODE];   /* TSF Out Cost */
extern const char GLMT_01R   [NULL_CODE];   /* Net Sales Retail */
extern const char GLMT_02R   [NULL_CODE];   /* Net Sales Retail ex VAT */
extern const char GLMT_01C   [NULL_CODE];   /* Net Sales Cost */
extern const char GLMT_04R   [NULL_CODE];   /* Returns Retail */
extern const char GLMT_04C   [NULL_CODE];   /* Returns Cost */
extern const char GLMT_11R   [NULL_CODE];   /* Markup Retail */
extern const char GLMT_12R   [NULL_CODE];   /* Markup Cancel Retail  */
extern const char GLMT_16R   [NULL_CODE];   /* Clear Markdown Retail */
extern const char GLMT_13R   [NULL_CODE];   /* Perm Markup Retail  */
extern const char GLMT_15R   [NULL_CODE];   /* Prom Markdown Retail */
extern const char GLMT_14R   [NULL_CODE];   /* Markdown Cancel Retail */
extern const char GLMT_51C   [NULL_CODE];   /* Shrinkage Cost */
extern const char GLMT_51R   [NULL_CODE];   /* Shrinkage Retail */
extern const char GLMT_60R   [NULL_CODE];   /* EMPL discount Retail */
extern const char GLMT_80C   [NULL_CODE];   /* Workroom Amount */
extern const char GLMT_81R   [NULL_CODE];   /* Cash discount Amount */
extern const char GLMT_52R   [NULL_CODE];   /* CLS STK Retail */
extern const char GLMT_52C   [NULL_CODE];   /* CLS STK Cost */
extern const char GLMT_53R   [NULL_CODE];   /* Gross Margin Amount */
extern const char GLMT_70C   [NULL_CODE];   /* Cost Variance Amount */
extern const char GLMT_54R   [NULL_CODE];   /* HTD GAFS Retail */
extern const char GLMT_54C   [NULL_CODE];   /* HTD GAFS Cost */
extern const char GLMT_55C   [NULL_CODE];   /* Inter stocktake sales amount */
extern const char GLMT_56C   [NULL_CODE];   /* Inter stocktake shrinkage amount */
extern const char GLMT_57C   [NULL_CODE];   /* Stocktake MTD sales amount */
extern const char GLMT_58C   [NULL_CODE];   /* Stocktake MTD shrink amount */
extern const char GLMT_59R   [NULL_CODE];   /* Stocktake BOOKSTK Retail */
extern const char GLMT_59C   [NULL_CODE];   /* Stocktake BOOKSTK Cost */
extern const char GLMT_61R   [NULL_CODE];   /* Stocktake ACTSTK Retail */
extern const char GLMT_61C   [NULL_CODE];   /* Stocktake ACTSTK Cost */
extern const char GLMT_34C   [NULL_CODE];   /* Reclass in Cost */
extern const char GLMT_34R   [NULL_CODE];   /* Reclass in Retail */
extern const char GLMT_36C   [NULL_CODE];   /* Reclass out Cost */
extern const char GLMT_36R   [NULL_CODE];   /* Reclass out Retail */
extern const char GLMT_62C   [NULL_CODE];   /* Freight Claim Cost */
extern const char GLMT_62R   [NULL_CODE];   /* Freight Claim Retail */
extern const char GLMT_23C   [NULL_CODE];   /* Stock Adj Cogs Cost */
extern const char GLMT_23R   [NULL_CODE];   /* Stock Adj Cogs Retail */
extern const char GLMT_17R   [NULL_CODE];   /* Intercompany Markup Retail */
extern const char GLMT_18R   [NULL_CODE];   /* Intercompany Markdown Retail */
extern const char GLMT_37C   [NULL_CODE];   /* Intercompany in Cost */
extern const char GLMT_37R   [NULL_CODE];   /* Intercompany in Retail */
extern const char GLMT_38C   [NULL_CODE];   /* Intercompany out Cost */
extern const char GLMT_38R   [NULL_CODE];   /* Intercompany out Retail */
extern const char GLMT_63C   [NULL_CODE];   /* WO activity UPD INV Cost */
extern const char GLMT_64C   [NULL_CODE];   /* WO activity POST FIN Cost */
extern const char GLMT_39R   [NULL_CODE];   /* Intercompany Margin */
extern const char GLMT_82R   [NULL_CODE];   /* @SUH4@ Sales Retail */
extern const char GLMT_82C   [NULL_CODE];   /* @SUH4@ Sales Cost */
extern const char GLMT_83R   [NULL_CODE];   /* @SUH4@ Returns Retail */
extern const char GLMT_83C   [NULL_CODE];   /* @SUH4@ Returns Cost */
extern const char GLMT_84R   [NULL_CODE];   /* @SUH4@ Markups */
extern const char GLMT_85R   [NULL_CODE];   /* @SUH4@ Markdowns */
extern const char GLMT_86C   [NULL_CODE];   /* @SUH4@ Restocking Fee */
extern const char GLMT_87C   [NULL_CODE];   /* Vat In Cost */
extern const char GLMT_88R   [NULL_CODE];   /* Vat Out Retail */
extern const char GLMT_10R   [NULL_CODE];   /* Weight Variance Retail */
extern const char GLMT_74C   [NULL_CODE];   /* Recoverable Tax at destination */
extern const char GLMT_75C   [NULL_CODE];   /* Recoverable Tax at source */
extern const char GLMT_03R   [NULL_CODE];   /* Net Sales Non Inv Retail */
extern const char GLMT_05R   [NULL_CODE];   /* Net Sales Non Inv Rtl ex VAT */
extern const char GLMT_03C   [NULL_CODE];   /* Net Sales Non Inv Cost */
extern const char GLMT_73C   [NULL_CODE];   /* Rec Cost Adjust Variance FIFO */
extern const char GLMT_29C   [NULL_CODE];   /* GL Interface - Expense Up Charge (Cost) */
extern const char GLMT_28C   [NULL_CODE];   /* GL Interface - Profit Up Charge (Cost) */
extern const char GLMT_31C   [NULL_CODE];   /* GL Interface - Book Transfers In (Cost) */
extern const char GLMT_33C   [NULL_CODE];   /* GL Interface - Book Transfers Out (Cost) */
extern const char GLMT_31R   [NULL_CODE];   /* GL Interface - Book Tsf In (Retail) */
extern const char GLMT_33R   [NULL_CODE];   /* GL Interface - Book Tsf Out (Retail) */

#define GLMTTT_50R      1   /* OPN STK Retail */
#define GLMTTT_50C      2   /* OPN STK Cost */
#define GLMTTT_22R      3   /* Stock Adj Retail */
#define GLMTTT_22C      4   /* Stock Adj Cost */
#define GLMTTT_20R      5   /* Purch Retail */
#define GLMTTT_20C      6   /* Purch Cost */
#define GLMTTT_24R      7   /* RTV Retail */
#define GLMTTT_24C      8   /* RTV Cost */
#define GLMTTT_26C      9   /* Freight Cost */
#define GLMTTT_30R     10   /* TSF in Retail */
#define GLMTTT_30C     11   /* TSF in Cost */
#define GLMTTT_32R     12   /* TSF Out Retail */
#define GLMTTT_32C     13   /* TSF Out Cost */
#define GLMTTT_01R     14   /* Net Sales Retail */
#define GLMTTT_02R     15   /* Net Sales Retail ex VAT */
#define GLMTTT_01C     16   /* Net Sales Cost */
#define GLMTTT_04R     17   /* Returns Retail */
#define GLMTTT_04C     18   /* Returns Cost */
#define GLMTTT_11R     19   /* Markup Retail */
#define GLMTTT_12R     20   /* Markup Cancel Retail  */
#define GLMTTT_16R     21   /* Clear Markdown Retail */
#define GLMTTT_13R     22   /* Perm Markup Retail  */
#define GLMTTT_15R     23   /* Prom Markdown Retail */
#define GLMTTT_14R     24   /* Markdown Cancel Retail */
#define GLMTTT_51C     25   /* Shrinkage Cost */
#define GLMTTT_51R     26   /* Shrinkage Retail */
#define GLMTTT_60R     27   /* EMPL discount Retail */
#define GLMTTT_80C     28   /* Workroom Amount */
#define GLMTTT_81R     29   /* Cash discount Amount */
#define GLMTTT_52R     30   /* CLS STK Retail */
#define GLMTTT_52C     31   /* CLS STK Cost */
#define GLMTTT_53R     32   /* Gross Margin Amount */
#define GLMTTT_70C     33   /* Cost Variance Amount */
#define GLMTTT_54R     34   /* HTD GAFS Retail */
#define GLMTTT_54C     35   /* HTD GAFS Cost */
#define GLMTTT_55C     36   /* Inter stocktake sales amount */
#define GLMTTT_56C     37   /* Inter stocktake shrinkage amount */
#define GLMTTT_57C     38   /* Stocktake MTD sales amount */
#define GLMTTT_58C     39   /* Stocktake MTD shrink amount */
#define GLMTTT_59R     40   /* Stocktake BOOKSTK Retail */
#define GLMTTT_59C     41   /* Stocktake BOOKSTK Cost */
#define GLMTTT_61R     42   /* Stocktake ACTSTK Retail */
#define GLMTTT_61C     43   /* Stocktake ACTSTK Cost */
#define GLMTTT_34C     44   /* Reclass in Cost */
#define GLMTTT_34R     45   /* Reclass in Retail */
#define GLMTTT_36C     46   /* Reclass out Cost */
#define GLMTTT_36R     47   /* Reclass out Retail */
#define GLMTTT_62C     48   /* Freight Claim Cost */
#define GLMTTT_62R     49   /* Freight Claim Retail */
#define GLMTTT_23C     50   /* Stock Adj Cogs Cost */
#define GLMTTT_23R     51   /* Stock Adj Cogs Retail */
#define GLMTTT_17R     52   /* Intercompany Markup Retail */
#define GLMTTT_18R     53   /* Intercompany Markdown Retail */
#define GLMTTT_37C     54   /* Intercompany in Cost */
#define GLMTTT_37R     55   /* Intercompany in Retail */
#define GLMTTT_38C     56   /* Intercompany out Cost */
#define GLMTTT_38R     57   /* Intercompany out Retail */
#define GLMTTT_63C     58   /* WO activity UPD INV Cost */
#define GLMTTT_64C     59   /* WO activity POST FIN Cost */
#define GLMTTT_39R     60   /* Intercompany Margin */
#define GLMTTT_82R     61   /* @SUH4@ Sales Retail */
#define GLMTTT_82C     62   /* @SUH4@ Sales Cost */
#define GLMTTT_83R     63   /* @SUH4@ Returns Retail */
#define GLMTTT_83C     64   /* @SUH4@ Returns Cost */
#define GLMTTT_84R     65   /* @SUH4@ Markups */
#define GLMTTT_85R     67   /* @SUH4@ Markdowns */
#define GLMTTT_86C     68   /* @SUH4@ Restocking Fee */
#define GLMTTT_87C     69   /* Vat In Cost */
#define GLMTTT_88R     70   /* Vat Out Retail */
#define GLMTTT_10R     71   /* Weight Variance Retail */
#define GLMTTT_74C     72   /* Recoverable Tax at destination */
#define GLMTTT_75C     73   /* Recoverable Tax at source */
#define GLMTTT_03R     74   /* Net Sales Non Inv Retail */
#define GLMTTT_05R     75   /* Net Sales Non Inv Rtl ex VAT */
#define GLMTTT_03C     76   /* Net Sales Non Inv Cost */
#define GLMTTT_73C     77   /* Rec Cost Adjust Variance FIFO */
#define GLMTTT_29C     78   /* GL Interface - Expense Up Charge (Cost) */
#define GLMTTT_28C     79   /* GL Interface - Profit Up Charge (Cost) */
#define GLMTTT_31C     80   /* GL Interface - Book Transfers In (Cost) */
#define GLMTTT_33C     81   /* GL Interface - Book Transfers Out (Cost) */
#define GLMTTT_31R     82   /* GL Interface - Book Tsf In (Retail) */
#define GLMTTT_33R     83   /* GL Interface - Book Tsf Out (Retail) */

#define _GLMTTT_MAX 83


/* GLRL = General Ledger Roll Up */
extern const char GLRL[NULL_CODE];

extern const char GLRL_D     [NULL_CODE];   /* @MH4@ */
extern const char GLRL_C     [NULL_CODE];   /* @MH5@ */
extern const char GLRL_S     [NULL_CODE];   /* @MH6@ */

#define GLRLTT_D        1   /* @MH4@ */
#define GLRLTT_C        2   /* @MH5@ */
#define GLRLTT_S        3   /* @MH6@ */

#define _GLRLTT_MAX 3


/* GLRT = General Ledger Rolled Transactions */
extern const char GLRT[NULL_CODE];

extern const char GLRT_01C   [NULL_CODE];   /* GL Interface - Net Sales (Cost) */
extern const char GLRT_04R   [NULL_CODE];   /* GL Interface - Markup (Retail) */
extern const char GLRT_15R   [NULL_CODE];   /* GL Interface - Promo Markdown (Retail) */
extern const char GLRT_17R   [NULL_CODE];   /* GL Interface - Intercompany Markup */
extern const char GLRT_18R   [NULL_CODE];   /* GL Interface - Intercompany Markdown */
extern const char GLRT_20C   [NULL_CODE];   /* GL Interface - Purchases (Cost) */
extern const char GLRT_22C   [NULL_CODE];   /* GL Interface - Stock Adj Non-COGS (Cost) */
extern const char GLRT_23C   [NULL_CODE];   /* GL Interface - Stock Adj COGS (Cost) */
extern const char GLRT_28C   [NULL_CODE];   /* GL Interface - Profit Up Charge (Cost) */
extern const char GLRT_29C   [NULL_CODE];   /* GL Interface - Expense Up Charge (Cost) */
extern const char GLRT_30C   [NULL_CODE];   /* GL Interface - Transfers In (Cost) */
extern const char GLRT_31C   [NULL_CODE];   /* GL Interface - Book Transfers In (Cost) */
extern const char GLRT_32C   [NULL_CODE];   /* GL Interface - Transfers Out (Cost) */
extern const char GLRT_33C   [NULL_CODE];   /* GL Interface - Book Transfers Out (Cost) */
extern const char GLRT_37C   [NULL_CODE];   /* GL Interface - Intercompany In (Cost) */
extern const char GLRT_37R   [NULL_CODE];   /* GL Interface - Intercompany In (Retail) */
extern const char GLRT_38C   [NULL_CODE];   /* GL Interface - Intercompany Out (Cost) */
extern const char GLRT_38R   [NULL_CODE];   /* GL Interface - Intercompany Out (Retail) */
extern const char GLRT_62C   [NULL_CODE];   /* GL Interface - Freight Claim (Cost) */
extern const char GLRT_63C   [NULL_CODE];   /* GL Interface - WO Activity - Update Inv */
extern const char GLRT_64C   [NULL_CODE];   /* GL Interface - WO Activity - Post to Fin */
extern const char GLRT_70C   [NULL_CODE];   /* GL Interface - Cost Variance (Cost) */
extern const char GLRT_01R   [NULL_CODE];   /* GL Intf - Net Sales (Retail) */
extern const char GLRT_02R   [NULL_CODE];   /* GL Intf - Net Sales ex VAT (Retail) */
extern const char GLRT_03C   [NULL_CODE];   /* GL Intf - Non-Inv Net Sales (Cost) */
extern const char GLRT_03R   [NULL_CODE];   /* GL Intf - Non-Inv Net Sales (Retail) */
extern const char GLRT_04C   [NULL_CODE];   /* GL Intf - Returns (Cost) */
extern const char GLRT_05C   [NULL_CODE];   /* GL Intf - Non-Inv Net Sales ex VAT (C) */
extern const char GLRT_05R   [NULL_CODE];   /* GL Intf - Non-Inv Net Sales ex VAT (R) */
extern const char GLRT_06R   [NULL_CODE];   /* GL Intf - Deal Income (Sales) */
extern const char GLRT_07C   [NULL_CODE];   /* GL Intf - Deal Income (Purchase) */
extern const char GLRT_08R   [NULL_CODE];   /* GL Intf - Fixed Income Accrual */
extern const char GLRT_10R   [NULL_CODE];   /* GL Intf - Weight Variance (Retail) */
extern const char GLRT_11R   [NULL_CODE];   /* GL Intf - Markup (Retail) */
extern const char GLRT_12R   [NULL_CODE];   /* GL Intf - Markup Cancel Retail  */
extern const char GLRT_13R   [NULL_CODE];   /* GL Intf - Perm Markup Retail  */
extern const char GLRT_14R   [NULL_CODE];   /* GL Intf - Markdown Cancel (Retail) */
extern const char GLRT_16R   [NULL_CODE];   /* GL Intf - Clear Markdown (Retail) */
extern const char GLRT_20R   [NULL_CODE];   /* GL Intf - Purch (Retail) */
extern const char GLRT_22R   [NULL_CODE];   /* GL Intf - Stock Adj (Retail) */
extern const char GLRT_23R   [NULL_CODE];   /* GL Intf - Stock Adj Cogs (Retail) */
extern const char GLRT_24C   [NULL_CODE];   /* GL Intf - RTV (Cost) */
extern const char GLRT_24R   [NULL_CODE];   /* GL Intf - RTV (Retail) */
extern const char GLRT_26C   [NULL_CODE];   /* GL Intf - Freight (Cost) */
extern const char GLRT_28R   [NULL_CODE];   /* GL Intf - Profit Up Charge- Rec loc */
extern const char GLRT_29R   [NULL_CODE];   /* GL Intf - Expense Up Charges -Rec loc */
extern const char GLRT_30R   [NULL_CODE];   /* GL Intf - TSF in (Retail) */
extern const char GLRT_32R   [NULL_CODE];   /* GL Intf - TSF Out (Retail) */
extern const char GLRT_34C   [NULL_CODE];   /* GL Intf - Reclass in (Cost) */
extern const char GLRT_34R   [NULL_CODE];   /* GL Intf - Reclass in (Retail) */
extern const char GLRT_36C   [NULL_CODE];   /* GL Intf - Reclass out (Cost) */
extern const char GLRT_36R   [NULL_CODE];   /* GL Intf - Reclass out (Retail) */
extern const char GLRT_39R   [NULL_CODE];   /* GL Intf - Intercompany Margin */
extern const char GLRT_60R   [NULL_CODE];   /* GL Intf - EMPL discount (Retail) */
extern const char GLRT_61C   [NULL_CODE];   /* GL Intf - Stocktake ACTSTK (Cost) */
extern const char GLRT_61R   [NULL_CODE];   /* GL Intf - Stocktake ACTSTK (Retail) */
extern const char GLRT_62R   [NULL_CODE];   /* GL Intf - Freight Claim (Retail) */
extern const char GLRT_65C   [NULL_CODE];   /* GL Intf - Restocking Fee */
extern const char GLRT_71C   [NULL_CODE];   /* GL Intf - Cost Var Amt- Cost Mthd */
extern const char GLRT_72R   [NULL_CODE];   /* GL Intf - Cost Var Amt- Retail Mthd */
extern const char GLRT_73C   [NULL_CODE];   /* GL Intf - Cost Var Amt- Cost Mthd-FIFO */
extern const char GLRT_80C   [NULL_CODE];   /* GL Intf - Workroom Amount */
extern const char GLRT_81R   [NULL_CODE];   /* GL Intf - Cash discount Amount */
extern const char GLRT_82C   [NULL_CODE];   /* GL Intf - @SUH4@ Sales (Cost) */
extern const char GLRT_82R   [NULL_CODE];   /* GL Intf - @SUH4@ Sales (Retail) */
extern const char GLRT_83C   [NULL_CODE];   /* GL Intf - @SUH4@ Returns (Cost) */
extern const char GLRT_83R   [NULL_CODE];   /* GL Intf - @SUH4@ Returns (Retail) */
extern const char GLRT_84R   [NULL_CODE];   /* GL Intf - @SUH4@ Markups */
extern const char GLRT_85R   [NULL_CODE];   /* GL Intf - @SUH4@ Markdowns */
extern const char GLRT_86C   [NULL_CODE];   /* GL Intf - @SUH4@ Restocking Fee */
extern const char GLRT_87C   [NULL_CODE];   /* GL Intf - Vat In (Cost) */
extern const char GLRT_88R   [NULL_CODE];   /* GL Intf - Vat Out (Retail) */
extern const char GLRT_74C   [NULL_CODE];   /* Recoverable Tax at destination */
extern const char GLRT_75C   [NULL_CODE];   /* Recoverable Tax at source */

#define GLRTTT_01C      1   /* GL Interface - Net Sales (Cost) */
#define GLRTTT_04R      2   /* GL Interface - Markup (Retail) */
#define GLRTTT_15R      3   /* GL Interface - Promo Markdown (Retail) */
#define GLRTTT_17R      4   /* GL Interface - Intercompany Markup */
#define GLRTTT_18R      5   /* GL Interface - Intercompany Markdown */
#define GLRTTT_20C      6   /* GL Interface - Purchases (Cost) */
#define GLRTTT_22C      7   /* GL Interface - Stock Adj Non-COGS (Cost) */
#define GLRTTT_23C      8   /* GL Interface - Stock Adj COGS (Cost) */
#define GLRTTT_28C      9   /* GL Interface - Profit Up Charge (Cost) */
#define GLRTTT_29C     10   /* GL Interface - Expense Up Charge (Cost) */
#define GLRTTT_30C     11   /* GL Interface - Transfers In (Cost) */
#define GLRTTT_31C     12   /* GL Interface - Book Transfers In (Cost) */
#define GLRTTT_32C     13   /* GL Interface - Transfers Out (Cost) */
#define GLRTTT_33C     14   /* GL Interface - Book Transfers Out (Cost) */
#define GLRTTT_37C     15   /* GL Interface - Intercompany In (Cost) */
#define GLRTTT_37R     16   /* GL Interface - Intercompany In (Retail) */
#define GLRTTT_38C     17   /* GL Interface - Intercompany Out (Cost) */
#define GLRTTT_38R     18   /* GL Interface - Intercompany Out (Retail) */
#define GLRTTT_62C     19   /* GL Interface - Freight Claim (Cost) */
#define GLRTTT_63C     20   /* GL Interface - WO Activity - Update Inv */
#define GLRTTT_64C     21   /* GL Interface - WO Activity - Post to Fin */
#define GLRTTT_70C     22   /* GL Interface - Cost Variance (Cost) */
#define GLRTTT_01R     23   /* GL Intf - Net Sales (Retail) */
#define GLRTTT_02R     24   /* GL Intf - Net Sales ex VAT (Retail) */
#define GLRTTT_03C     25   /* GL Intf - Non-Inv Net Sales (Cost) */
#define GLRTTT_03R     26   /* GL Intf - Non-Inv Net Sales (Retail) */
#define GLRTTT_04C     27   /* GL Intf - Returns (Cost) */
#define GLRTTT_05C     28   /* GL Intf - Non-Inv Net Sales ex VAT (C) */
#define GLRTTT_05R     29   /* GL Intf - Non-Inv Net Sales ex VAT (R) */
#define GLRTTT_06R     30   /* GL Intf - Deal Income (Sales) */
#define GLRTTT_07C     31   /* GL Intf - Deal Income (Purchase) */
#define GLRTTT_08R     32   /* GL Intf - Fixed Income Accrual */
#define GLRTTT_10R     33   /* GL Intf - Weight Variance (Retail) */
#define GLRTTT_11R     34   /* GL Intf - Markup (Retail) */
#define GLRTTT_12R     35   /* GL Intf - Markup Cancel Retail  */
#define GLRTTT_13R     36   /* GL Intf - Perm Markup Retail  */
#define GLRTTT_14R     37   /* GL Intf - Markdown Cancel (Retail) */
#define GLRTTT_16R     38   /* GL Intf - Clear Markdown (Retail) */
#define GLRTTT_20R     39   /* GL Intf - Purch (Retail) */
#define GLRTTT_22R     40   /* GL Intf - Stock Adj (Retail) */
#define GLRTTT_23R     41   /* GL Intf - Stock Adj Cogs (Retail) */
#define GLRTTT_24C     42   /* GL Intf - RTV (Cost) */
#define GLRTTT_24R     43   /* GL Intf - RTV (Retail) */
#define GLRTTT_26C     44   /* GL Intf - Freight (Cost) */
#define GLRTTT_28R     45   /* GL Intf - Profit Up Charge- Rec loc */
#define GLRTTT_29R     46   /* GL Intf - Expense Up Charges -Rec loc */
#define GLRTTT_30R     47   /* GL Intf - TSF in (Retail) */
#define GLRTTT_32R     48   /* GL Intf - TSF Out (Retail) */
#define GLRTTT_34C     49   /* GL Intf - Reclass in (Cost) */
#define GLRTTT_34R     50   /* GL Intf - Reclass in (Retail) */
#define GLRTTT_36C     51   /* GL Intf - Reclass out (Cost) */
#define GLRTTT_36R     52   /* GL Intf - Reclass out (Retail) */
#define GLRTTT_39R     53   /* GL Intf - Intercompany Margin */
#define GLRTTT_60R     54   /* GL Intf - EMPL discount (Retail) */
#define GLRTTT_61C     55   /* GL Intf - Stocktake ACTSTK (Cost) */
#define GLRTTT_61R     56   /* GL Intf - Stocktake ACTSTK (Retail) */
#define GLRTTT_62R     57   /* GL Intf - Freight Claim (Retail) */
#define GLRTTT_65C     58   /* GL Intf - Restocking Fee */
#define GLRTTT_71C     59   /* GL Intf - Cost Var Amt- Cost Mthd */
#define GLRTTT_72R     60   /* GL Intf - Cost Var Amt- Retail Mthd */
#define GLRTTT_73C     61   /* GL Intf - Cost Var Amt- Cost Mthd-FIFO */
#define GLRTTT_80C     62   /* GL Intf - Workroom Amount */
#define GLRTTT_81R     63   /* GL Intf - Cash discount Amount */
#define GLRTTT_82C     64   /* GL Intf - @SUH4@ Sales (Cost) */
#define GLRTTT_82R     65   /* GL Intf - @SUH4@ Sales (Retail) */
#define GLRTTT_83C     66   /* GL Intf - @SUH4@ Returns (Cost) */
#define GLRTTT_83R     67   /* GL Intf - @SUH4@ Returns (Retail) */
#define GLRTTT_84R     68   /* GL Intf - @SUH4@ Markups */
#define GLRTTT_85R     69   /* GL Intf - @SUH4@ Markdowns */
#define GLRTTT_86C     70   /* GL Intf - @SUH4@ Restocking Fee */
#define GLRTTT_87C     71   /* GL Intf - Vat In (Cost) */
#define GLRTTT_88R     72   /* GL Intf - Vat Out (Retail) */
#define GLRTTT_74C     73   /* Recoverable Tax at destination */
#define GLRTTT_75C     74   /* Recoverable Tax at source */

#define _GLRTTT_MAX 74


/* GLST = General Ledger Single Transactions */
extern const char GLST[NULL_CODE];

extern const char GLST_01C   [NULL_CODE];   /* GL Interface - Net Sales (Cost) */
extern const char GLST_04R   [NULL_CODE];   /* GL Interface - Markup (Retail) */
extern const char GLST_15R   [NULL_CODE];   /* GL Interface - Promo Markdown (Retail) */
extern const char GLST_17R   [NULL_CODE];   /* GL Interface - Intercompany Markup */
extern const char GLST_18R   [NULL_CODE];   /* GL Interface - Intercompany Markdown */
extern const char GLST_20C   [NULL_CODE];   /* GL Interface - Purchases (Cost) */
extern const char GLST_22C   [NULL_CODE];   /* GL Interface - Stock Adj Non-COGS (Cost) */
extern const char GLST_23C   [NULL_CODE];   /* GL Interface - Stock Adj COGS (Cost) */
extern const char GLST_28C   [NULL_CODE];   /* GL Interface - Profit Up Charge (Cost) */
extern const char GLST_29C   [NULL_CODE];   /* GL Interface - Expense Up Charge (Cost) */
extern const char GLST_30C   [NULL_CODE];   /* GL Interface - Transfers In (Cost) */
extern const char GLST_31C   [NULL_CODE];   /* GL Interface - Book Transfers In (Cost) */
extern const char GLST_32C   [NULL_CODE];   /* GL Interface - Transfers Out (Cost) */
extern const char GLST_33C   [NULL_CODE];   /* GL Interface - Book Transfers Out (Cost) */
extern const char GLST_37C   [NULL_CODE];   /* GL Interface - Intercompany In (Cost) */
extern const char GLST_37R   [NULL_CODE];   /* GL Interface - Intercompany In (Retail) */
extern const char GLST_38C   [NULL_CODE];   /* GL Interface - Intercompany Out (Cost) */
extern const char GLST_38R   [NULL_CODE];   /* GL Interface - Intercompany Out (Retail) */
extern const char GLST_62C   [NULL_CODE];   /* GL Interface - Freight Claim (Cost) */
extern const char GLST_63C   [NULL_CODE];   /* GL Interface - WO Activity - Update Inv */
extern const char GLST_64C   [NULL_CODE];   /* GL Interface - WO Activity - Post to Fin */
extern const char GLST_70C   [NULL_CODE];   /* GL Interface - Cost Variance (Cost) */
extern const char GLST_01R   [NULL_CODE];   /* GL Intf - Net Sales (Retail) */
extern const char GLST_02R   [NULL_CODE];   /* GL Intf - Net Sales Retail ex VAT */
extern const char GLST_03C   [NULL_CODE];   /* GL Intf - Non-Inv Net Sales (Cost) */
extern const char GLST_03R   [NULL_CODE];   /* GL Intf - Non-Inv Net Sales (Retail) */
extern const char GLST_04C   [NULL_CODE];   /* GL Intf - Returns (Cost) */
extern const char GLST_05C   [NULL_CODE];   /* GL Intf - Non-Inv Net Sales ex VAT (C) */
extern const char GLST_05R   [NULL_CODE];   /* GL Intf - Non-Inv Net Sales ex VAT (R) */
extern const char GLST_06R   [NULL_CODE];   /* GL Intf - Deal Income (Sales) */
extern const char GLST_07C   [NULL_CODE];   /* GL Intf - Deal Income (Purchase) */
extern const char GLST_08R   [NULL_CODE];   /* GL Intf - Fixed Income Accrual */
extern const char GLST_10R   [NULL_CODE];   /* GL Intf - Weight Variance (Retail) */
extern const char GLST_11R   [NULL_CODE];   /* GL Intf - Markup (Retail) */
extern const char GLST_12R   [NULL_CODE];   /* GL Intf - Markup Cancel Retail  */
extern const char GLST_13R   [NULL_CODE];   /* GL Intf - Perm Markup Retail  */
extern const char GLST_14R   [NULL_CODE];   /* GL Intf - Markdown Cancel (Retail) */
extern const char GLST_16R   [NULL_CODE];   /* GL Intf - Clear Markdown (Retail) */
extern const char GLST_20R   [NULL_CODE];   /* GL Intf - Purch (Retail) */
extern const char GLST_22R   [NULL_CODE];   /* GL Intf - Stock Adj (Retail) */
extern const char GLST_23R   [NULL_CODE];   /* GL Intf - Stock Adj Cogs (Retail) */
extern const char GLST_24C   [NULL_CODE];   /* GL Intf - RTV (Cost) */
extern const char GLST_24R   [NULL_CODE];   /* GL Intf - RTV (Retail) */
extern const char GLST_26C   [NULL_CODE];   /* GL Intf - Freight (Cost) */
extern const char GLST_28R   [NULL_CODE];   /* GL Intf - Profit Up Charge - Rec loc */
extern const char GLST_29R   [NULL_CODE];   /* GL Intf - Expense Up Charges - Rec loc */
extern const char GLST_30R   [NULL_CODE];   /* GL Intf - TSF in (Retail) */
extern const char GLST_32R   [NULL_CODE];   /* GL Intf - TSF Out (Retail) */
extern const char GLST_34C   [NULL_CODE];   /* GL Intf - Reclass in (Cost) */
extern const char GLST_34R   [NULL_CODE];   /* GL Intf - Reclass in (Retail) */
extern const char GLST_36C   [NULL_CODE];   /* GL Intf - Reclass out (Cost) */
extern const char GLST_36R   [NULL_CODE];   /* GL Intf - Reclass out (Retail) */
extern const char GLST_39R   [NULL_CODE];   /* GL Intf - Intercompany Margin */
extern const char GLST_60R   [NULL_CODE];   /* GL Intf - EMPL discount (Retail) */
extern const char GLST_61C   [NULL_CODE];   /* GL Intf - Stocktake ACTSTK (Cost) */
extern const char GLST_61R   [NULL_CODE];   /* GL Intf - Stocktake ACTSTK (Retail) */
extern const char GLST_62R   [NULL_CODE];   /* GL Intf - Freight Claim (Retail) */
extern const char GLST_65C   [NULL_CODE];   /* GL Intf - Restocking Fee */
extern const char GLST_71C   [NULL_CODE];   /* GL Intf - Cost Var Amt- Cost Mthd */
extern const char GLST_72R   [NULL_CODE];   /* GL Intf - Cost Var Amt- Retail Mthd */
extern const char GLST_73C   [NULL_CODE];   /* GL Intf - Cost Var Amt- Cost Mthd-FIFO */
extern const char GLST_80C   [NULL_CODE];   /* GL Intf - Workroom Amount */
extern const char GLST_81R   [NULL_CODE];   /* GL Intf - Cash discount Amount */
extern const char GLST_82C   [NULL_CODE];   /* GL Intf - @SUH4@ Sales (Cost) */
extern const char GLST_82R   [NULL_CODE];   /* GL Intf - @SUH4@ Sales (Retail) */
extern const char GLST_83C   [NULL_CODE];   /* GL Intf - @SUH4@ Returns (Cost) */
extern const char GLST_83R   [NULL_CODE];   /* GL Intf - @SUH4@ Returns (Retail) */
extern const char GLST_84R   [NULL_CODE];   /* GL Intf - @SUH4@ Markups */
extern const char GLST_85R   [NULL_CODE];   /* GL Intf - @SUH4@ Markdowns */
extern const char GLST_86C   [NULL_CODE];   /* GL Intf - @SUH4@ Restocking Fee */
extern const char GLST_87C   [NULL_CODE];   /* GL Intf - Vat In (Cost) */
extern const char GLST_88R   [NULL_CODE];   /* GL Intf - Vat Out (Retail) */
extern const char GLST_74C   [NULL_CODE];   /* Recoverable Tax at destination */
extern const char GLST_75C   [NULL_CODE];   /* Recoverable Tax at source */

#define GLSTTT_01C      1   /* GL Interface - Net Sales (Cost) */
#define GLSTTT_04R      2   /* GL Interface - Markup (Retail) */
#define GLSTTT_15R      3   /* GL Interface - Promo Markdown (Retail) */
#define GLSTTT_17R      4   /* GL Interface - Intercompany Markup */
#define GLSTTT_18R      5   /* GL Interface - Intercompany Markdown */
#define GLSTTT_20C      6   /* GL Interface - Purchases (Cost) */
#define GLSTTT_22C      7   /* GL Interface - Stock Adj Non-COGS (Cost) */
#define GLSTTT_23C      8   /* GL Interface - Stock Adj COGS (Cost) */
#define GLSTTT_28C      9   /* GL Interface - Profit Up Charge (Cost) */
#define GLSTTT_29C     10   /* GL Interface - Expense Up Charge (Cost) */
#define GLSTTT_30C     11   /* GL Interface - Transfers In (Cost) */
#define GLSTTT_31C     12   /* GL Interface - Book Transfers In (Cost) */
#define GLSTTT_32C     13   /* GL Interface - Transfers Out (Cost) */
#define GLSTTT_33C     14   /* GL Interface - Book Transfers Out (Cost) */
#define GLSTTT_37C     15   /* GL Interface - Intercompany In (Cost) */
#define GLSTTT_37R     16   /* GL Interface - Intercompany In (Retail) */
#define GLSTTT_38C     17   /* GL Interface - Intercompany Out (Cost) */
#define GLSTTT_38R     18   /* GL Interface - Intercompany Out (Retail) */
#define GLSTTT_62C     19   /* GL Interface - Freight Claim (Cost) */
#define GLSTTT_63C     20   /* GL Interface - WO Activity - Update Inv */
#define GLSTTT_64C     21   /* GL Interface - WO Activity - Post to Fin */
#define GLSTTT_70C     22   /* GL Interface - Cost Variance (Cost) */
#define GLSTTT_01R     23   /* GL Intf - Net Sales (Retail) */
#define GLSTTT_02R     24   /* GL Intf - Net Sales Retail ex VAT */
#define GLSTTT_03C     25   /* GL Intf - Non-Inv Net Sales (Cost) */
#define GLSTTT_03R     26   /* GL Intf - Non-Inv Net Sales (Retail) */
#define GLSTTT_04C     27   /* GL Intf - Returns (Cost) */
#define GLSTTT_05C     28   /* GL Intf - Non-Inv Net Sales ex VAT (C) */
#define GLSTTT_05R     29   /* GL Intf - Non-Inv Net Sales ex VAT (R) */
#define GLSTTT_06R     30   /* GL Intf - Deal Income (Sales) */
#define GLSTTT_07C     31   /* GL Intf - Deal Income (Purchase) */
#define GLSTTT_08R     32   /* GL Intf - Fixed Income Accrual */
#define GLSTTT_10R     33   /* GL Intf - Weight Variance (Retail) */
#define GLSTTT_11R     34   /* GL Intf - Markup (Retail) */
#define GLSTTT_12R     35   /* GL Intf - Markup Cancel Retail  */
#define GLSTTT_13R     36   /* GL Intf - Perm Markup Retail  */
#define GLSTTT_14R     37   /* GL Intf - Markdown Cancel (Retail) */
#define GLSTTT_16R     38   /* GL Intf - Clear Markdown (Retail) */
#define GLSTTT_20R     39   /* GL Intf - Purch (Retail) */
#define GLSTTT_22R     40   /* GL Intf - Stock Adj (Retail) */
#define GLSTTT_23R     41   /* GL Intf - Stock Adj Cogs (Retail) */
#define GLSTTT_24C     42   /* GL Intf - RTV (Cost) */
#define GLSTTT_24R     43   /* GL Intf - RTV (Retail) */
#define GLSTTT_26C     44   /* GL Intf - Freight (Cost) */
#define GLSTTT_28R     45   /* GL Intf - Profit Up Charge - Rec loc */
#define GLSTTT_29R     46   /* GL Intf - Expense Up Charges - Rec loc */
#define GLSTTT_30R     47   /* GL Intf - TSF in (Retail) */
#define GLSTTT_32R     48   /* GL Intf - TSF Out (Retail) */
#define GLSTTT_34C     49   /* GL Intf - Reclass in (Cost) */
#define GLSTTT_34R     50   /* GL Intf - Reclass in (Retail) */
#define GLSTTT_36C     51   /* GL Intf - Reclass out (Cost) */
#define GLSTTT_36R     52   /* GL Intf - Reclass out (Retail) */
#define GLSTTT_39R     53   /* GL Intf - Intercompany Margin */
#define GLSTTT_60R     54   /* GL Intf - EMPL discount (Retail) */
#define GLSTTT_61C     55   /* GL Intf - Stocktake ACTSTK (Cost) */
#define GLSTTT_61R     56   /* GL Intf - Stocktake ACTSTK (Retail) */
#define GLSTTT_62R     57   /* GL Intf - Freight Claim (Retail) */
#define GLSTTT_65C     58   /* GL Intf - Restocking Fee */
#define GLSTTT_71C     59   /* GL Intf - Cost Var Amt- Cost Mthd */
#define GLSTTT_72R     60   /* GL Intf - Cost Var Amt- Retail Mthd */
#define GLSTTT_73C     61   /* GL Intf - Cost Var Amt- Cost Mthd-FIFO */
#define GLSTTT_80C     62   /* GL Intf - Workroom Amount */
#define GLSTTT_81R     63   /* GL Intf - Cash discount Amount */
#define GLSTTT_82C     64   /* GL Intf - @SUH4@ Sales (Cost) */
#define GLSTTT_82R     65   /* GL Intf - @SUH4@ Sales (Retail) */
#define GLSTTT_83C     66   /* GL Intf - @SUH4@ Returns (Cost) */
#define GLSTTT_83R     67   /* GL Intf - @SUH4@ Returns (Retail) */
#define GLSTTT_84R     68   /* GL Intf - @SUH4@ Markups */
#define GLSTTT_85R     69   /* GL Intf - @SUH4@ Markdowns */
#define GLSTTT_86C     70   /* GL Intf - @SUH4@ Restocking Fee */
#define GLSTTT_87C     71   /* GL Intf - Vat In (Cost) */
#define GLSTTT_88R     72   /* GL Intf - Vat Out (Retail) */
#define GLSTTT_74C     73   /* Recoverable Tax at destination */
#define GLSTTT_75C     74   /* Recoverable Tax at source */

#define _GLSTTT_MAX 74


/* GRLB = Group form labels */
extern const char GRLB[NULL_CODE];

extern const char GRLB_B     [NULL_CODE];   /* Buyer */
extern const char GRLB_M     [NULL_CODE];   /* Merchandiser */

#define GRLBTT_B        1   /* Buyer */
#define GRLBTT_M        2   /* Merchandiser */

#define _GRLBTT_MAX 2


/* GRSW = Repl Attr Group Type - ST/WH */
extern const char GRSW[NULL_CODE];

extern const char GRSW_S     [NULL_CODE];   /* Store */
extern const char GRSW_W     [NULL_CODE];   /* Warehouse */

#define GRSWTT_S        1   /* Store */
#define GRSWTT_W        2   /* Warehouse */

#define _GRSWTT_MAX 2


/* GRT1 = Group Type 1 */
extern const char GRT1[NULL_CODE];

extern const char GRT1_AL    [NULL_CODE];   /* All Locations */
extern const char GRT1_AS    [NULL_CODE];   /* All Stores */
extern const char GRT1_S     [NULL_CODE];   /* Store */
extern const char GRT1_C     [NULL_CODE];   /* Store Class */
extern const char GRT1_D     [NULL_CODE];   /* @OH5@ */
extern const char GRT1_R     [NULL_CODE];   /* @OH4@ */
extern const char GRT1_A     [NULL_CODE];   /* @OH3@ */
extern const char GRT1_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRT1_L     [NULL_CODE];   /* Location Trait */
extern const char GRT1_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GRT1_LLS   [NULL_CODE];   /* Location List-Store */
extern const char GRT1_AW    [NULL_CODE];   /* All Warehouses */
extern const char GRT1_W     [NULL_CODE];   /* Warehouse */
extern const char GRT1_LLW   [NULL_CODE];   /* Location List-Warehouse */
extern const char GRT1_E     [NULL_CODE];   /* External Finisher */
extern const char GRT1_AE    [NULL_CODE];   /* All External Finishers */

#define GRT1TT_AL       1   /* All Locations */
#define GRT1TT_AS       2   /* All Stores */
#define GRT1TT_S        3   /* Store */
#define GRT1TT_C        4   /* Store Class */
#define GRT1TT_D        5   /* @OH5@ */
#define GRT1TT_R        6   /* @OH4@ */
#define GRT1TT_A        7   /* @OH3@ */
#define GRT1TT_T        9   /* Transfer Zone */
#define GRT1TT_L       11   /* Location Trait */
#define GRT1TT_DW      12   /* Default Warehouse */
#define GRT1TT_LLS     13   /* Location List-Store */
#define GRT1TT_AW      14   /* All Warehouses */
#define GRT1TT_W       15   /* Warehouse */
#define GRT1TT_LLW     16   /* Location List-Warehouse */
#define GRT1TT_E       17   /* External Finisher */
#define GRT1TT_AE      18   /* All External Finishers */

#define _GRT1TT_MAX 18


/* GRT2 = Group Type Short List */
extern const char GRT2[NULL_CODE];

extern const char GRT2_S     [NULL_CODE];   /* Store */
extern const char GRT2_C     [NULL_CODE];   /* Store Class */
extern const char GRT2_W     [NULL_CODE];   /* Warehouse */
extern const char GRT2_A     [NULL_CODE];   /* All Stores */
extern const char GRT2_H     [NULL_CODE];   /* All Warehouses */
extern const char GRT2_LLS   [NULL_CODE];   /* Location List - Store */
extern const char GRT2_LLW   [NULL_CODE];   /* Location List - Warehouse */

#define GRT2TT_S        1   /* Store */
#define GRT2TT_C        2   /* Store Class */
#define GRT2TT_W        3   /* Warehouse */
#define GRT2TT_A        4   /* All Stores */
#define GRT2TT_H        5   /* All Warehouses */
#define GRT2TT_LLS      6   /* Location List - Store */
#define GRT2TT_LLW      7   /* Location List - Warehouse */

#define _GRT2TT_MAX 7


/* GRT3 = Group type 3 */
extern const char GRT3[NULL_CODE];

extern const char GRT3_S     [NULL_CODE];   /* Store */
extern const char GRT3_C     [NULL_CODE];   /* Store Class */
extern const char GRT3_D     [NULL_CODE];   /* @OH5@ */
extern const char GRT3_R     [NULL_CODE];   /* @OH4@ */
extern const char GRT3_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRT3_L     [NULL_CODE];   /* Location Trait */
extern const char GRT3_DW    [NULL_CODE];   /* Default Wh */
extern const char GRT3_A     [NULL_CODE];   /* All Stores */

#define GRT3TT_S        1   /* Store */
#define GRT3TT_C        2   /* Store Class */
#define GRT3TT_D        4   /* @OH5@ */
#define GRT3TT_R        5   /* @OH4@ */
#define GRT3TT_T        9   /* Transfer Zone */
#define GRT3TT_L       10   /* Location Trait */
#define GRT3TT_DW      11   /* Default Wh */
#define GRT3TT_A       12   /* All Stores */

#define _GRT3TT_MAX 12


/* GRT4 = Group Type 4 */
extern const char GRT4[NULL_CODE];

extern const char GRT4_AL    [NULL_CODE];   /* All Locations */
extern const char GRT4_AS    [NULL_CODE];   /* All Stores */
extern const char GRT4_S     [NULL_CODE];   /* Store */
extern const char GRT4_C     [NULL_CODE];   /* Store Class */
extern const char GRT4_D     [NULL_CODE];   /* @OH5@ */
extern const char GRT4_R     [NULL_CODE];   /* @OH4@ */
extern const char GRT4_A     [NULL_CODE];   /* @OH3@ */
extern const char GRT4_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRT4_L     [NULL_CODE];   /* Location Trait */
extern const char GRT4_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GRT4_W     [NULL_CODE];   /* Warehouse */
extern const char GRT4_AW    [NULL_CODE];   /* All Warehouses */
extern const char GRT4_LLS   [NULL_CODE];   /* Location List-Store */
extern const char GRT4_LLW   [NULL_CODE];   /* Location List-Warehouse */

#define GRT4TT_AL       1   /* All Locations */
#define GRT4TT_AS       2   /* All Stores */
#define GRT4TT_S        3   /* Store */
#define GRT4TT_C        4   /* Store Class */
#define GRT4TT_D        5   /* @OH5@ */
#define GRT4TT_R        6   /* @OH4@ */
#define GRT4TT_A        7   /* @OH3@ */
#define GRT4TT_T        9   /* Transfer Zone */
#define GRT4TT_L       11   /* Location Trait */
#define GRT4TT_DW      12   /* Default Warehouse */
#define GRT4TT_W       13   /* Warehouse */
#define GRT4TT_AW      14   /* All Warehouses */
#define GRT4TT_LLS     15   /* Location List-Store */
#define GRT4TT_LLW     16   /* Location List-Warehouse */

#define _GRT4TT_MAX 16


/* GRT5 = Group Type 5 */
extern const char GRT5[NULL_CODE];

extern const char GRT5_S     [NULL_CODE];   /* Store */
extern const char GRT5_C     [NULL_CODE];   /* Store Class */
extern const char GRT5_D     [NULL_CODE];   /* @OH5@ */
extern const char GRT5_R     [NULL_CODE];   /* @OH4@ */
extern const char GRT5_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRT5_L     [NULL_CODE];   /* Location Trait */
extern const char GRT5_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GRT5_A     [NULL_CODE];   /* All Stores */

#define GRT5TT_S        1   /* Store */
#define GRT5TT_C        2   /* Store Class */
#define GRT5TT_D        3   /* @OH5@ */
#define GRT5TT_R        4   /* @OH4@ */
#define GRT5TT_T        8   /* Transfer Zone */
#define GRT5TT_L        9   /* Location Trait */
#define GRT5TT_DW      10   /* Default Warehouse */
#define GRT5TT_A       11   /* All Stores */

#define _GRT5TT_MAX 11


/* GRT6 = Group Type 6 */
extern const char GRT6[NULL_CODE];

extern const char GRT6_S     [NULL_CODE];   /* Store */
extern const char GRT6_C     [NULL_CODE];   /* Store Class */
extern const char GRT6_D     [NULL_CODE];   /* @OH5@ */
extern const char GRT6_R     [NULL_CODE];   /* @OH4@ */
extern const char GRT6_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRT6_L     [NULL_CODE];   /* Location Trait */
extern const char GRT6_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GRT6_A     [NULL_CODE];   /* All Stores */
extern const char GRT6_LL    [NULL_CODE];   /* Location List */

#define GRT6TT_S        1   /* Store */
#define GRT6TT_C        2   /* Store Class */
#define GRT6TT_D        3   /* @OH5@ */
#define GRT6TT_R        4   /* @OH4@ */
#define GRT6TT_T        8   /* Transfer Zone */
#define GRT6TT_L       10   /* Location Trait */
#define GRT6TT_DW      11   /* Default Warehouse */
#define GRT6TT_A       12   /* All Stores */
#define GRT6TT_LL      13   /* Location List */

#define _GRT6TT_MAX 13


/* GRT7 = Group Type Store Add */
extern const char GRT7[NULL_CODE];

extern const char GRT7_S     [NULL_CODE];   /* Store */
extern const char GRT7_C     [NULL_CODE];   /* Store Class */
extern const char GRT7_D     [NULL_CODE];   /* @OH5@ */
extern const char GRT7_R     [NULL_CODE];   /* @OH4@ */
extern const char GRT7_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRT7_L     [NULL_CODE];   /* Location Traits */
extern const char GRT7_A     [NULL_CODE];   /* All Stores */
extern const char GRT7_LL    [NULL_CODE];   /* Location List */

#define GRT7TT_S        1   /* Store */
#define GRT7TT_C        2   /* Store Class */
#define GRT7TT_D        3   /* @OH5@ */
#define GRT7TT_R        4   /* @OH4@ */
#define GRT7TT_T        6   /* Transfer Zone */
#define GRT7TT_L        7   /* Location Traits */
#define GRT7TT_A        8   /* All Stores */
#define GRT7TT_LL       9   /* Location List */

#define _GRT7TT_MAX 9


/* GRT8 = Repl Attribute Group Type - Stores */
extern const char GRT8[NULL_CODE];

extern const char GRT8_S     [NULL_CODE];   /* Store */
extern const char GRT8_C     [NULL_CODE];   /* Store Class */
extern const char GRT8_D     [NULL_CODE];   /* @OH5@ */
extern const char GRT8_R     [NULL_CODE];   /* @OH4@ */
extern const char GRT8_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRT8_L     [NULL_CODE];   /* Location Trait */
extern const char GRT8_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GRT8_AS    [NULL_CODE];   /* All Stores */
extern const char GRT8_LLS   [NULL_CODE];   /* Location List-Store */

#define GRT8TT_S        1   /* Store */
#define GRT8TT_C        2   /* Store Class */
#define GRT8TT_D        3   /* @OH5@ */
#define GRT8TT_R        4   /* @OH4@ */
#define GRT8TT_T        6   /* Transfer Zone */
#define GRT8TT_L        7   /* Location Trait */
#define GRT8TT_DW       8   /* Default Warehouse */
#define GRT8TT_AS       9   /* All Stores */
#define GRT8TT_LLS     10   /* Location List-Store */

#define _GRT8TT_MAX 10


/* GRT9 = Repl Attribute Group Type - Warehouse */
extern const char GRT9[NULL_CODE];

extern const char GRT9_W     [NULL_CODE];   /* Warehouse */
extern const char GRT9_AW    [NULL_CODE];   /* All Warehouses */
extern const char GRT9_LLW   [NULL_CODE];   /* Location List-Warehouse */

#define GRT9TT_W        1   /* Warehouse */
#define GRT9TT_AW       2   /* All Warehouses */
#define GRT9TT_LLW      3   /* Location List-Warehouse */

#define _GRT9TT_MAX 3


/* GRTA = Group Type 1 without Finishers */
extern const char GRTA[NULL_CODE];

extern const char GRTA_AS    [NULL_CODE];   /* All Stores */
extern const char GRTA_S     [NULL_CODE];   /* Store */
extern const char GRTA_C     [NULL_CODE];   /* Store Class */
extern const char GRTA_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GRTA_AW    [NULL_CODE];   /* All Warehouses */
extern const char GRTA_W     [NULL_CODE];   /* Warehouse */
extern const char GRTA_LLS   [NULL_CODE];   /* Location List - Store */
extern const char GRTA_LLW   [NULL_CODE];   /* Location List - Warehouse */
extern const char GRTA_D     [NULL_CODE];   /* @OH5@ */
extern const char GRTA_R     [NULL_CODE];   /* @OH4@ */
extern const char GRTA_A     [NULL_CODE];   /* @OH3@ */
extern const char GRTA_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRTA_L     [NULL_CODE];   /* Location Trait */

#define GRTATT_AS       2   /* All Stores */
#define GRTATT_S        3   /* Store */
#define GRTATT_C        4   /* Store Class */
#define GRTATT_DW       5   /* Default Warehouse */
#define GRTATT_AW       6   /* All Warehouses */
#define GRTATT_W        7   /* Warehouse */
#define GRTATT_LLS      8   /* Location List - Store */
#define GRTATT_LLW      9   /* Location List - Warehouse */
#define GRTATT_D       10   /* @OH5@ */
#define GRTATT_R       11   /* @OH4@ */
#define GRTATT_A       12   /* @OH3@ */
#define GRTATT_T       14   /* Transfer Zone */
#define GRTATT_L       16   /* Location Trait */

#define _GRTATT_MAX 16


/* GRTC = COST CHANGE BY LOCATION - LOC TYPE */
extern const char GRTC[NULL_CODE];

extern const char GRTC_AL    [NULL_CODE];   /* All Locations */
extern const char GRTC_AS    [NULL_CODE];   /* All Stores */
extern const char GRTC_S     [NULL_CODE];   /* Store */
extern const char GRTC_C     [NULL_CODE];   /* Store Class */
extern const char GRTC_R     [NULL_CODE];   /* @OH4@ */
extern const char GRTC_A     [NULL_CODE];   /* @OH3@ */
extern const char GRTC_L     [NULL_CODE];   /* Location Trait */
extern const char GRTC_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GRTC_LLS   [NULL_CODE];   /* Location List-Store */
extern const char GRTC_AW    [NULL_CODE];   /* All Warehouses */
extern const char GRTC_W     [NULL_CODE];   /* Warehouse */
extern const char GRTC_LLW   [NULL_CODE];   /* Location List-Warehouse */

#define GRTCTT_AL       1   /* All Locations */
#define GRTCTT_AS       2   /* All Stores */
#define GRTCTT_S        3   /* Store */
#define GRTCTT_C        4   /* Store Class */
#define GRTCTT_R        6   /* @OH4@ */
#define GRTCTT_A        7   /* @OH3@ */
#define GRTCTT_L        8   /* Location Trait */
#define GRTCTT_DW       9   /* Default Warehouse */
#define GRTCTT_LLS     10   /* Location List-Store */
#define GRTCTT_AW      11   /* All Warehouses */
#define GRTCTT_W       12   /* Warehouse */
#define GRTCTT_LLW     13   /* Location List-Warehouse */

#define _GRTCTT_MAX 13


/* GRTP = Group Types - Price History and Margin */
extern const char GRTP[NULL_CODE];

extern const char GRTP_A     [NULL_CODE];   /* All Locations */
extern const char GRTP_S     [NULL_CODE];   /* Single Location */
extern const char GRTP_L     [NULL_CODE];   /* Location List */

#define GRTPTT_A        1   /* All Locations */
#define GRTPTT_S        2   /* Single Location */
#define GRTPTT_L        3   /* Location List */

#define _GRTPTT_MAX 3


/* GRTV = Group Type Virtual */
extern const char GRTV[NULL_CODE];

extern const char GRTV_AL    [NULL_CODE];   /* All Locations */
extern const char GRTV_AS    [NULL_CODE];   /* All Stores */
extern const char GRTV_S     [NULL_CODE];   /* Store */
extern const char GRTV_C     [NULL_CODE];   /* Store Class */
extern const char GRTV_D     [NULL_CODE];   /* @OH5@ */
extern const char GRTV_R     [NULL_CODE];   /* @OH4@ */
extern const char GRTV_A     [NULL_CODE];   /* @OH3@ */
extern const char GRTV_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRTV_L     [NULL_CODE];   /* Location Trait */
extern const char GRTV_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GRTV_LLS   [NULL_CODE];   /* Location List - Store */
extern const char GRTV_AW    [NULL_CODE];   /* All Warehouses */
extern const char GRTV_W     [NULL_CODE];   /* Warehouse */
extern const char GRTV_PW    [NULL_CODE];   /* Physical Warehouse */
extern const char GRTV_LLW   [NULL_CODE];   /* Location List - Warehouse */

#define GRTVTT_AL       1   /* All Locations */
#define GRTVTT_AS       2   /* All Stores */
#define GRTVTT_S        3   /* Store */
#define GRTVTT_C        4   /* Store Class */
#define GRTVTT_D        5   /* @OH5@ */
#define GRTVTT_R        6   /* @OH4@ */
#define GRTVTT_A        7   /* @OH3@ */
#define GRTVTT_T        9   /* Transfer Zone */
#define GRTVTT_L       11   /* Location Trait */
#define GRTVTT_DW      12   /* Default Warehouse */
#define GRTVTT_LLS     13   /* Location List - Store */
#define GRTVTT_AW      14   /* All Warehouses */
#define GRTVTT_W       15   /* Warehouse */
#define GRTVTT_PW      16   /* Physical Warehouse */
#define GRTVTT_LLW     17   /* Location List - Warehouse */

#define _GRTVTT_MAX 17


/* GRTY = Group Type */
extern const char GRTY[NULL_CODE];

extern const char GRTY_S     [NULL_CODE];   /* Store */
extern const char GRTY_C     [NULL_CODE];   /* Store Class */
extern const char GRTY_D     [NULL_CODE];   /* @OH5@ */
extern const char GRTY_R     [NULL_CODE];   /* @OH4@ */
extern const char GRTY_T     [NULL_CODE];   /* Transfer Zone */
extern const char GRTY_L     [NULL_CODE];   /* Location Traits */
extern const char GRTY_A     [NULL_CODE];   /* All Stores */
extern const char GRTY_LL    [NULL_CODE];   /* Location List */

#define GRTYTT_S        1   /* Store */
#define GRTYTT_C        2   /* Store Class */
#define GRTYTT_D        3   /* @OH5@ */
#define GRTYTT_R        4   /* @OH4@ */
#define GRTYTT_T        6   /* Transfer Zone */
#define GRTYTT_L        8   /* Location Traits */
#define GRTYTT_A        9   /* All Stores */
#define GRTYTT_LL      10   /* Location List */

#define _GRTYTT_MAX 10


/* GT1E = Group Type 1 + External Finishers */
extern const char GT1E[NULL_CODE];

extern const char GT1E_AL    [NULL_CODE];   /* All Locations */
extern const char GT1E_AS    [NULL_CODE];   /* All Stores */
extern const char GT1E_S     [NULL_CODE];   /* Store */
extern const char GT1E_C     [NULL_CODE];   /* Store Class */
extern const char GT1E_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GT1E_AW    [NULL_CODE];   /* All Warehouses */
extern const char GT1E_W     [NULL_CODE];   /* Warehouse */
extern const char GT1E_AE    [NULL_CODE];   /* All External Finishers */
extern const char GT1E_E     [NULL_CODE];   /* External Finisher */
extern const char GT1E_L     [NULL_CODE];   /* Location Trait */
extern const char GT1E_LLS   [NULL_CODE];   /* Location List - Store */
extern const char GT1E_LLW   [NULL_CODE];   /* Location List - Warehouse */
extern const char GT1E_D     [NULL_CODE];   /* @OH5@ */
extern const char GT1E_R     [NULL_CODE];   /* @OH4@ */
extern const char GT1E_A     [NULL_CODE];   /* @OH3@ */
extern const char GT1E_T     [NULL_CODE];   /* Transfer Zone */
extern const char GT1E_ULOU  [NULL_CODE];   /* Unmatched Location Org Units */

#define GT1ETT_AL       1   /* All Locations */
#define GT1ETT_AS       2   /* All Stores */
#define GT1ETT_S        3   /* Store */
#define GT1ETT_C        4   /* Store Class */
#define GT1ETT_DW       5   /* Default Warehouse */
#define GT1ETT_AW       6   /* All Warehouses */
#define GT1ETT_W        7   /* Warehouse */
#define GT1ETT_AE       8   /* All External Finishers */
#define GT1ETT_E        9   /* External Finisher */
#define GT1ETT_L       10   /* Location Trait */
#define GT1ETT_LLS     11   /* Location List - Store */
#define GT1ETT_LLW     12   /* Location List - Warehouse */
#define GT1ETT_D       13   /* @OH5@ */
#define GT1ETT_R       14   /* @OH4@ */
#define GT1ETT_A       15   /* @OH3@ */
#define GT1ETT_T       17   /* Transfer Zone */
#define GT1ETT_ULOU    18   /* Unmatched Location Org Units */

#define _GT1ETT_MAX 18


/* GT3B = Group Type 3 B */
extern const char GT3B[NULL_CODE];

extern const char GT3B_S     [NULL_CODE];   /* Store */
extern const char GT3B_C     [NULL_CODE];   /* Store Class */
extern const char GT3B_D     [NULL_CODE];   /* @OH5@ */
extern const char GT3B_R     [NULL_CODE];   /* @OH4@ */
extern const char GT3B_T     [NULL_CODE];   /* Transfer Zone */
extern const char GT3B_L     [NULL_CODE];   /* Location Trait */
extern const char GT3B_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GT3B_A     [NULL_CODE];   /* All Stores */
extern const char GT3B_LL    [NULL_CODE];   /* Location List */

#define GT3BTT_S        1   /* Store */
#define GT3BTT_C        2   /* Store Class */
#define GT3BTT_D        4   /* @OH5@ */
#define GT3BTT_R        5   /* @OH4@ */
#define GT3BTT_T       10   /* Transfer Zone */
#define GT3BTT_L       11   /* Location Trait */
#define GT3BTT_DW      12   /* Default Warehouse */
#define GT3BTT_A       13   /* All Stores */
#define GT3BTT_LL      14   /* Location List */

#define _GT3BTT_MAX 14


/* GT6I = Group Type 6 + Internal Finisher */
extern const char GT6I[NULL_CODE];


/* GTMR = Group Type - MRTs */
extern const char GTMR[NULL_CODE];

extern const char GTMR_S     [NULL_CODE];   /* Store */
extern const char GTMR_W     [NULL_CODE];   /* Warehouse */
extern const char GTMR_AS    [NULL_CODE];   /* All Stores */
extern const char GTMR_AW    [NULL_CODE];   /* All Warehouses */
extern const char GTMR_LLS   [NULL_CODE];   /* Location List - Store */
extern const char GTMR_LLW   [NULL_CODE];   /* Location List - Warehouse */
extern const char GTMR_T     [NULL_CODE];   /* Transfer Zone */
extern const char GTMR_A     [NULL_CODE];   /* @OH3@ */
extern const char GTMR_R     [NULL_CODE];   /* @OH4@ */
extern const char GTMR_D     [NULL_CODE];   /* @OH5@ */
extern const char GTMR_L     [NULL_CODE];   /* Location Trait */
extern const char GTMR_C     [NULL_CODE];   /* Store Class */

#define GTMRTT_S        1   /* Store */
#define GTMRTT_W        2   /* Warehouse */
#define GTMRTT_AS       3   /* All Stores */
#define GTMRTT_AW       4   /* All Warehouses */
#define GTMRTT_LLS      5   /* Location List - Store */
#define GTMRTT_LLW      6   /* Location List - Warehouse */
#define GTMRTT_T        7   /* Transfer Zone */
#define GTMRTT_A        8   /* @OH3@ */
#define GTMRTT_R        9   /* @OH4@ */
#define GTMRTT_D       10   /* @OH5@ */
#define GTMRTT_L       11   /* Location Trait */
#define GTMRTT_C       12   /* Store Class */

#define _GTMRTT_MAX 12


/* GTSE = Group Type Short List + Ext Finishers */
extern const char GTSE[NULL_CODE];

extern const char GTSE_AS    [NULL_CODE];   /* All Stores */
extern const char GTSE_S     [NULL_CODE];   /* Store */
extern const char GTSE_C     [NULL_CODE];   /* Store Class */
extern const char GTSE_AW    [NULL_CODE];   /* All Warehouses */
extern const char GTSE_W     [NULL_CODE];   /* Warehouse */
extern const char GTSE_AE    [NULL_CODE];   /* All External Finishers */
extern const char GTSE_E     [NULL_CODE];   /* External Finisher */
extern const char GTSE_LLS   [NULL_CODE];   /* Location List - Store */
extern const char GTSE_LLW   [NULL_CODE];   /* Location List - Warehouse */

#define GTSETT_AS       1   /* All Stores */
#define GTSETT_S        2   /* Store */
#define GTSETT_C        3   /* Store Class */
#define GTSETT_AW       4   /* All Warehouses */
#define GTSETT_W        5   /* Warehouse */
#define GTSETT_AE       6   /* All External Finishers */
#define GTSETT_E        7   /* External Finisher */
#define GTSETT_LLS      8   /* Location List - Store */
#define GTSETT_LLW      9   /* Location List - Warehouse */

#define _GTSETT_MAX 9


/* GTVF = Group Type Virtual + Finishers */
extern const char GTVF[NULL_CODE];

extern const char GTVF_AL    [NULL_CODE];   /* All Locations */
extern const char GTVF_AS    [NULL_CODE];   /* All Stores */
extern const char GTVF_S     [NULL_CODE];   /* Store */
extern const char GTVF_C     [NULL_CODE];   /* Store Class */
extern const char GTVF_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GTVF_AW    [NULL_CODE];   /* All Warehouses */
extern const char GTVF_W     [NULL_CODE];   /* Warehouse */
extern const char GTVF_PW    [NULL_CODE];   /* Physical Warehouse */
extern const char GTVF_AI    [NULL_CODE];   /* All Internal Finishers */
extern const char GTVF_I     [NULL_CODE];   /* Internal Finisher */
extern const char GTVF_AE    [NULL_CODE];   /* All External Finishers */
extern const char GTVF_E     [NULL_CODE];   /* External Finisher */
extern const char GTVF_LLS   [NULL_CODE];   /* Location List - Store */
extern const char GTVF_LLW   [NULL_CODE];   /* Location List - Warehouse */
extern const char GTVF_D     [NULL_CODE];   /* @OH5@ */
extern const char GTVF_R     [NULL_CODE];   /* @OH4@ */
extern const char GTVF_A     [NULL_CODE];   /* @OH3@ */
extern const char GTVF_T     [NULL_CODE];   /* Transfer Zone */
extern const char GTVF_L     [NULL_CODE];   /* Location Trait */
extern const char GTVF_ULOU  [NULL_CODE];   /* Unmatched Location Org Units */

#define GTVFTT_AL       1   /* All Locations */
#define GTVFTT_AS       2   /* All Stores */
#define GTVFTT_S        3   /* Store */
#define GTVFTT_C        4   /* Store Class */
#define GTVFTT_DW       5   /* Default Warehouse */
#define GTVFTT_AW       6   /* All Warehouses */
#define GTVFTT_W        7   /* Warehouse */
#define GTVFTT_PW       8   /* Physical Warehouse */
#define GTVFTT_AI       9   /* All Internal Finishers */
#define GTVFTT_I       10   /* Internal Finisher */
#define GTVFTT_AE      11   /* All External Finishers */
#define GTVFTT_E       12   /* External Finisher */
#define GTVFTT_LLS     14   /* Location List - Store */
#define GTVFTT_LLW     15   /* Location List - Warehouse */
#define GTVFTT_D       16   /* @OH5@ */
#define GTVFTT_R       17   /* @OH4@ */
#define GTVFTT_A       18   /* @OH3@ */
#define GTVFTT_T       20   /* Transfer Zone */
#define GTVFTT_L       22   /* Location Trait */
#define GTVFTT_ULOU    23   /* Unmatched Location Org Units */

#define _GTVFTT_MAX 23


/* GTVI = Group Type with Internal Finishers */
extern const char GTVI[NULL_CODE];

extern const char GTVI_AL    [NULL_CODE];   /* All Locations */
extern const char GTVI_AS    [NULL_CODE];   /* All Stores */
extern const char GTVI_S     [NULL_CODE];   /* Store */
extern const char GTVI_C     [NULL_CODE];   /* Store Class */
extern const char GTVI_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GTVI_AW    [NULL_CODE];   /* All Warehouses */
extern const char GTVI_W     [NULL_CODE];   /* Warehouse */
extern const char GTVI_PW    [NULL_CODE];   /* Physical Warehouse */
extern const char GTVI_AI    [NULL_CODE];   /* All Internal Finishers */
extern const char GTVI_I     [NULL_CODE];   /* Internal Finisher */
extern const char GTVI_LLS   [NULL_CODE];   /* Location List - Store */
extern const char GTVI_LLW   [NULL_CODE];   /* Location List - Warehouse */
extern const char GTVI_D     [NULL_CODE];   /* @OH5@ */
extern const char GTVI_R     [NULL_CODE];   /* @OH4@ */
extern const char GTVI_A     [NULL_CODE];   /* @OH3@ */
extern const char GTVI_T     [NULL_CODE];   /* Transfer Zone */
extern const char GTVI_L     [NULL_CODE];   /* Location Trait */

#define GTVITT_AL       1   /* All Locations */
#define GTVITT_AS       2   /* All Stores */
#define GTVITT_S        3   /* Store */
#define GTVITT_C        4   /* Store Class */
#define GTVITT_DW       5   /* Default Warehouse */
#define GTVITT_AW       6   /* All Warehouses */
#define GTVITT_W        7   /* Warehouse */
#define GTVITT_PW       8   /* Physical Warehouse */
#define GTVITT_AI       9   /* All Internal Finishers */
#define GTVITT_I       10   /* Internal Finisher */
#define GTVITT_LLS     11   /* Location List - Store */
#define GTVITT_LLW     12   /* Location List - Warehouse */
#define GTVITT_D       13   /* @OH5@ */
#define GTVITT_R       14   /* @OH4@ */
#define GTVITT_A       15   /* @OH3@ */
#define GTVITT_T       17   /* Transfer Zone */
#define GTVITT_L       19   /* Location Trait */

#define _GTVITT_MAX 19


/* GTY4 = Group Type 4 */
extern const char GTY4[NULL_CODE];

extern const char GTY4_S     [NULL_CODE];   /* Store */
extern const char GTY4_C     [NULL_CODE];   /* Store Class */
extern const char GTY4_D     [NULL_CODE];   /* @OH5@ */
extern const char GTY4_R     [NULL_CODE];   /* @OH4@ */
extern const char GTY4_MA    [NULL_CODE];   /* Min Area */
extern const char GTY4_MX    [NULL_CODE];   /* Max Area */
extern const char GTY4_T     [NULL_CODE];   /* Transfer Zone */
extern const char GTY4_L     [NULL_CODE];   /* Location Trait */
extern const char GTY4_DW    [NULL_CODE];   /* Default Warehouse */
extern const char GTY4_A     [NULL_CODE];   /* All Stores */

#define GTY4TT_S        1   /* Store */
#define GTY4TT_C        2   /* Store Class */
#define GTY4TT_D        3   /* @OH5@ */
#define GTY4TT_R        4   /* @OH4@ */
#define GTY4TT_MA       5   /* Min Area */
#define GTY4TT_MX       6   /* Max Area */
#define GTY4TT_T        8   /* Transfer Zone */
#define GTY4TT_L       10   /* Location Trait */
#define GTY4TT_DW      11   /* Default Warehouse */
#define GTY4TT_A       12   /* All Stores */

#define _GTY4TT_MAX 12


/* HSEN = Handling Sensitivity */
extern const char HSEN[NULL_CODE];

extern const char HSEN_FRAG  [NULL_CODE];   /* Fragile */
extern const char HSEN_EXPLOD[NULL_CODE];   /* Explosive */
extern const char HSEN_COMPUS[NULL_CODE];   /* Combustable */
extern const char HSEN_AERO  [NULL_CODE];   /* Aerosol Container - flammable */
extern const char HSEN_TOXIC [NULL_CODE];   /* Toxic */

#define HSENTT_FRAG     1   /* Fragile */
#define HSENTT_EXPLOD   2   /* Explosive */
#define HSENTT_COMPUS   3   /* Combustable */
#define HSENTT_AERO     4   /* Aerosol Container - flammable */
#define HSENTT_TOXIC    5   /* Toxic */

#define _HSENTT_MAX 5


/* HTDS = HTS Description */
extern const char HTDS[NULL_CODE];


/* HTMP = Handling Temperature */
extern const char HTMP[NULL_CODE];

extern const char HTMP_ROOM  [NULL_CODE];   /* Keep at Room Temperature */
extern const char HTMP_FROZEN[NULL_CODE];   /* Keep Frozen */
extern const char HTMP_FRIDGE[NULL_CODE];   /* Keep Refrigerated */
extern const char HTMP_HOT   [NULL_CODE];   /* Keep Hot */

#define HTMPTT_ROOM     1   /* Keep at Room Temperature */
#define HTMPTT_FROZEN   2   /* Keep Frozen */
#define HTMPTT_FRIDGE   3   /* Keep Refrigerated */
#define HTMPTT_HOT      4   /* Keep Hot */

#define _HTMPTT_MAX 4


/* HTSF = HTS Fee or Tax */
extern const char HTSF[NULL_CODE];

extern const char HTSF_F     [NULL_CODE];   /* Fee */
extern const char HTSF_T     [NULL_CODE];   /* Tax */

#define HTSFTT_F        1   /* Fee */
#define HTSFTT_T        2   /* Tax */

#define _HTSFTT_MAX 2


/* HTSL = HTS Tracking Level */
extern const char HTSL[NULL_CODE];

extern const char HTSL_S     [NULL_CODE];   /* Country of Sourcing */
extern const char HTSL_M     [NULL_CODE];   /* @COM@ */

#define HTSLTT_S        1   /* Country of Sourcing */
#define HTSLTT_M        2   /* @COM@ */

#define _HTSLTT_MAX 2


/* IACS = Itemfind Action List Box (Select option) */
extern const char IACS[NULL_CODE];

extern const char IACS_SELE  [NULL_CODE];   /* Select an Item */

#define IACSTT_SELE     1   /* Select an Item */

#define _IACSTT_MAX 1


/* IACT = Itemfind Action List Box */
extern const char IACT[NULL_CODE];

extern const char IACT_NEW   [NULL_CODE];   /* New Item */
extern const char IACT_VIEW  [NULL_CODE];   /* View Item */
extern const char IACT_EDIT  [NULL_CODE];   /* Edit Item */
extern const char IACT_NEWE  [NULL_CODE];   /* New From Existing */
extern const char IACT_RETL  [NULL_CODE];   /* New Retail Change */
extern const char IACT_COST  [NULL_CODE];   /* New Cost Change */

#define IACTTT_NEW      1   /* New Item */
#define IACTTT_VIEW     2   /* View Item */
#define IACTTT_EDIT     3   /* Edit Item */
#define IACTTT_NEWE     4   /* New From Existing */
#define IACTTT_RETL     5   /* New Retail Change */
#define IACTTT_COST     6   /* New Cost Change */

#define _IACTTT_MAX 6


/* ICTS = Intercompany Transfer Location Security */
extern const char ICTS[NULL_CODE];

extern const char ICTS_LTXFTO[NULL_CODE];   /* Transfers to */
extern const char ICTS_LTXFRM[NULL_CODE];   /* Transfers from */

#define ICTSTT_LTXFTO   1   /* Transfers to */
#define ICTSTT_LTXFRM   2   /* Transfers from */

#define _ICTSTT_MAX 2


/* IDBT = Induction Download Blank Template */
extern const char IDBT[NULL_CODE];

extern const char IDBT_PODT  [NULL_CODE];   /* Purchase Orders */
extern const char IDBT_IS9M  [NULL_CODE];   /* Items */
extern const char IDBT_IS9C  [NULL_CODE];   /* Cost Changes */

#define IDBTTT_PODT     1   /* Purchase Orders */
#define IDBTTT_IS9M     2   /* Items */
#define IDBTTT_IS9C     3   /* Cost Changes */

#define _IDBTTT_MAX 3


/* IDFL = Item Default Level */
extern const char IDFL[NULL_CODE];

extern const char IDFL_T     [NULL_CODE];   /* Transaction Level */
extern const char IDFL_A     [NULL_CODE];   /* Above Transaction Level */

#define IDFLTT_T        1   /* Transaction Level */
#define IDFLTT_A        2   /* Above Transaction Level */

#define _IDFLTT_MAX 2


/* IDIL = Item-@MH4@-Item List */
extern const char IDIL[NULL_CODE];

extern const char IDIL_I     [NULL_CODE];   /* Item */
extern const char IDIL_D     [NULL_CODE];   /* @MH4@ */
extern const char IDIL_L     [NULL_CODE];   /* Item List */

#define IDILTT_I        1   /* Item */
#define IDILTT_D        2   /* @MH4@ */
#define IDILTT_L        3   /* Item List */

#define _IDILTT_MAX 3


/* IDMH = Identification Method */
extern const char IDMH[NULL_CODE];

extern const char IDMH_DRVRLC[NULL_CODE];   /* Driver's License */
extern const char IDMH_PASSPT[NULL_CODE];   /* Passport */
extern const char IDMH_MILTID[NULL_CODE];   /* Military ID */
extern const char IDMH_STRGID[NULL_CODE];   /* State or Region ID */
extern const char IDMH_STUDID[NULL_CODE];   /* Student ID */
extern const char IDMH_RSALID[NULL_CODE];   /* Resident Alien ID */

#define IDMHTT_DRVRLC   1   /* Driver's License */
#define IDMHTT_PASSPT   2   /* Passport */
#define IDMHTT_MILTID   3   /* Military ID */
#define IDMHTT_STRGID   4   /* State or Region ID */
#define IDMHTT_STUDID   5   /* Student ID */
#define IDMHTT_RSALID   6   /* Resident Alien ID */

#define _IDMHTT_MAX 6


/* IFLT = Financial Interface Line Types */
extern const char IFLT[NULL_CODE];

extern const char IFLT_I     [NULL_CODE];   /* Item */
extern const char IFLT_T     [NULL_CODE];   /* Tax */
extern const char IFLT_M     [NULL_CODE];   /* Misc. */
extern const char IFLT_F     [NULL_CODE];   /* Freight */

#define IFLTTT_I        1   /* Item */
#define IFLTTT_T        2   /* Tax */
#define IFLTTT_M        3   /* Misc. */
#define IFLTTT_F        4   /* Freight */

#define _IFLTTT_MAX 4


/* IFWN = Itemfind Canvas Names */
extern const char IFWN[NULL_CODE];

extern const char IFWN_MAIN  [NULL_CODE];   /* Item Search Window         (itemfind) */
extern const char IFWN_LIKE  [NULL_CODE];   /* Create Like Item              (itemfind) */

#define IFWNTT_MAIN     1   /* Item Search Window         (itemfind) */
#define IFWNTT_LIKE     2   /* Create Like Item              (itemfind) */

#define _IFWNTT_MAX 2


/* IGLB = Image Form Labels */
extern const char IGLB[NULL_CODE];

extern const char IGLB_DOC   [NULL_CODE];   /* DOC */
extern const char IGLB_TXT   [NULL_CODE];   /* TXT */
extern const char IGLB_XLS   [NULL_CODE];   /* XLS */
extern const char IGLB_BMP   [NULL_CODE];   /* BMP */
extern const char IGLB_CALS  [NULL_CODE];   /* CALS */
extern const char IGLB_GIF   [NULL_CODE];   /* GIF */
extern const char IGLB_JFIF  [NULL_CODE];   /* JFIF */
extern const char IGLB_PICT  [NULL_CODE];   /* PICT */
extern const char IGLB_RAS   [NULL_CODE];   /* RAS */
extern const char IGLB_TIFF  [NULL_CODE];   /* TIFF */

#define IGLBTT_DOC      1   /* DOC */
#define IGLBTT_TXT      2   /* TXT */
#define IGLBTT_XLS      3   /* XLS */
#define IGLBTT_BMP      4   /* BMP */
#define IGLBTT_CALS     5   /* CALS */
#define IGLBTT_GIF      6   /* GIF */
#define IGLBTT_JFIF     7   /* JFIF */
#define IGLBTT_PICT     8   /* PICT */
#define IGLBTT_RAS      9   /* RAS */
#define IGLBTT_TIFF    10   /* TIFF */

#define _IGLBTT_MAX 10


/* IIAL = Item Induction - List of actions */
extern const char IIAL[NULL_CODE];

extern const char IIAL_U     [NULL_CODE];   /* Upload */
extern const char IIAL_D     [NULL_CODE];   /* Download */
extern const char IIAL_DT    [NULL_CODE];   /* Download Blank Template */
extern const char IIAL_DF    [NULL_CODE];   /* Diff Finalization */

#define IIALTT_U        1   /* Upload */
#define IIALTT_D        2   /* Download */
#define IIALTT_DT       3   /* Download Blank Template */
#define IIALTT_DF       4   /* Diff Finalization */

#define _IIALTT_MAX 4


/* IILN = Item Induction - Level Names */
extern const char IILN[NULL_CODE];

extern const char IILN_1     [NULL_CODE];   /* Line */
extern const char IILN_2     [NULL_CODE];   /* Line Extension */
extern const char IILN_3     [NULL_CODE];   /* Variant */

#define IILNTT_1        1   /* Line */
#define IILNTT_2        2   /* Line Extension */
#define IILNTT_3        3   /* Variant */

#define _IILNTT_MAX 3


/* IIPS = Item Induction - List of Statuses */
extern const char IIPS[NULL_CODE];

extern const char IIPS_N     [NULL_CODE];   /* New */
extern const char IIPS_Q     [NULL_CODE];   /* In Queue */
extern const char IIPS_PR    [NULL_CODE];   /* Processing */
extern const char IIPS_PS    [NULL_CODE];   /* Processed Successfully */
extern const char IIPS_PE    [NULL_CODE];   /* Processed with errors */
extern const char IIPS_PW    [NULL_CODE];   /* Processed with warnings */

#define IIPSTT_N        1   /* New */
#define IIPSTT_Q        2   /* In Queue */
#define IIPSTT_PR       3   /* Processing */
#define IIPSTT_PS       4   /* Processed Successfully */
#define IIPSTT_PE       5   /* Processed with errors */
#define IIPSTT_PW       6   /* Processed with warnings */

#define _IIPSTT_MAX 6


/* IISL = Item Induction - List of sources */
extern const char IISL[NULL_CODE];

extern const char IISL_S9T   [NULL_CODE];   /* Spreadsheet */
extern const char IISL_RMS   [NULL_CODE];   /* RMS Tables */
extern const char IISL_STG   [NULL_CODE];   /* Staging Tables */
extern const char IISL_EXT   [NULL_CODE];   /* External System */
extern const char IISL_RESA  [NULL_CODE];   /* Resa Tables */

#define IISLTT_S9T      1   /* Spreadsheet */
#define IISLTT_RMS      2   /* RMS Tables */
#define IISLTT_STG      3   /* Staging Tables */
#define IISLTT_EXT      4   /* External System */
#define IISLTT_RESA     5   /* Resa Tables */

#define _IISLTT_MAX 5


/* IITD = Item Image Type Description */
extern const char IITD[NULL_CODE];

extern const char IITD_T     [NULL_CODE];   /* Thumbnail */
extern const char IITD_H     [NULL_CODE];   /* High */
extern const char IITD_M     [NULL_CODE];   /* Medium */
extern const char IITD_L     [NULL_CODE];   /* Low */

#define IITDTT_T        1   /* Thumbnail */
#define IITDTT_H        2   /* High */
#define IITDTT_M        3   /* Medium */
#define IITDTT_L        4   /* Low */

#define _IITDTT_MAX 4


/* ILLB = Item List Labels */
extern const char ILLB[NULL_CODE];

extern const char ILLB_VAL   [NULL_CODE];   /* UDA Value */
extern const char ILLB_MIN   [NULL_CODE];   /* Min Date */
extern const char ILLB_MAX   [NULL_CODE];   /* Max Date */
extern const char ILLB_DESC  [NULL_CODE];   /* Copied From: */

#define ILLBTT_VAL      1   /* UDA Value */
#define ILLBTT_MIN      2   /* Min Date */
#define ILLBTT_MAX      3   /* Max Date */
#define ILLBTT_DESC     4   /* Copied From: */

#define _ILLBTT_MAX 4


/* ILSD = Item List Static vs Dynamic */
extern const char ILSD[NULL_CODE];

extern const char ILSD_N     [NULL_CODE];   /* Dynamic */
extern const char ILSD_Y     [NULL_CODE];   /* Static */

#define ILSDTT_N        1   /* Dynamic */
#define ILSDTT_Y        2   /* Static */

#define _ILSDTT_MAX 2


/* ILVL = Single item, item list, or parent */
extern const char ILVL[NULL_CODE];

extern const char ILVL_I     [NULL_CODE];   /* Item */
extern const char ILVL_ST    [NULL_CODE];   /* Item Parent */
extern const char ILVL_STC   [NULL_CODE];   /* Item Parent/Diff */
extern const char ILVL_IL    [NULL_CODE];   /* Item List */

#define ILVLTT_I        1   /* Item */
#define ILVLTT_ST       2   /* Item Parent */
#define ILVLTT_STC      3   /* Item Parent/Diff */
#define ILVLTT_IL       4   /* Item List */

#define _ILVLTT_MAX 4


/* ILVS = Transaction Level Descriptions */
extern const char ILVS[NULL_CODE];

extern const char ILVS_1     [NULL_CODE];   /* Level 1 */
extern const char ILVS_2     [NULL_CODE];   /* Level 2 */
extern const char ILVS_3     [NULL_CODE];   /* Level 3 */

#define ILVSTT_1        1   /* Level 1 */
#define ILVSTT_2        2   /* Level 2 */
#define ILVSTT_3        3   /* Level 3 */

#define _ILVSTT_MAX 3


/* IMD2 = Invoice Matching Discrep Type Initials */
extern const char IMD2[NULL_CODE];

extern const char IMD2_Q     [NULL_CODE];   /* Q */
extern const char IMD2_C     [NULL_CODE];   /* C */
extern const char IMD2_V     [NULL_CODE];   /* V */
extern const char IMD2_A     [NULL_CODE];   /* A */

#define IMD2TT_Q        1   /* Q */
#define IMD2TT_C        2   /* C */
#define IMD2TT_V        3   /* V */
#define IMD2TT_A        4   /* A */

#define _IMD2TT_MAX 4


/* IMDB = Invoice Matching Debit/Credit Write */
extern const char IMDB[NULL_CODE];

extern const char IMDB_C     [NULL_CODE];   /* Write Crdt Req */
extern const char IMDB_D     [NULL_CODE];   /* Write Debit */

#define IMDBTT_C        1   /* Write Crdt Req */
#define IMDBTT_D        2   /* Write Debit */

#define _IMDBTT_MAX 2


/* IMDC = Supplier Debit Memo Code */
extern const char IMDC[NULL_CODE];

extern const char IMDC_Y     [NULL_CODE];   /* Always */
extern const char IMDC_N     [NULL_CODE];   /* Never */

#define IMDCTT_Y        1   /* Always */
#define IMDCTT_N        2   /* Never */

#define _IMDCTT_MAX 2


/* IMDS = Invoice Line Item Status */
extern const char IMDS[NULL_CODE];

extern const char IMDS_U     [NULL_CODE];   /* Unmatched */
extern const char IMDS_M     [NULL_CODE];   /* Matched */

#define IMDSTT_U        1   /* Unmatched */
#define IMDSTT_M        2   /* Matched */

#define _IMDSTT_MAX 2


/* IMDT = Invoice Matching Discrepancy Types */
extern const char IMDT[NULL_CODE];

extern const char IMDT_C     [NULL_CODE];   /* Cost */
extern const char IMDT_Q     [NULL_CODE];   /* Qty */
extern const char IMDT_V     [NULL_CODE];   /* VAT */
extern const char IMDT_A     [NULL_CODE];   /* Any */

#define IMDTTT_C        1   /* Cost */
#define IMDTTT_Q        2   /* Qty */
#define IMDTTT_V        3   /* VAT */
#define IMDTTT_A        4   /* Any */

#define _IMDTTT_MAX 4


/* IMEX = Sales Audit Import Export */
extern const char IMEX[NULL_CODE];

extern const char IMEX_I     [NULL_CODE];   /* Import */
extern const char IMEX_E     [NULL_CODE];   /* Export */

#define IMEXTT_I        1   /* Import */
#define IMEXTT_E        2   /* Export */

#define _IMEXTT_MAX 2


/* IMIT = Invoice Type */
extern const char IMIT[NULL_CODE];

extern const char IMIT_I     [NULL_CODE];   /* Merchandise Invoice */
extern const char IMIT_C     [NULL_CODE];   /* Credit Note */
extern const char IMIT_D     [NULL_CODE];   /* Debit Memo */
extern const char IMIT_M     [NULL_CODE];   /* Credit Memo */
extern const char IMIT_R     [NULL_CODE];   /* Credit Note Request */
extern const char IMIT_N     [NULL_CODE];   /* Non-Merchandise Invoice */
extern const char IMIT_O     [NULL_CODE];   /* Consignment Invoice */
extern const char IMIT_P     [NULL_CODE];   /* Debit Memo-Consignment Returns */

#define IMITTT_I        1   /* Merchandise Invoice */
#define IMITTT_C        2   /* Credit Note */
#define IMITTT_D        3   /* Debit Memo */
#define IMITTT_M        4   /* Credit Memo */
#define IMITTT_R        5   /* Credit Note Request */
#define IMITTT_N        6   /* Non-Merchandise Invoice */
#define IMITTT_O        7   /* Consignment Invoice */
#define IMITTT_P        8   /* Debit Memo-Consignment Returns */

#define _IMITTT_MAX 8


/* IMLV = Item Merchandise Level (Item, Parent) */
extern const char IMLV[NULL_CODE];

extern const char IMLV_STY   [NULL_CODE];   /* Item Parent */
extern const char IMLV_SKU   [NULL_CODE];   /* Item */

#define IMLVTT_STY      1   /* Item Parent */
#define IMLVTT_SKU      2   /* Item */

#define _IMLVTT_MAX 2


/* IMPD = Import HTS Date */
extern const char IMPD[NULL_CODE];

extern const char IMPD_W     [NULL_CODE];   /* Written Date */
extern const char IMPD_N     [NULL_CODE];   /* Not After Date */

#define IMPDTT_W        1   /* Written Date */
#define IMPDTT_N        2   /* Not After Date */

#define _IMPDTT_MAX 2


/* IMPL = Invoice Pay/Rcv Location */
extern const char IMPL[NULL_CODE];

extern const char IMPL_S     [NULL_CODE];   /* Store */
extern const char IMPL_C     [NULL_CODE];   /* Central */

#define IMPLTT_S        1   /* Store */
#define IMPLTT_C        2   /* Central */

#define _IMPLTT_MAX 2


/* IMRF = Invoice Matching Reference Type */
extern const char IMRF[NULL_CODE];

extern const char IMRF_I     [NULL_CODE];   /* Invoice */
extern const char IMRF_R     [NULL_CODE];   /* RTV */
extern const char IMRF_N     [NULL_CODE];   /* Invoice */

#define IMRFTT_I        1   /* Invoice */
#define IMRFTT_R        2   /* RTV */
#define IMRFTT_N        3   /* Invoice */

#define _IMRFTT_MAX 3


/* IMRR = Invoice Matching Credit/Debit Reasons */
extern const char IMRR[NULL_CODE];

extern const char IMRR_Q     [NULL_CODE];   /* Qty. Discrepancy */
extern const char IMRR_C     [NULL_CODE];   /* Cost Discrepancy */
extern const char IMRR_D     [NULL_CODE];   /* Discount Discrepancy */
extern const char IMRR_V     [NULL_CODE];   /* VAT Discrepancy */
extern const char IMRR_R     [NULL_CODE];   /* RTV */
extern const char IMRR_B     [NULL_CODE];   /* Qty and Cost Discrepancies */
extern const char IMRR_VFM   [NULL_CODE];   /* Vendor Funded Markdown */

#define IMRRTT_Q        1   /* Qty. Discrepancy */
#define IMRRTT_C        2   /* Cost Discrepancy */
#define IMRRTT_D        3   /* Discount Discrepancy */
#define IMRRTT_V        4   /* VAT Discrepancy */
#define IMRRTT_R        5   /* RTV */
#define IMRRTT_B        6   /* Qty and Cost Discrepancies */
#define IMRRTT_VFM      7   /* Vendor Funded Markdown */

#define _IMRRTT_MAX 7


/* IMSS = Shipment Invoice Match Status */
extern const char IMSS[NULL_CODE];

extern const char IMSS_U     [NULL_CODE];   /* Unmatched */
extern const char IMSS_P     [NULL_CODE];   /* Partially Matched */
extern const char IMSS_M     [NULL_CODE];   /* Matched */
extern const char IMSS_C     [NULL_CODE];   /* Closed */

#define IMSSTT_U        1   /* Unmatched */
#define IMSSTT_P        2   /* Partially Matched */
#define IMSSTT_M        3   /* Matched */
#define IMSSTT_C        4   /* Closed */

#define _IMSSTT_MAX 4


/* IMST = Invoice Header Status Types */
extern const char IMST[NULL_CODE];

extern const char IMST_U     [NULL_CODE];   /* Unmatched */
extern const char IMST_R     [NULL_CODE];   /* Partially Matched */
extern const char IMST_M     [NULL_CODE];   /* Matched */
extern const char IMST_A     [NULL_CODE];   /* Approved */
extern const char IMST_P     [NULL_CODE];   /* Posted */

#define IMSTTT_U        1   /* Unmatched */
#define IMSTTT_R        2   /* Partially Matched */
#define IMSTTT_M        3   /* Matched */
#define IMSTTT_A        4   /* Approved */
#define IMSTTT_P        5   /* Posted */

#define _IMSTTT_MAX 5


/* IMT2 = Invoice Matching EDIT/VIEW Types */
extern const char IMT2[NULL_CODE];

extern const char IMT2_I     [NULL_CODE];   /* Merchandise Invoice */
extern const char IMT2_C     [NULL_CODE];   /* Credit Note */
extern const char IMT2_D     [NULL_CODE];   /* Debit Memo */
extern const char IMT2_M     [NULL_CODE];   /* Credit Memo */
extern const char IMT2_R     [NULL_CODE];   /* Credit Note Request */
extern const char IMT2_N     [NULL_CODE];   /* Non-Merchandise Invoice */

#define IMT2TT_I        1   /* Merchandise Invoice */
#define IMT2TT_C        2   /* Credit Note */
#define IMT2TT_D        3   /* Debit Memo */
#define IMT2TT_M        4   /* Credit Memo */
#define IMT2TT_R        5   /* Credit Note Request */
#define IMT2TT_N        6   /* Non-Merchandise Invoice */

#define _IMT2TT_MAX 6


/* INDT = Invoice Discount Type */
extern const char INDT[NULL_CODE];

extern const char INDT_P     [NULL_CODE];   /* Percent */
extern const char INDT_A     [NULL_CODE];   /* Amount */

#define INDTTT_P        1   /* Percent */
#define INDTTT_A        2   /* Amount */

#define _INDTTT_MAX 2


/* INGN = Invoice Cost Type */
extern const char INGN[NULL_CODE];

extern const char INGN_G     [NULL_CODE];   /* Gross */
extern const char INGN_N     [NULL_CODE];   /* Net */

#define INGNTT_G        1   /* Gross */
#define INGNTT_N        2   /* Net */

#define _INGNTT_MAX 2


/* INRF = Reference types for invoice credit notes */
extern const char INRF[NULL_CODE];

extern const char INRF_INVC  [NULL_CODE];   /* Invoice */
extern const char INRF_RTV   [NULL_CODE];   /* RTV */
extern const char INRF_VFMD  [NULL_CODE];   /* Vendor-funded Markdown */
extern const char INRF_SDC   [NULL_CODE];   /* Stand Alone Debit/Credit */

#define INRFTT_INVC     1   /* Invoice */
#define INRFTT_RTV      2   /* RTV */
#define INRFTT_VFMD     3   /* Vendor-funded Markdown */
#define INRFTT_SDC      4   /* Stand Alone Debit/Credit */

#define _INRFTT_MAX 4


/* INRN = Inner and equivalent names */
extern const char INRN[NULL_CODE];

extern const char INRN_INR   [NULL_CODE];   /* Inner */
extern const char INRN_SCS   [NULL_CODE];   /* Sub-Case */
extern const char INRN_SPACK [NULL_CODE];   /* Sub-Pack */
extern const char INRN_EA    [NULL_CODE];   /* Eaches */

#define INRNTT_INR      1   /* Inner */
#define INRNTT_SCS      2   /* Sub-Case */
#define INRNTT_SPACK    3   /* Sub-Pack */
#define INRNTT_EA       4   /* Eaches */

#define _INRNTT_MAX 4


/* INST = Inventory Status */
extern const char INST[NULL_CODE];

extern const char INST_0     [NULL_CODE];   /* Total Stock on Hand */

#define INSTTT_0        1   /* Total Stock on Hand */

#define _INSTTT_MAX 1


/* INTS = Inv. Adj. Total Stock on Hand List Box */
extern const char INTS[NULL_CODE];

extern const char INTS_0     [NULL_CODE];   /* Total Stock on Hand */

#define INTSTT_0        1   /* Total Stock on Hand */

#define _INTSTT_MAX 1


/* INVH = Inventory History Level */
extern const char INVH[NULL_CODE];

extern const char INVH_A     [NULL_CODE];   /* All Items */
extern const char INVH_I     [NULL_CODE];   /* Items Sold Only */
extern const char INVH_N     [NULL_CODE];   /* No History */

#define INVHTT_A        1   /* All Items */
#define INVHTT_I        2   /* Items Sold Only */
#define INVHTT_N        3   /* No History */

#define _INVHTT_MAX 3


/* INVM = Inventory Movement Indicators */
extern const char INVM[NULL_CODE];

extern const char INVM_U     [NULL_CODE];   /* Unavailable */
extern const char INVM_A     [NULL_CODE];   /* Available */

#define INVMTT_U        1   /* Unavailable */
#define INVMTT_A        2   /* Available */

#define _INVMTT_MAX 2


/* INVS = Label Inventory Status */
extern const char INVS[NULL_CODE];

extern const char INVS_I     [NULL_CODE];   /* Inventory Status */

#define INVSTT_I        1   /* Inventory Status */

#define _INVSTT_MAX 1


/* INVX = Invoice Matching X-ref Types */
extern const char INVX[NULL_CODE];

extern const char INVX_O     [NULL_CODE];   /* Order No. */
extern const char INVX_A     [NULL_CODE];   /* ASN No. */

#define INVXTT_O        1   /* Order No. */
#define INVXTT_A        2   /* ASN No. */

#define _INVXTT_MAX 2


/* IREL = Item Relationship Types */
extern const char IREL[NULL_CODE];

extern const char IREL_CRSL  [NULL_CODE];   /* Cross Sell */
extern const char IREL_UPSL  [NULL_CODE];   /* Up-Sell */
extern const char IREL_SUBS  [NULL_CODE];   /* Substitution */

#define IRELTT_CRSL     1   /* Cross Sell */
#define IRELTT_UPSL     2   /* Up-Sell */
#define IRELTT_SUBS     3   /* Substitution */

#define _IRELTT_MAX 3


/* IS9T = Item Template Types */
extern const char IS9T[NULL_CODE];

extern const char IS9T_IS9M  [NULL_CODE];   /* Items */
extern const char IS9T_IS9X  [NULL_CODE];   /* XItem Data */

#define IS9TTT_IS9M     1   /* Items */
#define IS9TTT_IS9X     2   /* XItem Data */

#define _IS9TTT_MAX 2


/* ISLV = Item Service Level */
extern const char ISLV[NULL_CODE];

extern const char ISLV_GRND  [NULL_CODE];   /* Ground */
extern const char ISLV_2DAY  [NULL_CODE];   /* Second Day */
extern const char ISLV_OVRNT [NULL_CODE];   /* Overnight */
extern const char ISLV_POVRNT[NULL_CODE];   /* Priority Overnight */

#define ISLVTT_GRND     1   /* Ground */
#define ISLVTT_2DAY     2   /* Second Day */
#define ISLVTT_OVRNT    3   /* Overnight */
#define ISLVTT_POVRNT   4   /* Priority Overnight */

#define _ISLVTT_MAX 4


/* ISST = Issue Types */
extern const char ISST[NULL_CODE];

extern const char ISST_E     [NULL_CODE];   /* Error */
extern const char ISST_W     [NULL_CODE];   /* Warning */

#define ISSTTT_E        1   /* Error */
#define ISSTTT_W        2   /* Warning */

#define _ISSTTT_MAX 2


/* ITDP = Item-@MH4@ */
extern const char ITDP[NULL_CODE];

extern const char ITDP_I     [NULL_CODE];   /* Item */
extern const char ITDP_D     [NULL_CODE];   /* @MH4@ */

#define ITDPTT_I        1   /* Item */
#define ITDPTT_D        2   /* @MH4@ */

#define _ITDPTT_MAX 2


/* ITED = Item Editor Titles */
extern const char ITED[NULL_CODE];

extern const char ITED_IDESC [NULL_CODE];   /* Item Description */
extern const char ITED_PDESC [NULL_CODE];   /* Item Parent Description */
extern const char ITED_GPDESC[NULL_CODE];   /* Item Grandparent Description */
extern const char ITED_RIDESC[NULL_CODE];   /* Ref Item Description */
extern const char ITED_VPNDSC[NULL_CODE];   /* VPN Item Description */
extern const char ITED_SDESC [NULL_CODE];   /* Item Short Description */
extern const char ITED_SIDESC[NULL_CODE];   /* Secondary Item Description */

#define ITEDTT_IDESC    1   /* Item Description */
#define ITEDTT_PDESC    2   /* Item Parent Description */
#define ITEDTT_GPDESC   3   /* Item Grandparent Description */
#define ITEDTT_RIDESC   4   /* Ref Item Description */
#define ITEDTT_VPNDSC   5   /* VPN Item Description */
#define ITEDTT_SDESC    6   /* Item Short Description */
#define ITEDTT_SIDESC   7   /* Secondary Item Description */

#define _ITEDTT_MAX 7


/* ITLS = Item Level */
extern const char ITLS[NULL_CODE];

extern const char ITLS_1     [NULL_CODE];   /* Item */
extern const char ITLS_2     [NULL_CODE];   /* Item Parent/Differentiator 1 */
extern const char ITLS_3     [NULL_CODE];   /* Item Parent/Differentiator 2 */
extern const char ITLS_4     [NULL_CODE];   /* Item Parent */
extern const char ITLS_5     [NULL_CODE];   /* Item List */

#define ITLSTT_1        1   /* Item */
#define ITLSTT_2        2   /* Item Parent/Differentiator 1 */
#define ITLSTT_3        3   /* Item Parent/Differentiator 2 */
#define ITLSTT_4        4   /* Item Parent */
#define ITLSTT_5        5   /* Item List */

#define _ITLSTT_MAX 5


/* ITLV = Item Level Descriptions */
extern const char ITLV[NULL_CODE];

extern const char ITLV_1     [NULL_CODE];   /* 1 */
extern const char ITLV_2     [NULL_CODE];   /* 2 */
extern const char ITLV_3     [NULL_CODE];   /* 3 */

#define ITLVTT_1        1   /* 1 */
#define ITLVTT_2        2   /* 2 */
#define ITLVTT_3        3   /* 3 */

#define _ITLVTT_MAX 3


/* ITMT = Item Type */
extern const char ITMT[NULL_CODE];

extern const char ITMT_R     [NULL_CODE];   /* Regular Item */
extern const char ITMT_S     [NULL_CODE];   /* Simple Pack */
extern const char ITMT_C     [NULL_CODE];   /* Complex Pack */
extern const char ITMT_E     [NULL_CODE];   /* Deposit Contents */
extern const char ITMT_A     [NULL_CODE];   /* Deposit Container */
extern const char ITMT_Z     [NULL_CODE];   /* Deposit Crate */
extern const char ITMT_T     [NULL_CODE];   /* Deposit Returned Item */
extern const char ITMT_I     [NULL_CODE];   /* Consignment/Concession */
extern const char ITMT_O     [NULL_CODE];   /* Transformed Orderable */
extern const char ITMT_L     [NULL_CODE];   /* Transformed Sellable */
extern const char ITMT_P     [NULL_CODE];   /* Deposit Complex Pack */

#define ITMTTT_R        1   /* Regular Item */
#define ITMTTT_S        2   /* Simple Pack */
#define ITMTTT_C        3   /* Complex Pack */
#define ITMTTT_E        4   /* Deposit Contents */
#define ITMTTT_A        5   /* Deposit Container */
#define ITMTTT_Z        6   /* Deposit Crate */
#define ITMTTT_T        7   /* Deposit Returned Item */
#define ITMTTT_I        9   /* Consignment/Concession */
#define ITMTTT_O       10   /* Transformed Orderable */
#define ITMTTT_L       11   /* Transformed Sellable */
#define ITMTTT_P       12   /* Deposit Complex Pack */

#define _ITMTTT_MAX 12


/* ITNG = Non-Grocery Item Type */
extern const char ITNG[NULL_CODE];

extern const char ITNG_R     [NULL_CODE];   /* Regular Item */
extern const char ITNG_S     [NULL_CODE];   /* Simple Pack */
extern const char ITNG_C     [NULL_CODE];   /* Complex Pack */
extern const char ITNG_I     [NULL_CODE];   /* Consignment/Concession */

#define ITNGTT_R        1   /* Regular Item */
#define ITNGTT_S        2   /* Simple Pack */
#define ITNGTT_C        3   /* Complex Pack */
#define ITNGTT_I        4   /* Consignment/Concession */

#define _ITNGTT_MAX 4


/* ITOS = Item or Item Parent */
extern const char ITOS[NULL_CODE];

extern const char ITOS_I     [NULL_CODE];   /* Item */
extern const char ITOS_S     [NULL_CODE];   /* Item Parent */

#define ITOSTT_I        1   /* Item */
#define ITOSTT_S        2   /* Item Parent */

#define _ITOSTT_MAX 2


/* ITPK = Itemfind Pack Types */
extern const char ITPK[NULL_CODE];

extern const char ITPK_C     [NULL_CODE];   /* Complex */
extern const char ITPK_S     [NULL_CODE];   /* Simple */
extern const char ITPK_N     [NULL_CODE];   /* No Pack */

#define ITPKTT_C        1   /* Complex */
#define ITPKTT_S        2   /* Simple */
#define ITPKTT_N        3   /* No Pack */

#define _ITPKTT_MAX 3


/* ITRC = Item Transformation Codes */
extern const char ITRC[NULL_CODE];

extern const char ITRC_1     [NULL_CODE];   /* 9999 */

#define ITRCTT_1        1   /* 9999 */

#define _ITRCTT_MAX 1


/* ITST = Item Status */
extern const char ITST[NULL_CODE];

extern const char ITST_W     [NULL_CODE];   /* Worksheet */
extern const char ITST_S     [NULL_CODE];   /* Submitted */
extern const char ITST_A     [NULL_CODE];   /* Approved */
extern const char ITST_D     [NULL_CODE];   /* Delete Pending */

#define ITSTTT_W        1   /* Worksheet */
#define ITSTTT_S        2   /* Submitted */
#define ITSTTT_A        3   /* Approved */
#define ITSTTT_D        4   /* Delete Pending */

#define _ITSTTT_MAX 4


/* ITTY = Item Types */
extern const char ITTY[NULL_CODE];

extern const char ITTY_SS    [NULL_CODE];   /* Single Item */
extern const char ITTY_IL    [NULL_CODE];   /* Item List */

#define ITTYTT_SS       1   /* Single Item */
#define ITTYTT_IL       2   /* Item List */

#define _ITTYTT_MAX 2


/* IVAC = Invfind Action List Box */
extern const char IVAC[NULL_CODE];

extern const char IVAC_NEW   [NULL_CODE];   /* New */
extern const char IVAC_VIEW  [NULL_CODE];   /* View */
extern const char IVAC_MATC  [NULL_CODE];   /* Matching */

#define IVACTT_NEW      1   /* New */
#define IVACTT_VIEW     2   /* View */
#define IVACTT_MATC     3   /* Matching */

#define _IVACTT_MAX 3


/* IVLB = Invenq form labels */
extern const char IVLB[NULL_CODE];

extern const char IVLB_D     [NULL_CODE];   /* @MH4@ */
extern const char IVLB_S     [NULL_CODE];   /* Supplier */
extern const char IVLB_I     [NULL_CODE];   /* Invoice */
extern const char IVLB_C     [NULL_CODE];   /* Claim */
extern const char IVLB_G     [NULL_CODE];   /* G.W.I. */

#define IVLBTT_D        1   /* @MH4@ */
#define IVLBTT_S        2   /* Supplier */
#define IVLBTT_I        3   /* Invoice */
#define IVLBTT_C        4   /* Claim */
#define IVLBTT_G        5   /* G.W.I. */

#define _IVLBTT_MAX 5


/* IVMH = Invmatch form labels */
extern const char IVMH[NULL_CODE];

extern const char IVMH_E     [NULL_CODE];   /* Extended VAT Cost */
extern const char IVMH_A     [NULL_CODE];   /* Actual VAT Cost */

#define IVMHTT_E        1   /* Extended VAT Cost */
#define IVMHTT_A        2   /* Actual VAT Cost */

#define _IVMHTT_MAX 2


/* IVTP = Invoice Type */
extern const char IVTP[NULL_CODE];

extern const char IVTP_I     [NULL_CODE];   /* Invoice */
extern const char IVTP_C     [NULL_CODE];   /* Claim */

#define IVTPTT_I        1   /* Invoice */
#define IVTPTT_C        2   /* Claim */

#define _IVTPTT_MAX 2


/* JALT = Job Audit Logs - List of Log Types */
extern const char JALT[NULL_CODE];

extern const char JALT_E     [NULL_CODE];   /* Error */
extern const char JALT_I     [NULL_CODE];   /* Info */
extern const char JALT_W     [NULL_CODE];   /* Warning */

#define JALTTT_E        1   /* Error */
#define JALTTT_I        2   /* Info */
#define JALTTT_W        3   /* Warning */

#define _JALTTT_MAX 3


/* JAST = Job Audit Logs - List of Statuses */
extern const char JAST[NULL_CODE];

extern const char JAST_S     [NULL_CODE];   /* Started */
extern const char JAST_F     [NULL_CODE];   /* Failure */
extern const char JAST_C     [NULL_CODE];   /* Completed */
extern const char JAST_I     [NULL_CODE];   /* In Progress */

#define JASTTT_S        1   /* Started */
#define JASTTT_F        2   /* Failure */
#define JASTTT_C        3   /* Completed */
#define JASTTT_I        4   /* In Progress */

#define _JASTTT_MAX 4


/* JBST = Statuses of Asynchronous Jobs */
extern const char JBST[NULL_CODE];

extern const char JBST_N     [NULL_CODE];   /* New */
extern const char JBST_E     [NULL_CODE];   /* Completed with Error */
extern const char JBST_S     [NULL_CODE];   /* Completed Successfully */
extern const char JBST_R     [NULL_CODE];   /* Retry Job */
extern const char JBST_I     [NULL_CODE];   /* In Progress */

#define JBSTTT_N        1   /* New */
#define JBSTTT_E        2   /* Completed with Error */
#define JBSTTT_S        3   /* Completed Successfully */
#define JBSTTT_R        4   /* Retry Job */
#define JBSTTT_I        5   /* In Progress */

#define _JBSTTT_MAX 5


/* LABL = Label */
extern const char LABL[NULL_CODE];

extern const char LABL_Z     [NULL_CODE];   /* New Zone Group */
extern const char LABL_VAT   [NULL_CODE];   /* VAT Region */
extern const char LABL_POS   [NULL_CODE];   /* POS Includes VAT? */
extern const char LABL_D     [NULL_CODE];   /* Dollar */
extern const char LABL_UD    [NULL_CODE];   /* Unit and Dollar */
extern const char LABL_U     [NULL_CODE];   /* Unit */
extern const char LABL_PC    [NULL_CODE];   /* Price Change */
extern const char LABL_CE    [NULL_CODE];   /* Clearance Event */
extern const char LABL_ST    [NULL_CODE];   /* Store */
extern const char LABL_NA    [NULL_CODE];   /* Name */
extern const char LABL_DESC  [NULL_CODE];   /* Description */
extern const char LABL_ZONE  [NULL_CODE];   /* Zone */
extern const char LABL_PSC   [NULL_CODE];   /* Price Store Change */
extern const char LABL_AS    [NULL_CODE];   /* Add Stores */
extern const char LABL_PZC   [NULL_CODE];   /* Price Zone Change */
extern const char LABL_AZ    [NULL_CODE];   /* Add Zones */
extern const char LABL_C     [NULL_CODE];   /* Clear */
extern const char LABL_REG   [NULL_CODE];   /* Regular */
extern const char LABL_TRAN  [NULL_CODE];   /* Transportation ID */
extern const char LABL_ADD   [NULL_CODE];   /* Add */
extern const char LABL_CA    [NULL_CODE];   /* Case/ */
extern const char LABL_PP    [NULL_CODE];   /* Pre-Pack */
extern const char LABL_PPD   [NULL_CODE];   /* Pre-Pack Detail */
extern const char LABL_PSCW  [NULL_CODE];   /* Price Store Change (pcstore) */
extern const char LABL_PZCW  [NULL_CODE];   /* Price Zone Change             (pcstore) */
extern const char LABL_CD    [NULL_CODE];   /* Case Detail */
extern const char LABL_VSKU  [NULL_CODE];   /* View Item */
extern const char LABL_VUPC  [NULL_CODE];   /* View Child Item */
extern const char LABL_CEDT  [NULL_CODE];   /* Customs Detail */
extern const char LABL_VESS  [NULL_CODE];   /* Vessel */
extern const char LABL_VOY   [NULL_CODE];   /* Voyage/Flight */
extern const char LABL_ETD   [NULL_CODE];   /* Est. Depart Date */
extern const char LABL_OBL   [NULL_CODE];   /* Obligation */
extern const char LABL_EM    [NULL_CODE];   /* Edit Matrix */
extern const char LABL_EQ    [NULL_CODE];   /* Edit Quantity */
extern const char LABL_ER    [NULL_CODE];   /* Edit Ratio */
extern const char LABL_Q     [NULL_CODE];   /* Quantity */
extern const char LABL_RAT   [NULL_CODE];   /* Ratio */
extern const char LABL_COUR  [NULL_CODE];   /* Courier */
extern const char LABL_SHDT  [NULL_CODE];   /* Shipment Date */
extern const char LABL_MH4NAM[NULL_CODE];   /* @MH4@ Name */
extern const char LABL_TXCD  [NULL_CODE];   /* Tax Code */
extern const char LABL_ATXCD [NULL_CODE];   /* All Tax Codes */
extern const char LABL_FI    [NULL_CODE];   /*   Find Item   */
extern const char LABL_FU    [NULL_CODE];   /*   Find Child Item   */
extern const char LABL_FV    [NULL_CODE];   /*   Find VPN   */
extern const char LABL_SD    [NULL_CODE];   /* Store Detail */
extern const char LABL_ZD    [NULL_CODE];   /* Zone Detail */
extern const char LABL_AV    [NULL_CODE];   /* Availability */
extern const char LABL_PA    [NULL_CODE];   /* Pack */
extern const char LABL_LC    [NULL_CODE];   /* Landed Cost Per Unit */
extern const char LABL_SUPP  [NULL_CODE];   /* Supp. */
extern const char LABL_CART  [NULL_CODE];   /* Carton */
extern const char LABL_MON   [NULL_CODE];   /* Monetary */
extern const char LABL_SKU   [NULL_CODE];   /* Item */
extern const char LABL_DEPT  [NULL_CODE];   /* @MHP4@ */
extern const char LABL_ITEM  [NULL_CODE];   /* Items */
extern const char LABL_UPC   [NULL_CODE];   /* UPC */
extern const char LABL_VPN   [NULL_CODE];   /* VPN */
extern const char LABL_UDA   [NULL_CODE];   /* UDA */
extern const char LABL_UDAD  [NULL_CODE];   /* UDA Date */
extern const char LABL_UDAT  [NULL_CODE];   /* UDA Text */
extern const char LABL_UDAV  [NULL_CODE];   /* UDA Value */
extern const char LABL_ILST  [NULL_CODE];   /* Item List */
extern const char LABL_STY   [NULL_CODE];   /* Item Parent */
extern const char LABL_SIZE1 [NULL_CODE];   /* Size 1 */
extern const char LABL_SIZE2 [NULL_CODE];   /* Size 2 */
extern const char LABL_COLOR [NULL_CODE];   /* Color */
extern const char LABL_CUCL  [NULL_CODE];   /* Currently on Clearance */
extern const char LABL_BCK2  [NULL_CODE];   /* Back To */
extern const char LABL_PLUS  [NULL_CODE];   /* Plus Last */
extern const char LABL_LSTW  [NULL_CODE];   /* Weeks */
extern const char LABL_MH5NAM[NULL_CODE];   /* @MH5@ Name */
extern const char LABL_CI    [NULL_CODE];   /* Cancel Item */
extern const char LABL_DI    [NULL_CODE];   /* Delete Item */
extern const char LABL_DEP   [NULL_CODE];   /* @MH4@ */
extern const char LABL_ITM   [NULL_CODE];   /* Item */
extern const char LABL_PLC   [NULL_CODE];   /* Total Pack Landed Cost */
extern const char LABL_NULL  [NULL_CODE];   /* Blank */
extern const char LABL_NEWS  [NULL_CODE];   /* New Item Parent */
extern const char LABL_NEWI  [NULL_CODE];   /* New Item */
extern const char LABL_AI    [NULL_CODE];   /* Apply Item */
extern const char LABL_AL    [NULL_CODE];   /* Apply Item List */
extern const char LABL_AU    [NULL_CODE];   /* Apply Child Item */
extern const char LABL_TA    [NULL_CODE];   /* Threshold Amount */
extern const char LABL_TU    [NULL_CODE];   /* Threshold Unit */
extern const char LABL_DCD   [NULL_CODE];   /* Document Details */
extern const char LABL_ETDT  [NULL_CODE];   /* Entity Details */
extern const char LABL_TXBR  [NULL_CODE];   /* Tax Breakup */
extern const char LABL_MH6NAM[NULL_CODE];   /* @MH6@ Name */
extern const char LABL_LTCR  [NULL_CODE];   /* Letter of Credit */
extern const char LABL_PO    [NULL_CODE];   /* Order No. */
extern const char LABL_CNTR  [NULL_CODE];   /* Contract No. */
extern const char LABL_CTRY  [NULL_CODE];   /* Country */
extern const char LABL_PYMT  [NULL_CODE];   /* Letter of Credit */
extern const char LABL_SUP   [NULL_CODE];   /* Supplier */
extern const char LABL_BANK  [NULL_CODE];   /* Bank */
extern const char LABL_PTNR  [NULL_CODE];   /* Partner */
extern const char LABL_PTYP  [NULL_CODE];   /* Partner Type */
extern const char LABL_CU    [NULL_CODE];   /* Entry No. */
extern const char LABL_LO    [NULL_CODE];   /* Logistics No. */
extern const char LABL_LOBL  [NULL_CODE];   /* BOL/AWB */
extern const char LABL_LOCO  [NULL_CODE];   /* Container */
extern const char LABL_LOCI  [NULL_CODE];   /* Invoice No. */
extern const char LABL_HTSC  [NULL_CODE];   /* HTS Heading */
extern const char LABL_CB    [NULL_CODE];   /* Order Cost Basis */
extern const char LABL_EC    [NULL_CODE];   /* Expense Category */
extern const char LABL_OC    [NULL_CODE];   /* Country Of Sourcing */
extern const char LABL_TMBL  [NULL_CODE];   /* Timeline Base */
extern const char LABL_DPD   [NULL_CODE];   /* Discharge Port Description */
extern const char LABL_LPD   [NULL_CODE];   /* Lading port Description */
extern const char LABL_ZND   [NULL_CODE];   /* Zone Description */
extern const char LABL_IC    [NULL_CODE];   /* Import Country */
extern const char LABL_EXRTAB[NULL_CODE];   /* Exch. Rt. */
extern const char LABL_EURXAB[NULL_CODE];   /* Exch. Rt.(Euro) */
extern const char LABL_RDLB  [NULL_CODE];   /* Required Documents */
extern const char LABL_IHLB  [NULL_CODE];   /* HTS Assessment */
extern const char LABL_PARD  [NULL_CODE];   /* Partner/Required Documents */
extern const char LABL_ITHT  [NULL_CODE];   /* Item/HTS */
extern const char LABL_ITIM  [NULL_CODE];   /* Item/Import Attributes */
extern const char LABL_ITRD  [NULL_CODE];   /* Item/Required Documents */
extern const char LABL_ITTM  [NULL_CODE];   /* Item/Timelines */
extern const char LABL_ITEL  [NULL_CODE];   /* Eligible Tariff Treatments */
extern const char LABL_IDLB  [NULL_CODE];   /* Import Desc. */
extern const char LABL_CTRD  [NULL_CODE];   /* Country Required Documents */
extern const char LABL_CTTT  [NULL_CODE];   /* Country Tariff Treatments */
extern const char LABL_CZG   [NULL_CODE];   /* Cost Zone Group */
extern const char LABL_CZ    [NULL_CODE];   /* Cost Zone */
extern const char LABL_BP    [NULL_CODE];   /* Base Profile */
extern const char LABL_PROF  [NULL_CODE];   /* Profile Type */
extern const char LABL_IT    [NULL_CODE];   /* Item */
extern const char LABL_LOG   [NULL_CODE];   /* Logistics */
extern const char LABL_CUST  [NULL_CODE];   /* Customs */
extern const char LABL_CNT   [NULL_CODE];   /* Contract */
extern const char LABL_PORD  [NULL_CODE];   /* Purchase Order */
extern const char LABL_POIT  [NULL_CODE];   /* Order/Item */
extern const char LABL_LIST  [NULL_CODE];   /* List of */
extern const char LABL_TRTW  [NULL_CODE];   /* Associated with Trait */
extern const char LABL_MAXL  [NULL_CODE];   /* Maximum Length */
extern const char LABL_LEVEL [NULL_CODE];   /* Level */
extern const char LABL_COM   [NULL_CODE];   /* Comments */
extern const char LABL_TEXT  [NULL_CODE];   /* Text */
extern const char LABL_TERM  [NULL_CODE];   /* Terms */
extern const char LABL_IMPD  [NULL_CODE];   /* Import Description */
extern const char LABL_DCTX  [NULL_CODE];   /* Document Text */
extern const char LABL_MWIN  [NULL_CODE];   /* Maintenance Window */
extern const char LABL_HTDS  [NULL_CODE];   /* HTS Description */
extern const char LABL_SQF   [NULL_CODE];   /* Square Footage Area */
extern const char LABL_DEPS  [NULL_CODE];   /* *All @MHP4@ */
extern const char LABL_CLASS [NULL_CODE];   /* *All @MHP5@ */
extern const char LABL_SCLASS[NULL_CODE];   /* *All @MHP6@ */
extern const char LABL_LOCS  [NULL_CODE];   /* *All Locations */
extern const char LABL_TRCODE[NULL_CODE];   /* *All Tran Codes */
extern const char LABL_UOT   [NULL_CODE];   /* Unit of Transfer */
extern const char LABL_UOP   [NULL_CODE];   /* Unit of Purchase */
extern const char LABL_UOA   [NULL_CODE];   /* Unit of Allocation */
extern const char LABL_CLS   [NULL_CODE];   /* @MH5@ */
extern const char LABL_DIV   [NULL_CODE];   /* @MH2@ */
extern const char LABL_GROUP [NULL_CODE];   /* @MH3@ */
extern const char LABL_SCLS  [NULL_CODE];   /* @MH6@ */
extern const char LABL_COMP  [NULL_CODE];   /* @OH1@ */
extern const char LABL_DIS   [NULL_CODE];   /* @OH5@ */
extern const char LABL_ORC   [NULL_CODE];   /* Orig. Country */
extern const char LABL_ORGC  [NULL_CODE];   /* Orig. Ctry */
extern const char LABL_ALL   [NULL_CODE];   /* *All */
extern const char LABL_CLRS  [NULL_CODE];   /* Clearance Defaults */
extern const char LABL_EWIN  [NULL_CODE];   /* Entry Window */
extern const char LABL_PROM  [NULL_CODE];   /* Promotion */
extern const char LABL_EVDS  [NULL_CODE];   /* Event Description */
extern const char LABL_PRDS  [NULL_CODE];   /* Promotion Description */
extern const char LABL_KEY   [NULL_CODE];   /* Key Value */
extern const char LABL_TRANS [NULL_CODE];   /* Translated Value */
extern const char LABL_UOM   [NULL_CODE];   /* UOM */
extern const char LABL_MERCH [NULL_CODE];   /* Merchandise Description */
extern const char LABL_ERCODE[NULL_CODE];   /* Merchandise Description */
extern const char LABL_ACT   [NULL_CODE];   /* Actual */
extern const char LABL_PROJ  [NULL_CODE];   /* Projected */
extern const char LABL_ADF   [NULL_CODE];   /* Apply Defaults For */
extern const char LABL_WKE   [NULL_CODE];   /* Week */
extern const char LABL_TR    [NULL_CODE];   /* Transportation */
extern const char LABL_TRCO  [NULL_CODE];   /* Container */
extern const char LABL_TRBL  [NULL_CODE];   /* BOL/AWB */
extern const char LABL_TRCI  [NULL_CODE];   /* Commercial Invoice */
extern const char LABL_NEXT  [NULL_CODE];   /* Next */
extern const char LABL_CASE  [NULL_CODE];   /* Case */
extern const char LABL_SUOM  [NULL_CODE];   /* SUOM */
extern const char LABL_PAL   [NULL_CODE];   /* Pallet */
extern const char LABL_SPPACK[NULL_CODE];   /* Supp Pack */
extern const char LABL_EUROEX[NULL_CODE];   /* Exchange Rate(Euro) */
extern const char LABL_EXRATE[NULL_CODE];   /* Exchange Rate */
extern const char LABL_ORDEX [NULL_CODE];   /* Order Exchange Rate */
extern const char LABL_ISSUE [NULL_CODE];   /* Form Issue */
extern const char LABL_GVN   [NULL_CODE];   /* Gift Certificate Number */
extern const char LABL_SQST  [NULL_CODE];   /* SQL Statement */
extern const char LABL_FUEL  [NULL_CODE];   /* Fuel  */
extern const char LABL_DEFA  [NULL_CODE];   /* Default  */
extern const char LABL_DEPIMP[NULL_CODE];   /* @MH4@ Impact */
extern const char LABL_TRAW  [NULL_CODE];   /* Traits associated with  */
extern const char LABL_SLCM  [NULL_CODE];   /* Cost Maintenance          (stkldgmn) */
extern const char LABL_SLRM  [NULL_CODE];   /* Retail Maintenance          (stkldgmn) */
extern const char LABL_VCRM  [NULL_CODE];   /* Rates Maintenance          (vatcode) */
extern const char LABL_SCRT  [NULL_CODE];   /* Search Criteria */
extern const char LABL_SRLT  [NULL_CODE];   /* Search Results */
extern const char LABL_LYR1  [NULL_CODE];   /* First Layer */
extern const char LABL_LYR2  [NULL_CODE];   /* Second Layer */
extern const char LABL_ITBN  [NULL_CODE];   /* Item/Button */
extern const char LABL_INCL  [NULL_CODE];   /* Include */
extern const char LABL_EXCL  [NULL_CODE];   /* Exclude */
extern const char LABL_CRIT  [NULL_CODE];   /* Criteria */
extern const char LABL_COUP  [NULL_CODE];   /* Coupon */
extern const char LABL_PRES  [NULL_CODE];   /* Product Restriction */
extern const char LABL_CCVL  [NULL_CODE];   /* Credit Card Validation */
extern const char LABL_APLY  [NULL_CODE];   /* Apply */
extern const char LABL_DAY   [NULL_CODE];   /* Day */
extern const char LABL_TIME  [NULL_CODE];   /* Time */
extern const char LABL_DATE  [NULL_CODE];   /* Date */
extern const char LABL_TXCDE [NULL_CODE];   /* Tax Codes */
extern const char LABL_TXRT  [NULL_CODE];   /* Tax Rates */
extern const char LABL_WAST  [NULL_CODE];   /* Wastage Attributes */
extern const char LABL_TICK  [NULL_CODE];   /* Ticket Attributes */
extern const char LABL_COPY  [NULL_CODE];   /* Copy tax codes from item */
extern const char LABL_DIP   [NULL_CODE];   /* Dis. Port */
extern const char LABL_DIST  [NULL_CODE];   /* Distribute By */
extern const char LABL_ALLTLS[NULL_CODE];   /* All Totals */
extern const char LABL_SZRANG[NULL_CODE];   /* Size Range */
extern const char LABL_SZTEMP[NULL_CODE];   /* Size Template */
extern const char LABL_NULL1 [NULL_CODE];   /* NULL */
extern const char LABL_ALLOC [NULL_CODE];   /* Allocate to Location */
extern const char LABL_ALWAR [NULL_CODE];   /* Allocate from Warehouse */
extern const char LABL_SUMM  [NULL_CODE];   /* Summary */
extern const char LABL_SPDP  [NULL_CODE];   /* Supplier/@MH4@ */
extern const char LABL_G     [NULL_CODE];   /* Group */
extern const char LABL_VAL   [NULL_CODE];   /* Value */
extern const char LABL_GRP   [NULL_CODE];   /* Group */
extern const char LABL_A     [NULL_CODE];   /* Available */
extern const char LABL_IR    [NULL_CODE];   /* In Range */
extern const char LABL_DP    [NULL_CODE];   /* Delete Pending */
extern const char LABL_RFIT  [NULL_CODE];   /* Ref Item */
extern const char LABL_PARENT[NULL_CODE];   /* Item Parent/Grandparent */
extern const char LABL_DIFF1 [NULL_CODE];   /* Diff 1 */
extern const char LABL_DIFF2 [NULL_CODE];   /* Diff 2 */
extern const char LABL_FT    [NULL_CODE];   /* Feet */
extern const char LABL_M     [NULL_CODE];   /* Meters */
extern const char LABL_LCID  [NULL_CODE];   /* Letter of Credit */
extern const char LABL_UCHRG [NULL_CODE];   /* Up Charges */
extern const char LABL_MLOC  [NULL_CODE];   /* Multiple Locations */
extern const char LABL_SUBPO [NULL_CODE];   /* Submitted PO */
extern const char LABL_POTO1 [NULL_CODE];   /* 1st Order Total */
extern const char LABL_POTO2 [NULL_CODE];   /* 2nd Order Total */
extern const char LABL_SCIS  [NULL_CODE];   /* Scaling Issues */
extern const char LABL_TRIS  [NULL_CODE];   /* Truck Splitting Issues */
extern const char LABL_P     [NULL_CODE];   /* Physical */
extern const char LABL_V     [NULL_CODE];   /* Virtual */
extern const char LABL_UNKNWN[NULL_CODE];   /* Unknown */
extern const char LABL_ORD   [NULL_CODE];   /* Order */
extern const char LABL_TSF   [NULL_CODE];   /* Transfer */
extern const char LABL_ALLC  [NULL_CODE];   /* Allocation */
extern const char LABL_DOCTYP[NULL_CODE];   /* Document Type */
extern const char LABL_LOCTYP[NULL_CODE];   /* Location Type */
extern const char LABL_UCST  [NULL_CODE];   /* Unit Cost */
extern const char LABL_UELC  [NULL_CODE];   /* Unit ELC */
extern const char LABL_LD    [NULL_CODE];   /* Launch Date */
extern const char LABL_QO    [NULL_CODE];   /* Qty/Key Options */
extern const char LABL_MP    [NULL_CODE];   /* Manual Price Entry */
extern const char LABL_DC    [NULL_CODE];   /* Deposit Code */
extern const char LABL_FS    [NULL_CODE];   /* Food Stamp Ind */
extern const char LABL_WI    [NULL_CODE];   /* WIC Ind */
extern const char LABL_PT    [NULL_CODE];   /* Proportional Tare Pct */
extern const char LABL_FX    [NULL_CODE];   /* Fixed Tare Value */
extern const char LABL_FM    [NULL_CODE];   /* Fixed Tare UOM */
extern const char LABL_RE    [NULL_CODE];   /* Reward Eligible Ind */
extern const char LABL_NB    [NULL_CODE];   /* National Brand Comparison Item */
extern const char LABL_RP    [NULL_CODE];   /* Return Policy */
extern const char LABL_SS    [NULL_CODE];   /* Stop Sale Ind */
extern const char LABL_EL    [NULL_CODE];   /* Electronic Market Clubs */
extern const char LABL_RC    [NULL_CODE];   /* Report Code */
extern const char LABL_SL    [NULL_CODE];   /* Shelf Life on Select */
extern const char LABL_SR    [NULL_CODE];   /* Shelf Life on Rcpt */
extern const char LABL_IB    [NULL_CODE];   /* Investment Buy Shelf Life */
extern const char LABL_SI    [NULL_CODE];   /* Store Reorderable Ind */
extern const char LABL_RS    [NULL_CODE];   /* Rack Size */
extern const char LABL_FP    [NULL_CODE];   /* Full Pallet Item */
extern const char LABL_IM    [NULL_CODE];   /* In Store Market Basket */
extern const char LABL_SC    [NULL_CODE];   /* Storage Location */
extern const char LABL_AT    [NULL_CODE];   /* Alt Storage Location */
extern const char LABL_RI    [NULL_CODE];   /* Returnable Ind */
extern const char LABL_RF    [NULL_CODE];   /* Refundable Ind */
extern const char LABL_BI    [NULL_CODE];   /* Back Orderable Ind */
extern const char LABL_DIFF3 [NULL_CODE];   /* Diff 3 */
extern const char LABL_DIFF4 [NULL_CODE];   /* Diff 4 */
extern const char LABL_DIFFT [NULL_CODE];   /* Diff Type */
extern const char LABL_IDAPPL[NULL_CODE];   /* ID Applied */
extern const char LABL_ACDS  [NULL_CODE];   /* Activity Description */
extern const char LABL_AACT  [NULL_CODE];   /* All Activities */
extern const char LABL_SPLU  [NULL_CODE];   /* Stores */
extern const char LABL_WPLU  [NULL_CODE];   /* Warehouses */
extern const char LABL_IPLU  [NULL_CODE];   /* Internal Finishers */
extern const char LABL_EPLU  [NULL_CODE];   /* External Finishers */
extern const char LABL_WH    [NULL_CODE];   /* Warehouse */
extern const char LABL_NOEXST[NULL_CODE];   /* No longer exists in pricing system */
extern const char LABL_OWCUD [NULL_CODE];   /* Worksheet PO Clean Up Delay */
extern const char LABL_OPRCD [NULL_CODE];   /* Partially Received PO Close Delay */
extern const char LABL_OACD  [NULL_CODE];   /* Approved PO Close Delay */
extern const char LABL_DIYRS [NULL_CODE];   /* Years */
extern const char LABL_RSUD  [NULL_CODE];   /* Retention of Scheduled Updates Days */
extern const char LABL_MRD   [NULL_CODE];   /* Retention for MRT Transfers */
extern const char LABL_RRTI  [NULL_CODE];   /* RTV/RAC Transfer */
extern const char LABL_MSTSSR[NULL_CODE];   /* Store-to-Store */
extern const char LABL_MWTSSR[NULL_CODE];   /* Whs-to-Store */
extern const char LABL_MSTWSR[NULL_CODE];   /* Store-to-Whs */
extern const char LABL_MWTWSR[NULL_CODE];   /* Whs-to-Whs */
extern const char LABL_RCAT  [NULL_CODE];   /* Receiver Cost Adjustment */
extern const char LABL_CIF   [NULL_CODE];   /* Frequency */
extern const char LABL_CIISLI[NULL_CODE];   /* Level */
extern const char LABL_AIPI  [NULL_CODE];   /* Advance Inventory Planning */
extern const char LABL_SPAGI [NULL_CODE];   /* Automatically Generate ID */
extern const char LABL_PIUI  [NULL_CODE];   /* Partner ID Uniq Across All Partner Types */
extern const char LABL_CNCR  [NULL_CODE];   /* Concession Rate */
extern const char LABL_CNSR  [NULL_CODE];   /* Consignment Rate */
extern const char LABL_FRMLOC[NULL_CODE];   /* From Loc */
extern const char LABL_FINSHR[NULL_CODE];   /* Finisher */
extern const char LABL_TSFPRC[NULL_CODE];   /* Transfer
 Price */

extern const char LABL_TSFPAT[NULL_CODE];   /* Transfer Price Adjustment Type */
extern const char LABL_TSFCST[NULL_CODE];   /* Transfer
 Cost */

extern const char LABL_TSFCAT[NULL_CODE];   /* Transfer Cost
 Adjustment Type */

extern const char LABL_UL    [NULL_CODE];   /* Upper Limit */
extern const char LABL_LL    [NULL_CODE];   /* Lower Limit */
extern const char LABL_LSTWHS[NULL_CODE];   /* List of Warehouses */
extern const char LABL_LSTSTS[NULL_CODE];   /* List of Stores */
extern const char LABL_VIEWBY[NULL_CODE];   /* View By */
extern const char LABL_DATEEN[NULL_CODE];   /* Date Entry     (date) */
extern const char LABL_UDA1  [NULL_CODE];   /* UDA - List of Values */
extern const char LABL_UDA2  [NULL_CODE];   /* UDA - Dates */
extern const char LABL_UDA3  [NULL_CODE];   /* UDA - Free Form Text */
extern const char LABL_TOTALW[NULL_CODE];   /* Total Def. Wizard  (satotal) */
extern const char LABL_RULEW [NULL_CODE];   /* Rules  Def. Wizard  (sarule) */
extern const char LABL_SHOWDT[NULL_CODE];   /* Show Detail */
extern const char LABL_CONTOF[NULL_CODE];   /* Contents of */
extern const char LABL_REF1DE[NULL_CODE];   /* Reference No. 1 */
extern const char LABL_REF2DE[NULL_CODE];   /* Reference No. 2 */
extern const char LABL_USEOWD[NULL_CODE];   /* Unit Sales by End of Week Date */
extern const char LABL_UIEOWD[NULL_CODE];   /* Unit Issues by End of Week Date */
extern const char LABL_AREA  [NULL_CODE];   /* @OH3@ */
extern const char LABL_CHAIN [NULL_CODE];   /* @OH2@ */
extern const char LABL_DISTRI[NULL_CODE];   /* @OH5@ */
extern const char LABL_VIEWNM[NULL_CODE];   /* View Name */
extern const char LABL_REALMI[NULL_CODE];   /* Realm Id */
extern const char LABL_REALMA[NULL_CODE];   /* Realm Alias */
extern const char LABL_PARAM [NULL_CODE];   /* Parameter */
extern const char LABL_PARAMA[NULL_CODE];   /* Parameter Alias */
extern const char LABL_MODE  [NULL_CODE];   /* Mode */
extern const char LABL_BLOCK [NULL_CODE];   /* Block */
extern const char LABL_RECGRP[NULL_CODE];   /* Record Group Column */
extern const char LABL_SUBMIT[NULL_CODE];   /* submitted */
extern const char LABL_APPRVE[NULL_CODE];   /* approved */
extern const char LABL_PICKUP[NULL_CODE];   /* Pickup Date */
extern const char LABL_NOTBEF[NULL_CODE];   /* Not Before Date */
extern const char LABL_NOTAFT[NULL_CODE];   /* Not After Date */
extern const char LABL_REGION[NULL_CODE];   /* @OH4@ */
extern const char LABL_SIZE  [NULL_CODE];   /* Size */
extern const char LABL_INFO  [NULL_CODE];   /* Information */
extern const char LABL_COST  [NULL_CODE];   /* Cost */
extern const char LABL_NEEDDT[NULL_CODE];   /* Need Date */
extern const char LABL_VDATE [NULL_CODE];   /* Business Date */
extern const char LABL_ASN   [NULL_CODE];   /* ASN */
extern const char LABL_FLSOB [NULL_CODE];   /* From Set of Books */
extern const char LABL_TLSOB [NULL_CODE];   /* To Set of Books */
extern const char LABL_FNSOB [NULL_CODE];   /* Finisher Set of Books */
extern const char LABL_CUSDLY[NULL_CODE];   /* Customer Delivery Date */
extern const char LABL_CUSDLT[NULL_CODE];   /* Customer Delivery Time */
extern const char LABL_BINFO [NULL_CODE];   /* Billing Information */
extern const char LABL_DINFO [NULL_CODE];   /* Delivery Information */
extern const char LABL_NOMF1 [NULL_CODE];   /* Flag 1 */
extern const char LABL_NOMF2 [NULL_CODE];   /* In Duty */
extern const char LABL_NOMF3 [NULL_CODE];   /* Flag 3 */
extern const char LABL_NOMF4 [NULL_CODE];   /* In Exp */
extern const char LABL_NOMF5 [NULL_CODE];   /* In ALC */
extern const char LABL_R     [NULL_CODE];   /* Supplier Revision */
extern const char LABL_RV    [NULL_CODE];   /* Retailer Version */
extern const char LABL_MARGIN[NULL_CODE];   /* Margin */
extern const char LABL_RETAIL[NULL_CODE];   /* Retail */
extern const char LABL_MARGPT[NULL_CODE];   /* Margin Percent */
extern const char LABL_RETLPT[NULL_CODE];   /* Retail Percent */
extern const char LABL_FCOST [NULL_CODE];   /* Fixed Cost */
extern const char LABL_CRET  [NULL_CODE];   /* Completed Reclassification Event */
extern const char LABL_OAL   [NULL_CODE];   /* Order Appr. Limit */
extern const char LABL_TCC   [NULL_CODE];   /* Total Customer Cost */
extern const char LABL_TP    [NULL_CODE];   /* Transfer Price */
extern const char LABL_ADJRC [NULL_CODE];   /* Adjustment Reason Code */
extern const char LABL_FRMLC [NULL_CODE];   /* From Location */
extern const char LABL_TOLOC [NULL_CODE];   /* To Location */
extern const char LABL_ACTID [NULL_CODE];   /* Activity ID */
extern const char LABL_TRFNO [NULL_CODE];   /* Tran Ref No */
extern const char LABL_TCAR  [NULL_CODE];   /* Adjustment Reason Code */
extern const char LABL_TCFL  [NULL_CODE];   /* From Location */
extern const char LABL_TCTL  [NULL_CODE];   /* To Location */
extern const char LABL_TCTR  [NULL_CODE];   /* Tran. Ref. No. */
extern const char LABL_SP    [NULL_CODE];   /* space */
extern const char LABL_CTMZ  [NULL_CODE];   /* Customize */
extern const char LABL_BLANKC[NULL_CODE];   /* Blank Color */
extern const char LABL_BLANKF[NULL_CODE];   /* Blank Flavor */
extern const char LABL_BLANKP[NULL_CODE];   /* Blank Pattern */
extern const char LABL_BLANKE[NULL_CODE];   /* Blank Scent */
extern const char LABL_BLANKS[NULL_CODE];   /* Blank Size */
extern const char LABL_ASATW [NULL_CODE];   /* @OHP3@ Associated with Trait */
extern const char LABL_TSAWR [NULL_CODE];   /* Traits Associated with @OHP4@ */
extern const char LABL_RSAWT [NULL_CODE];   /* @OHP4@ Associated with Trait */
extern const char LABL_SDSFA [NULL_CODE];   /* Store Department Square Footage Area */
extern const char LABL_ISIZE [NULL_CODE];   /* Inner Size */
extern const char LABL_CSIZE [NULL_CODE];   /* Case Size */
extern const char LABL_CNTACT[NULL_CODE];   /* Contact */
extern const char LABL_TO    [NULL_CODE];   /* To */
extern const char LABL_SEARCH[NULL_CODE];   /* Search */
extern const char LABL_APPT  [NULL_CODE];   /* Appointment */
extern const char LABL_APPTD [NULL_CODE];   /* Appointment Details */
extern const char LABL_FILCRT[NULL_CODE];   /* Filter Criteria */
extern const char LABL_SCALCO[NULL_CODE];   /* Scaling Constraints */
extern const char LABL_COMPIN[NULL_CODE];   /* Competitor Info */
extern const char LABL_MCUC  [NULL_CODE];   /* Mass Change Unit Cost */
extern const char LABL_BLIT  [NULL_CODE];   /* Blank Item */
extern const char LABL_COMPTO[NULL_CODE];   /* Compared To */
extern const char LABL_FILBY [NULL_CODE];   /* Filter By */
extern const char LABL_ENTAS [NULL_CODE];   /* Enter As: */
extern const char LABL_TRANL [NULL_CODE];   /* Tran Level: */
extern const char LABL_SUNAB [NULL_CODE];   /* Sun */
extern const char LABL_MONAB [NULL_CODE];   /* Mon */
extern const char LABL_FRIAB [NULL_CODE];   /* Fri */
extern const char LABL_TUEAB [NULL_CODE];   /* Tue */
extern const char LABL_WEDAB [NULL_CODE];   /* Wed */
extern const char LABL_SATAB [NULL_CODE];   /* Sat */
extern const char LABL_THUAB [NULL_CODE];   /* Thu */
extern const char LABL_NOTBET[NULL_CODE];   /* Not Before Datetime */
extern const char LABL_DEALH [NULL_CODE];   /* Deal Head */
extern const char LABL_DEALD [NULL_CODE];   /* Deal Head */
extern const char LABL_GET   [NULL_CODE];   /* Get */
extern const char LABL_BUY   [NULL_CODE];   /* Buy */
extern const char LABL_LOCS1 [NULL_CODE];   /* Locations */
extern const char LABL_MUCTYP[NULL_CODE];   /* Markup Calculation Type */
extern const char LABL_PCTYP [NULL_CODE];   /* Profit Calculation Type */
extern const char LABL_OTBTYP[NULL_CODE];   /* OTB Calculation Type */
extern const char LABL_PURTYP[NULL_CODE];   /* Purchase Type */
extern const char LABL_BTPER [NULL_CODE];   /* Build Time Period */
extern const char LABL_GENBY [NULL_CODE];   /* Generated By */
extern const char LABL_SALTYP[NULL_CODE];   /* Sales Type */
extern const char LABL_FILTER[NULL_CODE];   /* Filter */
extern const char LABL_NIUI  [NULL_CODE];   /* New Items/Unavailable Items */
extern const char LABL_DIFFS [NULL_CODE];   /* Diffs */
extern const char LABL_OVRRID[NULL_CODE];   /* Override */
extern const char LABL_GMLVL [NULL_CODE];   /* @MH3@/Merchandise Level */
extern const char LABL_GOLVL [NULL_CODE];   /* @MH3@/Organization Level */
extern const char LABL_TOTALS[NULL_CODE];   /* Totals */
extern const char LABL_DIFFER[NULL_CODE];   /* Differentiators */
extern const char LABL_X     [NULL_CODE];   /* X */
extern const char LABL_GENINF[NULL_CODE];   /* General Info */
extern const char LABL_DEMINS[NULL_CODE];   /* Dimensions */
extern const char LABL_WEIGHT[NULL_CODE];   /* Weight */
extern const char LABL_PACKS [NULL_CODE];   /* Packs */
extern const char LABL_PRMSRC[NULL_CODE];   /* Primary Sourcing */
extern const char LABL_CPKINF[NULL_CODE];   /* Case Pack Information */
extern const char LABL_PAR   [NULL_CODE];   /* Parent */
extern const char LABL_LTIHI [NULL_CODE];   /* Location Ti - Hi */
extern const char LABL_RCOM  [NULL_CODE];   /* Customer Order Specific Attributes */
extern const char LABL_DEPITM[NULL_CODE];   /* Deposit Item */
extern const char LABL_CTCHWT[NULL_CODE];   /* Catch Weight */
extern const char LABL_ACNTIM[NULL_CODE];   /* Associated Contents Items */
extern const char LABL_ATTRS [NULL_CODE];   /* Attributes */
extern const char LABL_PRMSUP[NULL_CODE];   /* Primary Supplier */
extern const char LABL_PRMCTY[NULL_CODE];   /* Primary Country */
extern const char LABL_POINFO[NULL_CODE];   /* Ordering Information */
extern const char LABL_SPTIHI[NULL_CODE];   /* Supplier/Inbound Ti - Hi */
extern const char LABL_RNDTHR[NULL_CODE];   /* Rounding Thresholds */
extern const char LABL_TOLTYP[NULL_CODE];   /* Tolerance Type */
extern const char LABL_OCDETL[NULL_CODE];   /* Country Of Sourcing Details */
extern const char LABL_LOCDIN[NULL_CODE];   /* Location Default Information */
extern const char LABL_PRMARY[NULL_CODE];   /* Primary */
extern const char LABL_SECOND[NULL_CODE];   /* Secondary */
extern const char LABL_STSALE[NULL_CODE];   /* Store Sales */
extern const char LABL_SOH   [NULL_CODE];   /* Stock On Hand */
extern const char LABL_VIEW  [NULL_CODE];   /* View */
extern const char LABL_COPYFR[NULL_CODE];   /* Copy From */
extern const char LABL_RTVDTL[NULL_CODE];   /* RTV Details */
extern const char LABL_MRTDTL[NULL_CODE];   /* MRT Details */
extern const char LABL_ADITMS[NULL_CODE];   /* Add Items */
extern const char LABL_ITMDTL[NULL_CODE];   /* Item Details */
extern const char LABL_LOCDTL[NULL_CODE];   /* Location Details */
extern const char LABL_APPDEL[NULL_CODE];   /* Apply or Delete To: */
extern const char LABL_ALTCP [NULL_CODE];   /* Apply Locations and Transfer Cost/Price */
extern const char LABL_CURNAM[NULL_CODE];   /* Current Name */
extern const char LABL_NEWNAM[NULL_CODE];   /* New Name */
extern const char LABL_DEFNAM[NULL_CODE];   /* Default Name */
extern const char LABL_PHNCON[NULL_CODE];   /* Phone Numbers/Contact Name */
extern const char LABL_OINFO [NULL_CODE];   /* Other Information */
extern const char LABL_MIN1  [NULL_CODE];   /* Minimum 1 */
extern const char LABL_MIN2  [NULL_CODE];   /* Minimum 2 */
extern const char LABL_MINCON[NULL_CODE];   /* Minimum Constraints */
extern const char LABL_TRSCON[NULL_CODE];   /* Truck Splitting Constraints */
extern const char LABL_SUPPOC[NULL_CODE];   /* Supplier Order Costs */
extern const char LABL_QTYS  [NULL_CODE];   /* Quantities */
extern const char LABL_PODUES[NULL_CODE];   /* Order Due Status */
extern const char LABL_CON2  [NULL_CODE];   /* Constraint 2 */
extern const char LABL_CON1  [NULL_CODE];   /* Constraint 1 */
extern const char LABL_ISSUES[NULL_CODE];   /* Issues */
extern const char LABL_SUPPOL[NULL_CODE];   /* Supplier Pooling */
extern const char LABL_POCOST[NULL_CODE];   /* Order Costs */
extern const char LABL_POQTYS[NULL_CODE];   /* Order Quantities */
extern const char LABL_ALLOCD[NULL_CODE];   /* Allocation Details */
extern const char LABL_PODETL[NULL_CODE];   /* Order Details */
extern const char LABL_LOCATS[NULL_CODE];   /* Order Details */
extern const char LABL_DIFMTX[NULL_CODE];   /* Diff Matrix */
extern const char LABL_ENTRAS[NULL_CODE];   /* Enter As */
extern const char LABL_OHDCHG[NULL_CODE];   /* Order Header Date Changes */
extern const char LABL_TYPE  [NULL_CODE];   /* Type */
extern const char LABL_CQTYIL[NULL_CODE];   /* Cost and Qty Changes By Item/Location */
extern const char LABL_MODLST[NULL_CODE];   /* Modify List */
extern const char LABL_PNDPRC[NULL_CODE];   /* Pending Price Changes */
extern const char LABL_LOCSEL[NULL_CODE];   /* Location Selection */
extern const char LABL_PRCDEF[NULL_CODE];   /* Price Change Default */
extern const char LABL_DMTXBY[NULL_CODE];   /*  Distribute Matrix  By */
extern const char LABL_DSTZAB[NULL_CODE];   /* Distribute Z Axis By */
extern const char LABL_AXDGRP[NULL_CODE];   /* Axes Diff Groups */
extern const char LABL_DRLMT [NULL_CODE];   /* Diff Range Limiting */
extern const char LABL_ZAXIS [NULL_CODE];   /* Z Axis */
extern const char LABL_DFFRT [NULL_CODE];   /* Diff Ratio */
extern const char LABL_XTOTAL[NULL_CODE];   /* X Total */
extern const char LABL_XYDIST[NULL_CODE];   /* X-Y Distribution */
extern const char LABL_DISTY [NULL_CODE];   /* Distribute Y By */
extern const char LABL_XDIST [NULL_CODE];   /* X Distribution */
extern const char LABL_DISTX [NULL_CODE];   /* Distribute X By */
extern const char LABL_PRFFUL[NULL_CODE];   /* Proof of Performance Fulfillment */
extern const char LABL_PRFDEF[NULL_CODE];   /* Proof of Performance Definitions */
extern const char LABL_MOVE  [NULL_CODE];   /* Move */
extern const char LABL_PREPLP[NULL_CODE];   /* Primary Replenishment Pack */
extern const char LABL_STKVAL[NULL_CODE];   /* Stock Values */
extern const char LABL_LDTIME[NULL_CODE];   /* Lead Times */
extern const char LABL_RPLCYL[NULL_CODE];   /* Replenishment Cycle */
extern const char LABL_REPLDT[NULL_CODE];   /* Replenishment Dates */
extern const char LABL_RPLMA [NULL_CODE];   /* Replenishment Method Attributes */
extern const char LABL_RPLAMA[NULL_CODE];   /* Repl Attribute Maintenance Action */
extern const char LABL_UINFO [NULL_CODE];   /* User Information */
extern const char LABL_SCHADT[NULL_CODE];   /* Scheduled Active Date */
extern const char LABL_MADMIN[NULL_CODE];   /* Master Administration */
extern const char LABL_AOCTOL[NULL_CODE];   /* Allowable Order Change Tolerances */
extern const char LABL_SCALE [NULL_CODE];   /* Scaling */
extern const char LABL_RECALC[NULL_CODE];   /* Recalculation */
extern const char LABL_ORDREV[NULL_CODE];   /* Order Review */
extern const char LABL_DOINFO[NULL_CODE];   /* Due Order Information */
extern const char LABL_CPOSTS[NULL_CODE];   /* Current Order Status */
extern const char LABL_REPLC [NULL_CODE];   /* Replenishment Costs */
extern const char LABL_GENFOR[NULL_CODE];   /* Generated Forecast Results */
extern const char LABL_LOCINV[NULL_CODE];   /* Location Inventory */
extern const char LABL_PACKSZ[NULL_CODE];   /* Pack Sizes */
extern const char LABL_REPLRS[NULL_CODE];   /* Replenishment Results */
extern const char LABL_OITEM [NULL_CODE];   /* Original Item */
extern const char LABL_CTITEM[NULL_CODE];   /* Change To Item */
extern const char LABL_RELDTS[NULL_CODE];   /* Item Relationship Details */
extern const char LABL_RELIMS[NULL_CODE];   /* Related Items */
extern const char LABL_PRESAB[NULL_CODE];   /* Presence or absence creates error? */
extern const char LABL_EXGRP [NULL_CODE];   /* Execute Group */
extern const char LABL_EXGRPD[NULL_CODE];   /* Execute Group Details */
extern const char LABL_WIZRUL[NULL_CODE];   /* Will you use the wizard to write a rule? */
extern const char LABL_BDRANG[NULL_CODE];   /* Business Day Range? */
extern const char LABL_STORES[NULL_CODE];   /* Stores */
extern const char LABL_STATUS[NULL_CODE];   /* Status */
extern const char LABL_STSTAT[NULL_CODE];   /* Store Status */
extern const char LABL_HQSTAT[NULL_CODE];   /* Headquarter Status */
extern const char LABL_TINFO [NULL_CODE];   /* Transaction Information */
extern const char LABL_FMERCH[NULL_CODE];   /* Do you have Fuel Merchandise? */
extern const char LABL_SVALCO[NULL_CODE];   /* System Validation Constraints */
extern const char LABL_CSTCRT[NULL_CODE];   /* Compare Store Criteria */
extern const char LABL_ESCIND[NULL_CODE];   /* Escheatment Indicator */
extern const char LABL_TVWOPT[NULL_CODE];   /* Total View Option */
extern const char LABL_ACHINF[NULL_CODE];   /* ACH Information */
extern const char LABL_RINADJ[NULL_CODE];   /* Retailer Details for Income Adjustment */
extern const char LABL_TNDARG[NULL_CODE];   /* Tender Amount Range */
extern const char LABL_AUDTRD[NULL_CODE];   /* Audit Trail Details */
extern const char LABL_BSDYRG[NULL_CODE];   /* Business Day Range */
extern const char LABL_UPDTRG[NULL_CODE];   /* Update Date/Time Range */
extern const char LABL_TOTDTL[NULL_CODE];   /* Totaling Details */
extern const char LABL_TRDTRG[NULL_CODE];   /* Tran. Date/Time Range */
extern const char LABL_POSTRG[NULL_CODE];   /* POS Tran. No. Range */
extern const char LABL_STUOMC[NULL_CODE];   /* Standard Unit of Measure Conversions */
extern const char LABL_SELUOM[NULL_CODE];   /* Selling Unit of Measure */
extern const char LABL_STDUOM[NULL_CODE];   /* Standard Unit of Measure */
extern const char LABL_DSCUOM[NULL_CODE];   /* Discount Unit of Measure */
extern const char LABL_TLCOA [NULL_CODE];   /* Transaction Level Customer Order Attribs */
extern const char LABL_ITMREF[NULL_CODE];   /* Item Reference */
extern const char LABL_TNDREF[NULL_CODE];   /* Tender Reference */
extern const char LABL_TXNREF[NULL_CODE];   /* Transaction Reference */
extern const char LABL_ISSSLD[NULL_CODE];   /* Issued/Sold */
extern const char LABL_ILCOA [NULL_CODE];   /* Item Level Customer Order Attributes */
extern const char LABL_REDEEM[NULL_CODE];   /* Redeemed */
extern const char LABL_ASSIGN[NULL_CODE];   /* Assigned */
extern const char LABL_TAXREF[NULL_CODE];   /* Tax Reference */
extern const char LABL_SHDTS [NULL_CODE];   /* Shipment Dates */
extern const char LABL_RECDAT[NULL_CODE];   /* Shipment Dates */
extern const char LABL_FROM  [NULL_CODE];   /* From */
extern const char LABL_MERCHH[NULL_CODE];   /* Merchandise Hierarchy */
extern const char LABL_ORGH  [NULL_CODE];   /* Organizational Hierarchy */
extern const char LABL_DETLS [NULL_CODE];   /* Details */
extern const char LABL_DETLIN[NULL_CODE];   /* Detail Information */
extern const char LABL_HEADIN[NULL_CODE];   /* Header Information */
extern const char LABL_COUNT [NULL_CODE];   /* Count */
extern const char LABL_LIST2 [NULL_CODE];   /* List */
extern const char LABL_CINFCI[NULL_CODE];   /* Copy Info From Component Item */
extern const char LABL_DIMINF[NULL_CODE];   /* Dimension Info */
extern const char LABL_APKINF[NULL_CODE];   /* Apply Pack Info */
extern const char LABL_TOLS  [NULL_CODE];   /* Tolerances */
extern const char LABL_CUR   [NULL_CODE];   /* Currency */
extern const char LABL_SITMBY[NULL_CODE];   /* Select Item By: */
extern const char LABL_DAYCNT[NULL_CODE];   /* Days of Count */
extern const char LABL_REPL  [NULL_CODE];   /* Replenishment */
extern const char LABL_INVBUY[NULL_CODE];   /* Investment Buy */
extern const char LABL_LOC   [NULL_CODE];   /* Location */
extern const char LABL_DATERG[NULL_CODE];   /* Date Range */
extern const char LABL_APPLYB[NULL_CODE];   /* Apply Block */
extern const char LABL_FILPRI[NULL_CODE];   /* Fill Priority */
extern const char LABL_DOPROC[NULL_CODE];   /* Due Order Processing */
extern const char LABL_SCALAT[NULL_CODE];   /* Scaling Attributes */
extern const char LABL_INVBAT[NULL_CODE];   /* Investment Buy Attributes */
extern const char LABL_DFSCLT[NULL_CODE];   /* Default From Scaling Constraints to: */
extern const char LABL_TRSATT[NULL_CODE];   /* Truck Splitting Attributes */
extern const char LABL_RNDATT[NULL_CODE];   /* Rounding Attributes */
extern const char LABL_SMNATT[NULL_CODE];   /* Supplier Minimum Attributes */
extern const char LABL_OR    [NULL_CODE];   /* Or */
extern const char LABL_SUPEDI[NULL_CODE];   /* EDI Trans. Supported By the Supplier */
extern const char LABL_EDICCV[NULL_CODE];   /* EDI Cost Change Approval Variance */
extern const char LABL_INDS  [NULL_CODE];   /* Indicators */
extern const char LABL_HQCTRY[NULL_CODE];   /* Corporate HQ Country */
extern const char LABL_INVENT[NULL_CODE];   /* Inventory */
extern const char LABL_PRMLNG[NULL_CODE];   /* Primary Language */
extern const char LABL_CHANNL[NULL_CODE];   /* Channel */
extern const char LABL_TSFS  [NULL_CODE];   /* Transfers */
extern const char LABL_MONTHS[NULL_CODE];   /* Months */
extern const char LABL_MLOCRD[NULL_CODE];   /* Markdown Location For Retail Difference */
extern const char LABL_HTS   [NULL_CODE];   /* HTS */
extern const char LABL_RIMOPT[NULL_CODE];   /* Invoice Matching Options */
extern const char LABL_DAYS  [NULL_CODE];   /* Days */
extern const char LABL_DEFUOP[NULL_CODE];   /* Default Unit of Purchase */
extern const char LABL_ITMOPT[NULL_CODE];   /* Item Options */
extern const char LABL_DEFUOM[NULL_CODE];   /* Default UOM */
extern const char LABL_SOB   [NULL_CODE];   /* Set of Books */
extern const char LABL_DISTRO[NULL_CODE];   /* Distribution */
extern const char LABL_PACKNG[NULL_CODE];   /* Packing */
extern const char LABL_TCKLBL[NULL_CODE];   /* Tickets/Labels */
extern const char LABL_TIMPRD[NULL_CODE];   /* Time Period Detail */
extern const char LABL_SYSREC[NULL_CODE];   /* System of Record */
extern const char LABL_STOREC[NULL_CODE];   /* Stock Order Reconciliation */
extern const char LABL_LOVFIL[NULL_CODE];   /* LOV Filtering */
extern const char LABL_OUTSTR[NULL_CODE];   /* Outside Storage */
extern const char LABL_WHSTR [NULL_CODE];   /* Warehouse Storage */
extern const char LABL_DEALS [NULL_CODE];   /* Deals */
extern const char LABL_LTRCRT[NULL_CODE];   /* Letter of Credit */
extern const char LABL_ORDERS[NULL_CODE];   /* Orders */
extern const char LABL_EDITXN[NULL_CODE];   /* EDI Transactions */
extern const char LABL_CNTRTS[NULL_CODE];   /* Contracts */
extern const char LABL_NWPPRC[NULL_CODE];   /* LVP Process Controls */
extern const char LABL_HISTRY[NULL_CODE];   /* History */
extern const char LABL_BILLAD[NULL_CODE];   /* Bill of Lading */
extern const char LABL_STKCNT[NULL_CODE];   /* Stock Count */
extern const char LABL_COSTMD[NULL_CODE];   /* Cost Method */
extern const char LABL_CHKDIG[NULL_CODE];   /* Check Digit */
extern const char LABL_DCSTYP[NULL_CODE];   /* Discount Type */
extern const char LABL_APPLTP[NULL_CODE];   /* Apply Type */
extern const char LABL_CNTXTD[NULL_CODE];   /* Context Details */
extern const char LABL_WLKTST[NULL_CODE];   /* Walk Through Store */
extern const char LABL_CSTLOC[NULL_CODE];   /* Cost Location */
extern const char LABL_RLMS  [NULL_CODE];   /* Realms */
extern const char LABL_SELRLM[NULL_CODE];   /* Selected Realm */
extern const char LABL_RRBTV [NULL_CODE];   /* Restrict Results By Table Values */
extern const char LABL_RRBCV [NULL_CODE];   /* Restrict Results By Constant Values */
extern const char LABL_PCTSG [NULL_CODE];   /* Pct */
extern const char LABL_MRKUPR[NULL_CODE];   /* Markup Pct Retail */
extern const char LABL_ROUNDP[NULL_CODE];   /* Rounding Pct */
extern const char LABL_LOCSQ [NULL_CODE];   /* locations? */
extern const char LABL_STDAYQ[NULL_CODE];   /* store/day? */
extern const char LABL_EVALQ [NULL_CODE];   /* evaluation? */
extern const char LABL_DTATIM[NULL_CODE];   /* Date And Time */
extern const char LABL_WHISS [NULL_CODE];   /* Warehouse Issues */
extern const char LABL_RPLATT[NULL_CODE];   /* Replenishment Attributes */
extern const char LABL_BRKTCO[NULL_CODE];   /* Bracket Constraints */
extern const char LABL_OTHRAT[NULL_CODE];   /* Other Attributes */
extern const char LABL_STKLGR[NULL_CODE];   /* Stock Ledger */
extern const char LABL_RECV  [NULL_CODE];   /* Receiving */
extern const char LABL_OPENTB[NULL_CODE];   /* Open to Buy */
extern const char LABL_VATT  [NULL_CODE];   /* VAT */
extern const char LABL_EXTSYS[NULL_CODE];   /* External Systems */
extern const char LABL_CNSPOI[NULL_CODE];   /* Consignment POs and Invoices */
extern const char LABL_SUPPAR[NULL_CODE];   /* Supplier-Partner */
extern const char LABL_GLROLU[NULL_CODE];   /* Finance */
extern const char LABL_PCCL  [NULL_CODE];   /* Price Change / Clearance */
extern const char LABL_AA2BC [NULL_CODE];   /* Attributes Available to be Copied */
extern const char LABL_ACINFO[NULL_CODE];   /* Accounting Information */
extern const char LABL_ACOPNS[NULL_CODE];   /* At Cost OR % of Net Sales */
extern const char LABL_ACTION[NULL_CODE];   /* Action */
extern const char LABL_ADDINF[NULL_CODE];   /* Address Info */
extern const char LABL_ADJAMT[NULL_CODE];   /* Adjustment Amount */
extern const char LABL_AEDTLS[NULL_CODE];   /* Apply Entry Details */
extern const char LABL_ALCDET[NULL_CODE];   /* View ALC details for: */
extern const char LABL_ALVDTS[NULL_CODE];   /* Apply License/Visa Details */
extern const char LABL_AND   [NULL_CODE];   /* and */
extern const char LABL_AORITD[NULL_CODE];   /* Apply Order/Item Details */
extern const char LABL_APDTE [NULL_CODE];   /* Approval Date: */
extern const char LABL_APLHTS[NULL_CODE];   /* Apply HTS */
extern const char LABL_APPDT [NULL_CODE];   /* Application Date: */
extern const char LABL_APPLY [NULL_CODE];   /* Apply */
extern const char LABL_APPTTL[NULL_CODE];   /* Apply Totals */
extern const char LABL_APSHPD[NULL_CODE];   /* Apply Shipment Details */
extern const char LABL_APYCRT[NULL_CODE];   /* Apply Criteria */
extern const char LABL_ASDTLS[NULL_CODE];   /* Assessment Details */
extern const char LABL_ASNVAL[NULL_CODE];   /* Assign Values */
extern const char LABL_ATCOST[NULL_CODE];   /* At Cost */
extern const char LABL_ATRETL[NULL_CODE];   /* At Retail */
extern const char LABL_BANKS [NULL_CODE];   /* Banks */
extern const char LABL_BATSTA[NULL_CODE];   /* Batch Status */
extern const char LABL_BENATT[NULL_CODE];   /* Beneficiary Attributes */
extern const char LABL_BKINFO[NULL_CODE];   /* Banking Information */
extern const char LABL_BREAK2[NULL_CODE];   /* Break to */
extern const char LABL_CALCDB[NULL_CODE];   /* Calculate Dates Based On */
extern const char LABL_CCAM  [NULL_CODE];   /* Cost Change Approval Method */
extern const char LABL_CGVAL2[NULL_CODE];   /* Change Value To */
extern const char LABL_CLSD4 [NULL_CODE];   /* Closed For */
extern const char LABL_CNFLCT[NULL_CODE];   /* Conflicts */
extern const char LABL_COLLDA[NULL_CODE];   /* Collect Dates */
extern const char LABL_COMNOM[NULL_CODE];   /* Component Nomination */
extern const char LABL_CONDIT[NULL_CODE];   /* Conditions */
extern const char LABL_CONDT [NULL_CODE];   /* Confirmation Date: */
extern const char LABL_CONINF[NULL_CODE];   /* Contact Info */
extern const char LABL_CTGTTH[NULL_CODE];   /* Component Target Thresholds */
extern const char LABL_CURFMT[NULL_CODE];   /* Currency Formats */
extern const char LABL_CZCHG [NULL_CODE];   /* Cost Zone Change */
extern const char LABL_DATES [NULL_CODE];   /* Dates */
extern const char LABL_DCOMIN[NULL_CODE];   /* Deal Component Information */
extern const char LABL_DECPLC[NULL_CODE];   /* Decimal Places */
extern const char LABL_DG    [NULL_CODE];   /* Display Group */
extern const char LABL_DGD   [NULL_CODE];   /* Display Group Details */
extern const char LABL_DL2DAT[NULL_CODE];   /* Deal to Date */
extern const char LABL_DLCDAT[NULL_CODE];   /* Deal Component to Date */
extern const char LABL_DLTTL [NULL_CODE];   /* Deal Total */
extern const char LABL_ENDTLS[NULL_CODE];   /* Entry Details */
extern const char LABL_ENTOBL[NULL_CODE];   /* Entries / Obligations */
extern const char LABL_EPDTE [NULL_CODE];   /* Effective Date: */
extern const char LABL_ESHPDT[NULL_CODE];   /* Earliest Ship Date: */
extern const char LABL_EXDTLS[NULL_CODE];   /* Expense Details */
extern const char LABL_EXPDAT[NULL_CODE];   /* Expiration Date: */
extern const char LABL_EXPHDR[NULL_CODE];   /* Expense Header */
extern const char LABL_EXPRD [NULL_CODE];   /* Expense Profile Detail */
extern const char LABL_EXPRH [NULL_CODE];   /* Expense Profile Header */
extern const char LABL_FIXED [NULL_CODE];   /* Fixed */
extern const char LABL_HTSINF[NULL_CODE];   /* HTS Information */
extern const char LABL_IMPATT[NULL_CODE];   /* Import Attributes */
extern const char LABL_INCAMT[NULL_CODE];   /* Income Amount */
extern const char LABL_INUMTY[NULL_CODE];   /* Item Number Type */
extern const char LABL_ITCOLN[NULL_CODE];   /* Item Type: */
extern const char LABL_ITEMS [NULL_CODE];   /* Items */
extern const char LABL_ITMSEL[NULL_CODE];   /* Item Selection */
extern const char LABL_ITMTYP[NULL_CODE];   /* Item Type */
extern const char LABL_LCAMTS[NULL_CODE];   /* LC Amounts */
extern const char LABL_LEASED[NULL_CODE];   /* Leased */
extern const char LABL_LETCC [NULL_CODE];   /* Location Exceptions to @OH1@ Close */
extern const char LABL_LOCATT[NULL_CODE];   /* Location Attributes */
extern const char LABL_LOCINF[NULL_CODE];   /* Location Information */
extern const char LABL_LOCLSD[NULL_CODE];   /* Location Closed */
extern const char LABL_LOCLST[NULL_CODE];   /* Location List */
extern const char LABL_LSHPDT[NULL_CODE];   /* Latest Ship Date: */
extern const char LABL_LVLCOL[NULL_CODE];   /* Level: */
extern const char LABL_MONTH1[NULL_CODE];   /* Month 1 */
extern const char LABL_MONTH2[NULL_CODE];   /* Month 2 */
extern const char LABL_MONTH3[NULL_CODE];   /* Month 3 */
extern const char LABL_MONTH4[NULL_CODE];   /* Month 4 */
extern const char LABL_MONTH5[NULL_CODE];   /* Month 5 */
extern const char LABL_MONTH6[NULL_CODE];   /* Month 6 */
extern const char LABL_NETEFT[NULL_CODE];   /* Net Effect */
extern const char LABL_NEW   [NULL_CODE];   /* New */
extern const char LABL_NEWVAL[NULL_CODE];   /* New Value */
extern const char LABL_OBLDTL[NULL_CODE];   /* Obligation Detail */
extern const char LABL_OLD   [NULL_CODE];   /* Old */
extern const char LABL_OPEN4 [NULL_CODE];   /* Open For */
extern const char LABL_ORVAL [NULL_CODE];   /* Original Value */
extern const char LABL_OVRSHR[NULL_CODE];   /* Over/Short */
extern const char LABL_PREVAL[NULL_CODE];   /* Previous Value */
extern const char LABL_PTNRS [NULL_CODE];   /* Partners */
extern const char LABL_REQDFT[NULL_CODE];   /* Request Default */
extern const char LABL_RTNINV[NULL_CODE];   /* Return From Inventory */
extern const char LABL_RXINFO[NULL_CODE];   /* Pharmacy Information */
extern const char LABL_SCAMT [NULL_CODE];   /* Stock Count Amount */
extern const char LABL_SCRIT [NULL_CODE];   /* Selection Criteria */
extern const char LABL_SHIPS [NULL_CODE];   /* Shipments */
extern const char LABL_SOHCLN[NULL_CODE];   /* Stock on Hand: */
extern const char LABL_SPCATT[NULL_CODE];   /* Specific Component Attribute */
extern const char LABL_STATTR[NULL_CODE];   /* Store Attributes */
extern const char LABL_STDFLT[NULL_CODE];   /* Store Default */
extern const char LABL_SUPHIE[NULL_CODE];   /* Supplier Hierarchy */
extern const char LABL_TGTBGR[NULL_CODE];   /* Target Baseline Growth % */
extern const char LABL_TRNDET[NULL_CODE];   /* Transportation Details */
extern const char LABL_TRNFS [NULL_CODE];   /* Transportation Finalize Selection */
extern const char LABL_TSTEPS[NULL_CODE];   /* Timeline Steps */
extern const char LABL_TTLBGR[NULL_CODE];   /* Total Baseline Growth % */
extern const char LABL_TVRAMT[NULL_CODE];   /* Turnover Amount */
extern const char LABL_TVRUNS[NULL_CODE];   /* Turnover Units */
extern const char LABL_UDAVAL[NULL_CODE];   /* UDA Values */
extern const char LABL_UPDTE [NULL_CODE];   /* Update */
extern const char LABL_VWCRIT[NULL_CODE];   /* View Criteria */
extern const char LABL_WHINFO[NULL_CODE];   /* Warehouse Information */
extern const char LABL_FIND  [NULL_CODE];   /* Find */
extern const char LABL_DIMENS[NULL_CODE];   /* Dimensions */
extern const char LABL_SHPRCV[NULL_CODE];   /* Shipping and Receiving */
extern const char LABL_RTMM  [NULL_CODE];   /* RTM */
extern const char LABL_ADITDI[NULL_CODE];   /* Add Items from Distros */
extern const char LABL_SRCH  [NULL_CODE];   /* Search */
extern const char LABL_JIDS  [NULL_CODE];   /* Job Id Status */
extern const char LABL_JIRL  [NULL_CODE];   /* Job Id Retry Log */
extern const char LABL_FUNC  [NULL_CODE];   /* Functions */
extern const char LABL_OTBEOW[NULL_CODE];   /* OTB EOW Date */
extern const char LABL_RECDTE[NULL_CODE];   /* Receipt Date */
extern const char LABL_SHPDTE[NULL_CODE];   /* Ship Date */
extern const char LABL_TRNDTL[NULL_CODE];   /* Transaction Details */
extern const char LABL_DAY1  [NULL_CODE];   /* Day 1 */
extern const char LABL_DAY2  [NULL_CODE];   /* Day 2 */
extern const char LABL_DAY3  [NULL_CODE];   /* Day 3 */
extern const char LABL_DAY4  [NULL_CODE];   /* Day 4 */
extern const char LABL_DAY5  [NULL_CODE];   /* Day 5 */
extern const char LABL_DAY6  [NULL_CODE];   /* Day 6 */
extern const char LABL_DAY7  [NULL_CODE];   /* Day 7 */
extern const char LABL_TOTAL [NULL_CODE];   /* Total */
extern const char LABL_WEEK1 [NULL_CODE];   /* Week 1 */
extern const char LABL_WEEK2 [NULL_CODE];   /* Week 2 */
extern const char LABL_WEEK3 [NULL_CODE];   /* Week 3 */
extern const char LABL_WEEK4 [NULL_CODE];   /* Week 4 */
extern const char LABL_WEEK5 [NULL_CODE];   /* Week 5 */
extern const char LABL_TLEMP [NULL_CODE];   /* Transaction Level Employees */
extern const char LABL_ILEMP [NULL_CODE];   /* Item Level Employees */
extern const char LABL_WFRTY [NULL_CODE];   /* Return Type */
extern const char LABL_WFRM  [NULL_CODE];   /* Return Method */
extern const char LABL_ELC   [NULL_CODE];   /* ELC Component */
extern const char LABL_ML    [NULL_CODE];   /* Manual */
extern const char LABL_EXTTRG[NULL_CODE];   /* External System Tran. No. Range */
extern const char LABL_FORW  [NULL_CODE];   /* Transfers for @SUH4@ Returns          (wfordrettsf) */
extern const char LABL_FORL  [NULL_CODE];   /* @SUH4@ Return */
extern const char LABL_FOOL  [NULL_CODE];   /* @SUH4@ Order No. */
extern const char LABL_OKFIN [NULL_CODE];   /* OK / Finish */
extern const char LABL_FINISH[NULL_CODE];   /* Finish */
extern const char LABL_WFOPT [NULL_CODE];   /* @SUH4@ Options */
extern const char LABL_WFST  [NULL_CODE];   /* Store */
extern const char LABL_CREL  [NULL_CODE];   /* Cost Relationship */
extern const char LABL_AREL  [NULL_CODE];   /* Add Relationship */
extern const char LABL_MRANGE[NULL_CODE];   /* Margin Range: */
extern const char LABL_OBLS  [NULL_CODE];   /* Obligations */
extern const char LABL_INTLNG[NULL_CODE];   /* Data Integration Language */
extern const char LABL_RNDF  [NULL_CODE];   /* Rounding (Default) */
extern const char LABL_PCKGNG[NULL_CODE];   /* Packaging */
extern const char LABL_NOMFLG[NULL_CODE];   /* Nomination Flag */
extern const char LABL_SHPMNT[NULL_CODE];   /* Shipment */
extern const char LABL_TSFRTV[NULL_CODE];   /* Transfers and RTVs */
extern const char LABL_RMS   [NULL_CODE];   /* Retail Merchandising System */
extern const char LABL_PORDS [NULL_CODE];   /* Purchase Orders */
extern const char LABL_CADJ  [NULL_CODE];   /* Cost Adjustments */
extern const char LABL_VRSN  [NULL_CODE];   /* Version */
extern const char LABL_RPM   [NULL_CODE];   /* Retail Price Management */
extern const char LABL_ERR   [NULL_CODE];   /* Errors */
extern const char LABL_DLDFIL[NULL_CODE];   /* Download File */
extern const char LABL_DLDSTG[NULL_CODE];   /* Download Staged */
extern const char LABL_TSFDCT[NULL_CODE];   /* Transfer Cost Adjustment */
extern const char LABL_TSFDPT[NULL_CODE];   /* Transfer Price Adjustment */
extern const char LABL_REDDAT[NULL_CODE];   /* Receive Dates */
extern const char LABL_ORET  [NULL_CODE];   /* % Off Retail */
extern const char LABL_CSTM  [NULL_CODE];   /* Cost */
extern const char LABL_TEMT  [NULL_CODE];   /* Template Type */
extern const char LABL_REBATS[NULL_CODE];   /* Rebates */
extern const char LABL_BGTHIN[NULL_CODE];   /* Buy / Get Item Threshold Information */
extern const char LABL_HISTPD[NULL_CODE];   /* Historical Period */
extern const char LABL_READAT[NULL_CODE];   /* Ready By Date */
extern const char LABL_ACLS  [NULL_CODE];   /* Stock Order Auto Close */
extern const char LABL_TAX   [NULL_CODE];   /* Tax */
extern const char LABL_FSLT  [NULL_CODE];   /* Source Loc Type */
extern const char LABL_SLID  [NULL_CODE];   /* Source
Loc ID */

extern const char LABL_FRLT  [NULL_CODE];   /* Return Loc Type */
extern const char LABL_RLID  [NULL_CODE];   /* Return Loc ID */
extern const char LABL_WFOT  [NULL_CODE];   /* Order Type */
extern const char LABL_VEHINF[NULL_CODE];   /* Vehicle Information */
extern const char LABL_SUPOOL[NULL_CODE];   /* Supplier Pooling */

#define LABLTT_Z        1   /* New Zone Group */
#define LABLTT_VAT      2   /* VAT Region */
#define LABLTT_POS      3   /* POS Includes VAT? */
#define LABLTT_D        4   /* Dollar */
#define LABLTT_UD       5   /* Unit and Dollar */
#define LABLTT_U        6   /* Unit */
#define LABLTT_PC       7   /* Price Change */
#define LABLTT_CE       8   /* Clearance Event */
#define LABLTT_ST       9   /* Store */
#define LABLTT_NA      10   /* Name */
#define LABLTT_DESC    11   /* Description */
#define LABLTT_ZONE    12   /* Zone */
#define LABLTT_PSC     13   /* Price Store Change */
#define LABLTT_AS      14   /* Add Stores */
#define LABLTT_PZC     15   /* Price Zone Change */
#define LABLTT_AZ      16   /* Add Zones */
#define LABLTT_C       17   /* Clear */
#define LABLTT_REG     18   /* Regular */
#define LABLTT_TRAN    19   /* Transportation ID */
#define LABLTT_ADD     20   /* Add */
#define LABLTT_CA      21   /* Case/ */
#define LABLTT_PP      22   /* Pre-Pack */
#define LABLTT_PPD     23   /* Pre-Pack Detail */
#define LABLTT_PSCW    24   /* Price Store Change (pcstore) */
#define LABLTT_PZCW    25   /* Price Zone Change             (pcstore) */
#define LABLTT_CD      26   /* Case Detail */
#define LABLTT_VSKU    27   /* View Item */
#define LABLTT_VUPC    28   /* View Child Item */
#define LABLTT_CEDT    29   /* Customs Detail */
#define LABLTT_VESS    30   /* Vessel */
#define LABLTT_VOY     31   /* Voyage/Flight */
#define LABLTT_ETD     32   /* Est. Depart Date */
#define LABLTT_OBL     33   /* Obligation */
#define LABLTT_EM      34   /* Edit Matrix */
#define LABLTT_EQ      35   /* Edit Quantity */
#define LABLTT_ER      36   /* Edit Ratio */
#define LABLTT_Q       37   /* Quantity */
#define LABLTT_RAT     38   /* Ratio */
#define LABLTT_COUR    39   /* Courier */
#define LABLTT_SHDT    40   /* Shipment Date */
#define LABLTT_MH4NAM  41   /* @MH4@ Name */
#define LABLTT_TXCD    42   /* Tax Code */
#define LABLTT_ATXCD   43   /* All Tax Codes */
#define LABLTT_FI      50   /*   Find Item   */
#define LABLTT_FU      51   /*   Find Child Item   */
#define LABLTT_FV      52   /*   Find VPN   */
#define LABLTT_SD      53   /* Store Detail */
#define LABLTT_ZD      54   /* Zone Detail */
#define LABLTT_AV      55   /* Availability */
#define LABLTT_PA      56   /* Pack */
#define LABLTT_LC      57   /* Landed Cost Per Unit */
#define LABLTT_SUPP    58   /* Supp. */
#define LABLTT_CART    59   /* Carton */
#define LABLTT_MON     60   /* Monetary */
#define LABLTT_SKU     61   /* Item */
#define LABLTT_DEPT    62   /* @MHP4@ */
#define LABLTT_ITEM    63   /* Items */
#define LABLTT_UPC     64   /* UPC */
#define LABLTT_VPN     65   /* VPN */
#define LABLTT_UDA     66   /* UDA */
#define LABLTT_UDAD    67   /* UDA Date */
#define LABLTT_UDAT    68   /* UDA Text */
#define LABLTT_UDAV    69   /* UDA Value */
#define LABLTT_ILST    70   /* Item List */
#define LABLTT_STY     71   /* Item Parent */
#define LABLTT_SIZE1   72   /* Size 1 */
#define LABLTT_SIZE2   73   /* Size 2 */
#define LABLTT_COLOR   74   /* Color */
#define LABLTT_CUCL    75   /* Currently on Clearance */
#define LABLTT_BCK2    76   /* Back To */
#define LABLTT_PLUS    77   /* Plus Last */
#define LABLTT_LSTW    78   /* Weeks */
#define LABLTT_MH5NAM  79   /* @MH5@ Name */
#define LABLTT_CI      80   /* Cancel Item */
#define LABLTT_DI      81   /* Delete Item */
#define LABLTT_DEP     82   /* @MH4@ */
#define LABLTT_ITM     83   /* Item */
#define LABLTT_PLC     84   /* Total Pack Landed Cost */
#define LABLTT_NULL    85   /* Blank */
#define LABLTT_NEWS    86   /* New Item Parent */
#define LABLTT_NEWI    87   /* New Item */
#define LABLTT_AI      88   /* Apply Item */
#define LABLTT_AL      89   /* Apply Item List */
#define LABLTT_AU      90   /* Apply Child Item */
#define LABLTT_TA      91   /* Threshold Amount */
#define LABLTT_TU      92   /* Threshold Unit */
#define LABLTT_DCD     93   /* Document Details */
#define LABLTT_ETDT    94   /* Entity Details */
#define LABLTT_TXBR    95   /* Tax Breakup */
#define LABLTT_MH6NAM  96   /* @MH6@ Name */
#define LABLTT_LTCR    98   /* Letter of Credit */
#define LABLTT_PO      99   /* Order No. */
#define LABLTT_CNTR   100   /* Contract No. */
#define LABLTT_CTRY   101   /* Country */
#define LABLTT_PYMT   102   /* Letter of Credit */
#define LABLTT_SUP    103   /* Supplier */
#define LABLTT_BANK   104   /* Bank */
#define LABLTT_PTNR   105   /* Partner */
#define LABLTT_PTYP   106   /* Partner Type */
#define LABLTT_CU     107   /* Entry No. */
#define LABLTT_LO     108   /* Logistics No. */
#define LABLTT_LOBL   109   /* BOL/AWB */
#define LABLTT_LOCO   110   /* Container */
#define LABLTT_LOCI   111   /* Invoice No. */
#define LABLTT_HTSC   112   /* HTS Heading */
#define LABLTT_CB     113   /* Order Cost Basis */
#define LABLTT_EC     114   /* Expense Category */
#define LABLTT_OC     115   /* Country Of Sourcing */
#define LABLTT_TMBL   116   /* Timeline Base */
#define LABLTT_DPD    117   /* Discharge Port Description */
#define LABLTT_LPD    118   /* Lading port Description */
#define LABLTT_ZND    119   /* Zone Description */
#define LABLTT_IC     120   /* Import Country */
#define LABLTT_EXRTAB 128   /* Exch. Rt. */
#define LABLTT_EURXAB 129   /* Exch. Rt.(Euro) */
#define LABLTT_RDLB   130   /* Required Documents */
#define LABLTT_IHLB   131   /* HTS Assessment */
#define LABLTT_PARD   132   /* Partner/Required Documents */
#define LABLTT_ITHT   133   /* Item/HTS */
#define LABLTT_ITIM   134   /* Item/Import Attributes */
#define LABLTT_ITRD   135   /* Item/Required Documents */
#define LABLTT_ITTM   136   /* Item/Timelines */
#define LABLTT_ITEL   137   /* Eligible Tariff Treatments */
#define LABLTT_IDLB   140   /* Import Desc. */
#define LABLTT_CTRD   141   /* Country Required Documents */
#define LABLTT_CTTT   142   /* Country Tariff Treatments */
#define LABLTT_CZG    143   /* Cost Zone Group */
#define LABLTT_CZ     144   /* Cost Zone */
#define LABLTT_BP     146   /* Base Profile */
#define LABLTT_PROF   147   /* Profile Type */
#define LABLTT_IT     148   /* Item */
#define LABLTT_LOG    149   /* Logistics */
#define LABLTT_CUST   150   /* Customs */
#define LABLTT_CNT    151   /* Contract */
#define LABLTT_PORD   152   /* Purchase Order */
#define LABLTT_POIT   153   /* Order/Item */
#define LABLTT_LIST   154   /* List of */
#define LABLTT_TRTW   155   /* Associated with Trait */
#define LABLTT_MAXL   156   /* Maximum Length */
#define LABLTT_LEVEL  157   /* Level */
#define LABLTT_COM    158   /* Comments */
#define LABLTT_TEXT   159   /* Text */
#define LABLTT_TERM   160   /* Terms */
#define LABLTT_IMPD   161   /* Import Description */
#define LABLTT_DCTX   162   /* Document Text */
#define LABLTT_MWIN   163   /* Maintenance Window */
#define LABLTT_HTDS   164   /* HTS Description */
#define LABLTT_SQF    165   /* Square Footage Area */
#define LABLTT_DEPS   173   /* *All @MHP4@ */
#define LABLTT_CLASS  174   /* *All @MHP5@ */
#define LABLTT_SCLASS 175   /* *All @MHP6@ */
#define LABLTT_LOCS   176   /* *All Locations */
#define LABLTT_TRCODE 177   /* *All Tran Codes */
#define LABLTT_UOT    178   /* Unit of Transfer */
#define LABLTT_UOP    179   /* Unit of Purchase */
#define LABLTT_UOA    180   /* Unit of Allocation */
#define LABLTT_CLS    181   /* @MH5@ */
#define LABLTT_DIV    182   /* @MH2@ */
#define LABLTT_GROUP  183   /* @MH3@ */
#define LABLTT_SCLS   184   /* @MH6@ */
#define LABLTT_COMP   185   /* @OH1@ */
#define LABLTT_DIS    186   /* @OH5@ */
#define LABLTT_ORC    190   /* Orig. Country */
#define LABLTT_ORGC   191   /* Orig. Ctry */
#define LABLTT_ALL    200   /* *All */
#define LABLTT_CLRS   201   /* Clearance Defaults */
#define LABLTT_EWIN   202   /* Entry Window */
#define LABLTT_PROM   203   /* Promotion */
#define LABLTT_EVDS   204   /* Event Description */
#define LABLTT_PRDS   205   /* Promotion Description */
#define LABLTT_KEY    206   /* Key Value */
#define LABLTT_TRANS  207   /* Translated Value */
#define LABLTT_UOM    208   /* UOM */
#define LABLTT_MERCH  209   /* Merchandise Description */
#define LABLTT_ERCODE 210   /* Merchandise Description */
#define LABLTT_ACT    211   /* Actual */
#define LABLTT_PROJ   212   /* Projected */
#define LABLTT_ADF    213   /* Apply Defaults For */
#define LABLTT_WKE    214   /* Week */
#define LABLTT_TR     215   /* Transportation */
#define LABLTT_TRCO   216   /* Container */
#define LABLTT_TRBL   217   /* BOL/AWB */
#define LABLTT_TRCI   218   /* Commercial Invoice */
#define LABLTT_NEXT   219   /* Next */
#define LABLTT_CASE   220   /* Case */
#define LABLTT_SUOM   221   /* SUOM */
#define LABLTT_PAL    222   /* Pallet */
#define LABLTT_SPPACK 223   /* Supp Pack */
#define LABLTT_EUROEX 224   /* Exchange Rate(Euro) */
#define LABLTT_EXRATE 225   /* Exchange Rate */
#define LABLTT_ORDEX  227   /* Order Exchange Rate */
#define LABLTT_ISSUE  228   /* Form Issue */
#define LABLTT_GVN    229   /* Gift Certificate Number */
#define LABLTT_SQST   230   /* SQL Statement */
#define LABLTT_FUEL   231   /* Fuel  */
#define LABLTT_DEFA   232   /* Default  */
#define LABLTT_DEPIMP 233   /* @MH4@ Impact */
#define LABLTT_TRAW   234   /* Traits associated with  */
#define LABLTT_SLCM   235   /* Cost Maintenance          (stkldgmn) */
#define LABLTT_SLRM   236   /* Retail Maintenance          (stkldgmn) */
#define LABLTT_VCRM   237   /* Rates Maintenance          (vatcode) */
#define LABLTT_SCRT   238   /* Search Criteria */
#define LABLTT_SRLT   239   /* Search Results */
#define LABLTT_LYR1   240   /* First Layer */
#define LABLTT_LYR2   241   /* Second Layer */
#define LABLTT_ITBN   242   /* Item/Button */
#define LABLTT_INCL   243   /* Include */
#define LABLTT_EXCL   244   /* Exclude */
#define LABLTT_CRIT   245   /* Criteria */
#define LABLTT_COUP   246   /* Coupon */
#define LABLTT_PRES   247   /* Product Restriction */
#define LABLTT_CCVL   249   /* Credit Card Validation */
#define LABLTT_APLY   252   /* Apply */
#define LABLTT_DAY    253   /* Day */
#define LABLTT_TIME   254   /* Time */
#define LABLTT_DATE   255   /* Date */
#define LABLTT_TXCDE  256   /* Tax Codes */
#define LABLTT_TXRT   257   /* Tax Rates */
#define LABLTT_WAST   258   /* Wastage Attributes */
#define LABLTT_TICK   259   /* Ticket Attributes */
#define LABLTT_COPY   260   /* Copy tax codes from item */
#define LABLTT_DIP    261   /* Dis. Port */
#define LABLTT_DIST   262   /* Distribute By */
#define LABLTT_ALLTLS 263   /* All Totals */
#define LABLTT_SZRANG 264   /* Size Range */
#define LABLTT_SZTEMP 265   /* Size Template */
#define LABLTT_NULL1  266   /* NULL */
#define LABLTT_ALLOC  267   /* Allocate to Location */
#define LABLTT_ALWAR  268   /* Allocate from Warehouse */
#define LABLTT_SUMM   269   /* Summary */
#define LABLTT_SPDP   270   /* Supplier/@MH4@ */
#define LABLTT_G      271   /* Group */
#define LABLTT_VAL    272   /* Value */
#define LABLTT_GRP    273   /* Group */
#define LABLTT_A      274   /* Available */
#define LABLTT_IR     275   /* In Range */
#define LABLTT_DP     276   /* Delete Pending */
#define LABLTT_RFIT   277   /* Ref Item */
#define LABLTT_PARENT 280   /* Item Parent/Grandparent */
#define LABLTT_DIFF1  281   /* Diff 1 */
#define LABLTT_DIFF2  282   /* Diff 2 */
#define LABLTT_FT     283   /* Feet */
#define LABLTT_M      284   /* Meters */
#define LABLTT_LCID   285   /* Letter of Credit */
#define LABLTT_UCHRG  286   /* Up Charges */
#define LABLTT_MLOC   287   /* Multiple Locations */
#define LABLTT_SUBPO  288   /* Submitted PO */
#define LABLTT_POTO1  289   /* 1st Order Total */
#define LABLTT_POTO2  290   /* 2nd Order Total */
#define LABLTT_SCIS   293   /* Scaling Issues */
#define LABLTT_TRIS   294   /* Truck Splitting Issues */
#define LABLTT_P      295   /* Physical */
#define LABLTT_V      296   /* Virtual */
#define LABLTT_UNKNWN 297   /* Unknown */
#define LABLTT_ORD    298   /* Order */
#define LABLTT_TSF    299   /* Transfer */
#define LABLTT_ALLC   300   /* Allocation */
#define LABLTT_DOCTYP 301   /* Document Type */
#define LABLTT_LOCTYP 302   /* Location Type */
#define LABLTT_UCST   303   /* Unit Cost */
#define LABLTT_UELC   304   /* Unit ELC */
#define LABLTT_LD     305   /* Launch Date */
#define LABLTT_QO     306   /* Qty/Key Options */
#define LABLTT_MP     307   /* Manual Price Entry */
#define LABLTT_DC     308   /* Deposit Code */
#define LABLTT_FS     309   /* Food Stamp Ind */
#define LABLTT_WI     310   /* WIC Ind */
#define LABLTT_PT     311   /* Proportional Tare Pct */
#define LABLTT_FX     312   /* Fixed Tare Value */
#define LABLTT_FM     313   /* Fixed Tare UOM */
#define LABLTT_RE     314   /* Reward Eligible Ind */
#define LABLTT_NB     315   /* National Brand Comparison Item */
#define LABLTT_RP     316   /* Return Policy */
#define LABLTT_SS     317   /* Stop Sale Ind */
#define LABLTT_EL     318   /* Electronic Market Clubs */
#define LABLTT_RC     319   /* Report Code */
#define LABLTT_SL     320   /* Shelf Life on Select */
#define LABLTT_SR     321   /* Shelf Life on Rcpt */
#define LABLTT_IB     322   /* Investment Buy Shelf Life */
#define LABLTT_SI     323   /* Store Reorderable Ind */
#define LABLTT_RS     324   /* Rack Size */
#define LABLTT_FP     325   /* Full Pallet Item */
#define LABLTT_IM     326   /* In Store Market Basket */
#define LABLTT_SC     327   /* Storage Location */
#define LABLTT_AT     328   /* Alt Storage Location */
#define LABLTT_RI     329   /* Returnable Ind */
#define LABLTT_RF     330   /* Refundable Ind */
#define LABLTT_BI     331   /* Back Orderable Ind */
#define LABLTT_DIFF3  332   /* Diff 3 */
#define LABLTT_DIFF4  333   /* Diff 4 */
#define LABLTT_DIFFT  334   /* Diff Type */
#define LABLTT_IDAPPL 335   /* ID Applied */
#define LABLTT_ACDS   336   /* Activity Description */
#define LABLTT_AACT   337   /* All Activities */
#define LABLTT_SPLU   338   /* Stores */
#define LABLTT_WPLU   339   /* Warehouses */
#define LABLTT_IPLU   340   /* Internal Finishers */
#define LABLTT_EPLU   341   /* External Finishers */
#define LABLTT_WH     342   /* Warehouse */
#define LABLTT_NOEXST 400   /* No longer exists in pricing system */
#define LABLTT_OWCUD  401   /* Worksheet PO Clean Up Delay */
#define LABLTT_OPRCD  402   /* Partially Received PO Close Delay */
#define LABLTT_OACD   403   /* Approved PO Close Delay */
#define LABLTT_DIYRS  404   /* Years */
#define LABLTT_RSUD   405   /* Retention of Scheduled Updates Days */
#define LABLTT_MRD    407   /* Retention for MRT Transfers */
#define LABLTT_RRTI   408   /* RTV/RAC Transfer */
#define LABLTT_MSTSSR 409   /* Store-to-Store */
#define LABLTT_MWTSSR 410   /* Whs-to-Store */
#define LABLTT_MSTWSR 411   /* Store-to-Whs */
#define LABLTT_MWTWSR 412   /* Whs-to-Whs */
#define LABLTT_RCAT   413   /* Receiver Cost Adjustment */
#define LABLTT_CIF    414   /* Frequency */
#define LABLTT_CIISLI 415   /* Level */
#define LABLTT_AIPI   416   /* Advance Inventory Planning */
#define LABLTT_SPAGI  417   /* Automatically Generate ID */
#define LABLTT_PIUI   418   /* Partner ID Uniq Across All Partner Types */
#define LABLTT_CNCR   419   /* Concession Rate */
#define LABLTT_CNSR   420   /* Consignment Rate */
#define LABLTT_FRMLOC 421   /* From Loc */
#define LABLTT_FINSHR 422   /* Finisher */
#define LABLTT_TSFPRC 423   /* Transfer
 Price */

#define LABLTT_TSFPAT 424   /* Transfer Price Adjustment Type */
#define LABLTT_TSFCST 425   /* Transfer
 Cost */

#define LABLTT_TSFCAT 426   /* Transfer Cost
 Adjustment Type */

#define LABLTT_UL     427   /* Upper Limit */
#define LABLTT_LL     428   /* Lower Limit */
#define LABLTT_LSTWHS 429   /* List of Warehouses */
#define LABLTT_LSTSTS 430   /* List of Stores */
#define LABLTT_VIEWBY 431   /* View By */
#define LABLTT_DATEEN 432   /* Date Entry     (date) */
#define LABLTT_UDA1   433   /* UDA - List of Values */
#define LABLTT_UDA2   434   /* UDA - Dates */
#define LABLTT_UDA3   435   /* UDA - Free Form Text */
#define LABLTT_TOTALW 436   /* Total Def. Wizard  (satotal) */
#define LABLTT_RULEW  437   /* Rules  Def. Wizard  (sarule) */
#define LABLTT_SHOWDT 438   /* Show Detail */
#define LABLTT_CONTOF 439   /* Contents of */
#define LABLTT_REF1DE 440   /* Reference No. 1 */
#define LABLTT_REF2DE 441   /* Reference No. 2 */
#define LABLTT_USEOWD 442   /* Unit Sales by End of Week Date */
#define LABLTT_UIEOWD 443   /* Unit Issues by End of Week Date */
#define LABLTT_AREA   444   /* @OH3@ */
#define LABLTT_CHAIN  445   /* @OH2@ */
#define LABLTT_DISTRI 446   /* @OH5@ */
#define LABLTT_VIEWNM 447   /* View Name */
#define LABLTT_REALMI 448   /* Realm Id */
#define LABLTT_REALMA 449   /* Realm Alias */
#define LABLTT_PARAM  450   /* Parameter */
#define LABLTT_PARAMA 451   /* Parameter Alias */
#define LABLTT_MODE   452   /* Mode */
#define LABLTT_BLOCK  453   /* Block */
#define LABLTT_RECGRP 454   /* Record Group Column */
#define LABLTT_SUBMIT 455   /* submitted */
#define LABLTT_APPRVE 456   /* approved */
#define LABLTT_PICKUP 457   /* Pickup Date */
#define LABLTT_NOTBEF 458   /* Not Before Date */
#define LABLTT_NOTAFT 459   /* Not After Date */
#define LABLTT_REGION 460   /* @OH4@ */
#define LABLTT_SIZE   461   /* Size */
#define LABLTT_INFO   462   /* Information */
#define LABLTT_COST   463   /* Cost */
#define LABLTT_NEEDDT 464   /* Need Date */
#define LABLTT_VDATE  465   /* Business Date */
#define LABLTT_ASN    466   /* ASN */
#define LABLTT_FLSOB  501   /* From Set of Books */
#define LABLTT_TLSOB  502   /* To Set of Books */
#define LABLTT_FNSOB  503   /* Finisher Set of Books */
#define LABLTT_CUSDLY 510   /* Customer Delivery Date */
#define LABLTT_CUSDLT 511   /* Customer Delivery Time */
#define LABLTT_BINFO  600   /* Billing Information */
#define LABLTT_DINFO  601   /* Delivery Information */
#define LABLTT_NOMF1  611   /* Flag 1 */
#define LABLTT_NOMF2  612   /* In Duty */
#define LABLTT_NOMF3  613   /* Flag 3 */
#define LABLTT_NOMF4  614   /* In Exp */
#define LABLTT_NOMF5  615   /* In ALC */
#define LABLTT_R      616   /* Supplier Revision */
#define LABLTT_RV     617   /* Retailer Version */
#define LABLTT_MARGIN 700   /* Margin */
#define LABLTT_RETAIL 701   /* Retail */
#define LABLTT_MARGPT 702   /* Margin Percent */
#define LABLTT_RETLPT 703   /* Retail Percent */
#define LABLTT_FCOST  704   /* Fixed Cost */
#define LABLTT_CRET   705   /* Completed Reclassification Event */
#define LABLTT_OAL    800   /* Order Appr. Limit */
#define LABLTT_TCC    821   /* Total Customer Cost */
#define LABLTT_TP     827   /* Transfer Price */
#define LABLTT_ADJRC  849   /* Adjustment Reason Code */
#define LABLTT_FRMLC  850   /* From Location */
#define LABLTT_TOLOC  851   /* To Location */
#define LABLTT_ACTID  852   /* Activity ID */
#define LABLTT_TRFNO  853   /* Tran Ref No */
#define LABLTT_TCAR   854   /* Adjustment Reason Code */
#define LABLTT_TCFL   855   /* From Location */
#define LABLTT_TCTL   856   /* To Location */
#define LABLTT_TCTR   857   /* Tran. Ref. No. */
#define LABLTT_SP     858   /* space */
#define LABLTT_CTMZ   859   /* Customize */
#define LABLTT_BLANKC 860   /* Blank Color */
#define LABLTT_BLANKF 861   /* Blank Flavor */
#define LABLTT_BLANKP 862   /* Blank Pattern */
#define LABLTT_BLANKE 863   /* Blank Scent */
#define LABLTT_BLANKS 864   /* Blank Size */
#define LABLTT_ASATW  865   /* @OHP3@ Associated with Trait */
#define LABLTT_TSAWR  866   /* Traits Associated with @OHP4@ */
#define LABLTT_RSAWT  867   /* @OHP4@ Associated with Trait */
#define LABLTT_SDSFA  868   /* Store Department Square Footage Area */
#define LABLTT_ISIZE  869   /* Inner Size */
#define LABLTT_CSIZE  870   /* Case Size */
#define LABLTT_CNTACT 871   /* Contact */
#define LABLTT_TO     872   /* To */
#define LABLTT_SEARCH 873   /* Search */
#define LABLTT_APPT   874   /* Appointment */
#define LABLTT_APPTD  875   /* Appointment Details */
#define LABLTT_FILCRT 876   /* Filter Criteria */
#define LABLTT_SCALCO 877   /* Scaling Constraints */
#define LABLTT_COMPIN 878   /* Competitor Info */
#define LABLTT_MCUC   879   /* Mass Change Unit Cost */
#define LABLTT_BLIT   880   /* Blank Item */
#define LABLTT_COMPTO 881   /* Compared To */
#define LABLTT_FILBY  882   /* Filter By */
#define LABLTT_ENTAS  883   /* Enter As: */
#define LABLTT_TRANL  884   /* Tran Level: */
#define LABLTT_SUNAB  885   /* Sun */
#define LABLTT_MONAB  886   /* Mon */
#define LABLTT_FRIAB  887   /* Fri */
#define LABLTT_TUEAB  888   /* Tue */
#define LABLTT_WEDAB  889   /* Wed */
#define LABLTT_SATAB  890   /* Sat */
#define LABLTT_THUAB  891   /* Thu */
#define LABLTT_NOTBET 892   /* Not Before Datetime */
#define LABLTT_DEALH  893   /* Deal Head */
#define LABLTT_DEALD  894   /* Deal Head */
#define LABLTT_GET    895   /* Get */
#define LABLTT_BUY    896   /* Buy */
#define LABLTT_LOCS1  898   /* Locations */
#define LABLTT_MUCTYP 899   /* Markup Calculation Type */
#define LABLTT_PCTYP  900   /* Profit Calculation Type */
#define LABLTT_OTBTYP 901   /* OTB Calculation Type */
#define LABLTT_PURTYP 902   /* Purchase Type */
#define LABLTT_BTPER  903   /* Build Time Period */
#define LABLTT_GENBY  904   /* Generated By */
#define LABLTT_SALTYP 905   /* Sales Type */
#define LABLTT_FILTER 906   /* Filter */
#define LABLTT_NIUI   907   /* New Items/Unavailable Items */
#define LABLTT_DIFFS  908   /* Diffs */
#define LABLTT_OVRRID 909   /* Override */
#define LABLTT_GMLVL  910   /* @MH3@/Merchandise Level */
#define LABLTT_GOLVL  911   /* @MH3@/Organization Level */
#define LABLTT_TOTALS 912   /* Totals */
#define LABLTT_DIFFER 913   /* Differentiators */
#define LABLTT_X      914   /* X */
#define LABLTT_GENINF 915   /* General Info */
#define LABLTT_DEMINS 916   /* Dimensions */
#define LABLTT_WEIGHT 917   /* Weight */
#define LABLTT_PACKS  918   /* Packs */
#define LABLTT_PRMSRC 919   /* Primary Sourcing */
#define LABLTT_CPKINF 921   /* Case Pack Information */
#define LABLTT_PAR    922   /* Parent */
#define LABLTT_LTIHI  923   /* Location Ti - Hi */
#define LABLTT_RCOM   924   /* Customer Order Specific Attributes */
#define LABLTT_DEPITM 925   /* Deposit Item */
#define LABLTT_CTCHWT 926   /* Catch Weight */
#define LABLTT_ACNTIM 927   /* Associated Contents Items */
#define LABLTT_ATTRS  928   /* Attributes */
#define LABLTT_PRMSUP 929   /* Primary Supplier */
#define LABLTT_PRMCTY 930   /* Primary Country */
#define LABLTT_POINFO 931   /* Ordering Information */
#define LABLTT_SPTIHI 932   /* Supplier/Inbound Ti - Hi */
#define LABLTT_RNDTHR 933   /* Rounding Thresholds */
#define LABLTT_TOLTYP 934   /* Tolerance Type */
#define LABLTT_OCDETL 936   /* Country Of Sourcing Details */
#define LABLTT_LOCDIN 937   /* Location Default Information */
#define LABLTT_PRMARY 938   /* Primary */
#define LABLTT_SECOND 939   /* Secondary */
#define LABLTT_STSALE 940   /* Store Sales */
#define LABLTT_SOH    941   /* Stock On Hand */
#define LABLTT_VIEW   943   /* View */
#define LABLTT_COPYFR 944   /* Copy From */
#define LABLTT_RTVDTL 945   /* RTV Details */
#define LABLTT_MRTDTL 946   /* MRT Details */
#define LABLTT_ADITMS 947   /* Add Items */
#define LABLTT_ITMDTL 948   /* Item Details */
#define LABLTT_LOCDTL 949   /* Location Details */
#define LABLTT_APPDEL 950   /* Apply or Delete To: */
#define LABLTT_ALTCP  951   /* Apply Locations and Transfer Cost/Price */
#define LABLTT_CURNAM 952   /* Current Name */
#define LABLTT_NEWNAM 953   /* New Name */
#define LABLTT_DEFNAM 954   /* Default Name */
#define LABLTT_PHNCON 955   /* Phone Numbers/Contact Name */
#define LABLTT_OINFO  956   /* Other Information */
#define LABLTT_MIN1   957   /* Minimum 1 */
#define LABLTT_MIN2   958   /* Minimum 2 */
#define LABLTT_MINCON 959   /* Minimum Constraints */
#define LABLTT_TRSCON 960   /* Truck Splitting Constraints */
#define LABLTT_SUPPOC 961   /* Supplier Order Costs */
#define LABLTT_QTYS   962   /* Quantities */
#define LABLTT_PODUES 963   /* Order Due Status */
#define LABLTT_CON2   964   /* Constraint 2 */
#define LABLTT_CON1   965   /* Constraint 1 */
#define LABLTT_ISSUES 966   /* Issues */
#define LABLTT_SUPPOL 967   /* Supplier Pooling */
#define LABLTT_POCOST 968   /* Order Costs */
#define LABLTT_POQTYS 969   /* Order Quantities */
#define LABLTT_ALLOCD 970   /* Allocation Details */
#define LABLTT_PODETL 971   /* Order Details */
#define LABLTT_LOCATS 972   /* Order Details */
#define LABLTT_DIFMTX 973   /* Diff Matrix */
#define LABLTT_ENTRAS 974   /* Enter As */
#define LABLTT_OHDCHG 975   /* Order Header Date Changes */
#define LABLTT_TYPE   976   /* Type */
#define LABLTT_CQTYIL 977   /* Cost and Qty Changes By Item/Location */
#define LABLTT_MODLST 978   /* Modify List */
#define LABLTT_PNDPRC 979   /* Pending Price Changes */
#define LABLTT_LOCSEL 980   /* Location Selection */
#define LABLTT_PRCDEF 981   /* Price Change Default */
#define LABLTT_DMTXBY 982   /*  Distribute Matrix  By */
#define LABLTT_DSTZAB 983   /* Distribute Z Axis By */
#define LABLTT_AXDGRP 984   /* Axes Diff Groups */
#define LABLTT_DRLMT  985   /* Diff Range Limiting */
#define LABLTT_ZAXIS  986   /* Z Axis */
#define LABLTT_DFFRT  987   /* Diff Ratio */
#define LABLTT_XTOTAL 988   /* X Total */
#define LABLTT_XYDIST 989   /* X-Y Distribution */
#define LABLTT_DISTY  990   /* Distribute Y By */
#define LABLTT_XDIST  991   /* X Distribution */
#define LABLTT_DISTX  992   /* Distribute X By */
#define LABLTT_PRFFUL 993   /* Proof of Performance Fulfillment */
#define LABLTT_PRFDEF 994   /* Proof of Performance Definitions */
#define LABLTT_MOVE   995   /* Move */
#define LABLTT_PREPLP 996   /* Primary Replenishment Pack */
#define LABLTT_STKVAL 997   /* Stock Values */
#define LABLTT_LDTIME 998   /* Lead Times */
#define LABLTT_RPLCYL 999   /* Replenishment Cycle */
#define LABLTT_REPLDT 100   /* Replenishment Dates */
#define LABLTT_RPLMA  100   /* Replenishment Method Attributes */
#define LABLTT_RPLAMA 100   /* Repl Attribute Maintenance Action */
#define LABLTT_UINFO  100   /* User Information */
#define LABLTT_SCHADT 100   /* Scheduled Active Date */
#define LABLTT_MADMIN 100   /* Master Administration */
#define LABLTT_AOCTOL 100   /* Allowable Order Change Tolerances */
#define LABLTT_SCALE  100   /* Scaling */
#define LABLTT_RECALC 100   /* Recalculation */
#define LABLTT_ORDREV 101   /* Order Review */
#define LABLTT_DOINFO 101   /* Due Order Information */
#define LABLTT_CPOSTS 101   /* Current Order Status */
#define LABLTT_REPLC  101   /* Replenishment Costs */
#define LABLTT_GENFOR 101   /* Generated Forecast Results */
#define LABLTT_LOCINV 101   /* Location Inventory */
#define LABLTT_PACKSZ 101   /* Pack Sizes */
#define LABLTT_REPLRS 101   /* Replenishment Results */
#define LABLTT_OITEM  101   /* Original Item */
#define LABLTT_CTITEM 101   /* Change To Item */
#define LABLTT_RELDTS 102   /* Item Relationship Details */
#define LABLTT_RELIMS 102   /* Related Items */
#define LABLTT_PRESAB 102   /* Presence or absence creates error? */
#define LABLTT_EXGRP  102   /* Execute Group */
#define LABLTT_EXGRPD 102   /* Execute Group Details */
#define LABLTT_WIZRUL 102   /* Will you use the wizard to write a rule? */
#define LABLTT_BDRANG 102   /* Business Day Range? */
#define LABLTT_STORES 102   /* Stores */
#define LABLTT_STATUS 103   /* Status */
#define LABLTT_STSTAT 103   /* Store Status */
#define LABLTT_HQSTAT 103   /* Headquarter Status */
#define LABLTT_TINFO  103   /* Transaction Information */
#define LABLTT_FMERCH 103   /* Do you have Fuel Merchandise? */
#define LABLTT_SVALCO 103   /* System Validation Constraints */
#define LABLTT_CSTCRT 103   /* Compare Store Criteria */
#define LABLTT_ESCIND 103   /* Escheatment Indicator */
#define LABLTT_TVWOPT 103   /* Total View Option */
#define LABLTT_ACHINF 103   /* ACH Information */
#define LABLTT_RINADJ 104   /* Retailer Details for Income Adjustment */
#define LABLTT_TNDARG 104   /* Tender Amount Range */
#define LABLTT_AUDTRD 104   /* Audit Trail Details */
#define LABLTT_BSDYRG 104   /* Business Day Range */
#define LABLTT_UPDTRG 104   /* Update Date/Time Range */
#define LABLTT_TOTDTL 104   /* Totaling Details */
#define LABLTT_TRDTRG 104   /* Tran. Date/Time Range */
#define LABLTT_POSTRG 104   /* POS Tran. No. Range */
#define LABLTT_STUOMC 104   /* Standard Unit of Measure Conversions */
#define LABLTT_SELUOM 104   /* Selling Unit of Measure */
#define LABLTT_STDUOM 105   /* Standard Unit of Measure */
#define LABLTT_DSCUOM 105   /* Discount Unit of Measure */
#define LABLTT_TLCOA  105   /* Transaction Level Customer Order Attribs */
#define LABLTT_ITMREF 105   /* Item Reference */
#define LABLTT_TNDREF 105   /* Tender Reference */
#define LABLTT_TXNREF 105   /* Transaction Reference */
#define LABLTT_ISSSLD 105   /* Issued/Sold */
#define LABLTT_ILCOA  105   /* Item Level Customer Order Attributes */
#define LABLTT_REDEEM 105   /* Redeemed */
#define LABLTT_ASSIGN 105   /* Assigned */
#define LABLTT_TAXREF 106   /* Tax Reference */
#define LABLTT_SHDTS  106   /* Shipment Dates */
#define LABLTT_RECDAT 106   /* Shipment Dates */
#define LABLTT_FROM   106   /* From */
#define LABLTT_MERCHH 106   /* Merchandise Hierarchy */
#define LABLTT_ORGH   106   /* Organizational Hierarchy */
#define LABLTT_DETLS  106   /* Details */
#define LABLTT_DETLIN 106   /* Detail Information */
#define LABLTT_HEADIN 106   /* Header Information */
#define LABLTT_COUNT  106   /* Count */
#define LABLTT_LIST2  107   /* List */
#define LABLTT_CINFCI 107   /* Copy Info From Component Item */
#define LABLTT_DIMINF 107   /* Dimension Info */
#define LABLTT_APKINF 107   /* Apply Pack Info */
#define LABLTT_TOLS   107   /* Tolerances */
#define LABLTT_CUR    107   /* Currency */
#define LABLTT_SITMBY 107   /* Select Item By: */
#define LABLTT_DAYCNT 107   /* Days of Count */
#define LABLTT_REPL   107   /* Replenishment */
#define LABLTT_INVBUY 108   /* Investment Buy */
#define LABLTT_LOC    108   /* Location */
#define LABLTT_DATERG 108   /* Date Range */
#define LABLTT_APPLYB 108   /* Apply Block */
#define LABLTT_FILPRI 108   /* Fill Priority */
#define LABLTT_DOPROC 108   /* Due Order Processing */
#define LABLTT_SCALAT 108   /* Scaling Attributes */
#define LABLTT_INVBAT 108   /* Investment Buy Attributes */
#define LABLTT_DFSCLT 108   /* Default From Scaling Constraints to: */
#define LABLTT_TRSATT 109   /* Truck Splitting Attributes */
#define LABLTT_RNDATT 109   /* Rounding Attributes */
#define LABLTT_SMNATT 109   /* Supplier Minimum Attributes */
#define LABLTT_OR     109   /* Or */
#define LABLTT_SUPEDI 109   /* EDI Trans. Supported By the Supplier */
#define LABLTT_EDICCV 109   /* EDI Cost Change Approval Variance */
#define LABLTT_INDS   109   /* Indicators */
#define LABLTT_HQCTRY 109   /* Corporate HQ Country */
#define LABLTT_INVENT 110   /* Inventory */
#define LABLTT_PRMLNG 110   /* Primary Language */
#define LABLTT_CHANNL 110   /* Channel */
#define LABLTT_TSFS   110   /* Transfers */
#define LABLTT_MONTHS 110   /* Months */
#define LABLTT_MLOCRD 110   /* Markdown Location For Retail Difference */
#define LABLTT_HTS    110   /* HTS */
#define LABLTT_RIMOPT 110   /* Invoice Matching Options */
#define LABLTT_DAYS   110   /* Days */
#define LABLTT_DEFUOP 110   /* Default Unit of Purchase */
#define LABLTT_ITMOPT 111   /* Item Options */
#define LABLTT_DEFUOM 111   /* Default UOM */
#define LABLTT_SOB    111   /* Set of Books */
#define LABLTT_DISTRO 111   /* Distribution */
#define LABLTT_PACKNG 111   /* Packing */
#define LABLTT_TCKLBL 111   /* Tickets/Labels */
#define LABLTT_TIMPRD 111   /* Time Period Detail */
#define LABLTT_SYSREC 111   /* System of Record */
#define LABLTT_STOREC 111   /* Stock Order Reconciliation */
#define LABLTT_LOVFIL 112   /* LOV Filtering */
#define LABLTT_OUTSTR 112   /* Outside Storage */
#define LABLTT_WHSTR  112   /* Warehouse Storage */
#define LABLTT_DEALS  112   /* Deals */
#define LABLTT_LTRCRT 112   /* Letter of Credit */
#define LABLTT_ORDERS 112   /* Orders */
#define LABLTT_EDITXN 112   /* EDI Transactions */
#define LABLTT_CNTRTS 112   /* Contracts */
#define LABLTT_NWPPRC 112   /* LVP Process Controls */
#define LABLTT_HISTRY 112   /* History */
#define LABLTT_BILLAD 113   /* Bill of Lading */
#define LABLTT_STKCNT 113   /* Stock Count */
#define LABLTT_COSTMD 113   /* Cost Method */
#define LABLTT_CHKDIG 113   /* Check Digit */
#define LABLTT_DCSTYP 113   /* Discount Type */
#define LABLTT_APPLTP 113   /* Apply Type */
#define LABLTT_CNTXTD 113   /* Context Details */
#define LABLTT_WLKTST 113   /* Walk Through Store */
#define LABLTT_CSTLOC 113   /* Cost Location */
#define LABLTT_RLMS   113   /* Realms */
#define LABLTT_SELRLM 114   /* Selected Realm */
#define LABLTT_RRBTV  114   /* Restrict Results By Table Values */
#define LABLTT_RRBCV  114   /* Restrict Results By Constant Values */
#define LABLTT_PCTSG  114   /* Pct */
#define LABLTT_MRKUPR 114   /* Markup Pct Retail */
#define LABLTT_ROUNDP 114   /* Rounding Pct */
#define LABLTT_LOCSQ  114   /* locations? */
#define LABLTT_STDAYQ 114   /* store/day? */
#define LABLTT_EVALQ  114   /* evaluation? */
#define LABLTT_DTATIM 114   /* Date And Time */
#define LABLTT_WHISS  115   /* Warehouse Issues */
#define LABLTT_RPLATT 115   /* Replenishment Attributes */
#define LABLTT_BRKTCO 115   /* Bracket Constraints */
#define LABLTT_OTHRAT 115   /* Other Attributes */
#define LABLTT_STKLGR 115   /* Stock Ledger */
#define LABLTT_RECV   115   /* Receiving */
#define LABLTT_OPENTB 115   /* Open to Buy */
#define LABLTT_VATT   115   /* VAT */
#define LABLTT_EXTSYS 115   /* External Systems */
#define LABLTT_CNSPOI 115   /* Consignment POs and Invoices */
#define LABLTT_SUPPAR 116   /* Supplier-Partner */
#define LABLTT_GLROLU 116   /* Finance */
#define LABLTT_PCCL   116   /* Price Change / Clearance */
#define LABLTT_AA2BC  116   /* Attributes Available to be Copied */
#define LABLTT_ACINFO 116   /* Accounting Information */
#define LABLTT_ACOPNS 116   /* At Cost OR % of Net Sales */
#define LABLTT_ACTION 116   /* Action */
#define LABLTT_ADDINF 116   /* Address Info */
#define LABLTT_ADJAMT 116   /* Adjustment Amount */
#define LABLTT_AEDTLS 116   /* Apply Entry Details */
#define LABLTT_ALCDET 117   /* View ALC details for: */
#define LABLTT_ALVDTS 117   /* Apply License/Visa Details */
#define LABLTT_AND    117   /* and */
#define LABLTT_AORITD 117   /* Apply Order/Item Details */
#define LABLTT_APDTE  117   /* Approval Date: */
#define LABLTT_APLHTS 117   /* Apply HTS */
#define LABLTT_APPDT  117   /* Application Date: */
#define LABLTT_APPLY  117   /* Apply */
#define LABLTT_APPTTL 117   /* Apply Totals */
#define LABLTT_APSHPD 117   /* Apply Shipment Details */
#define LABLTT_APYCRT 118   /* Apply Criteria */
#define LABLTT_ASDTLS 118   /* Assessment Details */
#define LABLTT_ASNVAL 118   /* Assign Values */
#define LABLTT_ATCOST 118   /* At Cost */
#define LABLTT_ATRETL 118   /* At Retail */
#define LABLTT_BANKS  118   /* Banks */
#define LABLTT_BATSTA 118   /* Batch Status */
#define LABLTT_BENATT 118   /* Beneficiary Attributes */
#define LABLTT_BKINFO 118   /* Banking Information */
#define LABLTT_BREAK2 118   /* Break to */
#define LABLTT_CALCDB 119   /* Calculate Dates Based On */
#define LABLTT_CCAM   119   /* Cost Change Approval Method */
#define LABLTT_CGVAL2 119   /* Change Value To */
#define LABLTT_CLSD4  119   /* Closed For */
#define LABLTT_CNFLCT 119   /* Conflicts */
#define LABLTT_COLLDA 119   /* Collect Dates */
#define LABLTT_COMNOM 119   /* Component Nomination */
#define LABLTT_CONDIT 119   /* Conditions */
#define LABLTT_CONDT  119   /* Confirmation Date: */
#define LABLTT_CONINF 119   /* Contact Info */
#define LABLTT_CTGTTH 120   /* Component Target Thresholds */
#define LABLTT_CURFMT 120   /* Currency Formats */
#define LABLTT_CZCHG  120   /* Cost Zone Change */
#define LABLTT_DATES  120   /* Dates */
#define LABLTT_DCOMIN 120   /* Deal Component Information */
#define LABLTT_DECPLC 120   /* Decimal Places */
#define LABLTT_DG     120   /* Display Group */
#define LABLTT_DGD    120   /* Display Group Details */
#define LABLTT_DL2DAT 120   /* Deal to Date */
#define LABLTT_DLCDAT 120   /* Deal Component to Date */
#define LABLTT_DLTTL  121   /* Deal Total */
#define LABLTT_ENDTLS 121   /* Entry Details */
#define LABLTT_ENTOBL 121   /* Entries / Obligations */
#define LABLTT_EPDTE  121   /* Effective Date: */
#define LABLTT_ESHPDT 121   /* Earliest Ship Date: */
#define LABLTT_EXDTLS 121   /* Expense Details */
#define LABLTT_EXPDAT 121   /* Expiration Date: */
#define LABLTT_EXPHDR 121   /* Expense Header */
#define LABLTT_EXPRD  121   /* Expense Profile Detail */
#define LABLTT_EXPRH  121   /* Expense Profile Header */
#define LABLTT_FIXED  122   /* Fixed */
#define LABLTT_HTSINF 122   /* HTS Information */
#define LABLTT_IMPATT 122   /* Import Attributes */
#define LABLTT_INCAMT 122   /* Income Amount */
#define LABLTT_INUMTY 122   /* Item Number Type */
#define LABLTT_ITCOLN 122   /* Item Type: */
#define LABLTT_ITEMS  122   /* Items */
#define LABLTT_ITMSEL 122   /* Item Selection */
#define LABLTT_ITMTYP 122   /* Item Type */
#define LABLTT_LCAMTS 122   /* LC Amounts */
#define LABLTT_LEASED 123   /* Leased */
#define LABLTT_LETCC  123   /* Location Exceptions to @OH1@ Close */
#define LABLTT_LOCATT 123   /* Location Attributes */
#define LABLTT_LOCINF 123   /* Location Information */
#define LABLTT_LOCLSD 123   /* Location Closed */
#define LABLTT_LOCLST 123   /* Location List */
#define LABLTT_LSHPDT 123   /* Latest Ship Date: */
#define LABLTT_LVLCOL 123   /* Level: */
#define LABLTT_MONTH1 123   /* Month 1 */
#define LABLTT_MONTH2 123   /* Month 2 */
#define LABLTT_MONTH3 124   /* Month 3 */
#define LABLTT_MONTH4 124   /* Month 4 */
#define LABLTT_MONTH5 124   /* Month 5 */
#define LABLTT_MONTH6 124   /* Month 6 */
#define LABLTT_NETEFT 124   /* Net Effect */
#define LABLTT_NEW    124   /* New */
#define LABLTT_NEWVAL 124   /* New Value */
#define LABLTT_OBLDTL 124   /* Obligation Detail */
#define LABLTT_OLD    124   /* Old */
#define LABLTT_OPEN4  124   /* Open For */
#define LABLTT_ORVAL  125   /* Original Value */
#define LABLTT_OVRSHR 125   /* Over/Short */
#define LABLTT_PREVAL 125   /* Previous Value */
#define LABLTT_PTNRS  125   /* Partners */
#define LABLTT_REQDFT 125   /* Request Default */
#define LABLTT_RTNINV 125   /* Return From Inventory */
#define LABLTT_RXINFO 125   /* Pharmacy Information */
#define LABLTT_SCAMT  125   /* Stock Count Amount */
#define LABLTT_SCRIT  125   /* Selection Criteria */
#define LABLTT_SHIPS  125   /* Shipments */
#define LABLTT_SOHCLN 126   /* Stock on Hand: */
#define LABLTT_SPCATT 126   /* Specific Component Attribute */
#define LABLTT_STATTR 126   /* Store Attributes */
#define LABLTT_STDFLT 126   /* Store Default */
#define LABLTT_SUPHIE 126   /* Supplier Hierarchy */
#define LABLTT_TGTBGR 126   /* Target Baseline Growth % */
#define LABLTT_TRNDET 126   /* Transportation Details */
#define LABLTT_TRNFS  126   /* Transportation Finalize Selection */
#define LABLTT_TSTEPS 126   /* Timeline Steps */
#define LABLTT_TTLBGR 126   /* Total Baseline Growth % */
#define LABLTT_TVRAMT 127   /* Turnover Amount */
#define LABLTT_TVRUNS 127   /* Turnover Units */
#define LABLTT_UDAVAL 127   /* UDA Values */
#define LABLTT_UPDTE  127   /* Update */
#define LABLTT_VWCRIT 127   /* View Criteria */
#define LABLTT_WHINFO 127   /* Warehouse Information */
#define LABLTT_FIND   127   /* Find */
#define LABLTT_DIMENS 127   /* Dimensions */
#define LABLTT_SHPRCV 127   /* Shipping and Receiving */
#define LABLTT_RTMM   127   /* RTM */
#define LABLTT_ADITDI 128   /* Add Items from Distros */
#define LABLTT_SRCH   128   /* Search */
#define LABLTT_JIDS   128   /* Job Id Status */
#define LABLTT_JIRL   128   /* Job Id Retry Log */
#define LABLTT_FUNC   128   /* Functions */
#define LABLTT_OTBEOW 128   /* OTB EOW Date */
#define LABLTT_RECDTE 130   /* Receipt Date */
#define LABLTT_SHPDTE 130   /* Ship Date */
#define LABLTT_TRNDTL 130   /* Transaction Details */
#define LABLTT_DAY1   150   /* Day 1 */
#define LABLTT_DAY2   150   /* Day 2 */
#define LABLTT_DAY3   150   /* Day 3 */
#define LABLTT_DAY4   150   /* Day 4 */
#define LABLTT_DAY5   150   /* Day 5 */
#define LABLTT_DAY6   150   /* Day 6 */
#define LABLTT_DAY7   150   /* Day 7 */
#define LABLTT_TOTAL  150   /* Total */
#define LABLTT_WEEK1  150   /* Week 1 */
#define LABLTT_WEEK2  150   /* Week 2 */
#define LABLTT_WEEK3  151   /* Week 3 */
#define LABLTT_WEEK4  151   /* Week 4 */
#define LABLTT_WEEK5  151   /* Week 5 */
#define LABLTT_TLEMP  151   /* Transaction Level Employees */
#define LABLTT_ILEMP  151   /* Item Level Employees */
#define LABLTT_WFRTY  151   /* Return Type */
#define LABLTT_WFRM   151   /* Return Method */
#define LABLTT_ELC    151   /* ELC Component */
#define LABLTT_ML     152   /* Manual */
#define LABLTT_EXTTRG 152   /* External System Tran. No. Range */
#define LABLTT_FORW   170   /* Transfers for @SUH4@ Returns          (wfordrettsf) */
#define LABLTT_FORL   170   /* @SUH4@ Return */
#define LABLTT_FOOL   170   /* @SUH4@ Order No. */
#define LABLTT_OKFIN  476   /* OK / Finish */
#define LABLTT_FINISH 476   /* Finish */
#define LABLTT_WFOPT  480   /* @SUH4@ Options */
#define LABLTT_WFST   480   /* Store */
#define LABLTT_CREL   480   /* Cost Relationship */
#define LABLTT_AREL   480   /* Add Relationship */
#define LABLTT_MRANGE 480   /* Margin Range: */
#define LABLTT_OBLS   882   /* Obligations */
#define LABLTT_INTLNG 900   /* Data Integration Language */
#define LABLTT_RNDF   900   /* Rounding (Default) */
#define LABLTT_PCKGNG 900   /* Packaging */
#define LABLTT_NOMFLG 900   /* Nomination Flag */
#define LABLTT_SHPMNT 900   /* Shipment */
#define LABLTT_TSFRTV 900   /* Transfers and RTVs */
#define LABLTT_RMS    900   /* Retail Merchandising System */
#define LABLTT_PORDS  900   /* Purchase Orders */
#define LABLTT_CADJ   900   /* Cost Adjustments */
#define LABLTT_VRSN   900   /* Version */
#define LABLTT_RPM    901   /* Retail Price Management */
#define LABLTT_ERR    901   /* Errors */
#define LABLTT_DLDFIL 901   /* Download File */
#define LABLTT_DLDSTG 901   /* Download Staged */
#define LABLTT_TSFDCT 902   /* Transfer Cost Adjustment */
#define LABLTT_TSFDPT 902   /* Transfer Price Adjustment */
#define LABLTT_REDDAT 906   /* Receive Dates */
#define LABLTT_ORET   910   /* % Off Retail */
#define LABLTT_CSTM   910   /* Cost */
#define LABLTT_TEMT   910   /* Template Type */
#define LABLTT_REBATS 983   /* Rebates */
#define LABLTT_BGTHIN 983   /* Buy / Get Item Threshold Information */
#define LABLTT_HISTPD 983   /* Historical Period */
#define LABLTT_READAT 983   /* Ready By Date */
#define LABLTT_ACLS   983   /* Stock Order Auto Close */
#define LABLTT_TAX    999   /* Tax */
#define LABLTT_FSLT   999   /* Source Loc Type */
#define LABLTT_SLID   999   /* Source
Loc ID */

#define LABLTT_FRLT   999   /* Return Loc Type */
#define LABLTT_RLID   999   /* Return Loc ID */
#define LABLTT_WFOT   999   /* Order Type */
#define LABLTT_VEHINF 999   /* Vehicle Information */
#define LABLTT_SUPOOL 999   /* Supplier Pooling */

#define _LABLTT_MAX 9999


/* LACO = Standard/Average Cost */
extern const char LACO[NULL_CODE];

extern const char LACO_A     [NULL_CODE];   /* Average Cost */
extern const char LACO_S     [NULL_CODE];   /* Standard Cost */

#define LACOTT_A        1   /* Average Cost */
#define LACOTT_S        2   /* Standard Cost */

#define _LACOTT_MAX 2


/* LANG = List of non-primary languages for maintainence of translation records through spreadsheet API and UI. */
extern const char LANG[NULL_CODE];

extern const char LANG_1     [NULL_CODE];   /* English */
extern const char LANG_2     [NULL_CODE];   /* German */
extern const char LANG_3     [NULL_CODE];   /* French */
extern const char LANG_4     [NULL_CODE];   /* Spanish */
extern const char LANG_5     [NULL_CODE];   /* Japanese */
extern const char LANG_6     [NULL_CODE];   /* Korean */
extern const char LANG_7     [NULL_CODE];   /* Russian */
extern const char LANG_8     [NULL_CODE];   /* Chinese-Simplified */
extern const char LANG_9     [NULL_CODE];   /* Turkish */
extern const char LANG_10    [NULL_CODE];   /* Hungarian */
extern const char LANG_11    [NULL_CODE];   /* Chinese-Traditional */
extern const char LANG_12    [NULL_CODE];   /* Portuguese-Brazil */
extern const char LANG_15    [NULL_CODE];   /* Croatian */
extern const char LANG_18    [NULL_CODE];   /* Dutch */
extern const char LANG_20    [NULL_CODE];   /* Greek */
extern const char LANG_22    [NULL_CODE];   /* Italian */
extern const char LANG_26    [NULL_CODE];   /* Polish */
extern const char LANG_31    [NULL_CODE];   /* Swedish */

#define LANGTT_1        1   /* English */
#define LANGTT_2        2   /* German */
#define LANGTT_3        3   /* French */
#define LANGTT_4        4   /* Spanish */
#define LANGTT_5        5   /* Japanese */
#define LANGTT_6        6   /* Korean */
#define LANGTT_7        7   /* Russian */
#define LANGTT_8        8   /* Chinese-Simplified */
#define LANGTT_9        9   /* Turkish */
#define LANGTT_10      10   /* Hungarian */
#define LANGTT_11      11   /* Chinese-Traditional */
#define LANGTT_12      12   /* Portuguese-Brazil */
#define LANGTT_15      15   /* Croatian */
#define LANGTT_18      18   /* Dutch */
#define LANGTT_20      20   /* Greek */
#define LANGTT_22      22   /* Italian */
#define LANGTT_26      26   /* Polish */
#define LANGTT_31      31   /* Swedish */

#define _LANGTT_MAX 31


/* LCAC = Letter of Credit Find From Action */
extern const char LCAC[NULL_CODE];

extern const char LCAC_NEW   [NULL_CODE];   /* New */
extern const char LCAC_EDIT  [NULL_CODE];   /* Edit */
extern const char LCAC_VIEW  [NULL_CODE];   /* View */
extern const char LCAC_A     [NULL_CODE];   /* Download Application */
extern const char LCAC_M     [NULL_CODE];   /* Download Amendment */

#define LCACTT_NEW      1   /* New */
#define LCACTT_EDIT     2   /* Edit */
#define LCACTT_VIEW     3   /* View */
#define LCACTT_A        4   /* Download Application */
#define LCACTT_M        5   /* Download Amendment */

#define _LCACTT_MAX 5


/* LCAD = Letter of Credit Advice Method */
extern const char LCAD[NULL_CODE];

extern const char LCAD_F     [NULL_CODE];   /* Full Wire */
extern const char LCAD_M     [NULL_CODE];   /* Mail */
extern const char LCAD_O     [NULL_CODE];   /* Overnight */

#define LCADTT_F        1   /* Full Wire */
#define LCADTT_M        2   /* Mail */
#define LCADTT_O        3   /* Overnight */

#define _LCADTT_MAX 3


/* LCAF = Letter of Credit Amendment Field */
extern const char LCAF[NULL_CODE];

extern const char LCAF_LSD   [NULL_CODE];   /* Latest Ship Date */
extern const char LCAF_ESD   [NULL_CODE];   /* Earliest Ship Date */
extern const char LCAF_OC    [NULL_CODE];   /* Country Of Sourcing */
extern const char LCAF_TSF   [NULL_CODE];   /* Transshipment Flag */
extern const char LCAF_TFF   [NULL_CODE];   /* Transferable Flag */
extern const char LCAF_PSF   [NULL_CODE];   /* Partial Ship Flag */
extern const char LCAF_C     [NULL_CODE];   /* Cost */
extern const char LCAF_OQ    [NULL_CODE];   /* Order Quantity */
extern const char LCAF_NA    [NULL_CODE];   /* Net Amount */
extern const char LCAF_ED    [NULL_CODE];   /* Expiration Date */
extern const char LCAF_PE    [NULL_CODE];   /* Place of Expiry */
extern const char LCAF_PRT   [NULL_CODE];   /* Presentation Terms */
extern const char LCAF_ND    [NULL_CODE];   /* Negotiation Days */
extern const char LCAF_RO    [NULL_CODE];   /* Remove PO */
extern const char LCAF_AO    [NULL_CODE];   /* Add PO */
extern const char LCAF_ARQD  [NULL_CODE];   /* Add Required Doc. */
extern const char LCAF_RRQD  [NULL_CODE];   /* Remove Required Doc. */
extern const char LCAF_AI    [NULL_CODE];   /* Add Item */
extern const char LCAF_RI    [NULL_CODE];   /* Remove Item */
extern const char LCAF_TRT   [NULL_CODE];   /* Transport To */
extern const char LCAF_NC    [NULL_CODE];   /* New Comments */

#define LCAFTT_LSD      1   /* Latest Ship Date */
#define LCAFTT_ESD      2   /* Earliest Ship Date */
#define LCAFTT_OC       3   /* Country Of Sourcing */
#define LCAFTT_TSF      5   /* Transshipment Flag */
#define LCAFTT_TFF      6   /* Transferable Flag */
#define LCAFTT_PSF      7   /* Partial Ship Flag */
#define LCAFTT_C       10   /* Cost */
#define LCAFTT_OQ      11   /* Order Quantity */
#define LCAFTT_NA      12   /* Net Amount */
#define LCAFTT_ED      13   /* Expiration Date */
#define LCAFTT_PE      14   /* Place of Expiry */
#define LCAFTT_PRT     15   /* Presentation Terms */
#define LCAFTT_ND      16   /* Negotiation Days */
#define LCAFTT_RO      17   /* Remove PO */
#define LCAFTT_AO      18   /* Add PO */
#define LCAFTT_ARQD    19   /* Add Required Doc. */
#define LCAFTT_RRQD    20   /* Remove Required Doc. */
#define LCAFTT_AI      21   /* Add Item */
#define LCAFTT_RI      22   /* Remove Item */
#define LCAFTT_TRT     23   /* Transport To */
#define LCAFTT_NC      24   /* New Comments */

#define _LCAFTT_MAX 24


/* LCAM = Cost Component Amount Type */
extern const char LCAM[NULL_CODE];

extern const char LCAM_C     [NULL_CODE];   /* Currency */
extern const char LCAM_D     [NULL_CODE];   /* Duty */
extern const char LCAM_P     [NULL_CODE];   /* Percent */

#define LCAMTT_C        1   /* Currency */
#define LCAMTT_D        2   /* Duty */
#define LCAMTT_P        3   /* Percent */

#define _LCAMTT_MAX 3


/* LCAS = Letter of Credit Amendment Status */
extern const char LCAS[NULL_CODE];

extern const char LCAS_N     [NULL_CODE];   /* New */
extern const char LCAS_A     [NULL_CODE];   /* Accept */
extern const char LCAS_H     [NULL_CODE];   /* Hold */
extern const char LCAS_D     [NULL_CODE];   /* Downloaded */

#define LCASTT_N        1   /* New */
#define LCASTT_A        2   /* Accept */
#define LCASTT_H        3   /* Hold */
#define LCASTT_D        4   /* Downloaded */

#define _LCASTT_MAX 4


/* LCAT = Letter of Credit Amount Type */
extern const char LCAT[NULL_CODE];

extern const char LCAT_E     [NULL_CODE];   /* Exact */
extern const char LCAT_A     [NULL_CODE];   /* Approximately */

#define LCATTT_E        1   /* Exact */
#define LCATTT_A        2   /* Approximately */

#define _LCATTT_MAX 2


/* LCDA = Letter of Credit Drafts At */
extern const char LCDA[NULL_CODE];

extern const char LCDA_01    [NULL_CODE];   /* At Sight */
extern const char LCDA_02    [NULL_CODE];   /* 30 Days */
extern const char LCDA_03    [NULL_CODE];   /* 60 Days */

#define LCDATT_01       1   /* At Sight */
#define LCDATT_02       2   /* 30 Days */
#define LCDATT_03       3   /* 60 Days */

#define _LCDATT_MAX 3


/* LCDS = Letter of Credit Download Selection */
extern const char LCDS[NULL_CODE];


/* LCFT = Letter of Credit Issuance */
extern const char LCFT[NULL_CODE];

extern const char LCFT_S     [NULL_CODE];   /* Short */
extern const char LCFT_L     [NULL_CODE];   /* Long */

#define LCFTTT_S        1   /* Short */
#define LCFTTT_L        2   /* Long */

#define _LCFTTT_MAX 2


/* LCIL = Letter of Credit Item Levels */
extern const char LCIL[NULL_CODE];

extern const char LCIL_S     [NULL_CODE];   /* Item Parent */
extern const char LCIL_SC    [NULL_CODE];   /* Item Parent/Differentiator 1 */
extern const char LCIL_SS    [NULL_CODE];   /* Item Parent/Differentiator 2 */
extern const char LCIL_SKU   [NULL_CODE];   /* Item */

#define LCILTT_S        1   /* Item Parent */
#define LCILTT_SC       2   /* Item Parent/Differentiator 1 */
#define LCILTT_SS       3   /* Item Parent/Differentiator 2 */
#define LCILTT_SKU      4   /* Item */

#define _LCILTT_MAX 4


/* LCIS = Letter of Credit Issuance */
extern const char LCIS[NULL_CODE];

extern const char LCIS_C     [NULL_CODE];   /* Cable */
extern const char LCIS_T     [NULL_CODE];   /* Telex */

#define LCISTT_C        1   /* Cable */
#define LCISTT_T        2   /* Telex */

#define _LCISTT_MAX 2


/* LCLS = Location Classification Method */
extern const char LCLS[NULL_CODE];

extern const char LCLS_S     [NULL_CODE];   /* Sales History */
extern const char LCLS_H     [NULL_CODE];   /* Shrinkage History */
extern const char LCLS_R     [NULL_CODE];   /* Retail Price */

#define LCLSTT_S        1   /* Sales History */
#define LCLSTT_H        2   /* Shrinkage History */
#define LCLSTT_R        3   /* Retail Price */

#define _LCLSTT_MAX 3


/* LCPE = Letter of Credit Place of Expiry */
extern const char LCPE[NULL_CODE];

extern const char LCPE_01    [NULL_CODE];   /* Issuing Bank */
extern const char LCPE_02    [NULL_CODE];   /* Advising Bank */
extern const char LCPE_03    [NULL_CODE];   /* Miami */
extern const char LCPE_04    [NULL_CODE];   /* New York */
extern const char LCPE_05    [NULL_CODE];   /* Los Angeles */

#define LCPETT_01       1   /* Issuing Bank */
#define LCPETT_02       2   /* Advising Bank */
#define LCPETT_03       3   /* Miami */
#define LCPETT_04       4   /* New York */
#define LCPETT_05       5   /* Los Angeles */

#define _LCPETT_MAX 5


/* LCPT = Letter of Credit Presentation Terms */
extern const char LCPT[NULL_CODE];

extern const char LCPT_P     [NULL_CODE];   /* By Payment */
extern const char LCPT_A     [NULL_CODE];   /* By Acceptance */
extern const char LCPT_N     [NULL_CODE];   /* By Negotiation */

#define LCPTTT_P        1   /* By Payment */
#define LCPTTT_A        2   /* By Acceptance */
#define LCPTTT_N        3   /* By Negotiation */

#define _LCPTTT_MAX 3


/* LCRL = LC Bank Applicable Rules */
extern const char LCRL[NULL_CODE];

extern const char LCRL_EUCP  [NULL_CODE];   /* EUCP Latest Version */
extern const char LCRL_EURR  [NULL_CODE];   /* EUCPURR Latest Version */
extern const char LCRL_ISP   [NULL_CODE];   /* ISP Latest Version */
extern const char LCRL_OTHR  [NULL_CODE];   /* OTHR */
extern const char LCRL_UCP   [NULL_CODE];   /* UCP Latest Version */
extern const char LCRL_UCPURR[NULL_CODE];   /* UCPURR Latest Version */

#define LCRLTT_EUCP     1   /* EUCP Latest Version */
#define LCRLTT_EURR     2   /* EUCPURR Latest Version */
#define LCRLTT_ISP      3   /* ISP Latest Version */
#define LCRLTT_OTHR     4   /* OTHR */
#define LCRLTT_UCP      5   /* UCP Latest Version */
#define LCRLTT_UCPURR   6   /* UCPURR Latest Version */

#define _LCRLTT_MAX 6


/* LCSP = Letter of Credit Specification */
extern const char LCSP[NULL_CODE];

extern const char LCSP_M     [NULL_CODE];   /* Maximum */
extern const char LCSP_U     [NULL_CODE];   /* Up TO */
extern const char LCSP_N     [NULL_CODE];   /* Not Exceeding */

#define LCSPTT_M        1   /* Maximum */
#define LCSPTT_U        2   /* Up TO */
#define LCSPTT_N        3   /* Not Exceeding */

#define _LCSPTT_MAX 3


/* LCST = Letter of Credit Status */
extern const char LCST[NULL_CODE];

extern const char LCST_W     [NULL_CODE];   /* Worksheet */
extern const char LCST_S     [NULL_CODE];   /* Submitted */
extern const char LCST_A     [NULL_CODE];   /* Approved */
extern const char LCST_E     [NULL_CODE];   /* Extracted */
extern const char LCST_C     [NULL_CODE];   /* Confirmed */
extern const char LCST_L     [NULL_CODE];   /* Closed */

#define LCSTTT_W        1   /* Worksheet */
#define LCSTTT_S        2   /* Submitted */
#define LCSTTT_A        3   /* Approved */
#define LCSTTT_E        4   /* Extracted */
#define LCSTTT_C        5   /* Confirmed */
#define LCSTTT_L        6   /* Closed */

#define _LCSTTT_MAX 6


/* LCTC = Letter of Credit Transaction Code */
extern const char LCTC[NULL_CODE];

extern const char LCTC_A     [NULL_CODE];   /* Amendment */
extern const char LCTC_B     [NULL_CODE];   /* Bank Charge */
extern const char LCTC_D     [NULL_CODE];   /* Drawdown */
extern const char LCTC_L     [NULL_CODE];   /* Letter of Credit */

#define LCTCTT_A        1   /* Amendment */
#define LCTCTT_B        2   /* Bank Charge */
#define LCTCTT_D        3   /* Drawdown */
#define LCTCTT_L        4   /* Letter of Credit */

#define _LCTCTT_MAX 4


/* LCTP = Letter of Credit Type */
extern const char LCTP[NULL_CODE];

extern const char LCTP_N     [NULL_CODE];   /* Normal */
extern const char LCTP_M     [NULL_CODE];   /* Master */
extern const char LCTP_R     [NULL_CODE];   /* Revolving */
extern const char LCTP_O     [NULL_CODE];   /* Open */

#define LCTPTT_N        1   /* Normal */
#define LCTPTT_M        2   /* Master */
#define LCTPTT_R        3   /* Revolving */
#define LCTPTT_O        4   /* Open */

#define _LCTPTT_MAX 4


/* LCTY = Letter of Credit Type */
extern const char LCTY[NULL_CODE];

extern const char LCTY_N     [NULL_CODE];   /* Normal */
extern const char LCTY_M     [NULL_CODE];   /* Master */
extern const char LCTY_R     [NULL_CODE];   /* Revolving */
extern const char LCTY_O     [NULL_CODE];   /* Open */

#define LCTYTT_N        1   /* Normal */
#define LCTYTT_M        2   /* Master */
#define LCTYTT_R        3   /* Revolving */
#define LCTYTT_O        4   /* Open */

#define _LCTYTT_MAX 4


/* LGRP = Item Retail Group Type */
extern const char LGRP[NULL_CODE];

extern const char LGRP_AS    [NULL_CODE];   /* All Stores */
extern const char LGRP_Cl    [NULL_CODE];   /* Store Class */
extern const char LGRP_D     [NULL_CODE];   /* @OH5@ */
extern const char LGRP_DW    [NULL_CODE];   /* Default WH */
extern const char LGRP_LL    [NULL_CODE];   /* Location List */
extern const char LGRP_R     [NULL_CODE];   /* @OH4@ */
extern const char LGRP_S     [NULL_CODE];   /* Store */
extern const char LGRP_St    [NULL_CODE];   /* State */
extern const char LGRP_T     [NULL_CODE];   /* Trait */
extern const char LGRP_TZ    [NULL_CODE];   /* Transfer Zone */

#define LGRPTT_AS       1   /* All Stores */
#define LGRPTT_Cl       2   /* Store Class */
#define LGRPTT_D        3   /* @OH5@ */
#define LGRPTT_DW       4   /* Default WH */
#define LGRPTT_LL       5   /* Location List */
#define LGRPTT_R        8   /* @OH4@ */
#define LGRPTT_S        9   /* Store */
#define LGRPTT_St      10   /* State */
#define LGRPTT_T       11   /* Trait */
#define LGRPTT_TZ      12   /* Transfer Zone */

#define _LGRPTT_MAX 12


/* LIVI = License/Visa Indicator */
extern const char LIVI[NULL_CODE];

extern const char LIVI_L     [NULL_CODE];   /* License */
extern const char LIVI_V     [NULL_CODE];   /* Visa */

#define LIVITT_L        1   /* License */
#define LIVITT_V        2   /* Visa */

#define _LIVITT_MAX 2


/* LLAB = Language Action */
extern const char LLAB[NULL_CODE];

extern const char LLAB_NEW   [NULL_CODE];   /* New */
extern const char LLAB_VIEW  [NULL_CODE];   /* View */
extern const char LLAB_EDIT  [NULL_CODE];   /* Edit */
extern const char LLAB_ED_ALL[NULL_CODE];   /* Edit All */

#define LLABTT_NEW      1   /* New */
#define LLABTT_VIEW     2   /* View */
#define LLABTT_EDIT     3   /* Edit */
#define LLABTT_ED_ALL   4   /* Edit All */

#define _LLABTT_MAX 4


/* LLBR = Batch Rebuild */
extern const char LLBR[NULL_CODE];

extern const char LLBR_Y     [NULL_CODE];   /* Yes */
extern const char LLBR_N     [NULL_CODE];   /* No */

#define LLBRTT_Y        1   /* Yes */
#define LLBRTT_N        2   /* No */

#define _LLBRTT_MAX 2


/* LLCM = Location List Comparison */
extern const char LLCM[NULL_CODE];

extern const char LLCM_EQ    [NULL_CODE];   /* = */
extern const char LLCM_NE    [NULL_CODE];   /* != */
extern const char LLCM_GT    [NULL_CODE];   /* > */
extern const char LLCM_LT    [NULL_CODE];   /* < */
extern const char LLCM_GE    [NULL_CODE];   /* >= */
extern const char LLCM_LE    [NULL_CODE];   /* <= */

#define LLCMTT_EQ       1   /* = */
#define LLCMTT_NE       2   /* != */
#define LLCMTT_GT       3   /* > */
#define LLCMTT_LT       4   /* < */
#define LLCMTT_GE       5   /* >= */
#define LLCMTT_LE       6   /* <= */

#define _LLCMTT_MAX 6


/* LLDA = Location List Detail Actions */
extern const char LLDA[NULL_CODE];

extern const char LLDA_A     [NULL_CODE];   /* Add */
extern const char LLDA_D     [NULL_CODE];   /* Delete */

#define LLDATT_A        1   /* Add */
#define LLDATT_D        2   /* Delete */

#define _LLDATT_MAX 2


/* LLLB = Location List Labels */
extern const char LLLB[NULL_CODE];

extern const char LLLB_DESC  [NULL_CODE];   /* Copied From: */

#define LLLBTT_DESC     1   /* Copied From: */

#define _LLLBTT_MAX 1


/* LLLO = Location List Logic Operation */
extern const char LLLO[NULL_CODE];

extern const char LLLO_AND   [NULL_CODE];   /* AND */
extern const char LLLO_OR    [NULL_CODE];   /* OR */

#define LLLOTT_AND      1   /* AND */
#define LLLOTT_OR       2   /* OR */

#define _LLLOTT_MAX 2


/* LLSC = Location List Source Field */
extern const char LLSC[NULL_CODE];

extern const char LLSC_RDF   [NULL_CODE];   /* Demand Forecasting */
extern const char LLSC_RA    [NULL_CODE];   /* Retail Analytics */
extern const char LLSC_RMS   [NULL_CODE];   /* Merchandising System */

#define LLSCTT_RDF      1   /* Demand Forecasting */
#define LLSCTT_RA       2   /* Retail Analytics */
#define LLSCTT_RMS      3   /* Merchandising System */

#define _LLSCTT_MAX 3


/* LLSD = Static\Dynamic */
extern const char LLSD[NULL_CODE];

extern const char LLSD_Y     [NULL_CODE];   /* Static */
extern const char LLSD_N     [NULL_CODE];   /* Dynamic */

#define LLSDTT_Y        1   /* Static */
#define LLSDTT_N        2   /* Dynamic */

#define _LLSDTT_MAX 2


/* LLST = Location List Store Criteria */
extern const char LLST[NULL_CODE];

extern const char LLST_DT    [NULL_CODE];   /* @OH5@ */
extern const char LLST_CZ    [NULL_CODE];   /* Cost Zone */
extern const char LLST_CTY   [NULL_CODE];   /* Country ID */
extern const char LLST_CUR   [NULL_CODE];   /* Currency */
extern const char LLST_DW    [NULL_CODE];   /* Default WH */
extern const char LLST_LNG   [NULL_CODE];   /* Language */
extern const char LLST_LT    [NULL_CODE];   /* Location Traits */
extern const char LLST_SA    [NULL_CODE];   /* Selling Area */
extern const char LLST_ST    [NULL_CODE];   /* State */
extern const char LLST_AD    [NULL_CODE];   /* Store Acquired Date */
extern const char LLST_SC    [NULL_CODE];   /* Store Class */
extern const char LLST_FL    [NULL_CODE];   /* Store Forecast Level for Week */
extern const char LLST_SF    [NULL_CODE];   /* Store Format */
extern const char LLST_SG    [NULL_CODE];   /* Store Grade */
extern const char LLST_SN    [NULL_CODE];   /* Store Number */
extern const char LLST_OD    [NULL_CODE];   /* Store Open Date */
extern const char LLST_RD    [NULL_CODE];   /* Store Remodel Date */
extern const char LLST_SL    [NULL_CODE];   /* Store Sales Level for Week */
extern const char LLST_TA    [NULL_CODE];   /* Total Area */
extern const char LLST_TE    [NULL_CODE];   /* Transfer Entity ID */
extern const char LLST_TSF   [NULL_CODE];   /* Transfer Zone */
extern const char LLST_VR    [NULL_CODE];   /* VAT Region */
extern const char LLST_ZC    [NULL_CODE];   /* Zip Code */
extern const char LLST_WFC   [NULL_CODE];   /* @SUH4@ Customer ID */

#define LLSTTT_DT       1   /* @OH5@ */
#define LLSTTT_CZ       2   /* Cost Zone */
#define LLSTTT_CTY      3   /* Country ID */
#define LLSTTT_CUR      4   /* Currency */
#define LLSTTT_DW       5   /* Default WH */
#define LLSTTT_LNG      6   /* Language */
#define LLSTTT_LT       7   /* Location Traits */
#define LLSTTT_SA      10   /* Selling Area */
#define LLSTTT_ST      11   /* State */
#define LLSTTT_AD      12   /* Store Acquired Date */
#define LLSTTT_SC      13   /* Store Class */
#define LLSTTT_FL      14   /* Store Forecast Level for Week */
#define LLSTTT_SF      15   /* Store Format */
#define LLSTTT_SG      16   /* Store Grade */
#define LLSTTT_SN      17   /* Store Number */
#define LLSTTT_OD      18   /* Store Open Date */
#define LLSTTT_RD      19   /* Store Remodel Date */
#define LLSTTT_SL      20   /* Store Sales Level for Week */
#define LLSTTT_TA      21   /* Total Area */
#define LLSTTT_TE      22   /* Transfer Entity ID */
#define LLSTTT_TSF     23   /* Transfer Zone */
#define LLSTTT_VR      24   /* VAT Region */
#define LLSTTT_ZC      25   /* Zip Code */
#define LLSTTT_WFC     26   /* @SUH4@ Customer ID */

#define _LLSTTT_MAX 26


/* LLWH = Location List Warehouse Criteria */
extern const char LLWH[NULL_CODE];

extern const char LLWH_CTY   [NULL_CODE];   /* Country ID */
extern const char LLWH_CUR   [NULL_CODE];   /* Currency */
extern const char LLWH_PWN   [NULL_CODE];   /* Physical WH Number */
extern const char LLWH_ST    [NULL_CODE];   /* State */
extern const char LLWH_TE    [NULL_CODE];   /* Transfer Entity ID */
extern const char LLWH_VR    [NULL_CODE];   /* VAT Region */
extern const char LLWH_WN    [NULL_CODE];   /* WH Number */
extern const char LLWH_ZC    [NULL_CODE];   /* Zip Code */

#define LLWHTT_CTY      1   /* Country ID */
#define LLWHTT_CUR      2   /* Currency */
#define LLWHTT_PWN      3   /* Physical WH Number */
#define LLWHTT_ST       4   /* State */
#define LLWHTT_TE       5   /* Transfer Entity ID */
#define LLWHTT_VR       6   /* VAT Region */
#define LLWHTT_WN       7   /* WH Number */
#define LLWHTT_ZC       8   /* Zip Code */

#define _LLWHTT_MAX 8


/* LOC = Location */
extern const char LOC[NULL_CODE];

extern const char LOC_S     [NULL_CODE];   /* Store */
extern const char LOC_W     [NULL_CODE];   /* Warehouse */
extern const char LOC_A     [NULL_CODE];   /* All Locations */

#define LOCTT_S        1   /* Store */
#define LOCTT_W        2   /* Warehouse */
#define LOCTT_A        3   /* All Locations */

#define _LOCTT_MAX 3


/* LOC2 = Locations (All) */
extern const char LOC2[NULL_CODE];

extern const char LOC2_S     [NULL_CODE];   /* Store */
extern const char LOC2_W     [NULL_CODE];   /* Warehouse */
extern const char LOC2_A     [NULL_CODE];   /* All Stores */
extern const char LOC2_L     [NULL_CODE];   /* All Warehouses */

#define LOC2TT_S        1   /* Store */
#define LOC2TT_W        2   /* Warehouse */
#define LOC2TT_A        3   /* All Stores */
#define LOC2TT_L        4   /* All Warehouses */

#define _LOC2TT_MAX 4


/* LOC3 = Location Types */
extern const char LOC3[NULL_CODE];

extern const char LOC3_S     [NULL_CODE];   /* Store */
extern const char LOC3_W     [NULL_CODE];   /* Warehouse */
extern const char LOC3_L     [NULL_CODE];   /* Location List */

#define LOC3TT_S        1   /* Store */
#define LOC3TT_W        2   /* Warehouse */
#define LOC3TT_L        3   /* Location List */

#define _LOC3TT_MAX 3


/* LOC4 = Location Types - Store & Phys Warehouse */
extern const char LOC4[NULL_CODE];

extern const char LOC4_S     [NULL_CODE];   /* Store */
extern const char LOC4_W     [NULL_CODE];   /* Physical Warehouse */

#define LOC4TT_S        1   /* Store */
#define LOC4TT_W        2   /* Physical Warehouse */

#define _LOC4TT_MAX 2


/* LOC9 = Locations (ST/WH) */
extern const char LOC9[NULL_CODE];

extern const char LOC9_S     [NULL_CODE];   /* Store */
extern const char LOC9_W     [NULL_CODE];   /* Warehouse */

#define LOC9TT_S        1   /* Store */
#define LOC9TT_W        2   /* Warehouse */

#define _LOC9TT_MAX 2


/* LOCS = Locations - store, wh, All stores. */
extern const char LOCS[NULL_CODE];

extern const char LOCS_S     [NULL_CODE];   /* Store */
extern const char LOCS_W     [NULL_CODE];   /* Warehouse */
extern const char LOCS_A     [NULL_CODE];   /* All Stores */

#define LOCSTT_S        1   /* Store */
#define LOCSTT_W        2   /* Warehouse */
#define LOCSTT_A        3   /* All Stores */

#define _LOCSTT_MAX 3


/* LOCT = Location Type */
extern const char LOCT[NULL_CODE];

extern const char LOCT_DP    [NULL_CODE];   /* Discharge Port */
extern const char LOCT_LP    [NULL_CODE];   /* Lading Port */
extern const char LOCT_EP    [NULL_CODE];   /* Entry Port */
extern const char LOCT_RC    [NULL_CODE];   /* Return Center */
extern const char LOCT_BT    [NULL_CODE];   /* Bill To Location */
extern const char LOCT_LL    [NULL_CODE];   /* LC To Location */
extern const char LOCT_RL    [NULL_CODE];   /* Routing Location */
extern const char LOCT_CZ    [NULL_CODE];   /* Clearing Zone */

#define LOCTTT_DP       1   /* Discharge Port */
#define LOCTTT_LP       2   /* Lading Port */
#define LOCTTT_EP       3   /* Entry Port */
#define LOCTTT_RC       4   /* Return Center */
#define LOCTTT_BT       5   /* Bill To Location */
#define LOCTTT_LL       6   /* LC To Location */
#define LOCTTT_RL       7   /* Routing Location */
#define LOCTTT_CZ       8   /* Clearing Zone */

#define _LOCTTT_MAX 8


/* LOFT = Template Type */
extern const char LOFT[NULL_CODE];

extern const char LOFT_M     [NULL_CODE];   /* Margin then Up Charges */
extern const char LOFT_U     [NULL_CODE];   /* Up Charges then Margin */
extern const char LOFT_R     [NULL_CODE];   /* Percent Off Retail */
extern const char LOFT_C     [NULL_CODE];   /* Fixed Cost */

#define LOFTTT_M        1   /* Margin then Up Charges */
#define LOFTTT_U        2   /* Up Charges then Margin */
#define LOFTTT_R        3   /* Percent Off Retail */
#define LOFTTT_C        4   /* Fixed Cost */

#define _LOFTTT_MAX 4


/* LOHR = Location Hierarchy */
extern const char LOHR[NULL_CODE];

extern const char LOHR_C     [NULL_CODE];   /* @OH2@ */
extern const char LOHR_A     [NULL_CODE];   /* @OH3@ */
extern const char LOHR_R     [NULL_CODE];   /* @OH4@ */
extern const char LOHR_D     [NULL_CODE];   /* @OH5@ */
extern const char LOHR_S     [NULL_CODE];   /* Store */
extern const char LOHR_W     [NULL_CODE];   /* Warehouse */
extern const char LOHR_L     [NULL_CODE];   /* Location List */

#define LOHRTT_C        1   /* @OH2@ */
#define LOHRTT_A        2   /* @OH3@ */
#define LOHRTT_R        3   /* @OH4@ */
#define LOHRTT_D        4   /* @OH5@ */
#define LOHRTT_S        5   /* Store */
#define LOHRTT_W        6   /* Warehouse */
#define LOHRTT_L        7   /* Location List */

#define _LOHRTT_MAX 7


/* LORM = Return Method Types */
extern const char LORM[NULL_CODE];

extern const char LORM_R     [NULL_CODE];   /* Return to Location */
extern const char LORM_D     [NULL_CODE];   /* Destroy on Site */

#define LORMTT_R        1   /* Return to Location */
#define LORMTT_D        2   /* Destroy on Site */

#define _LORMTT_MAX 2


/* LOTP = Location Type */
extern const char LOTP[NULL_CODE];

extern const char LOTP_S     [NULL_CODE];   /* Store */
extern const char LOTP_W     [NULL_CODE];   /* Warehouse */

#define LOTPTT_S        1   /* Store */
#define LOTPTT_W        2   /* Warehouse */

#define _LOTPTT_MAX 2


/* LPOS = Transportation Load Position */
extern const char LPOS[NULL_CODE];

extern const char LPOS_M     [NULL_CODE];   /* Middle of the Container */
extern const char LPOS_N     [NULL_CODE];   /* Nose of the Container */
extern const char LPOS_R     [NULL_CODE];   /* Rear of the Container */
extern const char LPOS_T     [NULL_CODE];   /* Tail of the Container */

#define LPOSTT_M        1   /* Middle of the Container */
#define LPOSTT_N        2   /* Nose of the Container */
#define LPOSTT_R        3   /* Rear of the Container */
#define LPOSTT_T        4   /* Tail of the Container */

#define _LPOSTT_MAX 4


/* LSEC = Location Security Areas */
extern const char LSEC[NULL_CODE];

extern const char LSEC_LALL  [NULL_CODE];   /* All */
extern const char LSEC_LPRM  [NULL_CODE];   /* Promotion */
extern const char LSEC_LTXFTO[NULL_CODE];   /* Transfers to */
extern const char LSEC_LTXFRM[NULL_CODE];   /* Transfers from */
extern const char LSEC_LALCTO[NULL_CODE];   /* Allocations to */
extern const char LSEC_LALCFR[NULL_CODE];   /* Allocations from */
extern const char LSEC_LSHP  [NULL_CODE];   /* Shipments */
extern const char LSEC_LPO   [NULL_CODE];   /* Orders */
extern const char LSEC_LSTK  [NULL_CODE];   /* Stock Counts */
extern const char LSEC_LTRQ  [NULL_CODE];   /* Ticket Requests */
extern const char LSEC_LIVJ  [NULL_CODE];   /* Inventory Adjustments */
extern const char LSEC_LRTV  [NULL_CODE];   /* RTVs */
extern const char LSEC_LSTR  [NULL_CODE];   /* Store */
extern const char LSEC_LSTDY [NULL_CODE];   /* Sales Audit Store Day */
extern const char LSEC_LSTACH[NULL_CODE];   /* Sales Audit Store ACH */
extern const char LSEC_LSFM  [NULL_CODE];   /* Site Fuel Management */

#define LSECTT_LALL     1   /* All */
#define LSECTT_LPRM     2   /* Promotion */
#define LSECTT_LTXFTO   3   /* Transfers to */
#define LSECTT_LTXFRM   4   /* Transfers from */
#define LSECTT_LALCTO   5   /* Allocations to */
#define LSECTT_LALCFR   6   /* Allocations from */
#define LSECTT_LSHP     7   /* Shipments */
#define LSECTT_LPO      8   /* Orders */
#define LSECTT_LSTK     9   /* Stock Counts */
#define LSECTT_LTRQ    10   /* Ticket Requests */
#define LSECTT_LIVJ    11   /* Inventory Adjustments */
#define LSECTT_LRTV    12   /* RTVs */
#define LSECTT_LSTR    13   /* Store */
#define LSECTT_LSTDY   14   /* Sales Audit Store Day */
#define LSECTT_LSTACH  15   /* Sales Audit Store ACH */
#define LSECTT_LSFM    16   /* Site Fuel Management */

#define _LSECTT_MAX 16


/* LSUC = Lead Time Sales Calc Method for Prom Imp */
extern const char LSUC[NULL_CODE];

extern const char LSUC_T     [NULL_CODE];   /* Turns */
extern const char LSUC_S     [NULL_CODE];   /* Sell-through */
extern const char LSUC_F     [NULL_CODE];   /* Forecasted Sales */
extern const char LSUC_P     [NULL_CODE];   /* Percent of Current SOH */

#define LSUCTT_T        1   /* Turns */
#define LSUCTT_S        2   /* Sell-through */
#define LSUCTT_F        3   /* Forecasted Sales */
#define LSUCTT_P        4   /* Percent of Current SOH */

#define _LSUCTT_MAX 4


/* LTLA = Store, WH, All, Int. and Ext. Fins */
extern const char LTLA[NULL_CODE];

extern const char LTLA_AS    [NULL_CODE];   /* All Stores */
extern const char LTLA_S     [NULL_CODE];   /* Store */
extern const char LTLA_AW    [NULL_CODE];   /* All Warehouses */
extern const char LTLA_W     [NULL_CODE];   /* Warehouse */
extern const char LTLA_AI    [NULL_CODE];   /* All Internal Finishers */
extern const char LTLA_I     [NULL_CODE];   /* Internal Finisher */
extern const char LTLA_AE    [NULL_CODE];   /* All External Finishers */
extern const char LTLA_E     [NULL_CODE];   /* External Finisher */

#define LTLATT_AS       1   /* All Stores */
#define LTLATT_S        2   /* Store */
#define LTLATT_AW       3   /* All Warehouses */
#define LTLATT_W        4   /* Warehouse */
#define LTLATT_AI       5   /* All Internal Finishers */
#define LTLATT_I        6   /* Internal Finisher */
#define LTLATT_AE       7   /* All External Finishers */
#define LTLATT_E        8   /* External Finisher */

#define _LTLATT_MAX 8


/* LTLE = Store, WH, Loc List and Ext. Finishers */
extern const char LTLE[NULL_CODE];

extern const char LTLE_S     [NULL_CODE];   /* Store */
extern const char LTLE_W     [NULL_CODE];   /* Warehouse */
extern const char LTLE_E     [NULL_CODE];   /* External Finisher */
extern const char LTLE_L     [NULL_CODE];   /* Location List */

#define LTLETT_S        1   /* Store */
#define LTLETT_W        2   /* Warehouse */
#define LTLETT_E        3   /* External Finisher */
#define LTLETT_L        4   /* Location List */

#define _LTLETT_MAX 4


/* LTLF = Store, WH, Loc List, Int. and Ext. Fins */
extern const char LTLF[NULL_CODE];

extern const char LTLF_S     [NULL_CODE];   /* Store */
extern const char LTLF_W     [NULL_CODE];   /* Warehouse */
extern const char LTLF_I     [NULL_CODE];   /* Internal Finisher */
extern const char LTLF_E     [NULL_CODE];   /* External Finisher */
extern const char LTLF_LL    [NULL_CODE];   /* Location List */

#define LTLFTT_S        1   /* Store */
#define LTLFTT_W        2   /* Warehouse */
#define LTLFTT_I        3   /* Internal Finisher */
#define LTLFTT_E        4   /* External Finisher */
#define LTLFTT_LL       5   /* Location List */

#define _LTLFTT_MAX 5


/* LTPE = Store, WH and External Finishers */
extern const char LTPE[NULL_CODE];

extern const char LTPE_S     [NULL_CODE];   /* Store */
extern const char LTPE_W     [NULL_CODE];   /* Warehouse */
extern const char LTPE_E     [NULL_CODE];   /* External Finisher */

#define LTPETT_S        1   /* Store */
#define LTPETT_W        2   /* Warehouse */
#define LTPETT_E        3   /* External Finisher */

#define _LTPETT_MAX 3


/* LTPF = Store, WH and Int. and Ext. Finishers */
extern const char LTPF[NULL_CODE];

extern const char LTPF_S     [NULL_CODE];   /* Store */
extern const char LTPF_W     [NULL_CODE];   /* Warehouse */
extern const char LTPF_I     [NULL_CODE];   /* Internal Finisher */
extern const char LTPF_E     [NULL_CODE];   /* External Finisher */

#define LTPFTT_S        1   /* Store */
#define LTPFTT_W        2   /* Warehouse */
#define LTPFTT_I        3   /* Internal Finisher */
#define LTPFTT_E        4   /* External Finisher */

#define _LTPFTT_MAX 4


/* LTPI = Store, WH, Int. Fins */
extern const char LTPI[NULL_CODE];

extern const char LTPI_S     [NULL_CODE];   /* Store */
extern const char LTPI_W     [NULL_CODE];   /* Warehouse */
extern const char LTPI_I     [NULL_CODE];   /* Internal Finisher */

#define LTPITT_S        1   /* Store */
#define LTPITT_W        2   /* Warehouse */
#define LTPITT_I        3   /* Internal Finisher */

#define _LTPITT_MAX 3


/* LTPM = Store, WH , External Finishers and SOB */
extern const char LTPM[NULL_CODE];

extern const char LTPM_S     [NULL_CODE];   /* Store */
extern const char LTPM_W     [NULL_CODE];   /* Warehouse */
extern const char LTPM_E     [NULL_CODE];   /* External Finisher */
extern const char LTPM_M     [NULL_CODE];   /* Set of Books */

#define LTPMTT_S        1   /* Store */
#define LTPMTT_W        2   /* Warehouse */
#define LTPMTT_E        3   /* External Finisher */
#define LTPMTT_M        4   /* Set of Books */

#define _LTPMTT_MAX 4


/* LTSE = Store, WH, all stores, Ext Finishers */
extern const char LTSE[NULL_CODE];


/* LTSF = Store, WH, all Stores, Ext & Int Finish */
extern const char LTSF[NULL_CODE];


/* MALD = Mall Editor Titles */
extern const char MALD[NULL_CODE];

extern const char MALD_MADESC[NULL_CODE];   /* Mall Description */

#define MALDTT_MADESC   1   /* Mall Description */

#define _MALDTT_MAX 1


/* MCAC = MCLOCN type list box */
extern const char MCAC[NULL_CODE];

extern const char MCAC_S     [NULL_CODE];   /* Store */
extern const char MCAC_C     [NULL_CODE];   /* Store Class */
extern const char MCAC_D     [NULL_CODE];   /* @OH5@ */
extern const char MCAC_R     [NULL_CODE];   /* @OH4@ */
extern const char MCAC_T     [NULL_CODE];   /* Transfer Zone */
extern const char MCAC_L     [NULL_CODE];   /* Location Trait */
extern const char MCAC_Z     [NULL_CODE];   /* Zone Group */
extern const char MCAC_V     [NULL_CODE];   /* Active Stores */
extern const char MCAC_LL    [NULL_CODE];   /* Location List */

#define MCACTT_S        1   /* Store */
#define MCACTT_C        2   /* Store Class */
#define MCACTT_D        3   /* @OH5@ */
#define MCACTT_R        4   /* @OH4@ */
#define MCACTT_T        6   /* Transfer Zone */
#define MCACTT_L        7   /* Location Trait */
#define MCACTT_Z        8   /* Zone Group */
#define MCACTT_V        9   /* Active Stores */
#define MCACTT_LL      11   /* Location List */

#define _MCACTT_MAX 11


/* MCLB = MCLOCN form labels */
extern const char MCLB[NULL_CODE];

extern const char MCLB_I     [NULL_CODE];   /* Item List */
extern const char MCLB_S     [NULL_CODE];   /* Store */
extern const char MCLB_SKU   [NULL_CODE];   /* Item */
extern const char MCLB_SL    [NULL_CODE];   /* Store List */
extern const char MCLB_SS    [NULL_CODE];   /* Store Selection */
extern const char MCLB_ST    [NULL_CODE];   /* Item Parent */
extern const char MCLB_W     [NULL_CODE];   /* Wh */
extern const char MCLB_WL    [NULL_CODE];   /* Warehouse List */
extern const char MCLB_WS    [NULL_CODE];   /* Warehouse Selection */

#define MCLBTT_I        1   /* Item List */
#define MCLBTT_S        2   /* Store */
#define MCLBTT_SKU      3   /* Item */
#define MCLBTT_SL       4   /* Store List */
#define MCLBTT_SS       5   /* Store Selection */
#define MCLBTT_ST       6   /* Item Parent */
#define MCLBTT_W        7   /* Wh */
#define MCLBTT_WL       8   /* Warehouse List */
#define MCLBTT_WS       9   /* Warehouse Selection */

#define _MCLBTT_MAX 9


/* MCLT = Multiple Channel Location Type */
extern const char MCLT[NULL_CODE];

extern const char MCLT_S     [NULL_CODE];   /* Store */
extern const char MCLT_W     [NULL_CODE];   /* Warehouse */
extern const char MCLT_PW    [NULL_CODE];   /* Physical Warehouse */

#define MCLTTT_S        1   /* Store */
#define MCLTTT_W        2   /* Warehouse */
#define MCLTTT_PW       3   /* Physical Warehouse */

#define _MCLTTT_MAX 3


/* MCTP = Rejection Change Type */
extern const char MCTP[NULL_CODE];

extern const char MCTP_I     [NULL_CODE];   /* Item Indicator */
extern const char MCTP_L     [NULL_CODE];   /* Location-level Attributes */
extern const char MCTP_M     [NULL_CODE];   /* Merchandise Hierarchy */
extern const char MCTP_P     [NULL_CODE];   /* Seasons/Phases */
extern const char MCTP_R     [NULL_CODE];   /* Replenishment */
extern const char MCTP_S     [NULL_CODE];   /* Substitute Items */
extern const char MCTP_U     [NULL_CODE];   /* User-defined Attributes */
extern const char MCTP_V     [NULL_CODE];   /* Vat Indicator */

#define MCTPTT_I        1   /* Item Indicator */
#define MCTPTT_L        2   /* Location-level Attributes */
#define MCTPTT_M        3   /* Merchandise Hierarchy */
#define MCTPTT_P        4   /* Seasons/Phases */
#define MCTPTT_R        5   /* Replenishment */
#define MCTPTT_S        6   /* Substitute Items */
#define MCTPTT_U        7   /* User-defined Attributes */
#define MCTPTT_V        8   /* Vat Indicator */

#define _MCTPTT_MAX 8


/* MDLB = Mkdvars - Form Labels */
extern const char MDLB[NULL_CODE];

extern const char MDLB_MC    [NULL_CODE];   /* Markup % Cost */
extern const char MDLB_MR    [NULL_CODE];   /* Markup % Retail */

#define MDLBTT_MC       1   /* Markup % Cost */
#define MDLBTT_MR       2   /* Markup % Retail */

#define _MDLBTT_MAX 2


/* MDTP = Markdown Type */
extern const char MDTP[NULL_CODE];

extern const char MDTP_11    [NULL_CODE];   /* Markup */
extern const char MDTP_12    [NULL_CODE];   /* Markup Cancel */
extern const char MDTP_13    [NULL_CODE];   /* Permanent Markdown */
extern const char MDTP_14    [NULL_CODE];   /* Markdown Cancel */
extern const char MDTP_70    [NULL_CODE];   /* Cost Change */

#define MDTPTT_11       1   /* Markup */
#define MDTPTT_12       2   /* Markup Cancel */
#define MDTPTT_13       3   /* Permanent Markdown */
#define MDTPTT_14       4   /* Markdown Cancel */
#define MDTPTT_70       5   /* Cost Change */

#define _MDTPTT_MAX 5


/* MEAS = Measurment Type */
extern const char MEAS[NULL_CODE];

extern const char MEAS_M     [NULL_CODE];   /* Metric */
extern const char MEAS_I     [NULL_CODE];   /* Imperial */

#define MEASTT_M        1   /* Metric */
#define MEASTT_I        2   /* Imperial */

#define _MEASTT_MAX 2


/* MELB = Merchandise Hierarchy Labels */
extern const char MELB[NULL_CODE];

extern const char MELB_C     [NULL_CODE];   /* @OH1@ */
extern const char MELB_DI    [NULL_CODE];   /* @MH2@ */
extern const char MELB_D     [NULL_CODE];   /* @MH4@ */
extern const char MELB_CL    [NULL_CODE];   /* @MH5@ */
extern const char MELB_S     [NULL_CODE];   /* @MH6@ */
extern const char MELB_ASIT  [NULL_CODE];   /* All @MHP6@ in the @MH5@ */
extern const char MELB_ACIT  [NULL_CODE];   /* All @MHP5@ in the @MH4@ */
extern const char MELB_ADPI  [NULL_CODE];   /* All @MHP4@ in the @MH3@ */
extern const char MELB_AGIT  [NULL_CODE];   /* All @MHP3@ in the @MH2@ */
extern const char MELB_ADIT  [NULL_CODE];   /* All @MHP2@ in the @OH1@ */
extern const char MELB_ADP   [NULL_CODE];   /* All @MHP4@ */
extern const char MELB_AG    [NULL_CODE];   /* All @MHP3@ */
extern const char MELB_AD    [NULL_CODE];   /* All @MHP2@ */
extern const char MELB_ENDS  [NULL_CODE];   /* Enter @MH4@ and @MH5@ */
extern const char MELB_ENGP  [NULL_CODE];   /* Enter @MH3@ */
extern const char MELB_ENDP  [NULL_CODE];   /* Enter @MH4@ */
extern const char MELB_AC    [NULL_CODE];   /* All @OHP1@ */
extern const char MELB_G     [NULL_CODE];   /* @MH3@ */
extern const char MELB_M     [NULL_CODE];   /* Maintenance */
extern const char MELB_LVC   [NULL_CODE];   /* List of @OHP1@ */
extern const char MELB_LVDI  [NULL_CODE];   /* List of @MHP2@ */
extern const char MELB_LVG   [NULL_CODE];   /* List of @MHP3@ */
extern const char MELB_LVD   [NULL_CODE];   /* List of @MHP4@ */
extern const char MELB_LVCL  [NULL_CODE];   /* List of @MHP5@ */
extern const char MELB_LVS   [NULL_CODE];   /* List of @MHP6@ */

#define MELBTT_C        1   /* @OH1@ */
#define MELBTT_DI       2   /* @MH2@ */
#define MELBTT_D        3   /* @MH4@ */
#define MELBTT_CL       4   /* @MH5@ */
#define MELBTT_S        5   /* @MH6@ */
#define MELBTT_ASIT     6   /* All @MHP6@ in the @MH5@ */
#define MELBTT_ACIT     7   /* All @MHP5@ in the @MH4@ */
#define MELBTT_ADPI     8   /* All @MHP4@ in the @MH3@ */
#define MELBTT_AGIT     9   /* All @MHP3@ in the @MH2@ */
#define MELBTT_ADIT    10   /* All @MHP2@ in the @OH1@ */
#define MELBTT_ADP     11   /* All @MHP4@ */
#define MELBTT_AG      12   /* All @MHP3@ */
#define MELBTT_AD      13   /* All @MHP2@ */
#define MELBTT_ENDS    14   /* Enter @MH4@ and @MH5@ */
#define MELBTT_ENGP    15   /* Enter @MH3@ */
#define MELBTT_ENDP    16   /* Enter @MH4@ */
#define MELBTT_AC      17   /* All @OHP1@ */
#define MELBTT_G       18   /* @MH3@ */
#define MELBTT_M       19   /* Maintenance */
#define MELBTT_LVC     20   /* List of @OHP1@ */
#define MELBTT_LVDI    21   /* List of @MHP2@ */
#define MELBTT_LVG     22   /* List of @MHP3@ */
#define MELBTT_LVD     23   /* List of @MHP4@ */
#define MELBTT_LVCL    24   /* List of @MHP5@ */
#define MELBTT_LVS     25   /* List of @MHP6@ */

#define _MELBTT_MAX 25


/* MER1 = Merchandise Level */
extern const char MER1[NULL_CODE];

extern const char MER1_D     [NULL_CODE];   /* @MH4@ */
extern const char MER1_C     [NULL_CODE];   /* @MH5@ */
extern const char MER1_S     [NULL_CODE];   /* @MH6@ */

#define MER1TT_D        1   /* @MH4@ */
#define MER1TT_C        2   /* @MH5@ */
#define MER1TT_S        3   /* @MH6@ */

#define _MER1TT_MAX 3


/* MFIN = ALC Method of Finalization */
extern const char MFIN[NULL_CODE];

extern const char MFIN_N     [NULL_CODE];   /* No Finalization */
extern const char MFIN_W     [NULL_CODE];   /* Finalization/WAC */

#define MFINTT_N        1   /* No Finalization */
#define MFINTT_W        2   /* Finalization/WAC */

#define _MFINTT_MAX 2


/* MGUC = Cost Template Margin/Upcharge */
extern const char MGUC[NULL_CODE];

extern const char MGUC_M     [NULL_CODE];   /* Margin */
extern const char MGUC_U     [NULL_CODE];   /* Upcharge */

#define MGUCTT_M        1   /* Margin */
#define MGUCTT_U        2   /* Upcharge */

#define _MGUCTT_MAX 2


/* MHDD = Merch Hier Default Dialog */
extern const char MHDD[NULL_CODE];

extern const char MHDD_ITEM  [NULL_CODE];   /* Item Master */

#define MHDDTT_ITEM     1   /* Item Master */

#define _MHDDTT_MAX 1


/* MHDI = Merch Hier Default Info */
extern const char MHDI[NULL_CODE];

extern const char MHDI_LOC   [NULL_CODE];   /* Locations */
extern const char MHDI_SEAS  [NULL_CODE];   /* Seasons */
extern const char MHDI_IMPA  [NULL_CODE];   /* Import Attributes */
extern const char MHDI_DOCS  [NULL_CODE];   /* Documents */
extern const char MHDI_HTS   [NULL_CODE];   /* HTS */
extern const char MHDI_ETT   [NULL_CODE];   /* Eligible Tariff Treatments */
extern const char MHDI_EXP   [NULL_CODE];   /* Expenses */
extern const char MHDI_TIMEL [NULL_CODE];   /* Timelines */
extern const char MHDI_TICKET[NULL_CODE];   /* Tickets */
extern const char MHDI_IMAGE [NULL_CODE];   /* Image */
extern const char MHDI_SUBTRN[NULL_CODE];   /* Sub-Tran Level Items */
extern const char MHDI_DIMO  [NULL_CODE];   /* Case Dimensions */
extern const char MHDI_DIFF  [NULL_CODE];   /* Diffs */
extern const char MHDI_RECRET[NULL_CODE];   /* Suggested Retail */
extern const char MHDI_PCKSZE[NULL_CODE];   /* Package Size Info */
extern const char MHDI_RETLBL[NULL_CODE];   /* Retail Label */
extern const char MHDI_HSEN  [NULL_CODE];   /* Handling Sensitivity */
extern const char MHDI_HTEMP [NULL_CODE];   /* Handling Temperature */
extern const char MHDI_WASTE [NULL_CODE];   /* Wastage */
extern const char MHDI_COM   [NULL_CODE];   /* Comments */

#define MHDITT_LOC      1   /* Locations */
#define MHDITT_SEAS     2   /* Seasons */
#define MHDITT_IMPA     5   /* Import Attributes */
#define MHDITT_DOCS     6   /* Documents */
#define MHDITT_HTS      7   /* HTS */
#define MHDITT_ETT      8   /* Eligible Tariff Treatments */
#define MHDITT_EXP      9   /* Expenses */
#define MHDITT_TIMEL   10   /* Timelines */
#define MHDITT_TICKET  11   /* Tickets */
#define MHDITT_IMAGE   12   /* Image */
#define MHDITT_SUBTRN  13   /* Sub-Tran Level Items */
#define MHDITT_DIMO    14   /* Case Dimensions */
#define MHDITT_DIFF    15   /* Diffs */
#define MHDITT_RECRET  16   /* Suggested Retail */
#define MHDITT_PCKSZE  17   /* Package Size Info */
#define MHDITT_RETLBL  18   /* Retail Label */
#define MHDITT_HSEN    19   /* Handling Sensitivity */
#define MHDITT_HTEMP   20   /* Handling Temperature */
#define MHDITT_WASTE   21   /* Wastage */
#define MHDITT_COM     22   /* Comments */

#define _MHDITT_MAX 22


/* MIKI = Distance Unit of Measure */
extern const char MIKI[NULL_CODE];

extern const char MIKI_1     [NULL_CODE];   /* Miles */
extern const char MIKI_2     [NULL_CODE];   /* Kilometers */

#define MIKITT_1        1   /* Miles */
#define MIKITT_2        2   /* Kilometers */

#define _MIKITT_MAX 2


/* MKDV = Mkdvars - Store Cost Change Types */
extern const char MKDV[NULL_CODE];

extern const char MKDV_S     [NULL_CODE];   /* Store */
extern const char MKDV_C     [NULL_CODE];   /* Store Class */
extern const char MKDV_D     [NULL_CODE];   /* @OH5@ */
extern const char MKDV_R     [NULL_CODE];   /* @OH4@ */
extern const char MKDV_T     [NULL_CODE];   /* Transfer Zone */
extern const char MKDV_L     [NULL_CODE];   /* Location Traits */
extern const char MKDV_A     [NULL_CODE];   /* All Stores */

#define MKDVTT_S        1   /* Store */
#define MKDVTT_C        2   /* Store Class */
#define MKDVTT_D        3   /* @OH5@ */
#define MKDVTT_R        4   /* @OH4@ */
#define MKDVTT_T        6   /* Transfer Zone */
#define MKDVTT_L        7   /* Location Traits */
#define MKDVTT_A        8   /* All Stores */

#define _MKDVTT_MAX 8


/* MKTC = Electronic Marketing Clubs */
extern const char MKTC[NULL_CODE];

extern const char MKTC_B     [NULL_CODE];   /* Baby Club */
extern const char MKTC_F     [NULL_CODE];   /* Family Club */
extern const char MKTC_BF    [NULL_CODE];   /* Baby/Family Clubs */
extern const char MKTC_FR    [NULL_CODE];   /* Frozen Foods Club */
extern const char MKTC_D     [NULL_CODE];   /* Deli Club */
extern const char MKTC_P     [NULL_CODE];   /* Produce Club */
extern const char MKTC_HL    [NULL_CODE];   /* Healthy Living Club */
extern const char MKTC_NF    [NULL_CODE];   /* Natural Foods Club */
extern const char MKTC_PHL   [NULL_CODE];   /* Produce/Healthy Living Clubs */
extern const char MKTC_PHLNF [NULL_CODE];   /* Produce/Healthy Living/Natural Clubs */

#define MKTCTT_B        1   /* Baby Club */
#define MKTCTT_F        2   /* Family Club */
#define MKTCTT_BF       3   /* Baby/Family Clubs */
#define MKTCTT_FR       4   /* Frozen Foods Club */
#define MKTCTT_D        5   /* Deli Club */
#define MKTCTT_P        6   /* Produce Club */
#define MKTCTT_HL       7   /* Healthy Living Club */
#define MKTCTT_NF       8   /* Natural Foods Club */
#define MKTCTT_PHL      9   /* Produce/Healthy Living Clubs */
#define MKTCTT_PHLNF   10   /* Produce/Healthy Living/Natural Clubs */

#define _MKTCTT_MAX 10


/* MLVL = Filter Merchandise Levels */
extern const char MLVL[NULL_CODE];

extern const char MLVL_D     [NULL_CODE];   /* @MH2@ */
extern const char MLVL_G     [NULL_CODE];   /* Groups */
extern const char MLVL_P     [NULL_CODE];   /* @MH4@ */
extern const char MLVL_C     [NULL_CODE];   /* @MH5@ */
extern const char MLVL_S     [NULL_CODE];   /* @MH6@ */

#define MLVLTT_D        1   /* @MH2@ */
#define MLVLTT_G        2   /* Groups */
#define MLVLTT_P        3   /* @MH4@ */
#define MLVLTT_C        4   /* @MH5@ */
#define MLVLTT_S        5   /* @MH6@ */

#define _MLVLTT_MAX 5


/* MNCJ = Minimum Order Conjunction */
extern const char MNCJ[NULL_CODE];

extern const char MNCJ_A     [NULL_CODE];   /* AND */
extern const char MNCJ_O     [NULL_CODE];   /* OR */

#define MNCJTT_A        1   /* AND */
#define MNCJTT_O        2   /* OR */

#define _MNCJTT_MAX 2


/* MNLV = Supplier Order Min Level */
extern const char MNLV[NULL_CODE];

extern const char MNLV_O     [NULL_CODE];   /* Total Order */
extern const char MNLV_L     [NULL_CODE];   /* Location */
extern const char MNLV_N     [NULL_CODE];   /* None Required */

#define MNLVTT_O        1   /* Total Order */
#define MNLVTT_L        2   /* Location */
#define MNLVTT_N        3   /* None Required */

#define _MNLVTT_MAX 3


/* MNTH = Month Abbreviations */
extern const char MNTH[NULL_CODE];

extern const char MNTH_1     [NULL_CODE];   /* JAN */
extern const char MNTH_2     [NULL_CODE];   /* FEB */
extern const char MNTH_3     [NULL_CODE];   /* MAR */
extern const char MNTH_4     [NULL_CODE];   /* APR */
extern const char MNTH_5     [NULL_CODE];   /* MAY */
extern const char MNTH_6     [NULL_CODE];   /* JUN */
extern const char MNTH_7     [NULL_CODE];   /* JUL */
extern const char MNTH_8     [NULL_CODE];   /* AUG */
extern const char MNTH_9     [NULL_CODE];   /* SEP */
extern const char MNTH_10    [NULL_CODE];   /* OCT */
extern const char MNTH_11    [NULL_CODE];   /* NOV */
extern const char MNTH_12    [NULL_CODE];   /* DEC */

#define MNTHTT_1        1   /* JAN */
#define MNTHTT_2        2   /* FEB */
#define MNTHTT_3        3   /* MAR */
#define MNTHTT_4        4   /* APR */
#define MNTHTT_5        5   /* MAY */
#define MNTHTT_6        6   /* JUN */
#define MNTHTT_7        7   /* JUL */
#define MNTHTT_8        8   /* AUG */
#define MNTHTT_9        9   /* SEP */
#define MNTHTT_10      10   /* OCT */
#define MNTHTT_11      11   /* NOV */
#define MNTHTT_12      12   /* DEC */

#define _MNTHTT_MAX 12


/* MNTP = Supplier Order Min Type */
extern const char MNTP[NULL_CODE];

extern const char MNTP_VOL   [NULL_CODE];   /* Volume */
extern const char MNTP_MASS  [NULL_CODE];   /* Mass */
extern const char MNTP_PACK  [NULL_CODE];   /* Pack */
extern const char MNTP_QTY   [NULL_CODE];   /* Quantity */
extern const char MNTP_AMT   [NULL_CODE];   /* Amount */

#define MNTPTT_VOL      1   /* Volume */
#define MNTPTT_MASS     2   /* Mass */
#define MNTPTT_PACK     3   /* Pack */
#define MNTPTT_QTY      4   /* Quantity */
#define MNTPTT_AMT      5   /* Amount */

#define _MNTPTT_MAX 5


/* MONT = Months of the Year */
extern const char MONT[NULL_CODE];

extern const char MONT_1     [NULL_CODE];   /* January */
extern const char MONT_2     [NULL_CODE];   /* February */
extern const char MONT_3     [NULL_CODE];   /* March */
extern const char MONT_4     [NULL_CODE];   /* April */
extern const char MONT_5     [NULL_CODE];   /* May */
extern const char MONT_6     [NULL_CODE];   /* June */
extern const char MONT_7     [NULL_CODE];   /* July */
extern const char MONT_8     [NULL_CODE];   /* August */
extern const char MONT_9     [NULL_CODE];   /* September */
extern const char MONT_10    [NULL_CODE];   /* October */
extern const char MONT_11    [NULL_CODE];   /* November */
extern const char MONT_12    [NULL_CODE];   /* December */

#define MONTTT_1        1   /* January */
#define MONTTT_2        2   /* February */
#define MONTTT_3        3   /* March */
#define MONTTT_4        4   /* April */
#define MONTTT_5        5   /* May */
#define MONTTT_6        6   /* June */
#define MONTTT_7        7   /* July */
#define MONTTT_8        8   /* August */
#define MONTTT_9        9   /* September */
#define MONTTT_10      10   /* October */
#define MONTTT_11      11   /* November */
#define MONTTT_12      12   /* December */

#define _MONTTT_MAX 12


extern const char MORDER_6000  [NULL_CODE];   /* Money Orders */


/* MRIT = MRT apply Item Types */
extern const char MRIT[NULL_CODE];

extern const char MRIT_I     [NULL_CODE];   /* Item */
extern const char MRIT_IP    [NULL_CODE];   /* Item Parent */
extern const char MRIT_IG    [NULL_CODE];   /* Item Grandparent */
extern const char MRIT_IL    [NULL_CODE];   /* Item List */

#define MRITTT_I        1   /* Item */
#define MRITTT_IP       2   /* Item Parent */
#define MRITTT_IG       3   /* Item Grandparent */
#define MRITTT_IL       4   /* Item List */

#define _MRITTT_MAX 4


/* MRLV = Merchandise Levels */
extern const char MRLV[NULL_CODE];

extern const char MRLV_DEP   [NULL_CODE];   /* @MH4@ */
extern const char MRLV_CLA   [NULL_CODE];   /* @MH5@ */
extern const char MRLV_SUB   [NULL_CODE];   /* @MH6@ */
extern const char MRLV_STY   [NULL_CODE];   /* Item Parent */
extern const char MRLV_SKU   [NULL_CODE];   /* Item */
extern const char MRLV_LST   [NULL_CODE];   /* Item List */

#define MRLVTT_DEP      1   /* @MH4@ */
#define MRLVTT_CLA      2   /* @MH5@ */
#define MRLVTT_SUB      3   /* @MH6@ */
#define MRLVTT_STY      4   /* Item Parent */
#define MRLVTT_SKU      5   /* Item */
#define MRLVTT_LST      6   /* Item List */

#define _MRLVTT_MAX 6


/* MRTS = MRT Status */
extern const char MRTS[NULL_CODE];

extern const char MRTS_I     [NULL_CODE];   /* Input */
extern const char MRTS_S     [NULL_CODE];   /* Submitted */
extern const char MRTS_A     [NULL_CODE];   /* Approved */
extern const char MRTS_R     [NULL_CODE];   /* RTV Created */
extern const char MRTS_C     [NULL_CODE];   /* Closed */

#define MRTSTT_I        1   /* Input */
#define MRTSTT_S        2   /* Submitted */
#define MRTSTT_A        3   /* Approved */
#define MRTSTT_R        4   /* RTV Created */
#define MRTSTT_C        5   /* Closed */

#define _MRTSTT_MAX 5


/* MSCR = Miscellaneous Report Type */
extern const char MSCR[NULL_CODE];

extern const char MSCR_M     [NULL_CODE];   /* Money Orders */
extern const char MSCR_P     [NULL_CODE];   /* Pay In/Out */
extern const char MSCR_B     [NULL_CODE];   /* Button Configurations */
extern const char MSCR_T     [NULL_CODE];   /* Tender Types */
extern const char MSCR_S     [NULL_CODE];   /* Supplier Payment Methods */

#define MSCRTT_M        1   /* Money Orders */
#define MSCRTT_P        2   /* Pay In/Out */
#define MSCRTT_B        3   /* Button Configurations */
#define MSCRTT_T        4   /* Tender Types */
#define MSCRTT_S        5   /* Supplier Payment Methods */

#define _MSCRTT_MAX 5


/* MSRP = Miscellaneous Sales Tax Reports */
extern const char MSRP[NULL_CODE];

extern const char MSRP_W     [NULL_CODE];   /* Items With No Tax Codes */
extern const char MSRP_L     [NULL_CODE];   /* Tax Rates by Location */
extern const char MSRP_I     [NULL_CODE];   /* Tax Rates by Item/@MH4@ */

#define MSRPTT_W        1   /* Items With No Tax Codes */
#define MSRPTT_L        2   /* Tax Rates by Location */
#define MSRPTT_I        3   /* Tax Rates by Item/@MH4@ */

#define _MSRPTT_MAX 3


/* MTCH = Ship Match Criteria */
extern const char MTCH[NULL_CODE];

extern const char MTCH_P     [NULL_CODE];   /* Purchase Order */
extern const char MTCH_S     [NULL_CODE];   /* Supplier */
extern const char MTCH_A     [NULL_CODE];   /* All Shipments */

#define MTCHTT_P        1   /* Purchase Order */
#define MTCHTT_S        2   /* Supplier */
#define MTCHTT_A        3   /* All Shipments */

#define _MTCHTT_MAX 3


/* MUFR = Mass Update Failure Reason */
extern const char MUFR[NULL_CODE];

extern const char MUFR_F     [NULL_CODE];   /* Failed */
extern const char MUFR_N     [NULL_CODE];   /* Not Reviewed */

#define MUFRTT_F        1   /* Failed */
#define MUFRTT_N        2   /* Not Reviewed */

#define _MUFRTT_MAX 2


/* MUTP = Markup Calculation Type */
extern const char MUTP[NULL_CODE];

extern const char MUTP_C     [NULL_CODE];   /* Cost */
extern const char MUTP_R     [NULL_CODE];   /* Retail */

#define MUTPTT_C        1   /* Cost */
#define MUTPTT_R        2   /* Retail */

#define _MUTPTT_MAX 2


/* MUTY = Markup Type */
extern const char MUTY[NULL_CODE];

extern const char MUTY_C     [NULL_CODE];   /* Markup % Cost */
extern const char MUTY_M     [NULL_CODE];   /* Markup % */
extern const char MUTY_R     [NULL_CODE];   /* Markup % Retail */
extern const char MUTY_S     [NULL_CODE];   /*        Markup % */

#define MUTYTT_C        1   /* Markup % Cost */
#define MUTYTT_M        2   /* Markup % */
#define MUTYTT_R        3   /* Markup % Retail */
#define MUTYTT_S        4   /*        Markup % */

#define _MUTYTT_MAX 4


/* NOMF = Nomination Flags */
extern const char NOMF[NULL_CODE];

extern const char NOMF_N     [NULL_CODE];   /* N/A */
extern const char NOMF_PLUS  [NULL_CODE];   /* + */
extern const char NOMF_MINUS [NULL_CODE];   /* - */

#define NOMFTT_N        1   /* N/A */
#define NOMFTT_PLUS     2   /* + */
#define NOMFTT_MINUS    3   /* - */

#define _NOMFTT_MAX 3


/* OACE = Ordfind Action List with Contract */
extern const char OACE[NULL_CODE];

extern const char OACE_NEW   [NULL_CODE];   /* New Order */
extern const char OACE_S     [NULL_CODE];   /* New Order with Contract */
extern const char OACE_EXIST [NULL_CODE];   /* Create Order from Existing */
extern const char OACE_PRE   [NULL_CODE];   /* Create Order with Pre-Issued Number */
extern const char OACE_VIEW  [NULL_CODE];   /* View Order */
extern const char OACE_EDIT  [NULL_CODE];   /* Edit Order */
extern const char OACE_NEWC  [NULL_CODE];   /* Create from Contract */
extern const char OACE_DSDORD[NULL_CODE];   /* DSD Order Entry */

#define OACETT_NEW      1   /* New Order */
#define OACETT_S        2   /* New Order with Contract */
#define OACETT_EXIST    3   /* Create Order from Existing */
#define OACETT_PRE      4   /* Create Order with Pre-Issued Number */
#define OACETT_VIEW     5   /* View Order */
#define OACETT_EDIT     6   /* Edit Order */
#define OACETT_NEWC     7   /* Create from Contract */
#define OACETT_DSDORD   8   /* DSD Order Entry */

#define _OACETT_MAX 8


/* OAIT = Order Apply Item Types */
extern const char OAIT[NULL_CODE];

extern const char OAIT_I     [NULL_CODE];   /* Item */
extern const char OAIT_P     [NULL_CODE];   /* Item Parent */
extern const char OAIT_G     [NULL_CODE];   /* Item Grandparent */
extern const char OAIT_A     [NULL_CODE];   /* All Items */

#define OAITTT_I        1   /* Item */
#define OAITTT_P        2   /* Item Parent */
#define OAITTT_G        3   /* Item Grandparent */
#define OAITTT_A        4   /* All Items */

#define _OAITTT_MAX 4


/* OANC = Ordfind Action List with No Contract */
extern const char OANC[NULL_CODE];

extern const char OANC_NEW   [NULL_CODE];   /* New Order */
extern const char OANC_EXIST [NULL_CODE];   /* Create Order from Existing */
extern const char OANC_PRE   [NULL_CODE];   /* Create Order with Pre-Issued Number */
extern const char OANC_VIEW  [NULL_CODE];   /* View Order */
extern const char OANC_EDIT  [NULL_CODE];   /* Edit Order */
extern const char OANC_DSDORD[NULL_CODE];   /* DSD Order Entry */

#define OANCTT_NEW      1   /* New Order */
#define OANCTT_EXIST    2   /* Create Order from Existing */
#define OANCTT_PRE      3   /* Create Order with Pre-Issued Number */
#define OANCTT_VIEW     4   /* View Order */
#define OANCTT_EDIT     5   /* Edit Order */
#define OANCTT_DSDORD   6   /* DSD Order Entry */

#define _OANCTT_MAX 6


/* OARR = Order Rejection Reasons */
extern const char OARR[NULL_CODE];

extern const char OARR_VM    [NULL_CODE];   /* Vendor minimum not met */
extern const char OARR_NC    [NULL_CODE];   /* Negative cost associated on an item */
extern const char OARR_UOM   [NULL_CODE];   /* UOM convert error due to incomplete data */
extern const char OARR_LC    [NULL_CODE];   /* Letter of Credit information required */
extern const char OARR_HTS   [NULL_CODE];   /* HTS information incomplete */
extern const char OARR_SO    [NULL_CODE];   /* Order Scaling errors */
extern const char OARR_UR    [NULL_CODE];   /* User role approval amounts exceeded */
extern const char OARR_OTB   [NULL_CODE];   /* OTB Check failed */
extern const char OARR_PT    [NULL_CODE];   /* Sup.bracket costed,purchase type reqd */
extern const char OARR_LTL   [NULL_CODE];   /* Cannot approve LTL orders */
extern const char OARR_SNA   [NULL_CODE];   /* Supplier on order currently isn't active */
extern const char OARR_VMI   [NULL_CODE];   /* Vendor Managed Inventory */
extern const char OARR_NASA  [NULL_CODE];   /* Not Adequate Supplier Availability */
extern const char OARR_CCF   [NULL_CODE];   /* Credit Check failed */
extern const char OARR_CUST  [NULL_CODE];   /* Custom approval failed */

#define OARRTT_VM       1   /* Vendor minimum not met */
#define OARRTT_NC       2   /* Negative cost associated on an item */
#define OARRTT_UOM      3   /* UOM convert error due to incomplete data */
#define OARRTT_LC       4   /* Letter of Credit information required */
#define OARRTT_HTS      5   /* HTS information incomplete */
#define OARRTT_SO       6   /* Order Scaling errors */
#define OARRTT_UR       7   /* User role approval amounts exceeded */
#define OARRTT_OTB      8   /* OTB Check failed */
#define OARRTT_PT       9   /* Sup.bracket costed,purchase type reqd */
#define OARRTT_LTL     10   /* Cannot approve LTL orders */
#define OARRTT_SNA     11   /* Supplier on order currently isn't active */
#define OARRTT_VMI     12   /* Vendor Managed Inventory */
#define OARRTT_NASA    13   /* Not Adequate Supplier Availability */
#define OARRTT_CCF     14   /* Credit Check failed */
#define OARRTT_CUST    15   /* Custom approval failed */

#define _OARRTT_MAX 15


/* OBLG = Obligation Levels */
extern const char OBLG[NULL_CODE];

extern const char OBLG_TRCO  [NULL_CODE];   /* Trans. Container */
extern const char OBLG_TRCPO [NULL_CODE];   /* Trans. Container PO */
extern const char OBLG_TRCP  [NULL_CODE];   /* Trans. Container PO/Item */
extern const char OBLG_TRBL  [NULL_CODE];   /* Trans. BOL/AWB */
extern const char OBLG_TRBLP [NULL_CODE];   /* Trans. BOL/AWB PO */
extern const char OBLG_TRBP  [NULL_CODE];   /* Trans. BOL/AWB PO/Item */
extern const char OBLG_TRVV  [NULL_CODE];   /* Trans. Vessel/Voyage/ETD */
extern const char OBLG_TRVVEP[NULL_CODE];   /* Trans. Vessel/Voyage/ETD PO */
extern const char OBLG_TRVP  [NULL_CODE];   /* Trans. Vessel/Voyage/ETD PO/Item */
extern const char OBLG_PO    [NULL_CODE];   /* Purchase Order Header */
extern const char OBLG_POIT  [NULL_CODE];   /* Purchase Order/Item */
extern const char OBLG_CUST  [NULL_CODE];   /* Customs Entry Header */
extern const char OBLG_POT   [NULL_CODE];   /* PO Trans. Vessel/Voyage/ETD */
extern const char OBLG_ASN   [NULL_CODE];   /* ASN */
extern const char OBLG_ASNP  [NULL_CODE];   /* ASN PO */
extern const char OBLG_ASNC  [NULL_CODE];   /* ASN Carton */

#define OBLGTT_TRCO     1   /* Trans. Container */
#define OBLGTT_TRCPO    2   /* Trans. Container PO */
#define OBLGTT_TRCP     3   /* Trans. Container PO/Item */
#define OBLGTT_TRBL     4   /* Trans. BOL/AWB */
#define OBLGTT_TRBLP    5   /* Trans. BOL/AWB PO */
#define OBLGTT_TRBP     6   /* Trans. BOL/AWB PO/Item */
#define OBLGTT_TRVV     7   /* Trans. Vessel/Voyage/ETD */
#define OBLGTT_TRVVEP   8   /* Trans. Vessel/Voyage/ETD PO */
#define OBLGTT_TRVP     9   /* Trans. Vessel/Voyage/ETD PO/Item */
#define OBLGTT_PO      10   /* Purchase Order Header */
#define OBLGTT_POIT    11   /* Purchase Order/Item */
#define OBLGTT_CUST    12   /* Customs Entry Header */
#define OBLGTT_POT     13   /* PO Trans. Vessel/Voyage/ETD */
#define OBLGTT_ASN     14   /* ASN */
#define OBLGTT_ASNP    15   /* ASN PO */
#define OBLGTT_ASNC    16   /* ASN Carton */

#define _OBLGTT_MAX 16


/* OBST = Obligation Status */
extern const char OBST[NULL_CODE];

extern const char OBST_P     [NULL_CODE];   /* Pending */
extern const char OBST_A     [NULL_CODE];   /* Approved */

#define OBSTTT_P        1   /* Pending */
#define OBSTTT_A        2   /* Approved */

#define _OBSTTT_MAX 2


/* ODCS = Orddtl Create status list box */
extern const char ODCS[NULL_CODE];

extern const char ODCS_B     [NULL_CODE];   /* Built */
extern const char ODCS_G     [NULL_CODE];   /* Generated */

#define ODCSTT_B        1   /* Built */
#define ODCSTT_G        2   /* Generated */

#define _ODCSTT_MAX 2


/* ODIT = Order Distribution Item Types */
extern const char ODIT[NULL_CODE];

extern const char ODIT_I     [NULL_CODE];   /* Item */
extern const char ODIT_VPN   [NULL_CODE];   /* Reference Item */
extern const char ODIT_IL    [NULL_CODE];   /* Item List */
extern const char ODIT_PT    [NULL_CODE];   /* Pack Template */
extern const char ODIT_P     [NULL_CODE];   /* Prepack */

#define ODITTT_I        1   /* Item */
#define ODITTT_VPN      2   /* Reference Item */
#define ODITTT_IL       3   /* Item List */
#define ODITTT_PT       4   /* Pack Template */
#define ODITTT_P        5   /* Prepack */

#define _ODITTT_MAX 5


/* ODLB = ORDDTL form labels */
extern const char ODLB[NULL_CODE];

extern const char ODLB_C     [NULL_CODE];   /* Contract */
extern const char ODLB_CC    [NULL_CODE];   /* Currency Code */
extern const char ODLB_D     [NULL_CODE];   /* Description */
extern const char ODLB_S     [NULL_CODE];   /* Supplier */
extern const char ODLB_T     [NULL_CODE];   /* Total Cost */

#define ODLBTT_C        1   /* Contract */
#define ODLBTT_CC       2   /* Currency Code */
#define ODLBTT_D        3   /* Description */
#define ODLBTT_S        4   /* Supplier */
#define ODLBTT_T        5   /* Total Cost */

#define _ODLBTT_MAX 5


/* OEIE = The error messages for the OI Inventory Analysis Order Item Errors Report. */
extern const char OEIE[NULL_CODE];

extern const char OEIE_HTS   [NULL_CODE];   /* Approved HTS codes do not exist for this item on the order */
extern const char OEIE_MARG  [NULL_CODE];   /* Margin percentage has not been meet for this item on the order */
extern const char OEIE_REFIT [NULL_CODE];   /* Primary Ref Item is either not defined or not specified on the Order */

#define OEIETT_HTS      1   /* Approved HTS codes do not exist for this item on the order */
#define OEIETT_MARG     2   /* Margin percentage has not been meet for this item on the order */
#define OEIETT_REFIT    3   /* Primary Ref Item is either not defined or not specified on the Order */

#define _OEIETT_MAX 3


/* OEOE = The error messages for the OI Inventory Analysis Order Errors Report. */
extern const char OEOE[NULL_CODE];

extern const char OEOE_LAD   [NULL_CODE];   /* Lading port is missing for this order */
extern const char OEOE_DISC  [NULL_CODE];   /* Discharge port is missing for this order */
extern const char OEOE_FACT  [NULL_CODE];   /* Factory is missing for this order */

#define OEOETT_LAD      1   /* Lading port is missing for this order */
#define OEOETT_DISC     2   /* Discharge port is missing for this order */
#define OEOETT_FACT     3   /* Factory is missing for this order */

#define _OEOETT_MAX 3


/* OET = Organization Entity Type */
extern const char OET[NULL_CODE];

extern const char OET_R     [NULL_CODE];   /* Regular Warehouse */
extern const char OET_M     [NULL_CODE];   /* Importer */
extern const char OET_X     [NULL_CODE];   /* Exporter */

#define OETTT_R        1   /* Regular Warehouse */
#define OETTT_M        2   /* Importer */
#define OETTT_X        3   /* Exporter */

#define _OETTT_MAX 3


/* OFTP = Offer Type */
extern const char OFTP[NULL_CODE];

extern const char OFTP_1     [NULL_CODE];   /* Coupon */
extern const char OFTP_2     [NULL_CODE];   /* Mailer */
extern const char OFTP_3     [NULL_CODE];   /* Bonus Card */
extern const char OFTP_4     [NULL_CODE];   /* Bonus Card - End of Aisle */
extern const char OFTP_5     [NULL_CODE];   /* BOGO - Buy One Get One */
extern const char OFTP_6     [NULL_CODE];   /* BOGO - End of Aisle */
extern const char OFTP_7     [NULL_CODE];   /* Cents Off */
extern const char OFTP_8     [NULL_CODE];   /* Regular */
extern const char OFTP_9     [NULL_CODE];   /* Pre-priced */

#define OFTPTT_1        1   /* Coupon */
#define OFTPTT_2        2   /* Mailer */
#define OFTPTT_3        3   /* Bonus Card */
#define OFTPTT_4        4   /* Bonus Card - End of Aisle */
#define OFTPTT_5        5   /* BOGO - Buy One Get One */
#define OFTPTT_6        6   /* BOGO - End of Aisle */
#define OFTPTT_7        7   /* Cents Off */
#define OFTPTT_8        8   /* Regular */
#define OFTPTT_9        9   /* Pre-priced */

#define _OFTPTT_MAX 9


/* OHTP = Organizational Hierarchy Types */
extern const char OHTP[NULL_CODE];

extern const char OHTP_1     [NULL_CODE];   /* @OH1@ */
extern const char OHTP_10    [NULL_CODE];   /* @OH2@ */
extern const char OHTP_20    [NULL_CODE];   /* @OH3@ */
extern const char OHTP_30    [NULL_CODE];   /* @OH4@ */
extern const char OHTP_40    [NULL_CODE];   /* @OH5@ */
extern const char OHTP_50    [NULL_CODE];   /* Store */

#define OHTPTT_1        1   /* @OH1@ */
#define OHTPTT_10       2   /* @OH2@ */
#define OHTPTT_20       3   /* @OH3@ */
#define OHTPTT_30       4   /* @OH4@ */
#define OHTPTT_40       5   /* @OH5@ */
#define OHTPTT_50       6   /* Store */

#define _OHTPTT_MAX 6


/* OIBI = Operation Insight early or late shipments issue list */
extern const char OIBI[NULL_CODE];

extern const char OIBI_ESHIP [NULL_CODE];   /* Early Shipment */
extern const char OIBI_LSHIP [NULL_CODE];   /* Late Shipment */
extern const char OIBI_ASNNR [NULL_CODE];   /* ASN Not Received */
extern const char OIBI_OTBIN [NULL_CODE];   /* OTB Shift In */
extern const char OIBI_OTBOUT[NULL_CODE];   /* OTB Shift Out */

#define OIBITT_ESHIP    1   /* Early Shipment */
#define OIBITT_LSHIP    2   /* Late Shipment */
#define OIBITT_ASNNR    3   /* ASN Not Received */
#define OIBITT_OTBIN    4   /* OTB Shift In */
#define OIBITT_OTBOUT   5   /* OTB Shift Out */

#define _OIBITT_MAX 5


/* OICS = Order/Item Cost Source */
extern const char OICS[NULL_CODE];

extern const char OICS_NORM  [NULL_CODE];   /* Supplier */
extern const char OICS_BRKT  [NULL_CODE];   /* Bracket */
extern const char OICS_DEAL  [NULL_CODE];   /* Deal */
extern const char OICS_CONT  [NULL_CODE];   /* Contract */
extern const char OICS_MANL  [NULL_CODE];   /* Manual */
extern const char OICS_MULT  [NULL_CODE];   /* Multiple */

#define OICSTT_NORM     1   /* Supplier */
#define OICSTT_BRKT     2   /* Bracket */
#define OICSTT_DEAL     3   /* Deal */
#define OICSTT_CONT     4   /* Contract */
#define OICSTT_MANL     5   /* Manual */
#define OICSTT_MULT     6   /* Multiple */

#define _OICSTT_MAX 6


/* OLVL = Filter Organization Levels */
extern const char OLVL[NULL_CODE];

extern const char OLVL_C     [NULL_CODE];   /* @OH2@ */
extern const char OLVL_A     [NULL_CODE];   /* Area Retail Merchandising System */
extern const char OLVL_R     [NULL_CODE];   /* @OH4@ */
extern const char OLVL_D     [NULL_CODE];   /* @OH5@ */
extern const char OLVL_W     [NULL_CODE];   /* Warehouse */
extern const char OLVL_I     [NULL_CODE];   /* Internal Finisher */
extern const char OLVL_E     [NULL_CODE];   /* External Finisher */
extern const char OLVL_T     [NULL_CODE];   /* Transfer Entity */
extern const char OLVL_O     [NULL_CODE];   /* Org Unit */

#define OLVLTT_C        1   /* @OH2@ */
#define OLVLTT_A        2   /* Area Retail Merchandising System */
#define OLVLTT_R        3   /* @OH4@ */
#define OLVLTT_D        4   /* @OH5@ */
#define OLVLTT_W        5   /* Warehouse */
#define OLVLTT_I        6   /* Internal Finisher */
#define OLVLTT_E        7   /* External Finisher */
#define OLVLTT_T        8   /* Transfer Entity */
#define OLVLTT_O        9   /* Org Unit */

#define _OLVLTT_MAX 9


/* OPER = Combination Operations */
extern const char OPER[NULL_CODE];

extern const char OPER_PLUS  [NULL_CODE];   /* + */
extern const char OPER_MINUS [NULL_CODE];   /* - */

#define OPERTT_PLUS     1   /* + */
#define OPERTT_MINUS    2   /* - */

#define _OPERTT_MAX 2


/* OPYM = Obligation Payment Method */
extern const char OPYM[NULL_CODE];

extern const char OPYM_C     [NULL_CODE];   /* Check */
extern const char OPYM_W     [NULL_CODE];   /* Wired Transfer */
extern const char OPYM_A     [NULL_CODE];   /* ACH */
extern const char OPYM_L     [NULL_CODE];   /* Letter of Credit */

#define OPYMTT_C        1   /* Check */
#define OPYMTT_W        2   /* Wired Transfer */
#define OPYMTT_A        3   /* ACH */
#define OPYMTT_L        4   /* Letter of Credit */

#define _OPYMTT_MAX 4


/* ORCA = Ordhead Cancel List Box */
extern const char ORCA[NULL_CODE];

extern const char ORCA_B     [NULL_CODE];   /* Buyer Cancelled */
extern const char ORCA_V     [NULL_CODE];   /* Vendor Cancelled */
extern const char ORCA_A     [NULL_CODE];   /* Automatic */

#define ORCATT_B        1   /* Buyer Cancelled */
#define ORCATT_V        2   /* Vendor Cancelled */
#define ORCATT_A        3   /* Automatic */

#define _ORCATT_MAX 3


/* ORCS = Order Cost Source */
extern const char ORCS[NULL_CODE];

extern const char ORCS_NORM  [NULL_CODE];   /* Supplier */
extern const char ORCS_BRKT  [NULL_CODE];   /* Bracket */
extern const char ORCS_DEAL  [NULL_CODE];   /* Deal */
extern const char ORCS_CONT  [NULL_CODE];   /* Contract */
extern const char ORCS_MANL  [NULL_CODE];   /* Manual */

#define ORCSTT_NORM     1   /* Supplier */
#define ORCSTT_BRKT     2   /* Bracket */
#define ORCSTT_DEAL     3   /* Deal */
#define ORCSTT_CONT     4   /* Contract */
#define ORCSTT_MANL     5   /* Manual */

#define _ORCSTT_MAX 5


/* ORDB = Ordfind Form Labels */
extern const char ORDB[NULL_CODE];

extern const char ORDB_C     [NULL_CODE];   /* Contract */
extern const char ORDB_CN    [NULL_CODE];   /* Contract No. */

#define ORDBTT_C        1   /* Contract */
#define ORDBTT_CN       2   /* Contract No. */

#define _ORDBTT_MAX 2


/* ORDL = Ordadd Form Labels */
extern const char ORDL[NULL_CODE];

extern const char ORDL_N     [NULL_CODE];   /* No orders address on file. */
extern const char ORDL_M     [NULL_CODE];   /* Markup % */
extern const char ORDL_MR    [NULL_CODE];   /* Markup % Retail */
extern const char ORDL_MC    [NULL_CODE];   /* Markup % Cost */

#define ORDLTT_N        1   /* No orders address on file. */
#define ORDLTT_M        2   /* Markup % */
#define ORDLTT_MR       3   /* Markup % Retail */
#define ORDLTT_MC       4   /* Markup % Cost */

#define _ORDLTT_MAX 4


/* ORDM = Order Methods */
extern const char ORDM[NULL_CODE];

extern const char ORDM_I     [NULL_CODE];   /* Invoice Entry */
extern const char ORDM_U     [NULL_CODE];   /* Unit Extension */

#define ORDMTT_I        1   /* Invoice Entry */
#define ORDMTT_U        2   /* Unit Extension */

#define _ORDMTT_MAX 2


/* ORDO = Ordadd Order Type List Box (Abreviated) */
extern const char ORDO[NULL_CODE];

extern const char ORDO_NONBAS[NULL_CODE];   /* N/B */
extern const char ORDO_ARB   [NULL_CODE];   /* ARB */
extern const char ORDO_BRB   [NULL_CODE];   /* BRB */
extern const char ORDO_DSD   [NULL_CODE];   /* DSD */
extern const char ORDO_CO    [NULL_CODE];   /* Customer Order */

#define ORDOTT_NONBAS   1   /* N/B */
#define ORDOTT_ARB      2   /* ARB */
#define ORDOTT_BRB      3   /* BRB */
#define ORDOTT_DSD      4   /* DSD */
#define ORDOTT_CO       5   /* Customer Order */

#define _ORDOTT_MAX 5


/* ORDR = Order Revision Labels */
extern const char ORDR[NULL_CODE];

extern const char ORDR_R     [NULL_CODE];   /* Revision */
extern const char ORDR_V     [NULL_CODE];   /* Version */

#define ORDRTT_R        1   /* Revision */
#define ORDRTT_V        2   /* Version */

#define _ORDRTT_MAX 2


/* ORDS = Ordsumm form labels */
extern const char ORDS[NULL_CODE];

extern const char ORDS_L     [NULL_CODE];   /*  Location Totals */
extern const char ORDS_S     [NULL_CODE];   /*  Item */
extern const char ORDS_O     [NULL_CODE];   /*  Order Totals */

#define ORDSTT_L        1   /*  Location Totals */
#define ORDSTT_S        2   /*  Item */
#define ORDSTT_O        3   /*  Order Totals */

#define _ORDSTT_MAX 3


/* ORDT = Order Type */
extern const char ORDT[NULL_CODE];

extern const char ORDT_F     [NULL_CODE];   /* Fixed Weight */
extern const char ORDT_V     [NULL_CODE];   /* Variable Weight */

#define ORDTTT_F       10   /* Fixed Weight */
#define ORDTTT_V       20   /* Variable Weight */

#define _ORDTTT_MAX 20


/* ORG1 = Organizational Hierarchy */
extern const char ORG1[NULL_CODE];

extern const char ORG1_CO    [NULL_CODE];   /* @OH1@ */
extern const char ORG1_CH    [NULL_CODE];   /* @OH2@ */
extern const char ORG1_AR    [NULL_CODE];   /* @OH3@ */
extern const char ORG1_RE    [NULL_CODE];   /* @OH4@ */
extern const char ORG1_DI    [NULL_CODE];   /* @OH5@ */
extern const char ORG1_ST    [NULL_CODE];   /* Store */
extern const char ORG1_WH    [NULL_CODE];   /* Warehouse */

#define ORG1TT_CO       1   /* @OH1@ */
#define ORG1TT_CH       2   /* @OH2@ */
#define ORG1TT_AR       3   /* @OH3@ */
#define ORG1TT_RE       4   /* @OH4@ */
#define ORG1TT_DI       5   /* @OH5@ */
#define ORG1TT_ST       6   /* Store */
#define ORG1TT_WH       7   /* Warehouse */

#define _ORG1TT_MAX 7


/* ORGH = Organizational Hierarchy */
extern const char ORGH[NULL_CODE];

extern const char ORGH_R     [NULL_CODE];   /* @OH4@ */
extern const char ORGH_D     [NULL_CODE];   /* @OH5@ */
extern const char ORGH_S     [NULL_CODE];   /* Store */
extern const char ORGH_W     [NULL_CODE];   /* Warehouse */

#define ORGHTT_R        1   /* @OH4@ */
#define ORGHTT_D        2   /* @OH5@ */
#define ORGHTT_S        3   /* Store */
#define ORGHTT_W        4   /* Warehouse */

#define _ORGHTT_MAX 4


/* ORL = Order Rounding Level */
extern const char ORL[NULL_CODE];

extern const char ORL_C     [NULL_CODE];   /* Case */
extern const char ORL_L     [NULL_CODE];   /* Layer */
extern const char ORL_P     [NULL_CODE];   /* Pallet */
extern const char ORL_CL    [NULL_CODE];   /* Case/Layer */
extern const char ORL_LP    [NULL_CODE];   /* Layer/Pallet */
extern const char ORL_CLP   [NULL_CODE];   /* Case/Layer/Pallet */

#define ORLTT_C        1   /* Case */
#define ORLTT_L        2   /* Layer */
#define ORLTT_P        3   /* Pallet */
#define ORLTT_CL       4   /* Case/Layer */
#define ORLTT_LP       5   /* Layer/Pallet */
#define ORLTT_CLP      6   /* Case/Layer/Pallet */

#define _ORLTT_MAX 6


/* ORLB = Organizational Hierarchy Labels */
extern const char ORLB[NULL_CODE];

extern const char ORLB_AC    [NULL_CODE];   /* All @OHP1@ */
extern const char ORLB_ACH   [NULL_CODE];   /* All @OHP2@ */
extern const char ORLB_AR    [NULL_CODE];   /* All @OHP4@ */
extern const char ORLB_AA    [NULL_CODE];   /* All @OHP3@ */
extern const char ORLB_D     [NULL_CODE];   /* @OH5@ */
extern const char ORLB_R     [NULL_CODE];   /* @OH4@ */
extern const char ORLB_A     [NULL_CODE];   /* @OH3@ */
extern const char ORLB_CH    [NULL_CODE];   /* @OH2@ */
extern const char ORLB_C     [NULL_CODE];   /* @OH1@ */
extern const char ORLB_ASIT  [NULL_CODE];   /* All Stores in the @OH5@ */
extern const char ORLB_ADIT  [NULL_CODE];   /* All @OHP5@ in the @OH4@ */
extern const char ORLB_ARIT  [NULL_CODE];   /* All @OHP4@ in the @OH3@ */
extern const char ORLB_W     [NULL_CODE];   /* Warehouse */
extern const char ORLB_S     [NULL_CODE];   /* Store */
extern const char ORLB_AAIT  [NULL_CODE];   /* All @OHP3@ in the @OH2@ */
extern const char ORLB_ACIT  [NULL_CODE];   /* All @OHP2@ in the @OH1@ */
extern const char ORLB_AW    [NULL_CODE];   /* All Warehouses */
extern const char ORLB_AS    [NULL_CODE];   /* All Stores */
extern const char ORLB_AD    [NULL_CODE];   /* All @OHP5@ */
extern const char ORLB_PHWH  [NULL_CODE];   /* Physical Warehouses */
extern const char ORLB_LVA   [NULL_CODE];   /* List of @OHP3@ */
extern const char ORLB_LVR   [NULL_CODE];   /* List of @OHP4@ */
extern const char ORLB_LVD   [NULL_CODE];   /* List of @OHP5@ */

#define ORLBTT_AC       1   /* All @OHP1@ */
#define ORLBTT_ACH      2   /* All @OHP2@ */
#define ORLBTT_AR       3   /* All @OHP4@ */
#define ORLBTT_AA       4   /* All @OHP3@ */
#define ORLBTT_D        5   /* @OH5@ */
#define ORLBTT_R        6   /* @OH4@ */
#define ORLBTT_A        7   /* @OH3@ */
#define ORLBTT_CH       8   /* @OH2@ */
#define ORLBTT_C        9   /* @OH1@ */
#define ORLBTT_ASIT    10   /* All Stores in the @OH5@ */
#define ORLBTT_ADIT    11   /* All @OHP5@ in the @OH4@ */
#define ORLBTT_ARIT    12   /* All @OHP4@ in the @OH3@ */
#define ORLBTT_W       13   /* Warehouse */
#define ORLBTT_S       14   /* Store */
#define ORLBTT_AAIT    15   /* All @OHP3@ in the @OH2@ */
#define ORLBTT_ACIT    16   /* All @OHP2@ in the @OH1@ */
#define ORLBTT_AW      17   /* All Warehouses */
#define ORLBTT_AS      18   /* All Stores */
#define ORLBTT_AD      19   /* All @OHP5@ */
#define ORLBTT_PHWH    20   /* Physical Warehouses */
#define ORLBTT_LVA     21   /* List of @OHP3@ */
#define ORLBTT_LVR     22   /* List of @OHP4@ */
#define ORLBTT_LVD     23   /* List of @OHP5@ */

#define _ORLBTT_MAX 23


/* ORM2 = Merchandise Order */
extern const char ORM2[NULL_CODE];

extern const char ORM2_C     [NULL_CODE];   /* Case Pack */
extern const char ORM2_I     [NULL_CODE];   /* Inner Pack */
extern const char ORM2_E     [NULL_CODE];   /* Eaches */

#define ORM2TT_C        1   /* Case Pack */
#define ORM2TT_I        2   /* Inner Pack */
#define ORM2TT_E        3   /* Eaches */

#define _ORM2TT_MAX 3


/* ORML = Merchandise Order Multiples */
extern const char ORML[NULL_CODE];

extern const char ORML_E     [NULL_CODE];   /* Eaches */
extern const char ORML_I     [NULL_CODE];   /* Inner */
extern const char ORML_C     [NULL_CODE];   /* Cases */

#define ORMLTT_E        1   /* Eaches */
#define ORMLTT_I        2   /* Inner */
#define ORMLTT_C        3   /* Cases */

#define _ORMLTT_MAX 3


/* OROR = Order Origin */
extern const char OROR[NULL_CODE];

extern const char OROR_0     [NULL_CODE];   /* Current System Generated */
extern const char OROR_1     [NULL_CODE];   /* Past System Generated */
extern const char OROR_2     [NULL_CODE];   /* Manual */
extern const char OROR_3     [NULL_CODE];   /* Buyer Worksheet */
extern const char OROR_4     [NULL_CODE];   /* Consignment */
extern const char OROR_5     [NULL_CODE];   /* Vendor Generated */
extern const char OROR_6     [NULL_CODE];   /* AIP Generated PO */
extern const char OROR_7     [NULL_CODE];   /* SIM Generated PO */
extern const char OROR_8     [NULL_CODE];   /* Allocation Generated PO */

#define ORORTT_0        1   /* Current System Generated */
#define ORORTT_1        2   /* Past System Generated */
#define ORORTT_2        3   /* Manual */
#define ORORTT_3        4   /* Buyer Worksheet */
#define ORORTT_4        5   /* Consignment */
#define ORORTT_5        6   /* Vendor Generated */
#define ORORTT_6        7   /* AIP Generated PO */
#define ORORTT_7        8   /* SIM Generated PO */
#define ORORTT_8        9   /* Allocation Generated PO */

#define _ORORTT_MAX 9


/* ORRC = Sales Audit Override Reason Codes */
extern const char ORRC[NULL_CODE];

extern const char ORRC_ERR   [NULL_CODE];   /* Error */
extern const char ORRC_D     [NULL_CODE];   /* Damaged Goods */
extern const char ORRC_T     [NULL_CODE];   /* Incorrect Tag */
extern const char ORRC_S     [NULL_CODE];   /* Incorrect Signage */
extern const char ORRC_CP    [NULL_CODE];   /* Competition Price */
extern const char ORRC_AP    [NULL_CODE];   /* Ad Price */
extern const char ORRC_MS    [NULL_CODE];   /* Managers Special */
extern const char ORRC_PROMPT[NULL_CODE];   /* Price Prompt in POS */
extern const char ORRC_BSPRC [NULL_CODE];   /* Base Price Rule */
extern const char ORRC_AUTHMT[NULL_CODE];   /* Authorized Amount */
extern const char ORRC_ARPR_1[NULL_CODE];   /* Insufficient Funds (House Account Reversal) */
extern const char ORRC_ARPR_2[NULL_CODE];   /* Wrong Account (House Account Reversal) */
extern const char ORRC_ARPR_3[NULL_CODE];   /* Wrong Amount (House Account Reversal) */
extern const char ORRC_ARPR_4[NULL_CODE];   /* Other - Enter Comment (House Account Reversal) */

#define ORRCTT_ERR      1   /* Error */
#define ORRCTT_D        2   /* Damaged Goods */
#define ORRCTT_T        3   /* Incorrect Tag */
#define ORRCTT_S        4   /* Incorrect Signage */
#define ORRCTT_CP       5   /* Competition Price */
#define ORRCTT_AP       6   /* Ad Price */
#define ORRCTT_MS       7   /* Managers Special */
#define ORRCTT_PROMPT   9   /* Price Prompt in POS */
#define ORRCTT_BSPRC   10   /* Base Price Rule */
#define ORRCTT_AUTHMT  11   /* Authorized Amount */
#define ORRCTT_ARPR_1  12   /* Insufficient Funds (House Account Reversal) */
#define ORRCTT_ARPR_2  13   /* Wrong Account (House Account Reversal) */
#define ORRCTT_ARPR_3  14   /* Wrong Amount (House Account Reversal) */
#define ORRCTT_ARPR_4  16   /* Other - Enter Comment (House Account Reversal) */

#define _ORRCTT_MAX 16


/* ORSI = PO Induction Order Status */
extern const char ORSI[NULL_CODE];

extern const char ORSI_W     [NULL_CODE];   /* Worksheet */
extern const char ORSI_S     [NULL_CODE];   /* Submitted */
extern const char ORSI_A     [NULL_CODE];   /* Approved */
extern const char ORSI_C     [NULL_CODE];   /* Closed */
extern const char ORSI_D     [NULL_CODE];   /* Deleted */

#define ORSITT_W        1   /* Worksheet */
#define ORSITT_S        2   /* Submitted */
#define ORSITT_A        3   /* Approved */
#define ORSITT_C        4   /* Closed */
#define ORSITT_D        5   /* Deleted */

#define _ORSITT_MAX 5


/* ORSS = Ordskus form labels */
extern const char ORSS[NULL_CODE];

extern const char ORSS_SK    [NULL_CODE];   /* Item Description */
extern const char ORSS_U     [NULL_CODE];   /* Child Item */
extern const char ORSS_S     [NULL_CODE];   /* Supplement */
extern const char ORSS_D     [NULL_CODE];   /* Description */
extern const char ORSS_V     [NULL_CODE];   /* VPN */
extern const char ORSS_P     [NULL_CODE];   /* Pre-Pack */

#define ORSSTT_SK       1   /* Item Description */
#define ORSSTT_U        2   /* Child Item */
#define ORSSTT_S        3   /* Supplement */
#define ORSSTT_D        4   /* Description */
#define ORSSTT_V        5   /* VPN */
#define ORSSTT_P        6   /* Pre-Pack */

#define _ORSSTT_MAX 6


/* ORST = Order Status */
extern const char ORST[NULL_CODE];

extern const char ORST_W     [NULL_CODE];   /* Worksheet */
extern const char ORST_S     [NULL_CODE];   /* Submitted */
extern const char ORST_A     [NULL_CODE];   /* Approved */
extern const char ORST_C     [NULL_CODE];   /* Closed */

#define ORSTTT_W        1   /* Worksheet */
#define ORSTTT_S        2   /* Submitted */
#define ORSTTT_A        3   /* Approved */
#define ORSTTT_C        4   /* Closed */

#define _ORSTTT_MAX 4


/* ORTP = Order Type */
extern const char ORTP[NULL_CODE];

extern const char ORTP_NONBAS[NULL_CODE];   /* Non-Basic */
extern const char ORTP_ARB   [NULL_CODE];   /* Automatic Reorder of Basic */
extern const char ORTP_BRB   [NULL_CODE];   /* Buyer Reorder of Basic */

#define ORTPTT_NONBAS   1   /* Non-Basic */
#define ORTPTT_ARB      2   /* Automatic Reorder of Basic */
#define ORTPTT_BRB      3   /* Buyer Reorder of Basic */

#define _ORTPTT_MAX 3


/* OS9T = Purchase Order Template Types */
extern const char OS9T[NULL_CODE];

extern const char OS9T_PODT  [NULL_CODE];   /* Purchase Orders */

#define OS9TTT_PODT     1   /* Purchase Orders */

#define _OS9TTT_MAX 1


/* OSGP = Sales Audit Over/Short Groups */
extern const char OSGP[NULL_CODE];

extern const char OSGP_B     [NULL_CODE];   /* Accountable For */
extern const char OSGP_D     [NULL_CODE];   /* Accounted For */

#define OSGPTT_B        1   /* Accountable For */
#define OSGPTT_D        2   /* Accounted For */

#define _OSGPTT_MAX 2


/* OTBC = OTB Calculation Type */
extern const char OTBC[NULL_CODE];

extern const char OTBC_C     [NULL_CODE];   /* Direct Cost */
extern const char OTBC_R     [NULL_CODE];   /* Retail Inventory */

#define OTBCTT_C        1   /* Direct Cost */
#define OTBCTT_R        2   /* Retail Inventory */

#define _OTBCTT_MAX 2


/* OTBR = Open to Buy Report Options */
extern const char OTBR[NULL_CODE];

extern const char OTBR_D     [NULL_CODE];   /* @MH4@ */
extern const char OTBR_AD    [NULL_CODE];   /* All @MHP4@ */
extern const char OTBR_C     [NULL_CODE];   /* @MH5@ */
extern const char OTBR_AC    [NULL_CODE];   /* All @MHP5@ */
extern const char OTBR_S     [NULL_CODE];   /* @MH6@ */
extern const char OTBR_AS    [NULL_CODE];   /* All @MHP6@ */

#define OTBRTT_D        1   /* @MH4@ */
#define OTBRTT_AD       2   /* All @MHP4@ */
#define OTBRTT_C        3   /* @MH5@ */
#define OTBRTT_AC       4   /* All @MHP5@ */
#define OTBRTT_S        5   /* @MH6@ */
#define OTBRTT_AS       6   /* All @MHP6@ */

#define _OTBRTT_MAX 6


/* OXTP = Exchange Types */
extern const char OXTP[NULL_CODE];

extern const char OXTP_O     [NULL_CODE];   /* Operational */
extern const char OXTP_L     [NULL_CODE];   /* LC/Bank */
extern const char OXTP_P     [NULL_CODE];   /* Purchase Order */
extern const char OXTP_U     [NULL_CODE];   /* Customs Entry */
extern const char OXTP_T     [NULL_CODE];   /* Transportation */

#define OXTPTT_O        1   /* Operational */
#define OXTPTT_L        2   /* LC/Bank */
#define OXTPTT_P        3   /* Purchase Order */
#define OXTPTT_U        4   /* Customs Entry */
#define OXTPTT_T        5   /* Transportation */

#define _OXTPTT_MAX 5


/* PACT = POS Action Listbox */
extern const char PACT[NULL_CODE];

extern const char PACT_NEW   [NULL_CODE];   /* New */
extern const char PACT_EDIT  [NULL_CODE];   /* Edit */
extern const char PACT_VIEW  [NULL_CODE];   /* View */
extern const char PACT_NEWE  [NULL_CODE];   /* Create From Existing */

#define PACTTT_NEW      1   /* New */
#define PACTTT_EDIT     2   /* Edit */
#define PACTTT_VIEW     3   /* View */
#define PACTTT_NEWE     4   /* Create From Existing */

#define _PACTTT_MAX 4


/* PALN = Pallet and Equivalent Types */
extern const char PALN[NULL_CODE];

extern const char PALN_PAL   [NULL_CODE];   /* Pallet */
extern const char PALN_FLA   [NULL_CODE];   /* Flat */

#define PALNTT_PAL      1   /* Pallet */
#define PALNTT_FLA      2   /* Flat */

#define _PALNTT_MAX 2


/* PARC = Pack Receive Indicator */
extern const char PARC[NULL_CODE];

extern const char PARC_E     [NULL_CODE];   /* Eaches */
extern const char PARC_P     [NULL_CODE];   /* Pack */

#define PARCTT_E        1   /* Eaches */
#define PARCTT_P        2   /* Pack */

#define _PARCTT_MAX 2


/* PART = Partner Types */
extern const char PART[NULL_CODE];

extern const char PART_BK    [NULL_CODE];   /* Bank */
extern const char PART_AG    [NULL_CODE];   /* Agent */
extern const char PART_FF    [NULL_CODE];   /* Freight Forwarder */
extern const char PART_IM    [NULL_CODE];   /* Importer */
extern const char PART_BR    [NULL_CODE];   /* Broker */
extern const char PART_FA    [NULL_CODE];   /* Factory */
extern const char PART_AP    [NULL_CODE];   /* Applicant */
extern const char PART_CO    [NULL_CODE];   /* Consolidator */
extern const char PART_CN    [NULL_CODE];   /* Consignee */

#define PARTTT_BK       1   /* Bank */
#define PARTTT_AG       2   /* Agent */
#define PARTTT_FF       3   /* Freight Forwarder */
#define PARTTT_IM       4   /* Importer */
#define PARTTT_BR       5   /* Broker */
#define PARTTT_FA       6   /* Factory */
#define PARTTT_AP       7   /* Applicant */
#define PARTTT_CO       8   /* Consolidator */
#define PARTTT_CN       9   /* Consignee */

#define _PARTTT_MAX 9


/* PATH = Phone Authorization Type */
extern const char PATH[NULL_CODE];

extern const char PATH_SEARS [NULL_CODE];   /* Sears */
extern const char PATH_NPC   [NULL_CODE];   /* NPC */
extern const char PATH_NONE  [NULL_CODE];   /* None */

#define PATHTT_SEARS    1   /* Sears */
#define PATHTT_NPC      2   /* NPC */
#define PATHTT_NONE     3   /* None */

#define _PATHTT_MAX 3


/* PATP = Pack Type */
extern const char PATP[NULL_CODE];

extern const char PATP_B     [NULL_CODE];   /* Buyer Pack */
extern const char PATP_V     [NULL_CODE];   /* Vendor Pack */

#define PATPTT_B        1   /* Buyer Pack */
#define PATPTT_V        2   /* Vendor Pack */

#define _PATPTT_MAX 2


/* PAYM = Payment Method */
extern const char PAYM[NULL_CODE];

extern const char PAYM_LC    [NULL_CODE];   /* Letter of Credit */
extern const char PAYM_OA    [NULL_CODE];   /* Open Account */
extern const char PAYM_WT    [NULL_CODE];   /* Wire Transfer */

#define PAYMTT_LC       1   /* Letter of Credit */
#define PAYMTT_OA       2   /* Open Account */
#define PAYMTT_WT       3   /* Wire Transfer */

#define _PAYMTT_MAX 3


/* PAYT = CASH,MORD,INVC list box */
extern const char PAYT[NULL_CODE];

extern const char PAYT_CASH  [NULL_CODE];   /* Cash */
extern const char PAYT_INVC  [NULL_CODE];   /* Invoice */

#define PAYTTT_CASH     1   /* Cash */
#define PAYTTT_INVC     3   /* Invoice */

#define _PAYTTT_MAX 3


/* PCAC = Price Change Action List */
extern const char PCAC[NULL_CODE];

extern const char PCAC_A     [NULL_CODE];   /* New */
extern const char PCAC_B     [NULL_CODE];   /* View */
extern const char PCAC_C     [NULL_CODE];   /* Edit */

#define PCACTT_A        1   /* New */
#define PCACTT_B        2   /* View */
#define PCACTT_C        3   /* Edit */

#define _PCACTT_MAX 3


/* PCCH = Overlap Types */
extern const char PCCH[NULL_CODE];

extern const char PCCH_PC    [NULL_CODE];   /* Price Change */
extern const char PCCH_PR    [NULL_CODE];   /* Promotion */
extern const char PCCH_CL    [NULL_CODE];   /* Clearance */
extern const char PCCH_ZC    [NULL_CODE];   /* Zone Type */

#define PCCHTT_PC       1   /* Price Change */
#define PCCHTT_PR       2   /* Promotion */
#define PCCHTT_CL       3   /* Clearance */
#define PCCHTT_ZC       4   /* Zone Type */

#define _PCCHTT_MAX 4


/* PCET = Price change event type */
extern const char PCET[NULL_CODE];

extern const char PCET_BASERT[NULL_CODE];   /* Base retail */
extern const char PCET_PROMS [NULL_CODE];   /* Promotion Start */
extern const char PCET_PROME [NULL_CODE];   /* Promotion End */
extern const char PCET_REG   [NULL_CODE];   /* Regular */
extern const char PCET_CLRS  [NULL_CODE];   /* Clearance Start */
extern const char PCET_CLRE  [NULL_CODE];   /* Clearance End */

#define PCETTT_BASERT   1   /* Base retail */
#define PCETTT_PROMS    2   /* Promotion Start */
#define PCETTT_PROME    3   /* Promotion End */
#define PCETTT_REG      4   /* Regular */
#define PCETTT_CLRS     5   /* Clearance Start */
#define PCETTT_CLRE     6   /* Clearance End */

#define _PCETTT_MAX 6


/* PCIT = Price Change Item */
extern const char PCIT[NULL_CODE];

extern const char PCIT_A     [NULL_CODE];   /* Single Item */
extern const char PCIT_B     [NULL_CODE];   /* Item List */
extern const char PCIT_C     [NULL_CODE];   /* Item Parent/Diff */

#define PCITTT_A        1   /* Single Item */
#define PCITTT_B        2   /* Item List */
#define PCITTT_C        3   /* Item Parent/Diff */

#define _PCITTT_MAX 3


/* PCKT = Packaging Type */
extern const char PCKT[NULL_CODE];

extern const char PCKT_JHOOK [NULL_CODE];   /* Peggable Packaging */
extern const char PCKT_STACK [NULL_CODE];   /* Stackable Packaging */

#define PCKTTT_JHOOK    1   /* Peggable Packaging */
#define PCKTTT_STACK    2   /* Stackable Packaging */

#define _PCKTTT_MAX 2


/* PCLA = Product Classification */
extern const char PCLA[NULL_CODE];

extern const char PCLA_ELC   [NULL_CODE];   /* Electronics */
extern const char PCLA_GRC   [NULL_CODE];   /* Grocery */
extern const char PCLA_APP   [NULL_CODE];   /* Apparel */

#define PCLATT_ELC      1   /* Electronics */
#define PCLATT_GRC      2   /* Grocery */
#define PCLATT_APP      3   /* Apparel */

#define _PCLATT_MAX 3


/* PCLB = Price Change Label */
extern const char PCLB[NULL_CODE];

extern const char PCLB_R     [NULL_CODE];   /* Regular Price Change */
extern const char PCLB_S     [NULL_CODE];   /* Item Zone Group Changed */

#define PCLBTT_R        1   /* Regular Price Change */
#define PCLBTT_S        2   /* Item Zone Group Changed */

#define _PCLBTT_MAX 2


/* PCLO = Price Change Location List */
extern const char PCLO[NULL_CODE];

extern const char PCLO_C     [NULL_CODE];   /* Store Class */
extern const char PCLO_D     [NULL_CODE];   /* @OH5@ */
extern const char PCLO_R     [NULL_CODE];   /* @OH4@ */
extern const char PCLO_S     [NULL_CODE];   /* Store */
extern const char PCLO_T     [NULL_CODE];   /* Transfer Zone */
extern const char PCLO_L     [NULL_CODE];   /* Location Traits */
extern const char PCLO_A     [NULL_CODE];   /* All Stores */
extern const char PCLO_LL    [NULL_CODE];   /* Location List */

#define PCLOTT_C        1   /* Store Class */
#define PCLOTT_D        2   /* @OH5@ */
#define PCLOTT_R        4   /* @OH4@ */
#define PCLOTT_S        5   /* Store */
#define PCLOTT_T        6   /* Transfer Zone */
#define PCLOTT_L        7   /* Location Traits */
#define PCLOTT_A        8   /* All Stores */
#define PCLOTT_LL       9   /* Location List */

#define _PCLOTT_MAX 9


/* PCLR = POS Color */
extern const char PCLR[NULL_CODE];

extern const char PCLR_BLACK [NULL_CODE];   /* Black */
extern const char PCLR_BLUE  [NULL_CODE];   /* Blue */
extern const char PCLR_CYAN  [NULL_CODE];   /* Cyan */
extern const char PCLR_GRAY  [NULL_CODE];   /* Gray */
extern const char PCLR_GREEN [NULL_CODE];   /* Green */
extern const char PCLR_KHAKI [NULL_CODE];   /* Khaki */
extern const char PCLR_LBLUE [NULL_CODE];   /* Light Blue */
extern const char PCLR_LGREEN[NULL_CODE];   /* Light Green */
extern const char PCLR_LPURPL[NULL_CODE];   /* Light Purple */
extern const char PCLR_MAGNTA[NULL_CODE];   /* Magenta */
extern const char PCLR_ORANGE[NULL_CODE];   /* Orange */
extern const char PCLR_PINK  [NULL_CODE];   /* Pink */
extern const char PCLR_PURPLE[NULL_CODE];   /* Purple */
extern const char PCLR_RED   [NULL_CODE];   /* Red */
extern const char PCLR_WHITE [NULL_CODE];   /* White */
extern const char PCLR_YELLOW[NULL_CODE];   /* Yellow */

#define PCLRTT_BLACK    1   /* Black */
#define PCLRTT_BLUE     2   /* Blue */
#define PCLRTT_CYAN     3   /* Cyan */
#define PCLRTT_GRAY     4   /* Gray */
#define PCLRTT_GREEN    5   /* Green */
#define PCLRTT_KHAKI    6   /* Khaki */
#define PCLRTT_LBLUE    7   /* Light Blue */
#define PCLRTT_LGREEN   8   /* Light Green */
#define PCLRTT_LPURPL   9   /* Light Purple */
#define PCLRTT_MAGNTA  10   /* Magenta */
#define PCLRTT_ORANGE  11   /* Orange */
#define PCLRTT_PINK    12   /* Pink */
#define PCLRTT_PURPLE  13   /* Purple */
#define PCLRTT_RED     14   /* Red */
#define PCLRTT_WHITE   15   /* White */
#define PCLRTT_YELLOW  16   /* Yellow */

#define _PCLRTT_MAX 16


/* PCO1 = Price Change Organization (zones) */
extern const char PCO1[NULL_CODE];

extern const char PCO1_1     [NULL_CODE];   /* Store */
extern const char PCO1_2     [NULL_CODE];   /* Store Class */
extern const char PCO1_3     [NULL_CODE];   /* @OH5@ */
extern const char PCO1_4     [NULL_CODE];   /* Transfer Zone */
extern const char PCO1_5     [NULL_CODE];   /* @OH4@ */
extern const char PCO1_6     [NULL_CODE];   /* Pricing Zone */

#define PCO1TT_1        1   /* Store */
#define PCO1TT_2        2   /* Store Class */
#define PCO1TT_3        3   /* @OH5@ */
#define PCO1TT_4        4   /* Transfer Zone */
#define PCO1TT_5        5   /* @OH4@ */
#define PCO1TT_6        6   /* Pricing Zone */

#define _PCO1TT_MAX 6


/* PCON = POS Configurations */
extern const char PCON[NULL_CODE];

extern const char PCON_ALL   [NULL_CODE];   /* All */
extern const char PCON_PRES  [NULL_CODE];   /* Product Restrictions */
extern const char PCON_COUP  [NULL_CODE];   /* Coupons */

#define PCONTT_ALL      1   /* All */
#define PCONTT_PRES     3   /* Product Restrictions */
#define PCONTT_COUP     7   /* Coupons */

#define _PCONTT_MAX 7


/* PCOR = Price Change Find Origin */
extern const char PCOR[NULL_CODE];

extern const char PCOR_I     [NULL_CODE];   /* Retail */
extern const char PCOR_C     [NULL_CODE];   /* Cost */

#define PCORTT_I        1   /* Retail */
#define PCORTT_C        2   /* Cost */

#define _PCORTT_MAX 2


/* PCOS = Price Change Overlap Status */
extern const char PCOS[NULL_CODE];

extern const char PCOS_W     [NULL_CODE];   /* Worksheet */
extern const char PCOS_S     [NULL_CODE];   /* Submitted */
extern const char PCOS_A     [NULL_CODE];   /* Approved */
extern const char PCOS_E     [NULL_CODE];   /* Extracted */
extern const char PCOS_R     [NULL_CODE];   /* Rejected */
extern const char PCOS_C     [NULL_CODE];   /* Cancelled */
extern const char PCOS_D     [NULL_CODE];   /* Deleted */
extern const char PCOS_T     [NULL_CODE];   /* Immediate */

#define PCOSTT_W        1   /* Worksheet */
#define PCOSTT_S        2   /* Submitted */
#define PCOSTT_A        3   /* Approved */
#define PCOSTT_E        4   /* Extracted */
#define PCOSTT_R        5   /* Rejected */
#define PCOSTT_C        6   /* Cancelled */
#define PCOSTT_D        7   /* Deleted */
#define PCOSTT_T        8   /* Immediate */

#define _PCOSTT_MAX 8


/* PCOV = Price Change Overlap (abbrev) */
extern const char PCOV[NULL_CODE];

extern const char PCOV_A     [NULL_CODE];   /* Apprv */
extern const char PCOV_E     [NULL_CODE];   /* Extr */
extern const char PCOV_S     [NULL_CODE];   /* Submt */
extern const char PCOV_R     [NULL_CODE];   /* Rejct */
extern const char PCOV_D     [NULL_CODE];   /* Del */
extern const char PCOV_C     [NULL_CODE];   /* Canc */
extern const char PCOV_T     [NULL_CODE];   /* Immdt */
extern const char PCOV_W     [NULL_CODE];   /* Work */

#define PCOVTT_A        1   /* Apprv */
#define PCOVTT_E        2   /* Extr */
#define PCOVTT_S        3   /* Submt */
#define PCOVTT_R        4   /* Rejct */
#define PCOVTT_D        5   /* Del */
#define PCOVTT_C        6   /* Canc */
#define PCOVTT_T        7   /* Immdt */
#define PCOVTT_W        8   /* Work */

#define _PCOVTT_MAX 8


/* PCPD = Price Change Pending */
extern const char PCPD[NULL_CODE];

extern const char PCPD_A     [NULL_CODE];   /* All */
extern const char PCPD_NP    [NULL_CODE];   /* No Price Change Pending */
extern const char PCPD_PP    [NULL_CODE];   /* Price Change Pending */

#define PCPDTT_A        1   /* All */
#define PCPDTT_NP       2   /* No Price Change Pending */
#define PCPDTT_PP       3   /* Price Change Pending */

#define _PCPDTT_MAX 3


/* PCSC = Price Change Style/Color */
extern const char PCSC[NULL_CODE];

extern const char PCSC_A     [NULL_CODE];   /* Single Item */
extern const char PCSC_B     [NULL_CODE];   /* Item List */
extern const char PCSC_C     [NULL_CODE];   /* Item Parent/Diff */

#define PCSCTT_A        1   /* Single Item */
#define PCSCTT_B        2   /* Item List */
#define PCSCTT_C        3   /* Item Parent/Diff */

#define _PCSCTT_MAX 3


/* PCST = Price Change Status */
extern const char PCST[NULL_CODE];

extern const char PCST_0     [NULL_CODE];   /* New Item Added */
extern const char PCST_2     [NULL_CODE];   /* Unit Cost Changed */
extern const char PCST_4     [NULL_CODE];   /* Unit Retail Changed */
extern const char PCST_8     [NULL_CODE];   /* Unit Retail Changed/Clearance */
extern const char PCST_9     [NULL_CODE];   /* Unit Retail Changed/Promotions */
extern const char PCST_10    [NULL_CODE];   /* Multi-Unit Retail Changed */
extern const char PCST_11    [NULL_CODE];   /* Multi/Single - U Retail Changed */
extern const char PCST_99    [NULL_CODE];   /* Multi/Single - U Retail Changed */
extern const char PCST_R     [NULL_CODE];   /* All Retail Changes */

#define PCSTTT_0        1   /* New Item Added */
#define PCSTTT_2        2   /* Unit Cost Changed */
#define PCSTTT_4        3   /* Unit Retail Changed */
#define PCSTTT_8        4   /* Unit Retail Changed/Clearance */
#define PCSTTT_9        5   /* Unit Retail Changed/Promotions */
#define PCSTTT_10       6   /* Multi-Unit Retail Changed */
#define PCSTTT_11       7   /* Multi/Single - U Retail Changed */
#define PCSTTT_99       8   /* Multi/Single - U Retail Changed */
#define PCSTTT_R        9   /* All Retail Changes */

#define _PCSTTT_MAX 9


/* PCT1 = Price Change Tran Type (abbrev) */
extern const char PCT1[NULL_CODE];

extern const char PCT1_R     [NULL_CODE];   /* Regular */
extern const char PCT1_S     [NULL_CODE];   /* Zone Grp. */

#define PCT1TT_R        1   /* Regular */
#define PCT1TT_S        2   /* Zone Grp. */

#define _PCT1TT_MAX 2


/* PCT2 = Price Change List (condensed) */
extern const char PCT2[NULL_CODE];

extern const char PCT2_MA    [NULL_CODE];   /* Change Price By Amount */
extern const char PCT2_PC    [NULL_CODE];   /* Change Price by % */
extern const char PCT2_PB    [NULL_CODE];   /* Set Price to % of Base */
extern const char PCT2_NP    [NULL_CODE];   /* Set Price to Set Amount */
extern const char PCT2_RP    [NULL_CODE];   /* Reset POS Price */

#define PCT2TT_MA       1   /* Change Price By Amount */
#define PCT2TT_PC       2   /* Change Price by % */
#define PCT2TT_PB       3   /* Set Price to % of Base */
#define PCT2TT_NP       4   /* Set Price to Set Amount */
#define PCT2TT_RP       5   /* Reset POS Price */

#define _PCT2TT_MAX 5


/* PCT3 = Price Change List (clearance) */
extern const char PCT3[NULL_CODE];

extern const char PCT3_MA    [NULL_CODE];   /* Reduce Price By Amount */
extern const char PCT3_PC    [NULL_CODE];   /* Reduce Price by % */
extern const char PCT3_PB    [NULL_CODE];   /* Set Price to % of Base */
extern const char PCT3_NP    [NULL_CODE];   /* Set Price to a Set Amount */

#define PCT3TT_MA       1   /* Reduce Price By Amount */
#define PCT3TT_PC       2   /* Reduce Price by % */
#define PCT3TT_PB       3   /* Set Price to % of Base */
#define PCT3TT_NP       4   /* Set Price to a Set Amount */

#define _PCT3TT_MAX 4


/* PCT4 = Price Change Type (pcstbysk) */
extern const char PCT4[NULL_CODE];

extern const char PCT4_MA    [NULL_CODE];   /* Change Price by Amount */
extern const char PCT4_PC    [NULL_CODE];   /* Change Price by % */
extern const char PCT4_PB    [NULL_CODE];   /* Set Price to % of Base */
extern const char PCT4_NP    [NULL_CODE];   /* Set Price to Set Amount */
extern const char PCT4_OS    [NULL_CODE];   /* Reset to Original Store Price */
extern const char PCT4_SB    [NULL_CODE];   /* Reset Store */

#define PCT4TT_MA       1   /* Change Price by Amount */
#define PCT4TT_PC       2   /* Change Price by % */
#define PCT4TT_PB       3   /* Set Price to % of Base */
#define PCT4TT_NP       4   /* Set Price to Set Amount */
#define PCT4TT_OS       5   /* Reset to Original Store Price */
#define PCT4TT_SB       6   /* Reset Store */

#define _PCT4TT_MAX 6


/* PCT5 = Price Change Type (pcstbysk1) */
extern const char PCT5[NULL_CODE];

extern const char PCT5_MA    [NULL_CODE];   /* Change Price by Amount */
extern const char PCT5_PC    [NULL_CODE];   /* Change Price by % */
extern const char PCT5_PB    [NULL_CODE];   /* Set Price to % of Base */
extern const char PCT5_NP    [NULL_CODE];   /* Set Price to Set Amount */
extern const char PCT5_SB    [NULL_CODE];   /* Reset Store */

#define PCT5TT_MA       1   /* Change Price by Amount */
#define PCT5TT_PC       2   /* Change Price by % */
#define PCT5TT_PB       3   /* Set Price to % of Base */
#define PCT5TT_NP       4   /* Set Price to Set Amount */
#define PCT5TT_SB       5   /* Reset Store */

#define _PCT5TT_MAX 5


/* PCTL = Price Change Type List */
extern const char PCTL[NULL_CODE];

extern const char PCTL_MA    [NULL_CODE];   /* Change Price By Amount */
extern const char PCTL_PC    [NULL_CODE];   /* Change Price by % */
extern const char PCTL_PB    [NULL_CODE];   /* Set Price to % of Base */
extern const char PCTL_NP    [NULL_CODE];   /* Set Price to a Set Amount */
extern const char PCTL_RP    [NULL_CODE];   /* Reset POS Price */
extern const char PCTL_OS    [NULL_CODE];   /* Reset to Original Store Price */
extern const char PCTL_SB    [NULL_CODE];   /* Reset Store */

#define PCTLTT_MA       1   /* Change Price By Amount */
#define PCTLTT_PC       2   /* Change Price by % */
#define PCTLTT_PB       3   /* Set Price to % of Base */
#define PCTLTT_NP       4   /* Set Price to a Set Amount */
#define PCTLTT_RP       5   /* Reset POS Price */
#define PCTLTT_OS       6   /* Reset to Original Store Price */
#define PCTLTT_SB       7   /* Reset Store */

#define _PCTLTT_MAX 7


/* PCTP = Price Change Exception Type */
extern const char PCTP[NULL_CODE];

extern const char PCTP_MA    [NULL_CODE];   /* Change Current Price by Amount */
extern const char PCTP_NC    [NULL_CODE];   /* No Change */
extern const char PCTP_NP    [NULL_CODE];   /* Set Price to an Amount */
extern const char PCTP_OS    [NULL_CODE];   /* Reset to Original Store Price */
extern const char PCTP_PB    [NULL_CODE];   /* Set Price to Percentage of Base */
extern const char PCTP_PC    [NULL_CODE];   /* Change Current Price by Percentage */
extern const char PCTP_RP    [NULL_CODE];   /* Reset POS Price */
extern const char PCTP_SB    [NULL_CODE];   /* Reset Store */

#define PCTPTT_MA       1   /* Change Current Price by Amount */
#define PCTPTT_NC       2   /* No Change */
#define PCTPTT_NP       3   /* Set Price to an Amount */
#define PCTPTT_OS       4   /* Reset to Original Store Price */
#define PCTPTT_PB       5   /* Set Price to Percentage of Base */
#define PCTPTT_PC       6   /* Change Current Price by Percentage */
#define PCTPTT_RP       7   /* Reset POS Price */
#define PCTPTT_SB       8   /* Reset Store */

#define _PCTPTT_MAX 8


/* PCTT = Price Change Transaction Type */
extern const char PCTT[NULL_CODE];

extern const char PCTT_R     [NULL_CODE];   /* Regular Price Change */
extern const char PCTT_S     [NULL_CODE];   /* Item Changed Zone Group */

#define PCTTTT_R        1   /* Regular Price Change */
#define PCTTTT_S        2   /* Item Changed Zone Group */

#define _PCTTTT_MAX 2


/* PCTY = Price Type */
extern const char PCTY[NULL_CODE];

extern const char PCTY_R     [NULL_CODE];   /* Regular */
extern const char PCTY_P     [NULL_CODE];   /* Promotion */
extern const char PCTY_C     [NULL_CODE];   /* Clearance */

#define PCTYTT_R        1   /* Regular */
#define PCTYTT_P        2   /* Promotion */
#define PCTYTT_C        3   /* Clearance */

#define _PCTYTT_MAX 3


/* PERT = Period Type */
extern const char PERT[NULL_CODE];

extern const char PERT_H     [NULL_CODE];   /* Half */
extern const char PERT_M     [NULL_CODE];   /* Month Ending */
extern const char PERT_W     [NULL_CODE];   /* Week Ending */

#define PERTTT_H        1   /* Half */
#define PERTTT_M        2   /* Month Ending */
#define PERTTT_W        3   /* Week Ending */

#define _PERTTT_MAX 3


/* PEST = Promotion Extract Status */
extern const char PEST[NULL_CODE];

extern const char PEST_E     [NULL_CODE];   /* Extracted */
extern const char PEST_M     [NULL_CODE];   /* Promotion Complete */

#define PESTTT_E        1   /* Extracted */
#define PESTTT_M        2   /* Promotion Complete */

#define _PESTTT_MAX 2


/* PETP = Item Price Event Type */
extern const char PETP[NULL_CODE];

extern const char PETP_REG   [NULL_CODE];   /* Regular Price Change */
extern const char PETP_CLEAR [NULL_CODE];   /* Clearance Event */
extern const char PETP_COST  [NULL_CODE];   /* Cost Change */

#define PETPTT_REG      1   /* Regular Price Change */
#define PETPTT_CLEAR    2   /* Clearance Event */
#define PETPTT_COST     3   /* Cost Change */

#define _PETPTT_MAX 3


/* PEZT = Zone Price Event Type */
extern const char PEZT[NULL_CODE];

extern const char PEZT_REG   [NULL_CODE];   /* Regular Price Change */
extern const char PEZT_CLEAR [NULL_CODE];   /* Clearance Event */

#define PEZTTT_REG      1   /* Regular Price Change */
#define PEZTTT_CLEAR    2   /* Clearance Event */

#define _PEZTTT_MAX 2


/* PFTP = Profit Calculation Type */
extern const char PFTP[NULL_CODE];

extern const char PFTP_1     [NULL_CODE];   /* Direct Cost */
extern const char PFTP_2     [NULL_CODE];   /* Retail Inventory */

#define PFTPTT_1        1   /* Direct Cost */
#define PFTPTT_2        2   /* Retail Inventory */

#define _PFTPTT_MAX 2


/* PITL = Pack Item Level Descriptions */
extern const char PITL[NULL_CODE];

extern const char PITL_1     [NULL_CODE];   /* Pack */
extern const char PITL_2     [NULL_CODE];   /* Reference Item */

#define PITLTT_1        1   /* Pack */
#define PITLTT_2        2   /* Reference Item */

#define _PITLTT_MAX 2


/* PKMT = Packing Method */
extern const char PKMT[NULL_CODE];

extern const char PKMT_HANG  [NULL_CODE];   /* Hanging */
extern const char PKMT_FLAT  [NULL_CODE];   /* Flat */

#define PKMTTT_HANG     1   /* Hanging */
#define PKMTTT_FLAT     2   /* Flat */

#define _PKMTTT_MAX 2


/* PLS1 = Pricing Level Select Type 1 */
extern const char PLS1[NULL_CODE];

extern const char PLS1_S     [NULL_CODE];   /* Store */
extern const char PLS1_O     [NULL_CODE];   /* All Stores */

#define PLS1TT_S        1   /* Store */
#define PLS1TT_O        2   /* All Stores */

#define _PLS1TT_MAX 2


/* PLST = Pricing Level Select Type */
extern const char PLST[NULL_CODE];

extern const char PLST_Z     [NULL_CODE];   /* Zone */
extern const char PLST_A     [NULL_CODE];   /* All Zone */

#define PLSTTT_Z        1   /* Zone */
#define PLSTTT_A        2   /* All Zone */

#define _PLSTTT_MAX 2


/* PMBT = Promotion Mix Match Buy Type */
extern const char PMBT[NULL_CODE];

extern const char PMBT_A     [NULL_CODE];   /* All */
extern const char PMBT_U     [NULL_CODE];   /* Any */

#define PMBTTT_A        1   /* All */
#define PMBTTT_U        2   /* Any */

#define _PMBTTT_MAX 2


/* PMGT = Promotion Mix Match Get Type */
extern const char PMGT[NULL_CODE];

extern const char PMGT_P     [NULL_CODE];   /* Percent Off */
extern const char PMGT_A     [NULL_CODE];   /* Amount Off */
extern const char PMGT_F     [NULL_CODE];   /* Fixed Amount */

#define PMGTTT_P        1   /* Percent Off */
#define PMGTTT_A        2   /* Amount Off */
#define PMGTTT_F        3   /* Fixed Amount */

#define _PMGTTT_MAX 3


/* POPS = Proof of Performance Search */
extern const char POPS[NULL_CODE];

extern const char POPS_D     [NULL_CODE];   /* Deal */
extern const char POPS_DD    [NULL_CODE];   /* Deal Detail/Component */
extern const char POPS_DIL   [NULL_CODE];   /* Deal Item/Location */
extern const char POPS_I     [NULL_CODE];   /* Item */
extern const char POPS_L     [NULL_CODE];   /* Location */

#define POPSTT_D        1   /* Deal */
#define POPSTT_DD       2   /* Deal Detail/Component */
#define POPSTT_DIL      3   /* Deal Item/Location */
#define POPSTT_I        4   /* Item */
#define POPSTT_L        5   /* Location */

#define _POPSTT_MAX 5


/* POPT = PO Print Type */
extern const char POPT[NULL_CODE];

extern const char POPT_A     [NULL_CODE];   /* On Approval */
extern const char POPT_R     [NULL_CODE];   /* On Receipt */

#define POPTTT_A        1   /* On Approval */
#define POPTTT_R        2   /* On Receipt */

#define _POPTTT_MAX 2


/* POSC = POS Configuration Types */
extern const char POSC[NULL_CODE];

extern const char POSC_COUP  [NULL_CODE];   /* Coupon */
extern const char POSC_PRES  [NULL_CODE];   /* Product Restriction */

#define POSCTT_COUP     1   /* Coupon */
#define POSCTT_PRES     3   /* Product Restriction */

#define _POSCTT_MAX 3


/* POSL = POS Locations Types */
extern const char POSL[NULL_CODE];

extern const char POSL_A     [NULL_CODE];   /* @OH3@ */
extern const char POSL_R     [NULL_CODE];   /* @OH4@ */
extern const char POSL_D     [NULL_CODE];   /* @OH5@ */
extern const char POSL_S     [NULL_CODE];   /* Store */
extern const char POSL_AS    [NULL_CODE];   /* All Stores */
extern const char POSL_LL    [NULL_CODE];   /* Location List */

#define POSLTT_A        1   /* @OH3@ */
#define POSLTT_R        2   /* @OH4@ */
#define POSLTT_D        3   /* @OH5@ */
#define POSLTT_S        4   /* Store */
#define POSLTT_AS       5   /* All Stores */
#define POSLTT_LL       6   /* Location List */

#define _POSLTT_MAX 6


/* POSM = POS Upload Transaction Types */
extern const char POSM[NULL_CODE];

extern const char POSM_1     [NULL_CODE];   /* New Item Added */
extern const char POSM_2     [NULL_CODE];   /* Added Child to Item */
extern const char POSM_10    [NULL_CODE];   /* Change Short Desc. of Existing Item */
extern const char POSM_11    [NULL_CODE];   /* Change Price of an Existing Item */
extern const char POSM_12    [NULL_CODE];   /* Change Description of an Existing Item */
extern const char POSM_13    [NULL_CODE];   /* Change @MH4@/@MH5@/@MH6@ of Existing Item */
extern const char POSM_16    [NULL_CODE];   /* Put Item on Clearance */
extern const char POSM_17    [NULL_CODE];   /* Change Existing Item Clearance Price */
extern const char POSM_18    [NULL_CODE];   /* Remove Item from Clearance and Reset */
extern const char POSM_21    [NULL_CODE];   /* Delete Existing Item */
extern const char POSM_22    [NULL_CODE];   /* Delete Child from Item */
extern const char POSM_25    [NULL_CODE];   /* Change Item Status */
extern const char POSM_26    [NULL_CODE];   /* Change Item Taxable Indicator */
extern const char POSM_31    [NULL_CODE];   /* Promotional Item - Start Maintenance */
extern const char POSM_32    [NULL_CODE];   /* Promotional Item - End Maintenance */

#define POSMTT_1        1   /* New Item Added */
#define POSMTT_2        2   /* Added Child to Item */
#define POSMTT_10       3   /* Change Short Desc. of Existing Item */
#define POSMTT_11       4   /* Change Price of an Existing Item */
#define POSMTT_12       5   /* Change Description of an Existing Item */
#define POSMTT_13       6   /* Change @MH4@/@MH5@/@MH6@ of Existing Item */
#define POSMTT_16       7   /* Put Item on Clearance */
#define POSMTT_17       8   /* Change Existing Item Clearance Price */
#define POSMTT_18       9   /* Remove Item from Clearance and Reset */
#define POSMTT_21      10   /* Delete Existing Item */
#define POSMTT_22      11   /* Delete Child from Item */
#define POSMTT_25      12   /* Change Item Status */
#define POSMTT_26      13   /* Change Item Taxable Indicator */
#define POSMTT_31      14   /* Promotional Item - Start Maintenance */
#define POSMTT_32      15   /* Promotional Item - End Maintenance */

#define _POSMTT_MAX 15


/* POST = Sales Audit POS types */
extern const char POST[NULL_CODE];

extern const char POST_1     [NULL_CODE];   /* POS type 1 */
extern const char POST_2     [NULL_CODE];   /* POS type 2 */

#define POSTTT_1        1   /* POS type 1 */
#define POSTTT_2        2   /* POS type 2 */

#define _POSTTT_MAX 2


/* PPSS = Promotion Status for Item/Store */
extern const char PPSS[NULL_CODE];

extern const char PPSS_PC    [NULL_CODE];   /* Price Change */
extern const char PPSS_AI    [NULL_CODE];   /* Added Item */
extern const char PPSS_DI    [NULL_CODE];   /* Deleted Item */
extern const char PPSS_DP    [NULL_CODE];   /* Delete Processed */

#define PPSSTT_PC       1   /* Price Change */
#define PPSSTT_AI       2   /* Added Item */
#define PPSSTT_DI       3   /* Deleted Item */
#define PPSSTT_DP       4   /* Delete Processed */

#define _PPSSTT_MAX 4


/* PPT = Proof of Performance Types */
extern const char PPT[NULL_CODE];

extern const char PPT_ECD   [NULL_CODE];   /* End Cap Display */
extern const char PPT_NAD   [NULL_CODE];   /* Newspaper Ad */
extern const char PPT_CAD   [NULL_CODE];   /* In Store Circular Ad */
extern const char PPT_RAD   [NULL_CODE];   /* Radio Ad */
extern const char PPT_TASTE [NULL_CODE];   /* Tasting/In Store Demonstration */
extern const char PPT_PROMO [NULL_CODE];   /* Promotion */
extern const char PPT_COUP  [NULL_CODE];   /* Coupon */
extern const char PPT_OTHER [NULL_CODE];   /* Other */

#define PPTTT_ECD      1   /* End Cap Display */
#define PPTTT_NAD      2   /* Newspaper Ad */
#define PPTTT_CAD      3   /* In Store Circular Ad */
#define PPTTT_RAD      4   /* Radio Ad */
#define PPTTT_TASTE    5   /* Tasting/In Store Demonstration */
#define PPTTT_PROMO    6   /* Promotion */
#define PPTTT_COUP     7   /* Coupon */
#define PPTTT_OTHER    8   /* Other */

#define _PPTTT_MAX 8


/* PPTP = Prepack Type */
extern const char PPTP[NULL_CODE];

extern const char PPTP_S     [NULL_CODE];   /* Standard */
extern const char PPTP_N     [NULL_CODE];   /* Non-Standard */

#define PPTPTT_S        1   /* Standard */
#define PPTPTT_N        2   /* Non-Standard */

#define _PPTPTT_MAX 2


/* PPVT = Proof of Performance Type Values */
extern const char PPVT[NULL_CODE];

extern const char PPVT_DAYS  [NULL_CODE];   /* Day(s) */
extern const char PPVT_WEEKS [NULL_CODE];   /* Week(s) */
extern const char PPVT_MONTH [NULL_CODE];   /* Month(s) */
extern const char PPVT_INST  [NULL_CODE];   /* Instances */

#define PPVTTT_DAYS     1   /* Day(s) */
#define PPVTTT_WEEKS    2   /* Week(s) */
#define PPVTTT_MONTH    3   /* Month(s) */
#define PPVTTT_INST     4   /* Instances */

#define _PPVTTT_MAX 4


/* PRAT = Promotion Adjust Types */
extern const char PRAT[NULL_CODE];

extern const char PRAT_RO    [NULL_CODE];   /* Round */
extern const char PRAT_EI    [NULL_CODE];   /* Ends In */
extern const char PRAT_PP    [NULL_CODE];   /* Price Point */
extern const char PRAT_NO    [NULL_CODE];   /* None */

#define PRATTT_RO       1   /* Round */
#define PRATTT_EI       2   /* Ends In */
#define PRATTT_PP       3   /* Price Point */
#define PRATTT_NO       4   /* None */

#define _PRATTT_MAX 4


/* PRCO = Customs Entry Protest Codes */
extern const char PRCO[NULL_CODE];

extern const char PRCO_1     [NULL_CODE];   /* Protest Code 1 */
extern const char PRCO_2     [NULL_CODE];   /* Protest Code 2 */
extern const char PRCO_3     [NULL_CODE];   /* Protest Code 3 */

#define PRCOTT_1        1   /* Protest Code 1 */
#define PRCOTT_2        2   /* Protest Code 2 */
#define PRCOTT_3        3   /* Protest Code 3 */

#define _PRCOTT_MAX 3


/* PREF = Prefix of Variable Weight PLU Item Number */
extern const char PREF[NULL_CODE];

extern const char PREF_2     [NULL_CODE];   /* 2 */
extern const char PREF_20    [NULL_CODE];   /* 20 */
extern const char PREF_21    [NULL_CODE];   /* 21 */
extern const char PREF_22    [NULL_CODE];   /* 22 */
extern const char PREF_23    [NULL_CODE];   /* 23 */
extern const char PREF_24    [NULL_CODE];   /* 24 */
extern const char PREF_25    [NULL_CODE];   /* 25 */
extern const char PREF_26    [NULL_CODE];   /* 26 */
extern const char PREF_27    [NULL_CODE];   /* 27 */
extern const char PREF_28    [NULL_CODE];   /* 28 */
extern const char PREF_29    [NULL_CODE];   /* 29 */

#define PREFTT_2        1   /* 2 */
#define PREFTT_20       2   /* 20 */
#define PREFTT_21       3   /* 21 */
#define PREFTT_22       4   /* 22 */
#define PREFTT_23       5   /* 23 */
#define PREFTT_24       6   /* 24 */
#define PREFTT_25       7   /* 25 */
#define PREFTT_26       8   /* 26 */
#define PREFTT_27       9   /* 27 */
#define PREFTT_28      10   /* 28 */
#define PREFTT_29      11   /* 29 */

#define _PREFTT_MAX 11


/* PREM = Promotion Item Type */
extern const char PREM[NULL_CODE];

extern const char PREM_R     [NULL_CODE];   /* Regular Price Items */
extern const char PREM_C     [NULL_CODE];   /* Clearance Price Items */
extern const char PREM_B     [NULL_CODE];   /* Reg and Clear Price Items */

#define PREMTT_R        1   /* Regular Price Items */
#define PREMTT_C        2   /* Clearance Price Items */
#define PREMTT_B        3   /* Reg and Clear Price Items */

#define _PREMTT_MAX 3


/* PRES = Product Restrictions */
extern const char PRES[NULL_CODE];

extern const char PRES_STMP  [NULL_CODE];   /* Food Stamp */
extern const char PRES_MNAG  [NULL_CODE];   /* Minimum Age */
extern const char PRES_CNDP  [NULL_CODE];   /* Container Deposit */
extern const char PRES_CNVL  [NULL_CODE];   /* Container Redemption Value */
extern const char PRES_DTDR  [NULL_CODE];   /* Day/Time/Date */
extern const char PRES_TENT  [NULL_CODE];   /* Tender Type */
extern const char PRES_NDSC  [NULL_CODE];   /* Non-Discountable */
extern const char PRES_RTRN  [NULL_CODE];   /* Returnable */
extern const char PRES_QLMT  [NULL_CODE];   /* Quantity Limit */

#define PRESTT_STMP     1   /* Food Stamp */
#define PRESTT_MNAG     2   /* Minimum Age */
#define PRESTT_CNDP     3   /* Container Deposit */
#define PRESTT_CNVL     4   /* Container Redemption Value */
#define PRESTT_DTDR     5   /* Day/Time/Date */
#define PRESTT_TENT     6   /* Tender Type */
#define PRESTT_NDSC     7   /* Non-Discountable */
#define PRESTT_RTRN     8   /* Returnable */
#define PRESTT_QLMT     9   /* Quantity Limit */

#define _PRESTT_MAX 9


/* PRET = Product Restriction Report Type */
extern const char PRET[NULL_CODE];

extern const char PRET_C     [NULL_CODE];   /* Merchandise Criteria */
extern const char PRET_S     [NULL_CODE];   /* Associated Stores */
extern const char PRET_I     [NULL_CODE];   /* Associated Items */

#define PRETTT_C        1   /* Merchandise Criteria */
#define PRETTT_S        2   /* Associated Stores */
#define PRETTT_I        3   /* Associated Items */

#define _PRETTT_MAX 3


/* PRFT = Profile Types */
extern const char PRFT[NULL_CODE];

extern const char PRFT_Z     [NULL_CODE];   /* Zone */
extern const char PRFT_C     [NULL_CODE];   /* Country */

#define PRFTTT_Z        1   /* Zone */
#define PRFTTT_C        2   /* Country */

#define _PRFTTT_MAX 2


/* PRMI = Item types for promotion */
extern const char PRMI[NULL_CODE];

extern const char PRMI_R     [NULL_CODE];   /* Regular */
extern const char PRMI_C     [NULL_CODE];   /* Clearance */
extern const char PRMI_B     [NULL_CODE];   /* Both Regular and Clearance */

#define PRMITT_R        1   /* Regular */
#define PRMITT_C        2   /* Clearance */
#define PRMITT_B        3   /* Both Regular and Clearance */

#define _PRMITT_MAX 3


/* PRMP = Promotion Parameters */
extern const char PRMP[NULL_CODE];

extern const char PRMP_LIST  [NULL_CODE];   /* LIST */

#define PRMPTT_LIST     1   /* LIST */

#define _PRMPTT_MAX 1


/* PRMT = Promotion Types */
extern const char PRMT[NULL_CODE];

extern const char PRMT_9999  [NULL_CODE];   /* RPM Promotion */
extern const char PRMT_1004  [NULL_CODE];   /* In Store Discount */
extern const char PRMT_1005  [NULL_CODE];   /* Employee Discount */
extern const char PRMT_2000  [NULL_CODE];   /* Non-RPM Promotion */

#define PRMTTT_9999     1   /* RPM Promotion */
#define PRMTTT_1004     5   /* In Store Discount */
#define PRMTTT_1005     6   /* Employee Discount */
#define PRMTTT_2000     9   /* Non-RPM Promotion */

#define _PRMTTT_MAX 9


/* PROM = Promotion Types */
extern const char PROM[NULL_CODE];

extern const char PROM_SK    [NULL_CODE];   /* Item Level */
extern const char PROM_DP    [NULL_CODE];   /* @MH4@ Level */
extern const char PROM_TH    [NULL_CODE];   /* Threshold */
extern const char PROM_MB    [NULL_CODE];   /* Mix/Match Buy */
extern const char PROM_MG    [NULL_CODE];   /* Mix/Match Get */
extern const char PROM_MU    [NULL_CODE];   /* Multi multi-units */

#define PROMTT_SK       1   /* Item Level */
#define PROMTT_DP       2   /* @MH4@ Level */
#define PROMTT_TH       3   /* Threshold */
#define PROMTT_MB       4   /* Mix/Match Buy */
#define PROMTT_MG       5   /* Mix/Match Get */
#define PROMTT_MU       6   /* Multi multi-units */

#define _PROMTT_MAX 6


/* PRPG = Promotions Price Change Type */
extern const char PRPG[NULL_CODE];

extern const char PRPG_PC    [NULL_CODE];   /* Percent Off */
extern const char PRPG_MA    [NULL_CODE];   /* Amount Off */
extern const char PRPG_NP    [NULL_CODE];   /* Fixed Price */
extern const char PRPG_NC    [NULL_CODE];   /* No Change */
extern const char PRPG_EX    [NULL_CODE];   /* Exclude */

#define PRPGTT_PC       1   /* Percent Off */
#define PRPGTT_MA       2   /* Amount Off */
#define PRPGTT_NP       3   /* Fixed Price */
#define PRPGTT_NC       4   /* No Change */
#define PRPGTT_EX       5   /* Exclude */

#define _PRPGTT_MAX 5


/* PRST = Promotion Status */
extern const char PRST[NULL_CODE];

extern const char PRST_W     [NULL_CODE];   /* Worksheet */
extern const char PRST_S     [NULL_CODE];   /* Submitted */
extern const char PRST_A     [NULL_CODE];   /* Approved */
extern const char PRST_R     [NULL_CODE];   /* Rejected */
extern const char PRST_E     [NULL_CODE];   /* Extracted */
extern const char PRST_M     [NULL_CODE];   /* Completed */
extern const char PRST_C     [NULL_CODE];   /* Cancelled */
extern const char PRST_D     [NULL_CODE];   /* Deleted */
extern const char PRST_I     [NULL_CODE];   /* Submit In Progress */
extern const char PRST_P     [NULL_CODE];   /* Approve In Progress */

#define PRSTTT_W        1   /* Worksheet */
#define PRSTTT_S        2   /* Submitted */
#define PRSTTT_A        3   /* Approved */
#define PRSTTT_R        4   /* Rejected */
#define PRSTTT_E        5   /* Extracted */
#define PRSTTT_M        6   /* Completed */
#define PRSTTT_C        7   /* Cancelled */
#define PRSTTT_D        8   /* Deleted */
#define PRSTTT_I        9   /* Submit In Progress */
#define PRSTTT_P       10   /* Approve In Progress */

#define _PRSTTT_MAX 10


/* PRTP = Consignment Purchase Indicator */
extern const char PRTP[NULL_CODE];

extern const char PRTP_0     [NULL_CODE];   /* Normal Merchandise */
extern const char PRTP_1     [NULL_CODE];   /* Consignment Merchandise */
extern const char PRTP_2     [NULL_CODE];   /* Concession Stock */

#define PRTPTT_0        1   /* Normal Merchandise */
#define PRTPTT_1        2   /* Consignment Merchandise */
#define PRTPTT_2        3   /* Concession Stock */

#define _PRTPTT_MAX 3


/* PRTY = Promotion Group Type */
extern const char PRTY[NULL_CODE];

extern const char PRTY_C     [NULL_CODE];   /* Store Class */
extern const char PRTY_D     [NULL_CODE];   /* @OH5@ */
extern const char PRTY_R     [NULL_CODE];   /* @OH4@ */
extern const char PRTY_S     [NULL_CODE];   /* Store */
extern const char PRTY_T     [NULL_CODE];   /* Transfer Zone */
extern const char PRTY_L     [NULL_CODE];   /* Location Traits */
extern const char PRTY_A     [NULL_CODE];   /* All Stores */
extern const char PRTY_Y     [NULL_CODE];   /* Country */
extern const char PRTY_LL    [NULL_CODE];   /* Location List */

#define PRTYTT_C        1   /* Store Class */
#define PRTYTT_D        2   /* @OH5@ */
#define PRTYTT_R        4   /* @OH4@ */
#define PRTYTT_S        5   /* Store */
#define PRTYTT_T        6   /* Transfer Zone */
#define PRTYTT_L        7   /* Location Traits */
#define PRTYTT_A        8   /* All Stores */
#define PRTYTT_Y        9   /* Country */
#define PRTYTT_LL      12   /* Location List */

#define _PRTYTT_MAX 12


/* PRUM = Purchase Unit of Measure */
extern const char PRUM[NULL_CODE];

extern const char PRUM_EA    [NULL_CODE];   /* Eaches */
extern const char PRUM_PA    [NULL_CODE];   /* Pairs */

#define PRUMTT_EA       1   /* Eaches */
#define PRUMTT_PA       2   /* Pairs */

#define _PRUMTT_MAX 2


/* PSAD = Promsku Adjust Type */
extern const char PSAD[NULL_CODE];

extern const char PSAD_NO    [NULL_CODE];   /* None */
extern const char PSAD_EI    [NULL_CODE];   /* Ends In */

#define PSADTT_NO       1   /* None */
#define PSADTT_EI       2   /* Ends In */

#define _PSADTT_MAX 2


/* PSEC = Product Security Areas */
extern const char PSEC[NULL_CODE];

extern const char PSEC_PALL  [NULL_CODE];   /* All */
extern const char PSEC_PPRC  [NULL_CODE];   /* Pricing */
extern const char PSEC_PCST  [NULL_CODE];   /* Costing */
extern const char PSEC_PPRM  [NULL_CODE];   /* Promotion */
extern const char PSEC_PCLR  [NULL_CODE];   /* Clearance */
extern const char PSEC_PTXF  [NULL_CODE];   /* Transfer */
extern const char PSEC_PALCFR[NULL_CODE];   /* Allocations */
extern const char PSEC_PPO   [NULL_CODE];   /* Orders */
extern const char PSEC_PSTK  [NULL_CODE];   /* Stock Counts */

#define PSECTT_PALL     1   /* All */
#define PSECTT_PPRC     2   /* Pricing */
#define PSECTT_PCST     3   /* Costing */
#define PSECTT_PPRM     4   /* Promotion */
#define PSECTT_PCLR     5   /* Clearance */
#define PSECTT_PTXF     6   /* Transfer */
#define PSECTT_PALCFR   7   /* Allocations */
#define PSECTT_PPO      8   /* Orders */
#define PSECTT_PSTK     9   /* Stock Counts */

#define _PSECTT_MAX 9


/* PSPG = Promsku Price Change Type */
extern const char PSPG[NULL_CODE];

extern const char PSPG_PC    [NULL_CODE];   /* Percent Off */
extern const char PSPG_MA    [NULL_CODE];   /* Amount Off */
extern const char PSPG_NP    [NULL_CODE];   /* Set Price to an Amount */

#define PSPGTT_PC       1   /* Percent Off */
#define PSPGTT_MA       2   /* Amount Off */
#define PSPGTT_NP       3   /* Set Price to an Amount */

#define _PSPGTT_MAX 3


/* PSUC = Projected Promotional Sales Calc Method */
extern const char PSUC[NULL_CODE];

extern const char PSUC_T     [NULL_CODE];   /* Turns */
extern const char PSUC_S     [NULL_CODE];   /* Sell-through */
extern const char PSUC_F     [NULL_CODE];   /* Forecasted Sales */
extern const char PSUC_P     [NULL_CODE];   /* Percent of Projected SOH */

#define PSUCTT_T        1   /* Turns */
#define PSUCTT_S        2   /* Sell-through */
#define PSUCTT_F        3   /* Forecasted Sales */
#define PSUCTT_P        4   /* Percent of Projected SOH */

#define _PSUCTT_MAX 4


/* PTAL = All Partner Types */
extern const char PTAL[NULL_CODE];

extern const char PTAL_BK    [NULL_CODE];   /* Bank */
extern const char PTAL_AG    [NULL_CODE];   /* Agent */
extern const char PTAL_FF    [NULL_CODE];   /* Freight Forwarder */
extern const char PTAL_IM    [NULL_CODE];   /* Importer */
extern const char PTAL_BR    [NULL_CODE];   /* Broker */
extern const char PTAL_FA    [NULL_CODE];   /* Factory */
extern const char PTAL_CO    [NULL_CODE];   /* Consolidator */
extern const char PTAL_AP    [NULL_CODE];   /* Applicant */
extern const char PTAL_CN    [NULL_CODE];   /* Consignee */
extern const char PTAL_S1    [NULL_CODE];   /* @SUH1@ */
extern const char PTAL_S2    [NULL_CODE];   /* @SUH2@ */
extern const char PTAL_S3    [NULL_CODE];   /* @SUH3@ */
extern const char PTAL_EV    [NULL_CODE];   /* Expense Vendor */
extern const char PTAL_IA    [NULL_CODE];   /* Import Authority */
extern const char PTAL_ES    [NULL_CODE];   /* Escheat - State */
extern const char PTAL_EC    [NULL_CODE];   /* Escheat - Country */
extern const char PTAL_E     [NULL_CODE];   /* External Finisher */
extern const char PTAL_S     [NULL_CODE];   /* Supplier */

#define PTALTT_BK       1   /* Bank */
#define PTALTT_AG       2   /* Agent */
#define PTALTT_FF       3   /* Freight Forwarder */
#define PTALTT_IM       4   /* Importer */
#define PTALTT_BR       5   /* Broker */
#define PTALTT_FA       6   /* Factory */
#define PTALTT_CO       7   /* Consolidator */
#define PTALTT_AP       8   /* Applicant */
#define PTALTT_CN       9   /* Consignee */
#define PTALTT_S1      10   /* @SUH1@ */
#define PTALTT_S2      11   /* @SUH2@ */
#define PTALTT_S3      12   /* @SUH3@ */
#define PTALTT_EV      13   /* Expense Vendor */
#define PTALTT_IA      14   /* Import Authority */
#define PTALTT_ES      15   /* Escheat - State */
#define PTALTT_EC      16   /* Escheat - Country */
#define PTALTT_E       17   /* External Finisher */
#define PTALTT_S       18   /* Supplier */

#define _PTALTT_MAX 18


/* PTAT = Promotion Threshold Apply To */
extern const char PTAT[NULL_CODE];

extern const char PTAT_C     [NULL_CODE];   /* Compound */
extern const char PTAT_E     [NULL_CODE];   /* Exclusive */

#define PTATTT_C        1   /* Compound */
#define PTATTT_E        2   /* Exclusive */

#define _PTATTT_MAX 2


/* PTDT = Promotion Threshold Discount Type */
extern const char PTDT[NULL_CODE];

extern const char PTDT_P     [NULL_CODE];   /* Percent Off */
extern const char PTDT_A     [NULL_CODE];   /* Amount Off */
extern const char PTDT_F     [NULL_CODE];   /* Fixed Amount */

#define PTDTTT_P        1   /* Percent Off */
#define PTDTTT_A        2   /* Amount Off */
#define PTDTTT_F        3   /* Fixed Amount */

#define _PTDTTT_MAX 3


/* PTNR = Partner Types */
extern const char PTNR[NULL_CODE];

extern const char PTNR_AG    [NULL_CODE];   /* Agent */
extern const char PTNR_AP    [NULL_CODE];   /* Applicant */
extern const char PTNR_BK    [NULL_CODE];   /* Bank */
extern const char PTNR_BR    [NULL_CODE];   /* Broker */
extern const char PTNR_CN    [NULL_CODE];   /* Consignee */
extern const char PTNR_CO    [NULL_CODE];   /* Consolidator */
extern const char PTNR_E     [NULL_CODE];   /* External Finisher */
extern const char PTNR_EC    [NULL_CODE];   /* Escheat-Country */
extern const char PTNR_ES    [NULL_CODE];   /* Escheat-State */
extern const char PTNR_EV    [NULL_CODE];   /* EV */
extern const char PTNR_FA    [NULL_CODE];   /* Factory */
extern const char PTNR_FF    [NULL_CODE];   /* Freight Forwarder */
extern const char PTNR_IA    [NULL_CODE];   /* IA */
extern const char PTNR_IM    [NULL_CODE];   /* Importer */
extern const char PTNR_S1    [NULL_CODE];   /* @SUH1@ */
extern const char PTNR_S2    [NULL_CODE];   /* @SUH2@ */
extern const char PTNR_S3    [NULL_CODE];   /* @SUH3@ */

#define PTNRTT_AG       1   /* Agent */
#define PTNRTT_AP       2   /* Applicant */
#define PTNRTT_BK       3   /* Bank */
#define PTNRTT_BR       4   /* Broker */
#define PTNRTT_CN       5   /* Consignee */
#define PTNRTT_CO       6   /* Consolidator */
#define PTNRTT_E        7   /* External Finisher */
#define PTNRTT_EC       8   /* Escheat-Country */
#define PTNRTT_ES       9   /* Escheat-State */
#define PTNRTT_EV      10   /* EV */
#define PTNRTT_FA      11   /* Factory */
#define PTNRTT_FF      12   /* Freight Forwarder */
#define PTNRTT_IA      13   /* IA */
#define PTNRTT_IM      14   /* Importer */
#define PTNRTT_S1      15   /* @SUH1@ */
#define PTNRTT_S2      16   /* @SUH2@ */
#define PTNRTT_S3      17   /* @SUH3@ */

#define _PTNRTT_MAX 17


/* PTPE = Processing Type */
extern const char PTPE[NULL_CODE];

extern const char PTPE_X     [NULL_CODE];   /* Cross Dock */
extern const char PTPE_C     [NULL_CODE];   /* Consolidation */

#define PTPETT_X        1   /* Cross Dock */
#define PTPETT_C        2   /* Consolidation */

#define _PTPETT_MAX 2


/* PTTC = Process Tracker Template Category. */
extern const char PTTC[NULL_CODE];

extern const char PTTC_IS9M  [NULL_CODE];   /* Items */
extern const char PTTC_IS9C  [NULL_CODE];   /* Cost Changes */
extern const char PTTC_PODT  [NULL_CODE];   /* Purchase Orders */
extern const char PTTC_OTHR  [NULL_CODE];   /* Foundation */

#define PTTCTT_IS9M     1   /* Items */
#define PTTCTT_IS9C     2   /* Cost Changes */
#define PTTCTT_PODT     3   /* Purchase Orders */
#define PTTCTT_OTHR     4   /* Foundation */

#define _PTTCTT_MAX 4


/* PTTY = Promotion Threshold Type */
extern const char PTTY[NULL_CODE];

extern const char PTTY_A     [NULL_CODE];   /* Amount */
extern const char PTTY_U     [NULL_CODE];   /* Unit */

#define PTTYTT_A        1   /* Amount */
#define PTTYTT_U        2   /* Unit */

#define _PTTYTT_MAX 2


/* PTYP = Processor Type */
extern const char PTYP[NULL_CODE];

extern const char PTYP_SEARS [NULL_CODE];   /* Sears */
extern const char PTYP_NPC   [NULL_CODE];   /* NPC */
extern const char PTYP_NONE  [NULL_CODE];   /* None */

#define PTYPTT_SEARS    1   /* Sears */
#define PTYPTT_NPC      2   /* NPC */
#define PTYPTT_NONE     3   /* None */

#define _PTYPTT_MAX 3


/* PURT = Purchase Types */
extern const char PURT[NULL_CODE];

extern const char PURT_DLV   [NULL_CODE];   /* Delivered */
extern const char PURT_FOB   [NULL_CODE];   /* Free on Board */
extern const char PURT_BACK  [NULL_CODE];   /* Backhaul */
extern const char PURT_PUP   [NULL_CODE];   /* Pick-up */

#define PURTTT_DLV      1   /* Delivered */
#define PURTTT_FOB      2   /* Free on Board */
#define PURTTT_BACK     3   /* Backhaul */
#define PURTTT_PUP      4   /* Pick-up */

#define _PURTTT_MAX 4


/* PXS1 = Price Change Status (no reject) */
extern const char PXS1[NULL_CODE];

extern const char PXS1_W     [NULL_CODE];   /* Worksheet */
extern const char PXS1_S     [NULL_CODE];   /* Submitted */
extern const char PXS1_A     [NULL_CODE];   /* Approved */
extern const char PXS1_E     [NULL_CODE];   /* Extracted */
extern const char PXS1_C     [NULL_CODE];   /* Cancelled */
extern const char PXS1_D     [NULL_CODE];   /* Deleted */

#define PXS1TT_W        1   /* Worksheet */
#define PXS1TT_S        2   /* Submitted */
#define PXS1TT_A        3   /* Approved */
#define PXS1TT_E        6   /* Extracted */
#define PXS1TT_C        7   /* Cancelled */
#define PXS1TT_D        8   /* Deleted */

#define _PXS1TT_MAX 8


/* PXST = Price Change Status */
extern const char PXST[NULL_CODE];

extern const char PXST_W     [NULL_CODE];   /* Worksheet */
extern const char PXST_S     [NULL_CODE];   /* Submitted */
extern const char PXST_A     [NULL_CODE];   /* Approved */
extern const char PXST_R     [NULL_CODE];   /* Rejected */
extern const char PXST_E     [NULL_CODE];   /* Extracted */
extern const char PXST_C     [NULL_CODE];   /* Cancelled */
extern const char PXST_D     [NULL_CODE];   /* Deleted */

#define PXSTTT_W        1   /* Worksheet */
#define PXSTTT_S        2   /* Submitted */
#define PXSTTT_A        3   /* Approved */
#define PXSTTT_R        4   /* Rejected */
#define PXSTTT_E        6   /* Extracted */
#define PXSTTT_C        7   /* Cancelled */
#define PXSTTT_D        8   /* Deleted */

#define _PXSTTT_MAX 8


/* PYMT = Payment Method */
extern const char PYMT[NULL_CODE];

extern const char PYMT_LC    [NULL_CODE];   /* Letter of Credit */
extern const char PYMT_OA    [NULL_CODE];   /* Open Account */
extern const char PYMT_WT    [NULL_CODE];   /* Wire Transfer */

#define PYMTTT_LC       1   /* Letter of Credit */
#define PYMTTT_OA       2   /* Open Account */
#define PYMTTT_WT       3   /* Wire Transfer */

#define _PYMTTT_MAX 3


/* PYTP = Payment Method */
extern const char PYTP[NULL_CODE];

extern const char PYTP_1     [NULL_CODE];   /* Cash */
extern const char PYTP_2     [NULL_CODE];   /* Check */
extern const char PYTP_3     [NULL_CODE];   /* Credit */

#define PYTPTT_1        1   /* Cash */
#define PYTPTT_2        2   /* Check */
#define PYTPTT_3        3   /* Credit */

#define _PYTPTT_MAX 3


/* PZG = Pricing Zone Group Label */
extern const char PZG[NULL_CODE];

extern const char PZG_PZG   [NULL_CODE];   /* Pricing Zone Group */

#define PZGTT_PZG      1   /* Pricing Zone Group */

#define _PZGTT_MAX 1


/* PZLB = Pricing Zone Labels */
extern const char PZLB[NULL_CODE];

extern const char PZLB_S     [NULL_CODE];   /* Store */
extern const char PZLB_Z     [NULL_CODE];   /* Zone */
extern const char PZLB_C     [NULL_CODE];   /* Corp. */

#define PZLBTT_S        1   /* Store */
#define PZLBTT_Z        2   /* Zone */
#define PZLBTT_C        3   /* Corp. */

#define _PZLBTT_MAX 3


/* PZP1 = Pricing Zone Group Level 1 */
extern const char PZP1[NULL_CODE];

extern const char PZP1_A     [NULL_CODE];   /* All Levels */

#define PZP1TT_A        1   /* All Levels */

#define _PZP1TT_MAX 1


/* PZPL = Zone Group Pricing Level */
extern const char PZPL[NULL_CODE];

extern const char PZPL_C     [NULL_CODE];   /* Corporate */
extern const char PZPL_Z     [NULL_CODE];   /* Zone */
extern const char PZPL_S     [NULL_CODE];   /* Store */

#define PZPLTT_C        1   /* Corporate */
#define PZPLTT_Z        2   /* Zone */
#define PZPLTT_S        3   /* Store */

#define _PZPLTT_MAX 3


/* QCIR = Quality Inspection Result */
extern const char QCIR[NULL_CODE];

extern const char QCIR_P     [NULL_CODE];   /* Pass */
extern const char QCIR_F     [NULL_CODE];   /* Fail */

#define QCIRTT_P        1   /* Pass */
#define QCIRTT_F        2   /* Fail */

#define _QCIRTT_MAX 2


/* QCPC = QC Percentage Amount */
extern const char QCPC[NULL_CODE];

extern const char QCPC_P     [NULL_CODE];   /* 10 */

#define QCPCTT_P        1   /* 10 */

#define _QCPCTT_MAX 1


/* QCST = Quality Control Status */
extern const char QCST[NULL_CODE];

extern const char QCST_R     [NULL_CODE];   /* Required */
extern const char QCST_P     [NULL_CODE];   /* Passed */
extern const char QCST_F     [NULL_CODE];   /* Failed */

#define QCSTTT_R        1   /* Required */
#define QCSTTT_P        2   /* Passed */
#define QCSTTT_F        3   /* Failed */

#define _QCSTTT_MAX 3


/* QLBL = Quantity Labels */
extern const char QLBL[NULL_CODE];

extern const char QLBL_P     [NULL_CODE];   /* Pct. */
extern const char QLBL_R     [NULL_CODE];   /* Ratio */

#define QLBLTT_P        1   /* Pct. */
#define QLBLTT_R        2   /* Ratio */

#define _QLBLTT_MAX 2


/* QTYP = MRT Quantity Type */
extern const char QTYP[NULL_CODE];

extern const char QTYP_A     [NULL_CODE];   /* All Inventory */
extern const char QTYP_M     [NULL_CODE];   /* Manual */

#define QTYPTT_A        1   /* All Inventory */
#define QTYPTT_M        2   /* Manual */

#define _QTYPTT_MAX 2


/* QUAN = Label Quantity */
extern const char QUAN[NULL_CODE];

extern const char QUAN_Q     [NULL_CODE];   /* Quantity */

#define QUANTT_Q        1   /* Quantity */

#define _QUANTT_MAX 1


/* QYTP = Transfer Quantity Type (Manual, SOH) */
extern const char QYTP[NULL_CODE];

extern const char QYTP_M     [NULL_CODE];   /* Manual */
extern const char QYTP_SOH   [NULL_CODE];   /* Total Stock on Hand */

#define QYTPTT_M        1   /* Manual */
#define QYTPTT_SOH      2   /* Total Stock on Hand */

#define _QYTPTT_MAX 2


/* RACK = Rack Sizes */
extern const char RACK[NULL_CODE];

extern const char RACK_S     [NULL_CODE];   /* Small */
extern const char RACK_M     [NULL_CODE];   /* Medium */
extern const char RACK_L     [NULL_CODE];   /* Large */

#define RACKTT_S        1   /* Small */
#define RACKTT_M        2   /* Medium */
#define RACKTT_L        3   /* Large */

#define _RACKTT_MAX 3


/* RAS = OI Report Option Region/Area/Store */
extern const char RAS[NULL_CODE];

extern const char RAS_30    [NULL_CODE];   /* Region */
extern const char RAS_40    [NULL_CODE];   /* Area */
extern const char RAS_50    [NULL_CODE];   /* Store */

#define RASTT_30       1   /* Region */
#define RASTT_40       2   /* Area */
#define RASTT_50       3   /* Store */

#define _RASTT_MAX 3


/* RASK = Ragskadd Labels */
extern const char RASK[NULL_CODE];

extern const char RASK_ON    [NULL_CODE];   /* Order Number */
extern const char RASK_D     [NULL_CODE];   /* Distribution */
extern const char RASK_E     [NULL_CODE];   /* End Item Parent */
extern const char RASK_C     [NULL_CODE];   /* Contract Number */
extern const char RASK_OK    [NULL_CODE];   /* OK */
extern const char RASK_CAN   [NULL_CODE];   /* Cancel */

#define RASKTT_ON       1   /* Order Number */
#define RASKTT_D        2   /* Distribution */
#define RASKTT_E        3   /* End Item Parent */
#define RASKTT_C        4   /* Contract Number */
#define RASKTT_OK       5   /* OK */
#define RASKTT_CAN      6   /* Cancel */

#define _RASKTT_MAX 6


/* RCAD = RCA Cost Change Description */
extern const char RCAD[NULL_CODE];

extern const char RCAD_RCAA  [NULL_CODE];   /* Receiver Cost Adjustment Cost Change */

#define RCADTT_RCAA     1   /* Receiver Cost Adjustment Cost Change */

#define _RCADTT_MAX 1


/* RCCQ = Reclass and Cost Change Queue Code Types */
extern const char RCCQ[NULL_CODE];

extern const char RCCQ_C     [NULL_CODE];   /* Cost Change */
extern const char RCCQ_R     [NULL_CODE];   /* Reclassification */
extern const char RCCQ_N     [NULL_CODE];   /* New item location */

#define RCCQTT_C        1   /* Cost Change */
#define RCCQTT_R        2   /* Reclassification */
#define RCCQTT_N        3   /* New item location */

#define _RCCQTT_MAX 3


/* RCER = Tran Data Adjustment */
extern const char RCER[NULL_CODE];

extern const char RCER_U     [NULL_CODE];   /* Units */
extern const char RCER_C     [NULL_CODE];   /* Cost */
extern const char RCER_R     [NULL_CODE];   /* Retail */
extern const char RCER_A     [NULL_CODE];   /* ALC/adjustments */

#define RCERTT_U        1   /* Units */
#define RCERTT_C        2   /* Cost */
#define RCERTT_R        3   /* Retail */
#define RCERTT_A        4   /* ALC/adjustments */

#define _RCERTT_MAX 4


/* RCLA = Reclass Maintenance Codes */
extern const char RCLA[NULL_CODE];

extern const char RCLA_A     [NULL_CODE];   /* Addition */
extern const char RCLA_M     [NULL_CODE];   /* Modification */

#define RCLATT_A        1   /* Addition */
#define RCLATT_M        2   /* Modification */

#define _RCLATT_MAX 2


/* RCLH = Reclass Hierarchy Codes */
extern const char RCLH[NULL_CODE];

extern const char RCLH_M     [NULL_CODE];   /* @OH1@ */
extern const char RCLH_V     [NULL_CODE];   /* @MH2@ */
extern const char RCLH_G     [NULL_CODE];   /* @MH3@ */
extern const char RCLH_D     [NULL_CODE];   /* @MH4@ */
extern const char RCLH_C     [NULL_CODE];   /* @MH5@ */
extern const char RCLH_S     [NULL_CODE];   /* @MH6@ */

#define RCLHTT_M        1   /* @OH1@ */
#define RCLHTT_V        2   /* @MH2@ */
#define RCLHTT_G        3   /* @MH3@ */
#define RCLHTT_D        4   /* @MH4@ */
#define RCLHTT_C        5   /* @MH5@ */
#define RCLHTT_S        6   /* @MH6@ */

#define _RCLHTT_MAX 6


/* RCST = Receiving Item Type */
extern const char RCST[NULL_CODE];

extern const char RCST_S     [NULL_CODE];   /* Item */
extern const char RCST_U     [NULL_CODE];   /* Child Item */
extern const char RCST_V     [NULL_CODE];   /* VPN */
extern const char RCST_R     [NULL_CODE];   /* Ref Item */

#define RCSTTT_S        1   /* Item */
#define RCSTTT_U        2   /* Child Item */
#define RCSTTT_V        3   /* VPN */
#define RCSTTT_R        4   /* Ref Item */

#define _RCSTTT_MAX 4


/* RDTP = Appointment Document Types */
extern const char RDTP[NULL_CODE];

extern const char RDTP_P     [NULL_CODE];   /* Purchase Order */
extern const char RDTP_T     [NULL_CODE];   /* Transfer */
extern const char RDTP_A     [NULL_CODE];   /* Allocation */

#define RDTPTT_P        1   /* Purchase Order */
#define RDTPTT_T        2   /* Transfer */
#define RDTPTT_A        3   /* Allocation */

#define _RDTPTT_MAX 3


/* REAC = Sales Audit Reason Codes */
extern const char REAC[NULL_CODE];

extern const char REAC_ERR   [NULL_CODE];   /* Error */
extern const char REAC_ACCT  [NULL_CODE];   /* Payment on Account */
extern const char REAC_INC   [NULL_CODE];   /* Incident Payout */
extern const char REAC_NSF   [NULL_CODE];   /* NSF Check Payment */
extern const char REAC_PAYRL [NULL_CODE];   /* Payroll Payout */
extern const char REAC_LOTWIN[NULL_CODE];   /* Lottery Winnings */
extern const char REAC_TPIVMR[NULL_CODE];   /* Till Pay In Vending Machine Revenue */
extern const char REAC_TPIMSC[NULL_CODE];   /* Till Pay In Miscellaneous */
extern const char REAC_TPOP  [NULL_CODE];   /* Till Pay Out Postage */
extern const char REAC_TPOS  [NULL_CODE];   /* Till Pay Out Supplies */
extern const char REAC_TPOE  [NULL_CODE];   /* Till Pay Out Entertainment */
extern const char REAC_NSCC  [NULL_CODE];   /* No Sale Customer Change */
extern const char REAC_NSCFR [NULL_CODE];   /* No Sale Change For Register */
extern const char REAC_PVIP  [NULL_CODE];   /* Post Void Incorrect Price */
extern const char REAC_PVDI  [NULL_CODE];   /* Post Void Discount Incorrect */
extern const char REAC_PVCCM [NULL_CODE];   /* Post Void Customer Changed Mind */
extern const char REAC_PVAE  [NULL_CODE];   /* Post Void Associate Error */
extern const char REAC_PVOFP [NULL_CODE];   /* Post Void Other Form Payment */
extern const char REAC_PVO   [NULL_CODE];   /* Post Void Other */
extern const char REAC_HOUSE [NULL_CODE];   /* House Payment */
extern const char REAC_LAYINT[NULL_CODE];   /* Layaway Initiate */
extern const char REAC_LAYPAY[NULL_CODE];   /* Layaway Payment */
extern const char REAC_ORDINT[NULL_CODE];   /* Order Initiate */
extern const char REAC_LAYDEL[NULL_CODE];   /* Layaway Delete */
extern const char REAC_ORDCAN[NULL_CODE];   /* Order Cancel */
extern const char REAC_COUPN [NULL_CODE];   /* Coupon */
extern const char REAC_FONCT [NULL_CODE];   /* Fonacot */
extern const char REAC_BLPYMT[NULL_CODE];   /* Telephone - Mobile Bill Payment */
extern const char REAC_PV1   [NULL_CODE];   /* Cashier Error */
extern const char REAC_PV2   [NULL_CODE];   /* Supervisors Discretion */
extern const char REAC_PV3   [NULL_CODE];   /* Customer Satisfaction */
extern const char REAC_NS1   [NULL_CODE];   /* Making Change */
extern const char REAC_NS2   [NULL_CODE];   /* Employee Check Cashed */
extern const char REAC_NS3   [NULL_CODE];   /* Petty Cash In */
extern const char REAC_NS4   [NULL_CODE];   /* Petty Cash Out */
extern const char REAC_NS5   [NULL_CODE];   /* Spiff/Bonus Out 1 */
extern const char REAC_CF1   [NULL_CODE];   /* Register Down 1 */
extern const char REAC_CF2   [NULL_CODE];   /* Holiday Adjustment */
extern const char REAC_PI1   [NULL_CODE];   /* Change from Paid Out */
extern const char REAC_PI2   [NULL_CODE];   /* Found Money */
extern const char REAC_PI3   [NULL_CODE];   /* Drawer Loan 1 */
extern const char REAC_PO1   [NULL_CODE];   /* Stocks */
extern const char REAC_P02   [NULL_CODE];   /* Delivery */
extern const char REAC_PO3   [NULL_CODE];   /* Postage */
extern const char REAC_PO4   [NULL_CODE];   /* Contractor Services */
extern const char REAC_PO5   [NULL_CODE];   /* Store Incentives */
extern const char REAC_TENDEX[NULL_CODE];   /* Tender Exchange */

#define REACTT_ERR      1   /* Error */
#define REACTT_ACCT     3   /* Payment on Account */
#define REACTT_INC      4   /* Incident Payout */
#define REACTT_NSF      6   /* NSF Check Payment */
#define REACTT_PAYRL    7   /* Payroll Payout */
#define REACTT_LOTWIN   8   /* Lottery Winnings */
#define REACTT_TPIVMR   9   /* Till Pay In Vending Machine Revenue */
#define REACTT_TPIMSC  10   /* Till Pay In Miscellaneous */
#define REACTT_TPOP    11   /* Till Pay Out Postage */
#define REACTT_TPOS    12   /* Till Pay Out Supplies */
#define REACTT_TPOE    13   /* Till Pay Out Entertainment */
#define REACTT_NSCC    14   /* No Sale Customer Change */
#define REACTT_NSCFR   15   /* No Sale Change For Register */
#define REACTT_PVIP    16   /* Post Void Incorrect Price */
#define REACTT_PVDI    17   /* Post Void Discount Incorrect */
#define REACTT_PVCCM   18   /* Post Void Customer Changed Mind */
#define REACTT_PVAE    19   /* Post Void Associate Error */
#define REACTT_PVOFP   20   /* Post Void Other Form Payment */
#define REACTT_PVO     21   /* Post Void Other */
#define REACTT_HOUSE   22   /* House Payment */
#define REACTT_LAYINT  23   /* Layaway Initiate */
#define REACTT_LAYPAY  24   /* Layaway Payment */
#define REACTT_ORDINT  25   /* Order Initiate */
#define REACTT_LAYDEL  26   /* Layaway Delete */
#define REACTT_ORDCAN  27   /* Order Cancel */
#define REACTT_COUPN   28   /* Coupon */
#define REACTT_FONCT   29   /* Fonacot */
#define REACTT_BLPYMT  30   /* Telephone - Mobile Bill Payment */
#define REACTT_PV1     31   /* Cashier Error */
#define REACTT_PV2     32   /* Supervisors Discretion */
#define REACTT_PV3     33   /* Customer Satisfaction */
#define REACTT_NS1     34   /* Making Change */
#define REACTT_NS2     35   /* Employee Check Cashed */
#define REACTT_NS3     36   /* Petty Cash In */
#define REACTT_NS4     37   /* Petty Cash Out */
#define REACTT_NS5     38   /* Spiff/Bonus Out 1 */
#define REACTT_CF1     39   /* Register Down 1 */
#define REACTT_CF2     41   /* Holiday Adjustment */
#define REACTT_PI1     43   /* Change from Paid Out */
#define REACTT_PI2     44   /* Found Money */
#define REACTT_PI3     45   /* Drawer Loan 1 */
#define REACTT_PO1     46   /* Stocks */
#define REACTT_P02     47   /* Delivery */
#define REACTT_PO3     48   /* Postage */
#define REACTT_PO4     49   /* Contractor Services */
#define REACTT_PO5     50   /* Store Incentives */
#define REACTT_TENDEX  51   /* Tender Exchange */

#define _REACTT_MAX 51


/* RECL = Reclassification Privilege */
extern const char RECL[NULL_CODE];

extern const char RECL_1001  [NULL_CODE];   /* Friemann */
extern const char RECL_4401  [NULL_CODE];   /* Mike's Super Group */
extern const char RECL_2200  [NULL_CODE];   /* eph */
extern const char RECL_4700  [NULL_CODE];   /* eph */

#define RECLTT_1001     1   /* Friemann */
#define RECLTT_4401     2   /* Mike's Super Group */
#define RECLTT_2200   220   /* eph */
#define RECLTT_4700   470   /* eph */

#define _RECLTT_MAX 4700


/* REFL = Sales Audit Reference Labels */
extern const char REFL[NULL_CODE];

extern const char REFL_1     [NULL_CODE];   /* Reference No. 1 */
extern const char REFL_2     [NULL_CODE];   /* Reference No. 2 */
extern const char REFL_3     [NULL_CODE];   /* Reference No. 3 */
extern const char REFL_4     [NULL_CODE];   /* Reference No. 4 */
extern const char REFL_5     [NULL_CODE];   /* Reference No. 5 */
extern const char REFL_6     [NULL_CODE];   /* Reference No. 6 */
extern const char REFL_7     [NULL_CODE];   /* Reference No. 7 */
extern const char REFL_8     [NULL_CODE];   /* Reference No. 8 */
extern const char REFL_9     [NULL_CODE];   /* Reference No. 9 */
extern const char REFL_10    [NULL_CODE];   /* Reference No. 10 */
extern const char REFL_11    [NULL_CODE];   /* Reference No. 11 */
extern const char REFL_12    [NULL_CODE];   /* Reference No. 12 */
extern const char REFL_13    [NULL_CODE];   /* Reference No. 13 */
extern const char REFL_14    [NULL_CODE];   /* Reference No. 14 */
extern const char REFL_15    [NULL_CODE];   /* Reference No. 15 */
extern const char REFL_16    [NULL_CODE];   /* Reference No. 16 */
extern const char REFL_S     [NULL_CODE];   /* Supplier */
extern const char REFL_M     [NULL_CODE];   /* Money Order No. */
extern const char REFL_C     [NULL_CODE];   /* Check No. */
extern const char REFL_SP    [NULL_CODE];   /* Special Order No. */
extern const char REFL_G     [NULL_CODE];   /* Gift Cert. Serial No. */
extern const char REFL_P     [NULL_CODE];   /* Personal ID No. */
extern const char REFL_I     [NULL_CODE];   /* Incident No. */
extern const char REFL_IT    [NULL_CODE];   /* Incident Type */
extern const char REFL_GT    [NULL_CODE];   /* Lottery Game */
extern const char REFL_L     [NULL_CODE];   /* Lottery Ticket No. */
extern const char REFL_IV    [NULL_CODE];   /* Invoice No. */
extern const char REFL_CC    [NULL_CODE];   /* Credit Card No. */
extern const char REFL_D     [NULL_CODE];   /* Driver's License No. */
extern const char REFL_A     [NULL_CODE];   /* Account No. */
extern const char REFL_R     [NULL_CODE];   /* Routing No. */
extern const char REFL_CH    [NULL_CODE];   /* Charity Org. No. */
extern const char REFL_CM    [NULL_CODE];   /* Coin Mach. Supplier No. */
extern const char REFL_T     [NULL_CODE];   /* Traffic */
extern const char REFL_TM    [NULL_CODE];   /* Temperature */
extern const char REFL_W     [NULL_CODE];   /* Weather */
extern const char REFL_RF    [NULL_CODE];   /* Refund Ref. No. */
extern const char REFL_CD    [NULL_CODE];   /* Coupon Discount */
extern const char REFL_CMR   [NULL_CODE];   /* Close Meter Reading */
extern const char REFL_CON   [NULL_CODE];   /* Construction */
extern const char REFL_CT    [NULL_CODE];   /* Coupon Tender */
extern const char REFL_DHJ   [NULL_CODE];   /* Dip Height Major */
extern const char REFL_DHN   [NULL_CODE];   /* Dip Height Minor */
extern const char REFL_DT    [NULL_CODE];   /* Dip Type */
extern const char REFL_E     [NULL_CODE];   /* Employee No. */
extern const char REFL_OMR   [NULL_CODE];   /* Open Meter Reading */
extern const char REFL_RI    [NULL_CODE];   /* Receipt Indicator */
extern const char REFL_RT    [NULL_CODE];   /* Reading Type */
extern const char REFL_SS    [NULL_CODE];   /* Speed Sale No. */
extern const char REFL_TK    [NULL_CODE];   /* Tank */
extern const char REFL_CMV   [NULL_CODE];   /* Close Meter Value */
extern const char REFL_OMV   [NULL_CODE];   /* Open Meter Value */
extern const char REFL_ED    [NULL_CODE];   /* Escheat Date */
extern const char REFL_RN    [NULL_CODE];   /* Recipient Name */
extern const char REFL_RS    [NULL_CODE];   /* Recipient State */
extern const char REFL_RC    [NULL_CODE];   /* Recipient Country */
extern const char REFL_17    [NULL_CODE];   /* Reference No. 17 */
extern const char REFL_18    [NULL_CODE];   /* Reference No. 18 */
extern const char REFL_19    [NULL_CODE];   /* Reference No. 19 */
extern const char REFL_20    [NULL_CODE];   /* Reference No. 20 */
extern const char REFL_TOTAL [NULL_CODE];   /* Total ID */
extern const char REFL_IFEID [NULL_CODE];   /* Instituto Federal Electoral ID */
extern const char REFL_PRD   [NULL_CODE];   /* Provider Name */
extern const char REFL_FON   [NULL_CODE];   /* Fonacot Number */
extern const char REFL_CKDATE[NULL_CODE];   /* Check Date */
extern const char REFL_CKDTFL[NULL_CODE];   /* Check post dated flag */
extern const char REFL_21    [NULL_CODE];   /* Reference No. 21 */
extern const char REFL_22    [NULL_CODE];   /* Reference No. 22 */
extern const char REFL_23    [NULL_CODE];   /* Reference No. 23 */
extern const char REFL_24    [NULL_CODE];   /* Reference No. 24 */
extern const char REFL_25    [NULL_CODE];   /* Reference No. 25 */
extern const char REFL_26    [NULL_CODE];   /* Reference No. 26 */
extern const char REFL_27    [NULL_CODE];   /* Reference No. 27 */
extern const char REFL_CI    [NULL_CODE];   /* Component Item */
extern const char REFL_CID   [NULL_CODE];   /* Component Item Description */
extern const char REFL_FNN   [NULL_CODE];   /* Fiscal Note Number */
extern const char REFL_CACN  [NULL_CODE];   /* Customer Account Number */
extern const char REFL_CN    [NULL_CODE];   /* Customer Name */

#define REFLTT_1        1   /* Reference No. 1 */
#define REFLTT_2        2   /* Reference No. 2 */
#define REFLTT_3        3   /* Reference No. 3 */
#define REFLTT_4        4   /* Reference No. 4 */
#define REFLTT_5        5   /* Reference No. 5 */
#define REFLTT_6        6   /* Reference No. 6 */
#define REFLTT_7        7   /* Reference No. 7 */
#define REFLTT_8        8   /* Reference No. 8 */
#define REFLTT_9        9   /* Reference No. 9 */
#define REFLTT_10      10   /* Reference No. 10 */
#define REFLTT_11      11   /* Reference No. 11 */
#define REFLTT_12      12   /* Reference No. 12 */
#define REFLTT_13      13   /* Reference No. 13 */
#define REFLTT_14      14   /* Reference No. 14 */
#define REFLTT_15      15   /* Reference No. 15 */
#define REFLTT_16      16   /* Reference No. 16 */
#define REFLTT_S       17   /* Supplier */
#define REFLTT_M       18   /* Money Order No. */
#define REFLTT_C       19   /* Check No. */
#define REFLTT_SP      20   /* Special Order No. */
#define REFLTT_G       21   /* Gift Cert. Serial No. */
#define REFLTT_P       23   /* Personal ID No. */
#define REFLTT_I       24   /* Incident No. */
#define REFLTT_IT      25   /* Incident Type */
#define REFLTT_GT      26   /* Lottery Game */
#define REFLTT_L       27   /* Lottery Ticket No. */
#define REFLTT_IV      28   /* Invoice No. */
#define REFLTT_CC      29   /* Credit Card No. */
#define REFLTT_D       30   /* Driver's License No. */
#define REFLTT_A       31   /* Account No. */
#define REFLTT_R       32   /* Routing No. */
#define REFLTT_CH      33   /* Charity Org. No. */
#define REFLTT_CM      34   /* Coin Mach. Supplier No. */
#define REFLTT_T       36   /* Traffic */
#define REFLTT_TM      37   /* Temperature */
#define REFLTT_W       38   /* Weather */
#define REFLTT_RF      39   /* Refund Ref. No. */
#define REFLTT_CD      40   /* Coupon Discount */
#define REFLTT_CMR     41   /* Close Meter Reading */
#define REFLTT_CON     42   /* Construction */
#define REFLTT_CT      43   /* Coupon Tender */
#define REFLTT_DHJ     44   /* Dip Height Major */
#define REFLTT_DHN     45   /* Dip Height Minor */
#define REFLTT_DT      46   /* Dip Type */
#define REFLTT_E       47   /* Employee No. */
#define REFLTT_OMR     48   /* Open Meter Reading */
#define REFLTT_RI      49   /* Receipt Indicator */
#define REFLTT_RT      50   /* Reading Type */
#define REFLTT_SS      51   /* Speed Sale No. */
#define REFLTT_TK      52   /* Tank */
#define REFLTT_CMV     53   /* Close Meter Value */
#define REFLTT_OMV     54   /* Open Meter Value */
#define REFLTT_ED      55   /* Escheat Date */
#define REFLTT_RN      56   /* Recipient Name */
#define REFLTT_RS      57   /* Recipient State */
#define REFLTT_RC      58   /* Recipient Country */
#define REFLTT_17      59   /* Reference No. 17 */
#define REFLTT_18      60   /* Reference No. 18 */
#define REFLTT_19      61   /* Reference No. 19 */
#define REFLTT_20      62   /* Reference No. 20 */
#define REFLTT_TOTAL   63   /* Total ID */
#define REFLTT_IFEID   64   /* Instituto Federal Electoral ID */
#define REFLTT_PRD     65   /* Provider Name */
#define REFLTT_FON     66   /* Fonacot Number */
#define REFLTT_CKDATE  67   /* Check Date */
#define REFLTT_CKDTFL  68   /* Check post dated flag */
#define REFLTT_21      69   /* Reference No. 21 */
#define REFLTT_22      70   /* Reference No. 22 */
#define REFLTT_23      71   /* Reference No. 23 */
#define REFLTT_24      72   /* Reference No. 24 */
#define REFLTT_25      73   /* Reference No. 25 */
#define REFLTT_26      74   /* Reference No. 26 */
#define REFLTT_27      75   /* Reference No. 27 */
#define REFLTT_CI      76   /* Component Item */
#define REFLTT_CID     77   /* Component Item Description */
#define REFLTT_FNN     78   /* Fiscal Note Number */
#define REFLTT_CACN    79   /* Customer Account Number */
#define REFLTT_CN      80   /* Customer Name */

#define _REFLTT_MAX 80


/* REFN = Sales Audit Reference Field Numbers */
extern const char REFN[NULL_CODE];

extern const char REFN_1     [NULL_CODE];   /* 1 */
extern const char REFN_2     [NULL_CODE];   /* 2 */
extern const char REFN_3     [NULL_CODE];   /* 3 */
extern const char REFN_4     [NULL_CODE];   /* 4 */
extern const char REFN_5     [NULL_CODE];   /* 5 */
extern const char REFN_6     [NULL_CODE];   /* 6 */
extern const char REFN_7     [NULL_CODE];   /* 7 */
extern const char REFN_8     [NULL_CODE];   /* 8 */
extern const char REFN_9     [NULL_CODE];   /* 9 */
extern const char REFN_10    [NULL_CODE];   /* 10 */
extern const char REFN_11    [NULL_CODE];   /* 11 */
extern const char REFN_12    [NULL_CODE];   /* 12 */
extern const char REFN_13    [NULL_CODE];   /* 13 */
extern const char REFN_14    [NULL_CODE];   /* 14 */
extern const char REFN_15    [NULL_CODE];   /* 15 */
extern const char REFN_16    [NULL_CODE];   /* 16 */
extern const char REFN_17    [NULL_CODE];   /* 17 */
extern const char REFN_18    [NULL_CODE];   /* 18 */
extern const char REFN_19    [NULL_CODE];   /* 19 */
extern const char REFN_20    [NULL_CODE];   /* 20 */
extern const char REFN_21    [NULL_CODE];   /* 21 */
extern const char REFN_22    [NULL_CODE];   /* 22 */
extern const char REFN_23    [NULL_CODE];   /* 23 */
extern const char REFN_24    [NULL_CODE];   /* 24 */
extern const char REFN_25    [NULL_CODE];   /* 25 */
extern const char REFN_26    [NULL_CODE];   /* 26 */
extern const char REFN_27    [NULL_CODE];   /* 27 */

#define REFNTT_1        1   /* 1 */
#define REFNTT_2        2   /* 2 */
#define REFNTT_3        3   /* 3 */
#define REFNTT_4        4   /* 4 */
#define REFNTT_5        5   /* 5 */
#define REFNTT_6        6   /* 6 */
#define REFNTT_7        7   /* 7 */
#define REFNTT_8        8   /* 8 */
#define REFNTT_9        9   /* 9 */
#define REFNTT_10      10   /* 10 */
#define REFNTT_11      11   /* 11 */
#define REFNTT_12      12   /* 12 */
#define REFNTT_13      13   /* 13 */
#define REFNTT_14      14   /* 14 */
#define REFNTT_15      15   /* 15 */
#define REFNTT_16      16   /* 16 */
#define REFNTT_17      17   /* 17 */
#define REFNTT_18      18   /* 18 */
#define REFNTT_19      19   /* 19 */
#define REFNTT_20      20   /* 20 */
#define REFNTT_21      21   /* 21 */
#define REFNTT_22      22   /* 22 */
#define REFNTT_23      23   /* 23 */
#define REFNTT_24      24   /* 24 */
#define REFNTT_25      25   /* 25 */
#define REFNTT_26      26   /* 26 */
#define REFNTT_27      27   /* 27 */

#define _REFNTT_MAX 27


/* RELM = SA Realm Addition Indicator */
extern const char RELM[NULL_CODE];

extern const char RELM_Y     [NULL_CODE];   /* Available */
extern const char RELM_N     [NULL_CODE];   /* Not Available */

#define RELMTT_Y        1   /* Available */
#define RELMTT_N        2   /* Not Available */

#define _RELMTT_MAX 2


/* REOP = Relational Operators */
extern const char REOP[NULL_CODE];

extern const char REOP_EQ    [NULL_CODE];   /* = */
extern const char REOP_LT    [NULL_CODE];   /* < */
extern const char REOP_GT    [NULL_CODE];   /* > */
extern const char REOP_LE    [NULL_CODE];   /* <= */
extern const char REOP_GE    [NULL_CODE];   /* >= */

#define REOPTT_EQ       1   /* = */
#define REOPTT_LT       2   /* < */
#define REOPTT_GT       3   /* > */
#define REOPTT_LE       4   /* <= */
#define REOPTT_GE       5   /* >= */

#define _REOPTT_MAX 5


/* REPC = Report Code */
extern const char REPC[NULL_CODE];

extern const char REPC_ALL   [NULL_CODE];   /* All Locations Reports */
extern const char REPC_NO    [NULL_CODE];   /* No Location Report */

#define REPCTT_ALL      1   /* All Locations Reports */
#define REPCTT_NO       2   /* No Location Report */

#define _REPCTT_MAX 2


/* RETP = Return Policy */
extern const char RETP[NULL_CODE];

extern const char RETP_NONE  [NULL_CODE];   /* Returns not allowed */
extern const char RETP_RCPT  [NULL_CODE];   /* Receipt needed for return */
extern const char RETP_ALL   [NULL_CODE];   /* All returns accepted */
extern const char RETP_MNGR  [NULL_CODE];   /* Manager approval needed for return */
extern const char RETP_ID    [NULL_CODE];   /* Customer ID needed for return */
extern const char RETP_NOSTCK[NULL_CODE];   /* Accept - do not return item to stock */

#define RETPTT_NONE     1   /* Returns not allowed */
#define RETPTT_RCPT     2   /* Receipt needed for return */
#define RETPTT_ALL      3   /* All returns accepted */
#define RETPTT_MNGR     4   /* Manager approval needed for return */
#define RETPTT_ID       5   /* Customer ID needed for return */
#define RETPTT_NOSTCK   6   /* Accept - do not return item to stock */

#define _RETPTT_MAX 6


/* RETY = Error Types */
extern const char RETY[NULL_CODE];

extern const char RETY_BL    [NULL_CODE];   /* Business Logic */
extern const char RETY_OR    [NULL_CODE];   /* API procedure call out of sequence */
extern const char RETY_LK    [NULL_CODE];   /* Record Locked */
extern const char RETY_OE    [NULL_CODE];   /* Oracle Error */
extern const char RETY_FE    [NULL_CODE];   /* Fatal Error */

#define RETYTT_BL       1   /* Business Logic */
#define RETYTT_OR       2   /* API procedure call out of sequence */
#define RETYTT_LK       3   /* Record Locked */
#define RETYTT_OE       4   /* Oracle Error */
#define RETYTT_FE       5   /* Fatal Error */

#define _RETYTT_MAX 5


/* RLCT = Sales Audit Rule Category */
extern const char RLCT[NULL_CODE];

extern const char RLCT_ALL   [NULL_CODE];   /* All */
extern const char RLCT_POS   [NULL_CODE];   /* POS rule */
extern const char RLCT_NON   [NULL_CODE];   /* Non-POS rule */

#define RLCTTT_ALL      1   /* All */
#define RLCTTT_POS      2   /* POS rule */
#define RLCTTT_NON      3   /* Non-POS rule */

#define _RLCTTT_MAX 3


/* RLPT = Sales Audit Rule Prompt */
extern const char RLPT[NULL_CODE];

extern const char RLPT_T     [NULL_CODE];   /* Tolerance */
extern const char RLPT_C     [NULL_CODE];   /* Column Name */
extern const char RLPT_P     [NULL_CODE];   /* Parameter Name */

#define RLPTTT_T        1   /* Tolerance */
#define RLPTTT_C        2   /* Column Name */
#define RLPTTT_P        3   /* Parameter Name */

#define _RLPTTT_MAX 3


/* RLTP = Sales Audit Rule Types */
extern const char RLTP[NULL_CODE];

extern const char RLTP_TAB   [NULL_CODE];   /* Table */
extern const char RLTP_TRN   [NULL_CODE];   /* Transaction */
extern const char RLTP_TOT   [NULL_CODE];   /* Totals */

#define RLTPTT_TAB      1   /* Table */
#define RLTPTT_TRN      2   /* Transaction */
#define RLTPTT_TOT      3   /* Totals */

#define _RLTPTT_MAX 3


/* RLVL = Warehouse Store Assignment Type */
extern const char RLVL[NULL_CODE];


/* RM = Replenishment Methods */
extern const char RM[NULL_CODE];

extern const char RM_C     [NULL_CODE];   /* Constant */
extern const char RM_M     [NULL_CODE];   /* Min / Max */
extern const char RM_F     [NULL_CODE];   /* Floating Point */
extern const char RM_T     [NULL_CODE];   /* Time Supply */
extern const char RM_E     [NULL_CODE];   /* Time Supply - Seasonal */
extern const char RM_TI    [NULL_CODE];   /* Time Supply - Issues */
extern const char RM_D     [NULL_CODE];   /* Dynamic */
extern const char RM_S     [NULL_CODE];   /* Dynamic - Seasonal */
extern const char RM_DI    [NULL_CODE];   /* Dynamic - Issues */
extern const char RM_SO    [NULL_CODE];   /* Store Orders */

#define RMTT_C        1   /* Constant */
#define RMTT_M        2   /* Min / Max */
#define RMTT_F        3   /* Floating Point */
#define RMTT_T        4   /* Time Supply */
#define RMTT_E        5   /* Time Supply - Seasonal */
#define RMTT_TI       6   /* Time Supply - Issues */
#define RMTT_D        7   /* Dynamic */
#define RMTT_S        8   /* Dynamic - Seasonal */
#define RMTT_DI       9   /* Dynamic - Issues */
#define RMTT_SO      10   /* Store Orders */

#define _RMTT_MAX 10


/* RMIL = Replenishment Methods Item Labels */
extern const char RMIL[NULL_CODE];

extern const char RMIL_S     [NULL_CODE];   /* Item */
extern const char RMIL_F     [NULL_CODE];   /* Item Parent */
extern const char RMIL_f     [NULL_CODE];   /* Item */
extern const char RMIL_I     [NULL_CODE];   /* Item List */

#define RMILTT_S        1   /* Item */
#define RMILTT_F        2   /* Item Parent */
#define RMILTT_f        3   /* Item */
#define RMILTT_I        4   /* Item List */

#define _RMILTT_MAX 4


/* RMLB = Replenishment Method Attribute Labels */
extern const char RMLB[NULL_CODE];

extern const char RMLB_MAXSTK[NULL_CODE];   /* Maximum Stock */
extern const char RMLB_MINSTK[NULL_CODE];   /* Minimum Stock */
extern const char RMLB_INCPCT[NULL_CODE];   /* Increment Percent */
extern const char RMLB_MINSUP[NULL_CODE];   /* Minimum Time Supply */
extern const char RMLB_MAXSUP[NULL_CODE];   /* Maximum Time Supply */
extern const char RMLB_INVSEL[NULL_CODE];   /* Inventory Selling Days */
extern const char RMLB_TMSUPH[NULL_CODE];   /* Time Supply Horizon */
extern const char RMLB_LSTSLS[NULL_CODE];   /* Lost Sales Factor */
extern const char RMLB_SERLVL[NULL_CODE];   /* Service Level */
extern const char RMLB_PERCNT[NULL_CODE];   /* % */
extern const char RMLB_DAYS  [NULL_CODE];   /* Days */
extern const char RMLB_SSNIND[NULL_CODE];   /* Seasonal Ind. */
extern const char RMLB_TERMNL[NULL_CODE];   /* Terminal */
extern const char RMLB_STOCK [NULL_CODE];   /* Stock */
extern const char RMLB_SSNID [NULL_CODE];   /* Season ID */
extern const char RMLB_PHASID[NULL_CODE];   /* Phase ID */

#define RMLBTT_MAXSTK   1   /* Maximum Stock */
#define RMLBTT_MINSTK   2   /* Minimum Stock */
#define RMLBTT_INCPCT   3   /* Increment Percent */
#define RMLBTT_MINSUP   4   /* Minimum Time Supply */
#define RMLBTT_MAXSUP   5   /* Maximum Time Supply */
#define RMLBTT_INVSEL   6   /* Inventory Selling Days */
#define RMLBTT_TMSUPH   7   /* Time Supply Horizon */
#define RMLBTT_LSTSLS   8   /* Lost Sales Factor */
#define RMLBTT_SERLVL   9   /* Service Level */
#define RMLBTT_PERCNT  10   /* % */
#define RMLBTT_DAYS    11   /* Days */
#define RMLBTT_SSNIND  12   /* Seasonal Ind. */
#define RMLBTT_TERMNL  13   /* Terminal */
#define RMLBTT_STOCK   14   /* Stock */
#define RMLBTT_SSNID   15   /* Season ID */
#define RMLBTT_PHASID  16   /* Phase ID */

#define _RMLBTT_MAX 16


/* RMST = RMS Admin API Templates */
extern const char RMST[NULL_CODE];

extern const char RMST_RMSADM[NULL_CODE];   /* Administration */
extern const char RMST_RMSSEC[NULL_CODE];   /* Security */
extern const char RMST_RMSFCO[NULL_CODE];   /* Financial Control */
extern const char RMST_RMSBUD[NULL_CODE];   /* Budgets */
extern const char RMST_RMSFIN[NULL_CODE];   /* Finance Administration */
extern const char RMST_RMSFRN[NULL_CODE];   /* Franchise */
extern const char RMST_RMSFND[NULL_CODE];   /* Foundation */
extern const char RMST_RMSINV[NULL_CODE];   /* Inventory */
extern const char RMST_RMSPCO[NULL_CODE];   /* Price and Cost */
extern const char RMST_RMSITM[NULL_CODE];   /* Items */
extern const char RMST_RMSIMP[NULL_CODE];   /* Import */

#define RMSTTT_RMSADM   1   /* Administration */
#define RMSTTT_RMSSEC   2   /* Security */
#define RMSTTT_RMSFCO   3   /* Financial Control */
#define RMSTTT_RMSBUD   4   /* Budgets */
#define RMSTTT_RMSFIN   5   /* Finance Administration */
#define RMSTTT_RMSFRN   6   /* Franchise */
#define RMSTTT_RMSFND   7   /* Foundation */
#define RMSTTT_RMSINV   8   /* Inventory */
#define RMSTTT_RMSPCO   9   /* Price and Cost */
#define RMSTTT_RMSITM  10   /* Items */
#define RMSTTT_RMSIMP  11   /* Import */

#define _RMSTTT_MAX 11


/* ROCT = Replenishment Order Control */
extern const char ROCT[NULL_CODE];

extern const char ROCT_M     [NULL_CODE];   /* Manual */
extern const char ROCT_B     [NULL_CODE];   /* Buyer Worksheet */
extern const char ROCT_S     [NULL_CODE];   /* Semi-Automatic */
extern const char ROCT_A     [NULL_CODE];   /* Automatic */

#define ROCTTT_M        1   /* Manual */
#define ROCTTT_B        2   /* Buyer Worksheet */
#define ROCTTT_S        3   /* Semi-Automatic */
#define ROCTTT_A        4   /* Automatic */

#define _ROCTTT_MAX 4


/* ROLE = Regionality Roles */
extern const char ROLE[NULL_CODE];

extern const char ROLE_BUYER [NULL_CODE];   /* Buyer */
extern const char ROLE_ACCT  [NULL_CODE];   /* Accounting Clerk */
extern const char ROLE_CATG  [NULL_CODE];   /* Category Manager */
extern const char ROLE_REGRL1[NULL_CODE];   /* Regionality Role #1 */
extern const char ROLE_REGRL2[NULL_CODE];   /* Regionality Role #2 */

#define ROLETT_BUYER    1   /* Buyer */
#define ROLETT_ACCT     2   /* Accounting Clerk */
#define ROLETT_CATG     3   /* Category Manager */
#define ROLETT_REGRL1   4   /* Regionality Role #1 */
#define ROLETT_REGRL2   5   /* Regionality Role #2 */

#define _ROLETT_MAX 5


/* RON = OI Report Option Req/Opt/No */
extern const char RON[NULL_CODE];

extern const char RON_R     [NULL_CODE];   /* Required */
extern const char RON_O     [NULL_CODE];   /* Optional */
extern const char RON_N     [NULL_CODE];   /* No */

#define RONTT_R        1   /* Required */
#define RONTT_O        2   /* Optional */
#define RONTT_N        3   /* No */

#define _RONTT_MAX 3


/* RPE = RPM Pricing Event */
extern const char RPE[NULL_CODE];

extern const char RPE_NI    [NULL_CODE];   /* New Item */
extern const char RPE_NL    [NULL_CODE];   /* New Item/Loc Association */
extern const char RPE_CC    [NULL_CODE];   /* Cost Change */
extern const char RPE_PS    [NULL_CODE];   /* Primary Store in a Zone Change */
extern const char RPE_CR    [NULL_CODE];   /* Competitor Retail Change */
extern const char RPE_PC    [NULL_CODE];   /* Primary Competitor Change */

#define RPETT_NI       1   /* New Item */
#define RPETT_NL       2   /* New Item/Loc Association */
#define RPETT_CC       3   /* Cost Change */
#define RPETT_PS       4   /* Primary Store in a Zone Change */
#define RPETT_CR       5   /* Competitor Retail Change */
#define RPETT_PC       6   /* Primary Competitor Change */

#define _RPETT_MAX 6


/* RPO = Req/Prohib/Optional */
extern const char RPO[NULL_CODE];

extern const char RPO_R     [NULL_CODE];   /* Required */
extern const char RPO_P     [NULL_CODE];   /* Prohibited */
extern const char RPO_O     [NULL_CODE];   /* Optional */

#define RPOTT_R        1   /* Required */
#define RPOTT_P        2   /* Prohibited */
#define RPOTT_O        3   /* Optional */

#define _RPOTT_MAX 3


/* RPPM = Report Print Mode */
extern const char RPPM[NULL_CODE];

extern const char RPPM_A     [NULL_CODE];   /* Asynchronous */
extern const char RPPM_S     [NULL_CODE];   /* Synchronous */

#define RPPMTT_A        1   /* Asynchronous */
#define RPPMTT_S        2   /* Synchronous */

#define _RPPMTT_MAX 2


/* RPRC = Replenishment Review Cycle */
extern const char RPRC[NULL_CODE];

extern const char RPRC_1     [NULL_CODE];   /* Every Day */
extern const char RPRC_0     [NULL_CODE];   /* Every Week */
extern const char RPRC_2     [NULL_CODE];   /* Every 2 Weeks */
extern const char RPRC_3     [NULL_CODE];   /* Every 3 Weeks */
extern const char RPRC_4     [NULL_CODE];   /* Every 4 Weeks */
extern const char RPRC_5     [NULL_CODE];   /* Every 5 Weeks */
extern const char RPRC_6     [NULL_CODE];   /* Every 6 Weeks */
extern const char RPRC_7     [NULL_CODE];   /* Every 7 Weeks */
extern const char RPRC_8     [NULL_CODE];   /* Every 8 Weeks */
extern const char RPRC_9     [NULL_CODE];   /* Every 9 Weeks */
extern const char RPRC_10    [NULL_CODE];   /* Every 10 Weeks */
extern const char RPRC_11    [NULL_CODE];   /* Every 11 Weeks */
extern const char RPRC_12    [NULL_CODE];   /* Every 12 Weeks */
extern const char RPRC_13    [NULL_CODE];   /* Every 13 Weeks */
extern const char RPRC_14    [NULL_CODE];   /* Every 14 Weeks */

#define RPRCTT_1        1   /* Every Day */
#define RPRCTT_0        2   /* Every Week */
#define RPRCTT_2        3   /* Every 2 Weeks */
#define RPRCTT_3        4   /* Every 3 Weeks */
#define RPRCTT_4        5   /* Every 4 Weeks */
#define RPRCTT_5        6   /* Every 5 Weeks */
#define RPRCTT_6        7   /* Every 6 Weeks */
#define RPRCTT_7        8   /* Every 7 Weeks */
#define RPRCTT_8        9   /* Every 8 Weeks */
#define RPRCTT_9       10   /* Every 9 Weeks */
#define RPRCTT_10      11   /* Every 10 Weeks */
#define RPRCTT_11      12   /* Every 11 Weeks */
#define RPRCTT_12      13   /* Every 12 Weeks */
#define RPRCTT_13      14   /* Every 13 Weeks */
#define RPRCTT_14      15   /* Every 14 Weeks */

#define _RPRCTT_MAX 15


/* RQAC = Reqfind action list box */
extern const char RQAC[NULL_CODE];

extern const char RQAC_NEW   [NULL_CODE];   /* New */
extern const char RQAC_VIEW  [NULL_CODE];   /* View */
extern const char RQAC_EDIT  [NULL_CODE];   /* Edit */
extern const char RQAC_PRINT [NULL_CODE];   /* Print */
extern const char RQAC_FEED  [NULL_CODE];   /* Feedback */

#define RQACTT_NEW      1   /* New */
#define RQACTT_VIEW     2   /* View */
#define RQACTT_EDIT     3   /* Edit */
#define RQACTT_PRINT    4   /* Print */
#define RQACTT_FEED     5   /* Feedback */

#define _RQACTT_MAX 5


/* RQFD = Requisition Feedback Indicator */
extern const char RQFD[NULL_CODE];

extern const char RQFD_0     [NULL_CODE];   /* Not Fed Back */
extern const char RQFD_1     [NULL_CODE];   /* Completed */

#define RQFDTT_0        1   /* Not Fed Back */
#define RQFDTT_1        2   /* Completed */

#define _RQFDTT_MAX 2


/* RQOR = Requisition Origin */
extern const char RQOR[NULL_CODE];

extern const char RQOR_1     [NULL_CODE];   /* System Generated */
extern const char RQOR_2     [NULL_CODE];   /* Manually Input */

#define RQORTT_1        1   /* System Generated */
#define RQORTT_2        2   /* Manually Input */

#define _RQORTT_MAX 2


/* RQPI = Requistion Print Indicator */
extern const char RQPI[NULL_CODE];

extern const char RQPI_0     [NULL_CODE];   /* Not Printed */
extern const char RQPI_1     [NULL_CODE];   /* Printed */

#define RQPITT_0        1   /* Not Printed */
#define RQPITT_1        2   /* Printed */

#define _RQPITT_MAX 2


/* RSAT = ReSA Admin API Templates */
extern const char RSAT[NULL_CODE];

extern const char RSAT_RSAEC [NULL_CODE];   /* Error Codes */
extern const char RSAT_RSARF [NULL_CODE];   /* Reference Field */
extern const char RSAT_RSACR [NULL_CODE];   /* Currency Rounding Rules */
extern const char RSAT_RSAGLC[NULL_CODE];   /* GL Cross Reference */
extern const char RSAT_RSASD [NULL_CODE];   /* Store Data */
extern const char RSAT_RSATT [NULL_CODE];   /* Tender Types */
extern const char RSAT_RSACD [NULL_CODE];   /* Constants */

#define RSATTT_RSAEC    1   /* Error Codes */
#define RSATTT_RSARF    2   /* Reference Field */
#define RSATTT_RSACR    3   /* Currency Rounding Rules */
#define RSATTT_RSAGLC   4   /* GL Cross Reference */
#define RSATTT_RSASD    5   /* Store Data */
#define RSATTT_RSATT    6   /* Tender Types */
#define RSATTT_RSACD    7   /* Constants */

#define _RSATTT_MAX 7


/* RSLT = Replenishment Service Level Types */
extern const char RSLT[NULL_CODE];

extern const char RSLT_S     [NULL_CODE];   /* Standard */
extern const char RSLT_SS    [NULL_CODE];   /* Simple Sales */

#define RSLTTT_S        1   /* Standard */
#define RSLTTT_SS       2   /* Simple Sales */

#define _RSLTTT_MAX 2


/* RSUF = Restraint Suffix */
extern const char RSUF[NULL_CODE];

extern const char RSUF_SU    [NULL_CODE];   /* Suffix1 */
extern const char RSUF_SU2   [NULL_CODE];   /* Suffix2 */
extern const char RSUF_SU3   [NULL_CODE];   /* Suffix3 */

#define RSUFTT_SU       1   /* Suffix1 */
#define RSUFTT_SU2      2   /* Suffix2 */
#define RSUFTT_SU3      3   /* Suffix3 */

#define _RSUFTT_MAX 3


/* RSYS = RTLog Originating System */
extern const char RSYS[NULL_CODE];

extern const char RSYS_OMS   [NULL_CODE];   /* OMS */
extern const char RSYS_POS   [NULL_CODE];   /* POS */

#define RSYSTT_OMS      1   /* OMS */
#define RSYSTT_POS      2   /* POS */

#define _RSYSTT_MAX 2


/* RTLT = Retail Label Type */
extern const char RTLT[NULL_CODE];

extern const char RTLT_PREP  [NULL_CODE];   /* Pre-Priced */
extern const char RTLT_COFF  [NULL_CODE];   /* Cents Off */
extern const char RTLT_EXTRA [NULL_CODE];   /* Extra Product */

#define RTLTTT_PREP     1   /* Pre-Priced */
#define RTLTTT_COFF     2   /* Cents Off */
#define RTLTTT_EXTRA    3   /* Extra Product */

#define _RTLTTT_MAX 3


/* RTSF = RTV/RAC Transfer */
extern const char RTSF[NULL_CODE];

extern const char RTSF_E     [NULL_CODE];   /* Intercompany */
extern const char RTSF_A     [NULL_CODE];   /* Intra-Company */

#define RTSFTT_E        1   /* Intercompany */
#define RTSFTT_A        2   /* Intra-Company */

#define _RTSFTT_MAX 2


/* RTV = Label RTV */
extern const char RTV[NULL_CODE];

extern const char RTV_R     [NULL_CODE];   /* RTV */

#define RTVTT_R        1   /* RTV */

#define _RTVTT_MAX 1


/* RTVR = Return to Vendor Reasons */
extern const char RTVR[NULL_CODE];

extern const char RTVR_U     [NULL_CODE];   /* Unavailable Inventory */
extern const char RTVR_O     [NULL_CODE];   /* Overstock */
extern const char RTVR_W     [NULL_CODE];   /* Externally Initiated RTV */

#define RTVRTT_U        2   /* Unavailable Inventory */
#define RTVRTT_O        3   /* Overstock */
#define RTVRTT_W        4   /* Externally Initiated RTV */

#define _RTVRTT_MAX 4


/* RTVS = MRT RTV Create Status */
extern const char RTVS[NULL_CODE];

extern const char RTVS_I     [NULL_CODE];   /* Input */
extern const char RTVS_A     [NULL_CODE];   /* Approved */
extern const char RTVS_M     [NULL_CODE];   /* Manual */

#define RTVSTT_I        1   /* Input */
#define RTVSTT_A        2   /* Approved */
#define RTVSTT_M        3   /* Manual */

#define _RTVSTT_MAX 3


/* RTYP = Restraint Type */
extern const char RTYP[NULL_CODE];

extern const char RTYP_TR    [NULL_CODE];   /* Test Restraint1 */
extern const char RTYP_TR2   [NULL_CODE];   /* Test Restraint2 */
extern const char RTYP_TR3   [NULL_CODE];   /* Test Restraint3 */

#define RTYPTT_TR       1   /* Test Restraint1 */
#define RTYPTT_TR2      2   /* Test Restraint2 */
#define RTYPTT_TR3      3   /* Test Restraint3 */

#define _RTYPTT_MAX 3


/* RUAE = Receiver Unit Adjustment Errors */
extern const char RUAE[NULL_CODE];

extern const char RUAE_RUAL  [NULL_CODE];   /* Record Locked */
extern const char RUAE_RUAU  [NULL_CODE];   /* Available Qty less than Adjusted Qty */

#define RUAETT_RUAL     1   /* Record Locked */
#define RUAETT_RUAU     2   /* Available Qty less than Adjusted Qty */

#define _RUAETT_MAX 2


/* RUCA =  Receiver Cost Adjustment */
extern const char RUCA[NULL_CODE];

extern const char RUCA_F     [NULL_CODE];   /* FIFO */
extern const char RUCA_S     [NULL_CODE];   /* Standard */

#define RUCATT_F        1   /* FIFO */
#define RUCATT_S        2   /* Standard */

#define _RUCATT_MAX 2


/* RUCI = List of RTV Unit Cost Values */
extern const char RUCI[NULL_CODE];

extern const char RUCI_A     [NULL_CODE];   /* Average Cost */
extern const char RUCI_S     [NULL_CODE];   /* Standard Cost */
extern const char RUCI_R     [NULL_CODE];   /* Last Receipt Cost */

#define RUCITT_A        1   /* Average Cost */
#define RUCITT_S        2   /* Standard Cost */
#define RUCITT_R        3   /* Last Receipt Cost */

#define _RUCITT_MAX 3


/* RVST = Return to Vendor Status */
extern const char RVST[NULL_CODE];

extern const char RVST_5     [NULL_CODE];   /* Input */
extern const char RVST_10    [NULL_CODE];   /* Approved */
extern const char RVST_12    [NULL_CODE];   /* In Progress */
extern const char RVST_15    [NULL_CODE];   /* Shipped */
extern const char RVST_20    [NULL_CODE];   /* Cancelled */

#define RVSTTT_5        1   /* Input */
#define RVSTTT_10       2   /* Approved */
#define RVSTTT_12       3   /* In Progress */
#define RVSTTT_15       4   /* Shipped */
#define RVSTTT_20       5   /* Cancelled */

#define _RVSTTT_MAX 5


/* RVTP = Origin of Item Returned to Vendor */
extern const char RVTP[NULL_CODE];

extern const char RVTP_I     [NULL_CODE];   /* Inventory */
extern const char RVTP_Q     [NULL_CODE];   /* Quality Control */

#define RVTPTT_I        1   /* Inventory */
#define RVTPTT_Q        2   /* Quality Control */

#define _RVTPTT_MAX 2


/* S9A1 = Spreadsheet action types - Only NEW */
extern const char S9A1[NULL_CODE];

extern const char S9A1_NEW   [NULL_CODE];   /* Create */

#define S9A1TT_NEW      1   /* Create */

#define _S9A1TT_MAX 1


/* S9A2 = Spreadsheet action types - Only MOD */
extern const char S9A2[NULL_CODE];

extern const char S9A2_MOD   [NULL_CODE];   /* Update */

#define S9A2TT_MOD      1   /* Update */

#define _S9A2TT_MAX 1


/* S9A3 = Spreadsheet action types - Only Create and Delete */
extern const char S9A3[NULL_CODE];

extern const char S9A3_NEW   [NULL_CODE];   /* Create */
extern const char S9A3_DEL   [NULL_CODE];   /* Delete */

#define S9A3TT_NEW      1   /* Create */
#define S9A3TT_DEL      2   /* Delete */

#define _S9A3TT_MAX 2


/* S9A4 = Spreadsheet action types - Only Create and Update */
extern const char S9A4[NULL_CODE];

extern const char S9A4_NEW   [NULL_CODE];   /* Create */
extern const char S9A4_MOD   [NULL_CODE];   /* Update */

#define S9A4TT_NEW      1   /* Create */
#define S9A4TT_MOD      2   /* Update */

#define _S9A4TT_MAX 2


/* S9A5 = Spreadsheet action types - Only Update and Delete */
extern const char S9A5[NULL_CODE];

extern const char S9A5_MOD   [NULL_CODE];   /* Update */
extern const char S9A5_DEL   [NULL_CODE];   /* Delete */

#define S9A5TT_MOD      1   /* Update */
#define S9A5TT_DEL      2   /* Delete */

#define _S9A5TT_MAX 2


/* S9AT = Spreadsheet action types */
extern const char S9AT[NULL_CODE];

extern const char S9AT_NEW   [NULL_CODE];   /* Create */
extern const char S9AT_MOD   [NULL_CODE];   /* Update */
extern const char S9AT_DEL   [NULL_CODE];   /* Delete */

#define S9ATTT_NEW      1   /* Create */
#define S9ATTT_MOD      2   /* Update */
#define S9ATTT_DEL      3   /* Delete */

#define _S9ATTT_MAX 3


/* S9LV = Spreadsheet List of Values */
extern const char S9LV[NULL_CODE];

extern const char S9LV_IS9T  [NULL_CODE];   /* Item Related Templates */
extern const char S9LV_IS9TT [NULL_CODE];   /* Item API Templates */
extern const char S9LV_POS9T [NULL_CODE];   /* Purchase Order API Templates */
extern const char S9LV_RMSADM[NULL_CODE];   /* Administration */
extern const char S9LV_RMSFIN[NULL_CODE];   /* Finance Administration */
extern const char S9LV_RMSFND[NULL_CODE];   /* Foundation */
extern const char S9LV_RMSFRN[NULL_CODE];   /* Franchise */
extern const char S9LV_RMSIMP[NULL_CODE];   /* Import */
extern const char S9LV_RMSINV[NULL_CODE];   /* Inventory */
extern const char S9LV_RMSITM[NULL_CODE];   /* Items */
extern const char S9LV_RMSPCO[NULL_CODE];   /* Price and Cost */
extern const char S9LV_RSAEC [NULL_CODE];   /* Error Codes */
extern const char S9LV_RSARF [NULL_CODE];   /* Reference Field */
extern const char S9LV_RSACR [NULL_CODE];   /* Currency Rounding Rules */
extern const char S9LV_RSAGLC[NULL_CODE];   /* GL Cross Reference */
extern const char S9LV_RSASD [NULL_CODE];   /* Store Data */
extern const char S9LV_RSATT [NULL_CODE];   /* Tender Types */
extern const char S9LV_RSACD [NULL_CODE];   /* Constants */

#define S9LVTT_IS9T     1   /* Item Related Templates */
#define S9LVTT_IS9TT    2   /* Item API Templates */
#define S9LVTT_POS9T    3   /* Purchase Order API Templates */
#define S9LVTT_RMSADM   4   /* Administration */
#define S9LVTT_RMSFIN   5   /* Finance Administration */
#define S9LVTT_RMSFND   6   /* Foundation */
#define S9LVTT_RMSFRN   7   /* Franchise */
#define S9LVTT_RMSIMP   8   /* Import */
#define S9LVTT_RMSINV   9   /* Inventory */
#define S9LVTT_RMSITM  10   /* Items */
#define S9LVTT_RMSPCO  11   /* Price and Cost */
#define S9LVTT_RSAEC   12   /* Error Codes */
#define S9LVTT_RSARF   13   /* Reference Field */
#define S9LVTT_RSACR   15   /* Currency Rounding Rules */
#define S9LVTT_RSAGLC  16   /* GL Cross Reference */
#define S9LVTT_RSASD   17   /* Store Data */
#define S9LVTT_RSATT   18   /* Tender Types */
#define S9LVTT_RSACD   19   /* Constants */

#define _S9LVTT_MAX 19


/* S9TC = Spreadsheet Template Categories */
extern const char S9TC[NULL_CODE];

extern const char S9TC_IS9T  [NULL_CODE];   /* Item Related Templates */
extern const char S9TC_RSARF [NULL_CODE];   /* Reference Field Template */

#define S9TCTT_IS9T     1   /* Item Related Templates */
#define S9TCTT_RSARF    4   /* Reference Field Template */

#define _S9TCTT_MAX 4


/* SAAC = Sales Audit Action Codes */
extern const char SAAC[NULL_CODE];

extern const char SAAC_NEW   [NULL_CODE];   /* New */
extern const char SAAC_VIEW  [NULL_CODE];   /* View */
extern const char SAAC_EDIT  [NULL_CODE];   /* Edit */
extern const char SAAC_PV    [NULL_CODE];   /* Post Void */

#define SAACTT_NEW      1   /* New */
#define SAACTT_VIEW     2   /* View */
#define SAACTT_EDIT     3   /* Edit */
#define SAACTT_PV       4   /* Post Void */

#define _SAACTT_MAX 4


/* SAAS = Sales Audit Audit Status */
extern const char SAAS[NULL_CODE];

extern const char SAAS_U     [NULL_CODE];   /* Unaudited */
extern const char SAAS_H     [NULL_CODE];   /* Errors Pending */
extern const char SAAS_R     [NULL_CODE];   /* Re-Totaling/Auditing Required */
extern const char SAAS_T     [NULL_CODE];   /* Totaled */
extern const char SAAS_A     [NULL_CODE];   /* Audited */

#define SAASTT_U        1   /* Unaudited */
#define SAASTT_H        3   /* Errors Pending */
#define SAASTT_R        4   /* Re-Totaling/Auditing Required */
#define SAASTT_T        5   /* Totaled */
#define SAASTT_A        6   /* Audited */

#define _SAASTT_MAX 6


/* SABA = Sales Audit Bank Account Types */
extern const char SABA[NULL_CODE];

extern const char SABA_C     [NULL_CODE];   /* Checking */
extern const char SABA_S     [NULL_CODE];   /* Savings */

#define SABATT_C        1   /* Checking */
#define SABATT_S        2   /* Savings */

#define _SABATT_MAX 2


/* SABL = Sales Audit Balancing Level */
extern const char SABL[NULL_CODE];

extern const char SABL_S     [NULL_CODE];   /* Store */
extern const char SABL_R     [NULL_CODE];   /* Register Summary */
extern const char SABL_C     [NULL_CODE];   /* Cashier Summary */
extern const char SABL_CS    [NULL_CODE];   /* Cashier/Register Summary */

#define SABLTT_S        1   /* Store */
#define SABLTT_R        2   /* Register Summary */
#define SABLTT_C        3   /* Cashier Summary */
#define SABLTT_CS       4   /* Cashier/Register Summary */

#define _SABLTT_MAX 4


/* SACA = Sales Audit Customer Attribute Types */
extern const char SACA[NULL_CODE];

extern const char SACA_ERR   [NULL_CODE];   /* Error */
extern const char SACA_TERM  [NULL_CODE];   /* Termination Record marker */
extern const char SACA_SAGC  [NULL_CODE];   /* Sales Audit Gender Codes */
extern const char SACA_SAIN  [NULL_CODE];   /* Sales Audit Income Codes */
extern const char SACA_SARD  [NULL_CODE];   /* Sales Audit Retailer Defined Codes */

#define SACATT_ERR      1   /* Error */
#define SACATT_TERM     2   /* Termination Record marker */
#define SACATT_SAGC     3   /* Sales Audit Gender Codes */
#define SACATT_SAIN     4   /* Sales Audit Income Codes */
#define SACATT_SARD     5   /* Sales Audit Retailer Defined Codes */

#define _SACATT_MAX 5


/* SACD = Sales Audit Comp Store Date */
extern const char SACD[NULL_CODE];

extern const char SACD_R     [NULL_CODE];   /* Remodel Date */
extern const char SACD_S     [NULL_CODE];   /* Store Open Date */
extern const char SACD_A     [NULL_CODE];   /* Acquire Date */

#define SACDTT_R        1   /* Remodel Date */
#define SACDTT_S        2   /* Store Open Date */
#define SACDTT_A        3   /* Acquire Date */

#define _SACDTT_MAX 3


/* SACI = Sales Audit Combined Total Ind */
extern const char SACI[NULL_CODE];

extern const char SACI_Y     [NULL_CODE];   /* Combined Total */
extern const char SACI_N     [NULL_CODE];   /* Raw Data */

#define SACITT_Y        1   /* Combined Total */
#define SACITT_N        2   /* Raw Data */

#define _SACITT_MAX 2


/* SACM = Sales Audit Comment Type */
extern const char SACM[NULL_CODE];

extern const char SACM_SATR  [NULL_CODE];   /* ReSA Transaction No */
extern const char SACM_SATL  [NULL_CODE];   /* Total */
extern const char SACM_SABG  [NULL_CODE];   /* Balance Group */
extern const char SACM_SASD  [NULL_CODE];   /* Store/Day */

#define SACMTT_SATR     1   /* ReSA Transaction No */
#define SACMTT_SATL     2   /* Total */
#define SACMTT_SABG     3   /* Balance Group */
#define SACMTT_SASD     4   /* Store/Day */

#define _SACMTT_MAX 4


/* SACO = Sales Audit Comparison Operators */
extern const char SACO[NULL_CODE];

extern const char SACO_EQ    [NULL_CODE];   /* = */
extern const char SACO_NE    [NULL_CODE];   /* != */
extern const char SACO_GT    [NULL_CODE];   /* > */
extern const char SACO_LT    [NULL_CODE];   /* < */
extern const char SACO_GE    [NULL_CODE];   /* >= */
extern const char SACO_LE    [NULL_CODE];   /* <= */
extern const char SACO_IN    [NULL_CODE];   /* in */
extern const char SACO_NOTIN [NULL_CODE];   /* not in */
extern const char SACO_NULL  [NULL_CODE];   /* is NULL */
extern const char SACO_NOTNUL[NULL_CODE];   /* is not NULL */
extern const char SACO_B     [NULL_CODE];   /* between */
extern const char SACO_NB    [NULL_CODE];   /* not between */
extern const char SACO_TOL   [NULL_CODE];   /* within tolerance */
extern const char SACO_NOTTOL[NULL_CODE];   /* not within tolerance */

#define SACOTT_EQ       1   /* = */
#define SACOTT_NE       2   /* != */
#define SACOTT_GT       3   /* > */
#define SACOTT_LT       4   /* < */
#define SACOTT_GE       5   /* >= */
#define SACOTT_LE       6   /* <= */
#define SACOTT_IN       7   /* in */
#define SACOTT_NOTIN    8   /* not in */
#define SACOTT_NULL    10   /* is NULL */
#define SACOTT_NOTNUL  11   /* is not NULL */
#define SACOTT_B       12   /* between */
#define SACOTT_NB      13   /* not between */
#define SACOTT_TOL     14   /* within tolerance */
#define SACOTT_NOTTOL  15   /* not within tolerance */

#define _SACOTT_MAX 15


/* SACS = Sales Audit Count/Sum */
extern const char SACS[NULL_CODE];

extern const char SACS_C     [NULL_CODE];   /* Count */
extern const char SACS_S     [NULL_CODE];   /* Sum */

#define SACSTT_C      999   /* Count */
#define SACSTT_S      999   /* Sum */

#define _SACSTT_MAX 9992


/* SACT = Shipment Actions */
extern const char SACT[NULL_CODE];

extern const char SACT_ITEM  [NULL_CODE];   /* View Shipments by Item */
extern const char SACT_ORDR  [NULL_CODE];   /* View Shipments by Order */
extern const char SACT_MTCH  [NULL_CODE];   /* Match Shipments */

#define SACTTT_ITEM     1   /* View Shipments by Item */
#define SACTTT_ORDR     2   /* View Shipments by Order */
#define SACTTT_MTCH     3   /* Match Shipments */

#define _SACTTT_MAX 3


/* SADS = Sales Audit Data Status */
extern const char SADS[NULL_CODE];

extern const char SADS_R     [NULL_CODE];   /* Ready for Import */
extern const char SADS_L     [NULL_CODE];   /* Loading */
extern const char SADS_P     [NULL_CODE];   /* Partially Loaded */
extern const char SADS_F     [NULL_CODE];   /* Fully Loaded */
extern const char SADS_G     [NULL_CODE];   /* Purged */

#define SADSTT_R        1   /* Ready for Import */
#define SADSTT_L        2   /* Loading */
#define SADSTT_P        3   /* Partially Loaded */
#define SADSTT_F        4   /* Fully Loaded */
#define SADSTT_G        5   /* Purged */

#define _SADSTT_MAX 5


/* SADT = Sales Audit Discount Types */
extern const char SADT[NULL_CODE];

extern const char SADT_ERR   [NULL_CODE];   /* Error */
extern const char SADT_TERM  [NULL_CODE];   /* Termination Record marker */
extern const char SADT_SCOUP [NULL_CODE];   /* Store Coupon */
extern const char SADT_CSTMER[NULL_CODE];   /* Merchandise Accommodation */
extern const char SADT_CSTSH [NULL_CODE];   /* Shipping and Handling Accommodation */
extern const char SADT_CSTVAS[NULL_CODE];   /* Value Added Service Accommodation */
extern const char SADT_SATSPL[NULL_CODE];   /* Saturday Morning Special */
extern const char SADT_SENCIT[NULL_CODE];   /* Senior Citizen */
extern const char SADT_CMPSPL[NULL_CODE];   /* Competition Special */
extern const char SADT_EMPDSC[NULL_CODE];   /* Employee Discount */
extern const char SADT_S     [NULL_CODE];   /* Incorrect Signage */
extern const char SADT_MS    [NULL_CODE];   /* Manager Discretion */
extern const char SADT_CP    [NULL_CODE];   /* Price Guarantee */
extern const char SADT_D     [NULL_CODE];   /* Damage Adjustment */
extern const char SADT_NEWPRC[NULL_CODE];   /* New Price Rule */
extern const char SADT_DOC   [NULL_CODE];   /* Document */
extern const char SADT_MCOUP [NULL_CODE];   /* @SUH1@ Coupon */
extern const char SADT_REFUND[NULL_CODE];   /* Refund Proration */
extern const char SADT_CALWAR[NULL_CODE];   /* Warranty Price */

#define SADTTT_ERR      1   /* Error */
#define SADTTT_TERM     2   /* Termination Record marker */
#define SADTTT_SCOUP    5   /* Store Coupon */
#define SADTTT_CSTMER   6   /* Merchandise Accommodation */
#define SADTTT_CSTSH    7   /* Shipping and Handling Accommodation */
#define SADTTT_CSTVAS   8   /* Value Added Service Accommodation */
#define SADTTT_SATSPL   9   /* Saturday Morning Special */
#define SADTTT_SENCIT  10   /* Senior Citizen */
#define SADTTT_CMPSPL  11   /* Competition Special */
#define SADTTT_EMPDSC  12   /* Employee Discount */
#define SADTTT_S       51   /* Incorrect Signage */
#define SADTTT_MS      52   /* Manager Discretion */
#define SADTTT_CP      53   /* Price Guarantee */
#define SADTTT_D       54   /* Damage Adjustment */
#define SADTTT_NEWPRC  55   /* New Price Rule */
#define SADTTT_DOC     56   /* Document */
#define SADTTT_MCOUP   57   /* @SUH1@ Coupon */
#define SADTTT_REFUND  58   /* Refund Proration */
#define SADTTT_CALWAR  59   /* Warranty Price */

#define _SADTTT_MAX 59


/* SAES = Sales Audit Export Status */
extern const char SAES[NULL_CODE];

extern const char SAES_R     [NULL_CODE];   /* Ready for Export */
extern const char SAES_P     [NULL_CODE];   /* Processed */
extern const char SAES_E     [NULL_CODE];   /* Exported */

#define SAESTT_R        1   /* Ready for Export */
#define SAESTT_P        2   /* Processed */
#define SAESTT_E        3   /* Exported */

#define _SAESTT_MAX 3


/* SAET = Sales Audit Excheat Type */
extern const char SAET[NULL_CODE];

extern const char SAET_E     [NULL_CODE];   /* Escheatment */
extern const char SAET_N     [NULL_CODE];   /* Income Adjustment */

#define SAETTT_E        1   /* Escheatment */
#define SAETTT_N        2   /* Income Adjustment */

#define _SAETTT_MAX 2


/* SAFD = Sales Audit Full Disclosure Indicator */
extern const char SAFD[NULL_CODE];

extern const char SAFD_P     [NULL_CODE];   /* Positive Transaction/Total */
extern const char SAFD_N     [NULL_CODE];   /* Negative (Reversal) Transaction/Total */

#define SAFDTT_P        1   /* Positive Transaction/Total */
#define SAFDTT_N        2   /* Negative (Reversal) Transaction/Total */

#define _SAFDTT_MAX 2


/* SAGC = Sales Audit Gender Codes */
extern const char SAGC[NULL_CODE];

extern const char SAGC_M     [NULL_CODE];   /* Male */
extern const char SAGC_F     [NULL_CODE];   /* Female */
extern const char SAGC_U     [NULL_CODE];   /* Unknown */
extern const char SAGC_ERR   [NULL_CODE];   /* Error */

#define SAGCTT_M        1   /* Male */
#define SAGCTT_F        2   /* Female */
#define SAGCTT_U        3   /* Unknown */
#define SAGCTT_ERR      4   /* Error */

#define _SAGCTT_MAX 4


/* SAIN = Sales Audit Income Codes */
extern const char SAIN[NULL_CODE];

extern const char SAIN_0     [NULL_CODE];   /* 0 - 9,999 */
extern const char SAIN_10    [NULL_CODE];   /* 10,000 - 14,999 */
extern const char SAIN_15    [NULL_CODE];   /* 15,000 - 19,999 */
extern const char SAIN_20    [NULL_CODE];   /* 20,000 - 24,999 */
extern const char SAIN_25    [NULL_CODE];   /* 25,000 - 29,999 */
extern const char SAIN_30    [NULL_CODE];   /* F,000 - 34,999 */
extern const char SAIN_35    [NULL_CODE];   /* 35,000 - 39,999 */
extern const char SAIN_40    [NULL_CODE];   /* 40,000 - 44,999 */
extern const char SAIN_45    [NULL_CODE];   /* 45,000 - 49,999 */
extern const char SAIN_50    [NULL_CODE];   /* 50,000 - 54,999 */
extern const char SAIN_55    [NULL_CODE];   /* 55,000 - 59,999 */
extern const char SAIN_60    [NULL_CODE];   /* 60,000 - 64,999 */
extern const char SAIN_65    [NULL_CODE];   /* 65,000 - 69,999 */
extern const char SAIN_70    [NULL_CODE];   /* 70,000 - 74,999 */
extern const char SAIN_75    [NULL_CODE];   /* 75,000 - 79,999 */
extern const char SAIN_80    [NULL_CODE];   /* 80,000 - 84,999 */
extern const char SAIN_85    [NULL_CODE];   /* 85,000 - 89,999 */
extern const char SAIN_90    [NULL_CODE];   /* 90,000 - 94,999 */
extern const char SAIN_95    [NULL_CODE];   /* 95,000 - 99,999 */
extern const char SAIN_100   [NULL_CODE];   /* 100,000 - 149,999 */
extern const char SAIN_150   [NULL_CODE];   /* 150,000 - 199,999 */
extern const char SAIN_200   [NULL_CODE];   /* 200,000 - 249,999 */
extern const char SAIN_RICH  [NULL_CODE];   /* Over 250,000 */
extern const char SAIN_ERR   [NULL_CODE];   /* Erroneous Value */

#define SAINTT_0        1   /* 0 - 9,999 */
#define SAINTT_10       2   /* 10,000 - 14,999 */
#define SAINTT_15       3   /* 15,000 - 19,999 */
#define SAINTT_20       4   /* 20,000 - 24,999 */
#define SAINTT_25       5   /* 25,000 - 29,999 */
#define SAINTT_30       6   /* F,000 - 34,999 */
#define SAINTT_35       7   /* 35,000 - 39,999 */
#define SAINTT_40       8   /* 40,000 - 44,999 */
#define SAINTT_45       9   /* 45,000 - 49,999 */
#define SAINTT_50      10   /* 50,000 - 54,999 */
#define SAINTT_55      11   /* 55,000 - 59,999 */
#define SAINTT_60      12   /* 60,000 - 64,999 */
#define SAINTT_65      13   /* 65,000 - 69,999 */
#define SAINTT_70      14   /* 70,000 - 74,999 */
#define SAINTT_75      15   /* 75,000 - 79,999 */
#define SAINTT_80      16   /* 80,000 - 84,999 */
#define SAINTT_85      17   /* 85,000 - 89,999 */
#define SAINTT_90      18   /* 90,000 - 94,999 */
#define SAINTT_95      19   /* 95,000 - 99,999 */
#define SAINTT_100     20   /* 100,000 - 149,999 */
#define SAINTT_150     21   /* 150,000 - 199,999 */
#define SAINTT_200     22   /* 200,000 - 249,999 */
#define SAINTT_RICH    23   /* Over 250,000 */
#define SAINTT_ERR     24   /* Erroneous Value */

#define _SAINTT_MAX 24


/* SAIS = Sales Audit Import Status */
extern const char SAIS[NULL_CODE];

extern const char SAIS_R     [NULL_CODE];   /* Ready for Import */
extern const char SAIS_L     [NULL_CODE];   /* Loaded into Database */
extern const char SAIS_U     [NULL_CODE];   /* Unexpected Import */

#define SAISTT_R        1   /* Ready for Import */
#define SAISTT_L        2   /* Loaded into Database */
#define SAISTT_U        3   /* Unexpected Import */

#define _SAISTT_MAX 3


/* SAIT = Sales Audit Item Types */
extern const char SAIT[NULL_CODE];

extern const char SAIT_ERR   [NULL_CODE];   /* Error */
extern const char SAIT_TERM  [NULL_CODE];   /* Termination Record Marker */
extern const char SAIT_ITEM  [NULL_CODE];   /* Item */
extern const char SAIT_REF   [NULL_CODE];   /* Reference Item */
extern const char SAIT_GCN   [NULL_CODE];   /* Voucher Number */
extern const char SAIT_NMITEM[NULL_CODE];   /* Non Merchandise Item */

#define SAITTT_ERR      1   /* Error */
#define SAITTT_TERM     2   /* Termination Record Marker */
#define SAITTT_ITEM     3   /* Item */
#define SAITTT_REF      4   /* Reference Item */
#define SAITTT_GCN      5   /* Voucher Number */
#define SAITTT_NMITEM   6   /* Non Merchandise Item */

#define _SAITTT_MAX 6


/* SALT = Sales Audit Lock Types */
extern const char SALT[NULL_CODE];

extern const char SALT_R     [NULL_CODE];   /* Read Only Lock */
extern const char SALT_W     [NULL_CODE];   /* Write Lock */

#define SALTTT_R        1   /* Read Only Lock */
#define SALTTT_W        2   /* Write Lock */

#define _SALTTT_MAX 2


/* SAMS = Sales Audit Missing Status */
extern const char SAMS[NULL_CODE];

extern const char SAMS_A     [NULL_CODE];   /* Added */
extern const char SAMS_D     [NULL_CODE];   /* Deleted */
extern const char SAMS_M     [NULL_CODE];   /* Missing */

#define SAMSTT_A        1   /* Added */
#define SAMSTT_D        2   /* Deleted */
#define SAMSTT_M        3   /* Missing */

#define _SAMSTT_MAX 3


/* SAND = Sales Audit Not Defined */
extern const char SAND[NULL_CODE];

extern const char SAND_N     [NULL_CODE];   /* Not Defined */
extern const char SAND_I     [NULL_CODE];   /* Invalid */
extern const char SAND_ERR   [NULL_CODE];   /* Record in Error */

#define SANDTT_N        1   /* Not Defined */
#define SANDTT_I        2   /* Invalid */
#define SANDTT_ERR      3   /* Record in Error */

#define _SANDTT_MAX 3


/* SAO = Sales Audit Order */
extern const char SAO[NULL_CODE];

extern const char SAO_1     [NULL_CODE];   /* 1 */
extern const char SAO_2     [NULL_CODE];   /* 2 */
extern const char SAO_3     [NULL_CODE];   /* 3 */
extern const char SAO_4     [NULL_CODE];   /* 4 */
extern const char SAO_5     [NULL_CODE];   /* 5 */
extern const char SAO_6     [NULL_CODE];   /* 6 */
extern const char SAO_7     [NULL_CODE];   /* 7 */
extern const char SAO_8     [NULL_CODE];   /* 8 */
extern const char SAO_9     [NULL_CODE];   /* 9 */
extern const char SAO_10    [NULL_CODE];   /* 10 */
extern const char SAO_11    [NULL_CODE];   /* 11 */
extern const char SAO_12    [NULL_CODE];   /* 12 */
extern const char SAO_13    [NULL_CODE];   /* 13 */
extern const char SAO_14    [NULL_CODE];   /* 14 */
extern const char SAO_15    [NULL_CODE];   /* 15 */
extern const char SAO_16    [NULL_CODE];   /* 16 */
extern const char SAO_17    [NULL_CODE];   /* 17 */
extern const char SAO_18    [NULL_CODE];   /* 18 */
extern const char SAO_19    [NULL_CODE];   /* 19 */
extern const char SAO_20    [NULL_CODE];   /* 20 */

#define SAOTT_1        1   /* 1 */
#define SAOTT_2        2   /* 2 */
#define SAOTT_3        3   /* 3 */
#define SAOTT_4        4   /* 4 */
#define SAOTT_5        5   /* 5 */
#define SAOTT_6        6   /* 6 */
#define SAOTT_7        7   /* 7 */
#define SAOTT_8        8   /* 8 */
#define SAOTT_9        9   /* 9 */
#define SAOTT_10      10   /* 10 */
#define SAOTT_11      11   /* 11 */
#define SAOTT_12      12   /* 12 */
#define SAOTT_13      13   /* 13 */
#define SAOTT_14      14   /* 14 */
#define SAOTT_15      15   /* 15 */
#define SAOTT_16      16   /* 16 */
#define SAOTT_17      17   /* 17 */
#define SAOTT_18      18   /* 18 */
#define SAOTT_19      19   /* 19 */
#define SAOTT_20      20   /* 20 */

#define _SAOTT_MAX 20


/* SAPA = Sales Audit Presence or Absence Indicator */
extern const char SAPA[NULL_CODE];

extern const char SAPA_P     [NULL_CODE];   /* Presence */
extern const char SAPA_A     [NULL_CODE];   /* Absence */

#define SAPATT_P        1   /* Presence */
#define SAPATT_A        2   /* Absence */

#define _SAPATT_MAX 2


/* SARD = Sales Audit Retailer Defined Codes */
extern const char SARD[NULL_CODE];

extern const char SARD_RD1   [NULL_CODE];   /* Retailer Defined 1 */
extern const char SARD_RD2   [NULL_CODE];   /* Retailer Defined 2 */
extern const char SARD_RD3   [NULL_CODE];   /* Retailer Defined 3 */
extern const char SARD_ERR   [NULL_CODE];   /* Error */

#define SARDTT_RD1      1   /* Retailer Defined 1 */
#define SARDTT_RD2      2   /* Retailer Defined 2 */
#define SARDTT_RD3      3   /* Retailer Defined 3 */
#define SARDTT_ERR      4   /* Error */

#define _SARDTT_MAX 4


/* SARL = Sales Audit Rule Definition Labels */
extern const char SARL[NULL_CODE];

extern const char SARL_RO    [NULL_CODE];   /* Rule Overview */
extern const char SARL_RC    [NULL_CODE];   /* Rule Characteristics */
extern const char SARL_R     [NULL_CODE];   /* Realms */
extern const char SARL_J     [NULL_CODE];   /* Joins */
extern const char SARL_P     [NULL_CODE];   /* Parameters */
extern const char SARL_RE    [NULL_CODE];   /* Restrictions */
extern const char SARL_LT    [NULL_CODE];   /* Location Traits */

#define SARLTT_RO       1   /* Rule Overview */
#define SARLTT_RC       2   /* Rule Characteristics */
#define SARLTT_R        3   /* Realms */
#define SARLTT_J        4   /* Joins */
#define SARLTT_P        5   /* Parameters */
#define SARLTT_RE       6   /* Restrictions */
#define SARLTT_LT       7   /* Location Traits */

#define _SARLTT_MAX 7


/* SARR = Return Reason Types */
extern const char SARR[NULL_CODE];

extern const char SARR_RET1  [NULL_CODE];   /* Did not like */
extern const char SARR_RET2  [NULL_CODE];   /* Better Price somewhere else. */
extern const char SARR_RET3  [NULL_CODE];   /* Did not fit */
extern const char SARR_RET4  [NULL_CODE];   /* Damaged */
extern const char SARR_RET5  [NULL_CODE];   /* Exchange */
extern const char SARR_RET6  [NULL_CODE];   /* Poor Quality */
extern const char SARR_RET41 [NULL_CODE];   /* Open Box */
extern const char SARR_RET42 [NULL_CODE];   /* Unusable */
extern const char SARR_RET43 [NULL_CODE];   /* Repairable */
extern const char SARR_ERR   [NULL_CODE];   /* Error */

#define SARRTT_RET1    30   /* Did not like */
#define SARRTT_RET2    31   /* Better Price somewhere else. */
#define SARRTT_RET3    32   /* Did not fit */
#define SARRTT_RET4    33   /* Damaged */
#define SARRTT_RET5    34   /* Exchange */
#define SARRTT_RET6    35   /* Poor Quality */
#define SARRTT_RET41   36   /* Open Box */
#define SARRTT_RET42   37   /* Unusable */
#define SARRTT_RET43   38   /* Repairable */
#define SARRTT_ERR     45   /* Error */

#define _SARRTT_MAX 45


/* SARS = Sales Audit Rounding Rules Status */
extern const char SARS[NULL_CODE];

extern const char SARS_A     [NULL_CODE];   /* Active */
extern const char SARS_I     [NULL_CODE];   /* Inactive */

#define SARSTT_A        1   /* Active */
#define SARSTT_I        2   /* Inactive */

#define _SARSTT_MAX 2


/* SART = Sales Audit Record Types */
extern const char SART[NULL_CODE];

extern const char SART_THEAD [NULL_CODE];   /* Header Level Error */
extern const char SART_TITEM [NULL_CODE];   /* Item Level Error */
extern const char SART_TTEND [NULL_CODE];   /* Tender Level Error */
extern const char SART_TTAX  [NULL_CODE];   /* Tax Level Error */
extern const char SART_IDISC [NULL_CODE];   /* Item Discount Level Error */
extern const char SART_TTAIL [NULL_CODE];   /* Tail Error */
extern const char SART_TCUST [NULL_CODE];   /* Customer Level Error */
extern const char SART_ERROR [NULL_CODE];   /* Error */
extern const char SART_TINFO [NULL_CODE];   /* Informational Error */
extern const char SART_FHEAD [NULL_CODE];   /* Header Level Error */
extern const char SART_FTAIL [NULL_CODE];   /* Tail Error */
extern const char SART_TOTAL [NULL_CODE];   /* Total Error */
extern const char SART_STORE [NULL_CODE];   /* Store Day Error */
extern const char SART_BALG  [NULL_CODE];   /* Balance Group Error */
extern const char SART_TCTYPE[NULL_CODE];   /* Invalid Customer ID Type */
extern const char SART_TCUSTA[NULL_CODE];   /* Customer Attribute Level Error */
extern const char SART_THBALC[NULL_CODE];   /* Transaction out of Balance */
extern const char SART_THPOSI[NULL_CODE];   /* Invalid POS Transaction Indicator */
extern const char SART_THREAC[NULL_CODE];   /* Invalid Reason Code */
extern const char SART_THSUBT[NULL_CODE];   /* Invalid Sub-Transaction Type */
extern const char SART_THTRAT[NULL_CODE];   /* Invalid Transaction Type */
extern const char SART_THDATE[NULL_CODE];   /* Invalid Transaction Date */
extern const char SART_CATT  [NULL_CODE];   /* Customer Attribute Type */
extern const char SART_THTRAN[NULL_CODE];   /* Invalid Transaction Number */
extern const char SART_THOREG[NULL_CODE];   /* Invalid Original Register Number */
extern const char SART_THOTRT[NULL_CODE];   /* Invalid Original Transaction Type */
extern const char SART_THOTRN[NULL_CODE];   /* Invalid Original Transaction Number */
extern const char SART_THVALU[NULL_CODE];   /* Invalid Value */
extern const char SART_RULERR[NULL_CODE];   /* Rule function did not execute */
extern const char SART_TOTERR[NULL_CODE];   /* Total function did not execute */
extern const char SART_THTEND[NULL_CODE];   /* Invalid Tender Ending */
extern const char SART_THTTSA[NULL_CODE];   /* No rounding rule for Sale Total */
extern const char SART_IGTAX [NULL_CODE];   /* Item Level Tax Error */
extern const char SART_TPYMT [NULL_CODE];   /* Payment Level Error */

#define SARTTT_THEAD    1   /* Header Level Error */
#define SARTTT_TITEM    2   /* Item Level Error */
#define SARTTT_TTEND    3   /* Tender Level Error */
#define SARTTT_TTAX     4   /* Tax Level Error */
#define SARTTT_IDISC    5   /* Item Discount Level Error */
#define SARTTT_TTAIL    6   /* Tail Error */
#define SARTTT_TCUST    7   /* Customer Level Error */
#define SARTTT_ERROR    8   /* Error */
#define SARTTT_TINFO    9   /* Informational Error */
#define SARTTT_FHEAD   10   /* Header Level Error */
#define SARTTT_FTAIL   11   /* Tail Error */
#define SARTTT_TOTAL   12   /* Total Error */
#define SARTTT_STORE   13   /* Store Day Error */
#define SARTTT_BALG    15   /* Balance Group Error */
#define SARTTT_TCTYPE  18   /* Invalid Customer ID Type */
#define SARTTT_TCUSTA  19   /* Customer Attribute Level Error */
#define SARTTT_THBALC  20   /* Transaction out of Balance */
#define SARTTT_THPOSI  21   /* Invalid POS Transaction Indicator */
#define SARTTT_THREAC  22   /* Invalid Reason Code */
#define SARTTT_THSUBT  23   /* Invalid Sub-Transaction Type */
#define SARTTT_THTRAT  24   /* Invalid Transaction Type */
#define SARTTT_THDATE  25   /* Invalid Transaction Date */
#define SARTTT_CATT    26   /* Customer Attribute Type */
#define SARTTT_THTRAN  27   /* Invalid Transaction Number */
#define SARTTT_THOREG  28   /* Invalid Original Register Number */
#define SARTTT_THOTRT  29   /* Invalid Original Transaction Type */
#define SARTTT_THOTRN  30   /* Invalid Original Transaction Number */
#define SARTTT_THVALU  31   /* Invalid Value */
#define SARTTT_RULERR  32   /* Rule function did not execute */
#define SARTTT_TOTERR  33   /* Total function did not execute */
#define SARTTT_THTEND  34   /* Invalid Tender Ending */
#define SARTTT_THTTSA  35   /* No rounding rule for Sale Total */
#define SARTTT_IGTAX   36   /* Item Level Tax Error */
#define SARTTT_TPYMT   37   /* Payment Level Error */

#define _SARTTT_MAX 37


/* SASC = Sales Audit Sum/Count Wizard Total */
extern const char SASC[NULL_CODE];

extern const char SASC_SUM   [NULL_CODE];   /* Parameter to be Totaled */
extern const char SASC_COUNT [NULL_CODE];   /* Parameter to be Counted */

#define SASCTT_SUM      1   /* Parameter to be Totaled */
#define SASCTT_COUNT    2   /* Parameter to be Counted */

#define _SASCTT_MAX 2


/* SASD = Sales Audit Discount Types */
extern const char SASD[NULL_CODE];

extern const char SASD_RMS   [NULL_CODE];   /* RMS Discount */
extern const char SASD_COUPON[NULL_CODE];   /* Coupon Type 1 */

#define SASDTT_RMS      1   /* RMS Discount */
#define SASDTT_COUPON   2   /* Coupon Type 1 */

#define _SASDTT_MAX 2


/* SASI = Sales Audit Item Status */
extern const char SASI[NULL_CODE];

extern const char SASI_ERR   [NULL_CODE];   /* Error */
extern const char SASI_V     [NULL_CODE];   /* Voided */
extern const char SASI_S     [NULL_CODE];   /* Sale */
extern const char SASI_R     [NULL_CODE];   /* Return */
extern const char SASI_O     [NULL_CODE];   /* Other */
extern const char SASI_ORI   [NULL_CODE];   /* Ord Ini */
extern const char SASI_ORC   [NULL_CODE];   /* Ord Can */
extern const char SASI_ORD   [NULL_CODE];   /* Ord Com */
extern const char SASI_LIN   [NULL_CODE];   /* Lay Ini */
extern const char SASI_LCA   [NULL_CODE];   /* Lay Can */
extern const char SASI_LCO   [NULL_CODE];   /* Lay Com */

#define SASITT_ERR      1   /* Error */
#define SASITT_V        2   /* Voided */
#define SASITT_S        3   /* Sale */
#define SASITT_R        4   /* Return */
#define SASITT_O        5   /* Other */
#define SASITT_ORI      6   /* Ord Ini */
#define SASITT_ORC      7   /* Ord Can */
#define SASITT_ORD      8   /* Ord Com */
#define SASITT_LIN      9   /* Lay Ini */
#define SASITT_LCA     10   /* Lay Can */
#define SASITT_LCO     11   /* Lay Com */

#define _SASITT_MAX 11


/* SASR = Sales Audit Supported Realms */
extern const char SASR[NULL_CODE];

extern const char SASR_T     [NULL_CODE];   /* Table */
extern const char SASR_V     [NULL_CODE];   /* View */
extern const char SASR_P     [NULL_CODE];   /* PL/SQL */

#define SASRTT_T        1   /* Table */
#define SASRTT_V        2   /* View */
#define SASRTT_P        3   /* PL/SQL */

#define _SASRTT_MAX 3


/* SASS = Sales Audit Store Status */
extern const char SASS[NULL_CODE];

extern const char SASS_W     [NULL_CODE];   /* Worksheet */
extern const char SASS_F     [NULL_CODE];   /* Fuel Closed */
extern const char SASS_C     [NULL_CODE];   /* Closed */

#define SASSTT_W        1   /* Worksheet */
#define SASSTT_F        2   /* Fuel Closed */
#define SASSTT_C        3   /* Closed */

#define _SASSTT_MAX 3


/* SAST = Sales Audit Status Codes */
extern const char SAST[NULL_CODE];

extern const char SAST_D     [NULL_CODE];   /* Delete */
extern const char SAST_P     [NULL_CODE];   /* Present */
extern const char SAST_V     [NULL_CODE];   /* Post-Voided */

#define SASTTT_D        1   /* Delete */
#define SASTTT_P        2   /* Present */
#define SASTTT_V        3   /* Post-Voided */

#define _SASTTT_MAX 3


/* SASV = Sales Audit Status Values */
extern const char SASV[NULL_CODE];

extern const char SASV_W     [NULL_CODE];   /* Worksheet */
extern const char SASV_S     [NULL_CODE];   /* Submitted */
extern const char SASV_A     [NULL_CODE];   /* Approved */
extern const char SASV_DI    [NULL_CODE];   /* Disabled */
extern const char SASV_DE    [NULL_CODE];   /* Deleted */
extern const char SASV_B     [NULL_CODE];   /* All */

#define SASVTT_W        1   /* Worksheet */
#define SASVTT_S        2   /* Submitted */
#define SASVTT_A        3   /* Approved */
#define SASVTT_DI       4   /* Disabled */
#define SASVTT_DE       5   /* Deleted */
#define SASVTT_B        6   /* All */

#define _SASVTT_MAX 6


/* SASY = Sales Audit Sales Type */
extern const char SASY[NULL_CODE];

extern const char SASY_R     [NULL_CODE];   /* Regular Sales */
extern const char SASY_I     [NULL_CODE];   /* In-Store Customer Order */
extern const char SASY_E     [NULL_CODE];   /* External Customer Order */

#define SASYTT_R        1   /* Regular Sales */
#define SASYTT_I        2   /* In-Store Customer Order */
#define SASYTT_E        3   /* External Customer Order */

#define _SASYTT_MAX 3


/* SATC = Sales Audit Totals Category */
extern const char SATC[NULL_CODE];

extern const char SATC_S     [NULL_CODE];   /* Sales */
extern const char SATC_TAX   [NULL_CODE];   /* Tax */
extern const char SATC_OS    [NULL_CODE];   /* Over/Short */
extern const char SATC_TT    [NULL_CODE];   /* Transaction Type */
extern const char SATC_TD    [NULL_CODE];   /* Tender Type */
extern const char SATC_PT    [NULL_CODE];   /* Payment Total */

#define SATCTT_S        1   /* Sales */
#define SATCTT_TAX      2   /* Tax */
#define SATCTT_OS       3   /* Over/Short */
#define SATCTT_TT       4   /* Transaction Type */
#define SATCTT_TD       5   /* Tender Type */
#define SATCTT_PT       6   /* Payment Total */

#define _SATCTT_MAX 6


/* SATL = Sales Audit Total Definition Labels */
extern const char SATL[NULL_CODE];

extern const char SATL_TO    [NULL_CODE];   /* Total Overview */
extern const char SATL_TC    [NULL_CODE];   /* Total Characteristics */
extern const char SATL_TCC   [NULL_CODE];   /* Total Characteristics Cont. */
extern const char SATL_R     [NULL_CODE];   /* Realms */
extern const char SATL_J     [NULL_CODE];   /* Joins */
extern const char SATL_P     [NULL_CODE];   /* Parameters */
extern const char SATL_RU    [NULL_CODE];   /* Roll-Ups */
extern const char SATL_RE    [NULL_CODE];   /* Restrictions */
extern const char SATL_CTD   [NULL_CODE];   /* Combined Total Details */
extern const char SATL_LT    [NULL_CODE];   /* Location Traits */
extern const char SATL_U     [NULL_CODE];   /* Usages */

#define SATLTT_TO       1   /* Total Overview */
#define SATLTT_TC       2   /* Total Characteristics */
#define SATLTT_TCC      3   /* Total Characteristics Cont. */
#define SATLTT_R        4   /* Realms */
#define SATLTT_J        5   /* Joins */
#define SATLTT_P        6   /* Parameters */
#define SATLTT_RU       7   /* Roll-Ups */
#define SATLTT_RE       8   /* Restrictions */
#define SATLTT_CTD      9   /* Combined Total Details */
#define SATLTT_LT      10   /* Location Traits */
#define SATLTT_U       11   /* Usages */

#define _SATLTT_MAX 11


/* SATO = Sales Audit Total Transaction Indicator */
extern const char SATO[NULL_CODE];

extern const char SATO_TO    [NULL_CODE];   /* Total */
extern const char SATO_TR    [NULL_CODE];   /* Transaction */
extern const char SATO_BG    [NULL_CODE];   /* Balance Group Level Error */

#define SATOTT_TO       1   /* Total */
#define SATOTT_TR       2   /* Transaction */
#define SATOTT_BG       3   /* Balance Group Level Error */

#define _SATOTT_MAX 3


/* SATP = Pack Sales Type */
extern const char SATP[NULL_CODE];

extern const char SATP_R     [NULL_CODE];   /* Regular */
extern const char SATP_P     [NULL_CODE];   /* Promotional */

#define SATPTT_R        1   /* Regular */
#define SATPTT_P        2   /* Promotional */

#define _SATPTT_MAX 2


/* SATT = Sales Audit Tolerance Types */
extern const char SATT[NULL_CODE];

extern const char SATT_A     [NULL_CODE];   /* Amount */
extern const char SATT_P     [NULL_CODE];   /* Percent */

#define SATTTT_A        1   /* Amount */
#define SATTTT_P        2   /* Percent */

#define _SATTTT_MAX 2


/* SATY = Sales Audit Total Types */
extern const char SATY[NULL_CODE];

extern const char SATY_O     [NULL_CODE];   /* Over/Short */
extern const char SATY_M     [NULL_CODE];   /* Miscellaneous */

#define SATYTT_O        1   /* Over/Short */
#define SATYTT_M        2   /* Miscellaneous */

#define _SATYTT_MAX 2


/* SAUT = Sales Audit Usage Types */
extern const char SAUT[NULL_CODE];

extern const char SAUT_FLR   [NULL_CODE];   /* Flash Totals Reporting */
extern const char SAUT_SFMS  [NULL_CODE];   /* Site Fuels Management Sales */
extern const char SAUT_SFMD  [NULL_CODE];   /* Site Fuels Management Drive Offs */
extern const char SAUT_SFMP  [NULL_CODE];   /* Site Fuels Management Pump Tests */
extern const char SAUT_UARBDP[NULL_CODE];   /* UAR Bank Deposits */
extern const char SAUT_UARMON[NULL_CODE];   /* UAR Money Orders */
extern const char SAUT_UARLOT[NULL_CODE];   /* UAR Lottery */
extern const char SAUT_UARCRD[NULL_CODE];   /* UAR Credit Card */
extern const char SAUT_ACH   [NULL_CODE];   /* Automated Clearing House */
extern const char SAUT_RA    [NULL_CODE];   /* RA Export */
extern const char SAUT_GL    [NULL_CODE];   /* General Ledger */
extern const char SAUT_IM    [NULL_CODE];   /* IM Export */

#define SAUTTT_FLR      1   /* Flash Totals Reporting */
#define SAUTTT_SFMS     2   /* Site Fuels Management Sales */
#define SAUTTT_SFMD     3   /* Site Fuels Management Drive Offs */
#define SAUTTT_SFMP     4   /* Site Fuels Management Pump Tests */
#define SAUTTT_UARBDP   5   /* UAR Bank Deposits */
#define SAUTTT_UARMON   6   /* UAR Money Orders */
#define SAUTTT_UARLOT   7   /* UAR Lottery */
#define SAUTTT_UARCRD   8   /* UAR Credit Card */
#define SAUTTT_ACH      9   /* Automated Clearing House */
#define SAUTTT_RA      10   /* RA Export */
#define SAUTTT_GL      11   /* General Ledger */
#define SAUTTT_IM      12   /* IM Export */

#define _SAUTTT_MAX 12


/* SAVS = Sales Audit Voucher Status */
extern const char SAVS[NULL_CODE];

extern const char SAVS_A     [NULL_CODE];   /* Assigned from HQ to a Store */
extern const char SAVS_I     [NULL_CODE];   /* Issued from a Store to a Customer */
extern const char SAVS_R     [NULL_CODE];   /* Redeemed by a Customer */
extern const char SAVS_E     [NULL_CODE];   /* Escheated to the State/Country */
extern const char SAVS_N     [NULL_CODE];   /* Income Adjustment to Retailer */

#define SAVSTT_A        1   /* Assigned from HQ to a Store */
#define SAVSTT_I        2   /* Issued from a Store to a Customer */
#define SAVSTT_R        3   /* Redeemed by a Customer */
#define SAVSTT_E        4   /* Escheated to the State/Country */
#define SAVSTT_N        5   /* Income Adjustment to Retailer */

#define _SAVSTT_MAX 5


/* SBCA = Supplier Bracket Category Abbreviations */
extern const char SBCA[NULL_CODE];

extern const char SBCA_AMNT  [NULL_CODE];   /* Amount */
extern const char SBCA_CUBE  [NULL_CODE];   /* Cube */
extern const char SBCA_WGHT  [NULL_CODE];   /* Weight */

#define SBCATT_AMNT     1   /* Amount */
#define SBCATT_CUBE     2   /* Cube */
#define SBCATT_WGHT     3   /* Weight */

#define _SBCATT_MAX 3


/* SCDS = Description for stkschedexpl.pc */
extern const char SCDS[NULL_CODE];

extern const char SCDS_1     [NULL_CODE];   /* Scheduled Count for %s */

#define SCDSTT_1        1   /* Scheduled Count for %s */

#define _SCDSTT_MAX 1


/* SCL = Scaling Constraint Type */
extern const char SCL[NULL_CODE];

extern const char SCL_O     [NULL_CODE];   /* Order Level */
extern const char SCL_L     [NULL_CODE];   /* Location Level */

#define SCLTT_O        1   /* Order Level */
#define SCLTT_L        2   /* Location Level */

#define _SCLTT_MAX 2


/* SCLN = Scale Order */
extern const char SCLN[NULL_CODE];

extern const char SCLN_S     [NULL_CODE];   /* Scale Order */
extern const char SCLN_U     [NULL_CODE];   /* Undo Scaling */

#define SCLNTT_S        1   /* Scale Order */
#define SCLNTT_U        2   /* Undo Scaling */

#define _SCLNTT_MAX 2


/* SCNT = Stock Count */
extern const char SCNT[NULL_CODE];

extern const char SCNT_Y     [NULL_CODE];   /* For Units Only */
extern const char SCNT_N     [NULL_CODE];   /* Never */
extern const char SCNT_S     [NULL_CODE];   /* For Unit and Value */

#define SCNTTT_Y        1   /* For Units Only */
#define SCNTTT_N        2   /* Never */
#define SCNTTT_S        3   /* For Unit and Value */

#define _SCNTTT_MAX 3


/* SCO = Scaling Constraint Objective */
extern const char SCO[NULL_CODE];

extern const char SCO_M     [NULL_CODE];   /* Minimum */
extern const char SCO_X     [NULL_CODE];   /* Maximum */

#define SCOTT_M        1   /* Minimum */
#define SCOTT_X        2   /* Maximum */

#define _SCOTT_MAX 2


/* SCRT = Stock Count Run Type */
extern const char SCRT[NULL_CODE];

extern const char SCRT_A     [NULL_CODE];   /* Actual */
extern const char SCRT_T     [NULL_CODE];   /* Trial Run */

#define SCRTTT_A        1   /* Actual */
#define SCRTTT_T        2   /* Trial Run */

#define _SCRTTT_MAX 2


/* SCSK = Cycle Count Item List */
extern const char SCSK[NULL_CODE];

extern const char SCSK_P     [NULL_CODE];   /* Product */
extern const char SCSK_I     [NULL_CODE];   /* Item List */

#define SCSKTT_P        1   /* Product */
#define SCSKTT_I        2   /* Item List */

#define _SCSKTT_MAX 2


/* SCST = Stock Categories - Store */
extern const char SCST[NULL_CODE];

extern const char SCST_D     [NULL_CODE];   /* Direct To Store */
extern const char SCST_C     [NULL_CODE];   /* Cross-Docked */
extern const char SCST_W     [NULL_CODE];   /* Warehouse Stocked */
extern const char SCST_L     [NULL_CODE];   /* WH/Cross Link */

#define SCSTTT_D        1   /* Direct To Store */
#define SCSTTT_C        2   /* Cross-Docked */
#define SCSTTT_W        3   /* Warehouse Stocked */
#define SCSTTT_L        4   /* WH/Cross Link */

#define _SCSTTT_MAX 4


/* SCT = Scaling Constraint Type */
extern const char SCT[NULL_CODE];

extern const char SCT_A     [NULL_CODE];   /* Amount */
extern const char SCT_M     [NULL_CODE];   /* Mass */
extern const char SCT_V     [NULL_CODE];   /* Volume */
extern const char SCT_P     [NULL_CODE];   /* Pallet */
extern const char SCT_C     [NULL_CODE];   /* Case */
extern const char SCT_E     [NULL_CODE];   /* Each */
extern const char SCT_S     [NULL_CODE];   /* Stat Case */

#define SCTTT_A        1   /* Amount */
#define SCTTT_M        2   /* Mass */
#define SCTTT_V        3   /* Volume */
#define SCTTT_P        4   /* Pallet */
#define SCTTT_C        5   /* Case */
#define SCTTT_E        6   /* Each */
#define SCTTT_S        7   /* Stat Case */

#define _SCTTT_MAX 7


/* SCTL = Invoice Matching Cost Tolerance Level */
extern const char SCTL[NULL_CODE];

extern const char SCTL_TC    [NULL_CODE];   /* Summary Cost */
extern const char SCTL_LC    [NULL_CODE];   /* Line Item Cost */
extern const char SCTL_LQ    [NULL_CODE];   /* Line Item Qty. */

#define SCTLTT_TC       1   /* Summary Cost */
#define SCTLTT_LC       2   /* Line Item Cost */
#define SCTLTT_LQ       3   /* Line Item Qty. */

#define _SCTLTT_MAX 3


/* SCTY = Stock Count Type */
extern const char SCTY[NULL_CODE];

extern const char SCTY_U     [NULL_CODE];   /* Unit */
extern const char SCTY_B     [NULL_CODE];   /* Unit & Value */

#define SCTYTT_U        1   /* Unit */
#define SCTYTT_B        2   /* Unit & Value */

#define _SCTYTT_MAX 2


/* SCWH = Stock Categories - Warehouse */
extern const char SCWH[NULL_CODE];

extern const char SCWH_W     [NULL_CODE];   /* Warehouse Stocked */

#define SCWHTT_W        1   /* Warehouse Stocked */

#define _SCWHTT_MAX 1


/* SDST = Sales Audit Store Status */
extern const char SDST[NULL_CODE];

extern const char SDST_I     [NULL_CODE];   /* Sales Audit in Progress */
extern const char SDST_A     [NULL_CODE];   /* Sales Audit Complete */

#define SDSTTT_I        1   /* Sales Audit in Progress */
#define SDSTTT_A        2   /* Sales Audit Complete */

#define _SDSTTT_MAX 2


/* SEPC = Season/Phase maintenance codes */
extern const char SEPC[NULL_CODE];

extern const char SEPC_A     [NULL_CODE];   /* Add */
extern const char SEPC_C     [NULL_CODE];   /* Change */
extern const char SEPC_D     [NULL_CODE];   /* Delete */

#define SEPCTT_A        1   /* Add */
#define SEPCTT_C        2   /* Change */
#define SEPCTT_D        3   /* Delete */

#define _SEPCTT_MAX 3


/* SEPS = Season Phase Style/color Maintenance */
extern const char SEPS[NULL_CODE];

extern const char SEPS_A     [NULL_CODE];   /* Add */
extern const char SEPS_O     [NULL_CODE];   /* Overwrite */

#define SEPSTT_A        1   /* Add */
#define SEPSTT_O        2   /* Overwrite */

#define _SEPSTT_MAX 2


/* SESF = Sales Error Status Filter */
extern const char SESF[NULL_CODE];

extern const char SESF_A     [NULL_CODE];   /* Approved Rules  */
extern const char SESF_W     [NULL_CODE];   /* Non-Approved Rules  */
extern const char SESF_B     [NULL_CODE];   /* All Rules */

#define SESFTT_A        1   /* Approved Rules  */
#define SESFTT_W        2   /* Non-Approved Rules  */
#define SESFTT_B        3   /* All Rules */

#define _SESFTT_MAX 3


/* SHDL = Shipment Discrepancy Labels */
extern const char SHDL[NULL_CODE];

extern const char SHDL_IS    [NULL_CODE];   /* Item Status */
extern const char SHDL_MQ    [NULL_CODE];   /* Match Qty. */
extern const char SHDL_QE    [NULL_CODE];   /* Qty. Exp'd */
extern const char SHDL_QA    [NULL_CODE];   /* ASN Qty. */

#define SHDLTT_IS       1   /* Item Status */
#define SHDLTT_MQ       2   /* Match Qty. */
#define SHDLTT_QE       3   /* Qty. Exp'd */
#define SHDLTT_QA       4   /* ASN Qty. */

#define _SHDLTT_MAX 4


/* SHMD = Schedule Modes */
extern const char SHMD[NULL_CODE];

extern const char SHMD_S     [NULL_CODE];   /* Signal Driven */
extern const char SHMD_T     [NULL_CODE];   /* Time Driven */

#define SHMDTT_S        1   /* Signal Driven */
#define SHMDTT_T        2   /* Time Driven */

#define _SHMDTT_MAX 2


/* SHMT = Shipment Payment Terms */
extern const char SHMT[NULL_CODE];

extern const char SHMT_CC    [NULL_CODE];   /* Collect */
extern const char SHMT_CF    [NULL_CODE];   /* Collected Freight Credited Back to Cust */
extern const char SHMT_DF    [NULL_CODE];   /* Defined by Buyer and Seller */
extern const char SHMT_MX    [NULL_CODE];   /* Mixed */
extern const char SHMT_PC    [NULL_CODE];   /* Prepaid but Charged to Customer */
extern const char SHMT_PO    [NULL_CODE];   /* Prepaid Only */
extern const char SHMT_PP    [NULL_CODE];   /* Prepaid by Seller */

#define SHMTTT_CC       1   /* Collect */
#define SHMTTT_CF       2   /* Collected Freight Credited Back to Cust */
#define SHMTTT_DF       3   /* Defined by Buyer and Seller */
#define SHMTTT_MX       4   /* Mixed */
#define SHMTTT_PC       5   /* Prepaid but Charged to Customer */
#define SHMTTT_PO       6   /* Prepaid Only */
#define SHMTTT_PP       7   /* Prepaid by Seller */

#define _SHMTTT_MAX 7


/* SHOR = Shipment Origin */
extern const char SHOR[NULL_CODE];

extern const char SHOR_0     [NULL_CODE];   /* ASN Shipments */
extern const char SHOR_1     [NULL_CODE];   /* Manual Shipment */
extern const char SHOR_2     [NULL_CODE];   /* Autoship */
extern const char SHOR_3     [NULL_CODE];   /* System */
extern const char SHOR_4     [NULL_CODE];   /* System ASN */
extern const char SHOR_5     [NULL_CODE];   /* System UCC-128 */
extern const char SHOR_6     [NULL_CODE];   /* ASN UCC-128 */

#define SHORTT_0        1   /* ASN Shipments */
#define SHORTT_1        2   /* Manual Shipment */
#define SHORTT_2        3   /* Autoship */
#define SHORTT_3        4   /* System */
#define SHORTT_4        5   /* System ASN */
#define SHORTT_5        6   /* System UCC-128 */
#define SHORTT_6        7   /* ASN UCC-128 */

#define _SHORTT_MAX 7


/* SHPM = Ship Method */
extern const char SHPM[NULL_CODE];

extern const char SHPM_10    [NULL_CODE];   /* Vessel, Non-container */
extern const char SHPM_11    [NULL_CODE];   /* Vessel, Container */
extern const char SHPM_12    [NULL_CODE];   /* Border Water-borne (Only Mex. and Can.) */
extern const char SHPM_20    [NULL_CODE];   /* Rail, Non-container */
extern const char SHPM_21    [NULL_CODE];   /* Rail, Container */
extern const char SHPM_30    [NULL_CODE];   /* Truck, Non-container */
extern const char SHPM_31    [NULL_CODE];   /* Truck, Container */
extern const char SHPM_32    [NULL_CODE];   /* Auto */
extern const char SHPM_33    [NULL_CODE];   /* Pedestrian */
extern const char SHPM_34    [NULL_CODE];   /* Road, other, incl. foot and animal borne */
extern const char SHPM_40    [NULL_CODE];   /* Air, Non-container */
extern const char SHPM_41    [NULL_CODE];   /* Air, container */
extern const char SHPM_50    [NULL_CODE];   /* Mail */
extern const char SHPM_60    [NULL_CODE];   /* Passenger, Hand carried */
extern const char SHPM_70    [NULL_CODE];   /* Fixed Transportation Installation */
extern const char SHPM_80    [NULL_CODE];   /* Not used at this time */

#define SHPMTT_10       1   /* Vessel, Non-container */
#define SHPMTT_11       2   /* Vessel, Container */
#define SHPMTT_12       3   /* Border Water-borne (Only Mex. and Can.) */
#define SHPMTT_20       4   /* Rail, Non-container */
#define SHPMTT_21       5   /* Rail, Container */
#define SHPMTT_30       6   /* Truck, Non-container */
#define SHPMTT_31       7   /* Truck, Container */
#define SHPMTT_32       8   /* Auto */
#define SHPMTT_33       9   /* Pedestrian */
#define SHPMTT_34      10   /* Road, other, incl. foot and animal borne */
#define SHPMTT_40      11   /* Air, Non-container */
#define SHPMTT_41      12   /* Air, container */
#define SHPMTT_50      13   /* Mail */
#define SHPMTT_60      14   /* Passenger, Hand carried */
#define SHPMTT_70      15   /* Fixed Transportation Installation */
#define SHPMTT_80      16   /* Not used at this time */

#define _SHPMTT_MAX 16


/* SHPO = ASN Shipment */
extern const char SHPO[NULL_CODE];

extern const char SHPO_0     [NULL_CODE];   /* ASN Shipment */
extern const char SHPO_1     [NULL_CODE];   /* Manual Shipment */
extern const char SHPO_2     [NULL_CODE];   /* Autoship */
extern const char SHPO_3     [NULL_CODE];   /* System */
extern const char SHPO_4     [NULL_CODE];   /* System ASN */
extern const char SHPO_5     [NULL_CODE];   /* System UCC-128 */
extern const char SHPO_6     [NULL_CODE];   /* ASN UCC-128 */

#define SHPOTT_0        1   /* ASN Shipment */
#define SHPOTT_1        2   /* Manual Shipment */
#define SHPOTT_2        3   /* Autoship */
#define SHPOTT_3        4   /* System */
#define SHPOTT_4        5   /* System ASN */
#define SHPOTT_5        6   /* System UCC-128 */
#define SHPOTT_6        7   /* ASN UCC-128 */

#define _SHPOTT_MAX 7


/* SHPS = Shipment Status */
extern const char SHPS[NULL_CODE];

extern const char SHPS_U     [NULL_CODE];   /* Unmatched */
extern const char SHPS_R     [NULL_CODE];   /* Received */
extern const char SHPS_I     [NULL_CODE];   /* Input */
extern const char SHPS_E     [NULL_CODE];   /* Extracted */
extern const char SHPS_P     [NULL_CODE];   /* Dispatched */
extern const char SHPS_T     [NULL_CODE];   /* Tickets Printed */
extern const char SHPS_V     [NULL_CODE];   /* Invoice Entered */

#define SHPSTT_U        1   /* Unmatched */
#define SHPSTT_R        2   /* Received */
#define SHPSTT_I        3   /* Input */
#define SHPSTT_E        4   /* Extracted */
#define SHPSTT_P        5   /* Dispatched */
#define SHPSTT_T        6   /* Tickets Printed */
#define SHPSTT_V        7   /* Invoice Entered */

#define _SHPSTT_MAX 7


/* SHST = Shipment Status Code */
extern const char SHST[NULL_CODE];

extern const char SHST_I     [NULL_CODE];   /* Input */
extern const char SHST_T     [NULL_CODE];   /* Tickets Printed */
extern const char SHST_V     [NULL_CODE];   /* Invoice Entered */
extern const char SHST_E     [NULL_CODE];   /* Extracted */
extern const char SHST_P     [NULL_CODE];   /* Dispatched */
extern const char SHST_R     [NULL_CODE];   /* Received */
extern const char SHST_C     [NULL_CODE];   /* Cancelled */
extern const char SHST_D     [NULL_CODE];   /* Delete at POS */
extern const char SHST_U     [NULL_CODE];   /* Unmatched */

#define SHSTTT_I        1   /* Input */
#define SHSTTT_T        2   /* Tickets Printed */
#define SHSTTT_V        3   /* Invoice Entered */
#define SHSTTT_E        4   /* Extracted */
#define SHSTTT_P        5   /* Dispatched */
#define SHSTTT_R        6   /* Received */
#define SHSTTT_C        7   /* Cancelled */
#define SHSTTT_D        8   /* Delete at POS */
#define SHSTTT_U        9   /* Unmatched */

#define _SHSTTT_MAX 9


/* SHTO = Shipment Types */
extern const char SHTO[NULL_CODE];

extern const char SHTO_O     [NULL_CODE];   /* Order */
extern const char SHTO_T     [NULL_CODE];   /* Transfer */
extern const char SHTO_C     [NULL_CODE];   /* Customer Order */

#define SHTOTT_O        1   /* Order */
#define SHTOTT_T        2   /* Transfer */
#define SHTOTT_C        3   /* Customer Order */

#define _SHTOTT_MAX 3


/* SIGN = Positive/Negative number indicator */
extern const char SIGN[NULL_CODE];

extern const char SIGN_P     [NULL_CODE];   /* Positive */
extern const char SIGN_N     [NULL_CODE];   /* Negative */

#define SIGNTT_P        1   /* Positive */
#define SIGNTT_N        2   /* Negative */

#define _SIGNTT_MAX 2


/* SIOP = Simple Relational Operators */
extern const char SIOP[NULL_CODE];

extern const char SIOP_EQ    [NULL_CODE];   /* = */
extern const char SIOP_GT    [NULL_CODE];   /* > */
extern const char SIOP_LT    [NULL_CODE];   /* < */

#define SIOPTT_EQ       1   /* = */
#define SIOPTT_GT       2   /* > */
#define SIOPTT_LT       3   /* < */

#define _SIOPTT_MAX 3


/* SIST = Sales Type For Store/Sales Issues */
extern const char SIST[NULL_CODE];

extern const char SIST_R     [NULL_CODE];   /* Regular */
extern const char SIST_P     [NULL_CODE];   /* Promotional */
extern const char SIST_C     [NULL_CODE];   /* Clearance */
extern const char SIST_I     [NULL_CODE];   /* Issues From Warehouses to Stores */

#define SISTTT_R        1   /* Regular */
#define SISTTT_P        2   /* Promotional */
#define SISTTT_C        3   /* Clearance */
#define SISTTT_I        4   /* Issues From Warehouses to Stores */

#define _SISTTT_MAX 4


/* SIZE = Size Labels */
extern const char SIZE[NULL_CODE];

extern const char SIZE_1     [NULL_CODE];   /* Size 1 */
extern const char SIZE_2     [NULL_CODE];   /* Size 2 */

#define SIZETT_1        1   /* Size 1 */
#define SIZETT_2        2   /* Size 2 */

#define _SIZETT_MAX 2


/* SKLB = Skusupp form labels */
extern const char SKLB[NULL_CODE];

extern const char SKLB_I     [NULL_CODE];   /* Item */
extern const char SKLB_P     [NULL_CODE];   /* Primary Supplier */
extern const char SKLB_S     [NULL_CODE];   /* Supplier */
extern const char SKLB_SL    [NULL_CODE];   /* Supplier Label */
extern const char SKLB_SS    [NULL_CODE];   /* Supplier Style Code */
extern const char SKLB_U     [NULL_CODE];   /* Child Item */
extern const char SKLB_V     [NULL_CODE];   /* VPN */

#define SKLBTT_I        1   /* Item */
#define SKLBTT_P        2   /* Primary Supplier */
#define SKLBTT_S        3   /* Supplier */
#define SKLBTT_SL       4   /* Supplier Label */
#define SKLBTT_SS       5   /* Supplier Style Code */
#define SKLBTT_U        6   /* Child Item */
#define SKLBTT_V        7   /* VPN */

#define _SKLBTT_MAX 7


/* SKLL = Stock Ledger Location Level */
extern const char SKLL[NULL_CODE];

extern const char SKLL_S     [NULL_CODE];   /* Each Location */
extern const char SKLL_T     [NULL_CODE];   /* Total Locations */

#define SKLLTT_S        1   /* Each Location */
#define SKLLTT_T        2   /* Total Locations */

#define _SKLLTT_MAX 2


/* SKPL = Stock Ledger Product Level */
extern const char SKPL[NULL_CODE];

extern const char SKPL_D     [NULL_CODE];   /* @MH4@ */
extern const char SKPL_C     [NULL_CODE];   /* @MH5@ */
extern const char SKPL_S     [NULL_CODE];   /* @MH6@ */
extern const char SKPL_K     [NULL_CODE];   /* Item */

#define SKPLTT_D        1   /* @MH4@ */
#define SKPLTT_C        2   /* @MH5@ */
#define SKPLTT_S        3   /* @MH6@ */
#define SKPLTT_K        4   /* Item */

#define _SKPLTT_MAX 4


/* SKTL = Stock Ledger Time Level */
extern const char SKTL[NULL_CODE];

extern const char SKTL_M     [NULL_CODE];   /* Month */
extern const char SKTL_W     [NULL_CODE];   /* Week */

#define SKTLTT_M        1   /* Month */
#define SKTLTT_W        2   /* Week */

#define _SKTLTT_MAX 2


/* SLTP = Sales Type (plus temp) */
extern const char SLTP[NULL_CODE];

extern const char SLTP_R     [NULL_CODE];   /* Regular */
extern const char SLTP_P     [NULL_CODE];   /* Promotional */
extern const char SLTP_C     [NULL_CODE];   /* Clearance */
extern const char SLTP_T     [NULL_CODE];   /* Temporary */

#define SLTPTT_R        1   /* Regular */
#define SLTPTT_P        2   /* Promotional */
#define SLTPTT_C        3   /* Clearance */
#define SLTPTT_T        4   /* Temporary */

#define _SLTPTT_MAX 4


/* SLVL = Stock Level */
extern const char SLVL[NULL_CODE];

extern const char SLVL_L     [NULL_CODE];   /* 85 */
extern const char SLVL_H     [NULL_CODE];   /* 98 */

#define SLVLTT_L        1   /* 85 */
#define SLVLTT_H        2   /* 98 */

#define _SLVLTT_MAX 2


/* SOAT = Stock Order Reconcile Adjust Type */
extern const char SOAT[NULL_CODE];

extern const char SOAT_SL    [NULL_CODE];   /* Shipping Location */
extern const char SOAT_RL    [NULL_CODE];   /* Receiving Location */
extern const char SOAT_FC    [NULL_CODE];   /* Freight Claim */
extern const char SOAT_FR    [NULL_CODE];   /* Forced Receipt */
extern const char SOAT_RE    [NULL_CODE];   /* Received Elsewhere */

#define SOATTT_SL       1   /* Shipping Location */
#define SOATTT_RL       2   /* Receiving Location */
#define SOATTT_FC       3   /* Freight Claim */
#define SOATTT_FR       4   /* Forced Receipt */
#define SOATTT_RE       5   /* Received Elsewhere */

#define _SOATTT_MAX 5


extern const char SOCASS_7000  [NULL_CODE];   /* Food Stamps */
extern const char SOCASS_7010  [NULL_CODE];   /* Electronic Benefits System -EBS */


/* SOLT = Stock Order Reconcile Location Types */
extern const char SOLT[NULL_CODE];

extern const char SOLT_S     [NULL_CODE];   /* Store */
extern const char SOLT_W     [NULL_CODE];   /* Warehouse */
extern const char SOLT_PW    [NULL_CODE];   /* Physical Warehouse */
extern const char SOLT_I     [NULL_CODE];   /* Internal Finisher */
extern const char SOLT_E     [NULL_CODE];   /* External Finisher */
extern const char SOLT_C     [NULL_CODE];   /* Store Class */
extern const char SOLT_R     [NULL_CODE];   /* @OH4@ */
extern const char SOLT_A     [NULL_CODE];   /* @OH3@ */
extern const char SOLT_T     [NULL_CODE];   /* Transfer Zone */
extern const char SOLT_L     [NULL_CODE];   /* Location Trait */
extern const char SOLT_DW    [NULL_CODE];   /* Default Warehouse */
extern const char SOLT_LLS   [NULL_CODE];   /* Loc List Store */
extern const char SOLT_LLW   [NULL_CODE];   /* Loc List Warehouse */

#define SOLTTT_S        1   /* Store */
#define SOLTTT_W        2   /* Warehouse */
#define SOLTTT_PW       3   /* Physical Warehouse */
#define SOLTTT_I        4   /* Internal Finisher */
#define SOLTTT_E        5   /* External Finisher */
#define SOLTTT_C        6   /* Store Class */
#define SOLTTT_R        7   /* @OH4@ */
#define SOLTTT_A        8   /* @OH3@ */
#define SOLTTT_T       10   /* Transfer Zone */
#define SOLTTT_L       12   /* Location Trait */
#define SOLTTT_DW      13   /* Default Warehouse */
#define SOLTTT_LLS     14   /* Loc List Store */
#define SOLTTT_LLW     15   /* Loc List Warehouse */

#define _SOLTTT_MAX 15


/* SOMD = Sourcing Methods for Item-Stores */
extern const char SOMD[NULL_CODE];

extern const char SOMD_S     [NULL_CODE];   /* Supplier */
extern const char SOMD_W     [NULL_CODE];   /* Warehouse */

#define SOMDTT_S        1   /* Supplier */
#define SOMDTT_W        2   /* Warehouse */

#define _SOMDTT_MAX 2


/* SORC = Source types for delivery schedules */
extern const char SORC[NULL_CODE];

extern const char SORC_SUP   [NULL_CODE];   /* Supplier */
extern const char SORC_W     [NULL_CODE];   /* Warehouse */

#define SORCTT_SUP      1   /* Supplier */
#define SORCTT_W        2   /* Warehouse */

#define _SORCTT_MAX 2


/* SPCS = Item Supplier Primary Case Sizes */
extern const char SPCS[NULL_CODE];

extern const char SPCS_S     [NULL_CODE];   /* Standard Unit Of Measure */
extern const char SPCS_I     [NULL_CODE];   /* Inner */
extern const char SPCS_C     [NULL_CODE];   /* Case */
extern const char SPCS_P     [NULL_CODE];   /* Pallet */

#define SPCSTT_S        1   /* Standard Unit Of Measure */
#define SPCSTT_I        2   /* Inner */
#define SPCSTT_C        3   /* Case */
#define SPCSTT_P        4   /* Pallet */

#define _SPCSTT_MAX 4


/* SPQL = Supplier Quantity Level */
extern const char SPQL[NULL_CODE];

extern const char SPQL_CA    [NULL_CODE];   /* Cases */
extern const char SPQL_EA    [NULL_CODE];   /* Eaches */

#define SPQLTT_CA       1   /* Cases */
#define SPQLTT_EA       2   /* Eaches */

#define _SPQLTT_MAX 2


/* SPSC = Supplier Settlement Codes */
extern const char SPSC[NULL_CODE];

extern const char SPSC_E     [NULL_CODE];   /* Evaluated Receipts Settlement */
extern const char SPSC_N     [NULL_CODE];   /* N/A */

#define SPSCTT_E        1   /* Evaluated Receipts Settlement */
#define SPSCTT_N        2   /* N/A */

#define _SPSCTT_MAX 2


/* SPST = Supplier Status */
extern const char SPST[NULL_CODE];

extern const char SPST_A     [NULL_CODE];   /* Active */
extern const char SPST_I     [NULL_CODE];   /* Inactive */

#define SPSTTT_A        1   /* Active */
#define SPSTTT_I        2   /* Inactive */

#define _SPSTTT_MAX 2


/* SQST = Shipment Item Quality Status */
extern const char SQST[NULL_CODE];

extern const char SQST_N     [NULL_CODE];   /* Not Required */
extern const char SQST_R     [NULL_CODE];   /* Required */
extern const char SQST_P     [NULL_CODE];   /* Passed */
extern const char SQST_F     [NULL_CODE];   /* Failed */
extern const char SQST_V     [NULL_CODE];   /* Returned to Vendor */

#define SQSTTT_N        1   /* Not Required */
#define SQSTTT_R        2   /* Required */
#define SQSTTT_P        3   /* Passed */
#define SQSTTT_F        4   /* Failed */
#define SQSTTT_V        5   /* Returned to Vendor */

#define _SQSTTT_MAX 5


/* SRAT = Size Ratio Apply Type */
extern const char SRAT[NULL_CODE];

extern const char SRAT_ST    [NULL_CODE];   /* Store */
extern const char SRAT_SU    [NULL_CODE];   /* @MH6@ */

#define SRATTT_ST       1   /* Store */
#define SRATTT_SU       2   /* @MH6@ */

#define _SRATTT_MAX 2


/* SRCT = Source Types */
extern const char SRCT[NULL_CODE];

extern const char SRCT_R     [NULL_CODE];   /* Replenishment */
extern const char SRCT_I     [NULL_CODE];   /* Investment Buy */
extern const char SRCT_M     [NULL_CODE];   /* Manual */

#define SRCTTT_R        1   /* Replenishment */
#define SRCTTT_I        2   /* Investment Buy */
#define SRCTTT_M        3   /* Manual */

#define _SRCTTT_MAX 3


/* SREC = Sales Audit Record Type Group */
extern const char SREC[NULL_CODE];

extern const char SREC_THEAD [NULL_CODE];   /* Transaction Header Level Error */
extern const char SREC_TITEM [NULL_CODE];   /* Transaction Item Level Error */
extern const char SREC_TTEND [NULL_CODE];   /* Transaction Tender Level Error */
extern const char SREC_TTAX  [NULL_CODE];   /* Transaction Tax Level Error */
extern const char SREC_IDISC [NULL_CODE];   /* Item Discount Level Error */
extern const char SREC_TCUST [NULL_CODE];   /* Transaction Customer Level Error */
extern const char SREC_TOTAL [NULL_CODE];   /* Total Level Error */
extern const char SREC_BALG  [NULL_CODE];   /* Balance Group Level Error */
extern const char SREC_STORE [NULL_CODE];   /* Store Day Level Error */
extern const char SREC_IGTAX [NULL_CODE];   /* Item Level Tax Error */
extern const char SREC_TPYMT [NULL_CODE];   /* Transaction Payment Level Error */

#define SRECTT_THEAD    1   /* Transaction Header Level Error */
#define SRECTT_TITEM    2   /* Transaction Item Level Error */
#define SRECTT_TTEND    3   /* Transaction Tender Level Error */
#define SRECTT_TTAX     4   /* Transaction Tax Level Error */
#define SRECTT_IDISC    5   /* Item Discount Level Error */
#define SRECTT_TCUST    6   /* Transaction Customer Level Error */
#define SRECTT_TOTAL    7   /* Total Level Error */
#define SRECTT_BALG     8   /* Balance Group Level Error */
#define SRECTT_STORE    9   /* Store Day Level Error */
#define SRECTT_IGTAX   10   /* Item Level Tax Error */
#define SRECTT_TPYMT   11   /* Transaction Payment Level Error */

#define _SRECTT_MAX 11


/* SRLB = Store Form Labels */
extern const char SRLB[NULL_CODE];

extern const char SRLB_SF    [NULL_CODE];   /* Sq Ft */
extern const char SRLB_SM    [NULL_CODE];   /* Sq M */

#define SRLBTT_SF       1   /* Sq Ft */
#define SRLBTT_SM       2   /* Sq M */

#define _SRLBTT_MAX 2


/* SRPA = Sales Audit Rule execution based on presence or absence of the rule */
extern const char SRPA[NULL_CODE];

extern const char SRPA_P     [NULL_CODE];   /* Presence */
extern const char SRPA_A     [NULL_CODE];   /* Absence */

#define SRPATT_P        1   /* Presence */
#define SRPATT_A        2   /* Absence */

#define _SRPATT_MAX 2


/* SRPT = Size Ratio Period Types */
extern const char SRPT[NULL_CODE];

extern const char SRPT_D     [NULL_CODE];   /* Date to Date */
extern const char SRPT_L     [NULL_CODE];   /* Last Weeks */
extern const char SRPT_W     [NULL_CODE];   /* Weeks to Date */

#define SRPTTT_D        1   /* Date to Date */
#define SRPTTT_L        2   /* Last Weeks */
#define SRPTTT_W        3   /* Weeks to Date */

#define _SRPTTT_MAX 3


/* SRRT = Sales Audit Rule Realm Types */
extern const char SRRT[NULL_CODE];

extern const char SRRT_A     [NULL_CODE];   /* All */
extern const char SRRT_T     [NULL_CODE];   /* Table */
extern const char SRRT_V     [NULL_CODE];   /* View */
extern const char SRRT_TOTAL [NULL_CODE];   /* Total */

#define SRRTTT_A        1   /* All */
#define SRRTTT_T        2   /* Table */
#define SRRTTT_V        3   /* View */
#define SRRTTT_TOTAL    4   /* Total */

#define _SRRTTT_MAX 4


/* SRTP = Storage Type */
extern const char SRTP[NULL_CODE];

extern const char SRTP_W     [NULL_CODE];   /* Warehouse */
extern const char SRTP_O     [NULL_CODE];   /* Outside */

#define SRTPTT_W        1   /* Warehouse */
#define SRTPTT_O        2   /* Outside */

#define _SRTPTT_MAX 2


/* SRTY = Stock Request Store Types */
extern const char SRTY[NULL_CODE];

extern const char SRTY_S     [NULL_CODE];   /* Store */
extern const char SRTY_C     [NULL_CODE];   /* Store Class */
extern const char SRTY_A     [NULL_CODE];   /* All Stores */

#define SRTYTT_S        1   /* Store */
#define SRTYTT_C        2   /* Store Class */
#define SRTYTT_A        3   /* All Stores */

#define _SRTYTT_MAX 3


/* SSBK = Supplier Item Bracket */
extern const char SSBK[NULL_CODE];

extern const char SSBK_AMNT  [NULL_CODE];   /* Amount */
extern const char SSBK_CUBE  [NULL_CODE];   /* Cube */
extern const char SSBK_WGHT  [NULL_CODE];   /* Weight */
extern const char SSBK_QTY   [NULL_CODE];   /* Quantity */

#define SSBKTT_AMNT     1   /* Amount */
#define SSBKTT_CUBE     2   /* Cube */
#define SSBKTT_WGHT     3   /* Weight */
#define SSBKTT_QTY      4   /* Quantity */

#define _SSBKTT_MAX 4


/* SSEF = Supplier, Site and External Finisher */
extern const char SSEF[NULL_CODE];

extern const char SSEF_S     [NULL_CODE];   /* Supplier */
extern const char SSEF_U     [NULL_CODE];   /* Supplier Site */
extern const char SSEF_E     [NULL_CODE];   /* External Finisher */

#define SSEFTT_S        1   /* Supplier */
#define SSEFTT_U        2   /* Supplier Site */
#define SSEFTT_E        3   /* External Finisher */

#define _SSEFTT_MAX 3


/* SSSA = Accept, Reject, and Hold */
extern const char SSSA[NULL_CODE];

extern const char SSSA_A     [NULL_CODE];   /* Accept */
extern const char SSSA_R     [NULL_CODE];   /* Reject */
extern const char SSSA_H     [NULL_CODE];   /* Hold */

#define SSSATT_A        1   /* Accept */
#define SSSATT_R        2   /* Reject */
#define SSSATT_H        3   /* Hold */

#define _SSSATT_MAX 3


/* SSSB = Accept, Reject or Hold */
extern const char SSSB[NULL_CODE];

extern const char SSSB_A     [NULL_CODE];   /* Accept */
extern const char SSSB_R     [NULL_CODE];   /* Reject */
extern const char SSSB_H     [NULL_CODE];   /* Hold */
extern const char SSSB_X     [NULL_CODE];   /* All */

#define SSSBTT_A        1   /* Accept */
#define SSSBTT_R        2   /* Reject */
#define SSSBTT_H        3   /* Hold */
#define SSSBTT_X        4   /* All */

#define _SSSBTT_MAX 4


/* SSST = Shipment Item Status */
extern const char SSST[NULL_CODE];

extern const char SSST_C     [NULL_CODE];   /* Carton Received */
extern const char SSST_A     [NULL_CODE];   /* Accept */
extern const char SSST_R     [NULL_CODE];   /* Reject */
extern const char SSST_H     [NULL_CODE];   /* Hold */

#define SSSTTT_C        1   /* Carton Received */
#define SSSTTT_A        2   /* Accept */
#define SSSTTT_R        3   /* Reject */
#define SSSTTT_H        4   /* Hold */

#define _SSSTTT_MAX 4


/* SSTP = Store Add Group Type--Store priced Item */
extern const char SSTP[NULL_CODE];

extern const char SSTP_S     [NULL_CODE];   /* Store */
extern const char SSTP_C     [NULL_CODE];   /* Store Class */
extern const char SSTP_D     [NULL_CODE];   /* @OH5@ */
extern const char SSTP_R     [NULL_CODE];   /* @OH4@ */
extern const char SSTP_MA    [NULL_CODE];   /* Min Area */
extern const char SSTP_MX    [NULL_CODE];   /* Max Area */
extern const char SSTP_T     [NULL_CODE];   /* Transfer Zone */
extern const char SSTP_L     [NULL_CODE];   /* Location Trait */
extern const char SSTP_DW    [NULL_CODE];   /* Default Warehouse */
extern const char SSTP_A     [NULL_CODE];   /* All Stores */

#define SSTPTT_S        1   /* Store */
#define SSTPTT_C        2   /* Store Class */
#define SSTPTT_D        3   /* @OH5@ */
#define SSTPTT_R        4   /* @OH4@ */
#define SSTPTT_MA       5   /* Min Area */
#define SSTPTT_MX       6   /* Max Area */
#define SSTPTT_T        8   /* Transfer Zone */
#define SSTPTT_L        9   /* Location Trait */
#define SSTPTT_DW      10   /* Default Warehouse */
#define SSTPTT_A       11   /* All Stores */

#define _SSTPTT_MAX 11


/* SSTY = Supplier Site Code */
extern const char SSTY[NULL_CODE];

extern const char SSTY_CCOSS [NULL_CODE];   /* Case Cost
(Sup. Site) */

extern const char SSTY_CURSS [NULL_CODE];   /* Currency
(Sup. Site) */

extern const char SSTY_PLSS  [NULL_CODE];   /* Pool Supplier Site */
extern const char SSTY_PLSSN [NULL_CODE];   /* Pool Supplier Site Name */
extern const char SSTY_POLSS [NULL_CODE];   /* Pooled
Supplier Site */

extern const char SSTY_PSLS  [NULL_CODE];   /* Primary Supplier Site */
extern const char SSTY_PSSOC [NULL_CODE];   /* Prescaled Supplier Site
Order Cost */

extern const char SSTY_PTSS  [NULL_CODE];   /* Partner/
Supplier Site */

extern const char SSTY_SS    [NULL_CODE];   /* Supplier Site */
extern const char SSTY_SSA   [NULL_CODE];   /* Supplier Site Availability */
extern const char SSTY_SSCU  [NULL_CODE];   /* Sup. Site Cost UOP */
extern const char SSTY_SSCUP1[NULL_CODE];   /* Supplier Site Cost
UOP Primary */

extern const char SSTY_SSD   [NULL_CODE];   /* Supplier Site Description */
extern const char SSTY_SSDIF [NULL_CODE];   /* Sup. Site Diff */
extern const char SSTY_SSDIF1[NULL_CODE];   /* Sup. Site
Diff */

extern const char SSTY_SSITES[NULL_CODE];   /* Supplier Sites */
extern const char SSTY_SSL   [NULL_CODE];   /* Supplier Site Label */
extern const char SSTY_SSLDTM[NULL_CODE];   /* Sup. Site Lead Time */
extern const char SSTY_SSN   [NULL_CODE];   /* Supplier Site Name */
extern const char SSTY_SSOC1 [NULL_CODE];   /* Supplier Site Order Cost */
extern const char SSTY_SSPD  [NULL_CODE];   /* Supplier Site Pack Desc */
extern const char SSTY_SSPS1 [NULL_CODE];   /* Supplier Site Pack Size */
extern const char SSTY_SSPT  [NULL_CODE];   /* Sup. Site Payment Type */
extern const char SSTY_SSUC  [NULL_CODE];   /* Supplier Site
Unit Cost */

extern const char SSTY_SUPP  [NULL_CODE];   /* Supplier */
extern const char SSTY_SUPST [NULL_CODE];   /* Sup. Site */
extern const char SSTY_UCOSS [NULL_CODE];   /* Unit Cost
(Sup. Site) */

extern const char SSTY_UCSS1 [NULL_CODE];   /* Unit Cost
(Supplier Site) */

extern const char SSTY_SSCA  [NULL_CODE];   /* Supplier Site Cost Adjustment */
extern const char SSTY_SSN1  [NULL_CODE];   /* Sup. Site Name */
extern const char SSTY_SSO   [NULL_CODE];   /* Supplier Site - Order */
extern const char SSTY_SD    [NULL_CODE];   /* Supplier Site - Delivery */

#define SSTYTT_CCOSS    1   /* Case Cost
(Sup. Site) */

#define SSTYTT_CURSS    2   /* Currency
(Sup. Site) */

#define SSTYTT_PLSS     3   /* Pool Supplier Site */
#define SSTYTT_PLSSN    4   /* Pool Supplier Site Name */
#define SSTYTT_POLSS    5   /* Pooled
Supplier Site */

#define SSTYTT_PSLS     6   /* Primary Supplier Site */
#define SSTYTT_PSSOC    7   /* Prescaled Supplier Site
Order Cost */

#define SSTYTT_PTSS     8   /* Partner/
Supplier Site */

#define SSTYTT_SS       9   /* Supplier Site */
#define SSTYTT_SSA     10   /* Supplier Site Availability */
#define SSTYTT_SSCU    11   /* Sup. Site Cost UOP */
#define SSTYTT_SSCUP1  12   /* Supplier Site Cost
UOP Primary */

#define SSTYTT_SSD     13   /* Supplier Site Description */
#define SSTYTT_SSDIF   14   /* Sup. Site Diff */
#define SSTYTT_SSDIF1  15   /* Sup. Site
Diff */

#define SSTYTT_SSITES  16   /* Supplier Sites */
#define SSTYTT_SSL     17   /* Supplier Site Label */
#define SSTYTT_SSLDTM  18   /* Sup. Site Lead Time */
#define SSTYTT_SSN     19   /* Supplier Site Name */
#define SSTYTT_SSOC1   20   /* Supplier Site Order Cost */
#define SSTYTT_SSPD    21   /* Supplier Site Pack Desc */
#define SSTYTT_SSPS1   22   /* Supplier Site Pack Size */
#define SSTYTT_SSPT    23   /* Sup. Site Payment Type */
#define SSTYTT_SSUC    24   /* Supplier Site
Unit Cost */

#define SSTYTT_SUPP    25   /* Supplier */
#define SSTYTT_SUPST   26   /* Sup. Site */
#define SSTYTT_UCOSS   27   /* Unit Cost
(Sup. Site) */

#define SSTYTT_UCSS1   28   /* Unit Cost
(Supplier Site) */

#define SSTYTT_SSCA    29   /* Supplier Site Cost Adjustment */
#define SSTYTT_SSN1    30   /* Sup. Site Name */
#define SSTYTT_SSO     31   /* Supplier Site - Order */
#define SSTYTT_SD      32   /* Supplier Site - Delivery */

#define _SSTYTT_MAX 32


/* STAT = RMS Standard Statuses */
extern const char STAT[NULL_CODE];

extern const char STAT_W     [NULL_CODE];   /* Worksheet */
extern const char STAT_S     [NULL_CODE];   /* Submitted */
extern const char STAT_A     [NULL_CODE];   /* Approved */
extern const char STAT_R     [NULL_CODE];   /* Rejected */
extern const char STAT_D     [NULL_CODE];   /* Deleted */

#define STATTT_W        1   /* Worksheet */
#define STATTT_S        2   /* Submitted */
#define STATTT_A        3   /* Approved */
#define STATTT_R        4   /* Rejected */
#define STATTT_D        5   /* Deleted */

#define _STATTT_MAX 5


/* STC2 = Item Stock Category - Store */
extern const char STC2[NULL_CODE];

extern const char STC2_D     [NULL_CODE];   /* Direct To Store Merchandise */

#define STC2TT_D        1   /* Direct To Store Merchandise */

#define _STC2TT_MAX 1


/* STCA = Sales Audit Store/Cashier */
extern const char STCA[NULL_CODE];

extern const char STCA_S     [NULL_CODE];   /* Store */
extern const char STCA_C     [NULL_CODE];   /* Cashier */

#define STCATT_S        1   /* Store */
#define STCATT_C        2   /* Cashier */

#define _STCATT_MAX 2


/* STFV = Supplier Invoice Match Tolerance Favor */
extern const char STFV[NULL_CODE];

extern const char STFV_S     [NULL_CODE];   /* Supplier */
extern const char STFV_R     [NULL_CODE];   /* Retailer */

#define STFVTT_S        1   /* Supplier */
#define STFVTT_R        2   /* Retailer */

#define _STFVTT_MAX 2


/* STGR = Sales Audit Store Groupings */
extern const char STGR[NULL_CODE];

extern const char STGR_A     [NULL_CODE];   /* All Stores */
extern const char STGR_S     [NULL_CODE];   /* Assigned Stores */
extern const char STGR_I     [NULL_CODE];   /* Individual Store */

#define STGRTT_A        1   /* All Stores */
#define STGRTT_S        2   /* Assigned Stores */
#define STGRTT_I        3   /* Individual Store */

#define _STGRTT_MAX 3


/* STKL = Stock Count Level */
extern const char STKL[NULL_CODE];

extern const char STKL_A     [NULL_CODE];   /* All @MHP4@ */
extern const char STKL_D     [NULL_CODE];   /* @MH4@ */
extern const char STKL_C     [NULL_CODE];   /* @MH5@ */
extern const char STKL_S     [NULL_CODE];   /* @MH6@ */
extern const char STKL_N     [NULL_CODE];   /* Non-Specific */

#define STKLTT_A        1   /* All @MHP4@ */
#define STKLTT_D        2   /* @MH4@ */
#define STKLTT_C        3   /* @MH5@ */
#define STKLTT_S        4   /* @MH6@ */
#define STKLTT_N        5   /* Non-Specific */

#define _STKLTT_MAX 5


/* STKT = Stock Ledger Download Templates */
extern const char STKT[NULL_CODE];

extern const char STKT_CTMPL [NULL_CODE];   /* Cost Template */
extern const char STKT_RTMPL [NULL_CODE];   /* Retail Template */
extern const char STKT_CRTMPL[NULL_CODE];   /* Cost-Retail Template */

#define STKTTT_CTMPL    1   /* Cost Template */
#define STKTTT_RTMPL    2   /* Retail Template */
#define STKTTT_CRTMPL   3   /* Cost-Retail Template */

#define _STKTTT_MAX 3


/* STLB = Inventory Stock Label */
extern const char STLB[NULL_CODE];

extern const char STLB_0     [NULL_CODE];   /* Total Stock(SUOM) */
extern const char STLB_1     [NULL_CODE];   /* Unavailable Stock(SUOM) */

#define STLBTT_0        1   /* Total Stock(SUOM) */
#define STLBTT_1        2   /* Unavailable Stock(SUOM) */

#define _STLBTT_MAX 2


/* STLL = Supivmgmt Loc Type */
extern const char STLL[NULL_CODE];

extern const char STLL_ST    [NULL_CODE];   /* Store */
extern const char STLL_WH    [NULL_CODE];   /* Warehouse */
extern const char STLL_SL    [NULL_CODE];   /* Location List */

#define STLLTT_ST       1   /* Store */
#define STLLTT_WH       2   /* Warehouse */
#define STLLTT_SL       3   /* Location List */

#define _STLLTT_MAX 3


/* STLV = Stock Level */
extern const char STLV[NULL_CODE];

extern const char STLV_H     [NULL_CODE];   /* High */
extern const char STLV_L     [NULL_CODE];   /* Low */

#define STLVTT_H        1   /* High */
#define STLVTT_L        2   /* Low */

#define _STLVTT_MAX 2


/* STMB = In Store Market Basket */
extern const char STMB[NULL_CODE];

extern const char STMB_A     [NULL_CODE];   /* A item at this loc */
extern const char STMB_B     [NULL_CODE];   /* B item at this loc */
extern const char STMB_C     [NULL_CODE];   /* C item at this loc */

#define STMBTT_A        1   /* A item at this loc */
#define STMBTT_B        2   /* B item at this loc */
#define STMBTT_C        3   /* C item at this loc */

#define _STMBTT_MAX 3


/* STMO = Status Modification Messages */
extern const char STMO[NULL_CODE];

extern const char STMO_CSWS  [NULL_CODE];   /* Change status Worksheet to Submitted: */
extern const char STMO_CSSW  [NULL_CODE];   /* Change status Submitted to Worksheet: */
extern const char STMO_CSSA  [NULL_CODE];   /* Change status Submitted to Approved: */

#define STMOTT_CSWS     1   /* Change status Worksheet to Submitted: */
#define STMOTT_CSSW     2   /* Change status Submitted to Worksheet: */
#define STMOTT_CSSA     3   /* Change status Submitted to Approved: */

#define _STMOTT_MAX 3


/* STNA = Store Editor Titles */
extern const char STNA[NULL_CODE];

extern const char STNA_SSTNA [NULL_CODE];   /* Secondary Store Name */

#define STNATT_SSTNA    1   /* Secondary Store Name */

#define _STNATT_MAX 1


/* STRC = Store Receive Type */
extern const char STRC[NULL_CODE];

extern const char STRC_A     [NULL_CODE];   /* Auto Receive */
extern const char STRC_B     [NULL_CODE];   /* Receive by BOL */
extern const char STRC_C     [NULL_CODE];   /* Receive by Carton */

#define STRCTT_A        1   /* Auto Receive */
#define STRCTT_B        2   /* Receive by BOL */
#define STRCTT_C        3   /* Receive by Carton */

#define _STRCTT_MAX 3


/* STRG = Sales Audit Store/Register */
extern const char STRG[NULL_CODE];

extern const char STRG_S     [NULL_CODE];   /* Store */
extern const char STRG_R     [NULL_CODE];   /* Register */

#define STRGTT_S        1   /* Store */
#define STRGTT_R        2   /* Register */

#define _STRGTT_MAX 2


/* STRT = Start Element Types */
extern const char STRT[NULL_CODE];

extern const char STRT_F     [NULL_CODE];   /* Form */
extern const char STRT_W     [NULL_CODE];   /* Web Page */
extern const char STRT_V     [NULL_CODE];   /* Desktop View */
extern const char STRT_S     [NULL_CODE];   /* Internal Item */
extern const char STRT_A     [NULL_CODE];   /* User Application */
extern const char STRT_R     [NULL_CODE];   /* Oracle Report */

#define STRTTT_F        1   /* Form */
#define STRTTT_W        2   /* Web Page */
#define STRTTT_V        3   /* Desktop View */
#define STRTTT_S        4   /* Internal Item */
#define STRTTT_A        5   /* User Application */
#define STRTTT_R        6   /* Oracle Report */

#define _STRTTT_MAX 6


/* STS3 = Item Status (no delete) */
extern const char STS3[NULL_CODE];

extern const char STS3_A     [NULL_CODE];   /* Active */
extern const char STS3_I     [NULL_CODE];   /* Inactive */
extern const char STS3_C     [NULL_CODE];   /* Discontinued */

#define STS3TT_A        1   /* Active */
#define STS3TT_I        2   /* Inactive */
#define STS3TT_C        3   /* Discontinued */

#define _STS3TT_MAX 3


/* STSF = Sales Total Status Filter */
extern const char STSF[NULL_CODE];

extern const char STSF_A     [NULL_CODE];   /* Approved Totals */
extern const char STSF_W     [NULL_CODE];   /* Non-Approved Totals */
extern const char STSF_B     [NULL_CODE];   /* All Totals */

#define STSFTT_A        1   /* Approved Totals */
#define STSFTT_W        2   /* Non-Approved Totals */
#define STSFTT_B        3   /* All Totals */

#define _STSFTT_MAX 3


/* STSS = Supplier Site Source Types for delivery schedules */
extern const char STSS[NULL_CODE];

extern const char STSS_SUP   [NULL_CODE];   /* Supplier Site */
extern const char STSS_W     [NULL_CODE];   /* Warehouse */

#define STSSTT_SUP      1   /* Supplier Site */
#define STSSTT_W        2   /* Warehouse */

#define _STSSTT_MAX 2


/* STST = Item Status */
extern const char STST[NULL_CODE];

extern const char STST_A     [NULL_CODE];   /* Active */
extern const char STST_I     [NULL_CODE];   /* Inactive */
extern const char STST_C     [NULL_CODE];   /* Discontinued */
extern const char STST_D     [NULL_CODE];   /* Delete */

#define STSTTT_A        1   /* Active */
#define STSTTT_I        2   /* Inactive */
#define STSTTT_C        3   /* Discontinued */
#define STSTTT_D        4   /* Delete */

#define _STSTTT_MAX 4


/* STTA = Store Task Type (Abbreviated) */
extern const char STTA[NULL_CODE];

extern const char STTA_REC   [NULL_CODE];   /* Shipment */
extern const char STTA_TSF   [NULL_CODE];   /* Transfer */
extern const char STTA_RPC   [NULL_CODE];   /* Price Chg. */
extern const char STTA_RTV   [NULL_CODE];   /* RTV */
extern const char STTA_CNT   [NULL_CODE];   /* Stock Cnt. */

#define STTATT_REC      1   /* Shipment */
#define STTATT_TSF      2   /* Transfer */
#define STTATT_RPC      3   /* Price Chg. */
#define STTATT_RTV      4   /* RTV */
#define STTATT_CNT      5   /* Stock Cnt. */

#define _STTATT_MAX 5


/* STTL = Stocktake Level */
extern const char STTL[NULL_CODE];

extern const char STTL_D     [NULL_CODE];   /* @MH4@ */
extern const char STTL_C     [NULL_CODE];   /* @MH5@ */
extern const char STTL_S     [NULL_CODE];   /* @MH6@ */
extern const char STTL_N     [NULL_CODE];   /* Non-Specific */

#define STTLTT_D        1   /* @MH4@ */
#define STTLTT_C        2   /* @MH5@ */
#define STTLTT_S        3   /* @MH6@ */
#define STTLTT_N        4   /* Non-Specific */

#define _STTLTT_MAX 4


/* STTP = Stocktake Type */
extern const char STTP[NULL_CODE];

extern const char STTP_B     [NULL_CODE];   /* Book Count */
extern const char STTP_U     [NULL_CODE];   /* Cycle Count */

#define STTPTT_B        1   /* Book Count */
#define STTPTT_U        2   /* Cycle Count */

#define _STTPTT_MAX 2


/* STTT = Supplier Invoice Match Tolerance Type */
extern const char STTT[NULL_CODE];

extern const char STTT_A     [NULL_CODE];   /* Amount */
extern const char STTT_P     [NULL_CODE];   /* Percent */

#define STTTTT_A        1   /* Amount */
#define STTTTT_P        2   /* Percent */

#define _STTTTT_MAX 2


/* STYP = Sale Type */
extern const char STYP[NULL_CODE];

extern const char STYP_L     [NULL_CODE];   /* Loose Weight */
extern const char STYP_V     [NULL_CODE];   /* Variable Weight Each */

#define STYPTT_L       10   /* Loose Weight */
#define STYPTT_V       20   /* Variable Weight Each */

#define _STYPTT_MAX 20


/* SUAP = Submit/Approve */
extern const char SUAP[NULL_CODE];

extern const char SUAP_S     [NULL_CODE];   /* Submit */
extern const char SUAP_A     [NULL_CODE];   /* Approve */

#define SUAPTT_S        1   /* Submit */
#define SUAPTT_A        2   /* Approve */

#define _SUAPTT_MAX 2


/* SUBR = Substitution reason codes */
extern const char SUBR[NULL_CODE];

extern const char SUBR_P     [NULL_CODE];   /* Promotional */
extern const char SUBR_T     [NULL_CODE];   /* Transitional */

#define SUBRTT_P        1   /* Promotional */
#define SUBRTT_T        2   /* Transitional */

#define _SUBRTT_MAX 2


/* SUCO = Surety Codes */
extern const char SUCO[NULL_CODE];

extern const char SUCO_1     [NULL_CODE];   /* Surety Code 1 */
extern const char SUCO_2     [NULL_CODE];   /* Surety Code 2 */
extern const char SUCO_3     [NULL_CODE];   /* Surety Code 3 */

#define SUCOTT_1        1   /* Surety Code 1 */
#define SUCOTT_2        2   /* Surety Code 2 */
#define SUCOTT_3        3   /* Surety Code 3 */

#define _SUCOTT_MAX 3


/* SUH2 = Deal Partner Types */
extern const char SUH2[NULL_CODE];

extern const char SUH2_S1    [NULL_CODE];   /* @SUH1@ */
extern const char SUH2_S2    [NULL_CODE];   /* @SUH2@ */
extern const char SUH2_S3    [NULL_CODE];   /* @SUH3@ */
extern const char SUH2_S     [NULL_CODE];   /* Supplier */

#define SUH2TT_S1       1   /* @SUH1@ */
#define SUH2TT_S2       2   /* @SUH2@ */
#define SUH2TT_S3       3   /* @SUH3@ */
#define SUH2TT_S        4   /* Supplier */

#define _SUH2TT_MAX 4


/* SUHL = Supplier Hierarchy Levels */
extern const char SUHL[NULL_CODE];

extern const char SUHL_S1    [NULL_CODE];   /* @SUH1@ */
extern const char SUHL_S2    [NULL_CODE];   /* @SUH2@ */
extern const char SUHL_S3    [NULL_CODE];   /* @SUH3@ */
extern const char SUHL_S     [NULL_CODE];   /* Supplier */

#define SUHLTT_S1       1   /* @SUH1@ */
#define SUHLTT_S2       2   /* @SUH2@ */
#define SUHLTT_S3       3   /* @SUH3@ */
#define SUHLTT_S        4   /* Supplier */

#define _SUHLTT_MAX 4


/* SUPC = Supplier Contact Defaults */
extern const char SUPC[NULL_CODE];

extern const char SUPC_NAME  [NULL_CODE];   /* Contact name not available. */
extern const char SUPC_PHONE [NULL_CODE];   /* Contact phone not available. */

#define SUPCTT_NAME     1   /* Contact name not available. */
#define SUPCTT_PHONE    2   /* Contact phone not available. */

#define _SUPCTT_MAX 2


/* SUPD = Split Replenishment Orders Indicator */
extern const char SUPD[NULL_CODE];

extern const char SUPD_S     [NULL_CODE];   /* Supplier */
extern const char SUPD_D     [NULL_CODE];   /* Supp/@MH4@ */
extern const char SUPD_L     [NULL_CODE];   /* Supp/Loc */
extern const char SUPD_A     [NULL_CODE];   /* Supp/@MH4@/Loc */

#define SUPDTT_S        1   /* Supplier */
#define SUPDTT_D        2   /* Supp/@MH4@ */
#define SUPDTT_L        3   /* Supp/Loc */
#define SUPDTT_A        4   /* Supp/@MH4@/Loc */

#define _SUPDTT_MAX 4


/* SUPT = Supplier Types */
extern const char SUPT[NULL_CODE];

extern const char SUPT_S     [NULL_CODE];   /* Supplier */
extern const char SUPT_P     [NULL_CODE];   /* Pool Supplier */

#define SUPTTT_S        1   /* Supplier */
#define SUPTTT_P        2   /* Pool Supplier */

#define _SUPTTT_MAX 2


/* SUPV = Supplier Violations */
extern const char SUPV[NULL_CODE];

extern const char SUPV_N     [NULL_CODE];   /* Not as expected */
extern const char SUPV_C     [NULL_CODE];   /* Carton labels missing */
extern const char SUPV_S     [NULL_CODE];   /* Shipped to wrong location */

#define SUPVTT_N        1   /* Not as expected */
#define SUPVTT_C        2   /* Carton labels missing */
#define SUPVTT_S        3   /* Shipped to wrong location */

#define _SUPVTT_MAX 3


/* SUTL = Supplier Invoice Match Tolerance Level */
extern const char SUTL[NULL_CODE];

extern const char SUTL_TC    [NULL_CODE];   /* Summary Cost */
extern const char SUTL_TQ    [NULL_CODE];   /* Summary Qty. */
extern const char SUTL_LC    [NULL_CODE];   /* Line Item Cost */
extern const char SUTL_LQ    [NULL_CODE];   /* Line Item Qty. */

#define SUTLTT_TC       1   /* Summary Cost */
#define SUTLTT_TQ       2   /* Summary Qty. */
#define SUTLTT_LC       3   /* Line Item Cost */
#define SUTLTT_LQ       4   /* Line Item Qty. */

#define _SUTLTT_MAX 4


/* SUTT = Supplier Transaction Type */
extern const char SUTT[NULL_CODE];

extern const char SUTT_1     [NULL_CODE];   /* Purchases at Cost */
extern const char SUTT_2     [NULL_CODE];   /* Purchases at Retail */
extern const char SUTT_3     [NULL_CODE];   /* Claims at Cost */
extern const char SUTT_10    [NULL_CODE];   /* Markdowns at Retail */
extern const char SUTT_20    [NULL_CODE];   /* Cancellations at Cost */
extern const char SUTT_30    [NULL_CODE];   /* Sales at Retail */
extern const char SUTT_40    [NULL_CODE];   /* Quantity Failed */
extern const char SUTT_70    [NULL_CODE];   /* Markdowns at Cost */

#define SUTTTT_1        1   /* Purchases at Cost */
#define SUTTTT_2        2   /* Purchases at Retail */
#define SUTTTT_3        3   /* Claims at Cost */
#define SUTTTT_10       4   /* Markdowns at Retail */
#define SUTTTT_20       5   /* Cancellations at Cost */
#define SUTTTT_30       6   /* Sales at Retail */
#define SUTTTT_40       7   /* Quantity Failed */
#define SUTTTT_70       8   /* Markdowns at Cost */

#define _SUTTTT_MAX 8


/* SUTY = Supplier Type */
extern const char SUTY[NULL_CODE];

extern const char SUTY_SUP   [NULL_CODE];   /* Merchandise Vendor */
extern const char SUTY_EV    [NULL_CODE];   /* Expense Vendor */
extern const char SUTY_BK    [NULL_CODE];   /* Bank */
extern const char SUTY_AG    [NULL_CODE];   /* Agent */
extern const char SUTY_FF    [NULL_CODE];   /* Freight Forwarder */
extern const char SUTY_IM    [NULL_CODE];   /* Importer */
extern const char SUTY_IA    [NULL_CODE];   /* Import Authority */
extern const char SUTY_BR    [NULL_CODE];   /* Broker */
extern const char SUTY_FA    [NULL_CODE];   /* Factory */
extern const char SUTY_CO    [NULL_CODE];   /* Consolidator */
extern const char SUTY_AP    [NULL_CODE];   /* Applicant */
extern const char SUTY_CN    [NULL_CODE];   /* Consignee */
extern const char SUTY_S1    [NULL_CODE];   /* @SUH1@ */
extern const char SUTY_S2    [NULL_CODE];   /* @SUH2@ */
extern const char SUTY_S3    [NULL_CODE];   /* @SUH3@ */

#define SUTYTT_SUP      1   /* Merchandise Vendor */
#define SUTYTT_EV       2   /* Expense Vendor */
#define SUTYTT_BK       3   /* Bank */
#define SUTYTT_AG       4   /* Agent */
#define SUTYTT_FF       5   /* Freight Forwarder */
#define SUTYTT_IM       6   /* Importer */
#define SUTYTT_IA       7   /* Import Authority */
#define SUTYTT_BR       8   /* Broker */
#define SUTYTT_FA       9   /* Factory */
#define SUTYTT_CO      10   /* Consolidator */
#define SUTYTT_AP      11   /* Applicant */
#define SUTYTT_CN      12   /* Consignee */
#define SUTYTT_S1      13   /* @SUH1@ */
#define SUTYTT_S2      14   /* @SUH2@ */
#define SUTYTT_S3      15   /* @SUH3@ */

#define _SUTYTT_MAX 15


/* SWDP = Store, Warehouse, Discharge Port */
extern const char SWDP[NULL_CODE];

extern const char SWDP_S     [NULL_CODE];   /* Store */
extern const char SWDP_W     [NULL_CODE];   /* Warehouse */
extern const char SWDP_DP    [NULL_CODE];   /* Discharge Port */

#define SWDPTT_S        1   /* Store */
#define SWDPTT_W        2   /* Warehouse */
#define SWDPTT_DP       3   /* Discharge Port */

#define _SWDPTT_MAX 3


/* SWFT = SWIFT Tags */
extern const char SWFT[NULL_CODE];

extern const char SWFT_46B   [NULL_CODE];   /* Required Document SWIFT Tag */
extern const char SWFT_71B   [NULL_CODE];   /* Charges SWIFT Tag */
extern const char SWFT_78    [NULL_CODE];   /* Bank Instructions SWIFT Tag */
extern const char SWFT_72    [NULL_CODE];   /* Sender Instructions SWIFT Tag */
extern const char SWFT_47B   [NULL_CODE];   /* Additional Instructions SWIFT Tag */

#define SWFTTT_46B      1   /* Required Document SWIFT Tag */
#define SWFTTT_71B      2   /* Charges SWIFT Tag */
#define SWFTTT_78       3   /* Bank Instructions SWIFT Tag */
#define SWFTTT_72       4   /* Sender Instructions SWIFT Tag */
#define SWFTTT_47B      5   /* Additional Instructions SWIFT Tag */

#define _SWFTTT_MAX 5


/* SYSE = Sales Audit System Export Interfaces */
extern const char SYSE[NULL_CODE];

extern const char SYSE_RMS   [NULL_CODE];   /* RMS Export */
extern const char SYSE_RA    [NULL_CODE];   /* RA Export */
extern const char SYSE_GL    [NULL_CODE];   /* GL Export */
extern const char SYSE_SFM   [NULL_CODE];   /* SFM Export */
extern const char SYSE_ACH   [NULL_CODE];   /* ACH Export */
extern const char SYSE_UAR   [NULL_CODE];   /* UAR Export */
extern const char SYSE_IM    [NULL_CODE];   /* IM Export */
extern const char SYSE_ORIN  [NULL_CODE];   /* ORIN Export */
extern const char SYSE_SIM   [NULL_CODE];   /* SIM Export */

#define SYSETT_RMS      1   /* RMS Export */
#define SYSETT_RA       2   /* RA Export */
#define SYSETT_GL       3   /* GL Export */
#define SYSETT_SFM      4   /* SFM Export */
#define SYSETT_ACH      5   /* ACH Export */
#define SYSETT_UAR      6   /* UAR Export */
#define SYSETT_IM       7   /* IM Export */
#define SYSETT_ORIN     8   /* ORIN Export */
#define SYSETT_SIM      9   /* SIM Export */

#define _SYSETT_MAX 9


/* SYSG = System Generated */
extern const char SYSG[NULL_CODE];

extern const char SYSG_S     [NULL_CODE];   /* System */
extern const char SYSG_M     [NULL_CODE];   /* Manual */

#define SYSGTT_S        1   /* System */
#define SYSGTT_M        2   /* Manual */

#define _SYSGTT_MAX 2


/* SYSI = Sales Audit System Import Interfaces */
extern const char SYSI[NULL_CODE];

extern const char SYSI_POS   [NULL_CODE];   /* Sales Import */
extern const char SYSI_SFM   [NULL_CODE];   /* SFM Import */
extern const char SYSI_UAR   [NULL_CODE];   /* UAR Import */
extern const char SYSI_IGTAX [NULL_CODE];   /* Item Level Tax */

#define SYSITT_POS      1   /* Sales Import */
#define SYSITT_SFM      3   /* SFM Import */
#define SYSITT_UAR      4   /* UAR Import */
#define SYSITT_IGTAX    5   /* Item Level Tax */

#define _SYSITT_MAX 5


/* SZTP = Store Add Group Type--Zone priced SKU */
extern const char SZTP[NULL_CODE];

extern const char SZTP_S     [NULL_CODE];   /* Store */
extern const char SZTP_C     [NULL_CODE];   /* Store Class */
extern const char SZTP_D     [NULL_CODE];   /* @OH5@ */
extern const char SZTP_R     [NULL_CODE];   /* @OH4@ */
extern const char SZTP_MA    [NULL_CODE];   /* Min Area */
extern const char SZTP_MX    [NULL_CODE];   /* Max Area */
extern const char SZTP_T     [NULL_CODE];   /* Transfer Zone */
extern const char SZTP_L     [NULL_CODE];   /* Location Trait */
extern const char SZTP_DW    [NULL_CODE];   /* Default Warehouse */
extern const char SZTP_A     [NULL_CODE];   /* All Stores */

#define SZTPTT_S        1   /* Store */
#define SZTPTT_C        2   /* Store Class */
#define SZTPTT_D        3   /* @OH5@ */
#define SZTPTT_R        4   /* @OH4@ */
#define SZTPTT_MA       5   /* Min Area */
#define SZTPTT_MX       6   /* Max Area */
#define SZTPTT_T        8   /* Transfer Zone */
#define SZTPTT_L       10   /* Location Trait */
#define SZTPTT_DW      11   /* Default Warehouse */
#define SZTPTT_A       12   /* All Stores */

#define _SZTPTT_MAX 12


/* SZTT = Size Template Type */
extern const char SZTT[NULL_CODE];

extern const char SZTT_P     [NULL_CODE];   /* Percentage */
extern const char SZTT_R     [NULL_CODE];   /* Ratio */

#define SZTTTT_P        1   /* Percentage */
#define SZTTTT_R        2   /* Ratio */

#define _SZTTTT_MAX 2


/* T1IT = Tran Level 1 Item Level Descriptions */
extern const char T1IT[NULL_CODE];

extern const char T1IT_1     [NULL_CODE];   /* SKU */
extern const char T1IT_2     [NULL_CODE];   /* Reference Item */

#define T1ITTT_1        1   /* SKU */
#define T1ITTT_2        2   /* Reference Item */

#define _T1ITTT_MAX 2


/* T2IT = Tran Level 2 Item Level Descriptions */
extern const char T2IT[NULL_CODE];

extern const char T2IT_1     [NULL_CODE];   /* Style */
extern const char T2IT_2     [NULL_CODE];   /* SKU */
extern const char T2IT_3     [NULL_CODE];   /* Reference Item */

#define T2ITTT_1        1   /* Style */
#define T2ITTT_2        2   /* SKU */
#define T2ITTT_3        3   /* Reference Item */

#define _T2ITTT_MAX 3


/* T3IT = Tran Level 3 Item Level Descriptions */
extern const char T3IT[NULL_CODE];

extern const char T3IT_1     [NULL_CODE];   /* Style */
extern const char T3IT_2     [NULL_CODE];   /* SKU */
extern const char T3IT_3     [NULL_CODE];   /* Reference Item */

#define T3ITTT_1        1   /* Style */
#define T3ITTT_2        2   /* SKU */
#define T3ITTT_3        3   /* Reference Item */

#define _T3ITTT_MAX 3


/* TABS = Sales Audit Form Tabs */
extern const char TABS[NULL_CODE];

extern const char TABS_I     [NULL_CODE];   /* Item */
extern const char TABS_T     [NULL_CODE];   /* Tender */
extern const char TABS_E     [NULL_CODE];   /* Exported */
extern const char TABS_TX    [NULL_CODE];   /* Tax */
extern const char TABS_C     [NULL_CODE];   /* Customer */
extern const char TABS_TA    [NULL_CODE];   /* Transaction Attributes */
extern const char TABS_P     [NULL_CODE];   /* Paid Out Details */

#define TABSTT_I        1   /* Item */
#define TABSTT_T        2   /* Tender */
#define TABSTT_E        3   /* Exported */
#define TABSTT_TX       4   /* Tax */
#define TABSTT_C        5   /* Customer */
#define TABSTT_TA       6   /* Transaction Attributes */
#define TABSTT_P        7   /* Paid Out Details */

#define _TABSTT_MAX 7


/* TABT = RMS Notifications Tab Title. */
extern const char TABT[NULL_CODE];

extern const char TABT_ITEM  [NULL_CODE];   /* Item */
extern const char TABT_PO    [NULL_CODE];   /* Order */
extern const char TABT_TSF   [NULL_CODE];   /* Transfer */
extern const char TABT_ASYNC [NULL_CODE];   /* Asynchronous Job Log */
extern const char TABT_LOAD  [NULL_CODE];   /* Data Loading Status */

#define TABTTT_ITEM     1   /* Item */
#define TABTTT_PO       2   /* Order */
#define TABTTT_TSF      3   /* Transfer */
#define TABTTT_ASYNC    4   /* Asynchronous Job Log */
#define TABTTT_LOAD     5   /* Data Loading Status */

#define _TABTTT_MAX 5


/* TARE = Tare Weight Type */
extern const char TARE[NULL_CODE];

extern const char TARE_W     [NULL_CODE];   /* Wet */
extern const char TARE_D     [NULL_CODE];   /* Dry */

#define TARETT_W        1   /* Wet */
#define TARETT_D        2   /* Dry */

#define _TARETT_MAX 2


/* TAXC = Sales Audit Tax Codes */
extern const char TAXC[NULL_CODE];

extern const char TAXC_ERR   [NULL_CODE];   /* Error */
extern const char TAXC_TERM  [NULL_CODE];   /* Termination Record */
extern const char TAXC_STATE [NULL_CODE];   /* State Tax */
extern const char TAXC_CITY  [NULL_CODE];   /* City Tax */
extern const char TAXC_TOTTAX[NULL_CODE];   /* Aggregate total of tax excluding VAT */

#define TAXCTT_ERR      1   /* Error */
#define TAXCTT_TERM     2   /* Termination Record */
#define TAXCTT_STATE    3   /* State Tax */
#define TAXCTT_CITY     4   /* City Tax */
#define TAXCTT_TOTTAX   5   /* Aggregate total of tax excluding VAT */

#define _TAXCTT_MAX 5


/* TCAT = Template Category */
extern const char TCAT[NULL_CODE];

extern const char TCAT_IS9T  [NULL_CODE];   /* Item API Templates */
extern const char TCAT_POS9T [NULL_CODE];   /* Purchase Order API Templates */

#define TCATTT_IS9T     1   /* Item API Templates */
#define TCATTT_POS9T    2   /* Purchase Order API Templates */

#define _TCATTT_MAX 2


/* TCKT = Ticket Type Items */
extern const char TCKT[NULL_CODE];

extern const char TCKT_ITEM  [NULL_CODE];   /* Item */
extern const char TCKT_ITDS  [NULL_CODE];   /* Item Description */
extern const char TCKT_ITSD  [NULL_CODE];   /* Item Short Description */
extern const char TCKT_VAR   [NULL_CODE];   /* Primary Variant */
extern const char TCKT_DIF1  [NULL_CODE];   /* Diff 1 */
extern const char TCKT_DIF2  [NULL_CODE];   /* Diff 2 */
extern const char TCKT_WGHT  [NULL_CODE];   /* Weight */
extern const char TCKT_DEPT  [NULL_CODE];   /* @MH4@ */
extern const char TCKT_CLSS  [NULL_CODE];   /* @MH5@ */
extern const char TCKT_SBCL  [NULL_CODE];   /* @MH6@ */
extern const char TCKT_RTPC  [NULL_CODE];   /* Retail Price */
extern const char TCKT_SRTP  [NULL_CODE];   /* Suggested Retail Price */
extern const char TCKT_MUPC  [NULL_CODE];   /* Multi-unit Price */
extern const char TCKT_SUPR  [NULL_CODE];   /* Supplier */
extern const char TCKT_SUP1  [NULL_CODE];   /* Supplier Diff 1 */
extern const char TCKT_SUP2  [NULL_CODE];   /* Supplier Diff 2 */
extern const char TCKT_STRE  [NULL_CODE];   /* Store */
extern const char TCKT_WHSE  [NULL_CODE];   /* Warehouse */
extern const char TCKT_COOG  [NULL_CODE];   /* Country Of Sourcing */
extern const char TCKT_UOM   [NULL_CODE];   /* Price Per Unit of Measure */
extern const char TCKT_ITPR  [NULL_CODE];   /* Item Parent ID */
extern const char TCKT_IPDS  [NULL_CODE];   /* Item Parent Description */
extern const char TCKT_EURO  [NULL_CODE];   /* Euro/Original Currency */
extern const char TCKT_DIF3  [NULL_CODE];   /* Diff 3 */
extern const char TCKT_DIF4  [NULL_CODE];   /* Diff 4 */
extern const char TCKT_SUP3  [NULL_CODE];   /* Supplier Diff 3 */
extern const char TCKT_SUP4  [NULL_CODE];   /* Supplier Diff 4 */
extern const char TCKT_NETV  [NULL_CODE];   /* Unit Retail of VAT */
extern const char TCKT_DTOT  [NULL_CODE];   /* Total deposit item unit retail */
extern const char TCKT_DPST  [NULL_CODE];   /* Contents Item container deposit amount */

#define TCKTTT_ITEM     1   /* Item */
#define TCKTTT_ITDS     2   /* Item Description */
#define TCKTTT_ITSD     3   /* Item Short Description */
#define TCKTTT_VAR      4   /* Primary Variant */
#define TCKTTT_DIF1     5   /* Diff 1 */
#define TCKTTT_DIF2     6   /* Diff 2 */
#define TCKTTT_WGHT     7   /* Weight */
#define TCKTTT_DEPT     8   /* @MH4@ */
#define TCKTTT_CLSS     9   /* @MH5@ */
#define TCKTTT_SBCL    10   /* @MH6@ */
#define TCKTTT_RTPC    11   /* Retail Price */
#define TCKTTT_SRTP    12   /* Suggested Retail Price */
#define TCKTTT_MUPC    13   /* Multi-unit Price */
#define TCKTTT_SUPR    14   /* Supplier */
#define TCKTTT_SUP1    15   /* Supplier Diff 1 */
#define TCKTTT_SUP2    16   /* Supplier Diff 2 */
#define TCKTTT_STRE    17   /* Store */
#define TCKTTT_WHSE    18   /* Warehouse */
#define TCKTTT_COOG    19   /* Country Of Sourcing */
#define TCKTTT_UOM     20   /* Price Per Unit of Measure */
#define TCKTTT_ITPR    21   /* Item Parent ID */
#define TCKTTT_IPDS    22   /* Item Parent Description */
#define TCKTTT_EURO    23   /* Euro/Original Currency */
#define TCKTTT_DIF3    24   /* Diff 3 */
#define TCKTTT_DIF4    25   /* Diff 4 */
#define TCKTTT_SUP3    26   /* Supplier Diff 3 */
#define TCKTTT_SUP4    27   /* Supplier Diff 4 */
#define TCKTTT_NETV    28   /* Unit Retail of VAT */
#define TCKTTT_DTOT    29   /* Total deposit item unit retail */
#define TCKTTT_DPST    30   /* Contents Item container deposit amount */

#define _TCKTTT_MAX 30


/* TENT = Sales Audit Tender Type Groups */
extern const char TENT[NULL_CODE];

extern const char TENT_ERR   [NULL_CODE];   /* Error */
extern const char TENT_TERM  [NULL_CODE];   /* Termination Record */
extern const char TENT_CASH  [NULL_CODE];   /* Cash */
extern const char TENT_CHECK [NULL_CODE];   /* Personal Check */
extern const char TENT_CCARD [NULL_CODE];   /* Credit Card */
extern const char TENT_VOUCH [NULL_CODE];   /* Voucher (gift cert. or credit) */
extern const char TENT_COUPON[NULL_CODE];   /* Coupon */
extern const char TENT_MORDER[NULL_CODE];   /* Money Order */
extern const char TENT_DCARD [NULL_CODE];   /* Debit Card */
extern const char TENT_DRIVEO[NULL_CODE];   /* Drive Off */
extern const char TENT_SOCASS[NULL_CODE];   /* Social Assistance */
extern const char TENT_FONCOT[NULL_CODE];   /* Fonacot */
extern const char TENT_PAYPAL[NULL_CODE];   /* Paypal */
extern const char TENT_OTHERS[NULL_CODE];   /* Others */

#define TENTTT_ERR      1   /* Error */
#define TENTTT_TERM     2   /* Termination Record */
#define TENTTT_CASH     3   /* Cash */
#define TENTTT_CHECK    4   /* Personal Check */
#define TENTTT_CCARD    5   /* Credit Card */
#define TENTTT_VOUCH    6   /* Voucher (gift cert. or credit) */
#define TENTTT_COUPON   7   /* Coupon */
#define TENTTT_MORDER   8   /* Money Order */
#define TENTTT_DCARD    9   /* Debit Card */
#define TENTTT_DRIVEO  10   /* Drive Off */
#define TENTTT_SOCASS  11   /* Social Assistance */
#define TENTTT_FONCOT  12   /* Fonacot */
#define TENTTT_PAYPAL  13   /* Paypal */
#define TENTTT_OTHERS  14   /* Others */

#define _TENTTT_MAX 14


/* TERM = Sales Audit Record Termination Marker */
extern const char TERM[NULL_CODE];

extern const char TERM_TERM  [NULL_CODE];   /* Termination Record marker */

#define TERMTT_TERM     1   /* Termination Record marker */

#define _TERMTT_MAX 1


/* TFML = Transfer Markdown Location */
extern const char TFML[NULL_CODE];

extern const char TFML_S     [NULL_CODE];   /* Sending Location */
extern const char TFML_R     [NULL_CODE];   /* Receiving Location */

#define TFMLTT_S        1   /* Sending Location */
#define TFMLTT_R        2   /* Receiving Location */

#define _TFMLTT_MAX 2


/* TIME = Before Time */
extern const char TIME[NULL_CODE];

extern const char TIME_1     [NULL_CODE];   /* 1:00 */
extern const char TIME_2     [NULL_CODE];   /* 2:00 */
extern const char TIME_3     [NULL_CODE];   /* 3:00 */
extern const char TIME_4     [NULL_CODE];   /* 4:00 */
extern const char TIME_5     [NULL_CODE];   /* 5:00 */
extern const char TIME_6     [NULL_CODE];   /* 6:00 */
extern const char TIME_7     [NULL_CODE];   /* 7:00 */
extern const char TIME_8     [NULL_CODE];   /* 8:00 */
extern const char TIME_9     [NULL_CODE];   /* 9:00 */
extern const char TIME_10    [NULL_CODE];   /* 10:00 */
extern const char TIME_11    [NULL_CODE];   /* 11:00 */
extern const char TIME_12    [NULL_CODE];   /* 12:00 */
extern const char TIME_13    [NULL_CODE];   /* 13:00 */
extern const char TIME_14    [NULL_CODE];   /* 14:00 */
extern const char TIME_15    [NULL_CODE];   /* 15:00 */
extern const char TIME_16    [NULL_CODE];   /* 16:00 */
extern const char TIME_17    [NULL_CODE];   /* 17:00 */
extern const char TIME_18    [NULL_CODE];   /* 18:00 */
extern const char TIME_19    [NULL_CODE];   /* 19:00 */
extern const char TIME_20    [NULL_CODE];   /* 20:00 */
extern const char TIME_21    [NULL_CODE];   /* 21:00 */
extern const char TIME_22    [NULL_CODE];   /* 22:00 */
extern const char TIME_23    [NULL_CODE];   /* 23:00 */
extern const char TIME_24    [NULL_CODE];   /* 24:00 */

#define TIMETT_1        1   /* 1:00 */
#define TIMETT_2        2   /* 2:00 */
#define TIMETT_3        3   /* 3:00 */
#define TIMETT_4        4   /* 4:00 */
#define TIMETT_5        5   /* 5:00 */
#define TIMETT_6        6   /* 6:00 */
#define TIMETT_7        7   /* 7:00 */
#define TIMETT_8        8   /* 8:00 */
#define TIMETT_9        9   /* 9:00 */
#define TIMETT_10      10   /* 10:00 */
#define TIMETT_11      11   /* 11:00 */
#define TIMETT_12      12   /* 12:00 */
#define TIMETT_13      13   /* 13:00 */
#define TIMETT_14      14   /* 14:00 */
#define TIMETT_15      15   /* 15:00 */
#define TIMETT_16      16   /* 16:00 */
#define TIMETT_17      17   /* 17:00 */
#define TIMETT_18      18   /* 18:00 */
#define TIMETT_19      19   /* 19:00 */
#define TIMETT_20      20   /* 20:00 */
#define TIMETT_21      21   /* 21:00 */
#define TIMETT_22      22   /* 22:00 */
#define TIMETT_23      23   /* 23:00 */
#define TIMETT_24      24   /* 24:00 */

#define _TIMETT_MAX 24


/* TINT = To Inventory Types */
extern const char TINT[NULL_CODE];

extern const char TINT_A     [NULL_CODE];   /* Available */
extern const char TINT_S     [NULL_CODE];   /* Specific Unavailable */

#define TINTTT_A        1   /* Available */
#define TINTTT_S        2   /* Specific Unavailable */

#define _TINTTT_MAX 2


/* TIRC = Timeline Reason Code */
extern const char TIRC[NULL_CODE];

extern const char TIRC_1     [NULL_CODE];   /* Shipment Arrived Early */
extern const char TIRC_2     [NULL_CODE];   /* Shipment Arrived Late */

#define TIRCTT_1        1   /* Shipment Arrived Early */
#define TIRCTT_2        2   /* Shipment Arrived Late */

#define _TIRCTT_MAX 2


/* TKIT = Ticket Print Items */
extern const char TKIT[NULL_CODE];

extern const char TKIT_I     [NULL_CODE];   /* Single Item */
extern const char TKIT_L     [NULL_CODE];   /* Item List */
extern const char TKIT_P     [NULL_CODE];   /* Purchase Order */

#define TKITTT_I        1   /* Single Item */
#define TKITTT_L        2   /* Item List */
#define TKITTT_P        3   /* Purchase Order */

#define _TKITTT_MAX 3


/* TKTA = Ticket Attribute Type */
extern const char TKTA[NULL_CODE];

extern const char TKTA_A     [NULL_CODE];   /* Attribute */
extern const char TKTA_U     [NULL_CODE];   /* User Defined Attribute */

#define TKTATT_A        1   /* Attribute */
#define TKTATT_U        2   /* User Defined Attribute */

#define _TKTATT_MAX 2


/* TLBC = Timeline Base Code */
extern const char TLBC[NULL_CODE];

extern const char TLBC_APD   [NULL_CODE];   /* Approval Date */
extern const char TLBC_NBD   [NULL_CODE];   /* Not Before Date */
extern const char TLBC_NAD   [NULL_CODE];   /* Not After Date */
extern const char TLBC_WRD   [NULL_CODE];   /* Written Date */

#define TLBCTT_APD      1   /* Approval Date */
#define TLBCTT_NBD      2   /* Not Before Date */
#define TLBCTT_NAD      3   /* Not After Date */
#define TLBCTT_WRD      4   /* Written Date */

#define _TLBCTT_MAX 4


/* TLLB = Tools Labels */
extern const char TLLB[NULL_CODE];

extern const char TLLB_SAVING[NULL_CODE];   /* Please Wait, saving... */
extern const char TLLB_SETNUM[NULL_CODE];   /* Set Number */

#define TLLBTT_SAVING   1   /* Please Wait, saving... */
#define TLLBTT_SETNUM   2   /* Set Number */

#define _TLLBTT_MAX 2


/* TLLC = Translation code for default UI label to be used for the field if the label is not available in the UI layer. */
extern const char TLLC[NULL_CODE];

extern const char TLLC_ADDKEY[NULL_CODE];   /* Address Id */
extern const char TLLC_ADD1  [NULL_CODE];   /* Line 1 */
extern const char TLLC_ADD2  [NULL_CODE];   /* Line 2 */
extern const char TLLC_ADD3  [NULL_CODE];   /* Line 3 */
extern const char TLLC_ACITY [NULL_CODE];   /* City */
extern const char TLLC_ACNTC [NULL_CODE];   /* Contact Name */
extern const char TLLC_ACNTY [NULL_CODE];   /* County */
extern const char TLLC_AREA  [NULL_CODE];   /* @OH3@ */
extern const char TLLC_ARNM  [NULL_CODE];   /* Name */
extern const char TLLC_CHAIN [NULL_CODE];   /* @OH2@ */
extern const char TLLC_CHNM  [NULL_CODE];   /* Name */
extern const char TLLC_CLDEPT[NULL_CODE];   /* Dept */
extern const char TLLC_CLASS [NULL_CODE];   /* @MH5@ */
extern const char TLLC_CLNM  [NULL_CODE];   /* Name */
extern const char TLLC_CZZGI [NULL_CODE];   /* Zone Group */
extern const char TLLC_CZZID [NULL_CODE];   /* Zone */
extern const char TLLC_COSTZN[NULL_CODE];   /* Description */
extern const char TLLC_CZGZGI[NULL_CODE];   /* Zone Group */
extern const char TLLC_CZGDES[NULL_CODE];   /* Description */
extern const char TLLC_DEPT  [NULL_CODE];   /* Dept */
extern const char TLLC_DEPTNM[NULL_CODE];   /* Name */
extern const char TLLC_DIFGID[NULL_CODE];   /* Diff Group */
extern const char TLLC_DIFGRP[NULL_CODE];   /* Description */
extern const char TLLC_RANGID[NULL_CODE];   /* Diff Range */
extern const char TLLC_RANG  [NULL_CODE];   /* Description */
extern const char TLLC_RATID [NULL_CODE];   /* Diff Ratio */
extern const char TLLC_RATDES[NULL_CODE];   /* Description */
extern const char TLLC_DISTNM[NULL_CODE];   /* Name */
extern const char TLLC_DIST  [NULL_CODE];   /* @OH5@ */
extern const char TLLC_DIV   [NULL_CODE];   /* @MH2@ */
extern const char TLLC_DIVNM [NULL_CODE];   /* Name */
extern const char TLLC_GRPNO [NULL_CODE];   /* Group */
extern const char TLLC_GRPNM [NULL_CODE];   /* Name */
extern const char TLLC_IMGITM[NULL_CODE];   /* Item */
extern const char TLLC_IMGNM [NULL_CODE];   /* Image Name */
extern const char TLLC_IMG   [NULL_CODE];   /* Description */
extern const char TLLC_ITDESC[NULL_CODE];   /* Description */
extern const char TLLC_ITEM  [NULL_CODE];   /* Item */
extern const char TLLC_IT2DES[NULL_CODE];   /* Secondary Description */
extern const char TLLC_ITSDES[NULL_CODE];   /* Short Description */
extern const char TLLC_ISITEM[NULL_CODE];   /* Item */
extern const char TLLC_ISSUPP[NULL_CODE];   /* Supplier */
extern const char TLLC_ISLABL[NULL_CODE];   /* Supplier Label */
extern const char TLLC_ISDIF1[NULL_CODE];   /* Diff 1 */
extern const char TLLC_ISDIF2[NULL_CODE];   /* Diff 2 */
extern const char TLLC_ISDIF3[NULL_CODE];   /* Diff 3 */
extern const char TLLC_ISDIF4[NULL_CODE];   /* Diff 4 */
extern const char TLLC_ITXFID[NULL_CODE];   /* Item Transform Id */
extern const char TLLC_ITXFRM[NULL_CODE];   /* Description */
extern const char TLLC_LLSTID[NULL_CODE];   /* Location List */
extern const char TLLC_LLIST [NULL_CODE];   /* Description */
extern const char TLLC_PACKTI[NULL_CODE];   /* Pack Template Id */
extern const char TLLC_PACKTD[NULL_CODE];   /* Description */
extern const char TLLC_PTNR2N[NULL_CODE];   /* Secondary Name */
extern const char TLLC_PTNRNM[NULL_CODE];   /* Name */
extern const char TLLC_PTNRID[NULL_CODE];   /* Partner */
extern const char TLLC_PTNRTY[NULL_CODE];   /* Type */
extern const char TLLC_PMHNM [NULL_CODE];   /* Name */
extern const char TLLC_PMHGP [NULL_CODE];   /* Grandparent Id */
extern const char TLLC_PMHID [NULL_CODE];   /* Pending Merch Id */
extern const char TLLC_PMHTYP[NULL_CODE];   /* Hierarchy Type */
extern const char TLLC_PMHPID[NULL_CODE];   /* Parent Id */
extern const char TLLC_CPNDES[NULL_CODE];   /* Description */
extern const char TLLC_CPNID [NULL_CODE];   /* Coupon */
extern const char TLLC_PGRPID[NULL_CODE];   /* Priority Group */
extern const char TLLC_PGRP  [NULL_CODE];   /* Description */
extern const char TLLC_RCLS  [NULL_CODE];   /* Description */
extern const char TLLC_RCLSNO[NULL_CODE];   /* Reclass number */
extern const char TLLC_REG   [NULL_CODE];   /* @OH4@ */
extern const char TLLC_REGNM [NULL_CODE];   /* Name */
extern const char TLLC_RLTNID[NULL_CODE];   /* Relationship Id */
extern const char TLLC_RLTNNM[NULL_CODE];   /* Description */
extern const char TLLC_SLSTD [NULL_CODE];   /* Description */
extern const char TLLC_SLST  [NULL_CODE];   /* Item List Id */
extern const char TLLC_STNM  [NULL_CODE];   /* Name */
extern const char TLLC_ST2NM [NULL_CODE];   /* Secondary Name */
extern const char TLLC_ST    [NULL_CODE];   /* Store */
extern const char TLLC_STAD2N[NULL_CODE];   /* Secondary Name */
extern const char TLLC_STADST[NULL_CODE];   /* Store */
extern const char TLLC_STADNM[NULL_CODE];   /* Name */
extern const char TLLC_SCDEPT[NULL_CODE];   /* Dept */
extern const char TLLC_SCNM  [NULL_CODE];   /* Name */
extern const char TLLC_SUBCLS[NULL_CODE];   /* @MH6@ */
extern const char TLLC_SCCLS [NULL_CODE];   /* @MH5@ */
extern const char TLLC_SUP2NM[NULL_CODE];   /* Secondary Name */
extern const char TLLC_SUPNM [NULL_CODE];   /* Name */
extern const char TLLC_SUPP  [NULL_CODE];   /* Supplier */
extern const char TLLC_SUPCNM[NULL_CODE];   /* Contact Name */
extern const char TLLC_SPTSUP[NULL_CODE];   /* Supplier */
extern const char TLLC_SPTID [NULL_CODE];   /* Pack Template Id */
extern const char TLLC_SPTDES[NULL_CODE];   /* Description */
extern const char TLLC_UDAID [NULL_CODE];   /* UDA Id */
extern const char TLLC_UDATXT[NULL_CODE];   /* Text */
extern const char TLLC_UDAITM[NULL_CODE];   /* Item */
extern const char TLLC_WCBDCI[NULL_CODE];   /* Cost Component */
extern const char TLLC_WCBDTI[NULL_CODE];   /* Template Id */
extern const char TLLC_WCBD  [NULL_CODE];   /* Description */
extern const char TLLC_WCBHTI[NULL_CODE];   /* Template Id */
extern const char TLLC_WCBH  [NULL_CODE];   /* Description */
extern const char TLLC_WH    [NULL_CODE];   /* Warehouse */
extern const char TLLC_WHNM  [NULL_CODE];   /* Name */
extern const char TLLC_WH2NM [NULL_CODE];   /* Secondary Name */
extern const char TLLC_UDATXD[NULL_CODE];   /* Description */

#define TLLCTT_ADDKEY   1   /* Address Id */
#define TLLCTT_ADD1     2   /* Line 1 */
#define TLLCTT_ADD2     3   /* Line 2 */
#define TLLCTT_ADD3     4   /* Line 3 */
#define TLLCTT_ACITY    5   /* City */
#define TLLCTT_ACNTC    6   /* Contact Name */
#define TLLCTT_ACNTY    7   /* County */
#define TLLCTT_AREA     8   /* @OH3@ */
#define TLLCTT_ARNM     9   /* Name */
#define TLLCTT_CHAIN   10   /* @OH2@ */
#define TLLCTT_CHNM    11   /* Name */
#define TLLCTT_CLDEPT  12   /* Dept */
#define TLLCTT_CLASS   13   /* @MH5@ */
#define TLLCTT_CLNM    14   /* Name */
#define TLLCTT_CZZGI   15   /* Zone Group */
#define TLLCTT_CZZID   16   /* Zone */
#define TLLCTT_COSTZN  17   /* Description */
#define TLLCTT_CZGZGI  18   /* Zone Group */
#define TLLCTT_CZGDES  19   /* Description */
#define TLLCTT_DEPT    20   /* Dept */
#define TLLCTT_DEPTNM  21   /* Name */
#define TLLCTT_DIFGID  22   /* Diff Group */
#define TLLCTT_DIFGRP  23   /* Description */
#define TLLCTT_RANGID  24   /* Diff Range */
#define TLLCTT_RANG    25   /* Description */
#define TLLCTT_RATID   26   /* Diff Ratio */
#define TLLCTT_RATDES  27   /* Description */
#define TLLCTT_DISTNM  28   /* Name */
#define TLLCTT_DIST    29   /* @OH5@ */
#define TLLCTT_DIV     30   /* @MH2@ */
#define TLLCTT_DIVNM   31   /* Name */
#define TLLCTT_GRPNO   32   /* Group */
#define TLLCTT_GRPNM   33   /* Name */
#define TLLCTT_IMGITM  34   /* Item */
#define TLLCTT_IMGNM   35   /* Image Name */
#define TLLCTT_IMG     36   /* Description */
#define TLLCTT_ITDESC  37   /* Description */
#define TLLCTT_ITEM    38   /* Item */
#define TLLCTT_IT2DES  39   /* Secondary Description */
#define TLLCTT_ITSDES  40   /* Short Description */
#define TLLCTT_ISITEM  41   /* Item */
#define TLLCTT_ISSUPP  42   /* Supplier */
#define TLLCTT_ISLABL  43   /* Supplier Label */
#define TLLCTT_ISDIF1  44   /* Diff 1 */
#define TLLCTT_ISDIF2  45   /* Diff 2 */
#define TLLCTT_ISDIF3  46   /* Diff 3 */
#define TLLCTT_ISDIF4  47   /* Diff 4 */
#define TLLCTT_ITXFID  48   /* Item Transform Id */
#define TLLCTT_ITXFRM  49   /* Description */
#define TLLCTT_LLSTID  50   /* Location List */
#define TLLCTT_LLIST   51   /* Description */
#define TLLCTT_PACKTI  52   /* Pack Template Id */
#define TLLCTT_PACKTD  53   /* Description */
#define TLLCTT_PTNR2N  54   /* Secondary Name */
#define TLLCTT_PTNRNM  55   /* Name */
#define TLLCTT_PTNRID  56   /* Partner */
#define TLLCTT_PTNRTY  57   /* Type */
#define TLLCTT_PMHNM   58   /* Name */
#define TLLCTT_PMHGP   59   /* Grandparent Id */
#define TLLCTT_PMHID   60   /* Pending Merch Id */
#define TLLCTT_PMHTYP  61   /* Hierarchy Type */
#define TLLCTT_PMHPID  62   /* Parent Id */
#define TLLCTT_CPNDES  63   /* Description */
#define TLLCTT_CPNID   64   /* Coupon */
#define TLLCTT_PGRPID  65   /* Priority Group */
#define TLLCTT_PGRP    66   /* Description */
#define TLLCTT_RCLS    67   /* Description */
#define TLLCTT_RCLSNO  68   /* Reclass number */
#define TLLCTT_REG     69   /* @OH4@ */
#define TLLCTT_REGNM   70   /* Name */
#define TLLCTT_RLTNID  71   /* Relationship Id */
#define TLLCTT_RLTNNM  72   /* Description */
#define TLLCTT_SLSTD   73   /* Description */
#define TLLCTT_SLST    74   /* Item List Id */
#define TLLCTT_STNM    75   /* Name */
#define TLLCTT_ST2NM   76   /* Secondary Name */
#define TLLCTT_ST      77   /* Store */
#define TLLCTT_STAD2N  78   /* Secondary Name */
#define TLLCTT_STADST  79   /* Store */
#define TLLCTT_STADNM  80   /* Name */
#define TLLCTT_SCDEPT  81   /* Dept */
#define TLLCTT_SCNM    82   /* Name */
#define TLLCTT_SUBCLS  83   /* @MH6@ */
#define TLLCTT_SCCLS   84   /* @MH5@ */
#define TLLCTT_SUP2NM  85   /* Secondary Name */
#define TLLCTT_SUPNM   86   /* Name */
#define TLLCTT_SUPP    87   /* Supplier */
#define TLLCTT_SUPCNM  88   /* Contact Name */
#define TLLCTT_SPTSUP  89   /* Supplier */
#define TLLCTT_SPTID   90   /* Pack Template Id */
#define TLLCTT_SPTDES  91   /* Description */
#define TLLCTT_UDAID   92   /* UDA Id */
#define TLLCTT_UDATXT  93   /* Text */
#define TLLCTT_UDAITM  94   /* Item */
#define TLLCTT_WCBDCI  95   /* Cost Component */
#define TLLCTT_WCBDTI  96   /* Template Id */
#define TLLCTT_WCBD    97   /* Description */
#define TLLCTT_WCBHTI  98   /* Template Id */
#define TLLCTT_WCBH    99   /* Description */
#define TLLCTT_WH     100   /* Warehouse */
#define TLLCTT_WHNM   101   /* Name */
#define TLLCTT_WH2NM  102   /* Secondary Name */
#define TLLCTT_UDATXD 103   /* Description */

#define _TLLCTT_MAX 103


/* TLLT = Translation code for default UI label to be used for base system of record RMS table for the entity. */
extern const char TLLT[NULL_CODE];

extern const char TLLT_ADDR  [NULL_CODE];   /* Addresses */
extern const char TLLT_AREA  [NULL_CODE];   /* @OH3@ */
extern const char TLLT_CHAIN [NULL_CODE];   /* @OH2@ */
extern const char TLLT_CLASS [NULL_CODE];   /* @MH5@ */
extern const char TLLT_CZ    [NULL_CODE];   /* Cost Zone */
extern const char TLLT_CZG   [NULL_CODE];   /* Cost Zone Group */
extern const char TLLT_DEPS  [NULL_CODE];   /* @MH4@ */
extern const char TLLT_DGH   [NULL_CODE];   /* Diff Group */
extern const char TLLT_DRGH  [NULL_CODE];   /* Diff Range */
extern const char TLLT_DRTH  [NULL_CODE];   /* Diff Ratio */
extern const char TLLT_DIST  [NULL_CODE];   /* @OH5@ */
extern const char TLLT_DIV   [NULL_CODE];   /* @MH2@ */
extern const char TLLT_GROUPS[NULL_CODE];   /* Groups */
extern const char TLLT_ITIMG [NULL_CODE];   /* Item Image */
extern const char TLLT_IM    [NULL_CODE];   /* Item Master */
extern const char TLLT_IS    [NULL_CODE];   /* Item Supplier */
extern const char TLLT_IXFROM[NULL_CODE];   /* Item Transform */
extern const char TLLT_LLH   [NULL_CODE];   /* Location List */
extern const char TLLT_PTH   [NULL_CODE];   /* Pack Template */
extern const char TLLT_PRTNR [NULL_CODE];   /* Partner */
extern const char TLLT_PENDMH[NULL_CODE];   /* Pending Merchandise Hierarchy */
extern const char TLLT_POSCPN[NULL_CODE];   /* POS Coupon */
extern const char TLLT_PRIGRP[NULL_CODE];   /* Priority Group */
extern const char TLLT_RECLS [NULL_CODE];   /* Reclass */
extern const char TLLT_REGION[NULL_CODE];   /* @OH4@ */
extern const char TLLT_RIH   [NULL_CODE];   /* Related Item */
extern const char TLLT_SKULST[NULL_CODE];   /* Item List */
extern const char TLLT_STORE [NULL_CODE];   /* Store */
extern const char TLLT_STRADD[NULL_CODE];   /* New Store */
extern const char TLLT_SBCLS [NULL_CODE];   /* @MH6@ */
extern const char TLLT_SUPS  [NULL_CODE];   /* Supplier */
extern const char TLLT_SPACKT[NULL_CODE];   /* Supplier Pack Template */
extern const char TLLT_UDAITF[NULL_CODE];   /* UDA Free Form */
extern const char TLLT_CSTMPD[NULL_CODE];   /* Cost Buildup Template Detail */
extern const char TLLT_CSTMPH[NULL_CODE];   /* Cost Buildup Template Header */
extern const char TLLT_WH    [NULL_CODE];   /* Warehouse */

#define TLLTTT_ADDR     1   /* Addresses */
#define TLLTTT_AREA     2   /* @OH3@ */
#define TLLTTT_CHAIN    3   /* @OH2@ */
#define TLLTTT_CLASS    4   /* @MH5@ */
#define TLLTTT_CZ       5   /* Cost Zone */
#define TLLTTT_CZG      6   /* Cost Zone Group */
#define TLLTTT_DEPS     7   /* @MH4@ */
#define TLLTTT_DGH      8   /* Diff Group */
#define TLLTTT_DRGH     9   /* Diff Range */
#define TLLTTT_DRTH    10   /* Diff Ratio */
#define TLLTTT_DIST    11   /* @OH5@ */
#define TLLTTT_DIV     12   /* @MH2@ */
#define TLLTTT_GROUPS  13   /* Groups */
#define TLLTTT_ITIMG   14   /* Item Image */
#define TLLTTT_IM      15   /* Item Master */
#define TLLTTT_IS      16   /* Item Supplier */
#define TLLTTT_IXFROM  17   /* Item Transform */
#define TLLTTT_LLH     18   /* Location List */
#define TLLTTT_PTH     19   /* Pack Template */
#define TLLTTT_PRTNR   20   /* Partner */
#define TLLTTT_PENDMH  21   /* Pending Merchandise Hierarchy */
#define TLLTTT_POSCPN  22   /* POS Coupon */
#define TLLTTT_PRIGRP  23   /* Priority Group */
#define TLLTTT_RECLS   24   /* Reclass */
#define TLLTTT_REGION  25   /* @OH4@ */
#define TLLTTT_RIH     26   /* Related Item */
#define TLLTTT_SKULST  27   /* Item List */
#define TLLTTT_STORE   28   /* Store */
#define TLLTTT_STRADD  29   /* New Store */
#define TLLTTT_SBCLS   30   /* @MH6@ */
#define TLLTTT_SUPS    31   /* Supplier */
#define TLLTTT_SPACKT  32   /* Supplier Pack Template */
#define TLLTTT_UDAITF  33   /* UDA Free Form */
#define TLLTTT_CSTMPD  34   /* Cost Buildup Template Detail */
#define TLLTTT_CSTMPH  35   /* Cost Buildup Template Header */
#define TLLTTT_WH      36   /* Warehouse */

#define _TLLTTT_MAX 36


/* TLOC = To Location */
extern const char TLOC[NULL_CODE];

extern const char TLOC_S     [NULL_CODE];   /* To St */
extern const char TLOC_W     [NULL_CODE];   /* To Wh */
extern const char TLOC_L     [NULL_CODE];   /* To Loc */

#define TLOCTT_S        1   /* To St */
#define TLOCTT_W        2   /* To Wh */
#define TLOCTT_L        3   /* To Loc */

#define _TLOCTT_MAX 3


/* TLVL = Transaction Level Descriptions */
extern const char TLVL[NULL_CODE];

extern const char TLVL_1     [NULL_CODE];   /* Level 1 */
extern const char TLVL_2     [NULL_CODE];   /* Level 2 */
extern const char TLVL_3     [NULL_CODE];   /* Level 3 */

#define TLVLTT_1        1   /* Level 1 */
#define TLVLTT_2        2   /* Level 2 */
#define TLVLTT_3        3   /* Level 3 */

#define _TLVLTT_MAX 3


/* TMLB = Timeline Base */
extern const char TMLB[NULL_CODE];


/* TMLN = Timeline Type Code */
extern const char TMLN[NULL_CODE];

extern const char TMLN_IT    [NULL_CODE];   /* Item */
extern const char TMLN_PO    [NULL_CODE];   /* Purchase Order */
extern const char TMLN_POIT  [NULL_CODE];   /* Order/Item */
extern const char TMLN_CE    [NULL_CODE];   /* Customs */
extern const char TMLN_TR    [NULL_CODE];   /* Transportation */
extern const char TMLN_TRPI  [NULL_CODE];   /* Transportation PO/Item */
extern const char TMLN_TRCO  [NULL_CODE];   /* Container */
extern const char TMLN_TRBL  [NULL_CODE];   /* BOL/AWB */
extern const char TMLN_TRCI  [NULL_CODE];   /* Commercial Invoice */
extern const char TMLN_TRPOBL[NULL_CODE];   /* PO-BOL/AWB */

#define TMLNTT_IT       1   /* Item */
#define TMLNTT_PO       2   /* Purchase Order */
#define TMLNTT_POIT     3   /* Order/Item */
#define TMLNTT_CE       4   /* Customs */
#define TMLNTT_TR       5   /* Transportation */
#define TMLNTT_TRPI     6   /* Transportation PO/Item */
#define TMLNTT_TRCO     7   /* Container */
#define TMLNTT_TRBL     8   /* BOL/AWB */
#define TMLNTT_TRCI     9   /* Commercial Invoice */
#define TMLNTT_TRPOBL  10   /* PO-BOL/AWB */

#define _TMLNTT_MAX 10


/* TMPL = Fashion Template Labels */
extern const char TMPL[NULL_CODE];

extern const char TMPL_S     [NULL_CODE];   /* Seq No */
extern const char TMPL_T     [NULL_CODE];   /* Pack Tmpl ID */

#define TMPLTT_S        1   /* Seq No */
#define TMPLTT_T        2   /* Pack Tmpl ID */

#define _TMPLTT_MAX 2


/* TMPT = Transformation Types */
extern const char TMPT[NULL_CODE];

extern const char TMPT_K     [NULL_CODE];   /* Break to Sell */
extern const char TMPT_O     [NULL_CODE];   /* Orderable */
extern const char TMPT_S     [NULL_CODE];   /* Sellable */

#define TMPTTT_K        1   /* Break to Sell */
#define TMPTTT_O        2   /* Orderable */
#define TMPTTT_S        3   /* Sellable */

#define _TMPTTT_MAX 3


/* TOLE = Tolerance Type */
extern const char TOLE[NULL_CODE];

extern const char TOLE_A     [NULL_CODE];   /* Actual */
extern const char TOLE_P     [NULL_CODE];   /* Percentage */

#define TOLETT_A       10   /* Actual */
#define TOLETT_P       20   /* Percentage */

#define _TOLETT_MAX 20


/* TOLT = Sales Audit Tolerance Types */
extern const char TOLT[NULL_CODE];

extern const char TOLT_OS    [NULL_CODE];   /* Over/Short Tolerance */
extern const char TOLT_DT    [NULL_CODE];   /* Date Range Tolerance */
extern const char TOLT_DV    [NULL_CODE];   /* Discount Value Tolerance */

#define TOLTTT_OS       1   /* Over/Short Tolerance */
#define TOLTTT_DT       2   /* Date Range Tolerance */
#define TOLTTT_DV       3   /* Discount Value Tolerance */

#define _TOLTTT_MAX 3


/* TOOP = Sales Audit Totals Operation */
extern const char TOOP[NULL_CODE];

extern const char TOOP_PLUS  [NULL_CODE];   /* + */
extern const char TOOP_MINUS [NULL_CODE];   /* - */

#define TOOPTT_PLUS     1   /* + */
#define TOOPTT_MINUS    2   /* - */

#define _TOOPTT_MAX 2


/* TOSC = Sales Audit Totals Sources */
extern const char TOSC[NULL_CODE];

extern const char TOSC_TRN   [NULL_CODE];   /* Transaction details */
extern const char TOSC_TOT   [NULL_CODE];   /* Total details */

#define TOSCTT_TRN      1   /* Transaction details */
#define TOSCTT_TOT      2   /* Total details */

#define _TOSCTT_MAX 2


/* TPA1 = Transfer Price Adjustment Types 1 */
extern const char TPA1[NULL_CODE];

extern const char TPA1_DP    [NULL_CODE];   /* Decrease by Percent */
extern const char TPA1_DA    [NULL_CODE];   /* Decrease by Amount */
extern const char TPA1_IP    [NULL_CODE];   /* Increase by Percent */
extern const char TPA1_IA    [NULL_CODE];   /* Increase by Amount */
extern const char TPA1_S     [NULL_CODE];   /* Set Price */

#define TPA1TT_DP       1   /* Decrease by Percent */
#define TPA1TT_DA       2   /* Decrease by Amount */
#define TPA1TT_IP       3   /* Increase by Percent */
#define TPA1TT_IA       4   /* Increase by Amount */
#define TPA1TT_S        5   /* Set Price */

#define _TPA1TT_MAX 5


/* TPA2 = Transfer Price Adjustment Types 2 */
extern const char TPA2[NULL_CODE];

extern const char TPA2_DP    [NULL_CODE];   /* Decrease by Percent */
extern const char TPA2_DA    [NULL_CODE];   /* Decrease by Amount */
extern const char TPA2_S     [NULL_CODE];   /* Set Price */

#define TPA2TT_DP       1   /* Decrease by Percent */
#define TPA2TT_DA       2   /* Decrease by Amount */
#define TPA2TT_S        3   /* Set Price */

#define _TPA2TT_MAX 3


/* TR1E = Tsf Type MTE Off MultiChan Off Edit */
extern const char TR1E[NULL_CODE];

extern const char TR1E_SR    [NULL_CODE];   /* Store Requisition */
extern const char TR1E_CO    [NULL_CODE];   /* Customer Order */
extern const char TR1E_RV    [NULL_CODE];   /* Return To Vendor */
extern const char TR1E_CF    [NULL_CODE];   /* Confirmation */
extern const char TR1E_AD    [NULL_CODE];   /* Administrative */
extern const char TR1E_MR    [NULL_CODE];   /* Manual Requisition */
extern const char TR1E_PL    [NULL_CODE];   /* PO-Linked Transfer */
extern const char TR1E_EG    [NULL_CODE];   /* Externally Generated */
extern const char TR1E_RAC   [NULL_CODE];   /* Reallocation Transfer */
extern const char TR1E_SIM   [NULL_CODE];   /* SIM Generated Transfer */
extern const char TR1E_AIP   [NULL_CODE];   /* AIP Generated Transfer */
extern const char TR1E_SG    [NULL_CODE];   /* System Generated Transfer */

#define TR1ETT_SR       1   /* Store Requisition */
#define TR1ETT_CO       2   /* Customer Order */
#define TR1ETT_RV       3   /* Return To Vendor */
#define TR1ETT_CF       4   /* Confirmation */
#define TR1ETT_AD       5   /* Administrative */
#define TR1ETT_MR       6   /* Manual Requisition */
#define TR1ETT_PL       7   /* PO-Linked Transfer */
#define TR1ETT_EG       8   /* Externally Generated */
#define TR1ETT_RAC      9   /* Reallocation Transfer */
#define TR1ETT_SIM     10   /* SIM Generated Transfer */
#define TR1ETT_AIP     11   /* AIP Generated Transfer */
#define TR1ETT_SG      12   /* System Generated Transfer */

#define _TR1ETT_MAX 12


/* TR1N = Tsf Type MTE Off MultiChan Off New */
extern const char TR1N[NULL_CODE];

extern const char TR1N_CO    [NULL_CODE];   /* Customer Order */
extern const char TR1N_RV    [NULL_CODE];   /* Return To Vendor */
extern const char TR1N_CF    [NULL_CODE];   /* Confirmation */
extern const char TR1N_AD    [NULL_CODE];   /* Administrative */
extern const char TR1N_MR    [NULL_CODE];   /* Manual Requisition */
extern const char TR1N_RAC   [NULL_CODE];   /* Reallocation Transfer */

#define TR1NTT_CO       1   /* Customer Order */
#define TR1NTT_RV       2   /* Return To Vendor */
#define TR1NTT_CF       3   /* Confirmation */
#define TR1NTT_AD       4   /* Administrative */
#define TR1NTT_MR       5   /* Manual Requisition */
#define TR1NTT_RAC      6   /* Reallocation Transfer */

#define _TR1NTT_MAX 6


/* TR2E = Tsf Type MTE Off MultiChan On Edit */
extern const char TR2E[NULL_CODE];

extern const char TR2E_AD    [NULL_CODE];   /* Administrative */
extern const char TR2E_AIP   [NULL_CODE];   /* AIP Generated Transfer */
extern const char TR2E_BT    [NULL_CODE];   /* Book Transfer */
extern const char TR2E_CF    [NULL_CODE];   /* Confirmation */
extern const char TR2E_CO    [NULL_CODE];   /* Customer Order */
extern const char TR2E_EG    [NULL_CODE];   /* Externally Generated */
extern const char TR2E_FO    [NULL_CODE];   /* @SUH4@ Order */
extern const char TR2E_FR    [NULL_CODE];   /* @SUH4@ Return */
extern const char TR2E_MR    [NULL_CODE];   /* Manual Requisition */
extern const char TR2E_PL    [NULL_CODE];   /* PO-Linked Transfer */
extern const char TR2E_RAC   [NULL_CODE];   /* Reallocation Transfer */
extern const char TR2E_RV    [NULL_CODE];   /* Return To Vendor */
extern const char TR2E_SG    [NULL_CODE];   /* System Generated Transfer */
extern const char TR2E_SIM   [NULL_CODE];   /* SIM Generated Transfer */
extern const char TR2E_SR    [NULL_CODE];   /* Store Requisition */

#define TR2ETT_AD       1   /* Administrative */
#define TR2ETT_AIP      2   /* AIP Generated Transfer */
#define TR2ETT_BT       3   /* Book Transfer */
#define TR2ETT_CF       4   /* Confirmation */
#define TR2ETT_CO       5   /* Customer Order */
#define TR2ETT_EG       6   /* Externally Generated */
#define TR2ETT_FO       7   /* @SUH4@ Order */
#define TR2ETT_FR       8   /* @SUH4@ Return */
#define TR2ETT_MR       9   /* Manual Requisition */
#define TR2ETT_PL      10   /* PO-Linked Transfer */
#define TR2ETT_RAC     11   /* Reallocation Transfer */
#define TR2ETT_RV      12   /* Return To Vendor */
#define TR2ETT_SG      13   /* System Generated Transfer */
#define TR2ETT_SIM     14   /* SIM Generated Transfer */
#define TR2ETT_SR      15   /* Store Requisition */

#define _TR2ETT_MAX 15


/* TR2N = Tsf Type MTE Off MultiChan On New */
extern const char TR2N[NULL_CODE];

extern const char TR2N_AD    [NULL_CODE];   /* Administrative */
extern const char TR2N_BT    [NULL_CODE];   /* Book Transfer */
extern const char TR2N_CF    [NULL_CODE];   /* Confirmation */
extern const char TR2N_CO    [NULL_CODE];   /* Customer Order */
extern const char TR2N_FO    [NULL_CODE];   /* @SUH4@ Order */
extern const char TR2N_FR    [NULL_CODE];   /* @SUH4@ Return */
extern const char TR2N_MR    [NULL_CODE];   /* Manual Requisition */
extern const char TR2N_RAC   [NULL_CODE];   /* Reallocation Transfer */
extern const char TR2N_RV    [NULL_CODE];   /* Return To Vendor */

#define TR2NTT_AD       1   /* Administrative */
#define TR2NTT_BT       2   /* Book Transfer */
#define TR2NTT_CF       3   /* Confirmation */
#define TR2NTT_CO       4   /* Customer Order */
#define TR2NTT_FO       5   /* @SUH4@ Order */
#define TR2NTT_FR       6   /* @SUH4@ Return */
#define TR2NTT_MR       7   /* Manual Requisition */
#define TR2NTT_RAC      8   /* Reallocation Transfer */
#define TR2NTT_RV       9   /* Return To Vendor */

#define _TR2NTT_MAX 9


/* TR3E = Tsf Type MTE On MultiChan Off Edit */
extern const char TR3E[NULL_CODE];

extern const char TR3E_SR    [NULL_CODE];   /* Store Requisition */
extern const char TR3E_CO    [NULL_CODE];   /* Customer Order */
extern const char TR3E_RV    [NULL_CODE];   /* Return To Vendor */
extern const char TR3E_CF    [NULL_CODE];   /* Confirmation */
extern const char TR3E_AD    [NULL_CODE];   /* Administrative */
extern const char TR3E_MR    [NULL_CODE];   /* Manual Requisition */
extern const char TR3E_IC    [NULL_CODE];   /* Intercompany */
extern const char TR3E_PL    [NULL_CODE];   /* PO-Linked Transfer */
extern const char TR3E_EG    [NULL_CODE];   /* Externally Generated */
extern const char TR3E_RAC   [NULL_CODE];   /* Reallocation Transfer */
extern const char TR3E_SIM   [NULL_CODE];   /* SIM Generated Transfer */
extern const char TR3E_AIP   [NULL_CODE];   /* AIP Generated Transfer */
extern const char TR3E_SG    [NULL_CODE];   /* System Generated Transfer */

#define TR3ETT_SR       1   /* Store Requisition */
#define TR3ETT_CO       2   /* Customer Order */
#define TR3ETT_RV       3   /* Return To Vendor */
#define TR3ETT_CF       4   /* Confirmation */
#define TR3ETT_AD       5   /* Administrative */
#define TR3ETT_MR       6   /* Manual Requisition */
#define TR3ETT_IC       7   /* Intercompany */
#define TR3ETT_PL       8   /* PO-Linked Transfer */
#define TR3ETT_EG       9   /* Externally Generated */
#define TR3ETT_RAC     10   /* Reallocation Transfer */
#define TR3ETT_SIM     11   /* SIM Generated Transfer */
#define TR3ETT_AIP     12   /* AIP Generated Transfer */
#define TR3ETT_SG      16   /* System Generated Transfer */

#define _TR3ETT_MAX 16


/* TR3N = Tsf Type MTE On MultiChan Off New */
extern const char TR3N[NULL_CODE];

extern const char TR3N_CO    [NULL_CODE];   /* Customer Order */
extern const char TR3N_RV    [NULL_CODE];   /* Return To Vendor */
extern const char TR3N_CF    [NULL_CODE];   /* Confirmation */
extern const char TR3N_AD    [NULL_CODE];   /* Administrative */
extern const char TR3N_MR    [NULL_CODE];   /* Manual Requisition */
extern const char TR3N_IC    [NULL_CODE];   /* Intercompany */
extern const char TR3N_RAC   [NULL_CODE];   /* Reallocation Transfer */

#define TR3NTT_CO       1   /* Customer Order */
#define TR3NTT_RV       2   /* Return To Vendor */
#define TR3NTT_CF       3   /* Confirmation */
#define TR3NTT_AD       4   /* Administrative */
#define TR3NTT_MR       5   /* Manual Requisition */
#define TR3NTT_IC       6   /* Intercompany */
#define TR3NTT_RAC      7   /* Reallocation Transfer */

#define _TR3NTT_MAX 7


/* TR4E = Tsf Type MTE On MultiChan On Edit */
extern const char TR4E[NULL_CODE];

extern const char TR4E_AD    [NULL_CODE];   /* Administrative */
extern const char TR4E_AIP   [NULL_CODE];   /* AIP Generated Transfer */
extern const char TR4E_BT    [NULL_CODE];   /* Book Transfer */
extern const char TR4E_CF    [NULL_CODE];   /* Confirmation */
extern const char TR4E_CO    [NULL_CODE];   /* Customer Order */
extern const char TR4E_EG    [NULL_CODE];   /* Externally Generated */
extern const char TR4E_FO    [NULL_CODE];   /* @SUH4@ Order */
extern const char TR4E_FR    [NULL_CODE];   /* @SUH4@ Return */
extern const char TR4E_IC    [NULL_CODE];   /* Intercompany */
extern const char TR4E_MR    [NULL_CODE];   /* Manual Requisition */
extern const char TR4E_PL    [NULL_CODE];   /* PO-Linked Transfer */
extern const char TR4E_RAC   [NULL_CODE];   /* Reallocation Transfer */
extern const char TR4E_RV    [NULL_CODE];   /* Return To Vendor */
extern const char TR4E_SG    [NULL_CODE];   /* System Generated Transfer */
extern const char TR4E_SIM   [NULL_CODE];   /* SIM Generated Transfer */
extern const char TR4E_SR    [NULL_CODE];   /* Store Requisition */

#define TR4ETT_AD       1   /* Administrative */
#define TR4ETT_AIP      2   /* AIP Generated Transfer */
#define TR4ETT_BT       3   /* Book Transfer */
#define TR4ETT_CF       4   /* Confirmation */
#define TR4ETT_CO       5   /* Customer Order */
#define TR4ETT_EG       6   /* Externally Generated */
#define TR4ETT_FO       7   /* @SUH4@ Order */
#define TR4ETT_FR       8   /* @SUH4@ Return */
#define TR4ETT_IC       9   /* Intercompany */
#define TR4ETT_MR      10   /* Manual Requisition */
#define TR4ETT_PL      11   /* PO-Linked Transfer */
#define TR4ETT_RAC     12   /* Reallocation Transfer */
#define TR4ETT_RV      13   /* Return To Vendor */
#define TR4ETT_SG      14   /* System Generated Transfer */
#define TR4ETT_SIM     15   /* SIM Generated Transfer */
#define TR4ETT_SR      16   /* Store Requisition */

#define _TR4ETT_MAX 16


/* TR4N = Tsf Type MTE On MultiChan On New */
extern const char TR4N[NULL_CODE];

extern const char TR4N_AD    [NULL_CODE];   /* Administrative */
extern const char TR4N_BT    [NULL_CODE];   /* Book Transfer */
extern const char TR4N_CF    [NULL_CODE];   /* Confirmation */
extern const char TR4N_CO    [NULL_CODE];   /* Customer Order */
extern const char TR4N_FO    [NULL_CODE];   /* @SUH4@ Order */
extern const char TR4N_FR    [NULL_CODE];   /* @SUH4@ Return */
extern const char TR4N_IC    [NULL_CODE];   /* Intercompany */
extern const char TR4N_MR    [NULL_CODE];   /* Manual Requisition */
extern const char TR4N_RAC   [NULL_CODE];   /* Reallocation Transfer */
extern const char TR4N_RV    [NULL_CODE];   /* Return To Vendor */

#define TR4NTT_AD       1   /* Administrative */
#define TR4NTT_BT       2   /* Book Transfer */
#define TR4NTT_CF       3   /* Confirmation */
#define TR4NTT_CO       4   /* Customer Order */
#define TR4NTT_FO       5   /* @SUH4@ Order */
#define TR4NTT_FR       6   /* @SUH4@ Return */
#define TR4NTT_IC       7   /* Intercompany */
#define TR4NTT_MR       8   /* Manual Requisition */
#define TR4NTT_RAC      9   /* Reallocation Transfer */
#define TR4NTT_RV      10   /* Return To Vendor */

#define _TR4NTT_MAX 10


/* TRA1 = Transfer Allocation Types */
extern const char TRA1[NULL_CODE];

extern const char TRA1_SR    [NULL_CODE];   /* Stock Requisition */
extern const char TRA1_PO    [NULL_CODE];   /* Cross-Dock PO */
extern const char TRA1_A     [NULL_CODE];   /* All */

#define TRA1TT_SR       1   /* Stock Requisition */
#define TRA1TT_PO       2   /* Cross-Dock PO */
#define TRA1TT_A        3   /* All */

#define _TRA1TT_MAX 3


/* TRA2 = Transfer Allocation Types (incl. All) */
extern const char TRA2[NULL_CODE];

extern const char TRA2_SR    [NULL_CODE];   /* Stock Requisition */
extern const char TRA2_PO    [NULL_CODE];   /* Cross-Dock PO */

#define TRA2TT_SR       1   /* Stock Requisition */
#define TRA2TT_PO       2   /* Cross-Dock PO */

#define _TRA2TT_MAX 2


/* TRAC = Transfer Action List */
extern const char TRAC[NULL_CODE];

extern const char TRAC_NEW   [NULL_CODE];   /* New */
extern const char TRAC_NEWE  [NULL_CODE];   /* New from Existing */
extern const char TRAC_VIEW  [NULL_CODE];   /* View */
extern const char TRAC_EDIT  [NULL_CODE];   /* Edit */

#define TRACTT_NEW      1   /* New */
#define TRACTT_NEWE     2   /* New from Existing */
#define TRACTT_VIEW     3   /* View */
#define TRACTT_EDIT     4   /* Edit */

#define _TRACTT_MAX 4


/* TRAF = Traffic Conditions */
extern const char TRAF[NULL_CODE];

extern const char TRAF_HIG   [NULL_CODE];   /* High */
extern const char TRAF_MED   [NULL_CODE];   /* Medium */
extern const char TRAF_LOW   [NULL_CODE];   /* Low */

#define TRAFTT_HIG      1   /* High */
#define TRAFTT_MED      2   /* Medium */
#define TRAFTT_LOW      3   /* Low */

#define _TRAFTT_MAX 3


/* TRAI = Sales Audit Sub-transaction Indicator */
extern const char TRAI[NULL_CODE];

extern const char TRAI_ACC   [NULL_CODE];   /* Accomodation */
extern const char TRAI_DIS   [NULL_CODE];   /* Disposed */

#define TRAITT_ACC      1   /* Accomodation */
#define TRAITT_DIS      2   /* Disposed */

#define _TRAITT_MAX 2


/* TRAS = Sales Audit Sub-transaction Types */
extern const char TRAS[NULL_CODE];

extern const char TRAS_ERR   [NULL_CODE];   /* Error */
extern const char TRAS_EMP   [NULL_CODE];   /* Employee */
extern const char TRAS_DRIVEO[NULL_CODE];   /* Fuel Drive Off */
extern const char TRAS_MV    [NULL_CODE];   /* Merchandise Vendor Payout */
extern const char TRAS_EV    [NULL_CODE];   /* Expense Vendor Payout */
extern const char TRAS_EXCH  [NULL_CODE];   /* Exchange in Item Sale/Return trans */
extern const char TRAS_CACCOM[NULL_CODE];   /* Credit Accommodation */
extern const char TRAS_CSTGEN[NULL_CODE];   /* General Accommodation */
extern const char TRAS_CSTTAX[NULL_CODE];   /* Tax Accommodation */
extern const char TRAS_DRET  [NULL_CODE];   /* Disposed Return */
extern const char TRAS_DEXCHI[NULL_CODE];   /* Disposed Exchange */
extern const char TRAS_SALE  [NULL_CODE];   /* Sale */
extern const char TRAS_RETURN[NULL_CODE];   /* Return */
extern const char TRAS_VOID  [NULL_CODE];   /* Void */
extern const char TRAS_NOSALE[NULL_CODE];   /* No Sale */
extern const char TRAS_OSTORE[NULL_CODE];   /* Open Store */
extern const char TRAS_CSTORE[NULL_CODE];   /* Close Store */
extern const char TRAS_DSTORE[NULL_CODE];   /* Close Store */
extern const char TRAS_OREG  [NULL_CODE];   /* Open Register */
extern const char TRAS_CREG  [NULL_CODE];   /* Close Register */
extern const char TRAS_CRGRC [NULL_CODE];   /* Close Register */
extern const char TRAS_OTILL [NULL_CODE];   /* Open Till */
extern const char TRAS_CTILL [NULL_CODE];   /* Close Till */
extern const char TRAS_CTILLT[NULL_CODE];   /* Close Till (Total) */
extern const char TRAS_LOTILL[NULL_CODE];   /* Loan Till */
extern const char TRAS_PUTILL[NULL_CODE];   /* Pickup Till */
extern const char TRAS_STILL [NULL_CODE];   /* Suspend Till */
extern const char TRAS_RTILL [NULL_CODE];   /* Resume Till */
extern const char TRAS_PITILL[NULL_CODE];   /* Payin Till */
extern const char TRAS_POTILL[NULL_CODE];   /* Payout Till */
extern const char TRAS_HOUSE [NULL_CODE];   /* House Payment */
extern const char TRAS_LAYINT[NULL_CODE];   /* Initiate Layaway */
extern const char TRAS_LAYCMP[NULL_CODE];   /* Layaway Complete */
extern const char TRAS_LAYPAY[NULL_CODE];   /* Layaway Payment */
extern const char TRAS_LAYDEL[NULL_CODE];   /* Layaway Delete */
extern const char TRAS_ORDINT[NULL_CODE];   /* Initiate Order */
extern const char TRAS_ORDCMP[NULL_CODE];   /* Order Complete */
extern const char TRAS_ORDCAN[NULL_CODE];   /* Order Cancel */
extern const char TRAS_ORDPAR[NULL_CODE];   /* Order Partial */
extern const char TRAS_BANK  [NULL_CODE];   /* Bank Deposit Store */
extern const char TRAS_INSCRE[NULL_CODE];   /* Instant Credit Enrollment */
extern const char TRAS_REDEEM[NULL_CODE];   /* Redeem */
extern const char TRAS_ETRAIN[NULL_CODE];   /* Enter Training Mode */
extern const char TRAS_XTRAIN[NULL_CODE];   /* Exit Training Mode */
extern const char TRAS_SEND  [NULL_CODE];   /* Send */
extern const char TRAS_PAYOUT[NULL_CODE];   /* Payroll Payout */
extern const char TRAS_NTRENT[NULL_CODE];   /* Enter Transaction Reentry */
extern const char TRAS_XTRENT[NULL_CODE];   /* Exit Transaction Reentry */
extern const char TRAS_CANCEL[NULL_CODE];   /* Canceled Transaction */
extern const char TRAS_TRAIN [NULL_CODE];   /* Training Mode Transaction */
extern const char TRAS_SUSPND[NULL_CODE];   /* Suspend Transaction */
extern const char TRAS_SSPNDR[NULL_CODE];   /* Resume Suspended Transaction */
extern const char TRAS_SSPNDC[NULL_CODE];   /* Cancelled Suspended Transaction */
extern const char TRAS_BILLPY[NULL_CODE];   /* Telephone - Mobile Bill Payment */
extern const char TRAS_STCHG [NULL_CODE];   /* Transaction Status Change */
extern const char TRAS_AUDIT [NULL_CODE];   /* Till Audit */
extern const char TRAS_GRGRC [NULL_CODE];   /* Workstation Close */
extern const char TRAS_OTHER [NULL_CODE];   /* Miscellaneous */
extern const char TRAS_STRBNK[NULL_CODE];   /* Currency Deposit Into Store Bank */
extern const char TRAS_OTILLT[NULL_CODE];   /* Open Till (Total) */

#define TRASTT_ERR      1   /* Error */
#define TRASTT_EMP      2   /* Employee */
#define TRASTT_DRIVEO   3   /* Fuel Drive Off */
#define TRASTT_MV       4   /* Merchandise Vendor Payout */
#define TRASTT_EV       5   /* Expense Vendor Payout */
#define TRASTT_EXCH     6   /* Exchange in Item Sale/Return trans */
#define TRASTT_CACCOM   7   /* Credit Accommodation */
#define TRASTT_CSTGEN   8   /* General Accommodation */
#define TRASTT_CSTTAX   9   /* Tax Accommodation */
#define TRASTT_DRET    10   /* Disposed Return */
#define TRASTT_DEXCHI  11   /* Disposed Exchange */
#define TRASTT_SALE    12   /* Sale */
#define TRASTT_RETURN  13   /* Return */
#define TRASTT_VOID    14   /* Void */
#define TRASTT_NOSALE  15   /* No Sale */
#define TRASTT_OSTORE  16   /* Open Store */
#define TRASTT_CSTORE  17   /* Close Store */
#define TRASTT_DSTORE  18   /* Close Store */
#define TRASTT_OREG    19   /* Open Register */
#define TRASTT_CREG    20   /* Close Register */
#define TRASTT_CRGRC   21   /* Close Register */
#define TRASTT_OTILL   22   /* Open Till */
#define TRASTT_CTILL   23   /* Close Till */
#define TRASTT_CTILLT  24   /* Close Till (Total) */
#define TRASTT_LOTILL  25   /* Loan Till */
#define TRASTT_PUTILL  26   /* Pickup Till */
#define TRASTT_STILL   27   /* Suspend Till */
#define TRASTT_RTILL   28   /* Resume Till */
#define TRASTT_PITILL  29   /* Payin Till */
#define TRASTT_POTILL  30   /* Payout Till */
#define TRASTT_HOUSE   31   /* House Payment */
#define TRASTT_LAYINT  32   /* Initiate Layaway */
#define TRASTT_LAYCMP  33   /* Layaway Complete */
#define TRASTT_LAYPAY  34   /* Layaway Payment */
#define TRASTT_LAYDEL  35   /* Layaway Delete */
#define TRASTT_ORDINT  36   /* Initiate Order */
#define TRASTT_ORDCMP  37   /* Order Complete */
#define TRASTT_ORDCAN  38   /* Order Cancel */
#define TRASTT_ORDPAR  39   /* Order Partial */
#define TRASTT_BANK    40   /* Bank Deposit Store */
#define TRASTT_INSCRE  41   /* Instant Credit Enrollment */
#define TRASTT_REDEEM  42   /* Redeem */
#define TRASTT_ETRAIN  43   /* Enter Training Mode */
#define TRASTT_XTRAIN  44   /* Exit Training Mode */
#define TRASTT_SEND    45   /* Send */
#define TRASTT_PAYOUT  46   /* Payroll Payout */
#define TRASTT_NTRENT  47   /* Enter Transaction Reentry */
#define TRASTT_XTRENT  48   /* Exit Transaction Reentry */
#define TRASTT_CANCEL  49   /* Canceled Transaction */
#define TRASTT_TRAIN   50   /* Training Mode Transaction */
#define TRASTT_SUSPND  51   /* Suspend Transaction */
#define TRASTT_SSPNDR  52   /* Resume Suspended Transaction */
#define TRASTT_SSPNDC  53   /* Cancelled Suspended Transaction */
#define TRASTT_BILLPY  54   /* Telephone - Mobile Bill Payment */
#define TRASTT_STCHG   55   /* Transaction Status Change */
#define TRASTT_AUDIT   56   /* Till Audit */
#define TRASTT_GRGRC   57   /* Workstation Close */
#define TRASTT_OTHER   58   /* Miscellaneous */
#define TRASTT_STRBNK  59   /* Currency Deposit Into Store Bank */
#define TRASTT_OTILLT  60   /* Open Till (Total) */

#define _TRASTT_MAX 60


/* TRAT = Sales Audit Transaction Types */
extern const char TRAT[NULL_CODE];

extern const char TRAT_ERR   [NULL_CODE];   /* Error */
extern const char TRAT_TERM  [NULL_CODE];   /* Termination Record */
extern const char TRAT_DCLOSE[NULL_CODE];   /* Day has been closed at the store */
extern const char TRAT_OPEN  [NULL_CODE];   /* Open */
extern const char TRAT_NOSALE[NULL_CODE];   /* No Sale */
extern const char TRAT_VOID  [NULL_CODE];   /* Void */
extern const char TRAT_PVOID [NULL_CODE];   /* Post Void */
extern const char TRAT_SALE  [NULL_CODE];   /* Sale */
extern const char TRAT_SPLORD[NULL_CODE];   /* Special Order */
extern const char TRAT_RETURN[NULL_CODE];   /* Return */
extern const char TRAT_PAIDIN[NULL_CODE];   /* Paid In */
extern const char TRAT_PAIDOU[NULL_CODE];   /* Paid Out */
extern const char TRAT_PULL  [NULL_CODE];   /* Pull */
extern const char TRAT_LOAN  [NULL_CODE];   /* Loan */
extern const char TRAT_COND  [NULL_CODE];   /* Daily Store Conditions */
extern const char TRAT_CLOSE [NULL_CODE];   /* Close */
extern const char TRAT_REFUND[NULL_CODE];   /* Return of customer's original check */
extern const char TRAT_TOTAL [NULL_CODE];   /* RTLOG declared totals */
extern const char TRAT_METER [NULL_CODE];   /* Meter Reading for Fuel */
extern const char TRAT_PUMPT [NULL_CODE];   /* Pump Test for Fuel */
extern const char TRAT_TANKDP[NULL_CODE];   /* Tank Dip */
extern const char TRAT_EEXCH [NULL_CODE];   /* Even Exchange */
extern const char TRAT_OTHER [NULL_CODE];   /* Other */

#define TRATTT_ERR      1   /* Error */
#define TRATTT_TERM     2   /* Termination Record */
#define TRATTT_DCLOSE   3   /* Day has been closed at the store */
#define TRATTT_OPEN     4   /* Open */
#define TRATTT_NOSALE   5   /* No Sale */
#define TRATTT_VOID     6   /* Void */
#define TRATTT_PVOID    7   /* Post Void */
#define TRATTT_SALE     8   /* Sale */
#define TRATTT_SPLORD   9   /* Special Order */
#define TRATTT_RETURN  10   /* Return */
#define TRATTT_PAIDIN  11   /* Paid In */
#define TRATTT_PAIDOU  12   /* Paid Out */
#define TRATTT_PULL    13   /* Pull */
#define TRATTT_LOAN    14   /* Loan */
#define TRATTT_COND    15   /* Daily Store Conditions */
#define TRATTT_CLOSE   16   /* Close */
#define TRATTT_REFUND  17   /* Return of customer's original check */
#define TRATTT_TOTAL   18   /* RTLOG declared totals */
#define TRATTT_METER   19   /* Meter Reading for Fuel */
#define TRATTT_PUMPT   20   /* Pump Test for Fuel */
#define TRATTT_TANKDP  21   /* Tank Dip */
#define TRATTT_EEXCH   22   /* Even Exchange */
#define TRATTT_OTHER   23   /* Other */

#define _TRATTT_MAX 23


/* TRCO = Transportation Conditions */
extern const char TRCO[NULL_CODE];

extern const char TRCO_D     [NULL_CODE];   /* Delivered */
extern const char TRCO_L     [NULL_CODE];   /* Loaded */
extern const char TRCO_S     [NULL_CODE];   /* Sailed */
extern const char TRCO_F     [NULL_CODE];   /* Finalized */

#define TRCOTT_D        2   /* Delivered */
#define TRCOTT_L        3   /* Loaded */
#define TRCOTT_S        4   /* Sailed */
#define TRCOTT_F        5   /* Finalized */

#define _TRCOTT_MAX 5


/* TRFC = Freight Type */
extern const char TRFC[NULL_CODE];

extern const char TRFC_N     [NULL_CODE];   /* Normal */
extern const char TRFC_E     [NULL_CODE];   /* Expedite */
extern const char TRFC_H     [NULL_CODE];   /* Hold */

#define TRFCTT_N        1   /* Normal */
#define TRFCTT_E        2   /* Expedite */
#define TRFCTT_H        3   /* Hold */

#define _TRFCTT_MAX 3


/* TRFL = Totals Reference Labels */
extern const char TRFL[NULL_CODE];

extern const char TRFL_1     [NULL_CODE];   /* Ref. No. 1 */
extern const char TRFL_2     [NULL_CODE];   /* Ref. No. 2 */
extern const char TRFL_3     [NULL_CODE];   /* Ref. No. 3 */
extern const char TRFL_D     [NULL_CODE];   /* Dept. */
extern const char TRFL_P     [NULL_CODE];   /* Pump */

#define TRFLTT_1        1   /* Ref. No. 1 */
#define TRFLTT_2        2   /* Ref. No. 2 */
#define TRFLTT_3        3   /* Ref. No. 3 */
#define TRFLTT_D        4   /* Dept. */
#define TRFLTT_P        5   /* Pump */

#define _TRFLTT_MAX 5


/* TRLB = Transfer Label */
extern const char TRLB[NULL_CODE];

extern const char TRLB_T     [NULL_CODE];   /* Transfer */
extern const char TRLB_A     [NULL_CODE];   /* Allocation */

#define TRLBTT_T        1   /* Transfer */
#define TRLBTT_A        2   /* Allocation */

#define _TRLBTT_MAX 2


/* TRMF = Transportation Modes (Find Form) */
extern const char TRMF[NULL_CODE];

extern const char TRMF_NEW   [NULL_CODE];   /* New */
extern const char TRMF_NEWE  [NULL_CODE];   /* Create From Existing */
extern const char TRMF_EDIT  [NULL_CODE];   /* Edit */
extern const char TRMF_VIEW  [NULL_CODE];   /* View */
extern const char TRMF_L     [NULL_CODE];   /* License */
extern const char TRMF_V     [NULL_CODE];   /* Visa */
extern const char TRMF_VVE   [NULL_CODE];   /* Vessel/Voyage/ETD */
extern const char TRMF_BL    [NULL_CODE];   /* BOL/AWB */
extern const char TRMF_CI    [NULL_CODE];   /* Invoice No. */
extern const char TRMF_CO    [NULL_CODE];   /* Container */
extern const char TRMF_POIT  [NULL_CODE];   /* Order No./Item */

#define TRMFTT_NEW      1   /* New */
#define TRMFTT_NEWE     2   /* Create From Existing */
#define TRMFTT_EDIT     3   /* Edit */
#define TRMFTT_VIEW     4   /* View */
#define TRMFTT_L        5   /* License */
#define TRMFTT_V        6   /* Visa */
#define TRMFTT_VVE      7   /* Vessel/Voyage/ETD */
#define TRMFTT_BL       8   /* BOL/AWB */
#define TRMFTT_CI       9   /* Invoice No. */
#define TRMFTT_CO      10   /* Container */
#define TRMFTT_POIT    11   /* Order No./Item */

#define _TRMFTT_MAX 11


/* TRMO = Transportation Mode */
extern const char TRMO[NULL_CODE];

extern const char TRMO_10    [NULL_CODE];   /* Vessel, Non-container */
extern const char TRMO_11    [NULL_CODE];   /* Vessel, Container */
extern const char TRMO_12    [NULL_CODE];   /* Border Water-borne (Mexico and Canada) */
extern const char TRMO_20    [NULL_CODE];   /* Rail, Non-container */
extern const char TRMO_21    [NULL_CODE];   /* Rail, Container */
extern const char TRMO_30    [NULL_CODE];   /* TRUCK, Non-container */
extern const char TRMO_31    [NULL_CODE];   /* TRUCK, Container */
extern const char TRMO_32    [NULL_CODE];   /* Auto */
extern const char TRMO_33    [NULL_CODE];   /* Pedestrian */
extern const char TRMO_34    [NULL_CODE];   /* Road,other,Includes foot and animalborne */
extern const char TRMO_40    [NULL_CODE];   /* Air, Non-container */
extern const char TRMO_41    [NULL_CODE];   /* Air, Container */
extern const char TRMO_50    [NULL_CODE];   /* Mail */
extern const char TRMO_60    [NULL_CODE];   /* Passenger, hand-carried */
extern const char TRMO_70    [NULL_CODE];   /* Fixed Tran Installation */
extern const char TRMO_80    [NULL_CODE];   /* Not used at this time */

#define TRMOTT_10       1   /* Vessel, Non-container */
#define TRMOTT_11       2   /* Vessel, Container */
#define TRMOTT_12       3   /* Border Water-borne (Mexico and Canada) */
#define TRMOTT_20       4   /* Rail, Non-container */
#define TRMOTT_21       5   /* Rail, Container */
#define TRMOTT_30       6   /* TRUCK, Non-container */
#define TRMOTT_31       7   /* TRUCK, Container */
#define TRMOTT_32       8   /* Auto */
#define TRMOTT_33       9   /* Pedestrian */
#define TRMOTT_34      10   /* Road,other,Includes foot and animalborne */
#define TRMOTT_40      11   /* Air, Non-container */
#define TRMOTT_41      12   /* Air, Container */
#define TRMOTT_50      13   /* Mail */
#define TRMOTT_60      14   /* Passenger, hand-carried */
#define TRMOTT_70      15   /* Fixed Tran Installation */
#define TRMOTT_80      16   /* Not used at this time */

#define _TRMOTT_MAX 16


/* TRNV = Transaction Maintenance Navigation Type */
extern const char TRNV[NULL_CODE];

extern const char TRNV_R     [NULL_CODE];   /* Next Transaction Sequence Number */
extern const char TRNV_P     [NULL_CODE];   /* Register Transaction */

#define TRNVTT_R        1   /* Next Transaction Sequence Number */
#define TRNVTT_P        2   /* Register Transaction */

#define _TRNVTT_MAX 2


/* TRRC = Routing Type */
extern const char TRRC[NULL_CODE];

extern const char TRRC_1     [NULL_CODE];   /* Federal Express */
extern const char TRRC_2     [NULL_CODE];   /* UPS */
extern const char TRRC_3     [NULL_CODE];   /* Mail */

#define TRRCTT_1        1   /* Federal Express */
#define TRRCTT_2        2   /* UPS */
#define TRRCTT_3        3   /* Mail */

#define _TRRCTT_MAX 3


/* TRS1 = MLE Transfer Overall Status */
extern const char TRS1[NULL_CODE];

extern const char TRS1_I     [NULL_CODE];   /* Input */
extern const char TRS1_B     [NULL_CODE];   /* Submitted */
extern const char TRS1_A     [NULL_CODE];   /* Approved */
extern const char TRS1_C     [NULL_CODE];   /* Closed */
extern const char TRS1_D     [NULL_CODE];   /* Deleted */
extern const char TRS1_N     [NULL_CODE];   /* In Progress */

#define TRS1TT_I        1   /* Input */
#define TRS1TT_B        2   /* Submitted */
#define TRS1TT_A        3   /* Approved */
#define TRS1TT_C        4   /* Closed */
#define TRS1TT_D        5   /* Deleted */
#define TRS1TT_N        6   /* In Progress */

#define _TRS1TT_MAX 6


/* TRS2 = MLE Transfer Leg Status */
extern const char TRS2[NULL_CODE];

extern const char TRS2_I     [NULL_CODE];   /* Input */
extern const char TRS2_B     [NULL_CODE];   /* Submitted */
extern const char TRS2_A     [NULL_CODE];   /* Approved */
extern const char TRS2_S     [NULL_CODE];   /* Shipped */
extern const char TRS2_C     [NULL_CODE];   /* Closed */
extern const char TRS2_D     [NULL_CODE];   /* Deleted */
extern const char TRS2_P     [NULL_CODE];   /* Picked */
extern const char TRS2_L     [NULL_CODE];   /* Selected */

#define TRS2TT_I        1   /* Input */
#define TRS2TT_B        2   /* Submitted */
#define TRS2TT_A        3   /* Approved */
#define TRS2TT_S        4   /* Shipped */
#define TRS2TT_C        5   /* Closed */
#define TRS2TT_D        6   /* Deleted */
#define TRS2TT_P        7   /* Picked */
#define TRS2TT_L        8   /* Selected */

#define _TRS2TT_MAX 8


/* TRSP = Truck Splitting Method */
extern const char TRSP[NULL_CODE];

extern const char TRSP_B     [NULL_CODE];   /* Balanced Assortment */
extern const char TRSP_I     [NULL_CODE];   /* Item Sequence */

#define TRSPTT_B        1   /* Balanced Assortment */
#define TRSPTT_I        2   /* Item Sequence */

#define _TRSPTT_MAX 2


/* TRST = Transfer Status */
extern const char TRST[NULL_CODE];

extern const char TRST_I     [NULL_CODE];   /* Input */
extern const char TRST_B     [NULL_CODE];   /* Submitted */
extern const char TRST_A     [NULL_CODE];   /* Approved */
extern const char TRST_S     [NULL_CODE];   /* Shipped */
extern const char TRST_C     [NULL_CODE];   /* Closed */
extern const char TRST_D     [NULL_CODE];   /* Deleted */
extern const char TRST_P     [NULL_CODE];   /* Picked */
extern const char TRST_N     [NULL_CODE];   /* In Progress */

#define TRSTTT_I        1   /* Input */
#define TRSTTT_B        2   /* Submitted */
#define TRSTTT_A        3   /* Approved */
#define TRSTTT_S        4   /* Shipped */
#define TRSTTT_C        5   /* Closed */
#define TRSTTT_D        6   /* Deleted */
#define TRSTTT_P        7   /* Picked */
#define TRSTTT_N        8   /* In Progress */

#define _TRSTTT_MAX 8


/* TRTO = Transportation Types */
extern const char TRTO[NULL_CODE];

extern const char TRTO_VVE   [NULL_CODE];   /* Vessel/Voyage/ETD */
extern const char TRTO_BL    [NULL_CODE];   /* BOL/AWB */
extern const char TRTO_CI    [NULL_CODE];   /* Invoice No. */
extern const char TRTO_CO    [NULL_CODE];   /* Container */
extern const char TRTO_POIT  [NULL_CODE];   /* Order No./Item */

#define TRTOTT_VVE      1   /* Vessel/Voyage/ETD */
#define TRTOTT_BL       2   /* BOL/AWB */
#define TRTOTT_CI       3   /* Invoice No. */
#define TRTOTT_CO       4   /* Container */
#define TRTOTT_POIT     5   /* Order No./Item */

#define _TRTOTT_MAX 5


/* TRTP = Tax Rate Type */
extern const char TRTP[NULL_CODE];

extern const char TRTP_P     [NULL_CODE];   /* Percentage */
extern const char TRTP_S     [NULL_CODE];   /* Specific */
extern const char TRTP_V     [NULL_CODE];   /* Value */
extern const char TRTP_A     [NULL_CODE];   /* Amount */

#define TRTPTT_P        1   /* Percentage */
#define TRTPTT_S        2   /* Specific */
#define TRTPTT_V        3   /* Value */
#define TRTPTT_A        4   /* Amount */

#define _TRTPTT_MAX 4


/* TRTS = Transfer Receipt Treatments */
extern const char TRTS[NULL_CODE];

extern const char TRTS_BL    [NULL_CODE];   /* Both Locations */
extern const char TRTS_NL    [NULL_CODE];   /* No Loss */
extern const char TRTS_RL    [NULL_CODE];   /* Receiving Location */
extern const char TRTS_SL    [NULL_CODE];   /* Sending Location */

#define TRTSTT_BL       1   /* Both Locations */
#define TRTSTT_NL       2   /* No Loss */
#define TRTSTT_RL       3   /* Receiving Location */
#define TRTSTT_SL       4   /* Sending Location */

#define _TRTSTT_MAX 4


/* TRTY = Transportation Shipment Types */
extern const char TRTY[NULL_CODE];

extern const char TRTY_DESH  [NULL_CODE];   /* Deconsolidated Shipment */
extern const char TRTY_DISH  [NULL_CODE];   /* Direct Shipment */

#define TRTYTT_DESH     1   /* Deconsolidated Shipment */
#define TRTYTT_DISH     2   /* Direct Shipment */

#define _TRTYTT_MAX 2


/* TSFB = Transfer Basis */
extern const char TSFB[NULL_CODE];

extern const char TSFB_T     [NULL_CODE];   /* Transfer Entity */
extern const char TSFB_B     [NULL_CODE];   /* Set of Books */

#define TSFBTT_T        1   /* Transfer Entity */
#define TSFBTT_B        2   /* Set of Books */

#define _TSFBTT_MAX 2


/* TSFC = Transfer Codes */
extern const char TSFC[NULL_CODE];

extern const char TSFC_C     [NULL_CODE];   /* Carrier */
extern const char TSFC_RT    [NULL_CODE];   /* Routing Type */

#define TSFCTT_C        1   /* Carrier */
#define TSFCTT_RT       2   /* Routing Type */

#define _TSFCTT_MAX 2


/* TSFL = Tsf Leg */
extern const char TSFL[NULL_CODE];

extern const char TSFL_F     [NULL_CODE];   /* 1st Leg */
extern const char TSFL_S     [NULL_CODE];   /* 2nd Leg */

#define TSFLTT_F        1   /* 1st Leg */
#define TSFLTT_S        2   /* 2nd Leg */

#define _TSFLTT_MAX 2


/* TSFO = Shipment Type (Transfer or Order) */
extern const char TSFO[NULL_CODE];

extern const char TSFO_T     [NULL_CODE];   /* Transfer */
extern const char TSFO_O     [NULL_CODE];   /* Order */
extern const char TSFO_OS    [NULL_CODE];   /* Ord */
extern const char TSFO_TS    [NULL_CODE];   /* Tsf */

#define TSFOTT_T        1   /* Transfer */
#define TSFOTT_O        2   /* Order */
#define TSFOTT_OS       3   /* Ord */
#define TSFOTT_TS       4   /* Tsf */

#define _TSFOTT_MAX 4


/* TSFS = Transfer Security */
extern const char TSFS[NULL_CODE];

extern const char TSFS_PIXF  [NULL_CODE];   /* Intercompany Transfer Privilege */
extern const char TSFS_PAXF  [NULL_CODE];   /* Approve Transfer Privilege */
extern const char TSFS_PSXF  [NULL_CODE];   /* Submit Transfer Privilege */
extern const char TSFS_PFIN  [NULL_CODE];   /* Allow Finishing Privilege */
extern const char TSFS_PITX  [NULL_CODE];   /* Item Xform Packing */

#define TSFSTT_PIXF     1   /* Intercompany Transfer Privilege */
#define TSFSTT_PAXF     2   /* Approve Transfer Privilege */
#define TSFSTT_PSXF     3   /* Submit Transfer Privilege */
#define TSFSTT_PFIN     4   /* Allow Finishing Privilege */
#define TSFSTT_PITX     5   /* Item Xform Packing */

#define _TSFSTT_MAX 5


/* TSFW = TSF WO Items */
extern const char TSFW[NULL_CODE];

extern const char TSFW_I     [NULL_CODE];   /* Item */
extern const char TSFW_IP    [NULL_CODE];   /* Item Parent */
extern const char TSFW_IPD   [NULL_CODE];   /* Item Parent/Diff */
extern const char TSFW_AI    [NULL_CODE];   /* All Items */

#define TSFWTT_I        1   /* Item */
#define TSFWTT_IP       2   /* Item Parent */
#define TSFWTT_IPD      3   /* Item Parent/Diff */
#define TSFWTT_AI       4   /* All Items */

#define _TSFWTT_MAX 4


/* TSIT = Transfer Inventory Types */
extern const char TSIT[NULL_CODE];

extern const char TSIT_A     [NULL_CODE];   /* Available */
extern const char TSIT_U     [NULL_CODE];   /* Unavailable */

#define TSITTT_A        1   /* Available */
#define TSITTT_U        2   /* Unavailable */

#define _TSITTT_MAX 2


/* TSLC = Transfer Locations */
extern const char TSLC[NULL_CODE];

extern const char TSLC_FS    [NULL_CODE];   /* From Store */
extern const char TSLC_FW    [NULL_CODE];   /* From WH */
extern const char TSLC_TS    [NULL_CODE];   /* To Store */
extern const char TSLC_TW    [NULL_CODE];   /* To WH */
extern const char TSLC_FF    [NULL_CODE];   /* From Finisher */
extern const char TSLC_TF    [NULL_CODE];   /* To Finisher */

#define TSLCTT_FS       1   /* From Store */
#define TSLCTT_FW       2   /* From WH */
#define TSLCTT_TS       3   /* To Store */
#define TSLCTT_TW       4   /* To WH */
#define TSLCTT_FF       5   /* From Finisher */
#define TSLCTT_TF       6   /* To Finisher */

#define _TSLCTT_MAX 6


/* TSLG = Transfer Legs */
extern const char TSLG[NULL_CODE];

extern const char TSLG_1     [NULL_CODE];   /* 1st Leg */
extern const char TSLG_2     [NULL_CODE];   /* 2nd Leg */

#define TSLGTT_1        1   /* 1st Leg */
#define TSLGTT_2        2   /* 2nd Leg */

#define _TSLGTT_MAX 2


/* TSYS = Transaction Processing System */
extern const char TSYS[NULL_CODE];

extern const char TSYS_OMS   [NULL_CODE];   /* OMS */
extern const char TSYS_POS   [NULL_CODE];   /* POS */
extern const char TSYS_SIM   [NULL_CODE];   /* SIM */

#define TSYSTT_OMS      1   /* OMS */
#define TSYSTT_POS      2   /* POS */
#define TSYSTT_SIM      3   /* SIM */

#define _TSYSTT_MAX 3


/* TTDT = Transit Times Origin Type */
extern const char TTDT[NULL_CODE];

extern const char TTDT_ST    [NULL_CODE];   /* Store */
extern const char TTDT_WH    [NULL_CODE];   /* Warehouse */

#define TTDTTT_ST       1   /* Store */
#define TTDTTT_WH       2   /* Warehouse */

#define _TTDTTT_MAX 2


/* TTOT = Transit Times Origin Type */
extern const char TTOT[NULL_CODE];

extern const char TTOT_SU    [NULL_CODE];   /* Supplier */
extern const char TTOT_ST    [NULL_CODE];   /* Store */
extern const char TTOT_LLS   [NULL_CODE];   /* Location List-Store */
extern const char TTOT_WH    [NULL_CODE];   /* Warehouse */
extern const char TTOT_LLW   [NULL_CODE];   /* Location List-Warehouse */

#define TTOTTT_SU       1   /* Supplier */
#define TTOTTT_ST       2   /* Store */
#define TTOTTT_LLS      3   /* Location List-Store */
#define TTOTTT_WH       4   /* Warehouse */
#define TTOTTT_LLW      5   /* Location List-Warehouse */

#define _TTOTTT_MAX 5


/* TTST = Transit Times Origin Type */
extern const char TTST[NULL_CODE];

extern const char TTST_SU    [NULL_CODE];   /* Supplier Site */
extern const char TTST_ST    [NULL_CODE];   /* Store */
extern const char TTST_LLS   [NULL_CODE];   /* Location List-Store */
extern const char TTST_WH    [NULL_CODE];   /* Warehouse */
extern const char TTST_LLW   [NULL_CODE];   /* Location List-Warehouse */

#define TTSTTT_SU       1   /* Supplier Site */
#define TTSTTT_ST       2   /* Store */
#define TTSTTT_LLS      3   /* Location List-Store */
#define TTSTTT_WH       4   /* Warehouse */
#define TTSTTT_LLW      5   /* Location List-Warehouse */

#define _TTSTTT_MAX 5


/* TXCL = Tax Class */
extern const char TXCL[NULL_CODE];

extern const char TXCL_TAX   [NULL_CODE];   /* Taxable */
extern const char TXCL_NON   [NULL_CODE];   /* Non */
extern const char TXCL_FULL  [NULL_CODE];   /* Full */
extern const char TXCL_ALT   [NULL_CODE];   /* Alternate */

#define TXCLTT_TAX      1   /* Taxable */
#define TXCLTT_NON      2   /* Non */
#define TXCLTT_FULL     3   /* Full */
#define TXCLTT_ALT      4   /* Alternate */

#define _TXCLTT_MAX 4


/* TXHD = Tax Header */
extern const char TXHD[NULL_CODE];

extern const char TXHD_I     [NULL_CODE];   /* Item */
extern const char TXHD_D     [NULL_CODE];   /* @MH4@ */
extern const char TXHD_L     [NULL_CODE];   /* Item List */

#define TXHDTT_I        1   /* Item */
#define TXHDTT_D        2   /* @MH4@ */
#define TXHDTT_L        3   /* Item List */

#define _TXHDTT_MAX 3


/* TXTY = Tax Types */
extern const char TXTY[NULL_CODE];

extern const char TXTY_SALES [NULL_CODE];   /* Sales */
extern const char TXTY_HWY   [NULL_CODE];   /* Highway */
extern const char TXTY_TRST  [NULL_CODE];   /* Transit */
extern const char TXTY_HOSP  [NULL_CODE];   /* Hospital */
extern const char TXTY_STAD  [NULL_CODE];   /* Stadium */

#define TXTYTT_SALES    1   /* Sales */
#define TXTYTT_HWY      2   /* Highway */
#define TXTYTT_TRST     3   /* Transit */
#define TXTYTT_HOSP     4   /* Hospital */
#define TXTYTT_STAD     5   /* Stadium */

#define _TXTYTT_MAX 5


/* TZON = Time Zone */
extern const char TZON[NULL_CODE];

extern const char TZON_E     [NULL_CODE];   /* Eastern */
extern const char TZON_C     [NULL_CODE];   /* Central */
extern const char TZON_M     [NULL_CODE];   /* Mountain */
extern const char TZON_P     [NULL_CODE];   /* Pacific */

#define TZONTT_E        1   /* Eastern */
#define TZONTT_C        2   /* Central */
#define TZONTT_M        3   /* Mountain */
#define TZONTT_P        4   /* Pacific */

#define _TZONTT_MAX 4


/* UCHG = Up Charge Group */
extern const char UCHG[NULL_CODE];

extern const char UCHG_F     [NULL_CODE];   /* Freight */
extern const char UCHG_M     [NULL_CODE];   /* Miscellaneous */
extern const char UCHG_A     [NULL_CODE];   /* Admin. Fee */
extern const char UCHG_T     [NULL_CODE];   /* Taxes */
extern const char UCHG_K     [NULL_CODE];   /* Special K Fees */
extern const char UCHG_W     [NULL_CODE];   /* @SUH4@ */

#define UCHGTT_F        1   /* Freight */
#define UCHGTT_M        2   /* Miscellaneous */
#define UCHGTT_A        3   /* Admin. Fee */
#define UCHGTT_T        4   /* Taxes */
#define UCHGTT_K        5   /* Special K Fees */
#define UCHGTT_W        6   /* @SUH4@ */

#define _UCHGTT_MAX 6


/* UDAC = UDA Maintenance Types */
extern const char UDAC[NULL_CODE];

extern const char UDAC_ADD   [NULL_CODE];   /* Add */
extern const char UDAC_DELETE[NULL_CODE];   /* Delete */
extern const char UDAC_CHANGE[NULL_CODE];   /* Change */
extern const char UDAC_CREATE[NULL_CODE];   /* Create */

#define UDACTT_ADD      1   /* Add */
#define UDACTT_DELETE   2   /* Delete */
#define UDACTT_CHANGE   3   /* Change */
#define UDACTT_CREATE   4   /* Create */

#define _UDACTT_MAX 4


/* UDAM = UDA Dialog Values */
extern const char UDAM[NULL_CODE];

extern const char UDAM_ITEM  [NULL_CODE];   /* Item */

#define UDAMTT_ITEM     1   /* Item */

#define _UDAMTT_MAX 1


/* UDDT = UDA Data Types */
extern const char UDDT[NULL_CODE];

extern const char UDDT_NUM   [NULL_CODE];   /* Number */
extern const char UDDT_ALPHA [NULL_CODE];   /* Alphanumeric */
extern const char UDDT_DATE  [NULL_CODE];   /* Date */

#define UDDTTT_NUM      1   /* Number */
#define UDDTTT_ALPHA    2   /* Alphanumeric */
#define UDDTTT_DATE     3   /* Date */

#define _UDDTTT_MAX 3


/* UDIS = UDA Display Types */
extern const char UDIS[NULL_CODE];

extern const char UDIS_LV    [NULL_CODE];   /* List of Values */
extern const char UDIS_FF    [NULL_CODE];   /* Free Form Text */
extern const char UDIS_DT    [NULL_CODE];   /* Date */

#define UDISTT_LV       1   /* List of Values */
#define UDISTT_FF       2   /* Free Form Text */
#define UDISTT_DT       3   /* Date */

#define _UDISTT_MAX 3


/* UINT = UIN Type */
extern const char UINT[NULL_CODE];

extern const char UINT_SN    [NULL_CODE];   /* Serial Number */
extern const char UINT_AGSN  [NULL_CODE];   /* Auto Generate SN */

#define UINTTT_SN       1   /* Serial Number */
#define UINTTT_AGSN     2   /* Auto Generate SN */

#define _UINTTT_MAX 2


/* ULBL = UIN Label */
extern const char ULBL[NULL_CODE];

extern const char ULBL_SN    [NULL_CODE];   /* Serial Number */
extern const char ULBL_LN    [NULL_CODE];   /* License Number */
extern const char ULBL_PN    [NULL_CODE];   /* Part Number */
extern const char ULBL_IMEI  [NULL_CODE];   /* International Mobile Equipment Identity */
extern const char ULBL_SIN   [NULL_CODE];   /* Serial Identification Number */

#define ULBLTT_SN       1   /* Serial Number */
#define ULBLTT_LN       2   /* License Number */
#define ULBLTT_PN       3   /* Part Number */
#define ULBLTT_IMEI     4   /* International Mobile Equipment Identity */
#define ULBLTT_SIN      5   /* Serial Identification Number */

#define _ULBLTT_MAX 5


/* UMOP = UOM Operator */
extern const char UMOP[NULL_CODE];

extern const char UMOP_D     [NULL_CODE];   /* Divide */
extern const char UMOP_M     [NULL_CODE];   /* Multiply */

#define UMOPTT_D        1   /* Divide */
#define UMOPTT_M        2   /* Multiply */

#define _UMOPTT_MAX 2


/* UNIT = Multi-Unit/Single Unit Labels */
extern const char UNIT[NULL_CODE];

extern const char UNIT_S     [NULL_CODE];   /* Single-Unit */
extern const char UNIT_M     [NULL_CODE];   /* Multi-Unit */

#define UNITTT_S        1   /* Single-Unit */
#define UNITTT_M        2   /* Multi-Unit */

#define _UNITTT_MAX 2


/* UOMC = UOM Class Description */
extern const char UOMC[NULL_CODE];

extern const char UOMC_AREA  [NULL_CODE];   /* Area */
extern const char UOMC_DIMEN [NULL_CODE];   /* Dimension */
extern const char UOMC_MASS  [NULL_CODE];   /* Mass */
extern const char UOMC_MISC  [NULL_CODE];   /* Miscellaneous */
extern const char UOMC_PACK  [NULL_CODE];   /* Pack */
extern const char UOMC_QTY   [NULL_CODE];   /* Quantity */
extern const char UOMC_VOL   [NULL_CODE];   /* Volume */
extern const char UOMC_LVOL  [NULL_CODE];   /* Liquid Volume */

#define UOMCTT_AREA     1   /* Area */
#define UOMCTT_DIMEN    2   /* Dimension */
#define UOMCTT_MASS     3   /* Mass */
#define UOMCTT_MISC     4   /* Miscellaneous */
#define UOMCTT_PACK     5   /* Pack */
#define UOMCTT_QTY      6   /* Quantity */
#define UOMCTT_VOL      7   /* Volume */
#define UOMCTT_LVOL     8   /* Liquid Volume */

#define _UOMCTT_MAX 8


/* UORG = Up Charge Organizational Hierarchy */
extern const char UORG[NULL_CODE];

extern const char UORG_AL    [NULL_CODE];   /* All Locations */
extern const char UORG_AS    [NULL_CODE];   /* All Stores */
extern const char UORG_S     [NULL_CODE];   /* Store */
extern const char UORG_AW    [NULL_CODE];   /* All Warehouses */
extern const char UORG_W     [NULL_CODE];   /* Warehouse */
extern const char UORG_PW    [NULL_CODE];   /* Physical Warehouse */
extern const char UORG_AI    [NULL_CODE];   /* All Internal Finishers */
extern const char UORG_I     [NULL_CODE];   /* Internal Finisher */
extern const char UORG_AE    [NULL_CODE];   /* All External Finishers */
extern const char UORG_E     [NULL_CODE];   /* External Finisher */
extern const char UORG_LL    [NULL_CODE];   /* Location List */
extern const char UORG_A     [NULL_CODE];   /* @OH3@ */
extern const char UORG_R     [NULL_CODE];   /* @OH4@ */
extern const char UORG_D     [NULL_CODE];   /* @OH5@ */
extern const char UORG_T     [NULL_CODE];   /* Transfer Zone */
extern const char UORG_CZ    [NULL_CODE];   /* Cost Zone */
extern const char UORG_M     [NULL_CODE];   /* Importer */
extern const char UORG_X     [NULL_CODE];   /* Exporter */

#define UORGTT_AL       1   /* All Locations */
#define UORGTT_AS       2   /* All Stores */
#define UORGTT_S        3   /* Store */
#define UORGTT_AW       4   /* All Warehouses */
#define UORGTT_W        5   /* Warehouse */
#define UORGTT_PW       6   /* Physical Warehouse */
#define UORGTT_AI       7   /* All Internal Finishers */
#define UORGTT_I        8   /* Internal Finisher */
#define UORGTT_AE       9   /* All External Finishers */
#define UORGTT_E       10   /* External Finisher */
#define UORGTT_LL      11   /* Location List */
#define UORGTT_A       12   /* @OH3@ */
#define UORGTT_R       13   /* @OH4@ */
#define UORGTT_D       14   /* @OH5@ */
#define UORGTT_T       15   /* Transfer Zone */
#define UORGTT_CZ      16   /* Cost Zone */
#define UORGTT_M       17   /* Importer */
#define UORGTT_X       18   /* Exporter */

#define _UORGTT_MAX 18


/* UOWK = Sales Audit Unit of Work */
extern const char UOWK[NULL_CODE];

extern const char UOWK_T     [NULL_CODE];   /* Transaction */
extern const char UOWK_S     [NULL_CODE];   /* Store Day */

#define UOWKTT_T        1   /* Transaction */
#define UOWKTT_S        2   /* Store Day */

#define _UOWKTT_MAX 2


/* UPCM = Multiple UPC Indicator */
extern const char UPCM[NULL_CODE];

extern const char UPCM_M     [NULL_CODE];   /* Multiple */

#define UPCMTT_M        1   /* Multiple */

#define _UPCMTT_MAX 1


/* UPCT = UPC Type */
extern const char UPCT[NULL_CODE];

extern const char UPCT_ITEM  [NULL_CODE];   /* Oracle Retail Item Number */
extern const char UPCT_UPC_A [NULL_CODE];   /* UCC12 */
extern const char UPCT_UPC_AS[NULL_CODE];   /* UCC12 with Supplement */
extern const char UPCT_UPC_E [NULL_CODE];   /* UCC8 */
extern const char UPCT_UPC_ES[NULL_CODE];   /* UCC8 with Supplement */
extern const char UPCT_EAN8  [NULL_CODE];   /* EAN/UCC-8 */
extern const char UPCT_EAN13 [NULL_CODE];   /* EAN/UCC-13 */
extern const char UPCT_EAN13S[NULL_CODE];   /* EAN/UCC-13 with Supplement */
extern const char UPCT_ISBN10[NULL_CODE];   /* ISBN-10 */
extern const char UPCT_ISBN13[NULL_CODE];   /* ISBN-13 */
extern const char UPCT_NDC   [NULL_CODE];   /* NDC/NHRIC - National Drug Code */
extern const char UPCT_PLU   [NULL_CODE];   /* PLU */
extern const char UPCT_VPLU  [NULL_CODE];   /* Variable Weight PLU */
extern const char UPCT_SSCC  [NULL_CODE];   /* SSCC Shipper Carton */
extern const char UPCT_UCC14 [NULL_CODE];   /* EAN/UCC-14 */
extern const char UPCT_MANL  [NULL_CODE];   /* Manual */

#define UPCTTT_ITEM     1   /* Oracle Retail Item Number */
#define UPCTTT_UPC_A    2   /* UCC12 */
#define UPCTTT_UPC_AS   3   /* UCC12 with Supplement */
#define UPCTTT_UPC_E    4   /* UCC8 */
#define UPCTTT_UPC_ES   5   /* UCC8 with Supplement */
#define UPCTTT_EAN8     6   /* EAN/UCC-8 */
#define UPCTTT_EAN13    7   /* EAN/UCC-13 */
#define UPCTTT_EAN13S   8   /* EAN/UCC-13 with Supplement */
#define UPCTTT_ISBN10   9   /* ISBN-10 */
#define UPCTTT_ISBN13  10   /* ISBN-13 */
#define UPCTTT_NDC     11   /* NDC/NHRIC - National Drug Code */
#define UPCTTT_PLU     12   /* PLU */
#define UPCTTT_VPLU    13   /* Variable Weight PLU */
#define UPCTTT_SSCC    14   /* SSCC Shipper Carton */
#define UPCTTT_UCC14   15   /* EAN/UCC-14 */
#define UPCTTT_MANL    16   /* Manual */

#define _UPCTTT_MAX 16


/* USOL = Unsatisfied Order Location */
extern const char USOL[NULL_CODE];

extern const char USOL_1     [NULL_CODE];   /* Store */
extern const char USOL_0     [NULL_CODE];   /* Warehouse */

#define USOLTT_1        1   /* Store */
#define USOLTT_0        2   /* Warehouse */

#define _USOLTT_MAX 2


/* UTYP = Up Charge Types */
extern const char UTYP[NULL_CODE];

extern const char UTYP_E     [NULL_CODE];   /* Expense */
extern const char UTYP_P     [NULL_CODE];   /* Profit */

#define UTYPTT_E        1   /* Expense */
#define UTYPTT_P        2   /* Profit */

#define _UTYPTT_MAX 2


/* VALT = Validation Type */
extern const char VALT[NULL_CODE];


/* VATC = RMS Vat Codes */
extern const char VATC[NULL_CODE];


/* VATL = VAT Labels */
extern const char VATL[NULL_CODE];

extern const char VATL_EX    [NULL_CODE];   /* (Excl. VAT) */
extern const char VATL_IN    [NULL_CODE];   /* (Incl. VAT) */
extern const char VATL_TV    [NULL_CODE];   /* Total VAT */

#define VATLTT_EX       1   /* (Excl. VAT) */
#define VATLTT_IN       2   /* (Incl. VAT) */
#define VATLTT_TV       3   /* Total VAT */

#define _VATLTT_MAX 3


/* VATR = VAT Region */
extern const char VATR[NULL_CODE];

extern const char VATR_VR    [NULL_CODE];   /* VAT Region */

#define VATRTT_VR       2   /* VAT Region */

#define _VATRTT_MAX 2


/* VEND = Vendor Types */
extern const char VEND[NULL_CODE];

extern const char VEND_AG    [NULL_CODE];   /* Agent */
extern const char VEND_AP    [NULL_CODE];   /* Applicant */
extern const char VEND_BK    [NULL_CODE];   /* Bank */
extern const char VEND_BR    [NULL_CODE];   /* Broker */
extern const char VEND_CN    [NULL_CODE];   /* Consignee */
extern const char VEND_CO    [NULL_CODE];   /* Consolidator */
extern const char VEND_FA    [NULL_CODE];   /* Factory */
extern const char VEND_FF    [NULL_CODE];   /* Freight Forwarder */
extern const char VEND_IM    [NULL_CODE];   /* Importer */
extern const char VEND_SU    [NULL_CODE];   /* Supplier */

#define VENDTT_AG       1   /* Agent */
#define VENDTT_AP       2   /* Applicant */
#define VENDTT_BK       3   /* Bank */
#define VENDTT_BR       4   /* Broker */
#define VENDTT_CN       5   /* Consignee */
#define VENDTT_CO       6   /* Consolidator */
#define VENDTT_FA       7   /* Factory */
#define VENDTT_FF       8   /* Freight Forwarder */
#define VENDTT_IM       9   /* Importer */
#define VENDTT_SU      10   /* Supplier */

#define _VENDTT_MAX 10


extern const char VOUCH_4000  [NULL_CODE];   /* Credit Voucher */
extern const char VOUCH_4010  [NULL_CODE];   /* Manual Credit */
extern const char VOUCH_4020  [NULL_CODE];   /* Manual Imprint */
extern const char VOUCH_4030  [NULL_CODE];   /* Gift Certificate */
extern const char VOUCH_4040  [NULL_CODE];   /* Gift Card */
extern const char VOUCH_4050  [NULL_CODE];   /* Store Credit */
extern const char VOUCH_4060  [NULL_CODE];   /* Mail Certificate */
extern const char VOUCH_4070  [NULL_CODE];   /* Purchase Order */
extern const char VOUCH_4080  [NULL_CODE];   /* PrePaid */
extern const char VOUCH_4090  [NULL_CODE];   /* Store Credit Alternate Currency */
extern const char VOUCH_4100  [NULL_CODE];   /* Gift Certificate Alternate Currency */
extern const char VOUCH_4110  [NULL_CODE];   /* Customer Credit */


/* VOUT = Voucher Types */
extern const char VOUT[NULL_CODE];

extern const char VOUT_GIC   [NULL_CODE];   /* Gift Certificate */
extern const char VOUT_VOU   [NULL_CODE];   /* Voucher */

#define VOUTTT_GIC      1   /* Gift Certificate */
#define VOUTTT_VOU      2   /* Voucher */

#define _VOUTTT_MAX 2


/* VRCT = VAT Region Calc Type */
extern const char VRCT[NULL_CODE];

extern const char VRCT_S     [NULL_CODE];   /* Simple */
extern const char VRCT_E     [NULL_CODE];   /* Exempt */
extern const char VRCT_C     [NULL_CODE];   /* Custom */

#define VRCTTT_S        1   /* Simple */
#define VRCTTT_E        2   /* Exempt */
#define VRCTTT_C        3   /* Custom */

#define _VRCTTT_MAX 3


/* VRRT = Virtual Realm Types */
extern const char VRRT[NULL_CODE];

extern const char VRRT_PKTI  [NULL_CODE];   /* Table(INT) PK or Single Row */
extern const char VRRT_PKV   [NULL_CODE];   /* View(INT/EXT) PK or Single Row */
extern const char VRRT_TOTAL [NULL_CODE];   /* View based on SA_TOTAL */

#define VRRTTT_PKTI     1   /* Table(INT) PK or Single Row */
#define VRRTTT_PKV      2   /* View(INT/EXT) PK or Single Row */
#define VRRTTT_TOTAL    3   /* View based on SA_TOTAL */

#define _VRRTTT_MAX 3


/* VRTY = VAT Region Type */
extern const char VRTY[NULL_CODE];

extern const char VRTY_N     [NULL_CODE];   /* Non-Member */
extern const char VRTY_M     [NULL_CODE];   /* EU Member */
extern const char VRTY_E     [NULL_CODE];   /* Base EU Region */

#define VRTYTT_N        1   /* Non-Member */
#define VRTYTT_M        2   /* EU Member */
#define VRTYTT_E        3   /* Base EU Region */

#define _VRTYTT_MAX 3


/* VSCT = Sales Audit View System Calculated Total */
extern const char VSCT[NULL_CODE];

extern const char VSCT_A     [NULL_CODE];   /* ALL */
extern const char VSCT_HQ    [NULL_CODE];   /* HQ only */
extern const char VSCT_HSM   [NULL_CODE];   /* HQ & Store Manager only */
extern const char VSCT_N     [NULL_CODE];   /* None */

#define VSCTTT_A        1   /* ALL */
#define VSCTTT_HQ       2   /* HQ only */
#define VSCTTT_HSM      3   /* HQ & Store Manager only */
#define VSCTTT_N        4   /* None */

#define _VSCTTT_MAX 4


/* VTTP = VAT Type */
extern const char VTTP[NULL_CODE];

extern const char VTTP_B     [NULL_CODE];   /* Both */
extern const char VTTP_C     [NULL_CODE];   /* Cost */
extern const char VTTP_R     [NULL_CODE];   /* Retail */

#define VTTPTT_B        1   /* Both */
#define VTTPTT_C        2   /* Cost */
#define VTTPTT_R        3   /* Retail */

#define _VTTPTT_MAX 3


/* WEAT = Sales Audit Weather Conditions */
extern const char WEAT[NULL_CODE];

extern const char WEAT_SNO   [NULL_CODE];   /* Snow */
extern const char WEAT_RAI   [NULL_CODE];   /* Rain */
extern const char WEAT_SUN   [NULL_CODE];   /* Sunny */
extern const char WEAT_CLO   [NULL_CODE];   /* Cloudy */
extern const char WEAT_HAZ   [NULL_CODE];   /* Hazy */
extern const char WEAT_HUM   [NULL_CODE];   /* Humid */
extern const char WEAT_DAM   [NULL_CODE];   /* Damp */
extern const char WEAT_FLO   [NULL_CODE];   /* Flood */
extern const char WEAT_HUR   [NULL_CODE];   /* Hurricane */
extern const char WEAT_TOR   [NULL_CODE];   /* Tornado */

#define WEATTT_SNO      1   /* Snow */
#define WEATTT_RAI      2   /* Rain */
#define WEATTT_SUN      3   /* Sunny */
#define WEATTT_CLO      4   /* Cloudy */
#define WEATTT_HAZ      5   /* Hazy */
#define WEATTT_HUM      6   /* Humid */
#define WEATTT_DAM      7   /* Damp */
#define WEATTT_FLO      8   /* Flood */
#define WEATTT_HUR      9   /* Hurricane */
#define WEATTT_TOR     10   /* Tornado */

#define _WEATTT_MAX 10


/* WEEK = Label "Weeks" */
extern const char WEEK[NULL_CODE];

extern const char WEEK_W     [NULL_CODE];   /* weeks */

#define WEEKTT_W        1   /* weeks */

#define _WEEKTT_MAX 1


/* WFCH = @SUH4@ Customer Hierarchy */
extern const char WFCH[NULL_CODE];

extern const char WFCH_W     [NULL_CODE];   /* @SUH4@ */
extern const char WFCH_F     [NULL_CODE];   /* @SUH4@ */

#define WFCHTT_W        1   /* @SUH4@ */
#define WFCHTT_F        2   /* @SUH4@ */

#define _WFCHTT_MAX 2


/* WFCO = @SUH4@ Orders Cancel Reason */
extern const char WFCO[NULL_CODE];

extern const char WFCO_PD    [NULL_CODE];   /* Past Due Date */
extern const char WFCO_AS    [NULL_CODE];   /* Alternate Supplier */
extern const char WFCO_NS    [NULL_CODE];   /* Unknown (not specified) */
extern const char WFCO_SI    [NULL_CODE];   /* Substitute Item */
extern const char WFCO_NR    [NULL_CODE];   /* Item cannot be replenished */
extern const char WFCO_ED    [NULL_CODE];   /* Externally Deleted */
extern const char WFCO_BC    [NULL_CODE];   /* Bad Credit Customer */

#define WFCOTT_PD       1   /* Past Due Date */
#define WFCOTT_AS       2   /* Alternate Supplier */
#define WFCOTT_NS       3   /* Unknown (not specified) */
#define WFCOTT_SI       4   /* Substitute Item */
#define WFCOTT_NR       5   /* Item cannot be replenished */
#define WFCOTT_ED       6   /* Externally Deleted */
#define WFCOTT_BC       7   /* Bad Credit Customer */

#define _WFCOTT_MAX 7


/* WFCR = @SUH4@ Returns Cancel Reason */
extern const char WFCR[NULL_CODE];

extern const char WFCR_II    [NULL_CODE];   /* Insufficient Inventory */
extern const char WFCR_DS    [NULL_CODE];   /* Damaged Stock */
extern const char WFCR_OR    [NULL_CODE];   /* Overdue Return */
extern const char WFCR_ED    [NULL_CODE];   /* Externally Deleted */

#define WFCRTT_II       1   /* Insufficient Inventory */
#define WFCRTT_DS       2   /* Damaged Stock */
#define WFCRTT_OR       3   /* Overdue Return */
#define WFCRTT_ED       4   /* Externally Deleted */

#define _WFCRTT_MAX 4


/* WFIT = WF Item Types */
extern const char WFIT[NULL_CODE];

extern const char WFIT_SS    [NULL_CODE];   /* Item */
extern const char WFIT_IL    [NULL_CODE];   /* Item List */
extern const char WFIT_IP    [NULL_CODE];   /* Item Parent */
extern const char WFIT_RI    [NULL_CODE];   /* Reference Item */

#define WFITTT_SS       1   /* Item */
#define WFITTT_IL       2   /* Item List */
#define WFITTT_IP       3   /* Item Parent */
#define WFITTT_RI       4   /* Reference Item */

#define _WFITTT_MAX 4


/* WFOH = @SUH4@ Organization Hierarchies */
extern const char WFOH[NULL_CODE];

extern const char WFOH_C     [NULL_CODE];   /* @OH1@ */
extern const char WFOH_F     [NULL_CODE];   /* @SUH4@ */

#define WFOHTT_C        1   /* @OH1@ */
#define WFOHTT_F        3   /* @SUH4@ */

#define _WFOHTT_MAX 3


/* WFOR = WF Order Type */
extern const char WFOR[NULL_CODE];

extern const char WFOR_FO    [NULL_CODE];   /* @SUH4@ Order */
extern const char WFOR_FR    [NULL_CODE];   /* @SUH4@ Return */

#define WFORTT_FO       1   /* @SUH4@ Order */
#define WFORTT_FR       2   /* @SUH4@ Return */

#define _WFORTT_MAX 2


/* WFOS = WF Order Status */
extern const char WFOS[NULL_CODE];

extern const char WFOS_I     [NULL_CODE];   /* Input */
extern const char WFOS_A     [NULL_CODE];   /* Approved */
extern const char WFOS_P     [NULL_CODE];   /* In Progress */
extern const char WFOS_C     [NULL_CODE];   /* Cancelled */
extern const char WFOS_D     [NULL_CODE];   /* Closed */
extern const char WFOS_R     [NULL_CODE];   /* Pending Credit Approval */

#define WFOSTT_I        1   /* Input */
#define WFOSTT_A        2   /* Approved */
#define WFOSTT_P        3   /* In Progress */
#define WFOSTT_C        4   /* Cancelled */
#define WFOSTT_D        5   /* Closed */
#define WFOSTT_R        6   /* Pending Credit Approval */

#define _WFOSTT_MAX 6


/* WFOT = WF Order Types */
extern const char WFOT[NULL_CODE];

extern const char WFOT_M     [NULL_CODE];   /* Manual */
extern const char WFOT_E     [NULL_CODE];   /* EDI */
extern const char WFOT_A     [NULL_CODE];   /* Auto */
extern const char WFOT_X     [NULL_CODE];   /* External */

#define WFOTTT_M        1   /* Manual */
#define WFOTTT_E        2   /* EDI */
#define WFOTTT_A        3   /* Auto */
#define WFOTTT_X        4   /* External */

#define _WFOTTT_MAX 4


/* WFRE = WF Return Types */
extern const char WFRE[NULL_CODE];

extern const char WFRE_M     [NULL_CODE];   /* Manual */
extern const char WFRE_E     [NULL_CODE];   /* EDI */
extern const char WFRE_A     [NULL_CODE];   /* Auto */
extern const char WFRE_X     [NULL_CODE];   /* External */

#define WFRETT_M        1   /* Manual */
#define WFRETT_E        2   /* EDI */
#define WFRETT_A        3   /* Auto */
#define WFRETT_X        4   /* External */

#define _WFRETT_MAX 4


/* WFRF = WF Return Method */
extern const char WFRF[NULL_CODE];

extern const char WFRF_R     [NULL_CODE];   /* Return To warehouse */
extern const char WFRF_D     [NULL_CODE];   /* Destroy on site */

#define WFRFTT_R        1   /* Return To warehouse */
#define WFRFTT_D        2   /* Destroy on site */

#define _WFRFTT_MAX 2


/* WFRS = Status of WF Returns */
extern const char WFRS[NULL_CODE];

extern const char WFRS_I     [NULL_CODE];   /* Input */
extern const char WFRS_A     [NULL_CODE];   /* Approved */
extern const char WFRS_P     [NULL_CODE];   /* In Progress */
extern const char WFRS_C     [NULL_CODE];   /* Cancelled */
extern const char WFRS_D     [NULL_CODE];   /* Closed */

#define WFRSTT_I        1   /* Input */
#define WFRSTT_A        2   /* Approved */
#define WFRSTT_P        3   /* In Progress */
#define WFRSTT_C        4   /* Cancelled */
#define WFRSTT_D        5   /* Closed */

#define _WFRSTT_MAX 5


/* WFRT = WF Restocking Type */
extern const char WFRT[NULL_CODE];

extern const char WFRT_V     [NULL_CODE];   /* Percent */
extern const char WFRT_S     [NULL_CODE];   /* Specific Amount */

#define WFRTTT_V        1   /* Percent */
#define WFRTTT_S        2   /* Specific Amount */

#define _WFRTTT_MAX 2


/* WFSC = @SUH4@ Stores */
extern const char WFSC[NULL_CODE];

extern const char WFSC_X     [NULL_CODE];   /* Class Stores X */

#define WFSCTT_X        1   /* Class Stores X */

#define _WFSCTT_MAX 1


/* WHLL = Warehouse Location List */
extern const char WHLL[NULL_CODE];

extern const char WHLL_W     [NULL_CODE];   /* Warehouse */
extern const char WHLL_A     [NULL_CODE];   /* All Warehouses */
extern const char WHLL_LL    [NULL_CODE];   /* Location List */

#define WHLLTT_W        1   /* Warehouse */
#define WHLLTT_A        2   /* All Warehouses */
#define WHLLTT_LL       3   /* Location List */

#define _WHLLTT_MAX 3


/* WHSE = Warehouse */
extern const char WHSE[NULL_CODE];

extern const char WHSE_W     [NULL_CODE];   /* Warehouse */

#define WHSETT_W        1   /* Warehouse */

#define _WHSETT_MAX 1


/* WHTP = Number of Warehouses */
extern const char WHTP[NULL_CODE];

extern const char WHTP_W     [NULL_CODE];   /* Warehouse */
extern const char WHTP_A     [NULL_CODE];   /* All Warehouses */
extern const char WHTP_LL    [NULL_CODE];   /* Location List */

#define WHTPTT_W        1   /* Warehouse */
#define WHTPTT_A        2   /* All Warehouses */
#define WHTPTT_LL       3   /* Location List */

#define _WHTPTT_MAX 3


/* WHTY = Virtual Warehouse Types */
extern const char WHTY[NULL_CODE];

extern const char WHTY_XD_RG [NULL_CODE];   /* Cross Dock */
extern const char WHTY_CS_RG [NULL_CODE];   /* CSC */
extern const char WHTY_XD_GS [NULL_CODE];   /* Global Sourcing */
extern const char WHTY_CS_NT [NULL_CODE];   /* Non-traditional */

#define WHTYTT_XD_RG    1   /* Cross Dock */
#define WHTYTT_CS_RG    2   /* CSC */
#define WHTYTT_XD_GS    3   /* Global Sourcing */
#define WHTYTT_CS_NT    4   /* Non-traditional */

#define _WHTYTT_MAX 4


/* WIPP = WIP code for kitted items */
extern const char WIPP[NULL_CODE];

extern const char WIPP_WIP   [NULL_CODE];   /* Pack kitting */

#define WIPPTT_WIP      1   /* Pack kitting */

#define _WIPPTT_MAX 1


/* WOLT = Customer Location Type */
extern const char WOLT[NULL_CODE];

extern const char WOLT_W     [NULL_CODE];   /* @SUH4@ */
extern const char WOLT_F     [NULL_CODE];   /* @SUH4@ */
extern const char WOLT_C     [NULL_CODE];   /* @OH1@ */

#define WOLTTT_W        1   /* @SUH4@ */
#define WOLTTT_F        2   /* @SUH4@ */
#define WOLTTT_C        3   /* @OH1@ */

#define _WOLTTT_MAX 3


/* WSAT = Warehouse Store Assignment Type */
extern const char WSAT[NULL_CODE];

extern const char WSAT_W     [NULL_CODE];   /* Warehouse Stocked */
extern const char WSAT_C     [NULL_CODE];   /* Cross-docked */
extern const char WSAT_L     [NULL_CODE];   /* WH/Cross Link */
extern const char WSAT_A     [NULL_CODE];   /* All */

#define WSATTT_W        1   /* Warehouse Stocked */
#define WSATTT_C        2   /* Cross-docked */
#define WSATTT_L        3   /* WH/Cross Link */
#define WSATTT_A        4   /* All */

#define _WSATTT_MAX 4


/* WSIS = Warehouse Store Issue Labels */
extern const char WSIS[NULL_CODE];

extern const char WSIS_SUS   [NULL_CODE];   /* Total Unit Sales */
extern const char WSIS_SUW   [NULL_CODE];   /* Unit Sales By End Of Week Date */
extern const char WSIS_IUI   [NULL_CODE];   /* Total Issues */
extern const char WSIS_IUW   [NULL_CODE];   /* Unit Issues By End Of Week Date */

#define WSISTT_SUS      1   /* Total Unit Sales */
#define WSISTT_SUW      2   /* Unit Sales By End Of Week Date */
#define WSISTT_IUI      3   /* Total Issues */
#define WSISTT_IUW      4   /* Unit Issues By End Of Week Date */

#define _WSISTT_MAX 4


/* WSTG = Wastage Type */
extern const char WSTG[NULL_CODE];

extern const char WSTG_SL    [NULL_CODE];   /* Sales Wastage */
extern const char WSTG_SP    [NULL_CODE];   /* Spoilage Wastage */

#define WSTGTT_SL       1   /* Sales Wastage */
#define WSTGTT_SP       2   /* Spoilage Wastage */

#define _WSTGTT_MAX 2


/* WWIP = Work Order WIP codes */
extern const char WWIP[NULL_CODE];

extern const char WWIP_QC    [NULL_CODE];   /* Quality Control */
extern const char WWIP_KIT   [NULL_CODE];   /* Kitting */
extern const char WWIP_TICKET[NULL_CODE];   /* Ticket Item */

#define WWIPTT_QC       1   /* Quality Control */
#define WWIPTT_KIT      2   /* Kitting */
#define WWIPTT_TICKET   3   /* Ticket Item */

#define _WWIPTT_MAX 3


/* YSNO = Yes or No Indicator */
extern const char YSNO[NULL_CODE];

extern const char YSNO_Y     [NULL_CODE];   /* Yes */
extern const char YSNO_N     [NULL_CODE];   /* No */

#define YSNOTT_Y        1   /* Yes */
#define YSNOTT_N        2   /* No */

#define _YSNOTT_MAX 2


/* ZLVL = Zone Levels */
extern const char ZLVL[NULL_CODE];

extern const char ZLVL_A     [NULL_CODE];   /* All Zones */
extern const char ZLVL_Z     [NULL_CODE];   /* Zone */

#define ZLVLTT_A        1   /* All Zones */
#define ZLVLTT_Z        2   /* Zone */

#define _ZLVLTT_MAX 2


/* ZONE = Zones */
extern const char ZONE[NULL_CODE];

extern const char ZONE_Z     [NULL_CODE];   /* Zone */
extern const char ZONE_O     [NULL_CODE];   /* All Zones */

#define ZONETT_Z        1   /* Zone */
#define ZONETT_O        2   /* All Zones */

#define _ZONETT_MAX 2


/* ZSEC = Zone Security Areas */
extern const char ZSEC[NULL_CODE];

extern const char ZSEC_ZALL  [NULL_CODE];   /* All */
extern const char ZSEC_ZPRC  [NULL_CODE];   /* Pricing */
extern const char ZSEC_ZCLR  [NULL_CODE];   /* Clearances */

#define ZSECTT_ZALL     1   /* All */
#define ZSECTT_ZPRC     2   /* Pricing */
#define ZSECTT_ZCLR     3   /* Clearances */

#define _ZSECTT_MAX 3


/* ZZAC = Action Type Category */
extern const char ZZAC[NULL_CODE];

extern const char ZZAC_F     [NULL_CODE];   /* Open Form */
extern const char ZZAC_P     [NULL_CODE];   /* Procedure */
extern const char ZZAC_W     [NULL_CODE];   /* Windows Call */
extern const char ZZAC_S     [NULL_CODE];   /* System */
extern const char ZZAC_N     [NULL_CODE];   /* Function */

#define ZZACTT_F        1   /* Open Form */
#define ZZACTT_P        2   /* Procedure */
#define ZZACTT_W        3   /* Windows Call */
#define ZZACTT_S        4   /* System */
#define ZZACTT_N        5   /* Function */

#define _ZZACTT_MAX 5


/* ZZAD = Action / drill indicator */
extern const char ZZAD[NULL_CODE];

extern const char ZZAD_A     [NULL_CODE];   /* Action */
extern const char ZZAD_D     [NULL_CODE];   /* Drill */
extern const char ZZAD_N     [NULL_CODE];   /* Neither */

#define ZZADTT_A        1   /* Action */
#define ZZADTT_D        2   /* Drill */
#define ZZADTT_N        3   /* Neither */

#define _ZZADTT_MAX 3


/* ZZAL = Active Only or All */
extern const char ZZAL[NULL_CODE];

extern const char ZZAL_A     [NULL_CODE];   /* Only Active and Future */
extern const char ZZAL_L     [NULL_CODE];   /* All */

#define ZZALTT_A        1   /* Only Active and Future */
#define ZZALTT_L        2   /* All */

#define _ZZALTT_MAX 2


/* ZZAN = Action Category Syntax */
extern const char ZZAN[NULL_CODE];

extern const char ZZAN_F     [NULL_CODE];   /* Form File Name */
extern const char ZZAN_P     [NULL_CODE];   /* Procedure Syntax */
extern const char ZZAN_N     [NULL_CODE];   /* Function Syntax */
extern const char ZZAN_W     [NULL_CODE];   /* Command Line */

#define ZZANTT_F        1   /* Form File Name */
#define ZZANTT_P        2   /* Procedure Syntax */
#define ZZANTT_N        3   /* Function Syntax */
#define ZZANTT_W        4   /* Command Line */

#define _ZZANTT_MAX 4


/* ZZAS = Action Type Category (no system) */
extern const char ZZAS[NULL_CODE];

extern const char ZZAS_F     [NULL_CODE];   /* Open Form */
extern const char ZZAS_P     [NULL_CODE];   /* Procedure */
extern const char ZZAS_W     [NULL_CODE];   /* Windows Call */
extern const char ZZAS_N     [NULL_CODE];   /* Function */

#define ZZASTT_F        1   /* Open Form */
#define ZZASTT_P        2   /* Procedure */
#define ZZASTT_W        3   /* Windows Call */
#define ZZASTT_N        4   /* Function */

#define _ZZASTT_MAX 4


/* ZZAT = Alternative Notification Type */
extern const char ZZAT[NULL_CODE];

extern const char ZZAT_EMAIL [NULL_CODE];   /* E-mail */
extern const char ZZAT_FAX   [NULL_CODE];   /* FAX */
extern const char ZZAT_PAGE  [NULL_CODE];   /* Page */

#define ZZATTT_EMAIL    1   /* E-mail */
#define ZZATTT_FAX      2   /* FAX */
#define ZZATTT_PAGE     3   /* Page */

#define _ZZATTT_MAX 3


/* ZZAU = Alternative Notification Use */
extern const char ZZAU[NULL_CODE];

extern const char ZZAU_HO    [NULL_CODE];   /* High Priority Only */
extern const char ZZAU_ALL   [NULL_CODE];   /* All Events */
extern const char ZZAU_NO    [NULL_CODE];   /* None */

#define ZZAUTT_HO       1   /* High Priority Only */
#define ZZAUTT_ALL      2   /* All Events */
#define ZZAUTT_NO       3   /* None */

#define _ZZAUTT_MAX 3


/* ZZCF = Schedule Cycle Frequency */
extern const char ZZCF[NULL_CODE];

extern const char ZZCF_O     [NULL_CODE];   /* Once */
extern const char ZZCF_R     [NULL_CODE];   /* Repeat */

#define ZZCFTT_O        1   /* Once */
#define ZZCFTT_R        2   /* Repeat */

#define _ZZCFTT_MAX 2


/* ZZCT = Comment Window Titles */
extern const char ZZCT[NULL_CODE];

extern const char ZZCT_SCHDSC[NULL_CODE];   /* Schedule Description */
extern const char ZZCT_EVTDSC[NULL_CODE];   /* Event Type Description */
extern const char ZZCT_EXTDSC[NULL_CODE];   /* Exception Type Description */
extern const char ZZCT_EGLDSC[NULL_CODE];   /* Event Group List Description */
extern const char ZZCT_ECRDSC[NULL_CODE];   /* Event Type Closure Rule Description */

#define ZZCTTT_SCHDSC   1   /* Schedule Description */
#define ZZCTTT_EVTDSC   2   /* Event Type Description */
#define ZZCTTT_EXTDSC   3   /* Exception Type Description */
#define ZZCTTT_EGLDSC   4   /* Event Group List Description */
#define ZZCTTT_ECRDSC   5   /* Event Type Closure Rule Description */

#define _ZZCTTT_MAX 5


/* ZZDE = Deferred Indicator */
extern const char ZZDE[NULL_CODE];

extern const char ZZDE_Y     [NULL_CODE];   /* Deferred */
extern const char ZZDE_N     [NULL_CODE];   /* Not Deferred */

#define ZZDETT_Y        1   /* Deferred */
#define ZZDETT_N        2   /* Not Deferred */

#define _ZZDETT_MAX 2


/* ZZEP = Event Type Parameter Details */
extern const char ZZEP[NULL_CODE];

extern const char ZZEP_P     [NULL_CODE];   /* Parameter Types */
extern const char ZZEP_A     [NULL_CODE];   /* All Available */
extern const char ZZEP_S     [NULL_CODE];   /* System Only */
extern const char ZZEP_X     [NULL_CODE];   /* Exception Context */

#define ZZEPTT_P        1   /* Parameter Types */
#define ZZEPTT_A        2   /* All Available */
#define ZZEPTT_S        3   /* System Only */
#define ZZEPTT_X        4   /* Exception Context */

#define _ZZEPTT_MAX 4


/* ZZES = Exception Type Status */
extern const char ZZES[NULL_CODE];

extern const char ZZES_A     [NULL_CODE];   /* Active */
extern const char ZZES_D     [NULL_CODE];   /* Disabled */
extern const char ZZES_E     [NULL_CODE];   /* Expired */
extern const char ZZES_P     [NULL_CODE];   /* Paused */

#define ZZESTT_A        1   /* Active */
#define ZZESTT_D        2   /* Disabled */
#define ZZESTT_E        3   /* Expired */
#define ZZESTT_P        4   /* Paused */

#define _ZZESTT_MAX 4


/* ZZEX = External Realm Types */
extern const char ZZEX[NULL_CODE];

extern const char ZZEX_RA    [NULL_CODE];   /* Retail Analytics */

#define ZZEXTT_RA       1   /* Retail Analytics */

#define _ZZEXTT_MAX 1


/* ZZGT = ARI Group Types */
extern const char ZZGT[NULL_CODE];

extern const char ZZGT_S     [NULL_CODE];   /* Simple */
extern const char ZZGT_P     [NULL_CODE];   /* Parameter Driven */
extern const char ZZGT_U     [NULL_CODE];   /* User Driven */

#define ZZGTTT_S        1   /* Simple */
#define ZZGTTT_P        2   /* Parameter Driven */
#define ZZGTTT_U        3   /* User Driven */

#define _ZZGTTT_MAX 3


/* ZZLB = ARI Labels */
extern const char ZZLB[NULL_CODE];

extern const char ZZLB_EDESC [NULL_CODE];   /* Event Description */
extern const char ZZLB_ADESC [NULL_CODE];   /* Action Description */
extern const char ZZLB_DDESC [NULL_CODE];   /* Drill Description */
extern const char ZZLB_DF    [NULL_CODE];   /* Default */
extern const char ZZLB_XDESC [NULL_CODE];   /* Exception Description */
extern const char ZZLB_IMAGE [NULL_CODE];   /* Image */
extern const char ZZLB_TEXT  [NULL_CODE];   /* Text */
extern const char ZZLB_WORK  [NULL_CODE];   /* Revalidating events . . . */
extern const char ZZLB_EVACTS[NULL_CODE];   /* Event Actions */
extern const char ZZLB_DRACTS[NULL_CODE];   /* Event Drills */
extern const char ZZLB_DRILLS[NULL_CODE];   /* Drills */
extern const char ZZLB_ACTION[NULL_CODE];   /* Actions */
extern const char ZZLB_AVAILA[NULL_CODE];   /* Available Actions */
extern const char ZZLB_AVAILD[NULL_CODE];   /* Available Drills */
extern const char ZZLB_ACTPRM[NULL_CODE];   /* Action Parameter */
extern const char ZZLB_DRLPRM[NULL_CODE];   /* Drill Parameter */
extern const char ZZLB_EAPM  [NULL_CODE];   /* Event Action Link Parameter Mapping */
extern const char ZZLB_EDPM  [NULL_CODE];   /* Event Drill Link Parameter Mapping */
extern const char ZZLB_EDITBL[NULL_CODE];   /* Editable */
extern const char ZZLB_ACTNM [NULL_CODE];   /* Action Name */
extern const char ZZLB_DRLNM [NULL_CODE];   /* Drill Name */
extern const char ZZLB_ACTPNM[NULL_CODE];   /* Action Parameter Name */
extern const char ZZLB_DRLPNM[NULL_CODE];   /* Drill Parameter Name */
extern const char ZZLB_REQUIR[NULL_CODE];   /* Required */
extern const char ZZLB_WHERE [NULL_CODE];   /* Where Clause */
extern const char ZZLB_FROM  [NULL_CODE];   /* From Clause */
extern const char ZZLB_RDESC [NULL_CODE];   /* Realm Description */
extern const char ZZLB_XRULES[NULL_CODE];   /* Exception Rules */
extern const char ZZLB_ERULES[NULL_CODE];   /* Event Rules */
extern const char ZZLB_CHANGE[NULL_CODE];   /* Changed */
extern const char ZZLB_HEADP [NULL_CODE];   /* Headline Parameter Description */
extern const char ZZLB_AR    [NULL_CODE];   /* Show Related Events */
extern const char ZZLB_NOAR  [NULL_CODE];   /* Related Events Search */
extern const char ZZLB_ABOR  [NULL_CODE];   /* Abort */
extern const char ZZLB_AUTO  [NULL_CODE];   /* Auto Action */
extern const char ZZLB_MVALUE[NULL_CODE];   /* <Multiple Values> */
extern const char ZZLB_WORKAC[NULL_CODE];   /* Performing actions .  .  . */
extern const char ZZLB_FOWRD [NULL_CODE];   /* This is a forwarded event. */
extern const char ZZLB_REIN  [NULL_CODE];   /* This is a reinstated event. */

#define ZZLBTT_EDESC    1   /* Event Description */
#define ZZLBTT_ADESC    2   /* Action Description */
#define ZZLBTT_DDESC    3   /* Drill Description */
#define ZZLBTT_DF       4   /* Default */
#define ZZLBTT_XDESC    5   /* Exception Description */
#define ZZLBTT_IMAGE    6   /* Image */
#define ZZLBTT_TEXT     7   /* Text */
#define ZZLBTT_WORK     8   /* Revalidating events . . . */
#define ZZLBTT_EVACTS   9   /* Event Actions */
#define ZZLBTT_DRACTS  10   /* Event Drills */
#define ZZLBTT_DRILLS  11   /* Drills */
#define ZZLBTT_ACTION  12   /* Actions */
#define ZZLBTT_AVAILA  13   /* Available Actions */
#define ZZLBTT_AVAILD  14   /* Available Drills */
#define ZZLBTT_ACTPRM  15   /* Action Parameter */
#define ZZLBTT_DRLPRM  16   /* Drill Parameter */
#define ZZLBTT_EAPM    17   /* Event Action Link Parameter Mapping */
#define ZZLBTT_EDPM    18   /* Event Drill Link Parameter Mapping */
#define ZZLBTT_EDITBL  19   /* Editable */
#define ZZLBTT_ACTNM   20   /* Action Name */
#define ZZLBTT_DRLNM   21   /* Drill Name */
#define ZZLBTT_ACTPNM  22   /* Action Parameter Name */
#define ZZLBTT_DRLPNM  23   /* Drill Parameter Name */
#define ZZLBTT_REQUIR  24   /* Required */
#define ZZLBTT_WHERE   25   /* Where Clause */
#define ZZLBTT_FROM    26   /* From Clause */
#define ZZLBTT_RDESC   27   /* Realm Description */
#define ZZLBTT_XRULES  28   /* Exception Rules */
#define ZZLBTT_ERULES  29   /* Event Rules */
#define ZZLBTT_CHANGE  30   /* Changed */
#define ZZLBTT_HEADP   31   /* Headline Parameter Description */
#define ZZLBTT_AR      32   /* Show Related Events */
#define ZZLBTT_NOAR    33   /* Related Events Search */
#define ZZLBTT_ABOR    34   /* Abort */
#define ZZLBTT_AUTO    35   /* Auto Action */
#define ZZLBTT_MVALUE  36   /* <Multiple Values> */
#define ZZLBTT_WORKAC  37   /* Performing actions .  .  . */
#define ZZLBTT_FOWRD   38   /* This is a forwarded event. */
#define ZZLBTT_REIN    39   /* This is a reinstated event. */

#define _ZZLBTT_MAX 39


/* ZZMT = Exception Type Version Monitor Types */
extern const char ZZMT[NULL_CODE];

extern const char ZZMT_R     [NULL_CODE];   /* Real Time */
extern const char ZZMT_B     [NULL_CODE];   /* Batch Scan */
extern const char ZZMT_T     [NULL_CODE];   /* Trickle */

#define ZZMTTT_R        1   /* Real Time */
#define ZZMTTT_B        2   /* Batch Scan */
#define ZZMTTT_T        3   /* Trickle */

#define _ZZMTTT_MAX 3


/* ZZRM = Routing Method */
extern const char ZZRM[NULL_CODE];

extern const char ZZRM_A     [NULL_CODE];   /* All */
extern const char ZZRM_B     [NULL_CODE];   /* Balanced */
extern const char ZZRM_R     [NULL_CODE];   /* Random */
extern const char ZZRM_E     [NULL_CODE];   /* Event Balanced */

#define ZZRMTT_A        1   /* All */
#define ZZRMTT_B        2   /* Balanced */
#define ZZRMTT_R        3   /* Random */
#define ZZRMTT_E        4   /* Event Balanced */

#define _ZZRMTT_MAX 4


/* ZZRO = Routing Methods */
extern const char ZZRO[NULL_CODE];

extern const char ZZRO_ALL   [NULL_CODE];   /* All */
extern const char ZZRO_RRA   [NULL_CODE];   /* Round Robin All */
extern const char ZZRO_RRE   [NULL_CODE];   /* Round Robin Event */
extern const char ZZRO_WL    [NULL_CODE];   /* Workload */

#define ZZROTT_ALL      1   /* All */
#define ZZROTT_RRA      2   /* Round Robin All */
#define ZZROTT_RRE      3   /* Round Robin Event */
#define ZZROTT_WL       4   /* Workload */

#define _ZZROTT_MAX 4


/* ZZRT = Realm Types */
extern const char ZZRT[NULL_CODE];

extern const char ZZRT_L     [NULL_CODE];   /* Lookup */
extern const char ZZRT_V     [NULL_CODE];   /* View */
extern const char ZZRT_T     [NULL_CODE];   /* Table */
extern const char ZZRT_R     [NULL_CODE];   /* Report */
extern const char ZZRT_X     [NULL_CODE];   /* External */
extern const char ZZRT_P     [NULL_CODE];   /* PL/SQL */
extern const char ZZRT_F     [NULL_CODE];   /* Forms */
extern const char ZZRT_K     [NULL_CODE];   /* WaveLink */
extern const char ZZRT_S     [NULL_CODE];   /* Special */

#define ZZRTTT_L        1   /* Lookup */
#define ZZRTTT_V        2   /* View */
#define ZZRTTT_T        3   /* Table */
#define ZZRTTT_R        4   /* Report */
#define ZZRTTT_X        5   /* External */
#define ZZRTTT_P        6   /* PL/SQL */
#define ZZRTTT_F        7   /* Forms */
#define ZZRTTT_K        8   /* WaveLink */
#define ZZRTTT_S        9   /* Special */

#define _ZZRTTT_MAX 9


/* ZZSB = Sort Item Conversions */
extern const char ZZSB[NULL_CODE];

extern const char ZZSB_P     [NULL_CODE];   /* Event Priority */
extern const char ZZSB_N     [NULL_CODE];   /* Event Name */
extern const char ZZSB_S     [NULL_CODE];   /* State Name */
extern const char ZZSB_C     [NULL_CODE];   /* Event Count */

#define ZZSBTT_P        1   /* Event Priority */
#define ZZSBTT_N        2   /* Event Name */
#define ZZSBTT_S        3   /* State Name */
#define ZZSBTT_C        4   /* Event Count */

#define _ZZSBTT_MAX 4


/* ZZSC = Event Parameter Sort Category */
extern const char ZZSC[NULL_CODE];

extern const char ZZSC_H     [NULL_CODE];   /* Header */
extern const char ZZSC_D     [NULL_CODE];   /* Detail */

#define ZZSCTT_H        1   /* Header */
#define ZZSCTT_D        2   /* Detail */

#define _ZZSCTT_MAX 2


/* ZZSM = Schedule Modes */
extern const char ZZSM[NULL_CODE];

extern const char ZZSM_S     [NULL_CODE];   /* Signal */
extern const char ZZSM_T     [NULL_CODE];   /* Time */

#define ZZSMTT_S        1   /* Signal */
#define ZZSMTT_T        2   /* Time */

#define _ZZSMTT_MAX 2


/* ZZST = Schedule Type Description */
extern const char ZZST[NULL_CODE];

extern const char ZZST_S     [NULL_CODE];   /* Signal Based */
extern const char ZZST_T     [NULL_CODE];   /* Time Based */

#define ZZSTTT_S        1   /* Signal Based */
#define ZZSTTT_T        2   /* Time Based */

#define _ZZSTTT_MAX 2


/* ZZTF = Boolean Values */
extern const char ZZTF[NULL_CODE];

extern const char ZZTF_T     [NULL_CODE];   /* True */
extern const char ZZTF_F     [NULL_CODE];   /* False */

#define ZZTFTT_T        1   /* True */
#define ZZTFTT_F        2   /* False */

#define _ZZTFTT_MAX 2


/* ZZXP = Exception Parameter Details */
extern const char ZZXP[NULL_CODE];

extern const char ZZXP_S     [NULL_CODE];   /* System Only */
extern const char ZZXP_A     [NULL_CODE];   /* All */
extern const char ZZXP_M     [NULL_CODE];   /* Monitored */

#define ZZXPTT_S        1   /* System Only */
#define ZZXPTT_A        2   /* All */
#define ZZXPTT_M        3   /* Monitored */

#define _ZZXPTT_MAX 3



#endif
