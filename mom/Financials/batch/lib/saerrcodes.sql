SET DEFINE OFF;
SET PAGESIZE 32000;
SET LINESIZE 1000;
SET HEADING OFF;
SET FEEDBACK OFF;
SET TERMOUT OFF;
SET TRIMOUT ON;
SET TRIMSPOOL ON;

BREAK ON error_code SKIP 1;
COLUMN np NOPRINT;
COLUMN s NOPRINT;
COLUMN error_code NOPRINT;

SPOOL newh;

SELECT 1 np, '/* This is an automatically generated file. DO NOT EDIT. */'
  FROM DUAL
UNION
SELECT 2 np, '/* To make changes, update the  sa_error_codes table. */'
  FROM DUAL
UNION
SELECT 3 np, '/* Than run: make -f retek.mk saerrcodes.h */'
  FROM DUAL
ORDER BY 1;

SELECT 1 np, '#ifndef _ERROR_CODES_H'
  FROM DUAL
UNION
SELECT 2 np, '#define _ERROR_CODES_H'
  FROM DUAL
ORDER BY 1;

SELECT error_code, 1 s, '/* Problem: ' || error_desc || ' */'
  FROM sa_error_codes
 WHERE required_ind = 'Y'
UNION
SELECT error_code, 2 s, '/* Solution: ' || rec_solution || ' */'
  FROM sa_error_codes
 WHERE required_ind = 'Y'
UNION
SELECT error_code, 3 s, '#define ' || RPAD( error_code, 25, ' ') || ' "' || error_code || '"'
  FROM sa_error_codes
 WHERE required_ind = 'Y'
ORDER BY 1, 2;

SELECT '#endif'
  FROM DUAL;

SPOOL OFF;
QUIT;


