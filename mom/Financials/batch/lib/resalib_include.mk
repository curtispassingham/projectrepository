#--- Object file groupings.

RESALIB_C_OBJS = \
   saatoin.o \
   saatoln.o \
   sacode.o \
   sacodes.o \
   sadatetime.o \
   saemployee.o \
   saemptyfld.o \
   saerror.o \
   safldcpy.o \
   safxl2str.o \
   sagetrec.o \
   sainvstatus.o \
   saisleap.o \
   samerchcode.o \
   sapartner.o \
   saprimvariant.o \
   saprom.o \
   saputrec.o \
   saitem.o \
   sastoreday.o \
   sastorepos.o \
   sawh.o\
   sastr2fxl.o \
   sastrsig.o \
   sasupplier.o \
   satendertype.o \
   satrat.o \
   sarefitem.o \
   savaldate.o \
   savalnum.o \
   savaltime.o \
   savupc.o \
   sawaste.o \
   sabanner.o \
   sacurrency.o

RESALIB_PC_OBJS = \
   sacrstdy.o \
   sacvtcur.o \
   saftchstdy.o \
   saftchstdyerrcnt.o \
   saftchstdyexp.o \
   saftchstdyexpgl.o \
   saftchstdyexplike.o \
   saftchsysdate.o \
   saftchsysopt.o \
   saftchvatcalctype.o\
   saftchvdate.o \
   sagetbaltot.o \
   sagetconv.o \
   sagetlock.o \
   sagettot.o \
   sagettotlike.o \
   saincchar.o \
   samrkstdyexp.o \
   samrktotexp.o \
   samrktrexp.o \
   saretailtype.o \
   satotavail.o

RESALIB_C_SRCS = \
   $(RESALIB_C_OBJS:.o=.c)

RESALIB_PC_SRCS = \
   $(RESALIB_PC_OBJS:.o=.pc)

#--- Target groupings.

RESALIB_EXECUTABLES = 

RESALIB_LIBS = \
   libresa.a

RESALIB_INCLUDES = \
   sacodes.h \
   saerrcodes.h \
   salib.h \
   sastdlen.h

RESALIB_LN = \
   llib-lresa.ln

#--- Put list of all targets for resa.mk here

RESALIB_TARGETS = \
   $(RESALIB_EXECUTABLES) \
   $(RESALIB_EXECUTABLES:=.lint) \
   $(RESALIB_C_SRCS:.c=.o) \
   $(RESALIB_PC_SRCS:.pc=.o) \
   $(RESALIB_PC_SRCS:.pc=.c) \
   $(RESALIB_LIBS) \
   $(RESALIB_INCLUDES) \
   $(RESALIB_LN) \
   resalib-all \
   resalib-libs \
   resalib-includes \
   resalib-clobber \
   resalib-clean \
   resalib-install \
   resalib-lint \
   resalib-depend \
   resa

