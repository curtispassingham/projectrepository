/* getrec.c */

#include <retek.h>
#include "salib.h"


/*-------------------------------------------------------------------------
getrec(p, f) gets the next record from the file and puts it at p.
Returns TRUE if ok or FALSE if EOF.
-------------------------------------------------------------------------*/
int getrec(char *os_p, FILE *if_f)
{
     int li_c, li_n;

     li_n = 0;
     while((li_c = getc(if_f)) != EOF)
     {
          *os_p++ = li_c;
          if(li_c == '\n')
	  {
             return(TRUE);
	  } /* end if */
          if(++li_n >= MAXRECSIZE - 1)
          {
               /* terminate unknown record */
               *os_p = '\n';
               /* eat rest of it */
               while((li_c = getc(if_f)) != EOF)
	       {
                  if(li_c == '\n')
		  {
                     break;
		  } /* end if */
	       } /* end while */

               return(TRUE);
          } /* end if */
     } /* end while */

     return(FALSE);
} /* end getrec() */


