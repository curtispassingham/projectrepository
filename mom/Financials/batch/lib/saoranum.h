#ifndef _ORANUM_H_
#define _ORANUM_H_

/* oranum.h: API calls to OCI so as to mask the details from the user */

/* Reminder: Sufficient space must be allocated in the character arrays so as 
             to be able to handle the '-' for negative numbers */

#include <sql2oci.h>
#include <orl.h>
#include <stdio.h>

extern OCIEnv *oeh;
extern OCIError *err;

extern text errbuf[512];  /* size is taken from example in OCI Manual */
extern sb4 errcode;

/* il_size is the maximum size that the number can be, including space for a sign
   and space for null terminator */
/* Although the functions write a message to the error log, the calling 
   function should also write a message, so as to know in which function
   the error occurred. */

/* Initialize - Required before calling any of the other functions */
int OraNumInit();
/* Add: num3 = num1 + num2 */
int OraNumAdd(char *is_num1, char *is_num2, char *os_num3, long il_size);
/* Subtract: num3 = num1 - num2  */
int OraNumSub(char *is_num1, char *is_num2, char *os_num3, long il_size);
/* Multiply: num3 = num1 * num2  */
int OraNumMul(char *is_num1, char *is_num2, char *os_num3, long il_size);
/* Divide: num3 = num1 / num2  */
/* This function returns an integer value only, that is, 
   it returns the result to the nearest integer, 
   e.g. 1 is returned instead of 0.5 */
int OraNumDiv(char *is_num1, char *is_num2, char *os_num3, long il_size);
/* Multiply Oracle number by double */
int OraNumMuld(char *is_num1, double is_num2, char *os_num3, long il_size);
/* Round: Rounds an Oracle number to a specified decimal place */
/* num is rounded to decplace places right of decimal point to give result */
int OraNumRound( char *is_num, char *is_decplace, char *os_result, long il_result);
/* QtyRtl: Multiplies retail and quantity, divides by the multiplier, */
/* usually 10000, rounds to decplace */
int OraNumQtyRtl(char *is_num1, char *is_num2, char *os_num3, long il_size, char *is_decplace, char *is_multiplier);
/* Internal functions - do no use */
int OraNumFromString(OCINumber *o_ora_num, char *is_string);
int OraNumToString(OCINumber *i_ora_num, char *is_string, 
                   ub4 *io_string_len);
void write_oci_error(char *is_function, char *ios_error_string);

#endif /* _ORANUM_H_ */


