
/*=========================================================================

   sainvstatus.c: invstatus load and lookup

=========================================================================*/

#include "sagetref.h"

static INVSTATUSDAT *pa_invstatustable = NULL;  /* memory-mapped inv status table */
static size_t pl_invstatustablelen = 0;   /* number of inv status in invstatustable */

/* prototypes */
INVSTATUSDAT *invstatus_lookup(char *is_key);
static int invstatuscmp(const void *is_s1, const void *is_s2);
int invstatus_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
invstatus_lookup() checks the memory-mapped file for the inv status and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
INVSTATUSDAT *invstatus_lookup(char *is_key)
{
   char ls_key[NULL_RETURN_DISP];

   if (pl_invstatustablelen == 0)
   {
      return(NULL);
   }

   sprintf(ls_key,"%-*.*s",LEN_RETURN_DISP,LEN_RETURN_DISP,is_key);

   return((INVSTATUSDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_invstatustable,
           pl_invstatustablelen, sizeof(INVSTATUSDAT), invstatuscmp));
} /* end invstatus_lookup() */


/*-------------------------------------------------------------------------
invstatuscmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int invstatuscmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_RETURN_DISP));
} /* end invstatuscmp() */


/*-------------------------------------------------------------------------
invstatus_loadfile() loads the inv status data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int invstatus_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_invstatustable = (INVSTATUSDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ,
                        MAP_SHARED, li_datafile, 0 );
            if(pa_invstatustable != (INVSTATUSDAT *)MAP_FAILED)
            {
               pl_invstatustablelen = lr_statbuf.st_size / sizeof(INVSTATUSDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
         } /* end else */
      } /* end if */
      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end invstatus_loadfile() */


