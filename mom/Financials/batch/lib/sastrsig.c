/*-------------------------------------------------------------------------
strsig( s ) returns a 'signature' for the string, an integer value semi-
unique to the string. This signature can be stored along with the string
itself in binary trees, etc. When finding strings in the tree, doing the
compares int-to-int at first and only comparing the actual string when
required will significantly increase performance.

This also fills the requirements of a hash function for strings, if the
returned value is modulo hash-table-size.

From "A C++ Toolkit" by Jonathan Shapiro.
-------------------------------------------------------------------------*/

#ifdef SELFTEST
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
     char ls_string[1024];

     /* test from stdio stream of strings */
     while(gets(ls_string))
     {
         printf("%d : %s\n", strsig(ls_string), ls_string);
     } /* end while */

     return(EXIT_SUCCESS);
}
#endif

int strsig(char *is_string)
{
     int li_i, li_j, li_sig;

     li_sig = 0;
     li_i = 0;
     li_j = strlen(is_string) - 1;
     while (li_i < li_j)
     {
         li_sig = li_sig * 27 + is_string[li_i++];
         li_sig = li_sig * 27 + is_string[li_j--];
     } /* end while */

     return(li_sig);
}




