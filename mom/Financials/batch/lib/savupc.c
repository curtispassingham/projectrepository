/*=========================================================================

   savupc.c: vupc load and lookup

=========================================================================*/

#include "sagetref.h"

static VUPCDAT *pa_vupctable = NULL;  /* memory-mapped Variable UPC table */
static size_t pl_vupctablelen = 0;   /* number of items in vupctable */

/* prototypes */
VUPCDAT *vupc_lookup(char *is_key);
static int vupccmp(const void *is_s1, const void *is_s2);
int vupc_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
vupc_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
VUPCDAT *vupc_lookup(char *is_key)
{
   char ls_key[NULL_FORMAT_ID];

   if(pl_vupctablelen == 0)
   {
      return(NULL);
   } /* end if */

   sprintf(ls_key, "%-*.*s", LEN_FORMAT_ID, LEN_FORMAT_ID, is_key);

   return((VUPCDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_vupctable,
           pl_vupctablelen, sizeof(VUPCDAT), vupccmp));
} /* end vupc_lookup() */


/*-------------------------------------------------------------------------
vupccmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int vupccmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_FORMAT_ID));
} /* end vupccmp() */


/*-------------------------------------------------------------------------
vupc_loadfile() loads the Variable UPC data file into memory. Returns TRUE
if successor FALSE if not.
-------------------------------------------------------------------------*/
int vupc_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_vupctable = (VUPCDAT *)mmap(NULL, lr_statbuf.st_size, PROT_READ, MAP_SHARED, li_datafile, 0);
            if(pa_vupctable != (VUPCDAT *)MAP_FAILED)
            {
               pl_vupctablelen = lr_statbuf.st_size / sizeof(VUPCDAT);
               li_rv = TRUE;
            } /* end if */
         } /* end if */
         else
	 {
            li_rv = TRUE;
	 } /* end else */
      } /* end if */

      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end vupc_loadfile() */


