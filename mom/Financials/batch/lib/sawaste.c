/*=========================================================================

   sawaste.c: waste load and lookup

=========================================================================*/

#include "sagetref.h"

static WASTEDAT *pa_wastetable = NULL;  /* memory-mapped waste table */
static size_t pl_wastetablelen = 0;   /* number of items in wastetable */

/* prototypes */
WASTEDAT *waste_lookup(char *is_key);
static int wastecmp(const void *is_s1, const void *is_s2);
int waste_loadfile(char *is_filename);


/*-------------------------------------------------------------------------
waste_lookup() checks the memory-mapped file for the item and returns a
pointer to the matching data structure, or NULL if not found.
-------------------------------------------------------------------------*/
WASTEDAT *waste_lookup(char *is_key)
{
   char ls_key[NULL_ITEM];

   if(pl_wastetablelen == 0)
   {
      return(NULL);
   } /* end if */

   sprintf(ls_key, "%-*.*s", LEN_ITEM, LEN_ITEM, is_key);

   return((WASTEDAT *)bsearch(
          (const void *)ls_key, (const void *)pa_wastetable,
           pl_wastetablelen, sizeof(WASTEDAT), wastecmp));
} /* end waste_lookup() */


/*-------------------------------------------------------------------------
wastecmp() is a compare routine for bsearch.
-------------------------------------------------------------------------*/
static int wastecmp(const void *is_s1, const void *is_s2)
{
   return(strncmp((char *)is_s1, (char *)is_s2, LEN_ITEM));
} /* end wastecmp() */


/*-------------------------------------------------------------------------
waste_loadfile() loads the waste data file into memory. Returns TRUE if success
or FALSE if not.
-------------------------------------------------------------------------*/
int waste_loadfile(char *is_filename)
{
   int li_datafile, li_rv;
   struct stat lr_statbuf;

   li_rv = FALSE;
   if((li_datafile = open(is_filename, O_RDONLY)) != -1)
   {
      if(fstat(li_datafile, &lr_statbuf) == 0)
      {
         if(lr_statbuf.st_size != 0)
         {
            pa_wastetable = (WASTEDAT *)mmap( NULL, lr_statbuf.st_size, PROT_READ, MAP_SHARED, li_datafile, 0);
            if(pa_wastetable != (WASTEDAT *)MAP_FAILED)
            {
               pl_wastetablelen = lr_statbuf.st_size / sizeof(WASTEDAT);
               li_rv = TRUE;
            } /* end if */
         }
         else
	 {
            li_rv = TRUE;
	 } /* end else */
      } /* end if */

      close(li_datafile);
   } /* end if */

   return(li_rv);
} /* end waste_loadfile() */


