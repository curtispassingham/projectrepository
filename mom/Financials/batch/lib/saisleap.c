/* isleapyear( year ) */

#include <retek.h>
#include "salib.h"

/* isleapyear() checks the year and returns TRUE/FALSE */
int isleapyear(int ii_year)
{
     if(ii_year % 4 == 0)
     {
         if(ii_year % 100 == 0)
         {
             if(ii_year % 400 == 0)
	     {
                return(TRUE);
             } /* end if */
             else 
             {
                return(FALSE);
             } /* end else */
         } /* end if */
         else
         {
            return(TRUE);
	 } /* end else */
     } /* end if */
     else 
     {
        return(FALSE);
     } /* end else */

} /* end isleapyear() */


