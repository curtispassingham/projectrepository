#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  salesgenrej.ksh
#
#  Desc:  UNIX shell script to create reject files from the table SVC_POSUPLD_REJ_RECS  
#  table.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='salesgenrej.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date


LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
EXECFILE="${MMHOME}/oracle/proc/src/gen_rej_file"

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <reject file> <process ID> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <input file>       Reject file to be created. Can include directory path.

   <process ID>       Process ID of chunk of data to be processed. 
"
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_result         NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_result := 0;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`
    
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: SVC_GEN_REJ
# Purpose      : This will create the reject files.
#-------------------------------------------------------------------------
function SVC_GEN_REJ
{
   sqlTxt="
      DECLARE
         L_str_error_tst   VARCHAR2(1) := NULL;

         FUNCTION_ERROR    EXCEPTION;
         
      BEGIN

        if NOT CORESVC_SALES_UPLOAD_SQL.GENERATE_REJ_FILE ( $2,
                                                            :GV_script_error)then
            raise FUNCTION_ERROR;
        end if;      
         
        COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   
   EXEC_SQL ${sqlTxt} 
    
   if [[ $? -ne ${OK} ]]; then
        echo "CORESVC_SALES_UPLOAD_SQL.GENERATE_REJ_FILE Process id: $2 Failed" >>${ERRORFILE}
        return ${FATAL}
   fi

   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} @${EXECFILE} $1 $2
   
   if [[ $? -ne ${OK} ]]; then
        echo "CORESVC_SALES_UPLOAD_SQL.GENERATE_REJ_FILE Failed" >>${ERRORFILE}
        return ${FATAL}
     else
        LOG_MESSAGE "CORESVC_SALES_UPLOAD_SQL.GENERATE_REJ_FILE - Successfully Completed" "SVC_GEN_REJ" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        return ${OK}
   fi

}


#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------

if [ $# -lt 3 ]
then
   USAGE
   exit 1;
fi


#Validate input parameters
CONNECT=$1

if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "salesgenrej.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

CHECK_FILENAME $2 ${ERRORFILE} ${LOGFILE} ${pgmName}
if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
fi

#--- Invoke Archive Function
SVC_GEN_REJ $2 $3
if [[ $? -ne ${OK} ]]; then
   return ${FATAL}
fi

sqlTxt="
      DECLARE
         L_table_owner       USER_SYNONYMS.TABLE_OWNER%TYPE;
         L_table_name        USER_SYNONYMS.TABLE_NAME%TYPE;
         cursor C_GET_TABLE_OWNER is
            select table_owner
              from user_synonyms
             where synonym_name = L_table_name;

      BEGIN
         L_table_name := 'SVC_POSUPLD_STAGING';
         open C_GET_TABLE_OWNER;
         fetch C_GET_TABLE_OWNER into L_table_owner;
         close C_GET_TABLE_OWNER;
         if L_table_owner is not null then
            EXECUTE IMMEDIATE 'truncate table '||L_table_owner||'.'||' svc_posupld_staging';
         else
            EXECUTE IMMEDIATE 'truncate table svc_posupld_staging';
         end if;

      EXCEPTION
         when OTHERS then
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   
EXEC_SQL ${sqlTxt} 
   
if [[ $? -ne ${OK} ]]; then
        return ${FATAL}
fi

#Delete the staging rejects once the rejects are processed.

sqlTxt="
      DECLARE

      BEGIN
         DELETE FROM svc_posupld_staging_rej WHERE sales_process_id = $3;
         COMMIT;

      EXCEPTION
         when OTHERS then
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   
EXEC_SQL ${sqlTxt}
if [[ $? -ne ${OK} ]]; then
   return ${FATAL}
fi

# --  Check for any reject files created
if [ `grep "FHEAD" $2 | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Reject file created" "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
else
   rm -f $2
fi

# --  Check for any Oracle errors from the SQLPLUS process
if [ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. See error file" "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "Program salesgenrej.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

exit 0
