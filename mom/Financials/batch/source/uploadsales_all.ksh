#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  salesprocess.ksh
#
#  Desc:  UNIX shell script to process sales information uploaded
#  into POSUPLD* tables. Wrapper script for CORESVC_SALES_UPLOAD_SQL.PROCESS_SALES
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='uploadsales_all.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <directory path>  &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <direcory path> optional parameter"
}

#-------------------------------------------------------------------------
# Function Name: GET_CHUNKS
# Purpose      : populates thre restart_arr array..
#-------------------------------------------------------------------------

function GET_CHUNKS
{

set -A restart_arr `ls -a ${inputDir}/POSU*`

}

#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------
 
# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit 1
elif [ $# -lt 2 ]
then
   inputDir=$HOME
else
   inputDir=$2
fi

UP=$1
if [[ -n ${UP%/*} ]]; then
   USER=${UP%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${UP}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "uploadsales_all.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#------------------------------------------------------------
# Initialize number of parallel threads.
# Retrieve from the rms_plsql_batch_config table
#------------------------------------------------------------
parallelThreads=""
parallelThreads=`$ORACLE_HOME/bin/sqlplus -s $UP  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select max_concurrent_threads
  from rms_plsql_batch_config
 where program_name = 'CORESVC_SALES_UPLOAD_SQL';
exit;
EOF`
# Check the number of threads
if [ -z $parallelThreads ]; then
   LOG_ERROR "Unable to retrieve the number of threads from the rms_plsql_batch_config table." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
else
   LOG_MESSAGE "The number of concurrent threads for processing input file ${inputFile} is ${parallelThreads}." "" ${OK} ${LOGFILE}  ${pgmName} ${pgmPID}
fi


#------------------------------------------------------------
# Get Process ID and Chunk ID for POSU input file.
#------------------------------------------------------------
GET_CHUNKS 

#------------------------------------------------------------------------------
# Start to invoke and process sales based on Process ID and Chunk ID
#------------------------------------------------------------------------------

num_chunks=${#restart_arr[*]}

if [ num_chunks -lt 1 ]; then
  LOG_MESSAGE "No chunks to be processed for this file: $2" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
  if [ `wc -l $ERRORFILE | awk '{print $1}'` -eq 0 ]
  then
     rm -f $ERRORFILE
  fi
  exit 0
fi

arr_index=0


while [ $arr_index -lt  num_chunks ]
do  
 if [ `jobs | wc -l` -lt ${parallelThreads} ]
   then
      (  
         ${MMHOME}/oracle/proc/bin/uploadsales.ksh $UP ${restart_arr[arr_index]}    
      ) &
   else
      # Loop until a thread becomes available
      while [ `jobs | wc -l` -ge ${parallelThreads} ]
      do
         sleep 1
      done
      ( 
         ${MMHOME}/oracle/proc/bin/uploadsales.ksh $UP ${restart_arr[arr_index]}   
      ) &
   fi        
  let arr_index=$arr_index+1;
done

# Wait for all threads to complete
wait               

# Check for any Oracle errors from the SQLPLUS process
if [ `grep "${pgmName}: PID=${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. See error file." "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "Program uploadsales_all.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#-----------End of Processing -------------------------------------------------

exit 0
