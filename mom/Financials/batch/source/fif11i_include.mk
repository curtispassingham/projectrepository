#--- Object file groupings.

FIFGLDN1_PC_OBJS = fifgldn1.o

FIFGLDN2_PC_OBJS = fifgldn2.o

FIFGLDN3_PC_OBJS = fifgldn3.o

FIF_C_SRCS = 

FIF_PC_SRCS = \
	$(FIFGLDN1_PC_OBJS:.o=.pc) \
	$(FIFGLDN2_PC_OBJS:.o=.pc) \
	$(FIFGLDN3_PC_OBJS:.o=.pc)

#--- Target groupings.

FIF_GEN_EXECUTABLES= \
	fifgldn1 \
	fifgldn2 \
	fifgldn3

FIF_EXECUTABLES= \
	$(FIF_GEN_EXECUTABLES)

FIF_LIBS = 

FIF_INCLUDES = 

#--- Put list of all targets for fif.mk here

FIF_TARGETS = \
	$(FIF_EXECUTABLES) \
	$(FIF_EXECUTABLES:=.lint) \
	$(FIF_C_SRCS:.c=.o) \
	$(FIF_PC_SRCS:.pc=.o) \
	$(FIF_PC_SRCS:.pc=.c) \
	$(FIF_LIBS) \
	$(FIF_INCLUDES) \
	fif-all \
	fif \
	fif-ALL \
	fif-libs \
	fif-includes \
	fif-clobber \
	fif-libchange \
	fif-clean \
	fif-install \
	fif-lint \
	fif-depend
