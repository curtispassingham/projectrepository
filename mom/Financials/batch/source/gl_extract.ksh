#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  gl_extract.ksh
#
#  Desc:  UNIX shell script to extract GL data.
#-------------------------------------------------------------------------
. ${RETAIL_HOME}/oracle/lib/src/rmsksh.lib
pgmName='gl_extract.ksh'
pgmName=${pgmName##*/}               # remove the path
pgmExt=${pgmName##*.}                # get the extension
pgmName=${pgmName%.*}                # get the program name
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
LOGFILE="${RETAIL_HOME}/log/$exeDate.log"
ERRORFILE="${RETAIL_HOME}/error/err.$pgmName.$exeDate.$pgmPID"
OUTGOING_DIR=`pwd`
OUT_DIR=`pwd`
EXTRACTFILE="${OUT_DIR}/GL_EXTRACT_${extractDate}.dat"
OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string>
   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.  "
}

#-------------------------------------------------------------------------
# Function Name: remove_extract_file
# Purpose      : Remove the extracted file
#-------------------------------------------------------------------------
function remove_extract_file
{
 rm -f $EXTRACTFILE
 retval=$?
 if [[ retval -ne 0 ]]; then
    LOG_ERROR "RM command failed . Delete the ${EXTRACTFILE} File Manually" "remove_extract_file" ${retval} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
 fi
 return $retval
}

#-------------------------------------------------------------------------
# Function Name: EXTRACT_DATA
# Purpose      : Export GL records
#-------------------------------------------------------------------------

function EXTRACT_DATA
{
 $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$EXTRACTFILE
    set pause off
    set echo off
    set heading off
    set feedback off
    set verify off
    set linesize 1000;
    set pages 0
	WHENEVER SQLERROR EXIT 1
    SELECT SET_OF_BOOKS_ID                  || ',' ||
           ACCOUNTING_DATE                  || ',' ||
           CURRENCY_CODE                    || ',' ||
           STATUS                           || ',' ||
           DATE_CREATED                     || ',' ||
           CREATED_BY                       || ',' ||
           ACTUAL_FLAG                      || ',' ||
           USER_JE_CATEGORY_NAME            || ',' ||
           USER_JE_SOURCE_NAME              || ',' ||
           CURRENCY_CONVERSION_DATE         || ',' ||
           CURRENCY_CONVERSION_TYPE         || ',' ||
           ACCT_SEGMENT1                    || ',' ||
           ACCT_SEGMENT2                    || ',' ||
           ACCT_SEGMENT3                    || ',' ||
           ACCT_SEGMENT4                    || ',' ||
           ACCT_SEGMENT5                    || ',' ||
           ACCT_SEGMENT6                    || ',' ||
           ACCT_SEGMENT7                    || ',' ||
           ACCT_SEGMENT8                    || ',' ||
           ACCT_SEGMENT9                    || ',' ||
           ACCT_SEGMENT10                   || ',' ||
           ENTERED_DR_AMOUNT                || ',' ||
           ENTERED_CR_AMOUNT                || ',' ||
           TRANSACTION_DATE                 || ',' ||
           REFERENCE1                       || ',' ||
           REFERENCE2                       || ',' ||
           REFERENCE3                       || ',' ||
           REFERENCE4                       || ',' ||
           REFERENCE5                       || ',' ||
           ATTRIBUTE1                       || ',' ||
           ATTRIBUTE2                       || ',' ||
           ATTRIBUTE3                       || ',' ||
           ATTRIBUTE4                       || ',' ||
           ATTRIBUTE5                       || ',' ||
           ATTRIBUTE6                       || ',' ||
           PERIOD_NAME                      || ',' ||
           CODE_COMBINATION_ID              || ',' ||
           PGM_NAME                         || ',' ||
           ACCT_SEGMENT11                   || ',' ||
           ACCT_SEGMENT12                   || ',' ||
           ACCT_SEGMENT13                   || ',' ||
           ACCT_SEGMENT14                   || ',' ||
           ACCT_SEGMENT15                   || ',' ||
           ACCT_SEGMENT16                   || ',' ||
           ACCT_SEGMENT17                   || ',' ||
           ACCT_SEGMENT18                   || ',' ||
           ACCT_SEGMENT19                   || ',' ||
           ACCT_SEGMENT20                   || ',' ||
           REFERENCE_TRACE_ID               || ',' ||
           PRIM_CURRENCY_CODE               || ',' ||
           PRIM_ENTERED_DR_AMOUNT           || ',' ||
           PRIM_ENTERED_CR_AMOUNT           || ',' ||
           FIN_GL_SEQ_ID                    || ',' ||
           PROCESSED_FLAG
    FROM     STG_FIF_GL_DATA;
EOF
err=$?

if [[ `grep "^ORA-" $EXTRACTFILE | wc -l` -gt 0 ]]; then
    cat $EXTRACTFILE >> ${ERRORFILE}
    remove_extract_file
	remove_extract_file_status=$?
    if [[ $? -ne 0 ]]; then
        LOG_ERROR "remove_extract_file() failed" "EXTRACT_DATA" ${remove_extract_file_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
	fi
fi
return ${err}
}

#-------------------------------------------------------------------------
# Function Name: VALIDATE_EXTRACT
# Purpose      : Checks the record count file and table
#-------------------------------------------------------------------------

function VALIDATE_EXTRACT
{
SQL_LOG=`pwd`/validate_extract_log
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF > $SQL_LOG
set feedback off
variable row_count number;
variable record_status char(255);
exec :row_count:=$1;
exec :record_status:=null;
whenever SQLERROR EXIT 1
declare
   cursor C_stg_fif_gl_count
    is
   select count(*)
     from    STG_FIF_GL_DATA;
   L_number number(32):=NULL;
begin
   open C_stg_fif_gl_count;
   fetch C_stg_fif_gl_count into L_number;
   close C_stg_fif_gl_count;
   if ( :row_count= L_number)
   then
      :record_status:='Records in File and table Match';
   else
      :record_status:='Records in File and table missmatch'||' File count:'||:row_count||' Table count:'||L_number||'.';
   end if;
end;
/
print :record_status
EOF
err=$?
remove_extract_file_ind="N";
if [[ `grep "^ORA-" $SQL_LOG | wc -l` -gt 0 ]]; then
    LOG_ERROR "validate_extract() failed" "VALIDATE_EXTRACT" ${err} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    cat $SQL_LOG >> $ERRORFILE
	remove_extract_file_ind="Y"
fi

if [[ `grep "Records in File and table missmatch" $SQL_LOG|wc -l ` -gt 0 ]]; then
    cat $SQL_LOG >> $ERRORFILE
	remove_extract_file_ind="Y"
    err=$FATAL
fi

if [[ $remove_extract_file_ind = "Y" ]]; then
     remove_extract_file
     remove_extract_file_status=$?
    if [[ $remove_extract_file_status -ne 0 ]]; then
        LOG_ERROR "remove_extract_file() failed" "EXTRACT_DATA" ${remove_extract_file_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
	fi
    err=$FATAL
fi
rm $SQL_LOG	
return $err
}
#-------------------------------------------------------------------------
# Function Name: move_file
# Purpose      : Moves the extracted file to the Outgoing directory
#-------------------------------------------------------------------------
move_file()
{
  mv  $EXTRACTFILE $OUTGOING_DIR
  retval=$?
  if [ $retval -ne 0 ]; then
    echo "Move $EXTRACTFILE $OUTGOING_DIR failed"
  fi
  return $retval
}

######################################################################


#-------------------------------------------------------------------------
# Function Name: Delete GL DATA
# Purpose      : Remove the data which are already extracted by batch
#-------------------------------------------------------------------------
function DELETE_GL_DATA
{
#SQL_LOG=$RETAIL_HOME/data/temp/delete_gl_data_log
SQL_LOG=`pwd`/delete_gl_data_log
$ORACLE_HOME/bin/sqlplus -s  ${CONNECT} <<EOF  > $SQL_LOG
set feedback off
variable t_return_code number;
exec :t_return_code:=0;
set serveroutput on
DECLARE
   error_message   VARCHAR2(255);
   l_table_schema  VARCHAR2(255);
   l_statement     VARCHAR2(1000);
   cursor  c_table_schema  is 
     select table_owner from system_options;
BEGIN
    open c_table_schema;
    fetch c_table_schema into l_table_schema;
    close c_table_schema;
    l_statement:='TRUNCATE TABLE '||l_table_schema||'.STG_FIF_GL_DATA';
    EXECUTE IMMEDIATE l_statement;
EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line('FATAL: ' || SQLERRM);
      :t_return_code := -1;
END;
/
EXIT :t_return_code
EOF
err=$?
if [[ `grep "ORA-" $SQL_LOG| wc -l` -gt 0 ]]; then
    cat $SQL_LOG >> ${ERRORFILE}
    remove_extract_file
	remove_extract_file_status=$?
    if [[ $remove_extract_file_status -ne 0 ]]; then
        LOG_ERROR "remove_extract_file() failed" "" ${remove_extract_file_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    fi
fi
rm $SQL_LOG
return $err
}
#-----------------------------------------------------------------
#                     MAIN
#-----------------------------------------------------------------
# Validate the number of arguments

if [[ $# -ne 1 ]]; then
   USAGE
   exit 1
fi
########## Check Environment Variables ##########
if [ -z "$RETAIL_HOME" ]
then
   LOG_ERROR "RMS - Environment variable RETAIL_HOME not set - Exiting " "" "" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
fi
#########Checking Directory########
if [ ! -d "$OUT_DIR" ] || [ ! -w $OUT_DIR ]
then
   LOG_ERROR "Out directory doesn't exist or No permission to write to the directory " "" "" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
fi

if [ ! -d "$OUTGOING_DIR" ] || [ ! -w $OUTGOING_DIR ]
then
   LOG_ERROR "Outgoing directory doesn't exist or No permission to write to the directory " "" "" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
fi

##########################################
CONNECT=$1
conncheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${con_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "gl_extract.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#Retrieve No of records in STG_FIF_GL_DATA table 
tab_record_count=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select count(*) 
  from stg_fif_gl_data;
exit;
EOF`
tab_record_status=$?
if [[ $tab_record_status -ne 0 ]]; then
    LOG_ERROR "Failed while fetching Record count" "GL_RECORD_COUNT" ${FATAL} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    exit ${FATAL}
fi
   
if [[ $tab_record_count -eq 0 ]]; then
   LOG_MESSAGE "No data in GL table. File not created." "" "" ${LOGFILE} ${pgmName} ${pgmPID}
   LOG_MESSAGE "gl_extract.ksh - completed successfully by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit ${OK}
fi

EXTRACT_DATA
retval=$?
if [[ $retval -ne 0 ]]; then
    LOG_ERROR "Job terminated with fatal error." "EXTRACT_DATA" ${FATAL} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    exit ${FATAL}
fi

record_count=`wc -l $EXTRACTFILE`
record_count_status=$?
if [[ $record_count_status -ne 0 ]]; then
    remove_extract_file
    LOG_ERROR "Command to count the no of rows extracted failed" "" ${FATAL} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
	remove_extract_file_status=$?
	LOG_ERROR "remove_remove_extract_fileextract_file() Failed " "remove_extract_file()" ${FATAL} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
	exit ${FATAL}
fi

VALIDATE_EXTRACT $record_count
retval=$?
if [[ $retval -ne 0 ]]; then
    LOG_ERROR "Job terminated with fatal error." "VALIDATE_EXTRACT" ${FATAL} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    exit ${FATAL}
fi

DELETE_GL_DATA
retval=$?
if [[ $retval -ne 0 ]]; then
    LOG_ERROR "Job terminated with fatal error" "DELETE_GL_DATA" ${FATAL} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    exit ${FATAL}
fi
#move_file
#retval=$?
#if [[ $retval -ne 0 ]]; then
#LOG_ERROR "Job terminated with fatal error." "move_file" ${FATAL} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
#    exit ${FATAL}
#else
LOG_MESSAGE "gl_extract.ksh.ksh - Completed Successfully by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
exit ${OK}
#fi
