
-- GEN_POS_FILE.SQL
--  This script will generate posu file given a particular store. 


set PAGESIZE 32000;
set LINESIZE 31000;
set tab off;
set HEADING OFF;
set FEEDBACK OFF;
set NEWPAGE NONE;
set TERMOUT OFF;
set SHOWMODE OFF;
set AUTOPRINT OFF;
set VERIFY OFF;
set TRIMOUT ON;
set TRIMSPOOL ON;
set serveroutput ON;


-- specify data dir if needed, default directory is where the script is executed.         
spool &1

--  GENERATE POSU reject files

   select line_text from svc_posupld_rej_recs where sales_process_id = &2 order by line_seq_id;
            




spool off;

exit;
