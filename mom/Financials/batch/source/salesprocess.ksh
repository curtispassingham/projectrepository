#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  salesprocess.ksh
#
#  Desc:  UNIX shell script to process sales information uploaded
#  into POSUPLD* tables. Wrapper script for CORESVC_SALES_UPLOAD_SQL.PROCESS_SALES
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='salesprocess.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
traceLog="${MMHOME}/log/${pgmName}_${pgmPID}_tracefiles.log"

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <sql trace> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <sql trace> optional parameter. (valid values are:
               SQL_TRACE1,  -- no binds and waits
               SQL_TRACE4,  -- trace with binds
               SQL_TRACE8,  -- trace with waits
            or SQL_TRACE12).-- trace witn binds and waits

"
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_result         NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_result := 0;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL " ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GET_CHUNKS
# Purpose      : Populates thre restart_arr array.
#-------------------------------------------------------------------------

function GET_CHUNKS
{

set -A restart_arr `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off

select distinct chunk_id
  from svc_posupld_status sps
 where location = '$1'
   and status = 'N'
   and chunk_id is not NULL
   and exists (select 1
                from svc_posupld_staging svc
                where sps.process_id = svc.sales_process_id
                  and location = '$1'
                  and rownum = 1)
   order by chunk_id;
exit;
EOF`

}
#-------------------------------------------------------------------------
# Function Name: CHUNK_STORE
# Purpose      : Divides the data of store passed into different chunks
#-------------------------------------------------------------------------

function CHUNK_STORE
{

   sqlTxt="
      DECLARE

         FUNCTION_ERROR  EXCEPTION;

         cursor C_LOCK_STATUS_ERR is
            select 1
              from svc_posupld_status sps
             where sps.location = $1
               and exists (select 1
                             from svc_posupld_staging svc
                            where sps.process_id = svc.sales_process_id
                              and rownum = 1)
               for update nowait;

      BEGIN
         if '${SQL_TRACE}' = 'SQL_TRACE0' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET SQL_TRACE=FALSE';
         elsif '${SQL_TRACE}' = 'SQL_TRACE1' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET EVENTS ''10046 trace name context forever, level 1'' TIMED_STATISTICS=TRUE tracefile_identifier=${pgmName}_store_$1';
         elsif '${SQL_TRACE}' = 'SQL_TRACE4' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET EVENTS ''10046 trace name context forever, level 4'' TIMED_STATISTICS=TRUE tracefile_identifier=${pgmName}_store_$1';
         elsif '${SQL_TRACE}' = 'SQL_TRACE8' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET EVENTS ''10046 trace name context forever, level 8'' TIMED_STATISTICS=TRUE tracefile_identifier=${pgmName}_store_$1';
         elsif '${SQL_TRACE}' = 'SQL_TRACE12' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET EVENTS ''10046 trace name context forever, level 12'' TIMED_STATISTICS=TRUE tracefile_identifier=${pgmName}_store_$1';
         else
            EXECUTE IMMEDIATE 'ALTER SESSION SET SQL_TRACE=FALSE';
         end if;

         if NOT CORESVC_SALES_UPLOAD_SQL.CHUNK_STORE( :GV_script_error,
                                                      $1
                                                     )then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
        LOG_ERROR "CORESVC_SALES_UPLOAD_SQL.CHUNK_STORE Location:$1 Failed" "CHUNK_STORE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
        exit ${FATAL}
   else
        LOG_MESSAGE "Chunking of Location $1 - Successfully Completed" "CHUNK_STORE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        return ${OK}
   fi

}

#-------------------------------------------------------------------------
# Function Name: RANGE_ITEMLOC
# Purpose      : Ranges missing item-locs
#-------------------------------------------------------------------------

function RANGE_ITEMLOC
{
   sqlTxt="
      DECLARE

         FUNCTION_ERROR  EXCEPTION;

      BEGIN
         if '${SQL_TRACE}' = 'SQL_TRACE0' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET SQL_TRACE=FALSE';
         elsif '${SQL_TRACE}' = 'SQL_TRACE1' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET EVENTS ''10046 trace name context forever, level 1'' TIMED_STATISTICS=TRUE tracefile_identifier=${pgmName}_itemloc';
         elsif '${SQL_TRACE}' = 'SQL_TRACE4' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET EVENTS ''10046 trace name context forever, level 4'' TIMED_STATISTICS=TRUE tracefile_identifier=${pgmName}_itemloc';
         elsif '${SQL_TRACE}' = 'SQL_TRACE8' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET EVENTS ''10046 trace name context forever, level 8'' TIMED_STATISTICS=TRUE tracefile_identifier=${pgmName}_itemloc';
         elsif '${SQL_TRACE}' = 'SQL_TRACE12' then
            EXECUTE IMMEDIATE 'ALTER SESSION SET EVENTS ''10046 trace name context forever, level 12'' TIMED_STATISTICS=TRUE tracefile_identifier=${pgmName}_itemloc';
         else
            EXECUTE IMMEDIATE 'ALTER SESSION SET SQL_TRACE=FALSE';
         end if;         
         
         if NOT CORESVC_SALES_UPLOAD_SQL.RANGE_ITEM_LOC ( :GV_script_error
                                                         )then
            raise FUNCTION_ERROR;
         end if;

         if NOT TRUNCATE_TABLE(:GV_script_error,
                               'SVC_POSUPLD_NIL') then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "CORESVC_SALES_UPLOAD_SQL.RANGE_ITEM_LOC Failed." "RANGE_ITEMLOC" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      exit ${FATAL}
   else
      LOG_MESSAGE "Ranging of missing Item-Locs - Successfully Completed" "RANGE_ITEMLOC" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi
}

#-------------------------------------------------------------------------
# Function Name: PROCESS
# Purpose      : This funciton process one store at a time.
#                It does the chunking and then it selects all the chunks belonging to the 
#                store and then calls PROCESS_CHUNK
#-------------------------------------------------------------------------

function PROCESS
{
   CHUNK_STORE $1
   if [ $? = ${FATAL} ]; then
      exit ${FATAL};
   fi   

   # Check if the all the File headers and trailers of the location are valid before processing
   set -A process_id `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off

   select distinct process_id
     from svc_posupld_status sps
    where status = 'N'
      and location = $1
      and exists (select 1
                    from svc_posupld_staging svc
                   where sps.process_id = svc.sales_process_id
                     and svc.location = $1);
   exit;
   EOF`
   
   
   num_process=${#process_id[*]}
   
   arr_ind_process=0;
   while [ $arr_ind_process -lt num_process ]
   do

      CHECK_FHEAD ${process_id[arr_ind_process]}
      CHECK_FTAIL ${process_id[arr_ind_process]}
      let arr_ind_process=$arr_ind_process+1;
      
   done 
   
   GET_CHUNKS $1

   num_chunks=${#restart_arr[*]}

   if [ num_chunks -lt 1 ]; then
      LOG_MESSAGE "No chunks to be processed. Store id: $1" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      sqlTxt="
         DECLARE

            cursor C_LOCK_STATUS_ERR is
               select 1
                 from svc_posupld_status
                where location = $1
                  for update nowait;

         BEGIN
            open C_LOCK_STATUS_ERR;
            close C_LOCK_STATUS_ERR;

            update svc_posupld_status sps
               set status = 'C'
             where location = $1
               and status != 'E'
               and exists (select 1
                             from svc_posupld_staging svc
                            where sps.process_id = svc.sales_process_id
                              and svc.location = $1
                              and rownum = 1); 
        
             COMMIT;       

         EXCEPTION
           when OTHERS then
              ROLLBACK;
              :GV_script_error := SQLERRM;
              open  C_LOCK_STATUS_ERR;
              close C_LOCK_STATUS_ERR;

              update svc_posupld_status sps
                     set status = 'E',
                         error_msg = rtrim(:GV_script_error)||' Location: '||$1
                    where location = $1
                      and exists (select 1
                                    from svc_posupld_staging svc
                                   where sps.process_id = svc.sales_process_id
                                     and svc.location = $1
                                     and rownum = 1);

              COMMIT;
              :GV_return_code := ${FATAL};
         END;"

      EXEC_SQL "${sqlTxt}"

   fi
   
   # Process each chunk belonging to the location. Each chunk can have parts of multiple files.
   arr_index=0
   while [ $arr_index -lt  num_chunks ]
   do
      if [ `jobs | wc -l` -lt ${parallelThreads} ]
      then
      (
        PROCESS_CHUNK $1 ${restart_arr[arr_index]}
      ) &
      else
         # Loop until a thread becomes available
         while [ `jobs | wc -l` -ge ${parallelThreads} ]
         do
            sleep 1
         done
         (
           PROCESS_CHUNK $1 ${restart_arr[arr_index]}
         ) &
      fi
     let arr_index=$arr_index+1;
   done

   # Wait for all threads to complete
   wait

sqlTxt="
      DECLARE

         cursor C_LOCK_STATUS_ERR is
            select 1
              from svc_posupld_status
             where location = $1
               for update nowait;

         cursor C_LOCK_STAGING is
            select 1
              from svc_posupld_staging
             where location = $1
               for update nowait;

      BEGIN
        update svc_posupld_status sps
               set status = 'E',
                   error_msg = 'CHUNK_NOT_PROCESS'
             where location = $1
               and status = 'N'
               and exists (select 1
                             from svc_posupld_staging svc
                            where sps.process_id = svc.sales_process_id
                              and svc.location = $1
                              and sps.chunk_id=svc.chunk_id);
                              
         insert into svc_posupld_staging_rej (file_type,
                                              filename,
                                              line_seq_id,
                                              thread_val,
                                              location,
                                              sales_process_id,
                                              chunk_id,
                                              thead_id,
                                              head_detl_id,
                                              tdetl_id,
                                              error_msg)
         select sps.file_type,
                sps.filename,
                sps.line_seq_id,
                sps.thread_val,
                sps.location,
                sps.sales_process_id,
                sps.chunk_id,
                sps.thead_id,
                sps.head_detl_id,
                sps.tdetl_id,
                svl.error_msg
           from svc_posupld_staging sps,
                svc_posupld_load svl
          where sps.location = $1
            and sps.sales_process_id = svl.sales_process_id
            and sps.line_seq_id = svl.line_seq_id
            and sps.file_type = svl.file_type
	    and exists (select 1
	                  from svc_posupld_status sp
              	         where sp.location = sps.location
			   and sp.process_id = sps.sales_process_id
			   and sp.status in ('E','R')
			   and rownum = 1);
			   
         open C_LOCK_STAGING;
         close C_LOCK_STAGING;

         delete svc_posupld_staging
          where location = $1;
          
	 COMMIT;          

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            open  C_LOCK_STATUS_ERR;
            close C_LOCK_STATUS_ERR;
            update svc_posupld_status sps
                   set status = 'E',
                       error_msg = rtrim(:GV_script_error)||' Location: '||$1
                  where location = $1
                    and exists (select 1
                                  from svc_posupld_staging svc
                                 where sps.process_id = svc.sales_process_id
                                   and rownum = 1);


            COMMIT;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL "${sqlTxt}"

   if [[ $? -ne ${OK} ]]; then
        LOG_ERROR "Location:$1 Thread: $threadVal Failed." "PROCESS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_CHUNK
# Purpose      : This funciton process one chunk at a time. 
#                It selects all the process id belonging to the chunk and then calls PROCESS_SALES
#-------------------------------------------------------------------------

function PROCESS_CHUNK
{
   set -A process_id_chunk `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off

   select distinct process_id
     from svc_posupld_status
    where status = 'N'
      and location = $1
      and chunk_id = $2;
   exit;
   EOF`
      
   num_process=${#process_id_chunk[*]}  
   
   arr_index2=0
   while [ $arr_index2 -lt  num_process ]
   do

     PROCESS_SALES ${process_id_chunk[arr_index2]} $2

     let arr_index2=$arr_index2+1;

   done  
   
   return ${OK}   
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_SALES
# Purpose      : Calls the package CORESVC_SALES_UPLOAD_SQL.PROCESS_SALES.
#-------------------------------------------------------------------------

function PROCESS_SALES
{
   sqlTxt="
      DECLARE

         FUNCTION_ERROR  EXCEPTION;

         cursor C_LOCK_STATUS_ERR is
            select 1
              from svc_posupld_status
             where process_id = $1
               and chunk_id = $2
               for update nowait;

      BEGIN
         if NOT CORESVC_SALES_UPLOAD_SQL.PROCESS_SALES ( $1,
                                                         $2,
                                                         :GV_script_error
                                                         )then
            raise FUNCTION_ERROR;
         end if;
         
         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            open  C_LOCK_STATUS_ERR;
            close C_LOCK_STATUS_ERR;
            update svc_posupld_status
               set status = 'E',
                   error_msg = substr(rtrim(:GV_script_error)||' Process_id: '||$1||' Chunk_id: '||$2,1,255)
             where process_id = $1
               and chunk_id = $2;

             COMMIT;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            open  C_LOCK_STATUS_ERR;
            close C_LOCK_STATUS_ERR;
            update svc_posupld_status
               set status = 'E',
                   error_msg = substr(rtrim(:GV_script_error)||' Process_id: '||$1||' Chunk_id: '||$2,1,255)
             where process_id = $1
               and chunk_id = $2;

            COMMIT;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL_T "$sqlTxt" ${CONNECT} ${SQL_TRACE} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID} "PROCESS_$1_$2" ""

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "CORESVC_SALES_UPLOAD_SQL.PROCESS_SALES Process:$1 Thread: $threadVal Failed." "PROCESS_SALES" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   else
      LOG_MESSAGE "Process $1 Chunk $2 - Successfully Completed" "PROCESS_SALES" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi
}
#-------------------------------------------------------------------------
# Function Name: CHECK_FHEAD
# Purpose      : Checks if the store is valid sent from the file.
#-------------------------------------------------------------------------

function CHECK_FHEAD
{
   sqlTxt="
      DECLARE

         FUNCTION_ERROR    EXCEPTION;
         DETAIL_ERROR      EXCEPTION;

         cursor C_LOCK_STATUS is
            select 'x'
              from svc_posupld_status
             where process_id = $1
               for update nowait;

      BEGIN

         if NOT CORESVC_SALES_UPLOAD_SQL.VALIDATE_FHEAD (:GV_script_error,
                                                         :GV_result,
                                                         $1)then
            raise FUNCTION_ERROR;
         end if;
         if :GV_result = 0 then
            raise DETAIL_ERROR;
         end if;
         COMMIT;

      EXCEPTION
         when DETAIL_ERROR then
            open C_LOCK_STATUS;
            close C_LOCK_STATUS;
            update svc_posupld_status
               set status = 'E',
                   error_msg = substr(decode(chunk_id, null, rtrim(:GV_script_error), null),1,255)
             where process_id = $1;
            COMMIT;
            :GV_return_code := ${FATAL};
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL "${sqlTxt}"

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "CORESVC_SALES_UPLOAD_SQL.VALIDATE_FHEAD Process id: $1 Failed." "CHECK_FHEAD" ${OK} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   else
      LOG_MESSAGE "Process $1 - FHEAD details correct" "CHECK_FHEAD" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi
}
#-------------------------------------------------------------------------
# Function Name: CHECK_FTAIL
# Purpose      : Checks if the number of transaction records is correct.
#-------------------------------------------------------------------------

function CHECK_FTAIL
{
   sqlTxt="
      DECLARE

         FUNCTION_ERROR    EXCEPTION;
         DETAIL_ERROR      EXCEPTION;

         cursor C_LOCK_STATUS is
            select 'x'
              from svc_posupld_status
             where process_id = $1
               for update nowait;

         cursor C_LOCK_FHEAD_FTAIL is
            select 'x'
              from svc_posupld_status
             where process_id = $1
               and chunk_id is null
               and status != 'E'
               for update nowait; 

      BEGIN

         if NOT CORESVC_SALES_UPLOAD_SQL.VALIDATE_FTAIL (:GV_script_error,
                                                         :GV_result,
                                                         $1)then
            raise FUNCTION_ERROR;
         end if;
         if :GV_result = 0 then
            raise DETAIL_ERROR;
         else
            open  C_LOCK_FHEAD_FTAIL;
            close C_LOCK_FHEAD_FTAIL;
            update svc_posupld_status
               set status = 'C'
             where process_id = $1
               and chunk_id is NULL;
         end if;
         COMMIT;

      EXCEPTION
         when DETAIL_ERROR then
            open C_LOCK_STATUS;
            close C_LOCK_STATUS;
            update svc_posupld_status
               set status = 'E',
                   error_msg = substr(decode(chunk_id, null, rtrim(:GV_script_error), null),1,255)
             where process_id = $1;
            COMMIT;
            :GV_return_code := ${FATAL};
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL "${sqlTxt}"

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "CORESVC_SALES_UPLOAD_SQL.VALIDATE_FTAIL Process id: $1 Failed." "CHECK_FTAIL" ${OK} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   else
      LOG_MESSAGE "Process $1 - FTAIL details correct" "CHECK_FTAIL" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi
}
#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit 1
elif [ $# -lt 2 ]
then
   SQL_TRACE="SQL_TRACE0"
elif [ $# -eq 2 ]
then
   case "$2" in
      "SQL_TRACE1") SQL_TRACE=$2
      ;;
      "SQL_TRACE4") SQL_TRACE=$2
      ;;
      "SQL_TRACE8") SQL_TRACE=$2
      ;;
      "SQL_TRACE12") SQL_TRACE=$2
      ;;
      *) USAGE
         exit 1
      ;;
   esac
else
   USAGE
   exit 1
fi

CONNECT=$1
if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "salesprocess.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#------------------------------------------------------------
# Initialize number of parallel threads.
# Retrieve from the rms_plsql_batch_config table
#------------------------------------------------------------
parallelThreads=""
parallelThreads=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select max_concurrent_threads
  from rms_plsql_batch_config
 where program_name = 'CORESVC_SALES_UPLOAD_SQL';
exit;
EOF`
# Check the number of threads
if [ -z $parallelThreads ]; then
   LOG_ERROR "Unable to retrieve the number of threads from the rms_plsql_batch_config table." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
   exit ${FATAL}
else
   LOG_MESSAGE "The number of concurrent threads for processing input file ${inputFile} is ${parallelThreads}." "" ${OK} ${LOGFILE}  ${pgmName} ${pgmPID}
fi

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Range all the missing item-locs. Since same item can be part of multiple POSU files, duplicate entries on ITEM_COUNTRY and duplicate ranging of default wh is possible by parallel threads.
# Hence range the missing item-locs before processing chunks.
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

RANGE_ITEMLOC

#------------------------------------------------------------
# Get unique locations to process.
#------------------------------------------------------------

set -A store_id `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off

select distinct location
  from svc_posupld_status
 where status = 'N';
exit;
EOF`

num_locs=${#store_id[*]}
arr_index1=0;
while [ $arr_index1 -lt num_locs ]
do
   LOG_MESSAGE "The location is ${store_id[arr_index1]}." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

   if [ `jobs | wc -l` -lt ${parallelThreads} ]
   then
   (
      PROCESS ${store_id[arr_index1]}
   )&
   else
      # Loop until a thread becomes available
      while [ `jobs | wc -l` -ge ${parallelThreads} ]
      do
         sleep 1
      done
      (
        PROCESS ${store_id[arr_index1]}
      ) &
   fi
   let arr_index1=$arr_index1+1;
done  

# Wait for all threads to complete
wait

# Check for any Oracle errors from the SQLPLUS process
if [ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. See error file." "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   if [ -e $traceLog ]
   then
      if [ `wc -l $traceLog | awk '{print $1}'` -gt 0 ]
      then
         LOG_MESSAGE "SQL TRACE Level ${SQL_TRACE##*E} files created." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
         LOG_MESSAGE "Please see ${pgmName}_${pgmPID}_tracefiles.log in the LOG directory for the listed trace files." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      fi
   fi
   LOG_MESSAGE "Program Salesprocess.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#-----------End of Processing -------------------------------------------------

exit 0
