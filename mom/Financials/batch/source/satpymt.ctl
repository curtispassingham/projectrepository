LOAD DATA
INFILE  'satpymt.dat'
BADFILE 'satpymt.bad'

APPEND
INTO TABLE      sa_tran_payment

(tran_seq_no                POSITION (1:20)         INTEGER EXTERNAL,
 payment_seq_no             POSITION (21:24)        INTEGER EXTERNAL,
 payment_amt                POSITION (25:46)        DECIMAL EXTERNAL,
 error_ind                  POSITION (47:47)        CHAR,
 store                      POSITION (48:57)        INTEGER EXTERNAL,
 day                        POSITION (58:60)        INTEGER EXTERNAL)