LOAD DATA
APPEND
INTO TABLE svc_posupld_load
  (
   file_type                   position(1:5)   char,
   thread_val                 "(select thread_id from svc_posupld_thread_lookup)",
   sales_process_id           "(select sales_process_id from svc_posupld_thread_lookup)",
   filename                   "(select filename from svc_posupld_thread_lookup)",
   line_id                     position(6:15)  integer external,
   line_seq_id                 "line_process_id_seq.nextval",
   line_text                   position(1:199) char
  )
