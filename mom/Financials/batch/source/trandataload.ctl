LOAD DATA
REPLACE 
INTO TABLE STAGE_EXT_TRAN_DATA
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(TRAN_ID                 "EXT_TRAN_DATA_SEQUENCE.NEXTVAL"
,ITEM                    
,DEPT                    
,CLASS                   
,SUBCLASS                
,LOC_TYPE                
,LOCATION                
,TRAN_DATE               
,TRAN_CODE               
,ADJ_CODE                
,UNITS                   
,TOTAL_COST              
,TOTAL_RETAIL            
,REF_NO_1                
,REF_NO_2                
,GL_REF_NO               
,OLD_UNIT_RETAIL         
,NEW_UNIT_RETAIL         
,PGM_NAME                
,SALES_TYPE              
,VAT_RATE                
,AV_COST                 
,REF_PACK_NO             
,TOTAL_COST_EXCL_ELC     
,WAC_RECALC_IND          
,STATUS                  CONSTANT "N"
,ERR_MSG                 
,CREATE_TIMESTAMP        "TO_DATE(SYSDATE)"
,LAST_UPDATED_TIMESTAMP  "TO_DATE(SYSDATE)"
)
