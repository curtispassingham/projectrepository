/*=========================================================================

   SAIMPTLOG
   output.c: output data

=========================================================================*/

#include "saimptlog.h"
#include <unistd.h>
#include <sys/stat.h>

/* filenames */
#define SAVOUCHER_FILENAME   "savouch"

/*-------------------------------------------------------------------------
data structures
-------------------------------------------------------------------------*/

/* sa output file record structures */

/* voucher */
typedef struct {    /* SA_VOUCHER struct */
   char fdetl[ LEN_FREC_DESC ];
   char file_line_no[ LEN_FILE_LINE_NO ];
   char voucher_seq_no[ LEN_BIG_SEQ_NO ];
   char voucher_no[ LEN_VOUCHER_NO ];
   char voucher_type[ LEN_VOUCHER_TYPE ];
   char ass_date[ LEN_DATEONLY ];
   char ass_store[ LEN_LOC ];
   char iss_date[ LEN_DATEONLY ];
   char iss_store[ LEN_LOC ];
   char iss_register[ LEN_REGISTER ];
   char iss_cashier[ LEN_EMP_ID ];
   char iss_tran_seq_no[ LEN_BIG_SEQ_NO ];
   char iss_item_seq_no[ LEN_LITTLE_SEQ_NO ];
   char iss_tender_seq_no[ LEN_LITTLE_SEQ_NO ];
   char iss_amt[ LEN_AMT ];
   char iss_cust_name[ LEN_CUST_NAME ];
   char iss_cust_addr1[ LEN_CUST_ADDR ];
   char iss_cust_addr2[ LEN_CUST_ADDR ];
   char iss_cust_city[ LEN_CITY ];
   char iss_cust_state[ LEN_STATE ];
   char iss_cust_postal_code[ LEN_POSTAL_CODE ];
   char iss_cust_country[ LEN_COUNTRY ];
   char recipient_name[ LEN_CUST_NAME ];
   char recipient_state[ LEN_STATE ];
   char recipient_country[ LEN_COUNTRY ];
   char red_date[ LEN_DATEONLY ];
   char red_store[ LEN_LOC ];
   char red_register[ LEN_REGISTER ];
   char red_cashier[ LEN_EMP_ID ];
   char red_tran_seq_no[ LEN_BIG_SEQ_NO ];
   char red_tender_seq_no[ LEN_LITTLE_SEQ_NO ];
   char red_amt[ LEN_AMT ];
   char exp_date[ LEN_DATEONLY ];
   char status[ LEN_STATUS ];
   char comments[ LEN_COMMENTS ];
   char newline[ LEN_NEWLINE ];
} SA_VOUCHER;

/*-------------------------------------------------------------------------
module globals
-------------------------------------------------------------------------*/

char SAVoucherFileTmp[PATH_MAX] = "./vouchtemp_XXXXXX"; /* temporary file name for the voucher file */
int pi_temp_filedesc = -1;
static FILE *SAVoucherFile = NULL;    /* voucher output file */
static long SAFileLineNo;      /* current line number in the savouch file */

/*-------------------------------------------------------------------------
local function prototypes
-------------------------------------------------------------------------*/
static int WrSoldSAVoucher( RTL_THEAD *thead,
                            RTL_TCUST *cust,
                            RTL_TITEM *p,
                            char is_ItemSeqNo[NULL_LITTLE_SEQ_NO] );
static int WrIssuedSAVoucher( RTL_THEAD *thead,
                              RTL_TCUST *cust,
                              RTL_TTEND *p,
                              char is_TenderSeqNo[NULL_LITTLE_SEQ_NO] );
static int WrRedeemedSAVoucher( RTL_THEAD *thead,
                                RTL_TTEND *p,
                                char is_TenderSeqNo[NULL_LITTLE_SEQ_NO] );
static int WrReturnedSAVoucher(RTL_THEAD *thead,
                               RTL_TITEM *p,
                               char is_ItemSeqNo[NULL_LITTLE_SEQ_NO] );

/*-------------------------------------------------------------------------
openSAVoucher() opens a temporary file for writing the voucher data.
-------------------------------------------------------------------------*/
int openSAVoucher( void )
{
   char *function = "openSAVoucher";

   pi_temp_filedesc = mkstemp(SAVoucherFileTmp);
   if (pi_temp_filedesc < 1)
   {
      sprintf(logmsg, "ERROR: Cannot create temp file <%s>", SAVOUCHER_FILENAME);
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ((SAVoucherFile = fdopen( pi_temp_filedesc, SA_WRITEMODE)) == NULL)
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SAVOUCHER_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   return( 0 );
}


/*-------------------------------------------------------------------------
closeSAVoucher() closes the temporary file for the voucher data and renames
it to a perminant name.
-------------------------------------------------------------------------*/
int closeSAVoucher( void )
{
   char *function = "closeSAVoucher";
   int  li_file_permission = 0664;
   
   int rv = 0;
   char ls_new_file_name[MAX_FILE_NAME_LEN +
                         LEN_LOC +
                         LEN_DATEONLY +
                         LEN_DATETIME +
                         4 +
                         sizeof( SUFFIX)];

   if (SAVoucherFile != NULL && fclose( SAVoucherFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing voucher output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = -1;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0) &&
       (SAFileLineNo > 2L))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SAVOUCHER_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SAVoucherFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming voucher output file - %s",
                  strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = -1;
      } 
      else 
      { 
         if (chmod(ls_new_file_name,li_file_permission) < 0) 
         { 
                 sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s", 
                                                                 ls_new_file_name, strerror(errno)); 
                 WRITE_ERROR(RET_FILE_ERR,function,"",err_data); 
                 return(FALSE); 
         }
      }
   }
   else
   {
      (void)unlink( SAVoucherFileTmp);
   }

   return( rv);
}

/*-------------------------------------------------------------------------
writeSAVoucherFHEAD() writes the FHEAD record to the Voucher file.
-------------------------------------------------------------------------*/
int writeSAVoucherFHEAD( char *is_business_date )
{
   char *function = "writeSAVoucherFHEAD";

   SAFileLineNo = 1L;
   if ( fprintf( SAVoucherFile, "FHEAD%0*liSAVO%*.*s%*.*s\n",
                 LEN_FILE_LINE_NO, SAFileLineNo,
                 LEN_DATETIME, LEN_DATETIME, SysDate,
                 LEN_DATEONLY, LEN_DATEONLY, is_business_date) == 0 )
   {
      sprintf( logmsg, "%s: Cannot write FHEAD - %s",
               SAVOUCHER_FILENAME, strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   return( 0 );
}


/*-------------------------------------------------------------------------
writeSAVoucherFTAIL() writes the FTAIL record to the Voucher file.
-------------------------------------------------------------------------*/
int writeSAVoucherFTAIL( void )
{
   char *function = "writeSAVoucherFTAIL";

   SAFileLineNo++;
   if ( fprintf( SAVoucherFile, "FTAIL%0*li%0*li\n",
                 LEN_FILE_LINE_NO, SAFileLineNo,
                 LEN_FILE_LINE_NO, SAFileLineNo - 2 ) == 0)
   {
      sprintf( logmsg, "$s: Failure writing FTAIL to voucher output file - %s",
               strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   return( 0 );
}


/*-------------------------------------------------------------------------
writeSAVoucherData() writes the current transaction voucher data to the
savouch output file.
-------------------------------------------------------------------------*/
int writeSAVoucherData( void )
{
   int i, j, rv;
   RTL_THEAD *thead;
   RTL_TCUST *tcust;
   RTL_TITEM *titem, *titem2;
   RTL_TTEND *ttend;
   int CurTrat; /* current transaction type */
   int CurSasi; /* current item status      */
   short li_voided_voucher; /* has the current voucher been voided? */
   long ll_item_seq_no, ll_tend_seq_no;
   char ls_seq_no[NULL_LITTLE_SEQ_NO];

   rv = TRUE;

   VERBOSE( "INFO: Outputing transaction voucher data\n" );


   for ( i = 0; Rectype[ i ] != RTLRT_END; ++i )
   {
      switch( Rectype[ i ] )
      {
         case RTLRT_THEAD:
            thead = (RTL_THEAD *)Recbuf[ i ];
            tcust = NULL;
            ll_item_seq_no = 0;
            ll_tend_seq_no = 0;
            CurTrat = code_get_seq( TRAT, thead->tran_type,
                                    sizeof( thead->tran_type));
            break;

         case RTLRT_TCUST:
            tcust = (RTL_TCUST *)Recbuf[ i ];
            break;

         case RTLRT_TITEM:
            ll_item_seq_no++;
            titem = (RTL_TITEM *)Recbuf[ i ];
            CurSasi = code_get_seq( SASI, titem->item_status,
                                    sizeof( titem->item_status));
            if (((CurTrat == TRATTT_SALE) ||
                 (CurTrat == TRATTT_RETURN))&&
                ((CurSasi == SASITT_S) || (CurSasi == SASITT_R)) &&
                (CMPFXLSTR( titem->item_type, SAIT_GCN)) &&
                (!EMPTY_FIELD( titem->voucher_no)))
            {
               /* determine if this item has been voided */
               li_voided_voucher = 0;
               for (j = i + 1; Rectype[ j ] != RTLRT_END; ++j )
               {
                  if (Rectype[ j ] == RTLRT_TITEM)
                  {
                     titem2 = (RTL_TITEM *)Recbuf[ j ];
                     if ((code_get_seq( SASI, titem2->item_status,
                                        sizeof( titem2->item_status)) == SASITT_V) &&
                         (CMPFXLSTR( titem2->item_type, SAIT_GCN)) &&
                         (strncmp(titem->voucher_no, titem2->voucher_no, LEN_VOUCHER_NO) == 0))
                     {
                        li_voided_voucher = 1;
                        break;
                     }
                  }
               }
               if (li_voided_voucher == 0)
               {
                  sprintf(ls_seq_no, "%0*d", LEN_LITTLE_SEQ_NO, ll_item_seq_no);
                  if (CurSasi == SASITT_S)
                     rv = WrSoldSAVoucher( thead, tcust, titem, ls_seq_no);
                  else if (CurSasi == SASITT_R)
                     rv = WrReturnedSAVoucher( thead, titem , ls_seq_no);
               }
            }
            /* it is not possible to return a gift certificate */
            break;

         case RTLRT_TTEND:
            ll_tend_seq_no++;
            ttend = (RTL_TTEND *)Recbuf[ i ];
            if (((CurTrat == TRATTT_SALE) || (CurTrat == TRATTT_PAIDIN) ||
                 (CurTrat == TRATTT_RETURN) || (CurTrat == TRATTT_PAIDOU)) &&
                (code_get_seq( TENT, ttend->tender_type_group,
                               sizeof( ttend->tender_type_group)) == TENTTT_VOUCH))
            {
               sprintf(ls_seq_no, "%0*d", LEN_LITTLE_SEQ_NO, ll_tend_seq_no);
               /* if negative amount, then we are issuing the voucher */
               if (CMPFXLSTR( ttend->tender_amt_sign, SIGN_N))
                  rv = WrIssuedSAVoucher( thead, tcust, ttend, ls_seq_no);
               /* else it is a positive amount and we are redeeming the voucher */
               else
                  rv = WrRedeemedSAVoucher( thead, ttend, ls_seq_no);
            }
            break;

      }
   }

   return( rv );
}


/*-------------------------------------------------------------------------
WrSoldSAVoucher() writes a voucher file record when a voucher or gift
certificate is encountered in a SA_TITEM as a sold item; in other words,
when you *buy* a gift certificate.
-------------------------------------------------------------------------*/
static int WrSoldSAVoucher( RTL_THEAD *thead,
                            RTL_TCUST *cust,
                            RTL_TITEM *p,
                            char is_ItemSeqNo[NULL_LITTLE_SEQ_NO] )
{
   SA_VOUCHER r;
   char *vsn;
   char file_line_no[NULL_FILE_LINE_NO];

   memset( &r, ' ', sizeof( r ) );

   CPYSTRFXL( r.fdetl, "FDETL", FT_VARCHAR );
   SAFileLineNo++;
   sprintf( file_line_no, "%0*li", LEN_FILE_LINE_NO, SAFileLineNo );
   CPYSTRFXL( r.file_line_no, file_line_no, 10 );
   if ( ( vsn = nextVoucherSeqNo(SysOpt.s_table_owner) ) != NULL )
      CPYSTRFXL( r.voucher_seq_no, vsn, FT_NUMBER );
   CPYFXLFXL( r.voucher_no, p->voucher_no, FT_VARCHAR );
   if ( !EMPTY_FIELD(p->ref_no8) )
      CPYSTRFXL( r.voucher_type, p->ref_no8, FT_VARCHAR );
   else
      CPYSTRFXL( r.voucher_type, VOUCH_4030, FT_VARCHAR );
   /* r.ass_date */    /* we do not do assigned stuff */
   /* r.ass_store */
   CPYSTRFXL( r.iss_date, BusinessDate, FT_DATE );
   CPYSTRFXL( r.iss_store, Location, FT_NUMBER );
   CPYFXLFXL( r.iss_register, thead->pos_register, FT_VARCHAR );
   CPYFXLFXL( r.iss_cashier, thead->cashier, FT_VARCHAR );
   CPYSTRFXL( r.iss_tran_seq_no, TranSeqNo, FT_NUMBER );
   if (atoi(is_ItemSeqNo) != 0)
      CPYSTRFXL( r.iss_item_seq_no, is_ItemSeqNo, FT_NUMBER );
   /* r.iss_tender_seq_no */
   CPYFXLFXL( r.iss_amt, p->unit_retail, FT_NUMBER );
   /* We expect only one customer record */
   if ( cust != NULL )
   {
      CPYFXLFXL( r.iss_cust_name, cust->cust_name, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_addr1, cust->addr1, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_addr2, cust->addr2, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_city, cust->city, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_state, cust->state, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_postal_code, cust->postalcode, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_country, cust->country, FT_VARCHAR );
   }
   CPYFXLFXL( r.recipient_name, p->ref_no5, FT_VARCHAR );
   CPYFXLFXL( r.recipient_state, p->ref_no6, FT_VARCHAR );
   CPYFXLFXL( r.recipient_country, p->ref_no7, FT_VARCHAR );
   /* Note: We leave the implied decimal places in the voucher file */
   /* r.red_date */    /* we do not do redeemed stuff */
   /* r.red_store */
   /* r.red_register */
   /* r.red_cashier */
   /* r.red_tran_seq_no */
   /* r.red_tender_seq_no */
   /* r.red_amt */
   CPYFXLFXL( r.exp_date, p->expiration_date, FT_DATE );
   CPYSTRFXL( r.status, SAVS_I, FT_VARCHAR );
   /* r.comments */    /* for later corrections use */
   r.newline[ 0 ] = '\n';

   if ( putrec( (char *)&r, SAVoucherFile ) )
      return( TRUE );
   else return( FALSE );
}


/*-------------------------------------------------------------------------
WrIssuedSAVoucher() writes a voucher file record when a voucher or gift
certificate is encountered in a SA_TTEND as a returned tender type; in
other words, when a voucher is given instead of cash.
-------------------------------------------------------------------------*/
static int WrIssuedSAVoucher( RTL_THEAD *thead,
                              RTL_TCUST *cust,
                              RTL_TTEND *p,
                              char is_TenderSeqNo[NULL_LITTLE_SEQ_NO] )
{
   SA_VOUCHER r;
   char *vsn;
   char file_line_no[NULL_FILE_LINE_NO];

   memset( &r, ' ', sizeof( r ) );

   CPYSTRFXL( r.fdetl, "FDETL", FT_VARCHAR );
   SAFileLineNo++;
   sprintf( file_line_no, "%0*li", LEN_FILE_LINE_NO, SAFileLineNo );
   CPYSTRFXL( r.file_line_no, file_line_no, 10 );
   if ( ( vsn = nextVoucherSeqNo(SysOpt.s_table_owner) ) != NULL )
      CPYSTRFXL( r.voucher_seq_no, vsn, FT_NUMBER );
   CPYFXLFXL( r.voucher_no, p->voucher_no, FT_VARCHAR );
   CPYSTRFXL( r.voucher_type, p->tender_type_id, FT_VARCHAR );
   /* r.ass_date */    /* we do not do assigned stuff */
   /* r.ass_store */
   CPYSTRFXL( r.iss_date, BusinessDate, FT_DATE );
   CPYSTRFXL( r.iss_store, Location, FT_NUMBER );
   CPYFXLFXL( r.iss_register, thead->pos_register, FT_VARCHAR );
   CPYFXLFXL( r.iss_cashier, thead->cashier, FT_VARCHAR );
   CPYSTRFXL( r.iss_tran_seq_no, TranSeqNo, FT_NUMBER );
   /* r.iss_item_seq_no */
   CPYSTRFXL( r.iss_tender_seq_no, is_TenderSeqNo, FT_NUMBER );
   CPYFXLFXL( r.iss_amt, p->tender_amt, FT_NUMBER );
   /* We expect only one customer record */
   if ( cust != NULL )
   {
      CPYFXLFXL( r.iss_cust_name, cust->cust_name, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_addr1, cust->addr1, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_addr2, cust->addr2, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_city, cust->city, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_state, cust->state, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_postal_code, cust->postalcode, FT_VARCHAR );
      CPYFXLFXL( r.iss_cust_country, cust->country, FT_VARCHAR );
      CPYFXLFXL( r.recipient_name, cust->cust_name, FT_VARCHAR );
      CPYFXLFXL( r.recipient_state, cust->state, FT_VARCHAR );
      CPYFXLFXL( r.recipient_country, cust->country, FT_VARCHAR );
   }
   /* Note: We leave the implied decimal places in the voucher file */
   /* r.red_date */    /* we do not do redeemed stuff */
   /* r.red_store */
   /* r.red_register */
   /* r.red_cashier */
   /* r.red_tran_no */
   /* r.red_amt */
   /* We do not need the expiry date in this case */
   /* r.exp_date */
   CPYSTRFXL( r.status, SAVS_I, FT_VARCHAR );
   /* r.comments */    /* for later corrections use */
   r.newline[ 0 ] = '\n';

   if ( putrec( (char *)&r, SAVoucherFile ) )
      return( TRUE );
   else return( FALSE );
}


/*-------------------------------------------------------------------------
WrRedeemedSAVoucher() writes a voucher file record when a voucher or gift
certificate is encountered in a SA_TTEND as a tender type; in other words,
when you *use* a gift certificate to buy something.
-------------------------------------------------------------------------*/
static int WrRedeemedSAVoucher( RTL_THEAD *thead,
                                RTL_TTEND *p,
                                char is_TenderSeqNo[NULL_LITTLE_SEQ_NO] )
{
   SA_VOUCHER r;
   char *vsn;
   char file_line_no[NULL_FILE_LINE_NO];

   memset( &r, ' ', sizeof( r ) );

   CPYSTRFXL( r.fdetl, "FDETL", FT_VARCHAR );
   SAFileLineNo++;
   sprintf( file_line_no, "%0*li", LEN_FILE_LINE_NO, SAFileLineNo );
   CPYSTRFXL( r.file_line_no, file_line_no, 10 );
   if ( ( vsn = nextVoucherSeqNo(SysOpt.s_table_owner) ) != NULL )
      CPYSTRFXL( r.voucher_seq_no, vsn, FT_NUMBER );
   CPYFXLFXL( r.voucher_no, p->voucher_no, FT_VARCHAR );
   CPYSTRFXL( r.voucher_type, p->tender_type_id, FT_VARCHAR );
   /* r.ass_date */    /* we do not do assigned stuff */
   /* r.ass_store */
   /* r.iss_date */    /* we do not do issued stuff */
   /* r.iss_store */
   /* r.iss_register */
   /* r.iss_cashier */
   /* r.iss_tran_seq_no */
   /* r.iss_item_seq_no */
   /* r.iss_tender_seq_no */
   /* r.iss_amt */
   CPYSTRFXL( r.red_date, BusinessDate, FT_DATE );
   CPYSTRFXL( r.red_store, Location, FT_NUMBER );
   CPYFXLFXL( r.red_register, thead->pos_register, FT_VARCHAR );
   CPYFXLFXL( r.red_cashier, thead->cashier, FT_VARCHAR );
   CPYSTRFXL( r.red_tran_seq_no, TranSeqNo, FT_NUMBER );
   CPYSTRFXL( r.red_tender_seq_no, is_TenderSeqNo, FT_NUMBER );
   CPYFXLFXL( r.red_amt, p->tender_amt, FT_NUMBER );
   /* Note: We leave the implied decimal places in the voucher file */
   /* r.exp_date */
   CPYSTRFXL( r.status, SAVS_R, FT_VARCHAR );
   /* r.comments */    /* for later corrections use */
   r.newline[ 0 ] = '\n';

   if ( putrec( (char *)&r, SAVoucherFile ) )
      return( TRUE );
   else return( FALSE );
}

/*-------------------------------------------------------------------------
WrReturnedSAVoucher() writes a voucher file record as redeemed, when a voucher
or gift certificate is encountered in a SA_TITEM as a returned item; in other
 words, when you *return* a gift certificate.
-------------------------------------------------------------------------*/
static int WrReturnedSAVoucher( RTL_THEAD *thead,
                                RTL_TITEM *p,
                                char is_ItemSeqNo[NULL_LITTLE_SEQ_NO] )
{
   SA_VOUCHER r;
   char *vsn;
   char file_line_no[NULL_FILE_LINE_NO];

   memset( &r, ' ', sizeof( r ) );

   CPYSTRFXL( r.fdetl, "FDETL", FT_VARCHAR );
   SAFileLineNo++;
   sprintf( file_line_no, "%0*li", LEN_FILE_LINE_NO, SAFileLineNo );
   CPYSTRFXL( r.file_line_no, file_line_no, 10 );
   if ( ( vsn = nextVoucherSeqNo(SysOpt.s_table_owner) ) != NULL )
      CPYSTRFXL( r.voucher_seq_no, vsn, FT_NUMBER );
   CPYFXLFXL( r.voucher_no, p->voucher_no, FT_VARCHAR );
   if ( !EMPTY_FIELD(p->ref_no8) )
      CPYSTRFXL( r.voucher_type, p->ref_no8, FT_VARCHAR );
   else
      CPYSTRFXL( r.voucher_type, VOUCH_4030, FT_VARCHAR );
   CPYSTRFXL( r.red_date, BusinessDate, FT_DATE );
   CPYSTRFXL( r.red_store, Location, FT_NUMBER );
   CPYFXLFXL( r.red_register, thead->pos_register, FT_VARCHAR );
   CPYFXLFXL( r.red_cashier, thead->cashier, FT_VARCHAR );
   CPYSTRFXL( r.red_tran_seq_no, TranSeqNo, FT_NUMBER );
   if (atoi(is_ItemSeqNo) != 0)
      CPYSTRFXL( r.red_tender_seq_no, is_ItemSeqNo, FT_NUMBER );
   CPYFXLFXL( r.red_amt, p->unit_retail, FT_NUMBER );
   /* Note: We leave the implied decimal places in the voucher file */
   CPYSTRFXL( r.status, SAVS_R, FT_VARCHAR );
   /* r.comments */    /* for later corrections use */
   r.newline[ 0 ] = '\n';

   if ( putrec( (char *)&r, SAVoucherFile ) )
      return( TRUE );
   else
      return( FALSE );
}

/*-------------------------------------------------------------------------
DeleteSAVoucher() closes and removes the temporary voucher SQL*Ldr file.
-------------------------------------------------------------------------*/
void DeleteSAVoucher( void )
{
   char *function = "DeleteSAVoucher";
   if (SAVoucherFile != NULL && fclose( SAVoucherFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing voucher output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
   }
   unlink( SAVoucherFileTmp );
}
