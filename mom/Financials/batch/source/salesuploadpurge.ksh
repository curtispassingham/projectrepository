#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  salesuploadpurge.ksh
#
#  Desc:  UNIX shell script to purge sales information from SVC_POSUPLD_LOAD_ARC  
#  table.
#  Expected input parameter is the history transaction days. 
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='salesuploadpurge.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <retention period> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <retention period> Retention period. Age of records to be purged. 
"
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`
    
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PURGE_SVC_ARCHIVE
# Purpose      : This will purge SVC_POSUPLD_ARCH records.
#-------------------------------------------------------------------------
function PURGE_SVC_ARCHIVE
{
   sqlTxt="
      DECLARE
         L_str_error_tst   VARCHAR2(1) := NULL;
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT CORESVC_SALES_UPLOAD_SQL.PURGE_ARCHIVE ( $1, 
                                                         :GV_script_error
                                                         )then
            raise FUNCTION_ERROR;
         end if;
      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt} 
   
   if [[ $? -ne ${OK} ]]; then
       LOG_ERROR "CORESVC_SALES_UPLOAD_SQL.PURGE_ARCHIVE Failed" "PURGE_SVC_ARCHIVE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
       return ${FATAL}
   else
      LOG_MESSAGE "CORESVC_SALES_UPLOAD_SQL.PURGE_ARCHIVE - Successfully Completed" "PURGE_SVC_ARCHIVE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID} 
      return ${OK}
   fi

}


#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------

if [ $# -lt 2 ]
then
  USAGE
  exit 1;
fi

# Test for the number of input arguments
if [ $# -eq 2 ]
then   
   #--- Determine if num_of_days_retention value is a valid integer
   error_code=0
   num_of_days_retention=$2   
   case $num_of_days_retention in
     *[!0-9]*) error_code=1;; 
            *);;
   esac
   if [ ${error_code} == 1 ]; then
      echo "Invalid number of days" >> $LOGFILE;
      exit 1; 
   fi
else 
  num_of_days_retetion=95
fi

#Validate input parameters
CONNECT=$1

if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "salesuploadpurge.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#--- Invoke Archive Function
PURGE_SVC_ARCHIVE $num_of_days_retention

# --  Check for any Oracle errors from the SQLPLUS process
if [ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. See error file" "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "Program salesuploadpurge.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

exit 0
