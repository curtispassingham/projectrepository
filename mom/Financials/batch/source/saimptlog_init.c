
#include "saimptlog.h"
#include "saimptlog_ccval.h"
#include "sagetref.h"
#include "saoranum.h"

/************************************\
| Array of retek_init() parameters:  |
| See retek_2.h for correct format.  |
\************************************/
init_parameter parameter[] =
{
   /* NAME ----------------- TYPE ------ SUB_TYPE */
   "commit_max_ctr",         "uint",     "",
   "thread_val",             "string",   ""
};
#define NUM_INIT_PARAMETERS (sizeof( parameter) / sizeof( init_parameter))


/*-------------------------------------------------------------------------
Init() opens all files used by the application and does any other special
initialization that may be required.
-------------------------------------------------------------------------*/
int Init( int argc, char *argv[] )
{
   char *function = "Init";
   int li_init_return;

   gi_rej_record = FALSE;
   gi_non_fatal_err_flag = 0;

   strcpy(TranNoGen, " ");

   li_init_return = retek_init(NUM_INIT_PARAMETERS,
                               parameter,
                               &pi_commit_max_ctr,
                               ps_thread_val);
   if (li_init_return < 0)
   {
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   else if (li_init_return == NO_THREAD_AVAILABLE)
   {
      return( li_init_return );
   }

   /* load system options values into sysopt */
   VERBOSE( "INFO: Loading system options\n" );
   if ( fetchSaSystemOptions( &SysOpt ) != 0)
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot load system options" );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   /* Get the current date and time */
   if (fetchSysdate(SysDate) < 0)
   {
      sprintf( logmsg, "APPLICATION ERROR: Fetching SYSDATE" );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   /* call init for input format */
   VERBOSE( "INFO: Calling input initialization\n" );
   if (check_filename(argv[ ARG_INFILE ]) < 0)
   {
      return(FAILED);
   }

   if (check_filename(argv[ ARG_BADFILE ]) < 0)
   {
      return(FAILED);
   }

   if ( ! InitInputData( argv[ ARG_INFILE ], argv[ ARG_BADFILE ] ) )
      return( -1 );

   /* call to load rounding rule information */
   VERBOSE("INFO: Loading rounding rule information \n");
   if ( rounding_rule_loaddata() < 0)
      return( -1);

   /* load item, wastage, ref_item, vupc, store day, promotions, codes,
    * and error databases */
   VERBOSE( "INFO: Loading item lookup file\n" );
   if (check_filename(argv[ ARG_ITEMFILE ]) < 0)
   {
      return(FAILED);
   }

   if ( ! item_loadfile( argv[ ARG_ITEMFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map item data file <%s>", argv[ ARG_ITEMFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Wastage lookup file\n" );
   if (check_filename(argv[ ARG_WASTEFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! waste_loadfile( argv[ ARG_WASTEFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Wastage data file <%s>", argv[ ARG_WASTEFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading UPC lookup file\n" );
   if (check_filename(argv[ ARG_REFITEMFILE ] ) < 0)
   {
      return(FAILED);
   }


   if ( ! ref_item_loadfile( argv[ ARG_REFITEMFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map ref_item data file <%s>", argv[ ARG_REFITEMFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading primary_variant lookup file\n" );
   if (check_filename(argv[ ARG_PRIMVARFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! primvariant_loadfile( argv[ ARG_PRIMVARFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map prim_variant data file <%s>", argv[ ARG_PRIMVARFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Variable UPC file\n" );
   if (check_filename(argv[ ARG_VUPCFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! vupc_loadfile( argv[ ARG_VUPCFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Variable UPC data file <%s>", argv[ ARG_VUPCFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Store/Day file\n" );
   if (check_filename(argv[ ARG_STOREDAYFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! storeday_loadfile( argv[ ARG_STOREDAYFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Store/Day data file <%s>", argv[ ARG_STOREDAYFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Promotions file\n" );
   if (check_filename(argv[ ARG_PROMFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! prom_loadfile( argv[ ARG_PROMFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Promotions data file <%s>", argv[ ARG_PROMFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Code Type file\n" );
   if (check_filename(argv[ ARG_CODEFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! code_loadfile( argv[ ARG_CODEFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Code Type data file <%s>", argv[ ARG_CODEFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Store POS file\n" );
   if (check_filename(argv[ ARG_STOREPOSFILE ] )  < 0)
   {
      return(FAILED);
   }

   if ( ! storepos_loadfile( argv[ ARG_STOREPOSFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Store POS file <%s>", argv[ ARG_STOREPOSFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Tender Type file\n" );
   if (check_filename(argv[ ARG_TENDERTYPEFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! tendertype_loadfile( argv[ ARG_TENDERTYPEFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Tender Type file <%s>", argv[ ARG_TENDERTYPEFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Error file\n" );
   if (check_filename(argv[ ARG_ERRORFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! error_loadfile( argv[ ARG_ERRORFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Error file <%s>", argv[ ARG_ERRORFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Merchant Code lookup file\n" );
   if (check_filename(argv[ ARG_MERCHCODEFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! merchcode_loadfile( argv[ ARG_MERCHCODEFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Merchant Code data file <%s>", argv[ ARG_MERCHCODEFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Partner lookup file\n" );
   if (check_filename(argv[ ARG_PARTNERFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! partner_loadfile( argv[ ARG_PARTNERFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Partner data file <%s>", argv[ ARG_PARTNERFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Supplier lookup file\n" );
   if (check_filename(argv[ ARG_SUPPLIERFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! supplier_loadfile( argv[ ARG_SUPPLIERFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Supplier data file <%s>", argv[ ARG_SUPPLIERFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Employee lookup file\n" );
   if (check_filename(argv[ ARG_EMPLOYEEFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! employee_loadfile( argv[ ARG_EMPLOYEEFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Employee data file <%s>", argv[ ARG_EMPLOYEEFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Banner lookup file\n" );
   if (check_filename(argv[ ARG_BANNERFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! banner_loadfile( argv[ ARG_BANNERFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Banner data file <%s>", argv[ ARG_BANNERFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Currency lookup file\n" );
   if (check_filename(argv[ ARG_CURRENCYFILE ]) < 0)
   {
      return(FAILED);
   }

   if ( ! currency_loadfile( argv[ ARG_CURRENCYFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Currency data file <%s>", argv[ ARG_CURRENCYFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Warehouse lookup file\n" );
   if (check_filename(argv[ ARG_WHFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! wh_loadfile( argv[ ARG_WHFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Warehouse data file <%s>", argv[ ARG_WHFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   VERBOSE( "INFO: Loading Inv status lookup file\n" );
   if (check_filename(argv[ ARG_INVSTATUSFILE ] ) < 0)
   {
      return(FAILED);
   }

   if ( ! invstatus_loadfile( argv[ ARG_INVSTATUSFILE ] ) )
   {
      sprintf( logmsg, "APPLICATION ERROR: Cannot open/map Inv Status data file <%s>", argv[ ARG_INVSTATUSFILE ] );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }

   if (InitOutputData( argc, argv) != 0)
      return(-1 );

   if ( openSAVoucher() != 0 )
   {
      InitOutputClean();
      return( -1 );
   }

   if (OraNumInit() < 0)
   {
      InitOutputClean();
      return( -1 );
   }

   /* if still all ok... */
   VERBOSE( "INFO: Init finished\n" );
   return( 0 );
}
