lOAD DATA
INFILE	'satdisc.dat'
BADFILE	'satdisc.bad'

APPEND
INTO TABLE	sa_tran_disc

(tran_seq_no		POSITION (1:20)		INTEGER EXTERNAL,
 item_seq_no		POSITION (21:24)	INTEGER EXTERNAL,
 discount_seq_no        POSITION (25:28)        INTEGER EXTERNAL,
 rms_promo_type		POSITION (29:34)	CHAR,
 promotion      	POSITION (35:44)	INTEGER EXTERNAL,
 disc_type      	POSITION (45:50)	CHAR,
 coupon_no      	POSITION (51:90)	CHAR,
 coupon_ref_no      	POSITION (91:106)	CHAR,
 qty			POSITION (107:120)	DECIMAL EXTERNAL,
 unit_discount_amt	POSITION (121:141)	DECIMAL EXTERNAL,
 standard_qty		POSITION (142:155)	DECIMAL EXTERNAL,
 standard_unit_disc_amt POSITION (156:176)	DECIMAL EXTERNAL,
 ref_no13               POSITION (177:206)      CHAR,
 ref_no14               POSITION (207:236)      CHAR,
 ref_no15               POSITION (237:266)      CHAR,
 ref_no16               POSITION (267:296)      CHAR,
 error_ind		POSITION (297:297)	CHAR,
 catchweight_ind        POSITION (298:298)      CHAR,
 uom_quantity           POSITION (299:310)      INTEGER EXTERNAL,
 promo_comp             POSITION (311:320)      INTEGER EXTERNAL,
 store                  POSITION (321:330)      INTEGER EXTERNAL,
 day                    POSITION (331:333)      INTEGER EXTERNAL)
