

/*=========================================================================

   Each transaction must pass "mandatory validations", which refers
   to a basic check of fields which are numeric, date, not-null, etc.

   The object of this is to assure a valid SQL load/INSERT process. For
   example, a numeric field which contains character data would
   fail during SQL load/INSERT, so we must be sure this is detected, even
   if that field has no validation rules associated with it.

   This basic checking does not concern itself with actual values
   or ranges, simply that the data in the field is the appropriate
   type.

=========================================================================*/

#include "saimptlog.h"
#include "saimptlog_ccval.h"
#include <ctype.h>
#define ALLOCTRANIGTAXS   8  /* max number of igtax records */

/*-------------------------------------------------------------------------
local data structures
-------------------------------------------------------------------------*/
typedef struct
   {  /*item igtax info*/
      char igtax_code[LEN_IGTAX_CODE]; /* igtax code identifier */
      char pack_comp_item[LEN_REF_NO]; /* component item for pack item */
      char tax_authority[LEN_TAX_AUTHORITY]; /* tax authority */
   } pt_igtax;

/*-------------------------------------------------------------------------
local function prototypes
-------------------------------------------------------------------------*/
static void num2str( char *str, int lstr, char *num, int lnum, const char *sign);

/*-------------------------------------------------------------------------
local global variables
-------------------------------------------------------------------------*/
static long UsedTranIgtaxs = 0;
static pt_igtax **la_igtax = NULL;

/*-------------------------------------------------------------------------
local variables
-------------------------------------------------------------------------*/
static int CurTrat;
static int CurSubTrat;
static int CurCWInd;
static int CurInvInd;

/*-------------------------------------------------------------------------
mvSATHead() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSATHead( RTL_THEAD *head )
{
   int rv = TRUE;
   int tras;
   BANNERDAT *banner_data;

   /* make sure tran_date exists and is valid */
   if (( EMPTY_FIELD( head->tran_datetime) ) ||
       ( SA_VALID_ALL_NUMERIC( head->tran_datetime) != 0 ) ||
       ( SA_VALID_DATE( head->tran_datetime) != 0 ))
   {
      rv = FALSE;
      WrBadTranError( INVLD_TRAN_DATETIME, TranSeqNo, -1, -1, SART_THDATE,
                      head->tran_datetime, sizeof( head->tran_datetime));
      CPYSTRFXL( head->tran_datetime, BusinessDate, FT_DATE);
      memset( head->tran_datetime + LEN_DATEONLY, '0', LEN_TIME);
   }
   /* if sa_system_options.auto_validate_tran_employee_id, validate
      cashier and salesperson if present */
   if (strcmp(SysOpt.s_auto_validate_tran_employee_id, YSNO_Y) == 0)
   {
      if (( ! EMPTY_FIELD( head->cashier) ) &&
          ( employee_lookup(Location, head->cashier) == NULL ))
      {
         rv = FALSE;
         WrBadTranError( INVLD_CASHIER_ID, TranSeqNo, -1, -1, SART_THEAD,
                         head->cashier, sizeof( head->cashier));
         CLRTOSPACES( head->cashier );
         CLRTOERR( head->cashier );
      }

      if (( ! EMPTY_FIELD( head->salesperson) ) &&
          ( employee_lookup(Location, head->salesperson) == NULL ))
      {
         rv = FALSE;
         WrBadTranError( INVLD_SALESPERSON_ID, TranSeqNo, -1, -1, SART_THEAD,
                         head->salesperson, sizeof( head->salesperson));
         CLRTOSPACES( head->salesperson );
         CLRTOERR( head->salesperson );
      }
   }

   /* make sure tran_type code exists and is valid */
   CurTrat = code_get_seq( TRAT, head->tran_type, sizeof( head->tran_type));
   if ( (CurTrat == 0) || (CurTrat == TRATTT_ERR) )
   {
      rv = FALSE;
      WrBadTranError( INVLD_TRAT, TranSeqNo, -1, -1, SART_THTRAT,
                      head->tran_type, sizeof( head->tran_type));
      CLRTOERR( head->tran_type );
   }

   CurSubTrat = code_get_seq( TRAS,
                              head->sub_tran_type,
                              sizeof( head->sub_tran_type) );

   /* cashier and register fields can be null for tran_type codes DCLOSE, ERR, TERM, and TOTAL */
   if( ((CurTrat != TRATTT_DCLOSE) && (CurTrat != TRATTT_ERR) && (CurTrat != TRATTT_TERM)
       && (CurTrat != TRATTT_TOTAL)) || ((CurTrat == TRATTT_TOTAL) && ( !EMPTY_FIELD( head->tran_no ))) )
   {
      if ((CMPFXLSTR(TranNoGen, STRG_R)) &&
          (EMPTY_FIELD( head->pos_register )))
      {
         rv = FALSE;
         WrBadTranError( REGISTER_ID_REQ, TranSeqNo, -1, -1, SART_THEAD,
                         head->pos_register, sizeof( head->pos_register));
         CPYSTRFXL( head->pos_register, "ERR", FT_VARCHAR );
      } /* end if */

      /* make sure that cashier or register fields are non-empty depending on balancing level */
      if ((strcmp(SysOpt.s_balance_level_ind, SABL_R) == 0) &&
          (EMPTY_FIELD( head->pos_register )))
      {
         rv = FALSE;
         WrBadTranError( REGISTER_ID_REQ_BAL_LEVEL, TranSeqNo, -1, -1, SART_THEAD,
                         head->pos_register, sizeof( head->pos_register));
         CPYSTRFXL( head->pos_register, "ERR", FT_VARCHAR );
      } /* end if */
      else if ((strcmp(SysOpt.s_balance_level_ind, SABL_C) == 0) &&
               (EMPTY_FIELD( head->cashier )))
      {
         rv = FALSE;
         WrBadTranError( CASHIER_ID_REQ_BAL_LEVEL, TranSeqNo, -1, -1, SART_THEAD,
                         head->cashier, sizeof( head->cashier));
         CLRTOSPACES( head->cashier );
         CLRTOERR( head->cashier );
      } /* end else */
   } /* end if */

   /* make sure tran_no exists */
   if ((CurTrat != TRATTT_DCLOSE) &&
       (CurTrat != TRATTT_TOTAL) && ( EMPTY_FIELD( head->tran_no )))
   {
      rv = FALSE;
      WrBadTranError( TRAN_NO_REQ, TranSeqNo,
                      -1, -1, SART_THTRAN,
                      head->tran_no, sizeof( head->tran_no));
      CLRTOZEROS( head->tran_no );
   }

   /* make sure tran_no is numeric; if bad set it to -1 */
   if (( ! EMPTY_FIELD( head->tran_no ) ) &&
       ( SA_VALID_ALL_NUMERIC( head->tran_no ) != 0 ))
   {
      rv = FALSE;
      WrBadTranError( THEAD_TRN_STIN, TranSeqNo, -1, -1, SART_THTRAN,
                      head->tran_no, sizeof( head->tran_no));
      CPYSTRFXL( head->tran_no, "1", FT_NUMBER );
      head->tran_no[0] = '-';
   }

   /* make sure if sub_tran type code exists, that it is valid */
   if ( ! EMPTY_FIELD( head->sub_tran_type ) )
      if ( code_lookup( TRAS, head->sub_tran_type,
                        sizeof( head->sub_tran_type) ) == NULL )
      {
         rv = FALSE;
         WrBadTranError( INVLD_TRAS, TranSeqNo, -1, -1, SART_THSUBT,
                         head->sub_tran_type, sizeof( head->sub_tran_type));
         CLRTOERR( head->sub_tran_type);
      }

   /* make sure orig_tran_no is valid if it exists or */
   /* the transaction is a Post Void; if bad set it to -1 */
   if (((CurTrat == TRATTT_PVOID) || ( ! EMPTY_FIELD( head->orig_tran_no))) &&
       (SA_VALID_ALL_NUMERIC( head->orig_tran_no ) != 0 ))
   {
      rv = FALSE;
      WrBadTranError( THEAD_OTN_STIN, TranSeqNo, -1, -1, SART_THOTRN,
                      head->orig_tran_no, sizeof( head->orig_tran_no));
      CPYSTRFXL( head->orig_tran_no, "1", FT_NUMBER );
      head->orig_tran_no[0] = '-';
   }

   /* make sure that Paid In transaction have a Reason Code */
   if ((CurTrat == TRATTT_PAIDIN) && (EMPTY_FIELD( head->reason_code)))
   {
      rv = FALSE;
      WrBadTranError( PAIDIN_REASON_CODE_REQ, TranSeqNo, -1, -1, SART_THREAC,
                      head->reason_code, sizeof( head->reason_code));
      CLRTOERR( head->reason_code);
   }

   /* if transaction is a Paid Out */
   if (CurTrat == TRATTT_PAIDOU)
   {
      tras = CurSubTrat;
      /* make sure that Paid Out transactions have a Reason Code */
      if (EMPTY_FIELD( head->reason_code))
      {
         rv = FALSE;
         WrBadTranError( PAIDOU_REASON_CODE_REQ, TranSeqNo, -1, -1, SART_THREAC,
                         head->reason_code, sizeof( head->reason_code));
         CLRTOERR( head->reason_code);
      }
      /* make sure if reason code exists, that it is valid */
      else
      {
         /* if sub_tran_type is MV or EV, then reason code comes from
            non_merch_code_head table */
         if ((tras == TRASTT_MV) || (tras == TRASTT_EV))
         {
            if ( merchcode_lookup( head->reason_code) == NULL )
            {
               rv = FALSE;
               WrBadTranError( INVLD_NON_MERCH_CODE, TranSeqNo, -1, -1, SART_THREAC,
                               head->reason_code, sizeof( head->reason_code));
               CLRTOERR( head->reason_code);
            }
         }
         else
         {
            if ( code_lookup( REAC, head->reason_code,
                              sizeof( head->reason_code) ) == NULL )
            {
               rv = FALSE;
               WrBadTranError( INVLD_REAC, TranSeqNo, -1, -1, SART_THREAC,
                               head->reason_code, sizeof( head->reason_code));
               CLRTOERR( head->reason_code);
            }
         }
      }

      /* if sub_tran_type is MV or EV, then validate vendor info */
      if ((tras == TRASTT_MV) || (tras == TRASTT_EV))
      {
         /* make sure that vendor number exists */
         if (EMPTY_FIELD( head->vendor_no))
         {
            rv = FALSE;
            WrBadTranError( VENDOR_NO_REQ, TranSeqNo, -1, -1, SART_THEAD,
                            head->vendor_no, sizeof( head->vendor_no));
         }
         /* make sure that vendor number is valid */
         else
         {
            if ((tras == TRASTT_MV) && ( supplier_lookup( head->vendor_no) == NULL ))
            {
               rv = FALSE;
               WrBadTranError( INVLD_VENDOR_NO_MV, TranSeqNo, -1, -1, SART_THEAD,
                               head->vendor_no, sizeof( head->vendor_no));
               CLRTOSPACES( head->vendor_no);
            }
            else if ((tras == TRASTT_EV) && ( partner_lookup( head->vendor_no) == NULL ))
            {
               rv = FALSE;
               WrBadTranError( INVLD_VENDOR_NO_EV, TranSeqNo, -1, -1, SART_THEAD,
                               head->vendor_no, sizeof( head->vendor_no));
               CLRTOSPACES( head->vendor_no);
            }
         }

         /* make sure that at least one of the fields (vendor_invc_no, */
         /* payment_ref_no, proof_of_delivery_no) is not null */
         if ((EMPTY_FIELD( head->vendor_invc_no)) &&
             (EMPTY_FIELD( head->payment_ref_no)) &&
             (EMPTY_FIELD( head->proof_of_delivery_no)))
         {
            rv = FALSE;
            WrBadTranError( VENDOR_DATA_REQ, TranSeqNo, -1, -1, SART_THEAD,
                            "", 0);
         }
      } /* end of if MV or EV */
   }
   else
   {
      if ((!EMPTY_FIELD( head->reason_code)) &&
           ( code_lookup( REAC, head->reason_code, sizeof( head->reason_code) ) == NULL ))
      {
         rv = FALSE;
         WrBadTranError( INVLD_REAC, TranSeqNo, -1, -1, SART_THREAC,
                         head->reason_code, sizeof( head->reason_code));
         CLRTOERR( head->reason_code);
      }
   } /* End of if PAIDOU */

   /* if this is a TOTAL transaction, make sure ref_no1 & value exists */
   if ( CurTrat == TRATTT_TOTAL )
   {
      if ( EMPTY_FIELD( head->ref_no1 ) )
      {
         rv = FALSE;
         WrBadTranError( TOTAL_REF_NO_1_REQ, TranSeqNo, -1, -1, SART_THEAD,
                         head->ref_no1, sizeof( head->ref_no1));
         CLRTOSPACES( head->ref_no1);
      }
      if ( EMPTY_FIELD( head->value ) )
      {
         rv = FALSE;
         WrBadTranError( TOTAL_VALUE_REQ, TranSeqNo, -1, -1, SART_THVALU,
                         head->value, sizeof( head->value));
         CLRTOZEROS( head->value);
      }
   }

   /* make sure if value exists, that it is valid */
   if ( ! EMPTY_FIELD( head->value ) )
      if (( SA_VALID_ALL_NUMERIC( head->value ) != 0 ) ||
          ( code_lookup( SIGN, head->value_sign, sizeof( head->value_sign)) == NULL ))
      {
         char ls_value[LEN_SIGNED_DEC_AMT];

         rv = FALSE;
         CPYFXLFXL( ls_value, head->value, FT_NUMBER);
         ls_value[0] = signcvt( head->value_sign);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_value, AMT_DECPLACES);
#endif
         WrBadTranError( THEAD_VAL_STIN, TranSeqNo, -1, -1, SART_THVALU,
                         ls_value, sizeof( ls_value));
         CLRTOZEROS( head->value);
      }

   /* make sure if rounded_amt exists, that it is valid */
   if ( ! EMPTY_FIELD( head->rounded_amt ) )
      if (( SA_VALID_ALL_NUMERIC( head->rounded_amt ) != 0 ) ||
          ( code_lookup( SIGN, head->rounded_amt_sign, sizeof( head->rounded_amt_sign)) == NULL ))
      {
         char ls_rounded_amt[LEN_SIGNED_DEC_AMT];

         rv = FALSE;
         CPYFXLFXL( ls_rounded_amt, head->rounded_amt, FT_NUMBER);
         ls_rounded_amt[0] = signcvt( head->rounded_amt_sign);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_rounded_amt, AMT_DECPLACES);
#endif
         WrBadTranError( THEAD_RAMT_STIN, TranSeqNo, -1, -1, SART_THEAD,
                         ls_rounded_amt, sizeof( ls_rounded_amt));
         CLRTOZEROS( head->rounded_amt);
      }

   /* make sure if rounded_off_amt exists, that it is valid */
   if ( ! EMPTY_FIELD( head->rounded_off_amt ) )
      if (( SA_VALID_ALL_NUMERIC( head->rounded_off_amt ) != 0 ) ||
          ( code_lookup( SIGN, head->rounded_off_amt_sign, sizeof( head->rounded_off_amt_sign)) == NULL ))
      {
         char ls_rounded_off_amt[LEN_SIGNED_DEC_AMT];

         rv = FALSE;
         CPYFXLFXL( ls_rounded_off_amt, head->rounded_off_amt, FT_NUMBER);
         ls_rounded_off_amt[0] = signcvt( head->rounded_off_amt_sign);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_rounded_off_amt, AMT_DECPLACES);
#endif
         WrBadTranError( THEAD_ROAMT_STIN, TranSeqNo, -1, -1, SART_THEAD,
                         ls_rounded_off_amt, sizeof( ls_rounded_off_amt));
         CLRTOZEROS( head->rounded_off_amt);
      }

   /* If banner_id exists make sure its valid */
   if ( ! EMPTY_FIELD( head->banner_id ) )
   {
      if (( SA_VALID_ALL_NUMERIC( head->banner_id ) != 0 ) ||
          ( banner_lookup( Location, head->banner_id ) == NULL ))
      {
         rv = FALSE;
         WrBadTranError(INVLD_BANNER_ID, TranSeqNo, -1, -1, SART_THEAD,
                         head->banner_id, sizeof( head->banner_id));
         CLRTONULL( head->banner_id);
      }
   }
   else
   {
      /* Banner ID is empty so retrieve the correct value from the database */
      if ( (banner_data = banner_store_lookup(Location)) != NULL )
      {
         CPYFXLFXL( head->banner_id, banner_data->banner_id, FT_NUMBER );
      }
      else
      {
         rv = FALSE;
         WrBadTranError(THEAD_LOC_NO_BANN, TranSeqNo, -1, -1, SART_THEAD,
                         head->banner_id, sizeof( head->banner_id));
         CLRTONULL( head->banner_id);
      }
   }

   /* If credit_promotion_id exists make sure it is numeric */
   if ( ! EMPTY_FIELD( head->credit_promotion_id ) )
      if ( SA_VALID_ALL_NUMERIC( head->credit_promotion_id ) != 0 )
      {
         rv = FALSE;
         WrBadTranError(CREDIT_PROMO_ID_STIN, TranSeqNo, -1, -1, SART_THEAD,
                        head->credit_promotion_id, sizeof( head->credit_promotion_id));
         CLRTOSPACES( head->credit_promotion_id);
      }

   return( rv );
}


/*-------------------------------------------------------------------------
mvSATCust() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSATCust( RTL_TCUST *cust )
{
   int rv = TRUE;

   /* make sure cust_id exists */
   if ( EMPTY_FIELD( cust->cust_id ) )
   {
      rv = FALSE;
      WrBadTranError( CUST_ID_REQ, TranSeqNo, -1, -1, SART_TCUST, "", 0);
      CLRTOZEROS( cust->cust_id);
   }

   /* make sure cust_id_type is valid */
   if (code_lookup( CIDT, cust->cust_id_type,
                    sizeof( cust->cust_id_type) ) == NULL)
   {
      rv = FALSE;
      WrBadTranError( INVLD_CIDT, TranSeqNo, -1, -1, SART_TCTYPE,
                      cust->cust_id_type, sizeof( cust->cust_id_type));
      CLRTOERR( cust->cust_id_type);
   }

   /* make sure if birthdate exists and that it is valid */
   if ( ! EMPTY_FIELD( cust->birthdate ) )
      if (( SA_VALID_ALL_NUMERIC( cust->birthdate ) != 0 ) ||
          ( SA_VALID_DATE( cust->birthdate ) != 0 ))
      {
         rv = FALSE;
         WrBadTranError( INVLD_BIRTHDATE, TranSeqNo, -1, -1, SART_TCUST,
                         cust->birthdate, sizeof( cust->birthdate));
         CLRTOSPACES( cust->birthdate);
      }

   return( rv );
}


/*-------------------------------------------------------------------------
mvSACAtt() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSACAtt( RTL_CATT *catt, int attrib_seq_no )
{
   int rv = TRUE;

   /* make sure attrib_type exists and is valid */
   if (code_lookup( SACA, catt->attrib_type,
                    sizeof( catt->attrib_type) ) == NULL)
   {
      rv = FALSE;
      WrBadTranError( INVLD_SACA, TranSeqNo,
                      attrib_seq_no, -1, SART_CATT,
                      catt->attrib_type, sizeof( catt->attrib_type));
      CLRTOERR( catt->attrib_type);
   }

   /* make sure attrib_value exists and is valid */
   if (code_lookup( catt->attrib_type, catt->attrib_value, LEN_CODE) == NULL)
   {
      rv = FALSE;
      WrBadTranError( INVLD_SUB_SACA, TranSeqNo,
                      attrib_seq_no, -1, SART_CATT,
                      catt->attrib_value, sizeof( catt->attrib_value));
      CLRTOERR( catt->attrib_value);
   }

   return( rv );
}


/*-------------------------------------------------------------------------
mvSATItem() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSATItem( RTL_TITEM *item, int item_seq_no, pt_item_uom *uom )
{
   int rv = TRUE;
   int item_lookup_flag = TRUE;
   int ref_item_lookup_flag = TRUE;
   int sait, sasi, sasy;
   int li_qty_urt_error = 0;
   ITEMDAT *tableitem;
   REFITEMDAT *tablerefitem;

   int CurCustOrdDate = 0;

   /* If cust_ord_date exists make sure it is a properly formatted date. */
   /* The cust_ord_date will be checked for it's presence in case of     */
   /* items on Customer Orders or Layaway.It is manadatory field if the       */
   /* Transasction is coming with these items, even if it has regular sale or */
   /* return items in the transcation. */
   CurCustOrdDate = 0;
   if ( ! EMPTY_FIELD( item->cust_order_date ) )
   {
      if (( SA_VALID_ALL_NUMERIC( item->cust_order_date ) != 0 ) ||
          ( SA_VALID_DATE( item->cust_order_date ) != 0 ))
      {
         rv = FALSE;
         WrBadTranError( INVLD_CUST_ORD_DATE, TranSeqNo, item_seq_no, -1, SART_TITEM,
                         item->cust_order_date,
                         sizeof( item->cust_order_date));
         CLRTOSPACES( item->cust_order_date);
      }
   }
   else
   {
      CurCustOrdDate = 1;
   }

   /* make sure item_status exists */
   if (EMPTY_FIELD( item->item_status))
   {
      rv = FALSE;
      WrBadTranError( ITEM_STATUS_REQ, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->item_status, sizeof( item->item_status));
      CLRTOERR( item->item_status);
   }
   else
   {
      /* make sure item_status is valid */
      sasi = code_get_seq( SASI, item->item_status, sizeof( item->item_status) );
      if ((sasi == 0) || (sasi == SASITT_ERR))
      {
         rv = FALSE;
         WrBadTranError( INVLD_SASI, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->item_status, sizeof( item->item_status));
         CLRTOERR( item->item_status);
      }
      else
      {
         /* If transaction is a SALE, RETURN or EEXCH, item status can only be S, R or V.  */
         /* Due File Changes - Consider the SALE transcation separately below for handling */
         /* Customer Order and Layaway item statuses */
         if (((CurTrat == TRATTT_RETURN) || (CurTrat == TRATTT_EEXCH)) &&
             ((sasi != SASITT_S) && (sasi != SASITT_R) && (sasi != SASITT_V)))
         {
            rv = FALSE;
            WrBadTranError( INVLD_SASI, TranSeqNo,
                            item_seq_no, -1, SART_TITEM,
                            item->item_status, sizeof( item->item_status));
            CLRTOERR( item->item_status);
         }

         /* If transaction is a SALE, item status can only be S, R, V, ORI, ORD, ORC, LIN, LCA, LCO */
         /* as part of Pick up, Delivery, Layaway functionality */
         if (((CurTrat == TRATTT_SALE)) &&
             ((sasi != SASITT_S) && (sasi != SASITT_R) && (sasi != SASITT_V) && (sasi != SASITT_ORI)
             && (sasi != SASITT_ORC) && (sasi != SASITT_ORD) && (sasi != SASITT_LIN) && (sasi != SASITT_LCA)
             && (sasi != SASITT_LCO)))
         {
            rv = FALSE;
            WrBadTranError( INVLD_SASI, TranSeqNo,
                            item_seq_no, -1, SART_TITEM,
                            item->item_status, sizeof( item->item_status));
            CLRTOERR( item->item_status);
         }

         /* If item status is S, ORI, ORD, LIN, LCO then quantity sign must be positive - Due File Changes */
         if (((sasi == SASITT_S) || (sasi == SASITT_ORI) || (sasi == SASITT_LIN) ||
              (sasi == SASITT_ORD) || (sasi == SASITT_LCO)) && (! CMPFXLSTR( item->qty_sign, SIGN_P)))
         {
            if(CurTrat == TRATTT_SALE && CurSubTrat == TRASTT_CACCOM)
            {
               /* Allow negative sales for post shipment customer accomodations */
            }
            else
            {
               rv = FALSE;
               WrBadTranError( TITEM_QTY_SIGN, TranSeqNo,
                               item_seq_no, -1, SART_TITEM,
                               item->qty, sizeof( item->qty));
            }
         }
         /* else if item status is R, ORC, LCA,then quantity sign must be negative */
         else if (((sasi == SASITT_R) || (sasi == SASITT_ORC) || (sasi == SASITT_LCA)) && (! CMPFXLSTR( item->qty_sign, SIGN_N)))
         {
            rv = FALSE;
            WrBadTranError( TITEM_QTY_SIGN, TranSeqNo,
                            item_seq_no, -1, SART_TITEM,
                            item->qty, sizeof( item->qty));
         }
         /* If item status is ORI, ORC, ORD, LIN, LCA, LCO validate */
         /* the customer order  date being present in TITEM     */
         if (((sasi == SASITT_ORI) || (sasi == SASITT_ORC) || (sasi == SASITT_ORD) ||
              (sasi == SASITT_LIN) || (sasi == SASITT_LCA) || (sasi == SASITT_LCO)) &&
              (CurCustOrdDate == 1))
         {
            rv = FALSE;
            WrBadTranError( CUST_ORD_DATE_REQDINTITEM, TranSeqNo,
                            item_seq_no, -1, SART_TITEM,
                            item->cust_order_date, sizeof( item->cust_order_date));
         }
      }
   }

   /* make sure item_type exists and is valid */
   sait = code_get_seq( SAIT, item->item_type, sizeof( item->item_type) );
   if ((sait == 0) || (sait == SAITTT_ERR))
   {
      rv = FALSE;
      WrBadTranError( INVLD_SAIT, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->item_type, sizeof( item->item_type));
      CLRTOERR( item->item_type);
   }
   else if ((sait != SAITTT_ITEM) &&
            (sait != SAITTT_REF) &&
            (sait != SAITTT_GCN) &&
            (EMPTY_FIELD( item->non_merch_item)))
   {
      rv = FALSE;
      WrBadTranError( NON_MERCH_ITEM_NO_REQ, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->non_merch_item, sizeof( item->non_merch_item ));
      CLRTOSPACES( item->non_merch_item );
   }

   /* make sure item exists */
   if (( sait == SAITTT_ITEM) || ( ! EMPTY_FIELD( item->item) ))
      if ( item_lookup( item->item) == NULL)
      {
         rv = FALSE;
         WrBadTranError( SKU_NOT_FOUND, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->item, sizeof( item->item));
         CLRTOSPACES( item->item );
         CPYSTRFXL( uom->class_vat_ind, YSNO_N, FT_VARCHAR );
         item_lookup_flag = FALSE;
      }
      else /* if ( item_lookup( item->item) IS NOT NULL) */
      {
         item_lookup_flag = TRUE;
      }

   /* make sure ref_item exists */
   if (( sait == SAITTT_REF) || ( ! EMPTY_FIELD( item->ref_item) ))
      if ( ref_item_lookup( item->ref_item) == NULL )
      {
         rv = FALSE;
         WrBadTranError( UPC_NOT_FOUND, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->ref_item, sizeof( item->ref_item));
         CLRTOSPACES( item->ref_item );
         ref_item_lookup_flag = FALSE;
         CPYSTRFXL( uom->class_vat_ind, YSNO_N, FT_VARCHAR );
      }
      else /* if ( ref_item_lookup( item->ref_item) IS NOT NULL) */
      {
         ref_item_lookup_flag = TRUE;
      }

   /* check for NMITEM */
   if (( sait == SAITTT_NMITEM) || ( ! EMPTY_FIELD( item->non_merch_item) ))
      if ( item_lookup( item->non_merch_item) == NULL )
      {
         CPYSTRFXL( uom->class_vat_ind, YSNO_N, FT_VARCHAR );
      }

   /* make sure voucher number exists */
   if (( sait == SAITTT_GCN) || ( ! EMPTY_FIELD( item->voucher_no) ))
      if ( EMPTY_FIELD( item->voucher_no) )
      {
         rv = FALSE;
         WrBadTranError( INVLD_DOCUMENT, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->voucher_no, sizeof( item->voucher_no));
         CLRTOSPACES( item->voucher_no );
      }

   /* make sure dept exists and is numeric */
   if ( ! EMPTY_FIELD( item->dept) )
      if ( SA_VALID_ALL_NUMERIC( item->dept) != 0 )
      {
         rv = FALSE;
         WrBadTranError( TITEM_DEP_STIN, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->dept, sizeof( item->dept));
         CLRTOSPACES( item->dept );
      }

   /* make sure class exists and is numeric */
   if ( ! EMPTY_FIELD( item->class) )
      if ( SA_VALID_ALL_NUMERIC( item->class) != 0 )
      {
         rv = FALSE;
         WrBadTranError( TITEM_CLS_STIN, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->class, sizeof( item->class));
         CLRTOSPACES( item->class );
      }

   /* make sure subclass exists and is numeric */
   if ( ! EMPTY_FIELD( item->subclass) )
      if ( SA_VALID_ALL_NUMERIC( item->subclass) != 0 )
      {
         rv = FALSE;
         WrBadTranError( TITEM_SBC_STIN, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->subclass, sizeof( item->subclass));
         CLRTOSPACES( item->subclass );
      }

   /* make sure qty exists and is numeric */
   if (( EMPTY_FIELD( item->qty) ) ||
       ( SA_VALID_ALL_NUMERIC( item->qty) != 0 ) ||
       ( code_lookup( SIGN, item->qty_sign, sizeof( item->qty_sign)) == NULL ))
   {
      char ls_qty[LEN_SIGNED_DEC_QTY];

      rv = FALSE;
      li_qty_urt_error = 1;

      CPYFXLFXL(ls_qty, item->qty, FT_NUMBER);
      ls_qty[0] = signcvt( item->qty_sign);
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( ls_qty, QTY_DECPLACES);
#endif
      WrBadTranError( TITEM_QTY_STIN, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      ls_qty, sizeof( ls_qty));
      CLRTOSPACES( item->qty );
   }
   /* If the transaction type is sale and the sub transaction type
      is accommodation then the unit_retail can be empty.
      Otherwise make sure unit_retail exists and is numeric */
   if ( ( (CurTrat == TRATTT_SALE && CurSubTrat == TRASTT_CACCOM)
          && ( SA_VALID_ALL_NUMERIC( item->unit_retail) != 0 ) )
        ||
        ( !(CurTrat == TRATTT_SALE && CurSubTrat == TRASTT_CACCOM)
          && ( EMPTY_FIELD( item->unit_retail) ||
               SA_VALID_ALL_NUMERIC( item->unit_retail) != 0 ) ) )
   {
      char ls_unit_retail[LEN_DEC_AMT];

      rv = FALSE;
      li_qty_urt_error = 1;

      CPYFXLFXL( ls_unit_retail, item->unit_retail, FT_NUMBER);
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( ls_unit_retail, AMT_DECPLACES);
#endif
      WrBadTranError( TITEM_URT_STIN, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      ls_unit_retail, sizeof( ls_unit_retail));
      CLRTOSPACES( item->unit_retail );
   }

   /* make sure standard_qty and standard_unit_retail exists and is numeric */
   if ( CMPFXLSTR( uom->conversion_error, YSNO_Y ) ||
      ( li_qty_urt_error == 1 ) )
   {
      rv = FALSE;
      if ( CMPFXLSTR( uom->conversion_error, YSNO_Y ) )
         WrBadTranError( INVLD_SELLING_UOM, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->selling_uom, sizeof( item->selling_uom));
      CLRTOSPACES( uom->standard_qty );
      CLRTOSPACES( uom->standard_unit_retail );
      CLRTOSPACES( uom->standard_orig_unit_retail );
   }

   /* if override reason exists, make sure it is valid */
   if ( ! EMPTY_FIELD( item->override_reason) )
   {
      if (code_lookup( ORRC, item->override_reason,
                       sizeof( item->override_reason)) == NULL)
      {
         rv = FALSE;
         WrBadTranError( INVLD_ORRC, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->override_reason, sizeof( item->override_reason));
         CLRTOERR( item->override_reason );
      }

      /* make sure orig_unit_retail exists and is numeric */
      if (( EMPTY_FIELD( item->orig_unit_retail) ) ||
          ( SA_VALID_ALL_NUMERIC( item->orig_unit_retail) != 0 ))
      {
         char ls_orig_unit_retail[LEN_DEC_AMT];

         rv = FALSE;
         CPYFXLFXL( ls_orig_unit_retail, item->orig_unit_retail, FT_NUMBER);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_orig_unit_retail, AMT_DECPLACES);
#endif
         WrBadTranError( TITEM_OUR_STIN, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         ls_orig_unit_retail, sizeof( ls_orig_unit_retail));
         CLRTOSPACES( item->orig_unit_retail );
      }
   }

   /* make sure tax_ind has a valid value */
   if ( code_lookup( YSNO, item->tax_ind, sizeof( item->tax_ind) ) == NULL )
   {
      rv = FALSE;
      WrBadTranError( INVLD_TAX_IND, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->tax_ind, sizeof( item->tax_ind));
      CPYSTRFXL( item->tax_ind, YSNO_Y, FT_VARCHAR );
   }

   /* make sure item_swiped_ind has a valid value*/
   if (code_lookup( YSNO, item->item_swiped_ind,
                    sizeof( item->item_swiped_ind) ) == NULL)
   {
      rv = FALSE;
      WrBadTranError( INVLD_ITEM_SWIPED_IND, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->item_swiped_ind, sizeof( item->item_swiped_ind));
      CPYSTRFXL( item->item_swiped_ind, YSNO_Y, FT_VARCHAR );
   }

   /* make sure that if item status is SASI_R then
      there is a valid return reason code */
   if ((sasi == SASITT_R) && (!EMPTY_FIELD( item->return_reason_code)))
      if (code_lookup( SARR, item->return_reason_code,
                     sizeof( item->return_reason_code) ) == NULL)
      {
         rv = FALSE;
         WrBadTranError( INVLD_SARR, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->return_reason_code, sizeof( item->return_reason_code));
         CLRTOERR( item->return_reason_code );
      }

   /* if sa_system_options.auto_validate_tran_employee_id, validate
      salesperson if present */
   if (strcmp(SysOpt.s_auto_validate_tran_employee_id, YSNO_Y) == 0)
   {
      if (( ! EMPTY_FIELD( item->salesperson) ) &&
          ( employee_lookup(Location, item->salesperson) == NULL ))
      {
         rv = FALSE;
         WrBadTranError( INVLD_SALESPERSON_ID, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->salesperson, sizeof( item->salesperson));
         CLRTOSPACES (item->salesperson );
         CLRTOERR( item->salesperson );
      }
   }

   /* if expiration date is not null, then validate it */
   if ((!EMPTY_FIELD( item->expiration_date)) &&
       (SA_VALID_DATE( item->expiration_date) != 0 ))
   {
      rv = FALSE;
      WrBadTranError( INVLD_EXP_DATE, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->expiration_date, sizeof( item->expiration_date));
      CLRTOSPACES( item->expiration_date );
   }

   /* Make sure drop_ship_ind has a valid value.
      If anything other than Y or N, write error
      and set to N. */

   if ( code_lookup( YSNO, item->drop_ship_ind, sizeof( item->drop_ship_ind) ) == NULL )
   {
      rv = FALSE;
      WrBadTranError( SET_DROP_SHIP, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->drop_ship_ind, sizeof(item->drop_ship_ind) );
      CPYSTRFXL( item->drop_ship_ind, YSNO_N, FT_VARCHAR );
   }

   /* make sure catch weight ind has a valid value if set */
   CurCWInd = 0; /* CW ind = 0 as default means that the UOM qty will be the same as ITEM qty */
   if ( ! EMPTY_FIELD( item->catchweight_ind ) )
      if ( code_lookup( YSNO,
                        item->catchweight_ind,
                        sizeof( item->catchweight_ind) ) == NULL )
      {
         if (item_lookup_flag == TRUE)
         {
            tableitem = item_lookup(item->item);
            CPYSTRFXL( item->catchweight_ind, tableitem->catchweight_ind, FT_VARCHAR );
         }
         /* Find out what the catchweigth_ind should be for UOM qty validation */
         if ( sait == SAITTT_ITEM)
         {
            if (item_lookup_flag == TRUE)
            {
               /* tableitem = item_lookup(item->item); */
               if ( CMPFXLSTR( tableitem->catchweight_ind, YSNO_Y ) )
                  CurCWInd = 1;
               else
                  CurCWInd = 0;
            }
         }
         else if ( sait == SAITTT_REF )
         {
            if (ref_item_lookup_flag == TRUE)
            {
               tablerefitem = ref_item_lookup(item->ref_item);
               tableitem = item_lookup(tablerefitem->item);
               if ( CMPFXLSTR( tableitem->catchweight_ind, YSNO_Y ) )
                  CurCWInd = 1;
               else
                  CurCWInd = 0;
            }
         }
      }

   /* The catchweight_ind for the item in the database should match the RTLOG record. */
   if ( sait == SAITTT_ITEM)
   {
      if (item_lookup_flag == TRUE)
      {
         tableitem = item_lookup(item->item);
         if ( strncmp(tableitem->catchweight_ind, item->catchweight_ind, LEN_CATCHWEIGHT_IND) != 0 )
         {
            CPYSTRFXL( item->catchweight_ind, tableitem->catchweight_ind, FT_VARCHAR );
         }
         if ( CMPFXLSTR( tableitem->catchweight_ind, YSNO_Y ) )
            CurCWInd = 1;
         else
            CurCWInd = 0;
      }
   }
   else if ( sait == SAITTT_REF )
   {
      if (ref_item_lookup_flag == TRUE)
      {
         tablerefitem = ref_item_lookup(item->ref_item);
         tableitem = item_lookup(tablerefitem->item);
         if ( strncmp(tableitem->catchweight_ind, item->catchweight_ind, LEN_CATCHWEIGHT_IND) != 0 )
         {
            tableitem = item_lookup(item->item);
            CPYSTRFXL( item->catchweight_ind, tableitem->catchweight_ind, FT_VARCHAR );
            tableitem = item_lookup(tablerefitem->item);
         }
         if ( CMPFXLSTR( tableitem->catchweight_ind, YSNO_Y ) )
            CurCWInd = 1;
         else
            CurCWInd = 0;
      }
   }

   /* If the catchweight_ind is 'N' then the UOM quantity should be equal to qty */
   if ( CurCWInd == 0 )
   {
      CPYFXLFXL( item->uom_qty, item->qty, FT_NUMBER);
   }
   else
   {
      /* Make sure uom_qty exists and is numeric. */
      if (( EMPTY_FIELD( item->uom_qty) ) ||
          ( SA_VALID_ALL_NUMERIC( item->uom_qty) != 0 ))
      {
         char ls_uom_qty[LEN_UOM_QTY];

         rv = FALSE;

         CPYFXLFXL( ls_uom_qty, item->uom_qty, FT_NUMBER);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_uom_qty, AMT_DECPLACES);
#endif
         WrBadTranError( TITEM_UOM_QTY_STIN, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         ls_uom_qty, sizeof( ls_uom_qty));
         CLRTOZEROS( item->uom_qty );
      }
      if ((atoin(item->qty,LEN_QTY)==0 && atoin(item->uom_qty,LEN_UOM_QTY)!=0) ||
          (atoin(item->qty,LEN_QTY)!=0 && atoin(item->uom_qty,LEN_UOM_QTY)==0))
      {
         char ls_uom_qty[LEN_UOM_QTY];

         rv = FALSE;

         CPYFXLFXL( ls_uom_qty, item->uom_qty, FT_NUMBER);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_uom_qty, AMT_DECPLACES);
#endif

         WrBadTranError( TITEM_UOM_QTY_ZERO, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         ls_uom_qty, sizeof( ls_uom_qty));
      }
   }

   /* Make sure cust_ord_line_no is numeric if it exists. */
   if ( ! EMPTY_FIELD( item->cust_ord_line_no ) )
      if ( SA_VALID_ALL_NUMERIC( item->cust_ord_line_no ) != 0 )
      {
         rv = FALSE;
         WrBadTranError( TITEM_CUST_ORD_STIN, TranSeqNo, item_seq_no, -1, SART_TITEM,
                         item->cust_ord_line_no, sizeof(item->cust_ord_line_no));
         CLRTOSPACES(item->cust_ord_line_no);
      }

   /* If media_id exists make sure its numeric */
   if ( ! EMPTY_FIELD( item->media_id ) )
      if ( SA_VALID_ALL_NUMERIC( item->media_id ) != 0 )
      {
         rv = FALSE;
         WrBadTranError( TITEM_MID_STIN, TranSeqNo, item_seq_no, -1, SART_TITEM,
                         item->media_id, sizeof( item->media_id));
         CLRTOSPACES( item->media_id);
      }

   /* Validate total igtax amount in the TITEM line.  If the field is NOT empty, make sure it is numeric */
   /* Do not flag any errors, online forms will recalculate this field based on the IGTAX line. */

   if ( ! EMPTY_FIELD( item->total_igtax_amt ) )
      if ( SA_VALID_ALL_NUMERIC( item->total_igtax_amt ) != 0 )
      {
         CLRTOSPACES(item->total_igtax_amt);
      }

   /* Make sure Total Retail is greater than Total IGTAX Amount */
   if ( ! EMPTY_FIELD( item->unit_retail) &&
        ! EMPTY_FIELD( item->qty ) &&
        ! EMPTY_FIELD( item->total_igtax_amt) )
   {
      char qty[NULL_SIGNED_DEC_QTY];
      char unit_retail[NULL_DEC_AMT];

      char TotalRetail[NULL_SIGNED_AMT+4]; /* +4 because this gets multipled by 10000 twice: qty & amt */
      char ItemTotalRetail[NULL_SIGNED_AMT]; /* length 21 hold the sign */
      char ItemTotalIGTaxAmt[NULL_SIGNED_AMT];

      memset( TotalRetail, '0', sizeof( TotalRetail) - 1);
      TotalRetail[ sizeof( TotalRetail) - 1] = '\0';
      TotalRetail[0] = ' ';

      memset( ItemTotalRetail, '0', sizeof( ItemTotalRetail) - 1);
      ItemTotalRetail[ sizeof( ItemTotalRetail) - 1] = '\0';
      ItemTotalRetail[0] = ' ';

      memset( ItemTotalIGTaxAmt, '0', sizeof( ItemTotalIGTaxAmt) - 1);
      ItemTotalIGTaxAmt[ sizeof( ItemTotalIGTaxAmt) - 1] = '\0';
      ItemTotalIGTaxAmt[0] = ' ';

      num2str( qty, sizeof( qty), item->qty, sizeof( item->qty), item->qty_sign);
      num2str( unit_retail, sizeof(unit_retail),
               item->unit_retail, sizeof( item->unit_retail) , SIGN_P);

      num2str( ItemTotalIGTaxAmt, sizeof(ItemTotalIGTaxAmt),
               uom->total_igtax_amt, sizeof( uom->total_igtax_amt) , SIGN_P);
      ItemTotalIGTaxAmt[0] = ' ';

      /* Total Retail (Unit Retail * qty) will be considered in ItemTotalRetail for validating with ItemTotalIGTaxAmt */
      OraNumQtyRtl( unit_retail, qty, TotalRetail, sizeof(TotalRetail),
                    Currency_rtl_dec, "10000" );
      OraNumAdd( TotalRetail, ItemTotalRetail, ItemTotalRetail, sizeof( ItemTotalRetail));

      if ( strcmp(ItemTotalIGTaxAmt,ItemTotalRetail) > 0 )
      {
         char msg[ NULL_ORIG_VALUE ];
         rv = FALSE;
         sprintf( msg, "TR:%s TIG:%s", ItemTotalRetail, ItemTotalIGTaxAmt);
         WrBadTranError(TOTAL_IGTAX_AMT_GT_TOTRET, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        msg, strlen(msg));
         CLRTOZEROS(ItemTotalIGTaxAmt);
      }
   }

   sasy = code_get_seq( SASY, item->sales_type, sizeof( item->sales_type) );

   if (sasy == 0)
   {
      rv = FALSE;
      WrBadTranError( INVLD_SASY, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->sales_type, sizeof( item->sales_type));
      CLRTOSPACES( item->sales_type);
   }

   /* Validate the sales type. */

   /* The sales type of 'E'xternal Customer Order is only for a transaction with item status of ORI, ORC, ORD, and R */
   /* For transactions with item status of LIN, LCA or LCO, the sales type should only be 'I'n Store Customer Order */
   if (((sasy == SASYTT_E) &&
        (sasi != SASITT_ORI) &&
        (sasi != SASITT_ORD) &&
        (sasi != SASITT_ORC) &&
        (sasi != SASITT_R)) ||
	   (((sasi == SASITT_LIN) ||
         (sasi == SASITT_LCA) ||
		 (sasi == SASITT_LCO)) &&
		 (sasy != SASYTT_I)))
   {
      rv = FALSE;
      WrBadTranError( INVLD_SASY_E, TranSeqNo,
                      item_seq_no, -1, SART_TITEM,
                      item->sales_type, sizeof( item->sales_type));
   }

   CurInvInd = 0;
   if ( !EMPTY_FIELD( item->return_wh ))
   {
      if ( SA_VALID_ALL_NUMERIC( item->return_wh ) != 0 )
      {
         rv = FALSE;
         WrBadTranError( TITEM_WH_STIN, TranSeqNo, item_seq_no, -1, SART_TITEM,
                         item->media_id, sizeof( item->return_wh));
         CLRTOSPACES( item->return_wh);
      }
      if (sasy == SASYTT_E)
      {
         if (((gi_rsys == RSYSTT_OMS) && (sasi != SASITT_R)) || (gi_rsys != RSYSTT_OMS) || (gi_tsys != TSYSTT_OMS))
         {
            rv = FALSE;
            WrBadTranError(RETWH_OMS, TranSeqNo, item_seq_no, -1, SART_TITEM,
                            item->return_wh, sizeof(item->return_wh));
         }
      }
      else
      {
         rv = FALSE;
         WrBadTranError(RETWH_EXT_ORD, TranSeqNo, item_seq_no, -1, SART_TITEM,
                        item->return_wh, sizeof(item->return_wh));
      }
      if ( wh_lookup( item->return_wh) == NULL)
      {
         rv = FALSE;
         WrBadTranError(RETWH_NOT_FOUND, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        item->return_wh, sizeof( item->return_wh));
      }
   }
   else /* Return WH is NULL */
   {
      if ((sasy == SASYTT_E) && (gi_rsys == RSYSTT_OMS) && (gi_tsys == TSYSTT_OMS) && (sasi == SASITT_R))
      {
         if ( sait == SAITTT_ITEM)
         {
            if (item_lookup_flag == TRUE)
            {
               tableitem = item_lookup(item->item);
               if ( CMPFXLSTR( tableitem->inventory_ind, YSNO_Y ) )
                  CurInvInd = 1;
               else
                  CurInvInd = 0;
            }
         }
         else if ( sait == SAITTT_REF )
         {
            if (ref_item_lookup_flag == TRUE)
            {
               tablerefitem = ref_item_lookup(item->ref_item);
               tableitem = item_lookup(tablerefitem->item);
   
               if ( CMPFXLSTR( tableitem->inventory_ind, YSNO_Y ) )
                  CurInvInd = 1;
               else
                  CurInvInd = 0;
            }
         }
         if (CurInvInd == 1)
         {
            rv = FALSE;
            WrBadTranError(RETWH_REQ, TranSeqNo, item_seq_no, -1, SART_TITEM,
                           item->return_wh, sizeof(item->return_wh));
         }
      }
   }

   if ( !EMPTY_FIELD( item->no_inv_ret_ind ))
   {

      /* make sure item_swiped_ind has a valid value*/
      if (code_lookup( YSNO, item->no_inv_ret_ind,
                       sizeof( item->no_inv_ret_ind) ) == NULL)
      {
         rv = FALSE;
         WrBadTranError( INVLD_NO_INV_RET_IND, TranSeqNo,
                         item_seq_no, -1, SART_TITEM,
                         item->no_inv_ret_ind, sizeof( item->no_inv_ret_ind));
         CLRTOSPACES( item->no_inv_ret_ind);
      }

      if (sasi != SASITT_R)
      {
         rv = FALSE;
         WrBadTranError(NO_INV_RET_IND_RETURN, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        item->no_inv_ret_ind, sizeof( item->no_inv_ret_ind));
      }

   }
   else
   {
      if ((sasy == SASYTT_E) && (gi_rsys == RSYSTT_OMS) && (gi_tsys == TSYSTT_OMS) && (sasi == SASITT_R))
      {
         rv = FALSE;
         WrBadTranError(NO_INV_RET_IND_REQ, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        item->no_inv_ret_ind, sizeof( item->no_inv_ret_ind));
      }
   }


   if (EMPTY_FIELD( item->fulfill_order_no ) && (sasy == SASYTT_E) && (sasi == SASITT_ORD))
   {
      rv = FALSE;
      WrBadTranError(FULFILL_ORDER_NO_REQ, TranSeqNo,
                     item_seq_no, -1, SART_TITEM,
                     item->fulfill_order_no, sizeof( item->fulfill_order_no));
   }

   if (!EMPTY_FIELD( item->fulfill_order_no ))
   {
      if ((sasy != SASYTT_E) || ((sasi != SASITT_ORD) && (sasi != SASITT_ORC)))
      {
         rv = FALSE;
         WrBadTranError(FULFILL_ORDER_NO_ERR, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        item->fulfill_order_no, sizeof( item->fulfill_order_no));
      }
   }

   if ( !EMPTY_FIELD( item->return_disp ))
   {
      if ( CMPFXLSTR( item->no_inv_ret_ind, YSNO_Y ))
      {
         rv = FALSE;
         WrBadTranError(RETDISP_INV_RET, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        item->return_disp, sizeof( item->return_disp));
      }

      if (sasy != SASYTT_E)
      {
         rv = FALSE;
         WrBadTranError(RETDISP_EXT_ORD, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        item->return_disp, sizeof( item->return_disp));
      }

      if (((gi_rsys == RSYSTT_OMS) && (sasi != SASITT_R)) || (gi_rsys != RSYSTT_OMS) || (gi_tsys != TSYSTT_OMS))
      {
         rv = FALSE;
         WrBadTranError(RETDISP_OMS, TranSeqNo, item_seq_no, -1, SART_TITEM,
                        item->return_disp, sizeof(item->return_disp));
      }

      if ( invstatus_lookup( item->return_disp) == NULL)
      {
         rv = FALSE;
         WrBadTranError(INVLD_RETURN_DISP, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        item->return_disp, sizeof( item->return_disp));
      }

   }
   else
   {
      if ((sasy == SASYTT_E) && CMPFXLSTR( item->no_inv_ret_ind, YSNO_N ) &&
          (gi_rsys == RSYSTT_OMS) && (gi_tsys == TSYSTT_OMS) && (sasi == SASITT_R))
      {
         rv = FALSE;
         WrBadTranError(RETURN_DISP_REQ, TranSeqNo,
                        item_seq_no, -1, SART_TITEM,
                        item->return_disp, sizeof( item->return_disp));
      }
   }

   return( rv );
}


/*-------------------------------------------------------------------------
mvSATDisc() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSATDisc( RTL_IDISC *disc,
               int item_seq_no,
               int disc_seq_no,
               pt_disc_uom *disc_uom )
{
   int rv = TRUE;
   int prmt, sadt;
   PROMDAT *p;

   if (strcmp( SysOpt.s_rpm_ind, YSNO_Y) == 0)
   {
      /* make sure rms_promo_type exists and is valid */
      prmt = code_get_seq( PRMT, disc->rms_promo_type, sizeof( disc->rms_promo_type) );
      if (prmt == 0)
      {
         rv = FALSE;
         WrBadTranError( INVLD_PRMT, TranSeqNo,
                         item_seq_no, disc_seq_no, SART_IDISC,
                         disc->rms_promo_type, sizeof( disc->rms_promo_type));
         CLRTOERR( disc->rms_promo_type );
      }
      
      /* make sure disc_ref_no exists and is valid */
      if ( prmt == PRMTTT_9999 )
      {
         /* This is a promotion, so check the promo_comp is valid */
         if ( ! EMPTY_FIELD( disc->disc_ref_no) )
         {
            p = prom_lookup( disc->disc_ref_no );
            if (p == NULL)
            {
               rv = FALSE;
               WrBadTranError( IDISC_DRN_STIN, TranSeqNo,
                               item_seq_no, disc_seq_no, SART_IDISC,
                               disc->disc_ref_no, sizeof( disc->disc_ref_no));
               CLRTOSPACES( disc->disc_ref_no );
            }
            else
            {
               if (EMPTY_FIELD( disc->promo_comp))
               {
                  rv = FALSE;
                  WrBadTranError( PROM_COMP_REQ, TranSeqNo,
                                 item_seq_no, disc_seq_no, SART_IDISC,
                                 disc->promo_comp, sizeof( disc->promo_comp));
                  CLRTOSPACES( disc->promo_comp );
               }
               else
               {
                  /* Validate the promotion component field */
                  p = prom_comp_lookup( disc->disc_ref_no, disc->promo_comp );
                  if (p == NULL)
                  {
                     rv = FALSE;
                     WrBadTranError( IDISC_PRC_STIN, TranSeqNo,
                                     item_seq_no, disc_seq_no, SART_IDISC,
                                     disc->promo_comp, sizeof( disc->promo_comp));
                     CLRTOSPACES( disc->promo_comp );
                  }
               }
            }
         }
         else
         {
            /* Throw error message for the NULL value in discount ref no */
            rv = FALSE;
            WrBadTranError( DISC_REF_NO_REQ, TranSeqNo,
                            item_seq_no, disc_seq_no, SART_IDISC,
                            disc->disc_ref_no, sizeof( disc->disc_ref_no));
            CLRTOSPACES( disc->disc_ref_no );
      
            /* validate the component exists for a discount */
            if (EMPTY_FIELD( disc->promo_comp))
            {
                rv = FALSE;
                WrBadTranError( PROM_COMP_REQ, TranSeqNo,
                              item_seq_no, disc_seq_no, SART_IDISC,
                              disc->promo_comp, sizeof( disc->promo_comp));
                CLRTOSPACES( disc->promo_comp );
            }
            else
            {
               /* Validate the promotion component field */
               p = prom_comp_lookup( disc->disc_ref_no, disc->promo_comp );
               if (p == NULL)
               {
                  rv = FALSE;
                  WrBadTranError( IDISC_PRC_STIN, TranSeqNo,
                                  item_seq_no, disc_seq_no, SART_IDISC,
                                  disc->promo_comp, sizeof( disc->promo_comp));
                  CLRTOSPACES( disc->promo_comp );
               }
            }
         }
      }
   }

   /* make sure that if disc_type exists and valid */
   sadt = code_get_seq( SADT, disc->disc_type, sizeof( disc->disc_type) );
   if ( ( ! EMPTY_FIELD( disc->disc_type)) &&
        (sadt == 0) )
   {
      rv = FALSE;
      WrBadTranError( INVLD_SADT, TranSeqNo,
                      item_seq_no, disc_seq_no, SART_IDISC,
                      disc->disc_type, sizeof( disc->disc_type));
      CLRTOERR( disc->disc_type );
   }
   else
   {
      /* make sure coupon_no is given for SCOUP disc type */
      if ((sadt == SADTTT_SCOUP) && (EMPTY_FIELD(disc->coupon_no)))
      {
          rv = FALSE;
          WrBadTranError( IDISC_COUPON_NO_REQ, TranSeqNo,
                          item_seq_no, disc_seq_no, SART_IDISC,
                          disc->coupon_no, sizeof( disc->coupon_no));
          CLRTOERR( disc->coupon_no );
      }
   }

   /*make sure a discount reason exists for the discount*/
   prmt = code_get_seq( PRMT, disc->rms_promo_type, sizeof( disc->rms_promo_type) );
   if (((prmt != PRMTTT_9999) && (prmt != PRMTTT_2000)) && (EMPTY_FIELD( disc->disc_type)))
   {
       rv = FALSE;
       WrBadTranError( DISC_REASON_REQ, TranSeqNo,
                     item_seq_no, disc_seq_no, SART_IDISC,
                     disc->disc_type, sizeof( disc->disc_type));
       CLRTOSPACES( disc->disc_type );
   }

   /* make sure qty exists and is numeric */
   if (( EMPTY_FIELD( disc->qty) ) ||
       ( SA_VALID_ALL_NUMERIC( disc->qty) != 0 ) ||
       ( code_lookup( SIGN, disc->qty_sign, sizeof( disc->qty_sign)) == NULL ))
      {
         char ls_qty[LEN_SIGNED_DEC_QTY];

         rv = FALSE;
         CPYFXLFXL(ls_qty, disc->qty, FT_NUMBER);
         ls_qty[0] = signcvt( disc->qty_sign);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_qty, QTY_DECPLACES);
#endif
         WrBadTranError( IDISC_QTY_STIN, TranSeqNo,
                         item_seq_no, disc_seq_no, SART_IDISC,
                         ls_qty, sizeof( ls_qty));
         CLRTOSPACES( disc->qty );
      }

   /* make sure unit_disc_amt exists and is numeric */
   if (( EMPTY_FIELD( disc->unit_disc_amt ) ) ||
       ( SA_VALID_ALL_NUMERIC( disc->unit_disc_amt ) != 0 ) )
      {
         char ls_unit_disc_amt[LEN_DEC_AMT];

         rv = FALSE;
         CPYFXLFXL(ls_unit_disc_amt,disc->unit_disc_amt,FT_NUMBER);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_unit_disc_amt,AMT_DECPLACES );
#endif
         WrBadTranError(IDISC_UDA_STIN, TranSeqNo,
                        item_seq_no, disc_seq_no, SART_IDISC,
                        ls_unit_disc_amt, sizeof( ls_unit_disc_amt ));
         CLRTOSPACES(disc->unit_disc_amt);
      }

   /* make sure standard_qty and standard_unit_disc_amt exist and are numeric */
   if ( CMPFXLSTR( disc_uom->conversion_error, YSNO_Y ) )
      {
         char ls_qty[LEN_SIGNED_DEC_QTY];
         char ls_unit_disc_amt[LEN_DEC_AMT];

         rv = FALSE;
         CPYFXLFXL(ls_qty, disc_uom->standard_qty, FT_NUMBER);
         CPYFXLFXL(ls_unit_disc_amt, disc_uom->standard_unit_disc_amt, FT_NUMBER);
         ls_qty[0] = signcvt( disc->qty_sign);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_qty, QTY_DECPLACES);
         FIX_DP( ls_unit_disc_amt, AMT_DECPLACES);
#endif
         WrBadTranError( INVLD_DISCOUNT_UOM, TranSeqNo,
                         item_seq_no, disc_seq_no, SART_IDISC,
                         "", 0);
         CLRTOSPACES(disc_uom->standard_qty);
         CLRTOSPACES(disc_uom->standard_unit_disc_amt);
      }

   /* make sure catch weight ind has a valid value if set */
   if ( ! EMPTY_FIELD( disc->catchweight_ind ) )
      if ( code_lookup( YSNO,
                        disc->catchweight_ind,
                        sizeof( disc->catchweight_ind) ) == NULL )
      {
         if ( CurCWInd == 1 )
            CPYSTRFXL( disc->catchweight_ind, YSNO_Y, FT_VARCHAR );
         else
            CPYSTRFXL( disc->catchweight_ind, YSNO_N, FT_VARCHAR );
      }

   /* Make sure the catch weight ind matches the value for the item */
   if ( ( CurCWInd == 1 && !CMPFXLSTR( disc->catchweight_ind, YSNO_Y ) )
        || ( CurCWInd == 0 && CMPFXLSTR( disc->catchweight_ind, YSNO_Y ) ) )
   {
      if ( CurCWInd == 1 )
         CPYSTRFXL( disc->catchweight_ind, YSNO_Y, FT_VARCHAR );
      else
         CPYSTRFXL( disc->catchweight_ind, YSNO_N, FT_VARCHAR );
   }

   if ( CurCWInd == 0 )
   {
      CPYFXLFXL( disc->uom_qty, disc->qty, FT_NUMBER);
   }
   else
   {
      /* Make sure uom_qty exists and has is numeric. */
      if (( EMPTY_FIELD( disc->uom_qty) ) ||
          ( SA_VALID_ALL_NUMERIC( disc->uom_qty) != 0 ))
      {
         char ls_uom_qty[LEN_UOM_QTY];

         rv = FALSE;

         CPYFXLFXL( ls_uom_qty, disc->uom_qty, FT_NUMBER);
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( ls_uom_qty, AMT_DECPLACES);
#endif
         WrBadTranError( IDISC_UOM_QTY_STIN, TranSeqNo,
                         item_seq_no, disc_seq_no, SART_IDISC,
                         ls_uom_qty, sizeof( ls_uom_qty));
         CLRTOZEROS( disc->uom_qty );
      }
   }

   return( rv );
}


/*-------------------------------------------------------------------------
mvSAIGtax() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSAIGtax( RTL_IGTAX *igtax,
               int item_seq_no,
               int igtax_seq_no,
               int igtax_seq,
               pt_igtax_tia *igtax_tia,
               char loc[ NULL_LOC ])
{
   char *function = "mvSAIGtax";
   int rv = TRUE;
   int i;
   pt_igtax **t;
   int l_vat_result;
   char ls_vat_calc_type [2];

   l_vat_result = fetchvatcalctype(loc,ls_vat_calc_type);

   /* Allocate more space as needed for the IGTAX records */
   if ( igtax_seq >= UsedTranIgtaxs )
   {
      t = (pt_igtax **)realloc( la_igtax, (UsedTranIgtaxs + ALLOCTRANIGTAXS) *
                                          sizeof(pt_igtax *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                     "Cannot allocate space for la_igtax" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      la_igtax = t;
      for (i = UsedTranIgtaxs; i < (UsedTranIgtaxs + ALLOCTRANIGTAXS); i++)
         la_igtax[ i ] = NULL;
      UsedTranIgtaxs += ALLOCTRANIGTAXS;
   }

   if ( la_igtax[ igtax_seq ] == NULL )
   {
      la_igtax[ igtax_seq ] = (pt_igtax *)malloc( sizeof( pt_igtax ) );
      if ( la_igtax[ igtax_seq ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for la_igtax record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   /* make sure igtax code exists and is valid */
   /* validate against TAXC in SALES environment */
   /* validate against VATC in SVAT and GTAX environments */
   if ( strcmp( SysOpt.s_default_tax_type, "SALES") == 0 || (strcmp( SysOpt.s_default_tax_type, "SVAT") == 0 && strcmp(ls_vat_calc_type,"E") == 0))
   {
      if ( code_lookup( TAXC, igtax->igtax_code, sizeof( igtax->igtax_code) ) == NULL )
      {
         rv = FALSE;
         WrBadTranError( INVLD_TAXC, TranSeqNo,
                         item_seq_no, igtax_seq_no, SART_IGTAX,
                         igtax->igtax_code, sizeof( igtax->igtax_code));
         CLRTOERR( igtax->igtax_code );
      }
   }
   else if ( strcmp( SysOpt.s_default_tax_type, "SVAT") == 0 || strcmp( SysOpt.s_default_tax_type, "GTAX") == 0 )
   {
      if ( code_lookup( VATC, igtax->igtax_code, sizeof( igtax->igtax_code) ) == NULL )
      {
         rv = FALSE;
         WrBadTranError( INVLD_VATC, TranSeqNo,
                         item_seq_no, igtax_seq_no, SART_IGTAX,
                         igtax->igtax_code, sizeof( igtax->igtax_code));
         CLRTOERR( igtax->igtax_code );
      }
   }

   /* make sure igtax_rate exists and is numeric */
   if ( !EMPTY_FIELD( igtax->igtax_rate ) )
   {
      if ( SA_VALID_ALL_NUMERIC( igtax->igtax_rate ) != 0 )
      {
          char ls_igtax_rate[LEN_DEC_AMT];

          rv = FALSE;
          CPYFXLFXL(ls_igtax_rate,igtax->igtax_rate,FT_NUMBER);
   #ifdef ADD_DECIMAL_POINTS
          FIX_DP( ls_igtax_rate,AMT_DECPLACES );
   #endif
          WrBadTranError(IGTAX_RAT_STIN, TranSeqNo,
                         item_seq_no, igtax_seq_no, SART_IGTAX,
                         ls_igtax_rate, sizeof( ls_igtax_rate ));
          CLRTOZEROS(igtax->igtax_rate);
      }
   }

   /* make sure total igtax amt exists and is numeric */
   if (( EMPTY_FIELD( igtax->total_igtax_amt ) ) ||
       ( SA_VALID_ALL_NUMERIC( igtax->total_igtax_amt ) != 0 ) ||
       ( code_lookup( SIGN, igtax->igtax_amt_sign, sizeof(igtax->igtax_amt_sign) ) == NULL ) )
   {
       char ls_total_igtax_amt[LEN_DEC_AMT];

       rv = FALSE;
       CPYFXLFXL(ls_total_igtax_amt,igtax->total_igtax_amt,FT_NUMBER);
       #ifdef ADD_DECIMAL_POINTS
          FIX_DP( ls_total_igtax_amt, AMT_DECPLACES);
       #endif
       ls_total_igtax_amt[0] = signcvt( igtax->igtax_amt_sign );
       WrBadTranError(TOTAL_IGTAX_AMT_STIN, TranSeqNo,
                      item_seq_no, igtax_seq_no, SART_IGTAX,
                      ls_total_igtax_amt, sizeof( ls_total_igtax_amt ));
       CLRTOZEROS(igtax->total_igtax_amt);
    }

   if ( !EMPTY_FIELD( igtax->igtax_code ) )
   {
      /* copy the igtax_code to the la_igtax structure */
      strncpy(la_igtax[igtax_seq]->igtax_code, igtax->igtax_code, LEN_IGTAX_CODE);
      nullpad(la_igtax[igtax_seq]->igtax_code, NULL_IGTAX_CODE);
   }

   if ( !EMPTY_FIELD( igtax->tax_authority ) )
   {
      /* copy the tax_authority to the la_igtax structure */
      strncpy(la_igtax[igtax_seq]->tax_authority, igtax->tax_authority, LEN_TAX_AUTHORITY);
      nullpad(la_igtax[igtax_seq]->tax_authority, NULL_TAX_AUTHORITY);
   }

   /* ref_21 refers to component item, in case of pack item */
   if ( !EMPTY_FIELD( igtax->ref_no21 ) )
   {
      /* copy the ref_no21 to the la_igtax structure */
      strncpy(la_igtax[igtax_seq]->pack_comp_item, igtax->ref_no21, LEN_ITEM);
      nullpad(la_igtax[igtax_seq]->pack_comp_item, NULL_ITEM);
   }

   return( rv );
}

/*-------------------------------------------------------------------------
mvSAIGtaxVal() does the duplicate IGTAX code validations. Returns TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSAIGTaxVal( int item_seq, int igtax_seq )
{
   char *function = "mvSAIGTaxVal";
   int rv = TRUE;
   int i, j;
   char ls_igtax_code[LEN_IGTAX_CODE];
   char ls_igtax_code_tmp[LEN_IGTAX_CODE];
   char ls_tax_authority[LEN_TAX_AUTHORITY];
   char ls_tax_authority_tmp[LEN_TAX_AUTHORITY];
   char ls_comp_item[LEN_ITEM];
   char ls_comp_item_tmp[LEN_ITEM];
   char msg[ NULL_ORIG_VALUE ];

   /* Validate for duplicate igtax codes */
   /* In case of pack item igtax code and component item(ref_21) should be unique */
   memset( ls_igtax_code, ' ', sizeof(ls_igtax_code) );
   ls_igtax_code[ sizeof( ls_igtax_code ) - 1 ] = '\0';
   memset( ls_igtax_code_tmp, ' ', sizeof(ls_igtax_code_tmp) );
   ls_igtax_code_tmp[ sizeof( ls_igtax_code_tmp ) - 1 ] = '\0';

   memset( ls_tax_authority, ' ', sizeof(ls_tax_authority) );
   ls_tax_authority[ sizeof( ls_tax_authority ) ] = '\0';
   memset( ls_tax_authority_tmp, ' ', sizeof(ls_tax_authority_tmp) );
   ls_tax_authority_tmp[ sizeof( ls_tax_authority_tmp ) ] = '\0';

   memset( ls_comp_item, ' ', sizeof(ls_comp_item) );
   ls_comp_item[ sizeof( ls_comp_item ) - 1 ] = '\0';
   memset( ls_comp_item_tmp, ' ', sizeof(ls_comp_item_tmp) );
   ls_comp_item_tmp[ sizeof( ls_comp_item_tmp ) - 1 ] = '\0';

   for (i=0;i<igtax_seq;i++)
   {
      strncpy(ls_igtax_code, la_igtax[i]->igtax_code,LEN_IGTAX_CODE);
      strncpy(ls_tax_authority, la_igtax[i]->tax_authority,LEN_TAX_AUTHORITY);
      strncpy(ls_comp_item, la_igtax[i]->pack_comp_item,LEN_ITEM);
      for (j=i+1;j<igtax_seq;j++)
      {
         strncpy(ls_igtax_code_tmp, la_igtax[j]->igtax_code,LEN_IGTAX_CODE);
         strncpy(ls_tax_authority_tmp, la_igtax[j]->tax_authority,LEN_TAX_AUTHORITY);
         strncpy(ls_comp_item_tmp, la_igtax[j]->pack_comp_item,LEN_ITEM);
         if ( strcmp( ls_comp_item, "" ) == 0 &&
              strcmp( ls_comp_item_tmp, "" ) == 0 )
         {
            if ( (CMPFXLSTR(ls_igtax_code, ls_igtax_code_tmp) ) && (CMPFXLSTR(ls_tax_authority, ls_tax_authority_tmp) ) )
            {
               rv = FALSE;
               sprintf( msg, "T:%s", ls_igtax_code );
               WrBadTranError(DUP_TAXC, TranSeqNo,
                              item_seq, i+1, SART_TITEM,
                              msg, strlen(msg));
            }
         }
         else if ( CMPFXLSTR(ls_igtax_code, ls_igtax_code_tmp) &&
                   CMPFXLSTR(ls_comp_item, ls_comp_item_tmp) &&
                   strcmp( ls_comp_item, "" ) != 0 &&
                   strcmp( ls_comp_item_tmp, "" ) != 0 )
            {
               rv = FALSE;
               sprintf( msg, "T:%s I:%s", ls_igtax_code, ls_comp_item);
               WrBadTranError(DUP_TAXC_COMPITEM, TranSeqNo,
                              item_seq, i+1, SART_TITEM,
                              msg, strlen(msg));
            }
      }
   }
   return( rv );
}

/*-------------------------------------------------------------------------
mvSATTax() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSATTax( RTL_TTAX *tax, int tax_seq_no, char loc[ NULL_LOC ] )
{
   int rv = TRUE;
   int l_vat_result;
   char ls_vat_calc_type [2];

   l_vat_result = fetchvatcalctype(loc,ls_vat_calc_type);
   /* make sure tax_code exists and is valid */
   /* validate against TAXC in SALES environment */
   /* validate against VATC in SVAT and GTAX environments */
   if ( strcmp( SysOpt.s_default_tax_type, "SALES") == 0 || (strcmp( SysOpt.s_default_tax_type, "SVAT") == 0 && strcmp(ls_vat_calc_type,"E")==0))
   {
      if ( code_lookup( TAXC, tax->tax_code, sizeof( tax->tax_code) ) == NULL )
      {
         rv = FALSE;
         WrBadTranError( INVLD_TAXC, TranSeqNo,
                         tax_seq_no, -1, SART_TTAX,
                         tax->tax_code, sizeof( tax->tax_code));
         CLRTOERR( tax->tax_code );
      }
   }
   else if ( strcmp( SysOpt.s_default_tax_type, "SVAT") == 0 || strcmp( SysOpt.s_default_tax_type, "GTAX") == 0 )
   {
      if ( code_lookup( VATC, tax->tax_code, sizeof( tax->tax_code) ) == NULL )
      {
         rv = FALSE;
         WrBadTranError( INVLD_VATC, TranSeqNo,
                         tax_seq_no, -1, SART_TTAX,
                         tax->tax_code, sizeof( tax->tax_code));
         CLRTOERR( tax->tax_code );
      }
   }

   /* make sure tax_amt exists and is numeric */
   if (( EMPTY_FIELD( tax->tax_amt) ) ||
       ( SA_VALID_ALL_NUMERIC( tax->tax_amt ) != 0 ) ||
       ( code_lookup( SIGN, tax->tax_amt_sign, sizeof( tax->tax_amt_sign)) == NULL ))
   {
      char ls_tax_amt[LEN_SIGNED_DEC_AMT];

      rv = FALSE;
      CPYFXLFXL(ls_tax_amt, tax->tax_amt, FT_NUMBER);
      ls_tax_amt[0] = signcvt( tax->tax_amt_sign);
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( ls_tax_amt, AMT_DECPLACES);
#endif
      WrBadTranError( TTAX_AMT_STIN, TranSeqNo,
                      tax_seq_no, -1, SART_TTAX,
                      ls_tax_amt, sizeof( ls_tax_amt));
      CLRTOZEROS( tax->tax_amt );
   }

   return( rv );
}


/*-------------------------------------------------------------------------
mvSATPymt() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSATPymt( RTL_TPYMT *pymt, int payment_seq_no )
{
   int rv = TRUE;

   /* make sure payment_amt exists and is numeric */
   if (( EMPTY_FIELD( pymt->payment_amt) ) ||
       ( SA_VALID_ALL_NUMERIC( pymt->payment_amt ) != 0 ) ||
       ( code_lookup( SIGN, pymt->payment_amt_sign, sizeof( pymt->payment_amt_sign)) == NULL ))
   {
      char ls_payment_amt[LEN_SIGNED_DEC_AMT];

      rv = FALSE;
      CPYFXLFXL(ls_payment_amt, pymt->payment_amt, FT_NUMBER);
      ls_payment_amt[0] = signcvt( pymt->payment_amt_sign);
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( ls_payment_amt, AMT_DECPLACES);
#endif
      WrBadTranError( TPYMT_AMT_STIN, TranSeqNo,
                      payment_seq_no, -1, SART_TPYMT,
                      ls_payment_amt, sizeof( ls_payment_amt));
      CLRTOZEROS( pymt->payment_amt );
   }

   return( rv );
}


/*-------------------------------------------------------------------------
mvSATTend() does the mandatory validations of the record. Return TRUE if
valid or FALSE if any error.
-------------------------------------------------------------------------*/
int mvSATTend( RTL_TTEND *tend, int tender_seq_no  )
{
   int rv = TRUE;
   long tent;

   int li_cc_len;

   /* make sure tender_type_group exists and is valid */
   tent = code_get_seq( TENT, tend->tender_type_group,
                        sizeof( tend->tender_type_group) );
   if (( tent == 0 ) || ( tent == TENTTT_ERR ))
   {
      rv = FALSE;
      WrBadTranError( INVLD_TENT, TranSeqNo,
                      tender_seq_no, -1, SART_TTEND,
                      tend->tender_type_group, sizeof( tend->tender_type_group));
      CLRTOERR( tend->tender_type_group );
   }

   /* make sure tender_type_id exists and is valid */
   if ( tendertype_lookup( tend->tender_type_group,
                           tend->tender_type_id ) == NULL )
   {
      rv = FALSE;
      WrBadTranError( TTEND_TTI_STIN, TranSeqNo,
                      tender_seq_no, -1, SART_TTEND,
                      tend->tender_type_id, sizeof( tend->tender_type_id));
      CLRTOZEROS( tend->tender_type_id );
   }

   /* make sure tender_amt exists and is numeric */
   if (( EMPTY_FIELD( tend->tender_amt) ) ||
       ( SA_VALID_ALL_NUMERIC( tend->tender_amt ) != 0 ) ||
       ( code_lookup( SIGN, tend->tender_amt_sign, sizeof( tend->tender_amt_sign)) == NULL ))
   {
      char ls_tender_amt[LEN_SIGNED_DEC_AMT];

      rv = FALSE;
      CPYFXLFXL(ls_tender_amt, tend->tender_amt, FT_NUMBER);
      ls_tender_amt[0] = signcvt( tend->tender_amt_sign);
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( ls_tender_amt, AMT_DECPLACES);
#endif
      WrBadTranError( TTEND_TAM_STIN, TranSeqNo,
                      tender_seq_no, -1, SART_TTEND,
                      ls_tender_amt, sizeof( ls_tender_amt));
      CLRTOZEROS( tend->tender_amt );
   }

   /* make sure if identi_method exists, it is valid    */
   /* also validate identi_id exists when identi_method */
   /* is present and vice versa */
   if ( ! EMPTY_FIELD( tend->identi_method) )
   {
      if ( code_lookup( IDMH, tend->identi_method,
                        sizeof( tend->identi_method) ) == NULL )
      {
         rv = FALSE;
         WrBadTranError( INVLD_IDMH, TranSeqNo,
                         tender_seq_no, -1, SART_TTEND,
                         tend->identi_method, sizeof( tend->identi_method));
         CLRTOERR( tend->identi_method );
      }
      if ( EMPTY_FIELD( tend->identi_id) )
      {
         rv = FALSE;
         WrBadTranError( IDNT_MTHD_WITHOUT_IDNT_ID, TranSeqNo,
                         tender_seq_no, -1, SART_TTEND,
                         tend->identi_id, sizeof( tend->identi_id));
          CLRTOSPACES( tend->identi_id );
      }
    }
    else
    {
       if ( ! EMPTY_FIELD( tend->identi_id) )
       {
          rv = FALSE;
          WrBadTranError( IDNT_ID_WITHOUT_IDNT_MTHD, TranSeqNo,
                          tender_seq_no, -1, SART_TTEND,
                          tend->identi_id, sizeof( tend->identi_id));
          CLRTOSPACES( tend->identi_id );
       }
    }

    /* make sure if orig_currency exists, it is valid.    */
    /* also validate orig_curr_amt exists and is numeric, */
    /* when orig_currency is present and vice versa       */
    if ( ! EMPTY_FIELD( tend->orig_currency) )
    {
       if ( currency_lookup( tend->orig_currency) == NULL )
       {
          rv = FALSE;
          WrBadTranError( INVLD_ORIG_CURR, TranSeqNo,
                          tender_seq_no, -1, SART_TTEND,
                          tend->orig_currency, sizeof( tend->orig_currency));
          CLRTOSPACES( tend->orig_currency );
       }
       else
       {
          if ( ! EMPTY_FIELD( tend->orig_curr_amt) )
          {
             if ( SA_VALID_ALL_NUMERIC( tend->orig_curr_amt ) != 0 )
             {
                char ls_orig_curr_amt[LEN_SIGNED_DEC_AMT];

                rv = FALSE;
                CPYFXLFXL(ls_orig_curr_amt, tend->orig_curr_amt, FT_NUMBER);
                ls_orig_curr_amt[0] = signcvt( tend->tender_amt_sign);
        #ifdef ADD_DECIMAL_POINTS
              FIX_DP( ls_orig_curr_amt, AMT_DECPLACES);
        #endif
                WrBadTranError( TTEND_OCA_STIN, TranSeqNo,
                                tender_seq_no, -1, SART_TTEND,
                                ls_orig_curr_amt, sizeof( ls_orig_curr_amt));
                CLRTOZEROS( tend->orig_curr_amt );
             }
          }
          else
          {
             rv = FALSE;
             WrBadTranError( ORGCUR_WITHOUT_ORGCUR_AMT, TranSeqNo,
                             tender_seq_no, -1, SART_TTEND,
                            tend->orig_curr_amt, sizeof( tend->orig_curr_amt));
             CLRTOZEROS( tend->orig_curr_amt );
          }
       }
    }
    else
    {
       if ( ! EMPTY_FIELD( tend->orig_curr_amt) )
       {
          char ls_orig_curr_amt[LEN_SIGNED_DEC_AMT];

          rv = FALSE;
          CPYFXLFXL(ls_orig_curr_amt, tend->orig_curr_amt, FT_NUMBER);
          ls_orig_curr_amt[0] = signcvt( tend->tender_amt_sign);
    #ifdef ADD_DECIMAL_POINTS
          FIX_DP( ls_orig_curr_amt, AMT_DECPLACES);
    #endif
          WrBadTranError( ORGCUR_AMT_WITHOUT_ORGCUR, TranSeqNo,
                          tender_seq_no, -1, SART_TTEND,
                          ls_orig_curr_amt, sizeof( ls_orig_curr_amt));
          CLRTOZEROS( tend->orig_curr_amt );
       }
    }

   /* validate COUPON */
   if (((tent == TENTTT_COUPON) && ( EMPTY_FIELD( tend->coupon_no) ))
      && (CurTrat != TRATTT_PULL) && (CurTrat != TRATTT_LOAN)
      && (CurTrat != TRATTT_NOSALE) && (CurTrat != TRATTT_PAIDOU))
   {
      rv = FALSE;
      WrBadTranError( TTEND_COUPON_NO_REQ, TranSeqNo,
                      tender_seq_no, -1, SART_TTEND,
                      tend->coupon_no, sizeof( tend->coupon_no));
   }

   /* validate CHECK, check_no field should be valid if exists */
   else if ( tent == TENTTT_CHECK )
      if ((CurTrat != TRATTT_PULL) && (CurTrat != TRATTT_LOAN)
      && (CurTrat != TRATTT_NOSALE) && (CurTrat != TRATTT_PAIDOU))
      {
         if ( !EMPTY_FIELD(tend->check_no))
         {
            if ( SA_VALID_ALL_NUMERIC( tend->check_no ) != 0 )
            {
               rv = FALSE;
               WrBadTranError( INVLD_CHECK_NO, TranSeqNo,
                               tender_seq_no, -1, SART_TTEND,
                               tend->check_no, sizeof( tend->check_no));
               CLRTOZEROS( tend->check_no );
            }
         }
      }

   return( rv );
}


/*-------------------------------------------------------------------------
Convert the sign of a number.
-------------------------------------------------------------------------*/
char signcvt( const char *sign)
{
   if (strncmp( sign, SIGN_P, LEN_SALES_SIGN) == 0)
      return( '+');
   else if (strncmp( sign, SIGN_N, LEN_SALES_SIGN) == 0)
      return( '-');
   else
      return( sign[0]);
}


#ifdef ADD_DECIMAL_POINTS
/*-------------------------------------------------------------------------
fix_dp() fixes a weird problem that SQL-load has - it *needs* a decimal
point in an amount field, i.e. it cannot deal with a number(20,4), you have
to have the decimal point physically present. To get around this, we
sacrifice the most significant character (usually 0), move the non-fractional
digits up one, and insert the decimal place at the right location.
-------------------------------------------------------------------------*/
void fix_dp( char *field, int len, int decplaces )
{
   int n;
   char *temp;
   temp = (char *)malloc( len+1 );
   memset( temp,'\0',len+1 );

   /* Do not trash negative numbers */
   if ((*field == '-')||(*field == '+'))
   {
      field++;
      len--;
   }

   /* figure out how many to move (non-fractional portion - 1) */
   n = ( len - decplaces ) - 1;

   /* drop first digit by moving non-fractional side up one position */
   strncpy( temp, field + 1, n );
   strncpy( field, temp, n);

   /* set now available position to decimal point */
   /* leaving following positions as fractional part */
   field[ n ] = '.';
   free(temp);
}
#endif

/*-------------------------------------------------------------------------
Convert a number (stored in a string) with a SIGN indicator to a string
that has a leading - if appropriate and with 0 padding on the left.
A + is never added - it is always assumed.
-------------------------------------------------------------------------*/
static void num2str( char *str, int lstr, char *num, int lnum, const char *sign)
{
   fldcpy( str, lstr - 1, num, lnum, FT_NUMBER);
   str[ lstr - 1 ] = '\0';
   if (strncmp( sign, SIGN_N, LEN_SALES_SIGN) == 0)
      str[0] = '-';
}

