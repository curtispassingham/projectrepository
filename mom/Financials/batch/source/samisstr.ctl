LOAD DATA
INFILE	'samisstr.dat'
BADFILE	'samisstr.bad'

APPEND
INTO TABLE	sa_missing_tran

(MISS_TRAN_SEQ_NO	POSITION (1:20)		INTEGER EXTERNAL,
 STORE_DAY_SEQ_NO	POSITION (21:40)	INTEGER EXTERNAL,
 REGISTER		POSITION (41:45)	CHAR,
 TRAN_NO		POSITION (46:55)	INTEGER EXTERNAL,
 STATUS  		POSITION (56:61)	CHAR,
 RTLOG_ORIG_SYS         POSITION (62:64)        CHAR)

