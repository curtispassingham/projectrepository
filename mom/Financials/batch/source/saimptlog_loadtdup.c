#include <ctype.h>
#include "retek.h"
#include "salib.h"
#include "saimptlog.h"
#include "saimptlog_tdup.h"

/*-------------------------------------------------------------------------
tdup_loaddata() loads in the data file of past ranges. Returns TRUE all the
time because its not an error if no file exists.
-------------------------------------------------------------------------*/
int tdup_loaddata( char *loc, char *dup_cutoff_date,
                   long int min_tranno, long int max_tranno,
                   char *rtlog_orig_sys)
{
   FILE *f;
   TDUP_UID *lp;
   long int fval, lval, fdate, ldate;
   char freptd, lreptd;
   char uid[ TDUP_UID_SIZE + 1 ];
   char *cp, s[ 1025 ];
   int hv, i;
   char *msggap;

   /* Set range of tranno's */
   ll_range_tranno_min = min_tranno;
   ll_range_tranno_max = max_tranno;

   /* Strip off the leading spaces from 'loc' since it makes the tdup file name
      with space when we get location value as less than 4 digits and in turn
      it makes the tdup_loaddata logic to fail  */

   for (i = LEN_LOC - 1; (i > 0) && (loc[i] == ' '); i--)
       loc[i] = '\0';

   /* set cutoff date */
   date_too_old = atoln( dup_cutoff_date, LEN_DATEONLY );

   /* read in file if there */
   makefilename( loc,rtlog_orig_sys );
   if ( ( f = fopen( datafilename, SA_READMODE ) ) != NULL )
   {
      TDUP_VERBOSE("Loading...\n");
      while ( fgets( s, sizeof( s), f ) )
      {
         /* break line up */
         cp = strtok_r( s, ",\n", &msggap );
         strncpy(uid, cp, strlen(cp));
         uid[ strlen(cp) ] = '\0';
         cp = strtok_r( NULL, ",\n", &msggap );
         fval = atol( cp );
         cp = strtok_r( NULL, ",\n", &msggap );
         fdate = atol( cp );
         cp = strtok_r( NULL, ",\n", &msggap );
         freptd = cp[0];
         cp = strtok_r( NULL, ",\n", &msggap );
         lval = atol( cp );
         cp = strtok_r( NULL, ",\n", &msggap );
         ldate = atol( cp );
         cp = strtok_r( NULL, ",\n", &msggap );
         lreptd = cp[0];

         if ( ldate > date_too_old )
         {
            hv = hashval( uid );

            /* save range into data structure */
            if ( ( lp = finduid( locht[ hv ], uid ) ) != NULL )
               lp->exist = addrange( lp->exist, fval, fdate );
            else locht[ hv ] = adduid( locht[ hv ], uid, fval, fdate );

            /* update range with full range data */
            updrange( fval, fdate, freptd, lval, ldate, lreptd );
         }
      }

      (void)fclose( f );
   }

   /* always true, not an error if no file there */
   return( TRUE );
}
/*-------------------------------------------------------------------------
finduid() finds the uid and returns a pointer, or NULL if not found.
-------------------------------------------------------------------------*/
TDUP_UID *finduid( TDUP_UID *lp, char *uid )
{
   for ( ; lp; lp = lp->next )
      if ( strcmp( uid, lp->uid ) == 0 )
         return( lp );

   return( NULL );
}
/*-------------------------------------------------------------------------
adduid() adds a new location onto the existing linked list.
-------------------------------------------------------------------------*/
TDUP_UID *adduid( TDUP_UID *lp, char *uid, long int tid, long int tdate )
{
   TDUP_UID *tmp;

   if ( lp != NULL )
   {
      if ( ( tmp = newuid( uid, tid, tdate ) ) != NULL )
      {
         tmp->next = lp;
         lp = tmp;
      }
   }
   else lp = newuid( uid, tid, tdate );
   return( lp );
}
/*-------------------------------------------------------------------------
newuid() mallocs a new node and returns a pointer, or NULL if failure.
-------------------------------------------------------------------------*/
TDUP_UID *newuid( char *uid, long int tid, long int tdate )
{
   TDUP_UID *lp;

   if ( ( lp = (TDUP_UID *)malloc( sizeof( TDUP_UID ) ) ) != NULL )
   {
      strncpy(lp->uid, uid, strlen(uid));
      lp->uid[strlen(uid)] = '\0';
      lp->exist = newrange( tid, tdate );
      lp->next = NULL;
   }
   return( lp );
}
/*-------------------------------------------------------------------------
delalluids() frees the space allocated by newuid.
-------------------------------------------------------------------------*/
void delalluids( TDUP_UID *lp )
{
   if (lp != NULL)
   {
      delalluids(lp->next);
      free(lp);
   }
}
/*-------------------------------------------------------------------------
addrange() adds a new range into its correct place on the linked list and
returns a pointer, or NULL if failure.
-------------------------------------------------------------------------*/
TDUP_RANGE *addrange( TDUP_RANGE *r, long int tid, long int tdate )
{
   TDUP_RANGE *tmp;

   if ( r != NULL )
   {
      if ( tid < r->first )
      {
         if ( ( tmp = newrange( tid, tdate ) ) != NULL )
         {
            tmp->next = r;
            r = tmp;
         }
      }
      else r->next = addrange( r->next, tid, tdate );
   }
   else r = newrange( tid, tdate );
   return( r );
}
/*-------------------------------------------------------------------------
newrange() mallocs a new node and returns a pointer or NULL if failure.
-------------------------------------------------------------------------*/
TDUP_RANGE *newrange( long int tid, long int tdate )
{
   TDUP_RANGE *r;

   if ( ( r = (TDUP_RANGE *)malloc( sizeof( TDUP_RANGE ) ) ) != NULL )
   {
      r->first = r->last = tid;
      r->firstdate = r->lastdate = tdate;
      r->freported = r->lreported = NOT_REPORTED;
      r->next = NULL;
      last_created_range = r; /* remember for later */
   }
   return( r );
}
/*-------------------------------------------------------------------------
delallranges() frees all the space allocated for ranges.
-------------------------------------------------------------------------*/
void delallranges( TDUP_RANGE *r )
{
   if (r != NULL)
   {
      delallranges(r->next);
      free(r);
   }
}
/*-------------------------------------------------------------------------
updrange() updates the last created range with the data passed in.
-------------------------------------------------------------------------*/
void updrange( long int first, long int firstdate, char freptd,
                      long int last,  long int lastdate,  char lreptd)
{
   if ( last_created_range )
   {
      last_created_range->first = first;
      last_created_range->firstdate = firstdate;
      last_created_range->freported = freptd;
      last_created_range->last = last;
      last_created_range->lastdate = lastdate;
      last_created_range->lreported = lreptd;
   }
}
/*-------------------------------------------------------------------------
makefilename() makes the filename for this location.
-------------------------------------------------------------------------*/
void makefilename( char *loc, char *rtlog_orig_sys )
{
   char temp_loc[LEN_LOC + 1];
   int  i;

   strcpy( datafilename, "tdup" );
   strncpy(temp_loc, loc, LEN_LOC);
   temp_loc[LEN_LOC] = '\0';

   for (i = LEN_LOC - 1; (i > 0) && (temp_loc[i] == ' '); i--)
       temp_loc[i] = '\0';

   strncpy( &datafilename[ 4 ], loc, strlen(temp_loc) );
   datafilename[ 4 + strlen(temp_loc) ] = '\0';
   strcat( datafilename, rtlog_orig_sys);
   strcat( datafilename, ".dat" );
}
/*-------------------------------------------------------------------------
hashval() returns the hash value for the string.
-------------------------------------------------------------------------*/
int hashval( char *instr )
{
   int hv;

   hv = strsig( instr );
   if ( hv < 0 )
      hv = -hv;
   /* hv %= TDUP_HASHTABLE_SIZE; */   /* could do this... */
   hv &= TDUP_HASHTABLE_MASK;       /* this is faster... */
   return( hv );
}
