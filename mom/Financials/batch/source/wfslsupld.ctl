LOAD DATA
APPEND
INTO TABLE WFSLSUPLD_STAGING
WHEN FILE_RECORD_DESCRIPTOR = 'FDETL'
   (
     FILE_RECORD_DESCRIPTOR     position(1:5),
     CHUNK_ID                   constant "",
     ITEM                       position(16:40) char,
     NET_SALES_QTY              position(41:52) ":NET_SALES_QTY/10000",
     NET_SALES_QTY_UOM          position(53:56) char,
     TOTAL_RETAIL_AMT           position(57:76) ":TOTAL_RETAIL_AMT/10000",
     TOTAL_RETAIL_AMT_CURR      position(77:79) char,
     ERROR_MSG                  constant ""
   )
