#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  wfbillex.ksh
#
#  Desc:  This shell script will be used to retrieve WF billing information
#         and write the information to separate output files per location.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='wfbillex.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

#file locations
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"

#initialize return values
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked.
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName.$pgmExt <connect string>

   <connect string>   Username/password@db.  Use "'UP'" if using Oracle Wallet.
"
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_result         NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_result := 0;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GET_LOCS
# Purpose      : Retrieve the locations to determine number of threads..
#-------------------------------------------------------------------------
function GET_LOCS
{
set -A restart_arr `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off
with tab as
(
select customer_loc
  from wf_billing_returns
union all
select customer_loc
  from wf_billing_sales
)
select distinct customer_loc
  from tab
 order by customer_loc;
exit;
EOF`
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_BILLEX
# Purpose      : Process and spool the WF sales and returns.
#-------------------------------------------------------------------------
function PROCESS_BILLEX
{

sysDate=`date +"%Y%m%d%H%M%S"`   # get the sysdate

set -A file_data `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
set pause off
set linesize 32766
set pagesize 0
set feedback off
set verify off
set heading off
set echo off
set trimspool on
set trims on

spool $HOME/WFBX_$1_$sysDate.tmp

with aux2 as
   (select rpad(w.customer_loc,10) customer_loc, 
           rpad(nvl(w.cust_ord_ref_no,' '),20) cust_ord_ref_no,
           rpad(w.wf_order_no,10) wf_order_no,
           'SALES ' tran_type,
           rpad(' ',10) rma_no,
           to_char(w.order_date,'YYYYMMDD') order_return_date,
           rpad(to_char(w.shipment_date,'YYYYMMDD'),8) shipment_date,
           rpad(w.item,25) item,
           lpad(w.dept,4,'0') dept,
           lpad(w.class,4,'0') class,
           lpad(w.subclass,4,'0') subclass,
           lpad(nvl(w.order_qty * 10000,'0'),12,'0') order_return_qty,
           rpad(im.standard_uom,4) order_return_cost_uom,
           lpad(nvl(w.customer_cost * 10000,'0'),20,'0') order_return_cost,
           lpad(nvl(w.freight_cost * 10000,'0'),20,'0') freight_cost,
           rpad('0',20,'0') return_restocking_fee,
           rpad(nvl(w.vat_code,'0'),6) vat_code,
           lpad(nvl(w.vat_rate * 10000,'0'),20,'0') vat_rate,
           lpad(nvl(w.other_charges * 10000,'0'),20,'0') order_other_charges
      from wf_billing_sales w,
           item_master im
     where w.customer_loc    = $1
       and w.extracted_ind   = 'N'
       and w.extracted_date is NULL
       and w.item = im.item
   union all
    select rpad(w.customer_loc,10) customer_loc,
           rpad(nvl(w.cust_ord_ref_no,' '),20) cust_ord_ref_no,
           rpad(w.wf_order_no,10) wf_order_no,
           'RETURN' tran_type,
           rpad(w.rma_no,10) rma_no,
           to_char(w.return_date,'YYYYMMDD') order_return_date,
           rpad(' ',8) shipment_date,
           rpad(w.item,25) item,
           lpad(w.dept,4,'0') dept,
           lpad(w.class,4,'0') class,
           lpad(w.subclass,4,'0') subclass,
           lpad(w.returned_qty * 10000,12,'0') order_return_qty,
           rpad(im.standard_uom,4) order_return_cost_uom,
           lpad(nvl(w.return_unit_cost * 10000,'0'),20,'0') order_return_cost,
           lpad('0',20,'0') freight_cost,
           lpad(nvl(w.restocking_fee * 10000,'0'),20,'0') return_restocking_fee,
           rpad(nvl(w.vat_code,' '),6) vat_code,
           lpad(nvl(w.vat_rate * 10000,'0'),20,'0') vat_rate,
           lpad('0',20,'0') order_other_charges
      from wf_billing_returns w,
           item_master im
     where w.customer_loc    = $1
       and w.extracted_ind   = 'N'
       and w.extracted_date is NULL
       and w.item = im.item),
     aux1 as
     (select customer_loc || wf_order_no || order_return_date || cust_ord_ref_no || shipment_date || rma_no || tran_type prim_key,
             customer_loc || cust_ord_ref_no || wf_order_no ||
             tran_type || rma_no  || order_return_date || shipment_date || ',' ||
             item || dept || class || subclass || order_return_qty ||
             order_return_cost_uom || order_return_cost || freight_cost ||
             return_restocking_fee || vat_code || vat_rate || order_other_charges data
        from aux2
      ),
    final_data as
    (select '0' prim_key,
            'WFBX' || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') des,
            'FHEAD'  file_type,
            0 level_no 
       from dual
      union all
     select distinct prim_key, 
            regexp_substr(data,'[^,]+',1,level) des,
            decode(level, 1, 'THEAD','TDETL') file_type,
            level level_no
       from aux1
    connect by level <= 2
      union all
     select distinct prim_key prim_key,
            lpad(count(*) over (partition by prim_key order by prim_key),6,'0') des,
            'TTAIL' file_type,
            3 level_no 
       from aux1
      union all
     select null prim_key,
            null des,
            'FTAIL' file_type,
            4 level_no 
       from dual
      order by prim_key,level_no)
select case
          when file_type = 'FTAIL' then file_type || lpad(rownum,10,'0') || lpad(rownum-2,10,'0') || chr(13)
          else file_type || lpad(rownum,10,'0') || des || chr(13)
       end
  from final_data;

spool off;
host mv $HOME/WFBX_$1_$sysDate.tmp $HOME/WFBX_$1_$sysDate
exit;
EOF`

# Update the WF BILLING tables

   sqlTxt="
      DECLARE

         cursor C_LOCK_WF_BILLING_SALES is
            select 1
              from wf_billing_sales
             where customer_loc   = $1
               and extracted_ind  = 'N'
               and extracted_date is NULL
               for update nowait;

         cursor C_LOCK_WF_BILLING_RETURNS is
            select 1
              from wf_billing_returns
             where customer_loc   = $1
               and extracted_ind  = 'N'
               and extracted_date is NULL
               for update nowait;

      BEGIN

         open C_LOCK_WF_BILLING_SALES;
         close C_LOCK_WF_BILLING_SALES;

         update wf_billing_sales
            set extracted_ind = 'Y',
                extracted_date = sysdate
          where customer_loc   = $1
            and extracted_ind  = 'N'
            and extracted_date is NULL;

         open C_LOCK_WF_BILLING_RETURNS;
         close C_LOCK_WF_BILLING_RETURNS;

         update wf_billing_returns
            set extracted_ind = 'Y',
                extracted_date = sysdate
          where customer_loc   = $1
            and extracted_ind  = 'N'
            and extracted_date is NULL;

         COMMIT;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Update WF Billing Tables Failed." "" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   else
      LOG_MESSAGE "Location $1 - Successfully Completed" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi
}

#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit 1
fi

CONNECT=$1
if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "wfbillex.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#------------------------------------------------------------
# Initialize number of parallel threads.
# Retrieve from the rms_plsql_batch_config table
#------------------------------------------------------------
parallelThreads=""
parallelThreads=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select max_concurrent_threads
  from rms_plsql_batch_config
 where program_name = 'CORESVC_BILLEX_SQL';
exit;
EOF`
# Check the number of threads
if [ -z $parallelThreads ]; then
   LOG_ERROR "Unable to retrieve the number of threads from the rms_plsql_batch_config table." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
else
   LOG_MESSAGE "The number of concurrent threads for processing is ${parallelThreads}." "" ${OK} ${LOGFILE}  ${pgmName} ${pgmPID}
fi

#------------------------------------------------------------
# Get NUM_THREADS
#------------------------------------------------------------
GET_LOCS

#------------------------------------------------------------------------------
# Start to invoke process billex based on Thread ID
#------------------------------------------------------------------------------
NUM_THREADS=${#restart_arr[*]}

if [ NUM_THREADS -lt 1 ]; then
   LOG_MESSAGE "No threads to be processed for this run." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
fi

arr_index=0

while [ $arr_index -lt  NUM_THREADS ]
do
   if [ `jobs | wc -l` -lt ${parallelThreads} ]
      then
         (
            PROCESS_BILLEX  ${restart_arr[arr_index]} 
         ) &
   else
      # Loop until a thread becomes available
      while [ `jobs | wc -l` -ge ${parallelThreads} ]
      do
         sleep 1
      done
      (
         PROCESS_BILLEX ${restart_arr[arr_index]}
      ) &
   fi
   let arr_index=$arr_index+1;
   done

# Wait for all threads to complete
wait


LOG_MESSAGE "Program wfbillex.ksh Terminated." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

#------------------------------------------------------------------------------

# Check for any Oracle errors from the SQLPLUS process
if [ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. See error file." "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
fi
#-----------End of Processing -------------------------------------------------

exit 0
