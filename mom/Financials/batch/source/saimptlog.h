
#ifndef SAIMPTLOG_H
#define SAIMPTLOG_H

#define SEQUENCE_FIX

#ifdef DEBUG
#define VERBOSE(a1) printf(a1)
#define VERBOSE2(a1,a2) printf(a1,a2)
#define VERBOSE3(a1,a2,a3) printf(a1,a2,a3)
#define VERBOSE4(a1,a2,a3,a4) printf(a1,a2,a3,a4)
#else
#define VERBOSE(a1)
#define VERBOSE2(a1,a2)
#define VERBOSE3(a1,a2,a3)
#define VERBOSE4(a1,a2,a3,a4)
#endif

#ifdef TDUP_DEBUG
#define TDUP_VERBOSE(a1) printf(a1)
#define TDUP_VERBOSE2(a1,a2) printf(a1,a2)
#define TDUP_VERBOSE3(a1,a2,a3) printf(a1,a2,a3)
#define TDUP_VERBOSE4(a1,a2,a3,a4) printf(a1,a2,a3,a4)
#else
#define TDUP_VERBOSE(a1)
#define TDUP_VERBOSE2(a1,a2)
#define TDUP_VERBOSE3(a1,a2,a3)
#define TDUP_VERBOSE4(a1,a2,a3,a4)
#endif

#ifdef RRC_DEBUG
#define RRC_VERBOSE(a1) printf(a1)
#define RRC_VERBOSE2(a1,a2) printf(a1,a2)
#define RRC_VERBOSE3(a1,a2,a3) printf(a1,a2,a3)
#define RRC_VERBOSE4(a1,a2,a3,a4) printf(a1,a2,a3,a4)
#define RRC_VERBOSE5(a1,a2,a3,a4,a5) printf(a1,a2,a3,a4,a5)
#else
#define RRC_VERBOSE(a1)
#define RRC_VERBOSE2(a1,a2)
#define RRC_VERBOSE3(a1,a2,a3)
#define RRC_VERBOSE4(a1,a2,a3,a4)
#define RRC_VERBOSE5(a1,a2,a3,a4,a5)
#endif

/* behavior defines */
#define ADD_DECIMAL_POINTS
#define TRAN_BALANCE_FUDGE_FACTOR  0.0005
#define EOF_REACHED               -2


#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "retek_2.h"
#include "salib.h"
#include "saimptlog_tdup.h"
#include "sagetref.h"
#include "saerrcodes.h"

#define ARG_USRPW           1
#define ARG_INFILE          2
#define ARG_BADFILE         3
#define ARG_ITEMFILE        4
#define ARG_WASTEFILE       5
#define ARG_REFITEMFILE     6
#define ARG_PRIMVARFILE     7
#define ARG_VUPCFILE        8
#define ARG_STOREDAYFILE    9
#define ARG_PROMFILE       10
#define ARG_CODEFILE       11
#define ARG_ERRORFILE      12
#define ARG_STOREPOSFILE   13
#define ARG_TENDERTYPEFILE 14
#define ARG_MERCHCODEFILE  15
#define ARG_PARTNERFILE    16
#define ARG_SUPPLIERFILE   17
#define ARG_EMPLOYEEFILE   18
#define ARG_BANNERFILE     19
#define ARG_CURRENCYFILE   20
#define ARG_WHFILE         21
#define ARG_INVSTATUSFILE  22
#define ARG_MAXTRANGAP     23
#define NUMARGS            23

/* filenames */
#define MAX_FILE_NAME_LEN    10

#define SUFFIX ".out"

#ifndef LINUX
char *strdup( const char * ); /* seems to be missing from C library proto's?? */
#endif


/*-------------------------------------------------------------------------
global variables
-------------------------------------------------------------------------*/
#ifdef MAIN_MODULE
  #define GLOBAL
#else
  #define GLOBAL extern
#endif

GLOBAL int ExitCode;       /* main program exit code */

GLOBAL long int SQLCODE;     /* oracle status code */


GLOBAL pt_sa_system_options SysOpt;    /* sa system options structure */
GLOBAL unsigned int pi_commit_max_ctr;
GLOBAL char ps_thread_val[NULL_THREAD];

GLOBAL char *Rectype;     /* type of record in Recbuf */
GLOBAL void **Recbuf;     /* main record buffers */

GLOBAL char Location[ NULL_LOC ];              /* current location */
GLOBAL char Currency_rtl_dec[ NULL_CURRENCY_RTL_DEC ]; /* Currency Retail Decimal */
GLOBAL char StoreDaySeqNo[ LEN_BIG_SEQ_NO ];   /* current location */
GLOBAL char BusinessDate[ NULL_DATEONLY ];     /* business date */
GLOBAL char Day[ LEN_DAY ];                    /* hash day */
GLOBAL char SysDate[ NULL_DATETIME ];          /* SYSDATE date */
GLOBAL char *TranSeqNo;                        /* current tran seq no */
GLOBAL char TranNoGen[ STRG_SIZE ];
GLOBAL char Rtlog_Orig_Sys[ NULL_RTLOG_ORIG_SYS ];

GLOBAL char logmsg[ 1100 ];     /* message buffer for logging */

GLOBAL int gi_rej_record;
GLOBAL int l_max_tran_gap;
GLOBAL int gi_rsys; /* indicator for Rtlog Originating System */
GLOBAL int gi_tsys; /* indicator for Transaction processing system */
#include "saimptlog_proto.h"

#endif
