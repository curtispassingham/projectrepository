/* saimptlog_datastat.pc */
int updateDataStatus( char *store_day_seq_no );

/* saimptlog_final.c */
int Final( void );

/* saimptlog_init.c */
int Init( int argc, char *argv[] );

/* saimptlog_manval.c */
int mvSATHead( RTL_THEAD *head );
int mvSATCust( RTL_TCUST *cust );
int mvSACAtt( RTL_CATT *catt, int attrib_seq_no );
int mvSATItem( RTL_TITEM *item, int item_seq_no, pt_item_uom *uom );
int mvSATDisc( RTL_IDISC *disc, int item_seq_no, int disc_seq_no, pt_disc_uom *disc_uom );
int mvSAIGtax( RTL_IGTAX *igtax, int item_seq_no, int igtax_seq_no, int igtax_seq, pt_igtax_tia *igtax_tia, char loc[ NULL_LOC ] ); /* IGTAX Changes */
int mvSAIGTaxVal( int item_seq, int igta_seq ); /*Duplicate IGTAX Validation*/
int mvSATTax( RTL_TTAX *tax, int tax_seq_no, char loc[ NULL_LOC ]);
int mvSATPymt( RTL_TPYMT *pymt, int payment_seq_no ); /* Due File Changes */
int mvSATTend( RTL_TTEND *tend, int tender_seq_no );
#ifdef ADD_DECIMAL_POINTS
   void fix_dp( char *field, int len, int decplaces );
# define FIX_DP( field, decplaces) fix_dp( field, sizeof( field), decplaces)
# define NFIX_DP( field, decplaces) fix_dp( field, sizeof( field) - 1, decplaces)
#endif
char signcvt( const char *sign);

/* saimptlog_nextbgsn.pc */
char *nextBalGroupSeqNo( char *is_table_owner );

/* saimptlog_nextesn.pc */
char *nextErrorSeqNo( char *is_table_owner );

/* saimptlog_nextmtsn.pc */
char *nextMissingTranSeqNo( char *is_table_owner );

/* saimptlog_nexttsn.pc */
char *nextTranSeqNo( char *is_table_owner );

/* saimptlog_nextvsn.pc */
char *nextVoucherSeqNo( char *is_table_owner );

/* saimptlog_sqlldr.c & saimptlog_insert.pc */
int InitOutputData( int argc, char *argv[] );
void InitOutputClean( void );
int WrOutputData( void );
int FinalOutputData( void );
int CreateTermRecords( void );
void resetFmt( void );
void saveFmt( void );
void abortFmt( void );
int fmtSATHead( RTL_THEAD *p, char *sale_total );
void setErrorSATHead( void );
int fmtSACustomer( RTL_TCUST *p );
int fmtSACAtt( RTL_CATT *p, int attribseq );
int fmtSATItem( RTL_TITEM *p, int itemseq, pt_item_uom *uom );
void setErrorSATItem( void );
int fmtSATDisc( RTL_IDISC *p, int itemseq, int discseq, pt_disc_uom *disc_uom );
void setErrorSATDisc( void );
int fmtSAIGtax( RTL_IGTAX *p, int itemseq, int igtaxseq, pt_igtax_tia *igtax_tia ); /* IGTAX Changes */
void setErrorSAIGtax( void );
int fmtSATTax( RTL_TTAX *p, int taxseq );
void setErrorSATTax( void );
int fmtSATPymt( RTL_TPYMT *p, int pymtseq ); /* Due File Changes */
void setErrorSATPymt( void );
int fmtSATTend( RTL_TTEND *p, int tendseq );
void setErrorSATTend( void );
int fmtSAMissTran( char *is_reg, long il_tran_no );
int WrBadTranError(
   const char *err,
   char       *tran_seq_no,
   int         seq_no1,
   int         seq_no2,
   const char *rec_type,
   char       *orig_value,
   int         orig_value_len);
int clearBigGapErrors( void );

/* saimptlog_output.c */
int openSAVoucher( void);
int closeSAVoucher( void);
int writeSAVoucherFHEAD( char *is_business_date);
int writeSAVoucherFTAIL( void);
int writeSAVoucherData( void );

/* saimptlog_uom.pc */

int uom_convert(char    is_item[NULL_ITEM],
                char    is_from_uom[NULL_UOM],
                char    is_to_uom[NULL_UOM],
                double *iod_output_value);
int decodeVPLU(RTL_TITEM *p );

/* saimptlog_rtlog.c */
int InitInputData( char *infile, char *badfile );
int getNextTran( void );
int FinalInputData( void );
void WrBadTranErrorFile(
   const char *err,
   char       *func,
   const char *rec_type,
   char       *line_no);
int cmpfxlstr( const char *s1, unsigned int l1, const char *s2, unsigned int l2);
#define CMPFXLSTR( s1, s2) cmpfxlstr( s1, sizeof( s1), s2, strlen( s2))
