lOAD DATA
INFILE    'saigtax.dat'
BADFILE   'saigtax.bad'

APPEND
INTO TABLE   sa_tran_igtax

(tran_seq_no             POSITION (1:20)           INTEGER EXTERNAL,
 item_seq_no             POSITION (21:24)          INTEGER EXTERNAL,
 igtax_seq_no            POSITION (25:28)          INTEGER EXTERNAL,
 tax_authority           POSITION (29:38)          CHAR,
 igtax_code              POSITION (39:44)          CHAR,
 igtax_rate              POSITION (45:65)          DECIMAL EXTERNAL,
 total_igtax_amt         POSITION (66:87)          DECIMAL EXTERNAL,
 standard_qty            POSITION (88:101)         DECIMAL EXTERNAL,
 standard_unit_igtax_amt POSITION (102:122)        DECIMAL EXTERNAL,
 error_ind               POSITION (123:123)        CHAR,
 ref_no21                POSITION (124:153)        CHAR,
 ref_no22                POSITION (154:183)        CHAR,
 ref_no23                POSITION (184:213)        CHAR,
 ref_no24                POSITION (214:243)        CHAR,
 store                   POSITION (244:253)        INTEGER EXTERNAL,
 day                     POSITION (254:256)        INTEGER EXTERNAL)