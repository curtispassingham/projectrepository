LOAD DATA
INFILE	'sacust.dat'
BADFILE	'sacust.bad'

APPEND
INTO TABLE	sa_customer

(tran_seq_no      POSITION (1:20)         INTEGER EXTERNAL,
 cust_id          POSITION (21:36)        CHAR,
 cust_id_type     POSITION (37:42)        CHAR,
 name             POSITION (43:162)       CHAR,
 addr1            POSITION (163:402)      CHAR,
 addr2            POSITION (403:642)      CHAR,
 city             POSITION (643:762)      CHAR,
 state            POSITION (763:765)      CHAR,
 postal_code      POSITION (766:795)      CHAR,
 country          POSITION (796:798)      CHAR,
 home_phone       POSITION (799:818)      CHAR,
 work_phone       POSITION (819:838)      CHAR,
 e_mail           POSITION (839:938)      CHAR,
 birthdate        POSITION (939:946)      DATE "YYYYMMDD" DEFAULTIF (birthdate = BLANKS),
 store            POSITION (947:956)      INTEGER EXTERNAL,
 day              POSITION (957:959)      INTEGER EXTERNAL)
