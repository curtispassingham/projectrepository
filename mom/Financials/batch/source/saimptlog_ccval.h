/*=========================================================================
SAIMPTLOG
ccval.h: This function validates credit card numbers in an arithmetic fashion.
=========================================================================*/

#ifndef _SAIMPTLOG_CCVAL_H
#define _SAIMPTLOG_CCVAL_H

/* ccval() return tokens */
/* Return values are ORed together so that multiple errors can be returned. */
#define CCVAL_OK                0
#define CCVAL_NOTOK_MASKDIGIT   1
#define CCVAL_NOTOK_EXPDATE     2

int ccval(
   char *is_cc_no,
   char *is_cc_msk_char,
   char *is_cc_exp_date,
   char *is_cc_tran_date);

#endif
