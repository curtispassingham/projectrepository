LOAD DATA
INFILE	'sacustatt.dat'
BADFILE	'sacustatt.bad'

APPEND
INTO TABLE	sa_cust_attrib

(TRAN_SEQ_NO            POSITION (1:20)         INTEGER EXTERNAL,
 ATTRIB_SEQ_NO          POSITION (21:24)        CHAR,
 ATTRIB_TYPE            POSITION (25:30)        CHAR,
 ATTRIB_VALUE           POSITION (31:36)        CHAR,
 STORE                  POSITION (37:46)        INTEGER EXTERNAL,
 DAY                    POSITION (47:49)        INTEGER EXTERNAL)
