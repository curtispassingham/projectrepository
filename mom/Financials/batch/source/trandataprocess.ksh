#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  trandataprocess.ksh
#  Desc:  This script calls the TRAN_DATA_IMPORT_SQL for all the chunks on STAGE_EXT_TRAN_DATA_CHUNK 
#         to import the Transaction records on STAGE_EXT_TRAN_DATA table that haven�t been processed yet.
#         The STAGE_EXT_TRAN_DATA.STATUS flag aids in the implicit restart.

#         This script should accept the following parameters:
#         1.Connect string
#         2.Number of parallel threads optional parameter. This is to override the value set on RESTART_CONTROL table.

#         Validations and Processing:
#         Get the distinct chunks present in STAGE_EXT_TRAN_DATA which are in New status,
#         for each chunk check if there is a thread available,
#         If available
#         Then call TRAN_DATA_IMPORT_SQL.IMPORT_CHUNK function.
#         If thread unavailable then wait and check again for thread availability.
#         Once all the chunks are processed, generate a single error file with all the records that failed the validations.(STAGE_EXT_TRAN_DATA.STATUS = 'E')
#         The format of the error file should have fields delimited by '|'.
#         For each thread processed, a record is written to RESTART_PROGRAM_STATUS table.
#         TRUNCATE the table STG_TRAN_DATA_EXT_CHUNK at the end of processing.

#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib


pgmName='trandataprocess.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
ERRINDFILE=err.ind

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <number of threads>

   <connect string>   Username/password@db.

   <number of threads>       optional parameter. This is to override the value set on RESTART_CONTROL table.
"
}

#-------------------------------------------------------------------------
# Function Name: GET_MAX_THREADS
# Purpose      : Retrieves the maximum number of concurrent threads
#                that can be run for this batch.
#-------------------------------------------------------------------------
function GET_THREADS
{
parallelThreads=0
   parallelThreads=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select trunc(num_threads)
     from restart_control
    where lower(program_name) = 'trandataprocess';
   exit;
   EOF`

parallelThreads=`echo $parallelThreads | tr -d '\0'`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${parallelThreads}" "GET_THREADS" ${FATAL} ${ERRORFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned number of parallel threads
   if [ -z ${parallelThreads} ]; then
      LOG_ERROR "Unable to retrieve the number of threads from the RESTART_CONTROL table." "GET_THREADS" ${FATAL} ${ERRORFILE} ${pgmName}
      exit ${FATAL}
   else
      #check if the restartArr contains only numeric values
      if [[ ${parallelThreads} != +([0-9]) ]]; then
         LOG_ERROR "Database Access failed." "GET_THREADS" ${FATAL} ${ERRORFILE} ${pgmName}
         USAGE
         exit ${FATAL}
      else
         LOG_MESSAGE "GET_THREADS - The number of concurrent threads for processing is ${parallelThreads}." "GET_THREADS" ${OK} ${LOGFILE} ${pgmName}
      fi
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GET_CHUNKS
# Purpose      : Retrieves the distinct chunks present in STAGE_EXT_TRAN_DATA which are in 'N'ew status.
#-------------------------------------------------------------------------
function GET_CHUNKS
{

   # Retrieve chunks to process into an indexed array
   set -A restartArr `$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select distinct chunk_id
     from stage_ext_tran_data_chunk
    where chunk_status = 'N'
      and chunk_id <> 0
      order by chunk_id;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${restartArr}" "GET_CHUNKS" ${FATAL} ${ERRORFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned restart array
   if [ -z ${restartArr} ]; then
      LOG_ERROR "Unable to retrieve unprocessed chunk IDs." "GET_CHUNKS" ${FATAL} ${ERRORFILE} ${pgmName}
      exit ${FATAL}
   else
      #check if the restartArr contains only numeric values
      if [[ ${restartArr[0]} != +([0-9]) ]]; then
         LOG_ERROR "Database Access failed." "GET_CHUNKS" ${FATAL} ${ERRORFILE} ${pgmName}
         USAGE
         exit ${FATAL}
      else
         LOG_MESSAGE "GET_CHUNKS - Chunk IDs retrieved." "GET_CHUNKS" ${OK} ${LOGFILE} ${pgmName}
      fi
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_TRAN_DATA
# Purpose      : Calls function TRAN_DATA_IMPORT_SQL.IMPORT_CHUNK for processing each chunk.
#-------------------------------------------------------------------------
function PROCESS_TRAN_DATA
{
   chunkVal=$1

   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT TRAN_DATA_IMPORT_SQL.IMPORT_CHUNK(${chunkVal},
                                                  :GV_script_error) then
            raise FUNCTION_ERROR;
         end if;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   # insert into RESTART_PROGRAM_HISTORY for thread_val
   INSERT_PROGRAM_STATUS_HISTORY ${thread_val}

   if [[ $? -ne ${OK} ]]; then
      echo "Function call PROCESS_TRAN_DATA Chunk ID ${chunkVal} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "Chunk ID ${chunkVal} successfully processed." "PROCESS_TRAN_DATA" ${OK} ${LOGFILE} ${pgmName}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: INSERT_PROGRAM_STATUS_HISTORY
# Purpose      : Inserts record into PROGRAM_STATUS_HISTORY for each thread processed.
#-------------------------------------------------------------------------
function INSERT_PROGRAM_STATUS_HISTORY
{
   threadVal=$1

   sqlTxt="
   DECLARE
      L_commit_max_ctr   RESTART_CONTROL.COMMIT_MAX_CTR%TYPE;
   BEGIN

      select commit_max_ctr
        into L_commit_max_ctr
        from restart_control
       where lower(program_name) = 'trandataprocess';

      insert into restart_program_history
                 (restart_name
                 ,thread_val
                 ,start_time
                 ,program_name
                 ,num_threads
                 ,commit_max_ctr
                 ,finish_time)
           values 
                 ('${pgmName}'
                 ,${threadVal}
                 ,sysdate
                 ,'${pgmName}'
                 ,${parallelThreads}
                 ,L_commit_max_ctr
                 ,sysdate);

         COMMIT;

   EXCEPTION
      when OTHERS then
         ROLLBACK;
         :GV_script_error := SQLERRM;
         :GV_return_code := ${FATAL};
   END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "Function call INSERT_PROGRAM_STATUS_HISTORY for Thread ID ${threadVal} failed." >>${ERRORFILE}
      return ${NON_FATAL}
   else
      LOG_MESSAGE "Insert into restart_program_history for Thread ID ${threadVal} successful." "INSERT_PROGRAM_STATUS_HISTORY" ${OK} ${LOGFILE} ${pgmName}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: WRITE_REJECT_FILE
# Purpose      : Writes errored out records to the reject file.
#-------------------------------------------------------------------------
function WRITE_REJECT_FILE
{
   LOG_MESSAGE "Writing reject file ${rejectFile}." "WRITE_REJECT_FILE" ${OK} ${LOGFILE} ${pgmName}

   echo `sqlplus -s $connectStr <<EOF
        set PAGESIZE 0
        set LINESIZE 1000
        set HEADING OFF
        set FEEDBACK OFF
        set NEWPAGE NONE
        set TERMOUT OFF
        set SHOWMODE OFF
        set AUTOPRINT OFF
        set VERIFY OFF
        set TRIMOUT OFF
        set TRIMSPOOL ON
        select TRAN_ID||'|'|| 
               ITEM||'|'|| 
               DEPT||'|'|| 
               CLASS||'|'|| 
               SUBCLASS||'|'|| 
               LOC_TYPE||'|'|| 
               LOCATION||'|'|| 
               TRAN_DATE||'|'|| 
               TRAN_CODE||'|'|| 
               ADJ_CODE||'|'|| 
               UNITS||'|'|| 
               TOTAL_COST||'|'|| 
               TOTAL_RETAIL||'|'|| 
               REF_NO_1||'|'|| 
               REF_NO_2||'|'|| 
               GL_REF_NO||'|'|| 
               OLD_UNIT_RETAIL||'|'|| 
               NEW_UNIT_RETAIL||'|'|| 
               PGM_NAME||'|'|| 
               SALES_TYPE||'|'|| 
               VAT_RATE||'|'|| 
               AV_COST||'|'|| 
               REF_PACK_NO||'|'|| 
               TOTAL_COST_EXCL_ELC||'|'|| 
               WAC_RECALC_IND||'|'|| 
               STATUS||'|'|| 
               ERR_MSG||'|'|| 
               CREATE_TIMESTAMP||'|'|| 
               LAST_UPDATED_TIMESTAMP||CHR(13)
          from STAGE_EXT_TRAN_DATA
         where status = 'E';
        exit;
        EOF` >> ${rejectFile}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error writing reject file ${rejectFile}." "WRITE_REJECT_FILE" ${FATAL} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: TRUNCATE_TABLES
# Purpose      : Clearing out the staging tables.
#-------------------------------------------------------------------------
function TRUNCATE_TABLES
{

   sqlTxt="
   DECLARE
      L_owner VARCHAR2(30) := NULL;
   BEGIN

      select table_owner
        into L_owner
        from system_options;

   EXECUTE IMMEDIATE 'TRUNCATE TABLE '||L_owner||'.STAGE_EXT_TRAN_DATA_CHUNK';

   EXCEPTION
      when OTHERS then
         :GV_script_error := SQLERRM;
         :GV_return_code := ${FATAL};
   END;"

   EXEC_SQL ${sqlTxt}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error truncating tables." "TRUNCATE_TABLES" ${FATAL} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "${pgmName} - External Tran data Staging tables truncated." "TRUNCATE_TABLES" ${OK} ${LOGFILE} ${pgmName}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
numThreads=$2
rejectFile=reject.dat
LOG_MESSAGE "trandataprocess.ksh started by ${USER}." "MAIN function" ${OK} ${LOGFILE} ${pgmName}

# Get the maximum concurrent threads from the configuration table
if [ $# -eq 2 ] 
then
parallelThreads=$numThreads
else
GET_THREADS
fi

# Retrieve chunks to process into an indexed array
GET_CHUNKS

# Iterate through the chunk IDs and call the core logic for each
arr_index=0
thread_val=1

while [ ${arr_index} -lt  ${#restartArr[*]} ]
do
   if [[ `jobs | wc -l` -lt ${parallelThreads} ]]
   then 
       (
         PROCESS_TRAN_DATA ${restartArr[${arr_index}]}
       ) &
   else
      # Loop until a thread becomes available
      while [[ `jobs | wc -l` -ge ${parallelThreads} ]]
      do
         sleep 1
      done
       (
         PROCESS_TRAN_DATA ${restartArr[${arr_index}]}
       ) &
   fi
   let arr_index=${arr_index}+1;

   let thread_val=${thread_val}+1;
   if [[ ${thread_val} -gt ${parallelThreads} ]]
   then
      #reset 
      let thread_val=1; 
   fi

done
# Wait for all of the threads to complete
wait

# Write single error file with all the records that failed the validations
WRITE_REJECT_FILE

#TRUNCATE the table STG_TRAN_DATA_EXT_CHUNK
TRUNCATE_TABLES

LOG_MESSAGE "trandataprocess.ksh finished for ${USER}." "MAIN function" ${OK} ${LOGFILE} ${pgmName}

exit ${OK}
