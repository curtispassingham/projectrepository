
#define MAIN_MODULE
#include "saimptlog.h"

static int Process(void);

/*-------------------------------------------------------------------------
main()
-------------------------------------------------------------------------*/
int main( int argc, char *argv[] )
{
   char* function = "main";
   char* file = logmsg + sizeof(logmsg);
   int   li_init_results;

   if ( argc < NUMARGS )
   {
      fprintf( stderr, "ERROR: Usage: saimptlog user/pw infile badfile itemfile wastefile ref_itemfile prim_variantfile varupcfile storedayfile promfile codesfile errorfile storeposfile tendertypefile merchcodesfile partnerfile supplierfile employeefile bannerfile currencyfile whfile invstatusfile max_tran_gap(Optional)\n");
      sprintf( logmsg, "ERROR: Usage: saimptlog user/pw infile badfile itemfile wastefile ref_itemfile prim_variantfile varupcfile storedayfile promfile codesfile errorfile storeposfile tendertypefile merchcodesfile partnerfile supplierfile employeefile bannerfile currencyfile whfile invstatusfile max_tran_gap(Optional)");
      printf("Overflow chars: %s \n", file);
      strcpy(file, logmsg);
      LOG_MESSAGE( logmsg );
      return(FAILED);
   }

   VERBOSE2( "INFO: Logging on: <%s>\n", argv[ ARG_USRPW ] );
   if ( LOGON( argc, argv ) < 0 )
      return(FAILED);

   if (argv[ ARG_MAXTRANGAP ] == NULL)
   {
      l_max_tran_gap = NUM_MISSTRAN_ERR;
   }

   else if ( argc > NUMARGS )
   {
      if ( sa_valid_all_numeric(argv[ ARG_MAXTRANGAP ], strlen(argv[ ARG_MAXTRANGAP ])) != 0  || atoi(argv[ ARG_MAXTRANGAP ]) > 999 || atoi(argv[ ARG_MAXTRANGAP ]) == 0)
      {
         sprintf( logmsg, "Invalid MaxTranGap,Tran Gap should be Numeric and between 1-999.");
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         return( FATAL );
      }
   }

   if ( argv[ ARG_MAXTRANGAP ] != NULL )
   {
	  VERBOSE2( "INFO: Getting MAX_TRAN_GAP as Input: <%s>\n", atoi(argv[ ARG_MAXTRANGAP ] ));
      l_max_tran_gap = atoi(argv[ ARG_MAXTRANGAP ]);
   }

   if((li_init_results = Init( argc, argv )) < 0)
      gi_error_flag = 2;

   if(li_init_results != NO_THREAD_AVAILABLE)
   {
      if(li_init_results == OK)
      {
    if ( ! Process() )
       gi_error_flag = 1;
      }

      if ( ! Final() )
    if(gi_error_flag == 0)
       gi_error_flag = 3;
   }

   if(gi_error_flag == 2)
   {
      LOG_MESSAGE("Aborted in init");
      VERBOSE( "Aborted in init" );
      return(FAILED);
   }
   else if(gi_error_flag == 1)
   {
      sprintf(logmsg, "Thread %s - Aborted in process",
         ps_thread_val);
      VERBOSE( logmsg );
      LOG_MESSAGE(logmsg);
      return(FAILED);
   }
   else if(gi_error_flag == 3)
   {
      sprintf(logmsg, "Thread %s - Aborted in final", ps_thread_val);
      VERBOSE( logmsg );
      LOG_MESSAGE(logmsg);
      return(FAILED);
   }
   else if(li_init_results == NO_THREAD_AVAILABLE)
   {
      VERBOSE( "Terminated - no threads available" );
      LOG_MESSAGE("Terminated - no threads available");
      return(NO_THREADS);
   }
   else
   {
      sprintf(logmsg, "Thread %s - Terminated Successfully",
                              ps_thread_val);
      VERBOSE( logmsg );
      LOG_MESSAGE(logmsg);
   }

   return(SUCCEEDED);
}  /* end of main() */


/*-------------------------------------------------------------------------
Process() calls getNextTran to get one transaction at a time, then calls
WrOutputData to write to the output files, then writeSAVoucherData
-------------------------------------------------------------------------*/
static int Process( void )
{
   int rv = TRUE;
   int li_return = 0;

   while ( (li_return = getNextTran()) >= 0)
   {
      /* if getNextTran() returned a 0, then validate else don't */
      if (li_return == 0)
      {
         if ( ! WrOutputData())
            rv = FALSE;
         if ( ! writeSAVoucherData())
            rv = FALSE;
      }
   }
   if (li_return == -1)
      rv = FALSE;

   return( rv );
}
