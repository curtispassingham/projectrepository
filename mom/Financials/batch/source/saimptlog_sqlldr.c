
#include "saimptlog.h"
#include "saoranum.h"
#include <unistd.h>
#include <sys/stat.h>

/* filenames */
#define SATHEAD_FILENAME     "sathead"
#define SATITEM_FILENAME     "satitem"
#define SATDISC_FILENAME     "satdisc"
#define SAIGTAX_FILENAME     "saigtax"
#define SATTAX_FILENAME      "sattax"
#define SATPYMT_FILENAME     "satpymt"
#define SATTEND_FILENAME     "sattend"
#define SAERROR_FILENAME     "saerror"
#define SACUSTOMER_FILENAME  "sacust"
#define SACATT_FILENAME      "sacustatt"
#define MISSTRAN_FILENAME    "samisstr"

/* grow transaction arrays by these sizes */
#define ALLOCTRANHEADS    1
#define ALLOCTRANCUSTS    1
#define ALLOCTRANCATTS    4
#define ALLOCTRANITEMS   64
#define ALLOCTRANDISCS   32
#define ALLOCTRANIGTAXS   8
#define ALLOCTRANTAXS     8
#define ALLOCTRANPYMTS    8
#define ALLOCTRANTENDS    8

/*-------------------------------------------------------------------------
data structures
-------------------------------------------------------------------------*/

/* sa output file record structures */

/* transaction header */
typedef struct {   /* SA_THEAD struct */
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char rev_no[ LEN_SA_REV_NO ];
   char store_day_seq_no[ LEN_BIG_SEQ_NO ];
   char tran_datetime[ LEN_DATETIME ];
   char pos_register[ LEN_REGISTER ];
   char tran_no[ LEN_TRAN_NO ];
   char cashier[ LEN_EMP_ID ];
   char salesperson[ LEN_EMP_ID ];
   char tran_type[ TRAT_SIZE ];
   char sub_tran_type[ TRAS_SIZE ];
   char orig_tran_no[ LEN_TRAN_NO ];
   char orig_reg_no[ LEN_REGISTER ];
   char ref_no1[ LEN_REF_NO ];
   char ref_no2[ LEN_REF_NO ];
   char ref_no3[ LEN_REF_NO ];
   char ref_no4[ LEN_REF_NO ];
   char reason_code[ REAC_SIZE ];
   char vendor_no[ LEN_VENDOR_NO ];
   char vendor_invc_no[ LEN_VENDOR_INVC_NO ];
   char payment_ref_no[ LEN_PAYMENT_REF_NO ];
   char proof_of_delivery_no[ LEN_PROOF_DELIVERY_NO ];
   char status[ SAST_SIZE ];
   char value[ LEN_SIGNED_DEC_AMT ];
   char pos_tran_ind[ LEN_IND ];
   char update_id[ LEN_UPDATE_ID ];
   char update_datetime[ LEN_DATETIME ];
   char error_ind[ LEN_IND ];
   char banner_id[ LEN_BANNER_ID ];
   char rounded_amt[ LEN_SIGNED_DEC_AMT ];
   char rounded_off_amt[ LEN_SIGNED_DEC_AMT ];
   char credit_promotion_id[ LEN_CREDIT_PROMOTION_ID ];
   char ref_no25 [ LEN_REF_NO ];
   char ref_no26 [ LEN_REF_NO ];
   char ref_no27 [ LEN_REF_NO ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char rtlog_orig_sys[ LEN_RTLOG_ORIG_SYS ];
   char tran_process_sys[ LEN_TRAN_PROCESS_SYS ];
   char newline[ LEN_NEWLINE ];
} SA_THEAD;

/* transaction item */
typedef struct {    /* SA_TITEM struct */
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char item_seq_no[ LEN_LITTLE_SEQ_NO ];
   char item_status[ SASI_SIZE ];
   char item_type[ SAIT_SIZE ];
   char item[ LEN_ITEM ];
   char ref_item[ LEN_ITEM ];
   char non_merch_item[ LEN_ITEM ];
   char voucher_no[ LEN_VOUCHER_NO ];
   char dept[ LEN_DEPT ];
   char class[ LEN_CLASS ];
   char subclass[ LEN_SUBCLASS ];
   char qty[ LEN_SIGNED_DEC_QTY ];
   char unit_retail[ LEN_DEC_AMT ];
   char unit_retail_vat_incl[ LEN_IND ];
   char as_selling_uom[ LEN_UOM ];
   char override_reason[ ORRC_SIZE ];
   char orig_unit_retail[ LEN_DEC_AMT ];
   char standard_orig_unit_retail[ LEN_DEC_AMT ];
   char tax_ind[ LEN_IND ];
   char item_swiped_ind[ LEN_IND ];
   char error_ind[ LEN_IND ];
   char as_drop_ship_ind[ LEN_IND ];
   char waste_type[ WSTG_SIZE ];
   char waste_pct[ LEN_PCT ];
   char pump[ LEN_PUMP ];
   char return_reason_code[ SARR_SIZE ];
   char salesperson[ LEN_EMP_ID ];
   char expiration_date[ LEN_DATEONLY ];
   char as_standard_qty[ LEN_SIGNED_DEC_QTY ];
   char as_standard_unit_retail[ LEN_DEC_AMT ];
   char as_standard_uom[ LEN_UOM ];
   char ref_no5[ LEN_REF_NO ];
   char ref_no6[ LEN_REF_NO ];
   char ref_no7[ LEN_REF_NO ];
   char ref_no8[ LEN_REF_NO ];
   char catchweight_ind[ LEN_CATCHWEIGHT_IND ];
   char selling_item[ LEN_SELLING_ITEM ];
   char cust_ord_line_no[ LEN_CUST_ORD_LINE_NO ];
   char media_id[ LEN_MEDIA_ID ];
   char uom_qty[ LEN_UOM_QTY ];
   char total_igtax_amt[ LEN_DEC_AMT ];
   char unique_id[LEN_UNIQUE_ID];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char cust_order_no[ LEN_CUST_ORDER_NO ];
   char cust_order_date[ LEN_CUST_ORD_HEAD_DATE ];
   char fulfill_order_no[ LEN_CUST_ORDER_NO ];
   char no_inv_ret_ind[ LEN_IND ];
   char sales_type[ LEN_IND ];
   char return_wh[ LEN_LOC ];
   char return_disp[ LEN_RETURN_DISP ];
   char newline[ LEN_NEWLINE ];
} SA_TITEM;

/* transaction discount */
typedef struct { /* SA_TDISC struct */
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char item_seq_no[ LEN_LITTLE_SEQ_NO ];
   char disc_seq_no[ LEN_LITTLE_SEQ_NO ];
   char rms_promo_type[ PRMT_SIZE ];
   char promotion[ LEN_PROMOTION ];
   char disc_type[ SADT_SIZE ];
   char coupon_no[ LEN_COUPON_NO ];
   char coupon_ref_no[ LEN_COUPON_REF_NO ];
   char qty[ LEN_SIGNED_DEC_QTY ];
   char unit_disc_amt[ LEN_DEC_AMT ];
   char as_standard_qty[ LEN_SIGNED_DEC_QTY ];
   char as_standard_unit_disc_amt[ LEN_DEC_AMT ];
   char ref_no13[ LEN_REF_NO ];
   char ref_no14[ LEN_REF_NO ];
   char ref_no15[ LEN_REF_NO ];
   char ref_no16[ LEN_REF_NO ];
   char error_ind[ LEN_IND ];
   char catchweight_ind[ LEN_CATCHWEIGHT_IND ];
   char uom_qty[ LEN_UOM_QTY ];
   char promo_comp[ LEN_PROMO_COMP ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char newline[ LEN_NEWLINE ];
} SA_TDISC;

/* item level tax */
typedef struct { /* SA_IGTAX struct */
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char item_seq_no[ LEN_LITTLE_SEQ_NO ];
   char igtax_seq_no [ LEN_LITTLE_SEQ_NO ];
   char tax_authority [ LEN_TAX_AUTHORITY ];
   char igtax_code [ LEN_IGTAX_CODE ];
   char igtax_rate [ LEN_DEC_IGTAX_RATE ];
   char total_igtax_amt [ LEN_SIGNED_DEC_AMT ] ;
   char as_standard_qty[ LEN_SIGNED_DEC_QTY ];
   char as_standard_unit_igtax_amt[ LEN_DEC_AMT ];
   char error_ind[ LEN_IND ];
   char ref_no21[ LEN_REF_NO ];
   char ref_no22[ LEN_REF_NO ];
   char ref_no23[ LEN_REF_NO ];
   char ref_no24[ LEN_REF_NO ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char newline[ LEN_NEWLINE ];
} SA_IGTAX;

/* transaction tax */
typedef struct {    /* SA_TTAX struct */
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char tax_code[ TAXC_SIZE ];
   char tax_seq_no[ LEN_LITTLE_SEQ_NO ];
   char tax_amt[ LEN_SIGNED_DEC_AMT ];
   char error_ind[ LEN_IND ];
   char ref_no17[ LEN_REF_NO ];
   char ref_no18[ LEN_REF_NO ];
   char ref_no19[ LEN_REF_NO ];
   char ref_no20[ LEN_REF_NO ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char newline[ LEN_NEWLINE ];
} SA_TTAX;

/* transaction payment */
typedef struct {    /* SA_TPYMT struct */
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char payment_seq_no[ LEN_LITTLE_SEQ_NO ];
   char payment_amt[ LEN_SIGNED_DEC_AMT ];
   char error_ind[ LEN_IND ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char newline[ LEN_NEWLINE ];
} SA_TPYMT;

/* transaction tender */
typedef struct {
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char tender_seq_no[ LEN_LITTLE_SEQ_NO ];
   char tender_type_group[ TENT_SIZE ];
   char tender_type_id[ TENS_SIZE ];
   char tender_amt[ LEN_SIGNED_DEC_AMT ];
   char cc_no[ LEN_CC_NO ];
   char cc_exp_date[ LEN_DATEONLY ];
   char cc_auth_no[ LEN_CC_AUTH_NO ];
   char cc_auth_src[ CCAS_SIZE ];
   char cc_entry_mode[ CCEM_SIZE ];
   char cc_cardholder_verf[ CCVF_SIZE ];
   char cc_term_id[ LEN_TERM_ID ];
   char cc_spec_cond[ CCSC_SIZE ];
   char voucher_no[ LEN_VOUCHER_NO ];
   char coupon_no[ LEN_COUPON_NO ];
   char coupon_ref_no[ LEN_COUPON_REF_NO ];
   char check_acct_no[ LEN_CHECK_ACCT_NO ];
   char check_no[ LEN_CHECK_NO ];
   char identi_method[ IDMH_SIZE ];
   char identi_id[ LEN_IDENTI_ID ];
   char orig_currency[ LEN_CURRENCY_CODE ];
   char orig_curr_amt[ LEN_SIGNED_DEC_AMT ];
   char ref_no9[ LEN_REF_NO ];
   char ref_no10[ LEN_REF_NO ];
   char ref_no11[ LEN_REF_NO ];
   char ref_no12[ LEN_REF_NO ];
   char error_ind[ LEN_IND ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char newline[ LEN_NEWLINE ];
} SA_TTEND;

/* transaction error */
typedef struct {    /* SA_ERROR struct */
   char error_seq_no[ LEN_BIG_SEQ_NO ];
   char store_day_seq_no[ LEN_BIG_SEQ_NO ];
   char bal_group_seq_no[ LEN_BIG_SEQ_NO ];
   char total_seq_no[ LEN_BIG_SEQ_NO ];
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char error_code[ LEN_ERROR_CODE ];
   char seq_no1[ LEN_LITTLE_SEQ_NO ];
   char seq_no2[ LEN_LITTLE_SEQ_NO ];
   char rec_type[ SART_SIZE ];
   char store_override_ind[ LEN_IND ];
   char hq_override_ind[ LEN_IND ];
   char update_id[ LEN_UPDATE_ID ];
   char update_datetime[ LEN_DATETIME ];
   char orig_value[ LEN_ORIG_VALUE ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char newline[ LEN_NEWLINE ];
} SA_ERROR;

/* customer */
typedef struct {    /* SA_CUST struct */
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char cust_id[ LEN_CUST_ID ];
   char cust_id_type[ CIDT_SIZE ];
   char name[ LEN_CUST_NAME ];
   char addr1[ LEN_CUST_ADDR ];
   char addr2[ LEN_CUST_ADDR ];
   char city[ LEN_CITY ];
   char state[ LEN_STATE ];
   char postal_code[ LEN_POSTAL_CODE ];
   char country[ LEN_COUNTRY ];
   char home_phone[ LEN_PHONE ];
   char work_phone[ LEN_PHONE ];
   char e_mail[ LEN_EMAIL ];
   char birthdate[ LEN_DATEONLY ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char newline[ LEN_NEWLINE ];
} SA_CUST;

/* customer attribute */
typedef struct {    /* SA_CATT */
   char tran_seq_no[ LEN_BIG_SEQ_NO ];
   char attrib_seq_no[ LEN_LITTLE_SEQ_NO ];
   char attrib_type[ SACA_SIZE ];
   char attrib_value[ LEN_CODE ];
   char store[ LEN_LOC ];
   char day[ LEN_DAY ];
   char newline[ LEN_NEWLINE ];
} SA_CATT;

/* missing transaction */
typedef struct {    /* SA_MISSTRAN struct */
   char miss_tran_seq_no[ LEN_BIG_SEQ_NO ];
   char store_day_seq_no[ LEN_BIG_SEQ_NO ];
   char pos_register[ LEN_REGISTER ];
   char tran_no[ LEN_TRAN_NO ];
   char status[ SAMS_SIZE ];
   char rtlog_orig_sys[ LEN_RTLOG_ORIG_SYS ];
   char newline[ LEN_NEWLINE ];
} SA_MISSTRAN;

/*-------------------------------------------------------------------------
module globals
-------------------------------------------------------------------------*/

static long UsedTranHeads  = 0;
static long UsedTranCusts  = 0;
static long UsedTranCAtts  = 0;
static long UsedTranItems  = 0;
static long UsedTranDiscs  = 0;
static long UsedTranIgtaxs = 0;
static long UsedTranTaxs   = 0;
static long UsedTranPymts  = 0;
static long UsedTranTends  = 0;

char SATHeadFileTmp[PATH_MAX] = "./sathead_XXXXXX";  /* Temporary file names */
char SACustomerFileTmp[PATH_MAX] = "./sacust_XXXXXX";
char SACAttFileTmp[PATH_MAX] = "./saatt_XXXXXX";
char SATItemFileTmp[PATH_MAX] = "./saitem_XXXXXX";
char SATDiscFileTmp[PATH_MAX] = "./sadisc_XXXXXX";
char SAIGtaxFileTmp[PATH_MAX] = "./saigtax_XXXXXX";
char SATTaxFileTmp[PATH_MAX] = "./satax_XXXXXX";
char SATPymtFileTmp[PATH_MAX] = "./sapymt_XXXXXX";
char SATTendFileTmp[PATH_MAX] = "./satend_XXXXXX";
char SAErrorFileTmp[PATH_MAX] = "./saerr_XXXXXX";
char SAMissTranFileTmp[PATH_MAX] = "./samisst_XXXXXX";
int SATHeadFiledes = -1;  /* Temp file descriptors */
int SACustomerFiledes = -1;
int SACAttFiledes = -1;
int SATItemFiledes = -1;
int SATDiscFiledes = -1;
int SAIGtaxFiledes = -1;
int SATTaxFiledes = -1;
int SATPymtFiledes = -1;
int SATTendFiledes = -1;
int SAErrorFiledes = -1;
int SAMissTranFiledes = -1;

/* file handles */
static FILE *SATHeadFile = NULL;    /* SQL load output files */
static FILE *SACustomerFile = NULL;
static FILE *SACAttFile = NULL;
static FILE *SATItemFile = NULL;
static FILE *SATDiscFile = NULL;
static FILE *SAIGtaxFile = NULL;
static FILE *SATTaxFile = NULL;
static FILE *SATPymtFile = NULL;
static FILE *SATTendFile = NULL;
static FILE *SAErrorFile = NULL;
static FILE *SAMissTranFile = NULL;

/* standard format output records */
static SA_THEAD   **SATHead;
static SA_CUST    **SACust;
static SA_CATT    **SACAtt;
static SA_TITEM   **SATItem;
static SA_TDISC   **SATDisc;
static SA_IGTAX   **SAIGtax;
static SA_TTAX    **SATTax;
static SA_TPYMT   **SATPymt;
static SA_TTEND   **SATTend;
/* others to come... */

/* indexes into standard format output records */
static int HeadNo;     /* which head # we're doing */
static int CustNo;     /* which customer # we're doing */
static int CAttNo;     /* which customer attribute # we're doing */
static int ItemNo;     /* which item # we're doing */
static int DiscNo;     /* which disc # we're doing */
static int IgtaxNo;    /* which igtax # we're doing */
static int TaxNo;      /* which tax # we're doing */
static int PymtNo;     /* which pymt # we're doing */
static int TendNo;     /* which tender # we're doing */

/*-------------------------------------------------------------------------
local function prototypes
-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
InitOutputData() opens the temporary SQL*Ldr output files.
-------------------------------------------------------------------------*/
int InitOutputData( int argc, char *argv[] )
{
   char *function = "InitOutputData";

   VERBOSE( "INFO: Opening output files\n" );

   /* open SQL load output files */
   SATHeadFiledes = mkstemp(SATHeadFileTmp);
   if (SATHeadFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SATHEAD_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SATHeadFile = fdopen( SATHeadFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SATHEAD_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SACustomerFiledes = mkstemp(SACustomerFileTmp);
   if (SACustomerFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SACUSTOMER_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SACustomerFile = fdopen( SACustomerFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SACUSTOMER_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SACAttFiledes = mkstemp(SACAttFileTmp);
   if (SACAttFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SACATT_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SACAttFile = fdopen( SACAttFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SACATT_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      fclose( SACustomerFile );
      unlink( SACustomerFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SATItemFiledes = mkstemp(SATItemFileTmp);
   if (SATItemFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SATITEM_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SATItemFile = fdopen( SATItemFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SATITEM_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      fclose( SACustomerFile );
      unlink( SACustomerFileTmp );
      fclose( SACAttFile );
      unlink( SACAttFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SATDiscFiledes = mkstemp(SATDiscFileTmp);
   if (SATDiscFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SATDISC_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SATDiscFile = fdopen( SATDiscFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SATDISC_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      fclose( SACustomerFile );
      unlink( SACustomerFileTmp );
      fclose( SACAttFile );
      unlink( SACAttFileTmp );
      fclose( SATItemFile );
      unlink( SATItemFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SAIGtaxFiledes = mkstemp(SAIGtaxFileTmp);
   if (SAIGtaxFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SAIGTAX_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SAIGtaxFile = fdopen( SAIGtaxFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SAIGTAX_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      fclose( SACustomerFile );
      unlink( SACustomerFileTmp );
      fclose( SACAttFile );
      unlink( SACAttFileTmp );
      fclose( SATItemFile );
      unlink( SATItemFileTmp );
      fclose( SATDiscFile );
      unlink( SATDiscFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SATTaxFiledes = mkstemp(SATTaxFileTmp);
   if (SATTaxFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SATTAX_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SATTaxFile = fdopen( SATTaxFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SATTAX_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      fclose( SACustomerFile );
      unlink( SACustomerFileTmp );
      fclose( SACAttFile );
      unlink( SACAttFileTmp );
      fclose( SATItemFile );
      unlink( SATItemFileTmp );
      fclose( SATDiscFile );
      unlink( SATDiscFileTmp );
      fclose( SAIGtaxFile );
      unlink( SAIGtaxFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SATPymtFiledes = mkstemp(SATPymtFileTmp);
   if (SATPymtFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SATPYMT_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SATPymtFile = fdopen( SATPymtFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SATPYMT_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      fclose( SACustomerFile );
      unlink( SACustomerFileTmp );
      fclose( SACAttFile );
      unlink( SACAttFileTmp );
      fclose( SATItemFile );
      unlink( SATItemFileTmp );
      fclose( SATDiscFile );
      unlink( SATDiscFileTmp );
      fclose( SAIGtaxFile );
      unlink( SAIGtaxFileTmp );
      fclose( SATTaxFile );
      unlink( SATTaxFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SATTendFiledes = mkstemp(SATTendFileTmp);
   if (SATTendFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SATTEND_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SATTendFile = fdopen( SATTendFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SATTEND_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      fclose( SACustomerFile );
      unlink( SACustomerFileTmp );
      fclose( SACAttFile );
      unlink( SACAttFileTmp );
      fclose( SATItemFile );
      unlink( SATItemFileTmp );
      fclose( SATDiscFile );
      unlink( SATDiscFileTmp );
      fclose( SAIGtaxFile );
      unlink( SAIGtaxFileTmp );
      fclose( SATTaxFile );
      unlink( SATTaxFileTmp );
      fclose( SATPymtFile );
      unlink( SATPymtFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   SAErrorFiledes = mkstemp(SAErrorFileTmp);
   if (SAErrorFiledes < 1)
   {
      sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", SAERROR_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if ( ( SAErrorFile = fdopen( SAErrorFiledes, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "ERROR: Cannot open output file <%s>", SAERROR_FILENAME );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      fclose( SATHeadFile );
      unlink( SATHeadFileTmp );
      fclose( SACustomerFile );
      unlink( SACustomerFileTmp );
      fclose( SACAttFile );
      unlink( SACAttFileTmp );
      fclose( SATItemFile );
      unlink( SATItemFileTmp );
      fclose( SATDiscFile );
      unlink( SATDiscFileTmp );
      fclose( SAIGtaxFile );
      unlink( SAIGtaxFileTmp );
      fclose( SATTaxFile );
      unlink( SATTaxFileTmp );
      fclose( SATPymtFile );
      unlink( SATPymtFileTmp );
      fclose( SATTendFile );
      unlink( SATTendFileTmp );
      ExitCode = EXIT_FAILURE;
      return( -1 );
   }
   if (strcmp( SysOpt.s_check_dup_miss_tran, YSNO_Y) == 0)
   {
      /* open missing transaction output file */
      SAMissTranFiledes = mkstemp(SAMissTranFileTmp);
      if (SAMissTranFiledes < 1)
      {
         sprintf( logmsg, "ERROR: Cannot create temp file for <%s>", MISSTRAN_FILENAME );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         return( -1 );
      }
      if ( ( SAMissTranFile = fdopen( SAMissTranFiledes, SA_WRITEMODE ) ) == NULL )
      {
         sprintf( logmsg, "ERROR: Cannot open output file <%s>", MISSTRAN_FILENAME );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         fclose( SATHeadFile );
         unlink( SATHeadFileTmp );
         fclose( SACustomerFile );
         unlink( SACustomerFileTmp );
         fclose( SACAttFile );
         unlink( SACAttFileTmp );
         fclose( SATItemFile );
         unlink( SATItemFileTmp );
         fclose( SATDiscFile );
         unlink( SATDiscFileTmp );
         fclose( SAIGtaxFile );
         unlink( SAIGtaxFileTmp );
         fclose( SATTaxFile );
         unlink( SATTaxFileTmp );
         fclose( SATPymtFile );
         unlink( SATPymtFileTmp );
         fclose( SATTendFile );
         unlink( SATTendFileTmp );
         fclose( SAErrorFile );
         unlink( SAErrorFileTmp );
         ExitCode = EXIT_FAILURE;
         return( -1 );
      }
   }

   SATHead = NULL;
   SACust = NULL;
   SACAtt = NULL;
   SATItem = NULL;
   SATDisc = NULL;
   SAIGtax = NULL;
   SATTax = NULL;
   SATPymt = NULL;
   SATTend = NULL;

   return( 0 );
}

/*-------------------------------------------------------------------------
InitOutputClean() closes and removes the temporary SQL*Ldr files.
-------------------------------------------------------------------------*/
void InitOutputClean( void )
{

   fclose( SATHeadFile );
   unlink( SATHeadFileTmp );
   fclose( SACustomerFile );
   unlink( SACustomerFileTmp );
   fclose( SACAttFile );
   unlink( SACAttFileTmp );
   fclose( SATItemFile );
   unlink( SATItemFileTmp );
   fclose( SATDiscFile );
   unlink( SATDiscFileTmp );
   fclose( SAIGtaxFile );
   unlink( SAIGtaxFileTmp );
   fclose( SATTaxFile );
   unlink( SATTaxFileTmp );
   fclose( SATPymtFile );
   unlink( SATPymtFileTmp );
   fclose( SATTendFile );
   unlink( SATTendFileTmp );
   fclose( SAErrorFile );
   unlink( SAErrorFileTmp );

   if (strcmp( SysOpt.s_check_dup_miss_tran, YSNO_Y) == 0)
   {
      fclose( SAMissTranFile );
      unlink( SAMissTranFileTmp );
   }
}


/*-------------------------------------------------------------------------
CreateTermRecords() creates terminating records for each record type.
-------------------------------------------------------------------------*/
int CreateTermRecords( void )
{
   RTL_THEAD thead;
   RTL_TCUST tcust;
   RTL_CATT catt;
   RTL_TITEM titem;
   RTL_IDISC idisc;
   RTL_IGTAX igtax;
   RTL_TTAX ttax;
   RTL_TPYMT tpymt;
   RTL_TTEND ttend;
   SA_MISSTRAN mtr;
   pt_item_uom uom;
   pt_disc_uom disc_uom;
   pt_igtax_tia igtax_tia;
   char *mtsn = NULL;
   char tran_no[ NULL_TRAN_NO ];
   int rv = TRUE;

   HeadNo = CustNo = CAttNo = ItemNo = DiscNo = IgtaxNo = TaxNo = PymtNo = TendNo = 0;

   if (strcmp( SysOpt.s_check_dup_miss_tran, YSNO_Y) == 0)
   {
      memset( &mtr, ' ', sizeof( SA_MISSTRAN ) );
      if ( (mtsn = nextMissingTranSeqNo(SysOpt.s_table_owner)) != NULL)
         CPYSTRFXL( mtr.miss_tran_seq_no, mtsn, FT_NUMBER );
      CPYSTRFXL( mtr.store_day_seq_no, StoreDaySeqNo, LEN_BIG_SEQ_NO );
      sprintf( tran_no, "%0*d", LEN_TRAN_NO, -1 );
      CPYSTRFXL( mtr.tran_no, tran_no, FT_NUMBER );
      CPYSTRFXL( mtr.status, SAMS_M, FT_VARCHAR );
      CPYSTRFXL( mtr.rtlog_orig_sys, Rtlog_Orig_Sys, FT_VARCHAR );
      mtr.newline[ 0 ] = '\n';
      if ( ! putrec( (char *)&mtr, SAMissTranFile ) )
      {
         rv = FALSE;
         ExitCode = EXIT_FAILURE;
      }
   }

   TranSeqNo = nextTranSeqNo(SysOpt.s_table_owner);
   memset( &thead, ' ', sizeof( thead));
   CPYSTRFXL( thead.tran_datetime, BusinessDate, FT_DATE);
   memset( thead.tran_datetime + LEN_DATEONLY, '0', LEN_TIME);
   memset( thead.tran_no, '0', sizeof( thead.tran_no));
   CPYSTRFXL( thead.tran_type, TRAT_TERM, FT_VARCHAR);
   CPYSTRFXL( thead.rounded_amt_sign, SIGN_P, FT_VARCHAR);
   CPYSTRFXL( thead.rounded_off_amt_sign, SIGN_P, FT_VARCHAR);
   memset( thead.rounded_amt, '0', sizeof( thead.rounded_amt));
   memset( thead.rounded_off_amt, '0', sizeof( thead.rounded_off_amt));
   memset( thead.credit_promotion_id, '0', sizeof( thead.credit_promotion_id));
   if (strcmp( SysOpt.s_check_dup_miss_tran, YSNO_Y) == 0)
      CPYSTRFXL( thead.ref_no1, mtsn, FT_VARCHAR);
   if ( ! fmtSATHead( &thead, "0") )
      rv = FALSE;

   memset( &tcust, ' ', sizeof( tcust));
   memset( tcust.cust_id, '0', sizeof( tcust.cust_id));
   CPYSTRFXL( tcust.cust_id_type, CIDT_TERM, FT_VARCHAR);
   if ( ! fmtSACustomer( &tcust) )
      rv = FALSE;

   memset( &catt, ' ', sizeof( catt));
   CPYSTRFXL( catt.attrib_type, SACA_TERM, FT_VARCHAR);
   CPYSTRFXL( catt.attrib_value, TERM_TERM, FT_VARCHAR);
   if ( ! fmtSACAtt( &catt, 1) )
      rv = FALSE;

   memset( &titem, ' ', sizeof( titem));
   CPYSTRFXL( titem.item_status, SASI_S, FT_VARCHAR);
   CPYSTRFXL( titem.item_type, SAIT_TERM, FT_VARCHAR);
   CPYSTRFXL( titem.qty_sign, SIGN_P, FT_VARCHAR);
   memset( titem.qty, '0', sizeof( titem.qty));
   memset( titem.uom_qty, '0', sizeof( titem.uom_qty));
   memset( titem.unit_retail, '0', sizeof( titem.unit_retail));
   memset( uom.standard_uom, ' ', sizeof( uom.standard_uom));
   memset( uom.standard_qty, '0', sizeof( uom.standard_qty));
   memset( uom.standard_unit_retail, '0', sizeof( uom.standard_unit_retail));
   CPYSTRFXL( uom.class_vat_ind, YSNO_N, FT_VARCHAR);
   CPYSTRFXL( titem.tax_ind, YSNO_N, FT_VARCHAR);
   CPYSTRFXL( titem.item_swiped_ind, YSNO_N, FT_VARCHAR);
   CPYSTRFXL( titem.drop_ship_ind, YSNO_N, FT_VARCHAR);
   memset( uom.total_igtax_amt, '0', sizeof( uom.total_igtax_amt ) );
   if ( ! fmtSATItem( &titem, 1, &uom ) )
      rv = FALSE;

   memset( &idisc, ' ', sizeof( idisc));
   CPYSTRFXL( idisc.rms_promo_type, "TERM", FT_VARCHAR);
   CPYSTRFXL( idisc.disc_type, "TERM", FT_VARCHAR);
   CPYSTRFXL( idisc.qty_sign, SIGN_P, FT_VARCHAR);
   memset( idisc.qty, '0', sizeof( idisc.qty));
   memset( idisc.uom_qty, '0', sizeof( idisc.uom_qty));
   memset( idisc.unit_disc_amt, '0', sizeof( idisc.unit_disc_amt));
   memset( disc_uom.standard_qty, '0', sizeof( disc_uom.standard_qty));
   memset( disc_uom.standard_unit_disc_amt, '0', sizeof( disc_uom.standard_unit_disc_amt));
   if ( ! fmtSATDisc( &idisc, 1, 1, &disc_uom) )
      rv = FALSE;

   memset( &igtax, ' ', sizeof( igtax));
   memset( igtax.tax_authority, '0', sizeof( igtax.tax_authority));
   CPYSTRFXL( igtax.igtax_code, "TERM", FT_VARCHAR);
   memset( igtax.igtax_rate, '0', sizeof( igtax.igtax_rate));
   CPYSTRFXL( igtax.igtax_amt_sign, SIGN_P, FT_VARCHAR);
   memset( igtax_tia.TotalIgtaxAmt, '0', sizeof( igtax_tia.TotalIgtaxAmt ) );
   if ( ! fmtSAIGtax( &igtax, 1, 1, &igtax_tia ) )
      rv = FALSE;

   memset( &ttax, ' ', sizeof( ttax));
   CPYSTRFXL( ttax.tax_code, TAXC_TERM, FT_VARCHAR);
   CPYSTRFXL( ttax.tax_amt_sign, SIGN_P, FT_VARCHAR);
   memset( ttax.tax_amt, '0', sizeof( ttax.tax_amt));
   if ( ! fmtSATTax( &ttax, 1) )
      rv = FALSE;

   memset( &tpymt, ' ', sizeof( tpymt));
   CPYSTRFXL( tpymt.payment_amt_sign, SIGN_P, FT_VARCHAR);
   memset( tpymt.payment_amt, '0', sizeof( tpymt.payment_amt));
   if ( ! fmtSATPymt( &tpymt, 1) )
      rv = FALSE;

   memset( &ttend, ' ', sizeof( ttend));
   CPYSTRFXL( ttend.tender_type_group, TENT_TERM, FT_VARCHAR);
   CPYSTRFXL( ttend.tender_amt_sign, SIGN_P, FT_VARCHAR);
   memset( ttend.tender_type_id, '0', sizeof( ttend.tender_type_id));
   memset( ttend.tender_amt, '0', sizeof( ttend.tender_amt));

   if ( ! fmtSATTend( &ttend, 1) )
      rv = FALSE;

   WrBadTranError( TERM_MARKER_NO_ERROR, TranSeqNo, -1, -1, SART_THEAD, "", 0 );

   return( rv);
}


/*-------------------------------------------------------------------------
resetFmt() clears the arrays used for format the SQL*Ldr files.
-------------------------------------------------------------------------*/
void resetFmt( void )
{
   HeadNo = CustNo = CAttNo = ItemNo = DiscNo = IgtaxNo = TaxNo = PymtNo = TendNo = 0;
}


/*-------------------------------------------------------------------------
saveFmt() dummy routine to support array INSERTs.
-------------------------------------------------------------------------*/
void saveFmt( void )
{
}


/*-------------------------------------------------------------------------
abortFmt() dummy routine to support array INSERTs.
-------------------------------------------------------------------------*/
void abortFmt( void )
{
}

/*-------------------------------------------------------------------------
clearBigGapErrors() dummy routine to support array INSERTs.
-------------------------------------------------------------------------*/
int clearBigGapErrors( void )
{
   return( TRUE );
}

/*-------------------------------------------------------------------------
fmtSATHead() formats the global SA_THEAD record from the existing
transactions data.
-------------------------------------------------------------------------*/
int fmtSATHead( RTL_THEAD *p, char *sale_total )
{
   char *function = "fmtSATHead";
   SA_THEAD **t;
   long i;

   /* Allocate more space as needed for the transactions */
   if ( HeadNo >= UsedTranHeads )
   {
      t = (SA_THEAD **)realloc( SATHead, (UsedTranHeads + ALLOCTRANHEADS) *
                                         sizeof( SA_THEAD *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATHead" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SATHead = t;
      for (i = UsedTranHeads; i < (UsedTranHeads + ALLOCTRANHEADS); i++)
         SATHead[ i ] = NULL;
      UsedTranHeads += ALLOCTRANHEADS;
   }
   if (SATHead[ HeadNo ] == NULL)
   {
      SATHead[ HeadNo ] = (SA_THEAD *)malloc( sizeof( SA_THEAD));
      if (SATHead[ HeadNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATHead record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SATHead[ HeadNo ]), ' ', sizeof( SA_THEAD ) );

   CPYSTRFXL( SATHead[ HeadNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   CPYSTRFXL( SATHead[ HeadNo ]->rev_no, "1", FT_NUMBER );
   CPYFXLFXL( SATHead[ HeadNo ]->store_day_seq_no, StoreDaySeqNo, FT_NUMBER );
   CPYFXLFXL( SATHead[ HeadNo ]->tran_datetime, p->tran_datetime, FT_DATE );
   CPYFXLFXL( SATHead[ HeadNo ]->pos_register, p->pos_register, FT_VARCHAR );
   if (!CMPFXLSTR( p->tran_type, TRAT_DCLOSE))
      CPYFXLFXL( SATHead[ HeadNo ]->tran_no, p->tran_no, FT_NUMBER );
   else if ( !EMPTY_FIELD( p->tran_no))
      CPYFXLFXL( SATHead[ HeadNo ]->tran_no, p->tran_no, FT_NUMBER );
   CPYFXLFXL( SATHead[ HeadNo ]->cashier, p->cashier, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->salesperson, p->salesperson, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->tran_type, p->tran_type, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->sub_tran_type, p->sub_tran_type, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->orig_tran_no, p->orig_tran_no, FT_NUMBER );
   CPYFXLFXL( SATHead[ HeadNo ]->orig_reg_no, p->orig_reg_no, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->ref_no1, p->ref_no1, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->ref_no2, p->ref_no2, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->ref_no3, p->ref_no3, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->ref_no4, p->ref_no4, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->reason_code, p->reason_code, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->vendor_no, p->vendor_no, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->vendor_invc_no, p->vendor_invc_no, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->payment_ref_no, p->payment_ref_no, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->proof_of_delivery_no, p->proof_of_delivery_no, FT_VARCHAR );
   CPYSTRFXL( SATHead[ HeadNo ]->status, SAST_P, FT_VARCHAR );
   if ((CMPFXLSTR( p->tran_type, TRAT_TOTAL))||(CMPFXLSTR( p->tran_type, TRAT_OTHER)))
   {
    if ( ! EMPTY_FIELD( p->value))
      {
         CPYFXLFXL( SATHead[ HeadNo ]->value, p->value, FT_NUMBER );
         SATHead[ HeadNo ]->value[0] = signcvt( p->value_sign);
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( SATHead[ HeadNo ]->value, AMT_DECPLACES );
#endif
      }
   }
   else
      {
       CPYSTRFXL( SATHead[ HeadNo ]->value, sale_total, FT_NUMBER );
       if (sale_total[0] != '-')
           SATHead[ HeadNo ]->value[0] = '+' ;
#ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATHead[ HeadNo ]->value, AMT_DECPLACES );
#endif
      }
   CPYFXLFXL( SATHead[ HeadNo ]->banner_id, p->banner_id, FT_NUMBER );
   CPYSTRFXL( SATHead[ HeadNo ]->pos_tran_ind, (char *)YSNO_Y, FT_VARCHAR );
   CPYSTRFXL( SATHead[ HeadNo ]->update_id, "TLOG", FT_VARCHAR );
   CPYSTRFXL( SATHead[ HeadNo ]->update_datetime, SysDate, FT_DATE );
   CPYSTRFXL( SATHead[ HeadNo ]->error_ind, YSNO_N, FT_VARCHAR);
   CPYSTRFXL( SATHead[ HeadNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SATHead[ HeadNo ]->day, Day, FT_NUMBER );
   CPYSTRFXL( SATHead[ HeadNo ]->rtlog_orig_sys, Rtlog_Orig_Sys, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->tran_process_sys, p->tran_process_sys, FT_VARCHAR );
   if ( ! EMPTY_FIELD( p->rounded_amt))
   {
      CPYFXLFXL( SATHead[ HeadNo ]->rounded_amt, p->rounded_amt, FT_NUMBER );
      SATHead[ HeadNo ]->rounded_amt[0] = signcvt( p->rounded_amt_sign);
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( SATHead[ HeadNo ]->rounded_amt, AMT_DECPLACES );
#endif
   }
   if ( ! EMPTY_FIELD( p->rounded_off_amt))
   {
      CPYFXLFXL( SATHead[ HeadNo ]->rounded_off_amt, p->rounded_off_amt, FT_NUMBER );
      SATHead[ HeadNo ]->rounded_off_amt[0] = signcvt( p->rounded_off_amt_sign);
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( SATHead[ HeadNo ]->rounded_off_amt, AMT_DECPLACES );
#endif
   }
   CPYFXLFXL( SATHead[ HeadNo ]->credit_promotion_id, p->credit_promotion_id, FT_NUMBER );
   CPYFXLFXL( SATHead[ HeadNo ]->ref_no25, p->ref_no25, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->ref_no26, p->ref_no26, FT_VARCHAR );
   CPYFXLFXL( SATHead[ HeadNo ]->ref_no27, p->ref_no27, FT_VARCHAR );
   /* comments */
   SATHead[ HeadNo ]->newline[ 0 ] = '\n';

   ++HeadNo;

   return( TRUE );
}


/*-------------------------------------------------------------------------
setErrorSATHead() sets the error_ind to yes for the last SATHead.
-------------------------------------------------------------------------*/
void setErrorSATHead( void )
{
   if (HeadNo > 0)
      CPYSTRFXL( SATHead[ HeadNo - 1 ]->error_ind, YSNO_Y, FT_VARCHAR);
}


/*-------------------------------------------------------------------------
fmtSACustomer() formats the global SA_CUSTOMER record from the existing
transactions data.
-------------------------------------------------------------------------*/
int fmtSACustomer( RTL_TCUST *p )
{
   char *function = "fmtSACustomer";
   SA_CUST **t;
   long i;

   /* Allocate more space as needed for the transactions */
   if ( CustNo >= UsedTranCusts )
   {
      t = (SA_CUST **)realloc( SACust, (UsedTranCusts + ALLOCTRANCUSTS) *
                                       sizeof( SA_CUST *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SACust" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SACust = t;
      for (i = UsedTranCusts; i < (UsedTranCusts + ALLOCTRANCUSTS); i++)
         SACust[ i ] = NULL;
      UsedTranCusts += ALLOCTRANCUSTS;
   }
   if (SACust[ CustNo ] == NULL)
   {
      SACust[ CustNo ] = (SA_CUST *)malloc( sizeof( SA_CUST));
      if (SACust[ CustNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SACust record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SACust[ CustNo ]), ' ', sizeof( SA_CUST ) );

   CPYSTRFXL( SACust[ CustNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   CPYFXLFXL( SACust[ CustNo ]->cust_id, p->cust_id, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->cust_id_type, p->cust_id_type, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->name, p->cust_name, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->addr1, p->addr1, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->addr2, p->addr2, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->city, p->city, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->state, p->state, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->postal_code, p->postalcode, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->country, p->country, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->home_phone, p->home_phone, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->work_phone, p->work_phone, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->e_mail, p->email, FT_VARCHAR );
   CPYFXLFXL( SACust[ CustNo ]->birthdate, p->birthdate, FT_DATE );
   SACust[ CustNo ]->newline[ 0 ] = '\n';
   CPYSTRFXL( SACust[ CustNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SACust[ CustNo ]->day, Day, FT_NUMBER );

   ++CustNo;
   return( TRUE );
}

/*-------------------------------------------------------------------------
fmtSACAtt() formats the global SA_CATT record from the existing
transactions data.
-------------------------------------------------------------------------*/
int fmtSACAtt( RTL_CATT *p, int attribseq )
{
   char *function = "fmtSACAtt";
   SA_CATT **t;
   long i;
   char s[ NULL_LITTLE_SEQ_NO ];

   /* Allocate more space as needed for the transactions */
   if ( CAttNo >= UsedTranCAtts )
   {
      t = (SA_CATT **)realloc( SACAtt, (UsedTranCAtts + ALLOCTRANCATTS) *
                                       sizeof( SA_CATT *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SACAtt" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SACAtt = t;
      for (i = UsedTranCAtts; i < (UsedTranCAtts + ALLOCTRANCATTS); i++)
         SACAtt[ i ] = NULL;
      UsedTranCAtts += ALLOCTRANCATTS;
   }
   if (SACAtt[ CAttNo ] == NULL)
   {
      SACAtt[ CAttNo ] = (SA_CATT *)malloc( sizeof( SA_CATT));
      if (SACAtt[ CAttNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SACAtt record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SACAtt[ CAttNo ]), ' ', sizeof( SA_CATT ) );

   CPYSTRFXL( SACAtt[ CAttNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, attribseq );
   CPYSTRFXL( SACAtt[ CAttNo ]->attrib_seq_no, s, FT_NUMBER );
   CPYFXLFXL( SACAtt[ CAttNo ]->attrib_type, p->attrib_type, FT_VARCHAR );
   CPYFXLFXL( SACAtt[ CAttNo ]->attrib_value, p->attrib_value, FT_VARCHAR );
   SACAtt[ CAttNo ]->newline[ 0 ] = '\n';
   CPYSTRFXL( SACAtt[ CAttNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SACAtt[ CAttNo ]->day, Day, FT_NUMBER );

   ++CAttNo;
   return( TRUE );
}


/*-------------------------------------------------------------------------
fmtSATItem() formats the global SA_TITEM record from the existing
transactions data.
-------------------------------------------------------------------------*/
int fmtSATItem( RTL_TITEM *p, int itemseq, pt_item_uom *uom )
{
   char *function = "fmtSATItem";
   char s[ NULL_LITTLE_SEQ_NO ];
   SA_TITEM **t;
   long i;

   /* Allocate more space as needed for the transactions */
   if ( ItemNo >= UsedTranItems )
   {
      t = (SA_TITEM **)realloc( SATItem, (UsedTranItems + ALLOCTRANITEMS) *
                                         sizeof( SA_TITEM *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATItem" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SATItem = t;
      for (i = UsedTranItems; i < (UsedTranItems + ALLOCTRANITEMS); i++)
         SATItem[ i ] = NULL;
      UsedTranItems += ALLOCTRANITEMS;
   }
   if (SATItem[ ItemNo ] == NULL)
   {
      SATItem[ ItemNo ] = (SA_TITEM *)malloc( sizeof( SA_TITEM));
      if (SATItem[ ItemNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATItem record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SATItem[ ItemNo ]), ' ', sizeof( SA_TITEM ) );


   CPYSTRFXL( SATItem[ ItemNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, itemseq );
   CPYSTRFXL( SATItem[ ItemNo ]->item_seq_no, s, FT_NUMBER );
   CPYFXLFXL( SATItem[ ItemNo ]->item_status, p->item_status, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->item_type, p->item_type, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->item, p->item, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->ref_item, p->ref_item, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->non_merch_item, p->non_merch_item, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->voucher_no, p->voucher_no, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->dept, p->dept, FT_NUMBER );
   CPYFXLFXL( SATItem[ ItemNo ]->class, p->class, FT_NUMBER );
   CPYFXLFXL( SATItem[ ItemNo ]->subclass, p->subclass, FT_NUMBER );
   if ( !EMPTY_FIELD(p->qty))
   {
      CPYFXLFXL( SATItem[ ItemNo ]->qty, p->qty, FT_NUMBER );
      SATItem[ ItemNo ]->qty[0] = signcvt( p->qty_sign);
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATItem[ ItemNo ]->qty, QTY_DECPLACES );
      #endif

   }
   if ( !EMPTY_FIELD(uom->standard_qty))
   {
      CPYFXLFXL( SATItem[ ItemNo ]->as_standard_qty, uom->standard_qty, FT_NUMBER );
      SATItem[ ItemNo ]->as_standard_qty[0] = signcvt( p->qty_sign);
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATItem[ ItemNo ]->as_standard_qty, QTY_DECPLACES );
      #endif
   }
   if ( !EMPTY_FIELD(p->unit_retail))
   {
      CPYFXLFXL( SATItem[ ItemNo ]->unit_retail, p->unit_retail, FT_NUMBER );
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATItem[ ItemNo ]->unit_retail, AMT_DECPLACES );
      #endif
   }
   CPYFXLFXL( SATItem[ ItemNo ]->unit_retail_vat_incl, uom->class_vat_ind, FT_VARCHAR );
   if ( !EMPTY_FIELD(uom->standard_unit_retail))
   {
      CPYFXLFXL( SATItem[ ItemNo ]->as_standard_unit_retail, uom->standard_unit_retail, FT_NUMBER );
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATItem[ ItemNo ]->as_standard_unit_retail, AMT_DECPLACES );
      #endif
   }
   if ( ! EMPTY_FIELD( p->override_reason) )
   {
      CPYFXLFXL( SATItem[ ItemNo ]->override_reason, p->override_reason, FT_VARCHAR );
      CPYFXLFXL( SATItem[ ItemNo ]->orig_unit_retail, p->orig_unit_retail, FT_NUMBER );
      CPYFXLFXL( SATItem[ ItemNo ]->standard_orig_unit_retail, uom->standard_orig_unit_retail, FT_NUMBER );
#ifdef ADD_DECIMAL_POINTS
      FIX_DP( SATItem[ ItemNo ]->orig_unit_retail, AMT_DECPLACES );
      FIX_DP( SATItem[ ItemNo ]->standard_orig_unit_retail, AMT_DECPLACES );
#endif
   }
   CPYFXLFXL( SATItem[ ItemNo ]->tax_ind, p->tax_ind, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->ref_no5, p->ref_no5, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->ref_no6, p->ref_no6, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->ref_no7, p->ref_no7, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->ref_no8, p->ref_no8, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->item_swiped_ind, p->item_swiped_ind, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->as_selling_uom, p->selling_uom, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->as_standard_uom, uom->standard_uom, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->as_drop_ship_ind, p->drop_ship_ind, FT_VARCHAR );
   CPYSTRFXL( SATItem[ ItemNo ]->error_ind, YSNO_N, FT_VARCHAR);
   CPYFXLFXL( SATItem[ ItemNo ]->pump, p->pump, FT_VARCHAR );
   /* lookup fill waste fields */
   if (! EMPTY_FIELD( p->item))
   {
      WASTEDAT *w;

      if ( ( w = waste_lookup( p->item ) ) != NULL )
      {
         CPYFXLFXL( SATItem[ ItemNo ]->waste_type, w->waste_type, FT_VARCHAR );
         CPYFXLFXL( SATItem[ ItemNo ]->waste_pct, w->waste_pct, FT_NUMBER );
      }
   }

   CPYFXLFXL( SATItem[ ItemNo ]->return_reason_code, p->return_reason_code, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->salesperson, p->salesperson, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->expiration_date, p->expiration_date, FT_VARCHAR );
   if ( !EMPTY_FIELD(p->uom_qty))
   {
       CPYFXLFXL( SATItem[ ItemNo ]->uom_qty, p->uom_qty, FT_NUMBER );
       SATItem[ ItemNo ]->uom_qty[0] = signcvt( p->qty_sign);
       #ifdef ADD_DECIMAL_POINTS
          FIX_DP( SATItem[ ItemNo ]->uom_qty, QTY_DECPLACES );
       #endif
   }
   CPYFXLFXL( SATItem[ ItemNo ]->catchweight_ind, p->catchweight_ind, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->selling_item, p->selling_item, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->cust_ord_line_no, p->cust_ord_line_no, FT_NUMBER );
   CPYFXLFXL( SATItem[ ItemNo ]->media_id, p->media_id, FT_NUMBER );

   CPYFXLFXL( SATItem[ ItemNo ]->total_igtax_amt, uom->total_igtax_amt, FT_NUMBER );
   SATItem[ ItemNo ]->total_igtax_amt[0] = signcvt( p->qty_sign);
   #ifdef ADD_DECIMAL_POINTS
      FIX_DP( SATItem[ ItemNo ]->total_igtax_amt, AMT_DECPLACES);
   #endif
   CPYFXLFXL( SATItem[ ItemNo ]->unique_id, p->unique_id, FT_VARCHAR );

   CPYSTRFXL( SATItem[ ItemNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SATItem[ ItemNo ]->day, Day, FT_NUMBER );
   CPYFXLFXL( SATItem[ ItemNo ]->cust_order_no, p->cust_order_no, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->cust_order_date, p->cust_order_date, FT_DATE );
   CPYFXLFXL( SATItem[ ItemNo ]->fulfill_order_no, p->fulfill_order_no, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->no_inv_ret_ind, p->no_inv_ret_ind, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->sales_type, p->sales_type, FT_VARCHAR );
   CPYFXLFXL( SATItem[ ItemNo ]->return_wh, p->return_wh, FT_NUMBER );
   CPYFXLFXL( SATItem[ ItemNo ]->return_disp, p->return_disp, FT_VARCHAR );
   SATItem[ ItemNo ]->newline[ 0 ] = '\n';

   ++ItemNo;
   return( TRUE );
}


/*-------------------------------------------------------------------------
setErrorSATItem() sets the error_ind to yes for the last SATItem.
-------------------------------------------------------------------------*/
void setErrorSATItem( void )
{
   if (ItemNo > 0)
      CPYSTRFXL( SATItem[ ItemNo - 1 ]->error_ind, YSNO_Y, FT_VARCHAR);
}


/*-------------------------------------------------------------------------
fmtSATDisc() formats the global SA_TDISC record from the existing
transactions data.
-------------------------------------------------------------------------*/
int fmtSATDisc( RTL_IDISC *p, int itemseq, int discseq, pt_disc_uom *disc_uom )
{
   char *function = "fmtSATDisc";
   char s[ NULL_LITTLE_SEQ_NO ];
   SA_TDISC **t;
   long i;

   /* Allocate more space as needed for the transactions */
   if ( DiscNo >= UsedTranDiscs )
   {
      t = (SA_TDISC **)realloc( SATDisc, (UsedTranDiscs + ALLOCTRANDISCS) *
                                         sizeof( SA_TDISC *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATDisc" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SATDisc = t;
      for (i = UsedTranDiscs; i < (UsedTranDiscs + ALLOCTRANDISCS); i++)
         SATDisc[ i ] = NULL;
      UsedTranDiscs += ALLOCTRANDISCS;
   }
   if (SATDisc[ DiscNo ] == NULL)
   {
      SATDisc[ DiscNo ] = (SA_TDISC *)malloc( sizeof( SA_TDISC));
      if (SATDisc[ DiscNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATDisc record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SATDisc[ DiscNo ]), ' ', sizeof( SA_TDISC ) );

   CPYSTRFXL( SATDisc[ DiscNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, itemseq );
   CPYSTRFXL( SATDisc[ DiscNo ]->item_seq_no, s, FT_NUMBER );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, discseq );
   CPYSTRFXL( SATDisc[ DiscNo ]->disc_seq_no, s, FT_NUMBER );
   CPYFXLFXL( SATDisc[ DiscNo ]->rms_promo_type, p->rms_promo_type, FT_VARCHAR );
   CPYFXLFXL( SATDisc[ DiscNo ]->promotion, p->disc_ref_no, FT_VARCHAR );
   CPYFXLFXL( SATDisc[ DiscNo ]->disc_type, p->disc_type, FT_VARCHAR );
   CPYFXLFXL( SATDisc[ DiscNo ]->coupon_no, p->coupon_no, FT_VARCHAR );
   CPYFXLFXL( SATDisc[ DiscNo ]->coupon_ref_no, p->coupon_ref_no, FT_VARCHAR );
   if( !EMPTY_FIELD( p->qty ))
   {
      CPYFXLFXL( SATDisc[ DiscNo ]->qty, p->qty, FT_NUMBER );
      SATDisc[ DiscNo ]->qty[0] = signcvt( p->qty_sign);
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATDisc[ DiscNo ]->qty, QTY_DECPLACES );
      #endif
   }
   if( !EMPTY_FIELD( disc_uom->standard_qty ))
   {
      CPYFXLFXL( SATDisc[ DiscNo ]->as_standard_qty, disc_uom->standard_qty, FT_NUMBER );
      SATDisc[ DiscNo ]->qty[0] = signcvt( p->qty_sign);
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATDisc[ DiscNo ]->as_standard_qty, QTY_DECPLACES );
      #endif
   }
   if( !EMPTY_FIELD( p->unit_disc_amt ))
   {
      CPYFXLFXL( SATDisc[ DiscNo ]->unit_disc_amt, p->unit_disc_amt, FT_NUMBER );
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATDisc[ DiscNo ]->unit_disc_amt, AMT_DECPLACES );
      #endif
   }
   if( !EMPTY_FIELD( disc_uom->standard_unit_disc_amt ))
   {
      CPYFXLFXL( SATDisc[ DiscNo ]->as_standard_unit_disc_amt, disc_uom->standard_unit_disc_amt, FT_NUMBER );
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATDisc[ DiscNo ]->as_standard_unit_disc_amt, AMT_DECPLACES );
      #endif
   }
   CPYFXLFXL( SATDisc[ DiscNo ]->ref_no13, p->ref_no13, FT_VARCHAR );
   CPYFXLFXL( SATDisc[ DiscNo ]->ref_no14, p->ref_no14, FT_VARCHAR );
   CPYFXLFXL( SATDisc[ DiscNo ]->ref_no15, p->ref_no15, FT_VARCHAR );
   CPYFXLFXL( SATDisc[ DiscNo ]->ref_no16, p->ref_no16, FT_VARCHAR );
   CPYSTRFXL( SATDisc[ DiscNo ]->error_ind, YSNO_N, FT_VARCHAR);
   if ( !EMPTY_FIELD(p->uom_qty))
   {
       CPYFXLFXL( SATDisc[ DiscNo ]->uom_qty, p->uom_qty, FT_NUMBER );
       SATDisc[ DiscNo ]->uom_qty[0] = signcvt( p->qty_sign);
       #ifdef ADD_DECIMAL_POINTS
          FIX_DP( SATDisc[ DiscNo ]->uom_qty, QTY_DECPLACES );
       #endif
   }
   CPYSTRFXL( SATDisc[ DiscNo ]->catchweight_ind, p->catchweight_ind, FT_VARCHAR );
   CPYFXLFXL( SATDisc[ DiscNo ]->promo_comp, p->promo_comp, FT_NUMBER );
   CPYSTRFXL( SATDisc[ DiscNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SATDisc[ DiscNo ]->day, Day, FT_NUMBER );
   SATDisc[ DiscNo ]->newline[ 0 ] = '\n';

   ++DiscNo;
   return( TRUE );
}


/*-------------------------------------------------------------------------
setErrorSATDisc() sets the error_ind to yes for the last SATDisc.
-------------------------------------------------------------------------*/
void setErrorSATDisc( void )
{
   if (DiscNo > 0)
      CPYSTRFXL( SATDisc[ DiscNo - 1 ]->error_ind, YSNO_Y, FT_VARCHAR);
}


/*-------------------------------------------------------------------------
fmtSAIGtax() formats the global SA_IGTAX record from the existing
transactions data.
-------------------------------------------------------------------------*/
int fmtSAIGtax( RTL_IGTAX *p, int itemseq, int igtaxseq, pt_igtax_tia *igtax_tia )
{
   char *function = "fmtSAIGtax";
   char s[ NULL_LITTLE_SEQ_NO ];
   SA_IGTAX **t;
   long i;

   /* Allocate more space as needed for the transactions */
   if ( IgtaxNo >= UsedTranIgtaxs )
   {
      t = (SA_IGTAX **)realloc( SAIGtax, (UsedTranIgtaxs + ALLOCTRANIGTAXS) *
                                         sizeof( SA_IGTAX *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SAIGtax" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SAIGtax = t;
      for (i = UsedTranIgtaxs; i < (UsedTranIgtaxs + ALLOCTRANIGTAXS); i++)
         SAIGtax[ i ] = NULL;
      UsedTranIgtaxs += ALLOCTRANIGTAXS;
   }
   if (SAIGtax[ IgtaxNo ] == NULL)
   {
      SAIGtax[ IgtaxNo ] = (SA_IGTAX *)malloc( sizeof( SA_IGTAX));
      if (SAIGtax[ IgtaxNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SAIGtax record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SAIGtax[ IgtaxNo ]), ' ', sizeof( SA_IGTAX ) );
   CPYSTRFXL( SAIGtax[ IgtaxNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, itemseq );
   CPYSTRFXL( SAIGtax[ IgtaxNo ]->item_seq_no, s, FT_NUMBER );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, igtaxseq );
   CPYSTRFXL( SAIGtax[ IgtaxNo ]->igtax_seq_no, s, FT_NUMBER );
   CPYFXLFXL( SAIGtax[ IgtaxNo ]->tax_authority, p->tax_authority, FT_VARCHAR );
   CPYFXLFXL( SAIGtax[ IgtaxNo ]->igtax_code, p->igtax_code, FT_VARCHAR );

   if( !EMPTY_FIELD( p->igtax_rate ))
   {
       CPYFXLFXL( SAIGtax[ IgtaxNo ]->igtax_rate, p->igtax_rate, FT_NUMBER );
     #ifdef ADD_DECIMAL_POINTS
        FIX_DP( SAIGtax[ IgtaxNo ]->igtax_rate, AMT_DECPLACES );
     #endif
   }

   if(!(EMPTY_FIELD( p->total_igtax_amt ) && EMPTY_FIELD( igtax_tia->TotalIgtaxAmt )))
   {
      CPYFXLFXL( SAIGtax[ IgtaxNo ]->total_igtax_amt, igtax_tia->TotalIgtaxAmt, FT_NUMBER );
      SAIGtax[ IgtaxNo ]->total_igtax_amt[0] = signcvt( p->igtax_amt_sign);
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SAIGtax[ IgtaxNo ]->total_igtax_amt, (AMT_DECPLACES));
      #endif
   }

   CPYSTRFXL( SAIGtax[ IgtaxNo ]->error_ind, YSNO_N, FT_VARCHAR);
   CPYFXLFXL( SAIGtax[ IgtaxNo ]->ref_no21, p->ref_no21, FT_VARCHAR);
   CPYFXLFXL( SAIGtax[ IgtaxNo ]->ref_no22, p->ref_no22, FT_VARCHAR);
   CPYFXLFXL( SAIGtax[ IgtaxNo ]->ref_no23, p->ref_no23, FT_VARCHAR);
   CPYFXLFXL( SAIGtax[ IgtaxNo ]->ref_no24, p->ref_no24, FT_VARCHAR);

   CPYSTRFXL( SAIGtax[ IgtaxNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SAIGtax[ IgtaxNo ]->day, Day, FT_NUMBER );
   SAIGtax[ IgtaxNo ]->newline[ 0 ] = '\n';

   ++IgtaxNo;
   return( TRUE );
}

/*-------------------------------------------------------------------------
setErrorSAIGtax() sets the error_ind to yes for the last SAIGtax.
-------------------------------------------------------------------------*/
void setErrorSAIGtax( void )
{
   if (IgtaxNo > 0)
      CPYSTRFXL( SAIGtax[ IgtaxNo - 1 ]->error_ind, YSNO_Y, FT_VARCHAR);
}


/*-------------------------------------------------------------------------
fmtSATTax() formats the global SA_TTAX record from the existing
transactions data.
-------------------------------------------------------------------------*/
int fmtSATTax( RTL_TTAX *p, int taxseq )
{
   char *function = "fmtSATTax";
   char s[ NULL_LITTLE_SEQ_NO ];
   SA_TTAX **t;
   long i;

   /* Allocate more space as needed for the transactions */
   if ( TaxNo >= UsedTranTaxs )
   {
      t = (SA_TTAX **)realloc( SATTax, (UsedTranTaxs + ALLOCTRANTAXS) *
                               sizeof( SA_TTAX *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATTax" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SATTax = t;
      for (i = UsedTranTaxs; i < (UsedTranTaxs + ALLOCTRANTAXS); i++)
         SATTax[ i ] = NULL;
      UsedTranTaxs += ALLOCTRANTAXS;
   }
   if (SATTax[ TaxNo ] == NULL)
   {
      SATTax[ TaxNo ] = (SA_TTAX *)malloc( sizeof( SA_TTAX));
      if (SATTax[ TaxNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATTax record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SATTax[ TaxNo ]), ' ', sizeof( SA_TTAX ) );

   CPYSTRFXL( SATTax[ TaxNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   CPYFXLFXL( SATTax[ TaxNo ]->tax_code, p->tax_code, FT_VARCHAR );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, taxseq );
   CPYSTRFXL( SATTax[ TaxNo ]->tax_seq_no, s, FT_NUMBER );
   CPYFXLFXL( SATTax[ TaxNo ]->tax_amt, p->tax_amt, FT_NUMBER );
   SATTax[ TaxNo ]->tax_amt[0] = signcvt( p->tax_amt_sign);
#ifdef ADD_DECIMAL_POINTS
   FIX_DP( SATTax[ TaxNo ]->tax_amt, AMT_DECPLACES );
#endif
   CPYSTRFXL( SATTax[ TaxNo ]->error_ind, YSNO_N, FT_VARCHAR);
   CPYFXLFXL( SATTax[ TaxNo ]->ref_no17, p->ref_no17, FT_VARCHAR );
   CPYFXLFXL( SATTax[ TaxNo ]->ref_no18, p->ref_no18, FT_VARCHAR );
   CPYFXLFXL( SATTax[ TaxNo ]->ref_no19, p->ref_no19, FT_VARCHAR );
   CPYFXLFXL( SATTax[ TaxNo ]->ref_no20, p->ref_no20, FT_VARCHAR );
   CPYSTRFXL( SATTax[ TaxNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SATTax[ TaxNo ]->day, Day, FT_NUMBER );
   SATTax[ TaxNo ]->newline[ 0 ] = '\n';

   ++TaxNo;
   return( TRUE );
}


/*-------------------------------------------------------------------------
setErrorSATTax() sets the error_ind to yes for the last SATTax.
-------------------------------------------------------------------------*/
void setErrorSATTax( void )
{
   if (TaxNo > 0)
      CPYSTRFXL( SATTax[ TaxNo - 1 ]->error_ind, YSNO_Y, FT_VARCHAR);
}


/*-------------------------------------------------------------------------
fmtSATPymt() formats the global SA_TPYMT record from the existing
transactions data - Due File Changes
-------------------------------------------------------------------------*/
int fmtSATPymt( RTL_TPYMT *p, int pymtseq )
{
   char *function = "fmtSATPymt";
   char s[ NULL_LITTLE_SEQ_NO ];
   SA_TPYMT **t;
   long i;

   /* Allocate more space as needed for the transactions */
   if ( PymtNo >= UsedTranPymts )
   {
      t = (SA_TPYMT **)realloc( SATPymt, (UsedTranPymts + ALLOCTRANPYMTS) *
                               sizeof( SA_TPYMT *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATPymt" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SATPymt = t;
      for (i = UsedTranPymts; i < (UsedTranPymts + ALLOCTRANPYMTS); i++)
         SATPymt[ i ] = NULL;
      UsedTranPymts += ALLOCTRANPYMTS;
   }
   if (SATPymt[ PymtNo ] == NULL)
   {
      SATPymt[ PymtNo ] = (SA_TPYMT *)malloc( sizeof( SA_TPYMT));
      if (SATPymt[ PymtNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATPymt record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SATPymt[ PymtNo ]), ' ', sizeof( SA_TPYMT ) );
   CPYSTRFXL( SATPymt[ PymtNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, pymtseq );
   CPYSTRFXL( SATPymt[ PymtNo ]->payment_seq_no, s, FT_NUMBER );
   CPYFXLFXL( SATPymt[ PymtNo ]->payment_amt, p->payment_amt, FT_NUMBER );
   SATPymt[ PymtNo ]->payment_amt[0] = signcvt( p->payment_amt_sign);
#ifdef ADD_DECIMAL_POINTS
   FIX_DP( SATPymt[ PymtNo ]->payment_amt, AMT_DECPLACES );
#endif
   CPYSTRFXL( SATPymt[ PymtNo ]->error_ind, YSNO_N, FT_VARCHAR);
   CPYSTRFXL( SATPymt[ PymtNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SATPymt[ PymtNo ]->day, Day, FT_NUMBER );
   SATPymt[ PymtNo ]->newline[ 0 ] = '\n';

   ++PymtNo;
   return( TRUE );
}


/*-------------------------------------------------------------------------
setErrorSATPymt() sets the error_ind to yes for the last SATPymt.
-------------------------------------------------------------------------*/
void setErrorSATPymt( void )
{
   if (PymtNo > 0)
      CPYSTRFXL( SATPymt[ PymtNo - 1 ]->error_ind, YSNO_Y, FT_VARCHAR);
}


/*-------------------------------------------------------------------------
fmtSATTend() formats the global SA_TTEND record from the existing
transactions data.
-------------------------------------------------------------------------*/
int fmtSATTend( RTL_TTEND *p, int tendseq )
{
   char *function = "fmtSATTend";
   char s[ NULL_LITTLE_SEQ_NO ];
   SA_TTEND **t;
   long i;

   /* Allocate more space as needed for the transactions */
   if ( TendNo >= UsedTranTends )
   {
      t = (SA_TTEND **)realloc( SATTend, (UsedTranTends + ALLOCTRANTENDS) *
                                         sizeof( SA_TTEND *));
      if (t == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATTend" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
      SATTend = t;
      for (i = UsedTranTends; i < (UsedTranTends + ALLOCTRANTENDS); i++)
         SATTend[ i ] = NULL;
      UsedTranTends += ALLOCTRANTENDS;
   }
   if (SATTend[ TendNo ] == NULL)
   {
      SATTend[ TendNo ] = (SA_TTEND *)malloc( sizeof( SA_TTEND));
      if (SATTend[ TendNo ] == NULL)
      {
         WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                      "Cannot allocate space for SATTend record" );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
      }
   }

   memset( (char *)(SATTend[ TendNo ]), ' ', sizeof( SA_TTEND ) );

   CPYSTRFXL( SATTend[ TendNo ]->tran_seq_no, TranSeqNo, FT_NUMBER );
   sprintf( s, "%0*d", LEN_LITTLE_SEQ_NO, tendseq );
   CPYSTRFXL( SATTend[ TendNo ]->tender_seq_no, s, FT_NUMBER );
   CPYFXLFXL( SATTend[ TendNo ]->tender_type_group, p->tender_type_group, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->tender_type_id, p->tender_type_id, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->tender_amt, p->tender_amt, FT_NUMBER );
   SATTend[ TendNo ]->tender_amt[0] = signcvt( p->tender_amt_sign);
#ifdef ADD_DECIMAL_POINTS
   FIX_DP( SATTend[ TendNo ]->tender_amt, AMT_DECPLACES );
#endif
   CPYFXLFXL( SATTend[ TendNo ]->cc_no, p->cc_no, FT_NUMBER );
   CPYFXLFXL( SATTend[ TendNo ]->cc_exp_date, p->cc_exp_date, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->cc_auth_no, p->cc_auth_no, FT_NUMBER );
   CPYFXLFXL( SATTend[ TendNo ]->cc_auth_src, p->cc_auth_src, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->cc_entry_mode, p->cc_entry_mode, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->cc_cardholder_verf, p->cc_cardholder_verf, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->cc_term_id, p->cc_terminal_id, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->cc_spec_cond, p->cc_special_cond, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->voucher_no, p->voucher_no, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->coupon_no, p->coupon_no, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->coupon_ref_no, p->coupon_ref_no, FT_VARCHAR );

   if (!CMPFXLSTR( p->tender_type_group, TENT_CHECK))
   {
      /* Nullify the check_acct_no and check_no if the tender type group is not CHECK */
      CLRTOSPACES( p->check_acct_no );
      CPYFXLFXL( SATTend[ TendNo ]->check_acct_no, p->check_acct_no, FT_VARCHAR );
      CLRTOSPACES( p->check_no );
      CPYFXLFXL( SATTend[ TendNo ]->check_no, p->check_no, FT_NUMBER );
   }
   else
   {
      CPYFXLFXL( SATTend[ TendNo ]->check_acct_no, p->check_acct_no, FT_VARCHAR );
      CPYFXLFXL( SATTend[ TendNo ]->check_no, p->check_no, FT_NUMBER );
   }

   CPYFXLFXL( SATTend[ TendNo ]->identi_method, p->identi_method, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->identi_id, p->identi_id, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->orig_currency, p->orig_currency, FT_VARCHAR );
   if( !EMPTY_FIELD( p->orig_curr_amt ))
   {
      CPYFXLFXL( SATTend[ TendNo ]->orig_curr_amt, p->orig_curr_amt, FT_NUMBER );
      SATTend[ TendNo ]->orig_curr_amt[0] = signcvt( p->tender_amt_sign);
      #ifdef ADD_DECIMAL_POINTS
         FIX_DP( SATTend[ TendNo ]->orig_curr_amt, AMT_DECPLACES );
      #endif
   }
   CPYFXLFXL( SATTend[ TendNo ]->ref_no9, p->ref_no9, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->ref_no10, p->ref_no10, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->ref_no11, p->ref_no11, FT_VARCHAR );
   CPYFXLFXL( SATTend[ TendNo ]->ref_no12, p->ref_no12, FT_VARCHAR );
   CPYSTRFXL( SATTend[ TendNo ]->error_ind, YSNO_N, FT_VARCHAR);
   CPYSTRFXL( SATTend[ TendNo ]->store, Location, FT_NUMBER );
   CPYFXLFXL( SATTend[ TendNo ]->day, Day, FT_NUMBER );
   SATTend[ TendNo ]->newline[ 0 ] = '\n';

   ++TendNo;
   return( TRUE );
}


/*-------------------------------------------------------------------------
setErrorSATTend() sets the error_ind to yes for the last SATTend.
-------------------------------------------------------------------------*/
void setErrorSATTend( void )
{
   if (TendNo > 0)
      CPYSTRFXL( SATTend[ TendNo - 1 ]->error_ind, YSNO_Y, FT_VARCHAR);
}


/*-------------------------------------------------------------------------
fmtSAMissTran() writes amissing transaction record to the missing
transactions file.
-------------------------------------------------------------------------*/
int fmtSAMissTran( char *is_reg, long il_tran_no )
{
   SA_MISSTRAN mtr;
   char ls_tran_no[ NULL_TRAN_NO ];
   char *mtsn = NULL;
   int rv = TRUE;

   memset( &mtr, ' ', sizeof( SA_MISSTRAN ) );
   if ( (mtsn = nextMissingTranSeqNo(SysOpt.s_table_owner)) != NULL)
      CPYSTRFXL( mtr.miss_tran_seq_no, mtsn, FT_NUMBER );
   CPYSTRFXL( mtr.store_day_seq_no, StoreDaySeqNo, LEN_BIG_SEQ_NO );
   if (CMPFXLSTR(TranNoGen, STRG_R))
   {
      /* register id relevant only if tranno's */
      /* are sequential from the register */
      CPYSTRFXL(mtr.pos_register, is_reg, FT_VARCHAR);
   }
   sprintf( ls_tran_no, "%0*d", LEN_TRAN_NO, il_tran_no );
   CPYSTRFXL( mtr.tran_no, ls_tran_no, FT_VARCHAR );
   CPYSTRFXL( mtr.status, SAMS_M, FT_VARCHAR );
   CPYSTRFXL( mtr.rtlog_orig_sys, Rtlog_Orig_Sys, FT_VARCHAR );
   mtr.newline[ 0 ] = '\n';
   if ( ! putrec( (char *)&mtr, SAMissTranFile ) )
   {
      rv = FALSE;
      ExitCode = EXIT_FAILURE;
   }
   return( rv );
}


/*-------------------------------------------------------------------------
WrBadTranError() writes an error record to the bad tran file after looking
up the error description and recommended solution.
-------------------------------------------------------------------------*/
int WrBadTranError(
   const char *err,
   char       *tran_seq_no,
   int         seq_no1,
   int         seq_no2,
   const char *rec_type,
   char       *orig_value,
   int         orig_value_len)
{
   SA_ERROR r;
   char *esn;
   char seq_no[NULL_LITTLE_SEQ_NO];
   int rv = TRUE;

   memset( &r, ' ', sizeof( r));

   if ( (esn = nextErrorSeqNo(SysOpt.s_table_owner)) != NULL )
      CPYSTRFXL( r.error_seq_no, esn, FT_NUMBER);
   CPYFXLFXL( r.store_day_seq_no, StoreDaySeqNo, FT_NUMBER);
   /* bal_group_seq_no */
   /* total_seq_no */
   if ((tran_seq_no != NULL) && (tran_seq_no[0] != '\0'))
      CPYSTRFXL( r.tran_seq_no, tran_seq_no, FT_NUMBER);
   CPYSTRFXL( r.error_code, err, FT_VARCHAR);
   if (seq_no1 != -1)
   {
      sprintf( seq_no, "%0*d", LEN_LITTLE_SEQ_NO, seq_no1);
      CPYSTRFXL( r.seq_no1, seq_no, FT_NUMBER);
   }
   if (seq_no2 != -1)
   {
      sprintf( seq_no, "%0*d", LEN_LITTLE_SEQ_NO, seq_no2);
      CPYSTRFXL( r.seq_no2, seq_no, FT_NUMBER);
   }
   CPYSTRFXL( r.rec_type, rec_type, FT_VARCHAR);
   CPYSTRFXL( r.store_override_ind, YSNO_N, FT_VARCHAR );
   CPYSTRFXL( r.hq_override_ind, YSNO_N, FT_VARCHAR );
   CPYSTRFXL( r.update_id, "TLOG", FT_VARCHAR );
   CPYSTRFXL( r.update_datetime, SysDate, FT_DATE );
   CPYSTRFXL( r.store, Location, FT_NUMBER );
   CPYFXLFXL( r.day, Day, FT_NUMBER );
   fldcpy( r.orig_value, sizeof( r.orig_value), orig_value, orig_value_len, FT_VARCHAR);
   r.newline[ 0 ] = '\n';

   if ( ! putrec( (char *)&r, SAErrorFile ) )
      rv = FALSE;

   return( rv );
}


/*-------------------------------------------------------------------------
WrOutputData() writes the current transaction to the SQL load files.
-------------------------------------------------------------------------*/
int WrOutputData( void )
{
   int i, rv;

   rv = TRUE;

   VERBOSE( "INFO: Outputing transaction\n" );

   if ( ! putrec( (char *)SATHead[0], SATHeadFile ) )
   {
      VERBOSE( "ERROR: Write of SATHead failed\n" );
      rv = FALSE;
   }

   if ( CustNo > 0 )
   {
      if ( ! putrec( (char *)SACust[0], SACustomerFile ) )
      {
         VERBOSE( "ERROR: Write of SACustomer failed\n" );
         rv = FALSE;
      }
   }

   for ( i = 0; i < CAttNo; ++i )
      if ( ! putrec( (char *)SACAtt[ i ], SACAttFile ) )
      {
         VERBOSE( "ERROR: Write of SACAtt failed\n" );
         rv = FALSE;
      }

   for ( i = 0; i < ItemNo; ++i )
   {
      if ( ! putrec( (char *)SATItem[ i ], SATItemFile ) )
      {
         VERBOSE( "ERROR: Write of SATItem failed\n" );
         rv = FALSE;
      }
   }

   for ( i = 0; i < DiscNo; ++i )
      if ( ! putrec( (char *)SATDisc[ i ], SATDiscFile ) )
      {
         VERBOSE( "ERROR: Write of SATDisc failed\n" );
         rv = FALSE;
      }

   for ( i = 0; i < IgtaxNo; ++i )
      if ( ! putrec( (char *)SAIGtax[ i ], SAIGtaxFile ) )
      {
         VERBOSE( "ERROR: Write of SAIGtax failed\n" );
         rv = FALSE;
      }

   for ( i = 0; i < TaxNo; ++i )
      if ( ! putrec( (char *)SATTax[ i ], SATTaxFile ) )
      {
         VERBOSE( "ERROR: Write of SATTax failed\n" );
         rv = FALSE;
      }

   for ( i = 0; i < PymtNo; ++i )
      if ( ! putrec( (char *)SATPymt[ i ], SATPymtFile ) )
      {
         VERBOSE( "ERROR: Write of SATPymt failed\n" );
         rv = FALSE;
      }

   for ( i = 0; i < TendNo; ++i )
   {
      if ( ! putrec( (char *)SATTend[ i ], SATTendFile ) )
      {
         VERBOSE( "ERROR: Write of SATTend failed\n" );
         rv = FALSE;
      }
   }

   return( rv );
}


/*-------------------------------------------------------------------------
FinalOutputData() closes the temporary SQL*Ldr output files and renames
them to their permanent names.
-------------------------------------------------------------------------*/
int FinalOutputData( void )
{
   char *function = "FinalOutputData";
   int rv;
   int li_file_permission = 0664;
   char ls_new_file_name[MAX_FILE_NAME_LEN +
                         LEN_LOC +
                         LEN_DATEONLY +
                         LEN_DATETIME +
                         4 +
                         sizeof( SUFFIX)];

   /* close files */
   VERBOSE( "INFO: Closing files\n" );

   rv = TRUE;

   /* close and rename output files */
   if (SATHeadFile != NULL && fclose( SATHeadFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing thead output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SATHEAD_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SATHeadFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming thead output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
                ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SATHeadFileTmp);
   }

   if (SACustomerFile != NULL && fclose( SACustomerFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing customer output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SACUSTOMER_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SACustomerFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming customer output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
                ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SACustomerFileTmp);
   }

   if (SACAttFile != NULL && fclose( SACAttFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing customer attribute output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SACATT_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SACAttFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming customer attribute output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
              ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SACAttFileTmp);
   }

   if (SATItemFile != NULL &&  fclose( SATItemFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing titem output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SATITEM_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SATItemFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming titem output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
                ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SATItemFileTmp);
   }

   if (SATDiscFile != NULL && fclose( SATDiscFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing tdisc output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SATDISC_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SATDiscFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming tdisc output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
          if (chmod(ls_new_file_name,li_file_permission) < 0)
          {
             sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
                ls_new_file_name, strerror(errno));
             WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
             return(FALSE);
          }
      }
   }
   else
   {
      (void)unlink( SATDiscFileTmp);
   }

   if (SAIGtaxFile != NULL && fclose( SAIGtaxFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing igtax output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SAIGTAX_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SAIGtaxFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming igtax output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
               ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SAIGtaxFileTmp);
   }

   if (SATTaxFile != NULL && fclose( SATTaxFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing ttax output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SATTAX_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SATTaxFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming ttax output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
               ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SATTaxFileTmp);
   }

   if (SATPymtFile != NULL && fclose( SATPymtFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing tpymt output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SATPYMT_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SATPymtFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming tpymt output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
                ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SATPymtFileTmp);
   }

   if (SATTendFile != NULL && fclose( SATTendFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing ttend output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SATTEND_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SATTendFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming ttend output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
                ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SATTendFileTmp);
   }

   if (SAErrorFile != NULL && fclose( SAErrorFile ) != 0 )
   {
      sprintf( logmsg, "ERROR: Failure closing error output file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }
   if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
   {
      sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
              SAERROR_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
      if (rename( SAErrorFileTmp, ls_new_file_name) < 0)
      {
         sprintf( logmsg, "ERROR: Failure renaming error output file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      else
      {
         if (chmod(ls_new_file_name,li_file_permission) < 0)
         {
            sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
                ls_new_file_name, strerror(errno));
            WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
            return(FALSE);
         }
      }
   }
   else
   {
      (void)unlink( SAErrorFileTmp);
   }

   if (strcmp( SysOpt.s_check_dup_miss_tran, YSNO_Y) == 0)
   {
      if (SAMissTranFile != NULL && fclose( SAMissTranFile ) != 0 )
      {
         sprintf( logmsg, "ERROR: Failure closing missing tran file - %s", strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
      if ((strlen( Location) != 0) && (strlen( BusinessDate) != 0))
      {
         sprintf(ls_new_file_name, "%s_%s_%s_%s%s",
                 MISSTRAN_FILENAME, Location, BusinessDate, SysDate, SUFFIX);
         if (rename( SAMissTranFileTmp, ls_new_file_name) < 0)
         {
            sprintf( logmsg, "ERROR: Failure renaming missing tran output file - %s", strerror( errno) );
            WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
            ExitCode = EXIT_FAILURE;
            rv = FALSE;
         }
         else
         {
            if (chmod(ls_new_file_name,li_file_permission) < 0)
            {
               sprintf( err_data, "FILE PERMISSION CHANGE: file=%s, %s",
                   ls_new_file_name, strerror(errno));
               WRITE_ERROR(RET_FILE_ERR,function,"",err_data);
               return(FALSE);
            }
         }
      }
      else
      {
         (void)unlink( SAMissTranFileTmp);
      }

   }

   return( rv );
}
