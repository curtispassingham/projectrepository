#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  uploadsales.ksh
#
#  Desc:  UNIX shell script to upload sales information from POSU file 
#  into SVC_POSUPLD_LOAD. 
#  Thread parameter is expected to be the file extension of the file. 
#  i.e. Thread 1 = posu_file.1 
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='uploadsales.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

CTLFILE="${MMHOME}/oracle/proc/src/salesload.ctl"

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <input file> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <input file>       Input file. Can include directory path.
"
}

#-------------------------------------------------------------------------
# Function Name: GEN_CTL
# Function Name: generate control file based on file name
# Purpose      : 
#-------------------------------------------------------------------------

function GEN_CTL
{

   rm -f ${MMHOME}/log/$filename.ctl

   echo 'LOAD DATA' >> ${MMHOME}/log/$filename.ctl
   echo 'APPEND' >> ${MMHOME}/log/$filename.ctl
   echo 'INTO TABLE svc_posupld_load' >> ${MMHOME}/log/$filename.ctl
   echo '(' >> ${MMHOME}/log/$filename.ctl
   echo 'file_type         position(1:5)   char,' >> ${MMHOME}/log/$filename.ctl
   echo 'thread_val        "' $inputfile_extension '",' >> ${MMHOME}/log/$filename.ctl
   echo 'sales_process_id  "' ${salesprocess_id} '",' >> ${MMHOME}/log/$filename.ctl
   echo 'filename CONSTANT ' \'$filename\'  ',' >> ${MMHOME}/log/$filename.ctl
   echo 'line_id           position(6:15)  integer external,' >> ${MMHOME}/log/$filename.ctl
   echo 'line_seq_id       position(6:15)  integer external,' >> ${MMHOME}/log/$filename.ctl
   echo 'line_text         position(1:199) char' >> ${MMHOME}/log/$filename.ctl
   echo ')' >> ${MMHOME}/log/$filename.ctl

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_result         NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_result := 0;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`
    
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: POPULATE_POSUPLD_STATUS
# Purpose:       Initiate records to be inserted into the svc_posupld_status   
#                table based on the new POSU file uploaded. 
#-------------------------------------------------------------------------

function POPULATE_POSUPLD_STATUS
{
     fullpath=$1
     filename=${fullpath##*/}

     sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      VARIABLE GV_filename       CHAR(255);
      VARIABLE GV_salesprocess_id NUMBER;

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_filename := '$filename';
      EXEC :GV_salesprocess_id := $salesprocess_id;
      
      -- insert staging table with chunk id as 0. Actual chunking happens in salesprocess.
      WHENEVER SQLERROR EXIT ${FATAL}
      INSERT INTO svc_posupld_staging (file_type,
                                       filename,
                                       line_seq_id,
                                       thread_val,
                                       location,
                                       sales_process_id,
                                       chunk_id,
                                       thead_id,
                                       head_detl_id,
                                       ttax_id,
                                       tdetl_id,
                                       col_1,
                                       col_2,
                                       col_3,
                                       col_4,
                                       col_5,
                                       col_6,
                                       col_7,
                                       col_8,
                                       col_9,
                                       col_10,
                                       col_11,
                                       col_12,
                                       col_13,
                                       col_14,
                                       col_15,
                                       col_16,
                                       col_17,
                                       col_18,
                                       col_19,
                                       col_20,
                                       col_21,
                                       col_22,
                                       col_23,
                                       col_24,
                                       col_25,
                                       col_26,
                                       error_msg)                                      
                                select file_type,
                                       filename,
                                       line_seq_id,
                                       thread_val,
                                       null location,
                                       sales_process_id,
                                       0 chunk_id,
                                       thead_id,
                                       head_detl_id,
                                       ttax_id,
                                       tdetl_id,
                                       col_1,
                                       col_2,
                                       col_3,
                                       col_4,
                                       col_5,
                                       col_6,
                                       col_7,
                                       col_8,
                                       col_9,
                                       col_10,
                                       col_11,
                                       col_12,
                                       col_13,
                                       col_14,
                                       col_15,
                                       col_16,
                                       col_17,
                                       col_18,
                                       col_19,
                                       col_20,
                                       col_21,
                                       col_22,
                                       col_23,
                                       col_24,
                                       col_25,
                                       col_26,
                                       error_msg
                                  FROM (SELECT  file_type,
                                                filename,
                                                line_seq_id,
                                                thread_val,
                                                sales_process_id,
                                                thead_id,
                                                CASE
                                                   WHEN file_type IN ('THEAD')
                                                   THEN
                                                      thead_id
                                                   WHEN file_type IN ('TTAX','TDETL', 'TTAIL')
                                                   THEN
                                                      MAX(thead_id)
                                                         OVER
                                                         (
                                                            ORDER BY line_seq_id
                                                            ROWS BETWEEN UNBOUNDED PRECEDING
                                                            AND     1 PRECEDING
                                                         )
                                                END
                                                   AS head_detl_id,
                                                ttax_id,                                                
                                                tdetl_id,
                                                col_1,
                                                col_2,
                                                col_3,
                                                col_4,
                                                col_5,
                                                col_6,
                                                col_7,
                                                col_8,
                                                col_9,
                                                col_10,
                                                col_11,
                                                col_12,
                                                col_13,
                                                col_14,
                                                col_15,
                                                col_16,
                                                col_17,
                                                col_18,
                                                col_19,
                                                col_20,
                                                col_21,
                                                col_22,
                                                col_23,
                                                col_24,
                                                col_25,
                                                col_26,
                                                error_msg
                                           FROM (SELECT file_type,
                                                        line_seq_id,
                                                        thread_val,
                                                        sales_process_id,
                                                        filename,
                                                        DECODE (file_type,
                                                                'THEAD',line_seq_id,
                                                                NULL)
                                                           thead_id,
                                                        DECODE (file_type,
                                                                'TTAX', line_seq_id,
                                                                NULL)
                                                           ttax_id,
                                                        DECODE (file_type,
                                                                'TDETL', line_seq_id,
                                                                NULL)
                                                           tdetl_id,
                                                        DECODE (file_type,
                                                                'FHEAD',
                                                                TRIM (SUBSTR (line_text, 20, 14)), --FHEAD: file create date
                                                                'THEAD',
                                                                TRIM (SUBSTR (line_text, 16, 14)), --THEAD: transaction date
                                                                'TTAX',
                                                                TRIM (SUBSTR (line_text, 16, 6)),  --TTAX: tax code
                                                                'TDETL',
                                                                TRIM (SUBSTR (line_text, 16, 6)),  --TDETL: promo tran type
                                                                'TTAIL',
                                                                TRIM (SUBSTR (line_text, 16, 6)),  --TTAIL: transaction count
                                                                'FTAIL',
                                                                TRIM (SUBSTR (line_text, 16, 10))) --FTAIL: file record counter
                                                           col_1,
                                                        DECODE (file_type,
                                                                'FHEAD',
                                                                TRIM (SUBSTR (line_text, 34, 10)), --FHEAD: location number
                                                                'THEAD',
                                                                TRIM (SUBSTR (line_text, 30, 3)),  --THEAD: item type
                                                                'TTAX',                             
                                                                TRIM (SUBSTR (line_text, 22, 20)), --TTAX: tax rate
                                                                'TDETL',                            
                                                                TRIM (SUBSTR (line_text, 22, 10))) --TDETL: promo number
                                                           col_2,
                                                        DECODE (file_type,
                                                                'FHEAD',
                                                                TRIM (SUBSTR (line_text, 44, 1)),  --FHEAD: vat include indicator
                                                                'THEAD',                            
                                                                TRIM (SUBSTR (line_text, 33, 25)), --THEAD: item
                                                                'TTAX',                             
                                                                TRIM (SUBSTR (line_text, 42, 20)), --TTAX: tax amount
                                                                'TDETL',
                                                                TRIM (SUBSTR (line_text, 32, 12))) --TDETL: sales qty
                                                           col_3,
                                                        DECODE (file_type,
                                                                'FHEAD',
                                                                TRIM (SUBSTR (line_text, 45, 4)),  --FHEAD: vat region
                                                                'THEAD',                            
                                                                TRIM (SUBSTR (line_text, 58, 4)),  --THEAD: dept
                                                                'TDETL',                            
                                                                TRIM (SUBSTR (line_text, 44, 20))) --TDETL: sales value
                                                           col_4,
                                                        DECODE (file_type,
                                                                'FHEAD',
                                                                TRIM (SUBSTR (line_text, 49, 3)),  --FHEAD: currency code
                                                                'THEAD',                            
                                                                TRIM (SUBSTR (line_text, 62, 4)),  --THEAD: class
                                                                'TDETL',                            
                                                                TRIM (SUBSTR (line_text, 64, 20))) --TDETL: discount value
                                                           col_5,
                                                        DECODE (file_type,
                                                                'FHEAD',
                                                                TRIM (SUBSTR (line_text, 52, 1)),  --FHEAD: currency rtl decimals
                                                                'THEAD',                            
                                                                TRIM (SUBSTR (line_text, 66, 4)),  --THEAD: subclass
                                                                'TDETL',                            
                                                                TRIM (SUBSTR (line_text, 84, 10))) --TDETL: promo component
                                                           col_6,
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 70, 1))) col_7,    --THEAD: pack ind                                                           
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 71, 1))) col_8,    --THEAD: item level                                                           
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 72, 1))) col_9,    --THEAD: tran level                                                           
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 73, 6))) col_10,   --THEAD: wastage type                                                           
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 79, 12))) col_11,  --THEAD: wastage percent
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 91, 1))) col_12,   --THEAD: tran type                                                           
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 92, 1))) col_13,   --THEAD: drop ship ind                                                           
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 93, 12))) col_14,  --THEAD: total sales qty                                                          
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 105, 4))) col_15,  --THEAD: selling uom
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 109, 1))) col_16,  --THEAD: sales sign
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 110, 20))) col_17, --THEAD: total sales value
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 130, 14))) col_18, --THEAD: last modified date
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 144, 1))) col_19,  --THEAD: catchweight ind
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 145, 12))) col_20, --THEAD: actual weight qty
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 157, 1))) col_21,  --THEAD: sub trantype ind
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 158, 20))) col_22, --THEAD: total igtax value
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 178, 1))) col_23,  --THEAD: sales type
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 179, 1))) col_24,  --THEAD: return without inventory ind
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 180, 10))) col_25, --THEAD: return disposition
                                                        DECODE (file_type, 'THEAD', TRIM (SUBSTR (line_text, 190, 10))) col_26, --THEAD: return warehouse
                                                        error_msg
                                                   FROM svc_posupld_load
                                                  WHERE trim(filename) = trim(:GV_filename) and sales_process_id = trim(:GV_salesprocess_id)));
      -- update the new column with the store value.
      MERGE INTO svc_posupld_staging s
      USING (SELECT sales_process_id,
                    TO_NUMBER(col_2) location
               FROM svc_posupld_staging
              WHERE file_type = 'FHEAD'
                AND TRIM(filename) = TRIM(:GV_filename) 
                AND sales_process_id = TRIM(:GV_salesprocess_id)) sps
      ON (s.sales_process_id = sps.sales_process_id)
      WHEN MATCHED THEN UPDATE
      SET s.location = sps.location;                                                  

      INSERT INTO SVC_POSUPLD_STATUS (reference_id,
                                      thread_val,
                                      location,
                                      process_id,
                                      chunk_id,
                                      status,
                                      last_update_datetime)
                             SELECT   sps.filename,
                                      sps.thread_val,
                                      sps.location,
                                      sps.sales_process_id,
                                      sps.chunk_id,
                                      'N',
                                      sysdate
                      FROM svc_posupld_staging sps
                     WHERE sps.sales_process_id = trim(:GV_salesprocess_id)
                       AND trim(sps.filename) = trim(:GV_filename)
                       AND file_type = 'FHEAD';

      INSERT INTO SVC_POSUPLD_NIL (loc,
                                   item,
                                   sales_process_id)
                           SELECT loc.col_2,
                                  item.item,
                                  item.sales_process_id
                             FROM svc_posupld_staging loc,
                                  store s,
                                  (SELECT DECODE(itm.col_2, 'ITM', im.item, im.item_parent) item,
                                          itm.sales_process_id
                                     FROM svc_posupld_staging itm,
                                          item_master im
                                    WHERE itm.sales_process_id = trim(:GV_salesprocess_id)
                                      AND itm.file_type = 'THEAD'
                                      AND im.item = itm.col_3) item
                            WHERE loc.sales_process_id = item.sales_process_id
                              AND loc.file_type = 'FHEAD'
                              AND loc.col_2 = s.store
                              AND NOT EXISTS (SELECT 1
                                                FROM item_loc il
                                               WHERE il.item = item.item
                                                 AND il.loc = loc.col_2
                                                 AND rownum = 1);

      COMMIT;
      /
      
      print :GV_script_error;
      exit  :GV_return_code;
     "  | sqlplus -s ${CONNECT} `
    
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "POPULATE_POSUPLD_STATUS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHECK_INPUT_FILE
# Purpose      :
#-------------------------------------------------------------------------
function CHECK_INPUT_FILE
{
   typeset filewithPath=$1

   if [[ -e $filewithPath ]]; then #Check to see if the file exists and if so then continue
      read firstLine < $filewithPath
   else
      LOG_ERROR "Cannot find input file $filewithPath." "CHK_IN_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      exit ${FATAL}
   fi

   
   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      :
#-------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${inputFile##*/}
   sqlldr ${CONNECT} silent=feedback,header \
      control=${MMHOME}/log/$filename.ctl \
      log=${MMHOME}/log/$sqlldrFile.${dtStamp}.log \
      data=${inputFile} \
      bad=${MMHOME}/log/$sqlldrFile.bad \
      rows=65534 \
      bindsize=2048000 \
      readsize=2048000 \
      discard=${MMHOME}/log/$sqlldrFile.dsc
      
   LOG_MESSAGE "Completed loading input file ${inputFile} to SVC_POSUPLD_LOAD." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

   return ${OK}
}

#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   USAGE
   exit 1
fi

inputfile_extension=0

#Validate input parameters
CONNECT=$1
if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "uploadsales.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

inputFile=$2
inputfile_extension=${inputFile##*.}
filename=${inputFile##*/}

#--- Validate if file extension of input file is a valid thread number.
error_code=0
case $inputfile_extension in
  *[!0-9]*) error_code=1;; 
         *);;
esac

if [ ${error_code} == 1 ]; then
   LOG_MESSAGE "Invalid Thread Number - $inputfile_extension" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1; 
fi

#--- Check if input file exists.
CHECK_FILENAME $2 ${ERRORFILE} ${LOGFILE} ${pgmName}
if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
fi

CHECK_INPUT_FILE $inputFile

sqlTxt="
      DECLARE
         L_dummy   VARCHAR2(1) := NULL;
         
         FUNCTION_ERROR  EXCEPTION;

         cursor CHECK_PLSQL_CONFIG is
            select 1
              from rms_plsql_batch_config
             where program_name = 'CORESVC_SALES_UPLOAD_SQL'
               and rownum = 1;

      BEGIN
         open CHECK_PLSQL_CONFIG;
         fetch CHECK_PLSQL_CONFIG into L_dummy;
         close CHECK_PLSQL_CONFIG;
         
         if L_dummy is NULL then
            raise FUNCTION_ERROR;
         end if; 
         

      EXCEPTION
         when FUNCTION_ERROR then
            :GV_script_error := 'No record for CORESVC_SALES_UPLOAD_SQL in RMS_PLSQL_BATCH_CONFIG';
            :GV_return_code := ${FATAL};
         when OTHERS then
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};     
      END;"
   
   EXEC_SQL ${sqlTxt} 
   
   if [[ $? -ne ${OK} ]]; then
        return ${FATAL}
   fi
   
salesprocess_id=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select sales_process_id_seq.nextval
  from dual;
exit;
EOF`


GEN_CTL

# --- Start load of input file into SVC_POSUPLD_LOAD by using sqlldr tool
LOAD_FILE

# --- Populate SVC_POSUPLD_STATUS table to write new records to be processed. 
POPULATE_POSUPLD_STATUS $inputFile 

# --  Check for any Oracle errors from the SQLPLUS process
if [ `grep "${pgmName}: PID=${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. See error file" "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "Program uploadsales.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}   
fi

exit 0
