/*=========================================================================

   tdup: Detection of duplicate/missing transaction id's.

The TDUP_RANGE structure is used in a linked list of nodes that track the
current transaction id's as they are processed. With this linked list
we are able to determine if a duplicate transaction id is submitted.
At the end of the run we can also see any gaps in the transaction id
sequence and report them as missing transactions.

Each TDUP_RANGE node represents a contiguous block of transaction id's
that have been processed. The beginning of the block is indicated
by the "first" value. The end of the block is indicated by the
"last" value. Both are inclusive; therefore it will be seen that
a newly allocated TDUP_RANGE will have first == last, with the value
of both being that of the single transaction id that caused the
allocation.

A duplicate transaction id will of course be readily evident at the
time it arrives, since it will fall within the first <--> last range
of an existing TDUP_RANGE on the list.

Missing transactions can be reported at the end of the run by simply
printing the transaction id's not covered by existing TDUP_RANGE's.

Note that date info is also included. This is required so that a TDUP_RANGE
which has become uninteresting due to age can be dropped from the list.

Tdup data is kept in a file which has the location code as part of its
name: tdupxxxx.dat, where xxxx is the RTL_FHEAD location code. The
location is passed in to the load and save data routines.

=========================================================================*/

#include <ctype.h>
#include "retek.h"
#include "salib.h"
#include "saimptlog.h"
#include "saimptlog_tdup.h"

static void condense_nodes( TDUP_RANGE *r );

/*-------------------------------------------------------------------------
tdup_addtran() adds the transaction id into the linked list. Returns
various informative tokens.
-------------------------------------------------------------------------*/
int tdup_addtran( char *loc, char *posreg, char *tranno, char *date )
{
   TDUP_UID *lp; TDUP_RANGE *r;
   char uid[ TDUP_UID_SIZE + 1 ];
   long int tid, tdate, txn_no, wkid;
   int hv, i;

   char pos_register[NULL_REGISTER];
   /* Information about previous transaction (used for determining exchanges) */
   static char prev_loc[NULL_LOC] = "";
   static char prev_posreg[NULL_REGISTER] = "";
   static char prev_tranno[NULL_TRAN_NO] = "";
   static char prev_date[NULL_DATE] = "";
   static char tmp_tran_no[NULL_TRAN_NO - LEN_WORKSTATION_ID];
   static char tmp_wkid[NULL_WORKSTATION_ID];

   for (i = LEN_LOC - 1; (i > 0) && (loc[i] == ' '); i--)
       loc[i] = '\0';

   for (i=0;i<LEN_REGISTER;i++)
   {
      if (posreg[i] == ' ')
         break;
       pos_register[i] = posreg[i];
   }
   pos_register[i]='\0';

   /* check for s_wkstation_tran_append_ind to find the workstation id and transaction no*/
   if (strcmp( SysOpt.s_wkstation_tran_append_ind, YSNO_Y) == 0)
   {
      memset(tmp_tran_no, '\0', sizeof(tmp_tran_no));
      memset(tmp_wkid, '\0', sizeof(tmp_wkid));
      strncpy(tmp_tran_no, &tranno[LEN_WORKSTATION_ID], (LEN_TRAN_NO - LEN_WORKSTATION_ID));
      strncpy(tmp_wkid, tranno, 3);
   }

  /* init values */
   memset( uid, ' ',  TDUP_UID_SIZE);
   if (CMPFXLSTR(TranNoGen, STRG_S))
   {
      /* use location as unique id */
      strncpy( &uid[ 0 ], loc, LEN_LOC );
      uid[ strlen(loc) ] = '\0';
   }
   else
   {
      if (strcmp( SysOpt.s_wkstation_tran_append_ind, YSNO_Y) == 0)
      {
         /* use location + workstation as unique id */
         strcpy(uid,loc);
         strcat(uid,tmp_wkid);
         uid[ strlen(loc) + strlen(tmp_wkid)] = '\0';
      }
      else
      {
         /* use location + posregister as unique id */
         strcpy(uid,loc);
         strcat(uid,pos_register);
      }
   }

   tid = atoln( tranno, LEN_TRAN_NO );

   if ((tid < ll_range_tranno_min) || (tid > ll_range_tranno_max))
      return(TDUP_TRANNO_OUTOFRANGE);

   if ( tid == 0 )
   {
      TDUP_VERBOSE( "INFO: No tran_no specified\n" );
      return( TDUP_BADTRANNO );
   }
   tdate = atoln( date, LEN_DATEONLY );

   /* check for existing duplicate */
   hv = hashval( uid );
   if ( ( lp = finduid( locht[ hv ], uid ) ) != NULL )
   {
      TDUP_VERBOSE2( "INFO: Found location <%s>\n", lp->uid );
      for ( r = lp->exist; r; r = r->next )
         /* Since the transaction falls within the already processed transaction number
            range and we are sure about the location, so we just need to check for the
            business date  */

         if  (tid >= r->first && tid <= r->last)
            return(TDUP_TRANDUP_ATFILE);

         else if ( tid == r->last + 1 && tdate == r->lastdate )
         {
            if ( r->next != NULL)
               if ( tid >= r->next->first && tid <= r->next->last)
                  return(TDUP_TRANDUP_ATFILE);
            /* congruent to this range on end, so expand it */
            TDUP_VERBOSE( "INFO: Found congruency\n" );
            ++r->last;
            condense_nodes( lp->exist );

            /* save data about previous transactions */
            strncpy(prev_loc, loc, LEN_LOC);
            if (strcmp( SysOpt.s_wkstation_tran_append_ind, YSNO_Y) == 0)
            {  /* copy workstation */
               strncpy(prev_posreg, tmp_wkid, LEN_WORKSTATION_ID);
               strncpy(prev_tranno, tmp_tran_no, LEN_TRAN_NO - LEN_WORKSTATION_ID);
            }
            else
            {  /* copy posreg */
               strncpy(prev_posreg, posreg, LEN_REGISTER);
               strncpy(prev_tranno, tranno, LEN_TRAN_NO);
            }
            strncpy(prev_date, date, LEN_DATE);

            return( TDUP_TRANOK );
         }
         else if ( tid == r->first - 1 && tdate == r->firstdate)
         {
            /* congruent to this range at start, so expand it */
            TDUP_VERBOSE( "INFO: Found congruency\n" );
            --r->first;
             condense_nodes( lp->exist );

            /* save data about previous transactions */
            strncpy(prev_loc, loc, LEN_LOC);
            if (strcmp( SysOpt.s_wkstation_tran_append_ind, YSNO_Y) == 0)
            {  /* copy workstation */
               strncpy(prev_posreg, tmp_wkid, LEN_WORKSTATION_ID);
               strncpy(prev_tranno, tmp_tran_no, LEN_TRAN_NO - LEN_WORKSTATION_ID);
            }
            else
            {  /* copy posreg */
               strncpy(prev_posreg, posreg, LEN_REGISTER);
               strncpy(prev_tranno, tranno, LEN_TRAN_NO);
            }
            strncpy(prev_date, date, LEN_DATE);

            return( TDUP_TRANOK );
         }

      /* no logical place found, so add new range */
      TDUP_VERBOSE( "INFO: Adding new range\n" );
      lp->exist = addrange( lp->exist, tid, tdate );

      /* save data about previous transactions */
      strncpy(prev_loc, loc, LEN_LOC);

      if (strcmp( SysOpt.s_wkstation_tran_append_ind, YSNO_Y) == 0)
      {  /* copy workstation */
         strncpy(prev_posreg, tmp_wkid, LEN_WORKSTATION_ID);
         strncpy(prev_tranno, tmp_tran_no, LEN_TRAN_NO - LEN_WORKSTATION_ID);
      }
      else
      {  /* copy posreg */
         strncpy(prev_posreg, posreg, LEN_REGISTER);
         strncpy(prev_tranno, tranno, LEN_TRAN_NO);
      }
      strncpy(prev_date, date, LEN_DATE);

      return( TDUP_TRANOK );
   }
   else
   {
      TDUP_VERBOSE( "INFO: Adding new location\n" );
      locht[ hv ] = adduid( locht[ hv ], uid, tid, tdate );

      /* save data about previous transactions */
      strncpy(prev_loc, loc, LEN_LOC);
      if (strcmp( SysOpt.s_wkstation_tran_append_ind, YSNO_Y) == 0)
      {  /* copy workstation */
         strncpy(prev_posreg, tmp_wkid, LEN_WORKSTATION_ID);
         strncpy(prev_tranno, tmp_tran_no, LEN_TRAN_NO - LEN_WORKSTATION_ID);
      }
      else
      {  /* copy posreg */
         strncpy(prev_posreg, posreg, LEN_REGISTER);
         strncpy(prev_tranno, tranno, LEN_TRAN_NO);
      }
      strncpy(prev_date, date, LEN_DATE);

      return( TDUP_TRANOK );
   }
}
/*-------------------------------------------------------------------------
report_misstran() reports missing transactions.
-------------------------------------------------------------------------*/
int report_misstran( char *loc, char *uid, TDUP_RANGE *r, TDUP_RANGE *next)
{
   long tran, n;
   int j, k, rv, gap;
   char ls_reg[LEN_REGISTER + 1];
   long int bdate =  atoln( BusinessDate, LEN_DATEONLY );
   if (next->first > r->last)
      gap = next->first - r->last - 1;
   else
      gap = ll_range_tranno_max - r->last + next->first - ll_range_tranno_min;
   if (gap >= l_max_tran_gap)
   {
      if (bdate == next->firstdate)
      {
         sprintf( logmsg, "<%s><%0*ld> to <%s><%0*ld>",
                  uid, LEN_TRAN_NO, r->last, uid, LEN_TRAN_NO, next->first);
         if (next->first < r->last)
            strcat(logmsg, " reset transaction sequence");
         WrBadTranError( MISSING_TRAN_BIG_GAP, "", -1, -1, SART_FHEAD, logmsg, strlen( logmsg));

      }
   }
   else
   {
      if ((r->lreported == NOT_REPORTED) && (next->freported == NOT_REPORTED))
      {
         tran = r->last + 1;
         for ( n = 0; n < gap; ++n, ++tran )
         {
         if(tran > ll_range_tranno_max)
            tran = ll_range_tranno_min;
            TDUP_VERBOSE4( "MISSING: <%s><%0*ld>\n", uid, LEN_TRAN_NO, n );
            k = 0;
            for (j = strlen(loc); j < strlen(uid); j++)
            {
               ls_reg[ k ] = uid[ j ];
               k++;
            }
            ls_reg[ k ] = '\0';
            /* output to missing tran file */
            if (!fmtSAMissTran( ls_reg, tran ))
               rv = FALSE;
         }

         /* Set reported */
         r->lreported = REPORTED;
         next->freported = REPORTED;
      }
   }
   return 0;
}
/*-------------------------------------------------------------------------
tdup_misstran() reports all missing transactions.
-------------------------------------------------------------------------*/
int tdup_misstran( char *loc,char *business_date)
{
   TDUP_UID *lp; TDUP_RANGE *r; TDUP_RANGE *o;
   int rv, j, k,rollover=0,latesttran=0,roll_maxgap=0;
   long i, n;
   char ls_reg[LEN_REGISTER + 1];
   long int bdate;

   rv = TRUE;
   clearBigGapErrors();

   bdate =  atoln( business_date, LEN_DATEONLY );


   /* search entire table */
   for ( i = 0; i < TDUP_HASHTABLE_SIZE; ++i )
   {
      for ( lp = locht[ i ]; lp; lp = lp->next )
      {
         /* Logic to set Rollover flag */
         rollover = 0;
         for ( o = lp->exist; o->next != NULL; o = o->next )
         {
           if (o->next->first - o->last > roll_maxgap)
           {
             roll_maxgap = o->next->first - o->last;
             if (o->lastdate > o->next->firstdate)
             {
                latesttran = o->last;
                rollover = 1;
             }
             else
             {
                rollover = 0;
             }
           }
        }
        if ( o->lastdate > lp->exist->firstdate )
        {
           rollover = 0;
        }
        /* End logic to set Rollover */
         for ( r = lp->exist; r->next != NULL; r = r->next )
         {
            if (!(rollover && r->last == latesttran ))
               report_misstran(loc, lp->uid, r, r->next);
         }
         if ( rollover )
         {
            report_misstran(loc, lp->uid, r, lp->exist);
         }
      }
   }

   return( rv );
}
/*-------------------------------------------------------------------------
tdup_savedata() writes the current data out to a file for next time.
-------------------------------------------------------------------------*/
int tdup_savedata( char *loc, char *rtlog_orig_sys )
{
   char *function = "tdup_savedata";
   FILE *f;
   TDUP_UID *lp; TDUP_RANGE *r;
   int i;

   if(check_filename(datafilename)<0)
   {
   return(FAILED);
   }

   makefilename( loc, rtlog_orig_sys );
   if ( ( f = fopen( datafilename, SA_WRITEMODE ) ) != NULL )
   {
      for ( i = 0; i < TDUP_HASHTABLE_SIZE; ++i )
      {
         for ( lp = locht[ i ]; lp; lp = lp->next )
         {
            for ( r = lp->exist; r; r = r->next )
               if (fprintf( f, "%s,%ld,%ld,%c,%ld,%ld,%c\n",
                            lp->uid,
                            r->first, r->firstdate, r->freported,
                            r->last,  r->lastdate,  r->lreported ) == 0)
               {
                  sprintf( logmsg, "ERROR: Failure writing Transaction Number Duplication (tdup) output file - %s - %s",
                           datafilename, strerror( errno) );
                  WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
                  ExitCode = EXIT_FAILURE;
                  return( FALSE );
               }
            delallranges(lp->exist);
            lp->exist = NULL;
         }
         delalluids(locht[i]);
         locht[i] = NULL;
      }
      if ( fclose( f ) != 0 )
      {
         sprintf( logmsg, "ERROR: Failure closing Transaction Number Duplication (tdup) output file - %s - %s",
                  datafilename, strerror( errno) );
         WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
         ExitCode = EXIT_FAILURE;
         return( FALSE );
       }

      return( TRUE );
   }
   return( FALSE );
}
/*-------------------------------------------------------------------------
condense_nodes() runs thru the linked list looking for contiguous ranges
we can condense into one. Note: Depends on list being ordered.
-------------------------------------------------------------------------*/
static void condense_nodes( TDUP_RANGE *r )
{
   TDUP_RANGE *tmp;

   TDUP_VERBOSE( "INFO: Condense check... " );
   for ( ; r && r->next; r = r->next )
      if (( r->last + 1 == r->next->first )&& ((r->next->firstdate == r->firstdate) && (r->next->lastdate == r->lastdate)) )
      {
         TDUP_VERBOSE( "found candidate, condensing..." );
         /* keep widest date range */
         if ( r->next->firstdate < r->firstdate )
            r->firstdate = r->next->firstdate;
         if ( r->next->lastdate > r->lastdate )
            r->lastdate = r->next->lastdate;

         /* add next range onto r */
         r->last = r->next->last;

         r->lreported = r->next->lreported;

         /* remove next from list */
         tmp = r->next;
         r->next = r->next->next;
         free( tmp );

         /* can only happen once since we are */
         /* called every time, so just return */
         break;
      }

   TDUP_VERBOSE( "\n" );
}
