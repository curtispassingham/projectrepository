#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  wfslsupld.ksh
#
#  Desc:  This shell script uploads non-stockholding franchise store sales to the
#         WF_NON_STOCKHOLDING_SALES table.  This script has 4 different process modes.
#
#         The following are done in this script
#
#         PROCESS MODE = "LOAD"
#         1. Check if the input file exists and have appropriate access.
#         2. SQL Load (sqlldr) the input file into the WFSLSUPLD_STAGING table.
#            A fatal error from sqlldr will halt the process. Rejected records
#            is a non-fatal error and will be continue processing.
#         3. Group or "chunk" the records on the WFSLSUPLD_STAGING table.
#            This will group the records into chunks whose size is defined on the
#            RMS_PLSQL_BATCH_CONFIG table.
#         4. Validate the records on the WFSLSUPLD_STAGING table.  A validation failure
#            is a non-fatal error and processing will continue to the next record.
#
#         PROCESS MODE = "PROCESS"
#         1. Process all records with no error message in the WFSLSUPLD_STAGING table.
#         2. Roll up the records by Customer Location, Report Date and Item.  Insert
#            the rolled up records to the WFSLSUPLD_ROLLUP table.
#         3. Group or "chunk"  the records on the WFSLSUPLD_ROLLUP table.
#         4. Merge the records from the WFSLSUPLD_ROLLUP table to the
#            WF_NON_STOCKHOLDING_SALES table.
#
#         PROCESS MODE = "REJECT"
#         1. Process all records with error message in the WFSLSUPLD_STAGING table.
#         2. Write the records from the WFSLSUPLD_STAGING table into sepearate reject by Customer Location
#            files by Customer Location and Report Date.
#         3. Delete the records from the WFSLSUPLD_STAGING table.
#
#         PROCESS MODE = "PURGE"
#         1. Delete records from the WF_NON_STOCKHOLDING_SALES whose report date is older
#            than the retention days from PURGE_CONFIG_OPTIONS.WF_NON_STOCK_SALES_HIST_DAYS.
#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

kshName='wfslsupld.ksh'
kshName=${kshName##*/}    # remove the path
kshExt=${kshName##*.}     # get the extension
kshName=${kshName%.*}     # get the program name
pgmPID=$$                 # get the process ID

# File locations
CTLFILE="${MMHOME}/oracle/proc/src/wfslsupld.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$kshName.$exeDate.$pgmPID"

# Initialize variables and constants
inputfile_ext=0
REJECT="R"
FTYPE="WFSU"

# KSH valid mode values
PMLOAD="LOAD"
PMPRC="PROCESS"
PMREJ="REJECT"
PMPRG="PURGE"

SQLLDR_FAIL=1
SQLLDR_FATAL=3

custLoc=0
reportDate=0

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $kshName <connect string> <process mode> <input file>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <process mode>     Required parameter. Valid process modes are:

                      LOAD    - Upload sales information from input file.
                      PROCESS - Process sales information.
                      REJECT  - Create reject file and purge staging table.
                      PURGE   - Delete sales information that is older than the
                                retention date.

   <input file>       Required only for "'LOAD'" mode. Input file. Can include directory path.
"
}

#-------------------------------------------------------------------------
# Function Name: GET_MAX_THREADS
# Purpose      : Retrieves the maximum number of concurrent threads
#                that can be run for this batch.
#-------------------------------------------------------------------------
function GET_MAX_THREADS
{
   parallelThreads=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select max_concurrent_threads
     from rms_plsql_batch_config
    where program_name = 'WF_SALES_UPLOAD_SQL';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${parallelThreads}" "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   fi

   # Check the returned number of parallel threads
   if [ -z ${parallelThreads} ]; then
      LOG_ERROR "Unable to retrieve the number of threads from the RMS_PLSQL_BATCH_CONFIG table." "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   else
      LOG_MESSAGE "The number of concurrent threads for processing is ${parallelThreads}." "GET_MAX_THREADS" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GET_VAL_CHUNKS
# Purpose      : populates thre valArr array..
#-------------------------------------------------------------------------
function GET_VAL_CHUNKS
{
   # Retrieve chunks to process into an indexed array
   set -A valArr `$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select distinct chunk_id
     from wfslsupld_staging
      order by chunk_id;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${restartArr}" "GET_VAL_CHUNKS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   fi

   # Check the returned restart array
   if [ -z ${valArr} ]; then
      LOG_ERROR "Unable to retrieve the chunk IDs for the file ${inputFileNoPath}." "GET_VAL_CHUNKS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Chunk IDs retrieved." "GET_VAL_CHUNKS" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: VALIDATE_CUSTLOC
# Purpose      : Check if the customer location is valid
#-------------------------------------------------------------------------
function VALIDATE_CUSTLOC
{
set -A LocErr `$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off
with tmp (sdata) as
(
select case CHECK_NUM_VAL('${1}')
            when 'FALSE' then
               'INV_NUMBER'
            else NVL((select store_type||stockholding_ind
                        from store 
                       where store = TO_NUMBER('${1}')), NULL)
       end as sdata
 from dual
)
select error_msg
  from (select case
               when '"${1}"' is NULL then
                  SQL_LIB.CREATE_MSG('LOC_REQ', 'CUSTOMER_LOC', NULL, NULL)
               when tmp.sdata = 'INV_NUMBER' then
                  SQL_LIB.CREATE_MSG('INV_NUMBER', '${1}', NULL, NULL)
               when tmp.sdata is NULL then
                  SQL_LIB.CREATE_MSG('INV_STORE', TO_NUMBER('${1}'), NULL, NULL)
               when substr(tmp.sdata,1,1) != 'F' then
                  SQL_LIB.CREATE_MSG('MUST_BE_FR_STORE', TO_NUMBER('${1}'), NULL, NULL)
               when substr(tmp.sdata,2,1) != 'N' then
                  SQL_LIB.CREATE_MSG('NONSTOCK_WF_STORE_REQ', TO_NUMBER('${1}'), NULL, NULL)
               end as error_msg
          from tmp)
where error_msg is NOT NULL;
exit;
EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${LocErr}" "VALIDATE_CUSTLOC" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: VALIDATE_REPDATE
# Purpose      : Check if the report date is valid
#-------------------------------------------------------------------------
function VALIDATE_REPDATE
{
set -A DateErr `$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off
select error_msg
  from (select case
               when '${1}' is NULL then
                  SQL_LIB.CREATE_MSG('DATE_REQ', 'REPORT_DATE', NULL, NULL)
               else
                  case CHECK_DATE_VAL('${1}', 'YYYYMMDDHH24MISS') when 'FALSE' then
                     SQL_LIB.CREATE_MSG('INV_DATE', '${1}', NULL, NULL)
                  end
               end as error_msg
          from dual)
 where error_msg is NOT NULL;
exit;
EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${DateErr}" "VALIDATE_REPDATE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHECK_INPUT_FILE
# Purpose      : Validate the Input file
#-------------------------------------------------------------------------
function CHECK_INPUT_FILE
{
   typeset filewithPath=$1
   typeset rejectAll="N"

   if [[ -e ${filewithPath} ]]; then #Check to see if the file exists and if so then continue

      CHECK_FILENAME ${filewithPath} ${ERRORFILE} ${LOGFILE} ${kshName}
      if [[ $? -ne ${OK} ]]; then
         exit ${FATAL}
      fi

      # retrieve the FHEAD required fields
      fileType=`awk '/FHEAD/ {one=substr($0,16,4) ; print one} ' ${filewithPath}`
      custLoc=`awk '/FHEAD/ {one=substr($0,20,10) ; printf one} ' ${filewithPath}`
      reportDate=`awk '/FHEAD/ {one=substr($0,30,14) ; print one} ' ${filewithPath}`

      # retrieve the FTAIL record count
      ftailCount=`awk '/FTAIL/ {one=substr($0,16,10) ; printf ("%d",one)} ' ${filewithPath}`

      # count the actual FDETL records
      ftailActual=`awk '/FDETL/ {++count} END {print count}' ${filewithPath}`

   else
      LOG_ERROR "Cannot find input file ${filewithPath}." "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   fi

   # Validate the customer location.
   # Only non-stockholding franchise stores are valid.
   VALIDATE_CUSTLOC ${custLoc}

   if [[ ${LocErr} != "" ]]; then
      PARSE_ERROR_MSG ${LocErr}
      LocErrMsg=${sqlReturn}
   fi

   # Validate the report date.
   VALIDATE_REPDATE ${reportDate}

   if [[ ${DateErr} != "" ]]; then
      PARSE_ERROR_MSG ${DateErr}
      DateErrMsg=${sqlReturn}
   fi

   # Validate the FHEAD required fields
   if [[ ${fileType} != ${FTYPE} ]]; then
      LOG_ERROR "Input file ${filewithPath} not processed. Invalid value in the FILE TYPE field - ${fileType}." "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      rejectAll="Y"
   elif [[ ${LocErrMsg} != "" ]]; then
      LOG_ERROR "Input file ${filewithPath} not processed. ${LocErrMsg}" "VALIDATE_CUSTLOC" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      rejectAll="Y"
   elif [[ ${DateErrMsg} != "" ]]; then
      LOG_ERROR "Input file ${filewithPath} not processed. ${DateErrMsg}" "VALIDATE_REPDATE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      rejectAll="Y"
   elif [[ ${ftailCount} -ne ${ftailActual} ]] then
      LOG_ERROR "Input file ${filewithPath} not processed. Invalid file record count from file $filewithPath." "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      rejectAll="Y"
   fi

   # Exit fatal if FHEAD fails validation
   if [[ ${rejectAll} = "Y" ]]; then
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      : SQL Load the input file into the staging table
#-------------------------------------------------------------------------
function LOAD_FILE
{

   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${inputFile##*/}
   sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
   sqlldr ${connectStr} silent=feedback,header \
      control=${CTLFILE} \
      log=${sqlldrLog} \
      data=${inputFile} \
      bad=${MMHOME}/log/$sqlldrFile.bad \
      discard=${MMHOME}/log/$sqlldrFile.dsc
   sqlldr_status=$?

   # Check execution status.  Only SQLLDR return value of Fail or Fatal will be considered as a FATAL KSH error.
   if [[ $sqlldr_status = ${SQLLDR_FAIL} ||  $sqlldr_status = ${SQLLDR_FATAL} ]]; then
      LOG_ERROR "Error while loading file ${filewithPath}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   else
      # Check log file for sql loader errors
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows successfully loaded."` != "" ]];
      then
         LOG_MESSAGE "${inputFileNoPath} - No records loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
         LOG_MESSAGE "Program wfslsupld.ksh Terminated." "LOAD MODE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
         exit ${OK}
      elif [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` != ""
         && `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` != "" ]];
      then
         LOG_MESSAGE "${inputFileNoPath} - Completed loading file to the WFSLSUPLD_STAGING table." "LOAD_FILE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
      else
         LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
      fi
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PARSE_ERROR_MSG
# Purpose      : Parse error message for error log.
#-------------------------------------------------------------------------
function PARSE_ERROR_MSG
{

   sqlTxt="
      DECLARE
         L_error          VARCHAR2(4000) := '"${1}"';
         L_error_key      RTK_ERRORS.RTK_KEY%TYPE;
         FUNCTION_ERROR   EXCEPTION;

      BEGIN
        if SQL_LIB.PARSE_MSG(L_error,
                             L_error_key) = FALSE then
             raise FUNCTION_ERROR;
        end if;

          :GV_script_error := L_error;

      EXCEPTION
         when FUNCTION_ERROR then
            :GV_script_error := 'Error parsing error message';
            :GV_return_code := ${FATAL};
         when OTHERS then
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: VALIDATE_DETAIL
# Purpose      : Perform validation on loaded tables.
#              : Validation failure will result in error messages.
#-------------------------------------------------------------------------
function VALIDATE_DETAIL
{
   sqlTxt="
      DECLARE
         O_key             RTK_ERRORS.RTK_KEY%TYPE;
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT WF_SALES_UPLOAD_SQL.VALIDATE_DETAIL(:GV_script_error,
                                                    ${1}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Successfully validated: Chunk Id ${1}." "VALIDATE_DETAIL" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHECK_FOR_REJECTS
# Purpose      : Retrieve the Customer Location and Report Date with errors
#-------------------------------------------------------------------------
function CHECK_FOR_REJECTS
{
   set -A rejCustLoc `$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select distinct to_char(customer_loc)||'_'||to_char(report_date,'YYYYMMDDHH24MISS')
     from wfslsupld_staging
    where error_msg is NOT NULL;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Unable to retrieve rejected customer location records" "CHECK_FOR_REJECTS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   fi
   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHUNK_WFSLSUPLD_STAGING
# Purpose      : Chunk the staging table
#-------------------------------------------------------------------------
function CHUNK_WFSLSUPLD_STAGING
{
   sqlTxt="
      DECLARE
         O_key             RTK_ERRORS.RTK_KEY%TYPE;
         FUNCTION_ERROR    EXCEPTION;

      BEGIN
         if NOT WF_SALES_UPLOAD_SQL.CHUNK_STAGING(:GV_script_error,
                                                  TO_NUMBER($1),
                                                  TO_DATE($2,'YYYYMMDDHH24MISS')) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   else
      LOG_MESSAGE "Successfully chunked staging records." "CHUNK_WFSLSUPLD_STAGING" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_SALES
# Purpose      : Roll up and chunk the rollup table
#-------------------------------------------------------------------------
function PROCESS_SALES
{
   sqlTxt="
      DECLARE
         O_key             RTK_ERRORS.RTK_KEY%TYPE;
         FUNCTION_ERROR    EXCEPTION;

      BEGIN
         if NOT WF_SALES_UPLOAD_SQL.PROCESS_SALES(:GV_script_error) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
       exit ${FATAL}
   else
      LOG_MESSAGE "Successfully chunked rollup records." "PROCESS_SALES" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GET_ROLLUP_CHUNKS
# Purpose      : populates thre rollArr array.
#-------------------------------------------------------------------------
function GET_ROLLUP_CHUNKS
{
   # Retrieve chunks to process into an indexed array
   set -A rollArr `$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select distinct chunk_id
     from wfslsupld_rollup
      order by chunk_id;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${restartArr}" "GET_ROLLUP_CHUNKS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: LOAD_SALES
# Purpose      : Perform merge on the WF_NON_STOCKHOLDING_SALES table
#-------------------------------------------------------------------------
function LOAD_SALES
{
   sqlTxt="
      DECLARE
         O_key             RTK_ERRORS.RTK_KEY%TYPE;
         FUNCTION_ERROR    EXCEPTION;

      BEGIN

         if NOT WF_SALES_UPLOAD_SQL.LOAD_WF_SALES(:GV_script_error,
                                                  ${1}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   else
      LOG_MESSAGE "Successfully loaded sales: Roll Up Chunk Id ${1}." "LOAD_SALES" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: DEL_REJ_RECS
# Purpose      : Delete rejected records from the staging table.
#-------------------------------------------------------------------------
function DEL_REJ_RECS
{
   sqlTxt="
      DECLARE
         O_key             RTK_ERRORS.RTK_KEY%TYPE;
         FUNCTION_ERROR    EXCEPTION;

      BEGIN

         if NOT WF_SALES_UPLOAD_SQL.PURGE_REJECTS(:GV_script_error,
                                                  TO_CHAR('${1}')) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: WRITE_REJECT_FILE
# Purpose      : Generates the reject file
#-------------------------------------------------------------------------
function WRITE_REJECT_FILE
{

   sysDate=`date +"%Y%m%d%H%M%S"`   # get the sysdate
   locDate=${1}
   rejectFile="${HOME}/wfslsupld_${locDate}_${sysDate}.rej"
   rejFilename=${rejectFile##*/}    # remove the path

   LOG_MESSAGE "Writing reject file ${rejFilename}." "WRITE_REJECT_FILE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}

   echo "set PAGESIZE 32000
         set LINESIZE 57
         set HEADING OFF
         set FEEDBACK OFF
         set NEWPAGE NONE
         set TERMOUT OFF
         set SHOWMODE OFF
         set AUTOPRINT OFF
         set VERIFY OFF
         set TRIMOUT OFF
         set TRIMSPOOL OFF
         set TRIMS OFF
         select distinct 'FHEAD'||
                LPAD(1,10,0)||
                '${FTYPE}'||
                LPAD(customer_loc, 10, 0)||
                TO_CHAR(report_date, 'YYYYMMDDHH24MISS')||
                TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')
           from wfslsupld_staging
          where error_msg is NOT NULL
            and customer_loc||'_'||to_char(report_date,'YYYYMMDDHH24MISS')='${locDate}';
         exit;" | $ORACLE_HOME/bin/sqlplus -s $connectStr >> ${rejectFile}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error writing reject file ${rejFilename}." "WRITE_REJECT_FILE" ${FATAL} ${LOGFILE} ${kshName} ${pgmPID}
      exit ${FATAL}
   fi

   echo "set PAGESIZE 32000
         set TAB OFF
         set LINESIZE 79
         set HEADING OFF
         set FEEDBACK OFF
         set NEWPAGE NONE
         set TERMOUT OFF
         set SHOWMODE OFF
         set AUTOPRINT OFF
         set VERIFY OFF
         set TRIMOUT OFF
         set TRIMSPOOL OFF
         set TRIMS OFF
         select 'FDETL'||
                LPAD(ROWNUM + 1,10,0)||
                RPAD(NVL(item,' '),25,' ')||
                DECODE(net_sales_qty, NULL, RPAD(' ',12, ' '),
                                            LPAD((net_sales_qty*10000),12,0))||
                RPAD(NVL(net_sales_qty_uom,' '),4,' ')||
                DECODE(total_retail_amt, NULL, RPAD(' ',20, ' '),
                                               LPAD((total_retail_amt*10000),20,0))||
                RPAD(NVL(total_retail_amt_curr,' '),3,' ')
           from wfslsupld_staging
          where error_msg is NOT NULL
            and customer_loc||'_'||to_char(report_date,'YYYYMMDDHH24MISS')='${locDate}';
          exit;" | $ORACLE_HOME/bin/sqlplus -s $connectStr >> ${rejectFile}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error writing reject file ${rejFilename}." "WRITE_REJECT_FILE" ${FATAL} ${LOGFILE} ${kshName} ${pgmPID}
      exit ${FATAL}
   fi

   line_count=`wc -l < ${rejectFile}`

   echo "set PAGESIZE 32000
         set LINESIZE 25
         set HEADING OFF
         set FEEDBACK OFF
         set NEWPAGE NONE
         set TERMOUT OFF
         set SHOWMODE OFF
         set AUTOPRINT OFF
         set VERIFY OFF
         set TRIMOUT OFF
         set TRIMSPOOL OFF
         set TRIMS OFF
         select 'FTAIL'||
                LPAD($line_count + 1,10, 0)||
                LPAD($line_count - 1,10,0)
           from dual;
         exit;" | $ORACLE_HOME/bin/sqlplus -s $connectStr >> ${rejectFile}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error writing reject file ${rejFilename}." "WRITE_REJECT_FILE" ${FATAL} ${LOGFILE} ${kshName} ${pgmPID}
      exit ${FATAL}
   fi

   # write to the ERROR file
   echo "set PAGESIZE 32000
         set LINESIZE 1000
         set HEADING OFF
         set FEEDBACK OFF
         set NEWPAGE NONE
         set TERMOUT OFF
         set SHOWMODE OFF
         set AUTOPRINT OFF
         set VERIFY OFF
         set TRIMOUT ON
         set TRIMSPOOL ON
         set TRIMS ON
         select '${kshName}'||
                '~'||
                TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||
                '~'||
                'WRITE_REJECT_FILE'||
                '~'||
                '${rejFilename}'||
                '~'||
                error_msg
           from wfslsupld_staging
          where error_msg is NOT NULL
            and customer_loc||'_'||to_char(report_date,'YYYYMMDDHH24MISS')='${locDate}';
         exit;" | $ORACLE_HOME/bin/sqlplus -s $connectStr >> ${ERRORFILE}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error writing reject file ${rejFilename}." "WRITE_REJECT_FILE" ${FATAL} ${LOGFILE} ${kshName} ${pgmPID}
      exit ${FATAL}
   fi

   # Delete rejected records from the staging table
   DEL_REJ_RECS ${locDate}

   LOG_MESSAGE "Finished writing ${rejFilename}." "WRITE_REJECT_FILE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PURGE_HISTORY
# Purpose      : Purge the WF_NON_STOCKHOLDING_SALES records that are older
#                than the retention period.
#-------------------------------------------------------------------------
function PURGE_HISTORY
{
   sqlTxt="
      DECLARE
         O_key             RTK_ERRORS.RTK_KEY%TYPE;
         FUNCTION_ERROR    EXCEPTION;

      BEGIN
         if NOT WF_SALES_UPLOAD_SQL.WF_PURGE_NON_STOCK_SALES_HIST(:GV_script_error) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
       exit ${FATAL}
   else
      LOG_MESSAGE "Successfully purged records." "PURGE_HISTORY" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments

if [ $# -lt 2 ]; then
   USAGE
   exit ${NON_FATAL}
else
   if [ $2 = "LOAD" ]; then
      if [ $# -lt 3 ]; then
         USAGE
         exit ${NON_FATAL}
      fi
   fi
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------

connectStr=$1
USER=${connectStr%/*}

# Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${connectStr}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   exit ${FATAL}
else
   LOG_MESSAGE "wfslsupld.ksh started by ${USER}." "MAIN" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
fi

processMode=$2

# Validate the process mode
if [[ ${processMode} != ${PMLOAD} && ${processMode} != ${PMPRC} && ${processMode} != ${PMREJ} && ${processMode} != ${PMPRG} ]]; then
   LOG_ERROR "Invalid process mode. Check usage for valid modes." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${kshName}
   exit ${FATAL}
fi

inputFile=$3
inputfile_ext=${inputFile##*.}
inputFileNoPath=${inputFile##*/}    # remove the path

# Get the max concurrent threads
GET_MAX_THREADS

if [[ ${processMode} = ${PMLOAD} ]]; then

   ###################################################
   #                  LOAD MODE                      #
   ###################################################

   # Check if input file exists.
   CHECK_INPUT_FILE $inputFile

   # Load the staging table
   LOAD_FILE

   # Chunk the WFSLSUPLD_STAGING table
   CHUNK_WFSLSUPLD_STAGING ${custLoc} ${reportDate}

   # Get chunk ids
   GET_VAL_CHUNKS

   #----------------------------------------------------
   # Start to invoke VALIDATE_DETAIL based on Chunk ID
   #----------------------------------------------------

   val_index=0
   while [ ${val_index} -lt  ${#valArr[*]} ]
   do
      if [ `jobs | wc -l` -lt ${parallelThreads} ]
      then
         (
            VALIDATE_DETAIL ${valArr[${val_index}]}
         ) &
      else
         # Loop until a thread becomes available
         while [ `jobs | wc -l` -ge ${parallelThreads} ]
         do
            sleep 1
         done
         (
            VALIDATE_DETAIL ${valArr[${val_index}]}
         ) &
      fi
      let val_index=${val_index}+1;
   done
   # Wait for all of the threads to complete
   wait

   LOG_MESSAGE "Program wfslsupld.ksh Terminated." "LOAD MODE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}

elif [[ ${processMode} = ${PMPRC} ]]; then

   ###################################################
   #                PROCESS MODE                     #
   ###################################################

   # Roll up non-stockholding sales
   PROCESS_SALES

   # Get chunk id from roll up
   GET_ROLLUP_CHUNKS

   # Check the returned roll up array
   if [ -z ${rollArr} ]; then
      LOG_MESSAGE "No chunk IDs retrieved from the rollup." "GET_ROLLUP_CHUNKS" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   else
      LOG_MESSAGE "Chunk IDs retrieved." "GET_ROLLUP_CHUNKS" ${OK} ${LOGFILE} ${kshName} ${pgmPID}

      # Write the rolled up data to the target table
      write_index=0

      while [ ${write_index} -lt  ${#rollArr[*]} ]
      do
         if [ `jobs | wc -l` -lt ${parallelThreads} ]
            then
               (
                  LOAD_SALES ${rollArr[write_index]}
               ) &
         else
            # Loop until a thread becomes available
            while [ `jobs | wc -l` -ge ${parallelThreads} ]
            do
               sleep 1
            done
            (
               LOAD_SALES ${rollArr[write_index]}
            ) &
         fi
         let write_index=$write_index+1;
      done

      # Wait for all threads to complete
      wait
   fi

   LOG_MESSAGE "Program wfslsupld.ksh Terminated." "PROCESS MODE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}

elif [[ ${processMode} = ${PMREJ} ]]; then

   ###################################################
   #                REJECT MODE                      #
   ###################################################

   # Check if there are rejected records
   CHECK_FOR_REJECTS

   if [[ ! -z ${rejCustLoc} ]]; then

      # write error messages for rejected records in error log
      rej_index=0

      while [ ${rej_index} -lt ${#rejCustLoc[*]} ]
      do
         if [ `jobs | wc -l` -lt ${parallelThreads} ]
            then
               (
                  WRITE_REJECT_FILE ${rejCustLoc[rej_index]}
               ) &
         else
            # Loop until a thread becomes available
            while [ `jobs | wc -l` -ge ${parallelThreads} ]
            do
               sleep 1
            done
            (
                WRITE_REJECT_FILE ${rejCustLoc[rej_index]}
            ) &
         fi
         let rej_index=$rej_index+1;
      done

      # Wait for all threads to complete
      wait

      LOG_MESSAGE "Program wfslsupld.ksh Terminated." "REJECT MODE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}
   fi

elif [[ ${processMode} = ${PMPRG} ]]; then

   ###################################################
   #                 PURGE MODE                      #
   ###################################################

   # call the purge process
   PURGE_HISTORY

   LOG_MESSAGE "Program wfslsupld.ksh Terminated." "PURGE MODE" ${OK} ${LOGFILE} ${kshName} ${pgmPID}

fi

exit ${OK}
