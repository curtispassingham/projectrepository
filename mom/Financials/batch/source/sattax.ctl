LOAD DATA
INFILE	'sattax.dat'
BADFILE	'sattax.bad'

APPEND
INTO TABLE	sa_tran_tax

(tran_seq_no		POSITION (1:20)		INTEGER EXTERNAL,
 tax_code		POSITION (21:26)	CHAR,
 tax_seq_no             POSITION (27:30)        INTEGER EXTERNAL,
 tax_amt		POSITION (31:52)	DECIMAL EXTERNAL,
 error_ind		POSITION (53:53)	CHAR,
 ref_no17               POSITION (54:83)        CHAR,
 ref_no18               POSITION (84:113)       CHAR,
 ref_no19               POSITION (114:143)      CHAR,
 ref_no20               POSITION (144:173)      CHAR,
 store                  POSITION (174:183)      INTEGER EXTERNAL,
 day                    POSITION (184:186)      INTEGER EXTERNAL)  
