#ifndef SAIMPLOG_TDUP_H
#define SAIMPLOG_TDUP_H

/* tdup_addtran() return tokens */
#define TDUP_TRANOK             0
#define TDUP_BADTRANNO          1
#define TDUP_OUTOFMEMORY        2
#define TDUP_TRANDUPLICATE      3
#define TDUP_TRANNO_OUTOFRANGE  4
#define TDUP_TRANDUP_ATFILE     5
#define TDUP_INVLD_UID          6

#define REPORTED     'Y'
#define NOT_REPORTED 'N'

#define TDUP_UID_SIZE          (LEN_LOC+LEN_REGISTER) /* unique id size */
#define TDUP_HASHTABLE_SIZE    256                    /* size of hash table */
#define TDUP_HASHTABLE_MASK    0x00FF                 /* mask to limit value to table size */

typedef struct TDUP_RANGE_TAG {    /* TDUP_RANGE structure */
   long int first;                 /* tid at beginning of range */
   long int firstdate;             /* date first was added */
   char freported;                 /* reported earlier missing tran */
   long int last;                  /* tid at end of range */
   long int lastdate;              /* date last was added */
   char lreported;                 /* reported later missing tran */
   struct TDUP_RANGE_TAG *next;    /* link to next */
} TDUP_RANGE;

typedef struct TDUP_UID_TAG {     /* TDUP_UID structure, linked list node */
   char uid[ TDUP_UID_SIZE + 1 ]; /* unique id */
   TDUP_RANGE *exist;             /* existing transactions linked list */
   struct TDUP_UID_TAG *next;     /* link to next */
} TDUP_UID;


char datafilename[ FILENAME_MAX ];                 /* name for data file */
TDUP_UID *locht[ TDUP_HASHTABLE_SIZE ];            /* location hash table */
long int date_too_old;                             /* date before which we ain't interested */
long int ll_range_tranno_min, ll_range_tranno_max; /* range of tranno's */
TDUP_RANGE *last_created_range;                    /* initialization band-aid */


/* tdup: how many contiguous missing transactions constitute a major error */
#define NUM_MISSTRAN_ERR  20

int tdup_addtran( char *loc, char *posreg, char *tranno, char *date );
int tdup_misstran( char *loc, char *business_date );
int tdup_loaddata( char *loc, char *dup_cutoff_date,
                   long int min_tranno, long int max_tranno,
                   char *rtlog_orig_sys);
int tdup_savedata( char *loc , char *rtlog_orig_sys );

TDUP_UID *finduid( TDUP_UID *lp, char *uid );
void makefilename( char *loc, char *rtlog_orig_sys );
int hashval( char *instr );
void delallranges( TDUP_RANGE *r );
TDUP_UID *newuid( char *uid, long int tid, long int tdate );
TDUP_RANGE *newrange( long int tid, long int tdate );
TDUP_UID *adduid( TDUP_UID *lp, char *uid, long int tid, long int tdate );
void updrange( long int first, long int firstdate, char freptd,
                      long int last,  long int lastdate,  char lreptd);
void delalluids( TDUP_UID *lp );
TDUP_RANGE *addrange( TDUP_RANGE *r, long int tid, long int tdate );

#endif
