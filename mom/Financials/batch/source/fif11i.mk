include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS = 
PRODUCT_LDFLAGS = -L.
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/proc/src/fif11i_include.mk

#--- Standard targets.

ALL: fif-ALL FORCE

all: fif-all FORCE

libs: fif-libs FORCE

includes: fif-includes FORCE

libchange: fif-libchange FORCE

clean: fif-clean FORCE

clobber: fif-clobber FORCE

install: fif-install FORCE

lint: fif-lint FORCE

depend: fif-depend

FORCE:

#--- fif targets

fif: fif-all FORCE

fif-ALL: fif-all FORCE

fif-all: fif-libs $(FIF_EXECUTABLES) FORCE

fif-libs: fif-includes $(FIF_LIBS) FORCE

fif-includes: $(FIF_INCLUDES) FORCE

fif-libchange: FORCE
	rm -f $(FIF_EXECUTABLES)

fif-clean: FORCE
	rm -f $(FIF_C_SRCS:.c=.o)
	rm -f $(FIF_PC_SRCS:.pc=.o)
	rm -f $(FIF_PC_SRCS:.pc=.c)
	rm -f $(FIF_PC_SRCS:.pc=.lis)
	rm -f $(FIF_EXECUTABLES:=.lint)
	rm -f $(FIF_EXECUTABLES:=.ln)
	FIF_LIBS="$(FIF_LIBS)"; \
	for f in $$FIF_LIBS; \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

fif-clobber: libchange clean FORCE
	rm -f $(FIF_LIBS)
	rm -f $(FIF_INCLUDES)

fif-install: FORCE
	-@FIF_EXECUTABLES="$(FIF_EXECUTABLES)"; \
	for f in $$FIF_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

fif-lint: FORCE
	-@FIF_EXECUTABLES="$(FIF_EXECUTABLES)"; \
	for f in $$FIF_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/fif.mk $$e.lint; \
	done


fif-depend: fif11i.d

fif.d: $(FIF_C_SRCS) $(FIF_PC_SRCS)
	@echo "Making dependencies fif.d..."
	@$(MAKEDEPEND) $(FIF_C_SRCS) $(FIF_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

#--- Executable targets.

fifgldn1: $(FIFGLDN1_PC_OBJS)
	$(LD) -o `basename $@` `echo $(FIFGLDN1_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

fifgldn2: $(FIFGLDN2_PC_OBJS)
	$(LD) -o `basename $@` `echo $(FIFGLDN2_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

fifgldn3: $(FIFGLDN3_PC_OBJS)
	$(LD) -o `basename $@` `echo $(FIFGLDN3_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

#--- Executable lint targets.

fifgldn1.lint: $(FIFGLDN1_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(FIFGLDN1_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

fifgldn2.lint: $(FIFGLDN2_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(FIFGLDN2_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

fifgldn3.lint: $(FIFGLDN3_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(FIFGLDN3_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

#--- Library targets.

#--- Include targets.

#--- Object dependencies.

include fif11i.d
