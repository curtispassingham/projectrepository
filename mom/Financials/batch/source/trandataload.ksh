#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  Trandataload.ksh
#
#  Desc:  This script loads the staging TRAN DATA table (STAGE_EXT_TRAN_DATA) from a flat file using SQL Loader
#         and divides the data into Chunks to be processed in parallel threads 
#         based on the Commit Max Counter and Num_threads on RESTART_CONTROL table

#         This script should accept the following parameters:
#         1. Connect string
#         2. File_load_ind � This indicator is Y if a flat file has to be loaded into the table STAGE_EXT_TRAN_DATA else its N
#         3. Input file � This can include the directory path. This is mandatory when File_load_ind is Y.


#         The SQL loading is an option in the script. If File_load_ind is Y
#         1. Check if the input file provided exists and has appropriate access. If it doesn�t then log an error.
#         2. SQL Load (sqlldr) the input file using trandataload.ctl into the STAGE_EXT_TRAN_DATA table.
#            A fatal error from sqlldr will halt the process. Rejected records
#            is a non-fatal error and will be continue processing.
#            Also create bad file and discard files in case the input file does not match the expected format.
#         3. Log a message in case there were errors during the load by checking the sql loader log files created.
#            Exit with non fatal error.

#         The SQL loading is an option in the script. If File_load_ind is N 
#         Assume that data has been loaded on the staging table from some other source and try to chunk the data.
#         1. Check if the staging table is empty then log an error. 

#         Validations and Processing:
#         If the table STAGE_EXT_TRAN_DATA is empty or if all the records are already processed (status = 'P') then raise an exception and exit.
#         If data exists CHUNK the data on the table by OrgHier/Location. Get the commit max counter from restart control table for program �trandataprocess�.
#         Dense_rank the records over subclass, item and location. Then divide the rank value by commit max counter value and round up the value.
#         This value gives the Chunk ID in which the particular record falls into. Insert this info into STAGE_EXT_TRAN_DATA_CHUNK table. 
#         Item can be NULL on the staging table, when NULL consider item to be '-999'.
#         This will make sure the records with same subclass value and having item as NULL and NOT NULL are not grouped together in a chunk.
#         Truncate table STAGE_EXT_TRAN_DATA_CHUNK.

#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='trandataload.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID

# File locations
CTLFILE="${MMHOME}/oracle/proc/src/trandataload.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate

# Initialize variables and constants
inputfile_ext=0
REJECT="R"

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <file load indicator> <input file>

   <connect string>       Username/password@db.

   <file load indicator>  This indicator is Y/y if a flat file has to be loaded into the table STAGE_EXT_TRAN_DATA else its N/n.

   <input file>           Input file. Can include directory path. Mandatory if File Load Indicator is Y/y.
"
}

#-------------------------------------------------------------------------
# Function Name: CHECK_INPUT_FILE_EXIST
# Purpose      : Check if the input file exists
#-------------------------------------------------------------------------
function CHECK_INPUT_FILE_EXIST
{
   typeset filewithPath=$1

   if [[ -e ${filewithPath} ]]; then #Check to see if the file exists and if so then continue
      read firstLine < ${filewithPath}
   else
      LOG_ERROR "Cannot find input file ${filewithPath}." "CHECK_INPUT_FILE_EXIST" ${FATAL} ${ERRORFILE} ${pgmName}
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHECK_STAGE_EXT_TRAN_DATA
# Purpose      : If the table STAGE_EXT_TRAN_DATA is empty or if all the records are already processed (status = 'P') then raise an exception and exit.
#-------------------------------------------------------------------------
function CHECK_STAGE_EXT_TRAN_DATA
{

   stgTableCount=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select COUNT(1)
     from stage_ext_tran_data
    where status = 'N';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function CHECK_STAGE_EXT_TRAN_DATA failed" >>${ERRORFILE}
      exit ${FATAL}
   elif [[ ${stgTableCount} -eq 0 ]]; then
      LOG_ERROR "Table STAGE_EXT_TRAN_DATA is empty or contains no unprocessed records for chunking." "CHECK_STAGE_EXT_TRAN_DATA" ${NON_FATAL} ${ERRORFILE} ${pgmName}
      exit ${NON_FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Table STAGE_EXT_TRAN_DATA contains records for chunking." "CHECK_STAGE_EXT_TRAN_DATA" ${OK} ${LOGFILE} ${pgmName}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      : SQL Load the input file into the staging table
#-------------------------------------------------------------------------
function LOAD_FILE
{

   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${inputFile##*/}
   sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${MMHOME}/log/$sqlldrFile.dsc
   sqlldrBad=${MMHOME}/log/$sqlldrFile.bad
   sqlldr ${connectStr} silent=feedback,header \
      control=${CTLFILE} \
      log=${sqlldrLog} \
      data=${inputFile} \
      bad=${sqlldrBad} \
      discard=${sqlldrDsc} 
   sqlldr_status=$?

   # Check execution status
   if [[ $sqlldr_status != ${OK} ]]; then
      # Check if execution status is a warning
      if [[ $sqlldr_status = 2 ]]; then
         LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_ERROR "Error while loading file ${filewithPath}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${pgmName}
         exit ${FATAL}
      fi
   else
      # Check log file for sql loader errors
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` != ""
         && `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` != "" ]];
      then
         LOG_MESSAGE "${inputFileNoPath} - Completed loading file to the external tran data staging tables." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
      else
         LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
         return ${NON_FATAL}
      fi
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GET_COMMIT_MAX_COUNTER
# Purpose      : Retrieves the value of commit_max_ctr from restart_control table.
#-------------------------------------------------------------------------
function GET_COMMIT_MAX_COUNTER
{
   commitMaxCounter=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select commit_max_ctr
     from restart_control
    where lower(program_name) = 'trandataprocess';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${commitMaxCounter}" "GET_COMMIT_MAX_COUNTER" ${FATAL} ${ERRORFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned number of parallel threads
   if [ -z ${commitMaxCounter} ]; then
      LOG_ERROR "Unable to retrieve the commit max counter from the RESTART_CONTROL table." "GET_COMMIT_MAX_COUNTER" ${FATAL} ${ERRORFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "${pgmName} - The commit max counter for processing is ${commitMaxCounter}." "GET_COMMIT_MAX_COUNTER" ${OK} ${LOGFILE} ${pgmName}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHUNK_STAGE_EXT_TRAN_DATA
# Purpose      : Chunk the data on table STAGE_EXT_TRAN_DATA  by Subclass/Location
#                Empty table STAGE_EXT_TRAN_DATA_CHUNK.
#-------------------------------------------------------------------------
function CHUNK_STAGE_EXT_TRAN_DATA
{

   sqlTxt="
      BEGIN

         delete from STAGE_EXT_TRAN_DATA_CHUNK;
         insert into  STAGE_EXT_TRAN_DATA_CHUNK
                    ( dept,
                      class,
                      subclass,
                      item,
                      loc_type,
                      location,
                      chunk_id,
                      chunk_status)
               select distinct use_this.dept,
                      use_this.class,
                      use_this.subclass,
                      use_this.item,
                      use_this.loc_type,
                      use_this.location,
                      ceil(use_this.rank/${commitMaxCounter}) chunk_id,
                      'N'
                from  (select dept,
                              class,
                              subclass,
                              NVL(item,'-999') item,
                              loc_type,
                              location,
                              dense_rank() over(order by dept, class, subclass, NVL(item,'-999'), loc_type, location) rank
                         from stage_ext_tran_data
                        where status = 'N') use_this;

         COMMIT;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function CHUNK_STAGE_EXT_TRAN_DATA failed" >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Successfully chunked records for STAGE_EXT_TRAN_DATA." "CHUNK_STAGE_EXT_TRAN_DATA" ${OK} ${LOGFILE} ${pgmName}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for valid input arguments
if [[ ( $# -lt 2 ) || ( $2 = Y  &&  $# -lt 3 ) || ( $2 = y  &&  $# -lt 3 ) ]];
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
fileLoadInd=$2
inputFile=$3
inputFileNoPath=${inputFile##*/}    # remove the path
inputfile_ext=${inputFile##*.}
LOG_MESSAGE "trandataload.ksh started by ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName}


case ${fileLoadInd} in 
[Yy])
   # Check if input file exists.
   CHECK_INPUT_FILE_EXIST $inputFile
   # Load the external tran data staging table
   LOAD_FILE ;;
[Nn])
   # check if the external tran data staging table is empty, then log an error
   CHECK_STAGE_EXT_TRAN_DATA ;;
*)
   # not a valid option
   USAGE
   exit ${NON_FATAL} ;;
esac 

#Retrieve the value of commit_max_ctr from restart_control table.
GET_COMMIT_MAX_COUNTER

# Chunk the records in the parameter tables
CHUNK_STAGE_EXT_TRAN_DATA

   LOG_MESSAGE "trandataload.ksh finished for ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName}



exit ${OK}
