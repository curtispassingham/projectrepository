/*=========================================================================
SAIMPTLOG
ccval.c: This function validates the credit card numbers are in masked fashion.

ccval() checks out the card number and returns
a token signifying the cards validity.

A card number is validated in two way: its length and checks atleast two digits
of masked on the number. 

=========================================================================*/

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "retek.h"
#include "salib.h"
#include "sastdlen.h"
#include "saimptlog_proto.h"
#include "saimptlog_ccval.h"

/*-------------------------------------------------------------------------
data structures
-------------------------------------------------------------------------*/

typedef struct {
   char card_number[ NULL_CC_NO ];
   long card_type;
   char tran_date[ NULL_DATEONLY ];
   char newline_null[ 2 ];
} CCDATAIN;

typedef struct {
   char card_length[ LEN_CC_LENGTH ];
   char from_prefix[ LEN_CC_PREFIX ];
   char to_prefix[ LEN_CC_PREFIX ];
   char card_type[ LEN_TENDER_TYPE_ID ];
   char val_type[ CCVT_SIZE ];
   char newline[ LEN_NEWLINE ];
} CCVALDATIN;

typedef struct CCVDLL{
   int  card_length;
   long from_prefix;
   long to_prefix;
   long card_type;
   char val_type[ CCVT_SIZE ];
   struct CCVDLL *next;
} CCVALDAT;

/*-------------------------------------------------------------------------
function prototypes
-------------------------------------------------------------------------*/
static int sa_cc_len_mask_val( char *is_cc_no, char *is_cc_msk_char );

/*-------------------------------------------------------------------------
global variables
-------------------------------------------------------------------------*/
static CCVALDAT *ccvaltable = NULL;  /* linked list cc validation table */

/*-------------------------------------------------------------------------
ccval()
-------------------------------------------------------------------------*/
int ccval( char *is_cc_no,
           char *is_cc_msk_char,
           char *is_cc_exp_date,
           char *is_cc_tran_date)
{
   int   li_len = 0;
   int   rv;

   /* Return values are ORed together so that multiple errors can be returned. */
   rv = CCVAL_OK;

   rv |= sa_cc_len_mask_val( is_cc_no, is_cc_msk_char );

   return( rv );
} /* end of ccval */

/* sa_cc_len_mask_val() is used to find true length of card number
   and check the masked digit in the CC number */
/*--------------------------------------------------------------------------*/
static int sa_cc_len_mask_val( char *is_cc_no, char *is_cc_msk_char )
{
   int  li_len, li_num, mask_len=0;
   char ls_cc_mask[LEN_CC_NO];
   char ls_cc_mask_tmp[LEN_CC_NO];

   for ( li_len = LEN_CC_NO - 1; li_len >= 0; --li_len )

      if ( is_cc_no[ li_len ] != ' ' )
         break;

   li_len = li_len + 1;

   /* check for a masked digit in the CC number*/
   strncpy( ls_cc_mask, is_cc_no, li_len );
   nullpad( ls_cc_mask, (li_len + LEN_IND) );

   for (li_num=0; li_num < li_len; li_num++)
   {
      strncpy(ls_cc_mask_tmp,&ls_cc_mask[li_num],LEN_IND);
      nullpad(ls_cc_mask_tmp,NULL_IND);
      if ( strcmp( ls_cc_mask_tmp, is_cc_msk_char ) == 0 )
         mask_len = mask_len + 1;
   }

   if (mask_len < 2)
      return( CCVAL_NOTOK_MASKDIGIT );
   else
      return( CCVAL_OK );

} /* end of sa_cc_len_mask_val */

