-----------------------------------------------------------------------------
DROP TYPE mbl_sa_sum_open_tbl FORCE;
DROP TYPE mbl_sa_sum_open_rec FORCE;
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE mbl_sa_sum_open_rec AS OBJECT
(
   record_type             varchar2(10), --DATE, OLDER, ALL
   record_date             date,         --date of DATE type rows
   open_store_count        number(10)
)
/
CREATE OR REPLACE TYPE mbl_sa_sum_open_tbl AS TABLE OF mbl_sa_sum_open_rec
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE mbl_sa_sum_error_tbl FORCE;
DROP TYPE mbl_sa_sum_error_rec FORCE;
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE mbl_sa_sum_error_rec AS OBJECT
(
   record_type             varchar2(10), --DATE, OLDER, ALL
   record_date             date,         --date of DATE type rows
   error_count             number(10)
)
/
CREATE OR REPLACE TYPE mbl_sa_sum_error_tbl AS TABLE OF mbl_sa_sum_error_rec
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE mbl_sa_sum_os_count_tbl FORCE;
DROP TYPE mbl_sa_sum_os_count_rec FORCE;
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE mbl_sa_sum_os_count_rec AS OBJECT
(
   record_type             varchar2(10), --DATE, OLDER, ALL
   record_date             date,         --date of DATE type rows
   over_count              number(10),
   short_count             number(10)
)
/
CREATE OR REPLACE TYPE mbl_sa_sum_os_count_tbl AS TABLE OF mbl_sa_sum_os_count_rec
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE mbl_sa_sum_os_amount_tbl FORCE;
DROP TYPE mbl_sa_sum_os_amount_rec FORCE;
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE mbl_sa_sum_os_amount_rec AS OBJECT
(
   record_type             varchar2(10), --DATE, OLDER, ALL
   record_date             date,         --date of DATE type rows
   over_amount             number(20,4),
   short_amount            number(20,4),
   currency_code           varchar2(3)
)
/
CREATE OR REPLACE TYPE mbl_sa_sum_os_amount_tbl AS TABLE OF mbl_sa_sum_os_amount_rec
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE mbl_sa_store_day_tbl FORCE;
DROP TYPE mbl_sa_store_day_rec FORCE;
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE mbl_sa_store_day_rec AS OBJECT
(
   store                   number(10),
   store_day_seq_no        number(20),
   auditors                varchar2(255),
   business_date           date,
   store_name              varchar2(150),
   chain                   number(10),
   chain_name              varchar2(120),
   data_status             varchar2(6),
   data_status_desc        varchar2(250),
   audit_status            varchar2(6),
   audit_status_desc       varchar2(250),
   audit_changed_datetime  date,
   fuel_status             varchar2(6),
   fuel_status_desc        varchar2(250),
   --
   over_short_amount       number(20,4),
   currency_code           varchar2(3),
   --
   error_count             number(10),
   transaction_count       number(10),
   loaded_file_count       number(10),
   expected_file_count     number(10)
)
/
CREATE OR REPLACE TYPE mbl_sa_store_day_tbl AS TABLE OF mbl_sa_store_day_rec
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE mbl_sa_store_aggregation_tbl FORCE;
DROP TYPE mbl_sa_store_aggregation_rec FORCE;
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE mbl_sa_store_aggregation_rec AS OBJECT
(
   store                   number(10),
   store_name              varchar2(150),
   chain                   number(10),
   chain_name              varchar2(120),
   auditors                varchar2(255),
   --
   open_days               number(10),
   over_days               number(10),
   short_days              number(10),
   --
   over_amount             number(20,4),
   short_amount            number(20,4),
   currency_code           varchar2(3),
   --
   error_count             number(10)
)
/
CREATE OR REPLACE TYPE mbl_sa_store_aggregation_tbl AS TABLE OF mbl_sa_store_aggregation_rec
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE mbl_sa_store_error_tbl FORCE;
DROP TYPE mbl_sa_store_error_rec FORCE;
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE mbl_sa_store_error_rec AS OBJECT
(
   store                   number(10),
   error_code              varchar2(25),
   error_desc              varchar2(255),
   error_percentage        number(10,6)
)
/
CREATE OR REPLACE TYPE mbl_sa_store_error_tbl AS TABLE OF mbl_sa_store_error_rec
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE MBL_SA_STORE_QUERY_TBL FORCE;
DROP TYPE MBL_SA_STORE_QUERY_REC FORCE;
--------------------------------------------------------------------
CREATE OR REPLACE TYPE MBL_SA_STORE_QUERY_REC AS OBJECT
   (
     RECORD_TYPE            VARCHAR2(10)
    ,RECORD_DATE            DATE
    ,STORE                  NUMBER(10)
    ,STORE_NAME             VARCHAR2(150)
    ,AUDITORS               VARCHAR2(255)
    ,CURRENCY_CODE          VARCHAR2(3)
    ,CHAIN                  NUMBER(10)
    ,CHAIN_NAME             VARCHAR2(120)
    ,STORE_DAY_SEQ_NO       NUMBER(20)
    ,DATA_STATUS            VARCHAR2(6)
    ,DATA_STATUS_DESC       VARCHAR2(250)
    ,AUDIT_STATUS           VARCHAR2(6)
    ,AUDIT_STATUS_DESC      VARCHAR2(250)
    ,STORE_STATUS           VARCHAR2(6)
    ,STORE_STATUS_DESC      VARCHAR2(250)
    ,AUDIT_CHANGED_DATETIME DATE
    ,FILES_LOADED           NUMBER(10)
    ,TRANSACTION_COUNT      NUMBER(10)
    ,FILES_EXPECTED         NUMBER(10)
    ,ERR_CNT                NUMBER(10)
    ,BUSINESS_DATE          DATE
    ,START_DATE             DATE
    ,END_DATE               DATE
    ,TOTAL_SEQ_NO           NUMBER(20)  
    )
/
CREATE OR REPLACE TYPE MBL_SA_STORE_QUERY_TBL AS TABLE OF MBL_SA_STORE_QUERY_REC
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE MBL_SA_STORE_DATE_IND_TBL FORCE;
DROP TYPE MBL_SA_STORE_DATE_IND_REC FORCE;
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE MBL_SA_STORE_DATE_IND_REC AS OBJECT
(
    RECORD_TYPE             VARCHAR2(10) --DATE, OLDER, ALL
   ,RECORD_DATE             DATE         --date of DATE type rows
   ,VALUE_INDICATOR         NUMBER(1)
)
/
CREATE OR REPLACE TYPE MBL_SA_STORE_DATE_IND_TBL AS TABLE OF MBL_SA_STORE_DATE_IND_REC
/
-----------------------------------------------------------------------------