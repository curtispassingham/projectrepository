CREATE OR REPLACE PACKAGE MBL_RESA_SVC AUTHID CURRENT_USER AS
--------------------------------------------------------------------
-- CONSTANTS

RECORD_TYPE_DATE          CONSTANT VARCHAR2(10) := 'DATE';
RECORD_TYPE_OLDER         CONSTANT VARCHAR2(10) := 'OLDER';
RECORD_TYPE_ALL           CONSTANT VARCHAR2(10) := 'ALL';

SORT_BY_STORE_NAME        CONSTANT VARCHAR2(12) := 'STORENAME';
SORT_BY_AUDITOR           CONSTANT VARCHAR2(12) := 'AUDITOR';
SORT_BY_OSVALUE           CONSTANT VARCHAR2(12) := 'OSVALUE';
SORT_BY_ERROR_COUNT       CONSTANT VARCHAR2(12) := 'ERRORCNT';
SORT_BY_DATA_STATUS       CONSTANT VARCHAR2(12) := 'DATASTATUS';
SORT_BY_OPEN_DAYS         CONSTANT VARCHAR2(12) := 'OPENDAYS';
SORT_BY_OS_DAYS           CONSTANT VARCHAR2(12) := 'OSDAYS';
SORT_BY_OS_SUMS           CONSTANT VARCHAR2(12) := 'OSSUMS';
SORT_BY_BUSI_DATE         CONSTANT VARCHAR2(12) := 'BUSIDATE';

ORDER_ASC                 CONSTANT VARCHAR2(10) := 'ASC';
ORDER_DESC                CONSTANT VARCHAR2(10) := 'DESC';

SEARCH_ALL_STORES         CONSTANT VARCHAR2(10) := 'ALL';
SEARCH_ASSIGN_STORES      CONSTANT VARCHAR2(10) := 'ASSIGN';

--------------------------------------------------------------------
FUNCTION GET_SUMMARY_OPEN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_mbl_sa_sum_open_tbl IN OUT mbl_sa_sum_open_tbl,
                          O_store_count            OUT NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_SUMMARY_ERROR(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_mbl_sa_sum_error_tbl IN OUT mbl_sa_sum_error_tbl,
                           O_store_count             OUT NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_SUMMARY_OS_COUNT(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_mbl_sa_sum_os_count_tbl IN OUT mbl_sa_sum_os_count_tbl,
                              O_store_count                OUT NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_SUMMARY_OS_AMOUNT(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_mbl_sa_sum_os_amount_tbl IN OUT mbl_sa_sum_os_amount_tbl,
                               O_store_count                 OUT NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_STORE_ERRORS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_mbl_sa_store_error_tbl     IN OUT mbl_sa_store_error_tbl,
                          I_record_type                IN     VARCHAR2,
                          I_record_date                IN     DATE,
                          I_store                      IN     STORE.STORE%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_STORE_DAYS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_mbl_sa_store_day_tbl       IN OUT mbl_sa_store_day_tbl,
                        IO_pagination                IN OUT mbl_pagination_rec,
                        I_record_type                IN     VARCHAR2,
                        I_record_date                IN     DATE,
                        I_store                      IN     STORE.STORE%TYPE,
                        I_sort_attrib                IN     VARCHAR2,
                        I_sort_direction             IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_STORE_AGGREGATIONS(O_error_message                 IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_mbl_sa_store_aggregation_tbl  IN OUT mbl_sa_store_aggregation_tbl,
                                IO_pagination                   IN OUT mbl_pagination_rec,
                                I_all_older_ind                 IN     VARCHAR2,
                                I_store_tbl                     IN     OBJ_NUMERIC_ID_TABLE, --optional
                                I_sort_attrib                   IN     VARCHAR2,
                                I_sort_direction                IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION LOC_SEARCH(O_error_message                 IN OUT VARCHAR2,
                    O_mbl_sa_store_aggregation_tbl  IN OUT mbl_sa_store_aggregation_tbl,
                    IO_pagination                   IN OUT mbl_pagination_rec,
                    I_search_string                 IN     VARCHAR2,
                    I_search_filter                 IN     VARCHAR2,
                    I_sort_attrib                   IN     VARCHAR2,
                    I_sort_direction                IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_STORE_DATE_INDS(O_error_message                 IN OUT VARCHAR2,
                             O_mbl_sa_store_date_ind_tbl     IN OUT MBL_SA_STORE_DATE_IND_TBL,
                             I_store                         IN     STORE.STORE%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------
END MBL_RESA_SVC;
/
