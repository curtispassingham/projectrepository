CREATE OR REPLACE PACKAGE BODY MBL_RESA_SVC AS
--------------------------------------------------------------------
FUNCTION POP_SUM_DATES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION GET_ASSIGN_STORE_COUNT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_store_count      OUT NUMBER)
RETURN BOOLEAN;
--
FUNCTION GET_STORE_CURRENCY_CODE_COUNT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_currency_code_count     OUT NUMBER,
                                       O_user_currency           OUT STORE.CURRENCY_CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------
FUNCTION POP_SUM_DATES(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program      varchar2(61) := 'MBL_RESA_SVC.POP_SUM_DATES';
   L_vdate        DATE         := GET_VDATE;
BEGIN

   delete from gtt_num_num_str_str_date_date;

   --number_1    - helper for ordering results
   --varchar2_1  - record type
   --date_1      - start_date
   --date_2      - end date

   insert into gtt_num_num_str_str_date_date
      (number_1, number_2, varchar2_1, varchar2_2, date_1, date_2)
      select 1, null, MBL_RESA_SVC.RECORD_TYPE_DATE,  null, L_vdate-1, L_vdate-1   from dual union all
      select 2, null, MBL_RESA_SVC.RECORD_TYPE_DATE,  null, L_vdate-2, L_vdate-2   from dual union all
      select 3, null, MBL_RESA_SVC.RECORD_TYPE_DATE,  null, L_vdate-3, L_vdate-3   from dual union all
      select 4, null, MBL_RESA_SVC.RECORD_TYPE_DATE,  null, L_vdate-4, L_vdate-4   from dual union all
      select 5, null, MBL_RESA_SVC.RECORD_TYPE_DATE,  null, L_vdate-5, L_vdate-5   from dual union all
      select 6, null, MBL_RESA_SVC.RECORD_TYPE_OLDER, null, L_vdate-6, L_vdate-999 from dual union all
      select 7, null, MBL_RESA_SVC.RECORD_TYPE_ALL,   null, L_vdate-1, L_vdate-999 from dual;

   return TRUE;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN FALSE;
END POP_SUM_DATES;
--------------------------------------------------------------------
FUNCTION GET_ASSIGN_STORE_COUNT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_store_count      OUT NUMBER)
RETURN BOOLEAN IS
   L_program      varchar2(61)     := 'MBL_RESA_SVC.GET_ASSIGN_STORE_COUNT';
   L_user         varchar2(30)     := SVCPROV_CONTEXT.USER_NAME;
BEGIN

   select count(distinct m.store) 
     into O_store_count
     from sa_user_loc_traits t,
          loc_traits_matrix m,
          v_store vs
    where UPPER(t.user_id)  = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                            SYS_CONTEXT('USERENV', 'SESSION_USER')))
      and t.loc_trait = m.loc_trait
      and m.store     = vs.store;

   return TRUE;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN FALSE;
END GET_ASSIGN_STORE_COUNT;
--------------------------------------------------------------------
FUNCTION GET_STORE_CURRENCY_CODE_COUNT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_currency_code_count     OUT NUMBER,
                                       O_user_currency           OUT STORE.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS
   L_program      varchar2(61)     := 'MBL_RESA_SVC.GET_STORE_CURRENCY_CODE_COUNT';
   L_user         varchar2(30)     := SVCPROV_CONTEXT.USER_NAME;
   
   cursor c_currency_code_count is
   select count(1) 
       from(select distinct st.currency_code,
                              count(st.currency_code) over (partition by st.currency_code) 
                         from v_store st,
                              sa_user_loc_traits t,
                              loc_traits_matrix m
                        where UPPER(t.user_id)   =  UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                                 SYS_CONTEXT('USERENV', 'SESSION_USER')))
                          and t.loc_trait = m.loc_trait
                       and st.store = m.store);
                       
   cursor c_currency_code is
    select distinct st.currency_code
        from v_store st,
             sa_user_loc_traits t,
             loc_traits_matrix m
       where UPPER(t.user_id)   =  UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                SYS_CONTEXT('USERENV', 'SESSION_USER')))
         and t.loc_trait = m.loc_trait
         and st.store = m.store;
                       
BEGIN

   open  c_currency_code_count;
   fetch c_currency_code_count into O_currency_code_count;
   close c_currency_code_count;
   
   if O_currency_code_count > 1 then
      O_user_currency := NULL;
   else
      open  c_currency_code;
      fetch c_currency_code into O_user_currency;
      close c_currency_code;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      if c_currency_code_count%isopen then 
         close c_currency_code_count;
      end if;   
      if c_currency_code%isopen then
         close c_currency_code;
      end if;   
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN FALSE;
END GET_STORE_CURRENCY_CODE_COUNT;
---------------------------------------------------------------------
FUNCTION GET_SUMMARY_OPEN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_mbl_sa_sum_open_tbl IN OUT mbl_sa_sum_open_tbl,
                          O_store_count            OUT NUMBER)
RETURN NUMBER IS
   L_program      varchar2(61)     := 'MBL_RESA_SVC.GET_SUMMARY_OPEN';

   cursor c_store is
      select mbl_sa_sum_open_rec(inner.record_type,
                                 decode(inner.record_type,MBL_RESA_SVC.RECORD_TYPE_DATE,inner.end_date,null),
                                 count(sd.store_day_seq_no)) 
        from (select s.store,
                     gtt.varchar2_1 record_type,
                     gtt.date_1 start_date,
                     gtt.date_2 end_date,
                     gtt.number_1 orderby
                from (select distinct m.store
                        from sa_user_loc_traits t,
                             loc_traits_matrix m,
                             v_store vs
                       where UPPER(t.user_id)   = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                               SYS_CONTEXT('USERENV', 'SESSION_USER')))
                         and t.loc_trait = m.loc_trait
                         and m.store     = vs.store) s,
                     gtt_num_num_str_str_date_date gtt) inner,
             sa_store_day sd
       where inner.store         = sd.store(+)
         and inner.start_date   >= sd.business_date(+)
         and inner.end_date     <= sd.business_date(+)
         and sd.audit_status(+) != 'A'
         and sd.data_status(+)  != 'G'
       group by inner.record_type,
                inner.end_date,
                inner.orderby
       order by inner.orderby;

BEGIN

   if POP_SUM_DATES(O_error_message) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   if GET_ASSIGN_STORE_COUNT(O_error_message,
                             O_store_count) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open c_store;
   fetch c_store bulk collect into O_mbl_sa_sum_open_tbl;
   close c_store;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_store%ISOPEN then close c_store; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END GET_SUMMARY_OPEN;
--------------------------------------------------------------------------
FUNCTION GET_SUMMARY_ERROR(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_mbl_sa_sum_error_tbl IN OUT mbl_sa_sum_error_tbl,
                           O_store_count             OUT NUMBER)
RETURN NUMBER IS
   L_program      varchar2(61)     := 'MBL_RESA_SVC.GET_SUMMARY_ERROR';

   cursor c_store is
      select mbl_sa_sum_error_rec(st.record_type,
                                  decode(st.record_type,MBL_RESA_SVC.RECORD_TYPE_DATE,st.end_date,null),
                                  sum(nvl(err.err_cnt,0)))
        from (select s.store,
                     gtt.varchar2_1 record_type,
                     gtt.date_1 start_date,
                     gtt.date_2 end_date,
                     gtt.number_1 orderby
                from (select distinct m.store
                        from sa_user_loc_traits t,
                             loc_traits_matrix m,
                             v_store vs
                       where UPPER(t.user_id)   = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                               SYS_CONTEXT('USERENV', 'SESSION_USER')))
                         and t.loc_trait = m.loc_trait
                         and m.store     = vs.store) s,
                     gtt_num_num_str_str_date_date gtt) st,
             (select distinct
                     sd.store,
                     sd.business_date,
                     count(se.store_day_seq_no) over (partition by se.store_day_seq_no) err_cnt
                from sa_store_day sd,
                     sa_error se
               where sd.store_day_seq_no   = se.store_day_seq_no(+)
                 and se.hq_override_ind(+) = 'N'
                 and sd.audit_status     != 'A'
                 and sd.data_status      != 'G'
              ) err
       where st.store         = err.store(+)
         and st.start_date   >= err.business_date(+)
         and st.end_date     <= nvl(err.business_date(+),st.end_date)
       group by st.record_type,
                st.start_date,
                st.end_date,
                st.orderby
       order by st.orderby;

BEGIN

   if POP_SUM_DATES(O_error_message) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   if GET_ASSIGN_STORE_COUNT(O_error_message,
                             O_store_count) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open c_store;
   fetch c_store bulk collect into O_mbl_sa_sum_error_tbl;
   close c_store;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_store%ISOPEN then close c_store; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END GET_SUMMARY_ERROR;
--------------------------------------------------------------------------
FUNCTION GET_SUMMARY_OS_COUNT(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_mbl_sa_sum_os_count_tbl IN OUT mbl_sa_sum_os_count_tbl,
                              O_store_count                OUT NUMBER)
RETURN NUMBER IS
   L_program      varchar2(61) := 'MBL_RESA_SVC.GET_SUMMARY_OS_COUNT';
   L_vdate        DATE         := GET_VDATE;
   
   cursor c_os_count is
   --get total_seq_no's that we care about per each record we will return
   with total_seq as
   (select inner.record_type,
           decode(inner.record_type,MBL_RESA_SVC.RECORD_TYPE_DATE,inner.end_date,null) record_date,
           inner.orderby,
           inner.store,
           inner.currency_code,
           sd.store_day_seq_no,
           sd.business_date,
           st.total_seq_no total_seq_no
      from (select s.store,
                   gtt.varchar2_1 record_type,
                   gtt.date_1 start_date,
                   gtt.date_2 end_date,
                   s.currency_code,
                   gtt.number_1 orderby
              from (select distinct m.store,
                           st.currency_code
                      from sa_user_loc_traits t,
                           loc_traits_matrix m,
                           v_store st
                     where UPPER(t.user_id)   = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                             SYS_CONTEXT('USERENV', 'SESSION_USER')))
                       and t.loc_trait = m.loc_trait
                       and m.store     = st.store) s,
                   gtt_num_num_str_str_date_date gtt
            ) inner,
           sa_store_day sd,
           sa_total st
     where inner.store          = sd.store
       and inner.start_date    >= sd.business_date
       and inner.end_date      <= sd.business_date
       and sd.audit_status     != 'A'
       and sd.data_status      != 'G'
       and sd.store_day_seq_no  = st.store_day_seq_no
       and st.total_id          = 'OVRSHT_S'
       and st.bal_group_seq_no is NULL)
   --
   select mbl_sa_sum_os_count_rec(i3.record_type,
                                  i3.record_date,
                                  sum(decode(sign(i3.os_value),
                                         1,1,0)),
                                  sum(decode(sign(i3.os_value),
                                         -1,1,0)))
     from (--figure out level to use
           select i2.record_type,
                  i2.record_date,
                  i2.orderby,
                  i2.total_seq_no,
                  i2.os_value,
                  i2.total_level,
                  min(total_level) over (partition by i2.total_seq_no) min_total_level
             from (--totals for hq
                   select i.record_type,
                          i.record_date,
                          i.orderby,
                          i.total_seq_no,
                          i.hq_value os_value,
                          1 total_level
                     from (select h.total_seq_no, 
                                  t.store,
                                  t.store_day_seq_no,
                                  t.record_type,
                                  t.record_date,
                                  t.orderby,
                                  h.hq_value,
                                  h.value_rev_no,
                                  max(h.value_rev_no) over(partition by h.total_seq_no) max_rev
                             from sa_hq_value h,
                                  total_seq t
                            where h.total_seq_no = t.total_seq_no) i
                    where i.value_rev_no = i.max_rev
                   union all
                   --totals for sys 
                   select i.record_type,
                          i.record_date,
                          i.orderby,
                          i.total_seq_no,
                          i.sys_value os_value,
                          2 total_level
                     from (select s.total_seq_no, 
                                  t.record_type,
                                  t.record_date,
                                  t.orderby,
                                  s.sys_value, 
                                  s.value_rev_no, 
                                  max(s.value_rev_no) over(partition by s.total_seq_no) max_rev
                             from sa_sys_value s,
                                  total_seq t
                            where s.total_seq_no = t.total_seq_no) i
                    where i.value_rev_no = i.max_rev
                   union all
                   --totals for pos 
                   select i.record_type,
                          i.record_date,
                          i.orderby,
                          i.total_seq_no,
                          i.pos_value os_value,
                          3 total_level
                     from (select p.total_seq_no,
                                  t.record_type,
                                  t.record_date,
                                  t.orderby,
                                  p.pos_value,
                                  p.value_rev_no,
                                  max(p.value_rev_no) over(partition by p.total_seq_no) max_rev
                             from sa_pos_value p,
                                  total_seq t
                            where p.total_seq_no = t.total_seq_no) i
                    where i.value_rev_no = i.max_rev
                   union all
                   --fake total level
                   select t.record_type,
                          t.record_date,
                          t.orderby,
                          t.total_seq_no,
                          0 os_value,
                          4 total_level
                     from total_seq t
                  ) i2
          ) i3
    where i3.total_level = i3.min_total_level
    group by i3.record_type,
             i3.record_date,
             i3.orderby
    order by i3.orderby;
    
    cursor c_all_sum_dates is
       select mbl_sa_sum_os_count_rec(NVL(in_table.record_type, gtt.varchar2_1),
                                     DECODE(gtt.varchar2_1,MBL_RESA_SVC.RECORD_TYPE_DATE,
                                                           NVL(in_table.record_date,gtt.date_1),null),
                                     NVL(in_table.over_count, 0),
                                     NVL(in_table.short_count,0))
        from table(cast(o_mbl_sa_sum_os_count_tbl as mbl_sa_sum_os_count_tbl)) in_table,
             gtt_num_num_str_str_date_date gtt
       where gtt.varchar2_1 = in_table.record_type(+)
         and NVL(in_table.record_date(+), L_vdate) = DECODE(gtt.varchar2_1,MBL_RESA_SVC.RECORD_TYPE_DATE,
                                                                             gtt.date_1, L_vdate)
       order by gtt.number_1;

BEGIN

   if POP_SUM_DATES(O_error_message) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   if GET_ASSIGN_STORE_COUNT(O_error_message,
                             O_store_count) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open c_os_count;
   fetch c_os_count bulk collect into O_mbl_sa_sum_os_count_tbl;
   close c_os_count;
   
   if o_mbl_sa_sum_os_count_tbl is NULL or 
      o_mbl_sa_sum_os_count_tbl.COUNT != 7 then
      open c_all_sum_dates;
      fetch c_all_sum_dates bulk collect into O_mbl_sa_sum_os_count_tbl;
      close c_all_sum_dates;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_os_count%isopen then close c_os_count; end if;
      if c_all_sum_dates%isopen then close c_all_sum_dates; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END GET_SUMMARY_OS_COUNT;
--------------------------------------------------------------------------
FUNCTION GET_SUMMARY_OS_AMOUNT(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_mbl_sa_sum_os_amount_tbl IN OUT mbl_sa_sum_os_amount_tbl,
                               O_store_count                 OUT NUMBER)
RETURN NUMBER IS
   L_program            varchar2(61)             := 'MBL_RESA_SVC.GET_SUMMARY_OS_AMOUNT';
   L_vdate              DATE                     := GET_VDATE;
   L_primary_currency   store.currency_code%type := null;
   L_user_currency      store.currency_code%type := null;
   L_currency_code_count NUMBER;

   cursor c_amount is
   --get total_seq_no's that we care about per each record we will return
   with total_seq as
   (select inner.record_type,
           decode(inner.record_type,MBL_RESA_SVC.RECORD_TYPE_DATE,inner.end_date,null) record_date,
           inner.orderby,
           inner.store,
           inner.currency_code,
           sd.store_day_seq_no,
           sd.business_date,
           st.total_seq_no total_seq_no
      from (select s.store,
                   gtt.varchar2_1 record_type,
                   gtt.date_1 start_date,
                   gtt.date_2 end_date,
                   s.currency_code,
                   gtt.number_1 orderby
              from (select distinct m.store,
                           st.currency_code
                      from sa_user_loc_traits t,
                           loc_traits_matrix m,
                           v_store st
                     where UPPER(t.user_id)   = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                             SYS_CONTEXT('USERENV', 'SESSION_USER')))
                       and t.loc_trait = m.loc_trait
                       and m.store     = st.store) s,
                   gtt_num_num_str_str_date_date gtt
            ) inner,
           sa_store_day sd,
           sa_total st
     where inner.store          = sd.store
       and inner.start_date    >= sd.business_date
       and inner.end_date      <= sd.business_date
       and sd.audit_status     != 'A'
       and sd.data_status      != 'G'
       and sd.store_day_seq_no  = st.store_day_seq_no
       and st.total_id          = 'OVRSHT_S'
       and st.bal_group_seq_no is NULL)
   --build final results
   select mbl_sa_sum_os_amount_rec(
             i5.record_type,
             i5.record_date,
             sum(decode(sign(i5.os_value),
                        1, (i5.os_value*i5.exchange_rate),
                        0)),
             sum(decode(sign(i5.os_value),
                        1, 0,
                        (i5.os_value*i5.exchange_rate))),
             i5.to_currency)
     from (--get currency exchange rates
           select i4.record_type,
                  i4.record_date,
                  i4.orderby,
                  i4.os_value,
                  i4.business_date,
                  mv.from_currency,
                  mv.to_currency,
                  mv.effective_date,
                  mv.exchange_rate,
                  rank() over
                      (partition by mv.from_currency, mv.to_currency, mv.exchange_type
                           order by effective_date desc) date_rank
             from mv_currency_conversion_rates mv,
                  (--figure out if currency convert is needed
                   select i3.record_type,
                          i3.record_date,
                          i3.orderby,
                          i3.currency_code from_currency,
                          i3.business_date,
                          i3.total_seq_no,
                          i3.os_value,
                          case when L_currency_code_count > 1 then L_primary_currency
                             else  i3.currency_code 
                          end to_currency
                     from (--figure out level to use
                           select i2.record_type,
                                  i2.record_date,
                                  i2.orderby,
                                  i2.currency_code,
                                  i2.business_date,
                                  i2.total_seq_no,
                                  i2.os_value,
                                  i2.total_level,
                                  min(total_level) over (partition by i2.total_seq_no) min_total_level
                             from (--totals for hq
                                   select i.record_type,
                                          i.record_date,
                                          i.orderby,
                                          i.currency_code,
                                          i.business_date,
                                          i.total_seq_no,
                                          i.hq_value os_value,
                                          1 total_level
                                     from (select h.total_seq_no, 
                                                  t.store,
                                                  t.store_day_seq_no,
                                                  t.record_type,
                                                  t.record_date,
                                                  t.orderby,
                                                  t.currency_code,
                                                  t.business_date,
                                                  h.hq_value,
                                                  h.value_rev_no,
                                                  max(h.value_rev_no) over(partition by h.total_seq_no) max_rev
                                             from sa_hq_value h,
                                                  total_seq t
                                            where h.total_seq_no = t.total_seq_no) i
                                    where i.value_rev_no = i.max_rev
                                   union all
                                   --totals for sys 
                                   select i.record_type,
                                          i.record_date,
                                          i.orderby,
                                          i.currency_code,
                                          i.business_date,
                                          i.total_seq_no,
                                          i.sys_value os_value,
                                          2 total_level
                                     from (select s.total_seq_no, 
                                                  t.record_type,
                                                  t.record_date,
                                                  t.orderby,
                                                  t.currency_code,
                                                  t.business_date,
                                                  s.sys_value, 
                                                  s.value_rev_no, 
                                                  max(s.value_rev_no) over(partition by s.total_seq_no) max_rev
                                             from sa_sys_value s,
                                                  total_seq t
                                            where s.total_seq_no = t.total_seq_no) i
                                    where i.value_rev_no = i.max_rev
                                   union all
                                   --totals for pos 
                                   select i.record_type,
                                          i.record_date,
                                          i.orderby,
                                          i.currency_code,
                                          i.business_date,
                                          i.total_seq_no,
                                          i.pos_value os_value,
                                          3 total_level
                                     from (select p.total_seq_no,
                                                  t.record_type,
                                                  t.record_date,
                                                  t.orderby,
                                                  t.currency_code,
                                                  t.business_date,
                                                  p.pos_value,
                                                  p.value_rev_no,
                                                  max(p.value_rev_no) over(partition by p.total_seq_no) max_rev
                                             from sa_pos_value p,
                                                  total_seq t
                                            where p.total_seq_no = t.total_seq_no) i
                                    where i.value_rev_no = i.max_rev
                                   union all
                                   --fake total level
                                   select t.record_type,
                                          t.record_date,
                                          t.orderby,
                                          t.currency_code,
                                          t.business_date,
                                          t.total_seq_no,
                                          0 os_value,
                                          4 total_level
                                     from total_seq t
                                  ) i2
                          ) i3
                    where i3.total_level = i3.min_total_level) i4
             where mv.exchange_type   = 'C'
               and mv.effective_date <= i4.business_date
               and mv.from_currency   = i4.from_currency   
               and mv.to_currency     = i4.to_currency) i5
    where i5.date_rank = 1
    group by i5.record_type,
             i5.record_date,
             i5.orderby,
             i5.to_currency
    order by i5.orderby;
    
    cursor c_all_sum_dates is
       select mbl_sa_sum_os_amount_rec(NVL(in_table.record_type, gtt.varchar2_1),
                                       DECODE(gtt.varchar2_1,MBL_RESA_SVC.RECORD_TYPE_DATE,
                                                             NVL(in_table.record_date,gtt.date_1),null),
                                       NVL(in_table.over_amount, 0),
                                       NVL(in_table.short_amount,0),
                                       NVL(in_table.currency_code, case when L_currency_code_count > 1 then
                                                                          L_primary_currency
                                                                   else
                                                                      L_user_currency
                                                                   end))
        from table(cast(O_mbl_sa_sum_os_amount_tbl as mbl_sa_sum_os_amount_tbl)) in_table,
             gtt_num_num_str_str_date_date gtt
       where gtt.varchar2_1 = in_table.record_type(+)
         and NVL(in_table.record_date(+), L_vdate) = DECODE(gtt.varchar2_1,MBL_RESA_SVC.RECORD_TYPE_DATE,
                                                                             gtt.date_1, L_vdate)
       order by gtt.number_1;

BEGIN

   if POP_SUM_DATES(O_error_message) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   select currency_code 
     into L_primary_currency
     from system_options;

   if GET_ASSIGN_STORE_COUNT(O_error_message,
                             O_store_count) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   if GET_STORE_CURRENCY_CODE_COUNT(O_error_message,
                                    L_currency_code_count,
                                    L_user_currency) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   open c_amount;
   fetch c_amount bulk collect into O_mbl_sa_sum_os_amount_tbl;
   close c_amount;
   
   if O_mbl_sa_sum_os_amount_tbl is NULL or 
      O_mbl_sa_sum_os_amount_tbl.COUNT != 7 then
      open c_all_sum_dates;
      fetch c_all_sum_dates bulk collect into O_mbl_sa_sum_os_amount_tbl;
      close c_all_sum_dates;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_amount%isopen then close c_amount; end if;
      if c_all_sum_dates%isopen then close c_all_sum_dates; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END GET_SUMMARY_OS_AMOUNT;
--------------------------------------------------------------------------
FUNCTION GET_STORE_ERRORS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_mbl_sa_store_error_tbl     IN OUT mbl_sa_store_error_tbl,
                          I_record_type                IN     VARCHAR2,
                          I_record_date                IN     DATE,
                          I_store                      IN     STORE.STORE%TYPE)
RETURN NUMBER IS

   L_program            varchar2(61)   := 'MBL_RESA_SVC.GET_STORE_ERRORS';
   L_vdate              DATE           := GET_VDATE;

   cursor c_store_errors is
      select mbl_sa_store_error_rec(i3.store,
                                    i3.error_code, 
                                    i3.error_desc, 
                                    i3.error_code_cnt/i3.store_error_cnt)
        from (select i2.store,
                     i2.error_code, 
                     --
                     i2.error_desc,
                     --
                     i2.error_code_cnt, 
                     i2.store_error_cnt,
                     rank() over (partition by i2.store
                                      order by i2.error_code_cnt desc, i2.error_desc) error_rank
                from (select distinct i.store,
                                      se.error_code,
                                      ec.error_desc,
                                      count(*) over (partition by i.store, se.error_code, ec.error_desc) error_code_cnt,
                                      count(*) over (partition by i.store) store_error_cnt
                        from (select s.store,
                                        --
                                     case when I_record_type = MBL_RESA_SVC.RECORD_TYPE_DATE  then I_record_date
                                          when I_record_type = MBL_RESA_SVC.RECORD_TYPE_OLDER then L_vdate-6
                                          when I_record_type = MBL_RESA_SVC.RECORD_TYPE_ALL   then L_vdate-1
                                     end start_date,
                                     --
                                     case when I_record_type = MBL_RESA_SVC.RECORD_TYPE_DATE then I_record_date
                                          else L_vdate-999
                                     end end_date
                                from v_store s
                               where s.store  in (select I_store from dual
                                                  union all
                                                  select m.store
                                                    from sa_user_loc_traits t,
                                                         loc_traits_matrix m
                                                   where I_store is null
                                                     and UPPER(t.user_id) = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                                                         SYS_CONTEXT('USERENV', 'SESSION_USER')))
                                                     and t.loc_trait = m.loc_trait)
                             ) i,
                             sa_store_day sd,
                             sa_error se,
                             v_sa_error_codes ec
                       where i.store              = sd.store
                         and i.start_date        >= sd.business_date
                         and i.end_date          <= sd.business_date
                         and sd.audit_status         != 'A'
                         and sd.data_status          != 'G'
                         and sd.store_day_seq_no      = se.store_day_seq_no
                         and se.hq_override_ind       = 'N'
                         and se.error_code            = ec.error_code
                     ) i2
             ) I3
       where i3.error_rank <= 2; 

BEGIN

   open c_store_errors;
   fetch c_store_errors bulk collect into O_mbl_sa_store_error_tbl;
   close c_store_errors;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_store_errors%ISOPEN then close c_store_errors; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_STORE_ERRORS;
--------------------------------------------------------------------------
FUNCTION GET_STORE_DAYS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_mbl_sa_store_day_tbl       IN OUT mbl_sa_store_day_tbl,
                        IO_pagination                IN OUT mbl_pagination_rec,
                        I_record_type                IN     VARCHAR2,
                        I_record_date                IN     DATE,
                        I_store                      IN     STORE.STORE%TYPE,
                        I_sort_attrib                IN     VARCHAR2,
                        I_sort_direction             IN     VARCHAR2)
RETURN NUMBER IS

   L_program            varchar2(61)   := 'MBL_RESA_SVC.GET_STORE_DAYS';

   L_vdate              DATE           := GET_VDATE;
   L_fuel_dept          sa_system_options.fuel_dept%TYPE := null;

   L_row_start          NUMBER(10)   := 0;
   L_row_end            NUMBER(10)   := 0;
   L_num_rec_found      NUMBER(10)   := 0; 
   L_counter_table      OBJ_NUMERIC_ID_TABLE;
   L_store_query        MBL_SA_STORE_QUERY_TBL;

   cursor c_store_query is
      select MBL_SA_STORE_QUERY_REC(
              record_type
             ,record_date
             ,store
             ,store_name
             ,auditors
             ,currency_code
             ,chain
             ,chain_name
             ,store_day_seq_no
             ,data_status
             ,data_status_desc
             ,audit_status
             ,audit_status_desc
             ,store_status
             ,store_status_desc
             ,audit_changed_datetime
             ,files_loaded
             ,transaction_count
             ,files_expected
             ,err_cnt
             ,business_date
             ,start_date
             ,end_date
             ,total_seq_no)
        from (select DISTINCT 
             inner.record_type,
             decode(inner.record_type,MBL_RESA_SVC.RECORD_TYPE_DATE,inner.end_date,null) record_date,
             inner.store,
             --
             inner.store_name,
             --
             inner.auditors,
             inner.currency_code,
             inner.chain,
             inner.chain_name,
             sd.store_day_seq_no,
             sd.data_status,
             ds_code.code_desc data_status_desc,
             --
             sd.audit_status, 
             as_code.code_desc audit_status_desc,
             --
             case when L_fuel_dept is null then
                     null
                  when da.store is null then
                     null
                  else
                     sd.store_status
             end store_status,
             --
             case when L_fuel_dept is null then
                     null
                  when da.store is null then
                     null
                  else
                     ss_code.code_desc
             end store_status_desc,
             --
             sd.audit_changed_datetime,
             sd.files_loaded,
             --
             (select count(th_tran.tran_seq_no)
                from sa_tran_head th_tran
               where th_tran.store_day_seq_no = sd.store_day_seq_no
                 and th_tran.store            = sd.store
                 and th_tran.day              = sd.day
                 and nvl(th_tran.status,'D') != 'D') transaction_count,
             --
             (select max(th_file.ref_no1)
                from sa_tran_head th_file
               where th_file.store_day_seq_no = sd.store_day_seq_no
                 and th_file.store            = sd.store
                 and th_file.day              = sd.day
                 and th_file.tran_type        = 'DCLOSE') files_expected,
             --
             (select count(se.error_seq_no)
                from sa_error se
               where se.store_day_seq_no = sd.store_day_seq_no
                 and se.store            = sd.store
                 and se.day              = sd.day
                 and se.hq_override_ind  = 'N') err_cnt,
             --
             sd.business_date,
             inner.start_date,
             inner.end_date,
             --
             (select st.total_seq_no
                from sa_total st
               where st.store_day_seq_no = sd.store_day_seq_no
                 and st.store            = sd.store
                 and st.day              = sd.day
                 and st.total_id         = 'OVRSHT_S'
                 and st.bal_group_seq_no is NULL) total_seq_no
             --
        from (select s.store,
                     s.store_name,
                     gtt.varchar2_1 auditors,
                     s.currency_code,
                     s.chain,
                     ch.chain_name,
                     I_record_type record_type,
                     --
                     case when I_record_type = MBL_RESA_SVC.RECORD_TYPE_DATE  then I_record_date
                          when I_record_type = MBL_RESA_SVC.RECORD_TYPE_OLDER then L_vdate-6
                          when I_record_type = MBL_RESA_SVC.RECORD_TYPE_ALL   then L_vdate-1
                     end start_date,
                     --
                     case when I_record_type = MBL_RESA_SVC.RECORD_TYPE_DATE then I_record_date
                          else L_vdate-999
                     end end_date
                from v_store s,
                     v_chain ch,
                     gtt_num_num_str_str_date_date gtt
               where s.store  in (select I_store from dual
                                     union all
                                     select m.store
                                       from sa_user_loc_traits t,
                                            loc_traits_matrix m
                                      where I_store is null
                                        and UPPER(t.user_id) = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                                            SYS_CONTEXT('USERENV', 'SESSION_USER')))
                                        and t.loc_trait = m.loc_trait)
                 and s.chain  = ch.chain(+)
                 and s.store  = gtt.number_1(+)
                 ) inner,
             sa_store_day sd,
             sa_store_data da,
             v_code_detail as_code,
             v_code_detail ss_code,
             v_code_detail ds_code
       where inner.store                 = sd.store
         and inner.start_date           >= sd.business_date
         and inner.end_date             <= sd.business_date
         and sd.audit_status            != 'A'
         and sd.data_status             != 'G'
         --
         and sd.store                    = da.store(+)
         and da.system_code(+)           = 'SFM'
         --
         and as_code.code_type           = 'SAAS'
         and sd.audit_status             = as_code.code
         --
         and ss_code.code_type           = 'SASS'
         and sd.store_status             = ss_code.code
         --
         and ds_code.code_type           = 'SADS'
         and sd.data_status              = ds_code.code);

   cursor c_store_day is
   select mbl_sa_store_day_rec(i4.store,
                               i4.store_day_seq_no,
                               i4.auditors,
                               i4.business_date,
                               i4.store_name,
                               i4.chain,
                               i4.chain_name,
                               i4.data_status,
                               i4.data_status_desc,
                               i4.audit_status,
                               i4.audit_status_desc,
                               i4.audit_changed_datetime,
                               i4.store_status,
                               i4.store_status_desc,
                               i4.os_value,
                               i4.currency_code,
                               i4.err_cnt,
                               i4.transaction_count,
                               i4.files_loaded,
                               i4.files_expected) output,
          i4.total_rows
     from (select i3.store,
                  i3.store_day_seq_no,
                  i3.auditors,
                  i3.business_date,
                  i3.store_name,
                  i3.chain,
                  i3.chain_name,
                  i3.data_status,
                  i3.data_status_desc,
                  i3.audit_status,
                  i3.audit_status_desc,
                  i3.audit_changed_datetime,
                  i3.store_status,
                  i3.store_status_desc,
                  i3.os_value,
                  i3.currency_code,
                  i3.err_cnt,
                  i3.transaction_count,
                  i3.files_loaded,
                  i3.files_expected,
                  --
                  case when I_sort_attrib = MBL_RESA_SVC.SORT_BY_STORE_NAME and 
                            I_sort_direction  = MBL_RESA_SVC.ORDER_ASC then 
                          row_number() over (order by i3.store_name asc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_AUDITOR and 
                            I_sort_direction     = MBL_RESA_SVC.ORDER_ASC then 
                          row_number() over (order by i3.auditors asc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_OSVALUE and 
                            I_sort_direction     = MBL_RESA_SVC.ORDER_ASC then 
                          row_number() over (order by abs(i3.os_value) asc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_ERROR_COUNT and 
                            I_sort_direction = MBL_RESA_SVC.ORDER_ASC then 
                          row_number() over (order by i3.err_cnt asc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_DATA_STATUS and 
                            I_sort_direction = MBL_RESA_SVC.ORDER_ASC then 
                          row_number() over (order by i3.data_status asc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_BUSI_DATE and 
                            I_sort_direction = MBL_RESA_SVC.ORDER_ASC then 
                          row_number() over (order by i3.business_date asc, i3.store_day_seq_no)
                       --
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_STORE_NAME and 
                            I_sort_direction  = MBL_RESA_SVC.ORDER_DESC then
                          row_number() over (order by i3.store_name desc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_AUDITOR and 
                            I_sort_direction     = MBL_RESA_SVC.ORDER_DESC then
                          row_number() over (order by i3.auditors desc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_OSVALUE and 
                            I_sort_direction     = MBL_RESA_SVC.ORDER_DESC then
                          row_number() over (order by abs(i3.os_value) desc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_ERROR_COUNT and 
                            I_sort_direction = MBL_RESA_SVC.ORDER_DESC then
                          row_number() over (order by i3.err_cnt desc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_DATA_STATUS and 
                            I_sort_direction = MBL_RESA_SVC.ORDER_DESC then 
                          row_number() over (order by i3.data_status desc, i3.store_day_seq_no)
                       when I_sort_attrib = MBL_RESA_SVC.SORT_BY_BUSI_DATE and 
                            I_sort_direction = MBL_RESA_SVC.ORDER_DESC then 
                          row_number() over (order by i3.business_date desc, i3.store_day_seq_no)
                       else 
                          row_number() over (order by i3.store_name asc, i3.store_day_seq_no)
                  end counter,
                  count(*) over () total_rows
            from (--figure out level to use
                  select i2.record_type,
                         i2.record_date,
                         i2.total_seq_no,
                         i2.store,
                         i2.auditors,
                         i2.store_name,
                         i2.currency_code,
                         i2.chain,
                         i2.chain_name,
                         i2.store_day_seq_no,
                         i2.data_status,
                         i2.data_status_desc,
                         i2.audit_status,
                         i2.audit_status_desc,
                         i2.store_status,
                         i2.store_status_desc,
                         i2.audit_changed_datetime,
                         i2.files_loaded,
                         i2.transaction_count,
                         i2.files_expected,
                         i2.err_cnt,
                         i2.business_date,
                         i2.start_date,
                         i2.end_date,
                         i2.os_value,
                         i2.total_level,
                         min(total_level) over (partition by i2.store_day_seq_no) min_total_level
                    from (--totals for hq
                          select i.record_type,
                                 i.record_date,
                                 i.total_seq_no,
                                 i.store,
                                 i.store_name,
                                 i.auditors,
                                 i.currency_code,
                                 i.chain,
                                 i.chain_name,
                                 i.store_day_seq_no,
                                 i.data_status,
                                 i.data_status_desc,
                                 i.audit_status,
                                 i.audit_status_desc,
                                 i.store_status,
                                 i.store_status_desc,
                                 i.audit_changed_datetime,
                                 i.files_loaded,
                                 i.transaction_count,
                                 i.files_expected,
                                 i.err_cnt,
                                 i.business_date,
                                 i.start_date,
                                 i.end_date,
                                 i.hq_value os_value,
                                 1 total_level
                            from (select h.total_seq_no,
                                         --
                                         t.record_type,
                                         t.record_date,
                                         t.store,
                                         t.store_name,
                                         t.auditors,
                                         t.currency_code,
                                         t.chain,
                                         t.chain_name,
                                         t.store_day_seq_no,
                                         t.data_status,
                                         t.data_status_desc,
                                         t.audit_status,
                                         t.audit_status_desc,
                                         t.store_status,
                                         t.store_status_desc, 
                                         t.audit_changed_datetime,
                                         t.files_loaded,
                                         t.transaction_count,
                                         t.files_expected,
                                         t.err_cnt,
                                         t.business_date,
                                         t.start_date,
                                         t.end_date,
                                         --
                                         h.hq_value,
                                         h.value_rev_no,
                                         max(h.value_rev_no) over(partition by t.store_day_seq_no) max_rev
                                    from sa_hq_value h,
                                         table(cast(L_store_query AS MBL_SA_STORE_QUERY_TBL)) t
                                   where h.total_seq_no = t.total_seq_no) i
                           where i.value_rev_no = i.max_rev
                          --
                          union all
                          --totals for sys
                          select i.record_type,
                                 i.record_date,
                                 i.total_seq_no,
                                 i.store,
                                 i.store_name,
                                 i.auditors,
                                 i.currency_code,
                                 i.chain,
                                 i.chain_name,
                                 i.store_day_seq_no,
                                 i.data_status,
                                 i.data_status_desc,
                                 i.audit_status,
                                 i.audit_status_desc,
                                 i.store_status,
                                 i.store_status_desc,
                                 i.audit_changed_datetime,
                                 i.files_loaded,
                                 i.transaction_count,
                                 i.files_expected,
                                 i.err_cnt,
                                 i.business_date,
                                 i.start_date,
                                 i.end_date,
                                 i.sys_value os_value,
                                 2 total_level
                            from (select s.total_seq_no,
                                         --
                                         t.record_type,
                                         t.record_date,
                                         t.store,
                                         t.store_name,
                                         t.auditors,
                                         t.currency_code,
                                         t.chain,
                                         t.chain_name,
                                         t.store_day_seq_no,
                                         t.data_status,
                                         t.data_status_desc,
                                         t.audit_status,
                                         t.audit_status_desc,
                                         t.store_status,
                                         t.store_status_desc, 
                                         t.audit_changed_datetime,
                                         t.files_loaded,
                                         t.transaction_count,
                                         t.files_expected,
                                         t.err_cnt,
                                         t.business_date,
                                         t.start_date,
                                         t.end_date,
                                         --
                                         s.sys_value,
                                         s.value_rev_no,
                                         max(s.value_rev_no) over(partition by t.store_day_seq_no) max_rev
                                    from sa_sys_value s,
                                         table (cast(L_store_query AS MBL_SA_STORE_QUERY_TBL)) t
                                   where s.total_seq_no = t.total_seq_no) i
                           where i.value_rev_no = i.max_rev
                          --
                          union all
                          --totals for pos
                          select i.record_type,
                                 i.record_date,
                                 i.total_seq_no,
                                 i.store,
                                 i.store_name,
                                 i.auditors,
                                 i.currency_code,
                                 i.chain,
                                 i.chain_name,
                                 i.store_day_seq_no,
                                 i.data_status,
                                 i.data_status_desc,
                                 i.audit_status,
                                 i.audit_status_desc,
                                 i.store_status,
                                 i.store_status_desc,
                                 i.audit_changed_datetime,
                                 i.files_loaded,
                                 i.transaction_count,
                                 i.files_expected,
                                 i.err_cnt,
                                 i.business_date,
                                 i.start_date,
                                 i.end_date,
                                 i.pos_value os_value,
                                 3 total_level
                            from (select p.total_seq_no,
                                         --
                                         t.record_type,
                                         t.record_date,
                                         t.store,
                                         t.store_name,
                                         t.auditors,
                                         t.currency_code,
                                         t.chain,
                                         t.chain_name,
                                         t.store_day_seq_no,
                                         t.data_status,
                                         t.data_status_desc,
                                         t.audit_status,
                                         t.audit_status_desc,
                                         t.store_status,
                                         t.store_status_desc,
                                         t.audit_changed_datetime,
                                         t.files_loaded,
                                         t.transaction_count,
                                         t.files_expected,
                                         t.err_cnt,
                                         t.business_date,
                                         t.start_date,
                                         t.end_date,
                                         --
                                         p.pos_value,
                                         p.value_rev_no,
                                         max(p.value_rev_no) over(partition by t.store_day_seq_no) max_rev
                                    from sa_pos_value p,
                                         table (cast(L_store_query AS MBL_SA_STORE_QUERY_TBL)) t
                                   where p.total_seq_no = t.total_seq_no) i
                           where i.value_rev_no = i.max_rev
                          union all
                          --no totals
                          select t.record_type,
                                 t.record_date,
                                 t.total_seq_no,
                                 t.store,
                                 t.store_name,
                                 t.auditors,
                                 t.currency_code,
                                 t.chain,
                                 t.chain_name,
                                 t.store_day_seq_no,
                                 t.data_status,
                                 t.data_status_desc,
                                 t.audit_status,
                                 t.audit_status_desc,
                                 t.store_status,
                                 t.store_status_desc,
                                 t.audit_changed_datetime,
                                 t.files_loaded,
                                 t.transaction_count,
                                 t.files_expected,
                                 t.err_cnt,
                                 t.business_date,
                                 t.start_date,
                                 t.end_date,
                                 --
                                 0 os_value,
                                 4 total_level
                            from table(cast(L_store_query AS MBL_SA_STORE_QUERY_TBL)) t 
                         ) i2
                 ) i3
           where i3.total_level = i3.min_total_level) i4
       where i4.counter    >= L_row_start
         and i4.counter     < L_row_end
       order by i4.counter;

BEGIN

   select fuel_dept
     into L_fuel_dept
     from sa_system_options;

   --setup gtt with concatonated auditor string
   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(number_1, varchar2_1)
      select distinct i2.store, 
             substr(stragg(i2.user_id) over (partition by i2.store), 1, 255)
        from (select i.store,
                     i.user_id,
                     row_number() over (partition by i.store order by i.user_id) rn
                from (select distinct m.store,
                                      u.user_id
                        from sa_user_loc_traits u,
                             loc_traits_matrix m
                       where m.loc_trait = u.loc_trait
                         and UPPER(u.user_id)   not in(UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                                    SYS_CONTEXT('USERENV', 'SESSION_USER'))))
                     ) i
              ) i2
       where i2.rn <= 2;

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   
   open c_store_query;
   fetch c_store_query bulk collect into L_store_query;
   close c_store_query;   

   open c_store_day;
   fetch c_store_day bulk collect into O_mbl_sa_store_day_tbl, L_counter_table;
   close c_store_day;

   if L_counter_table       is NULL or 
      L_counter_table.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.first);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_store_query%ISOPEN then close c_store_query; end if;
      if c_store_day%ISOPEN then close c_store_day; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END GET_STORE_DAYS;
--------------------------------------------------------------------------
FUNCTION GET_STORE_AGGREGATIONS(O_error_message                 IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_mbl_sa_store_aggregation_tbl  IN OUT mbl_sa_store_aggregation_tbl,
                                IO_pagination                   IN OUT mbl_pagination_rec,
                                I_all_older_ind                 IN     VARCHAR2,
                                I_store_tbl                     IN     OBJ_NUMERIC_ID_TABLE, --optional
                                I_sort_attrib                   IN     VARCHAR2,
                                I_sort_direction                IN     VARCHAR2)
RETURN NUMBER IS

   L_program            varchar2(61)   := 'MBL_RESA_SVC.GET_STORE_AGGREGATIONS';
   L_vdate              DATE           := GET_VDATE;
   L_store_tbl          OBJ_NUMERIC_ID_TABLE;
   L_row_start          NUMBER(10)   := 0;
   L_row_end            NUMBER(10)   := 0;
   L_num_rec_found      NUMBER(10)   := 0; 
   L_counter_table      OBJ_NUMERIC_ID_TABLE;

   cursor c_store_agg is
      with store_days as 
      (select sd.store,
              sd.store_day_seq_no,
              --
              s.store_name,
              s.chain,
              ch.chain_name,
              --
              s.currency_code,
              gtt.varchar2_1 auditors,
              1 open_day_cnt,
              --
              (select count(se.error_seq_no)
                 from sa_error se
                where se.store_day_seq_no = sd.store_day_seq_no
                  and se.store            = sd.store
                  and se.day              = sd.day
                  and se.hq_override_ind  = 'N') error_cnt,
              --
              (select st.total_seq_no
                 from sa_total st
                where st.store_day_seq_no = sd.store_day_seq_no
                  and st.store            = sd.store
                  and st.day              = sd.day
                  and st.total_id         = 'OVRSHT_S'
                  and st.bal_group_seq_no is NULL) total_seq_no
         from gtt_num_num_str_str_date_date gtt,
              table (cast(L_store_tbl as OBJ_NUMERIC_ID_TABLE)) input_stores,
              v_store s,
              sa_store_day sd,
              v_chain ch
        where s.store               = gtt.number_1(+)
          and s.store               = value(input_stores)
          and s.chain               = ch.chain
          and s.store               = sd.store
          and sd.audit_status      != 'A'
          and sd.data_status       != 'G'
          and sd.business_date     <= decode(I_all_older_ind, MBL_RESA_SVC.RECORD_TYPE_ALL, L_vdate-1,
                                                              MBL_RESA_SVC.RECORD_TYPE_OLDER, L_vdate-6))
      --
      select mbl_sa_store_aggregation_rec(i4.store,
                                          i4.store_name,
                                          i4.chain,
                                          i4.chain_name,
                                          i4.auditors,
                                          i4.open_day_cnt,
                                          i4.over_days,
                                          i4.short_days,
                                          i4.over_amount,
                                          i4.short_amount,
                                          i4.currency_code,
                                          i4.error_cnt),
             i4.total_rows    
        from (select i3.store,
                     i3.store_name,
                     i3.chain,
                     i3.chain_name,
                     i3.currency_code,
                     i3.auditors,
                     sum(i3.open_day_cnt) open_day_cnt,
                     sum(i3.error_cnt) error_cnt,
                     sum(decode(sign(i3.os_value), 1,1,0)) over_days,
                     sum(decode(sign(i3.os_value), -1,1,0)) short_days,
                     sum(decode(sign(i3.os_value), 1,i3.os_value,0)) over_amount,
                     sum(decode(sign(i3.os_value), -1,i3.os_value,0)) short_amount,
                     --
                     case when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_STORE_NAME and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_ASC then
                             row_number() over (order by i3.store_name asc, i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_AUDITOR and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_ASC then
                             row_number() over (order by i3.auditors asc, i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_ERROR_COUNT and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_ASC then
                             row_number() over (order by sum(i3.error_cnt) asc, i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_OPEN_DAYS and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_ASC then
                             row_number() over (order by sum(i3.open_day_cnt) asc, i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_OS_DAYS and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_ASC then
                             row_number() over (order by sum(decode(sign(i3.os_value), 1,1,0)) + sum(decode(sign(i3.os_value), -1,1,0)) asc,
                                                         i3.store)
                          when I_sort_attrib = MBL_RESA_SVC.SORT_BY_OS_SUMS and 
                               I_sort_direction     = MBL_RESA_SVC.ORDER_ASC then
                             row_number() over (order by abs(sum(decode(sign(i3.os_value), 1,i3.os_value,0))) + 
                                                           abs(sum(decode(sign(i3.os_value), -1,i3.os_value,0))) asc,
                                                         i3.store)
                          --
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_STORE_NAME and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_DESC then
                             row_number() over (order by i3.store_name desc, i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_AUDITOR and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_DESC then
                             row_number() over (order by i3.auditors desc, i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_ERROR_COUNT and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_DESC then
                             row_number() over (order by sum(i3.error_cnt) desc, i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_OPEN_DAYS and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_DESC then
                             row_number() over (order by sum(i3.open_day_cnt) desc, i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_OS_DAYS and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_DESC then
                             row_number() over (order by sum(decode(sign(i3.os_value), 1,1,0)) + sum(decode(sign(i3.os_value), -1,1,0)) desc, 
                                                         i3.store)
                          when I_sort_attrib    = MBL_RESA_SVC.SORT_BY_OS_SUMS and 
                               I_sort_direction = MBL_RESA_SVC.ORDER_DESC then
                             row_number() over (order by abs(sum(decode(sign(i3.os_value), 1,i3.os_value,0))) +
                                                           abs(sum(decode(sign(i3.os_value), -1,i3.os_value,0))) desc, 
                                                         i3.store)
                          else
                             row_number() over (order by i3.store_name asc)
                     end counter,
                     --
                     count(*) over () total_rows
                from (--figure out level to use
                      select i2.store,
                             i2.store_day_seq_no,
                             i2.store_name,
                             i2.chain,
                             i2.chain_name,
                             i2.currency_code,
                             i2.auditors,
                             i2.open_day_cnt,
                             i2.error_cnt,
                             i2.os_value,
                             i2.total_level,
                             min(total_level) over (partition by i2.store_day_seq_no) min_total_level
                        from (--totals for hq
                              select i.store,
                                     i.store_day_seq_no,
                                     i.store_name,
                                     i.chain,
                                     i.chain_name,
                                     i.currency_code,
                                     i.auditors,
                                     i.open_day_cnt,
                                     i.error_cnt,
                                     i.total_seq_no,
                                     --
                                     i.hq_value os_value,
                                     1 total_level
                                from (select sds.store,
                                             sds.store_day_seq_no,
                                             sds.store_name,
                                             sds.chain,
                                             sds.chain_name,
                                             sds.currency_code,
                                             sds.auditors,
                                             sds.open_day_cnt,
                                             sds.error_cnt,
                                             sds.total_seq_no,
                                             h.hq_value,
                                             h.value_rev_no,
                                             max(h.value_rev_no) over(partition by sds.store_day_seq_no) max_rev
                                        from sa_hq_value h,
                                             store_days sds
                                       where h.total_seq_no = sds.total_seq_no) i
                               where i.value_rev_no = i.max_rev
                              --
                              union all
                              --totals for sys
                              select i.store,
                                     i.store_day_seq_no,
                                     i.store_name,
                                     i.chain,
                                     i.chain_name,
                                     i.currency_code,
                                     i.auditors,
                                     i.open_day_cnt,
                                     i.error_cnt,
                                     i.total_seq_no,
                                     --
                                     i.sys_value os_value,
                                     2 total_level
                                from (select sds.store,
                                             sds.store_day_seq_no,
                                             sds.store_name,
                                             sds.chain,
                                             sds.chain_name,
                                             sds.currency_code,
                                             sds.auditors,
                                             sds.open_day_cnt,
                                             sds.error_cnt,
                                             sds.total_seq_no,
                                             s.sys_value,
                                             s.value_rev_no,
                                             max(s.value_rev_no) over(partition by sds.store_day_seq_no) max_rev
                                        from sa_sys_value s,
                                             store_days sds
                                       where s.total_seq_no = sds.total_seq_no) i
                               where i.value_rev_no = i.max_rev
                              --
                              union all
                              --totals for pos
                              select i.store,
                                     i.store_day_seq_no,
                                     i.store_name,
                                     i.chain,
                                     i.chain_name,
                                     i.currency_code,
                                     i.auditors,
                                     i.open_day_cnt,
                                     i.error_cnt,
                                     i.total_seq_no,
                                     --
                                     i.pos_value os_value,
                                     3 total_level
                                from (select sds.store,
                                             sds.store_day_seq_no,
                                             sds.store_name,
                                             sds.chain,
                                             sds.chain_name,
                                             sds.currency_code,
                                             sds.auditors,
                                             sds.open_day_cnt,
                                             sds.error_cnt,
                                             sds.total_seq_no,
                                             p.pos_value,
                                             p.value_rev_no,
                                             max(p.value_rev_no) over(partition by sds.store_day_seq_no) max_rev
                                        from sa_pos_value p,
                                             store_days sds
                                       where p.total_seq_no = sds.total_seq_no) i
                               where i.value_rev_no = i.max_rev
                              union all
                              --no totals
                              select sds.store,
                                     sds.store_day_seq_no,
                                     sds.store_name,
                                     sds.chain,
                                     sds.chain_name,
                                     sds.currency_code,
                                     sds.auditors,
                                     sds.open_day_cnt,
                                     sds.error_cnt,
                                     sds.total_seq_no,
                                     0 os_value,
                                     4 total_level
                                from store_days sds
                             ) i2
                     ) i3
               where i3.total_level = i3.min_total_level
               group by i3.store,
                        i3.store_name,
                        i3.chain,
                        i3.chain_name,
                        i3.currency_code,
                        i3.auditors) i4
       where i4.counter    >= L_row_start
         and i4.counter     < L_row_end
       order by i4.counter;

BEGIN

   --filter by passed store table if populated.
   if I_store_tbl is null or I_store_tbl.count <= 0 then
      select distinct m.store
             bulk collect into L_store_tbl
                 from sa_user_loc_traits t,
                      LOC_TRAITS_MATRIX M
                where UPPER(t.user_id)   = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                        SYS_CONTEXT('USERENV', 'SESSION_USER')))
                  and t.loc_trait = m.loc_trait;

   else
      L_store_tbl := I_store_tbl;
   end if;

   --setup gtt with concatonated auditor string
   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(number_1, varchar2_1)
      select distinct i2.store,
             substr(stragg(i2.user_id) over (partition by i2.store), 1, 255)
        from (select i.store,
                     i.user_id,
                     row_number() over (partition by i.store order by i.user_id) rn
                from (select distinct m.store,
                                      u.user_id
                        from sa_user_loc_traits u,
                             loc_traits_matrix m
                       where m.loc_trait = u.loc_trait
                         and UPPER(u.user_id)   not in UPPER((NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                                    SYS_CONTEXT('USERENV', 'SESSION_USER'))))
                     ) i
              ) i2
       where i2.rn <= 2;

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open c_store_agg;
   fetch c_store_agg bulk collect into O_mbl_sa_store_aggregation_tbl, L_counter_table;
   close c_store_agg;

   if L_counter_table       is NULL or
      L_counter_table.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.first);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_store_agg%ISOPEN then close c_store_agg; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_STORE_AGGREGATIONS;
--------------------------------------------------------------------------
FUNCTION LOC_SEARCH(O_error_message                 IN OUT VARCHAR2,
                    O_mbl_sa_store_aggregation_tbl  IN OUT mbl_sa_store_aggregation_tbl,
                    IO_pagination                   IN OUT mbl_pagination_rec,
                    I_search_string                 IN     VARCHAR2,
                    I_search_filter                 IN     VARCHAR2,
                    I_sort_attrib                   IN     VARCHAR2,
                    I_sort_direction                IN     VARCHAR2)
RETURN NUMBER IS

   L_program          varchar2(61)          := 'MBL_RESA_SVC.LOC_SEARCH';
   L_stores           OBJ_NUMERIC_ID_TABLE;

   cursor c_store is
     select s.store
       from v_store s,
            sa_user_loc_traits t,
            loc_traits_matrix m
      where (s.store             like('%'||nvl(I_search_string,'-90909')||'%')
             or
             lower(s.store_name) like('%'||lower(nvl(I_search_string,'-90909'))||'%')
             or
             lower(t.user_id) like('%'||lower(nvl(I_search_string,'-90909'))||'%'))
        and t.loc_trait     = m.loc_trait
        and m.store         = s.store
        and I_search_filter = MBL_RESA_SVC.SEARCH_ALL_STORES
     union all
     select s.store
       from (select m.store,
                    st.store_name,
                    t.user_id
               from sa_user_loc_traits t,
                    loc_traits_matrix m,
                    store st
              where UPPER(t.user_id)   = UPPER(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), 
                                      SYS_CONTEXT('USERENV', 'SESSION_USER')))
                and t.loc_trait = m.loc_trait
                and m.store     = st.store) s
      where (s.store             like('%'||nvl(I_search_string,'-90909')||'%')
             or
             lower(s.store_name) like('%'||lower(nvl(I_search_string,'-90909'))||'%')
             or
             lower(s.user_id) like('%'||lower(nvl(I_search_string,'-90909'))||'%'))
        and I_search_filter = MBL_RESA_SVC.SEARCH_ASSIGN_STORES;

BEGIN

   open c_store;
   fetch c_store bulk collect into L_stores;
   close c_store;

   if L_stores is NULL or L_stores.count <= 0 then
   
      O_mbl_sa_store_aggregation_tbl := new mbl_sa_store_aggregation_tbl();

      if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                         IO_pagination,
                                         0) = MBL_CONSTANTS.FAILURE then
         RETURN MBL_CONSTANTS.FAILURE;
      end if;

   else

      if GET_STORE_AGGREGATIONS(O_error_message,
                                O_mbl_sa_store_aggregation_tbl,
                                IO_pagination,
                                'ALL',
                                L_stores,
                                I_sort_attrib,
                                I_sort_direction) = MBL_CONSTANTS.FAILURE then
         RETURN MBL_CONSTANTS.FAILURE;
      end if;

   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_store%ISOPEN then close c_store; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END LOC_SEARCH; 
--------------------------------------------------------------------------
FUNCTION GET_STORE_DATE_INDS(O_error_message                 IN OUT VARCHAR2,
                             O_mbl_sa_store_date_ind_tbl     IN OUT MBL_SA_STORE_DATE_IND_TBL,
                             I_store                         IN     STORE.STORE%TYPE)
RETURN NUMBER IS

   L_program      varchar2(61)     := 'MBL_RESA_SVC.GET_STORE_DATE_INDS';

   cursor c_store is
      select MBL_SA_STORE_DATE_IND_REC(
             record_type,
             record_date,
             value_indicator)
        from (select inner.record_type,
                     CASE WHEN inner.record_type = MBL_RESA_SVC.RECORD_TYPE_DATE THEN 
                               inner.end_date
                          ELSE NULL 
                     END record_date,
                     CASE WHEN MIN(sd.store_day_seq_no) is not NULL THEN
                          1 
                          ELSE 0 
                     END value_indicator
                from (select s.store,
                             gtt.varchar2_1 record_type,
                             gtt.date_1 start_date,
                             gtt.date_2 end_date,
                             gtt.number_1 record_id
                        from v_store s,
                             gtt_num_num_str_str_date_date gtt
                       where s.store = I_store) inner,
                     sa_store_day sd
               where inner.store         = sd.store(+)
                 and inner.start_date   >= sd.business_date(+)
                 and inner.end_date     <= sd.business_date(+)
                 and sd.audit_status(+) != 'A'
                 and sd.data_status(+)  != 'G'
               group by inner.record_id,
                        inner.record_type,
                        inner.end_date
               order by inner.record_id);


BEGIN

   if POP_SUM_DATES(O_error_message) = FALSE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   
   open c_store;
   fetch c_store bulk collect into O_mbl_sa_store_date_ind_tbl;
   close c_store;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_store%ISOPEN then close c_store; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_STORE_DATE_INDS;
--------------------------------------------------------------------------
--------------------------------------------------------------------------
END MBL_RESA_SVC;
/
