create or replace PACKAGE BODY STG_SVC_SA_ROUNDING_RULE
AS
    Type s9t_errors_tab_typ
    IS
    TABLE OF s9t_errors%rowtype;
    Lp_s9t_errors_tab s9t_errors_tab_typ;

    FUNCTION populate_lists(
     O_error_message IN OUT rtk_errors.rtk_text%type,
     I_file_id       IN NUMBER)RETURN BOOLEAN;
     -------------------------------------------------------------------------------
    ---  Name: write_s9t_error
    --- Purpose: For adding the encountered errors in to an object
    -------------------------------------------------------------------------------- 
    PROCEDURE write_s9t_error(
         I_file_id IN s9t_errors.file_id%type,
         I_sheet   IN VARCHAR2,
         I_row_seq IN NUMBER,
         I_col     IN VARCHAR2,
         I_sqlcode IN NUMBER,
         I_sqlerrm IN VARCHAR2)
    IS
    BEGIN
        Lp_s9t_errors_tab.extend();
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).FILE_ID              := I_file_id;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_SEQ_NO         := s9t_errors_seq.nextval;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).TEMPLATE_KEY         := stg_svc_sa_rounding_rule.template_key;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).WKSHT_KEY            := I_sheet;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).COLUMN_KEY           := I_col;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ROW_SEQ              := I_row_seq;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_ID            := GET_USER;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_DATETIME      := sysdate;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_update_ID       := GET_USER;
        Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_update_DATETIME := sysdate;
    end write_s9t_error;
     -------------------------------------------------------------------------------
    ---  Name: populate_names
    -------------------------------------------------------------------------------- 
    PROCEDURE populate_names
      (
         I_file_id NUMBER
      )
    IS
        l_sheets s9t_pkg.names_map_typ;
        SA_RR_HEAD_cols s9t_pkg.names_map_typ;
        SA_RR_HEAD_TL_cols s9t_pkg.names_map_typ;
        SA_RR_DETAIL_cols s9t_pkg.names_map_typ;
    BEGIN
        l_sheets                          :=s9t_pkg.get_sheet_names(I_file_id);
        SA_RR_HEAD_cols                   :=s9t_pkg.get_col_names(I_file_id,SA_RR_HEAD_sheet);
        SA_RR_HEAD$Action                 := SA_RR_HEAD_cols('ACTION');
        SA_RR_HEAD$STATUS                 := SA_RR_HEAD_cols('STATUS');
        SA_RR_HEAD$START_BUSINESS_DATE    := SA_RR_HEAD_cols('START_BUSINESS_DATE');
        SA_RR_HEAD$COUNTRY_ID             := SA_RR_HEAD_cols('COUNTRY_ID');
        SA_RR_HEAD$CURRENCY_CODE          := SA_RR_HEAD_cols('CURRENCY_CODE');
        SA_RR_HEAD$ROUNDING_RULE_NAME     := SA_RR_HEAD_cols('ROUNDING_RULE_NAME');
        SA_RR_HEAD$ROUNDING_RULE_ID       := SA_RR_HEAD_cols('ROUNDING_RULE_ID');
        
        SA_RR_HEAD_TL_cols                :=s9t_pkg.get_col_names(I_file_id,SA_RR_HEAD_TL_sheet);
        SA_RR_HEAD_TL$Action              := SA_RR_HEAD_TL_cols('ACTION');
        SA_RR_HEAD_TL$LANG                := SA_RR_HEAD_TL_cols('LANG');
        SA_RR_HEAD_TL$ROUNDING_RULE_ID    := SA_RR_HEAD_TL_cols('ROUNDING_RULE_ID');
        SA_RR_HEAD_TL$ROUNDING_RULE_NM    := SA_RR_HEAD_TL_cols('ROUNDING_RULE_NAME');
        
        SA_RR_DETAIL_cols                 :=s9t_pkg.get_col_names(I_file_id,SA_RR_DETAIL_sheet);
        SA_RR_DETAIL$Action               := SA_RR_DETAIL_cols('ACTION');
        SA_RR_DETAIL$ROUND_AMT            := SA_RR_DETAIL_cols('ROUND_AMT');
        SA_RR_DETAIL$HIGH_ENDING_AMT      := SA_RR_DETAIL_cols('HIGH_ENDING_AMT');
        SA_RR_DETAIL$LOW_ENDING_AMT       := SA_RR_DETAIL_cols('LOW_ENDING_AMT');
        SA_RR_DETAIL$ROUNDING_RULE_ID     := SA_RR_DETAIL_cols('ROUNDING_RULE_ID');
    end populate_names;

     -------------------------------------------------------------------------------
    ---  Name: populate_SA_RR_HEAD 
    --Purpose: Populate data from SA_ROUNDING_RULE_HEAD
    -------------------------------------------------------------------------------- 
    PROCEDURE populate_SA_RR_HEAD
      (
         I_file_id IN NUMBER
      )
    IS
    BEGIN
        insert
        INTO TABLE
         (select ss.s9t_rows
            from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
            where sf.file_id  = I_file_id
            and ss.sheet_name = SA_RR_HEAD_sheet
         )
        select s9t_row(s9t_cells( stg_svc_sa_rounding_rule.action_mod
                                         ,ROUNDING_RULE_ID
                                         ,ROUNDING_RULE_NAME
                                         ,CURRENCY_CODE
                                         ,COUNTRY_ID
                                         ,START_BUSINESS_DATE
                                         ,STATUS ))
        from SA_ROUNDING_RULE_HEAD ;
    end populate_SA_RR_HEAD;
    --------------------------------------------------------------------------------
    ---  Name: POPULATE_SA_RR_HEAD_TL
    --Purpose: Populate data from SA_ROUNDING_RULE_HEAD_TL
    -------------------------------------------------------------------------------- 
    PROCEDURE POPULATE_SA_RR_HEAD_TL
      (
         I_file_id IN NUMBER
      )
    IS
    BEGIN
        insert
        into table
         (select ss.s9t_rows
            from s9t_folder sf,
              table(sf.s9t_file_obj.sheets) ss
            where sf.file_id  = I_file_id
            and ss.sheet_name = sa_rr_head_sheet
         )
        select s9t_row(s9t_cells( stg_svc_sa_rounding_rule.action_mod
                                 ,lang
                                 ,rounding_rule_id
                                 ,rounding_rule_name))
        from sa_rounding_rule_head_tl ;
    end POPULATE_SA_RR_HEAD_TL;
    -------------------------------------------------------------------------------
    ---  Name: populate_SA_RR_DETAIL 
    --Purpose: Populate data from SA_ROUNDING_RULE_DETAIL
    -------------------------------------------------------------------------------- 
    PROCEDURE populate_SA_RR_DETAIL
      (
         I_file_id IN NUMBER
      )
    IS
    BEGIN
        insert
        INTO TABLE
         (select ss.s9t_rows
            from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
            where sf.file_id  = I_file_id
            and ss.sheet_name = SA_RR_DETAIL_sheet
         )
        select s9t_row(s9t_cells( NULL
        ,ROUNDING_RULE_ID,LOW_ENDING_AMT,HIGH_ENDING_AMT
                                         ,ROUND_AMT
                                    ))
        from SA_ROUNDING_RULE_DETAIL ;
    end populate_SA_RR_DETAIL;
    -------------------------------------------------------------------------------
    ---  Name: init_s9t 
    -------------------------------------------------------------------------------- 
    PROCEDURE init_s9t(
         O_file_id IN OUT NUMBER)
    IS
      l_file s9t_file;
      l_file_name s9t_folder.file_name%type;
      
    BEGIN
        l_file              := NEW s9t_file();
        O_file_id           := s9t_folder_seq.nextval;
        l_file.file_id      := O_file_id;
        l_file_name         := stg_svc_sa_rounding_rule.template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
        l_file.file_name    := l_file_name;
        l_file.template_key := stg_svc_sa_rounding_rule.template_key;
        l_file.user_lang    := GET_USER_LANG;
        l_file.add_sheet(SA_RR_HEAD_sheet);
        l_file.sheets(l_file.get_sheet_index(SA_RR_HEAD_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                                                        ,'ROUNDING_RULE_ID'
                                                                                                                        ,'ROUNDING_RULE_NAME'
                                                                                                                         ,'CURRENCY_CODE'
                                                                                                                            ,'COUNTRY_ID'
                                                                                                                            ,'START_BUSINESS_DATE'
                                                                                                                              ,'STATUS'
                                                                                                                                );
                                                                                                                                
        l_file.add_sheet(SA_RR_HEAD_TL_sheet);
        l_file.sheets(l_file.get_sheet_index(SA_RR_HEAD_TL_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                               ,'LANG' 
                                                                                               ,'ROUNDING_RULE_ID'
                                                                                               ,'ROUNDING_RULE_NAME');
        l_file.add_sheet(SA_RR_DETAIL_sheet);
        l_file.sheets(l_file.get_sheet_index(SA_RR_DETAIL_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                                                             ,'ROUNDING_RULE_ID'
                                                                                                                             ,'LOW_ENDING_AMT'
                                                                                                                             ,'HIGH_ENDING_AMT'
                                                                                                                              ,'ROUND_AMT'
                                                                                                                              );

        s9t_pkg.save_obj(l_file);
    end init_s9t;
    -------------------------------------------------------------------------------
    ---  Name: create_s9t 
    --Purpose: This is called from the UI for creating the download excel
    -------------------------------------------------------------------------------- 
    FUNCTION create_s9t(
         O_error_message     IN OUT rtk_errors.rtk_text%type,
         O_file_id           IN OUT s9t_folder.file_id%type,
         I_template_only_ind IN CHAR DEFAULT 'N')RETURN BOOLEAN
    IS
      l_file s9t_file;
      L_program VARCHAR2(255):='SA_ROUNDING_RULE.CREATE_S9T';
    BEGIN
        init_s9t(O_file_id);
        if populate_lists(O_error_message,O_file_id)=false then
        ---populate the column lists
         RETURN false;
        end if;
        if I_template_only_ind = 'N' then
        --populate data
            populate_SA_RR_HEAD(O_file_id);
            populate_SA_RR_HEAD_TL(O_file_id);
            populate_SA_RR_DETAIL(O_file_id);
            COMMIT;
        end if;
        s9t_pkg.translate_to_user_lang(O_file_id);
        s9t_pkg.apply_template(O_file_id,stg_svc_sa_rounding_rule.template_key);
        l_file:=s9t_file(O_file_id);
        if s9t_pkg.code2desc(O_error_message,
                             'RSACR',
                         l_file)=FALSE then
       return FALSE;
        end if;
        s9t_pkg.save_obj(l_file);
        s9t_pkg.update_ods(l_file);
        COMMIT;
        RETURN true;
    EXCEPTION
    WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
    end create_s9t;
    -------------------------------------------------------------------------------
    ---  Name: process_s9t_SA_RR_HEAD 
    --Purpose: To populate data in staging table- SVC_SA_ROUNDING_RULE_HEAD
    -------------------------------------------------------------------------------- 
    PROCEDURE process_s9t_SA_RR_HEAD
      (
         I_file_id    IN s9t_folder.file_id%type,
         I_process_id IN SVC_SA_ROUNDING_RULE_HEAD.process_id%type
      )
    IS
    Type svc_SA_RR_HEAD_col_typ
    IS
        TABLE OF SVC_SA_ROUNDING_RULE_HEAD%rowtype;
        l_temp_rec SVC_SA_ROUNDING_RULE_HEAD%rowtype;
        svc_SA_ROUNDING_RULE_HEAD_col svc_SA_RR_HEAD_col_typ :=NEW svc_SA_RR_HEAD_col_typ();
        l_process_id SVC_SA_ROUNDING_RULE_HEAD.process_id%type;
        l_error BOOLEAN:=false;
        l_default_rec SVC_SA_ROUNDING_RULE_HEAD%rowtype;
        CURSOR c_mandatory_ind
        IS
         select
          ROUNDING_RULE_ID_mi,
            ROUNDING_RULE_NAME_mi,
             CURRENCY_CODE_mi,
             COUNTRY_ID_mi,
             START_BUSINESS_DATE_mi,
            STATUS_mi, 1 as dummy
         from
            (select column_key,
              mandatory
            from s9t_tmpl_cols_def
            where template_key                              = stg_svc_sa_rounding_rule.template_key
            and wksht_key                                   = 'SA_ROUNDING_RULE_HEAD'
            ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN (
             'ROUNDING_RULE_ID' AS ROUNDING_RULE_ID,
              'ROUNDING_RULE_NAME' AS ROUNDING_RULE_NAME,
                'CURRENCY_CODE' AS CURRENCY_CODE,
                 'COUNTRY_ID' AS COUNTRY_ID,
                     'START_BUSINESS_DATE' AS START_BUSINESS_DATE,
                                    'STATUS' AS STATUS,null as dummy));
        l_mi_rec c_mandatory_ind%rowtype;
        dml_errors EXCEPTION;
        PRAGMA exception_init(dml_errors, -24381);
        L_uk_columns   VARCHAR2(255) := 'currency_code,country_id';
        L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
        L_error_code   NUMBER;
    L_table        VARCHAR2(30)  := 'SVC_SA_ROUNDING_RULE_HEAD';
    BEGIN
      -- Get default values.
      FOR rec IN
      (select  ROUNDING_RULE_ID_dv,
                 ROUNDING_RULE_NAME_dv,
                  CURRENCY_CODE_dv,
                    COUNTRY_ID_dv,
                     START_BUSINESS_DATE_dv,
                STATUS_dv,
      null as dummy
      from
         (select column_key,
            default_value
         from s9t_tmpl_cols_def
         where template_key                                  = stg_svc_sa_rounding_rule.template_key
         and wksht_key                                       = 'SA_ROUNDING_RULE_HEAD'
         ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'ROUNDING_RULE_ID' AS ROUNDING_RULE_ID,
                 'ROUNDING_RULE_NAME' AS ROUNDING_RULE_NAME,
                 'CURRENCY_CODE' AS CURRENCY_CODE,
                    'COUNTRY_ID' AS COUNTRY_ID,
                      'START_BUSINESS_DATE' AS START_BUSINESS_DATE,
                 'STATUS' AS STATUS, NULL      
    AS        
    dummy))
     )
     
    LOOP  

     
    BEGIN  

     l_default_rec.STATUS := rec.STATUS_dv;  

     
    EXCEPTION  

     
    WHEN OTHERS then  

     write_s9t_error(I_file_id,'SA_ROUNDING_RULE_HEAD',NULL,'STATUS','INV_DEFAULT',SQLERRM);  

     
    end;

     
    BEGIN  

     l_default_rec.START_BUSINESS_DATE := rec.START_BUSINESS_DATE_dv;  

     
    EXCEPTION  

     
    WHEN OTHERS then  

     write_s9t_error(I_file_id,'SA_ROUNDING_RULE_HEAD',NULL,'START_BUSINESS_DATE','INV_DEFAULT',SQLERRM);  

     
    end;

     
    BEGIN  

     l_default_rec.COUNTRY_ID := rec.COUNTRY_ID_dv;  

     
    EXCEPTION  

     
    WHEN OTHERS then  

     write_s9t_error(I_file_id,'SA_ROUNDING_RULE_HEAD',NULL,'COUNTRY_ID','INV_DEFAULT',SQLERRM);  

     
    end;

     
    BEGIN  

     l_default_rec.CURRENCY_CODE := rec.CURRENCY_CODE_dv;  

     
    EXCEPTION  

     
    WHEN OTHERS then  

     write_s9t_error(I_file_id,'SA_ROUNDING_RULE_HEAD',NULL,'CURRENCY_CODE','INV_DEFAULT',SQLERRM);  

     
    end;

     
    BEGIN  

     l_default_rec.ROUNDING_RULE_NAME := rec.ROUNDING_RULE_NAME_dv;  

     
    EXCEPTION  

     
    WHEN OTHERS then  

     write_s9t_error(I_file_id,'SA_ROUNDING_RULE_HEAD',NULL,'ROUNDING_RULE_NAME','INV_DEFAULT',SQLERRM);  

     
    end;

     
    BEGIN  

     l_default_rec.ROUNDING_RULE_ID := rec.ROUNDING_RULE_ID_dv;  

     
    EXCEPTION  

     
    WHEN OTHERS then  

     write_s9t_error(I_file_id,'SA_ROUNDING_RULE_HEAD',NULL,'ROUNDING_RULE_ID','INV_DEFAULT',SQLERRM);  

     
    end;

     
    end LOOP;

     --Get mandatory indicators
        OPEN C_mandatory_ind;
        FETCH C_mandatory_ind
        INTO l_mi_rec;
        CLOSE C_mandatory_ind;
        FOR rec IN
        (select r.get_cell(SA_RR_HEAD$Action)      AS Action,
        upper(r.get_cell(SA_RR_HEAD$ROUNDING_RULE_ID))             AS ROUNDING_RULE_ID,
        r.get_cell(SA_RR_HEAD$ROUNDING_RULE_NAME)              AS ROUNDING_RULE_NAME,
         upper(r.get_cell(SA_RR_HEAD$CURRENCY_CODE))              AS CURRENCY_CODE,
          upper(r.get_cell(SA_RR_HEAD$COUNTRY_ID) )             AS COUNTRY_ID,
         r.get_cell(SA_RR_HEAD$START_BUSINESS_DATE)              AS START_BUSINESS_DATE,
         upper(r.get_cell(SA_RR_HEAD$STATUS))              AS STATUS,
          r.get_row_seq()                             AS row_seq
        from s9t_folder sf,
         TABLE(sf.s9t_file_obj.sheets) ss,
         TABLE(ss.s9t_rows) r
        where sf.file_id  = I_file_id
        and ss.sheet_name = sheet_name_trans(SA_RR_HEAD_sheet)
        )
        LOOP
         l_temp_rec.process_id        := I_process_id;
         l_temp_rec.chunk_id          := 1;
         l_temp_rec.row_seq           := rec.row_seq;
         l_temp_rec.process$status    := 'N';
         l_temp_rec.create_id         := GET_USER;
         l_temp_rec.last_upd_id       := GET_USER;
         l_temp_rec.create_datetime   := sysdate;
         l_temp_rec.last_upd_datetime := sysdate;
         l_error := false;
         BEGIN
            l_temp_rec.Action := rec.Action;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_HEAD_sheet,rec.row_seq,'ACTION',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.STATUS := rec.STATUS;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_HEAD_sheet,rec.row_seq,'STATUS',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.START_BUSINESS_DATE := rec.START_BUSINESS_DATE;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_HEAD_sheet,rec.row_seq,'START_BUSINESS_DATE',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.COUNTRY_ID := rec.COUNTRY_ID;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_HEAD_sheet,rec.row_seq,'COUNTRY_ID',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.CURRENCY_CODE := rec.CURRENCY_CODE;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_HEAD_sheet,rec.row_seq,'CURRENCY_CODE',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.ROUNDING_RULE_NAME := rec.ROUNDING_RULE_NAME;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_HEAD_sheet,rec.row_seq,'ROUNDING_RULE_NAME',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.ROUNDING_RULE_ID := rec.ROUNDING_RULE_ID;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_HEAD_sheet,rec.row_seq,'ROUNDING_RULE_ID',SQLCODE,SQLERRM);
            l_error := true;
         end;
         if rec.action = stg_svc_SA_ROUNDING_RULE.action_new
         then
            l_temp_rec.STATUS := NVL( l_temp_rec.STATUS,l_default_rec.STATUS);
            l_temp_rec.START_BUSINESS_DATE := NVL( l_temp_rec.START_BUSINESS_DATE,l_default_rec.START_BUSINESS_DATE);
            l_temp_rec.COUNTRY_ID := NVL( l_temp_rec.COUNTRY_ID,l_default_rec.COUNTRY_ID);
            l_temp_rec.CURRENCY_CODE := NVL( l_temp_rec.CURRENCY_CODE,l_default_rec.CURRENCY_CODE);
            l_temp_rec.ROUNDING_RULE_NAME := NVL( l_temp_rec.ROUNDING_RULE_NAME,l_default_rec.ROUNDING_RULE_NAME);
            l_temp_rec.ROUNDING_RULE_ID := upper(NVL( l_temp_rec.ROUNDING_RULE_ID,l_default_rec.ROUNDING_RULE_ID));
        end if;
         if not (
                    l_temp_rec.ROUNDING_RULE_ID is not null and
                 --  l_temp_rec.COUNTRY_ID is not null and
                --   l_temp_rec.CURRENCY_CODE is not null and
                    1 = 1
                    )
         then
            write_s9t_error(I_file_id,SA_RR_HEAD_sheet,rec.row_seq,NULL,NULL,SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', 'ROUNDING_RULE_ID'));
         
            l_error := true;
         end if;
         if NOT l_error then
            svc_SA_ROUNDING_RULE_HEAD_col.extend();
            svc_SA_ROUNDING_RULE_HEAD_col(svc_SA_ROUNDING_RULE_HEAD_col.count()):=l_temp_rec;
         end if;
        end LOOP;
        BEGIN

        forall i IN 1..svc_SA_ROUNDING_RULE_HEAD_col.count SAVE EXCEPTIONS Merge INTO SVC_SA_ROUNDING_RULE_HEAD st USING
        (select
         (CASE
            WHEN l_mi_rec.STATUS_mi    = 'N'
            and svc_SA_ROUNDING_RULE_HEAD_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.STATUS             IS NULL
            then mt.STATUS
            ELSE s1.STATUS
         end) AS STATUS,
         (CASE
            WHEN l_mi_rec.START_BUSINESS_DATE_mi    = 'N'
            and svc_SA_ROUNDING_RULE_HEAD_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.START_BUSINESS_DATE             IS NULL
            then mt.START_BUSINESS_DATE
            ELSE s1.START_BUSINESS_DATE
         end) AS START_BUSINESS_DATE,
         (CASE
            WHEN l_mi_rec.COUNTRY_ID_mi    = 'N'
            and svc_SA_ROUNDING_RULE_HEAD_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.COUNTRY_ID             IS NULL
            then mt.COUNTRY_ID
            ELSE s1.COUNTRY_ID
         end) AS COUNTRY_ID,
         (CASE
            WHEN l_mi_rec.CURRENCY_CODE_mi    = 'N'
            and svc_SA_ROUNDING_RULE_HEAD_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.CURRENCY_CODE             IS NULL
            then mt.CURRENCY_CODE
            ELSE s1.CURRENCY_CODE
         end) AS CURRENCY_CODE,
         (CASE
            WHEN l_mi_rec.ROUNDING_RULE_NAME_mi    = 'N'
            and svc_SA_ROUNDING_RULE_HEAD_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.ROUNDING_RULE_NAME             IS NULL
            then mt.ROUNDING_RULE_NAME
            ELSE s1.ROUNDING_RULE_NAME
         end) AS ROUNDING_RULE_NAME,
         (CASE
            WHEN l_mi_rec.ROUNDING_RULE_ID_mi    = 'N'
            and svc_SA_ROUNDING_RULE_HEAD_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.ROUNDING_RULE_ID             IS NULL
            then mt.ROUNDING_RULE_ID
            ELSE s1.ROUNDING_RULE_ID
         end) AS ROUNDING_RULE_ID,
         null as dummy
        from (select
          svc_SA_ROUNDING_RULE_HEAD_col(i).STATUS AS STATUS,
          svc_SA_ROUNDING_RULE_HEAD_col(i).START_BUSINESS_DATE AS START_BUSINESS_DATE,
          svc_SA_ROUNDING_RULE_HEAD_col(i).COUNTRY_ID AS COUNTRY_ID,
          svc_SA_ROUNDING_RULE_HEAD_col(i).CURRENCY_CODE AS CURRENCY_CODE,
          svc_SA_ROUNDING_RULE_HEAD_col(i).ROUNDING_RULE_NAME AS ROUNDING_RULE_NAME,
          svc_SA_ROUNDING_RULE_HEAD_col(i).ROUNDING_RULE_ID AS ROUNDING_RULE_ID,
          null as dummy
         from dual
         ) s1,
         SA_ROUNDING_RULE_HEAD mt
        where
        mt.ROUNDING_RULE_ID (+)     = s1.ROUNDING_RULE_ID   and
        mt.COUNTRY_ID (+)     = s1.COUNTRY_ID   and
        mt.CURRENCY_CODE (+)     = s1.CURRENCY_CODE   and
        1 = 1
        ) sq ON (
        st.ROUNDING_RULE_ID      = sq.ROUNDING_RULE_ID and
        --  st.COUNTRY_ID      = sq.COUNTRY_ID and
        --  st.CURRENCY_CODE      = sq.CURRENCY_CODE and
        svc_SA_ROUNDING_RULE_HEAD_col(i).ACTION IN (stg_svc_SA_ROUNDING_RULE.action_mod,stg_svc_SA_ROUNDING_RULE.action_del)
        )
        WHEN matched then
        update
        SET PROCESS_ID      = svc_SA_ROUNDING_RULE_HEAD_col(i).PROCESS_ID ,
         CHUNK_ID          = svc_SA_ROUNDING_RULE_HEAD_col(i).CHUNK_ID ,
         ACTION            = svc_SA_ROUNDING_RULE_HEAD_col(i).ACTION ,
         ROW_SEQ           = svc_SA_ROUNDING_RULE_HEAD_col(i).ROW_SEQ ,
         PROCESS$STATUS    = svc_SA_ROUNDING_RULE_HEAD_col(i).PROCESS$STATUS ,
         COUNTRY_ID              = sq.COUNTRY_ID ,
         ROUNDING_RULE_NAME              = sq.ROUNDING_RULE_NAME ,
         CURRENCY_CODE              = sq.CURRENCY_CODE ,
         STATUS              = sq.STATUS ,
         START_BUSINESS_DATE              = sq.START_BUSINESS_DATE ,
         CREATE_ID         = svc_SA_ROUNDING_RULE_HEAD_col(i).CREATE_ID ,
         CREATE_DATETIME   = svc_SA_ROUNDING_RULE_HEAD_col(i).CREATE_DATETIME ,
         LAST_UPD_ID       = svc_SA_ROUNDING_RULE_HEAD_col(i).LAST_UPD_ID ,
         LAST_UPD_DATETIME = svc_SA_ROUNDING_RULE_HEAD_col(i).LAST_UPD_DATETIME WHEN NOT matched then
        insert
         (
            PROCESS_ID ,
            CHUNK_ID ,
            ROW_SEQ ,
            ACTION ,
            PROCESS$STATUS ,
            STATUS ,
            START_BUSINESS_DATE ,
            COUNTRY_ID ,
            CURRENCY_CODE ,
            ROUNDING_RULE_NAME ,
            ROUNDING_RULE_ID ,
            CREATE_ID ,
            CREATE_DATETIME ,
            LAST_UPD_ID ,
            LAST_UPD_DATETIME
         )
         VALUES
         (
          svc_SA_ROUNDING_RULE_HEAD_col(i).PROCESS_ID ,
            svc_SA_ROUNDING_RULE_HEAD_col(i).CHUNK_ID ,
         svc_SA_ROUNDING_RULE_HEAD_col(i).ROW_SEQ ,
            svc_SA_ROUNDING_RULE_HEAD_col(i).ACTION ,
            svc_SA_ROUNDING_RULE_HEAD_col(i).PROCESS$STATUS ,
            sq.STATUS ,
            sq.START_BUSINESS_DATE ,
            sq.COUNTRY_ID ,
            sq.CURRENCY_CODE ,
            sq.ROUNDING_RULE_NAME ,
            sq.ROUNDING_RULE_ID ,
            svc_SA_ROUNDING_RULE_HEAD_col(i).CREATE_ID ,
            svc_SA_ROUNDING_RULE_HEAD_col(i).CREATE_DATETIME ,
            svc_SA_ROUNDING_RULE_HEAD_col(i).LAST_UPD_ID ,
            svc_SA_ROUNDING_RULE_HEAD_col(i).LAST_UPD_DATETIME
         );
      EXCEPTION
      WHEN DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               l_error_msg:=sql_lib.create_msg('DUP_REC_EXISTS',l_uk_columns,l_table);
            end if;
         
            write_s9t_error
            (
              I_file_id,SA_RR_HEAD_sheet,svc_SA_ROUNDING_RULE_HEAD_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
            )
            ;
         end LOOP;
      end;
    end process_s9t_SA_RR_HEAD;
-------------------------------------------------------------------------------
---  Name: PROCESS_S9T_SA_RR_HEAD_TL 
--Purpose: To populate data in staging table- SVC_SA_ROUNDING_RULE_HEAD_TL
-------------------------------------------------------------------------------- 
PROCEDURE PROCESS_S9T_SA_RR_HEAD_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id   IN   SVC_SA_ROUNDING_RULE_HEAD_TL.PROCESS_ID%TYPE)
IS
   TYPE SVC_SA_RRHEAD_TL_COL_TYP IS TABLE OF SVC_SA_ROUNDING_RULE_HEAD_TL%ROWTYPE;

   L_temp_rec                 SVC_SA_ROUNDING_RULE_HEAD_TL%ROWTYPE;
   SVC_SA_RRHEAD_TL_COL       SVC_SA_RRHEAD_TL_COL_TYP := NEW SVC_SA_RRHEAD_TL_COL_TYP();
   L_process_id               SVC_SA_ROUNDING_RULE_HEAD_TL.PROCESS_ID%TYPE;
   L_error                    BOOLEAN := FALSE;
   L_default_rec              SVC_SA_ROUNDING_RULE_HEAD_TL%ROWTYPE;
   L_table                    VARCHAR2(30)  := 'SVC_SA_ROUNDING_RULE_HEAD_TL';
   
   cursor C_MANDATORY_IND is
      select lang_mi,
             rounding_rule_id_mi,
             rounding_rule_name_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = STG_SVC_SA_ROUNDING_RULE.TEMPLATE_KEY
                 and wksht_key         = SA_RR_HEAD_TL_sheet
              ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('LANG' AS LANG
                                                                ,'ROUNDING_RULE_ID'     AS ROUNDING_RULE_ID
                                                                ,'ROUNDING_RULE_NAME'   AS ROUNDING_RULE_NAME));
   L_mi_rec c_mandatory_ind%rowtype;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_pk_columns varchar2(255) := 'error_code, rounding_rule_id';
   L_error_msg rtk_errors.rtk_text%type;
   L_error_code number;
   l_count number;
   BEGIN
   -- Get default values.
   FOR rec IN (select lang_dv,
                      rounding_rule_id_dv,
                      rounding_rule_name_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = STG_SVC_SA_ROUNDING_RULE.TEMPLATE_KEY
                          and wksht_key     = SA_RR_HEAD_TL_sheet
                       ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('LANG' AS LANG
                                                                             ,'ROUNDING_RULE_ID'     AS ROUNDING_RULE_ID
                                                                             ,'ROUNDING_RULE_NAME'   AS ROUNDING_RULE_NAME))
                       )

   LOOP  
    BEGIN  
        l_default_rec.lang := rec.lang_dv;  
    EXCEPTION  
       when others then  
           WRITE_S9T_ERROR(I_file_id,SA_RR_HEAD_TL_sheet,NULL,'LANG','INV_DEFAULT',SQLERRM);  
    END;
    BEGIN  
        l_default_rec.rounding_rule_id := rec.rounding_rule_id_dv;  
    EXCEPTION  
       when others then  
           WRITE_S9T_ERROR(I_file_id,SA_RR_HEAD_TL_sheet,NULL,'ROUNDING_RULE_ID','INV_DEFAULT',SQLERRM);  
    END;
    BEGIN
        l_default_rec.rounding_rule_name := rec.rounding_rule_name_dv;
    EXCEPTION
        when others then
            WRITE_S9T_ERROR(I_file_id,SA_RR_HEAD_TL_sheet,NULL,'ROUNDING_RULE_NAME','INV_DEFAULT',SQLERRM);
    END;
   END LOOP;

   --Get mandatory indicators
   OPEN C_MANDATORY_IND;
   FETCH C_MANDATORY_IND
   INTO L_mi_rec;
   CLOSE C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(SA_RR_HEAD_TL$Action)               AS action,
                     r.get_cell(SA_RR_HEAD_TL$lang)                 AS lang,
                     r.get_cell(SA_RR_HEAD_TL$rounding_rule_id)     AS rounding_rule_id,
                     r.get_cell(SA_RR_HEAD_TL$rounding_rule_nm)     AS rounding_rule_name,
                     r.get_row_seq()                                AS row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(SA_RR_HEAD_TL_sheet)
             )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := false;
      BEGIN
        L_temp_rec.Action := rec.Action;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_RR_HEAD_TL_sheet,rec.row_seq,'ACTION_COLUMN',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.lang := rec.lang;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_RR_HEAD_TL_sheet,rec.row_seq,'LANG',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.rounding_rule_id := rec.rounding_rule_id;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_RR_HEAD_TL_sheet,rec.row_seq,'ROUNDING_RULE_ID',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.rounding_rule_name := rec.rounding_rule_name;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_RR_HEAD_TL_sheet,rec.row_seq,'ROUNDING_RULE_NAME',SQLCODE,SQLERRM);
        L_error := true;
      END;

      if rec.action = STG_SVC_SA_ROUNDING_RULE.action_new THEN
         L_temp_rec.lang := upper(NVL( L_temp_rec.lang,l_default_rec.lang));
         L_temp_rec.rounding_rule_id := NVL( L_temp_rec.rounding_rule_id,l_default_rec.rounding_rule_id);
         L_temp_rec.rounding_rule_name := NVL( L_temp_rec.rounding_rule_name,l_default_rec.rounding_rule_name);
      end if;
      if L_temp_rec.rounding_rule_id is NULL then
        WRITE_S9T_ERROR(I_file_id,
                        SA_RR_HEAD_TL_sheet,
                        rec.row_seq,
                        NULL,
                        NULL,
                        sql_lib.create_msg('PK_COLS_REQUIRED',L_pk_columns));
        L_error := true;
      end if;
      if NOT L_error THEN
        SVC_SA_RRHEAD_TL_COL.extend();
        SVC_SA_RRHEAD_TL_COL(SVC_SA_RRHEAD_TL_COL.count()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..SVC_SA_RRHEAD_TL_COL.count SAVE EXCEPTIONS 
         merge into SVC_SA_ROUNDING_RULE_HEAD_TL st using
            (select
                   (case
                     when L_mi_rec.lang_mi    = 'N'
                      and SVC_SA_RRHEAD_TL_COL(i).action = coresvc_item.action_mod
                      and s1.lang             is null
                     then mt.lang
                     else s1.lang
                   end) as lang,
                   (case
                     when L_mi_rec.rounding_rule_id_mi    = 'N'
                      and SVC_SA_RRHEAD_TL_COL(i).action = coresvc_item.action_mod
                      and s1.rounding_rule_id             is null
                     then mt.rounding_rule_id
                     else s1.rounding_rule_id
                   end) as rounding_rule_id,
                   (case
                     when L_mi_rec.rounding_rule_name_mi    = 'N'
                      and SVC_SA_RRHEAD_TL_COL(i).action = coresvc_item.action_mod
                      and s1.rounding_rule_name             is null
                     then mt.rounding_rule_name
                     else s1.rounding_rule_name
                   end) as rounding_rule_name,
                   null as dummy
              from (select SVC_SA_RRHEAD_TL_COL(i).lang as lang,
                           SVC_SA_RRHEAD_TL_COL(i).rounding_rule_id as rounding_rule_id,
                           SVC_SA_RRHEAD_TL_COL(i).rounding_rule_name as rounding_rule_name
                      from dual) s1,
                    sa_rounding_rule_head_tl mt
             where mt.rounding_rule_id (+)     = s1.rounding_rule_id   and
                   mt.lang (+)           = s1.lang
            ) sq on (
                     st.rounding_rule_id  = sq.rounding_rule_id and
                     st.lang              = sq.lang and
                     SVC_SA_RRHEAD_TL_COL(i).action in (STG_SVC_SA_ROUNDING_RULE.action_mod,STG_SVC_SA_ROUNDING_RULE.action_del)
                    )
         WHEN matched THEN
            update
               set process_id         = SVC_SA_RRHEAD_TL_COL(i).process_id ,
                   chunk_id           = SVC_SA_RRHEAD_TL_COL(i).chunk_id ,
                   row_seq            = SVC_SA_RRHEAD_TL_COL(i).row_seq ,
                   action             = SVC_SA_RRHEAD_TL_COL(i).action ,
                   process$status     = SVC_SA_RRHEAD_TL_COL(i).process$status ,
                   rounding_rule_name = sq.rounding_rule_name 
         WHEN NOT matched THEN
            insert
                  (
                   process_id ,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   rounding_rule_id ,
                   rounding_rule_name ,
                   lang
                  )
            values
                  (
                   SVC_SA_RRHEAD_TL_COL(i).process_id ,
                   SVC_SA_RRHEAD_TL_COL(i).chunk_id ,
                   SVC_SA_RRHEAD_TL_COL(i).row_seq ,
                   SVC_SA_RRHEAD_TL_COL(i).action ,
                   SVC_SA_RRHEAD_TL_COL(i).process$status ,
                   sq.rounding_rule_id ,
                   sq.rounding_rule_name ,
                   sq.lang
                  );

EXCEPTION
   WHEN DML_ERRORS THEN
      FOR i IN 1..sql%bulk_exceptions.count
      LOOP
        L_error_code:=sql%bulk_exceptions(i).error_code;
        if L_error_code=1 then 
            L_error_code:=null;
            L_error_msg:=sql_lib.create_msg('DUP_REC_EXISTS',L_pk_columns,L_table);
        end if;
        WRITE_S9T_ERROR
        (
           I_file_id,SA_RR_HEAD_TL_sheet,SVC_SA_RRHEAD_TL_COL(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
        )
        ;
      END LOOP;
   END;
END PROCESS_S9T_SA_RR_HEAD_TL;
    -------------------------------------------------------------------------------
    ---  Name: process_s9t_SA_RR_DETAIL 
    --Purpose: To populate data in staging table- SVC_SA_ROUNDING_RULE_DETAIL
    -------------------------------------------------------------------------------- 
    PROCEDURE process_s9t_SA_RR_DETAIL
      (
         I_file_id    IN s9t_folder.file_id%type,
         I_process_id IN SVC_SA_ROUNDING_RULE_DETAIL.process_id%type
      )
    IS
    Type svc_SA_RR_DETAIL_col_typ
    IS
        TABLE OF SVC_SA_ROUNDING_RULE_DETAIL%rowtype;
        l_temp_rec SVC_SA_ROUNDING_RULE_DETAIL%rowtype;
        svc_SA_RR_DETAIL_col svc_SA_RR_DETAIL_col_typ :=NEW svc_SA_RR_DETAIL_col_typ();
        l_process_id SVC_SA_ROUNDING_RULE_DETAIL.process_id%type;
        l_error BOOLEAN:=false;
        l_default_rec SVC_SA_ROUNDING_RULE_DETAIL%rowtype;
        L_uk_columns   VARCHAR2(255) := 'rounding_rule_id,low_ending_amt,high_ending_amt';
        L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
        L_error_code   NUMBER;
        CURSOR c_mandatory_ind
        IS
         select
            ROUND_AMT_mi,
            HIGH_ENDING_AMT_mi,
            LOW_ENDING_AMT_mi,
            ROUNDING_RULE_ID_mi,
            1 as dummy
         from
            (select column_key,
              mandatory
            from s9t_tmpl_cols_def
            where template_key                              = stg_svc_sa_rounding_rule.template_key
            and wksht_key                                   = 'SA_ROUNDING_RULE_DETAIL'
            ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('ROUNDING_RULE_ID' AS ROUNDING_RULE_ID,
            'LOW_ENDING_AMT' AS LOW_ENDING_AMT,'HIGH_ENDING_AMT' AS HIGH_ENDING_AMT,
                                    'ROUND_AMT' AS ROUND_AMT,
                                    
                                    
                                    
            null as dummy));
        l_mi_rec c_mandatory_ind%rowtype;
         dml_errors EXCEPTION;
     L_table        VARCHAR2(30)  := 'SVC_SA_ROUNDING_RULE_DETAIL';
         PRAGMA exception_init(dml_errors, -24381);
    BEGIN
        -- Get default values.
        FOR rec IN
        (select
                ROUND_AMT_dv,
                HIGH_ENDING_AMT_dv,
                LOW_ENDING_AMT_dv,
                ROUNDING_RULE_ID_dv,
         null as dummy
        from
         (select column_key,
            default_value
         from s9t_tmpl_cols_def
         where template_key                                  = stg_svc_sa_rounding_rule.template_key
         and wksht_key                                       = 'SA_ROUNDING_RULE_DETAIL'
         ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'ROUNDING_RULE_ID' AS ROUNDING_RULE_ID,
         'LOW_ENDING_AMT' AS LOW_ENDING_AMT,
            'HIGH_ENDING_AMT' AS HIGH_ENDING_AMT,
                 'ROUND_AMT' AS ROUND_AMT,
       NULL      
        AS        
        dummy))
        )

        LOOP  
        BEGIN  

            l_default_rec.ROUND_AMT := rec.ROUND_AMT_dv;  
      EXCEPTION  


        WHEN OTHERS then  

            write_s9t_error(I_file_id,'SA_ROUNDING_RULE_DETAIL',NULL,'ROUND_AMT','INV_DEFAULT',SQLERRM);  


        end;


        BEGIN  

            l_default_rec.HIGH_ENDING_AMT := rec.HIGH_ENDING_AMT_dv;  


        EXCEPTION  


        WHEN OTHERS then  

            write_s9t_error(I_file_id,'SA_ROUNDING_RULE_DETAIL',NULL,'HIGH_ENDING_AMT','INV_DEFAULT',SQLERRM);  


        end;


        BEGIN  

            l_default_rec.LOW_ENDING_AMT := rec.LOW_ENDING_AMT_dv;  


        EXCEPTION  


        WHEN OTHERS then  

            write_s9t_error(I_file_id,'SA_ROUNDING_RULE_DETAIL',NULL,'LOW_ENDING_AMT','INV_DEFAULT',SQLERRM);  


        end;


        BEGIN  

            l_default_rec.ROUNDING_RULE_ID := rec.ROUNDING_RULE_ID_dv;  


        EXCEPTION  


        WHEN OTHERS then  

            write_s9t_error(I_file_id,'SA_ROUNDING_RULE_DETAIL',NULL,'ROUNDING_RULE_ID','INV_DEFAULT',SQLERRM);  


        end;


        end LOOP;

        --Get mandatory indicators
        OPEN C_mandatory_ind;
        FETCH C_mandatory_ind
        INTO l_mi_rec;
        CLOSE C_mandatory_ind;
        FOR rec IN
        (select r.get_cell(SA_RR_DETAIL$Action)      AS Action,
        upper( r.get_cell(SA_RR_DETAIL$ROUNDING_RULE_ID)  )            AS ROUNDING_RULE_ID,
        r.get_cell(SA_RR_DETAIL$LOW_ENDING_AMT)              AS LOW_ENDING_AMT,
        r.get_cell(SA_RR_DETAIL$HIGH_ENDING_AMT)              AS HIGH_ENDING_AMT,
         r.get_cell(SA_RR_DETAIL$ROUND_AMT)              AS ROUND_AMT,
        r.get_row_seq()                             AS row_seq
        from s9t_folder sf,
         TABLE(sf.s9t_file_obj.sheets) ss,
         TABLE(ss.s9t_rows) r
        where sf.file_id  = I_file_id
        and ss.sheet_name = sheet_name_trans(SA_RR_DETAIL_sheet)
        )
        LOOP
         l_temp_rec.process_id        := I_process_id;
         l_temp_rec.chunk_id          := 1;
         l_temp_rec.row_seq           := rec.row_seq;
         l_temp_rec.process$status    := 'N';
         l_temp_rec.create_id         := GET_USER;
         l_temp_rec.last_upd_id       := GET_USER;
         l_temp_rec.create_datetime   := sysdate;
         l_temp_rec.last_upd_datetime := sysdate;
         l_error := false;
         BEGIN
            l_temp_rec.Action := rec.Action;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_DETAIL_sheet,rec.row_seq,'ACTION',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.ROUND_AMT := rec.ROUND_AMT;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_DETAIL_sheet,rec.row_seq,'ROUND_AMT',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.HIGH_ENDING_AMT := rec.HIGH_ENDING_AMT;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_DETAIL_sheet,rec.row_seq,'HIGH_ENDING_AMT',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.LOW_ENDING_AMT := rec.LOW_ENDING_AMT;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_DETAIL_sheet,rec.row_seq,'LOW_ENDING_AMT',SQLCODE,SQLERRM);
            l_error := true;
         end;
         BEGIN
            l_temp_rec.ROUNDING_RULE_ID := rec.ROUNDING_RULE_ID;
         EXCEPTION
         WHEN OTHERS then
            write_s9t_error(I_file_id,SA_RR_DETAIL_sheet,rec.row_seq,'ROUNDING_RULE_ID',SQLCODE,SQLERRM);
            l_error := true;
         end;
         if rec.action = stg_svc_SA_ROUNDING_RULE.action_new
         then
         l_temp_rec.ROUND_AMT := NVL( l_temp_rec.ROUND_AMT,l_default_rec.ROUND_AMT);
         l_temp_rec.HIGH_ENDING_AMT := NVL( l_temp_rec.HIGH_ENDING_AMT,l_default_rec.HIGH_ENDING_AMT);
         l_temp_rec.LOW_ENDING_AMT := NVL( l_temp_rec.LOW_ENDING_AMT,l_default_rec.LOW_ENDING_AMT);
         l_temp_rec.ROUNDING_RULE_ID := NVL( l_temp_rec.ROUNDING_RULE_ID,l_default_rec.ROUNDING_RULE_ID);
        end if;
         
         if NOT l_error then
            svc_SA_RR_DETAIL_col.extend();
            svc_SA_RR_DETAIL_col(svc_SA_RR_DETAIL_col.count()):=l_temp_rec;
         end if;
        end LOOP;
        BEGIN

        forall i IN 1..svc_SA_RR_DETAIL_col.count SAVE EXCEPTIONS Merge INTO SVC_SA_ROUNDING_RULE_DETAIL st USING
        (select
         (CASE
            WHEN l_mi_rec.ROUND_AMT_mi    = 'N'
            and svc_SA_RR_DETAIL_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.ROUND_AMT             IS NULL
            then mt.ROUND_AMT
            ELSE s1.ROUND_AMT
         end) AS ROUND_AMT,
         (CASE
            WHEN l_mi_rec.HIGH_ENDING_AMT_mi    = 'N'
            and svc_SA_RR_DETAIL_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.HIGH_ENDING_AMT             IS NULL
            then mt.HIGH_ENDING_AMT
            ELSE s1.HIGH_ENDING_AMT
         end) AS HIGH_ENDING_AMT,
         (CASE
            WHEN l_mi_rec.LOW_ENDING_AMT_mi    = 'N'
            and svc_SA_RR_DETAIL_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.LOW_ENDING_AMT             IS NULL
            then mt.LOW_ENDING_AMT
            ELSE s1.LOW_ENDING_AMT
         end) AS LOW_ENDING_AMT,
         (CASE
            WHEN l_mi_rec.ROUNDING_RULE_ID_mi    = 'N'
            and svc_SA_RR_DETAIL_col(i).action = stg_svc_SA_ROUNDING_RULE.action_mod
            and s1.ROUNDING_RULE_ID             IS NULL
            then mt.ROUNDING_RULE_ID
            ELSE s1.ROUNDING_RULE_ID
         end) AS ROUNDING_RULE_ID,
         null as dummy
        from (select
          svc_SA_RR_DETAIL_col(i).ROUND_AMT AS ROUND_AMT,
          svc_SA_RR_DETAIL_col(i).HIGH_ENDING_AMT AS HIGH_ENDING_AMT,
          svc_SA_RR_DETAIL_col(i).LOW_ENDING_AMT AS LOW_ENDING_AMT,
          svc_SA_RR_DETAIL_col(i).ROUNDING_RULE_ID AS ROUNDING_RULE_ID,
          null as dummy
         from dual
         ) s1,
         SA_ROUNDING_RULE_DETAIL mt
        where
        mt.ROUNDING_RULE_ID (+)     = s1.ROUNDING_RULE_ID   and
         mt.LOW_ENDING_AMT (+)     = s1.LOW_ENDING_AMT   and
          mt.high_endING_AMT (+)     = s1.high_endING_AMT   and
        1 = 1
        ) sq ON (st.ROUNDING_RULE_ID      = sq.ROUNDING_RULE_ID and
    st.low_ending_amt=sq.low_ending_amt and
    st.high_ending_amt=st.high_ending_amt and
        svc_SA_RR_DETAIL_col(i).ACTION IN (stg_svc_SA_ROUNDING_RULE.action_mod,stg_svc_SA_ROUNDING_RULE.action_del)
        )
        WHEN matched then
        update
        SET PROCESS_ID      = svc_SA_RR_DETAIL_col(i).PROCESS_ID ,
         CHUNK_ID          = svc_SA_RR_DETAIL_col(i).CHUNK_ID ,
         ROW_SEQ           = svc_SA_RR_DETAIL_col(i).ROW_SEQ ,
         ACTION            = svc_SA_RR_DETAIL_col(i).ACTION ,
         PROCESS$STATUS    = svc_SA_RR_DETAIL_col(i).PROCESS$STATUS ,
        -- LOW_ENDING_AMT              = sq.LOW_ENDING_AMT ,
         ROUND_AMT              = sq.ROUND_AMT ,
    --   HIGH_ENDING_AMT              = sq.HIGH_ENDING_AMT ,
        --  ROUNDING_RULE_ID              = sq.ROUNDING_RULE_ID ,
         CREATE_ID         = svc_SA_RR_DETAIL_col(i).CREATE_ID ,
         CREATE_DATETIME   = svc_SA_RR_DETAIL_col(i).CREATE_DATETIME ,
         LAST_UPD_ID       = svc_SA_RR_DETAIL_col(i).LAST_UPD_ID ,
         LAST_UPD_DATETIME = svc_SA_RR_DETAIL_col(i).LAST_UPD_DATETIME WHEN NOT matched then
        insert
         (
            PROCESS_ID ,
            CHUNK_ID ,
            ROW_SEQ ,
            ACTION ,
            PROCESS$STATUS ,
            ROUND_AMT ,
            HIGH_ENDING_AMT ,
            LOW_ENDING_AMT ,
            ROUNDING_RULE_ID ,
            CREATE_ID ,
            CREATE_DATETIME ,
            LAST_UPD_ID ,
            LAST_UPD_DATETIME
         )
         VALUES
         (
            svc_SA_RR_DETAIL_col(i).PROCESS_ID ,
            svc_SA_RR_DETAIL_col(i).CHUNK_ID ,
            svc_SA_RR_DETAIL_col(i).ROW_SEQ ,
            svc_SA_RR_DETAIL_col(i).ACTION ,
            svc_SA_RR_DETAIL_col(i).PROCESS$STATUS ,
            sq.ROUND_AMT ,
            sq.HIGH_ENDING_AMT ,
            sq.LOW_ENDING_AMT ,
            sq.ROUNDING_RULE_ID ,
            svc_SA_RR_DETAIL_col(i).CREATE_ID ,
            svc_SA_RR_DETAIL_col(i).CREATE_DATETIME ,
            svc_SA_RR_DETAIL_col(i).LAST_UPD_ID ,
            svc_SA_RR_DETAIL_col(i).LAST_UPD_DATETIME
         );
         
        EXCEPTION
        WHEN DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
              L_error_code := NULL;
              l_error_msg:=sql_lib.create_msg('DUP_REC_EXISTS',l_uk_columns,l_table);
            end if;
             write_s9t_error
            (
              I_file_id,SA_RR_DETAIL_sheet,svc_SA_RR_DETAIL_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
            )
            ;
         end LOOP;
        end;
    end process_s9t_SA_RR_DETAIL;
    -------------------------------------------------------------------------------
    ---  Name: process_s9t 
    --Purpose: This is called from UI to uplaod data from excel to staging tables
    -------------------------------------------------------------------------------- 
    FUNCTION process_s9t
      (
         O_error_message IN OUT rtk_errors.rtk_text%type ,
         I_file_id       IN s9t_folder.file_id%type,
         I_process_id IN Number,
         O_error_count OUT NUMBER
      )
      RETURN BOOLEAN
    IS
      l_file           s9t_file;
      l_sheets         s9t_pkg.names_map_typ;
      L_program        VARCHAR2(255):='SA_ROUNDING_RULE.process_s9t';
      l_process_status svc_process_tracker.status%type;
          MAX_CHAR     EXCEPTION;
          PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
      
    BEGIN

        COMMIT;--to ensure that the record in s9t_folder is commited
        s9t_pkg.ods2obj(I_file_id);
        COMMIT;  
        L_file := s9t_pkg.get_obj(I_file_id);
        Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
        if s9t_pkg.code2desc(O_error_message,
                             'RSACR',
                         l_file,
                         true)=FALSE then
       return FALSE;
        end if;

        s9t_pkg.save_obj(l_file);
        if s9t_pkg.validate_template(I_file_id) = FALSE then
           write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'S9T_INVALID_TEMPLATE');
        else
         populate_names(I_file_id);
            sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key,l_file.user_lang);
            PROCESS_S9T_SA_RR_HEAD(I_file_id,I_process_id);
            PROCESS_S9T_SA_RR_HEAD_TL(I_file_id,I_process_id);
            PROCESS_S9T_SA_RR_DETAIL(I_file_id,I_process_id);
        end if;
        O_error_count := Lp_s9t_errors_tab.count();
        FORALL i IN 1..O_error_count
         insert INTO s9t_errors VALUES Lp_s9t_errors_tab
            (i
            );
        Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
        if O_error_count    = 0 then
           l_process_status := 'PS';
        ELSE
           l_process_status := 'PE';
        end if;
        update svc_process_tracker
        SET status       = l_process_status,
         file_id        = I_file_id
        where process_id = I_process_id;
        COMMIT;
        RETURN true;
    EXCEPTION
           when MAX_CHAR then
              ROLLBACK;
              O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
              Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
              write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
              O_error_count := Lp_s9t_errors_tab.count();
              forall i IN 1..O_error_count
             insert into s9t_errors
                  values Lp_s9t_errors_tab(i);

              update svc_process_tracker
             set status = 'PE',
                 file_id  = I_file_id
               where process_id = I_process_id;
      
              COMMIT;
      
              return FALSE;
    
        WHEN OTHERS then
            O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
            RETURN FALSE;
    end process_s9t;
    -------------------------------------------------------------------------------
    ---  Name: populate_lists 
    --Purpose: To populate the column lists drop down with values from code_detail
    --------------------------------------------------------------------------------
    FUNCTION populate_lists(
         O_error_message IN OUT rtk_errors.rtk_text%type,
         I_file_id       IN NUMBER)RETURN BOOLEAN
    IS
        L_program VARCHAR2(75) := 'SA_ROUNDING_RULE.POPULATE_LISTS';
        l_s9t_action s9t_cells := NEW s9t_cells(stg_svc_SA_ROUNDING_RULE.action_new,stg_svc_SA_ROUNDING_RULE.action_mod,stg_svc_SA_ROUNDING_RULE.action_del);
    BEGIN
        if S9T_PKG.populate_lists(O_error_message,
                              I_file_id,
                              'RSACR')=FALSE then
       return FALSE;
        end if;
        RETURN true;
    EXCEPTION
    WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
      RETURN FALSE;
    end populate_lists;

end STG_SVC_SA_ROUNDING_RULE;
/ 