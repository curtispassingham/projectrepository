CREATE OR REPLACE PACKAGE BODY CORESVC_MDB AS
-----------------------------------------------------------------
   cursor C_SVC_MONTH_DATA_BUDGET(I_process_id   NUMBER,
                                  I_chunk_id     NUMBER) is
      select pk_mdb.rowid                 as pk_mdb_rid,
             st.rowid                     as st_rid, 
             st.opn_stk_retail,
             pk_mdb.opn_stk_retail        as mdb_opn_stk_retail,
             st.location,
             st.loc_type,
             st.half_no,
             st.month_no,
             st.dept,
             st.gross_margin,
             pk_mdb.gross_margin          as mdb_gross_margin,
             st.cls_stk_cost,
             pk_mdb.cls_stk_cost          as mdb_cls_stk_cost,
             st.cls_stk_retail,   
             pk_mdb.cls_stk_retail        as mdb_cls_stk_retail,   
             st.empl_disc_retail,
             pk_mdb.empl_disc_retail      as mdb_empL_disc_retail,
             st.shrinkage_cost,
             pk_mdb.shrinkage_cost        as mdb_shrinkage_cost,
             st.shrinkage_retail,
             pk_mdb.shrinkage_retail      as mdb_shrinkage_retail,
             st.prom_markdown_retail,
             pk_mdb.prom_markdown_retail  as mdb_prom_markdown_retail,
             st.perm_markdown_retail,
             pk_mdb.perm_markdown_retail  as mdb_perm_markdown_retail,
             st.clear_markdown_retail,
             pk_mdb.clear_markdown_retail as mdb_clear_markdown_retail,
             st.net_sales_cost,
             pk_mdb.net_sales_cost        as mdb_net_sales_cost,
             st.net_sales_retail,
             pk_mdb.net_sales_retail      as mdb_net_sales_retail,
             st.rtv_cost,
             pk_mdb.rtv_cost              as mdb_rtv_cost,
             st.rtv_retail,
             pk_mdb.rtv_retail            as mdb_rtv_retail,
             st.purch_cost,
             pk_mdb.purch_cost            as mdb_purch_cost,
             st.purch_retail,
             pk_mdb.purch_retail          as mdb_purch_retail,
             st.opn_stk_cost,
             pk_mdb.opn_stk_cost          as mdb_opn_stk_cost,
             st.set_of_books_id,  
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)             as action,
             st.process$status
        from svc_month_data_budget st,
             month_data_budget pk_mdb,
             dual
       where st.process_id      = I_process_id
         and st.chunk_id        = I_chunk_id
         and st.location        = pk_mdb.location (+)
         and st.loc_type        = pk_mdb.loc_type (+)
         and st.half_no         = pk_mdb.half_no (+)
         and st.month_no        = pk_mdb.month_no (+)
         and st.dept            = pk_mdb.dept (+)
         and st.set_of_books_id = pk_mdb.set_of_books_id (+);

   Type errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   Type s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   Lp_errors_tab       errors_tab_typ;
   Lp_s9t_errors_tab   s9t_errors_tab_typ;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS; 
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (CASE
                                                                         WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                         ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                          END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE) IS
BEGIN
  Lp_errors_tab.extend();
  Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
  Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
  Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
  Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
  Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
  Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
  Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES (I_file_id   NUMBER) IS
   L_sheets   s9t_pkg.names_map_typ;
   MDB_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets                  :=s9t_pkg.get_sheet_names(I_file_id);
   MDB_cols                  :=s9t_pkg.get_coL_names(I_file_id,
                                                    MDB_sheet);
   MDB$Action                := MDB_cols('ACTION');
   MDB$opn_stk_retail        := MDB_cols('OPN_STK_RETAIL');
   MDB$location              := MDB_cols('LOCATION');
   MDB$loc_type              := MDB_cols('LOC_TYPE');
   MDB$half_no               := MDB_cols('HALF_NO');
   MDB$month_no              := MDB_cols('MONTH_NO');
   MDB$dept                  := MDB_cols('DEPT');
   MDB$gross_margin          := MDB_cols('GROSS_MARGIN');
   MDB$cls_stk_cost          := MDB_cols('CLS_STK_COST');
   MDB$cls_stk_retail        := MDB_cols('CLS_STK_RETAIL');
   MDB$empl_disc_retail      := MDB_cols('EMPL_DISC_RETAIL');
   MDB$shrinkage_cost        := MDB_cols('SHRINKAGE_COST');
   MDB$shrinkage_retail      := MDB_cols('SHRINKAGE_RETAIL');
   MDB$prom_markdown_retail  := MDB_cols('PROM_MARKDOWN_RETAIL');
   MDB$perm_markdown_retail  := MDB_cols('PERM_MARKDOWN_RETAIL');
   MDB$clear_markdown_retail := MDB_cols('CLEAR_MARKDOWN_RETAIL');
   MDB$net_sales_cost        := MDB_cols('NET_SALES_COST');
   MDB$net_sales_retail      := MDB_cols('NET_SALES_RETAIL');
   MDB$rtv_cost              := MDB_cols('RTV_COST');
   MDB$rtv_retail            := MDB_cols('RTV_RETAIL');
   MDB$purch_cost            := MDB_cols('PURCH_COST');
   MDB$purch_retail          := MDB_cols('PURCH_RETAIL');
   MDB$opn_stk_cost          := MDB_cols('OPN_STK_COST');
   MDB$set_of_books_id       := MDB_cols('SET_OF_BOOKS_ID');
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_MDB(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = MDB_sheet)
               select s9t_row(s9t_cells(CORESVC_MDB.action_mod,
                                        dept,
                                        month_no,
                                        half_no,
                                        loc_type,
                                        location,
                                        set_of_books_id,
                                        opn_stk_retail,
                                        opn_stk_cost,
                                        purch_retail,
                                        purch_cost,
                                        rtv_retail,
                                        rtv_cost, 
                                        net_sales_retail,
                                        net_sales_cost,
                                        clear_markdown_retail,
                                        perm_markdown_retail,
                                        prom_markdown_retail,
                                        shrinkage_retail,
                                        shrinkage_cost,
                                        empl_disc_retail,
                                        cls_stk_retail,
                                        cls_stk_cost,
                                        gross_margin))
                 from MONTH_DATA_BUDGET ;
END POPULATE_MDB;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(MDB_sheet);
   L_file.sheets(L_file.get_sheet_index(MDB_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                 'DEPT',
                                                                                 'MONTH_NO',
                                                                                 'HALF_NO', 
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'SET_OF_BOOKS_ID',
                                                                                 'OPN_STK_RETAIL',
                                                                                 'OPN_STK_COST',              
                                                                                 'PURCH_RETAIL',             
                                                                                 'PURCH_COST',             
                                                                                 'RTV_RETAIL',             
                                                                                 'RTV_COST',            
                                                                                 'NET_SALES_RETAIL', 
                                                                                 'NET_SALES_COST',
                                                                                 'CLEAR_MARKDOWN_RETAIL', 
                                                                                 'PERM_MARKDOWN_RETAIL', 
                                                                                 'PROM_MARKDOWN_RETAIL',
                                                                                 --'TOTAL_MARKDOWN_RETAIL',
                                                                                 --'TOTAL_MARKDOWN_COST',
                                                                                 'SHRINKAGE_RETAIL',
                                                                                 'SHRINKAGE_COST',
                                                                                 'EMPL_DISC_RETAIL',
                                                                                 'CLS_STK_RETAIL',
                                                                                 'CLS_STK_COST',
                                                                                 'GROSS_MARGIN');                                                                             
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program    VARCHAR2(64) := 'CORESVC_MDB.CREATE_S9T';
   L_file       s9t_file;
   
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_MDB(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_MDB(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_MONTH_DATA_BUDGET.process_id%TYPE) IS
                                        
   Type svc_MONTH_DATA_BUDGET_coL_typ IS TABLE OF SVC_MONTH_DATA_BUDGET%ROWTYPE;
   L_temp_rec                  SVC_MONTH_DATA_BUDGET%ROWTYPE;
   svc_MONTH_DATA_BUDGET_col   svc_MONTH_DATA_BUDGET_coL_typ := NEW svc_MONTH_DATA_BUDGET_coL_typ();
   L_process_id                SVC_MONTH_DATA_BUDGET.process_id%TYPE;
   L_error                     BOOLEAN := FALSE;
   L_default_rec               SVC_MONTH_DATA_BUDGET%ROWTYPE;
   cursor c_mandatory_ind is
      select OPN_STK_RETAIL_mi,
             LOCATION_mi,
             LOC_TYPE_mi,
             HALF_NO_mi,
             MONTH_NO_mi,
             DEPT_mi,
             GROSS_MARGIN_mi,
             CLS_STK_COST_mi,
             CLS_STK_RETAIL_mi,
             EMPL_DISC_RETAIL_mi,
             SHRINKAGE_COST_mi,
             SHRINKAGE_RETAIL_mi,
             PROM_MARKDOWN_RETAIL_mi,
             PERM_MARKDOWN_RETAIL_mi,
             CLEAR_MARKDOWN_RETAIL_mi,
             NET_SALES_COST_mi,
             NET_SALES_RETAIL_mi,
             RTV_COST_mi,
             RTV_RETAIL_mi,
             PURCH_COST_mi,
             PURCH_RETAIL_mi,
             OPN_STK_COST_mi,
             SET_OF_BOOKS_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_MDB.template_key
                 and wksht_key = 'MDB') 
               PIVOT (MAX(mandatory) as mi 
                  FOR (column_key) IN ('OPN_STK_RETAIL'        as opn_stk_retail,
                                       'LOCATION'              as location,
                                       'LOC_TYPE'              as loc_type,
                                       'HALF_NO'               as half_no,
                                       'MONTH_NO'              as month_no,
                                       'DEPT'                  as dept,
                                       'GROSS_MARGIN'          as gross_margin,
                                       'CLS_STK_COST'          as cls_stk_cost,
                                       'CLS_STK_RETAIL'        as cls_stk_retail,
                                       'EMPL_DISC_RETAIL'      as empl_disc_retail,
                                       'SHRINKAGE_COST'        as shrinkage_cost,
                                       'SHRINKAGE_RETAIL'      as shrinkage_retail,
                                       'PROM_MARKDOWN_RETAIL'  as prom_markdown_retail,
                                       'PERM_MARKDOWN_RETAIL'  as perm_markdown_retail,
                                       'CLEAR_MARKDOWN_RETAIL' as clear_markdown_retail,
                                       'NET_SALES_COST'        as net_sales_cost,
                                       'NET_SALES_RETAIL'      as net_sales_retail,
                                       'RTV_COST'              as rtv_cost,
                                       'RTV_RETAIL'            as rtv_retail,
                                       'PURCH_COST'            as purch_cost,
                                       'PURCH_RETAIL'          as purch_retail,
                                       'OPN_STK_COST'          as opn_stk_cost,
                                       'SET_OF_BOOKS_ID'       as set_of_books_id,
                                       NULL as dummy));
                                       
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_MONTH_DATA_BUDGET';
   L_pk_columns    VARCHAR2(255)  := 'Department,Month,Half,Location Type,Location,Set of Books ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
   
BEGIN
  -- Get default values.
   FOR rec IN (select OPN_STK_RETAIL_dv,
                      LOCATION_dv,
                      LOC_TYPE_dv,
                      HALF_NO_dv,
                      MONTH_NO_dv,
                      DEPT_dv,
                      GROSS_MARGIN_dv,
                      CLS_STK_COST_dv,
                      CLS_STK_RETAIL_dv,
                      EMPL_DISC_RETAIL_dv,
                      SHRINKAGE_COST_dv,
                      SHRINKAGE_RETAIL_dv,
                      PROM_MARKDOWN_RETAIL_dv,
                      PERM_MARKDOWN_RETAIL_dv,
                      CLEAR_MARKDOWN_RETAIL_dv,
                      NET_SALES_COST_dv,
                      NET_SALES_RETAIL_dv,
                      RTV_COST_dv,
                      RTV_RETAIL_dv,
                      PURCH_COST_dv,
                      PURCH_RETAIL_dv,
                      OPN_STK_COST_dv,
                      SET_OF_BOOKS_ID_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key  = CORESVC_MDB.template_key
                          and wksht_key = 'MDB') 
                        PIVOT (MAX(default_value) as dv 
                           FOR (column_key) IN ('OPN_STK_RETAIL'        as opn_stk_retail,
                                                'LOCATION'              as location,
                                                'LOC_TYPE'              as loc_type,
                                                'HALF_NO'               as half_no,
                                                'MONTH_NO'              as month_no,
                                                'DEPT'                  as dept,
                                                'GROSS_MARGIN'          as gross_margin,
                                                'CLS_STK_COST'          as cls_stk_cost,
                                                'CLS_STK_RETAIL'        as cls_stk_retail,
                                                'EMPL_DISC_RETAIL'      as empl_disc_retail,
                                                'SHRINKAGE_COST'        as shrinkage_cost,
                                                'SHRINKAGE_RETAIL'      as shrinkage_retail,
                                                'PROM_MARKDOWN_RETAIL'  as prom_markdown_retail,
                                                'PERM_MARKDOWN_RETAIL'  as perm_markdown_retail,
                                                'CLEAR_MARKDOWN_RETAIL' as clear_markdown_retail,
                                                'NET_SALES_COST'        as net_sales_cost,
                                                'NET_SALES_RETAIL'      as net_sales_retail,
                                                'RTV_COST'              as rtv_cost,                                                   
                                                'RTV_RETAIL'            as rtv_retail,                                               
                                                'PURCH_COST'            as purch_cost,
                                                'PURCH_RETAIL'          as purch_retail,
                                                'OPN_STK_COST'          as opn_stk_cost,
                                                'SET_OF_BOOKS_ID'       as set_of_books_id,
                                                NULL                    as dummy)))
   LOOP  
      BEGIN  
         L_default_rec.opn_stk_retail := rec.OPN_STK_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MDB',
                         NULL,
                         'OPN_STK_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.location := rec.LOCATION_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'LOCATION',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.loc_type := rec.LOC_TYPE_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'LOC_TYPE',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.half_no := rec.HALF_NO_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'HALF_NO',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.month_no := rec.MONTH_NO_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'MONTH_NO',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.dept := rec.DEPT_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'DEPT',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.gross_margin := rec.GROSS_MARGIN_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'GROSS_MARGIN',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.cls_stk_cost := rec.CLS_STK_COST_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'CLS_STK_COST',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.cls_stk_retail := rec.CLS_STK_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'CLS_STK_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.empl_disc_retail := rec.EMPL_DISC_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'empl_disc_retail',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.shrinkage_cost := rec.SHRINKAGE_COST_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'SHRINKAGE_COST',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.shrinkage_retail := rec.SHRINKAGE_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'SHRINKAGE_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.prom_markdown_retail := rec.PROM_MARKDOWN_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'PROM_MARKDOWN_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.perm_markdown_retail := rec.PERM_MARKDOWN_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'PERM_MARKDOWN_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.clear_markdown_retail := rec.CLEAR_MARKDOWN_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'CLEAR_MARKDOWN_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.net_sales_cost := rec.NET_SALES_COST_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'NET_SALES_COST',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.net_sales_retail := rec.NET_SALES_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'NET_SALES_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.rtv_cost := rec.RTV_COST_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'RTV_COST',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.rtv_retail := rec.RTV_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'RTV_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.purch_cost := rec.PURCH_COST_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'PURCH_COST',
                         'INV_DEFAULT',
                         SQLERRM);
      END;
      BEGIN  
         L_default_rec.purch_retail := rec.PURCH_RETAIL_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'PURCH_RETAIL',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.opn_stk_cost := rec.OPN_STK_COST_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'OPN_STK_COST',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.set_of_books_id := rec.SET_OF_BOOKS_ID_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'MONTH_DATA_BUDGET',
                         NULL,
                         'SET_OF_BOOKS_ID',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
   END LOOP;

 --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(MDB$Action)                  as Action,
                      r.get_cell(MDB$opn_stk_retail)          as opn_stk_retail,
                      r.get_cell(MDB$location)                as location,
                      r.get_cell(MDB$loc_type)                as loc_type,
                      r.get_cell(MDB$half_no)                 as half_no,
                      r.get_cell(MDB$month_no)                as month_no,
                      r.get_cell(MDB$dept)                    as dept,
                      r.get_cell(MDB$gross_margin)            as gross_margin,
                      r.get_cell(MDB$cls_stk_cost)            as cls_stk_cost,
                      r.get_cell(MDB$cls_stk_retail)          as cls_stk_retail,
                      r.get_cell(MDB$empl_disc_retail)        as empl_disc_retail,
                      r.get_cell(MDB$shrinkage_cost)          as shrinkage_cost,
                      r.get_cell(MDB$shrinkage_retail)        as shrinkage_retail,
                      r.get_cell(MDB$prom_markdown_retail)    as prom_markdown_retail,
                      r.get_cell(MDB$perm_markdown_retail)    as perm_markdown_retail,
                      r.get_cell(MDB$clear_markdown_retail)   as clear_markdown_retail,
                      r.get_cell(MDB$net_sales_cost)          as net_sales_cost,
                      r.get_cell(MDB$net_sales_retail)        as net_sales_retail,
                      r.get_cell(MDB$rtv_cost)                as rtv_cost,
                      r.get_cell(MDB$rtv_retail)              as rtv_retail,
                      r.get_cell(MDB$purch_cost)              as purch_cost,
                      r.get_cell(MDB$purch_retail)            as purch_retail,
                      r.get_cell(MDB$opn_stk_cost)            as opn_stk_cost,
                      r.get_cell(MDB$set_of_books_id)         as set_of_books_id,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(MDB_sheet))
   LOOP
      L_temp_rec                   := NULL;
		L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         action_column,
                         SQLCODE,SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.opn_stk_retail := rec.opn_stk_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'OPN_STK_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.location := rec.location;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'LOCATION',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.loc_type := rec.loc_type;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'LOC_TYPE',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.half_no := rec.half_no;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'HALF_NO',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.month_no := rec.month_no;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'MONTH_NO',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.dept := rec.dept;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'DEPT',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.gross_margin := rec.gross_margin;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'GROSS_MARGIN',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cls_stk_cost := rec.cls_stk_cost;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'CLS_STK_COST',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cls_stk_retail := rec.cls_stk_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'CLS_STK_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.empl_disc_retail := rec.empl_disc_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'EMPL_DISC_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.shrinkage_cost := rec.shrinkage_cost;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'SHRINKAGE_COST',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.shrinkage_retail := rec.shrinkage_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'SHRINKAGE_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.prom_markdown_retail := rec.prom_markdown_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'PROM_MARKDOWN_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.perm_markdown_retail := rec.perm_markdown_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'PERM_MARKDOWN_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.clear_markdown_retail := rec.clear_markdown_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'CLEAR_MARKDOWN_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.net_sales_cost := rec.net_sales_cost;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'NET_SALES_COST',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.net_sales_retail := rec.net_sales_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'NET_SALES_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.rtv_cost := rec.rtv_cost;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'RTV_COST',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.rtv_retail := rec.rtv_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'RTV_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.purch_cost := rec.purch_cost;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'PURCH_COST',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.purch_retail := rec.purch_retail;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'PURCH_RETAIL',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.opn_stk_cost := rec.opn_stk_cost;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'OPN_STK_COST',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.set_of_books_id := rec.set_of_books_id;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         'SET_OF_BOOKS_ID',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      if NOT (L_temp_rec.location is NOT NULL and
              L_temp_rec.loc_type is NOT NULL and
              L_temp_rec.half_no is NOT NULL and
              L_temp_rec.month_no is NOT NULL and
              L_temp_rec.dept is NOT NULL and
              L_temp_rec.set_of_books_id is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         MDB_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_MONTH_DATA_BUDGET_col.extend();
         svc_MONTH_DATA_BUDGET_col(svc_MONTH_DATA_BUDGET_col.count()):=L_temp_rec;
      end if;
   END LOOP;
   
   BEGIN
      forall i IN 1..svc_MONTH_DATA_BUDGET_col.count SAVE EXCEPTIONS 
      Merge into SVC_MONTH_DATA_BUDGET st 
      USING (select (case
                     when L_mi_rec.OPN_STK_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.opn_stk_retail is NULL then
                          mt.opn_stk_retail 
                     else s1.opn_stk_retail 
                     end) as opn_stk_retail,
                     (case
                     when L_mi_rec.LOCATION_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.location is NULL then
                          mt.location
                     else s1.location
                     end) as location,
                     (case
                     when L_mi_rec.LOC_TYPE_mi = 'N'  
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod  
                      and s1.loc_type is NULL then
                          mt.loc_type
                     else s1.loc_type
                     end) as loc_type,
                     (case
                     when L_mi_rec.HALF_NO_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod  
                      and s1.half_no is NULL then
                          mt.half_no
                     else s1.half_no
                     end) as half_no,
                     (case
                     when L_mi_rec.MONTH_NO_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.month_no is NULL then
                          mt.month_no
                     else s1.month_no
                     end) as month_no,
                     (case
                     when L_mi_rec.DEPT_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.dept is NULL then
                          mt.dept
                     else s1.dept
                     end) as dept,
                     (case
                     when L_mi_rec.GROSS_MARGIN_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.gross_margin is NULL then
                          mt.gross_margin
                     else s1.gross_margin
                     end) as gross_margin,
                     (case
                     when L_mi_rec.CLS_STK_COST_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.cls_stk_cost is NULL then
                          mt.cls_stk_cost
                     else s1.cls_stk_cost
                     end) as cls_stk_cost,
                     (case
                     when L_mi_rec.CLS_STK_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.cls_stk_retail is NULL then
                          mt.cls_stk_retail
                     else s1.cls_stk_retail
                     end) as cls_stk_retail,
                     (case
                     when L_mi_rec.EMPL_DISC_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.empl_disc_retail is NULL then
                          mt.empl_disc_retail
                     else s1.empl_disc_retail
                     end) as empl_disc_retail,
                     (case
                     when L_mi_rec.SHRINKAGE_COST_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.shrinkage_cost is NULL then
                          mt.shrinkage_cost
                     else s1.shrinkage_cost
                     end) as shrinkage_cost,
                     (case
                     when L_mi_rec.SHRINKAGE_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.shrinkage_retail is NULL then
                          mt.shrinkage_retail
                     else s1.shrinkage_retail
                     end) as shrinkage_retail,
                     (case
                     when L_mi_rec.PROM_MARKDOWN_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.prom_markdown_retail is NULL then
                          mt.prom_markdown_retail
                     else s1.prom_markdown_retail
                     end) as prom_markdown_retail,
                     (case
                     when L_mi_rec.PERM_MARKDOWN_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.perm_markdown_retail is NULL then
                          mt.perm_markdown_retail
                     else s1.perm_markdown_retail
                     end) as perm_markdown_retail,
                     (case
                     when L_mi_rec.CLEAR_MARKDOWN_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.clear_markdown_retail is NULL then
                          mt.clear_markdown_retail
                     else s1.clear_markdown_retail
                     end) as clear_markdown_retail,
                     (case
                     when L_mi_rec.NET_SALES_COST_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.net_sales_cost is NULL then
                          mt.net_sales_cost
                     else s1.net_sales_cost
                     end) as net_sales_cost,
                     (case
                     when L_mi_rec.NET_SALES_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.net_sales_retail is NULL then
                          mt.net_sales_retail
                     else s1.net_sales_retail
                     end) as net_sales_retail,
                     (case
                     when L_mi_rec.RTV_COST_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.rtv_cost is NULL then
                          mt.rtv_cost
                     else s1.rtv_cost
                     end) as rtv_cost,
                     (case
                     when L_mi_rec.RTV_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.rtv_retail is NULL then
                          mt.rtv_retail
                     else s1.rtv_retail
                     end) as rtv_retail,
                     (case
                     when L_mi_rec.PURCH_COST_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.purch_cost is NULL then
                          mt.purch_cost
                     else s1.purch_cost
                     end) AS purch_cost,
                     (case
                     when L_mi_rec.PURCH_RETAIL_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.purch_retail is NULL then
                          mt.purch_retail
                     else s1.purch_retail
                     end) as purch_retail,
                     (case
                     when L_mi_rec.OPN_STK_COST_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.opn_stk_cost is NULL then
                          mt.opn_stk_cost
                     else s1.opn_stk_cost
                     end) as opn_stk_cost,
                     (case
                     when L_mi_rec.SET_OF_BOOKS_ID_mi = 'N' 
                      and svc_MONTH_DATA_BUDGET_col(i).action = coresvc_mdb.action_mod 
                      and s1.set_of_books_id is NULL then
                          mt.set_of_books_id
                     else s1.set_of_books_id
                     end) as set_of_books_id,
                     NULL as dummy
                from (select svc_MONTH_DATA_BUDGET_col(i).opn_stk_retail        as opn_stk_retail,
                             svc_MONTH_DATA_BUDGET_col(i).location              as location,
                             svc_MONTH_DATA_BUDGET_col(i).loc_type              as loc_type,
                             svc_MONTH_DATA_BUDGET_col(i).half_no               as half_no,
                             svc_MONTH_DATA_BUDGET_col(i).month_no              as month_no,
                             svc_MONTH_DATA_BUDGET_col(i).dept                  as dept,
                             svc_MONTH_DATA_BUDGET_col(i).gross_margin          as gross_margin,
                             svc_MONTH_DATA_BUDGET_col(i).cls_stk_cost          as cls_stk_cost,
                             svc_MONTH_DATA_BUDGET_col(i).cls_stk_retail        as cls_stk_retail,
                             svc_MONTH_DATA_BUDGET_col(i).empl_disc_retail      as empl_disc_retail,
                             svc_MONTH_DATA_BUDGET_col(i).shrinkage_cost        as shrinkage_cost,
                             svc_MONTH_DATA_BUDGET_col(i).shrinkage_retail      as shrinkage_retail,
                             svc_MONTH_DATA_BUDGET_col(i).prom_markdown_retail  as prom_markdown_retail,
                             svc_MONTH_DATA_BUDGET_col(i).perm_markdown_retail  as perm_markdown_retail,
                             svc_MONTH_DATA_BUDGET_col(i).clear_markdown_retail as clear_markdown_retail,
                             svc_MONTH_DATA_BUDGET_col(i).net_sales_cost        as net_sales_cost,
                             svc_MONTH_DATA_BUDGET_col(i).net_sales_retail      as net_sales_retail,
                             svc_MONTH_DATA_BUDGET_col(i).rtv_cost              as rtv_cost,
                             svc_MONTH_DATA_BUDGET_col(i).rtv_retail            as rtv_retail,
                             svc_MONTH_DATA_BUDGET_col(i).purch_cost            as purch_cost,
                             svc_MONTH_DATA_BUDGET_col(i).purch_retail          as purch_retail,
                             svc_MONTH_DATA_BUDGET_col(i).opn_stk_cost          as opn_stk_cost,
                             svc_MONTH_DATA_BUDGET_col(i).set_of_books_id       as set_of_books_id,
                             NULL as dummy
                        from dual) s1,
                             MONTH_DATA_BUDGET mt
                       where mt.location (+)        = s1.location   
                         and mt.loc_type (+)        = s1.loc_type   
                         and mt.half_no (+)         = s1.half_no   
                         and mt.month_no (+)        = s1.month_no   
                         and mt.dept (+)            = s1.dept   
                         and mt.set_of_books_id (+) = s1.set_of_books_id) sq 
                          ON (st.location        = sq.location and
                              st.loc_type        = sq.loc_type and
                              st.half_no         = sq.half_no and
                              st.month_no        = sq.month_no and
                              st.dept            = sq.dept and
                              st.set_of_books_id = sq.set_of_books_id and
                              svc_MONTH_DATA_BUDGET_col(i).action in (CORESVC_MDB.action_mod,CORESVC_MDB.action_del))
      when matched then
      update
         set process_id            = svc_MONTH_DATA_BUDGET_col(i).process_id ,
             chunk_id              = svc_MONTH_DATA_BUDGET_col(i).chunk_id ,
             row_seq               = svc_MONTH_DATA_BUDGET_col(i).row_seq ,
             action                = svc_MONTH_DATA_BUDGET_col(i).ACTION,
             PROCESS$STATUS        = svc_MONTH_DATA_BUDGET_col(i).PROCESS$STATUS ,
             clear_markdown_retail = sq.clear_markdown_retail ,
             net_sales_cost        = sq.net_sales_cost ,

             cls_stk_retail        = sq.cls_stk_retail ,
             purch_retail          = sq.purch_retail ,
             empl_disc_retail      = sq.empl_disc_retail ,
             rtv_cost              = sq.rtv_cost ,
             cls_stk_cost          = sq.cls_stk_cost ,
             opn_stk_retail        = sq.opn_stk_retail ,
             perm_markdown_retail  = sq.perm_markdown_retail ,
             opn_stk_cost          = sq.opn_stk_cost ,
             shrinkage_retail      = sq.shrinkage_retail ,
             rtv_retail            = sq.rtv_retail ,
             net_sales_retail      = sq.net_sales_retail ,
             gross_margin          = sq.gross_margin ,
             shrinkage_cost        = sq.shrinkage_cost ,
             purch_cost            = sq.purch_cost ,
             prom_markdown_retail  = sq.prom_markdown_retail ,
             CREATE_ID             = svc_MONTH_DATA_BUDGET_col(i).CREATE_ID ,
             CREATE_DATETIME       = svc_MONTH_DATA_BUDGET_col(i).CREATE_DATETIME ,
             LAST_UPD_ID           = svc_MONTH_DATA_BUDGET_col(i).LAST_UPD_ID ,
             LAST_UPD_DATETIME     = svc_MONTH_DATA_BUDGET_col(i).LAST_UPD_DATETIME 
      when NOT matched then
      insert(process_id ,
            chunk_id ,
            row_seq ,
            ACTION ,
            PROCESS$STATUS ,
            opn_stk_retail ,
            location ,
            loc_type ,
            half_no ,
            month_no ,
            dept ,
            gross_margin ,
            cls_stk_cost ,
            cls_stk_retail ,
            empl_disc_retail ,
            shrinkage_cost ,
            shrinkage_retail ,
            prom_markdown_retail ,
            perm_markdown_retail ,
            clear_markdown_retail ,
            net_sales_cost ,
            net_sales_retail ,
            rtv_cost ,
            rtv_retail ,
            purch_cost ,
            purch_retail ,
            opn_stk_cost ,
            set_of_books_id ,
            CREATE_ID ,
            CREATE_DATETIME ,
            LAST_UPD_ID ,
            LAST_UPD_DATETIME)
      values(svc_MONTH_DATA_BUDGET_col(i).process_id ,
            svc_MONTH_DATA_BUDGET_col(i).chunk_id ,
            svc_MONTH_DATA_BUDGET_col(i).row_seq ,
            svc_MONTH_DATA_BUDGET_col(i).ACTION ,
            svc_MONTH_DATA_BUDGET_col(i).PROCESS$STATUS ,
            sq.opn_stk_retail ,
            sq.location ,
            sq.loc_type ,
            sq.half_no ,
            sq.month_no ,
            sq.dept ,
            sq.gross_margin ,
            sq.cls_stk_cost ,
            sq.cls_stk_retail ,
            sq.empl_disc_retail ,
            sq.shrinkage_cost ,
            sq.shrinkage_retail ,
            sq.prom_markdown_retail ,
            sq.perm_markdown_retail ,
            sq.clear_markdown_retail ,
            sq.net_sales_cost ,
            sq.net_sales_retail ,
            sq.rtv_cost ,
            sq.rtv_retail ,
            sq.purch_cost ,
            sq.purch_retail ,
            sq.opn_stk_cost ,
            sq.set_of_books_id ,
            svc_MONTH_DATA_BUDGET_col(i).CREATE_ID ,
            svc_MONTH_DATA_BUDGET_col(i).CREATE_DATETIME ,
            svc_MONTH_DATA_BUDGET_col(i).LAST_UPD_ID ,
            svc_MONTH_DATA_BUDGET_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             MDB_sheet,
                             svc_MONTH_DATA_BUDGET_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);                            
         END LOOP;
   END;
END PROCESS_S9T_MDB;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_MDB.PROCESS_S9T';  
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	      EXCEPTION;
   PRAGMA	      EXCEPTION_INIT(MAX_CHAR, -01706);   
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_MDB(I_file_id,
                      I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   --Update process$status in svc_process_tracker
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);
      
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------
FUNCTION PROCESS_MDB_PRE_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error           IN OUT   BOOLEAN,
                             I_rec             IN       C_SVC_MONTH_DATA_BUDGET%ROWTYPE,
                             I_cost            IN       NUMBER,
                             I_column_name     IN       VARCHAR2,  
                             I_currency_fmt    IN       CURRENCIES.CURRENCY_RTL_FMT%TYPE) 
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'CORESVC_MDB.PROCESS_MDB_PRE_VAL';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_MONTH_DATA_BUDGET';
   L_length            NUMBER(2) := 0;
   L_length_cost       NUMBER(2) := 0;
   L_length_cost_dec   NUMBER(2) := 0;
   L_position          NUMBER(2) := 1;
   L_dec               NUMBER(2) := 0; 
   L_num               NUMBER(2) := 0; 
      
BEGIN
   L_length := Length(I_currency_fmt);
-- To check if the number of digits after decimal exceed those allowed by the format   
-- The Loop checks how many digits are allowed after decimal according to the format
   LOOP
	  if Substr(I_currency_fmt,
                L_position,
                1) = 'D' then
         
         LOOP
            if Substr(I_currency_fmt,
                      L_position,
                      1) in ('9', '0') then
               L_dec := L_dec + 1;
            end if;
            if L_length = L_position then 
               Exit;
            else
               L_position := L_position + 1;
            end if;
         END LOOP;
      end if;
      L_position := L_position + 1;
      if L_length <= L_position then 
         Exit;
      end if;
   END LOOP;

-- Checks how many digits are actually present after decimal point in the value passed.   
   L_length_cost_dec := length(abs(I_cost) - abs(trunc(I_cost))) - 1;
-- If the number of digits present in the value passed exceed those allowed by the format, then raise an error.
   if L_dec < L_length_cost_dec then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  I_column_name,
                  'INV_MDB_FRMT');
      O_error := TRUE;
   end if;
   
-- To check if the number of digits before decimal exceed those allowed by the format   
   L_position := 1;
   L_length := L_length - L_dec;
-- The WHILE loop checks how many digits are allowed before decimal according to the format
   WHILE Substr(I_currency_fmt,
                L_position,
                1) != 'D' 
                
      LOOP
         if Substr(I_currency_fmt,
                   L_position,
                   1) in ('9', '0') then
            L_num := L_num + 1;
         end if;
         if L_length = L_position then 
            Exit;
         else
            L_position := L_position + 1;
         end if;
      END LOOP;
-- Checks how many digits are actually present before decimal point in the value passed.       
   L_length_cost := length(abs(trunc(I_cost)));
-- If the number of digits present in the value passed exceed those allowed by the format, then raise an error.   
   if L_num < L_length_cost then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  I_column_name,
                  'INV_MDB_FRMT');
      O_error := TRUE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_MDB_PRE_VAL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_MDB_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error           IN OUT   BOOLEAN,
                         I_rec             IN       C_SVC_MONTH_DATA_BUDGET%ROWTYPE) 
RETURN BOOLEAN IS
   L_program             VARCHAR2(64) := 'CORESVC_MDB.PROCESS_MDB_VAL';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_MONTH_DATA_BUDGET';
   L_error_message       VARCHAR2(255);
   PM_prmry_curr         SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   L_currency_rtl_fmt    CURRENCIES.CURRENCY_RTL_FMT%TYPE;
   L_dumb1               CURRENCIES.CURRENCY_RTL_DEC%TYPE;
   L_currency_cost_fmt   CURRENCIES.CURRENCY_COST_FMT%TYPE;
   L_dumb3               CURRENCIES.CURRENCY_COST_DEC%TYPE;
   
BEGIN
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       PM_prmry_curr) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);                                
   else
      if CURRENCY_SQL.GET_FORMAT(O_error_message,
                                 PM_prmry_curr,
                                 L_currency_rtl_fmt,
                                 L_dumb1,
                                 L_currency_cost_fmt,
                                 L_dumb3) = FALSE then
 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);  
      else
         if I_rec.opn_stk_retail is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.opn_stk_retail,
                                   'OPN_STK_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'OPN_STK_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.opn_stk_cost is NOT NULL then         
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.opn_stk_cost,
                                   'OPN_STK_COST',
                                   L_currency_cost_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'OPN_STK_COST',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.purch_retail is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.purch_retail,
                                   'PURCH_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'PURCH_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.purch_cost is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.purch_cost,
                                   'PURCH_COST',
                                   L_currency_cost_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'PURCH_COST',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.rtv_retail is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.rtv_retail,
                                   'RTV_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'RTV_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.rtv_cost is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.rtv_cost,
                                   'RTV_COST',
                                   L_currency_cost_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'RTV_COST',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.net_sales_retail is NOT NULL then  
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.net_sales_retail,
                                   'NET_SALES_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'NET_SALES_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.net_sales_cost is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.net_sales_cost,
                                   'NET_SALES_COST',
                                   L_currency_cost_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'NET_SALES_COST',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.clear_markdown_retail is NOT NULL then   
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.clear_markdown_retail,
                                   'CLEAR_MARKDOWN_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'CLEAR_MARKDOWN_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.perm_markdown_retail is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.perm_markdown_retail,
                                   'PERM_MARKDOWN_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'PERM_MARKDOWN_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.prom_markdown_retail is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.prom_markdown_retail,
                                   'PROM_MARKDOWN_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'PROM_MARKDOWN_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.shrinkage_retail is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.shrinkage_retail,
                                   'SHRINKAGE_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'SHRINKAGE_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.shrinkage_cost is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.shrinkage_cost,
                                   'SHRINKAGE_COST',
                                   L_currency_cost_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'SHRINKAGE_COST',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.empl_disc_retail is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.empl_disc_retail,
                                   'EMPL_DISC_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'EMPL_DISC_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.cls_stk_retail is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.cls_stk_retail,
                                   'CLS_STK_RETAIL',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'CLS_STK_RETAIL',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
       if I_rec.cls_stk_cost is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.cls_stk_cost,
                                   'CLS_STK_COST',
                                   L_currency_cost_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'CLS_STK_COST',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;
         if I_rec.gross_margin is NOT NULL then
            if PROCESS_MDB_PRE_VAL(O_error_message,
                                   O_error,
                                   I_rec,
                                   I_rec.gross_margin,
                                   'GROSS_MARGIN',
                                   L_currency_rtl_fmt) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'GROSS_MARGIN',
                           O_error_message);
               O_error := TRUE;
            end if; 
         end if;   
      end if;                                        
   end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_MDB_VAL;
--------------------------------------------------------------------------------
FUNCTION EXEC_MDB_UPD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_mdb_temp_rec    IN       MONTH_DATA_BUDGET%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_MDB.EXEC_MDB_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_MONTH_DATA_BUDGET';   
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_MDB_UPD is
      select 'x'
        from MONTH_DATA_BUDGET
       where location = I_mdb_temp_rec.location
         and loc_type = I_mdb_temp_rec.loc_type
         and half_no = I_mdb_temp_rec.half_no
         and month_no = I_mdb_temp_rec.month_no
         and dept = I_mdb_temp_rec.dept
         and set_of_books_id = I_mdb_temp_rec.set_of_books_id       
         for update nowait;

BEGIN
   open  C_LOCK_MDB_UPD;
   close C_LOCK_MDB_UPD;
   
   update MONTH_DATA_BUDGET 
      set row = I_mdb_temp_rec 
    where location = I_mdb_temp_rec.location
      and loc_type = I_mdb_temp_rec.loc_type
      and half_no = I_mdb_temp_rec.half_no
      and month_no = I_mdb_temp_rec.month_no
      and dept = I_mdb_temp_rec.dept
      and set_of_books_id = I_mdb_temp_rec.set_of_books_id;
   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_mdb_temp_rec.dept,
                                             I_mdb_temp_rec.month_no);
      return FALSE;
   when OTHERS then
      if C_LOCK_MDB_UPD%ISOPEN then
         close C_LOCK_MDB_UPD;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program, 
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_MDB_UPD;
--------------------------------------------------------------------------------
FUNCTION PROCESS_MDB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id      IN       SVC_MONTH_DATA_BUDGET.process_id%TYPE,
                     I_chunk_id        IN       SVC_MONTH_DATA_BUDGET.chunk_id%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_MDB.PROCESS_MDB';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_MONTH_DATA_BUDGET';
   L_mdb_temp_rec    MONTH_DATA_BUDGET%ROWTYPE;
   L_process_error   BOOLEAN := FALSE; 
   L_do_mod          BOOLEAN := FALSE;
   L_error           BOOLEAN;

BEGIN
   FOR rec IN C_SVC_MONTH_DATA_BUDGET(I_process_id,
                                      I_chunk_id)
   LOOP
      L_do_mod := FALSE;
      L_error := FALSE;
      if rec.action is NULL or rec.action <> action_mod then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_mod 
         and rec.PK_MDB_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Department,Month,Half,Location Type,Location,Set of Books ID',
                     'NO_RECORD');
         L_error :=TRUE;
      end if;
     
      if ((NVL(rec.opn_stk_retail,-1) <> NVL(rec.mdb_opn_stk_retail,-1))
         or (NVL(rec.gross_margin,-1) <> NVL(rec.mdb_gross_margin,-1))
         or (NVL(rec.cls_stk_cost,-1) <> NVL(rec.mdb_cls_stk_cost,-1))
         or (NVL(rec.purch_retail,-1) <> NVL(rec.mdb_purch_retail,-1))
         or (NVL(rec.purch_cost,-1) <> NVL(rec.mdb_purch_cost,-1))
         or (NVL(rec.rtv_retail,-1) <> NVL(rec.mdb_rtv_retail,-1))
         or (NVL(rec.rtv_cost,-1) <> NVL(rec.mdb_rtv_cost,-1))
         or (NVL(rec.net_sales_retail,-1) <> NVL(rec.mdb_net_sales_retail,-1))
         or (NVL(rec.net_sales_cost,-1) <> NVL(rec.mdb_net_sales_cost,-1))
         or (NVL(rec.clear_markdown_retail,-1) <> NVL(rec.mdb_clear_markdown_retail,-1))
         or (NVL(rec.perm_markdown_retail,-1) <> NVL(rec.mdb_perm_markdown_retail,-1))
         or (NVL(rec.prom_markdown_retail,-1) <> NVL(rec.mdb_prom_markdown_retail,-1))
         or (NVL(rec.shrinkage_retail,-1) <> NVL(rec.mdb_shrinkage_retail,-1))
         or (NVL(rec.shrinkage_cost,-1) <> NVL(rec.mdb_shrinkage_cost,-1))
         or (NVL(rec.empl_disc_retail,-1) <> NVL(rec.mdb_empl_disc_retail,-1))
         or (NVL(rec.cls_stk_retail,-1) <> NVL(rec.mdb_cls_stk_retail,-1))
         or (NVL(rec.opn_stk_cost,-1) <> NVL(rec.mdb_opn_stk_cost,-1))) then
         L_do_mod := TRUE;
      end if;	         
			
      if L_do_mod then	
         if PROCESS_MDB_VAL(O_error_message,
                            L_error,
                            rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error := TRUE;
         end if;
      end if;
	  
      if NOT L_error
         and L_do_mod then
         L_mdb_temp_rec.set_of_books_id       := rec.set_of_books_id;
         L_mdb_temp_rec.opn_stk_cost          := rec.opn_stk_cost;
         L_mdb_temp_rec.purch_retail          := rec.purch_retail;
         L_mdb_temp_rec.purch_cost            := rec.purch_cost;
         L_mdb_temp_rec.rtv_retail            := rec.rtv_retail;
         L_mdb_temp_rec.rtv_cost              := rec.rtv_cost;
         L_mdb_temp_rec.net_sales_retail      := rec.net_sales_retail;
         L_mdb_temp_rec.net_sales_cost        := rec.net_sales_cost;
         L_mdb_temp_rec.clear_markdown_retail := rec.clear_markdown_retail;
         L_mdb_temp_rec.perm_markdown_retail  := rec.perm_markdown_retail;
         L_mdb_temp_rec.prom_markdown_retail  := rec.prom_markdown_retail;
         L_mdb_temp_rec.shrinkage_retail      := rec.shrinkage_retail;
         L_mdb_temp_rec.shrinkage_cost        := rec.shrinkage_cost;
         L_mdb_temp_rec.empl_disc_retail      := rec.empl_disc_retail;
         L_mdb_temp_rec.cls_stk_retail        := rec.cls_stk_retail;
         L_mdb_temp_rec.cls_stk_cost          := rec.cls_stk_cost;
         L_mdb_temp_rec.gross_margin          := rec.gross_margin;
         L_mdb_temp_rec.dept                  := rec.dept;
         L_mdb_temp_rec.month_no              := rec.month_no;
         L_mdb_temp_rec.half_no               := rec.half_no;
         L_mdb_temp_rec.loc_type              := rec.loc_type;
         L_mdb_temp_rec.location              := rec.location;
         L_mdb_temp_rec.opn_stk_retail        := rec.opn_stk_retail;
         if EXEC_MDB_UPD(O_error_message,
                         L_mdb_temp_rec) = FALSE then
            WRITE_ERROR(rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        rec.chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_process_error := TRUE;
         end if; 
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_MDB;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_month_data_budget 
      where process_id = I_process_id;
END;
---------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_MDB.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';  
   
BEGIN
   Lp_errors_tab := NEW errors_tab_typ();
   if PROCESS_MDB(O_error_message,
                  I_process_id,
                  I_chunk_id) = FALSE then
      return FALSE;
   end if;  
   O_error_count := Lp_errors_tab.count();
   forall i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER 
           values Lp_errors_tab(i);
   Lp_errors_tab := NEW errors_tab_typ();
   
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
	 else
	    L_process_status := 'PE';
	 end if;
		
   update svc_process_tracker
		  set status = (CASE
			              when status = 'PE'
			              then 'PE'
                    else L_process_status
                    END),
		      action_date = SYSDATE
		where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_MDB;
/