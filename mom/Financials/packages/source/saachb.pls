
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_ACH_SQL AS
-------------------------------------------------------------------------
FUNCTION MANUAL_ACH_ADJ_EXISTS(O_error_message   IN OUT VARCHAR2,
                               O_exists          IN OUT BOOLEAN,
                               I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
                               I_business_date   IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN IS

   L_exist             VARCHAR2(1):= 'N';

   cursor C_MAN_ACH_ADJ_EXIST is
      select 'Y'
        from sa_bank_ach 
       where sa_bank_ach.partner_id     =  I_partner_id
         and sa_bank_ach.business_date  =  I_business_date
	   and sa_bank_ach.next_day_man_ach_adj is not NULL;

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_MAN_ACH_ADJ_EXIST ','SA_BANK_ACH',NULL);
   open C_MAN_ACH_ADJ_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_MAN_ACH_ADJ_EXIST ','SA_BANK_ACH',NULL);
   fetch C_MAN_ACH_ADJ_EXIST into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_MAN_ACH_ADJ_EXIST ','SA_BANK_ACH',NULL);
   close C_MAN_ACH_ADJ_EXIST;
   ---
   if L_exist = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_ACH_SQL.MANUAL_ACH_ADJ_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
END MANUAL_ACH_ADJ_EXISTS;
-------------------------------------------------------------------------
FUNCTION BANK_ACH_EXIST(O_error_message   IN OUT VARCHAR2,
                        O_exists          IN OUT BOOLEAN,
                        I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
		        I_bank_acct_no    IN     SA_BANK_STORE.BANK_ACCT_NO%TYPE,
                        I_business_date   IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN IS

   L_exist             VARCHAR2(1):= 'N';

   cursor C_BANK_ACH_EXIST is
      select 'Y'
        from sa_bank_ach
       where sa_bank_ach.partner_id    = I_partner_id
         and sa_bank_ach.business_date = I_business_date
	   and sa_bank_ach.bank_acct_no  = I_bank_acct_no;

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_BANK_ACH_EXIST ', 'SA_BANK_ACH', NULL);
   open C_BANK_ACH_EXIST ;
   SQL_LIB.SET_MARK('FETCH', 'C_BANK_ACH_EXIST ', 'SA_BANK_ACH', NULL);
   fetch C_BANK_ACH_EXIST into L_exist;
   SQL_LIB.SET_MARK('FETCH', 'C_BANK_ACH_EXIST ', 'SA_BANK_ACH', NULL);
   close C_BANK_ACH_EXIST ;
   ---
   if L_exist = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_ACH_SQL.BANK_ACH_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END BANK_ACH_EXIST;
-------------------------------------------------------------------------
FUNCTION BANK_ACCT_NO_EXISTS(O_error_message   IN OUT VARCHAR2,
                             O_exists          IN OUT BOOLEAN,
                             I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
                             I_bank_acct_no    IN     SA_BANK_STORE.BANK_ACCT_NO%TYPE)
   RETURN BOOLEAN IS

   L_exist             VARCHAR2(1):= 'N';

   cursor C_BANK_ACCT_EXIST_P is
      select 'Y'
        from sa_bank_store 
       where sa_bank_store.partner_id    = I_partner_id
	   and sa_bank_store.bank_acct_no  = I_bank_acct_no;

   cursor C_BANK_ACCT_EXIST is
      select 'Y'
        from sa_bank_store 
       where sa_bank_store.bank_acct_no  = I_bank_acct_no;

BEGIN
   O_exists := FALSE;
   ---
   if I_partner_id is NOT NULL then
      SQL_LIB.SET_MARK('OPEN', ' C_BANK_ACCT_EXIST_P ', 'SA_BANK_STORE', 'partner_id:'||I_partner_id||'bank_acct_no: '||I_bank_acct_no);
      open  C_BANK_ACCT_EXIST_P  ;
      SQL_LIB.SET_MARK('FETCH', 'C_BANK_ACH_EXIST_P ', 'SA_BANK_STORE', 'partner_id:'||I_partner_id||'bank_acct_no: '||I_bank_acct_no);
      fetch  C_BANK_ACCT_EXIST_P into L_exist;
      SQL_LIB.SET_MARK('FETCH', 'C_BANK_ACH_EXIST_P ', 'SA_BANK_STORE', 'partner_id:'||I_partner_id||'bank_acct_no: '||I_bank_acct_no);
      close  C_BANK_ACCT_EXIST_P ;
   elsif I_partner_id is NULL then
      SQL_LIB.SET_MARK('OPEN', ' C_BANK_ACCT_EXIST', 'SA_BANK_STORE', 'bank_acct_no: '||I_bank_acct_no);
      open  C_BANK_ACCT_EXIST;
      SQL_LIB.SET_MARK('FETCH', 'C_BANK_ACH_EXIST', 'SA_BANK_STORE', 'bank_acct_no: '||I_bank_acct_no);
      fetch  C_BANK_ACCT_EXIST into L_exist;
      SQL_LIB.SET_MARK('FETCH', 'C_BANK_ACH_EXIST', 'SA_BANK_STORE', 'bank_acct_no: '||I_bank_acct_no);
      close  C_BANK_ACCT_EXIST;
   end if;

   if L_exist = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_ACH_SQL.BANK_ACCT_NO_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END BANK_ACCT_NO_EXISTS;
-------------------------------------------------------------------------
FUNCTION MANUAL_DEPOSIT_ADJ_EXISTS(O_error_message  IN OUT VARCHAR2,
                                   O_exists         IN OUT BOOLEAN,
                                   I_partner_id     IN     PARTNER.PARTNER_ID%TYPE,
                                   I_business_date  IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN IS

   L_exist             VARCHAR2(1):= 'N';

   cursor C_MAN_DEP_ADJ_EXISTS is
      select 'Y'
        from sa_store_ach 
       where sa_store_ach.partner_id         =  I_partner_id
	   and sa_store_ach.business_date      =  I_business_date
	   and sa_store_ach.next_day_man_adj_deposit is not NULL;

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_MAN_DEP_ADJ_EXISTS', 'SA_STORE_ACH', 'partner_id: '||I_partner_id||'business_date:'||I_business_date);
   open C_MAN_DEP_ADJ_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_MAN_DEP_ADJ_EXISTS', 'SA_STORE_ACH', 'partner_id: '||I_partner_id||'business_date:'||I_business_date);
   fetch C_MAN_DEP_ADJ_EXISTS into L_exist;
   SQL_LIB.SET_MARK('FETCH', 'C_MAN_DEP_ADJ_EXISTS', 'SA_STORE_ACH', 'partner_id: '||I_partner_id||'business_date:'||I_business_date);
   close C_MAN_DEP_ADJ_EXISTS;
   ---
   if L_exist = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_ACH_SQL.MANUAL_DEPOSIT_ADJ_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END MANUAL_DEPOSIT_ADJ_EXISTS;
-------------------------------------------------------------------------
FUNCTION STORE_ACH_EXIST (O_error_message   IN OUT VARCHAR2,
                          O_exists          IN OUT BOOLEAN,
			        I_store		  IN	   STORE.STORE%TYPE,
                          I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
                          I_business_date   IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN IS

   L_exist             VARCHAR2(1):= 'N';

   cursor C_STORE_ACH_EXISTS is
      select 'Y'
        from sa_store_ach
       where sa_store_ach.partner_id    = I_partner_id
	   and sa_store_ach.business_date = I_business_date
	   and sa_store_ach.store         = I_store;

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_STORE_ACH_EXISTS', 'SA_STORE_ACH', NULL);
   open C_STORE_ACH_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_STORE_ACH_EXISTS', 'SA_STORE_ACH', NULL);
   fetch C_STORE_ACH_EXISTS into L_exist;
   SQL_LIB.SET_MARK('FETCH', 'C_STORE_ACH_EXISTS',  'SA_STORE_ACH', NULL);
   close C_STORE_ACH_EXISTS;
   ---
   if L_exist = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_ACH_SQL.STORE_ACH_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END STORE_ACH_EXIST;
-------------------------------------------------------------------------
FUNCTION GET_MAX_BUS_DATE (O_error_message   IN OUT VARCHAR2,
                           O_business_date   IN OUT SA_STORE_DAY.BUSINESS_DATE%TYPE,
                           I_store           IN     STORE.STORE%TYPE,
                           I_partner_id      IN     PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_MAX_DATE is
      select sa_store_ach.business_date
        from sa_store_ach
       where sa_store_ach.partner_id       = I_partner_id
	   and sa_store_ach.store          = I_store
         and sa_store_ach.today_adj_deposit_est is not NULL;

BEGIN
   ---
   if (I_store is NOT NULL and I_partner_id is NOT NULL) then
      SQL_LIB.SET_MARK('OPEN', 'C_GET_MAX_DATE', 'SA_STORE_ACH', 'store: '||I_store||'partner_id'||I_partner_id);
      open C_GET_MAX_DATE;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_MAX_DATE', 'SA_STORE_ACH', 'store: '||I_store||'partner_id'||I_partner_id);
      fetch C_GET_MAX_DATE into O_business_date;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_MAX_DATE', 'SA_STORE_ACH', 'store: '||I_store||'partner_id'||I_partner_id);
      close C_GET_MAX_DATE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_ACH_SQL.GET_MAX_BUS_DATE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_MAX_BUS_DATE;
-------------------------------------------------------------------------
FUNCTION GET_MAX_BUS_DATE_BANK (O_error_message   IN OUT VARCHAR2,
                                O_business_date   IN OUT SA_BANK_ACH.BUSINESS_DATE%TYPE,
                                I_bank_acct_no    IN     SA_BANK_ACH.BANK_ACCT_NO%TYPE,
                                I_partner_id      IN     SA_BANK_ACH.PARTNER_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_MAX_DATE is
      select sa_bank_ach.business_date
        from sa_bank_ach
       where sa_bank_ach.partner_id      = I_partner_id
	   and sa_bank_ach.bank_acct_no  = I_bank_acct_no
         and sa_bank_ach.next_day_ach_amt is not NULL;

BEGIN
   if (I_bank_acct_no is NOT NULL and I_partner_id is NOT NULL) then
      SQL_LIB.SET_MARK('OPEN', 'C_GET_MAX_DATE', 'SA_BANK_ACH', 'bank_acct_no: '||I_bank_acct_no||'partner_id'||I_partner_id);
      open C_GET_MAX_DATE;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_MAX_DATE', 'SA_BANK_ACH', 'bank_acct_no: '||I_bank_acct_no||'partner_id'||I_partner_id);
      fetch C_GET_MAX_DATE into O_business_date;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_MAX_DATE', 'SA_BANK_ACH', 'bank_acct_no: '||I_bank_acct_no||'partner_id'||I_partner_id);
      close C_GET_MAX_DATE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_ACH_SQL.GET_MAX_BUS_DATE_BANK',
                                             to_char(SQLCODE));
      return FALSE;
END GET_MAX_BUS_DATE_BANK;
-------------------------------------------------------------------------
FUNCTION CHECK_STORE_EXISTS (O_error_message   IN OUT VARCHAR2,
                             O_exists          IN OUT BOOLEAN,
                             I_store           IN     STORE.STORE%TYPE,
                             I_partner_id      IN     SA_BANK_ACH.PARTNER_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_CHECK_STORE is
      select 'Y'
        from v_store s, sa_bank_store b
       where s.store = b.store
         and (I_partner_id = b.partner_id 
          or I_partner_id is NULL)  
         and s.store = I_store;

   L_exist         VARCHAR2(1) := 'N';
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   O_exists := FALSE;
   --
   SQL_LIB.SET_MARK('OPEN','C_CHECK_STORE','SA_BANK_STORE',NULL);
   open C_CHECK_STORE;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_STORE','SA_BANK_STORE',NULL);
   fetch C_CHECK_STORE into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_STORE','SA_BANK_STORE',NULL);
   close C_CHECK_STORE;
   --
   if L_exist = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_ACH_SQL.CHECK_STORE_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_STORE_EXISTS;
-------------------------------------------------------------------------
FUNCTION CHECK_BANK_EXISTS (O_error_message   IN OUT VARCHAR2,
                            O_exists          IN OUT BOOLEAN,
                            I_store           IN     STORE.STORE%TYPE,
                            I_partner_id      IN     SA_BANK_ACH.PARTNER_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_CHECK_BANK is
      select 'Y'
        from partner p,
             sa_bank_store b
       where p.partner_id = b.partner_id
         and p.partner_type = 'BK'
         and (I_store = b.store
          or I_store is NULL)
         and p.partner_id = I_partner_id;

   L_exist         VARCHAR2(1) := 'N';
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   O_exists := FALSE;
   --
   SQL_LIB.SET_MARK('OPEN','C_CHECK_BANK','SA_BANK_STORE',NULL);
   open C_CHECK_BANK;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_BANK','SA_BANK_STORE',NULL);
   fetch C_CHECK_BANK into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_BANK','SA_BANK_STORE',NULL);
   close C_CHECK_BANK;
   --
   if L_exist = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_ACH_SQL.CHECK_BANK_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_BANK_EXISTS;
-------------------------------------------------------------------------
END SA_ACH_SQL;
/