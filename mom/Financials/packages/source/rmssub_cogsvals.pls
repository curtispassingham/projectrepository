
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_COGS_VALIDATE AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION CHECK_MESSAGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cogs_rec      IN OUT RMSSUB_COGS.COGS_REC_TYPE,
                       I_message       IN     "RIB_CogsDesc_REC",
                       I_message_type  IN     VARCHAR2)
RETURN BOOLEAN;


END RMSSUB_COGS_VALIDATE;
/
