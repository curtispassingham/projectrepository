
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_COGS_VALIDATE AS

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------

G_cogs_rec   RMSSUB_COGS.COGS_REC_TYPE;


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION CHECK_FIELDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message       IN     "RIB_CogsDesc_REC",
                      I_message_type  IN     VARCHAR2)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

FUNCTION CHECK_MESSAGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cogs_rec      IN OUT RMSSUB_COGS.COGS_REC_TYPE,
                       I_message       IN     "RIB_CogsDesc_REC",
                       I_message_type  IN     VARCHAR2)
RETURN BOOLEAN IS

L_program   VARCHAR2(50) := 'RMSSUB_COGS_VALIDATE.CHECK_MESSAGE';

BEGIN

   if I_message is null then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', null, null,
      null);
      return FALSE;
   end if;

   if CHECK_FIELDS(O_error_message,
                   I_message,
                   I_message_type) = FALSE then
      return FALSE;
   end if;

   O_cogs_rec := G_cogs_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;


--------------------------------------------------------------------------------
-- PRIVATE PROCEDURES
--------------------------------------------------------------------------------

FUNCTION CHECK_FIELDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message       IN     "RIB_CogsDesc_REC",
                      I_message_type  IN     VARCHAR2)
RETURN BOOLEAN IS

L_program    VARCHAR2(50) := 'RMSSUB_COGS_VALIDATE.CHECK_FIELDS';

L_item_rec   ITEM_MASTER%ROWTYPE;

BEGIN

   -- trans_date
   if I_message.trans_date is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'trans_date',
      null, null);
      return FALSE;
   end if;

   -- item
   if I_message.item is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item', null,
      null);
      return FALSE;
   else
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_rec,
                                         I_message.item) = FALSE then
         return FALSE;
      end if;
   end if;

   -- store
   if I_message.store is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store', null,
      null);
      return FALSE;
   end if;

   -- units
   if I_message.units is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'units', null,
      null);
      return FALSE;
   end if;

   -- header_media
   if I_message.header_media is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'header_media',
      null, null);
      return FALSE;
   end if;

   -- line_media
   if I_message.line_media is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'line_media', null,
      null);
      return FALSE;
   end if;

   -- reason_code
   if I_message.reason_code is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'reason_code', null,
      null);
      return FALSE;
   end if;

   G_cogs_rec.trans_date := I_message.trans_date;
   G_cogs_rec.item := I_message.item;
   G_cogs_rec.store := I_message.store;
   G_cogs_rec.units := I_message.units;
   G_cogs_rec.header_media := I_message.header_media;
   G_cogs_rec.line_media := I_message.line_media;
   G_cogs_rec.reason_code := I_message.reason_code;
   G_cogs_rec.supplier := I_message.supplier;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FIELDS;
--------------------------------------------------------------------------------

END RMSSUB_COGS_VALIDATE;
/
