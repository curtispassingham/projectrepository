
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STORE_DAY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
-- Function: GET_STORE_DAY
-- Purpose : Retrieve the store number and the business date for
--           the given store day sequence number.
--
FUNCTION GET_STORE_DAY(O_error_message      IN OUT  VARCHAR2,
                       O_store              IN OUT  SA_STORE_DAY.STORE%TYPE,
                       O_business_date      IN OUT  SA_STORE_DAY.BUSINESS_DATE%TYPE,
                       I_store_day_seq_no   IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_INTERNAL_DAY
-- Purpose : Retrieve the store and internal day for
--           the given store day sequence number. For used by USPS
--
FUNCTION GET_INTERNAL_DAY(O_error_message      IN OUT  VARCHAR2,
                          O_store              IN OUT  SA_STORE_DAY.STORE%TYPE,
                          O_day                IN OUT  SA_STORE_DAY.DAY%TYPE,
                          I_store_day_seq_no   IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                          I_total_seq_no       IN      SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                          I_error_seq_no       IN      SA_ERROR.ERROR_SEQ_NO%TYPE,
                          I_update_id          IN      SA_TRAN_HEAD.UPDATE_ID%TYPE,
                          I_tran_seq_no        IN      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_TRAN_COUNT
-- Purpose : Retrieve the number of transactions for the store day.
--
FUNCTION GET_TRAN_COUNT(O_error_message      IN OUT  VARCHAR2,
                        O_tran_count         IN OUT  NUMBER,
                        I_store_day_seq_no   IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                        I_bal_group_seq_no   IN      SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                        I_store              IN      SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                        I_day                IN      SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_ERROR_COUNT
-- Purpose : Retrieve the number of errors for the store day, taking
--           into account the type of employee accessing the data (HQ/Store).
--
FUNCTION GET_ERROR_COUNT(O_error_message     IN OUT   VARCHAR2,
                         O_error_count       IN OUT   NUMBER,
                         I_store_day_seq_no  IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                         I_bal_group_seq_no  IN       SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
				 I_emp_type	  	   IN		SA_EMPLOYEE.EMP_TYPE%TYPE,
                         I_store             IN       SA_ERROR.STORE%TYPE DEFAULT NULL,
                         I_day               IN       SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: UPDATE_AUDIT_STATUS
-- Purpose:  Update the status to the value passed in the input.
--           variable and update the audit status changed date/time
--           to the system date.
--
FUNCTION UPDATE_AUDIT_STATUS(O_error_message    IN OUT VARCHAR2,
                             I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                             I_new_status       IN     SA_STORE_DAY.AUDIT_STATUS%TYPE,
                             I_store            IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day              IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_COUNT_INFO
-- Purpose:  Retrieve the transaction count, error count, and
--           O/S value for the given store day sequence number
--           store day balance group, based on Emplpyee Type.
--
FUNCTION GET_COUNT_INFO(O_error_message     IN OUT  VARCHAR2,
                        O_tran_count        IN OUT  NUMBER,
                        O_error_count       IN OUT  NUMBER,
                        O_os_value          IN OUT  SA_HQ_VALUE.HQ_VALUE%TYPE,
                        I_os_level          IN      VARCHAR2,
                        I_store_day_seq_no  IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                        I_bal_group_seq_no  IN      SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
				I_emp_type	  	  IN	    SA_EMPLOYEE.EMP_TYPE%TYPE,
                        I_store             IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day               IN      SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_STORE_DAY_SEQ
-- Purpose : This function gets the store day sequence number for
--           a selected store and business date.
--
FUNCTION GET_STORE_DAY_SEQ(O_error_message     IN OUT  VARCHAR2,
                           O_store_day_seq_no  IN OUT  SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           O_exists            IN OUT  BOOLEAN,
                           I_store             IN      STORE.STORE%TYPE,
                           I_business_date     IN      SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_AUDIT_STATUS
-- Purpose : This function gets the audit status for a selected
--           store day sequence number.
--
FUNCTION GET_AUDIT_STATUS(O_error_message     IN OUT  VARCHAR2,
                          O_audit_status      IN OUT  SA_STORE_DAY.AUDIT_STATUS%TYPE,
                          I_store_day_seq_no  IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                          I_store             IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                          I_day               IN      SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_DATA_STATUS
-- Purpose : This function gets the Data status for a selected
--           store day sequence number.
--
FUNCTION GET_DATA_STATUS(O_error_message     IN OUT  VARCHAR2,
                         O_data_status       IN OUT  SA_STORE_DAY.DATA_STATUS%TYPE,
                         I_store_day_seq_no  IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                         I_store             IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                         I_day               IN      SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: UPDATE_DATA_STATUS
-- Purpose:  This function updates the Data status for the passed in
--           store day sequence number.
--
FUNCTION UPDATE_DATA_STATUS(O_error_message     IN OUT  VARCHAR2,
                            I_store_day_seq_no  IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                            I_new_data_status   IN      SA_STORE_DAY.DATA_STATUS%TYPE,
                            I_store             IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                            I_day               IN      SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_STORE_STATUS
-- Purpose : This function gets the Store status for a selected
--           store day sequence number.
--
FUNCTION GET_STORE_STATUS(O_error_message     IN OUT  VARCHAR2,
                          O_store_status      IN OUT  SA_STORE_DAY.STORE_STATUS%TYPE,
                          I_store_day_seq_no  IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                          I_store             IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                          I_day               IN      SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: UPDATE_STORE_FUEL_STATUS
-- Purpose : This function updates the Store Status to 'F' (Fuel Close)
--           for the passed in Store and Business Date.  This funciton
--           will be called by an external Fuel Management System
--           to indicate that the Store Day has been closed in the
--           Fuel System and therefore the Store Day can now be
--           closed in the Sales Audit system.
--
FUNCTION UPDATE_STORE_FUEL_STATUS(O_error_message IN OUT VARCHAR2,
                                  I_store         IN     STORE.STORE%TYPE,
                                  I_business_date IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: GET_INTEGRATED_POS_IND
-- Purpose : This function will retrieve the POS Integrated indicator
--           for the given Store on the store table.
--
FUNCTION GET_INTEGRATED_POS_IND(O_error_message      IN OUT VARCHAR2,
                                O_integrated_pos_ind IN OUT STORE.INTEGRATED_POS_IND%TYPE,
                                I_store              IN     STORE.STORE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: DELETE_STORE_DAY
-- Purpose:  This function set the status of all transactions for
--           the passed in store day sequence number to 'Deleted'.
--
FUNCTION DELETE_STORE_DAY(O_error_message    IN OUT VARCHAR2,
                          I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                          I_store            IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                          I_day              IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: TOTAL_AUDIT
-- Purpose:  This function executes the Automated Totaling process
--           and then executes the Automated Audit Rule process for
--           the passed in store day sequence number.
--
FUNCTION TOTAL_AUDIT(O_error_message    IN OUT VARCHAR2,
                     I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                     I_user_id          IN     SA_EMPLOYEE.USER_ID%TYPE,
                     I_pos_totals_ind   IN     VARCHAR2,
                     I_store            IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                     I_day              IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: STATUS_FIX
-- Purpose:  Updates the store_day's audit_status to either 'A' or 'H'
--           if all the store or hq override_ind's are set to 'Y'.
--
FUNCTION STATUS_FIX(O_error_message        IN OUT VARCHAR2,
                    I_user_id              IN OUT SA_EMPLOYEE.USER_ID%TYPE,
                    I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                    I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                    I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function: PERFORM_SFM_FULL_CLOSE
-- Purpose:  Calls the SFM perform full close procedure for the store/day.
--
-- This function should only be active if the system uses Site Fuel Management.
/* FUNCTION PERFORM_SFM_FULL_CLOSE(O_error_message  IN OUT  VARCHAR2,
		                I_store_day_seq_no IN    SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                                I_store            IN    SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                                I_business_date    IN    SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL)
   RETURN BOOLEAN; */
--------------------------------------------------------------------
-- Function: UPDATE_TOTAL_AUDIT
-- Purpose:  Updates the audit status to 'R' and optionally performs the totaling
--           and auditing process for all store/days affected by the passed in
--           total_id or rule_id.
--
FUNCTION UPDATE_TOTAL_AUDIT(O_error_message        IN OUT VARCHAR2,
                            O_locked_list          IN OUT VARCHAR2,
                            O_locked_count         IN OUT NUMBER,
                            O_success_list         IN OUT VARCHAR2,
                            O_success_count        IN OUT NUMBER,
                            I_user_id              IN     SA_EMPLOYEE.USER_ID%TYPE,
                            I_rule_id              IN     SA_RULE_HEAD.RULE_ID%TYPE,
                            I_total_id             IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                            I_rev_no               IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                            I_run_total_audit_ind  IN     VARCHAR2,
                            I_update_datetime      IN     SA_STORE_DAY.AUDIT_CHANGED_DATETIME%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: GET_ERROR_COUNT_STATUS
-- Purpose : Retrieve the number of errors for the store day, taking
--           into account the type of employee accessing the data (HQ/Store) and the status.
----------------------------------------------------------------------------------------------------
FUNCTION GET_ERROR_COUNT_STATUS(O_error_message     IN OUT   VARCHAR2,
                                O_error_count       IN OUT   NUMBER,
                                I_store_day_seq_no  IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                                I_bal_group_seq_no  IN       SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
			        I_emp_type	    IN	     SA_EMPLOYEE.EMP_TYPE%TYPE,
                                I_error_status      IN       V_SA_ERROR_ALL.STATUS%TYPE,
                                I_store             IN       SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                                I_day               IN       SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: GET_LOCATION_INFO
-- Purpose : Retrieve the store, store name, chain, and chain name for the specified
--           store/day sequence number.
--
FUNCTION GET_LOCATION_INFO(O_error_message      IN OUT  VARCHAR2,
                           O_store              IN OUT  SA_STORE_DAY.STORE%TYPE,
                           O_store_name         IN OUT  STORE.STORE_NAME%TYPE,
                           O_chain              IN OUT  SA_STORE_DAY.STORE%TYPE,
                           O_chain_name         IN OUT  STORE.STORE_NAME%TYPE,
                           I_store_day_seq_no   IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Function: SETUP_FOR_AUDIT
-- Purpose : This function will insert all needed balance groups for the specified store/day. It will
--           update the required errors for the balance groups. It will adjust needed post void
--           transactions for the specified store/day. It will delete all of the missing transaction
--           records that now have transactions associated with them for the store/day. This function
--           provides the same functionality as CreateBalanceGroup, UpdateErrors, FixPostVoid, and
--           FixMissTran in saimpltogfin.pc except for only one specified store/day.
--
FUNCTION SETUP_FOR_AUDIT(O_error_message    IN OUT  VARCHAR2,
                         I_store_day_seq_no IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                         I_store            IN      SA_STORE_DAY.STORE%TYPE,
                         I_day              IN      SA_STORE_DAY.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Function: GET_DAY_POST_SALE
-- Purpose : To find out whether the business date 
--           is valid or it is too old to load into the system
--
FUNCTION GET_DAY_POST_SALE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,                          
                           I_business_date   IN      SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------

END STORE_DAY_SQL;
/
