
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_PARM_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------
--- Function Name: GET_NEXT_ID
--- Purpose:       
--- Calls:         None
--- Created by:    Sonia Wong, 10-Aug-98
---
FUNCTION GET_NEXT_ID(O_error_message   IN OUT VARCHAR2,
                     O_parm_id         IN OUT sa_parm.parm_id%TYPE)
                     return BOOLEAN;
------------------------------------------------------------------------
--- Function Name: GET_REALM_ID
--- Purpose:       This function will return the ID of the realm
---                which is associated with a specified parameter id.
--- Calls:         None
--- Created by:    Andy Beger, 29-Sep-98
---
FUNCTION GET_REALM_ID(O_error_message IN OUT VARCHAR2,
                      O_realm_id      IN OUT sa_realm.realm_id%TYPE,
                      I_parm_id       IN     sa_parm.parm_id%TYPE)
                      return BOOLEAN;
------------------------------------------------------------------------
--- Function Name: GET_UPDATED_PARM_ID
--- Purpose:       Get the updated Parm ID based on original parm and realm IDs
---                and the new realm_id;
---
FUNCTION GET_UPDATED_PARM_ID(O_error_message     IN OUT  VARCHAR2,
                             O_new_parm_id       IN OUT  SA_PARM.PARM_ID%TYPE,
                             I_original_parm_id  IN      SA_PARM.PARM_ID%TYPE,
                             I_original_realm_id IN      SA_REALM.REALM_ID%TYPE,
                             I_new_realm_id      IN      SA_REALM.REALM_ID%TYPE)

   return BOOLEAN;
------------------------------------------------------------------------
END SA_PARM_SQL;
/
