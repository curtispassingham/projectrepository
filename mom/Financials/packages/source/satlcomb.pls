
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_TOTAL_COMMON
as
/*----------------------------------------------------------------------------------------------
|| This is the full version that tests for the successful creation of an object of the given
|| type and name.
----------------------------------------------------------------------------------------------*/
function Call_Dynamic_Sql(v_spec IN sa_total_common.v_code_array,
                          v_object_type IN varchar2,
                          v_object_name IN varchar2,
                          o_error_message OUT varchar2,
                          v_object_owner IN varchar2 := USER)
return boolean
is
   cursor c_get_status is
      select status
        from all_objects
       where lower(object_name) = lower(v_object_name)
         and lower(object_type) = lower(v_object_type)
         and lower(owner)       = lower(v_object_owner) ;

   cursor c_get_owner is
      select table_owner
        from system_options;
 
   call_array       dbms_sql.varchar2s ;
   cur              binary_integer := dbms_sql.open_cursor ;
   n_cur_line       number ;
   v_status         all_objects.status%TYPE ;
   n_result         number ;
   b_status         boolean ;
begin

if V_internal_schema_name is NULL then
   open c_get_owner;
   fetch c_get_owner into V_internal_schema_name;
   close c_get_owner;
end if;       

   /*----------------------------------------------------------------------------------------------
   || Copy our v_code_array into a VARCHAR2S array suitable for parsing long (> 32K) statements in
   || dbms_sql.  This routine provides a choke point to centralize these calls for ease of using
   || ARI under Oracle 7.x which doesn't support the VARCHAR2S data type.
   ----------------------------------------------------------------------------------------------*/
/*   sa_error_logging.write_log(
                                 3,
                                 'sa_total_common.call_dynamic_sql',
                                 'Creating ' || v_object_type || ' ' || v_object_owner || '.' || v_object_name
                               ) ;     */
   for n_cur_line in 1 .. v_spec.count
   loop
      call_array(call_array.count+1) := v_spec(n_cur_line) ;
   end loop ;
   /*----------------------------------------------------------------------------------------------
   || To use this code under Oracle 7.x, the following dynamic SQL calls will need to replaced by
   || a different mechanism.  Two that come to mind are to write the SQL code to a file and use
   || a SQL*Plus session to replace the code.  A better approach would be to use a daemon and
   || an Oracle DBMS_PIPE to do the same thing. C/Perl/Java are all good candidates for the daemon
   ----------------------------------------------------------------------------------------------*/
   begin
      dbms_sql.parse(cur, call_array, call_array.first, call_array.last, TRUE, DBMS_SQL.NATIVE) ;
      n_result := dbms_sql.execute(cur) ;
      dbms_sql.close_cursor(cur) ;
      /*----------------------------------------------------------------------------------------------
      || It appears that Oracle generates some extraneous exceptions when using dynamic sql to create
      || cross schema code objects.  To deal with this, we eat any exceptions at this point, log the
      || fact that the exception occurred, and then ignore the exception.  The following code then
      || checks to see if the exception exists, and if so, if it is in a valid state.
      ----------------------------------------------------------------------------------------------*/
/*      exception
         when others then
            sa_error_logging.write_log(
                                          2,
                                          'sa_total_common.call_dynamic_sql',
                                          'Create error logged for ' || v_object_type || ' ' || v_object_owner || '.'
                                                                     || v_object_name,
                                          to_char(SQLCODE),
                                          SQLERRM
                                       ) ;   */
   end ;
   /*----------------------------------------------------------------------------------------------
   || Oracle's implementation of DBMS_SQL doesn't appear to provide a way to detect a failed DDL
   || call.  For example, trying to create a PL/SQL procedure that contains a syntax error will
   || appear to run just fine.  In fact the procedure will have been created, but its status will
   || have been set to INVALID. The less than elegant work-around is to query the ALL_OBJECTS
   || view to retreive the status for an object of the appropriate type and owner. If the status
   || is VALID, a TRUE value is returned; otherwise, an error is written to the ARI_ERROR_LOG and
   || a FALSE value is returned.
   ||
   || NOTE: This is only processed if the object type and name are specified
   ----------------------------------------------------------------------------------------------*/
   if(v_object_name IS NOT NULL and v_object_type IS NOT NULL) then
      v_status := '!FOUND' ;
      open c_get_status ;
      fetch c_get_status into v_status ;
      b_status := c_get_status%FOUND ;
      close c_get_status ;
      if(lower(v_status) <> 'valid' or not b_status) then
/*         sa_error_logging.write_error(
                                        'ARI',
                                        'sa_total_common.call_dynamic_sql',
                                        'Creation of ' || v_object_type || ' ' || v_object_owner || '.'
                                                       || v_object_name || ' failed',
                                        'y',
                                        'n',
                                        'Status is ' || v_status
                                     ) ;   */
         return false ;
/*      else
         sa_error_logging.write_log(
                                       3,
                                       'sa_total_common.call_dynamic_sql',
                                       'Sucessfully created ' || v_object_type || ' ' || v_object_owner || '.'
                                                              || v_object_name
                                     ) ;    */
      end if ;
   end if ;
   return true ;
   exception
      when others then
         if DBMS_SQL.IS_OPEN(cur) then
            dbms_sql.close_cursor(cur) ;
         end if ;
         o_error_message := sql_lib.create_msg( 'PACKAGE_ERROR',
                                                 SQLERRM,
                                                 'SA_TOTAL_COMMON.CALL_DYNAMIC_SQL',
                                                 SQLCODE
                                              ) ;
/*         sa_error_logging.write_error(
                                        'ARI',
                                        'sa_total_common.call_dynamic_sql',
                                        'y',
                                        'n',
                                        'Error creating ' || v_object_type || ' object ' || v_object_owner || '.'
                                                          || v_object_name,
                                        o_error_message
                                     ) ;     */
         return FALSE ;
end ;
/*----------------------------------------------------------------------------------------------
|| This version is designed for dealing with general SQL commands.  The commands are wrapped
|| in a BEGIN/EXCEPTION/END block for execution and returns true if the command succeeded
|| and false if there was a problem.
----------------------------------------------------------------------------------------------*/
function Call_Dynamic_Sql(v_spec IN sa_total_common.v_code_array,
                          o_error_message OUT varchar2)
return boolean
is   


cursor c_get_owner is
      select table_owner
        from system_options;

   v_ddl      sa_total_common.v_code_array ;
   cur        integer := dbms_sql.open_cursor ;
   call_array dbms_sql.varchar2s ;
   b_result   boolean ;
   n_result   number ;
begin

if V_internal_schema_name is NULL then
   open c_get_owner;
   fetch c_get_owner into V_internal_schema_name;
   close c_get_owner;
end if;       

   /*----------------------------------------------------------------------------------------------
   || Wrap the call with a begin, exception and end clause to trap any execution errors.  Note that
   || there are two things going on here. (1) we are trapping any errors in this code, and (2) we
   || are trapping any errors in the code being executed dynamically.  Either case will generate
   || a false return value.
   ----------------------------------------------------------------------------------------------*/
   call_array(1) := 'begin' ;
   for i in 1 .. v_spec.count
   loop
      call_array(call_array.count+1) := v_spec(i) ;
   end loop ;
   call_array(call_array.count+1) := '   :result := 1 ;' ;
   call_array(call_array.count+1) := '   ' ;
   call_array(call_array.count+1) := 'exception' ;
   call_array(call_array.count+1) := '   when others then' ;
   call_array(call_array.count+1) := '      :result := 0 ;' ;
   call_array(call_array.count+1) := '      sa_error_logging.write_error( ' ;
   call_array(call_array.count+1) := '                                     ' || chr(39) || 'ARI' || chr(39) || ',' ;
   call_array(call_array.count+1) := '                                     ' || chr(39) || 'sa_total_common.call_dynamic_sql' || chr(39) || ',' ;
   call_array(call_array.count+1) := '                                     ' || chr(39) || 'y' || chr(39) || ',' ;
   call_array(call_array.count+1) := '                                     ' || chr(39) || 'n' || chr(39) || ',' ;
   call_array(call_array.count+1) := '                                     ' || chr(39) || 'Error executing dynamic SQL' || chr(39) || ',' ;
   call_array(call_array.count+1) := '                                     sqlerrm' ;
   call_array(call_array.count+1) := '                                   ) ;' ;
   call_array(call_array.count+1) := 'end ;' ;
   /*----------------------------------------------------------------------------------------------
   || To use this code under Oracle 7.x, the following dynamic SQL calls will need to replaced by
   || a different mechanism.  Two that come to mind are to write the SQL code to a file and use
   || a SQL*Plus session to replace the code.  A better approach would be to use a daemon and
   || an Oracle DBMS_PIPE to do the same thing. C/Perl/Java are all good candidates for the daemon
   ----------------------------------------------------------------------------------------------*/
   dbms_sql.parse(cur, call_array, call_array.first, call_array.last, TRUE, DBMS_SQL.NATIVE) ;
   dbms_sql.bind_variable(cur, 'result', 1) ;
   n_result := dbms_sql.execute(cur) ;
   dbms_sql.variable_value(cur, 'result', n_result) ;
   dbms_sql.close_cursor(cur) ;
   if(n_result = 1) then
      return true ;
   else
      return false ;
   end if ;
   exception
      when others then
         dbms_sql.close_cursor(cur) ;
         o_error_message := sql_lib.create_msg(
                                                 'PACKAGE_ERROR',
                                                 SQLERRM,
                                                 'SA_TOTAL_COMMON.CALL_DYNAMIC_SQL',
                                                 SQLCODE
                                              ) ;
/*         sa_error_logging.write_error(
                                        'ARI',
                                        'sa_total_common.call_dynamic_sql',
                                        'y',
                                        'n',
                                        'Error creating ' || v_spec(1),
                                        o_error_message
                                     ) ;       */
         return false ;
end ;
/*----------------------------------------------------------------------------------
* Create an evaluation condition that returns a string version of the logical
* expression to test the condition.
*---------------------------------------------------------------------------------*/

function GET_TRAN_DATETIME (I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                            O_business_date    IN OUT SA_STORE_DAY.BUSINESS_DATE%TYPE,
                            O_store            IN OUT SA_STORE_DAY.STORE%TYPE,
                            O_day              IN OUT SA_STORE_DAY.DAY%TYPE,
                            O_error_message    OUT    VARCHAR2) return boolean

is

cursor c_get_tran_date is
select business_date, store, day
  from sa_store_day
 where store_day_seq_no = I_store_day_seq_no;

begin

   open c_get_tran_date;
   fetch c_get_tran_date into O_business_date, O_store, O_day;
   if O_business_date IS NULL then
      O_error_message := sql_lib.create_msg(
            'SATOTRUL_NOSTRDAY',
            I_store_day_seq_no,
            NULL,
            NULL);
      close c_get_tran_date;
      return false;
   end if;

   close c_get_tran_date;
   return true;
		
exception

	when others then
	   O_error_message := sql_lib.create_msg(
	             'PACKAGE_ERROR',
	             SQLERRM,
	             'GET_TRAN_DATETIME',
	             SQLCODE);
		return false;
			
end;
------------------------------------------------------------
FUNCTION INTERNAL_SCHEMA_NAME
   return CHAR is

   cursor C_GET_OWNER is
      select table_owner
        from system_options;

BEGIN

   if V_internal_schema_name is NULL then
      open C_GET_OWNER;
      fetch C_GET_OWNER into V_internal_schema_name;
      close C_GET_OWNER;
   end if;       
   ---
   return V_internal_schema_name;
END INTERNAL_SCHEMA_NAME;
------------------------------------------------------------
END;
/
