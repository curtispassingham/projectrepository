
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_LOCKING_SQL AS
-----------------------------------------------------------------------
-- INTERNAL FUNCTION  CHECK_INPUTS
--  This internal package function will be called by the Get_lock
--  and Release_lock packages.It will ensure that the required input
--  variables are not null.
------------------------------------------------------------------------
FUNCTION CHECK_INPUTS(O_error_message      IN OUT VARCHAR2,
                      I_store_day_seq_no   IN     NUMBER,
                      I_user_id            IN     VARCHAR2,
                      I_process            IN     VARCHAR2)
      RETURN BOOLEAN IS

   L_program         VARCHAR2(60)  := 'SA_LOCKING_SQL.CHECK_INPUTS';

BEGIN
   if I_store_day_seq_no is NULL or I_user_id is NULL or I_process is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_INPUTS;
----------------------------------------------------------------------
 --FUNCTION ERROR_HANDLING
 --  This internal function is used to evaluate the success of the
 --  call to DBMS_LOCK.REQUEST.  If the function returns 0,it has
 --  suceeded.  Otherwise it has failed.  If the function returns 1,
 --  it has timed out.  If the function returns 2, it has encountered
 --  a deadlock.  If the function returns a 3, it has been passed
 --  invalid in/out parameters.  If the function returns a 4, the
 --  processdoes not own the lock handle it is requesting a lock on.
 --  If the function returns a 5, the lock handle is illegal.
----------------------------------------------------------------------
FUNCTION ERROR_HANDLING(O_error_message      IN OUT VARCHAR2,
                        I_call_status        IN     VARCHAR2,
                        I_store_day_seq_no   IN     NUMBER)
      RETURN BOOLEAN IS

   L_table         VARCHAR2(30) := 'SA_STORE_DAY_READ_LOCK';
   L_program       VARCHAR2(60) := 'SA_LOCKING_SQL.ERROR_HANDLING';

BEGIN
   ---
   if I_call_status != 0 then
      ---
      if I_call_status = 1 then
         O_error_message := SQL_LIB.CREATE_MSG('TIME_OUT',
                                               'Store_day_seq_no: '||to_char(I_store_day_seq_no)||', '||L_table,
                                               NULL,
                                               NULL);
      elsif I_call_status = 2 then
         O_error_message := SQL_LIB.CREATE_MSG('DEADLOCK',
                                               'Store_day_seq_no: '||to_char(I_store_day_seq_no)||', '||L_table,
                                               NULL,
                                               NULL);
      elsif I_call_status = 3 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                               'DBMS_LOCK.REQUEST',
                                               NULL,
                                               NULL);
      elsif I_call_status = 4 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_OWN_LOCK',
                                               'Store_day_seq_no: '||to_char(I_store_day_seq_no)||', '||L_table,
                                               NULL,
                                               NULL);
      elsif I_call_status = 5 then
         O_error_message := SQL_LIB.CREATE_MSG('ILL_LOCKHANDLE',
                                               NULL,
                                               NULL,
                                               NULL);
      end if;
      ---
      return FALSE;
   else
      return TRUE;
   end if;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ERROR_HANDLING;
-------------------------------------------------------------------------
FUNCTION GET_WRITE_LOCK(O_error_message    IN OUT VARCHAR2,
                        I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(40)              := 'SA_LOCKING_SQL.GET_WRITE_LOCK';
   L_read_exists    VARCHAR2(1)               := 'N';
   ---
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_READ_EXIST is
     select 'Y'
       from sa_store_day_read_lock
      where store_day_seq_no = I_store_day_seq_no;

   cursor C_LOCK_STORE_DAY is
      select 'x'
        from sa_store_day_write_lock
       where store_day_seq_no = I_store_day_seq_no
         for update nowait;

BEGIN
   ---
   -- Not able to get a write lock if there are any read locks for a particular store day.
   -- Check for existing read locks.
   ---
   SQL_LIB.SET_MARK('OPEN','C_READ_EXIST','SA_STORE_DAY, SA_STORE_DAY_READ_LOCK','store_day_seq_no: '||to_char(I_store_day_seq_no));
   open C_READ_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_READ_EXIST','SA_STORE_DAY, SA_STORE_DAY_READ_LOCK','store_day_seq_no: '||to_char(I_store_day_seq_no));
   fetch C_READ_EXIST into L_read_exists;
   SQL_LIB.SET_MARK('CLOSE','C_READ_EXIST','SA_STORE_DAY, SA_STORE_DAY_READ_LOCK','store_day_seq_no: '||to_char(I_store_day_seq_no));
   close C_READ_EXIST;
   ---
   if L_read_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('STR_DAY_LOCKED',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
    end if;
   ---
   -- No read locks, so attempt to get write lock
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_STORE_DAY','SA_STORE_DAY_WRITE_LOCK','store_day_seq_no: '||to_char(I_store_day_seq_no));
   open C_LOCK_STORE_DAY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_STORE_DAY','SA_STORE_DAY_WRITE_LOCK','store_day_seq_no: '||to_char(I_store_day_seq_no));
   close C_LOCK_STORE_DAY;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('STR_DAY_LOCKED',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_WRITE_LOCK;
-------------------------------------------------------------------------------
FUNCTION GET_WRITE_LOCK_EXIST(O_error_message    IN OUT VARCHAR2,
                              I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(40)              := 'SA_LOCKING_SQL.GET_WRITE_LOCK_EXIST';
   ---
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STORE_DAY is
      select 'x'
        from sa_store_day_write_lock
       where store_day_seq_no = I_store_day_seq_no
         for update nowait;

BEGIN
   ---
   --- This function is called from the GET_READ_LOCk function and will attempt to
   --- place a write lock on the store_day_seq_no.  If another program has a write
   --- lock on this same record, this function will raise the RECORD_LOCKED exception
   --- and, in turn, no read lock will be placed on the record
   SQL_LIB.SET_MARK('OPEN','C_LOCK_STORE_DAY','SA_STORE_DAY_WRITE_LOCK_EXIST','store_day_seq_no: '||to_char(I_store_day_seq_no));
   open C_LOCK_STORE_DAY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_STORE_DAY','SA_STORE_DAY_WRITE_LOCK_EXIST','store_day_seq_no: '||to_char(I_store_day_seq_no));
   close C_LOCK_STORE_DAY;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('STR_DAY_LOCKED',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_WRITE_LOCK_EXIST;
-------------------------------------------------------------------------------
FUNCTION GET_READ_LOCK(O_error_message      IN OUT VARCHAR2,
                       O_lock_success       IN OUT BOOLEAN,
                       I_store_day_seq_no   IN     NUMBER,
                       I_user_id            IN     VARCHAR2,
                       I_process            IN     VARCHAR2)
      RETURN BOOLEAN IS

   L_program         VARCHAR2(60)  := 'SA_LOCKING_SQL.GET_READ_LOCK';
   L_lockname        VARCHAR2(128) := to_char(I_store_day_seq_no);
   L_store           SA_STORE_DAY.STORE%TYPE;
   L_business_date   SA_STORE_DAY.BUSINESS_DATE%TYPE;
   L_lockhandle      VARCHAR2(128);
   L_call_status     NUMBER;

BEGIN
   if CHECK_INPUTS(O_error_message,
                   I_store_day_seq_no,
                   I_user_id,
                   I_process) = FALSE then
      return FALSE;
   end if;
   ---
   -- Many read locks can be added for the same store_day_seq_no
   -- but no read lock can be added if a write lock already exists (and no write lock
   -- can be added if a read lock already exists)  the LOCK_STORE_DAY is checking
   -- for write locks, if it doesn't find one it will lock the Store Day.
   ---
   if GET_WRITE_LOCK_EXIST(O_error_message,
                           I_store_day_seq_no) = FALSE then
      O_lock_success  := FALSE;
      return FALSE;
   end if;
   ---
   -- Since the program was able to get a write lock.  Now get a read lock.
   ---
   DBMS_LOCK.ALLOCATE_UNIQUE(L_lockname,
                             L_lockhandle);
   ---
   -- the third input variable in DBMS_LOCK.REQUEST is the time until the
   -- process times out (in seconds) this can be customized to any
   -- value up to 864000 to fit within system requirements.
   ---
   L_call_status := DBMS_LOCK.REQUEST(L_lockhandle,
                                      DBMS_LOCK.x_mode,
                                      5,
                                      FALSE);
   ---
   -- DBMS_LOCK will return numbers 0-5 based on its success
   -- (0 being completed successfully, 1-5 being different errors,
   -- the ERROR_HANDLING package will associate the appropriate error
   -- message with the returned call status.
   ---
   if ERROR_HANDLING (O_error_message,
                      L_call_status,
                      I_store_day_seq_no) = FALSE then
      return FALSE;
   end if;
   ---
   ---
   insert into sa_store_day_read_lock(store_day_seq_no,
                                      locked_by_user,
                                      locked_by_process)
                               values(I_store_day_seq_no,
                                      I_user_id,
                                      I_process);
   Commit;
   ---
   L_call_status := DBMS_LOCK.RELEASE(L_lockhandle);
   ---
   if ERROR_HANDLING (O_error_message,
                      L_call_status,
                      I_store_day_seq_no) = FALSE then
      return FALSE;
   end if;
   ---
   O_lock_success := TRUE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      L_call_status   := DBMS_LOCK.RELEASE(L_lockhandle);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_READ_LOCK;
-----------------------------------------------------------------
FUNCTION RELEASE_LOCK(O_error_message      IN OUT VARCHAR2,
                      I_store_day_seq_no   IN     NUMBER,
                      I_user_id            IN     VARCHAR2,
                      I_process            IN     VARCHAR2)
      RETURN BOOLEAN IS

   L_program       VARCHAR2(60)  := 'SA_LOCKING_SQL.RELEASE_LOCK';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STORE_DAY is
      select 'x'
        from sa_store_day_read_lock
       where store_day_seq_no  = I_store_day_seq_no
         and locked_by_user    = I_user_id
         and locked_by_process = I_process
         for update nowait;

BEGIN
   if CHECK_INPUTS(O_error_message,
                   I_store_day_seq_no,
                   I_user_id,
                   I_process) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_STORE_DAY','SA_STORE_DAY_READ_LOCK',NULL);
   open C_LOCK_STORE_DAY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_STORE_DAY','SA_STORE_DAY_READ_LOCK',NULL);
   close C_LOCK_STORE_DAY;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'SA_STORE_DAY_READ_LOCK',NULL);
   delete from sa_store_day_read_lock
    where store_day_seq_no  = I_store_day_seq_no
      and locked_by_user    = I_user_id
      and locked_by_process = I_process;
   ---
   Commit;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'SA_STORE_DAY_READ_LOCK',
                                            to_char(I_store_day_seq_no),
                                            I_user_id);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RELEASE_LOCK;
-----------------------------------------------------------------------------------------
END SA_LOCKING_SQL;
/
