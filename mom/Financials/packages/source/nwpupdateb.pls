CREATE OR REPLACE PACKAGE BODY NWP_UPDATE_SQL AS

---
--- local function prototypes
---

--------------------------------------------------------------------------
-- Function: BEFORE_FREEZE_DATE
-- Purpose: Check that vdate is before the freeze date for the
--          year the shipment occured.
--------------------------------------------------------------------------

FUNCTION BEFORE_FREEZE_DATE ( O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE
                             ,O_before        OUT VARCHAR2
                             ,I_fiscal_year   IN  NWP.FISCAL_YEAR%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------
-- Function: CALCULATE_NEW_WAC
-- Purpose: Calculate Weighted Average Cost
--------------------------------------------------------------------------
FUNCTION CALCULATE_NEW_WAC (O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,O_new_WAC            OUT NWP.WAC%TYPE
                           ,I_item               IN  ITEM_MASTER.ITEM%TYPE
                           ,I_loc                IN  ORDLOC.LOCATION%TYPE
                           ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE
                           ,I_cost_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_unit_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------
-- Function: ADJUST_UNIT_AMT
-- Purpose: Update the adjustment_date, qty_received,unit_adj_qty and wac
--------------------------------------------------------------------------
FUNCTION ADJUST_UNIT_AMT (O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                         ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                         ,I_loc                IN  ORDLOC.LOCATION%TYPE
                         ,I_new_unit_amt       IN  ORDLOC.UNIT_COST%TYPE
                         ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE
                         ,I_WAC                IN  NWP.WAC%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------
-- Function: ADJUST_COST
-- Purpose: Update the adjustment_date, rec_unit_cost,cost_adj_amt,wac
--------------------------------------------------------------------------
FUNCTION ADJUST_COST ( O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                      ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                      ,I_loc                IN  ORDLOC.LOCATION%TYPE
                      ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                      ,I_new_cost_amt       IN  ORDLOC.UNIT_COST%TYPE
                      ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE
                      ,I_WAC                IN  NWP.WAC%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------
-- Function: RECEIVING_PROCESS_INSERT
-- Purpose: Create records in NWP
--------------------------------------------------------------------------
FUNCTION RECEIVING_PROCESS_INSERT (O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                                  ,I_loc                IN  ORDLOC.LOCATION%TYPE
                                  ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                                  ,I_receipt_shipment   IN  SHIPMENT.SHIPMENT%TYPE
                                  ,I_receipt_cost       IN  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
                                  ,I_receipt_quantity   IN  ORDLOC.QTY_RECEIVED%TYPE
                                  ,I_exchange_rate      IN  CURRENCY_RATES.EXCHANGE_RATE%TYPE
                                  ,I_PO_currency        IN  CURRENCIES.CURRENCY_CODE%TYPE
                                  ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------
-- Function: RECEIVING_PROCESS_UPDATE
-- Purpose:  Update records in NWP
--------------------------------------------------------------------------
FUNCTION RECEIVING_PROCESS_UPDATE (O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                                  ,I_loc                IN  ORDLOC.LOCATION%TYPE
                                  ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                                  ,I_receipt_shipment   IN  SHIPMENT.SHIPMENT%TYPE
                                  ,I_receipt_date       IN  NWP.RECEIVE_DATE%TYPE
                                  ,I_receipt_quantity   IN  ORDLOC.QTY_RECEIVED%TYPE
                                  ,I_receipt_cost       IN  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
                                  ,I_PO_currency        IN  CURRENCIES.CURRENCY_CODE%TYPE
                                  ,I_exchange_rate      IN  CURRENCY_RATES.EXCHANGE_RATE%TYPE
                                  ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------
-- Function: PROCESS_NWP_RECORD
-- Purpose:
--       1) For non adjustments - get currency and exchange rates, unit cost in local currency.
--       2) ascertains if an existing NWP record exists, If not then a record is created.
--       3) else, the I_cost_adjust_amt is checked for a value and AJUST_COST is calle
--       4) else, the I_unit_adjust_amt is checked for a value and ADJUST_UNIT_AMT is called
--       5) else the NWP record is updated (order_no,shipment,receive_date,qty_received
--          ,rec_unit_cost,currency_code,exchange_rate)
--------------------------------------------------------------------------
FUNCTION PROCESS_NWP_RECORD(O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                           ,I_order_item         IN  ITEM_MASTER.ITEM%TYPE
                           ,I_loc                IN  ORDLOC.LOCATION%TYPE
                           ,I_loc_type           IN  ITEM_LOC.LOC_TYPE%TYPE
                           ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                           ,I_receipt_shipment   IN  SHIPMENT.SHIPMENT%TYPE
                           ,I_receipt_date       IN  NWP.RECEIVE_DATE%TYPE
                           ,I_receipt_quantity   IN  ORDLOC.QTY_RECEIVED%TYPE
                           ,I_receipt_cost       IN  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
                           ,I_cost_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_unit_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_ord_currency       IN  ORDHEAD.CURRENCY_CODE%TYPE
                           ,I_loc_currency       IN  ORDHEAD.CURRENCY_CODE%TYPE
                           ,I_ord_exchange_rate  IN  ORDHEAD.EXCHANGE_RATE%TYPE
                           ,I_order_type         IN  VARCHAR2
                           ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE)
return BOOLEAN;

---
--- global vars
---

LP_vdate  DATE := get_vdate;

---
--- local function defintions
---

--------------------------------------------------------------------------

FUNCTION BEFORE_FREEZE_DATE ( O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE
                             ,O_before        OUT VARCHAR2
                             ,I_fiscal_year   IN  NWP.FISCAL_YEAR%TYPE)
return BOOLEAN IS

   L_function VARCHAR2(64) := g_package||'.BEFORE_FREEZE_DATE';
   -----------------------------------------
   -- Declare Cursors
   -----------------------------------------
   cursor C_CHK_FREEZE_DATE (c_fiscal_year IN nwp_freeze_date.fiscal_year%TYPE) is
    select 'Y'
      from nwp_freeze_date nfd
     where nfd.fiscal_year = c_fiscal_year
       and LP_vdate <= nfd.freeze_date;

BEGIN
  ---------------------------------------------------------------
  -- Check vdate less than freeze date for fiscal year item was
  -- shipped.
  ---------------------------------------------------------------
  O_before := 'N';

  open C_CHK_FREEZE_DATE ( I_fiscal_year );
  fetch C_CHK_FREEZE_DATE into O_before;
  close C_CHK_FREEZE_DATE;

  return TRUE;

EXCEPTION

 WHEN OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_function,
                                               to_char(SQLCODE));
END BEFORE_FREEZE_DATE;
--------------------------------------------------------------------------

FUNCTION CALCULATE_NEW_WAC (O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,O_new_WAC            OUT NWP.WAC%TYPE
                           ,I_item               IN  ITEM_MASTER.ITEM%TYPE
                           ,I_loc                IN  ORDLOC.LOCATION%TYPE
                           ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE
                           ,I_cost_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_unit_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE)
return BOOLEAN is

   -----------------------------------------
   -- Declare Vars
   -----------------------------------------
   L_function VARCHAR2(64) := g_package||'.CALCULATE_NEW_WAC';

   L_snap_on_hand         nwp.on_hand_qty%TYPE;
   L_snap_in_trans        nwp.in_transit_qty%TYPE;
   L_old_WAC              nwp.wac%TYPE;
   L_receipt_qty          shipsku.qty_received%TYPE;
   L_receipt_cost         shipsku.unit_cost%TYPE;
   L_old_unit_adjust_amt  ordloc.unit_cost%TYPE;


   -----------------------------------------
   -- Declare Cursors
   -----------------------------------------
   cursor C_CURRENT_VALUES ( c_item_number IN  item_master.item%TYPE
                           , c_location    IN  ordloc.location%TYPE
                           , c_fiscal_year IN  nwp.fiscal_year%TYPE)  is
      select NVL(n.on_hand_qty,0),
             NVL(n.in_transit_qty,0),
             NVL(n.WAC,0),
             NVL(n.qty_received,0),
             NVL(n.rec_unit_cost,0),
             NVL(n.unit_adj_qty,0)
        from NWP n
       where n.item        = c_item_number
         and n.location    = c_location
         and n.fiscal_year = c_fiscal_year;

BEGIN
   open C_CURRENT_VALUES ( I_item, I_loc, I_fiscal_year);
   fetch C_CURRENT_VALUES into L_snap_on_hand,
                               L_snap_in_trans,
                               L_old_WAC,
                               L_receipt_qty,
                               L_receipt_cost,
                               L_old_unit_adjust_amt;
   if C_CURRENT_VALUES%NOTFOUND then
      close C_CURRENT_VALUES;
      O_error_message := SQL_LIB.CREATE_MSG('NO_NWP_RECORD', NULL, NULL, NULL);
      return FALSE;
   else
      O_new_WAC := ((L_snap_on_hand+L_snap_in_trans+L_old_unit_adjust_amt)*L_old_WAC -
                    (L_receipt_qty*L_receipt_cost) +
                    (L_receipt_qty+I_unit_adjust_amt)*I_cost_adjust_amt)/
                    (L_snap_on_hand + L_snap_in_trans + (L_old_unit_adjust_amt + I_unit_adjust_amt));
   end if;
   close C_CURRENT_VALUES;
   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;

END CALCULATE_NEW_WAC;

--------------------------------------------------------------------------
FUNCTION ADJUST_UNIT_AMT (O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                         ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                         ,I_loc                IN  ORDLOC.LOCATION%TYPE
                         ,I_new_unit_amt       IN  ORDLOC.UNIT_COST%TYPE
                         ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE
                         ,I_WAC                IN  NWP.WAC%TYPE)
return BOOLEAN is

   -----------------------------------------
   -- Declare Vars
   -----------------------------------------
   L_new_wac   nwp.wac%TYPE := null;
   L_function  VARCHAR2(64) := g_package||'.ADJUST_UNIT_AMT';
   L_nwp_rowid rowid;
   L_table     VARCHAR2(30) := 'NWP';
   -----------------------------------------
   -- Declare Cursors
   -----------------------------------------
   CURSOR C_LOCK_NWP (c_item_number IN item_master.item%TYPE
                    , c_location    IN ordloc.location%TYPE
                    , c_fiscal_year IN nwp.fiscal_year%TYPE ) is
      select n.rowid
        from nwp n
       where n.item        = c_item_number
         and n.location    = c_location
         and n.fiscal_year = c_fiscal_year
         for update nowait;

   ----------------------------------------
   -- Declare Exceptions
   ----------------------------------------
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

BEGIN

   if I_WAC is not NULL then
      if NWP_UPDATE_SQL.CALCULATE_NEW_WAC(O_error_message
                                         ,L_new_wac
                                         ,I_comp_item
                                         ,I_loc
                                         ,I_fiscal_year
                                         ,0
                                         ,I_new_unit_amt) = FALSE then
         return FALSE;
      end if;
   end if;

   open C_LOCK_NWP ( I_comp_item
                    ,I_loc
                    ,I_fiscal_year );

   fetch C_LOCK_NWP into L_nwp_rowid;
   if C_LOCK_NWP%NOTFOUND THEN
      close C_LOCK_NWP;
      return TRUE;              -- do nothing

   else
      close C_LOCK_NWP;
      ----------------------------------------------------------------------
      -- Update Nwp record, increment unit_adj_qty if value already present
      ----------------------------------------------------------------------
      update NWP n
      set n.adjustment_date = LP_vdate
           , n.qty_received = ROUND( (nvl(n.qty_received,0) + I_new_unit_amt) ,4)
           , n.unit_adj_qty = ROUND(I_new_unit_amt ,4)
           , n.WAC = ROUND(nvl(L_new_WAC, n.wac),4)
       where n.rowid = L_nwp_rowid;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(I_comp_item),
                                             TO_CHAR(I_loc));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;

END ADJUST_UNIT_AMT;

--------------------------------------------------------------------------
FUNCTION ADJUST_COST ( O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                      ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                      ,I_loc                IN  ORDLOC.LOCATION%TYPE
                      ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                      ,I_new_cost_amt       IN  ORDLOC.UNIT_COST%TYPE
                      ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE
                      ,I_WAC                IN  NWP.WAC%TYPE)
return BOOLEAN is

   -----------------------------------------
   -- Declare Vars
   -----------------------------------------
   L_new_wac   nwp.wac%TYPE := null;
   L_function  VARCHAR2(64) := g_package||'.ADJUST_COST';
   L_nwp_rowid rowid;

   L_table     VARCHAR2(30) :=  'NWP';
   -----------------------------------------
   -- Declare Cursors
   -----------------------------------------
   CURSOR C_LOCK_NWP (c_item_number IN item_master.item%TYPE
                    , c_location    IN ordloc.location%TYPE
                    , c_receipt_po  IN nwp.order_no%TYPE
                    , c_fiscal_year IN nwp.fiscal_year%TYPE ) is
      select n.rowid
        from nwp n
       where n.item        = c_item_number
         and n.location    = c_location
         and n.order_no    = c_receipt_PO
         and n.fiscal_year = c_fiscal_year
         for update nowait;

   ----------------------------------------
   -- Declare Exceptions
   ----------------------------------------
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

BEGIN

   if I_WAC is not NULL then
      if NWP_UPDATE_SQL.CALCULATE_NEW_WAC (O_error_message,
                                           L_new_wac,
                                           I_comp_item,
                                           I_loc,
                                           I_fiscal_year,
                                           I_new_cost_amt,
                                           0) = FALSE then
         return FALSE;
      end if;
   end if;

   open C_LOCK_NWP ( I_comp_item
                    ,I_loc
                    ,I_receipt_po
                    ,I_fiscal_year );
   fetch C_LOCK_NWP into L_nwp_rowid;
   if C_LOCK_NWP%NOTFOUND THEN
      close C_LOCK_NWP;
      return TRUE ;
   else
      close C_LOCK_NWP;

      ----------------------------------------------------------------------
      -- Update Nwp record, increment cost_adj_amt if value already present
      ----------------------------------------------------------------------
      update NWP n
         set n.adjustment_date = LP_vdate
           , n.rec_unit_cost   = ROUND(I_new_cost_amt,4)
           , n.cost_adj_amt    = ROUND(I_new_cost_amt - n.rec_unit_cost + NVL(n.cost_adj_amt,0) ,4)
           , n.WAC             = ROUND(nvl(L_new_WAC, n.WAC),4)
       where n.rowid = L_nwp_rowid;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_comp_item,
                                            I_loc);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;

END ADJUST_COST;

--------------------------------------------------------------------------
FUNCTION RECEIVING_PROCESS_INSERT (O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                                  ,I_loc                IN  ORDLOC.LOCATION%TYPE
                                  ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                                  ,I_receipt_shipment   IN  SHIPMENT.SHIPMENT%TYPE
                                  ,I_receipt_cost       IN  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
                                  ,I_receipt_quantity   IN  ORDLOC.QTY_RECEIVED%TYPE
                                  ,I_exchange_rate      IN  CURRENCY_RATES.EXCHANGE_RATE%TYPE
                                  ,I_PO_currency        IN  CURRENCIES.CURRENCY_CODE%TYPE
                                  ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE)
return BOOLEAN IS

   -----------------------------------------
   -- Declare Vars
   -----------------------------------------
   L_function VARCHAR2(64) := g_package||'.RECEIVING_PROCESS_INSERT';
   L_receipt_date DATE ;

   -----------------------------------------
   -- Declare Cursors
   -----------------------------------------
   -- Get the received date from the shipment table.
   cursor C_SHIP ( c_receipt_shipment IN shipment.shipment%TYPE ) is
      select s.receive_date
        from shipment s
       where s.shipment = c_receipt_shipment;

BEGIN

   -------------------------------------------------------
   -- Check input parameter for shipment.
   -- Note: Item and location checked in calling function.
   -------------------------------------------------------
   if I_receipt_shipment is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_receipt_shipment',
                                            'NULL','NOT NULL');
     return FALSE;
   end if;

   open C_SHIP (I_receipt_shipment);
   fetch C_SHIP into L_receipt_date;
   if C_SHIP%NOTFOUND then
     close C_SHIP;
      O_error_message := SQL_LIB.CREATE_MSG('NO_SHIP_RECORD', NULL, NULL, NULL);
     return FALSE;
   end if;
   close C_SHIP;

   insert into NWP(item,
                   location,
                   order_no,
                   shipment,
                   receive_date,
                   qty_received,
                   rec_unit_cost,
                   exchange_rate,
                   currency_code,
                   fiscal_year)
           values (I_comp_item,
                   I_loc,
                   I_receipt_PO,
                   I_receipt_shipment,
                   L_receipt_date,
                   I_receipt_quantity,
                   I_receipt_cost,
                   I_exchange_rate,
                   I_PO_currency,
                   I_fiscal_year);

   return TRUE;

EXCEPTION
   WHEN OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_function,
                                           to_char(SQLCODE));
     return FALSE;

END RECEIVING_PROCESS_INSERT;

--------------------------------------------------------------------------
FUNCTION RECEIVING_PROCESS_UPDATE (O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                                  ,I_loc                IN  ORDLOC.LOCATION%TYPE
                                  ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                                  ,I_receipt_shipment   IN  SHIPMENT.SHIPMENT%TYPE
                                  ,I_receipt_date       IN  NWP.RECEIVE_DATE%TYPE
                                  ,I_receipt_quantity   IN  ORDLOC.QTY_RECEIVED%TYPE
                                  ,I_receipt_cost       IN  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
                                  ,I_PO_currency        IN  CURRENCIES.CURRENCY_CODE%TYPE
                                  ,I_exchange_rate      IN  CURRENCY_RATES.EXCHANGE_RATE%TYPE
                                  ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE)
return BOOLEAN IS

   -----------------------------------------
   -- Declare Vars
   -----------------------------------------
   L_function  VARCHAR2(64) := g_package||'.RECEIVING_PROCESS_UPDATE';
   L_nwp_rowid rowid;
   L_table     VARCHAR2(30) :=  'NWP';
   -----------------------------------------
   -- Declare Cursors
   -----------------------------------------
   cursor C_LOCK_NWP (c_item        IN item_master.item%TYPE
                    , c_location    IN ordloc.location%TYPE
                    , c_fiscal_year IN nwp.fiscal_year%TYPE) is
      select n.rowid
        from nwp n
       where n.item = c_item
         and n.location = c_location
         and n.fiscal_year = c_fiscal_year
         for update nowait;

   ----------------------------------------
   -- Declare Exceptions
   ----------------------------------------
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

BEGIN

   ------------------------------------------------------------
   -- Check order no has been passed.
   -- Note: Item and location checked in main calling function.
   ------------------------------------------------------------
   if I_receipt_po is null then
     O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_receipt_po',
                                            'NULL','NOT NULL');
     return FALSE;
   end if;

   open C_LOCK_NWP ( I_comp_item
                    ,I_loc
                    ,I_fiscal_year);
   fetch C_LOCK_NWP into L_nwp_rowid;
   if C_LOCK_NWP%NOTFOUND THEN
      close C_LOCK_NWP;
      return TRUE;
   else
      close C_LOCK_NWP;

      update NWP
         set order_no        = I_receipt_PO
            ,shipment        = I_receipt_shipment
            ,receive_date    = I_receipt_date
            ,qty_received    = I_receipt_quantity
            ,rec_unit_cost   = I_receipt_cost
            ,currency_code   = I_PO_currency
            ,exchange_rate   = I_exchange_rate
            ,unit_adj_qty    = null
            ,cost_adj_amt    = null
            ,adjustment_date = null
            ,on_hand_qty     = null
            ,in_transit_qty  = null
            ,wac             = null
            ,variance_qty    = null
         where rowid = L_nwp_rowid;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_comp_item,
                                            I_loc);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;

END RECEIVING_PROCESS_UPDATE;

---------------------------------------------------------------------------------------------
-- Function: PROCESS_NWP_RECORD
-- Purpose:
--       1) For non adjustments - get currency and exchange rates, unit cost in local currency.
--       2) ascertains if an existing NWP record exists, If not then a record is created.
--       3) else, the I_cost_adjust_amt is checked for a value and AJUST_COST is calle
--       4) else, the I_unit_adjust_amt is checked for a value and ADJUST_UNIT_AMT is called
--       5) else the NWP record is updated (order_no,shipment,receive_date,qty_received
--          ,rec_unit_cost,currency_code,exchange_rate)
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_NWP_RECORD(O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                           ,I_order_item         IN  ITEM_MASTER.ITEM%TYPE
                           ,I_loc                IN  ORDLOC.LOCATION%TYPE
                           ,I_loc_type           IN  ITEM_LOC.LOC_TYPE%TYPE
                           ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                           ,I_receipt_shipment   IN  SHIPMENT.SHIPMENT%TYPE
                           ,I_receipt_date       IN  NWP.RECEIVE_DATE%TYPE
                           ,I_receipt_quantity   IN  ORDLOC.QTY_RECEIVED%TYPE
                           ,I_receipt_cost       IN  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
                           ,I_cost_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_unit_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_ord_currency       IN  ORDHEAD.CURRENCY_CODE%TYPE         -- only exist for PO (not ajustments or stock order)
                           ,I_loc_currency       IN  ORDHEAD.CURRENCY_CODE%TYPE         -- only exist for PO (not ajustments or stock order)
                           ,I_ord_exchange_rate  IN  ORDHEAD.EXCHANGE_RATE%TYPE         -- only exist for PO (not ajustments or stock order)
                           ,I_order_type         IN  VARCHAR2
                           ,I_fiscal_year        IN  NWP.FISCAL_YEAR%TYPE)
return BOOLEAN IS

   ---------------------------
   -- Local Var Declarations
   ---------------------------
   L_function          VARCHAR2(64) :=  g_package||'.PROCESS_NWP_RECORD';
   L_PO_number         nwp.order_no%TYPE;
   L_WAC               nwp.wac%TYPE ;
   L_exchange_rate     nwp.exchange_rate%TYPE;
   L_currency_code     nwp.currency_code%TYPE  := I_loc_currency;
   L_sending_currency  nwp.currency_code%TYPE  := null;
   L_unit_cost_loc     ordloc.unit_cost%TYPE   := null;
   L_upd_rec_cost      ordloc.unit_cost%TYPE   := null;
   L_loc_exchange_rate currency_rates.EXCHANGE_RATE%TYPE := null;
   L_ord_exchange_rate currency_rates.EXCHANGE_RATE%TYPE := null;
   L_so_from_loc       ordloc.location%TYPE;
   L_so_from_loc_type  item_loc.loc_type%TYPE;
   L_fiscal_year       nwp.fiscal_year%TYPE  := I_fiscal_year;

   ---------------------------
   -- Cursor Declarations
   ---------------------------
   -- Get Order_no and WAC from NWP
   cursor C_NWP_DETAIL ( c_item        IN item_master.item%TYPE
                        ,c_location    IN ordloc.location%TYPE
                        ,c_fiscal_year IN nwp.fiscal_year%TYPE ) is
      select n.order_no
           , n.wac
        from nwp n
       where n.item        = c_item
         and n.location    = c_location
         and n.fiscal_year = c_fiscal_year ;

   -----------------------------------------------
   -- Get the from loc and type for stock orders
   -- so currency can be obtained.
   -----------------------------------------------
   cursor C_GET_FROM_LOC ( c_shipment IN shipment.shipment%TYPE ) is
      select sh.from_loc , sh.from_loc_type
        from shipment sh
       where sh.shipment = c_shipment ;

   -- Get currency code if sending loc is a Store
   cursor C_STORE_CURR ( c_from_loc store.store%TYPE ) is
      select st.currency_code
        from store st
       where st.store = c_from_loc;

   -- Get currency code if sending loc is a WH
   cursor C_WH_CURR ( c_from_loc wh.wh%TYPE ) is
      select wh.currency_code
        from wh
       where wh.wh = c_from_loc;

   -- Get currency code if sending loc is an external finisher
   cursor C_EF_CURR ( c_from_loc wh.wh%TYPE ) is
      select p.currency_code
        from partner p
       where p.partner_id = to_char(c_from_loc)
         and p.partner_type = 'E';

BEGIN

   ----------------------------------------------------------------------
   -- If not an adjustment then get the currency code and exchange rate
   -- according to the type of order ( purchase or stock)
   ----------------------------------------------------------------------
   if NOT ( nvl(I_unit_adjust_amt,0) <> 0 OR nvl(I_cost_adjust_amt,0) <> 0 ) then
      -----------------------------------------------------------------
      -- Get currency code based on currency code for purchase order
      -- For PO receipts use order_no.
      -- For Stock Order receipts use from/to locations and type.
      -----------------------------------------------------------------
      if I_order_type = 'PO' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message
                                     ,I_loc                -- to loc
                                     ,I_loc_type           -- location type
                                     ,null                 -- zone id
                                     ,L_currency_code  ) = FALSE then
            return FALSE;
         end if;
         -----------------------------------------------------------
         -- Convert unit cost in order currency to local currency.
         -----------------------------------------------------------
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 I_receipt_cost,
                                 L_currency_code,             -- receiving loc's currrency
                                 I_ord_currency,              -- order loc's currency
                                 L_unit_cost_loc,             -- converted currency value
                                 'C',                           -- cost retail ind
                                 NULL,                  -- effective date
                                 NULL,                  -- exchange type
                                 NULL,                  -- in exchange rate
                                 I_ord_exchange_rate            -- out exchange rate
                                 ) = FALSE then
            return FALSE;
         end if;    -- convert
      else       -- stock order
         --------------------------------------------
         -- Get the sending from location and type
         -- for stock orders.
         --------------------------------------------
         OPEN C_GET_FROM_LOC(I_receipt_shipment);
         FETCH C_GET_FROM_LOC into L_so_from_loc, L_so_from_loc_type;
         if C_GET_FROM_LOC%NOTFOUND then
            -- Error
            O_error_message := SQL_LIB.CREATE_MSG('NO_SHIP_RECORD', NULL, NULL, NULL);
            CLOSE C_GET_FROM_LOC;
            return FALSE;
         end if;
         CLOSE C_GET_FROM_LOC;

         ----------------------------------------------
         -- Get the currency code from sending location
         -- according to type of location
         ----------------------------------------------
         if L_so_from_loc_type = 'S' then
            open C_STORE_CURR ( L_so_from_loc );
            fetch C_STORE_CURR into L_sending_currency;
            close C_STORE_CURR;
         elsif L_so_from_loc_type = 'W' then
            open C_WH_CURR ( L_so_from_loc );
            fetch C_WH_CURR into L_sending_currency;
            close C_WH_CURR;
         elsif L_so_from_loc_type = 'E' then
            open C_EF_CURR ( L_so_from_loc );
            fetch C_EF_CURR into L_sending_currency;
            close C_EF_CURR;
         end if;

         --------------------------------------------
         -- Get the currency code for receiving loc
         -- This will be stored on the NWP table and
         -- also used to get the exchange rate.
         --------------------------------------------
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message
                                     ,I_loc                -- location
                                     ,I_loc_type           -- location type
                                     ,null                 -- zone id
                                     ,L_currency_code  ) = FALSE then
            return FALSE;
         end if;
         --------------------------------------------
         -- Convert currency for stock order transfer
         -- from sending to receiving.
         --------------------------------------------
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_so_from_loc,      -- from loc
                                             L_so_from_loc_type,      -- from loc type
                                             NULL,
                                             I_loc,
                                             I_loc_type,
                                             NULL,
                                             I_receipt_cost,
                                             L_unit_cost_loc,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      end if;    -- get currency codes according to PO/SO

      -----------------------------------
      -- General Code for both SO and PO
      -------------------------------------------------------
      -- Get the currency rate for the order location (PO)
      -- or sending location (SO)
      -------------------------------------------------------
      if CURRENCY_SQL.GET_RATE (O_error_message
                              , L_ord_exchange_rate
                              , nvl(I_ord_currency,L_sending_currency)
                              , 'C'
                              , null ) = FALSE then
         return FALSE;
      end if;
      ----------------------------------------------------------------
      -- Get get the currency rate for the receiving location (local)
      ----------------------------------------------------------------
      if CURRENCY_SQL.GET_RATE (O_error_message
                              , L_loc_exchange_rate
                              , L_currency_code            -- local currency for receiving location
                              , 'C'
                              , null ) = FALSE then
         return FALSE;
      end if;
   end if;  -- not an adjustment

   --------------------------------------------------------------------------
   -- Calculate exhange rate between receiving (local) and sending locations.
   --------------------------------------------------------------------------
   L_exchange_rate := ROUND( ( L_loc_exchange_rate / L_ord_exchange_rate ),4);

   --------------------------------------------------------------
   --  Set the L_receipt_cost if currency conversion occured
   --------------------------------------------------------------
   L_upd_rec_cost :=  nvl(L_unit_cost_loc,I_receipt_cost);

   open C_NWP_DETAIL ( I_comp_item, I_loc, L_fiscal_year );
   fetch C_NWP_DETAIL into L_PO_number, L_WAC;
   if C_NWP_DETAIL%NOTFOUND then
      close C_NWP_DETAIL;
      ----------------------------------------------------
      -- Do not create a record for adjustments
      ----------------------------------------------------
      if NOT ( nvl(I_unit_adjust_amt,0) <> 0 OR nvl(I_cost_adjust_amt,0) <> 0 ) then
         ---------------------------------------------------
         -- Insert for PO Receipt OR Stock Order Receipt
         ---------------------------------------------------
         if NWP_UPDATE_SQL.RECEIVING_PROCESS_INSERT( O_error_message
                                                    ,I_comp_item
                                                    ,I_loc
                                                    ,I_receipt_PO
                                                    ,I_receipt_shipment
                                                    ,L_upd_rec_cost
                                                    ,I_receipt_quantity
                                                    ,L_exchange_rate
                                                    ,L_currency_code
                                                    ,L_fiscal_year) = FALSE then
            return FALSE;
         end if;
      end if;        -- not for adjustments
   else       -- NWP record exists so update
      close C_NWP_DETAIL;
      ------------------------------------------------------------
      -- Nwp record Exists.
      -- Check cost_adjust_amt input parameter for a value
      ------------------------------------------------------------

      if nvl(I_cost_adjust_amt,0) <> 0  then
         if nvl(I_receipt_PO,-1) = L_PO_number then

            if NWP_UPDATE_SQL.ADJUST_COST ( O_error_message
                                           ,I_comp_item
                                           ,I_loc
                                           ,I_receipt_PO
                                           ,I_cost_adjust_amt
                                           ,L_fiscal_year
                                           ,L_WAC) = FALSE then
               return FALSE;
            end if;
         end if;
      ------------------------------------------------------------
      -- Check unit_adjust_amt input parameter for a value
      ------------------------------------------------------------
      elsif nvl(I_unit_adjust_amt,0) <> 0  then

         if NWP_UPDATE_SQL.ADJUST_UNIT_AMT( O_error_message
                                           ,I_comp_item
                                           ,I_loc
                                           ,I_unit_adjust_amt
                                           ,L_fiscal_year
                                           ,L_WAC) = FALSE then
           return FALSE;
        end if;
      ---------------------------------------------------
      -- Update for PO Receipt OR Stock Order Receipt
      ---------------------------------------------------
      else
         if NWP_UPDATE_SQL.RECEIVING_PROCESS_UPDATE( O_error_message
                                                    ,I_comp_item
                                                    ,I_loc
                                                    ,I_receipt_PO
                                                    ,I_receipt_shipment
                                                    ,I_receipt_date
                                                    ,I_receipt_quantity
                                                    ,L_upd_rec_cost
                                                    ,L_currency_code
                                                    ,L_exchange_rate
                                                    ,L_fiscal_year) = FALSE then
           return FALSE;
         end if;
      end if;   -- cost_adj_amt <> 0
   end if;   -- nwp record found

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_NWP_RECORD;
--------------------------------------------------------------------------------
-- Function : UPDATE_NWP_RECORD
-- Purpose  : Main controlling function.
--            Validate parameters.
--            Control calling of PROCESS_NWP_RECORD function according to whether
--            item is pack or not.
--------------------------------------------------------------------------------
FUNCTION UPDATE_NWP_RECORD(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                           ,I_order_item         IN  ITEM_MASTER.ITEM%TYPE
                           ,I_loc                IN  ORDLOC.LOCATION%TYPE
                           ,I_loc_type           IN  ITEM_LOC.LOC_TYPE%TYPE
                           ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                           ,I_receipt_shipment   IN  SHIPMENT.SHIPMENT%TYPE
                           ,I_receipt_date       IN  NWP.RECEIVE_DATE%TYPE
                           ,I_receipt_quantity   IN  ORDLOC.QTY_RECEIVED%TYPE
                           ,I_receipt_cost       IN  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
                           ,I_cost_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_unit_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_ord_currency       IN  ORDHEAD.CURRENCY_CODE%TYPE
                           ,I_loc_currency       IN  ORDHEAD.CURRENCY_CODE%TYPE
                           ,I_ord_exchange_rate  IN  ORDHEAD.EXCHANGE_RATE%TYPE
                           ,I_order_type         IN  VARCHAR2)
return BOOLEAN IS

   ---------------------------
   -- Local Var Declarations
   ---------------------------
   L_function          VARCHAR2(64) := g_package||'.UPDATE_NWP_RECORD';

   L_nwp_ind           system_options.nwp_ind%TYPE;
   L_valid             VARCHAR2(1) := 'N';
   L_ship_date         shipment.ship_date%TYPE;
   L_fiscal_year       nwp.fiscal_year%TYPE;

   ----------------------------------------------
   -- Variables that default to input parameters
   ----------------------------------------------
   L_shipment          shipment.shipment%TYPE := I_receipt_shipment;
   ---------------------------
   -- Cursor Declarations
   ---------------------------

   --------------------------------------------------------------------------
   -- Get shipment for either a Purchase order (PO) or Stock Order (SO)
   -- based on the ship date for the (PO) order or (SO) bol_no
   -- receipt_date is used as fiscal year
   --------------------------------------------------------------------------
   cursor C_SHIPDATE_FOR_ORDER (c_item       IN item_master.item%TYPE
                               ,c_order_no   IN nwp.order_no%TYPE
                               ,c_order_type IN VARCHAR2 ) is
      select sm.shipment
           , sm.ship_date
        from shipsku  ss,
             shipment sm
        where ss.item = c_item
          and ss.shipment = sm.shipment
          and decode(c_order_type,
                     'PO', to_char(sm.order_no),
                     sm.bol_no) = c_order_no ;

BEGIN
   -------------------------------------------------
   -- Check mandatory parameters
   -------------------------------------------------
   if I_comp_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_comp_item ', 'NULL','NOT NULL');
      return FALSE;
   end if;

   if I_order_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_order_item ', 'NULL','NOT NULL');
      return FALSE;
   end if;

   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_loc', 'NULL','NOT NULL');
      return FALSE;
   end if;

   if I_order_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_ord_type', 'NULL','NOT NULL');
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_NWP_IND(O_error_message,
                                     L_nwp_ind) = FALSE then
      return FALSE;
   end if;

   ---------------------------------------------------------------------
   -- If nwp processing is not required then return to calling function.
   ---------------------------------------------------------------------
   if L_nwp_ind = 'N' then
      return TRUE;
   end if;

   ---------------------------------------------------------------------
   -- Error if both cost and unit adjustments are populated.
   -- Only provision for one adjustment in a call.
   ---------------------------------------------------------------------
   if nvl(I_cost_adjust_amt,0) <> 0  and nvl(I_unit_adjust_amt,0) <> 0  then
      O_error_message := SQL_LIB.CREATE_MSG('ADJUSTING_COST_AND_UNIT', L_function, NULL, NULL);
      return FALSE;
   end if;

   ------------------------------------------------------------------------------------------
   -- Get first shipment number for an order item
   ------------------------------------------------------------------------------------------
   open C_SHIPDATE_FOR_ORDER ( I_order_item , I_receipt_PO, I_order_type );
   fetch C_SHIPDATE_FOR_ORDER into L_shipment, L_ship_date;
   if C_SHIPDATE_FOR_ORDER%NOTFOUND then
     close C_SHIPDATE_FOR_ORDER;
     -- Error
     O_error_message := SQL_LIB.CREATE_MSG('NO_SHIPMENT_FOR_ORDER'
                                           , L_function
                                           , NULL
                                           , NULL);
     return FALSE;
   end if;
   close C_SHIPDATE_FOR_ORDER;

   --------------------------------------------
   -- Set fiscal year to year of shipment date
   --------------------------------------------
   L_fiscal_year := to_number(to_char(L_ship_date,'YYYY'));

   ------------------------------------------------------------------------------------------
   -- If making an adjustment check the vdate is before the freeze date for the receipt year.
   ------------------------------------------------------------------------------------------
   if (nvl(I_cost_adjust_amt,0) <> 0 OR nvl(I_unit_adjust_amt,0) <> 0 ) then
     if BEFORE_FREEZE_DATE(O_error_message
                          ,L_valid
                          ,L_fiscal_year) = FALSE then
        return FALSE;
     end if;

     if L_valid = 'N' then
        O_error_message := SQL_LIB.CREATE_MSG('AFTER_NWP_FREEZE_DATE',
                                              L_fiscal_year,
                                              null,
                                              null);
        return FALSE;
     end if;
   end if;

   if I_receipt_cost <= 0 then
      return TRUE;
   end if;

   if NWP_UPDATE_SQL.PROCESS_NWP_RECORD(O_error_message
                                       ,I_comp_item
                                       ,I_order_item
                                       ,I_loc
                                       ,I_loc_type
                                       ,I_receipt_PO
                                       ,L_shipment
                                       ,I_receipt_date          -- use the receive_date passed in from calling module
                                       ,I_receipt_quantity
                                       ,I_receipt_cost
                                       ,I_cost_adjust_amt
                                       ,I_unit_adjust_amt
                                       ,I_ord_currency
                                       ,I_loc_currency
                                       ,I_ord_exchange_rate
                                       ,I_order_type
                                       ,L_fiscal_year
                                        ) = FALSE then
         return FALSE;
   end if;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_NWP_RECORD;

--------------------------------------------------------------------------------
FUNCTION FISCAL_YEAR_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_fiscal_yr       IN       NWP_FREEZE_DATE.FISCAL_YEAR%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'NWP_UPDATE_SQL.FISCAL_YEAR_EXISTS';
   L_exists_ind   VARCHAR2(1)  := 'N';

   cursor C_FISCAL_YEAR_EXISTS is
      select 'Y'
        from nwp_freeze_date
       where fiscal_year = I_fiscal_yr;

BEGIN

   if I_fiscal_yr is NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_yr',
                                            NULL,
                                            L_program);
      return FALSE;
   end if;


   SQL_LIB.SET_MARK('OPEN',
                    'C_FISCAL_YEAR_EXISTS',
                    'NWP_FREEZE_DATE',
                    'FISCAL_YEAR: '||to_char(I_fiscal_yr));
   open C_FISCAL_YEAR_EXISTS;

   fetch C_fiscal_year_exists into L_exists_ind;
   SQL_LIB.SET_MARK('FETCH',
                    'C_FISCAL_YEAR_EXISTS',
                    'NWP_FREEZE_DATE',
                    'FISCAL_YEAR: '||to_char(I_fiscal_yr));

   close C_FISCAL_YEAR_EXISTS;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_FISCAL_YEAR_EXISTS',
                    'NWP_FREEZE_DATE',
                    'FISCAL_YEAR: '||to_char(I_fiscal_yr));

   if L_exists_ind = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END FISCAL_YEAR_EXISTS;

--------------------------------------------------------------------------------
END NWP_UPDATE_SQL;
/
