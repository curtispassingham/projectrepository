CREATE OR REPLACE PACKAGE SA_BUILD_TOTAL_SQL AS
------------------------------------------------------------------------------------------------
--- Function: CREATE_REVISION
--- Purpose : Creates a revision for a dynamic total function.
---
FUNCTION CREATE_REVISION (O_error_message    IN OUT VARCHAR2,
                          O_new_total_rev_no IN OUT SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                          O_total_status     IN OUT SA_TOTAL_HEAD.STATUS%TYPE,
                          I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                          I_total_rev_no     IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- Function: BUILD_TOTAL_FUNCTION
--- Purpose : Creates a dynamic total function.
---
    FUNCTION BUILD_TOTAL_FUNCTION
        (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
         I_total_id           IN       SA_TOTAL_HEAD.TOTAL_ID%TYPE,
         I_total_rev_no       IN       SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
         I_pos_ind            IN       SA_TOTAL_HEAD.POS_IND%TYPE,
         I_refresh_override   IN       BOOLEAN)
    RETURN INT;
------------------------------------------------------------------------------------------------
--- Function: DROP_TOTAL_FUNCTION
--- Purpose : Drops a dynamic total function.
---
    FUNCTION DROP_TOTAL_FUNCTION
        (O_error_message   IN OUT VARCHAR2,
         I_total_id        IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
         I_total_rev_no    IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
    RETURN INT;
------------------------------------------------------------------------------------------------
--- Function: INSERT_TOTAL
--- Purpose : Inserts a total into the SA_TOTAL and SA_SYS_VALUE tables.
---
    FUNCTION INSERT_TOTAL (O_error_message          IN OUT VARCHAR2,
	                       I_total_id               IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
	                       I_store_day_seq_no       IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_balance_group_seq_no   IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
	                       I_ref_no1                IN     SA_TOTAL.REF_NO1%TYPE,
                           I_ref_no2                IN     SA_TOTAL.REF_NO2%TYPE,
                           I_ref_no3                IN     SA_TOTAL.REF_NO3%TYPE,
	                       I_total_amount           IN     SA_SYS_VALUE.SYS_VALUE%TYPE,
	                       I_total_rev_no           IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                           I_pos_ind                IN     SA_TOTAL_HEAD.POS_IND%TYPE,
                           I_store                  IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                           I_day                    IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
    RETURN INT;
------------------------------------------------------------------------------------------------
--- Function: BUILD_COMB_TOTAL_FUNCTION
--- Purpose : Creates a dynamic total function for a combination total.
---
    FUNCTION BUILD_COMB_TOTAL_FUNCTION
        (O_error_message    IN OUT VARCHAR2,
         I_total_id		    IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
		 I_total_rev_no	    IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
		 I_pos_ind 		    IN     SA_TOTAL_HEAD.POS_IND%TYPE,
         I_refresh_override IN     BOOLEAN)
    RETURN INT;
------------------------------------------------------------------------------------------------
--- Function: REBUILD_TOTAL
--- Purpose : Rebuilds a total.
---
FUNCTION REBUILD_TOTAL (O_error_message  IN OUT  VARCHAR2,
                           I_total_id       IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                           I_total_rev_no   IN      SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                           I_new_vr_id      IN      SA_VR_HEAD.VR_ID%TYPE,
                           I_new_vr_rev_no  IN      SA_VR_HEAD.VR_REV_NO%TYPE)
      RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- Function: PROCESS_CALC_TOTALS
--- Purpose : Recalculates totals for a given store_day_seq_no.
---
    FUNCTION PROCESS_CALC_TOTALS
        (O_error_message        IN OUT VARCHAR2,
         I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
         I_pos_totals_ind       IN     VARCHAR2,
         I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
         I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
    RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- Function: PROCESS_POS_TOTALS
--- Purpose : Recalculates POS totals for a given store_day_seq_no and optional total ID.
---
    FUNCTION PROCESS_POS_TOTALS
        (O_error_message        IN OUT VARCHAR2,
         I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
         I_total_id             IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
         I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
         I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
    RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- Function: INSERT_BAL_LVL_TOTALS
--- Purpose : Inserts SA_TOTAL entries for all BAL_LEVEL totals.
---
    FUNCTION INSERT_BAL_LVL_TOTALS
        (O_error_message		IN OUT VARCHAR2,
   		 I_store_day_seq_no 	IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
         I_bal_group_seq_no IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
         I_store            IN     SA_STORE_DAY.STORE%TYPE,
         I_day              IN     SA_STORE_DAY.DAY%TYPE)
    RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- Function: GET_HEIRARCHY_TOTAL
--- Purpose : Selects the priority total value according to the total
---           heirarchy.  If a total value is not passed it should be
---           NULL.
---
    FUNCTION GET_HEIRARCHY_TOTAL
        (O_error_message		IN OUT VARCHAR2,
         O_total_value          IN OUT SA_SYS_VALUE.SYS_VALUE%TYPE,
         I_hq_value             IN     SA_HQ_VALUE.HQ_VALUE%TYPE,
         I_store_value          IN     SA_STORE_VALUE.STORE_VALUE%TYPE,
         I_sys_value            IN     SA_SYS_VALUE.SYS_VALUE%TYPE,
         I_pos_value            IN     SA_POS_VALUE.POS_VALUE%TYPE)
    RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION GET_SCHEMA(O_error_message        IN OUT VARCHAR2,
                    O_internal_schema_name IN OUT ALL_OBJECTS.OWNER%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------------------
--- Function: DELETE_INVALID_DEF_ERROR
--- Purpose : This function will delete from the sa_error and sa_error_wksht tables
---           where the error code is either 'INV_TOTAL_DEF' or 'INV_RULE_DEF' and the
---           error is related to the passed in total or rule ID.
---
FUNCTION DELETE_INVALID_DEF_ERROR(O_error_message      IN OUT  VARCHAR2,
                                  I_total_id           IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                  I_rule_id            IN      SA_RULE_HEAD.RULE_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------------------
--- Purpose: Retrieve the most recent revision number for the passed-in
---          total ID.
---
FUNCTION MAX_TOTAL_REV_NO(O_error_message     IN OUT  VARCHAR2,
                          O_max_total_rev_no  IN OUT  SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                          I_total_id          IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
END SA_BUILD_TOTAL_SQL;
/
