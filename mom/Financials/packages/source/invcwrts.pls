CREATE OR REPLACE PACKAGE INVC_WRITE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------
--- Function: ASN_TO_INVC
--- Purpose:  This function will create an invoice using the information from an ASN,
---           assuming that the ASN is accepted as the invoice. This will only happen when
---           invoice matching is being used and for ASNs from suppliers with an evaluated
---           receipts settlement agreement. The invoice will be written at the point the
---           ASN is received or matched to an existing unmatched receipt.
---           This function may also be used in adjusting existing receipts. To do this,
---           a SKU, adjust qty, original cost, new cost will be available as
---           parameters.
--- Calls:    INVC_MATCH_SQL.UNMATCH, INVC_MATCH_SQL.UNAPPROVE,
---           SUPP_ATTRIB_SQL.DBT_MEMO_CODE, INVC_SQL.NEXT_INVC_ID,
---           SYSTEM_OPTIONS_SQL.GET_VAT_IND, TAX_SQL.GET_TAX_REGION_DESC, TAX_SQL.GET_TAX_RATE
--------------------------------------------------------------------------------------------
FUNCTION ASN_TO_INVC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_posted          IN OUT   BOOLEAN,
                     I_rcpt            IN       SHIPMENT.SHIPMENT%TYPE,
                     I_supplier        IN       SUPS.SUPPLIER%TYPE,
                     I_invc_type       IN       INVC_HEAD.INVC_TYPE%TYPE,
                     I_item            IN       SHIPSKU.ITEM%TYPE,
                     I_old_unit_cost   IN       SHIPSKU.UNIT_COST%TYPE,
                     I_new_unit_cost   IN       SHIPSKU.UNIT_COST%TYPE,
                     I_rcv_qty         IN       SHIPSKU.QTY_RECEIVED%TYPE,
                     I_adj_qty         IN       SHIPSKU.QTY_RECEIVED%TYPE,
                     I_vat_region      IN       VAT_ITEM.VAT_REGION%TYPE)
                     return BOOLEAN;
--------------------------------------------------------------------------------------------
--- Function: WRITE_DBT_CRDT
--- Purpose:  This function will write a debit memo or credit note request, depending on the
---           supplier's debit memo code. This memo/request will be written for the
---           difference between the invoice and the matching receipts.
--- Calls:    INVC_MATCH_SQL.CHECK_DETAILS,
---           SYSTEM_OPTIONS_SQL.GET_VAT_IND, TAX_SQL.GET_TAX_REGION_DESC,
---           INVC_MATCH_SQL.ITEM_MATCH_ALL, SUPP_ATTRIB_SQL.DBT_MEMO_CODE,
---           INVC_SQL.NEXT_INVC_ID, TAX_SQL.GET_TAX_RATE, INVC_SQL.UPDATE_STATUSES
--------------------------------------------------------------------------------------------
FUNCTION WRITE_DBT_CRDT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_invc_id         IN       INVC_HEAD.INVC_ID%TYPE,
                        I_invc_type       IN       INVC_HEAD.INVC_TYPE%TYPE,
                        I_user_id         IN       VARCHAR2,
                        I_ref_rsn_code    IN       INVC_HEAD.REF_RSN_CODE%TYPE,
                        I_supplier        IN       INVC_HEAD.SUPPLIER%TYPE,
                        I_dbt_crdt_id     IN       INVC_HEAD.INVC_ID%TYPE,
                        I_vat_region      IN       VAT_ITEM.VAT_REGION%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--- Function: WRITE_RTV_DBT_CRDT
--- Purpose:  This function will call WRITE_RTV based on the Credit or Debit memo.
--- Calls:    WRITE_RTV
--------------------------------------------------------------------------------------------
FUNCTION WRITE_RTV_DBT_CRDT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE,
                            I_invc_type       IN       INVC_HEAD.INVC_TYPE%TYPE,
                            I_user_id         IN       VARCHAR2,
                            I_ref_rsn_code    IN       INVC_HEAD.REF_RSN_CODE%TYPE,
                            I_supplier        IN       RTV_HEAD.SUPPLIER%TYPE,
                            I_dbt_crdt_id     IN       INVC_HEAD.INVC_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--- Function: WRITE_RTV
--- Purpose:  This function will write a debit memo or credit not request for a RTV order,
---           depending on the supplier's debit memo code. This memo/request will be written
---           for the total of the merchandise being returned.
--- Calls:    SUPP_ATTRIB_SQL.DBT_MEMO_CODE, INVC_SQL.NEXT_INVC_ID,
---           INVC_SQL.INVC_SYSTEM_OPTIONS_INDS
--------------------------------------------------------------------------------------------
FUNCTION WRITE_RTV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE,
                   I_invc_type       IN       INVC_HEAD.INVC_TYPE%TYPE,
                   I_user_id         IN       VARCHAR2,
                   I_ref_rsn_code    IN       INVC_HEAD.REF_RSN_CODE%TYPE,
                   I_supplier        IN       RTV_HEAD.SUPPLIER%TYPE,
                   I_dbt_crdt_id     IN       INVC_HEAD.INVC_ID%TYPE,
                   I_crdt_dbt        IN       VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
FUNCTION INVC_UNIT_COST_CHANGE (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_old_invc_unit_cost    IN     INVC_DETAIL.INVC_UNIT_COST%TYPE,
                                I_new_invc_unit_cost    IN     INVC_DETAIL.INVC_UNIT_COST%TYPE,
                                I_item                  IN     INVC_DETAIL.ITEM%TYPE,
                                I_invc_id               IN     INVC_DETAIL.INVC_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--- Function: WRITE_RCA_DBT_CRDT 
--- Purpose:  This function will write a debit memo or credit note request for a receiver cost adjustment, 
---           depending on the supplier's debit memo code. This memo/request will be written 
---           for the total of the merchandise for which RCA is done.
--- Calls:    SUPP_ATTRIB_SQL.DBT_MEMO_CODE, INVC_SQL.NEXT_INVC_ID,
---           INVC_SQL.INVC_SYSTEM_OPTIONS_INDS
--------------------------------------------------------------------------------------------
FUNCTION WRITE_RCA_DBT_CRDT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_invc_type       IN       INVC_HEAD.INVC_TYPE%TYPE,
                            I_user_id         IN       VARCHAR2,
                            I_ref_rsn_code    IN       INVC_HEAD.REF_RSN_CODE%TYPE,
                            I_supplier        IN       INVC_HEAD.SUPPLIER%TYPE,
                            I_dbt_crdt_id     IN       INVC_HEAD.INVC_ID%TYPE,
                            I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                            I_item            IN       SHIPSKU.ITEM%TYPE,
                            I_new_cost        IN       SHIPSKU.UNIT_COST%TYPE,
                            I_curr_cost       IN       SHIPSKU.UNIT_COST%TYPE,
                            I_qty_matched     IN       SHIPSKU.QTY_MATCHED%TYPE)
   RETURN BOOLEAN ;
--------------------------------------------------------------------------------------------
END INVC_WRITE_SQL;
/
