CREATE OR REPLACE PACKAGE BODY SA_IMPORT_ADJ_SQL AS

FUNCTION PROCESS_ADJUSTMENT(O_error_message        IN OUT  VARCHAR2,
                            I_total_seq_no         IN      SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                            I_new_value            IN      SA_HQ_VALUE.HQ_VALUE%TYPE,
                            I_store                IN      SA_TOTAL.STORE%TYPE,
                            I_day                  IN      SA_TOTAL.DAY%TYPE)
   RETURN BOOLEAN;

FUNCTION PROCESS_NEW_TOTAL(O_error_message        IN OUT  VARCHAR2,
                           I_store_day_seq_no     IN      SA_TOTAL.STORE_DAY_SEQ_NO%TYPE,
                           I_total_id             IN      SA_TOTAL.TOTAL_ID%TYPE,
                           I_ref_no1              IN      SA_TOTAL.REF_NO1%TYPE,
                           I_ref_no2              IN      SA_TOTAL.REF_NO2%TYPE,
                           I_ref_no3              IN      SA_TOTAL.REF_NO3%TYPE,
                           I_new_value            IN      SA_HQ_VALUE.HQ_VALUE%TYPE,
                           I_store                IN      SA_TOTAL.STORE%TYPE,
                           I_day                  IN      SA_TOTAL.DAY%TYPE)
   RETURN BOOLEAN;

FUNCTION MOVE_EXPORTED_ENTRY(O_error_message     IN OUT  VARCHAR2,
                             I_total_seq_no      IN      SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                             I_store             IN      SA_TOTAL.STORE%TYPE,
                             I_day               IN      SA_TOTAL.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------

FUNCTION POST_ADJUSTMENT(O_error_message   IN OUT  VARCHAR2,
                         I_source          IN      SA_STORE_DATA.SYSTEM_CODE%TYPE,
                         I_new_value       IN      SA_HQ_VALUE.HQ_VALUE%TYPE,
                         I_total_seq_no    IN      SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         I_store_no        IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                         I_business_date   IN      SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                         I_total_id        IN      SA_TOTAL.TOTAL_ID%TYPE DEFAULT NULL,
                         I_ref_no1         IN      SA_TOTAL.REF_NO1%TYPE DEFAULT NULL,
                         I_ref_no2         IN      SA_TOTAL.REF_NO2%TYPE DEFAULT NULL,
                         I_ref_no3         IN      SA_TOTAL.REF_NO3%TYPE DEFAULT NULL)
   RETURN BOOLEAN
   IS

   L_store_day_seq_no         SA_TOTAL.STORE_DAY_SEQ_NO%TYPE := NULL;
   L_total_seq_no             SA_TOTAL.TOTAL_SEQ_NO%TYPE := NULL;
   L_total_id                 SA_TOTAL.TOTAL_ID%TYPE := NULL;
   L_store                    SA_TOTAL.STORE%TYPE := NULL;
   L_day                      SA_TOTAL.DAY%TYPE := NULL;
   L_audit_status             SA_STORE_DAY.AUDIT_STATUS%TYPE := NULL;

   /* This query will get both the total_id and store_day_seq_no */
   CURSOR c_get_total_id IS
   SELECT store_day_seq_no, total_id, store, day
     FROM sa_total
    WHERE total_seq_no = L_total_seq_no
      AND (store = I_store_no or I_store_no is NULL)
      AND (day = SA_DATE_HASH(I_business_date) or I_business_date is NULL);

   CURSOR c_get_store_day_seq_no IS
   SELECT store_day_seq_no, store, day
     FROM sa_store_day
    WHERE to_char(business_date, 'DD-MON-RR') = to_char(I_business_date, 'DD-MON-RR')
      AND store = I_store_no;

   CURSOR c_get_sa_total_data IS
   SELECT total_seq_no, store, day
     FROM sa_total
    WHERE store_day_seq_no = (SELECT store_day_seq_no
                                 FROM sa_store_day
                                WHERE store = I_store_no
                                  AND business_date = I_business_date)
      AND store = I_store_no
      AND day = SA_DATE_HASH(I_business_date)
      AND total_id           = I_total_id
      AND NVL(ref_no1, ' ')  = NVL(I_ref_no1, ' ')
      AND NVL(ref_no2, ' ')  = NVL(I_ref_no2, ' ')
      AND NVL(ref_no3, ' ')  = NVL(I_ref_no3, ' ');

BEGIN

   /* If total_seq_no is not provided see if one exists for this data */
   if I_total_seq_no is NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_SA_TOTAL_DATA','SA_TOTAL', NULL);
      OPEN c_get_sa_total_data;

      SQL_LIB.SET_MARK('FETCH','C_GET_SA_TOTAL_DATA','SA_TOTAL', NULL);
      FETCH c_get_sa_total_data INTO L_total_seq_no, L_store, L_day;

      SQL_LIB.SET_MARK('CLOSE','C_GET_SA_TOTAL_DATA','SA_TOTAL', NULL);
      CLOSE c_get_sa_total_data;
   elsif I_total_seq_no is not NULL then
      L_total_seq_no := I_total_seq_no;
   end if;

   /* Perform adjustment if total_seq_no is provided */
   if L_total_seq_no is not NULL then

      SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_ID','SA_TOTAL', NULL);
      OPEN c_get_total_id;

      SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_ID','SA_TOTAL', NULL);
      FETCH c_get_total_id INTO L_store_day_seq_no, L_total_id, L_store, L_day;

      if c_get_total_id%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('SAIADJNT', L_total_seq_no, NULL, NULL);
         return false;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_ID','SA_TOTAL', NULL);
      CLOSE c_get_total_id;

      /* Lock record */
      if SA_LOCKING_SQL.GET_WRITE_LOCK(O_error_message,
                                       L_store_day_seq_no) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('SAIADJNL', L_store_day_seq_no, NULL, NULL);
         return false;
      end if;

      if MOVE_EXPORTED_ENTRY(O_error_message,
                             L_total_seq_no,
                             L_store,
                             L_day) = FALSE then
         return false;
      end if;

      if PROCESS_ADJUSTMENT(O_error_message,
                            L_total_seq_no,
                            I_new_value,
                            L_store,
                            L_day) = FALSE then
         return false;
      end if;

   /* Create new total */
   elsif L_total_seq_no is NULL then
      if (I_store_no is NULL) OR (I_business_date is NULL) OR (I_total_id is NULL) then

         O_error_message := SQL_LIB.CREATE_MSG('SA_IMPADJ_NULLVALUE',
                                                     I_store_no,
                                                     NULL,
                                                     NULL);
         return false;

      end if;

      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_DAY_SEQ_NO','SA_STORE_DAY', NULL);
      OPEN c_get_store_day_seq_no;

      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_DAY_SEQ_NO','SA_STORE_DAY', NULL);
      FETCH c_get_store_day_seq_no INTO L_store_day_seq_no, L_store, L_day;

      if c_get_store_day_seq_no%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('SAIADJNSD', I_business_date, I_store_no, NULL);
         return false;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_DAY_SEQ_NO','SA_STORE_DAY', NULL);
      CLOSE c_get_store_day_seq_no;

      /* Lock record */
      if SA_LOCKING_SQL.GET_WRITE_LOCK(O_error_message,
                                       L_store_day_seq_no) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('SAIADJNL', L_store_day_seq_no, NULL, NULL);
         return false;
      end if;

      if PROCESS_NEW_TOTAL(O_error_message,
                           L_store_day_seq_no,
                           I_total_id,
                           I_ref_no1,
                           I_ref_no2,
                           I_ref_no3,
                           I_new_value,
                           L_store,
                           L_day) = FALSE then
         return false;
      end if;

   end if;

   if STORE_DAY_SQL.GET_AUDIT_STATUS(O_error_message,
                                     L_audit_status,
                                     L_store_day_seq_no,
                                     L_store,
                                     L_day) = FALSE then
      return (FALSE);
   end if;

   if L_audit_status not in ('U','R') then 
      if STORE_DAY_SQL.UPDATE_AUDIT_STATUS(O_error_message,
                                           L_store_day_seq_no,
                                           'R',
                                           L_store,
                                           L_day) = FALSE then
         return (FALSE);
      end if;
    end if;

   /* if the total_seq_no was not inputted, then L_total_id has not been populated yet */
   if L_total_seq_no is NULL then
      L_total_id := I_total_id;
   end if;

   if SA_TOTAL_SQL.UPDATE_EXP_LOG(O_error_message,
                                  L_store_day_seq_no,
                                  L_total_id,
                                  I_source,
                                  L_store,
                                  L_day) = FALSE then
      return FALSE;
   end if;


   /* Unlock record */
   if SA_LOCKING_SQL.RELEASE_LOCK(O_error_message,
                                  L_store_day_seq_no,
                                  USER,
                                  0) = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('SAIADJRL', L_store_day_seq_no, NULL, NULL);
      return false;
   end if;

   return(true);

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, 'POST_ADJUSTMENT', SQLCODE);
      return false;

END POST_ADJUSTMENT;
----------------------------------------------------------------------------
FUNCTION PROCESS_ADJUSTMENT(O_error_message        IN OUT  VARCHAR2,
                            I_total_seq_no         IN      SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                            I_new_value            IN      SA_HQ_VALUE.HQ_VALUE%TYPE,
                            I_store                IN      SA_TOTAL.STORE%TYPE,
                            I_day                  IN      SA_TOTAL.DAY%TYPE)
   RETURN BOOLEAN
   IS

   L_max_value_rev_no         SA_POS_VALUE.VALUE_REV_NO%TYPE;

   /* This query finds the next value_rev_no by finding the greatest
      value _rev_no among the value tables and adding 1 */
   CURSOR c_max IS
   SELECT (GREATEST(NVL(MAX(pv.value_rev_no), 0),
          NVL(MAX(sys.value_rev_no), 0),
          NVL(MAX(store.value_rev_no), 0),
          NVL(MAX(hq.value_rev_no), 0)) + 1)
     FROM sa_pos_value pv,
          sa_sys_value sys,
          sa_store_value store,
          sa_hq_value hq,
          sa_total st
    WHERE st.total_seq_no = I_total_seq_no
      AND st.store        = I_store
      AND st.day          = I_day
      AND st.total_seq_no = pv.total_seq_no(+)
      AND st.store        = pv.store(+)
      AND st.day          = pv.day(+)
      AND st.total_seq_no = sys.total_seq_no(+)
      AND st.store        = sys.store(+)
      AND st.day          = sys.day(+)
      AND st.total_seq_no = store.total_seq_no(+)
      AND st.store        = store.store(+)
      AND st.day          = store.day(+)
      AND st.total_seq_no = hq.total_seq_no(+)
      AND st.store        = hq.store(+)
      AND st.day          = hq.day(+);

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_MAX','SA_HQ_VALUE', NULL);
   OPEN c_max;

   SQL_LIB.SET_MARK('FETCH','C_MAX','SA_HQ_VALUE', NULL);
   FETCH c_max INTO L_max_value_rev_no;

   if c_max%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('SAIADJNT', I_total_seq_no, NULL, NULL);
      return false;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_MAX','SA_HQ_VALUE', NULL);
   CLOSE c_max;

   SQL_LIB.SET_MARK('INSERT', NULL,'SA_HQ_VALUE', NULL);
   INSERT INTO sa_hq_value(total_seq_no,
                           store,
                           day,
                           value_rev_no,
                           hq_value,
                           update_id,
                           update_datetime)
          VALUES (I_total_seq_no,
                  I_store,
                  I_day,
                  L_max_value_rev_no,
                  I_new_value,
                  USER,
                  SYSDATE);

   return(true);

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, 'PROCESS_ADJUSTMENT', SQLCODE);
      return false;

END PROCESS_ADJUSTMENT;
----------------------------------------------------------------------------
FUNCTION PROCESS_NEW_TOTAL(O_error_message        IN OUT  VARCHAR2,
                           I_store_day_seq_no     IN      SA_TOTAL.STORE_DAY_SEQ_NO%TYPE,
                           I_total_id             IN      SA_TOTAL.TOTAL_ID%TYPE,
                           I_ref_no1              IN      SA_TOTAL.REF_NO1%TYPE,
                           I_ref_no2              IN      SA_TOTAL.REF_NO2%TYPE,
                           I_ref_no3              IN      SA_TOTAL.REF_NO3%TYPE,
                           I_new_value            IN      SA_HQ_VALUE.HQ_VALUE%TYPE,
                           I_store                IN      SA_TOTAL.STORE%TYPE,
                           I_day                  IN      SA_TOTAL.DAY%TYPE)
   RETURN BOOLEAN
   IS

   L_total_seq_no        SA_TOTAL.TOTAL_SEQ_NO%TYPE;
   L_display_order       SA_TOTAL_HEAD.DISPLAY_ORDER%TYPE;

   /* This query gets the display order for sa_total
      which is the same as from sa_total_head */
   CURSOR c_get_display_order IS
   SELECT display_order
     FROM sa_total_head
    WHERE total_id = I_total_id;

BEGIN

   /* Gets the next total_seq_no from the function */
   if SA_SEQUENCE2_SQL.GET_TOTAL_SEQ(O_error_message,
                                     L_total_seq_no) = FALSE then
      return false;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_ORDER','SA_TOTAL_HEAD', NULL);
   OPEN c_get_display_order;

   SQL_LIB.SET_MARK('FETCH','C_GET_DISPLAY_ORDER','SA_TOTAL_HEAD', NULL);
   FETCH c_get_display_order INTO L_display_order;

   if c_get_display_order%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('SAIADJNT', I_total_id, NULL, NULL);
      return false;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_GET_DISPLAY_ORDER','SA_TOTAL_HEAD', NULL);
   CLOSE c_get_display_order;

   SQL_LIB.SET_MARK('INSERT', NULL,'SA_TOTAL', NULL);
   INSERT INTO sa_total(total_seq_no,
                        store,
                        day,
                        total_id,
                        store_day_seq_no,
                        bal_group_seq_no,
                        ref_no1,
                        ref_no2,
                        ref_no3,
                        display_order,
                        error_ind,
                        status)
          VALUES (L_total_seq_no,
                  I_store,
                  I_day,
                  I_total_id,
                  I_store_day_seq_no,
                  NULL,
                  I_ref_no1,
                  I_ref_no2,
                  I_ref_no3,
                  L_display_order,
                  'N',
                  'P');

   if PROCESS_ADJUSTMENT(O_error_message,
                         L_total_seq_no,
                         I_new_value,
                         I_store,
                         I_day) = FALSE then
      return false;
   end if;

   return(true);

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, 'PROCESS_NEW_TOTAL', SQLCODE);
      return false;

END PROCESS_NEW_TOTAL;
----------------------------------------------------------------------------
FUNCTION MOVE_EXPORTED_ENTRY(O_error_message     IN OUT  VARCHAR2,
                             I_total_seq_no      IN      SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                             I_store             IN      SA_TOTAL.STORE%TYPE,
                             I_day               IN      SA_TOTAL.DAY%TYPE)
   RETURN BOOLEAN
   IS

   L_precedence   V_SA_GET_TOTAL.PRECEDENCE%TYPE   := NULL;
   L_value_rev_no V_SA_GET_TOTAL.VALUE_REV_NO%TYPE := NULL;

BEGIN
   if (SA_TOTAL_SQL.GET_PRECEDENCE(O_error_message,
                                   I_total_seq_no,
                                   L_precedence,
                                   L_value_rev_no,
                                   I_store,
                                   I_day) = FALSE) then
      return FALSE;
   end if;
   ---
   insert into sa_exported_rev(export_seq_no,
                               store,
                               day,
                               rev_no,
                               store_day_seq_no,
                               tran_seq_no,
                               total_seq_no,
                               acct_period,
                               system_code,
                               exp_datetime,
                               status)
                        select export_seq_no,
                               I_store,
                               I_day,
                               L_value_rev_no,
                               store_day_seq_no,
                               tran_seq_no,
                               total_seq_no,
                               acct_period,
                               system_code,
                               exp_datetime,
                               status
                          from sa_exported
                         where total_seq_no = I_total_seq_no
                           and store = I_store
                           and day = I_day;

   delete from sa_exported
      where total_seq_no = I_total_seq_no
        and store = I_store
        and day = I_day;



   RETURN (true);

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'MOVE_EXPORTED_ENTRY',
                                            SQLCODE);
      return (false);

END MOVE_EXPORTED_ENTRY;

END SA_IMPORT_ADJ_SQL;
/
