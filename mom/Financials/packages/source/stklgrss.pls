CREATE OR REPLACE PACKAGE STKLEDGR_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------
--Function Name:  tran_data_insert
--Purpose:  This function performs the insert into tran_data.
--      It first validates input parameters, making sure fields
--      are null or not null as required.    If the retail
--      method of accounting is used in the system, this function
--      calculates transfers at cost.  If a transfer is being
--      made (tran_code = 30), it inserts a record for both the
--      source and receiving locations.  Finally, the function inserts
--      a record with a tran_code of 13 (Permanent Markdown) or 11
--      (Markup) as appropriate for the receiving location.
--Local Functions:    RECORD_INSERTS
--                    RECLASSIFY
--Calls:    CURRENCY_SQL.CONVERT
--          ITEMLOC_ATTRIB_SQL.GET_AV_UNIT_COST
--          ITEMLOC_QUANTITY_SQL.GET_TRANSFER
--          GET_SYSTEM_IND
--          SYSTEM_OPTIONS_SQL.STD_AV_IND
--          CURRENCY_SQL.GET_CURR_LOC
--
--Date Created:  25-JUN-96
--Created By:  Chrissy Ronnback
--Note:  To implement stock ledger at a level other than
--   subclass/store/week, the cursor C_MARKON would need
--   to be modified.
--   EXAMPLE:
--   This cursor would work at the class/total store/month level.
--      cursor C_MARKON is
--         select cum_markon_pct
--           from month_data
--          where dept = I_dept
--            and class = I_class
--            and cum_markon_pct IS NOT NULL
--            and eom_date <= L_vdate
--          order by eom_date desc;
--
--Modified By:  Derek Hoffman
--Modification: Moved a majority of the code into the function RECORD_INSERTS.
--              The TRAN_DATA_INSERTS code now does the same validity checks,
--              but then it checks if the sku is a display pack.  If so,
--              the display pack is broken down to the component skus,
--              then the RECORD_INSERTS function is called for each sku.
--              If the sku is not a display pack, RECORD_INSERTS is called.
------------------------------------------------------------------------

--BULK tran_data insert tables
TYPE item_TBL            is table of TRAN_DATA.ITEM%TYPE            INDEX BY BINARY_INTEGER;
TYPE dept_TBL            is table of TRAN_DATA.DEPT%TYPE            INDEX BY BINARY_INTEGER;
TYPE class_TBL           is table of TRAN_DATA.CLASS%TYPE           INDEX BY BINARY_INTEGER;
TYPE subclass_TBL        is table of TRAN_DATA.SUBCLASS%TYPE        INDEX BY BINARY_INTEGER;
TYPE pack_ind_TBL        is table of TRAN_DATA.PACK_IND%TYPE        INDEX BY BINARY_INTEGER;
TYPE location_TBL        is table of TRAN_DATA.LOCATION%TYPE        INDEX BY BINARY_INTEGER;
TYPE loc_type_TBL        is table of TRAN_DATA.LOC_TYPE%TYPE        INDEX BY BINARY_INTEGER;
TYPE tran_date_TBL       is table of TRAN_DATA.TRAN_DATE%TYPE       INDEX BY BINARY_INTEGER;
TYPE tran_code_TBL       is table of TRAN_DATA.TRAN_CODE%TYPE       INDEX BY BINARY_INTEGER;
TYPE adj_code_TBL        is table of TRAN_DATA.ADJ_CODE%TYPE        INDEX BY BINARY_INTEGER;
TYPE units_TBL           is table of TRAN_DATA.UNITS%TYPE           INDEX BY BINARY_INTEGER;
TYPE total_cost_TBL      is table of TRAN_DATA.TOTAL_COST%TYPE      INDEX BY BINARY_INTEGER;
TYPE total_retail_TBL    is table of TRAN_DATA.TOTAL_RETAIL%TYPE    INDEX BY BINARY_INTEGER;
TYPE ref_no_1_TBL        is table of TRAN_DATA.REF_NO_1%TYPE        INDEX BY BINARY_INTEGER;
TYPE ref_no_2_TBL        is table of TRAN_DATA.REF_NO_2%TYPE        INDEX BY BINARY_INTEGER;
TYPE gl_ref_no_TBL       is table of TRAN_DATA.GL_REF_NO%TYPE       INDEX BY BINARY_INTEGER;
TYPE old_unit_retail_TBL is table of TRAN_DATA.OLD_UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
TYPE new_unit_retail_TBL is table of TRAN_DATA.NEW_UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
TYPE pgm_name_TBL        is table of TRAN_DATA.PGM_NAME%TYPE        INDEX BY BINARY_INTEGER;
TYPE sales_type_TBL      is table of TRAN_DATA.SALES_TYPE%TYPE      INDEX BY BINARY_INTEGER;
TYPE vat_rate_TBL        is table of TRAN_DATA.VAT_RATE%TYPE        INDEX BY BINARY_INTEGER;
TYPE av_cost_TBL         is table of TRAN_DATA.AV_COST%TYPE         INDEX BY BINARY_INTEGER;
TYPE timestamp_TBL       is table of TRAN_DATA.TIMESTAMP%TYPE       INDEX BY BINARY_INTEGER;
TYPE ref_pack_no_TBL     is table of TRAN_DATA.REF_PACK_NO%TYPE     INDEX BY BINARY_INTEGER;
TYPE total_cost_excl_elc_TBL is table of TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE INDEX BY BINARY_INTEGER;


P_tran_data_item            item_TBL;
P_tran_data_dept            dept_TBL;
P_tran_data_class           class_TBL;
P_tran_data_subclass        subclass_TBL;
P_tran_data_pack_ind        pack_ind_TBL;
P_tran_data_location        location_TBL;
P_tran_data_loc_type        loc_type_TBL;
P_tran_data_tran_date       tran_date_TBL;
P_tran_data_tran_code       tran_code_TBL;
P_tran_data_adj_code        adj_code_TBL;
P_tran_data_units           units_TBL;
P_tran_data_total_cost      total_cost_TBL;
P_tran_data_total_retail    total_retail_TBL;
P_tran_data_ref_no_1        ref_no_1_TBL;
P_tran_data_ref_no_2        ref_no_2_TBL;
P_tran_data_gl_ref_no       gl_ref_no_TBL;
P_tran_data_old_unit_retail old_unit_retail_TBL;
P_tran_data_new_unit_retail new_unit_retail_TBL;
P_tran_data_pgm_name        pgm_name_TBL;
P_tran_data_sales_type      sales_type_TBL;
P_tran_data_vat_rate        vat_rate_TBL;
P_tran_data_av_cost         av_cost_TBL;
P_tran_data_timestamp       timestamp_TBL;
P_tran_data_ref_pack_no     ref_pack_no_TBL;
P_tran_data_tot_cost_excl_elc total_cost_excl_elc_TBL;
P_tran_data_act_location    location_TBL;
P_tran_data_act_loc_type    loc_type_TBL;

 
P_deal_data_item            item_TBL; 
P_deal_data_dept            dept_TBL;     
P_deal_data_class           class_TBL;
P_deal_data_subclass        subclass_TBL;
P_deal_data_act_loc_type    loc_type_TBL;
P_deal_data_act_location    location_TBL; 
P_deal_data_tran_date       tran_date_TBL;
P_deal_data_tran_code       tran_code_TBL;
P_deal_data_adj_code        adj_code_TBL;
P_deal_data_pgm_name        pgm_name_TBL;
P_deal_data_units           units_TBL;
P_deal_data_total_cost      total_cost_TBL;
P_deal_data_total_retail    total_retail_TBL;
P_deal_data_ref_no_1        ref_no_1_TBL;
P_deal_data_vat_rate        ref_no_2_TBL;
P_deal_data_ref_pack_no     ref_pack_no_TBL;

P_tran_data_size NUMBER := 0;
P_tran_data_savepoint       NUMBER := 0;

-- global variable to hold system_options
LP_system_options   system_options%ROWTYPE;

---------------------------------------------------------------------------
--Function Name:  GET_DECODE
--Purpose:  This function retrieves the decode values (descriptions) from the
--          table tran_data_codes.  This function is Oracle specific
--          and will be used in the 7.0 Financial Interface Project.
--Date Created: 4-May-98
------------------------------------------------------------------------
FUNCTION GET_DECODE(O_error_message IN OUT   VARCHAR2,
                    O_decode        IN OUT   TRAN_DATA_CODES.DECODE%TYPE,
                    I_code          IN       TRAN_DATA_CODES.CODE%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------
--Function Name:  INIT_TRAN_DATA_INSERT
--Purpose:  make sure that the tran_data arrays are empty before using
--          with call to INIT_TRAN_DATA_INSERT.
--Date Created: 17-Jan-03
------------------------------------------------------------------------
FUNCTION INIT_TRAN_DATA_INSERT (O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------
--Function Name: SET_SAVEPOINT
--Purpose      : Save P_tran_data_size to allow rollbacks due to errors.
--Date Created : 06-June-06
------------------------------------------------------------------------
FUNCTION SET_SAVEPOINT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
--Function Name: ROLLBACK_TO_SAVEPOINT
--Purpose      : Rollback Tran data records when errors occur during processing.
--Date Created : 06-June-06
------------------------------------------------------------------------
FUNCTION ROLLBACK_TO_SAVEPOINT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------
--Function Name:  BUILD_TRAN_DATA_INSERT
--Purpose:  creates a cache of tran_data info to be bulk written when a call
--          to FLUSH_TRAN_DATA_INSERT is called.
--Date Created: 17-Jan-03
------------------------------------------------------------------------
FUNCTION BUILD_TRAN_DATA_INSERT
                (O_error_message        IN OUT  VARCHAR2,
                 I_item                 IN      TRAN_DATA.ITEM%TYPE,
                 I_dept                 IN      TRAN_DATA.DEPT%TYPE,
                 I_class                IN      TRAN_DATA.CLASS%TYPE,
                 I_subclass             IN      TRAN_DATA.SUBCLASS%TYPE,
                 I_location             IN      TRAN_DATA.LOCATION%TYPE,
                 I_loc_type             IN      TRAN_DATA.LOC_TYPE%TYPE,
                 I_tran_date            IN      TRAN_DATA.TRAN_DATE%TYPE,
                 IO_tran_code           IN OUT  TRAN_DATA.TRAN_CODE%TYPE,
                 I_adj_code             IN      TRAN_DATA.ADJ_CODE%TYPE,
                 I_units                IN      TRAN_DATA.UNITS%TYPE,
                 IO_total_cost          IN OUT  TRAN_DATA.TOTAL_COST%TYPE,
                 I_total_retail         IN      TRAN_DATA.TOTAL_RETAIL%TYPE,
                 I_ref_no_1             IN      TRAN_DATA.REF_NO_1%TYPE,
                 I_ref_no_2             IN      TRAN_DATA.REF_NO_2%TYPE,
                 I_tsf_source_location  IN      TRAN_DATA.LOCATION%TYPE,
                 I_tsf_source_loc_type  IN      TRAN_DATA.LOC_TYPE%TYPE,
                 I_old_unit_retail      IN      TRAN_DATA.OLD_UNIT_RETAIL%TYPE,
                 I_new_unit_retail      IN      TRAN_DATA.NEW_UNIT_RETAIL%TYPE,
                 I_source_dept          IN      TRAN_DATA.DEPT%TYPE,
                 I_source_class         IN      TRAN_DATA.CLASS%TYPE,
                 I_source_subclass      IN      TRAN_DATA.SUBCLASS%TYPE,
                 I_pgm_name             IN      TRAN_DATA.PGM_NAME%TYPE,
                 --
                 I_gl_ref_no            IN      TRAN_DATA.GL_REF_NO%TYPE DEFAULT NULL,
                 I_pack_ind             IN      ITEM_MASTER.PACK_IND%TYPE DEFAULT NULL,
                 I_sellable_ind         IN      ITEM_MASTER.SELLABLE_IND%TYPE DEFAULT NULL,
                 I_orderable_ind        IN      ITEM_MASTER.ORDERABLE_IND%TYPE DEFAULT NULL,
                 I_pack_type            IN      ITEM_MASTER.PACK_TYPE%TYPE DEFAULT NULL,
                 I_vat_rate             IN      VAT_CODE_RATES.VAT_RATE%TYPE DEFAULT NULL,
                 I_class_vat_ind        IN      CLASS.CLASS_VAT_IND%TYPE DEFAULT NULL,
                 I_ref_pack_no          IN      TRAN_DATA.REF_PACK_NO%TYPE DEFAULT NULL,
                 I_total_cost_excl_elc  IN      TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE DEFAULT NULL,
                 I_tax_value            IN      GTAX_ITEM_ROLLUP.CUM_TAX_PCT%TYPE DEFAULT NULL)
RETURN BOOLEAN;

------------------------------------------------------------------------
--Function Name:  TRAN_DATA_INSERT
--Purpose:  creates a cache of tran_data info to be bulk written when a call
--          to FLUSH_TRAN_DATA_INSERT is called.
--Date Created: 17-Jan-03
------------------------------------------------------------------------
FUNCTION TRAN_DATA_INSERT
                (O_error_message        IN OUT  VARCHAR2,
                 I_item                 IN      TRAN_DATA.ITEM%TYPE,
                 I_dept                 IN      TRAN_DATA.DEPT%TYPE,
                 I_class                IN      TRAN_DATA.CLASS%TYPE,
                 I_subclass             IN      TRAN_DATA.SUBCLASS%TYPE,
                 I_location             IN      TRAN_DATA.LOCATION%TYPE,
                 I_loc_type             IN      TRAN_DATA.LOC_TYPE%TYPE,
                 I_tran_date            IN      TRAN_DATA.TRAN_DATE%TYPE,
                 IO_tran_code           IN OUT  TRAN_DATA.TRAN_CODE%TYPE,
                 I_adj_code             IN      TRAN_DATA.ADJ_CODE%TYPE,
                 I_units                IN      TRAN_DATA.UNITS%TYPE,
                 IO_total_cost          IN OUT  TRAN_DATA.TOTAL_COST%TYPE,
                 I_total_retail         IN      TRAN_DATA.TOTAL_RETAIL%TYPE,
                 I_ref_no_1             IN      TRAN_DATA.REF_NO_1%TYPE,
                 I_ref_no_2             IN      TRAN_DATA.REF_NO_2%TYPE,
                 I_tsf_source_location  IN      TRAN_DATA.LOCATION%TYPE,
                 I_tsf_source_loc_type  IN      TRAN_DATA.LOC_TYPE%TYPE,
                 I_old_unit_retail      IN      TRAN_DATA.OLD_UNIT_RETAIL%TYPE,
                 I_new_unit_retail      IN      TRAN_DATA.NEW_UNIT_RETAIL%TYPE,
                 I_source_dept          IN      TRAN_DATA.DEPT%TYPE,
                 I_source_class         IN      TRAN_DATA.CLASS%TYPE,
                 I_source_subclass      IN      TRAN_DATA.SUBCLASS%TYPE,
                 I_pgm_name             IN      TRAN_DATA.PGM_NAME%TYPE,
                 I_gl_ref_no            IN      TRAN_DATA.GL_REF_NO%TYPE DEFAULT NULL,
                 I_ref_pack_no          IN      TRAN_DATA.REF_PACK_NO%TYPE DEFAULT NULL,
                 I_total_cost_excl_elc  IN      TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE DEFAULT NULL)
RETURN BOOLEAN;
------------------------------------------------------------------------
--Function Name:  FLUSH_TRAN_DATA_INSERT
--Purpose:  dump all tran_data writes to database that have been built
--          upto this point with call to FLUSH_TRAN_DATA_INSERT.
--Date Created: 17-Jan-03
------------------------------------------------------------------------
FUNCTION FLUSH_TRAN_DATA_INSERT (O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
--Function Name:  stock_ledger_insert
--Purpose:  This function performs the insert into stock_ledger_inserts,
--      after validating input parameters to make sure fields
--      are null or not null as required.
--Date Created: 10-Jul-96
--Created By:   Amy Selby
-----------------------------------------------------------------------------------
FUNCTION STOCK_LEDGER_INSERT
        (I_type_code        IN     VARCHAR2,
         I_dept             IN     NUMBER,
         I_class            IN     NUMBER,
         I_subclass         IN     NUMBER,
         I_location         IN     NUMBER,
         O_error_message    IN OUT VARCHAR2)
         RETURN BOOLEAN;
------------------------------------------------------------------------
--Function Name:  stock_ledger_insert
--Purpose:  This function performs the insert into stock_ledger_inserts,
--      after validating input parameters to make sure fields
--      are null or not null as required.
--Date Created: 10-Jul-96
--Created By:   Amy Selby
-----------------------------------------------------------------------------------
FUNCTION STOCK_LEDGER_INSERT
        (I_type_code        IN     VARCHAR2,
         I_dept             IN     NUMBER,
         I_class            IN     NUMBER,
         I_subclass         IN     NUMBER,
         I_location         IN     NUMBER,
         I_async_ind        IN     CHAR,
         O_error_message    IN OUT VARCHAR2)
         RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_TSF_COSTS_RETAILS
--       Purpose: Retreive av_cost and unit_retail at from-loc/to_loc for a transfer. The 
--                transfer here could be either 1st-leg or 2nd-leg or single transfer of
--                any kinds of transfer(Inter/Intra-company with/without item transformation).
--
-- Two-legged transfer --
-- If 1st leg = ICT, for 1st leg shipment, finisher's retail is using transformed item's unit 
-- retail at the final to-location (for both internal and external finishers), because we want 
-- to avoid writing MU/MD for the receiving entity (same as receiving a PO for the receiving 
-- location/entity).
--  
--    1. 1st leg 
--       tran_code 38:  
--       unit_cost = WAC at from-location (also include up charge when recalc WAC)
--       unit_retail = tsf price (in from-loc's currency)
--
--       tran code 37:  
--       unit_cost = tsf price (in finisher's currency)
--       unit_retail = unit retail of item (or transformed item) at to-location (in finisher's currency)
--
--       tran code 17/18: 
--       unit_retail = difference between unit retail of item at from-location and tsf price (in from-loc's currency)
--
--    2. 2nd leg 
--       tran code 30/32: 
--       unit_cost = WAC at finisher (also include up charge and WO cost when recalc WAC)
--       unit_retail = unit retail of item (or transformed item) at to-location (in finisher's currency).  
--
--       Note: regular MU/MD (11/13) should NOT be written in this case (regardless of the system option 
--       tsf_markdown_loc_code value), therefore finisher's retail is using to-loc's retail. 
--
--    3. MU/MD will not be written as result of writing inventory adjustment for item transformation
--       at the finisher, i.e. we use the to-item's unit retail at to-location to write inventory 
--       adjustment tran_data (tran code 23) records for both the from-item and to-item so that MU/MD 
--       is not needed.  
--
--    4. Note that shipment for one of the legs may not exist, i.e. internal finisher is in the same 
--       physical wh as the sending or receiving wh. 
--
-- If 2nd leg = ICT, for 2nd leg shipment, finisher's retail is using transformed item's unit retail
-- at the final to-location (for both internal and external finishers) as well.   
--
--    1. 1st leg 
--       tran code 30/32:
--       unit_cost = WAC of item (from-item) at from-location (also include up charge)
--       unit_retail = unit retail of item (from-item) at from-location (in from-locations' currency).
--  
--       Note: we can not write any MU/MD (11/13) in this case, so we must force the program to set 
--       to-loc's retail = from-loc's retail.  
--
--    2. 2nd leg 
--       tran code 38: 
--       unit_cost = WAC of item (transformed item) at finisher (also include up charge and WO cost)
--       unit_retail = tsf price (in finisher's currency).  
--
--       tran code 37: 
--       unit_cost = tsf price (at finisher's currency)  
--       unit_retail = unit retail of item (or transformed item) at to-location (in to-location's currency)
--
--       tran code 17/18, 
--       unit retail = difference between from-item's retail at from-loc and tsf price 
--    3. Inventory adjustment tran_data's (tran code 23) written for item transformation at finisher 
--       use from-item's retail at from-location for both from-item and to-item (in finisher's currency)
--       and no MU/MD is written.
--
-- If both legs are intra-company transfers -  MU/MD should be written to either from-location or 
-- to-location, based on the system options.tsf_markdown_loc_code, not at finisher 
--   
-- If there is no transformation, 
--    if from-loc takes MU/MD (tsf_markdown_loc_code = 'S'), 
--       finisher's retail = to-loc's retail  for both legs, 
--    else, 
--       finisher's retail = from-loc's retail for both legs.
-- Else  -- there is transformation
--    if sending loc takes MU/ MD (tsf_markdown_loc_code = 'R'), 
--       finisher's retail = to-items' retail at to-loc for both legs
--       tran code 23 unit_retail = to-item's retail at to-loc for both from-item and to-item
--    else
--       finisher's retail = from-items' retail at from-loc for both legs
--       tran code 23 unit_retail = from-item's retail at from-loc for both from-item and to-item
--
-- If retail method of accounting is used, although WAC is captured for 37/38, but it is NOT used 
-- in any stock ledger calculation.  
--
-- Note that Inter-company Margin =  Inter-company Transfer Out (38) Retail  * (1 - cum_markon%), 
-- in the stock ledger accounting package (STKLEDGR_ACCTING_SQL).
--
-------------------------------------------------------------------------------------------------
FUNCTION GET_TSF_COSTS_RETAILS(O_error_message         IN OUT rtk_errors.rtk_text%TYPE,
                               O_av_cost               IN OUT item_loc_soh.av_cost%TYPE,
                               O_from_unit_retail      IN OUT item_loc.unit_retail%TYPE,
                               O_to_unit_retail        IN OUT item_loc.unit_retail%TYPE,
                               I_tsf_no                IN     tsfhead.tsf_no%type,
                               I_intercompany          IN     BOOLEAN,
                               I_from_finisher_ind     IN     VARCHAR2,
                               I_to_finisher_ind       IN     VARCHAR2,
                               I_item                  IN     item_loc.item%TYPE,
                               I_from_loc              IN     item_loc.loc%TYPE,
                               I_from_loc_type         IN     item_loc.loc_type%TYPE,
                               I_to_loc                IN     item_loc.loc%TYPE,
                               I_to_loc_type           IN     item_loc.loc_type%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
--Function Name:  WRITE_FINANCIALS
--Purpose: This function is called from the following packages:
--         STOCK_ORDER_RCV_SQL, BOL_SQL and TSF_BT_SQL.BT_EXECUTE.
--         This function will cause records to be written to TRAN_DATA for
--         transfers (both book transfers and bol-based transfers) and allocations
--         using calls to the STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT function.
-----------------------------------------------------------------------------------
FUNCTION WRITE_FINANCIALS(O_error_message       IN OUT VARCHAR2,
                          O_tsf_alloc_unit_cost IN OUT item_loc_soh.av_cost%TYPE,
                          I_call_type           IN     VARCHAR2,
                          I_shipment            IN     shipment.shipment%TYPE,
                          I_distro_no           IN     shipsku.distro_no%TYPE,
                          I_tran_date           IN     period.vdate%TYPE,
                          I_item                IN     item_master.item%TYPE,
                          I_pack_no             IN     item_master.item%TYPE,
                          I_pct_in_pack         IN     NUMBER,
                          I_dept                IN     item_master.dept%TYPE,
                          I_class               IN     item_master.class%TYPE,
                          I_subclass            IN     item_master.subclass%TYPE,
                          I_qty                 IN     tsfdetail.tsf_qty%TYPE,
                          I_weight_cuom         IN     item_loc_soh.average_weight%TYPE,
                          I_from_loc            IN     item_loc.loc%TYPE,
                          I_from_loc_type       IN     item_loc.loc_type%TYPE,
                          I_from_finisher_ind   IN     wh.finisher_ind%TYPE,
                          I_to_loc              IN     item_loc.loc%TYPE,
                          I_to_loc_type         IN     item_loc.loc_type%TYPE,
                          I_to_finisher_ind     IN     wh.finisher_ind%TYPE,
                          I_from_wac            IN     item_loc_soh.av_cost%TYPE,
                          I_profit_chrgs_to_loc IN     NUMBER,
                          I_exp_chrgs_to_loc    IN     NUMBER,
                          I_intercompany        IN     BOOLEAN,
                          I_extended_base_cost  IN     ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE DEFAULT NULL,
                          I_base_cost           IN     ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE DEFAULT NULL,
                          I_ship_date           IN     shipment.ship_date%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--Function Name:  WRITE_FINANCIALS
--Purpose: This overloaded function is created to support calls from STOCK_ORDER_RECONCILE_SQL. 
--         It gets the from loc's wac and in turn calls the other WRITE_FINANCIALS.
-----------------------------------------------------------------------------------
FUNCTION WRITE_FINANCIALS(O_error_message       IN OUT VARCHAR2,
                          I_call_type           IN     VARCHAR2,
                          I_shipment            IN     shipment.shipment%TYPE,
                          I_distro_no           IN     shipsku.distro_no%TYPE,
                          I_tran_date           IN     period.vdate%TYPE,
                          I_item                IN     item_master.item%TYPE,
                          I_pack_no             IN     item_master.item%TYPE,
                          I_pct_in_pack         IN     NUMBER,
                          I_dept                IN     item_master.dept%TYPE,
                          I_class               IN     item_master.class%TYPE,
                          I_subclass            IN     item_master.subclass%TYPE,
                          I_qty                 IN     tsfdetail.tsf_qty%TYPE,
                          I_from_loc            IN     item_loc.loc%TYPE,
                          I_from_loc_type       IN     item_loc.loc_type%TYPE,
                          I_from_finisher_ind   IN     wh.finisher_ind%TYPE,
                          I_to_loc              IN     item_loc.loc%TYPE,
                          I_to_loc_type         IN     item_loc.loc_type%TYPE,
                          I_to_finisher_ind     IN     wh.finisher_ind%TYPE,
                          I_profit_chrgs_to_loc IN     NUMBER,
                          I_exp_chrgs_to_loc    IN     NUMBER,
                          I_intercompany        IN     BOOLEAN,
                          I_ship_date           IN     shipment.ship_date%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--Function Name:  WF_WRITE_FINANCIALS
--Purpose: This function is called to post the Franchise transactions.
--         It returns O_tsf_alloc_unit_cost and O_unit_retail for the cost and retail
--         used in tran_data.total_cost and tran_data.total_retail calculation. 
--         Both are converted to from-loc's currency. This is to allow the same
--         cost and retail to be used for updating unit_cost and unit_retail on
--         shipsku.

-----------------------------------------------------------------------------------
FUNCTION WF_WRITE_FINANCIALS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tsf_alloc_unit_cost OUT    TRAN_DATA.TOTAL_COST%TYPE,
                             O_unit_retail         OUT    TRAN_DATA.TOTAL_RETAIL%TYPE,
                             I_distro_no           IN     SHIPSKU.DISTRO_NO%TYPE,
                             I_distro_type         IN     SHIPSKU.DISTRO_TYPE%TYPE,
                             I_tran_date           IN     PERIOD.VDATE%TYPE,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             I_pack_no             IN     ITEM_MASTER.ITEM%TYPE,
                             I_pct_in_pack         IN     NUMBER,
                             I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                             I_class               IN     ITEM_MASTER.CLASS%TYPE,
                             I_subclass            IN     ITEM_MASTER.SUBCLASS%TYPE,
                             I_qty                 IN     TSFDETAIL.TSF_QTY%TYPE,
                             I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                             I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                             I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_weight_cuom         IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_rma_no              IN     WF_RETURN_HEAD.RMA_NO%TYPE,
                             I_shipment            IN     SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                             I_carton              IN     SHIPSKU.CARTON%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--Function Name:  POST_COST_VARIANCE
--Purpose: This function calculates the total extended cost difference between 
--         transfer/rtv cost and from loc's WAC, and writes stock ledger for 
--         the cost variance. Cost variance is written against the from location.
-----------------------------------------------------------------------------------
FUNCTION POST_COST_VARIANCE(O_error_message       IN OUT VARCHAR2,
                            I_item                IN     item_master.item%TYPE,
                            I_pack_no             IN     packitem.pack_no%TYPE,
                            I_dept                IN     item_master.dept%TYPE,
                            I_class               IN     item_master.class%TYPE,
                            I_subclass            IN     item_master.subclass%TYPE,
                            I_from_loc            IN     item_loc.loc%TYPE,
                            I_from_loc_type       IN     item_loc.loc_type%TYPE,
                            I_tsf_rtv_cost        IN     item_loc_soh.unit_cost%TYPE,
                            I_from_wac            IN     item_loc_soh.av_cost%TYPE,
                            I_qty                 IN     tsfdetail.tsf_qty%TYPE,
                            I_weight_cuom         IN     item_loc_soh.average_weight%TYPE,
                            I_ref_no_1            IN     tran_data.ref_no_1%TYPE,
                            I_ref_no_2            IN     tran_data.ref_no_2%TYPE,
                            I_tran_date           IN     period.vdate%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--Function Name:  GET_TSF_MARKDOWN_LOC
--Purpose: Determines tsf markdown location depending on system options.
-----------------------------------------------------------------------------------
FUNCTION GET_TSF_MARKDOWN_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_tsf_markdown_loc    IN OUT ITEM_LOC.LOC%TYPE,
                              I_tsf_no              IN     TSFHEAD.TSF_NO%TYPE,
                              I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                              I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                              I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------
--Function Name:  POST_VAT
--Purpose: This function will be responsible for posting vat related tran_code if
--         the localization is not localized. This will serve as the NBF for the
--         87 and 88 postings in tran_data.
--          This function will not have a corresponding LF for Brazil.
-----------------------------------------------------------------------------------
FUNCTION POST_VAT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  IO_l10n_fin_rec   IN OUT   L10N_OBJ)
   return BOOLEAN;
-----------------------------------------------------------------------------------
--Function Name:  WF_WRITE_FINANCIALS_PO
--Purpose: This function is similar to the existing STKLEDGER_SQL.WF_WRITE_FINANCIALS
--         function, but this will only process Franchise POs.
--------------------------------------------------------------------------
FUNCTION WF_WRITE_FINANCIALS_PO(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                                I_tran_date           IN     PERIOD.VDATE%TYPE,
                                I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                I_pack_no             IN     ITEM_MASTER.ITEM%TYPE,
                                I_pct_in_pack         IN     NUMBER,
                                I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                                I_class               IN     ITEM_MASTER.CLASS%TYPE,
                                I_subclass            IN     ITEM_MASTER.SUBCLASS%TYPE,
                                I_qty                 IN     ORDLOC.QTY_ORDERED%TYPE,
                                I_costing_loc         IN     ITEM_LOC.LOC%TYPE,
                                I_costing_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                                I_loc                 IN     ITEM_LOC.LOC%TYPE,
                                I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                                I_weight_cuom         IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
FUNCTION LIBRARY_TRAN_DATA_INSERT(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item                 IN      TRAN_DATA.ITEM%TYPE,
                                  I_dept                 IN      TRAN_DATA.DEPT%TYPE,
                                  I_class                IN      TRAN_DATA.CLASS%TYPE,
                                  I_subclass             IN      TRAN_DATA.SUBCLASS%TYPE,
                                  I_location             IN      TRAN_DATA.LOCATION%TYPE,
                                  I_loc_type             IN      TRAN_DATA.LOC_TYPE%TYPE,
                                  I_tran_date            IN      TRAN_DATA.TRAN_DATE%TYPE,
                                  IO_tran_code           IN OUT  TRAN_DATA.TRAN_CODE%TYPE,
                                  I_adj_code             IN      TRAN_DATA.ADJ_CODE%TYPE,
                                  I_units                IN      TRAN_DATA.UNITS%TYPE,
                                  IO_total_cost          IN OUT  TRAN_DATA.TOTAL_COST%TYPE,
                                  I_total_retail         IN      TRAN_DATA.TOTAL_RETAIL%TYPE,
                                  I_ref_no_1             IN      TRAN_DATA.REF_NO_1%TYPE,
                                  I_ref_no_2             IN      TRAN_DATA.REF_NO_2%TYPE,
                                  I_tsf_source_location  IN      TRAN_DATA.LOCATION%TYPE,
                                  I_tsf_source_loc_type  IN      TRAN_DATA.LOC_TYPE%TYPE,
                                  I_old_unit_retail      IN      TRAN_DATA.OLD_UNIT_RETAIL%TYPE,
                                  I_new_unit_retail      IN      TRAN_DATA.NEW_UNIT_RETAIL%TYPE,
                                  I_source_dept          IN      TRAN_DATA.DEPT%TYPE,
                                  I_source_class         IN      TRAN_DATA.CLASS%TYPE,
                                  I_source_subclass      IN      TRAN_DATA.SUBCLASS%TYPE,
                                  I_pgm_name             IN      TRAN_DATA.PGM_NAME%TYPE,
                                  I_gl_ref_no            IN      TRAN_DATA.GL_REF_NO%TYPE DEFAULT NULL)
								  RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------         
END;
/
