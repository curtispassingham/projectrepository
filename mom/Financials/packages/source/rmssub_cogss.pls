
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_COGS AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC VARIABLES
--------------------------------------------------------------------------------

COGS_ADD   CONSTANT VARCHAR2(30) := 'cogscre';


--------------------------------------------------------------------------------
-- PUBLIC RECORD TYPES
--------------------------------------------------------------------------------

TYPE COGS_REC_TYPE IS RECORD
(
trans_date     DATE,
item           ITEM_MASTER.ITEM%TYPE,
store          NUMBER,
units          NUMBER,
header_media   NUMBER,
line_media     NUMBER,
reason_code    CODE_DETAIL.CODE%TYPE,
supplier       SUPS.SUPPLIER%TYPE
);


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

PROCEDURE CONSUME(O_status_code   IN OUT VARCHAR2,
                  O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN     RIB_OBJECT,
                  I_message_type  IN     VARCHAR2);


END RMSSUB_COGS;
/
