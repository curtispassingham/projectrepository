CREATE OR REPLACE PACKAGE SA_MASS_ERROR_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_UPD_DEL
-- Purpose: This function will be called by the Mass Update Popup. Depending on the popup type, this will call
--          the MASS_DELETE or MASS_UPDATE function
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_UPD_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_error_ind               OUT   BOOLEAN,
                      I_tran_seq_no          IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                      I_key_value_1          IN       SA_ERROR.KEY_VALUE_1%TYPE DEFAULT NULL,
                      I_key_value_2          IN       SA_ERROR.KEY_VALUE_2%TYPE DEFAULT NULL,
                      I_store                IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                      I_day                  IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                      I_business_date        IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                      I_error_code           IN       SA_ERROR.ERROR_CODE%TYPE,
                      I_pop_up_type          IN       SA_ERROR_CODES.MASS_RES_POP_UP_TYPE%TYPE,
                      I_table                IN       SA_ERROR_CODES.ERROR_FIX_TABLE%TYPE,
                      I_fix_column           IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                      I_new_value            IN       VARCHAR2,
                      I_dependency           IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_OVERRIDE
-- Purpose: This function will be called by the Mass Update Popup if override radio button is selected.
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_OVERRIDE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                       I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                       I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                       I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                       I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_TOTAL_TRANSACTIONS
-- Purpose: This function will be called by the Mass Update Popup everytime the user selects on the
--          'Apply To' Radio buttons
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TOTAL_TRANSACTIONS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_total_transactions      OUT   NUMBER,
                                 I_tran_seq_no          IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                 I_key_value_1          IN       SA_ERROR.KEY_VALUE_1%TYPE DEFAULT NULL,
                                 I_key_value_2          IN       SA_ERROR.KEY_VALUE_2%TYPE DEFAULT NULL,
                                 I_store                IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                                 I_day                  IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                                 I_business_date        IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                 I_error_code           IN       SA_ERROR.ERROR_CODE%TYPE,
                                 I_table                IN       SA_ERROR_CODES.ERROR_FIX_TABLE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: GET_PROMO_DETAIL
-- Purpose: This function will be called by the Mass Update Popup when fix is related to promotions.
----------------------------------------------------------------------------------------------------------------
FUNCTION GET_PROMO_DETAIL(O_error_message     IN OUT  VARCHAR2,
                          O_rms_promo_type       OUT  SA_TRAN_DISC.RMS_PROMO_TYPE%TYPE,
                          O_promotion            OUT  SA_TRAN_DISC.PROMOTION%TYPE,
                          I_tran_seq_no       IN       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE,
                          I_item_seq_no       IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE,
                          I_discount_seq_no   IN       SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE,
                          I_store             IN       SA_TRAN_DISC.STORE%TYPE,
                          I_day               IN       SA_TRAN_DISC.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: GET_TENDER_DETAIL
-- Purpose: This function will be called by the Mass Update Popup when fix is related to tender.
----------------------------------------------------------------------------------------------------------------
FUNCTION GET_TENDER_DETAIL(O_error_message       IN OUT   VARCHAR2,
                           O_tender_type_group      OUT   SA_TRAN_TENDER.TENDER_TYPE_GROUP%TYPE,
                           I_tran_seq_no         IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE,
                           I_item_seq_no         IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE,
                           I_store               IN       SA_TRAN_TENDER.STORE%TYPE,
                           I_day                 IN       SA_TRAN_TENDER.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
END SA_MASS_ERROR_SQL;
/