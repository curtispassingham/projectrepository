
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_ERROR_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
-- Function Name: ERROR_SYSTEM_EXISTS
-- Purpose      : This function will check to see if an error code/error
--                category relationships exist.
--
FUNCTION ERROR_SYSTEM_EXISTS(O_error_message   IN OUT  VARCHAR2,
                             O_exists          IN OUT  BOOLEAN,
                             I_system_code     IN      SA_ERROR_IMPACT.SYSTEM_CODE%TYPE,
                             I_error_code      IN      SA_ERROR_IMPACT.ERROR_CODE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: ERROR_CODE_EXISTS
-- Purpose      : This function will check to see if an error codes exists.
--
FUNCTION ERROR_CODE_EXISTS(O_error_message   IN OUT  VARCHAR2,
                           O_exists          IN OUT  BOOLEAN,
                           I_error_code      IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: CHECK_DELETE
-- Purpose      : This function will check to see if any dependencies exist before
--                deleting an error codes.
--
FUNCTION CHECK_DELETE(O_error_message   IN OUT  VARCHAR2,
                      O_delete          IN OUT  BOOLEAN,
                      I_error_code      IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: LOCK_ERROR_IMPACT
-- Purpose      : This function will lock any records from the sa_error_impact table
--                for a passed error code.
--
FUNCTION LOCK_ERROR_IMPACT(O_error_message  IN OUT  VARCHAR2,
                           I_error_code     IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: DELETE_ERROR_IMPACT
-- Purpose      : This function will delete any records from the sa_error_impact table
--                for a passed error code.
--
FUNCTION DELETE_ERROR_IMPACT(O_error_message   IN OUT  VARCHAR2,
                             I_error_code      IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: GET_ERROR_INFO
-- Purpose      : This function will return information for a passed error code, or for passed
--                transaction or total criteria.
--
FUNCTION GET_ERROR_INFO(O_error_message      IN OUT VARCHAR2,
                        O_error_desc         IN OUT SA_ERROR_CODES.ERROR_DESC%TYPE,
                        O_rec_solution       IN OUT SA_ERROR_CODES.REC_SOLUTION%TYPE,
                        O_orig_value         IN OUT SA_ERROR.ORIG_VALUE%TYPE,
                        O_store_override_ind IN OUT SA_ERROR_CODES.STORE_OVERRIDE_IND%TYPE,
                        O_hq_override_ind    IN OUT SA_ERROR_CODES.HQ_OVERRIDE_IND%TYPE,
                        O_target_form        IN OUT SA_ERROR_CODES.TARGET_FORM%TYPE,
                        O_target_tab         IN OUT SA_ERROR_CODES.TARGET_TAB%TYPE,
                        O_update_id          IN OUT SA_ERROR.UPDATE_ID%TYPE,
                        O_update_datetime    IN OUT SA_ERROR.UPDATE_DATETIME%TYPE,
                        O_orig_cc_no         IN OUT SA_ERROR.ORIG_CC_NO%TYPE,
                        IO_error_code        IN OUT SA_ERROR.ERROR_CODE%TYPE,
                        I_error_seq_no       IN     SA_ERROR.ERROR_SEQ_NO%TYPE,
                        I_tran_seq_no        IN     SA_ERROR.TRAN_SEQ_NO%TYPE,
                        I_total_seq_no       IN     SA_ERROR.TOTAL_SEQ_NO%TYPE,
                        I_key_value_1        IN     SA_ERROR.KEY_VALUE_1%TYPE,
                        I_key_value_2        IN     SA_ERROR.KEY_VALUE_2%TYPE,
                        I_rec_type           IN     SA_ERROR.REC_TYPE%TYPE,
                        I_store              IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                        I_day                IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name : GET_ERROR_DESC
-- Purpose       : This function will return the description for a passed error code.
--
FUNCTION GET_ERROR_DESC(O_error_message      IN OUT VARCHAR2,
                        O_error_desc         IN OUT SA_ERROR_CODES.ERROR_DESC%TYPE,
                        I_error_code         IN     SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name : COUNT_ERRORS
-- Purpose       : This function will retrieve the total number of errors that exist for the passed
--                 in transaction sequence number.
--
FUNCTION COUNT_ERRORS(O_error_message    IN OUT VARCHAR2,
                      O_no_of_errors     IN OUT NUMBER,
                      I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                      I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name : ERRORS_EXIST
-- Purpose       : This function will check for the existence of errors for the passed in search criteria.
--                 Only a transaction seq no or a total seq no is required, but the search can be narrowed
--                 by passing in additional input variables.
--
FUNCTION ERRORS_EXIST(O_error_message    IN OUT VARCHAR2,
                      O_exists           IN OUT BOOLEAN,
                      I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                      I_total_seq_no     IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                      I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_key_value_1      IN     SA_ERROR.KEY_VALUE_1%TYPE,
                      I_key_value_2      IN     SA_ERROR.KEY_VALUE_2%TYPE,
                      I_rec_type         IN     SA_ERROR.REC_TYPE%TYPE,
                      I_store_day_seq_no IN     SA_TOTAL.STORE_DAY_SEQ_NO%TYPE,
                      I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                      I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name : ERROR_EXISTS_ON_FORM
-- Purpose       : This function will check if an error exists based on the passed in criteria.
--                 This will check if there are errors for a specific transaction or a
--                 specific total type.
--
FUNCTION ERROR_EXISTS_ON_FORM(O_error_message    IN OUT VARCHAR2,
                              O_exists           IN OUT VARCHAR2,
                              I_store_day_seq_no IN     SA_TOTAL.STORE_DAY_SEQ_NO%TYPE,
                              I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_bal_group_seq_no IN     SA_ERROR.BAL_GROUP_SEQ_NO%TYPE,
                              I_total_type       IN     SA_TOTAL_HEAD.TOTAL_TYPE%TYPE,
                              I_rev_table        IN     VARCHAR2,
                              I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                              I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name : INSERT_ERROR
-- Purpose       : This function will insert a record into the error table (SA_ERROR) with the information
--                 passed into the function.  This function can only be used to insert transaction and
--                 total errors.
--
FUNCTION INSERT_ERROR(O_error_message    IN OUT VARCHAR2,
                      I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                      I_bal_group_seq_no IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                      I_total_seq_no     IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                      I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                      I_key_value_1      IN     SA_ERROR.KEY_VALUE_1%TYPE,
                      I_key_value_2      IN     SA_ERROR.KEY_VALUE_2%TYPE,
                      I_rec_type         IN     SA_ERROR.REC_TYPE%TYPE,
                      I_update_id        IN     SA_ERROR.UPDATE_ID%TYPE,
                      I_update_datetime  IN     SA_ERROR.UPDATE_DATETIME%TYPE,
                      I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                      I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function Name : DELETE_TRAN_DETAIL_ERRORS
-- Purpose       : This funtion deletes and errors from the errors table (SA_ERROR) that
--                 correspond to the passed transaction error input variables.  This function
--                 can only be used to delete transaction errors.
--
FUNCTION DELETE_ERRORS(O_error_message    IN OUT VARCHAR2,
                       I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                       I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                       I_key_value_1      IN     SA_ERROR.KEY_VALUE_1%TYPE,
                       I_key_value_2      IN     SA_ERROR.KEY_VALUE_2%TYPE,
                       I_rec_type         IN     SA_ERROR.REC_TYPE%TYPE,
                       I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                       I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name : INVALID_CHAR_SEQ
-- Purpose       : This funtion checks the Error Description and Recommended Solution fields
--                 corresponding to the passed Error Code to determine whether either field
--                 contains the character strings /* or */.
--
FUNCTION INVALID_CHAR_SEQ(O_error_message  IN OUT  VARCHAR2,
		          O_exists         IN OUT  BOOLEAN,
                          I_error_code     IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
FUNCTION UPDATE_STORE_OVERRIDE(O_error_message       IN OUT  VARCHAR2,
                               I_error_seq_no        IN      SA_ERROR.ERROR_SEQ_NO%TYPE,
                               I_store_override_ind  IN      SA_ERROR.STORE_OVERRIDE_IND%TYPE,
                               I_store               IN      SA_ERROR.STORE%TYPE DEFAULT NULL,
                               I_day                 IN      SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
FUNCTION UPDATE_HQ_OVERRIDE(O_error_message   IN OUT  VARCHAR2,
                            I_error_seq_no    IN      SA_ERROR.ERROR_SEQ_NO%TYPE,
                            I_HQ_override_ind IN      SA_ERROR.HQ_OVERRIDE_IND%TYPE,
                            I_store           IN      SA_ERROR.STORE%TYPE DEFAULT NULL,
                            I_day             IN      SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name : REV_ERRORS_EXIST
-- Purpose       : This function will check for the existence of errors for the passed in search criteria
--                 on the revision table.  Only a transaction seq no or a total seq no is required, but
--                 the search can be narrowed by passing in additional input variables.
--
FUNCTION REV_ERRORS_EXIST(O_error_message    IN OUT VARCHAR2,
                          O_exists           IN OUT BOOLEAN,
                          I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                          I_total_seq_no     IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                          I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_key_value_1      IN     SA_ERROR_REV.KEY_VALUE_1%TYPE,
                          I_key_value_2      IN     SA_ERROR_REV.KEY_VALUE_2%TYPE,
                          I_rec_type         IN     SA_ERROR_REV.REC_TYPE%TYPE,
                          I_store_day_seq_no IN     SA_TOTAL.STORE_DAY_SEQ_NO%TYPE,
                          I_rev_no           IN     SA_ERROR_REV.REV_NO%TYPE,
                          I_tran_rev_no      IN     SA_ERROR_REV.TRAN_REV_NO%TYPE,
                          I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                          I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name : DELETE_NONTRAN_ERROR
-- Purpose       : This funtion deletes errors from the errors table (SA_ERROR) that
--                 are not associated to a SA transaction record.  These errors are
--                 RECORDS_REJECTED and MISSING_TRAN_BIG_GAP.
--
FUNCTION DELETE_NONTRAN_ERROR(O_error_message    IN OUT VARCHAR2,
                              I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                              I_error_seq_no     IN     SA_ERROR.ERROR_SEQ_NO%TYPE,
                              I_store            IN     SA_ERROR.STORE%TYPE,
                              I_day              IN     SA_ERROR.DAY%TYPE,
                              I_rec_type         IN     SA_ERROR.REC_TYPE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END SA_ERROR_SQL;
/
