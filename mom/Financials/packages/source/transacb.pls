CREATE OR REPLACE PACKAGE BODY TRANSACTION_SQL AS
------------------------------------------------------------------------------------
FUNCTION DISCOUNTS_EXIST(O_error_message   IN OUT VARCHAR2,
                         O_discount_exists IN OUT BOOLEAN,
                         I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_item_seq_no     IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                         I_store           IN     SA_TRAN_HEAD.STORE%TYPE,
                         I_day             IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60)                := 'TRANSACTION_SQL.DISCOUNTS_EXIST';
   L_exist            VARCHAR2(1);
   L_store            SA_TRAN_HEAD.STORE%TYPE     := I_store;
   L_day              SA_TRAN_HEAD.DAY%TYPE       := I_day;

   cursor C_DISCOUNTS_EXIST is
      select 'x'
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and item_seq_no = I_item_seq_no
         and store       = L_store
         and day         = L_day;

BEGIN

   if  I_tran_seq_no is NOT NULL and I_item_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_discount_exists := FALSE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_DISCOUNTS_EXIST','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no)||', '||
                       'item_seq_no: '||to_char(I_item_seq_no));
      open C_DISCOUNTS_EXIST;
      SQL_LIB.SET_MARK('FETCH','C_DISCOUNTS_EXIST','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no)||', '||
                       'item_seq_no: '||to_char(I_item_seq_no));
      fetch C_DISCOUNTS_EXIST into L_exist;
      if C_DISCOUNTS_EXIST%FOUND then
         O_discount_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_DISCOUNTS_EXIST','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no)||', '||
                       'item_seq_no: '||to_char(I_item_seq_no));
      close C_DISCOUNTS_EXIST;
   else
      O_error_message := SQL_LIB.CREATE_MSG('TRAN_HEAD_ITEM_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DISCOUNTS_EXIST;
----------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_DISC_AMT(O_error_message   IN OUT VARCHAR2,
                            O_total_disc_amt  IN OUT NUMBER,
                            I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                            I_store           IN     SA_TRAN_HEAD.STORE%TYPE,
                            I_day             IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50)                   := 'TRANSACTION_SQL.GET_TOTAL_DISC_AMT';
   L_store            SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day              SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_GET_DISC_AMT is
      select SUM(nvl(unit_discount_amt,0) * nvl(uom_quantity,0))
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = I_store
         and day         = I_day;

BEGIN

   if I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISC_AMT','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no));
      open C_GET_DISC_AMT;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISC_AMT','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no));
      fetch C_GET_DISC_AMT into O_total_disc_amt;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISC_AMT','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no));
      close C_GET_DISC_AMT;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_DISC_AMT;
----------------------------------------------------------------------------------------
FUNCTION CREATE_REVISIONS(O_error_message    IN OUT VARCHAR2,
                          I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_rev_no           IN     SA_TRAN_HEAD.REV_NO%TYPE,
                          I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                          I_day              IN     SA_TRAN_HEAD.DAY%TYPE,
                          I_pm_mode          IN     VARCHAR2 DEFAULT 'EDIT')
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60)                      := 'TRANSACTION_SQL.CREATE_REVISIONS';
   L_err_rev_no    SA_ERROR_REV.REV_NO%TYPE          := 0;
   L_store         SA_TRAN_HEAD.STORE%TYPE           := I_store;
   L_day           SA_TRAN_HEAD.DAY%TYPE             := I_day;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_ERR_REV_NO is
      select nvl(max(rev_no),0) + 1
        from sa_error_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day;

   cursor C_LOCK_EXP is
      select 'Y'
        from sa_exported
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         for update nowait;

BEGIN

   if  I_tran_seq_no is NOT NULL and I_rev_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      --- create revision records on revision tables ---
      insert into sa_tran_head_rev(tran_seq_no,
                                   store,
                                   day,
                                   rev_no,
                                   store_day_seq_no,
                                   tran_datetime,
                                   register,
                                   tran_no,
                                   cashier,
                                   salesperson,
                                   tran_type,
                                   sub_tran_type,
                                   orig_tran_no,
                                   orig_tran_type,
                                   orig_reg_no,
                                   ref_no1,
                                   ref_no2,
                                   ref_no3,
                                   ref_no4,
                                   reason_code,
                                   vendor_no,
                                   vendor_invc_no,
                                   payment_ref_no,
                                   proof_of_delivery_no,
                                   status,
                                   value,
                                   pos_tran_ind,
                                   update_id,
                                   update_datetime,
                                   error_ind,
                                   banner_no,
                                   rounded_amt,
                                   rounded_off_amt,
                                   credit_promotion_id,
                                   ref_no25,
                                   ref_no26,
                                   ref_no27,
                                   rtlog_orig_sys,
                                   tran_process_sys)
                            select tran_seq_no,
                                   store,
                                   day,
                                   I_rev_no,
                                   store_day_seq_no,
                                   tran_datetime,
                                   register,
                                   tran_no,
                                   cashier,
                                   salesperson,
                                   tran_type,
                                   sub_tran_type,
                                   orig_tran_no,
                                   orig_tran_type,
                                   orig_reg_no,
                                   ref_no1,
                                   ref_no2,
                                   ref_no3,
                                   ref_no4,
                                   reason_code,
                                   vendor_no,
                                   vendor_invc_no,
                                   payment_ref_no,
                                   proof_of_delivery_no,
                                   status,
                                   value,
                                   pos_tran_ind,
                                   update_id,
                                   update_datetime,
                                   error_ind,
                                   banner_no,
                                   rounded_amt,
                                   rounded_off_amt,
                                   credit_promotion_id,
                                   ref_no25,
                                   ref_no26,
                                   ref_no27,
                                   rtlog_orig_sys,
                                   tran_process_sys
                              from sa_tran_head
                             where tran_seq_no = I_tran_seq_no
                              and  store       = L_store
                              and  day         = L_day;
      ---
      insert into sa_tran_item_rev(tran_seq_no,
                                   store,
                                   day,
                                   item_seq_no,
                                   rev_no,
                                   item_status,
                                   item_type,
                                   item,
                                   ref_item,
                                   non_merch_item,
                                   voucher_no,
                                   dept,
                                   class,
                                   subclass,
                                   qty,
                                   unit_retail,
                                   unit_retail_vat_incl,
                                   selling_uom,
                                   standard_qty,
                                   standard_unit_retail,
                                   standard_uom,
                                   override_reason,
                                   orig_unit_retail,
                                   standard_orig_unit_retail,
                                   tax_ind,
                                   ref_no5,
                                   ref_no6,
                                   ref_no7,
                                   ref_no8,
                                   item_swiped_ind,
                                   error_ind,
                                   drop_ship_ind,
                                   waste_type,
                                   waste_pct,
                                   pump,
                                   return_reason_code,
                                   salesperson,
                                   uom_quantity,
                                   catchweight_ind,
                                   selling_item,
                                   customer_order_line_no,
                                   media_id,
                                   total_igtax_amt,
                                   unique_id,
                                   cust_order_no,
                                   cust_order_date,
                                   fulfill_order_no,
                                   no_inv_ret_ind,
                                   return_wh,
                                   sales_type,
                                   return_disposition,
                                   expiration_date)
                                  --EMD_RMS11_INS_TCD16--
                            select tran_seq_no,
                                   store,
                                   day,
                                   item_seq_no,
                                   I_rev_no,
                                   item_status,
                                   item_type,
                                   item,
                                   ref_item,
                                   non_merch_item,
                                   voucher_no,
                                   dept,
                                   class,
                                   subclass,
                                   qty,
                                   unit_retail,
                                   unit_retail_vat_incl,
                                   selling_uom,
                                   standard_qty,
                                   standard_unit_retail,
                                   standard_uom,
                                   override_reason,
                                   orig_unit_retail,
                                   standard_orig_unit_retail,
                                   tax_ind,
                                   ref_no5,
                                   ref_no6,
                                   ref_no7,
                                   ref_no8,
                                   item_swiped_ind,
                                   error_ind,
                                   drop_ship_ind,
                                   waste_type,
                                   waste_pct,
                                   pump,
                                   return_reason_code,
                                   salesperson,
                                   uom_quantity,
                                   catchweight_ind,
                                   selling_item,
                                   customer_order_line_no,
                                   media_id,
                                   total_igtax_amt,
                                   unique_id,
                                   cust_order_no,
                                   cust_order_date,
                                   fulfill_order_no,
                                   no_inv_ret_ind,
                                   return_wh,
                                   sales_type,
                                   return_disposition,
                                   expiration_date
                                  --EMD_RMS11_INS_TCD16--
                              from sa_tran_item
                             where tran_seq_no = I_tran_seq_no
                              and  store       = L_store
                              and  day         = L_day;
      ---
      insert into sa_tran_tender_rev(tran_seq_no,
                                     store,
                                     day,
                                     tender_seq_no,
                                     rev_no,
                                     tender_type_group,
                                     tender_type_id,
                                     tender_amt,
                                     cc_no,
                                     cc_exp_date,
                                     cc_auth_no,
                                     cc_auth_src,
                                     cc_entry_mode,
                                     cc_cardholder_verf,
                                     cc_term_id,
                                     cc_spec_cond,
                                     voucher_no,
                                     coupon_no,
                                     coupon_ref_no,
                                     check_acct_no,
                                     check_no,
                                     identi_method,
                                     identi_id,
                                     orig_currency,
                                     orig_curr_amt,
                                     ref_no9,
                                     ref_no10,
                                     ref_no11,
                                     ref_no12,
                                     error_ind)
                              select tran_seq_no,
                                     store,
                                     day,
                                     tender_seq_no,
                                     I_rev_no,
                                     tender_type_group,
                                     tender_type_id,
                                     tender_amt,
                                     cc_no,
                                     cc_exp_date,
                                     cc_auth_no,
                                     cc_auth_src,
                                     cc_entry_mode,
                                     cc_cardholder_verf,
                                     cc_term_id,
                                     cc_spec_cond,
                                     voucher_no,
                                     coupon_no,
                                     coupon_ref_no,
                                     check_acct_no,
                                     check_no,
                                     identi_method,
                                     identi_id,
                                     orig_currency,
                                     orig_curr_amt,
                                     ref_no9,
                                     ref_no10,
                                     ref_no11,
                                     ref_no12,
                                     error_ind
                                from sa_tran_tender
                               where tran_seq_no = I_tran_seq_no
                                 and  store      = L_store
                                 and  day        = L_day;
      ---
      insert into sa_tran_tax_rev(tran_seq_no,
                                  store,
                                  day,
                                  tax_seq_no,
                                  tax_code,
                                  rev_no,
                                  tax_amt,
                                  error_ind,
                                  ref_no17,
                                  ref_no18,
                                  ref_no19,
                                  ref_no20)
                           select tran_seq_no,
                                  store,
                                  day,
                                  tax_seq_no,
                                  tax_code,
                                  I_rev_no,
                                  tax_amt,
                                  error_ind,
                                  ref_no17,
                                  ref_no18,
                                  ref_no19,
                                  ref_no20
                             from sa_tran_tax
                            where tran_seq_no = I_tran_seq_no
                              and  store      = L_store
                              and  day        = L_day;
      ---
      insert into sa_tran_disc_rev(tran_seq_no,
                                   store,
                                   day,
                                   item_seq_no,
                                   discount_seq_no,
                                   rms_promo_type,
                                   rev_no,
                                   promotion,
                                   disc_type,
                                   coupon_no,
                                   coupon_ref_no,
                                   qty,
                                   unit_discount_amt,
                                   standard_qty,
                                   standard_unit_disc_amt,
                                   ref_no13,
                                   ref_no14,
                                   ref_no15,
                                   ref_no16,
                                   error_ind,
                                   uom_quantity,
                                   catchweight_ind,
                                   promo_comp)
                                  --EMD_RMS11_INS_TCD16--
                            select tran_seq_no,
                                   store,
                                   day,
                                   item_seq_no,
                                   discount_seq_no,
                                   rms_promo_type,
                                   I_rev_no,
                                   promotion,
                                   disc_type,
                                   coupon_no,
                                   coupon_ref_no,
                                   qty,
                                   unit_discount_amt,
                                   standard_qty,
                                   standard_unit_disc_amt,
                                   ref_no13,
                                   ref_no14,
                                   ref_no15,
                                   ref_no16,
                                   error_ind,
                                   uom_quantity,
                                   catchweight_ind,
                                   promo_comp
                                  --EMD_RMS11_INS_TCD16--
                              from sa_tran_disc
                             where tran_seq_no = I_tran_seq_no
                              and  store       = L_store
                              and  day         = L_day;
      ---
      insert into sa_tran_igtax_rev(store,
                                    day,
                                    tran_seq_no,
                                    item_seq_no,
                                    igtax_seq_no,
                                    rev_no,
                                    tax_authority,
                                    igtax_code,
                                    igtax_rate,
                                    total_igtax_amt,
                                    standard_qty,
                                    standard_unit_igtax_amt,
                                    error_ind,
                                    ref_no21,
                                    ref_no22,
                                    ref_no23,
                                    ref_no24)
                             select store,
                                    day,
                                    tran_seq_no,
                                    item_seq_no,
                                    igtax_seq_no,
                                    I_rev_no,
                                    tax_authority,
                                    igtax_code,
                                    igtax_rate,
                                    total_igtax_amt,
                                    standard_qty,
                                    standard_unit_igtax_amt,
                                    error_ind,
                                    ref_no21,
                                    ref_no22,
                                    ref_no23,
                                    ref_no24
                               from sa_tran_igtax
                              where tran_seq_no = I_tran_seq_no
                                and store       = L_store
                                and day         = L_day;
      ---
      insert into sa_tran_payment_rev(store,
                                      day,
                                      tran_seq_no,
                                      payment_seq_no,
                                      rev_no,
                                      payment_amt,
                                      error_ind)
                               select store,
                                      day,
                                      tran_seq_no,
                                      payment_seq_no,
                                      I_rev_no,
                                      payment_amt,
                                      error_ind
                                 from sa_tran_payment
                                where tran_seq_no = I_tran_seq_no
                                  and store       = L_store
                                  and day         = L_day;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_ERR_REV_NO','SA_ERROR_REV','TRAN SEQ: '||to_char(I_tran_seq_no));
      open C_GET_ERR_REV_NO;
      SQL_LIB.SET_MARK('FETCH','C_GET_ERR_REV_NO','SA_ERROR_REV','TRAN SEQ: '||to_char(I_tran_seq_no));
      fetch C_GET_ERR_REV_NO into L_err_rev_no;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ERR_REV_NO','SA_ERROR_REV','TRAN SEQ: '||to_char(I_tran_seq_no));
      close C_GET_ERR_REV_NO;
      ---
      insert into sa_error_rev(error_seq_no,
                               rev_no,
                               store_day_seq_no,
                               bal_group_seq_no,
                               total_seq_no,
                               value_rev_no,
                               tran_seq_no,
                               store,
                               day,
                               tran_rev_no,
                               error_code,
                               key_value_1,
                               key_value_2,
                               rec_type,
                               store_override_ind,
                               hq_override_ind,
                               update_id,
                               update_datetime,
                               orig_value,
                               orig_cc_no)
                        select error_seq_no,
                               L_err_rev_no + rownum,
                               store_day_seq_no,
                               bal_group_seq_no,
                               total_seq_no,
                               NULL,
                               tran_seq_no,
                               store,
                               day,
                               I_rev_no, --tran_rev_no,
                               error_code,
                               key_value_1,
                               key_value_2,
                               rec_type,
                               store_override_ind,
                               hq_override_ind,
                               update_id,
                               update_datetime,
                               orig_value,
                               orig_cc_no
                          from sa_error
                         where tran_seq_no = I_tran_seq_no
                              and  store   = L_store
                              and  day     = L_day;
      ---
      insert into sa_exported_rev(export_seq_no,
                                  rev_no,
                                  store_day_seq_no,
                                  tran_seq_no,
                                  store,
                                  day,
                                  total_seq_no,
                                  acct_period,
                                  system_code,
                                  exp_datetime,
                                  status)
                           select export_seq_no,
                                  I_rev_no,
                                  store_day_seq_no,
                                  tran_seq_no,
                                  store,
                                  day,
                                  total_seq_no,
                                  acct_period,
                                  system_code,
                                  exp_datetime,
                                  status
                             from sa_exported
                            where tran_seq_no = I_tran_seq_no
                              and store       = L_store
                              and day         = L_day;
      ---
      open C_LOCK_EXP;
      close C_LOCK_EXP;
      ---
      if I_pm_mode = 'EDIT' then
         delete from sa_exported
               where tran_seq_no = I_tran_seq_no
                 and store       = L_store
                 and day         = L_day;
      end if;

   ---
   else
      O_error_message := SQL_LIB.CREATE_MSG('TRAN_HEAD_REV_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'SA_EXPORTED',
                                            to_char(I_tran_seq_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_REVISIONS;
----------------------------------------------------------------------------------------
FUNCTION POST_VOID(O_error_message     IN OUT VARCHAR2,
                   I_orig_tran_no      IN     SA_TRAN_HEAD.TRAN_NO%TYPE,
                   I_orig_tran_seq_no  IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                   I_orig_tran_type    IN     SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                   I_update_datetime   IN     SA_TRAN_HEAD.UPDATE_DATETIME%TYPE,
                   I_update_id         IN     SA_TRAN_HEAD.UPDATE_ID%TYPE,
                   I_store             IN     SA_TRAN_HEAD.STORE%TYPE,
                   I_day               IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(60)                   := 'TRANSACTION_SQL.POST_VOID';
   L_next_tran_seq_no      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_store                 SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day                   SA_TRAN_HEAD.DAY%TYPE          := I_day;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SA_ERROR is
      select 'Y'
        from sa_error
       where tran_seq_no = I_orig_tran_seq_no
         and store = L_store
         and day = L_day
         for update nowait;


BEGIN

   if I_orig_tran_no       is NOT NULL and I_orig_tran_seq_no  is NOT NULL
    and I_update_datetime  is NOT NULL and I_update_id         is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_orig_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if SA_SEQUENCE2_SQL.GET_TRAN_SEQ(O_error_message,
                                       L_next_tran_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      insert into sa_tran_head(tran_seq_no,
                               store,
                               day,
                               rev_no,
                               store_day_seq_no,
                               tran_datetime,
                               register,
                               tran_no,
                               cashier,
                               salesperson,
                               tran_type,
                               sub_tran_type,
                               orig_tran_no,
                               orig_tran_type,
                               orig_reg_no,
                               ref_no1,
                               ref_no2,
                               ref_no3,
                               ref_no4,
                               reason_code,
                               value,
                               status,
                               pos_tran_ind,
                               update_id,
                               update_datetime,
                               error_ind,
                               banner_no,
                               rtlog_orig_sys,
                               tran_process_sys)
                        select L_next_tran_seq_no,
                               store,
                               day,
                               1,
                               store_day_seq_no,
                               sysdate,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               'PVOID',
                               NULL,    -- sub_tran_type
                               I_orig_tran_no,
                               I_orig_tran_type,
                               register,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,    -- reason_code
                               NULL,
                               'P',
                               'N',
                               I_update_id,
                               I_update_datetime,
                               'N',
                               banner_no,
                               rtlog_orig_sys,
                               tran_process_sys
                          from sa_tran_head
                         where tran_seq_no = I_orig_tran_seq_no
                           and store       = L_store
                           and day         = L_day;

-- Override any existing errors for this transaction
      open C_LOCK_SA_ERROR;
      close C_LOCK_SA_ERROR;

      update sa_error
         set store_override_ind = 'N',
             hq_override_ind = 'Y',
             update_id = I_update_id,
             update_datetime = sysdate
       where tran_seq_no = I_orig_tran_seq_no
         and store = L_store
         and day = L_day;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'SA_ERROR',
                                            to_char(I_orig_tran_seq_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POST_VOID;
--------------------------------------------------------------------------------------
FUNCTION GET_TRAN_NO_TYPE(O_error_message      IN OUT VARCHAR2,
                          O_tran_no            IN OUT SA_TRAN_HEAD.TRAN_NO%TYPE,
                          O_tran_type          IN OUT SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                          O_tran_type_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                          O_sub_tran_type      IN OUT SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                          O_sub_tran_type_desc IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                          O_reason_code        IN OUT SA_TRAN_HEAD.REASON_CODE%TYPE,
                          O_reason_code_desc   IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                          I_tran_seq_no        IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_store              IN     SA_TRAN_HEAD.STORE%TYPE,
                          I_day                IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(60)                   := 'TRANSACTION_SQL.GET_TRAN_NO_TYPE';
   L_dummy      VARCHAR2(60)                   := NULL;
   L_store      SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day        SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_TRANS is
      select tran_no,
             tran_type,
             sub_tran_type,
             reason_code
        from sa_tran_head
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day;

BEGIN

   if I_tran_seq_no is NULL then
      O_error_message := sql_lib.create_msg('REQUIRED_INPUT_IS_NULL','I_tran_seq_no',L_program,NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_TRANS','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
   open C_TRANS;
   SQL_LIB.SET_MARK('FETCH','C_TRANS','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
   fetch C_TRANS into O_tran_no,
                      O_tran_type,
                      O_sub_tran_type,
                      O_reason_code;
   SQL_LIB.SET_MARK('CLOSE','C_TRANS','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
   close C_TRANS;
   ---
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'TRAT',
                                 O_tran_type,
                                 O_tran_type_desc) = FALSE then
      return FALSE;
   end if;
   ---
   if O_sub_tran_type is NOT NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'TRAS',
                                    O_sub_tran_type,
                                    O_sub_tran_type_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if O_reason_code is NOT NULL then
      ---
      if O_sub_tran_type in  ('MV', 'EV') then
         ---
       if INVC_ATTRIB_SQL.GET_NMERCH_CODE_INFO(O_error_message,
                                               O_reason_code_desc,
                                               L_dummy,
                                               O_reason_code) = FALSE then
            return FALSE;
         end if;
         ---
      else
         ---
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'REAC',
                                       O_reason_code,
                                       O_reason_code_desc) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_TRAN_NO_TYPE;
----------------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message    IN OUT VARCHAR2,
                  O_exists           IN OUT BOOLEAN,
                  O_tran_datetime    IN OUT SA_TRAN_HEAD.TRAN_DATETIME%TYPE,
                  O_tran_no          IN OUT SA_TRAN_HEAD.TRAN_NO%TYPE,
                  O_ext_tran_sys     IN OUT SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                  O_register         IN OUT SA_TRAN_HEAD.REGISTER%TYPE,
                  O_cashier          IN OUT SA_TRAN_HEAD.CASHIER%TYPE,
                  O_tran_type        IN OUT SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                  O_sub_tran_type    IN OUT SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                  O_reason_code      IN OUT SA_TRAN_HEAD.REASON_CODE%TYPE,
                  O_status           IN OUT SA_TRAN_HEAD.STATUS%TYPE,
                  O_store_day_seq_no IN OUT SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                  O_store            IN OUT STORE.STORE%TYPE,
                  O_store_name       IN OUT STORE.STORE_NAME%TYPE,
                  O_chain            IN OUT CHAIN.CHAIN%TYPE,
                  O_chain_name       IN OUT CHAIN.CHAIN_NAME%TYPE,
                  I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                  I_rev_ind          IN     VARCHAR2,
                  I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                  I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(60)                   := 'TRANSACTION_SQL.GET_INFO';
   L_store                 SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day                   SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_GET_INFO is
     select tran_datetime,
            tran_no,
            rtlog_orig_sys,
            register,
            cashier,
            tran_type,
            sub_tran_type,
            reason_code,
            status,
            store_day_seq_no
       from sa_tran_head
      where tran_seq_no = I_tran_seq_no
       and  store       = L_store
       and  day         = L_day;

   cursor C_GET_INFO_REV is
     select tran_datetime,
            tran_no,
            rtlog_orig_sys,
            register,
            cashier,
            tran_type,
            sub_tran_type,
            reason_code,
            status,
            store_day_seq_no
       from sa_tran_head_rev
      where tran_seq_no = I_tran_seq_no
        and  store       = L_store
        and  day         = L_day;

BEGIN

   if I_tran_seq_no is NULL then
      O_error_message := sql_lib.create_msg('REQUIRED_INPUT_IS_NULL','I_tran_seq_no',L_program,NULL);
      return FALSE;
   end if;
   ---
   if (I_rev_ind is NULL) or (I_rev_ind not in ('N', 'Y')) then
      O_error_message := sql_lib.create_msg('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_rev_ind = 'N' then
      SQL_LIB.SET_MARK('OPEN','C_GET_INFO','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
      open C_GET_INFO;
      SQL_LIB.SET_MARK('FETCH','C_GET_INFO','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
      fetch C_GET_INFO into O_tran_datetime,
                            O_tran_no,
                            O_ext_tran_sys,
                            O_register,
                            O_cashier,
                            O_tran_type,
                            O_sub_tran_type,
                            O_reason_code,
                            O_status,
                            O_store_day_seq_no;
      if C_GET_INFO%NOTFOUND then
         O_exists := FALSE;
         close C_GET_INFO;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_INFO','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
      close C_GET_INFO;
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_INFO_REV','SA_TRAN_HEAD_REV','tran_seq_no: '||to_char(I_tran_seq_no));
      open C_GET_INFO_REV;
      SQL_LIB.SET_MARK('FETCH','C_GET_INFO_REV','SA_TRAN_HEAD_REV','tran_seq_no: '||to_char(I_tran_seq_no));
      fetch C_GET_INFO_REV into O_tran_datetime,
                                O_tran_no,
                                O_ext_tran_sys,
                                O_register,
                                O_cashier,
                                O_tran_type,
                                O_sub_tran_type,
                                O_reason_code,
                                O_status,
                                O_store_day_seq_no;
      if C_GET_INFO_REV%NOTFOUND then
         O_exists := FALSE;
         close C_GET_INFO_REV;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_INFO_REV','SA_TRAN_HEAD_REV','tran_seq_no: '||to_char(I_tran_seq_no));
      close C_GET_INFO_REV;
   end if;
   ---
   if STORE_DAY_SQL.GET_LOCATION_INFO(O_error_message,
                                      O_store,
                                      O_store_name,
                                      O_chain,
                                      O_chain_name,
                                      O_store_day_seq_no) = FALSE then
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_INFO;
---------------------------------------------------------------------------------------------------
FUNCTION GET_DISC_QTY(O_error_message      IN OUT   VARCHAR2,
                      O_discount_qty       IN OUT   SA_TRAN_ITEM.QTY%TYPE,
                      O_discount_uom_qty   IN OUT   SA_TRAN_ITEM.UOM_QUANTITY%TYPE,
                      I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_item_seq_no        IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                      I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                      I_day                IN       SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)                                 := 'TRANSACTION_SQL.GET_DISC_QTY';
   L_store                 SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day                   SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_GET_QTY is
      select NVL(MAX(qty),0)          discount_qty,
             NVL(MAX(uom_quantity),0) discount_uom_qty
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no;

BEGIN

   if I_tran_seq_no is NOT NULL and
      I_item_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_INFO','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no)||', '||
                                                          'item_seq_no: '||to_char(I_item_seq_no));
      open C_GET_QTY;
      SQL_LIB.SET_MARK('FETCH','C_GET_INFO','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no)||', '||
                                                          'item_seq_no: '||to_char(I_item_seq_no));
      fetch C_GET_QTY into O_discount_qty,
                           O_discount_uom_qty;
      SQL_LIB.SET_MARK('CLOSE','C_GET_INFO','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no)||', '||
                                                          'item_seq_no: '||to_char(I_item_seq_no));
      close C_GET_QTY;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END GET_DISC_QTY;
------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TRAN(O_error_message    IN OUT VARCHAR2,
                       O_exists           IN OUT BOOLEAN,
                       O_next_tran_seq_no IN OUT SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                       I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                       I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50)                   := 'TRANSACTION_SQL.GET_NEXT_TRAN';
   L_store                 SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day                   SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_GET_NEXT_TRAN is
      select MIN(tran_seq_no)
        from sa_tran_head
       where store_day_seq_no = I_store_day_seq_no
         and tran_seq_no > I_tran_seq_no
         and store = L_store
         and day = L_day
         and tran_type != 'TERM';

BEGIN

   if I_store_day_seq_no is NOT NULL and
      I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_exists := FALSE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_NEXT_TRAN','SA_TRAN_HEAD','store_day_seq_no: '||to_char(I_store_day_seq_no));
      open C_GET_NEXT_TRAN;
      SQL_LIB.SET_MARK('FETCH','C_GET_NEXT_TRAN','SA_TRAN_HEAD','store_day_seq_no: '||to_char(I_store_day_seq_no));
      fetch C_GET_NEXT_TRAN into O_next_tran_seq_no;
      SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT_TRAN','SA_TRAN_HEAD','store_day_seq_no: '||to_char(I_store_day_seq_no));
      close C_GET_NEXT_TRAN;
      ---
      if O_next_tran_seq_no is NOT NULL then
         O_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END GET_NEXT_TRAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_PREV_TRAN(O_error_message    IN OUT VARCHAR2,
                       O_exists           IN OUT BOOLEAN,
                       O_prev_tran_seq_no IN OUT SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                       I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                       I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50)                   := 'TRANSACTION_SQL.GET_PREV_TRAN';
   L_store                 SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day                   SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_GET_PREV_TRAN is
      select MAX(tran_seq_no)
        from sa_tran_head
       where store_day_seq_no = I_store_day_seq_no
         and tran_seq_no < I_tran_seq_no
         and store = L_store
         and day = L_day
         and tran_type != 'TERM';

BEGIN

   if I_store_day_seq_no is NOT NULL and
      I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_exists := FALSE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_PREV_TRAN','SA_TRAN_HEAD','store_day_seq_no: '||to_char(I_store_day_seq_no));
      open C_GET_PREV_TRAN;
      SQL_LIB.SET_MARK('FETCH','C_GET_PREV_TRAN','SA_TRAN_HEAD','store_day_seq_no: '||to_char(I_store_day_seq_no));
      fetch C_GET_PREV_TRAN into O_prev_tran_seq_no;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PREV_TRAN','SA_TRAN_HEAD','store_day_seq_no: '||to_char(I_store_day_seq_no));
      close C_GET_PREV_TRAN;
      ---
      if O_prev_tran_seq_no is NOT NULL then
         O_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END GET_PREV_TRAN;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_TRAN_DETAILS(O_error_message IN OUT VARCHAR2,
                             I_tran_seq_no   IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_tran_type     IN     SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                             I_store         IN     SA_TRAN_HEAD.STORE%TYPE,
                             I_day           IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)                   := 'TRANSACTION_SQL.DELETE_TRAN_DETAILS';
   L_table         VARCHAR2(30);
   L_store         SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day           SA_TRAN_HEAD.DAY%TYPE          := I_day;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_DISC is
      select 'x'
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
      for update nowait;

   cursor C_LOCK_ERROR_DISC is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rec_type = 'IDISC'
      for update nowait;

   cursor C_LOCK_IGTAX is
      select 'x'
        from sa_tran_igtax
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
      for update nowait;

   cursor C_LOCK_ERROR_IGTAX is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rec_type = 'IGTAX'
      for update nowait;

   cursor C_LOCK_ITEM is
      select 'x'
        from sa_tran_item
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
      for update nowait;

   cursor C_LOCK_ERROR_ITEM is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rec_type = 'TITEM'
      for update nowait;

   cursor C_LOCK_TENDER is
      select 'x'
        from sa_tran_tender
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
      for update nowait;

   cursor C_LOCK_ERROR_TENDER is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rec_type = 'TTEND'
      for update nowait;

   cursor C_LOCK_PAYMENT is
      select 'x'
        from sa_tran_payment
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
      for update nowait;

   cursor C_LOCK_ERROR_PAYMENT is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rec_type = 'TPYMT'
      for update nowait;

   cursor C_LOCK_TAX is
      select 'x'
        from sa_tran_tax
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
      for update nowait;

   cursor C_LOCK_ERROR_TAX is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rec_type = 'TTAX'
      for update nowait;

   cursor C_LOCK_CUST_ATTRIB is
      select 'x'
        from sa_cust_attrib
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
      for update nowait;

   cursor C_LOCK_ERROR_CUST_ATTRIB is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rec_type in ('CATT','CATYPE','CAVALU')
      for update nowait;

   cursor C_LOCK_CUSTOMER is
      select 'x'
        from sa_customer
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
      for update nowait;

   cursor C_LOCK_ERROR_CUSTOMER is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rec_type in ('TCUST','TCTYPE','TCUSTA')
        for update nowait;

BEGIN

   if I_tran_seq_no  is NOT NULL and
      I_tran_type    is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_tran_type NOT in ('VOID','SALE','CANCEL','RETURN','METER',
                                        'PUMPT','TANKDP','TERM','EEXCH') then
         -- delete all item and discount records
         L_table := 'sa_tran_disc';
         open C_LOCK_DISC;
         close C_LOCK_DISC;
         ---
         delete from sa_tran_disc
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day;
         ---
         L_table := 'sa_error';
         open C_LOCK_ERROR_DISC;
         close C_LOCK_ERROR_DISC;
         ---
         delete from sa_error
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day
            and rec_type = 'IDISC';
         ---
         L_table := 'sa_tran_igtax';
         open C_LOCK_IGTAX;
         close C_LOCK_IGTAX;
         ---
         delete from sa_tran_igtax
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day;
         ---
         L_table := 'sa_error';
         open C_LOCK_ERROR_IGTAX;
         close C_LOCK_ERROR_IGTAX;
         ---
         delete from sa_error
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day
            and rec_type    = 'IGTAX';

         ---
         L_table := 'sa_tran_item';
         open C_LOCK_ITEM;
         close C_LOCK_ITEM;
         ---
         delete from sa_tran_item
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day;
         ---
         L_table := 'sa_error';
         open C_LOCK_ERROR_ITEM;
         close C_LOCK_ERROR_ITEM;
         ---
         delete from sa_error
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day
            and rec_type = 'TITEM';
      end if;
      ---
      if I_tran_type NOT in ('SALE','CANCEL','RETURN','PAIDIN','PAIDOU',
                                        'PULL','LOAN','REFUND','TERM') then
         -- delete all tender records
         L_table := 'sa_tran_tender';
         open C_LOCK_TENDER;
         close C_LOCK_TENDER;
         ---
         delete from sa_tran_tender
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day;
         ---
         L_table := 'sa_error';
         open C_LOCK_ERROR_TENDER;
         close C_LOCK_ERROR_TENDER;
         ---
         delete from sa_error
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day
            and rec_type = 'TTEND';
            ---
         L_table := 'sa_tran_payment';
         open C_LOCK_PAYMENT;
         close C_LOCK_PAYMENT;
         ---
         delete from sa_tran_payment
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day;
         ---
         L_table := 'sa_error';
         open C_LOCK_ERROR_PAYMENT;
         close C_LOCK_ERROR_PAYMENT;
         ---
         delete from sa_error
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day
            and rec_type = 'TPYMT';
      end if;
      ---
      if I_tran_type NOT in ('SALE','CANCEL','RETURN','TERM','EEXCH') then
         -- delete all tax and customer records
         L_table := 'sa_tran_tax';
         open C_LOCK_TAX;
         close C_LOCK_TAX;
         ---
         delete from sa_tran_tax
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day;
         ---
         L_table := 'sa_error';
         open C_LOCK_ERROR_TAX;
         close C_LOCK_ERROR_TAX;
         ---
         delete from sa_error
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day
            and rec_type = 'TTAX';
         ---
         L_table := 'sa_cust_attrib';
         open C_LOCK_CUST_ATTRIB;
         close C_LOCK_CUST_ATTRIB;
         ---
         delete from sa_cust_attrib
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day;
         ---
         L_table := 'sa_error';
         open C_LOCK_ERROR_CUST_ATTRIB;
         close C_LOCK_ERROR_CUST_ATTRIB;
         ---
         delete from sa_error
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day
            and rec_type in ('CATT','CATYPE','CAVALU');
         ---
         L_table := 'sa_customer';
         open C_LOCK_CUSTOMER;
         close C_LOCK_CUSTOMER;
         ---
         delete from sa_customer
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day;
         ---
         L_table := 'sa_error';
         open C_LOCK_ERROR_CUSTOMER;
         close C_LOCK_ERROR_CUSTOMER;
         ---
         delete from sa_error
          where tran_seq_no = I_tran_seq_no
            and store       = L_store
            and day         = L_day
            and rec_type in ('TCUST','TCTYPE','TCUSTA');
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_tran_seq_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END DELETE_TRAN_DETAILS;
----------------------------------------------------------------------------------------------------
FUNCTION DELETE_REVISIONS(O_error_message    IN OUT VARCHAR2,
                          I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_rev_no           IN     SA_TRAN_HEAD.REV_NO%TYPE,
                          I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                          I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)                   := 'TRANSACTION_SQL.DELETE_REVISIONS';
   L_table         VARCHAR2(30);
   L_store         SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day           SA_TRAN_HEAD.DAY%TYPE          := I_day;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_TRAN_HEAD_REV is
      select 'x'
        from sa_tran_head_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no
      for update nowait;

   cursor C_LOCK_TRAN_ITEM_REV is
      select 'x'
        from sa_tran_item_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no
      for update nowait;

   cursor C_LOCK_TRAN_IGTAX_REV is
      select 'x'
        from sa_tran_igtax_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no
      for update nowait;

   cursor C_LOCK_TRAN_DISC_REV is
      select 'x'
        from sa_tran_disc_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no
      for update nowait;

   cursor C_LOCK_TRAN_TENDER_REV is
      select 'x'
        from sa_tran_tender_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no
      for update nowait;

   cursor C_LOCK_TRAN_PAYMENT_REV is
      select 'x'
        from sa_tran_payment_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no
      for update nowait;

   cursor C_LOCK_TRAN_TAX_REV is
      select 'x'
        from sa_tran_tax_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no
      for update nowait;

   cursor C_LOCK_ERROR_REV is
      select 'x'
        from sa_error_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and tran_rev_no = I_rev_no
      for update nowait;

   cursor C_LOCK_EXPORTED_REV is
      select 'x'
        from sa_exported_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no
      for update nowait;

BEGIN

   if I_tran_seq_no  is NOT NULL and
      I_rev_no       is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      L_table := 'sa_error_rev';
      open  C_LOCK_ERROR_REV;
      close C_LOCK_ERROR_REV;
      ---
      delete from sa_error_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and tran_rev_no = I_rev_no;
      ---
      L_table := 'sa_tran_tax_rev';
      open  C_LOCK_TRAN_TAX_REV;
      close C_LOCK_TRAN_TAX_REV;
      ---
      delete from sa_tran_tax_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no;
      ---
      L_table := 'sa_tran_tender_rev';
      open  C_LOCK_TRAN_TENDER_REV;
      close C_LOCK_TRAN_TENDER_REV;
      ---
      delete from sa_tran_tender_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no;
      ---
      L_table := 'sa_tran_disc_rev';
      open  C_LOCK_TRAN_DISC_REV;
      close C_LOCK_TRAN_DISC_REV;
      ---
      delete from sa_tran_disc_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no;
      ---
      L_table := 'sa_tran_payment_rev';
      open  C_LOCK_TRAN_PAYMENT_REV;
      close C_LOCK_TRAN_PAYMENT_REV;
      ---
      delete from sa_tran_payment_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no;
      ---
      L_table := 'sa_tran_igtax_rev';
      open  C_LOCK_TRAN_IGTAX_REV;
      close C_LOCK_TRAN_IGTAX_REV;
      ---
      delete from sa_tran_igtax_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no;
      ---
      L_table := 'sa_tran_item_rev';
      open  C_LOCK_TRAN_ITEM_REV;
      close C_LOCK_TRAN_ITEM_REV;
      ---
      delete from sa_tran_item_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no;
      ---
      L_table := 'sa_tran_tax_rev';
      open  C_LOCK_EXPORTED_REV;
      close C_LOCK_EXPORTED_REV;
      ---
      delete from sa_exported_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no;
      ---
      L_table := 'sa_tran_head_rev';
      open  C_LOCK_TRAN_HEAD_REV;
      close C_LOCK_TRAN_HEAD_REV;
      ---
      delete from sa_tran_head_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = I_rev_no;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_tran_seq_no),
                                            to_char(I_rev_no));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END DELETE_REVISIONS;
----------------------------------------------------------------------------------------------------
FUNCTION REVISIONS_EXIST(O_error_message   IN OUT VARCHAR2,
                         O_revs_exist      IN OUT BOOLEAN,
                         I_rev_no          IN     SA_TRAN_HEAD.REV_NO%TYPE,
                         I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_store           IN     SA_TRAN_HEAD.STORE%TYPE,
                         I_day             IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)                   := 'TRANSACTION_SQL.REVISIONS_EXIST';
   L_exists        VARCHAR2(1)                    := NULL;
   L_store         SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day           SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_REVS_EXIST is
      select 'x'
        from sa_tran_head_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and rev_no      = nvl(I_rev_no, rev_no);

BEGIN

   if I_tran_seq_no  is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_revs_exist := FALSE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_REVS_EXIST','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
      open C_REVS_EXIST;
      SQL_LIB.SET_MARK('FETCH','C_REVS_EXIST','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
      fetch C_REVS_EXIST into L_exists;
      ---
      if C_REVS_EXIST%FOUND then
         O_revs_exist := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_REVS_EXIST','SA_TRAN_HEAD','tran_seq_no: '||to_char(I_tran_seq_no));
      close C_REVS_EXIST;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END REVISIONS_EXIST;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DISC_AMT(O_error_message  IN OUT VARCHAR2,
                           O_item_disc_amt  IN OUT NUMBER,
                           I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_item_seq_no    IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                           I_store          IN     SA_TRAN_HEAD.STORE%TYPE,
                           I_day            IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)                   := 'TRANSACTION_SQL.GET_ITEM_DISC_AMT';
   L_store         SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day           SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_GET_DISC_AMT is
      select SUM(nvl(unit_discount_amt,0) * nvl(uom_quantity,0))
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no;

BEGIN

   if I_tran_seq_no  is NOT NULL and
      I_item_seq_no  is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISC_AMT','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no));
      open C_GET_DISC_AMT;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISC_AMT','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no));
      fetch C_GET_DISC_AMT into O_item_disc_amt;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISC_AMT','SA_TRAN_DISC','tran_seq_no: '||to_char(I_tran_seq_no));
      close C_GET_DISC_AMT;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ITEM_DISC_AMT;
--------------------------------------------------------------------------------------------------
FUNCTION UPDATE_EXP_LOG(O_error_message    IN OUT VARCHAR2,
                        I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                        I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                        I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)                   := 'TRANSACTION_SQL.UPDATE_EXP_LOG';
   L_system_code   SA_EXPORT_LOG.SYSTEM_CODE%TYPE;
   L_seq_no        SA_EXPORT_LOG.SEQ_NO%TYPE;
   L_store         SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day           SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_GET_SYSTEMS is
      select el.system_code,
             el.seq_no
        from sa_export_log el
       where el.store_day_seq_no = I_store_day_seq_no
         and el.status != 'R'
         and exists (select 'x'
                       from sa_export_options eo
                      where eo.system_code = el.system_code
                        and eo.multiple_exp_ind = 'Y')
         and el.seq_no = (select MAX(seq_no)
                            from sa_export_log
                           where store_day_seq_no = I_store_day_seq_no
                             and store = I_store
                             and day = I_day
                             and system_code = el.system_code)
         and el.store = I_store
         and el.day = I_day;

BEGIN

   if I_store_day_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_SYSTEMS','SA_EXPORT_LOG','store_day_seq_no: '||to_char(I_store_day_seq_no));
      for C_rec in C_GET_SYSTEMS LOOP
         L_system_code := C_rec.system_code;
         L_seq_no      := C_rec.seq_no + 1;
         ---
         SQL_LIB.SET_MARK('INSERT','SA_EXPORT_LOG','C_GET_SYSTEMS','system_code: '||L_system_code);
         insert into sa_export_log(store_day_seq_no,
                                   system_code,
                                   seq_no,
                                   store,
                                   day,
                                   status,
                                   datetime)
                            values(I_store_day_seq_no,
                                   L_system_code,
                                   L_seq_no,
                                   L_store,
                                   L_day,
                                   'R',
                                   SYSDATE);
      end loop;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_EXP_LOG;
---------------------------------------------------------------------------------------
FUNCTION GET_STORE_DAY(O_error_message   IN OUT VARCHAR2,
                       O_business_date   IN OUT SA_STORE_DAY.BUSINESS_DATE%TYPE,
                       O_store           IN OUT SA_STORE_DAY.STORE%TYPE,
                       I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store           IN     SA_TRAN_HEAD.STORE%TYPE,
                       I_day             IN     SA_TRAN_HEAD.DAY%TYPE)

   RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'TRANSACTION_SQL.GET_STOREDAY_INFO';
   L_store         SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day           SA_TRAN_HEAD.DAY%TYPE          := I_day;

   cursor C_GET_STOREDAY_INFO is
    select sd.business_date,
           sd.store
      from sa_store_day sd,
           sa_tran_head th
     where sd.store_day_seq_no = th.store_day_seq_no
       and sd.store            = L_store
       and sd.day              = L_day
       and th.tran_seq_no      = I_tran_seq_no
       and th.store            = sd.store
       and th.day              = sd.day;

BEGIN

   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_STOREDAY_INFO','SA_TRAN_HEAD, SA_STORE_DAY',
                    'tran_seq_no: '||to_char(I_tran_seq_no));
   open C_GET_STOREDAY_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_STOREDAY_INFO','SA_TRAN_HEAD, SA_STORE_DAY ',
                    'tran_seq_no: '||to_char(I_tran_seq_no));
   fetch C_GET_STOREDAY_INFO into O_business_date,
                                  O_store;
   if C_GET_STOREDAY_INFO%NOTFOUND then
      O_error_message := 'INVALID_TRAN_NO';
      SQL_LIB.SET_MARK('CLOSE','C_GET_STOREDAY_INFO','SA_TRAN_HEAD,
                       SA_STORE_DAY', 'tran_seq_no: '||to_char(I_tran_seq_no));

      close C_GET_STOREDAY_INFO;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_STOREDAY_INFO','SA_TRAN_HEAD, SA_STORE_DAY',
                    'tran_seq_no: '||to_char(I_tran_seq_no));
   close C_GET_STOREDAY_INFO;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_STORE_DAY;

---------------------------------------------------------------------------------------
FUNCTION CREATE_UPDATE_DCLOSE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_store_day_seq_no  IN     SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                              I_files_loaded      IN     SA_STORE_DAY.FILES_LOADED%TYPE,
                              I_store             IN     SA_STORE_DAY.STORE%TYPE,
                              I_day               IN     SA_STORE_DAY.DAY%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(60)                   := 'TRANSACTION_SQL.CREATE_UPDATE_DCLOSE';
   L_tran_seq_no      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_files_loaded     SA_TRAN_HEAD.REF_NO1%TYPE      := NULL;
   L_table            VARCHAR2(30);
   L_store            SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day              SA_TRAN_HEAD.DAY%TYPE          := I_day;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_FILES_LOADED is
      select to_char(files_loaded)
        from sa_store_day
       where store_day_seq_no = I_store_day_seq_no
         and store            = L_store
         and day              = L_day;

   cursor C_LOCK_TRAN_HEAD is
      select 'Y'
         from sa_tran_head
        where store_day_seq_no = I_store_day_seq_no
          and store            = L_store
          and day              = L_day
          and tran_type        = 'DCLOSE'
          for update nowait;

BEGIN

   if I_store_day_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('STORE_DAY_SEQ_NO_REQ',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_files_loaded is NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_FILES_LOADED',
                          'SA_STORE_DAY',
                          'store_day_seq_no '||to_char(I_store_day_seq_no));
         open C_GET_FILES_LOADED;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_FILES_LOADED',
                          'SA_STORE_DAY',
                          'store_day_seq_no '||to_char(I_store_day_seq_no));
         fetch C_GET_FILES_LOADED into L_files_loaded;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_FILES_LOADED',
                          'SA_STORE_DAY',
                          'store_day_seq_no '||to_char(I_store_day_seq_no));
         close C_GET_FILES_LOADED;
      else
         L_files_loaded := to_char(I_files_loaded);
      end if;
      ---
      open  C_LOCK_TRAN_HEAD;
      close C_LOCK_TRAN_HEAD;
      ---
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'SA_TRAN_HEAD',
                       NULL);
      update sa_tran_head
         set ref_no1 = L_files_loaded
       where store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day
         and tran_type = 'DCLOSE';
      ---
      if SQL%NOTFOUND then
         if SA_SEQUENCE2_SQL.GET_TRAN_SEQ(O_error_message,
                                          L_tran_seq_no) = FALSE then
            return FALSE;
         end if;
         insert into sa_tran_head(store,
                                  day,
                                  tran_seq_no,
                                  rev_no,
                                  store_day_seq_no,
                                  tran_datetime,
                                  register,
                                  tran_no,
                                  cashier,
                                  salesperson,
                                  tran_type,
                                  sub_tran_type,
                                  orig_tran_no,
                                  orig_tran_type,
                                  orig_reg_no,
                                  ref_no1,
                                  ref_no2,
                                  ref_no3,
                                  ref_no4,
                                  reason_code,
                                  vendor_no,
                                  vendor_invc_no,
                                  payment_ref_no,
                                  proof_of_delivery_no,
                                  status,
                                  value,
                                  pos_tran_ind,
                                  update_datetime,
                                  update_id,
                                  error_ind)
                          values (L_store,
                                  L_day,
                                  L_tran_seq_no,
                                  1,
                                  I_store_day_seq_no,
                                  GET_VDATE,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  'DCLOSE',
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_files_loaded,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  'P',
                                  '0',
                                  'N',
                                  SYSDATE,
                                  USER,
                                  'N');
      end if;
   end if;
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'sa_tran_head',
                                            to_char(I_store_day_seq_no),
                                            'DCLOSE');
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_UPDATE_DCLOSE;
---------------------------------------------------------------------------------------
FUNCTION GET_FILES_EXPECTED(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_files_known       IN OUT  BOOLEAN,
                            O_files_expected    IN OUT  SA_TRAN_HEAD.REF_NO1%TYPE,
                            I_store_day_seq_no  IN      SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                            I_store             IN      SA_TRAN_HEAD.STORE%TYPE,
                            I_day               IN      SA_TRAN_HEAD.DAY%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(60)            := 'TRANSACTION_SQL.GET_FILES_EXPECTED';
   L_ref_no1          VARCHAR2(30)            := NULL;
   L_store            SA_TRAN_HEAD.STORE%TYPE := I_store;
   L_day              SA_TRAN_HEAD.DAY%TYPE   := I_day;

   cursor C_GET_FILES_EXPECTED is
      select ref_no1
        from sa_tran_head
       where store_day_seq_no = I_store_day_seq_no
         and store            = I_store
         and day              = I_day
         and tran_type        = 'DCLOSE';

BEGIN

   if I_store_day_seq_no is NULL then

         O_error_message := SQL_LIB.CREATE_MSG('STORE_DAY_SEQ_NO_REQ',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;

   else
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FILES_EXPECTED',
                       'SA_TRAN_HEAD',
                       'store_day_seq_no '||to_char(I_store_day_seq_no)||
                       'tran_type '||'DCLOSE');

      open C_GET_FILES_EXPECTED;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_FILES_EXPECTED',
                       'SA_TRAN_HEAD',
                       'store_day_seq_no '||to_char(I_store_day_seq_no)||
                       'tran_type '||'DCLOSE');

      fetch C_GET_FILES_EXPECTED into L_ref_no1;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_FILES_EXPECTED',
                       'SA_TRAN_HEAD',
                       'store_day_seq_no '||to_char(I_store_day_seq_no)||
                       'tran_type '||'DCLOSE');
      close C_GET_FILES_EXPECTED;

      O_files_expected := L_ref_no1;

      ---
      if L_ref_no1 is NULL then
            O_files_known := FALSE;
         else
            O_files_known := TRUE;
      end if;
      ---

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            'SQLERRM',
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_FILES_EXPECTED;
---------------------------------------------------------------------------------------
FUNCTION DELETE_CUST_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_attrib_type       IN      SA_CUST_ATTRIB.ATTRIB_TYPE%TYPE,
                            I_attrib_seq_no     IN      SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE,
                            I_tran_seq_no       IN      SA_CUST_ATTRIB.TRAN_SEQ_NO%TYPE,
                            I_store             IN      SA_CUST_ATTRIB.STORE%TYPE,
                            I_day               IN      SA_CUST_ATTRIB.DAY%TYPE)
return BOOLEAN IS

   L_program   VARCHAR2(60)            := 'TRANSACTION_SQL.DELETE_CUST_ATTRIB';
   L_store     SA_TRAN_HEAD.STORE%TYPE := I_store;
   L_day       SA_TRAN_HEAD.DAY%TYPE   := I_day;

   cursor C_LOCK_CUST_ATTRIB is
      select 'x'
        from sa_cust_attrib
       where tran_seq_no   = I_tran_seq_no
         and store         = L_store
         and day           = L_day
         and attrib_seq_no = nvl(I_attrib_seq_no, attrib_seq_no)
         and attrib_type   = nvl(I_attrib_type, attrib_type)
      for update nowait;

   cursor C_LOCK_ERROR_CUST_ATTRIB is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store         = L_store
         and day           = L_day
         and key_value_1 = nvl(I_attrib_seq_no, key_value_1)
         and rec_type in ('CATT','CATYPE','CAVALU')
      for update nowait;

BEGIN

   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_TRAN_SEQ_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ERROR_CUST_ATTRIB',
                    'SA_ERROR',
                    'tran_seq_no '||to_char(I_tran_seq_no));
   open C_LOCK_ERROR_CUST_ATTRIB;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ERROR_CUST_ATTRIB',
                    'SA_ERROR',
                    'tran_seq_no '||to_char(I_tran_seq_no));
   close C_LOCK_ERROR_CUST_ATTRIB;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_CUST_ATTRIB',
                    'SA_CUST_ATTRIB',
                    'tran_seq_no '||to_char(I_tran_seq_no));
   open C_LOCK_CUST_ATTRIB;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_CUST_ATTRIB',
                    'SA_CUST_ATTRIB',
                    'tran_seq_no '||to_char(I_tran_seq_no));
   close C_LOCK_CUST_ATTRIB;
   ---
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store       = L_store
      and day         = L_day
      and key_value_1 = nvl(I_attrib_seq_no, key_value_1)
      and rec_type   in ('CATT','CATYPE','CAVALU');
   ---
   delete from sa_cust_attrib
    where tran_seq_no   = I_tran_seq_no
      and store         = L_store
      and day           = L_day
      and attrib_seq_no = nvl(I_attrib_seq_no, attrib_seq_no)
      and attrib_type   = nvl(I_attrib_type, attrib_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            'SQLERRM',
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_CUST_ATTRIB;
---------------------------------------------------------------------------------------
FUNCTION DETAIL_EXISTS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists             IN OUT   BOOLEAN,
                       I_rounding_rule_id   IN       SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE)
   RETURN BOOLEAN is

   L_dummy VARCHAR2(1) := NULL;

   cursor C_EXISTS is
      select 'Y'
        from sa_rounding_rule_detail
       where rounding_rule_id = I_rounding_rule_id
         and rownum = 1;

BEGIN

   if I_rounding_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rounding_rule_id',
                                            'TRANSACTION_SQL.DETAIL_EXISTS',
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'SA_ROUNDING_RULE_DETAIL',
                    NULL);
   open C_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'SA_ROUNDING_RULE_DETAIL',
                    NULL);
   fetch C_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    'SA_ROUNDING_RULE_DETAIL',
                    NULL);
   close C_EXISTS;

   if L_dummy is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSACTION_SQL.DETAIL_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END DETAIL_EXISTS;
---------------------------------------------------------------------------------------
FUNCTION GET_VALID_ROUNDING_RULE_ID(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_rounding_rule_id   IN OUT   SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE,
                                    I_currency_code      IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                    I_country_id         IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_vdate  PERIOD.VDATE%TYPE := GET_VDATE;

   cursor C_CURRENCY_COUNTRY_RULE is
     select h.rounding_rule_id
       from sa_rounding_rule_head   h
      where h.currency_code = I_currency_code
        and h.country_id    = I_country_id
        and h.status        = 'A'
        and nvl(h.start_business_date, L_vdate)<= L_vdate;

   cursor C_CURRENCY_NOCOUNTRY_RULE is
     select h.rounding_rule_id
       from sa_rounding_rule_head   h
      where h.currency_code = I_currency_code
        and h.country_id   is NULL
        and h.status        = 'A'
        and nvl(h.start_business_date, L_vdate)<= L_vdate;

BEGIN

   if I_currency_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_currency_code',
                                            'TRANSACTION_SQL.GET_VALID_ROUNDING_RULE_ID',
                                            NULL);
      return FALSE;
   end if;

   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_country_id',
                                            'TRANSACTION_SQL.GET_VALID_ROUNDING_RULE_ID',
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CURRENCY_COUNTRY_RULE',
                    'SA_ROUNDING_RULE_HEAD',
                    'currency_code: '||I_currency_code||' country_id: '||I_country_id);
   open C_CURRENCY_COUNTRY_RULE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CURRENCY_COUNTRY_RULE',
                    'SA_ROUNDING_RULE_HEAD',
                    'currency_code: '||I_currency_code||' country_id: '||I_country_id);
   fetch C_CURRENCY_COUNTRY_RULE into O_rounding_rule_id;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CURRENCY_COUNTRY_RULE',
                    'SA_ROUNDING_RULE_HEAD',
                    'currency_code: '||I_currency_code||' country_id: '||I_country_id);
   close C_CURRENCY_COUNTRY_RULE;

   if O_rounding_rule_id is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CURRENCY_NOCOUNTRY_RULE',
                       'SA_ROUNDING_RULE_HEAD',
                       'currency_code: '||I_currency_code||' country_id: '||I_country_id);
      open C_CURRENCY_NOCOUNTRY_RULE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CURRENCY_NOCOUNTRY_RULE',
                       'SA_ROUNDING_RULE_HEAD',
                       'currency_code: '||I_currency_code||' country_id: '||I_country_id);
      fetch C_CURRENCY_NOCOUNTRY_RULE into O_rounding_rule_id;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CURRENCY_NOCOUNTRY_RULE',
                       'SA_ROUNDING_RULE_HEAD',
                       'currency_code: '||I_currency_code||' country_id: '||I_country_id);
      close C_CURRENCY_NOCOUNTRY_RULE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSACTION_SQL.GET_VALID_ROUNDING_RULE_ID',
                                            to_char(SQLCODE));
      return FALSE;

END GET_VALID_ROUNDING_RULE_ID;
---------------------------------------------------------------------------------------
FUNCTION GET_ENDING_AMT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_ending_amt         IN OUT   SA_TRAN_TENDER.TENDER_AMT%TYPE,
                        I_amt                IN       SA_TRAN_TENDER.TENDER_AMT%TYPE,
                        I_rounding_rule_id   IN       SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_MIN_MAX is
     select min(d.high_ending_amt) min_high_ending_amt,
            max(d.high_ending_amt) max_high_ending_amt
       from sa_rounding_rule_detail d
      where d.rounding_rule_id = I_rounding_rule_id;

   L_min_high_ending_amt      SA_ROUNDING_RULE_DETAIL.HIGH_ENDING_AMT%TYPE;
   L_max_high_ending_amt      SA_ROUNDING_RULE_DETAIL.HIGH_ENDING_AMT%TYPE;
   -- NOTE: even if currencies.currency_rtl_dec is set to any values less than 4, *_amt will
   --       still accept atmost 4 decimal figures, since it is defined as number(20,4).
   --       Hard-coding multiplier will assure that we'd get rid of the decimal point before
   --       applying the logic to get the ending_amt. This is really important for rounding rules
   --       that define the ending_amt to the left of the decimal.
   L_multiplier               NUMBER := 10000;
   L_lenmax_high_ending_amt   NUMBER;
   L_value                    SA_TRAN_TENDER.TENDER_AMT%TYPE;
   L_beginning_amt            SA_TRAN_TENDER.TENDER_AMT%TYPE;
   L_negative_flag            BOOLEAN := FALSE;
   L_amt                      SA_TRAN_TENDER.TENDER_AMT%TYPE;

BEGIN
   if I_amt is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_amt',
                                            'TRANSACTION_SQL.GET_ENDING_AMT',
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_rounding_rule_id is null then
      -- no rules defined, return I_amt as the O_ending_amt
      O_ending_amt := I_amt;
      return TRUE;
   else
      ---
      if I_amt < 0 then
         L_negative_flag := TRUE;
         L_amt := I_amt * -1;
      else
         L_negative_flag := FALSE;
         L_amt := I_amt;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_MIN_MAX',
                       'SA_ROUNDING_RULE_DETAIL',
                       'rounding_rule_id: '||I_rounding_rule_id);
      open C_MIN_MAX;

      SQL_LIB.SET_MARK('FETCH',
                       'C_MIN_MAX',
                       'SA_ROUNDING_RULE_DETAIL',
                       'rounding_rule_id: '||I_rounding_rule_id);
      fetch C_MIN_MAX into L_min_high_ending_amt,
                           L_max_high_ending_amt;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_MIN_MAX',
                       'SA_ROUNDING_RULE_DETAIL',
                       'rounding_rule_id: '||I_rounding_rule_id);
      close C_MIN_MAX;
      ---
      L_value                  := L_amt * L_multiplier;
      L_max_high_ending_amt    := L_max_high_ending_amt * L_multiplier;
      L_lenmax_high_ending_amt := length(L_max_high_ending_amt);
      L_min_high_ending_amt    := L_min_high_ending_amt * L_multiplier;
      ---
      while L_value > L_max_high_ending_amt and
            length(L_value) > length(L_min_high_ending_amt) loop
         L_beginning_amt          := trunc(L_value, -1 * L_lenmax_high_ending_amt);
         L_value                  := L_value - L_beginning_amt;
         L_lenmax_high_ending_amt := L_lenmax_high_ending_amt - 1;
      end loop;
      ---
      O_ending_amt := L_value/L_multiplier;
      ---
      if L_negative_flag then
         O_ending_amt := O_ending_amt * -1;
      end if;
      ---
      return TRUE;
   end if;
   ---

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSACTION_SQL.GET_ENDING_AMT',
                                            to_char(SQLCODE));
      return FALSE;

END GET_ENDING_AMT;
---------------------------------------------------------------------------------------
FUNCTION CURRENCY_ROUND(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_rounded_amt        IN OUT   SA_TRAN_TENDER.TENDER_AMT%TYPE,
                        I_amt                IN       SA_TRAN_TENDER.TENDER_AMT%TYPE,
                        I_rounding_rule_id   IN       SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE,
                        I_raise_no_rule      IN       VARCHAR2 DEFAULT 'Y')
   RETURN BOOLEAN IS

   cursor C_ROUNDRULE is
      select d.high_ending_amt,
             d.low_ending_amt,
             d.round_amt
        from sa_rounding_rule_detail d
       where d.rounding_rule_id = I_rounding_rule_id
    order by d.high_ending_amt desc;

   L_found_flag         BOOLEAN := FALSE;
   L_high_ending_amt    SA_ROUNDING_RULE_DETAIL.HIGH_ENDING_AMT%TYPE;
   L_low_ending_amt     SA_ROUNDING_RULE_DETAIL.LOW_ENDING_AMT%TYPE;
   L_round_amt          SA_ROUNDING_RULE_DETAIL.ROUND_AMT%TYPE;
   L_rounded_amt        SA_TRAN_TENDER.TENDER_AMT%TYPE;
   L_ending_amt         SA_TRAN_TENDER.TENDER_AMT%TYPE;
   L_beginning_amt      SA_TRAN_TENDER.TENDER_AMT%TYPE;
   L_negative_flag      BOOLEAN := FALSE;
   L_amt                SA_TRAN_TENDER.TENDER_AMT%TYPE;

BEGIN
   if I_amt is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_amt',
                                            'TRANSACTION_SQL.CURRENCY_ROUND',
                                            NULL);
      return FALSE;
   end if;

   --- I_rounding_rule_id can be null

   --- no rule defined for currency/country combination; return the I_amt as O_rounded_amt
   if I_rounding_rule_id is null then
      O_rounded_amt := I_amt;
      return TRUE;
   end if;
   ---
   if I_amt < 0 then
      L_negative_flag := TRUE;
      L_amt := I_amt * -1;
   else
      L_negative_flag := FALSE;
      L_amt := I_amt;
   end if;
   ---
   if TRANSACTION_SQL.GET_ENDING_AMT(O_error_message,
                                     L_ending_amt,
                                     L_amt,
                                     I_rounding_rule_id) = FALSE then
      return FALSE;
   end if;
   ---
   L_beginning_amt := L_amt - nvl(L_ending_amt,0);
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ROUNDRULE',
                    'SA_ROUNDING_RULE_DETAIL',
                    'rounding_rule_id: '||I_rounding_rule_id);
   open  C_ROUNDRULE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ROUNDRULE',
                    'SA_ROUNDING_RULE_DETAIL',
                    'rounding_rule_id: '||I_rounding_rule_id);
   fetch C_ROUNDRULE into L_high_ending_amt,
                          L_low_ending_amt,
                          L_round_amt;

   -- rule defined for currency/country combination
   while (not(L_found_flag) and (C_ROUNDRULE%FOUND)) loop
      L_rounded_amt := 0;
      --
      if L_ending_amt >= L_low_ending_amt and L_ending_amt <= L_high_ending_amt then
         L_found_flag  := TRUE;
         L_rounded_amt := L_round_amt;
      end if;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_ROUNDRULE',
                       'SA_ROUNDING_RULE_DETAIL',
                       'rounding_rule_id: '||I_rounding_rule_id);
      fetch C_ROUNDRULE into L_high_ending_amt,
                             L_low_ending_amt,
                             L_round_amt;
   end loop;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ROUNDRULE',
                    'SA_ROUNDING_RULE_DETAIL',
                    'rounding_rule_id: '||I_rounding_rule_id);
   close C_ROUNDRULE;
   ---
   if L_found_flag then
      -- found in rules
      O_rounded_amt := L_beginning_amt + L_rounded_amt;
      ---
      if L_negative_flag then
         O_rounded_amt := O_rounded_amt * -1;
      end if;
      ---
      return TRUE;
   elsif NVL(I_raise_no_rule,'Y') = 'N' then
      O_rounded_amt := I_amt;
      return TRUE;
   else
      -- L_ending_amt not found in rules for the currency/country
      O_error_message := SQL_LIB.CREATE_MSG('NO_RULE_FOR_AMT',
                                            I_rounding_rule_id,
                                            to_char(L_amt),
                                            to_char(L_ending_amt));
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSACTION_SQL.CURRENCY_ROUND',
                                            to_char(SQLCODE));
      return FALSE;

END CURRENCY_ROUND;
---------------------------------------------------------------------------------------
FUNCTION LOCK_SA_TRAN_TENDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tran_seq_no          IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE,
                             I_tender_seq_no        IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(40) := 'TRANSACTION_SQL.LOCK_SA_TRAN_TENDER';

   cursor C_FULL_ACCESS_SA_TRAN_TEND is
      select store,
             day,
             tran_seq_no,
             tender_seq_no,
             tender_type_group,
             tender_type_id,
             tender_amt,
             cc_no,
             cc_exp_date,
             cc_auth_no,
             cc_auth_src,
             cc_entry_mode,
             cc_cardholder_verf,
             cc_spec_cond,
             voucher_no,
             coupon_no,
             coupon_ref_no,
             check_acct_no,
             check_no,
             identi_method,
             identi_id,
             orig_currency,
             orig_curr_amt,
             ref_no9,
             ref_no10,
             ref_no11,
             ref_no12,
             error_ind
        from sa_tran_tender
       where tran_seq_no = I_tran_seq_no
         and tender_seq_no = I_tender_seq_no
         FOR UPDATE nowait;

BEGIN

   open C_FULL_ACCESS_SA_TRAN_TEND;
   close C_FULL_ACCESS_SA_TRAN_TEND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_SA_TRAN_TENDER;
--------------------------------------------------------------------------------------------------------
FUNCTION ON_UPDATE_SA_TRAN_TENDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store                IN       SA_TRAN_TENDER.STORE%TYPE,
                                  I_day                  IN       SA_TRAN_TENDER.DAY%TYPE,
                                  I_tran_seq_no          IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE,
                                  I_tender_seq_no        IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE,
                                  I_tender_type_group    IN       SA_TRAN_TENDER.TENDER_TYPE_GROUP%TYPE,
                                  I_tender_type_id       IN       SA_TRAN_TENDER.TENDER_TYPE_ID%TYPE,
                                  I_tender_amt           IN       SA_TRAN_TENDER.TENDER_AMT%TYPE,
                                  I_cc_no                IN       SA_TRAN_TENDER.CC_NO%TYPE,
                                  I_cc_exp_date          IN       SA_TRAN_TENDER.CC_EXP_DATE%TYPE,
                                  I_cc_auth_no           IN       SA_TRAN_TENDER.CC_AUTH_NO%TYPE,
                                  I_cc_auth_src          IN       SA_TRAN_TENDER.CC_AUTH_SRC%TYPE,
                                  I_cc_entry_mode        IN       SA_TRAN_TENDER.CC_ENTRY_MODE%TYPE,
                                  I_cc_cardholder_verf   IN       SA_TRAN_TENDER.CC_CARDHOLDER_VERF%TYPE,
                                  I_cc_spec_cond         IN       SA_TRAN_TENDER.CC_SPEC_COND%TYPE,
                                  I_voucher_no           IN       SA_TRAN_TENDER.VOUCHER_NO%TYPE,
                                  I_coupon_no            IN       SA_TRAN_TENDER.COUPON_NO%TYPE,
                                  I_coupon_ref_no        IN       SA_TRAN_TENDER.COUPON_REF_NO%TYPE,
                                  I_ref_no9              IN       SA_TRAN_TENDER.REF_NO9%TYPE,
                                  I_ref_no10             IN       SA_TRAN_TENDER.REF_NO10%TYPE,
                                  I_ref_no11             IN       SA_TRAN_TENDER.REF_NO11%TYPE,
                                  I_ref_no12             IN       SA_TRAN_TENDER.REF_NO12%TYPE,
                                  I_check_acct_no        IN       SA_TRAN_TENDER.CHECK_ACCT_NO%TYPE,
                                  I_check_no             IN       SA_TRAN_TENDER.CHECK_NO%TYPE,
                                  I_identi_method        IN       SA_TRAN_TENDER.IDENTI_METHOD%TYPE,
                                  I_identi_id            IN       SA_TRAN_TENDER.IDENTI_ID%TYPE,
                                  I_orig_currency        IN       SA_TRAN_TENDER.ORIG_CURRENCY%TYPE,
                                  I_orig_curr_amt        IN       SA_TRAN_TENDER.ORIG_CURR_AMT%TYPE,
                                  I_error_ind            IN       SA_TRAN_TENDER.ERROR_IND%TYPE)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(40) := 'TRANSACTION_SQL.ON_UPDATE_SA_TRAN_TENDER';

BEGIN

   update sa_tran_tender
      set store  = I_store,
          day  = I_day,
          tran_seq_no  = I_tran_seq_no,
          tender_seq_no  = I_tender_seq_no,
          tender_type_group  = I_tender_type_group,
          tender_type_id  = I_tender_type_id,
          tender_amt  = I_tender_amt,
          cc_no  = I_cc_no,
          cc_exp_date  = I_cc_exp_date,
          cc_auth_no  = I_cc_auth_no,
          cc_auth_src  = I_cc_auth_src,
          cc_entry_mode  = I_cc_entry_mode,
          cc_cardholder_verf  = I_cc_cardholder_verf,
          cc_spec_cond  = I_cc_spec_cond,
          voucher_no  = I_voucher_no,
          coupon_no  = I_coupon_no,
          coupon_ref_no  = I_coupon_ref_no,
          ref_no9  = I_ref_no9,
          ref_no10  = I_ref_no10,
          ref_no11  = I_ref_no11,
          ref_no12  = I_ref_no12,
          check_acct_no = I_check_acct_no,
          check_no = I_check_no,
          identi_method = I_identi_method,
          identi_id = I_identi_id,
          orig_currency = I_orig_currency,
          orig_curr_amt = I_orig_curr_amt,
          error_ind  = I_error_ind
    where tran_seq_no = I_tran_seq_no
      and tender_seq_no = I_tender_seq_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ON_UPDATE_SA_TRAN_TENDER;
--------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_POS_TRAN(O_error_message      IN OUT   VARCHAR2,
                           O_exists             IN OUT   BOOLEAN,
                           O_next_tran_seq_no   IN OUT   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_store_day_seq_no   IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                           I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                           I_register           IN       SA_TRAN_HEAD.REGISTER%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)              := 'TRANSACTION_SQL.GET_NEXT_POS_TRAN';
   L_store     SA_TRAN_HEAD.STORE%TYPE   := I_store;
   L_day       SA_TRAN_HEAD.DAY%TYPE     := I_day;

   cursor C_GET_NEXT_POS_TRAN is
   select tran_seq_no 
     from ( select tran_seq_no
              from sa_tran_head
             where store_day_seq_no = I_store_day_seq_no
               and NVL(tran_no, ( tran_seq_no + 10000000000)) > (select NVL(tran_no, (tran_seq_no+10000000000))
                                                                   from sa_tran_head
                                                                   where tran_seq_no = I_tran_seq_no
                                                                     and store = L_store
                                                                     and day = L_day
                                                                     and tran_type != 'TERM'
                                                                     and register = I_register)
               and store = L_store
               and day = L_day
               and tran_type != 'TERM'
               and register = I_register
             order by NVL(tran_no,( tran_seq_no + 10000000000) ), tran_seq_no)
    where rownum=1;
BEGIN

   if I_store_day_seq_no is NOT NULL and
      I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_exists := FALSE;
      ---
      open C_GET_NEXT_POS_TRAN;
      fetch C_GET_NEXT_POS_TRAN into O_next_tran_seq_no;
      close C_GET_NEXT_POS_TRAN;
      ---
      if O_next_tran_seq_no is NOT NULL then
         O_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_NEXT_POS_TRAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_PREV_POS_TRAN(O_error_message      IN OUT   VARCHAR2,
                           O_exists             IN OUT   BOOLEAN,
                           O_prev_tran_seq_no   IN OUT   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_store_day_seq_no   IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                           I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                           I_register           IN       SA_TRAN_HEAD.REGISTER%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)              := 'TRANSACTION_SQL.GET_PREV_POS_TRAN';
   L_store     SA_TRAN_HEAD.STORE%TYPE   := I_store;
   L_day       SA_TRAN_HEAD.DAY%TYPE     := I_day;

   cursor C_GET_PREV_POS_TRAN is
   select tran_seq_no 
     from ( select tran_seq_no
              from sa_tran_head
             where store_day_seq_no = I_store_day_seq_no
               and NVL(tran_no, ( tran_seq_no + 10000000000)) < (select NVL(tran_no, (tran_seq_no+10000000000))
                                                                   from sa_tran_head
                                                                   where tran_seq_no = I_tran_seq_no
                                                                     and store = L_store
                                                                     and day = L_day
                                                                     and tran_type != 'TERM'
                                                                     and register = I_register)
               and store = L_store
               and day = L_day
               and tran_type != 'TERM'
               and register = I_register
             order by NVL(tran_no,( tran_seq_no + 10000000000) ) desc, tran_seq_no desc)
    where rownum=1;
BEGIN

   if I_store_day_seq_no is NOT NULL and
      I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_exists := FALSE;
      ---
      open C_GET_PREV_POS_TRAN;
      fetch C_GET_PREV_POS_TRAN into O_prev_tran_seq_no;
      close C_GET_PREV_POS_TRAN;
      ---
      if O_prev_tran_seq_no is NOT NULL then
         O_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PREV_POS_TRAN;
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_TRAN_PAYMENT(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_payment_seq_no    IN      SA_TRAN_PAYMENT.PAYMENT_SEQ_NO%TYPE,
                             I_tran_seq_no       IN      SA_TRAN_PAYMENT.TRAN_SEQ_NO%TYPE,
                             I_store             IN      SA_TRAN_PAYMENT.STORE%TYPE,
                             I_day               IN      SA_TRAN_PAYMENT.DAY%TYPE)
return BOOLEAN IS

   L_program   VARCHAR2(60)            := 'TRANSACTION_SQL.DELETE_TRAN_PAYMENT';
   L_store     SA_TRAN_HEAD.STORE%TYPE := I_store;
   L_day       SA_TRAN_HEAD.DAY%TYPE   := I_day;

   cursor C_LOCK_TRAN_PYMT is
      select 'x'
        from sa_tran_payment
       where tran_seq_no    = I_tran_seq_no
         and store          = L_store
         and day            = L_day
         and payment_seq_no = I_payment_seq_no
      for update nowait;

BEGIN

   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_TRAN_SEQ_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_TRAN_PYMT',
                    'SA_TRAN_PAYMENT',
                    'tran_seq_no '||to_char(I_tran_seq_no));
   open C_LOCK_TRAN_PYMT;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_TRAN_PYMT',
                    'SA_TRAN_PAYMENT',
                    'tran_seq_no '||to_char(I_tran_seq_no));
   close C_LOCK_TRAN_PYMT;
   ---
   delete from sa_tran_payment
    where tran_seq_no    = I_tran_seq_no
      and store          = L_store
      and day            = L_day
      and payment_seq_no = I_payment_seq_no;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            'SQLERRM',
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_TRAN_PAYMENT;
---------------------------------------------------------------------------------------
END TRANSACTION_SQL;
/