
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TRANSACTION_DETAIL_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------
--Filename : satrdetls.pls
--Purpose  : Pass out the banner_id and banner_name for 
--           given store based on the store's channel
--Author   : Shaheen Malik (Accenture - ERC)
--------------------------------------------------------
--------------------------------------------------------
--Function Name : get_store_banner
--Purpose       : This function will pass out the banner_id and banner_name
--                for the given store based on the store's channel
--------------------------------------------------------
FUNCTION GET_STORE_BANNER(O_error_message   IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                          O_banner_id       IN OUT     BANNER.BANNER_ID%TYPE,
                          O_banner_name     IN OUT     BANNER.BANNER_NAME%TYPE,
                          O_exists          IN OUT     BOOLEAN,
                          I_store           IN         STORE.STORE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------
--Function Name : ITEM_FILTER_LIST
--Purpose       : This function will validate if the item
--                is accessible to the user
--------------------------------------------------------
FUNCTION ITEM_FILTER_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_diff            IN OUT   VARCHAR2,
                          I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: GET_SUM_CONTENT_QTY
--Purpose      : This function will return the Content item qty for a given Container item/Item 
--               status in a transaction (tran_seq_no/store/day).
-----------------------------------------------------------------------------------------------
FUNCTION GET_SUM_CONTENT_QTY (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_sum_content_qty         IN OUT   SA_TRAN_ITEM.QTY%TYPE,
                              O_sum_content_std_qty     IN OUT   SA_TRAN_ITEM.STANDARD_QTY%TYPE,
                              O_sum_content_uom_qty     IN OUT   SA_TRAN_ITEM.UOM_QUANTITY%TYPE,
                              I_tran_seq_no             IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store                   IN       SA_TRAN_HEAD.STORE%TYPE,
                              I_day                     IN       SA_TRAN_HEAD.DAY%TYPE,
                              I_container_item          IN       ITEM_MASTER.ITEM%TYPE,
                              I_container_item_status   IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE)                              
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: GET_CONTAINER_QTY
--Purpose      : This function will primarily return the Container item qty for a given Content 
--               item/Item status in a transaction (tran_seq_no/store/day). This will also 
--               return the Container Item and Item Seq No.
-----------------------------------------------------------------------------------------------
FUNCTION GET_CONTAINER_QTY (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_container_item        IN OUT   ITEM_MASTER.ITEM%TYPE,
                            O_container_qty         IN OUT   SA_TRAN_ITEM.QTY%TYPE,
                            O_item_seq_no           IN OUT   SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                            I_tran_seq_no           IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                            I_store                 IN       SA_TRAN_HEAD.STORE%TYPE,
                            I_day                   IN       SA_TRAN_HEAD.DAY%TYPE,
                            I_content_item          IN       ITEM_MASTER.ITEM%TYPE,
                            I_content_item_status   IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: ADD_CONTAINER_ITEM_TO_TRAN
--Purpose      : This function will insert a Container item into sa_tran_item
-----------------------------------------------------------------------------------------------
FUNCTION ADD_CONTAINER_ITEM_TO_TRAN (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_tran_seq_no             IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                     I_store                   IN       SA_TRAN_HEAD.STORE%TYPE,
                                     I_day                     IN       SA_TRAN_HEAD.DAY%TYPE,
                                     I_container_item          IN       ITEM_MASTER.ITEM%TYPE,
                                     I_container_item_status   IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                     I_qty                     IN       SA_TRAN_ITEM.QTY%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: DELETE_SINGLE_CONTAINER_ITEM
--Purpose      : This function will delete the container for a content item in a transaction. 
-----------------------------------------------------------------------------------------------
FUNCTION DELETE_SINGLE_CONTAINER_ITEM (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_tran_seq_no             IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                       I_store                   IN       SA_TRAN_HEAD.STORE%TYPE,
                                       I_day                     IN       SA_TRAN_HEAD.DAY%TYPE,
                                       I_container_item_seq_no   IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE)
RETURN BOOLEAN;                                          
-----------------------------------------------------------------------------------------------
--Function Name: DELETE_ORPHAN_CONTAINER_ITEM
--Purpose      : This function will delete the container for a given transaction (tran_seq_no/
--               store/day) that does not have a corresponding content item within that same
--               transaction.
-----------------------------------------------------------------------------------------------
FUNCTION DELETE_ORPHAN_CONTAINER_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                       I_store           IN       SA_TRAN_HEAD.STORE%TYPE,
                                       I_day             IN       SA_TRAN_HEAD.DAY%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: UPDATE_CONTAINER_ITEM_QTY
--Purpose      : For a given transaction (tran_seq_no/store/day) and Content item, this function 
--               will compare the Container qty and sum(Content qty) then update the Container
--               Qty if they are different. This function will be applicable for scenarios where,
--               the Container is associated to more than 1 Content items on the transaction and
--               one of the content items have been modified (deleted, changed to another item, or
--               item_status, qty changed)
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONTAINER_ITEM_QTY (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_seq_no           IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                    I_store                 IN       SA_TRAN_HEAD.STORE%TYPE,
                                    I_day                   IN       SA_TRAN_HEAD.DAY%TYPE,
                                    I_content_item          IN       ITEM_MASTER.ITEM%TYPE,
                                    I_content_item_status   IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: POP_SINGLE_CONTAINER_ITEM
--Purpose      : This function will insert/update container for a single contents item on the
--               transaction. The input tran_seq_no/item_seq_no/store/day should refer to a
--               Content item
-----------------------------------------------------------------------------------------------
FUNCTION POP_SINGLE_CONTAINER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                   I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                   I_store           IN       SA_TRAN_HEAD.STORE%TYPE,
                                   I_day             IN       SA_TRAN_HEAD.DAY%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: VALIDATE_STORE_BANNER
--Purpose      : This function will validate if the store banner is a valid store/channel/banner id.
--               This function also gets the banner name.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE_BANNER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid           IN OUT   BOOLEAN,
                               O_banner_name     IN OUT   BANNER.BANNER_NAME%TYPE,
                               I_banner_id       IN       BANNER.BANNER_ID%TYPE,
                               I_store           IN       STORE.STORE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END TRANSACTION_DETAIL_SQL;
/
