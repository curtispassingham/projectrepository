
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TRANSACTION_REV_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
--- Function:  CHECK_DISC_REV_RECS
--- Purpose :  Called in the Transaction Audit Trail form to check if an item 
--             has discount records associated with it.  
--             All input parameters are required.
---------------------------------------------------------------------------------------
FUNCTION CHECK_DISC_REV_RECS(O_error_message   IN OUT VARCHAR2,
                             O_discount_exists IN OUT BOOLEAN,
                             I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_item_seq_no     IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                             I_rev_no          IN     SA_TRAN_ITEM_REV.REV_NO%TYPE,
                             I_store           IN     SA_TRAN_HEAD.STORE%TYPE,
                             I_day             IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--- Function:  GET_ERROR_REV
--- Purpose :  Called by Transaction Audit Trail form to return an error 
--             description for the passed in TRAN_SEQ_NO,
---            key_value_1, key_value_2(optional), rec_type, and rev_no.  
--             All inputs except key_value_2 are required.
-----------------------------------------------------------------------------------------
FUNCTION GET_ERROR_REV(O_error_message      IN OUT VARCHAR2,
                       O_error_desc         IN OUT SA_ERROR_CODES.ERROR_DESC%TYPE,
                       I_tran_seq_no        IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_key_value_1        IN     SA_ERROR.KEY_VALUE_1%TYPE,
                       I_key_value_2        IN     SA_ERROR.KEY_VALUE_2%TYPE,
                       I_rec_type           IN     SA_ERROR.REC_TYPE%TYPE,
                       I_rev_no             IN     SA_ERROR_REV.REV_NO%TYPE,
                       I_store              IN     SA_TRAN_HEAD.STORE%TYPE,
                       I_day                IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function:  COUNT_REVISIONS
-- Purpose :  This function will return the total number of revisions for a transaction.  
--            If the transaction head number input variable is not null, open a cursor 
--            that should count the total number of records on the sa_tran_head_rev table 
--            for the passed transaction head number.  This value should be fetch and returned
--            in an output variable and the function should return true.  If the transaction 
--            head number input variable is null, return an appropriate error message to the 
--            user and return false.  
----------------------------------------------------------------------------------------
FUNCTION COUNT_REVISIONS(O_error_message    IN OUT VARCHAR2,
                         O_no_revisions     IN OUT NUMBER,
                         I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                         I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- FUNCTION: GET_TRAN_BALANCE
-- Purpose : This function will return the transaction balance for a transaction.  
--	     If the transaction head(sequence) number input variable is not null, 	 
--	     fetch the following information into local variables:	
--	     Sum of all tenders from the sa_tran_tender_rev table for the passed transaction	
--	     head (sequence) number and revision number
--	     Sum of all the total item retails (qty * unit_retail) from the sa_tran_item_rev
--	     table for the passed transaction head/sequence number and revision number
--	     Sum of all the discount (qty * unit_discount_amt) from the sa_tran_disc_rev
--   	     table for the passed transaction head number and revision number
--	     Sum of the taxes from the sa_tran_tax_rev table for the passed transaction
--     	     head/sequence number and revision number
--	     Set O_balance equal to the sum of the tender minus the sum of the items less the discounts
--	     plus the total tax.  (O_balance := L_tender - ((L_item - L_discount) + L_tax)).  Return true.
-----------------------------------------------------------------------------------------			
FUNCTION GET_TRAN_BALANCE(O_error_message	IN OUT VARCHAR2,
		 	  O_balance		IN OUT NUMBER,
			  I_tran_seq_no		IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
			  I_rev_no		IN     SA_TRAN_HEAD.REV_NO%TYPE,
                          I_store               IN     SA_TRAN_HEAD.STORE%TYPE,
                          I_day                 IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN; 	
--------------------------------------------------------------------------------------
END TRANSACTION_REV_SQL;
/
