
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BUDGET_QUERY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
-- Name:    FETCH_BUDGET_INFO
-- Purpose: Used to query the records for the form deptbudv.
-- Created By: Amy Selby, 13-SEP-96.
---------------------------------------------------------------------
FUNCTION FETCH_BUDGET_INFO(i_budget_variable IN     VARCHAR2,
                           i_dept            IN     NUMBER,
                           i_month           IN     NUMBER,
                           i_half            IN     NUMBER,                           
                           i_loc_type        IN     VARCHAR2,
                           i_location        IN     NUMBER,      
                           io_ret            IN OUT NUMBER,
                           error_message     IN OUT VARCHAR2) RETURN BOOLEAN;
--------------------------------------------------------------------
END;
/
