CREATE OR REPLACE PACKAGE TRANSACTION_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
--- Function:  DISCOUNTS_EXIST
--- Purpose:   Checks the sa_tran_disc table to see if discount records exist for a
---            passed tran_seq_no and item_seq_no (both required).
---
FUNCTION DISCOUNTS_EXIST(O_error_message   IN OUT VARCHAR2,
                         O_discount_exists IN OUT BOOLEAN,
                         I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_item_seq_no     IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                         I_store           IN     SA_TRAN_HEAD.STORE%TYPE,
                         I_day             IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name:  GET_TOTAL_DISC_AMT
-- Purpose      :  This function will return the total discount amount for a transaction seq. no.
---
FUNCTION GET_TOTAL_DISC_AMT(O_error_message  IN OUT VARCHAR2,
                            O_total_disc_amt IN OUT NUMBER,
                            I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                            I_store          IN     SA_TRAN_HEAD.STORE%TYPE,
                            I_day            IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function name: Create_revisions
--- Purpose: This function is called from the satrdetl form to write mirror records to the
---          sales audit table so the audit trail can be tracked.
--           This function will also delete the sa_exported records for a transaction.
--           It also moves the records from the sa_exported table to the
--           sa_exported_rev table.
---
FUNCTION CREATE_REVISIONS(O_error_message    IN OUT VARCHAR2,
                          I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_rev_no           IN     SA_TRAN_HEAD.REV_NO%TYPE,
                          I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                          I_day              IN     SA_TRAN_HEAD.DAY%TYPE,
                          I_pm_mode          IN     VARCHAR2 DEFAULT 'EDIT')
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function name: Post_void
--- Purpose: This is called from the satrdetl form when the list item Post Void is chosen
---          on the satrfind form.  An sa_tran_head record is created.
---
FUNCTION POST_VOID(O_error_message     IN OUT VARCHAR2,
                   I_orig_tran_no      IN     SA_TRAN_HEAD.TRAN_NO%TYPE,
                   I_orig_tran_seq_no  IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                   I_orig_tran_type    IN     SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                   I_update_datetime   IN     SA_TRAN_HEAD.UPDATE_DATETIME%TYPE,
                   I_update_id         IN     SA_TRAN_HEAD.UPDATE_ID%TYPE,
                   I_store             IN     SA_TRAN_HEAD.STORE%TYPE,
                   I_day               IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--- Function name:  GET_TRAN_NO_TYPE
--- Purpose:   this function fetches POS tran_no, tran_type and it's desc, sub_tran_type (and it's desc),
---            and reason_code and it's desc.
---
FUNCTION GET_TRAN_NO_TYPE(O_error_message      IN OUT VARCHAR2,
                          O_tran_no            IN OUT SA_TRAN_HEAD.TRAN_NO%TYPE,
                          O_tran_type          IN OUT SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                          O_tran_type_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                          O_sub_tran_type      IN OUT SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                          O_sub_tran_type_desc IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                          O_reason_code        IN OUT SA_TRAN_HEAD.REASON_CODE%TYPE,
                          O_reason_code_desc   IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                          I_tran_seq_no        IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_store              IN     SA_TRAN_HEAD.STORE%TYPE,
                          I_day                IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--- Function name:  GET_INFO
--- Purpose: This function fetches the tran_datetime, tran_no, rtlog_orig_sys, register, cashier, tran_type,
---          sub_tran_type, reason_code, status, store/day sequence number, store, store name,
---          chain, and chain name from the sa_tran_head table for the specified transaction
---          sequence number.
---
FUNCTION GET_INFO(O_error_message    IN OUT VARCHAR2,
                  O_exists           IN OUT BOOLEAN,
                  O_tran_datetime    IN OUT SA_TRAN_HEAD.TRAN_DATETIME%TYPE,
                  O_tran_no          IN OUT SA_TRAN_HEAD.TRAN_NO%TYPE,
                  O_ext_tran_sys     IN OUT SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                  O_register         IN OUT SA_TRAN_HEAD.REGISTER%TYPE,
                  O_cashier          IN OUT SA_TRAN_HEAD.CASHIER%TYPE,
                  O_tran_type        IN OUT SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                  O_sub_tran_type    IN OUT SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                  O_reason_code      IN OUT SA_TRAN_HEAD.REASON_CODE%TYPE,
                  O_status           IN OUT SA_TRAN_HEAD.STATUS%TYPE,
                  O_store_day_seq_no IN OUT SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                  O_store            IN OUT STORE.STORE%TYPE,
                  O_store_name       IN OUT STORE.STORE_NAME%TYPE,
                  O_chain            IN OUT CHAIN.CHAIN%TYPE,
                  O_chain_name       IN OUT CHAIN.CHAIN_NAME%TYPE,
                  I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                  I_rev_ind          IN     VARCHAR2,
                  I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                  I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--- Function Name : GET_DISC_QTY
--- Purpose       : Returns the quantity and uom_quantity of a passed item on discount
---
FUNCTION GET_DISC_QTY(O_error_message      IN OUT   VARCHAR2,
                      O_discount_qty       IN OUT   SA_TRAN_ITEM.QTY%TYPE,
                      O_discount_uom_qty   IN OUT   SA_TRAN_ITEM.UOM_QUANTITY%TYPE,
                      I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_item_seq_no        IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                      I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                      I_day                IN       SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- Funtion name:   GET_NEXT_TRAN
--- Purpose     :   This function will return the next transaction sequence number for the passed in
---                 store and business day.
---
FUNCTION GET_NEXT_TRAN(O_error_message    IN OUT VARCHAR2,
                       O_exists           IN OUT BOOLEAN,
                       O_next_tran_seq_no IN OUT SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                       I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                       I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- Function name:   GET_PREV_TRAN
--- Purpose      :   This function will return the previous transaction sequence number for the passed
---                  in store and business day.
---
FUNCTION GET_PREV_TRAN(O_error_message    IN OUT VARCHAR2,
                       O_exists           IN OUT BOOLEAN,
                       O_prev_tran_seq_no IN OUT SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                       I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                       I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--- Function name: DELETE_TRAN_DETAILS
--- Purpose      : This function will delete any details records on a transaction that should not contain
---                values for the passed transaction type.
---
FUNCTION DELETE_TRAN_DETAILS(O_error_message IN OUT VARCHAR2,
                             I_tran_seq_no   IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_tran_type     IN     SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                             I_store         IN     SA_TRAN_HEAD.STORE%TYPE,
                             I_day           IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--- Function name: Delete_revisions
--- Purpose: This function is called from the trandetl form to delete mirror records on the revision tables
--           if the data was not revised.
---
FUNCTION DELETE_REVISIONS(O_error_message    IN OUT VARCHAR2,
                          I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_rev_no           IN     SA_TRAN_HEAD.REV_NO%TYPE,
                          I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                          I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function name: REVISIONS_EXIST
--- Purpose      : Checks the _rev tables to see if revisions exist for a transaction
---                sequence number and optional revision number.
---
FUNCTION REVISIONS_EXIST(O_error_message   IN OUT VARCHAR2,
                         O_revs_exist      IN OUT BOOLEAN,
                         I_rev_no          IN     SA_TRAN_HEAD.REV_NO%TYPE,
                         I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_store           IN     SA_TRAN_HEAD.STORE%TYPE,
                         I_day             IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name:  GET_ITEM_DISC_AMT
-- Purpose      :  This function will return the total discount amount for a transaction seq. no and
--                 item seq. no.
--
FUNCTION GET_ITEM_DISC_AMT(O_error_message  IN OUT VARCHAR2,
                           O_item_disc_amt  IN OUT NUMBER,
                           I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_item_seq_no    IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                           I_store          IN     SA_TRAN_HEAD.STORE%TYPE,
                           I_day            IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name: UPDATE_EXP_LOG
-- Purpose      : This function will be called by the transaction detail form to update the export log
--                table anytime a transaction changes.
--
FUNCTION UPDATE_EXP_LOG(O_error_message    IN OUT VARCHAR2,
                        I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                        I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                        I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: GET_STORE_DAY
-- Purpose      : This function will be called by the transaction find form to get the business
--                date and the store from the store day table for the transaction sequence number.
--
FUNCTION GET_STORE_DAY(O_error_message    IN OUT VARCHAR2,
                       O_business_date    IN OUT SA_STORE_DAY.BUSINESS_DATE%TYPE,
                       O_store            IN OUT SA_STORE_DAY.STORE%TYPE,
                       I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                       I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CREATE_UPDATE_DCLOSE
-- Purpose      : Function will create the day close transaction for the store/day if it does not
--                already exist, otherwise, it will update the day close transaction setting the
--                number of files expected (stored in ref_no1) equal to the number of files loaded.
--                This function is called when the user manually updates the data status to 'Fully Loaded'
--                in the store day summary form.  The number of files expected to be loaded will be stored
--                in reference number 1.  When manually updating the data status to 'Fully Loaded', the
--                number of expected files will be set to equal the number of files already loaded.
--
FUNCTION CREATE_UPDATE_DCLOSE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_store_day_seq_no  IN     SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                              I_files_loaded      IN     SA_STORE_DAY.FILES_LOADED%TYPE,
                              I_store             IN     SA_STORE_DAY.STORE%TYPE,
                              I_day               IN     SA_STORE_DAY.DAY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--Function Name: GET_FILES_EXPECTED
--Purpose:       Function will output the number of files expected to be loaded for a specified
--               store/day.  This value will be found in the ref_no1 column of the DCLOSE transaction.
--               If there isn't a DCLOSE transaction or if the ref_no1 column is NULL for the
--               transaction, a "files known" output variable will be false; otherwise it will be
--               true.  This function will be called in the store/day summary form.
--
FUNCTION GET_FILES_EXPECTED(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_files_known       IN OUT  BOOLEAN,
                            O_files_expected    IN OUT  SA_TRAN_HEAD.REF_NO1%TYPE,
                            I_store_day_seq_no  IN      SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                            I_store             IN      SA_TRAN_HEAD.STORE%TYPE,
                            I_day               IN      SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--Function Name: DELETE_CUST_ATTRIB
--Purpose:       Delete all of the customer attribute records for the specified tran_seq_no,
--               attrib_seq_no, and attrib_type combination. Only the tran_seq_no is required
--               to limit the deletion.
--
FUNCTION DELETE_CUST_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_attrib_type       IN      SA_CUST_ATTRIB.ATTRIB_TYPE%TYPE,
                            I_attrib_seq_no     IN      SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE,
                            I_tran_seq_no       IN      SA_CUST_ATTRIB.TRAN_SEQ_NO%TYPE,
                            I_store             IN      SA_CUST_ATTRIB.STORE%TYPE,
                            I_day               IN      SA_CUST_ATTRIB.DAY%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: Detail_exists
-- Purpose      : Check if records are found on sa_rounding_rule_detail.
------------------------------------------------------------------------------------
FUNCTION DETAIL_EXISTS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists             IN OUT   BOOLEAN,
                       I_rounding_rule_id   IN       SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: GET_VALID_ROUNDING_RULE_ID
-- Purpose      : Returns the Rounding Rule ID for a Currency/Country Combination.
--                Returns null if no rule is defined for the given combination.
------------------------------------------------------------------------------------
FUNCTION GET_VALID_ROUNDING_RULE_ID(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_rounding_rule_id   IN OUT   SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE,
                                    I_currency_code      IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                    I_country_id         IN       COUNTRY.COUNTRY_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: GET_ENDING_AMT
-- Purpose      : Returns the Ending Amount as per defined in
--                SA_ROUNDING_RULES_HEAD/DETAIL
--                If no rule is defined, O_ending_amt = I_amt
------------------------------------------------------------------------------------
FUNCTION GET_ENDING_AMT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_ending_amt         IN OUT   SA_TRAN_TENDER.TENDER_AMT%TYPE,
                        I_amt                IN       SA_TRAN_TENDER.TENDER_AMT%TYPE,
                        I_rounding_rule_id   IN       SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CURRENCY_ROUND
-- Purpose      : Returns the rounded amount based on the rounding rules
--                defined in SA_ROUNDING_RULES_HEAD/DETAIL
------------------------------------------------------------------------------------
FUNCTION CURRENCY_ROUND(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_rounded_amt        IN OUT   SA_TRAN_TENDER.TENDER_AMT%TYPE,
                        I_amt                IN       SA_TRAN_TENDER.TENDER_AMT%TYPE,
                        I_rounding_rule_id   IN       SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE,
                        I_raise_no_rule      IN       VARCHAR2 DEFAULT 'Y')
   return BOOLEAN;

------------------------------------------------------------------------------------
-- Function Name: LOCK_SA_TRAN_TENDER
-- Purpose      : This function will be called by satrdetl.fmb.  Due to a record-locking
--                issue with VPD and forms, a custom on-lock trigger is required to avoid
--                the "FRM-40654: Record has been updated by another user. Re-query to
--                see change." error. This is an issue when a user does not have access
--                to credit card information and attempts to edit a record where the
--                restricted data exists.  The column will be null in the form, but
--                when the form will check before updating the record, it will think
--                the record has been updated by another user (because the table value
--                is actually not null).
------------------------------------------------------------------------------------
FUNCTION LOCK_SA_TRAN_TENDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tran_seq_no          IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE,
                             I_tender_seq_no        IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------------
-- Function Name: LOCK_SA_TRAN_TENDER
-- Purpose      : This function will be called by satrdetl.fmb.  Due to the neccessity of
--                the on-lock trigger. an associated 'on-update' trigger is required to
--                ensure the form does not update the cc_no field to null for users
--                w/o cc access.
------------------------------------------------------------------------------------
FUNCTION ON_UPDATE_SA_TRAN_TENDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store                IN       SA_TRAN_TENDER.STORE%TYPE,
                                  I_day                  IN       SA_TRAN_TENDER.DAY%TYPE,
                                  I_tran_seq_no          IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE,
                                  I_tender_seq_no        IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE,
                                  I_tender_type_group    IN       SA_TRAN_TENDER.TENDER_TYPE_GROUP%TYPE,
                                  I_tender_type_id       IN       SA_TRAN_TENDER.TENDER_TYPE_ID%TYPE,
                                  I_tender_amt           IN       SA_TRAN_TENDER.TENDER_AMT%TYPE,
                                  I_cc_no                IN       SA_TRAN_TENDER.CC_NO%TYPE,
                                  I_cc_exp_date          IN       SA_TRAN_TENDER.CC_EXP_DATE%TYPE,
                                  I_cc_auth_no           IN       SA_TRAN_TENDER.CC_AUTH_NO%TYPE,
                                  I_cc_auth_src          IN       SA_TRAN_TENDER.CC_AUTH_SRC%TYPE,
                                  I_cc_entry_mode        IN       SA_TRAN_TENDER.CC_ENTRY_MODE%TYPE,
                                  I_cc_cardholder_verf   IN       SA_TRAN_TENDER.CC_CARDHOLDER_VERF%TYPE,
                                  I_cc_spec_cond         IN       SA_TRAN_TENDER.CC_SPEC_COND%TYPE,
                                  I_voucher_no           IN       SA_TRAN_TENDER.VOUCHER_NO%TYPE,
                                  I_coupon_no            IN       SA_TRAN_TENDER.COUPON_NO%TYPE,
                                  I_coupon_ref_no        IN       SA_TRAN_TENDER.COUPON_REF_NO%TYPE,
                                  I_ref_no9              IN       SA_TRAN_TENDER.REF_NO9%TYPE,
                                  I_ref_no10             IN       SA_TRAN_TENDER.REF_NO10%TYPE,
                                  I_ref_no11             IN       SA_TRAN_TENDER.REF_NO11%TYPE,
                                  I_ref_no12             IN       SA_TRAN_TENDER.REF_NO12%TYPE,
                                  I_check_acct_no        IN       SA_TRAN_TENDER.CHECK_ACCT_NO%TYPE,
                                  I_check_no             IN       SA_TRAN_TENDER.CHECK_NO%TYPE,
                                  I_identi_method        IN       SA_TRAN_TENDER.IDENTI_METHOD%TYPE,
                                  I_identi_id            IN       SA_TRAN_TENDER.IDENTI_ID%TYPE,
                                  I_orig_currency        IN       SA_TRAN_TENDER.ORIG_CURRENCY%TYPE,
                                  I_orig_curr_amt        IN       SA_TRAN_TENDER.ORIG_CURR_AMT%TYPE,
                                  I_error_ind            IN       SA_TRAN_TENDER.ERROR_IND%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------------------
--- Funtion name:   GET_NEXT_POS_TRAN
--- Purpose     :   This function will return the next POS transaction sequence number for the passed in
---                 store, business day and register.
---
FUNCTION GET_NEXT_POS_TRAN(O_error_message      IN OUT   VARCHAR2,
                           O_exists             IN OUT   BOOLEAN,
                           O_next_tran_seq_no   IN OUT   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_store_day_seq_no   IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                           I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                           I_register           IN       SA_TRAN_HEAD.REGISTER%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- Funtion name:   GET_PREV_POS_TRAN
--- Purpose     :   This function will return the previous POS transaction sequence number for the passed in
---                 store, business day and register.
---
FUNCTION GET_PREV_POS_TRAN(O_error_message      IN OUT   VARCHAR2,
                           O_exists             IN OUT   BOOLEAN,
                           O_prev_tran_seq_no   IN OUT   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_store_day_seq_no   IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                           I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                           I_register           IN       SA_TRAN_HEAD.REGISTER%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Funtion name:   DELETE_TRAN_PAYMENT
--- Purpose     :   This function will delete the SA_TRAN_PAYMENT record for a transaction
---                 which erroneously had a record created during import. 
---
FUNCTION DELETE_TRAN_PAYMENT(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_payment_seq_no    IN      SA_TRAN_PAYMENT.PAYMENT_SEQ_NO%TYPE,
                             I_tran_seq_no       IN      SA_TRAN_PAYMENT.TRAN_SEQ_NO%TYPE,
                             I_store             IN      SA_TRAN_PAYMENT.STORE%TYPE,
                             I_day               IN      SA_TRAN_PAYMENT.DAY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
END TRANSACTION_SQL;
/
