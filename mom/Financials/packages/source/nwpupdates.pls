
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE NWP_UPDATE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------
FUNCTION UPDATE_NWP_RECORD(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_comp_item          IN  ITEM_MASTER.ITEM%TYPE
                           ,I_order_item         IN  ITEM_MASTER.ITEM%TYPE
                           ,I_loc                IN  ORDLOC.LOCATION%TYPE
                           ,I_loc_type           IN  ITEM_LOC.LOC_TYPE%TYPE
                           ,I_receipt_PO         IN  NWP.ORDER_NO%TYPE
                           ,I_receipt_shipment   IN  SHIPMENT.SHIPMENT%TYPE
                           ,I_receipt_date       IN  NWP.RECEIVE_DATE%TYPE
                           ,I_receipt_quantity   IN  ORDLOC.QTY_RECEIVED%TYPE
                           ,I_receipt_cost       IN  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
                           ,I_cost_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_unit_adjust_amt    IN  ORDLOC.UNIT_COST%TYPE
                           ,I_ord_currency       IN  ORDHEAD.CURRENCY_CODE%TYPE
                           ,I_loc_currency       IN  ORDHEAD.CURRENCY_CODE%TYPE
                           ,I_ord_exchange_rate  IN  ORDHEAD.EXCHANGE_RATE%TYPE
                           ,I_order_type         IN  VARCHAR2)
return BOOLEAN;

TYPE comp_item IS RECORD
(
   item                 ITEM_MASTER.ITEM%TYPE,                 --lookup: v_packsku_qty
   qty                  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,       --lookup: v_packsku_qty
   unit_cost_loc        ITEM_LOC_SOH.UNIT_COST%TYPE,           --lookup: ITEMLOC_ATTRIB_SQL
   unit_retail_loc      ITEM_LOC.UNIT_RETAIL%TYPE,             --lookup: ITEMLOC_ATTRIB_SQL
   unit_retail_prim     ITEM_LOC.UNIT_RETAIL%TYPE,             --not currently used
   pack_ind             ITEM_MASTER.PACK_IND%TYPE,             --lookup: item_master
   dept                 ITEM_MASTER.DEPT%TYPE,                 --lookup: item_master
   class                ITEM_MASTER.CLASS%TYPE,                --lookup: item_master
   subclass             ITEM_MASTER.SUBCLASS%TYPE              --lookup: item_master
); -- end comp_item


TYPE comp_item_array IS TABLE of comp_item INDEX BY BINARY_INTEGER;

g_package varchar2(15) := 'NWP_UPDATE_SQL';
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
-- Function Name: FISCAL_YEAR_EXISTS
-- Purpose      : This function will check if fiscal_year already exists in NWP_FREEZE_DATE
--                table.
--------------------------------------------------------------------------------
FUNCTION FISCAL_YEAR_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_fiscal_yr       IN       NWP_FREEZE_DATE.FISCAL_YEAR%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------

END NWP_UPDATE_SQL;
/
