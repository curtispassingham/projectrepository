CREATE OR REPLACE PACKAGE SA_CC_VALIDATE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
--- Function:  CHECK_ALL_NUMBERS
--- Purpose:   Validates the given credit card number has all numeric characters.
-------------------------------------------------------------------------------------
FUNCTION CHECK_ALL_NUMBERS(O_error_message IN OUT VARCHAR2,
                           I_card_no       IN     SA_TRAN_TENDER.CC_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  MASK_CARD_NO
--- Purpose:   Masks the Credit/Debit card number.The first 6 and last 4 will not be 
--             changed and the remaining one will be replaced with the
--             SA_SYSTEM_OPTIONS.CC_NO_MASK_CHAR field character.
-------------------------------------------------------------------------------------
FUNCTION MASK_CARD_NO(O_error_message IN OUT VARCHAR2,
                      O_mask_card_no  IN OUT SA_TRAN_TENDER.CC_NO%TYPE,
                      I_card_no       IN     SA_TRAN_TENDER.CC_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END SA_CC_VALIDATE_SQL;
/