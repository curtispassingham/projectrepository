CREATE OR REPLACE PACKAGE BODY SA_SYSTEM_OPTIONS_SQL AS
--------------------------------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS(O_error_message           IN OUT  VARCHAR2,
                            O_days_before_purge       IN OUT  SA_SYSTEM_OPTIONS.DAYS_BEFORE_PURGE%TYPE,
                            O_day_post_sale           IN OUT  SA_SYSTEM_OPTIONS.DAY_POST_SALE%TYPE,
                            O_balance_level_ind       IN OUT  SA_SYSTEM_OPTIONS.BALANCE_LEVEL_IND%TYPE,
                            O_max_days_compare_dups   IN OUT  SA_SYSTEM_OPTIONS.MAX_DAYS_COMPARE_DUPS%TYPE,
                            O_comp_base_date          IN OUT  SA_SYSTEM_OPTIONS.COMP_BASE_DATE%TYPE,
                            O_comp_no_days            IN OUT  SA_SYSTEM_OPTIONS.COMP_NO_DAYS%TYPE,
                            O_check_dup_miss_tran     IN OUT  SA_SYSTEM_OPTIONS.CHECK_DUP_MISS_TRAN%TYPE,
                            O_unit_of_work            IN OUT  SA_SYSTEM_OPTIONS.UNIT_OF_WORK%TYPE,
                            O_audit_after_imp_ind     IN OUT  SA_SYSTEM_OPTIONS.AUDIT_AFTER_IMP_IND%TYPE,
                            O_fuel_dept               IN OUT  SA_SYSTEM_OPTIONS.FUEL_DEPT%TYPE,
                            O_default_chain           IN OUT  SA_SYSTEM_OPTIONS.DEFAULT_CHAIN%TYPE,
                            O_close_in_order          IN OUT  SA_SYSTEM_OPTIONS.CLOSE_IN_ORDER%TYPE,
                            O_auto_validate_emp_id    IN OUT  SA_SYSTEM_OPTIONS.AUTO_VALIDATE_TRAN_EMPLOYEE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60)   :=  'SA_SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS';

   cursor C_GET_SYSTEM_OPTIONS is
      select days_before_purge,
             day_post_sale,
             balance_level_ind,
             max_days_compare_dups,
             comp_base_date,
             comp_no_days,
             check_dup_miss_tran,
             unit_of_work,
             audit_after_imp_ind,
             fuel_dept,
             default_chain,
             close_in_order,
             auto_validate_tran_employee_id
        from sa_system_options;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_SYSTEM_OPTIONS','SA_SYSTEM_OPTIONS',NULL);
   open C_GET_SYSTEM_OPTIONS;
   SQL_LIB.SET_MARK('FETCH','C_GET_SYSTEM_OPTIONS','SA_SYSTEM_OPTIONS',NULL);
   fetch C_GET_SYSTEM_OPTIONS into O_days_before_purge,
                                   O_day_post_sale,
                                   O_balance_level_ind,
                                   O_max_days_compare_dups,
                                   O_comp_base_date,
                                   O_comp_no_days,
                                   O_check_dup_miss_tran,
                                   O_unit_of_work,
                                   O_audit_after_imp_ind,
                                   O_fuel_dept,
                                   O_default_chain,
                                   O_close_in_order,
                                   O_auto_validate_emp_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_SYSTEM_OPTIONS','SA_SYSTEM_OPTIONS',NULL);
   close C_GET_SYSTEM_OPTIONS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SYSTEM_OPTIONS;
--------------------------------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS(O_error_message           IN OUT  VARCHAR2,
                            O_days_before_purge       IN OUT  SA_SYSTEM_OPTIONS.DAYS_BEFORE_PURGE%TYPE,
                            O_day_post_sale           IN OUT  SA_SYSTEM_OPTIONS.DAY_POST_SALE%TYPE,
                            O_balance_level_ind       IN OUT  SA_SYSTEM_OPTIONS.BALANCE_LEVEL_IND%TYPE,
                            O_max_days_compare_dups   IN OUT  SA_SYSTEM_OPTIONS.MAX_DAYS_COMPARE_DUPS%TYPE,
                            O_comp_base_date          IN OUT  SA_SYSTEM_OPTIONS.COMP_BASE_DATE%TYPE,
                            O_comp_no_days            IN OUT  SA_SYSTEM_OPTIONS.COMP_NO_DAYS%TYPE,
                            O_check_dup_miss_tran     IN OUT  SA_SYSTEM_OPTIONS.CHECK_DUP_MISS_TRAN%TYPE,
                            O_unit_of_work            IN OUT  SA_SYSTEM_OPTIONS.UNIT_OF_WORK%TYPE,
                            O_audit_after_imp_ind     IN OUT  SA_SYSTEM_OPTIONS.AUDIT_AFTER_IMP_IND%TYPE,
                            O_fuel_dept               IN OUT  SA_SYSTEM_OPTIONS.FUEL_DEPT%TYPE,
                            O_default_chain           IN OUT  SA_SYSTEM_OPTIONS.DEFAULT_CHAIN%TYPE,
                            O_close_in_order          IN OUT  SA_SYSTEM_OPTIONS.CLOSE_IN_ORDER%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60)   :=  'SA_SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS';
   L_dummy        VARCHAR(1);

BEGIN

   if GET_SYSTEM_OPTIONS(O_error_message,
                         O_days_before_purge,
                         O_day_post_sale,
                         O_balance_level_ind,
                         O_max_days_compare_dups,
                         O_comp_base_date,
                         O_comp_no_days,
                         O_check_dup_miss_tran,
                         O_unit_of_work,
                         O_audit_after_imp_ind,
                         O_fuel_dept,
                         O_default_chain,
                         O_close_in_order,
                         L_dummy) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SYSTEM_OPTIONS;
----------------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_get_system_options_rec    OUT SA_SYSTEM_OPTIONS%ROWTYPE)
   RETURN BOOLEAN IS
   L_program      VARCHAR2(60)   := 'SA_SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS';

   cursor C_GET_SYSTEM_OPTIONS is
     select *
     from sa_system_options;

BEGIN
  SQL_LIB.SET_MARK('OPEN','C_GET_SYSTEM_OPTIONS','SA_SYSTEM_OPTIONS',NULL);
  OPEN C_GET_SYSTEM_OPTIONS;
  SQL_LIB.SET_MARK('FETCH','C_GET_SYSTEM_OPTIONS','SA_SYSTEM_OPTIONS',NULL);
  FETCH C_GET_SYSTEM_OPTIONS INTO O_GET_SYSTEM_OPTIONS_REC;
  SQL_LIB.SET_MARK('CLOSE','C_GET_SYSTEM_OPTIONS','SA_SYSTEM_OPTIONS',NULL);
  CLOSE C_GET_SYSTEM_OPTIONS;

  RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SYSTEM_OPTIONS;
----------------------------------------------------------------------------------------

FUNCTION GET_DEFAULT_CHAIN(O_error_message IN OUT VARCHAR2,
                           O_default_chain IN OUT SA_SYSTEM_OPTIONS.DEFAULT_CHAIN%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60)   :=  'SA_SYSTEM_OPTIONS_SQL.GET_DEFAULT_CHAIN';

   cursor C_GET_DEFAULT_CHAIN is
      select default_chain
        from sa_system_options;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_DEFAULT_CHAIN','SA_SYSTEM_OPTIONS',NULL);
   open C_GET_DEFAULT_CHAIN;
   SQL_LIB.SET_MARK('FETCH','C_GET_DEFAULT_CHAIN','SA_SYSTEM_OPTIONS',NULL);
   fetch C_GET_DEFAULT_CHAIN into O_default_chain;
   SQL_LIB.SET_MARK('CLOSE','C_GET_DEFAULT_CHAIN','SA_SYSTEM_OPTIONS',NULL);
   close C_GET_DEFAULT_CHAIN;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_CHAIN;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_BAL_LEVEL(O_error_message     IN OUT VARCHAR2,
                       O_balance_level_ind IN OUT SA_SYSTEM_OPTIONS.BALANCE_LEVEL_IND%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) :=  'SA_SYSTEM_OPTIONS_SQL.GET_BAL_LEVEL';

   cursor C_GET_BAL_LEVEL is
      select balance_level_ind
        from sa_system_options;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_BAL_LEVEL','SA_SYSTEM_OPTIONS',NULL);
   open C_GET_BAL_LEVEL;
   SQL_LIB.SET_MARK('FETCH','C_GET_BAL_LEVEL','SA_SYSTEM_OPTIONS',NULL);
   fetch C_GET_BAL_LEVEL into O_balance_level_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_BAL_LEVEL','SA_SYSTEM_OPTIONS',NULL);
   close C_GET_BAL_LEVEL;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_BAL_LEVEL;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_AUDIT_AFTER_IMP_IND
         (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
          O_audit_after_imp_ind  IN OUT SA_SYSTEM_OPTIONS.AUDIT_AFTER_IMP_IND%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60)   :=  'SA_SYSTEM_OPTIONS_SQL.GET_AUDIT_AFTER_IMP_IND';

   cursor C_GET_AUDIT_AFTER_IMP_IND is
      select audit_after_imp_ind
        from sa_system_options;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_AUDIT_AFTER_IMP_IND','SA_SYSTEM_OPTIONS',NULL);
   open C_GET_AUDIT_AFTER_IMP_IND;
   SQL_LIB.SET_MARK('FETCH','C_GET_AUDIT_AFTER_IMP_IND','SA_SYSTEM_OPTIONS',NULL);
   fetch C_GET_AUDIT_AFTER_IMP_IND into O_audit_after_imp_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_AUDIT_AFTER_IMP_IND','SA_SYSTEM_OPTIONS',NULL);
   close C_GET_AUDIT_AFTER_IMP_IND;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_AUDIT_AFTER_IMP_IND;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_CC_NO_MASK_CHAR(O_error_message     IN OUT VARCHAR2,
                             O_cc_no_mask_char   IN OUT SA_SYSTEM_OPTIONS.CC_NO_MASK_CHAR%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) :=  'SA_SYSTEM_OPTIONS_SQL.GET_CC_NO_MASK_CHAR';

   cursor C_GET_CC_NO_MASK_CHAR is
      select cc_no_mask_char
        from sa_system_options;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_CC_NO_MASK_CHAR','SA_SYSTEM_OPTIONS',NULL);
   open C_GET_CC_NO_MASK_CHAR;
   SQL_LIB.SET_MARK('FETCH','C_GET_CC_NO_MASK_CHAR','SA_SYSTEM_OPTIONS',NULL);
   fetch C_GET_CC_NO_MASK_CHAR into O_cc_no_mask_char;
   SQL_LIB.SET_MARK('CLOSE','C_GET_CC_NO_MASK_CHAR','SA_SYSTEM_OPTIONS',NULL);
   close C_GET_CC_NO_MASK_CHAR;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CC_NO_MASK_CHAR;
-----------------------------------------------------------------------------------------------------
END SA_SYSTEM_OPTIONS_SQL;
/
