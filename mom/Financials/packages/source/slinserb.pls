
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

CREATE OR REPLACE PACKAGE BODY STKLEDGR_INSERTS_SQL AS
----------------------------------------------------
FUNCTION ADD_LOC (I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                  I_location        IN       ITEM_LOC.LOC%TYPE,
                  I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
                  O_counter         IN OUT   NUMBER,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_cal                NUMBER(1);
   L_set_of_books_id    fif_gl_setup.set_of_books_id%TYPE;
   L_set_of_books_desc  fif_gl_setup.set_of_books_desc%TYPE;
   L_half_no            period.half_no%TYPE                              := 0;
   L_month_no           system_variables.last_eom_month%TYPE             := NULL;
   L_week_no            system_variables.last_eom_week%TYPE              := NULL;
   L_eow_date           system_variables.last_eom_date%TYPE              := NULL;
   L_vdate              period.vdate%TYPE                                := GET_VDATE;
   L_code               system_options.stock_ledger_time_level_code%TYPE := NULL;
   L_last_eom           system_variables.last_eom_date%TYPE              := NULL;
   L_multi_curr         system_options.multi_currency_ind%TYPE           := NULL;
   L_eom                DATE;
   L_in_day             NUMBER(2);
   L_in_month           NUMBER(2);
   L_in_year            NUMBER(4);
   L_out_day            NUMBER(2);
   L_out_month          NUMBER(2);
   L_out_year           NUMBER(4);
   L_half               NUMBER(1);
   L_this_half          NUMBER(5);
   L_next_half          NUMBER(5);
   L_no_halves          NUMBER(1);
   L_prev_half          NUMBER(5);
   L_primary_currency system_options.currency_code%TYPE          := NULL;

   L_error_message  VARCHAR2(255)   := NULL;
   L_return_code    VARCHAR2(5)     := 'TRUE';
   L_function       VARCHAR2(20)    := NULL;
   L_program        VARCHAR2(50)    := 'STKLEDGR_INSERTS_SQL.ADD_LOC';
   L_cursor         VARCHAR2(20)    := NULL;
   DATE_FAILED      EXCEPTION;

   cursor C_system is
      select so.stock_ledger_time_level_code,
             sv.last_eom_date,
             sv.last_eom_next_half_no,
             p.half_no,
             p.next_half_no,
             DECODE(so.calendar_454_ind,'C',1,0),
             so.currency_code
        from system_options so,
             system_variables sv,
             period p;

BEGIN

   /* determine if using multiple currencies */
   if SYSTEM_OPTIONS_SQL.MULTI_CURRENCY_IND(O_error_message,
                                            L_multi_curr) = FALSE then
      return FALSE;
   end if;

   /* Get Set Of Books ID for input location */
   if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                   L_set_of_books_desc,
                                   L_set_of_books_id,
                                   I_location,
                                   I_loc_type) = FALSE then
      return FALSE;
   end if;


   /* fetch system variables */

   /* batch error handling */
   SQL_LIB.SET_MARK('OPEN',
                    'C_system',
                    'system_options, system_variables, period',
                    NULL);
   open C_system;

   SQL_LIB.SET_MARK('FETCH',
                    'C_system',
                    'system_options, system_variables, period',
                    NULL);
   fetch C_system into L_code,
                       L_last_eom,
                       L_half_no,
                       L_this_half,
                       L_next_half,
                       L_cal,
                       L_primary_currency;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_system',
                    'system_options, system_variables, period',
                    NULL);
   if C_system%NOTFOUND then
      close C_system;
      L_cursor := 'C_system';
      raise NO_DATA_FOUND;
   end if;
   close C_system;

   L_in_day := TO_NUMBER(TO_CHAR((L_last_eom + 1),'DD'),'09');
   L_in_month := TO_NUMBER(TO_CHAR((L_last_eom + 1),'MM'),'09');
   L_in_year := TO_NUMBER(TO_CHAR((L_last_eom + 1),'YYYY'),'0999');


   /* calculate half_no, month_no, and eom_date for data currently
        due for processing */

   if L_cal = 1 then
      CAL_TO_CAL_HALF(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_half_no,
                      L_month_no,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_CAL_HALF';
         raise DATE_FAILED;
      end if;

      L_this_half := L_half_no;

      if (MOD(L_this_half,10) - 1) = 1 then
         L_next_half := L_this_half + 9;
      else
         L_next_half := L_this_half + 1;
      end if;

      CAL_TO_CAL_LDOM(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_out_day,
                      L_out_month,
                      L_out_year,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_CAL_LDOM';
         raise DATE_FAILED;
      end if;
   else
      CAL_TO_454_HALF(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_half_no,
                      L_month_no,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_454_HALF';
         raise DATE_FAILED;
      end if;

      CAL_TO_454_LDOM(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_out_day,
                      L_out_month,
                      L_out_year,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_454_LDOM';
         raise DATE_FAILED;
      end if;
   end if;

   /* if L_half_no differs from L_this_half, assume that the last half
      was not closed yet.  This means that the budget tables need data
      inserted into them for last half, this half, and next half.
      Otherwise, just this half and next half are needed.
   */
   if L_half_no < L_this_half then
      L_no_halves := 2;
      L_prev_half := L_half_no;
   else
      L_no_halves := 1;
   end if;

   L_eom := TO_DATE(TO_CHAR(L_out_day,'09')||TO_CHAR(L_out_month,'09')||
                    TO_CHAR(L_out_year,'0999'),'DDMMYYYY');


   /* insert empty rows into month_data for new location and all
         dept/class/subclass combinations */

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'month_data',
                    'dept, ' ||
                    'class, ' ||
                    'subclass, ' ||
                    'LOC_TYPE: '||I_loc_type ||', ' ||
                    'LOCATION: '||to_char(I_location) || ', ' ||
                    'HALF: '||to_char(L_half_no) || ', ' ||
                    'MONTH: '||to_char(L_month_no) || ', ' ||
                    'from subclass ' ||
                    'where dept != 9999');
   insert into month_data (set_of_books_id,
                           dept,
                           class,
                           subclass,
                           loc_type,
                           location,
                           half_no,
                           month_no,
                           currency_ind,
                           eom_date)
                    select L_set_of_books_id,
                           dept,
                           class,
                           subclass,
                           I_loc_type,
                           I_location,
                           L_half_no,
                           L_month_no,
                           'L',  --always insert a Local record
                           L_eom
                      from subclass
                     where dept != 9999;
   O_counter := O_counter + SQL%ROWCOUNT;

   --- insert a record for primary currency if local != primary
   if L_multi_curr = 'Y' and (L_primary_currency != I_currency_code) then
      insert into month_data (set_of_books_id,
                              dept,
                              class,
                              subclass,
                              loc_type,
                              location,
                              half_no,
                              month_no,
                              currency_ind,
                              eom_date)
                       select L_set_of_books_id,
                              dept,
                              class,
                              subclass,
                              I_loc_type,
                              I_location,
                              L_half_no,
                              L_month_no,
                              'P',  --add a primary record
                              L_eom
                         from subclass
                        where dept != 9999;
      O_counter := O_counter + SQL%ROWCOUNT;
   end if;


   /* if system_options.stock_ledger_time_level_code = 'W' then insert
         rows into week_data for new location and all
         dept/class/subclass combinations
      THIS IS ONLY SUPPORTED FOR THE 454 CALENDAR! */

   if L_code = 'W' and L_cal = 0 then
         L_eow_date := L_last_eom;
      LOOP
         L_eow_date := L_eow_date + 7;

         L_in_day := TO_NUMBER(TO_CHAR(L_eow_date,'DD'),'09');
         L_in_month := TO_NUMBER(TO_CHAR(L_eow_date,'MM'),'09');
         L_in_year := TO_NUMBER(TO_CHAR(L_eow_date,'YYYY'),'0999');

         CAL_TO_454_HALF(L_in_day,
                         L_in_month,
                         L_in_year,
                         L_half_no,
                         L_month_no,
                         L_return_code,
                         L_error_message);
         if L_return_code = 'FALSE' then
            L_function := 'CAL_TO_454_HALF';
            raise DATE_FAILED;
         end if;
         CAL_TO_454(L_in_day,
                    L_in_month,
                    L_in_year,
                    L_out_day,
                    L_week_no,
                    L_out_month,
                    L_out_year,
                    L_return_code,
                    L_error_message);
         if L_return_code = 'FALSE' then
            L_function := 'CAL_TO_454';
            raise DATE_FAILED;
         end if;

         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'week_data',
                          'SELECT ' ||
                          'dept, '||
                          'class, '||
                          'subclass, '||
                          'LOC_TYPE: '||I_loc_type ||', ' ||
                          'LOCATION: '||to_char(I_location) || ', ' ||
                          'HALF: '||to_char(L_half_no) || ',' ||
                          'MONTH: '||to_char(L_month_no) || ',' ||
                          'WEEK: '||to_char(L_week_no) || ',' ||
                          'EOW_DATE: '||to_char(L_eow_date) || ',' ||
                          'from subclass');
         insert into week_data (set_of_books_id,
                                dept,
                                class,
                                subclass,
                                loc_type,
                                location,
                                half_no,
                                month_no,
                                week_no,
                                currency_ind,
                                eow_date)
                         select L_set_of_books_id,
                                dept,
                                class,
                                subclass,
                                I_loc_type,
                                I_location,
                                L_half_no,
                                L_month_no,
                                L_week_no,
                                'L',  -- always insert a local record
                                L_eow_date
                           from subclass
                          where dept != 9999;
         O_counter := O_counter + SQL%ROWCOUNT;
         ---insert a primary record if local != primary
         if L_multi_curr = 'Y' and (L_primary_currency != I_currency_code) then
            insert into week_data (set_of_books_id,
                                   dept,
                                   class,
                                   subclass,
                                   loc_type,
                                   location,
                                   half_no,
                                   month_no,
                                   week_no,
                                   currency_ind,
                                   eow_date)
                            select L_set_of_books_id,
                                   dept,
                                   class,
                                   subclass,
                                   I_loc_type,
                                   I_location,
                                   L_half_no,
                                   L_month_no,
                                   L_week_no,
                                   'P',  -- insert a primary record
                                   L_eow_date
                              from subclass
                             where dept != 9999;
            O_counter := O_counter + SQL%ROWCOUNT;
         end if;

         if L_eow_date >= L_vdate then
            exit;
         end if;

      END LOOP;
   end if;


   /* insert rows into half_data for the new locations and all
         dept/class/subclass combinations for the next half */

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'half_data',
                    'SELECT dept, ' ||
                     'class, ' ||
                     'subclass, ' ||
                     'LOC_TYPE: '||I_loc_type ||', ' ||
                     'LOCATION: '||to_char(I_location) || ', ' ||
                     'HALF: '||to_char(L_half_no) || ', ' ||
                     'from subclass ' ||
                     'where dept != 9999');
   insert into half_data (set_of_books_id,
                          dept,
                          class,
                          subclass,
                          loc_type,
                          location,
                          half_no)
                   select L_set_of_books_id,
                          dept,
                          class,
                          subclass,
                          I_loc_type,
                          I_location,
                          L_half_no
                     from subclass
                    where dept != 9999;
   O_counter := O_counter + SQL%ROWCOUNT;

   if L_cal = 1 then
      HALF_TO_CAL_FDOH(L_this_half,
                       L_out_day,
                       L_out_month,
                       L_out_year,
                       L_return_code,
                       L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'HALF_TO_CAL_FDOH';
         raise DATE_FAILED;
      end if;
   else
      HALF_TO_454_FDOH(L_this_half,
                       L_out_day,
                       L_out_month,
                       L_out_year,
                       L_return_code,
                       L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'HALF_TO_454_FDOH';
         raise DATE_FAILED;
      end if;
   end if;

   L_eom := TO_DATE(TO_CHAR(L_out_day,'09')||TO_CHAR(L_out_month,'09')||
                    TO_CHAR(L_out_year,'0999'),'DDMMYYYY');

   FOR L_half in 0..L_no_halves LOOP
      if L_half = 1 then
         L_half_no := L_next_half;
      elsif L_half = 0 then
         L_half_no := L_this_half;
      else
         L_half_no := L_prev_half;
      end if;

      FOR L_month_no in 1..6 LOOP
         L_in_day := TO_NUMBER(TO_CHAR((L_eom + 1),'DD'),'09');
         L_in_month := TO_NUMBER(TO_CHAR((L_eom + 1),'MM'),'09');
         L_in_year := TO_NUMBER(TO_CHAR((L_eom + 1),'YYYY'),'0999');

         if L_cal = 1 then
            CAL_TO_CAL_LDOM(L_in_day,
                            L_in_month,
                            L_in_year,
                            L_out_day,
                            L_out_month,
                            L_out_year,
                            L_return_code,
                            L_error_message);
            if L_return_code = 'FALSE' then
               L_function := 'CAL_TO_CAL_LDOM';
               raise DATE_FAILED;
            end if;
         else
            CAL_TO_454_LDOM(L_in_day,
                            L_in_month,
                            L_in_year,
                            L_out_day,
                            L_out_month,
                            L_out_year,
                            L_return_code,
                            L_error_message);
            if L_return_code = 'FALSE' then
               L_function := 'CAL_TO_454_LDOM';
               raise DATE_FAILED;
            end if;
         end if;

         L_eom := TO_DATE(TO_CHAR(L_out_day,'09')||TO_CHAR(L_out_month,'09')||
                          TO_CHAR(L_out_year,'0999'),'DDMMYYYY');

         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'month_data_budget',
                          'SELECT dept, ' ||
                           'MONTH: '||to_char(L_month_no) || ', ' ||
                           'HALF: '||to_char(L_half_no) || ', ' ||
                           'LOC_TYPE: '||I_loc_type ||', ' ||
                           'LOCATION: '||to_char(I_location) || ', ' ||
                           'from deps ' ||
                           'where dept != 9999 ' );
         insert into month_data_budget (set_of_books_id,
                                        dept,
                                        month_no,
                                        half_no,
                                        loc_type,
                                        location)
                                 select L_set_of_books_id,
                                        dept,
                                        L_month_no,
                                        L_half_no,
                                        I_loc_type,
                                        I_location
                                   from deps
                                  where dept != 9999;

         O_counter := O_counter + SQL%ROWCOUNT;

      END LOOP; -- end all months

      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'half_data_budget',
                       ', SELECT dept, ' ||
                       'HALF: '||to_char(L_half_no) || ', ' ||
                       'LOC_TYPE: '||I_loc_type ||', ' ||
                       'LOCATION: '||to_char(I_location) || ', ' ||
                       'from deps ' ||
                       'where dept != 9999 ' );
      insert into half_data_budget (set_of_books_id,
                                    dept,
                                    half_no,
                                    loc_type,
                                    location)
                             select L_set_of_books_id,
                                    dept,
                                    L_half_no,
                                    I_loc_type,
                                    I_location
                               from deps
                              where dept != 9999;
      O_counter := O_counter + SQL%ROWCOUNT;

   END LOOP;  -- all halfs

   RETURN TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURSOR',
                                            L_cursor,
                                            L_program,
                                            NULL);
      return FALSE;

   when DATE_FAILED then
      O_error_message := SQL_LIB.CREATE_MSG('STKLEDGR_DATE',
                                            L_function,
                                            L_program,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_LOC;
------------------------------------------------------------------------------
FUNCTION ADD_SUBCLASS (I_dept            IN       SUBCLASS.DEPT%TYPE,
                       I_class           IN       SUBCLASS.CLASS%TYPE,
                       I_subclass        IN       SUBCLASS.SUBCLASS%TYPE,
                       O_counter         IN OUT   NUMBER,
                       O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_cal                 NUMBER(1);
   L_last_eom            SYSTEM_VARIABLES.LAST_EOM_DATE%TYPE                := NULL;
   L_half_no             PERIOD.HALF_NO%TYPE                                := NULL;
   L_month_no            SYSTEM_VARIABLES.LAST_EOM_MONTH%TYPE               := NULL;
   L_week_no             NUMBER(1);                                         
   L_code                SYSTEM_OPTIONS.STOCK_LEDGER_TIME_LEVEL_CODE%TYPE   := NULL;
   L_eow_date            SYSTEM_VARIABLES.LAST_EOM_DATE%TYPE                := NULL;
   L_vdate               PERIOD.VDATE%TYPE                                  := GET_VDATE;
   L_multi_curr          SYSTEM_OPTIONS.MULTI_CURRENCY_IND%TYPE             := NULL;
   L_in_day              NUMBER(2);                                         
   L_in_month            NUMBER(2);                                         
   L_in_year             NUMBER(4);                                         
   L_out_day             NUMBER(2);                                         
   L_out_month           NUMBER(2);                                         
   L_out_year            NUMBER(4);                                         
   L_eom                 DATE;                                              
   L_primary_currency    SYSTEM_OPTIONS.CURRENCY_CODE%TYPE                  := NULL;
                                                                            
   L_return_code         VARCHAR2(5)                                        := 'TRUE';
   L_error_message       VARCHAR2(255)                                      := NULL;
   L_function            VARCHAR2(20)                                       := NULL;
   L_program             VARCHAR(50)                                        := 'STKLEDGR_INSERTS_SQL.ADD_SUBCLASS';
   L_cursor              VARCHAR(20)                                        := NULL;
   L_loc_type            VARCHAR2(1)                                        := NULL;
   DATE_FAILED           EXCEPTION;

   cursor C_system is
      select so.stock_ledger_time_level_code,
             sv.last_eom_date,
             sv.last_eom_next_half_no,
             DECODE(so.calendar_454_ind,'C',1,0),
             so.currency_code
        from system_options so,
             system_variables sv;

   cursor C_all_store is
      select store,
             currency_code,
             NVL(vls.set_of_books_id, -1) set_of_books_id
        from store s,
             mv_loc_sob vls
       where vls.location_type = 'S'
         and vls.location      = s.store
         and not (s.store_type = 'F' AND s.stockholding_ind = 'N');

   cursor C_all_wh_multi_ch is
      select wh,
             currency_code,
             NVL(vls.set_of_books_id, -1) set_of_books_id
        from wh w,
             mv_loc_sob vls
       where vls.location_type = 'W'
         and vls.location      = w.wh
         and w.wh             != w.physical_wh
         and w.stockholding_ind = 'Y'
         and w.finisher_ind     = 'N';   

   cursor C_all_external_finisher is
      select partner_id,
             currency_code,
             NVL(vls.set_of_books_id, -1) set_of_books_id
        from partner prt,
             mv_loc_sob vls
       where vls.location_type = 'E'
         and vls.location      = prt.partner_id
         and partner_type      = 'E';

BEGIN

   /* determine if using multiple currencies */
   if SYSTEM_OPTIONS_SQL.MULTI_CURRENCY_IND(O_error_message,
                                            L_multi_curr) = FALSE then
      return FALSE;
   end if;

   /* fetch system variables */
   SQL_LIB.SET_MARK('OPEN',
                    'C_system',
                    'system_options, system_variables, period',
                    NULL);
   open C_system;

   SQL_LIB.SET_MARK('FETCH',
                    'C_system',
                    'system_options, system_variables, period',
                    NULL);
   fetch C_system into L_code,
                       L_last_eom,
                       L_half_no,
                       L_cal,
                       L_primary_currency;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_system',
                    'system_options, system_variables, period',
                    NULL);
   if C_system%NOTFOUND then
      close C_system;
      L_cursor := 'C_system';
      raise NO_DATA_FOUND;
   end if;
   close C_system;

   L_in_day   := TO_NUMBER(TO_CHAR((L_last_eom + 1),'DD'),'09');
   L_in_month := TO_NUMBER(TO_CHAR((L_last_eom + 1),'MM'),'09');
   L_in_year  := TO_NUMBER(TO_CHAR((L_last_eom + 1),'YYYY'),'0999');


   /* calculate half_no, month_no, and eom_date for data currently due
         for processing */

   if L_cal = 1 then
      CAL_TO_CAL_HALF(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_half_no,
                      L_month_no,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_CAL_HALF';
         raise DATE_FAILED;
      end if;

      CAL_TO_CAL_LDOM(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_out_day,
                      L_out_month,
                      L_out_year,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_CAL_LDOM';
         raise DATE_FAILED;
      end if;
   else
      CAL_TO_454_HALF(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_half_no,
                      L_month_no,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_454_HALF';
         raise DATE_FAILED;
      end if;

      CAL_TO_454_LDOM(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_out_day,
                      L_out_month,
                      L_out_year,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_454_LDOM';
         raise DATE_FAILED;
      end if;
   end if;

   L_eom := TO_DATE(TO_CHAR(L_out_day,'09')||TO_CHAR(L_out_month,'09')||
                    TO_CHAR(L_out_year,'0999'),'DDMMYYYY');

   /* insert empty rows into month_data for new subclass and all
         locations */
   L_loc_type := 'S';
   FOR C_all_store_rec in C_all_store LOOP
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'month_data',
                       'DEPT: '||TO_CHAR(I_dept)||
                       ', CLASS: '||TO_CHAR(I_class)||
                       ', SUBCLASS: '||TO_CHAR(I_subclass)||
                       ', LOC_TYPE: '||L_loc_type||
                       ', LOCATION: '||TO_CHAR(C_all_store_rec.store)||
                       ', HALF: '||TO_CHAR(L_half_no)||
                       ', MONTH: '||TO_CHAR(L_month_no));
      BEGIN
         insert into month_data (set_of_books_id,
                                 dept,
                                 class,
                                 subclass,
                                 loc_type,
                                 location,
                                 half_no,
                                 month_no,
                                 currency_ind,
                                 eom_date)
                         values (C_all_store_rec.set_of_books_id,
                                 I_dept,
                                 I_class,
                                 I_subclass,
                                 L_loc_type,
                                 C_all_store_rec.store,
                                 L_half_no,
                                 L_month_no,
                                 'L', -- always insert a local record
                                 L_eom);
         O_counter := O_counter + 1;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            NULL;
      END;

      --insert a primary record if local != primary
      if L_multi_curr = 'Y' and (L_primary_currency != C_all_store_rec.currency_code) then
         BEGIN
            insert into month_data (set_of_books_id,
                                    dept,
                                    class,
                                    subclass,
                                    loc_type,
                                    location,
                                    half_no,
                                    month_no,
                                    currency_ind,
                                    eom_date)
                            values (C_all_store_rec.set_of_books_id,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    L_loc_type,
                                    C_all_store_rec.store,
                                    L_half_no,
                                    L_month_no,
                                    'P', -- insert a primary record
                                    L_eom);
            O_counter := O_counter + 1;
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               NULL;
         END;
      end if;
   END LOOP;

   L_loc_type := 'W';
   FOR C_all_wh_rec in C_all_wh_multi_ch LOOP
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'month_data',
                       'DEPT: '||TO_CHAR(I_dept)||
                       ', CLASS: '||TO_CHAR(I_class)||
                       ', SUBCLASS: '||TO_CHAR(I_subclass)||
                       ', LOC_TYPE: '||L_loc_type||
                       ', LOCATION: '||TO_CHAR(C_all_wh_rec.wh)||
                       ', HALF: '||TO_CHAR(L_half_no)||
                       ', MONTH: '||TO_CHAR(L_month_no));
      BEGIN
         insert into month_data (set_of_books_id,
                                 dept,
                                 class,
                                 subclass,
                                 loc_type,
                                 location,
                                 half_no,
                                 month_no,
                                 currency_ind,
                                 eom_date)
                         values (C_all_wh_rec.set_of_books_id,
                                 I_dept,
                                 I_class,
                                 I_subclass,
                                 L_loc_type,
                                 C_all_wh_rec.wh,
                                 L_half_no,
                                 L_month_no,
                                 'L', -- always insert a local record
                                 L_eom);
         O_counter := O_counter + 1;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            NULL;
      END;

      --insert a primary record if local != primary
      if L_multi_curr = 'Y' and (L_primary_currency != C_all_wh_rec.currency_code) then
         BEGIN
            insert into month_data (set_of_books_id,
                                    dept,
                                    class,
                                    subclass,
                                    loc_type,
                                    location,
                                    half_no,
                                    month_no,
                                    currency_ind,
                                    eom_date)
                            values (C_all_wh_rec.set_of_books_id,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    L_loc_type,
                                    C_all_wh_rec.wh,
                                    L_half_no,
                                    L_month_no,
                                    'P', -- insert a primary record
                                    L_eom);
            O_counter := O_counter + 1;
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               NULL;
         END;
      end if;
   END LOOP;

   L_loc_type := 'E';
   FOR C_all_external_finisher_rec in C_all_external_finisher LOOP
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'month_data',
                       'DEPT: '||TO_CHAR(I_dept)||
                       ', CLASS: '||TO_CHAR(I_class)||
                       ', SUBCLASS: '||TO_CHAR(I_subclass)||
                       ', LOC_TYPE: '||L_loc_type||
                       ', LOCATION: '||C_all_external_finisher_rec.partner_id||
                       ', HALF: '||TO_CHAR(L_half_no)||
                       ', MONTH: '||TO_CHAR(L_month_no));
      BEGIN
         insert into month_data (set_of_books_id,
                                 dept,
                                 class,
                                 subclass,
                                 loc_type,
                                 location,
                                 half_no,
                                 month_no,
                                 currency_ind,
                                 eom_date)
                         values (C_all_external_finisher_rec.set_of_books_id,
                                 I_dept,
                                 I_class,
                                 I_subclass,
                                 L_loc_type,
                                 TO_NUMBER(C_all_external_finisher_rec.partner_id),
                                 L_half_no,
                                 L_month_no,
                                 'L', -- always insert a local record
                                 L_eom);
         O_counter := O_counter + 1;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            NULL;
      END;

      --insert a primary record if local != primary
      if L_multi_curr = 'Y' and (L_primary_currency != C_all_external_finisher_rec.currency_code) then
         BEGIN
            insert into month_data (set_of_books_id,
                                    dept,
                                    class,
                                    subclass,
                                    loc_type,
                                    location,
                                    half_no,
                                    month_no,
                                    currency_ind,
                                    eom_date)
                            values (C_all_external_finisher_rec.set_of_books_id,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    L_loc_type,
                                    TO_NUMBER(C_all_external_finisher_rec.partner_id),
                                    L_half_no,
                                    L_month_no,
                                    'P', -- insert a primary record
                                    L_eom);
            O_counter := O_counter + 1;
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               NULL;
         END;
      end if;
   END LOOP;
    /* if system_options.stock_ledger_time_level_code = 'W' and the
         calendar used is 454 then inserts rows into week_data for
         the new subclass and all locations from beginning of month */

   if L_code = 'W' and L_cal = 0 then
      L_eow_date := L_last_eom;

      LOOP
         L_eow_date := L_eow_date + 7;

         L_in_day   := TO_NUMBER(TO_CHAR(L_eow_date,'DD'),'09');
         L_in_month := TO_NUMBER(TO_CHAR(L_eow_date,'MM'),'09');
         L_in_year  := TO_NUMBER(TO_CHAR(L_eow_date,'YYYY'),'0999');

         CAL_TO_454_HALF(L_in_day,
                         L_in_month,
                         L_in_year,
                         L_half_no,
                         L_month_no,
                         L_return_code,
                         L_error_message);
         if L_return_code = 'FALSE' then
            L_function := 'CAL_TO_454_HALF';
            raise DATE_FAILED;
         end if;

         CAL_TO_454(L_in_day,
                    L_in_month,
                    L_in_year,
                    L_out_day,
                    L_week_no,
                    L_out_month,
                    L_out_year,
                    L_return_code,
                    L_error_message);
         if L_return_code = 'FALSE' then
            L_function := 'CAL_TO_454';
            raise DATE_FAILED;
         end if;

         L_loc_type := 'S';
         FOR C_all_store_rec in C_all_store LOOP
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'week_data',
                             'DEPT: '||TO_CHAR(I_dept)||
                             ', CLASS: '||TO_CHAR(I_class)||
                             ', SUBCLASS: '||TO_CHAR(I_subclass)||
                             ', LOC_TYPE: '||L_loc_type||
                             ', LOCATION: '||TO_CHAR(C_all_store_rec.store)||
                             ', HALF: '||TO_CHAR(L_half_no)||
                             ', MONTH: '||TO_CHAR(L_month_no)||
                             ', WEEK: '||TO_CHAR(L_week_no));
            BEGIN
               insert into week_data (set_of_books_id,
                                      dept,
                                      class,
                                      subclass,
                                      loc_type,
                                      location,
                                      half_no,
                                      month_no,
                                      week_no,
                                      currency_ind,
                                      eow_date)
                              values (C_all_store_rec.set_of_books_id,
                                      I_dept,
                                      I_class,
                                      I_subclass,
                                      L_loc_type,
                                      C_all_store_rec.store,
                                      L_half_no,
                                      L_month_no,
                                      L_week_no,
                                      'L',  --always insert a local record
                                      L_eow_date);
               O_counter := O_counter + 1;
            EXCEPTION
               when DUP_VAL_ON_INDEX then
                  NULL;
            END;

            --insert a primary record if local != primary
            if L_multi_curr = 'Y' and (L_primary_currency != C_all_store_rec.currency_code) then
               BEGIN
                  insert into week_data (set_of_books_id,
                                         dept,
                                         class,
                                         subclass,
                                         loc_type,
                                         location,
                                         half_no,
                                         month_no,
                                         week_no,
                                         currency_ind,
                                         eow_date)
                                 values (C_all_store_rec.set_of_books_id,
                                         I_dept,
                                         I_class,
                                         I_subclass,
                                         L_loc_type,
                                         C_all_store_rec.store,
                                         L_half_no,
                                         L_month_no,
                                         L_week_no,
                                         'P',  --insert a primary record
                                         L_eow_date);
                  O_counter := O_counter + 1;
               EXCEPTION
                  when DUP_VAL_ON_INDEX then
                     NULL;
               END;
            end if;
         END LOOP;

         L_loc_type := 'W';
         FOR C_all_wh_rec in C_all_wh_multi_ch LOOP -- use only virutal whs
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'week_data',
                             'DEPT: '||TO_CHAR(I_dept)||
                             ', CLASS: '||TO_CHAR(I_class)||
                             ', SUBCLASS: '||TO_CHAR(I_subclass)||
                             ', LOC_TYPE: '||L_loc_type||
                             ', LOCATION: '||TO_CHAR(C_all_wh_rec.wh)||
                             ', HALF: '||TO_CHAR(L_half_no)||
                             ', MONTH: '||TO_CHAR(L_month_no)||
                             ', WEEK: '||TO_CHAR(L_week_no));
            BEGIN
               insert into week_data (set_of_books_id,
                                      dept,
                                      class,
                                      subclass,
                                      loc_type,
                                      location,
                                      half_no,
                                      month_no,
                                      week_no,
                                      currency_ind,
                                      eow_date)
                              values (C_all_wh_rec.set_of_books_id,
                                      I_dept,
                                      I_class,
                                      I_subclass,
                                      L_loc_type,
                                      C_all_wh_rec.wh,
                                      L_half_no,
                                      L_month_no,
                                      L_week_no,
                                      'L', --always insert a local record
                                      L_eow_date);
               O_counter := O_counter + 1;
            EXCEPTION
               when DUP_VAL_ON_INDEX then
                     NULL;
            END;

            --insert a primary record if local != primary
            if L_multi_curr = 'Y' and (L_primary_currency != C_all_wh_rec.currency_code) then
               BEGIN
                  insert into week_data (set_of_books_id,
                                         dept,
                                         class,
                                         subclass,
                                         loc_type,
                                         location,
                                         half_no,
                                         month_no,
                                         week_no,
                                         currency_ind,
                                         eow_date)
                                 values (C_all_wh_rec.set_of_books_id,
                                         I_dept,
                                         I_class,
                                         I_subclass,
                                         L_loc_type,
                                         C_all_wh_rec.wh,
                                         L_half_no,
                                         L_month_no,
                                         L_week_no,
                                         'P',  --insert a primary record
                                         L_eow_date);
                  O_counter := O_counter + 1;
               EXCEPTION
                  when DUP_VAL_ON_INDEX then
                     NULL;
               END;
            end if;
         END LOOP;

         L_loc_type := 'E';
         FOR C_all_external_finisher_rec in C_all_external_finisher LOOP
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'week_data',
                             'DEPT: '||TO_CHAR(I_dept)||
                             ', CLASS: '||TO_CHAR(I_class)||
                             ', SUBCLASS: '||TO_CHAR(I_subclass)||
                             ', LOC_TYPE: '||L_loc_type||
                             ', LOCATION: '||C_all_external_finisher_rec.partner_id||
                             ', HALF: '||TO_CHAR(L_half_no)||
                             ', MONTH: '||TO_CHAR(L_month_no)||
                             ', WEEK: '||TO_CHAR(L_week_no));
            BEGIN
               insert into week_data (set_of_books_id,
                                      dept,
                                      class,
                                      subclass,
                                      loc_type,
                                      location,
                                      half_no,
                                      month_no,
                                      week_no,
                                      currency_ind,
                                      eow_date)
                              values (C_all_external_finisher_rec.set_of_books_id,
                                      I_dept,
                                      I_class,
                                      I_subclass,
                                      L_loc_type,
                                      TO_NUMBER(C_all_external_finisher_rec.partner_id),
                                      L_half_no,
                                      L_month_no,
                                      L_week_no,
                                      'L',  --always insert a local record
                                      L_eow_date);
               O_counter := O_counter + 1;
            EXCEPTION
               when DUP_VAL_ON_INDEX then
                  NULL;
            END;

            --insert a primary record if local != primary
            if L_multi_curr = 'Y' and (L_primary_currency != C_all_external_finisher_rec.currency_code) then
               BEGIN
                  insert into week_data (set_of_books_id,
                                         dept,
                                         class,
                                         subclass,
                                         loc_type,
                                         location,
                                         half_no,
                                         month_no,
                                         week_no,
                                         currency_ind,
                                         eow_date)
                                 values (C_all_external_finisher_rec.set_of_books_id,
                                         I_dept,
                                         I_class,
                                         I_subclass,
                                         L_loc_type,
                                         TO_NUMBER(C_all_external_finisher_rec.partner_id),
                                         L_half_no,
                                         L_month_no,
                                         L_week_no,
                                         'P',  --insert a primary record
                                         L_eow_date);
                  O_counter := O_counter + 1;
               EXCEPTION
                  when DUP_VAL_ON_INDEX then
                     NULL;
               END;
            end if;
         END LOOP;

         if L_eow_date >= L_vdate then exit;
         end if;

     END LOOP;
   end if;

   /* insert rows into half_data for the new subclass and all locations */
   L_loc_type := 'S';
   FOR C_all_store_rec in C_all_store LOOP
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'half_data',
                       'DEPT: '||TO_CHAR(I_dept)||
                       ', CLASS: '||TO_CHAR(I_class)||
                       ', SUBCLASS: '||TO_CHAR(I_subclass)||
                       ', LOC_TYPE: '||L_loc_type||
                       ', LOCATION: '||TO_CHAR(C_all_store_rec.store)||
                       ', HALF: '||TO_CHAR(L_half_no));
      BEGIN
         insert into half_data (set_of_books_id,
                                dept,
                                class,
                                subclass,
                                loc_type,
                                location,
                                half_no)
                        values (C_all_store_rec.set_of_books_id,
                                I_dept,
                                I_class,
                                I_subclass,
                                L_loc_type,
                                C_all_store_rec.store,
                                L_half_no);
         O_counter := O_counter + 1;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            NULL;
      END;
   END LOOP;

   L_loc_type := 'W';
   FOR C_all_wh_rec in C_all_wh_multi_ch LOOP -- use only virtual whs
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'half_data',
                       'DEPT: '||TO_CHAR(I_dept)||
                       ', CLASS: '||TO_CHAR(I_class)||
                       ', SUBCLASS: '||TO_CHAR(I_subclass)||
                       ', LOC_TYPE: '||L_loc_type||
                       ', LOCATION: '||TO_CHAR(C_all_wh_rec.wh)||
                       ', HALF: '||TO_CHAR(L_half_no));
      BEGIN
         insert into half_data (set_of_books_id,
                                dept,
                                class,
                                subclass,
                                loc_type,
                                location,
                                half_no)
                        values (C_all_wh_rec.set_of_books_id,
                                I_dept,
                                I_class,
                                I_subclass,
                                L_loc_type,
                                C_all_wh_rec.wh,
                                L_half_no);
         O_counter := O_counter + 1;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            NULL;
      END;
   END LOOP;

   L_loc_type := 'E';
   FOR C_all_external_finisher_rec in C_all_external_finisher LOOP
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'half_data',
                       'DEPT: '||TO_CHAR(I_dept)||
                       ', CLASS: '||TO_CHAR(I_class)||
                       ', SUBCLASS: '||TO_CHAR(I_subclass)||
                       ', LOC_TYPE: '||L_loc_type||
                       ', LOCATION: '||C_all_external_finisher_rec.partner_id||
                       ', HALF: '||TO_CHAR(L_half_no));
      BEGIN
         insert into half_data (set_of_books_id,
                                dept,
                                class,
                                subclass,
                                loc_type,
                                location,
                                half_no)
                        values (C_all_external_finisher_rec.set_of_books_id,
                                I_dept,
                                I_class,
                                I_subclass,
                                L_loc_type,
                                TO_NUMBER(C_all_external_finisher_rec.partner_id),
                                L_half_no);
         O_counter := O_counter + 1;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            NULL;
      END;
   END LOOP;

   RETURN TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURSOR',
                                            L_cursor,
                                            L_program,
                                            NULL);
      return FALSE;

   when DATE_FAILED then
      O_error_message := SQL_LIB.CREATE_MSG('STKLEDGR_DATE',
                                            L_function,
                                            L_program,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_SUBCLASS;
----------------------------------------------------------------------------

FUNCTION ADD_DEPT_BUDGET (I_dept          IN       NUMBER,
                          O_counter       IN OUT   NUMBER,
                          O_error_message IN OUT   VARCHAR2)
RETURN BOOLEAN IS

   L_cal             NUMBER(1);
   L_this_half       NUMBER(5);
   L_next_half       NUMBER(5);
   L_in_day          NUMBER(2);
   L_in_month        NUMBER(2);
   L_in_year         NUMBER(4);
   L_out_day         NUMBER(2);
   L_out_month       NUMBER(2);
   L_out_year        NUMBER(4);
   L_eom             DATE;
   L_half            NUMBER(1);
   L_half_no         period.half_no%TYPE;
   L_month_no        NUMBER(1);
   L_last_eom        system_variables.last_eom_date%TYPE         := NULL;
   L_no_halves       NUMBER(1);
   L_prev_half       NUMBER(5);
   L_return_code     VARCHAR2(5)     := 'TRUE';
   L_error_message   VARCHAR2(255)   := NULL;
   L_cursor          VARCHAR2(20)    := NULL;
   L_function        VARCHAR2(20)    := NULL;
   L_program         VARCHAR2(50)    := 'STKLEDGR_INSERTS_SQL.ADD_DEPT_BUDGET';
   L_loc_type        VARCHAR2(1)     := NULL;
   DATE_FAILED       EXCEPTION;

   cursor C_system is
      select DECODE(so.calendar_454_ind,'C',1,0),
             sv.last_eom_next_half_no,
             sv.last_eom_date,
             p.half_no,
             p.next_half_no
        from system_options so,
             system_variables sv,
             period p;

   cursor C_all_store is
      select store,
             NVL(set_of_books_id, -1) set_of_books_id
        from store s,
             mv_loc_sob vls
       where vls.location      = s.store
         and vls.location_type = 'S'
         and not (s.store_type = 'F' AND s.stockholding_ind = 'N');

   cursor C_all_wh_multi_ch is
      select wh,
             NVL(set_of_books_id, -1) set_of_books_id
        from wh w,
             mv_loc_sob vls
       where vls.location_type = 'W'
         and vls.location      = w.wh
         and w.wh             != w.physical_wh
         and w.stockholding_ind = 'Y'
         and w.finisher_ind     = 'N';   

   cursor C_all_external_finisher is
      select partner_id,
             NVL(set_of_books_id, -1) set_of_books_id
        from partner prt,
             mv_loc_sob vls
       where vls.location_type = 'E'
         and vls.location      = prt.partner_id
         and partner_type      = 'E';


BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_system',
                    'system_variables, system_options, period',
                    NULL);
   open C_system;

   SQL_LIB.SET_MARK('FETCH',
                    'C_system',
                    'system_variables, system_options, period',
                    NULL);
   fetch C_system into L_cal,
                       L_half_no,
                       L_last_eom,
                       L_this_half,
                       L_next_half;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_system',
                    'system_variables, system_options, period',
                    NULL);
   if C_system%NOTFOUND then
      close C_system;
      L_cursor := 'C_system';
      raise NO_DATA_FOUND;
   end if;
   close C_system;
   L_in_day := TO_NUMBER(TO_CHAR((L_last_eom + 1),'DD'),'09');
   L_in_month := TO_NUMBER(TO_CHAR((L_last_eom + 1),'MM'),'09');
   L_in_year := TO_NUMBER(TO_CHAR((L_last_eom + 1),'YYYY'),'0999');


   /* calculate half_no, month_no, and eom_date for data currently
        due for processing */

   if L_cal = 1 then
      CAL_TO_CAL_HALF(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_half_no,
                      L_month_no,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_CAL_HALF';
         raise DATE_FAILED;
      end if;

   L_this_half := L_half_no;

   if (MOD(L_this_half,10) - 1) = 1 then
      L_next_half := L_this_half + 9;
   else
      L_next_half := L_this_half + 1;
   end if;

      CAL_TO_CAL_LDOM(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_out_day,
                      L_out_month,
                      L_out_year,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_CAL_LDOM';
         raise DATE_FAILED;
      end if;
   else
      CAL_TO_454_HALF(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_half_no,
                      L_month_no,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_454_HALF';
         raise DATE_FAILED;
      end if;

      CAL_TO_454_LDOM(L_in_day,
                      L_in_month,
                      L_in_year,
                      L_out_day,
                      L_out_month,
                      L_out_year,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'CAL_TO_454_LDOM';
         raise DATE_FAILED;
      end if;
   end if;

   /* if L_half_no differs from L_this_half, assume that the last half
      was not closed yet.  This means that the budget tables need data
      inserted into them for last half, this half, and next half.
      Otherwise, just this half and next half are needed.
   */
   if L_half_no < L_this_half then
      L_no_halves := 2;
      L_prev_half := L_half_no;
   else
      L_no_halves := 1;
   end if;

   L_eom := TO_DATE(TO_CHAR(L_out_day,'09')||TO_CHAR(L_out_month,'09')||
                    TO_CHAR(L_out_year,'0999'),'DDMMYYYY');


   if L_cal = 1 then
      HALF_TO_CAL_FDOH(L_this_half,
                       L_out_day,
                       L_out_month,
                       L_out_year,
                       L_return_code,
                       L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'HALF_TO_CAL_FDOH';
         raise DATE_FAILED;
      end if;
   else
      HALF_TO_454_FDOH(L_this_half,
                       L_out_day,
                       L_out_month,
                       L_out_year,
                       L_return_code,
                       L_error_message);
      if L_return_code = 'FALSE' then
         L_function := 'HALF_TO_454_FDOH';
         raise DATE_FAILED;
      end if;
   end if;

   L_eom := TO_DATE(TO_CHAR(L_out_day,'09')||TO_CHAR(L_out_month,'09')||
                    TO_CHAR(L_out_year,'0999'),'DDMMYYYY');

   FOR L_half in 0..L_no_halves LOOP
      if L_half = 1 then
         L_half_no := L_next_half;
      elsif L_half = 0 then
         L_half_no := L_this_half;
      else
         L_half_no := L_prev_half;
      end if;

      FOR L_month_no in 1..6 LOOP
         L_in_day := TO_NUMBER(TO_CHAR((L_eom + 1),'DD'),'09');
         L_in_month := TO_NUMBER(TO_CHAR((L_eom + 1),'MM'),'09');
         L_in_year := TO_NUMBER(TO_CHAR((L_eom + 1),'YYYY'),'0999');

         if L_cal = 1 then
            CAL_TO_CAL_LDOM(L_in_day,
                            L_in_month,
                            L_in_year,
                            L_out_day,
                            L_out_month,
                            L_out_year,
                            L_return_code,
                            L_error_message);
            if L_return_code = 'FALSE' then
               L_function := 'CAL_TO_CAL_LDOM';
               raise DATE_FAILED;
            end if;
         else
            CAL_TO_454_LDOM(L_in_day,
                            L_in_month,
                            L_in_year,
                            L_out_day,
                            L_out_month,
                            L_out_year,
                            L_return_code,
                            L_error_message);
            if L_return_code = 'FALSE' then
               L_function := 'CAL_TO_454_LDOM';
               raise DATE_FAILED;
            end if;
         end if;

         L_eom := TO_DATE(TO_CHAR(L_out_day,'09')||TO_CHAR(L_out_month,'09')||
                          TO_CHAR(L_out_year,'0999'),'DDMMYYYY');
         L_loc_type := 'S';
         FOR C_all_store_rec in C_all_store LOOP
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'month_data_budget',
                             'DEPT: '||TO_CHAR(I_dept)||
                             ', MONTH: '||TO_CHAR(L_month_no)||
                             ', HALF: '||TO_CHAR(L_half_no)||
                             ', LOC_TYPE: '||L_loc_type||
                             ', LOCATION: '||TO_CHAR(C_all_store_rec.store));
            BEGIN
               insert into month_data_budget (set_of_books_id,
                                              dept,
                                              month_no,
                                              half_no,
                                              loc_type,
                                              location)
               values (C_all_store_rec.set_of_books_id,
                       I_dept,
                       L_month_no,
                       L_half_no,
                       L_loc_type,
                       C_all_store_rec.store);
               O_counter := O_counter + 1;
            EXCEPTION
               when DUP_VAL_ON_INDEX then
                  NULL;
            END;
         END LOOP;

         L_loc_type := 'W';
         FOR C_all_wh_rec in C_all_wh_multi_ch LOOP -- use only virutal_whs
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'month_data_budget',
                             'DEPT: '||TO_CHAR(I_dept)||
                             ', MONTH: '||TO_CHAR(L_month_no)||
                             ', HALF: '||TO_CHAR(L_half_no)||
                             ', LOCTYPE: '||L_loc_type||
                             ', LOCATION: '||TO_CHAR(C_all_wh_rec.wh));
            BEGIN
               insert into month_data_budget (set_of_books_id,
                                              dept,
                                              month_no,
                                              half_no,
                                              loc_type,
                                              location)
               values (C_all_wh_rec.set_of_books_id,
                       I_dept,
                       L_month_no,
                       L_half_no,
                       L_loc_type,
                       C_all_wh_rec.wh);
               O_counter := O_counter + 1;
            EXCEPTION
               when DUP_VAL_ON_INDEX then
                  NULL;
            END;
         END LOOP;

         L_loc_type := 'E';
         FOR C_all_external_finisher_rec in C_all_external_finisher LOOP
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'month_data_budget',
                             'DEPT: '||TO_CHAR(I_dept)||
                             ', MONTH: '||TO_CHAR(L_month_no)||
                             ', HALF: '||TO_CHAR(L_half_no)||
                             ', LOC_TYPE: '||L_loc_type||
                             ', LOCATION: '||C_all_external_finisher_rec.partner_id);
            BEGIN
               insert into month_data_budget (set_of_books_id,
                                              dept,
                                              month_no,
                                              half_no,
                                              loc_type,
                                              location)
               values (C_all_external_finisher_rec.set_of_books_id,
                       I_dept,
                       L_month_no,
                       L_half_no,
                       L_loc_type,
                       TO_NUMBER(C_all_external_finisher_rec.partner_id));
               O_counter := O_counter + 1;
            EXCEPTION
               when DUP_VAL_ON_INDEX then
                  NULL;
            END;
         END LOOP;

      END LOOP; -- all months
   END LOOP; -- all halfs

   FOR L_half in 0..L_no_halves LOOP
      if L_half = 1 then
         L_half_no := L_next_half;
      elsif L_half = 0 then
         L_half_no := L_this_half;
      else
         L_half_no := L_prev_half;
      end if;

      L_loc_type := 'S';
      FOR C_all_store_rec in C_all_store LOOP
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'half_data_budget',
                          'DEPT: '||TO_CHAR(I_dept)||
                          ', HALF: '||TO_CHAR(L_half_no)||
                          ', LOC_TYPE: '||L_loc_type||
                          ', LOCATION: '||TO_CHAR(C_all_store_rec.store));
         BEGIN
            insert into half_data_budget (set_of_books_id,
                                          dept,
                                          half_no,
                                          loc_type,
                                          location)
            values (C_all_store_rec.set_of_books_id,
                    I_dept,
                    L_half_no,
                    L_loc_type,
                    C_all_store_rec.store);
            O_counter := O_counter + 1;
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               NULL;
         END;
      END LOOP;

      L_loc_type := 'W';
      FOR C_all_wh_rec in C_all_wh_multi_ch LOOP -- use only virtual whs
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'half_data',
                          'DEPT: '||TO_CHAR(I_dept)||
                          ', HALF: '||TO_CHAR(L_half_no)||
                          ', LOC_TYPE: '||L_loc_type||
                          ', LOCATION: '||TO_CHAR(C_all_wh_rec.wh));
         BEGIN
            insert into half_data_budget (set_of_books_id,
                                          dept,
                                          half_no,
                                          loc_type,
                                          location)
            values (C_all_wh_rec.set_of_books_id,
                    I_dept,
                    L_half_no,
                    L_loc_type,
                    C_all_wh_rec.wh);
            O_counter := O_counter + 1;
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               NULL;
         END;
      END LOOP;

      L_loc_type := 'E';
      FOR C_all_external_finisher_rec in C_all_external_finisher LOOP
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'half_data_budget',
                          'DEPT: '||TO_CHAR(I_dept)||
                          ', HALF: '||TO_CHAR(L_half_no)||
                          ', LOC_TYPE: '||L_loc_type||
                          ', LOCATION: '||C_all_external_finisher_rec.partner_id);
         BEGIN
            insert into half_data_budget (set_of_books_id,
                                          dept,
                                          half_no,
                                          loc_type,
                                          location)
            values (C_all_external_finisher_rec.set_of_books_id,
                    I_dept,
                    L_half_no,
                    L_loc_type,
                    TO_NUMBER(C_all_external_finisher_rec.partner_id));
            O_counter := O_counter + 1;
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               NULL;
         END;
      END LOOP;

   END LOOP;

   RETURN TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURSOR',
                                            L_cursor,
                                            L_program,
                                            NULL);
      return FALSE;

   when DATE_FAILED then
      O_error_message := SQL_LIB.CREATE_MSG('STKLEDGR_DATE',
                                            L_function,
                                            L_program,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_DEPT_BUDGET;
------------------------------------------------------------------------------
END;
/