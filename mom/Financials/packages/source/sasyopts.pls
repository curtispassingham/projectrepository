CREATE OR REPLACE PACKAGE SA_SYSTEM_OPTIONS_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------------
--- Function:   GET_SYSTEM_OPTIONS
--- Purpose:    returns the system options for Sales Audit.  All system options will be returned, but
---             if certain ones aren't needed they can just be passed into dummy variables.
---
FUNCTION GET_SYSTEM_OPTIONS(O_error_message           IN OUT  VARCHAR2,
                            O_days_before_purge       IN OUT  SA_SYSTEM_OPTIONS.DAYS_BEFORE_PURGE%TYPE,
                            O_day_post_sale           IN OUT  SA_SYSTEM_OPTIONS.DAY_POST_SALE%TYPE,
                            O_balance_level_ind       IN OUT  SA_SYSTEM_OPTIONS.BALANCE_LEVEL_IND%TYPE,
                            O_max_days_compare_dups   IN OUT  SA_SYSTEM_OPTIONS.MAX_DAYS_COMPARE_DUPS%TYPE,
                            O_comp_base_date          IN OUT  SA_SYSTEM_OPTIONS.COMP_BASE_DATE%TYPE,
                            O_comp_no_days            IN OUT  SA_SYSTEM_OPTIONS.COMP_NO_DAYS%TYPE,
                            O_check_dup_miss_tran     IN OUT  SA_SYSTEM_OPTIONS.CHECK_DUP_MISS_TRAN%TYPE,
                            O_unit_of_work            IN OUT  SA_SYSTEM_OPTIONS.UNIT_OF_WORK%TYPE,
                            O_audit_after_imp_ind     IN OUT  SA_SYSTEM_OPTIONS.AUDIT_AFTER_IMP_IND%TYPE,
                            O_fuel_dept               IN OUT  SA_SYSTEM_OPTIONS.FUEL_DEPT%TYPE,
                            O_default_chain           IN OUT  SA_SYSTEM_OPTIONS.DEFAULT_CHAIN%TYPE,
                            O_close_in_order          IN OUT  SA_SYSTEM_OPTIONS.CLOSE_IN_ORDER%TYPE,
                            O_auto_validate_emp_id    IN OUT  SA_SYSTEM_OPTIONS.AUTO_VALIDATE_TRAN_EMPLOYEE_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS(O_error_message           IN OUT  VARCHAR2,
                            O_days_before_purge       IN OUT  SA_SYSTEM_OPTIONS.DAYS_BEFORE_PURGE%TYPE,
                            O_day_post_sale           IN OUT  SA_SYSTEM_OPTIONS.DAY_POST_SALE%TYPE,
                            O_balance_level_ind       IN OUT  SA_SYSTEM_OPTIONS.BALANCE_LEVEL_IND%TYPE,
                            O_max_days_compare_dups   IN OUT  SA_SYSTEM_OPTIONS.MAX_DAYS_COMPARE_DUPS%TYPE,
                            O_comp_base_date          IN OUT  SA_SYSTEM_OPTIONS.COMP_BASE_DATE%TYPE,
                            O_comp_no_days            IN OUT  SA_SYSTEM_OPTIONS.COMP_NO_DAYS%TYPE,
                            O_check_dup_miss_tran     IN OUT  SA_SYSTEM_OPTIONS.CHECK_DUP_MISS_TRAN%TYPE,
                            O_unit_of_work            IN OUT  SA_SYSTEM_OPTIONS.UNIT_OF_WORK%TYPE,
                            O_audit_after_imp_ind     IN OUT  SA_SYSTEM_OPTIONS.AUDIT_AFTER_IMP_IND%TYPE,
                            O_fuel_dept               IN OUT  SA_SYSTEM_OPTIONS.FUEL_DEPT%TYPE,
                            O_default_chain           IN OUT  SA_SYSTEM_OPTIONS.DEFAULT_CHAIN%TYPE,
                            O_close_in_order          IN OUT  SA_SYSTEM_OPTIONS.CLOSE_IN_ORDER%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_get_system_options_rec       OUT SA_SYSTEM_OPTIONS%ROWTYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--- Function:   GET_DEFAULT_CHAIN
--- Purpose:    returns the default chain
---
FUNCTION GET_DEFAULT_CHAIN(O_error_message IN OUT VARCHAR2,
                           O_default_chain IN OUT SA_SYSTEM_OPTIONS.DEFAULT_CHAIN%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--- Function:   GET_BAL_LEVEL
--- Purpose:    returns the balance level
---
FUNCTION GET_BAL_LEVEL(O_error_message     IN OUT VARCHAR2,
                       O_balance_level_ind IN OUT SA_SYSTEM_OPTIONS.BALANCE_LEVEL_IND%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--- Function:   GET_AUDIT_AFTER_IMP_IND
--- Purpose:    Returns the audit after import indicator.
---
FUNCTION GET_AUDIT_AFTER_IMP_IND
         (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
          O_audit_after_imp_ind  IN OUT SA_SYSTEM_OPTIONS.AUDIT_AFTER_IMP_IND%TYPE)
   RETURN BOOLEAN ;
-----------------------------------------------------------------------------------------------------
--- Function:   GET_CC_NO_MASK_CHAR
--- Purpose:    returns the Credit/Debit card masked character
---
FUNCTION GET_CC_NO_MASK_CHAR(O_error_message     IN OUT VARCHAR2,
                             O_cc_no_mask_char   IN OUT SA_SYSTEM_OPTIONS.CC_NO_MASK_CHAR%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------   
END SA_SYSTEM_OPTIONS_SQL;
/
