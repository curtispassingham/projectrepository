CREATE OR REPLACE PACKAGE CORESVC_SALES_UPLOAD_PRST_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
-- Function Name: PERSIST
-- Purpose      : This is the driving function used by CORESVC_SALES_UPLOAD_SQL package
--                (PROCESS_SALES function). Invokes private functions to modify and compute
--                the values related to sales processing.
-------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                 I_sales_process_id      IN     NUMBER,
                 I_chunk_id              IN     NUMBER)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
$if $$UTPLSQL=TRUE $then

   -------------------------------------------------------------------------------------------
   -- Function Name: LOCK_TABLES
   -- Purpose      : This function will check if sales transaction tables are locked.
   --                These tables are to be updated by succeeding processing of this
   --                package and record locking will be checked prior to processing.
   -------------------------------------------------------------------------------------------
   FUNCTION LOCK_TABLES(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                        I_sales_process_id      IN     NUMBER,
                        I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_ITEM_LOC_SOH
   -- Purpose      : This function will modify associated inventory columns at ITEM_LOC_SOH table
   --                based on the uploaded sales quantity and other item specifications.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_ITEM_LOC_SOH(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id      IN     NUMBER,
                                 I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_DAILY_SALES_DISCOUNT
   -- Purpose      : This function will modify associated columns at DAILY_SALES_DISCOUNT
   --                based on the uploaded discounted sales quantity and amount.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_DAILY_SALES_DISCOUNT(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_sales_process_id      IN     NUMBER,
                                         I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_WASTAGE
   -- Purpose      : This function will post transaction data and information (TRAN_DATA)
   --                where tran_code is 13. This function is being invoked by PERSIST_TRAN_DATA
   --                function.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_WASTAGE(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id      IN     NUMBER,
                                      I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_SNAPSHOT
   -- Purpose      : This function will update the records found at EDI_DAILY_SALES when applicable.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_SNAPSHOT(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id      IN     NUMBER,
                             I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_DEAL_INCOME
   -- Purpose      : This function will post adjustments to deal income (Billback Sales) posted
   --                for backposted transactions. Tran_code = 6. This is being invoked by
   --                PERSIST_TRAN_DATA function.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_DEAL_INCOME(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                          I_sales_process_id      IN     NUMBER,
                                          I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_DEAL_ACTUALS_IL
   -- Purpose      : This function will update deal_actuals_item_loc table
   --                and update turnover units with sales quantity and
   --                revenue with associated sales amount.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_DEAL_ACTUALS_IL(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id      IN     NUMBER,
                                    I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_VAT_HISTORY
   -- Purpose      : This function will update VAT_HISTORY table with associated VAT amount on sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_VAT_HISTORY(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id      IN     NUMBER,
                                I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_EDI_DAILY_SALES
   -- Purpose      : This function will update the EDI_DAILY_SALES table based on values associated
   --                with sales posted.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_EDI_DAILY_SALES(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id      IN     NUMBER,
                                    I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_ITEM_LOC_HIST
   -- Purpose      : This function will update the ITEM_LOC_HIST table on the retail change
   --                associated with the POSU sales file uploaded.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_ITEM_LOC_HIST(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_sales_process_id      IN     NUMBER,
                                  I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ------------------------------------------------------------------------
   -- Function Name: PERSIST_ITEM_LOC_HIST_MTH
   -- Purpose      : This function will update the ITEM_LOC_HIST_MTH table on the retail change
   --                associated with the POSU sales file uploaded if reporting period is Monthly..
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_ITEM_LOC_HIST_MTH(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id      IN     NUMBER,
                                      I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CONCESSION_DATA
   -- Purpose      : This function will insert new records on the CONCESSION_DATA table on
   --                the event of sales posted for concession items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CONCESSION_DATA(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id      IN     NUMBER,
                                    I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CONSIGNMENT_DATA
   -- Purpose      : This function will insert poste the transactions related to sales posted by
   --                items belonging to consignment departments.
   --                Post transaction data for purchase, update RTV tables, post records related to
   --                invoice tables.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CONSIGNMENT_DATA(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_sales_process_id      IN     NUMBER,
                                     I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: LOCK_CONSIGNMENT_TABLES
   -- Purpose      : This function will lock the associated tables related to consignment transactions
   --                such as RTV_HEAD and INVC_MERCH_VAT.
   -------------------------------------------------------------------------------------------
   FUNCTION LOCK_CONSIGNMENT_TABLES(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id      IN     NUMBER)
   RETURN BOOLEAN;

   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_RTV_HEAD
   -- Purpose      : This function will update the RTV tables related to the transaction posted
   --                for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_RTV_HEAD(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id      IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_ORDHEAD
   -- Purpose      : This function will update the order tables related to the transaction posted
   --                for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_ORDHEAD(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sales_process_id      IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_INVC_HEAD
   -- Purpose      : This function will update the inventory table (header)
   --                related to the transactions posted for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_INVC_HEAD(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sales_process_id      IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_INVC_DETAIL
   -- Purpose      : This function will update the inventory tables (detail)
   --                related to the transactions posted for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_INVC_DETAIL(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id      IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
      -- Function Name: PERSIST_INVC_MERCH_VAT
      -- Purpose      : This function will update the inventory tables (vat amount related to
      --                inventory) related to the transactions posted for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_INVC_MERCH_VAT(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id      IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CONSIGN_TRAN_DATA
   -- Purpose      : This function will post regular transaction data to stock ledger table
   --                TRAN_DATA related to consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CONSIGN_TRAN_DATA(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id      IN     NUMBER,
                                      I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CONSIGN_VAT_OUT
   -- Purpose      : This function will post VAT related transaction data to stock ledger table
   --                TRAN_DATA (tran code 88) related to consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CONSIGN_VAT_OUT(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id      IN     NUMBER,
                                    I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA
   -- Purpose      : This function will post transaction data to stock ledger tables
   --                TRAN_DATA related to sales transactions. This will call several functions
   --                that will post the different tran_data with different transaction codes
   --                relevant to the item and parameter configurations.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sales_process_id      IN     NUMBER,
                              I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_SALE
   -- Purpose      : This function will post transaction data to stock ledger tables.
   --                Tran_code 1 and 3=sales. This function is being invoked by PERSIST_TRAN_DATA.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_SALE(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id      IN     NUMBER,
                                   I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_RETURNS
   -- Purpose      : This function will post transaction data to stock ledger tables.
   --                Tran_code 4 =returns. This function is being invoked by PERSIST_TRAN_DATA.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_RETURN(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_sales_process_id      IN     NUMBER,
                                     I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_REVERSAL
   -- Purpose      : This function will post transaction data to stock ledger tables.
   --                Tran_code 13 and 14=markdown (different retail values).
   --                This function is being invoked by PERSIST_TRAN_DATA.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_REVERSAL(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_sales_process_id      IN     NUMBER,
                                       I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_VAT_OUT
   -- Purpose      : This function will post VAT related transaction data to stock ledger table
   --                TRAN_DATA (tran code 88) related to regular items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_VAT_OUT(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id      IN     NUMBER,
                                      I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_EMP_DISC
   -- Purpose      : This function will post transaction data related to employee discount promotion.
   --                TRAN_DATA (tran code 60) related to sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_EMP_DISC(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_sales_process_id      IN     NUMBER,
                                       I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_OFF_RETAIL
   -- Purpose      : This function will post transaction data related to off-retail promotions.
   --                TRAN_DATA (tran code 15) related to sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_OFF_RETAIL(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_sales_process_id      IN     NUMBER,
                                         I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_PACK_UP_DOWN
   -- Purpose      : This function will post transaction data related to pack items.
   --                TRAN_DATA (tran code 11 and 13) related to sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_PACK_UP_DOWN(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                           I_sales_process_id      IN     NUMBER,
                                           I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_PROMO_MKDN
   -- Purpose      : This function will post transaction data related to promotional markdowns.
   --                TRAN_DATA (tran code 15) related to sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_PROMO_MKDN(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_sales_process_id      IN     NUMBER,
                                         I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_WEIGHT_VAR
   -- Purpose      : This function will post transaction data related to weight variances.
   --                TRAN_DATA (tran code 10) related to catchweight items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_WEIGHT_VAR(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_sales_process_id      IN     NUMBER,
                                         I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CO_RTN_BOOK_TSF
   -- Purpose      : This function will create a book transfer for customer order returns
   --                originating from OMS.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CO_RTN_BOOK_TSF(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id      IN     NUMBER,
                                    I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------\
   -- Function Name: PERSIST_CUST_ORDER_RESV
   -- Purpose      : This function will knock off the customer reserve bucket for external
   --                customer order sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CUST_ORDER_RESV(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id      IN     NUMBER,
                                    I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------

$end
----------------------------------------------------------------------------------------
END CORESVC_SALES_UPLOAD_PRST_SQL;
/
