CREATE OR REPLACE PACKAGE BODY STKLEDGR_ACCTING_SQL AS

LP_bud_shrink_ind system_options.bud_shrink_ind%TYPE := NULL;

--------------------------------------------------------------------------------
-- i_stocktake_adj_retail is the adjustment dollar amount to the shrinkage dollar
-- calculation as result of a stock count, i.e. it is the difference between book
-- stock dollar amount and actual stock dollar amount. Note that book stock dollar
-- amount has already been reduced by the budgeted shrinkage dollar amount during
-- each period since the last stock count.
--
-- For example, if book stock =$1000 and actual stock =$900, this means that the
-- stock count has resulted in additional shrinkage of $100,  on top of the
-- budgeted shrinkage.
--
-- When salweek.pc calls this function :
--      i_stocktake_adj_retail = week_data.stocktake_adj_retail
--      if i_stocktake_adj_retail=NULL, this means that there was no stock count
--      taking place during that week
--      i_stocktake_mtd_shrink_amt and i_stocktake_mtd_sales_amt are always 0
--
-- When salmth.pc calls this function :
--      i_stocktake_adj_retail = month_data.stocktake_bookstk_retail -
--                               month_data.stocktake_actstk_retail
--
--      if i_stocktake_adj_retail=NULL, this means that there was no stock count
--      taking place during that month
--
--      if i_stocktake_adj_retail NOT = NULL,
--             i_stocktake_mtd_shrink_amt = month_data.stocktake_mtd_shrink_amt
--             i_stocktake_mtd_sales_amt = month_data. stocktake_mtd_sales_amt
--
-- When salstake.pc calls this function (salstake.pc calls this function to calculate
-- month_data.stocktake_bookstk_retail and month_data.stocktake_mtd_shrink_amt)
-- i_stocktake_adj_retail is always set to NULL
--------------------------------------------------------------------------------

FUNCTION RETAIL_METHOD_CALC(IO_stk_retail                    IN OUT   NUMBER, --opening stock in, closing out
                            IO_stk_cost                      IN OUT   NUMBER, --opening stock in, closing out
                            I_purch_retail                   IN       NUMBER,
                            I_purch_cost                     IN       NUMBER,
                            I_rtv_retail                     IN       NUMBER,
                            I_rtv_cost                       IN       NUMBER,
                            I_freight_cost                   IN       NUMBER,
                            I_markup_retail                  IN       NUMBER,
                            I_markup_can_retail              IN       NUMBER,
                            I_net_sales_retail               IN       NUMBER,
                            I_net_sales_retail_ex_vat        IN       NUMBER,
                            I_net_sales_cost                 IN       NUMBER,
                            I_returns_retail                 IN       NUMBER,
                            I_returns_cost                   IN       NUMBER,
                            I_perm_markdown_retail           IN       NUMBER,
                            I_promo_markdown_retail          IN       NUMBER,
                            I_weight_variance_retail         IN       NUMBER,
                            I_clear_markdown_retail          IN       NUMBER,
                            I_markdown_can_retail            IN       NUMBER,
                            I_up_chrg_amt_profit             IN       NUMBER,
                            I_up_chrg_amt_exp                IN       NUMBER,
                            I_tsf_in_retail                  IN       NUMBER,
                            I_tsf_in_cost                    IN       NUMBER,
                            I_tsf_in_book_retail             IN       NUMBER,
                            I_tsf_in_book_cost               IN       NUMBER,
                            I_tsf_out_retail                 IN       NUMBER,
                            I_tsf_out_cost                   IN       NUMBER,
                            I_tsf_out_book_retail            IN       NUMBER,
                            I_tsf_out_book_cost              IN       NUMBER,
                            I_reclass_in_retail              IN       NUMBER,
                            I_reclass_in_cost                IN       NUMBER,
                            I_reclass_out_retail             IN       NUMBER,
                            I_reclass_out_cost               IN       NUMBER,
                            I_stock_adj_retail               IN       NUMBER,
                            I_stock_adj_cost                 IN       NUMBER,
                            I_freight_claims_retail          IN       NUMBER,
                            I_stock_adj_cogs_retail          IN       NUMBER,
                            I_bud_shrinkage_pct              IN       NUMBER,
                            I_empl_disc_retail               IN       NUMBER,
                            IO_htd_gafs_retail               IN OUT   NUMBER,
                            IO_htd_gafs_cost                 IN OUT   NUMBER,
                            I_workroom_amt                   IN       NUMBER,
                            I_cash_discount_amt              IN       NUMBER,
                            I_stocktake_adj_retail           IN       NUMBER,
                            I_stocktake_mtd_shrink_amt       IN       NUMBER,
                            I_stocktake_mtd_sales_amt        IN       NUMBER,
                            I_intercompany_in_retail         IN       NUMBER,
                            I_intercompany_in_cost           IN       NUMBER,
                            I_intercompany_out_retail        IN       NUMBER,
                            I_intercompany_out_cost          IN       NUMBER,
                            I_intercompany_markup            IN       NUMBER,
                            I_intercompany_markdown          IN       NUMBER,
                            I_wo_activity_upd_inv            IN       NUMBER,
                            I_wo_activity_post_fin           IN       NUMBER,
                            I_deal_income_sales              IN       NUMBER,
                            I_deal_income_purch              IN       NUMBER,
                            I_restocking_fee                 IN       NUMBER,
                            IO_retail_cost_variance_amount   IN OUT   NUMBER,
                            IO_margin_cost_variance_amount   IN OUT   NUMBER,
                            O_intercompany_margin            IN OUT   NUMBER,
                            O_gross_margin_amt               IN OUT   NUMBER,
                            IO_cum_markon_pct                IN OUT   NUMBER,
                            I_recalc_cum_markon_pct          IN       BOOLEAN,
                            O_shrinkage_retail               IN OUT   NUMBER,
                            I_franchise_sales_retail         IN       NUMBER,
                            I_franchise_returns_retail       IN       NUMBER,
                            I_franchise_returns_cost         IN       NUMBER,
                            I_franchise_markup_retail        IN       NUMBER,
                            I_franchise_markdown_retail      IN       NUMBER,
                            I_freight_claims_cost            IN       NUMBER,
                            O_error_message                  IN OUT   VARCHAR2)

   RETURN BOOLEAN IS

   L_program                     VARCHAR(64) := 'STKLEDGR_ACCTING_SQL.RETAIL_METHOD_CALC';
   L_inventory_additions_retail  NUMBER(20,4);
   L_inventory_additions_cost    NUMBER(20,4);
   L_inventory_reductions        NUMBER(20,4);
   L_opn_stk_cost                NUMBER(20,4);
   L_cogs                        NUMBER(20,4);
   L_stkldgr_vat_incl_ind        system_options.stkldgr_vat_incl_retl_ind%type;
   L_franchise_net_sales_retail  NUMBER(20,4) := 0;


   cursor C_bud_shrink is
      select bud_shrink_ind, stkldgr_vat_incl_retl_ind
        from system_options;

BEGIN
   if LP_bud_shrink_ind IS NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_bud_shrink', 'system_options', NULL);
      OPEN C_bud_shrink;

      SQL_LIB.SET_MARK('FETCH', 'C_bud_shrink', 'system_options', NULL);
      FETCH C_bud_shrink INTO LP_bud_shrink_ind, L_stkldgr_vat_incl_ind;

      SQL_LIB.SET_MARK('CLOSE', 'C_bud_shrink', 'system_options', NULL);
      CLOSE C_bud_shrink;
   end if;

   L_franchise_net_sales_retail := nvl(I_franchise_sales_retail,0) - nvl(I_franchise_returns_retail,0);

   if LP_bud_shrink_ind = 'Y' then
      if I_stocktake_adj_retail is NULL then
         O_shrinkage_retail := nvl(I_bud_shrinkage_pct,0) * nvl(I_net_sales_retail,0);
      else
         O_shrinkage_retail := nvl(I_stocktake_mtd_shrink_amt,0)
                             + nvl(I_stocktake_adj_retail,0)
                             + (nvl(I_net_sales_retail,0) - nvl(I_stocktake_mtd_sales_amt,0))
                             * nvl(I_bud_shrinkage_pct,0);
      end if;
   else
      O_shrinkage_retail := (-1 * nvl(I_stock_adj_retail,0)) + nvl(I_stocktake_adj_retail,0);
   end if;

   L_opn_stk_cost := IO_stk_cost;

   L_inventory_additions_retail :=   nvl(I_purch_retail,0)
                                   - nvl(I_rtv_retail,0)
                                   + nvl(I_markup_retail,0)
                                   + nvl(I_intercompany_markup,0)
                                   - nvl(I_markup_can_retail,0)
                                   + nvl(I_tsf_in_retail,0)
                                   + nvl(I_tsf_in_book_retail,0)
                                   + nvl(I_intercompany_in_retail,0)
                                   - nvl(I_tsf_out_retail,0)
                                   - nvl(I_tsf_out_book_retail,0)
                                   + nvl(I_reclass_in_retail,0)
                                   - nvl(I_reclass_out_retail,0)
                                   + nvl(I_franchise_returns_retail,0)
                                   + nvl(I_franchise_markup_retail,0);

   L_inventory_additions_cost :=   nvl(I_purch_cost,0)
                                 - nvl(I_rtv_cost,0)
                                 + nvl(I_freight_cost,0)
                                 + nvl(I_tsf_in_cost,0)
                                 + nvl(I_tsf_in_book_cost,0)
                                 + nvl(I_intercompany_in_cost,0)
                                 - nvl(I_tsf_out_cost,0)
                                 - nvl(I_tsf_out_book_cost,0)
                                 - nvl(I_intercompany_out_cost,0)
                                 - nvl(I_freight_claims_cost,0)
                                 + nvl(I_reclass_in_cost,0)
                                 - nvl(I_reclass_out_cost,0)
                                 + nvl(I_up_chrg_amt_profit,0)
                                 + nvl(I_up_chrg_amt_exp,0)
                                 + nvl(I_wo_activity_upd_inv,0)
                                 + nvl(I_franchise_returns_cost,0);

   L_inventory_reductions :=   nvl(I_net_sales_retail,0)
                             + nvl(I_perm_markdown_retail,0)
                             + nvl(I_promo_markdown_retail,0)
                             + nvl(I_weight_variance_retail,0)
                             + nvl(I_clear_markdown_retail,0)
                             + nvl(I_intercompany_markdown,0)
                             - nvl(I_markdown_can_retail,0)
                             + nvl(I_empl_disc_retail,0)
                             + nvl(I_freight_claims_retail,0)
                             - nvl(I_stock_adj_cogs_retail,0)
                             + O_shrinkage_retail
                             + nvl(I_intercompany_out_retail,0)
                             + nvl(I_franchise_sales_retail,0)
                             + nvl(I_franchise_markdown_retail,0);

   IO_stk_retail :=   IO_stk_retail
                    + L_inventory_additions_retail
                    - L_inventory_reductions;

   IO_htd_gafs_retail := IO_htd_gafs_retail + L_inventory_additions_retail;

   IO_htd_gafs_cost := IO_htd_gafs_cost + L_inventory_additions_cost;

   if IO_htd_gafs_retail != 0 then
      if I_recalc_cum_markon_pct then
         IO_cum_markon_pct := (1 - (IO_htd_gafs_cost/IO_htd_gafs_retail)) * 100;
      end if;
   elsif IO_cum_markon_pct is NULL then
      IO_cum_markon_pct := 0;
   end if;

 --- Catch the overflow case.
 --- Should only happen with a very small value for htd_gafs_retail
   if abs(IO_cum_markon_pct) >= 100000000 then
      IO_cum_markon_pct := 0;
   end if;

   IO_stk_cost := IO_stk_retail * (1 - IO_cum_markon_pct/100);

   L_cogs := L_opn_stk_cost + L_inventory_additions_cost - IO_stk_cost;


      if nvl(L_stkldgr_vat_incl_ind,'N') = 'N' then
         O_gross_margin_amt :=   nvl(I_net_sales_retail,0)
                               - L_cogs
                               - nvl(I_workroom_amt,0)
                               + nvl(I_cash_discount_amt,0)
                               + L_franchise_net_sales_retail;
      else
         O_gross_margin_amt :=   nvl(I_net_sales_retail_ex_vat,0)
                               - L_cogs
                               - nvl(I_workroom_amt,0)
                               + nvl(I_cash_discount_amt,0)
                               + L_franchise_net_sales_retail;
      end if;


   O_intercompany_margin :=   nvl(I_intercompany_out_retail,0)
                            * ( 1 - IO_cum_markon_pct/100);

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      RETURN FALSE;

END RETAIL_METHOD_CALC;

--------------------------------------------------------------------------------
-- i_stocktake_adj_retail is the adjustment dollar amount to the shrinkage dollar
-- calculation as result of a stock count, i.e. it is the difference between book
-- stock dollar amount and actual stock dollar amount.  Note that book stock dollar
-- amount has already been reduced by the budgeted shrinkage dollar amount during
-- each period since the last stock count.
--
-- For example, if book stock =$1000 and actual stock =$900, this means that the
-- stock count has resulted in additional shrinkage of $100,  on top of the
-- budgeted shrinkage.
--
-- When salwka.pc calls this function :
--   i_stocktake_adj_retail = week_data.stocktake_adj_retail
--   if i_stocktake_adj_retail = NULL, this means that there was no stock count
--      taking place during that week
--      i_stocktake_mtd_shrink_amt and i_stocktake_mtd_sales_amt are always 0
--
-- When salmth.pc calls this function :
--   i_stocktake_adj_retail = month_data.stocktake_bookstk_retail -
--                              month_data.stocktake_actstk_retail
--
--   if i_stocktake_adj_retail = NULL, this means that there was no stock count
--      taking place during that month
--
--      if i_stocktake_adj_retail NOT = NULL,
--         i_stocktake_mtd_shrink_amt = month_data. stocktake_mtd_shrink_amt
--             i_stocktake_mtd_sales_amt = month_data. stocktake_mtd_sales_amt
--
-- When salstake.pc calls this function  (salstake.pc calls this function to calculate
-- month_data.stocktake_bookstk_retail and month_data.stocktake_mtd_shrink_amt:
--      i_stocktake_adj_retail is always set to NULL
--------------------------------------------------------------------------------

FUNCTION COST_METHOD_CALC(IO_stk_cost                      IN OUT   NUMBER,
                          I_purch_cost                     IN       NUMBER,
                          I_rtv_cost                       IN       NUMBER,
                          I_net_sales_cost                 IN       NUMBER,
                          I_net_sales_retail               IN       NUMBER,
                          I_up_chrg_amt_profit             IN       NUMBER,
                          I_up_chrg_amt_exp                IN       NUMBER,
                          I_tsf_in_cost                    IN       NUMBER,
                          I_tsf_in_book_cost               IN       NUMBER,
                          I_tsf_out_cost                   IN       NUMBER,
                          I_tsf_out_book_cost              IN       NUMBER,
                          I_reclass_in_cost                IN       NUMBER,
                          I_reclass_out_cost               IN       NUMBER,
                          I_stock_adj_cost                 IN       NUMBER,
                          I_freight_claims_cost            IN       NUMBER,
                          I_stock_adj_cogs_cost            IN       NUMBER,
                          I_bud_shrinkage_pct              IN       NUMBER,
                          I_stocktake_adj_cost             IN       NUMBER,
                          I_stocktake_mtd_shrink_amt       IN       NUMBER,
                          I_stocktake_mtd_sales_amt        IN       NUMBER,
                          I_cost_variance_amt              IN       NUMBER,
                          I_intercompany_in_retail         IN       NUMBER,
                          I_intercompany_in_cost           IN       NUMBER,
                          I_intercompany_out_retail        IN       NUMBER,
                          I_intercompany_out_cost          IN       NUMBER,
                          I_intercompany_markup            IN       NUMBER,
                          I_intercompany_markdown          IN       NUMBER,
                          I_wo_activity_upd_inv            IN       NUMBER,
                          I_wo_activity_post_fin           IN       NUMBER,
                          I_deal_income_sales              IN       NUMBER,
                          I_deal_income_purch              IN       NUMBER,
                          I_restocking_fee                 IN       NUMBER,
                          IO_retail_cost_variance_amount   IN OUT   NUMBER,
                          IO_margin_cost_variance_amount   IN OUT   NUMBER,
                          O_intercompany_margin            IN OUT   NUMBER,
                          O_shrinkage_cost                 IN OUT   NUMBER,
                          O_gross_margin_amt               IN OUT   NUMBER,
                          I_franchise_sales_retail         IN       NUMBER,
                          I_franchise_sales_cost           IN       NUMBER,
                          I_franchise_returns_retail       IN       NUMBER,
                          I_franchise_returns_cost         IN       NUMBER,
                          I_recoverable_tax                IN       NUMBER   DEFAULT NULL,
                          O_error_message                  IN OUT   VARCHAR2)


   RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'STKLEDGR_ACCTING_SQL.COST_METHOD_CALC';
   L_franchise_net_sales_retail  NUMBER(20,4) := 0;
   L_franchise_net_sales_cost    NUMBER(20,4) := 0;

   cursor C_bud_shrink is
      select bud_shrink_ind
        from system_options;

BEGIN

   if LP_bud_shrink_ind IS NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_bud_shrink', 'system_options', NULL);
      OPEN C_bud_shrink;

      SQL_LIB.SET_MARK('FETCH', 'C_bud_shrink', 'system_options', NULL);
      FETCH C_bud_shrink INTO LP_bud_shrink_ind;

      SQL_LIB.SET_MARK('CLOSE', 'C_bud_shrink', 'system_options', NULL);
      CLOSE C_bud_shrink;
   end if;

   L_franchise_net_sales_retail := nvl(I_franchise_sales_retail,0) - nvl(I_franchise_returns_retail,0);
   L_franchise_net_sales_cost := nvl(I_franchise_sales_cost,0) - nvl(I_franchise_returns_cost,0);

   if LP_bud_shrink_ind = 'Y' then
      if I_stocktake_adj_cost is NULL then
         O_shrinkage_cost := nvl(I_bud_shrinkage_pct,0) * nvl(I_net_sales_cost,0);
      else
         O_shrinkage_cost :=   nvl(I_stocktake_mtd_shrink_amt,0)
                             + nvl(I_stocktake_adj_cost,0)
                             + (nvl(I_net_sales_cost,0) - nvl(I_stocktake_mtd_sales_amt,0))
                             * nvl(I_bud_shrinkage_pct,0);
      end if;
   else
      O_shrinkage_cost := (-1 * nvl(I_stock_adj_cost,0)) + nvl(I_stocktake_adj_cost,0);
   end if;

   IO_stk_cost :=   IO_stk_cost
                  + nvl(I_purch_cost,0)
                  - nvl(I_rtv_cost,0)
                  - nvl(I_net_sales_cost,0)
                  + nvl(I_tsf_in_cost,0)
                  + nvl(I_tsf_in_book_cost,0)
                  + nvl(I_intercompany_in_cost,0)
                  - nvl(I_tsf_out_cost,0)
                  - nvl(I_tsf_out_book_cost,0)
                  - nvl(I_intercompany_out_cost,0)
                  + nvl(I_up_chrg_amt_profit,0)
                  + nvl(I_up_chrg_amt_exp,0)
                  - O_shrinkage_cost
                  + nvl(I_reclass_in_cost,0)
                  - nvl(I_reclass_out_cost,0)
                  - nvl(I_cost_variance_amt,0)
                  - nvl(I_freight_claims_cost,0)
                  + nvl(I_stock_adj_cogs_cost,0)
                  + nvl(I_wo_activity_upd_inv,0)
                  - nvl(IO_margin_cost_variance_amount,0)
                  - L_franchise_net_sales_cost
                  + nvl(I_recoverable_tax,0);

   O_gross_margin_amt := (nvl(I_net_sales_retail,0)
                          + L_franchise_net_sales_retail)
                       - (nvl(I_net_sales_cost,0)
                          + L_franchise_net_sales_cost);

   O_intercompany_margin :=   nvl(I_intercompany_out_retail,0)
                            - nvl(I_intercompany_out_cost,0);

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             null);
      RETURN FALSE;

END COST_METHOD_CALC;

--------------------------------------------------------------------------
---    Name: cost_method_calc_retail
--- Purpose: Calculates the ending inventory value at retail for cost
---          method
---------------------------------------------------------------------------
FUNCTION COST_METHOD_CALC_RETAIL(IO_stk_retail                    IN OUT   NUMBER,
                                 I_purch_retail                   IN       NUMBER,
                                 I_rtv_retail                     IN       NUMBER,
                                 I_markup_retail                  IN       NUMBER,
                                 I_markup_can_retail              IN       NUMBER,
                                 I_net_sales_retail               IN       NUMBER,
                                 I_perm_markdown_retail           IN       NUMBER,
                                 I_promo_markdown_retail          IN       NUMBER,
                                 I_weight_variance_retail         IN       NUMBER,
                                 I_clear_markdown_retail          IN       NUMBER,
                                 I_markdown_can_retail            IN       NUMBER,
                                 I_tsf_in_retail                  IN       NUMBER,
                                 I_tsf_in_book_retail             IN       NUMBER,
                                 I_tsf_out_retail                 IN       NUMBER,
                                 I_tsf_out_book_retail            IN       NUMBER,
                                 I_stock_adj_retail               IN       NUMBER,
                                 I_freight_claims_retail          IN       NUMBER,
                                 I_stock_adj_cogs_retail          IN       NUMBER,
                                 I_empl_disc_retail               IN       NUMBER,
                                 I_stocktake_adj_retail           IN       NUMBER,
                                 I_reclass_in_retail              IN       NUMBER,
                                 I_reclass_out_retail             IN       NUMBER,
                                 I_intercompany_in_retail         IN       NUMBER,
                                 I_intercompany_in_cost           IN       NUMBER,
                                 I_intercompany_out_retail        IN       NUMBER,
                                 I_intercompany_out_cost          IN       NUMBER,
                                 I_intercompany_markup            IN       NUMBER,
                                 I_intercompany_markdown          IN       NUMBER,
                                 I_wo_activity_upd_inv            IN       NUMBER,
                                 I_wo_activity_post_fin           IN       NUMBER,
                                 I_deal_income_sales              IN       NUMBER,
                                 I_deal_income_purch              IN       NUMBER,
                                 I_restocking_fee                 IN       NUMBER,
                                 IO_retail_cost_variance_amount   IN OUT   NUMBER,
                                 IO_margin_cost_variance_amount   IN OUT   NUMBER,
                                 O_intercompany_margin            IN OUT   NUMBER,
                                 I_franchise_sales_retail         IN       NUMBER,
                                 I_franchise_returns_retail       IN       NUMBER,
                                 I_franchise_markup_retail        IN       NUMBER,
                                 I_franchise_markdown_retail      IN       NUMBER,
                                 O_error_message                  IN OUT   VARCHAR2)

   RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'STKLEDGR_ACCTING_SQL.COST_METHOD_CALC_RETAIL';
   L_stock_adj_retail    MONTH_DATA.STOCK_ADJ_RETAIL%TYPE;
   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_franchise_net_sales_retail  NUMBER(20,4) := 0;

BEGIN
   ---
   if LP_bud_shrink_ind IS NULL then
      ---
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_row) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   L_franchise_net_sales_retail := nvl(I_franchise_sales_retail,0) - nvl(I_franchise_returns_retail,0);

   if LP_bud_shrink_ind = 'Y' then
      L_stock_adj_retail := 0 ;
   else
      L_stock_adj_retail := nvl(I_stock_adj_retail,0);
   end if;
   ---
   IO_stk_retail :=   IO_stk_retail
                    + nvl(I_purch_retail,0)
                    - nvl(I_rtv_retail,0)
                    + nvl(I_markup_retail,0)
                    - nvl(I_markup_can_retail,0)
                    + nvl(I_tsf_in_retail,0)
                    + nvl(I_tsf_in_book_retail,0)
                    + nvl(I_intercompany_in_retail,0)
                    - nvl(I_tsf_out_retail,0)
                    - nvl(I_tsf_out_book_retail,0)
                    - nvl(I_intercompany_out_retail,0)
                    + nvl(I_reclass_in_retail,0)
                    - nvl(I_reclass_out_retail,0)
                    - nvl(I_net_sales_retail,0)
                    - nvl(I_perm_markdown_retail,0)
                    - nvl(I_promo_markdown_retail,0)
                    - nvl(I_weight_variance_retail,0)
                    - nvl(I_clear_markdown_retail,0)
                    + nvl(I_markdown_can_retail,0)
                    - nvl(I_empl_disc_retail,0)
                    + L_stock_adj_retail
                    - nvl(I_stocktake_adj_retail,0)
                    - nvl(I_freight_claims_retail,0)
                    + nvl(I_stock_adj_cogs_retail,0)
                    + nvl(I_intercompany_markup,0)
                    - nvl(I_intercompany_markdown,0)
                    - L_franchise_net_sales_retail
                    + nvl(I_franchise_markup_retail,0)
                    - nvl(I_franchise_markdown_retail,0);

   O_intercompany_margin :=   nvl(I_intercompany_out_retail,0)
                            - nvl(I_intercompany_out_cost,0);

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             null);
      RETURN FALSE;

END COST_METHOD_CALC_RETAIL;
-------------------------------------------------------------------------------------------
---    Name: WAC_CALC_QTY_CHANGE
--- Purpose: Contains the formula for calculating WAC (Weighted average cost)
---          when receiving PO or stock order, or doing receiver unit adjustment.
---          I_receipt_cost is the unit cost of I_receipt_qty. I_total_cost is the
---          total cost of I_receipt_qty. If defined, I_total_cost is used in the
---          calculation.
-------------------------------------------------------------------------------------------
FUNCTION WAC_CALC_QTY_CHANGE(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_new_wac             IN OUT  ITEM_LOC_SOH.AV_COST%TYPE,
                             O_neg_soh_wac_adj_amt IN OUT  ITEM_LOC_SOH.AV_COST%TYPE,
                             I_old_wac             IN      ITEM_LOC_SOH.AV_COST%TYPE,
                             I_old_soh             IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_receipt_cost        IN      ITEM_LOC_SOH.AV_COST%TYPE,
                             I_receipt_qty         IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_total_cost          IN      ITEM_LOC_SOH.AV_COST%TYPE DEFAULT NULL,
                             I_recalc_ind          IN      BOOLEAN DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'STKLEDGR_ACCTING_SQL.WAC_CALC_QTY_CHANGE';

   L_receipt_cost     ITEM_LOC_SOH.AV_COST%TYPE;
   L_total_cost       TRAN_DATA.TOTAL_COST%TYPE;

BEGIN
   O_new_wac := 0;
   -- the following wac adjustment amount will be used to write tran_data
   -- with tran code = 70
   -- this is to ensure stock ledger in sync with extented item on hand
   O_neg_soh_wac_adj_amt := 0;

   -- If I_total_cost is defined, it is for a simple pack catch weight item.
   -- I_total_cost should be used to derive unit cost instead of I_receipt_cost.
   if I_total_cost is NOT NULL then
      L_receipt_cost := I_total_cost / I_receipt_qty;
      L_total_cost := I_total_cost;
   else
      L_receipt_cost := I_receipt_cost;
      L_total_cost := I_receipt_cost * I_receipt_qty;
   end if;

   if I_receipt_qty > 0 then
      if I_old_soh > 0 then
         O_new_wac := (I_old_wac * I_old_soh + L_total_cost)/(I_old_soh + I_receipt_qty);
      else
         -- stock on hand is negative before the receiving
         O_new_wac := L_receipt_cost;
         O_neg_soh_wac_adj_amt := I_old_soh * (I_old_wac - O_new_wac);
      end if;

   else
        -- for example, for wrong store receipt, needs to back out WAC
        -- at the original ship to store
        -- Or doing negative receiver unit adjustment for a PO receipt
      if (I_old_soh + I_receipt_qty) <= 0 or I_recalc_ind = FALSE then
           O_new_wac := I_old_wac;
           O_neg_soh_wac_adj_amt := L_total_cost - O_new_wac * I_receipt_qty;
      else -- New SOH > 0 and I_recalc_ind = TRUE
         if (I_old_wac * I_old_soh + L_total_cost) > 0 then
            O_new_wac := (I_old_wac * I_old_soh + L_total_cost)/(I_old_soh + I_receipt_qty);
         else
            O_new_wac := I_old_wac;
            O_neg_soh_wac_adj_amt := L_total_cost - O_new_wac * I_receipt_qty;
         end if;
      end if;
   end if;

   if O_neg_soh_wac_adj_amt is NULL then
      O_neg_soh_wac_adj_amt :=0;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      RETURN FALSE;

END WAC_CALC_QTY_CHANGE;
--------------------------------------------------------------------------
---    Name: WAC_CALC_COST_CHANGE
--- Purpose: Contains the formula for calculating WAC (Weighted average cost)
---          when doing receiver cost adjustment for a PO
---------------------------------------------------------------------------
FUNCTION WAC_CALC_COST_CHANGE(O_error_message         IN OUT   VARCHAR2,
                              O_new_wac               IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                              O_neg_soh_wac_adj_amt   IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                              I_old_wac               IN       ITEM_LOC_SOH.AV_COST%TYPE,
                              I_soh                   IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_new_po_cost           IN       ITEM_LOC_SOH.AV_COST%TYPE,
                              I_old_po_cost           IN       ITEM_LOC_SOH.AV_COST%TYPE,
                              I_receipt_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'STKLEDGR_ACCTING_SQL.WAC_CALC_COST_CHANGE';
   L_std_av_ind       SYSTEM_OPTIONS.STD_AV_IND%TYPE;
   L_temp_wac         ITEM_LOC_SOH.AV_COST%TYPE;

BEGIN

   O_new_wac             := 0;
   O_neg_soh_wac_adj_amt := 0;
   ---
   if SYSTEM_OPTIONS_SQL.STD_AV_IND(O_error_message,
                                    L_std_av_ind) = FALSE then
      return FALSE;
   end if;
   ---
   -- When all transactions are entered accurately and in the correct order
   -- I_soh will always be >= I_receipt_qty.  If this is not the case, then
   -- most likely the missing units have already been transferred to another
   -- location or sold.
   -- If I_soh < I_receipt_qty, the WAC calculation does not give an accurate
   -- result.  Therefore only the cost of the units still in stock (I_soh)
   -- will be used in the WAC calculation.
   -- At this time we do not have the capability to assign
   -- the cost of the missing units to a secondary location.  Therefore, the
   -- cost of the missing units is written to a cost variance transaction
   -- so that the cost in the stock ledger will match the inventory cost.
   -- If I_soh <= 0, then WAC will not be modified and the entire
   -- transaction cost will be written to a cost variance.
   -- The cost variance will only be written if Average cost is being used.
   ---
   if (I_soh >= I_receipt_qty) and (I_soh > 0) then
     L_temp_wac := (I_soh * I_old_wac + I_receipt_qty * (I_new_po_cost - I_old_po_cost))/I_soh;
   if L_temp_wac <= 0 then 
     O_new_wac := I_old_wac; 
   if L_std_av_ind = 'A' then 
     O_neg_soh_wac_adj_amt := I_receipt_qty * (I_new_po_cost - I_old_po_cost); 
   end if; 
   else 
     O_new_wac := L_temp_wac; 
   end if; 

   elsif (I_soh > 0) then
      if I_soh < I_receipt_qty and I_new_po_cost > I_old_po_cost then
         O_new_wac := I_old_wac + (I_new_po_cost - I_old_po_cost);
         if L_std_av_ind = 'A' then
            O_neg_soh_wac_adj_amt := (I_receipt_qty - I_soh) * (I_new_po_cost - I_old_po_cost);
         end if;
      elsif I_soh < I_receipt_qty and I_new_po_cost < I_old_po_cost then
         L_temp_wac := I_old_wac + (I_new_po_cost - I_old_po_cost);
         if L_temp_wac <= 0 then
            O_new_wac := I_old_wac;
            if L_std_av_ind = 'A' then
               O_neg_soh_wac_adj_amt := (I_receipt_qty * (I_new_po_cost - I_old_po_cost));
            end if;
         else
            O_new_wac := L_temp_wac;
            if L_std_av_ind = 'A' then
               O_neg_soh_wac_adj_amt := (I_receipt_qty - I_soh) * (I_new_po_cost - I_old_po_cost);
            end if;
         end if;
      end if;
   else
      O_new_wac := I_old_wac;
      if L_std_av_ind = 'A' then
         O_neg_soh_wac_adj_amt := I_receipt_qty * (I_new_po_cost - I_old_po_cost);
      end if;
   end if;
      
   return TRUE;
      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END WAC_CALC_COST_CHANGE;

--------------------------------------------------------------------------------------------
-- Function: ALT_WAC_CALC_COST_CHANGE
-- Purpose : This function is called by Itemloc_update_sql.upd_av_cost_change_cost to
-- calculate the new WAC, adjustment quantity and negative SOH when the FIFO/standard
-- indicator is set to FIFO
--
-- This function allocates the current on hand quantity to recent PO receipts
-- using the FIFO algorithm.   Received quantities that fall within the allocated
-- on-hand quantity will be reflected in O_adj_qty.  After the on-hand quantity
-- is exhausted by the FIFO algorithm, any remaining received quantities will be
-- reflected in O_neg_soh_wac_adj_qty.  Any receipts that are not eligible for
-- cost adjustment (see below) will not be reflected in either quantity, so the
-- total of the two quantities may be less than the total received quantity on
-- the PO (or even zero if no receipts are eligible for cost adjustment).
--
-- (1)Only unmatched receipts (invc_match_status='U'),
-- unmatched line items (qty_matched=0) will be considered, since we assume
-- that matched invoices are already paid and the cost adjustment only applies
-- to the receipts remaining unmatched.  Similarly, BOL/Stock Order receipts and
-- inventory adjustments won't be considered since they aren't eligible for
-- cost adjustment.  Closed shipments (status_code='C') also won't be adjusted.
--
-- (2)If the adjustment is at a virtual warehouse, all receipts will be distributed
-- according to the PO received quantites.
--
-- (3)If the receipt is for a simple/non-pack
-- item, or for a pack that is broken on receipt, then we have to consider both
-- receipts of the non-pack item, and any packs containing the item that are
-- broken on receipt (receive_as_type='E').  The allocation will be based on the
-- on hand quantity for the item that was received into inventory - that is, the
-- pack item (if the item was received as a pack and not broken down), otherwise
-- the component item.
--
-- (4)Since we don't have any way to order the line-items within a receipt, we will
-- group/order by received date, PO#, and item.  Then we can guarantee that the same
-- inventory won't be counted against two different receipt line-items.
--
--------------------------------------------------------------------------------------------

FUNCTION ALT_WAC_CALC_COST_CHANGE (O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_new_wac              IN OUT  ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   O_adj_qty              IN OUT  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   O_neg_soh_wac_adj_qty  IN OUT  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   I_old_wac              IN      ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   I_soh                  IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   I_old_po_cost          IN      ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   I_po                   IN      SHIPMENT.ORDER_NO%TYPE,
                                   I_new_po_cost          IN      ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   I_location             IN      ITEM_LOC_SOH.LOC%TYPE,
                                   I_item                 IN      ITEM_LOC_SOH.ITEM%TYPE,
                                   I_pack_comp_rcvd_qty   IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT NULL,
                                   I_pack_item            IN      ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                   I_shipment             IN      SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                   I_adj_code             IN      TRAN_DATA.ADJ_CODE%TYPE DEFAULT 'C')

   RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'STKLEDGR_ACCTING_SQL.ALT_WAC_CALC_COST_CHANGE';
   L_tot_adj_qty        ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_fifo_on_hand       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_new_on_hand        ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_shp_order_no       SHIPMENT.ORDER_NO%TYPE;
   L_po_pwh_rec_qty     SHIPSKU.QTY_RECEIVED%TYPE;
   L_po_vwh_rec_qty     SHIPSKU.QTY_RECEIVED%TYPE;
   L_shs_qty_received   SHIPSKU.QTY_RECEIVED%TYPE;
   L_received_qty       SHIPSKU.QTY_RECEIVED%TYPE;
   L_adj_qty            SHIPSKU.QTY_RECEIVED%TYPE;
   
   L_first_received     DATE;

   L_loc                SHIPMENT.TO_LOC%TYPE;
   L_loc_type           SHIPMENT.TO_LOC_TYPE%TYPE;

   L_process_item       ITEM_MASTER.ITEM%TYPE;
   L_pack_qty           ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_receive_as_type    ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   L_l10n_fin_rec       L10N_FIN_REC := L10N_FIN_REC();

   cursor C_PHY_SHIPMENT is
   select physical_wh
     from wh
    where wh = I_location;
    
   --
   -- For ALC finalization, select the open shipment whether it is matched or unmatched and perform the 
   -- WAC calculation using FIFO logic (the latest one first) but the receive date needs to be after 
   -- the shipment in scope meaning the shipment passed.
   -- For Receiver Cost Adjustment, select the minimum receive date for the open shipment which is 
   -- unmatched. Use this date to choose any matched or unmatched shipment in C_shipment cusrsor after 
   -- the minimum receive date of the unmatched shipment. Any match shipemnt after unmatched shipment
   -- will be used to calculate the correct on hand  and adjust quantity for WAC calculation.
   --  

   cursor C_FIRST_RECEIVED is
      select MIN (s2.receive_date)
        from shipment s2
       where s2.order_no = I_po
         and s2.to_loc = L_loc
         and s2.shipment = NVL(I_shipment,s2.shipment)
         and ( I_adj_code = 'A' or 
               (I_adj_code = 'C' and s2.invc_match_status  = 'U'));
   --               
   cursor C_SHIPMENT is
       WITH matching_items   -- the first subquery handles the receipt for L_process_item
          AS ( SELECT L_process_item item,
                      L_pack_qty comp_qty
                 FROM dual
               UNION ALL   -- the second subquery handles pack items broken on receipt*/
               SELECT il.item,
                      v.qty comp_qty -- pack component quantity
                 FROM v_packsku_qty v,
                      item_loc il
                WHERE L_receive_as_type = 'E'          --(3)
                  AND il.item = v.pack_no              --(3)
                  AND il.loc = I_location              --(3)
                  AND NVL(il.receive_as_type,'E') = 'E'         --(3)
                  AND v.item = I_item)
          SELECT sh.order_no,
                 sh.shipment,
                 sh.invc_match_status,
                 sh.receive_date,
                 SUM(ss.qty_received) qty_received, 
                 ss.item,
                 i.comp_qty
            FROM matching_items i,
                 shipment sh,
                 shipsku ss
           WHERE L_loc_type != 'W'
             AND ss.item         = i.item 
             AND sh.shipment     = ss.shipment
             AND sh.order_no     IS NOT NULL    --(1)
             AND sh.to_loc       = I_location
             AND ss.qty_received != 0
             AND sh.receive_date >= L_first_received  
        GROUP BY sh.order_no, sh.shipment, sh.receive_date, ss.item, i.comp_qty, sh.invc_match_status
       UNION ALL
          SELECT /*+ index (ssl pk_shipsku_loc) */ 
                 sh.order_no,
                 sh.shipment,
                 sh.invc_match_status,
                 sh.receive_date ,
                 SUM(ssl.qty_received) qty_received, 
                 ss.item,
                 i.comp_qty
            FROM matching_items i,
                 shipment sh,
                 shipsku ss,
                 shipsku_loc ssl
           WHERE L_loc_type ='W'
             AND ss.item             = i.item 
             AND sh.shipment         = ss.shipment
             AND sh.order_no         IS NOT NULL  --(1)
             AND ssl.shipment        = ss.shipment   
             AND ssl.seq_no          = ss.seq_no 
             AND ssl.item            = ss.item 
             AND ssl.to_loc          = I_location
             AND ssl.qty_received  != 0
             AND sh.receive_date >= L_first_received
        GROUP BY sh.order_no, sh.shipment, sh.receive_date, ss.item, i.comp_qty,sh.invc_match_status
        ORDER BY receive_date DESC,  order_no, item; --(4)
        
   cursor C_GET_PACK_COMP_QTY is
      select qty
        from v_packsku_qty
       where pack_no = I_pack_item
         and item    = I_item;

BEGIN

   if I_soh is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_soh',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_old_po_cost is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_old_po_cost',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_po is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_po',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_new_po_cost is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_new_po_cost',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_loc_type,
                                   I_location) = FALSE then
      return FALSE;
   end if;

   -- requirement (2)
   if L_loc_type = 'W' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_PHY_SHIPMENT',
                       NULL,
                       'Location: '||to_char(I_location));
      open C_PHY_SHIPMENT;
      SQL_LIB.SET_MARK('FETCH',
                       'C_PHY_SHIPMENT',
                       NULL,
                       'Location: '||to_char(I_location));
      fetch C_PHY_SHIPMENT into L_loc;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PHY_SHIPMENT',
                       NULL,
                       'Location: '||to_char(I_location));
      close C_PHY_SHIPMENT;
      if L_loc is null then
         O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                L_program,
                                                'WH',
                                                'WH:'||I_location);
         return FALSE;
      end if;


   else -- multichannel wh
      L_loc := I_location;
   end if;

   -- requirement (3)
   if I_pack_item is NULL then
      L_process_item := I_item;
      L_receive_as_type := 'E';
      L_pack_qty := 1;
   else
      if L_loc_type = 'W' then
         if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                   L_receive_as_type,
                                                   I_pack_item,
                                                   I_location) = FALSE then
            return FALSE;
         end if;
      else
         L_receive_as_type := 'E';
      end if;
      if L_receive_as_type = 'E' then
         L_process_item := I_item;
      else
         L_process_item := I_pack_item;
      end if;

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_PACK_COMP_QTY',
                       NULL,
                       'Pack Item: '||to_char(I_pack_item)|| ', Component Item: '||to_char(I_item));
      open C_GET_PACK_COMP_QTY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_PACK_COMP_QTY',
                       NULL,
                       'Pack Item: '||to_char(I_pack_item)|| ', Component Item: '||to_char(I_item));
      fetch C_GET_PACK_COMP_QTY into L_pack_qty;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PACK_COMP_QTY',
                       NULL,
                       'Pack Item: '||to_char(I_pack_item)|| ', Component Item: '||to_char(I_item));
      close C_GET_PACK_COMP_QTY;
      if L_pack_qty is null then
         O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                L_program,
                                                'V_PACKSKU_QTY',
                                                'PACK_NO:'||I_pack_item||', ITEM:'||I_item);
         return FALSE;
      end if;
   end if; -- pack item

   -- Get pack or comp soh according to requirement (3)
   L_l10n_fin_rec.procedure_key  := 'GET_CURRENT_SOH_WAC';
   L_l10n_fin_rec.item           := L_process_item;
   L_l10n_fin_rec.source_id      := I_location;
   L_l10n_fin_rec.source_type    := L_loc_type;
   L_l10n_fin_rec.source_entity  := 'LOC';
   L_l10n_fin_rec.dest_type      := NULL;
   L_l10n_fin_rec.doc_id         := NULL;
   L_l10n_fin_rec.calc_fld_type  := 'TOTSTK';
   ---
   if L10N_SQL.EXEC_FUNCTION(O_error_message,
                             L_l10n_fin_rec)= FALSE then
      return FALSE;
   end if;
   ---
   L_fifo_on_hand := L_l10n_fin_rec.stock_on_hand;
   ---
   if L_process_item != I_item then
      L_fifo_on_hand := L_pack_qty * L_fifo_on_hand;
   end if;

   if L_fifo_on_hand is null then
      O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                             L_program,
                                             'ITEM_LOC_SOH',
                                             'LOCATION:'||I_location||', ITEM:'||L_process_item);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_FIRST_RECEIVED',
                    NULL,
                    'Order no: ' || I_po || ', To loc: '||L_loc);
   open C_FIRST_RECEIVED;
   SQL_LIB.SET_MARK('FETCH',
                    'C_FIRST_RECEIVED',
                    NULL,
                    'Order no: ' || I_po || ', To loc: '||L_loc);
   fetch C_FIRST_RECEIVED into L_first_received;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_FIRST_RECEIVED',
                    NULL,
                    'Order no: ' || I_po || ', To loc: '||L_loc);
   close C_FIRST_RECEIVED;

   O_adj_qty              := 0;
   L_tot_adj_qty          := 0;

   SQL_LIB.SET_MARK('OPEN',
                    'C_SHIPMENT',
                    NULL,
                    'Item: ' || L_process_item || ', To loc type: '||L_loc_type
                    || ', First received: ' || L_first_received || ', To loc: '||L_loc);
   for rec in C_SHIPMENT LOOP

      L_shp_order_no := rec.order_no;
      L_shs_qty_received := rec.qty_received * rec.comp_qty;
      -- allocation of on hand qty to the shipment
      L_new_on_hand := L_fifo_on_hand - L_shs_qty_received;
      if L_shp_order_no = I_po and rec.item = NVL(I_pack_item, I_item) then
         if ( (I_adj_code = 'C' and rec.invc_match_status != 'M') or
              (I_adj_code = 'A' and rec.shipment = NVL(I_shipment, rec.shipment))) then 
            if L_new_on_hand > 0 and L_fifo_on_hand > 0 then
               O_adj_qty := O_adj_qty + L_shs_qty_received;
            elsif L_shs_qty_received > 0 then
               -- if on hand is going negative, do a partial adjustment
               O_adj_qty := O_adj_qty + GREATEST(L_fifo_on_hand, 0);
            elsif L_shs_qty_received < 0 then
               -- if on hand is going positive, do a partial adjustment
               O_adj_qty := O_adj_qty - GREATEST(L_new_on_hand, 0);
            end if;
            -- this gives the total shipment quantity that is eligible for adjustment
            L_tot_adj_qty := L_tot_adj_qty + L_shs_qty_received;
         end if;
      end if;
      L_fifo_on_hand := L_new_on_hand;

   end LOOP;

   L_received_qty := NVL(I_pack_comp_rcvd_qty,L_tot_adj_qty);
   -- We will only recalculate if the total SOH is positive,
   -- the eligible shipment qty is positive,
   -- and the allocated qty is positive

   if I_soh > 0 and O_adj_qty > 0 and L_tot_adj_qty > 0 then

      L_adj_qty := LEAST(O_adj_qty, L_tot_adj_qty, I_soh);
      O_adj_qty := L_received_qty * (L_adj_qty/L_tot_adj_qty);
      
      O_new_wac := ROUND((I_soh * NVL(I_old_wac, 0)
                             + O_adj_qty * (I_new_po_cost - I_old_po_cost))
                         / I_soh, 4);
   else
      O_adj_qty := 0;
      O_new_wac := NVL(I_old_wac, 0);
   end if;

   O_neg_soh_wac_adj_qty  := L_received_qty - O_adj_qty;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END ALT_WAC_CALC_COST_CHANGE;
------------------------------------------------------------------------------------------
FUNCTION GET_GAFS_BOM(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_gafs_retail_bom   IN OUT   MONTH_DATA.HTD_GAFS_RETAIL%TYPE,
                      O_gafs_cost_bom     IN OUT   MONTH_DATA.HTD_GAFS_COST%TYPE,
                      I_dept              IN       MONTH_DATA.DEPT%TYPE,
                      I_class             IN       MONTH_DATA.CLASS%TYPE,
                      I_subclass          IN       MONTH_DATA.SUBCLASS%TYPE,
                      I_loc_type          IN       MONTH_DATA.LOC_TYPE%TYPE,
                      I_location          IN       MONTH_DATA.LOCATION%TYPE,
                      I_eom_date          IN       MONTH_DATA.EOM_DATE%TYPE,
                      I_opn_stk_retail    IN       MONTH_DATA.OPN_STK_RETAIL%TYPE,
                      I_opn_stk_cost      IN       MONTH_DATA.OPN_STK_COST%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)             := 'STKLEDGR_ACCTING_SQL.GET_GAFS_BOM';
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   L_return_code        VARCHAR2(5)   := NULL;
   L_out_dd             NUMBER(2)     := 0;
   L_out_mm             NUMBER(2)     := 0;
   L_out_yyyy           NUMBER(4)     := 0;
   L_out_half           MONTH_DATA.HALF_NO%TYPE;
   L_out_month          MONTH_DATA.MONTH_NO%TYPE;
   L_in_dd              NUMBER(2);
   L_in_mm              NUMBER(2);
   L_in_yyyy            NUMBER(4);

   L_prev_month_eom     MONTH_DATA.EOM_DATE%TYPE;
   L_fdom               MONTH_DATA.EOM_DATE%TYPE;
   L_htd_gafs_retail    MONTH_DATA.HTD_GAFS_RETAIL%TYPE;
   L_htd_gafs_cost      MONTH_DATA.HTD_GAFS_COST%TYPE;

   cursor C_MD_PREV_MONTH is
      select htd_gafs_retail,
             htd_gafs_cost
        from month_data
       where dept      = I_dept
         and class     = I_class
         and subclass  = I_subclass
         and loc_type  = I_loc_type
         and location  = I_location
         and eom_date  = L_prev_month_eom
         and currency_ind = 'L';

BEGIN

   if I_dept is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_dept',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_class is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_class',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_subclass is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_subclass',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_loc_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_loc_type',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_eom_date is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_eom_date',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   L_in_dd   := TO_CHAR(I_eom_date,'DD');
   L_in_mm   := TO_CHAR(I_eom_date,'MM');
   L_in_yyyy := TO_CHAR(I_eom_date,'YYYY');

   CAL_TO_454_HALF(L_in_dd,
                   L_in_mm,
                   L_in_yyyy,
                   L_out_half,
                   L_out_month,
                   L_return_code,
                   L_error_message);

   if L_return_code = 'FALSE' then
      O_error_message:= L_error_message;
      return FALSE;
   end if;

   if L_out_month = 1 then
      O_gafs_retail_bom := I_opn_stk_retail;
      O_gafs_cost_bom   := I_opn_stk_cost;
   else
      CAL_TO_454_FDOM(L_in_dd,
                      L_in_mm,
                      L_in_yyyy,
                      L_out_dd,
                      L_out_mm,
                      L_out_yyyy,
                      L_return_code,
                      L_error_message);

      if L_return_code = 'FALSE' then
         O_error_message:= L_error_message;
         return FALSE;
      end if;

      L_fdom := TO_DATE(LPAD(TO_CHAR(L_out_dd),  2,'0') ||
                        LPAD(TO_CHAR(L_out_mm),  2,'0') ||
                        LPAD(TO_CHAR(L_out_yyyy),4,'0'),'DDMMYYYY');

      L_in_dd   := TO_CHAR(L_fdom - 1,'DD');
      L_in_mm   := TO_CHAR(L_fdom - 1,'MM');
      L_in_yyyy := TO_CHAR(L_fdom - 1,'YYYY');

      L_prev_month_eom := to_date(LPAD(TO_CHAR(L_in_dd),  2,'0') ||
                                  LPAD(TO_CHAR(L_in_mm),  2,'0') ||
                                  LPAD(TO_CHAR(L_in_yyyy),4,'0'),'DDMMYYYY');

      open C_MD_PREV_MONTH;
      fetch C_MD_PREV_MONTH into L_htd_gafs_retail,
                                 L_htd_gafs_cost;
      close C_MD_PREV_MONTH;

      if (L_htd_gafs_retail is NULL and L_htd_gafs_cost is NULL) then
         L_htd_gafs_retail := 0;
         L_htd_gafs_cost   := 0;
      end if;

      O_gafs_retail_bom := L_htd_gafs_retail;
      O_gafs_cost_bom   := L_htd_gafs_cost;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END GET_GAFS_BOM;
------------------------------------------------------------------------------------------
FUNCTION GET_PREV_NEXT_EOM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_prev_eom_date   IN OUT   MONTH_DATA.EOM_DATE%TYPE,
                           O_next_eom_date   IN OUT   MONTH_DATA.EOM_DATE%TYPE,
                           I_eom_date        IN       MONTH_DATA.EOM_DATE%TYPE,
                           I_prev_next_ind   IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50)  := 'STKLEDGR_ACCTING_SQL.GET_PREV_NEXT_EOM';

   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_return_code          VARCHAR2(5)   := NULL;
   L_out_dd               NUMBER(2)     := 0;
   L_out_mm               NUMBER(2)     := 0;
   L_out_yyyy             NUMBER(4)     := 0;
   L_in_dd                NUMBER(2)     := 15;
   L_in_mm                NUMBER(2)     := 0;
   L_in_yyyy              NUMBER(4)     := 0;
   L_fdom_date            MONTH_DATA.EOM_DATE%TYPE;

BEGIN

   if I_eom_date is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_eom_date',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_prev_next_ind is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_prev_next_ind',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if I_prev_next_ind = 'P' then  -- retrieve the previous End of Month date

      L_in_dd   := TO_NUMBER(to_char(I_eom_date,'DD'),'09');
      L_in_mm   := TO_NUMBER(to_char(I_eom_date,'MM'),'09');
      L_in_yyyy := TO_NUMBER(to_char(I_eom_date,'YYYY'),'0999');

      if L_system_options_row.calendar_454_ind = 'C' then
         CAL_TO_CAL_FDOM(L_in_dd,
                         L_in_mm,
                         L_in_yyyy,
                         L_out_dd,
                         L_out_mm,
                         L_out_yyyy,
                         L_return_code,
                         O_error_message);

         if L_return_code = 'FALSE' then
            return FALSE;
         end if;
      else
         CAL_TO_454_FDOM(L_in_dd,
                         L_in_mm,
                         L_in_yyyy,
                         L_out_dd,
                         L_out_mm,
                         L_out_yyyy,
                         L_return_code,
                         O_error_message);

         if L_return_code = 'FALSE' then
             return FALSE;
         end if;
      end if;

      L_fdom_date := TO_DATE(LPAD(TO_CHAR(L_out_dd),   2, '0') ||
                             LPAD(TO_CHAR(L_out_mm),   2, '0') ||
                             LPAD(TO_CHAR(L_out_yyyy), 4, '0'), 'DDMMYYYY');

      O_prev_eom_date := L_fdom_date -1;

   else -- retrive the next End of Month date

      L_in_dd   := TO_NUMBER(to_char(I_eom_date + 1,'DD'),'09');
      L_in_mm   := TO_NUMBER(to_char(I_eom_date + 1,'MM'),'09');
      L_in_yyyy := TO_NUMBER(to_char(I_eom_date + 1,'YYYY'),'0999');

      if L_system_options_row.calendar_454_ind = 'C' then 
         CAL_TO_CAL_LDOM(L_in_dd,
                         L_in_mm,
                         L_in_yyyy,
                         L_out_dd,
                         L_out_mm,
                         L_out_yyyy,
                         L_return_code,
                         O_error_message);

         if L_return_code = 'FALSE' then
             return FALSE;
         end if;
      else
         CAL_TO_454_LDOM(L_in_dd,
                         L_in_mm,
                         L_in_yyyy,
                         L_out_dd,
                         L_out_mm,
                         L_out_yyyy,
                         L_return_code,
                         O_error_message);

         if L_return_code = 'FALSE' then
             return FALSE;
         end if;
      end if;

      O_next_eom_date := TO_DATE(LPAD(TO_CHAR(L_out_dd),   2,'0') ||
                                 LPAD(TO_CHAR(L_out_mm),   2,'0') ||
                                 LPAD(TO_CHAR(L_out_yyyy), 4,'0'),'DDMMYYYY');
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END GET_PREV_NEXT_EOM;
------------------------------------------------------------------------------------------
END STKLEDGR_ACCTING_SQL;
/
