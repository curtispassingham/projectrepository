
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_TOTAL_SQL AS

FUNCTION UPDATE_USAGE_DEPENDENCY(O_error_message    IN OUT VARCHAR2,
                                 I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                 I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                                 I_system_code      IN     SA_EXPORT_LOG.SYSTEM_CODE%TYPE,
                                 I_seq_no           IN     SA_EXPORT_LOG.SEQ_NO%TYPE,
                                 I_store            IN     SA_EXPORT_LOG.STORE%TYPE,
                                 I_day              IN     SA_EXPORT_LOG.DAY%TYPE)
      RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_TOTAL_DESC(O_error_message IN OUT VARCHAR2,
                        O_total_desc    IN OUT SA_TOTAL_HEAD.TOTAL_DESC%TYPE,
                        I_total_id      IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                        I_total_rev_no  IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
   RETURN BOOLEAN IS

   L_total_rev_no SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE := I_TOTAL_REV_NO;
   L_program      VARCHAR2(60)                    := 'SA_TOTAL_SQL.GET_TOTAL_DESC';

  cursor C_GET_DESC is
    select total_desc
      from v_sa_total_head_tl
     where upper(total_id) = upper(I_total_id)
       and total_rev_no = L_total_rev_no;

  cursor C_MAX_REV is
    select max(total_rev_no)
      from sa_total_head
     where upper(total_id) = upper(I_total_id);


BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_total_rev_no is NULL then
      SQL_LIB.SET_MARK('OPEN','C_MAX_REV','SA_TOTAL_HEAD',NULL);
      open C_MAX_REV;
      SQL_LIB.SET_MARK('FETCH','C_MAX_REV','SA_TOTAL_HEAD',NULL);
      fetch C_MAX_REV into L_total_rev_no;
      SQL_LIB.SET_MARK('CLOSE','C_MAX_REV','SA_TOTAL_HEAD',NULL);
      close C_MAX_REV;
   end if;
   ---
   O_total_desc := NULL;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_DESC','V_SA_TOTAL_HEAD_TL',NULL);
   open C_GET_DESC;
   SQL_LIB.SET_MARK('FETCH','C_GET_DESC','V_SA_TOTAL_HEAD_TL',NULL);
   fetch C_GET_DESC into O_total_desc;
   SQL_LIB.SET_MARK('CLOSE','C_GET_DESC','V_SA_TOTAL_HEAD_TL',NULL);
   close C_GET_DESC;
   ---
   if O_total_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TOTAL_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_DESC;
---------------------------------------------------------------------------
FUNCTION GET_OS_VALUE(O_error_message     IN OUT VARCHAR2,
                      O_os_value          IN OUT SA_SYS_VALUE.SYS_VALUE%TYPE,
                      I_os_level          IN     VARCHAR2,
                      I_store_day_seq_no  IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                      I_bal_group_seq_no  IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                      I_store             IN     SA_STORE_DAY.STORE%TYPE,
                      I_day               IN     SA_STORE_DAY.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'SA_TOTAL_SQL.GET_OS_VALUE';
   L_exist            VARCHAR2(1)  := 'N';
   L_os_value         SA_HQ_VALUE.HQ_VALUE%TYPE;
   L_total            SA_TOTAL.TOTAL_ID%TYPE := 'OVRSHT_';
   L_data_status      SA_STORE_DAY.DATA_STATUS%TYPE;
   L_store            SA_STORE_DAY.STORE%TYPE  := I_store;
   L_day              SA_STORE_DAY.DAY%TYPE    := I_day;

   cursor C_GET_HQ is
      select 'Y',
             v.hq_value
        from sa_hq_value v,
             sa_total t
       where v.total_seq_no           = t.total_seq_no
         and v.store                  = t.store
         and v.day                    = t.day
         and t.store_day_seq_no       = I_store_day_seq_no
         and t.store                  = L_store
         and t.day                    = L_day
         and ((t.bal_group_seq_no     = I_bal_group_seq_no
               and I_bal_group_seq_no   is NOT NULL
               and t.bal_group_seq_no   is NOT NULL)
             or (I_bal_group_seq_no     is NULL
                 and t.bal_group_seq_no is NULL))
         and t.total_id = L_total
         and v.value_rev_no = (select max(hq.value_rev_no)
                                 from sa_hq_value hq,
                                      sa_total tl
                                where tl.total_seq_no           = hq.total_seq_no
                                  and tl.store                  = hq.store
                                  and tl.day                    = hq.day
                                  and tl.store_day_seq_no       = I_store_day_seq_no
                                  and tl.store                  = L_store
                                  and tl.day                    = L_day
                                  and ((tl.bal_group_seq_no     = I_bal_group_seq_no
                                        and I_bal_group_seq_no    is NOT NULL
                                        and tl.bal_group_seq_no   is NOT NULL)
                                      or (I_bal_group_seq_no      is NULL
                                          and tl.bal_group_seq_no is NULL))
                                  and tl.total_id = L_total);
   ---
   cursor C_GET_STORE is
      select 'Y',
             s.store_value
        from sa_store_value s,
             sa_total t
       where s.total_seq_no           = t.total_seq_no
         and s.store                  = t.store
         and s.day                    = t.day
         and t.store_day_seq_no       = I_store_day_seq_no
         and t.store                  = L_store
         and t.day                    = L_day
         and ((t.bal_group_seq_no     = I_bal_group_seq_no
               and I_bal_group_seq_no   is NOT NULL
               and t.bal_group_seq_no   is NOT NULL)
             or (I_bal_group_seq_no     is NULL
                 and t.bal_group_seq_no is NULL))
         and t.total_id = L_total
         and s.value_rev_no = (select max(st.value_rev_no)
                                 from sa_store_value st,
                                      sa_total tl
                                where tl.total_seq_no           = st.total_seq_no
                                  and tl.store                  = st.store
                                  and tl.day                    = st.day
                                  and tl.store_day_seq_no       = I_store_day_seq_no
                                  and tl.store                  = L_store
                                  and tl.day                    = L_day
                                  and ((tl.bal_group_seq_no     = I_bal_group_seq_no
                                        and I_bal_group_seq_no    is NOT NULL
                                        and tl.bal_group_seq_no   is NOT NULL)
                                      or (I_bal_group_seq_no      is NULL
                                          and tl.bal_group_seq_no is NULL))
                                  and tl.total_id = L_total);
   ---
   cursor C_GET_SYS is
      select 'Y',
             s.sys_value
        from sa_sys_value s,
             sa_total t
       where s.total_seq_no           = t.total_seq_no
         and s.store                  = t.store
         and s.day                    = t.day
         and t.store_day_seq_no       = I_store_day_seq_no
         and t.store                  = L_store
         and t.day                    = L_day
         and ((t.bal_group_seq_no     = I_bal_group_seq_no
               and I_bal_group_seq_no   is NOT NULL
               and t.bal_group_seq_no   is NOT NULL)
             or (I_bal_group_seq_no     is NULL
                 and t.bal_group_seq_no is NULL))
         and t.total_id = L_total
         and s.value_rev_no = (select max(sy.value_rev_no)
                                 from sa_sys_value sy,
                                      sa_total tl
                                where tl.total_seq_no           = sy.total_seq_no
                                  and tl.store                  = sy.store
                                  and tl.day                    = sy.day
                                  and tl.store_day_seq_no       = I_store_day_seq_no
                                  and tl.store                  = L_store
                                  and tl.day                    = L_day
                                  and ((tl.bal_group_seq_no     = I_bal_group_seq_no
                                        and I_bal_group_seq_no    is NOT NULL
                                        and tl.bal_group_seq_no   is NOT NULL)
                                      or (I_bal_group_seq_no      is NULL
                                          and tl.bal_group_seq_no is NULL))
                                  and tl.total_id = L_total);
   ---
   cursor C_GET_POS is
      select 'Y',
             p.pos_value
        from sa_pos_value p,
             sa_total t
       where p.total_seq_no           = t.total_seq_no
         and p.store                  = t.store
         and p.day                    = t.day
         and t.store_day_seq_no       = I_store_day_seq_no
         and t.store                  = L_store
         and t.day                    = L_day
         and ((t.bal_group_seq_no     = I_bal_group_seq_no
               and I_bal_group_seq_no   is NOT NULL
               and t.bal_group_seq_no   is NOT NULL)
             or (I_bal_group_seq_no     is NULL
                 and t.bal_group_seq_no is NULL))
         and t.total_id = L_total
         and p.value_rev_no = (select max(ps.value_rev_no)
                                 from sa_pos_value ps,
                                      sa_total tl
                                where tl.total_seq_no           = ps.total_seq_no
                                  and tl.store                  = ps.store
                                  and tl.day                    = ps.day
                                  and tl.store_day_seq_no       = I_store_day_seq_no
                                  and tl.store                  = L_store
                                  and tl.day                    = L_day
                                  and ((tl.bal_group_seq_no     = I_bal_group_seq_no
                                        and I_bal_group_seq_no    is NOT NULL
                                        and tl.bal_group_seq_no   is NOT NULL)
                                      or (I_bal_group_seq_no      is NULL
                                          and tl.bal_group_seq_no is NULL))
                                  and tl.total_id = L_total);

BEGIN
   if I_store_day_seq_no is NULL or I_os_level NOT in ('S','B') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        I_store_day_seq_no,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if STORE_DAY_SQL.GET_DATA_STATUS(O_error_message,
                                    L_data_status,
                                    I_store_day_seq_no,
                                    L_store,
                                    L_day) = FALSE then
      return FALSE;
   end if;
   ---
   L_total := L_total || I_os_level;
   ---
   O_os_value := 0;
   ---
   if L_data_status = 'R' then
      L_exist := 'Y';
   end if;
   ---
   if L_exist = 'N' then
      SQL_LIB.SET_MARK('OPEN','C_GET_HQ','SA_HQ_VALUE',NULL);
      open C_GET_HQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_HQ','SA_HQ_VALUE',NULL);
      fetch C_GET_HQ into L_exist,
                          O_os_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_HQ','SA_HQ_VALUE',NULL);
      close C_GET_HQ;
   end if;
   ---
   if L_exist = 'N' then
      SQL_LIB.SET_MARK('OPEN','C_GET_STORE','SA_STORE_VALUE',NULL);
      open C_GET_STORE;
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE','SA_STORE_VALUE',NULL);
      fetch C_GET_STORE into L_exist,
                             O_os_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE','SA_STORE_VALUE',NULL);
      close C_GET_STORE;
   end if;
   ---
   if L_exist = 'N' then
      SQL_LIB.SET_MARK('OPEN','C_GET_SYS','SA_SYS_VALUE',NULL);
      open C_GET_SYS;
      SQL_LIB.SET_MARK('FETCH','C_GET_SYS','SA_SYS_VALUE',NULL);
      fetch C_GET_SYS into L_exist,
                           O_os_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SYS','SA_SYS_VALUE',NULL);
      close C_GET_SYS;
   end if;
   ---
   if L_exist = 'N' then
      SQL_LIB.SET_MARK('OPEN','C_GET_POS','SA_POS_VALUE',NULL);
      open C_GET_POS;
      SQL_LIB.SET_MARK('FETCH','C_GET_POS','SA_POS_VALUE',NULL);
      fetch C_GET_POS into L_exist,
                           O_os_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_POS','SA_POS_VALUE',NULL);
      close C_GET_POS;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_OS_VALUE;
-----------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_HEADER(O_error_message      IN OUT VARCHAR2,
                          O_total_desc         IN OUT SA_TOTAL_HEAD.TOTAL_DESC%TYPE,
                          O_total_cat          IN OUT SA_TOTAL_HEAD.TOTAL_CAT%TYPE,
                          O_store_update_ind   IN OUT SA_TOTAL_HEAD.STORE_UPDATE_IND%TYPE,
                          O_hq_update_ind      IN OUT SA_TOTAL_HEAD.HQ_UPDATE_IND%TYPE,
                          O_comb_total_ind     IN OUT SA_TOTAL_HEAD.COMB_TOTAL_IND%TYPE,
                          I_total_id           IN     SA_TOTAL.TOTAL_ID%TYPE,
                          I_total_rev_no       IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'SA_TOTAL_SQL.GET_TOTAL_HEADER';

   cursor C_GET_HEADER is
      select total_desc,
             total_cat,
             store_update_ind,
             hq_update_ind,
             comb_total_ind
        from sa_total_head
       where total_id     = I_total_id
         and total_rev_no = I_total_rev_no;

BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_HEADER','SA_TOTAL_HEAD',NULL);
   open C_GET_HEADER;
   SQL_LIB.SET_MARK('FETCH','C_GET_HEADER','SA_TOTAL_HEAD',NULL);
   fetch C_GET_HEADER into O_total_desc,
                           O_total_cat,
                           O_store_update_ind,
                           O_hq_update_ind,
                           O_comb_total_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_HEADER','SA_TOTAL_HEAD',NULL);
   close C_GET_HEADER;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_HEADER;
-----------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_VALUES(O_error_message IN OUT VARCHAR2,
                          O_pos_value     IN OUT SA_POS_VALUE.POS_VALUE%TYPE,
                          O_sys_value     IN OUT SA_SYS_VALUE.SYS_VALUE%TYPE,
                          O_store_value   IN OUT SA_STORE_VALUE.STORE_VALUE%TYPE,
                          O_hq_value      IN OUT SA_HQ_VALUE.HQ_VALUE%TYPE,
                          O_last_value    IN OUT SA_HQ_VALUE.HQ_VALUE%TYPE,
                          I_total_seq_no  IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                          I_store         IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                          I_day           IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60)      := 'SA_TOTAL_SQL.GET_TOTAL_VALUES';
   L_pos_rev_no       SA_POS_VALUE.POS_VALUE%TYPE;
   L_sys_rev_no       SA_SYS_VALUE.SYS_VALUE%TYPE;
   L_store_rev_no     SA_STORE_VALUE.STORE_VALUE%TYPE;
   L_hq_rev_no        SA_HQ_VALUE.HQ_VALUE%TYPE;
   L_store            SA_TOTAL.STORE%TYPE := I_store;
   L_day              SA_TOTAL.DAY%TYPE   := I_day;

   cursor C_POS_VALUE is
      select pos_value,
             value_rev_no
        from sa_pos_value
       where total_seq_no = I_total_seq_no
         and store = L_store
         and day = L_day
         and value_rev_no = (select max(value_rev_no)
                               from sa_pos_value
                              where total_seq_no = I_total_seq_no
                                and store = L_store
                                and day = L_day);
   cursor C_SYS_VALUE is
      select sys_value,
             value_rev_no
        from sa_sys_value
       where total_seq_no = I_total_seq_no
         and store = L_store
         and day = L_day
         and value_rev_no = (select max(value_rev_no)
                               from sa_sys_value
                              where total_seq_no = I_total_seq_no
                                and store = L_store
                                and day = L_day);
   cursor C_STORE_VALUE is
      select store_value,
             value_rev_no
        from sa_store_value
       where total_seq_no = I_total_seq_no
         and store = L_store
         and day = L_day
         and value_rev_no = (select max(value_rev_no)
                               from sa_store_value
                              where total_seq_no = I_total_seq_no
                                and store = L_store
                                and day = L_day);
   cursor C_HQ_VALUE is
      select hq_value,
             value_rev_no
        from sa_hq_value
       where total_seq_no = I_total_seq_no
         and store = L_store
         and day = L_day
         and value_rev_no = (select max(value_rev_no)
                               from sa_hq_value
                              where total_seq_no = I_total_seq_no
                                and store = L_store
                                and day = L_day);
BEGIN
   if I_total_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        I_total_seq_no,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_POS_VALUE','SA_POS_VALUE',NULL);
   open C_POS_VALUE;
   SQL_LIB.SET_MARK('FETCH','C_POS_VALUE','SA_POS_VALUE',NULL);
   fetch C_POS_VALUE into O_pos_value,
                          L_pos_rev_no;
   SQL_LIB.SET_MARK('CLOSE','C_POS_VALUE','SA_POS_VALUE',NULL);
   close C_POS_VALUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_SYS_VALUE','SA_SYS_VALUE',NULL);
   open C_SYS_VALUE;
   SQL_LIB.SET_MARK('FETCH','C_SYS_VALUE','SA_SYS_VALUE',NULL);
   fetch C_SYS_VALUE into O_sys_value,
                          L_sys_rev_no;
   SQL_LIB.SET_MARK('CLOSE','C_SYS_VALUE','SA_SYS_VALUE',NULL);
   close C_SYS_VALUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_STORE_VALUE','SA_STORE_VALUE',NULL);
   open C_STORE_VALUE;
   SQL_LIB.SET_MARK('FETCH','C_STORE_VALUE','SA_STORE_VALUE',NULL);
   fetch C_STORE_VALUE into O_store_value,
                            L_store_rev_no;
   SQL_LIB.SET_MARK('CLOSE','C_STORE_VALUE','SA_STORE_VALUE',NULL);
   close C_STORE_VALUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_HQ_VALUE','SA_HQ_VALUE',NULL);
   open C_HQ_VALUE;
   SQL_LIB.SET_MARK('FETCH','C_HQ_VALUE','SA_HQ_VALUE',NULL);
   fetch C_HQ_VALUE into O_hq_value,
                         L_hq_rev_no;
   SQL_LIB.SET_MARK('CLOSE','C_HQ_VALUE','SA_HQ_VALUE',NULL);
   close C_HQ_VALUE;
   ---
   if O_hq_value is not NULL then
      O_last_value := O_hq_value;
   elsif O_store_value is not NULL then
      O_last_value := O_store_value;
   elsif O_sys_value is not NULL then
      O_last_value := O_sys_value;
   elsif O_pos_value is not NULL then
      O_last_value := O_pos_value;
   else
      O_last_value := 0;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_VALUES;
-----------------------------------------------------------------------------------------
FUNCTION TOTAL_REV_EXIST(O_error_message IN OUT VARCHAR2,
                         O_rev_exist     IN OUT BOOLEAN,
                         I_total_seq_no  IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         I_store         IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                         I_day           IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_count    NUMBER(4);
   L_program  VARCHAR2(60)  := 'SA_TOTAL_SQL.TOTAL_REV_EXIST';
   L_store    SA_TOTAL.STORE%TYPE       := I_store;
   L_day      SA_TOTAL.DAY%TYPE         := I_day;

   cursor C_HQ_REV is
      select max(value_rev_no)
        from sa_hq_value
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day;

   cursor C_STORE_REV is
      select max(value_rev_no)
        from sa_store_value
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day;

   cursor C_SYS_REV is
      select max(value_rev_no)
        from sa_sys_value
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day;

   cursor C_POS_REV is
      select max(value_rev_no)
        from sa_pos_value
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day;

BEGIN
   if I_total_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        I_total_seq_no,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_rev_exist := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_HQ_REV','SA_HQ_VALUE',NULL);
   open C_HQ_REV;
   SQL_LIB.SET_MARK('FETCH','C_HQ_REV','SA_HQ_VALUE',NULL);
   fetch C_HQ_REV into L_count;
   SQL_LIB.SET_MARK('CLOSE','C_HQ_REV','SA_HQ_VALUE',NULL);
   close C_HQ_REV;
   ---
   if L_count > 1 then
      O_rev_exist := TRUE;
   end if;
   ---
   if O_rev_exist = FALSE then
      SQL_LIB.SET_MARK('OPEN','C_STORE_REV','SA_STORE_VALUE',NULL);
      open C_STORE_REV;
      SQL_LIB.SET_MARK('FETCH','C_STORE_REV','SA_STORE_VALUE',NULL);
      fetch C_STORE_REV into L_count;
      SQL_LIB.SET_MARK('CLOSE','C_STORE_REV','SA_STORE_VALUE',NULL);
      close C_STORE_REV;
   end if;
   ---
   if L_count > 1 then
      O_rev_exist := TRUE;
   end if;
   ---
   if O_rev_exist = FALSE then
      SQL_LIB.SET_MARK('OPEN','C_SYS_REV','SA_SYS_VALUE',NULL);
      open C_SYS_REV;
      SQL_LIB.SET_MARK('FETCH','C_SYS_REV','SA_SYS_VALUE',NULL);
      fetch C_SYS_REV into L_count;
      SQL_LIB.SET_MARK('CLOSE','C_SYS_REV','SA_SYS_VALUE',NULL);
      close C_SYS_REV;
   end if;
   ---
   if L_count > 1 then
      O_rev_exist := TRUE;
   end if;
   ---
   if O_rev_exist = FALSE then
      SQL_LIB.SET_MARK('OPEN','C_POS_REV','SA_POS_VALUE',NULL);
      open C_POS_REV;
      SQL_LIB.SET_MARK('FETCH','C_POS_REV','SA_POS_VALUE',NULL);
      fetch C_POS_REV into L_count;
      SQL_LIB.SET_MARK('CLOSE','C_POS_REV','SA_POS_VALUE',NULL);
      close C_POS_REV;
   end if;
   ---
   if L_count > 1 then
      O_rev_exist := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TOTAL_REV_EXIST;
-----------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_INFO(O_error_message      IN OUT VARCHAR2,
                        O_total_id           IN OUT SA_TOTAL.TOTAL_ID%TYPE,
                        O_total_desc         IN OUT SA_TOTAL_HEAD.TOTAL_DESC%TYPE,
                        O_total_cat          IN OUT SA_TOTAL_HEAD.TOTAL_CAT%TYPE,
                        O_total_cat_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_pos_value          IN OUT SA_POS_VALUE.POS_VALUE%TYPE,
                        O_sys_value          IN OUT SA_SYS_VALUE.SYS_VALUE%TYPE,
                        O_store_value        IN OUT SA_STORE_VALUE.STORE_VALUE%TYPE,
                        O_hq_value           IN OUT SA_HQ_VALUE.HQ_VALUE%TYPE,
                        O_comb_total_ind     IN OUT SA_TOTAL_HEAD.COMB_TOTAL_IND%TYPE,
                        O_store_update_ind   IN OUT SA_TOTAL_HEAD.STORE_UPDATE_IND%TYPE,
                        O_hq_update_ind      IN OUT SA_TOTAL_HEAD.HQ_UPDATE_IND%TYPE,
                        I_total_rev_no       IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                        I_total_status       IN     SA_TOTAL_HEAD.STATUS%TYPE,
                        I_total_seq_no       IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                        I_store              IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                        I_day                IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(100) := 'SA_TOTAL_SQL.GET_TOTAL_INFO';
   L_store                 SA_TOTAL.STORE%TYPE  := I_store;
   L_day                   SA_TOTAL.DAY%TYPE    := I_day;

   cursor C_GET_TOTAL_INFO is
      select h.total_cat,
             c.code_desc,
             t.total_id,
             h.total_desc,
             h.comb_total_ind,
             h.store_update_ind,
             h.hq_update_ind
        from sa_total t,
             sa_total_head h,
             code_detail c
       where t.total_id = h.total_id
         and h.total_cat = c.code
         and c.code_type = 'SATC'
         and t.total_seq_no = I_total_seq_no
         and t.store        = L_store
         and t.day          = L_day
         and h.total_rev_no = I_total_rev_no
         and h.status = I_total_status;

   ---
   cursor C_GET_POS_VALUE is
      select pos_value
        from sa_pos_value
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day
         and total_rev_no = I_total_rev_no         
         and value_rev_no = (select MAX(value_rev_no)
                                from sa_pos_value
                               where total_seq_no = I_total_seq_no
                                 and store        = L_store
                                 and day          = L_day);
   ---
   cursor C_GET_POS_VALUE_WKSHT is
      select pos_value
        from sa_pos_value_wksht
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day
         and total_rev_no = I_total_rev_no         
         and value_rev_no = (select MAX(value_rev_no)
                                from sa_pos_value_wksht
                               where total_seq_no = I_total_seq_no
                                 and store        = L_store
                                 and day          = L_day);
   ---
   cursor C_GET_SYS_VALUE is
      select sys_value
        from sa_sys_value
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day
         and total_rev_no = I_total_rev_no
         and value_rev_no = (select MAX(value_rev_no)
                                from sa_sys_value
                               where total_seq_no = I_total_seq_no
                                 and store        = L_store
                                 and day          = L_day);
   ---
   cursor C_GET_SYS_VALUE_WKSHT is
      select sys_value
        from sa_sys_value_wksht
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day
         and total_rev_no = I_total_rev_no
         and value_rev_no = (select MAX(value_rev_no)
                                from sa_sys_value_wksht
                               where total_seq_no = I_total_seq_no
                                 and store        = L_store
                                 and day          = L_day);
   ---
   cursor C_GET_STORE_VALUE is
      select store_value
        from sa_store_value
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day
         and value_rev_no = (select MAX(value_rev_no)
                                from sa_store_value
                               where total_seq_no = I_total_seq_no
                                 and store        = L_store
                                 and day          = L_day);
   ---
   cursor C_GET_HQ_VALUE is
      select hq_value
        from sa_hq_value
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day
         and value_rev_no = (select MAX(value_rev_no)
                               from sa_hq_value
                              where total_seq_no = I_total_seq_no
                                and store        = L_store
                                and day          = L_day);

BEGIN
   if I_total_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        I_total_seq_no,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_INFO',NULL,NULL);
   open C_GET_TOTAL_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_INFO',NULL,NULL);
   fetch C_GET_TOTAL_INFO into O_total_cat,
                               O_total_cat_desc,
                               O_total_id,
                               O_total_desc,
                               O_comb_total_ind,
                               O_store_update_ind,
                               O_hq_update_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_INFO',NULL,NULL);
   close C_GET_TOTAL_INFO;
   ---
   if I_total_status = 'A' then
      SQL_LIB.SET_MARK('OPEN','C_GET_POS_VALUE','SA_POS_VALUE',NULL);
      open C_GET_POS_VALUE;
      SQL_LIB.SET_MARK('FETCH','C_GET_POS_VALUE','SA_POS_VALUE',NULL);
      fetch C_GET_POS_VALUE into O_pos_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_POS_VALUE','SA_POS_VALUE',NULL);
      close C_GET_POS_VALUE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_SYS_VALUE','SA_SYS_VALUE',NULL);
      open C_GET_SYS_VALUE;
      SQL_LIB.SET_MARK('FETCH','C_GET_SYS_VALUE','SA_SYS_VALUE',NULL);
      fetch C_GET_SYS_VALUE into O_sys_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SYS_VALUE','SA_SYS_VALUE',NULL);
      close C_GET_SYS_VALUE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_VALUE','SA_STORE_VALUE',NULL);
      open C_GET_STORE_VALUE;
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_VALUE','SA_STORE_VALUE',NULL);
      fetch C_GET_STORE_VALUE into O_store_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_VALUE','SA_STORE_VALUE',NULL);
      close C_GET_STORE_VALUE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_HQ_VALUE','SA_HQ_VALUE',NULL);
      open C_GET_HQ_VALUE;
      SQL_LIB.SET_MARK('FETCH','C_GET_HQ_VALUE','SA_HQ_VALUE',NULL);
      fetch C_GET_HQ_VALUE into O_hq_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_HQ_VALUE','SA_HQ_VALUE',NULL);
      close C_GET_HQ_VALUE;
   elsif I_total_status in ('W', 'S') then
      SQL_LIB.SET_MARK('OPEN','C_GET_POS_VALUE_WKSHT','SA_POS_VALUE_WKSHT',NULL);
      open C_GET_POS_VALUE_WKSHT;
      SQL_LIB.SET_MARK('FETCH','C_GET_POS_VALUE_WKSHT','SA_POS_VALUE_WKSHT',NULL);
      fetch C_GET_POS_VALUE_WKSHT into O_pos_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_POS_VALUE_WKSHT','SA_POS_VALUE_WKSHT',NULL);
      close C_GET_POS_VALUE_WKSHT;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_SYS_VALUE_WKSHT','SA_SYS_VALUE_WKSHT',NULL);
      open C_GET_SYS_VALUE_WKSHT;
      SQL_LIB.SET_MARK('FETCH','C_GET_SYS_VALUE_WKSHT','SA_SYS_VALUE_WKSHT',NULL);
      fetch C_GET_SYS_VALUE_WKSHT into O_sys_value;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SYS_VALUE_WKSHT','SA_SYS_VALUE_WKSHT',NULL);
      close C_GET_SYS_VALUE_WKSHT;
   elsif I_total_status in ('DI', 'DE') then
      return TRUE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_STATUS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_INFO;
---------------------------------------------------------------------------------
FUNCTION INSERT_REVISION(O_error_message     IN OUT VARCHAR2,
                         I_total_seq_no      IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         I_store_day_seq_no  IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                         I_value             IN     SA_STORE_VALUE.STORE_VALUE%TYPE,
                         I_update_value      IN     VARCHAR2,
                         I_update_id         IN     SA_STORE_VALUE.UPDATE_ID%TYPE,
                         I_store             IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                         I_day               IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'SA_TOTAL_SQL.INSERT_REVISION';
   L_max_rev_no   SA_STORE_VALUE.VALUE_REV_NO%TYPE;
   L_exp_rev_no   SA_EXPORTED_REV.REV_NO%TYPE;
   L_err_rev_no   SA_ERROR_REV.REV_NO%TYPE;
   L_precedence   V_SA_GET_TOTAL.PRECEDENCE%TYPE   := NULL;
   L_value_rev_no V_SA_GET_TOTAL.VALUE_REV_NO%TYPE := NULL;
   L_error_seq_no SA_ERROR_REV.ERROR_SEQ_NO%TYPE;
   L_store        SA_TOTAL.STORE%TYPE              := I_store;
   L_day          SA_TOTAL.DAY%TYPE                := I_day;

   cursor C_GET_MAX_VALUE_REV_NO is
      select NVL(MAX(value_rev_no),0)
        from v_sa_total
       where total_seq_no = I_total_seq_no
         and store        = L_store
         and day          = L_day;

   cursor C_GET_ERR_REV_NO is
      select NVL(max(rev_no),0)
        from sa_error_rev
       where store_day_seq_no = I_store_day_seq_no
         and total_seq_no     = I_total_seq_no
         and store            = L_store
         and day              = L_day
         and error_seq_no     = L_error_seq_no;

   cursor C_GET_ERRORS is
      select error_seq_no,
             bal_group_seq_no,
             tran_seq_no,
             error_code,
             key_value_1,
             key_value_2,
             rec_type,
             store_override_ind,
             hq_override_ind,
             update_id,
             update_datetime,
             orig_value
        from sa_error
       where store_day_seq_no = I_store_day_seq_no
         and store            = L_store
         and day              = L_day
         and total_seq_no     = I_total_seq_no;

BEGIN
   if I_total_seq_no is NULL or
      I_store_day_seq_no is NULL or
      I_update_value is NULL or
      I_update_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        I_total_seq_no,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if SA_TOTAL_SQL.GET_PRECEDENCE(O_error_message,
                                  I_total_seq_no,
                                  L_precedence,
                                  L_value_rev_no,
                                  L_store,
                                  L_day) = FALSE then
      return FALSE;
   end if;
   ---
   if (I_update_value = 'S' and L_precedence >= 2) or
      (I_update_value = 'H' and L_precedence >= 1) then
      insert into sa_exported_rev(store,
                                  day,
                                  export_seq_no,
                                  rev_no,
                                  store_day_seq_no,
                                  tran_seq_no,
                                  total_seq_no,
                                  acct_period,
                                  system_code,
                                  exp_datetime,
                                  status)
                           select store,
                                  day,
                                  export_seq_no,
                                  L_value_rev_no,
                                  store_day_seq_no,
                                  tran_seq_no,
                                  total_seq_no,
                                  acct_period,
                                  system_code,
                                  exp_datetime,
                                  status
                             from sa_exported
                            where store_day_seq_no = I_store_day_seq_no
                              and total_seq_no     = I_total_seq_no
                              and store            = L_store
                              and day              = L_day;

      delete from sa_exported
            where store_day_seq_no = I_store_day_seq_no
              and total_seq_no     = I_total_seq_no
              and store            = L_store
              and day              = L_day;

   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_MAX_VALUE_REV_NO','V_SA_TOTAL','TOTAL SEQ: '||to_char(I_total_seq_no));
   open C_GET_MAX_VALUE_REV_NO;
   SQL_LIB.SET_MARK('FETCH','C_GET_MAX_VALUE_REV_NO','V_SA_TOTAL','TOTAL SEQ: '||to_char(I_total_seq_no));
   fetch C_GET_MAX_VALUE_REV_NO into L_max_rev_no;
   SQL_LIB.SET_MARK('CLOSE','C_GET_MAX_VALUE_REV_NO','V_SA_TOTAL','TOTAL SEQ: '||to_char(I_total_seq_no));
   close C_GET_MAX_VALUE_REV_NO;
   ---
   if I_update_value = 'S' then
      insert into sa_store_value(store,
                                 day,
                                 total_seq_no,
                                 value_rev_no,
                                 store_value,
                                 update_id,
                                 update_datetime)
                          values(L_store,
                                 L_day,
                                 I_total_seq_no,
                                 L_max_rev_no + 1,
                                 NVL(I_value,0),
                                 I_update_id,
                                 SYSDATE);
   elsif I_update_value = 'H' then
      insert into sa_hq_value(store,
                              day,
                              total_seq_no,
                              value_rev_no,
                              hq_value,
                              update_id,
                              update_datetime)
                       values(L_store,
                              L_day,
                              I_total_seq_no,
                              L_max_rev_no + 1,
                              NVL(I_value,0),
                              I_update_id,
                              SYSDATE);
   end if;
   ---
   if L_max_rev_no > 0 then
      FOR C_rec in C_GET_ERRORS LOOP
         L_error_seq_no := C_rec.error_seq_no;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_ERR_REV_NO','SA_ERROR_REV','TOTAL SEQ: '||to_char(I_total_seq_no));
         open C_GET_ERR_REV_NO;
         SQL_LIB.SET_MARK('FETCH','C_GET_ERR_REV_NO','SA_ERROR_REV','TOTAL SEQ: '||to_char(I_total_seq_no));
         fetch C_GET_ERR_REV_NO into L_err_rev_no;
         SQL_LIB.SET_MARK('CLOSE','C_GET_ERR_REV_NO','SA_ERROR_REV','TOTAL SEQ: '||to_char(I_total_seq_no));
         close C_GET_ERR_REV_NO;
         ---
         insert into sa_error_rev(store,
                                  day,
                                  error_seq_no,
                                  rev_no,
                                  store_day_seq_no,
                                  bal_group_seq_no,
                                  total_seq_no,
                                  value_rev_no,
                                  tran_seq_no,
                                  tran_rev_no,
                                  error_code,
                                  key_value_1,
                                  key_value_2,
                                  rec_type,
                                  store_override_ind,
                                  hq_override_ind,
                                  update_id,
                                  update_datetime,
                                  orig_value)
                           values(L_store,
                                  L_day,
                                  C_rec.error_seq_no,
                                  L_err_rev_no + 1,
                                  I_store_day_seq_no,
                                  C_rec.bal_group_seq_no,
                                  I_total_seq_no,
                                  L_max_rev_no, --value_rev_no,
                                  C_rec.tran_seq_no,
                                  NULL, --tran_rev_no,
                                  C_rec.error_code,
                                  C_rec.key_value_1,
                                  C_rec.key_value_2,
                                  C_rec.rec_type,
                                  C_rec.store_override_ind,
                                  C_rec.hq_override_ind,
                                  C_rec.update_id,
                                  C_rec.update_datetime,
                                  C_rec.orig_value);
      end LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_REVISION;
------------------------------------------------------------------------------------
FUNCTION UPDATE_ID_EXISTS(O_error_message   IN OUT  VARCHAR2,
                          O_exists          IN OUT  BOOLEAN,
                          I_update_id       IN      SA_TOTAL_HEAD.UPDATE_ID%TYPE,
                          I_store           IN      SA_TOTAL.STORE%TYPE DEFAULT NULL,
                          I_day             IN      SA_TOTAL.DAY%TYPE DEFAULT NULL)

 RETURN BOOLEAN IS

   L_exists   VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'SA_TOTAL_SQL.UPDATE_ID_EXISTS';
   L_store    V_SA_TOTAL.STORE%TYPE     := I_store;
   L_day      V_SA_TOTAL.DAY%TYPE       := I_day;

   cursor C_EXISTS is
      select 'Y'
        from v_sa_total
       where store      = L_store
         and day        = L_day
         and update_id  = I_update_id;

BEGIN
   if I_update_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_update_id,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','V_SA_TOTAL','Update ID: '||I_update_id);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','V_SA_TOTAL','Update ID: '||I_update_id);
   fetch C_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','V_SA_TOTAL','Update ID: '||I_update_id);
   close C_EXISTS;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END UPDATE_ID_EXISTS;
------------------------------------------------------------------------------------
FUNCTION GET_COMB_TOTAL_INFO(O_error_message   IN OUT VARCHAR2,
                             O_total_desc      IN OUT SA_TOTAL_HEAD.TOTAL_DESC%TYPE,
                             O_total_cat       IN OUT SA_TOTAL_HEAD.TOTAL_CAT%TYPE,
                             O_total_cat_desc  IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                             I_detail_total_id IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SA_TOTAL_SQL.GET_COMB_TOTAL_INFO';

   cursor C_GET_COMB_TOTAL_INFO is
      select h.total_cat,
             c.code_desc,
             h.total_desc
        from sa_total_head h,
             code_detail c
       where h.total_id  = I_detail_total_id
         and h.total_cat = c.code
         and c.code_type = 'SATC';

BEGIN
   if I_detail_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_COMB_TOTAL_INFO','SA_TOTAL_HEAD,CODE_DETAIL',NULL);
   open C_GET_COMB_TOTAL_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_COMB_TOTAL_INFO','SA_TOTAL_HEAD,CODE_DETAIL',NULL);
   fetch C_GET_COMB_TOTAL_INFO into O_total_cat,
                O_total_cat_desc,
                O_total_desc;
   SQL_LIB.SET_MARK('CLOSE','C_GET_COMB_TOTAL_INFO','SA_TOTAL_HEAD,CODE_DETAIL',NULL);
   close C_GET_COMB_TOTAL_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_COMB_TOTAL_INFO;
------------------------------------------------------------------------------------
FUNCTION UPDATE_EXP_LOG(O_error_message    IN OUT VARCHAR2,
                        I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                        I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                        I_source           IN     SA_EXPORT_LOG.SYSTEM_CODE%TYPE DEFAULT NULL,
                        I_store            IN     SA_EXPORT_LOG.STORE%TYPE DEFAULT NULL,
                        I_day              IN     SA_EXPORT_LOG.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64)              := 'SA_TOTAL_SQL.UPDATE_EXP_LOG';
   L_max_seq_no    SA_EXPORT_LOG.SEQ_NO%TYPE := 0;
   L_sysdate       DATE                      := SYSDATE;
   L_system_code   SA_EXPORT_LOG.SYSTEM_CODE%TYPE;
   L_store         SA_EXPORT_LOG.STORE%TYPE  := I_store;
   L_day           SA_EXPORT_LOG.DAY%TYPE    := I_day;

   cursor C_GET_SYSTEMS is
      select l.system_code,
             l.seq_no
        from sa_export_log l,
             sa_export_options o
       where l.system_code = o.system_code
         and l.store_day_seq_no = I_store_day_seq_no
         and l.store            = L_store
         and l.day              = L_day
         and o.multiple_exp_ind = 'Y'
         and l.status           = 'E'
         and l.system_code     != NVL(I_source, ' ')
         and l.seq_no           = (select MAX(e.seq_no)
                                     from sa_export_log e
                                    where e.store_day_seq_no = I_store_day_seq_no
                                      and e.store            = L_store
                                      and e.day              = L_day
                                      and e.system_code      = l.system_code);

BEGIN
   if I_store_day_seq_no is NULL or I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        I_store_day_seq_no,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   for C_rec in C_GET_SYSTEMS loop
      L_system_code := C_rec.system_code;
      L_max_seq_no := C_rec.seq_no + 1;
      ---
      if (UPDATE_USAGE_DEPENDENCY(O_error_message,
                                  I_total_id,
                                  I_store_day_seq_no,
                                  L_system_code,
                                  L_max_seq_no,
                                  L_store,
                                  L_day) = FALSE) then
         return FALSE;
      end if;
      ---
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END UPDATE_EXP_LOG;
--------------------------------------------------------------------------------------
FUNCTION GET_OS_OPERATOR(O_error_message  IN OUT VARCHAR2,
                         O_os_operator    IN OUT SA_TOTAL_HEAD.OS_OPERATOR%TYPE,
                         I_total_rev_no   IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                         I_total_seq_no   IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         I_store          IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                         I_day            IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'SA_TOTAL_SQL.GET_OS_OPERATOR';
   L_store     SA_TOTAL.STORE%TYPE      := I_store;
   L_day       SA_TOTAL.DAY%TYPE        := I_day;

   cursor C_GET_OS_OPERATOR is
      select th.os_operator
        from sa_total_head th, sa_total t
       where th.total_id     = t.total_id
         and t.total_seq_no  = I_total_seq_no
         and t.store         = L_store
         and t.day           = L_day
         and th.total_rev_no = I_total_rev_no;

BEGIN
   if I_total_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        I_total_seq_no,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_OS_OPERATOR','SA_TOTAL_HEAD',NULL);
   open C_GET_OS_OPERATOR;
   SQL_LIB.SET_MARK('FETCH','C_GET_OS_OPERATOR','SA_TOTAL_HEAD',NULL);
   fetch C_GET_OS_OPERATOR into O_os_operator;
   SQL_LIB.SET_MARK('CLOSE','C_GET_OS_OPERATOR','SA_TOTAL_HEAD',NULL);
   close C_GET_OS_OPERATOR;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_OS_OPERATOR;
---------------------------------------------------------------------------------------
FUNCTION GET_REF_LABELS(O_error_message  IN OUT VARCHAR2,
                        O_ref_label_code_1 IN OUT SA_TOTAL_HEAD.REF_LABEL_CODE_1%TYPE,
                        O_ref_label_desc_1 IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_ref_label_code_2 IN OUT SA_TOTAL_HEAD.REF_LABEL_CODE_1%TYPE,
                        O_ref_label_desc_2 IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_ref_label_code_3 IN OUT SA_TOTAL_HEAD.REF_LABEL_CODE_1%TYPE,
                        O_ref_label_desc_3 IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'SA_TOTAL_SQL.GET_REF_LABELS';

   cursor C_GET_LABEL is
      select ref_label_code_1,
             ref_label_code_2,
             ref_label_code_3
        from sa_total_head
       where upper(total_id) = upper(I_total_id)
         and total_rev_no = (select MAX(total_rev_no)
                from sa_total_head
                    where upper(total_id) = upper(I_total_id));

BEGIN
   O_ref_label_desc_1 := NULL;
   O_ref_label_desc_2 := NULL;
   O_ref_label_desc_3 := NULL;
   ---
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_LABEL','SA_TOTAL_HEAD',NULL);
   open C_GET_LABEL;

   SQL_LIB.SET_MARK('FETCH','C_GET_LABEL','SA_TOTAL_HEAD',NULL);
   fetch C_GET_LABEL into O_ref_label_code_1,
                          O_ref_label_code_2,
                          O_ref_label_code_3;

   SQL_LIB.SET_MARK('CLOSE','C_GET_LABEL','SA_TOTAL_HEAD',NULL);
   close C_GET_LABEL;
   ---
   if O_ref_label_code_1 is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'TRFL',
                                     O_ref_label_code_1,
                                     O_ref_label_desc_1) = FALSE then
         return FALSE;
       end if;
   end if;
   ---
   if O_ref_label_code_2 is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'TRFL',
                                     O_ref_label_code_2,
                                     O_ref_label_desc_2) = FALSE then
         return FALSE;
       end if;
   end if;
   ---
   if O_ref_label_code_3 is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'TRFL',
                                     O_ref_label_code_3,
                                     O_ref_label_desc_3) = FALSE then
         return FALSE;
       end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_REF_LABELS;
----------------------------------------------------------------------------------------
FUNCTION TOTAL_LOC_EXISTS(O_error_message  IN OUT VARCHAR2,
                          O_exists         IN OUT BOOLEAN,
                          I_total_id       IN SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                          I_location       IN STORE.STORE%TYPE)
   RETURN BOOLEAN IS
   L_exists   VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'SA_TOTAL_SQL.TOTAL_LOC_EXISTS';

   cursor C_EXISTS is
   select 'Y'
          from sa_total_loc_trait sa,
               loc_traits_matrix loc
         where sa.loc_trait       = loc.loc_trait
           and upper(sa.total_id) = upper(I_total_id)
           and loc.store          = I_location
           and sa.total_rev_no    = (select MAX(total_rev_no)
                                       from sa_total_loc_trait
                                      where upper(total_id) = upper(I_total_id));

BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','SA_LOC_TRAIT,LOC_TRAITS_MATRIX',
      'Total ID: '||I_total_id);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','SA_LOC_TRAIT,LOC_TRAITS_MATRIX',
      'Total ID: '||I_total_id);
   fetch C_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','SA_LOC_TRAIT,LOC_TRAITS_MATRIX',
      'Total ID: '||I_total_id);
   close C_EXISTS;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END TOTAL_LOC_EXISTS;
----------------------------------------------------------------------------------------
FUNCTION TOTAL_USAGE_EXISTS(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_total_id       IN SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                            I_usage_type     IN SA_TOTAL_USAGE.USAGE_TYPE%TYPE)
   RETURN BOOLEAN IS
   
   L_exists   VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'SA_TOTAL_SQL.TOTAL_USAGE_EXISTS';

   cursor C_EXISTS is
      select 'Y'
        from sa_total_usage
       where upper(total_id) = upper(I_total_id)
         and usage_type      = I_usage_type
         and total_rev_no    = (select MAX(total_rev_no)
                                  from sa_total_usage
                                 where upper(total_id) = upper(I_total_id));

BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   if I_usage_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','SA_TOTAL_USAGE',
      'Total ID: '||I_total_id);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','SA_TOTAL_USAGE',
      'Total ID: '||I_total_id);
   fetch C_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','SA_TOTAL_USAGE',
      'Total ID: '||I_total_id);
   close C_EXISTS;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END TOTAL_USAGE_EXISTS;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_UPDATE_ID (O_error_message IN OUT   VARCHAR2,
                             I_update_id     IN       SA_TOTAL_HEAD.UPDATE_ID%TYPE,
                             I_total_id      IN       SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN IS

   L_program_name   VARCHAR2(60) := 'SA_TOTAL_SQL.VALIDATE_UPDATE_ID';
   L_dummy          VARCHAR2(1)  := NULL;

  cursor C_VAL is
     select 'X'
       from sa_total_head
      where update_id = I_update_id
        and total_id  = nvl(I_total_id, total_id);
   
BEGIN
   ---
   if I_update_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_VAL', 'sa_total_head', I_update_id);
   open C_VAL;

   SQL_LIB.SET_MARK('FETCH', 'C_VAL', 'sa_total_head', I_update_id);
   fetch C_VAL into L_dummy;

   SQL_LIB.SET_MARK('CLOSE', 'C_VAL', 'sa_total_head', I_update_id);
   close C_VAL;
   ---
   if L_dummy is NULL then
      ---
      if I_total_id is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('INVALID_UPDATE_ID',I_update_id , NULL, NULL);
      else
         O_error_message:= SQL_LIB.CREATE_MSG('INVALID_UPDATE_ID_RULE',I_update_id , I_total_id, NULL);
      end if;
      ---
      RETURN FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_TOTAL_SQL.VALIDATE_UPDATE_ID',
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_UPDATE_ID;
----------------------------------------------------------------------------------------
FUNCTION GET_PRECEDENCE (O_error_message IN OUT   VARCHAR2,
                         I_total_seq_no  IN       SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         O_precedence    IN OUT   V_SA_GET_TOTAL.PRECEDENCE%TYPE,
                         O_value_rev_no  IN OUT   V_SA_GET_TOTAL.VALUE_REV_NO%TYPE,
                         I_store         IN       SA_TOTAL.STORE%TYPE DEFAULT NULL,
                         I_day           IN       SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'SA_TOTAL_SQL.GET_PRECEDENCE';
   dummy        NUMBER(1)           := NULL;
   L_store      SA_TOTAL.STORE%TYPE := I_store;
   L_day        SA_TOTAL.DAY%TYPE   := I_day;

   cursor C_GET_PREC_1 is
      select max(value_rev_no),
             1
        from sa_hq_value
       where total_seq_no = I_total_seq_no
         and store = L_store
         and day = L_day;

   cursor C_GET_PREC_2 is
      select max(value_rev_no),
             2
        from sa_store_value
       where total_seq_no = I_total_seq_no
         and store = L_store
         and day = L_day;

   cursor C_GET_PREC_3 is
      select max(value_rev_no),
             3
        from sa_sys_value
       where total_seq_no = I_total_seq_no
         and store = L_store
         and day = L_day;

   cursor C_GET_PREC_4 is
      select max(value_rev_no),
             4
        from sa_pos_value
       where total_seq_no = I_total_seq_no
         and store = L_store
         and day = L_day;

BEGIN
   if I_total_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        I_total_seq_no,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_precedence   := NULL;
   O_value_rev_no := NULL;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PREC_1','SA_HQ_VALUE',NULL);
   open C_GET_PREC_1;
   SQL_LIB.SET_MARK('FETCH','C_GET_PREC_1','SA_HQ_VALUE',NULL);
   fetch C_GET_PREC_1 into O_value_rev_no,
                           O_precedence;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PREC_1','SA_HQ_VALUE',NULL);
   close C_GET_PREC_1;

   if O_value_rev_no is NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_PREC_2','SA_STORE_VALUE',NULL);
      open C_GET_PREC_2;
      SQL_LIB.SET_MARK('FETCH','C_GET_PREC_2','SA_STORE_VALUE',NULL);
      fetch C_GET_PREC_2 into O_value_rev_no,
                              O_precedence;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PREC_2','SA_STORE_VALUE',NULL);
      close C_GET_PREC_2;

      if O_value_rev_no is NULL then
         SQL_LIB.SET_MARK('OPEN','C_GET_PREC_3','SA_SYS_VALUE',NULL);
         open C_GET_PREC_3;
         SQL_LIB.SET_MARK('FETCH','C_GET_PREC_3','SA_SYS_VALUE',NULL);
         fetch C_GET_PREC_3 into O_value_rev_no,
                                 O_precedence;
         SQL_LIB.SET_MARK('CLOSE','C_GET_PREC_3','SA_SYS_VALUE',NULL);
         close C_GET_PREC_3;

         if O_value_rev_no is NULL then
            SQL_LIB.SET_MARK('OPEN','C_GET_PREC_4','SA_POS_VALUE',NULL);
            open C_GET_PREC_4;
            SQL_LIB.SET_MARK('FETCH','C_GET_PREC_4','SA_POS_VALUE',NULL);
            fetch C_GET_PREC_4 into O_value_rev_no,
                                    O_precedence;
            SQL_LIB.SET_MARK('CLOSE','C_GET_PREC_4','SA_POS_VALUE',NULL);
            close C_GET_PREC_4;
         end if; -- C_GET_PREC_3
      end if; -- C_GET_PREC_2
   end if; -- C_GET_PREC_1
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRECEDENCE;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_USAGE_DEPENDENCY(O_error_message    IN OUT VARCHAR2,
                                 I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                 I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                                 I_system_code      IN     SA_EXPORT_LOG.SYSTEM_CODE%TYPE,
                                 I_seq_no           IN     SA_EXPORT_LOG.SEQ_NO%TYPE,
                                 I_store            IN     SA_EXPORT_LOG.STORE%TYPE,
                                 I_day              IN     SA_EXPORT_LOG.DAY%TYPE)
   RETURN BOOLEAN IS
   
   L_program        VARCHAR2(60) := 'SA_TOTAL_SQL.UPDATE_USAGE_DEPENDENCY';
   L_update_req     NUMBER(1)                := NULL;
   L_store          SA_STORE_DAY.STORE%TYPE  := I_store;
   L_day            SA_STORE_DAY.DAY%TYPE    := I_day;
   
   cursor C_UPDATE_REQ is
      select 1
        from sa_total_usage
       where total_id = I_total_id
         and usage_type LIKE I_system_code || '%';

   cursor C_GET_DEPENDENT_TOTALS is
      select distinct total_id
        from sa_comb_total
       where detail_total_id = I_total_id;

BEGIN
   if I_store_day_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        I_store_day_seq_no,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_UPDATE_REQ','SA_TOTAL_USAGE','TOTAL_ID: ' || I_total_id);
   OPEN C_UPDATE_REQ;
   SQL_LIB.SET_MARK('FETCH','C_UPDATE_REQ','SA_TOTAL_USAGE','TOTAL_ID: ' || I_total_id);
   FETCH C_UPDATE_REQ INTO L_update_req;
   SQL_LIB.SET_MARK('CLOSE','C_UPDATE_REQ','SA_TOTAL_USAGE','TOTAL_ID: ' || I_total_id);
   CLOSE C_UPDATE_REQ;

   -- if we need to "update" the export status, then we are done.
   if (L_update_req = 1) THEN
      insert into sa_export_log(store,
                                day,
                                store_day_seq_no,
                                system_code,
                                seq_no,
                                status,
                                datetime)
                        values (L_store,
                                L_day,
                                I_store_day_seq_no,
                                I_system_code,
                                I_seq_no,
                                'R',
                                SYSDATE);
      return TRUE;
   end if;

   for C_rec in C_GET_DEPENDENT_TOTALS loop
      if (UPDATE_USAGE_DEPENDENCY(O_error_message,
                                  C_rec.total_id,
                                  I_store_day_seq_no,
                                  I_system_code,
                                  I_seq_no,
                                  L_store,
                                  L_day) = FALSE) then
         return FALSE;
      end if;
   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_USAGE_DEPENDENCY;
---------------------------------------------------------------------------------------------
END SA_TOTAL_SQL;
/
