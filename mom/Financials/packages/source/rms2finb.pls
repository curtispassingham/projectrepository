CREATE OR REPLACE PACKAGE BODY RMS2FIN AS
---------------------------------------------------------------------
FUNCTION GET_SEGMENTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_ccid             IN      FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE,
                      I_set_of_books_id  IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                      O_segment1         IN OUT  FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                      O_segment2         IN OUT  FIF_GL_ACCT.ATTRIBUTE2%TYPE,
                      O_segment3         IN OUT  FIF_GL_ACCT.ATTRIBUTE3%TYPE,
                      O_segment4         IN OUT  FIF_GL_ACCT.ATTRIBUTE4%TYPE,
                      O_segment5         IN OUT  FIF_GL_ACCT.ATTRIBUTE5%TYPE,
                      O_segment6         IN OUT  FIF_GL_ACCT.ATTRIBUTE6%TYPE,
                      O_segment7         IN OUT  FIF_GL_ACCT.ATTRIBUTE7%TYPE,
                      O_segment8         IN OUT  FIF_GL_ACCT.ATTRIBUTE8%TYPE,
                      O_segment9         IN OUT  FIF_GL_ACCT.ATTRIBUTE9%TYPE,
                      O_segment10        IN OUT  FIF_GL_ACCT.ATTRIBUTE10%TYPE,
                      O_segment1_label   IN OUT  FIF_GL_ACCT.DESCRIPTION1%TYPE,
                      O_segment2_label   IN OUT  FIF_GL_ACCT.DESCRIPTION2%TYPE,
                      O_segment3_label   IN OUT  FIF_GL_ACCT.DESCRIPTION3%TYPE,
                      O_segment4_label   IN OUT  FIF_GL_ACCT.DESCRIPTION4%TYPE,
                      O_segment5_label   IN OUT  FIF_GL_ACCT.DESCRIPTION5%TYPE,
                      O_segment6_label   IN OUT  FIF_GL_ACCT.DESCRIPTION6%TYPE,
                      O_segment7_label   IN OUT  FIF_GL_ACCT.DESCRIPTION7%TYPE,
                      O_segment8_label   IN OUT  FIF_GL_ACCT.DESCRIPTION8%TYPE,
                      O_segment9_label   IN OUT  FIF_GL_ACCT.DESCRIPTION9%TYPE,
                      O_segment10_label  IN OUT  FIF_GL_ACCT.DESCRIPTION10%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                       :=  'RMS2FIN.GET_SEGMENTS';
   L_fin_ap   SYSTEM_OPTIONS.FINANCIAL_AP%TYPE;
   ---
   cursor C_GET_SEGMENTS is
      select attribute1,
             attribute2,
             attribute3,
             attribute4,
             attribute5,
             attribute6,
             attribute7,
             attribute8,
             attribute9,
             attribute10,
             description1,
             description2,
             description3,
             description4,
             description5,
             description6,
             description7,
             description8,
             description9,
             description10
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and ((primary_account = I_ccid and L_fin_ap = 'O')
          or (L_fin_ap = 'P' and
              NVL(O_segment1, '!#$%^') = NVL(attribute1, '!#$%^') and
              NVL(O_segment2, '!#$%^') = NVL(attribute2, '!#$%^') and
              NVL(O_segment3, '!#$%^') = NVL(attribute3, '!#$%^') and
              NVL(O_segment4, '!#$%^') = NVL(attribute4, '!#$%^') and
              NVL(O_segment5, '!#$%^') = NVL(attribute5, '!#$%^') and
              NVL(O_segment6, '!#$%^') = NVL(attribute6, '!#$%^') and
              NVL(O_segment7, '!#$%^') = NVL(attribute7, '!#$%^') and
              NVL(O_segment8, '!#$%^') = NVL(attribute8, '!#$%^') and
              NVL(O_segment9, '!#$%^') = NVL(attribute9, '!#$%^') and
              NVL(O_segment10, '!#$%^') = NVL(attribute10, '!#$%^')));

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_FINANCIAL_AP(O_error_message,
                                       L_fin_ap) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SEGMENTS', 'FIF_GL_ACCT', NULL);
   open C_GET_SEGMENTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SEGMENTS', 'FIF_GL_ACCT', NULL);
   fetch C_GET_SEGMENTS into O_segment1,
                             O_segment2,
                             O_segment3,
                             O_segment4,
                             O_segment5,
                             O_segment6,
                             O_segment7,
                             O_segment8,
                             O_segment9,
                             O_segment10,
                             O_segment1_label,
                             O_segment2_label,
                             O_segment3_label,
                             O_segment4_label,
                             O_segment5_label,
                             O_segment6_label,
                             O_segment7_label,
                             O_segment8_label,
                             O_segment9_label,
                             O_segment10_label;
   ---
   if C_GET_SEGMENTS%NOTFOUND THEN
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_SEGMENTS', 'FIF_GL_ACCT', NULL);
      close C_GET_SEGMENTS;
      O_error_message := SQL_LIB.CREATE_MSG('INV_CCID', I_ccid, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SEGMENTS', 'FIF_GL_ACCT', NULL);
   close C_GET_SEGMENTS;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_SEGMENTS;
---------------------------------------------------------------------------------------
FUNCTION GET_CCID(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_ccid            IN OUT  FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE,
                  I_set_of_books_id IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                  I_sequence1       IN      FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                  I_sequence2       IN      FIF_GL_ACCT.ATTRIBUTE2%TYPE,
                  I_sequence3       IN      FIF_GL_ACCT.ATTRIBUTE3%TYPE,
                  I_sequence4       IN      FIF_GL_ACCT.ATTRIBUTE4%TYPE,
                  I_sequence5       IN      FIF_GL_ACCT.ATTRIBUTE5%TYPE,
                  I_sequence6       IN      FIF_GL_ACCT.ATTRIBUTE6%TYPE,
                  I_sequence7       IN      FIF_GL_ACCT.ATTRIBUTE7%TYPE,
                  I_sequence8       IN      FIF_GL_ACCT.ATTRIBUTE8%TYPE,
                  I_sequence9       IN      FIF_GL_ACCT.ATTRIBUTE9%TYPE,
                  I_sequence10      IN      FIF_GL_ACCT.ATTRIBUTE10%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)  :=  'RMS2FIN.GET_CCID';

   /* The use of '!#$%^' in this cursor is due to a quirk with alphabetic characters being
      compared instead of numbers.  Please leave!  */
   cursor C_CCID is
      select primary_account
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and NVL(attribute1,'!#$%^')  = NVL(I_sequence1,'!#$%^')
         and NVL(attribute2,'!#$%^')  = NVL(I_sequence2,'!#$%^')
         and NVL(attribute3,'!#$%^')  = NVL(I_sequence3,'!#$%^')
         and NVL(attribute4,'!#$%^')  = NVL(I_sequence4,'!#$%^')
         and NVL(attribute5,'!#$%^')  = NVL(I_sequence5,'!#$%^')
         and NVL(attribute6,'!#$%^')  = NVL(I_sequence6,'!#$%^')
         and NVL(attribute7,'!#$%^')  = NVL(I_sequence7,'!#$%^')
         and NVL(attribute8,'!#$%^')  = NVL(I_sequence8,'!#$%^')
         and NVL(attribute9,'!#$%^')  = NVL(I_sequence9,'!#$%^')
         and NVL(attribute10,'!#$%^') = NVL(I_sequence10,'!#$%^');

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_CCID', 'FIF_GL_ACCT', NULL);
   open C_CCID;
   SQL_LIB.SET_MARK('FETCH', 'C_CCID', 'FIF_GL_ACCT', NULL);
   fetch C_CCID into O_ccid;
   ---
   if C_CCID%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CCID', 'FIF_GL_ACCT', NULL);
      close C_CCID;
      O_error_message := SQL_LIB.CREATE_MSG('NO_CCID', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_CCID', 'FIF_GL_ACCT', NULL);
   close C_CCID;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_CCID;
---------------------------------------------------------------------------------
FUNCTION SA_FIF_VALIDATION( O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT  BOOLEAN,
                            I_set_of_books_id   IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                            I_store             IN      SA_FIF_GL_CROSS_REF.STORE%TYPE,
                            I_total_id          IN      SA_FIF_GL_CROSS_REF.TOTAL_ID%TYPE,
                            I_roll1             IN      SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_1%TYPE,
                            I_roll2             IN      SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_2%TYPE,
                            I_roll3             IN      SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_3%TYPE)
RETURN BOOLEAN IS

L_program              VARCHAR2(64)  :=  'RMS2FIN.SA_FIF_VALIDATION';
L_exists               VARCHAR2(1)   :=  'N';
RECORD_FOUND           EXCEPTION;

   cursor C_ROLLS is
      select 'Y'
        from SA_FIF_GL_cross_ref
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and (rollup_level_1 in (I_roll1, '-1')
             or I_roll1 = '-1')
         and (rollup_level_2 in (I_roll2, '-1')
             or I_roll2 = '-1')
         and (rollup_level_3 in (I_roll3, '-1')
             or I_roll3 = '-1');

    -- BEGIN INTERNAL FUNCTIONS --
    FUNCTION GET_TOTAL_ID(O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS

       cursor C_TOTAL_ID_STORE is
         select 'Y'
           from SA_FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (rollup_level_1 in (I_roll1, '-1')
                or I_roll1 = '-1')
            and (rollup_level_2 in (I_roll2, '-1')
                or I_roll2 = '-1')
            and (rollup_level_3 in (I_roll3, '-1')
                or I_roll3 = '-1')
            and upper(total_id) in (upper(I_total_id), '-1')
            and store in (I_store, -1);

       cursor C_TOTAL_ID is
         select 'Y'
           from SA_FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (rollup_level_1 in (I_roll1, '-1')
                or I_roll1 = '-1')
            and (rollup_level_2 in (I_roll2, '-1')
                or I_roll2 = '-1')
            and (rollup_level_3 in (I_roll3, '-1')
                or I_roll3 = '-1')
            and upper(total_id) in (upper(I_total_id), '-1');

    BEGIN
         if I_total_id != '-1' then
            if I_store != -1 then
               SQL_LIB.SET_MARK('OPEN','C_TOTAL_ID_STORE','SA_FIF_GL_CROSS_REF', NULL);
               open C_TOTAL_ID_STORE;
               SQL_LIB.SET_MARK('FETCH','C_TOTAL_ID_STORE','SA_FIF_GL_CROSS_REF', NULL);
               fetch C_TOTAL_ID_STORE into L_exists;
               SQL_LIB.SET_MARK('CLOSE','C_TOTAL_ID_STORE','SA_FIF_GL_CROSS_REF', NULL);
               close C_TOTAL_ID_STORE;
            else
               SQL_LIB.SET_MARK('OPEN','C_TOTAL_ID','SA_FIF_GL_CROSS_REF', NULL);
               open C_TOTAL_ID;
               SQL_LIB.SET_MARK('FETCH','C_TOTAL_ID','SA_FIF_GL_CROSS_REF', NULL);
               fetch C_TOTAL_ID into L_exists;
               SQL_LIB.SET_MARK('CLOSE','C_TOTAL_ID','SA_FIF_GL_CROSS_REF', NULL);
               close C_TOTAL_ID;
            end if;
         else
            O_exists := TRUE;
         end if;
         ---
         if L_exists = 'Y' then
            L_exists := 'N';
            O_exists := TRUE;
         end if;
         ---
         return TRUE;

         EXCEPTION
            when OTHERS then
               O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,L_program,NULL);
               return FALSE;
    END GET_TOTAL_ID;

    FUNCTION GET_STORE(O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS

       cursor C_STORE is
         select 'Y'
           from SA_FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (rollup_level_1 in (I_roll1, '-1')
                or I_roll1 = '-1')
            and (rollup_level_2 in (I_roll2, '-1')
                or I_roll2 = '-1')
            and (rollup_level_3 in (I_roll3, '-1')
                or I_roll3 = '-1')
            and store in (I_store, -1);
    BEGIN
         if I_store = -1 then
            if not GET_TOTAL_ID(O_error_message) then
               return FALSE;
            end if;
         else
            SQL_LIB.SET_MARK('OPEN','C_STORE','SA_FIF_GL_CROSS_REF', NULL);
            open C_STORE;
            SQL_LIB.SET_MARK('FETCH','C_STORE','SA_FIF_GL_CROSS_REF', NULL);
            fetch C_STORE into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_STORE','SA_FIF_GL_CROSS_REF', NULL);
            close C_STORE;
         end if;
         ---
         if L_exists = 'Y' then
            L_exists := 'N';
            if not GET_TOTAL_ID(O_error_message) then
               return FALSE;
            end if;
         end if;
         ---
         return TRUE;

         EXCEPTION
            when OTHERS then
               O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,L_program,NULL);
               return FALSE;
    END GET_STORE;
    -- END INTERNAL FUNCTIONS --

BEGIN
   O_exists := FALSE;
   ---
   /* 1)  check invalid inputs */
   if (I_roll1 = '-1' and (I_roll2 != '-1' or I_roll3 != '-1'))
      or (I_roll2 = '-1' and I_roll3 != '-1')
      or (I_roll1 is NULL or I_roll2 is NULL or I_roll3 is NULL)
      or I_store is NULL
      or I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC107',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ROLLS','SA_FIF_GL_CROSS_REF', NULL);
   open C_ROLLS;
   SQL_LIB.SET_MARK('FETCH','C_ROLLS','SA_FIF_GL_CROSS_REF', NULL);
   fetch C_ROLLS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_ROLLS','SA_FIF_GL_CROSS_REF', NULL);
   close C_ROLLS;
   ---
   if L_exists = 'Y' then
      L_exists := 'N';
      if not GET_STORE(O_error_message) then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END SA_FIF_VALIDATION;
---------------------------------------------------------------------------------------
FUNCTION FIF_VALIDATION(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists            IN OUT   BOOLEAN,
                        I_set_of_books_id   IN       FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                        I_dept              IN       FIF_GL_CROSS_REF.DEPT%TYPE,
                        I_class             IN       FIF_GL_CROSS_REF.CLASS%TYPE,
                        I_subclass          IN       FIF_GL_CROSS_REF.SUBCLASS%TYPE,
                        I_location          IN       FIF_GL_CROSS_REF.LOCATION%TYPE,
                        I_tran_code         IN       FIF_GL_CROSS_REF.TRAN_CODE%TYPE,
                        I_tran_ref_no       IN       FIF_GL_CROSS_REF.TRAN_REF_NO%TYPE,
                        I_cost_retail_flag  IN       FIF_GL_CROSS_REF.COST_RETAIL_FLAG%TYPE,
                        I_line_type         IN       FIF_GL_CROSS_REF.LINE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)  :=  'RMS2FIN.FIF_VALIDATION';
   L_exists               VARCHAR2(1)   :=  'N';
   RECORD_FOUND           EXCEPTION;

   cursor C_DEPT_CLASS_SUBCLASS is
      select 'Y'
        from FIF_GL_cross_ref
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and (dept in (I_dept, -1)
             or I_dept = -1)
         and (class in (I_class, -1)
             or I_class = -1)
         and (subclass in (I_subclass, -1)
             or I_subclass = -1);

   -- BEGIN INTERNAL FUNCTIONS --
   FUNCTION GET_LINE_TYPE(O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS

      cursor C_LINE_TYPE is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and cost_retail_flag = I_cost_retail_flag
            and line_type = I_line_type;

      cursor C_LINE_TYPE_LOC is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and location in (I_location, -1)
            and cost_retail_flag = I_cost_retail_flag
            and line_type = I_line_type;

      /* If a record exists on the table where tran_code = -1,  */
      /* no other tran_codes are allowed and tran_ref_no will   */
      /* be null.                                               */
      /* Tran_codes 22 and 23 allow null as a possible          */
      /* tran_ref_no so this possibility is checked.            */
      /* Tran_codes 37, 38, 63, 64, 87 and 88 allow tran_ref_no */
      /* to be -1, so this possibility also is checked.         */
      cursor C_LINE_TYPE_TRANCODE_REF is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and (tran_code = -1
                 or (tran_code = I_tran_code
                     and (tran_ref_no in (I_tran_ref_no, '-1')
                          or I_tran_ref_no = '-1'
                          or (tran_ref_no is NULL
                              and I_tran_ref_no is NULL))))
            and cost_retail_flag = I_cost_retail_flag
            and line_type = I_line_type;

      cursor C_LINE_TYPE_LOC_TRANCODE_REF is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and (tran_code = -1
                 or (tran_code = I_tran_code
                     and (tran_ref_no in (I_tran_ref_no, '-1')
                          or I_tran_ref_no = '-1'
                          or (tran_ref_no is NULL
                              and I_tran_ref_no is NULL))))
            and location in (I_location, -1)
            and cost_retail_flag = I_cost_retail_flag
            and line_type = I_line_type;

      cursor C_LINE_TYPE_TRANCODE is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and tran_code in (I_tran_code, -1)
            and cost_retail_flag = I_cost_retail_flag
            and line_type = I_line_type;

      cursor C_LINE_TYPE_LOC_TRANCODE is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and tran_code in (I_tran_code, -1)
            and location in (I_location, -1)
            and cost_retail_flag = I_cost_retail_flag
            and line_type = I_line_type;

   BEGIN
      if I_location = -1 and I_tran_code = -1 then
         SQL_LIB.SET_MARK('OPEN','C_LINE_TYPE','FIF_GL_CROSS_REF', NULL);
         open C_LINE_TYPE;
         SQL_LIB.SET_MARK('FETCH','C_LINE_TYPE','FIF_GL_CROSS_REF', NULL);
         fetch C_LINE_TYPE into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_LINE_TYPE','FIF_GL_CROSS_REF', NULL);
         close C_LINE_TYPE;
      elsif I_location != -1 and I_tran_code = -1 then
         SQL_LIB.SET_MARK('OPEN','C_LINE_TYPE_LOC','FIF_GL_CROSS_REF', NULL);
         open C_LINE_TYPE_LOC;
         SQL_LIB.SET_MARK('FETCH','C_LINE_TYPE_LOC','FIF_GL_CROSS_REF', NULL);
         fetch C_LINE_TYPE_LOC into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_LINE_TYPE_LOC','FIF_GL_CROSS_REF', NULL);
         close C_LINE_TYPE_LOC;
      elsif I_location = -1 and I_tran_code in (22,23,37,38,63,64,87,88) then
         SQL_LIB.SET_MARK('OPEN','C_LINE_TYPE_TRANCODE_REF','FIF_GL_CROSS_REF', NULL);
         open C_LINE_TYPE_TRANCODE_REF;
         SQL_LIB.SET_MARK('FETCH','C_LINE_TYPE_TRANCODE_REF','FIF_GL_CROSS_REF', NULL);
         fetch C_LINE_TYPE_TRANCODE_REF into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_LINE_TYPE_TRANCODE_REF','FIF_GL_CROSS_REF', NULL);
         close C_LINE_TYPE_TRANCODE_REF;
      elsif I_location != -1 and I_tran_code in (22,23,37,38,63,64,87,88) then
         SQL_LIB.SET_MARK('OPEN','C_LINE_TYPE_LOC_TRANCODE_REF','FIF_GL_CROSS_REF', NULL);
         open C_LINE_TYPE_LOC_TRANCODE_REF;
         SQL_LIB.SET_MARK('FETCH','C_LINE_TYPE_LOC_TRANCODE_REF','FIF_GL_CROSS_REF', NULL);
         fetch C_LINE_TYPE_LOC_TRANCODE_REF into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_LINE_TYPE_LOC_TRANCODE_REF','FIF_GL_CROSS_REF', NULL);
         close C_LINE_TYPE_LOC_TRANCODE_REF;
      elsif I_location = -1 and I_tran_code != -1 then
         SQL_LIB.SET_MARK('OPEN','C_LINE_TYPE_TRANCODE','FIF_GL_CROSS_REF', NULL);
         open C_LINE_TYPE_TRANCODE;
         SQL_LIB.SET_MARK('FETCH','C_LINE_TYPE_TRANCODE','FIF_GL_CROSS_REF', NULL);
         fetch C_LINE_TYPE_TRANCODE into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_LINE_TYPE_TRANCODE','FIF_GL_CROSS_REF', NULL);
         close C_LINE_TYPE_TRANCODE;
      elsif I_location != -1 and I_tran_code != -1 then
         SQL_LIB.SET_MARK('OPEN','C_LINE_TYPE_LOC_TRANCODE','FIF_GL_CROSS_REF', NULL);
         open C_LINE_TYPE_LOC_TRANCODE;
         SQL_LIB.SET_MARK('FETCH','C_LINE_TYPE_LOC_TRANCODE','FIF_GL_CROSS_REF', NULL);
         fetch C_LINE_TYPE_LOC_TRANCODE into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_LINE_TYPE_LOC_TRANCODE','FIF_GL_CROSS_REF', NULL);
         close C_LINE_TYPE_LOC_TRANCODE;
      end if;

      if L_exists = 'Y' then
         L_exists := 'N';
         O_exists := TRUE;
      end if;

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
         return FALSE;
   END GET_LINE_TYPE;

   FUNCTION GET_TRAN_REF_NO(O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS

      /* If a record exists on the table where tran_code = -1,  */
      /* no other tran_codes are allowed and tran_ref_no will   */
      /* be null.                                               */
      /* Tran_codes 22 and 23 allow null as a possible          */
      /* tran_ref_no so this possibility is checked.            */
      /* Tran_codes 37, 38, 63, 64, 87 and 88 allow tran_ref_no */
      /* to be -1, so this possibility also is checked.         */
      cursor C_TRAN_REF_NO is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and (  (tran_code = -1)
                 or  (tran_code = I_tran_code
                 and (tran_ref_no in (I_tran_ref_no, '-1')
                      or I_tran_ref_no = '-1'
                      or (tran_ref_no is NULL
                          and I_tran_ref_no is NULL))));

      cursor C_TRAN_REF_NO_LOC is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and (  (tran_code = -1)
                 or  (tran_code = I_tran_code
                 and (tran_ref_no in (I_tran_ref_no, '-1')
                      or I_tran_ref_no = '-1'
                      or (tran_ref_no is NULL
                          and I_tran_ref_no is NULL))))
            and location in (I_location, -1);

   BEGIN
      if I_tran_code in (22,23,37,38,63,64,87,88) then
         if I_location = -1 then
            SQL_LIB.SET_MARK('OPEN','C_TRAN_REF_NO','FIF_GL_CROSS_REF', NULL);
            open C_TRAN_REF_NO;
            SQL_LIB.SET_MARK('FETCH','C_TRAN_REF_NO','FIF_GL_CROSS_REF', NULL);
            fetch C_TRAN_REF_NO into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_TRAN_REF_NO','FIF_GL_CROSS_REF', NULL);
            close C_TRAN_REF_NO;
         else
            SQL_LIB.SET_MARK('OPEN','C_TRAN_REF_NO_LOC','FIF_GL_CROSS_REF', NULL);
            open C_TRAN_REF_NO_LOC;
            SQL_LIB.SET_MARK('FETCH','C_TRAN_REF_NO_LOC','FIF_GL_CROSS_REF', NULL);
            fetch C_TRAN_REF_NO_LOC into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_TRAN_REF_NO_LOC','FIF_GL_CROSS_REF', NULL);
            close C_TRAN_REF_NO_LOC;
         end if;
      else
         if not GET_LINE_TYPE(O_error_message) then
            return FALSE;
         end if;
      end if;

      if L_exists = 'Y' then
         L_exists := 'N';
         if not GET_LINE_TYPE(O_error_message) then
            return FALSE;
         end if;
      end if;

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
         return FALSE;
   END GET_TRAN_REF_NO;

   FUNCTION GET_TRAN_CODE(O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS

      cursor C_TRAN_CODE is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and tran_code in (I_tran_code, -1);

      cursor C_TRAN_CODE_LOC is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and tran_code in (I_tran_code, -1)
            and location in (I_location, -1);

   BEGIN
      if I_tran_code = -1 then
         if not GET_LINE_TYPE(O_error_message) then
            return FALSE;
         end if;
      else
         if I_location = -1 then
            SQL_LIB.SET_MARK('OPEN','C_TRAN_CODE','FIF_GL_CROSS_REF', NULL);
            open C_TRAN_CODE;
            SQL_LIB.SET_MARK('FETCH','C_TRAN_CODE','FIF_GL_CROSS_REF', NULL);
            fetch C_TRAN_CODE into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_TRAN_CODE','FIF_GL_CROSS_REF', NULL);
            close C_TRAN_CODE;
         else
            SQL_LIB.SET_MARK('OPEN','C_TRAN_CODE_LOC','FIF_GL_CROSS_REF', NULL);
            open C_TRAN_CODE_LOC;
            SQL_LIB.SET_MARK('FETCH','C_TRAN_CODE_LOC','FIF_GL_CROSS_REF', NULL);
            fetch C_TRAN_CODE_LOC into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_TRAN_CODE_LOC','FIF_GL_CROSS_REF', NULL);
            close C_TRAN_CODE_LOC;
         end if;
      end if;

      if L_exists = 'Y' then
         L_exists := 'N';
         if not GET_TRAN_REF_NO(O_error_message) then
            return FALSE;
         end if;
      end if;

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
         return FALSE;
   END GET_TRAN_CODE;

   FUNCTION GET_LOCATION(O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS

      cursor C_LOCATION is
         select 'Y'
           from FIF_GL_cross_ref
          where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
            and (dept in (I_dept, -1)
                 or I_dept = -1)
            and (class in (I_class, -1)
                 or I_class = -1)
            and (subclass in (I_subclass, -1)
                 or I_subclass = -1)
            and location in (I_location, -1);
   BEGIN
      if I_location = -1 then
         if not GET_TRAN_CODE(O_error_message) then
            return FALSE;
         end if;
      else
         SQL_LIB.SET_MARK('OPEN','C_LOCATION','FIF_GL_CROSS_REF', NULL);
         open C_LOCATION;
         SQL_LIB.SET_MARK('FETCH','C_LOCATION','FIF_GL_CROSS_REF', NULL);
         fetch C_LOCATION into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_LOCATION','FIF_GL_CROSS_REF', NULL);
         close C_LOCATION;
      end if;

      if L_exists = 'Y' then
         L_exists := 'N';
         if not GET_TRAN_CODE(O_error_message) then
            return FALSE;
         end if;
      end if;

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               NULL);
        return FALSE;
   END GET_LOCATION;
   -- END INTERNAL FUNCTIONS --

BEGIN
   O_exists := FALSE;
   ---
   /* 1)  check invalid inputs */
   if (I_dept = -1 and (I_class != -1 or I_subclass != -1))
      or (I_class = -1 and I_subclass != -1)
      or (I_dept is NULL or I_class is NULL or I_subclass is NULL)
      or I_location is NULL
      or I_tran_code is NULL
      or I_cost_retail_flag not in ('C','R')
      or I_line_type is NULL then
      O_error_message := sql_lib.create_msg('INV_INPUT_GENERIC107',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_DEPT_CLASS_SUBCLASS','FIF_GL_CROSS_REF', NULL);
   open C_DEPT_CLASS_SUBCLASS;
   SQL_LIB.SET_MARK('FETCH','C_DEPT_CLASS_SUBCLASS','FIF_GL_CROSS_REF', NULL);
   fetch C_DEPT_CLASS_SUBCLASS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_DEPT_CLASS_SUBCLASS','FIF_GL_CROSS_REF', NULL);
   close C_DEPT_CLASS_SUBCLASS;

   if L_exists = 'Y' then
      L_exists := 'N';
      if not GET_LOCATION(O_error_message) then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END FIF_VALIDATION;
---------------------------------------------------------------------------------------
FUNCTION GET_LINE_TYPE_DESC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_desc           IN OUT   FIF_LINE_TYPE_XREF.RMS_LINE_TYPE_DESC%TYPE,
                            I_line_type      IN       FIF_LINE_TYPE_XREF.RMS_LINE_TYPE %TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(64)  :=  'RMS2FIN.GET_LINE_TYPE_DESC';

   cursor C_LINE_TYPE is
      select rms_line_type_desc
        from fif_line_type_xref
       where rms_line_type = I_line_type;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LINE_TYPE','FIF_LINE_TYPE_XREF', NULL);
   open C_LINE_TYPE;
   SQL_LIB.SET_MARK('FETCH',' C_LINE_TYPE ',' FIF_LINE_TYPE_XREF', NULL);
   fetch C_LINE_TYPE into O_desc;
   ---
   if C_LINE_TYPE %notfound then
      close C_LINE_TYPE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_LINE_TYPE',NULL,
                                            NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',' C_LINE_TYPE',' FIF_LINE_TYPE_XREF', NULL);
   close C_LINE_TYPE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_LINE_TYPE_DESC;
---------------------------------------------------------------------------------------------------
FUNCTION GET_SEG_DESC(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_desc             IN OUT  FIF_GL_ACCT.DESCRIPTION1%TYPE,
                      I_set_of_books_id  IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                      I_segment          IN      FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                      I_segment_id       IN      VARCHAR2 )
   RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                       :=  'RMS2FIN.GET_SEG_DESC';
   ---
   cursor C_GET_SEG_DESC is
      select description1
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute1 = I_segment
         and I_segment_id = '1'
   union all
      select description2
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute2 = I_segment
         and I_segment_id = '2'
   union all
      select description3
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute3 = I_segment
         and I_segment_id = '3'
   union all
      select description4
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute4 = I_segment
         and I_segment_id = '4'
   union all
      select description5
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute5 = I_segment
         and I_segment_id = '5'
   union all
      select description6
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute6 = I_segment
         and I_segment_id = '6'
   union all
      select description7
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute7 = I_segment
         and I_segment_id = '7'
   union all
      select description8
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute8 = I_segment
         and I_segment_id = '8'
   union all
      select description9
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute9 = I_segment
         and I_segment_id = '9'
   union all
      select description10
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and attribute10 = I_segment
         and I_segment_id = '10';


BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SEG_DESC', 'FIF_GL_ACCT', NULL);
   open C_GET_SEG_DESC;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SEG_DESC', 'FIF_GL_ACCT', NULL);
   fetch C_GET_SEG_DESC into O_desc;
   if C_GET_SEG_DESC%NOTFOUND THEN
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_SEG_DESC', 'FIF_GL_ACCT', NULL);
      close C_GET_SEG_DESC;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SEG', NULL , NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SEG_DESC', 'FIF_GL_ACCT', NULL);
   close C_GET_SEG_DESC;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_SEG_DESC;
--------------------------------------------------------------------------------------------
FUNCTION GET_ACCT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_prim_acct        IN OUT  FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE,
                  I_set_of_books_id  IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                  I_segment1         IN      FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                  I_segment2         IN      FIF_GL_ACCT.ATTRIBUTE2%TYPE,
                  I_segment3         IN      FIF_GL_ACCT.ATTRIBUTE3%TYPE,
                  I_segment4         IN      FIF_GL_ACCT.ATTRIBUTE4%TYPE,
                  I_segment5         IN      FIF_GL_ACCT.ATTRIBUTE5%TYPE,
                  I_segment6         IN      FIF_GL_ACCT.ATTRIBUTE6%TYPE,
                  I_segment7         IN      FIF_GL_ACCT.ATTRIBUTE7%TYPE,
                  I_segment8         IN      FIF_GL_ACCT.ATTRIBUTE8%TYPE,
                  I_segment9         IN      FIF_GL_ACCT.ATTRIBUTE9%TYPE,
                  I_segment10        IN      FIF_GL_ACCT.ATTRIBUTE10%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                       :=  'RMS2FIN.GET_ACCT';
   ---
   cursor C_GET_ACCT is
      select primary_account
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and (attribute1 = I_segment1 or (attribute1 is NULL and I_segment1 is NULL))
         and (attribute2 = I_segment2 or (attribute2 is NULL and I_segment2 is NULL))
         and (attribute3 = I_segment3 or (attribute3 is NULL and I_segment3 is NULL))
         and (attribute4 = I_segment4 or (attribute4 is NULL and I_segment4 is NULL))
         and (attribute5 = I_segment5 or (attribute5 is NULL and I_segment5 is NULL))
         and (attribute6 = I_segment6 or (attribute6 is NULL and I_segment6 is NULL))
         and (attribute7 = I_segment7 or (attribute7 is NULL and I_segment7 is NULL))
         and (attribute8 = I_segment8 or (attribute8 is NULL and I_segment8 is NULL))
         and (attribute9 = I_segment9 or (attribute9 is NULL and I_segment9 is NULL))
         and (attribute10 = I_segment10 or (attribute10 is NULL and I_segment10 is NULL));

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ACCT', 'FIF_GL_ACCT', NULL);
   open C_GET_ACCT;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_ACCT', 'FIF_GL_ACCT', NULL);
   fetch C_GET_ACCT into O_prim_acct;
   if C_GET_ACCT%NOTFOUND THEN
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_ACCT', 'FIF_GL_ACCT', NULL);
      close C_GET_ACCT;
      O_error_message := SQL_LIB.CREATE_MSG('INV_COMB_SEQ', NULL , NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ACCT', 'FIF_GL_ACCT', NULL);
   close C_GET_ACCT;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ACCT;
--------------------------------------------------------------------------------------------
FUNCTION GET_ACCT_DESCS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_desc1            IN OUT  FIF_GL_ACCT.DESCRIPTION1%TYPE,
                        O_desc2            IN OUT  FIF_GL_ACCT.DESCRIPTION2%TYPE,
                        O_desc3            IN OUT  FIF_GL_ACCT.DESCRIPTION3%TYPE,
                        O_desc4            IN OUT  FIF_GL_ACCT.DESCRIPTION4%TYPE,
                        O_desc5            IN OUT  FIF_GL_ACCT.DESCRIPTION5%TYPE,
                        O_desc6            IN OUT  FIF_GL_ACCT.DESCRIPTION6%TYPE,
                        O_desc7            IN OUT  FIF_GL_ACCT.DESCRIPTION7%TYPE,
                        O_desc8            IN OUT  FIF_GL_ACCT.DESCRIPTION8%TYPE,
                        O_desc9            IN OUT  FIF_GL_ACCT.DESCRIPTION9%TYPE,
                        O_desc10           IN OUT  FIF_GL_ACCT.DESCRIPTION10%TYPE,
                        I_set_of_books_id  IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                        I_prim_acct        IN      FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)                       :=  'RMS2FIN.GET_ACCT_DESCS';

   cursor C_GET_ACCT_DESCS is
      select description1,
             description2,
             description3,
             description4,
             description5,
             description6,
             description7,
             description8,
             description9,
             description10
        from fif_gl_acct
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
         and primary_account = I_prim_acct;

BEGIN
   if I_prim_acct is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_prim_acct', 'NULL', 'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ACCT_DESCS', 'FIF_GL_ACCT', NULL);
   open C_GET_ACCT_DESCS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_ACCT_DESCS', 'FIF_GL_ACCT', NULL);
   fetch C_GET_ACCT_DESCS into O_desc1,
                               O_desc2,
                               O_desc3,
                               O_desc4,
                               O_desc5,
                               O_desc6,
                               O_desc7,
                               O_desc8,
                               O_desc9,
                               O_desc10;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ACCT_DESCS', 'FIF_GL_ACCT', NULL);
   close C_GET_ACCT_DESCS;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ACCT_DESCS;
--------------------------------------------------------------------------------
FUNCTION GET_NEXT_ID (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_id_number     IN OUT FIF_GL_CROSS_REF.FIF_GL_CROSS_REF_ID%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  :=  'RMS2FIN.GET_NEXT_ID';
   L_id_sequence            FIF_GL_CROSS_REF.FIF_GL_CROSS_REF_ID%TYPE;
   L_wrap_sequence_number   FIF_GL_CROSS_REF.FIF_GL_CROSS_REF_ID%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);

   CURSOR c_id_exists IS
      SELECT 'x'
        FROM fif_gl_cross_ref
       WHERE fif_gl_cross_ref_id = O_id_number;

BEGIN

   LOOP
      SELECT fif_gl_cross_ref_id_seq.NEXTVAL
        INTO L_id_sequence
        FROM dual;

      IF (L_first_time = 'Yes') THEN
          L_wrap_sequence_number := L_id_sequence;
          L_first_time := 'No';
      ELSIF (L_id_sequence = L_wrap_sequence_number) THEN
          O_error_message := 'Fatal error - no available fif_gl_cross_ref_id numbers';
          return FALSE;
      END IF;

      O_id_number := L_id_sequence;

      OPEN  c_id_exists;
      FETCH c_id_exists into L_dummy;
      CLOSE c_id_exists;

      if L_dummy is NULL then
         exit;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_NEXT_ID;
---------------------------------------------------------------------------------------
FUNCTION GET_SEQUENCE_LABELS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_sequence1_desc  IN OUT  FIF_GL_SETUP.SEQUENCE1_DESC%TYPE,
                             O_sequence2_desc  IN OUT  FIF_GL_SETUP.SEQUENCE2_DESC%TYPE,
                             O_sequence3_desc  IN OUT  FIF_GL_SETUP.SEQUENCE3_DESC%TYPE,
                             O_sequence4_desc  IN OUT  FIF_GL_SETUP.SEQUENCE4_DESC%TYPE,
                             O_sequence5_desc  IN OUT  FIF_GL_SETUP.SEQUENCE5_DESC%TYPE,
                             O_sequence6_desc  IN OUT  FIF_GL_SETUP.SEQUENCE6_DESC%TYPE,
                             O_sequence7_desc  IN OUT  FIF_GL_SETUP.SEQUENCE7_DESC%TYPE,
                             O_sequence8_desc  IN OUT  FIF_GL_SETUP.SEQUENCE8_DESC%TYPE,
                             O_sequence9_desc  IN OUT  FIF_GL_SETUP.SEQUENCE9_DESC%TYPE,
                             O_sequence10_desc IN OUT  FIF_GL_SETUP.SEQUENCE10_DESC%TYPE,
                             I_set_of_books_id IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'RMS2FIN.GET_SEQUENCE_LABELS';

   cursor C_GET_SEQ_DESC is
      select sequence1_desc,
             sequence2_desc,
             sequence3_desc,
             sequence4_desc,
             sequence5_desc,
             sequence6_desc,
             sequence7_desc,
             sequence8_desc,
             sequence9_desc,
             sequence10_desc
        from fif_gl_setup
       where set_of_books_id = NVL(I_set_of_books_id,set_of_books_id);

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SEQ_DESC', 'FIF_GL_SETUP', NULL);
   open C_GET_SEQ_DESC;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SEQ_DESC', 'FIF_GL_SETUP', NULL);
   fetch C_GET_SEQ_DESC into o_sequence1_desc,
                             o_sequence2_desc,
                             o_sequence3_desc,
                             o_sequence4_desc,
                             o_sequence5_desc,
                             o_sequence6_desc,
                             o_sequence7_desc,
                             o_sequence8_desc,
                             o_sequence9_desc,
                             o_sequence10_desc;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SEQ_DESC', 'FIF_GL_SETUP', NULL);
   close C_GET_SEQ_DESC;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SEQUENCE_LABELS;
--------------------------------------------------------------------------------------------------
FUNCTION GET_FIN_REF_KEY(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_reference_trace_id   IN OUT  KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE,
                         I_iftd_rowid           IN      KEY_MAP_GL.IFTD_ROWID%TYPE,
                         I_item                 IN      KEY_MAP_GL.ITEM%TYPE,
                         I_dept                 IN      KEY_MAP_GL.DEPT%TYPE,
                         I_class                IN      KEY_MAP_GL.CLASS%TYPE,
                         I_subclass             IN      KEY_MAP_GL.SUBCLASS%TYPE,
                         I_procesed_date        IN      KEY_MAP_GL.PROCESSED_DATE%TYPE,
                         I_tran_code            IN      KEY_MAP_GL.TRAN_CODE%TYPE,
                         I_location             IN      KEY_MAP_GL.LOCATION%TYPE,
                         I_loc_type             IN      KEY_MAP_GL.LOC_TYPE%TYPE,
                         I_reference_trace_type IN      KEY_MAP_GL.REFERENCE_TRACE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'RMS2FIN.GET_FIN_REF_KEY';
   L_system_variables_row    SYSTEM_VARIABLES%ROWTYPE;

   cursor C_REF_KEY is
      select reference_trace_id
        from key_map_gl
       where ((iftd_rowid = I_iftd_rowid)
          or  (iftd_rowid is NULL))
         and ((item = I_item)
          or  (item is NULL))
         and ((dept           = I_dept)
          or  (dept = -1))
         and ((class          = I_class)
          or  (class = -1))
         and ((subclass       = I_subclass)
          or  (subclass = -1))
         and processed_date = I_procesed_date
         and tran_code      = I_tran_code
         and location       = I_location
         and loc_type       = I_loc_type;

   cursor C_REF_KEY_GIM is
      select reference_trace_id
        from key_map_gl
       where ((iftd_rowid = I_iftd_rowid)
          or  (iftd_rowid is NULL))
         and ((item = I_item)
          or  (item is NULL))
         and ((dept           = I_dept)
          or  (dept = -1)) 
         and ((class          = I_class)
          or  (class = -1))
         and ((subclass       = I_subclass)
          or  (subclass = -1))
         and (processed_date  between L_system_variables_row.last_eom_start_month
         and  L_system_variables_row.last_eom_date)
         and tran_code      = I_tran_code
         and location       = I_location
         and loc_type       = I_loc_type;

BEGIN

   if GET_SYSTEM_VARIABLES(O_error_message ,
                           L_system_variables_row) = FALSE then
      return FALSE;
   end if;

   if I_reference_trace_type !='GIM' then

      open C_REF_KEY;
      fetch C_REF_KEY into O_reference_trace_id;
      if C_REF_KEY%NOTFOUND then
         O_reference_trace_id := NULL;
      end if;
      close C_REF_KEY;
   else
      open C_REF_KEY_GIM;
      fetch C_REF_KEY_GIM into O_reference_trace_id;
      if C_REF_KEY_GIM%NOTFOUND then
         O_reference_trace_id := NULL;
      end if;
      close C_REF_KEY_GIM;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_FIN_REF_KEY;
----------------------------------------------------------------------------------------------------
FUNCTION GET_KEY_MAP_GL(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_reference_trace_type  IN OUT  KEY_MAP_GL.REFERENCE_TRACE_TYPE%TYPE,
                        O_iftd_rowid            IN OUT  KEY_MAP_GL.IFTD_ROWID%TYPE,
                        O_item                  IN OUT  KEY_MAP_GL.ITEM%TYPE,
                        O_dept                  IN OUT  KEY_MAP_GL.DEPT%TYPE,
                        O_class                 IN OUT  KEY_MAP_GL.CLASS%TYPE,
                        O_subclass              IN OUT  KEY_MAP_GL.SUBCLASS%TYPE,
                        O_procesed_date         IN OUT  KEY_MAP_GL.PROCESSED_DATE%TYPE,
                        O_tran_code             IN OUT  KEY_MAP_GL.TRAN_CODE%TYPE,
                        O_location              IN OUT  KEY_MAP_GL.LOCATION%TYPE,
                        O_loc_type              IN OUT  KEY_MAP_GL.LOC_TYPE%TYPE,
                        I_reference_trace_id    IN      KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'RMS2FIN.GET_KEY_MAP_GL';

   cursor C_GET_KEY_MAP_GL is
      select reference_trace_type,
             iftd_rowid,
             item,
             dept,
             class,
             subclass,
             processed_date,
             tran_code,
             location,
             loc_type
        from key_map_gl
       where reference_trace_id = I_reference_trace_id;

BEGIN

   open C_GET_KEY_MAP_GL;
   fetch C_GET_KEY_MAP_GL into O_reference_trace_type,
                               O_iftd_rowid,
                               O_item,
                               O_dept,
                               O_class,
                               O_subclass,
                               O_procesed_date,
                               O_tran_code,
                               O_location,
                               O_loc_type;
   if C_GET_KEY_MAP_GL%NOTFOUND then
      close C_GET_KEY_MAP_GL;
      O_error_message := SQL_LIB.CREATE_MSG('INV_GL_REF_ID', NULL , NULL, NULL);
      return FALSE;
   end if;
   close C_GET_KEY_MAP_GL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_KEY_MAP_GL;
--------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE_DAY_SEQ_NO(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists           IN OUT  BOOLEAN,
                                   I_store_day_seq_no IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'RMS2FIN.VALIDATE_STORE_DAY_SEQ_NO';
   L_dummy1     VARCHAR2(1)  := NULL;
   L_dummy2     VARCHAR2(1)  := NULL;

   cursor C_EXIST_SA_STR_DAY is
      select 'x'
        from sa_store_day
       where store_day_seq_no = I_store_day_seq_no;

   cursor C_EXIST_SA_GL_REF_DATA is
      select 'x'
        from sa_gl_ref_data
       where store_day_seq_no = I_store_day_seq_no;

BEGIN

   open C_EXIST_SA_STR_DAY;
   fetch C_EXIST_SA_STR_DAY into L_dummy1;
   close C_EXIST_SA_STR_DAY;
   
   if L_dummy1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_DAY_SEQ', NULL , NULL, NULL);
      O_exists := FALSE;
      return TRUE;
   else
      open C_EXIST_SA_GL_REF_DATA;
      fetch C_EXIST_SA_GL_REF_DATA into L_dummy2;
      close C_EXIST_SA_GL_REF_DATA;
      
      if L_dummy2 is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_TRAN_STR_DAY_SEQ', NULL , NULL, NULL);
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_STORE_DAY_SEQ_NO;
--------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TOTAL_ID(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists           IN OUT  BOOLEAN,
                           I_total_id         IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'RMS2FIN.VALIDATE_TOTAL_ID';
   L_dummy1     VARCHAR2(1)  := NULL;
   L_dummy2     VARCHAR2(1)  := NULL;

   cursor C_EXIST_SA_TOT_HEAD is
      select 'x'
        from sa_total_head
       where total_id = I_total_id;

   cursor C_EXIST_SA_GL_REF_DATA is
      select 'x'
        from sa_gl_ref_data
       where total_id = I_total_id;

BEGIN

   open C_EXIST_SA_TOT_HEAD;
   fetch C_EXIST_SA_TOT_HEAD into L_dummy1;
   close C_EXIST_SA_TOT_HEAD;
   
   if L_dummy1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TOTAL_ID', NULL , NULL, NULL);
      O_exists := FALSE;
      return TRUE;
   else
      open C_EXIST_SA_GL_REF_DATA;
      fetch C_EXIST_SA_GL_REF_DATA into L_dummy2;
      close C_EXIST_SA_GL_REF_DATA;
      
      if L_dummy2 is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_TRAN_TOTAL_ID', NULL , NULL, NULL);
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_TOTAL_ID;
--------------------------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_VARIABLES(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_system_variables_row    IN OUT  SYSTEM_VARIABLES%ROWTYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'RMS2FIN.GET_SYSTEM_VARIABLES';

   cursor C_SYSTEM_VARIABLES is
      select *
        from system_variables;

BEGIN

   open  C_SYSTEM_VARIABLES;
   fetch C_SYSTEM_VARIABLES into O_system_variables_row;
   close C_SYSTEM_VARIABLES;

   -- Check a required field, if it is null then there was a problem
   -- reading from the table.
   if O_system_variables_row.last_eom_half_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURSOR',
                                            'C_SYSTEM_VARIABLES',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SYSTEM_VARIABLES;
--------------------------------------------------------------------------------------------------
END RMS2FIN;
/
