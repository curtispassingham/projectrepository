
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_IMPORT_ADJ_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION POST_ADJUSTMENT(O_error_message   IN OUT  VARCHAR2,
                         I_source          IN      SA_STORE_DATA.SYSTEM_CODE%TYPE,
                         I_new_value       IN      SA_HQ_VALUE.HQ_VALUE%TYPE,
                         I_total_seq_no    IN      SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         I_store_no        IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                         I_business_date   IN      SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                         I_total_id        IN      SA_TOTAL.TOTAL_ID%TYPE DEFAULT NULL,
                         I_ref_no1         IN      SA_TOTAL.REF_NO1%TYPE DEFAULT NULL,
                         I_ref_no2         IN      SA_TOTAL.REF_NO2%TYPE DEFAULT NULL,
                         I_ref_no3         IN      SA_TOTAL.REF_NO3%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END SA_IMPORT_ADJ_SQL;
/
