SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_RULE_SQL AUTHID CURRENT_USER AS
--
-----------------------------------------------------------------------
-- FUNCTION CHECK_UNIQUE_RULE_ID
-- will be used to ensure that a given
-- rule_id is unique.
--
FUNCTION CHECK_UNIQUE_RULE_ID(O_error_message   IN OUT   VARCHAR2,
                              O_unique          IN OUT   BOOLEAN,
                              I_rule_id         IN       SA_RULE_HEAD.RULE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
-- FUNCTION GET_MAX_REV
-- selects the maximum revision for a given rule.
--
FUNCTION GET_MAX_REV(O_error_message   IN OUT   VARCHAR2,
                     O_rule_rev_no     IN OUT   SA_RULE_HEAD.RULE_REV_NO%TYPE,
                     I_rule_id         IN       SA_RULE_HEAD.RULE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
-- FUNCTION GET_NO_ERRORS
-- counts the number of errors associated with a
-- rule_id from sa_rule_comp_errors table.
--
FUNCTION GET_NO_ERRORS(O_error_message   IN OUT   VARCHAR2,
                       O_no_errors       IN OUT   NUMBER,
                       I_rule_id         IN       SA_RULE_HEAD.RULE_ID%TYPE,
                       I_rule_rev_no     IN       SA_RULE_HEAD.RULE_REV_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
-- FUNCTION GET_ERROR_DESC_CONCAT
-- selects the error codes associated
-- with a rule id and concatonates all of the errors into one string
--
FUNCTION GET_ERROR_DESC_CONCAT(O_error_message       IN OUT   VARCHAR2,
                               O_error_desc_concat   IN OUT   VARCHAR2,
                               I_rule_id             IN       SA_RULE_HEAD.RULE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
-- FUNCTION VALIDATE_REV_NO
-- validates the rev_no field when a rule_id is
-- selected
--
FUNCTION VALIDATE_REV_NO(O_error_message   IN OUT   VARCHAR2,
                         I_rule_id         IN       SA_RULE_HEAD.RULE_ID%TYPE,
                         I_rule_rev_no     IN       SA_RULE_HEAD.RULE_REV_NO%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
-- FUNCTION VALIDATE_UPDATE_ID
-- validates that the update_id exists.
--
FUNCTION VALIDATE_UPDATE_ID(O_error_message   IN OUT   VARCHAR2,
                            I_update_id       IN       SA_RULE_HEAD.UPDATE_ID%TYPE,
                            I_rule_id         IN       SA_RULE_HEAD.RULE_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
-- FUNCTION VALID_MULTI_VERSIONS
-- Checks if the current rule can be active with previous revisions of
-- the same rule based on start and end dates for the rule.
--
FUNCTION VALID_MULTI_VERSIONS(O_error_message         IN OUT   VARCHAR2,
                              O_valid_multi_rev_ind   IN OUT   VARCHAR2,
                              I_rule_id               IN       SA_RULE_HEAD.RULE_ID%TYPE,
                              I_rule_rev_no           IN       SA_RULE_HEAD.RULE_REV_NO%TYPE,
                              I_start_date            IN       SA_RULE_HEAD.START_BUSINESS_DATE%TYPE,
                              I_end_date              IN       SA_RULE_HEAD.END_BUSINESS_DATE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
-- FUNCTION DROP_INVALID_MULTI_VERSIONS
-- Drops all revisions of the rule that are invalid with the current
-- rule revision.
--
FUNCTION DROP_INVALID_MULTI_VERSIONS(O_error_message   IN OUT   VARCHAR2,
                                     I_rule_id         IN       SA_RULE_HEAD.RULE_ID%TYPE,
                                     I_rule_rev_no     IN       SA_RULE_HEAD.RULE_REV_NO%TYPE,
                                     I_start_date      IN       SA_RULE_HEAD.START_BUSINESS_DATE%TYPE,
                                     I_end_date        IN       SA_RULE_HEAD.END_BUSINESS_DATE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
-- FUNCTION FUNCTION_STATUS
-- Determines if the current revision of the rule has an active
-- function associated with it.
--
FUNCTION FUNCTION_STATUS(O_error_message        IN OUT   VARCHAR2,
                         O_valid_function_ind   IN OUT   VARCHAR2,
                         I_rule_id              IN       SA_RULE_HEAD.RULE_ID%TYPE,
                         I_rule_rev_no          IN       SA_RULE_HEAD.RULE_REV_NO%TYPE,
                         I_start_date           IN       SA_RULE_HEAD.START_BUSINESS_DATE%TYPE,
                         I_end_date             IN       SA_RULE_HEAD.END_BUSINESS_DATE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------
END SA_RULE_SQL;
/
