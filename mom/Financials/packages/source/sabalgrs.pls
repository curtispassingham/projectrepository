
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BAL_GROUP_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
--- Function: GET_CASHIER_REGISTER
--- Purpose:  Get the cashier and register from the sa_balance_group table for 
---           the passed in store_day_seq_no and bal_group_seq_no.
---
---
FUNCTION GET_CASHIER_REGISTER(O_error_message    IN OUT VARCHAR2,
                              O_cashier          IN OUT SA_BALANCE_GROUP.CASHIER%TYPE,
                              O_register         IN OUT SA_BALANCE_GROUP.REGISTER%TYPE,
                              I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                              I_bal_group_seq_no IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Function: CHECK_DUPS
--- Purpose:  Check if duplicate records exists for a cashier/register in a given
---           store day with given start and end times. 
---
---
FUNCTION CHECK_DUPS(O_error_message    IN OUT VARCHAR2,
                    O_exists           IN OUT BOOLEAN,
                    I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                    I_cashier          IN     SA_BALANCE_GROUP.CASHIER%TYPE,
                    I_register         IN     SA_BALANCE_GROUP.REGISTER%TYPE,
                    I_start_datetime   IN     SA_BALANCE_GROUP.START_DATETIME%TYPE,
                    I_end_datetime     IN     SA_BALANCE_GROUP.END_DATETIME%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--- Function: INSERT_BAL_GROUP
--- Purpose:  Insert a new record into the sa_balance_group table that is not a 
---           duplicate record, and without start or end times.
---
---
FUNCTION INSERT_BAL_GROUP(O_error_message        IN OUT  VARCHAR2,
                          O_bal_group_inserted   IN OUT  BOOLEAN,
                          O_new_bal_group_seq_no IN OUT  SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                          I_store_day_seq_no     IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                          I_cashier              IN      SA_BALANCE_GROUP.CASHIER%TYPE,
                          I_register             IN      SA_BALANCE_GROUP.REGISTER%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function: CASHIER_EXISTS
-- Purpose : This function will check for the existence of the passed in cashier
--           on either the SA_STORE_EMP table or the SA_BALANCE_GROUP table.
--
FUNCTION CASHIER_EXISTS(O_error_message IN OUT  VARCHAR2,
                        O_exists 	    IN OUT  BOOLEAN,
                        O_cashier_name  IN OUT  SA_EMPLOYEE.NAME%TYPE,
                        I_cashier       IN      SA_BALANCE_GROUP.CASHIER%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--- Function:  CASHIER_REGISTER_EXISTS
--- Purpose:   Determine if transactions exist for the inputed cashier or register with 
---            the inputed store day sequence number in the inputed date/time range.
---
---
FUNCTION CASHIER_REGISTER_EXISTS(O_error_message     IN OUT  VARCHAR2,
                                 O_exists            IN OUT  BOOLEAN,
                                 I_store_day_seq_no  IN      SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                 I_cashier           IN      SA_TRAN_HEAD.CASHIER%TYPE,
                                 I_register          IN      SA_TRAN_HEAD.REGISTER%TYPE,
                                 I_start_datetime    IN      SA_TRAN_HEAD.TRAN_DATETIME%TYPE,
                                 I_end_datetime      IN      SA_TRAN_HEAD.TRAN_DATETIME%TYPE,
				 I_store	     IN      SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
				 I_day		     IN	     SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--- Function:  TOTAL_EXISTS
--- Purpose:   Check if any totals exist for the inputted balance group
---            sequence number.
---
---
FUNCTION TOTAL_EXISTS(O_error_message     IN OUT  VARCHAR2,
                      O_exists            IN OUT  BOOLEAN,
                      I_bal_group_seq_no  IN OUT  SA_TOTAL.BAL_GROUP_SEQ_NO%TYPE,
		      I_store		  IN      SA_TOTAL.STORE%TYPE DEFAULT NULL,
		      I_day		  IN      SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--- Function: GET_BAL_GROUP_INFO
--- Purpose:  This function will get the source information (cashier, register, name)
---           for a given store day or balance group sequence number.
---
FUNCTION GET_BAL_GROUP_INFO(O_error_message    IN OUT  VARCHAR2,
                            O_register         IN OUT  SA_TRAN_HEAD.REGISTER%TYPE,
                            O_cashier          IN OUT  SA_TRAN_HEAD.CASHIER%TYPE,
                            O_cashier_name     IN OUT  SA_EMPLOYEE.NAME%TYPE,
                            I_store_day_seq_no IN      SA_BALANCE_GROUP.STORE_DAY_SEQ_NO%TYPE,
                            I_bal_group_seq_no IN      SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                            I_store            IN      STORE.STORE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
---Function: DELETE_BALANCE_GROUP
---Purpose:  This function will accpet a store/day sequence number, transaction sequence
---          number and a cashier or register and check to see if there are any other
---          transactions other than the one passed in that have the same cashier or
---          register.  If not, the record for the store/day and cashier or register will
---          be deleted from sa_balance group provided there are no conflicts such as 
---          existing errors and totals.
---
FUNCTION DELETE_BALANCE_GROUP(O_error_message     IN OUT  VARCHAR2,
                              O_bal_group_deleted IN OUT  BOOLEAN,
                              I_tran_seq_no       IN      sa_tran_head.tran_seq_no%TYPE,
                              I_store_day_seq_no  IN      sa_balance_group.store_day_seq_no%TYPE,
                              I_cashier           IN      sa_balance_group.cashier%TYPE,
                              I_register          IN      sa_balance_group.register%TYPE,
			      I_store		  IN      sa_tran_head.store%TYPE DEFAULT NULL,
			      I_day		  IN      sa_tran_head.day%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
---Function:  GET_BAL_GROUP_SEQ
---Purpose:   This function will accept a store/day sequence number, cashier or register information,
---           and start/end date/times to output an existence variable and applicable balance group
---           sequence number.  It will be called from the transaction detail form when the user
---           changes the cashier (when the balance level is cashier) and when the user changes
---           the register (when the balance level is register).
---
FUNCTION GET_BAL_GROUP_SEQ(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists              IN OUT  BOOLEAN,
                           O_bal_group_seq_no    IN OUT  SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                           IO_balance_level_ind  IN OUT  SA_SYSTEM_OPTIONS.BALANCE_LEVEL_IND%TYPE,
                           I_store_day_seq_no    IN      SA_BALANCE_GROUP.STORE_DAY_SEQ_NO%TYPE,
                           I_cashier             IN      SA_BALANCE_GROUP.CASHIER%TYPE,
                           I_register            IN      SA_BALANCE_GROUP.REGISTER%TYPE,
                           I_start_datetime      IN      SA_BALANCE_GROUP.START_DATETIME%TYPE,
                           I_end_datetime        IN      SA_BALANCE_GROUP.END_DATETIME%TYPE)
RETURN BOOLEAN;                                       
--------------------------------------------------------------------------------------
END BAL_GROUP_SQL;
/
