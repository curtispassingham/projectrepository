
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_COGS_SQL AS

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION PROCESS_COGS_ADJ(O_error_message IN OUT VARCHAR2,
                          I_item          IN     ITEM_MASTER.ITEM%TYPE,
                          I_store         IN     STORE.STORE%TYPE,
                          I_trans_date    IN     DATE,
                          I_units         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_header_media  IN     VARCHAR2,
                          I_line_media    IN     VARCHAR2,
                          I_reason_code   IN     CODE_DETAIL.CODE%TYPE,
                          I_supplier      IN     SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

FUNCTION PERSIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message_type  IN     VARCHAR2,
                 I_cogs_rec      IN     RMSSUB_COGS.COGS_REC_TYPE)
RETURN BOOLEAN IS

L_program   VARCHAR2(50) := 'RMSSUB_COGS_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_COGS.COGS_ADD then
      if PROCESS_COGS_ADJ(O_error_message,
                          I_cogs_rec.item,
                          I_cogs_rec.store,
                          I_cogs_rec.trans_date,
                          I_cogs_rec.units,
                          I_cogs_rec.header_media,
                          I_cogs_rec.line_media,
                          I_cogs_rec.reason_code,
                          I_cogs_rec.supplier) = FALSE then
         return FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE',
      I_message_type, null, null);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;


--------------------------------------------------------------------------------
-- PRIVATE PPROCEDURES
--------------------------------------------------------------------------------

FUNCTION PROCESS_COGS_ADJ(O_error_message IN OUT VARCHAR2,
                          I_item          IN     ITEM_MASTER.ITEM%TYPE,
                          I_store         IN     STORE.STORE%TYPE,
                          I_trans_date    IN     DATE,
                          I_units         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_header_media  IN     VARCHAR2,
                          I_line_media    IN     VARCHAR2,
                          I_reason_code   IN     CODE_DETAIL.CODE%TYPE,
                          I_supplier      IN     SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS

L_program          VARCHAR2(40) := 'RMSSUB_COGS_SQL.PROCESS_COGS_ADJ';
L_null_parameter   VARCHAR2(20) := null;
L_exist            BOOLEAN      := TRUE;

L_dept             DEPS.DEPT%TYPE;
L_class            CLASS.CLASS%TYPE;
L_subclass         SUBCLASS.SUBCLASS%TYPE;
L_av_cost          ITEM_LOC_SOH.AV_COST%TYPE;
L_concat_media     VARCHAR2(10)  := null;
L_tran_code        TRAN_DATA.TRAN_CODE%TYPE;
L_total_cost       TRAN_DATA.TOTAL_COST%TYPE;

L_unit_retail      ITEM_LOC.UNIT_RETAIL%TYPE := null;
L_total_retail     TRAN_DATA.TOTAL_RETAIL%TYPE := 0;

cursor C_GET_UNIT_RETAIL is
select unit_retail
  from item_loc
 where item = I_item
   and loc = I_store
   and loc_type = 'S';

BEGIN

   if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                     I_item,
                                     L_dept,
                                     L_class,
                                     L_subclass) = FALSE then
      return FALSE;
   end if;

   if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                     I_item,
                                     I_store,
                                     'S',
                                     L_av_cost) = FALSE then
      return FALSE;
   end if;

   --- Concatinate header media and line media values for the insert into tran_data.ref_no_1.
   L_concat_media := I_header_media || I_line_media;
   ---
   L_tran_code := 1;
   L_total_cost := (I_units * L_av_cost);
   ---
   if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                    I_item,
                                    L_dept,
                                    L_class,
                                    L_subclass,
                                    I_store,
                                    'S',
                                    I_trans_date,
                                    L_tran_code,
                                    null,                 -- adj_code
                                    I_units,
                                    L_total_cost,
                                    0,                    -- total_retail
                                    L_concat_media,       -- I_ref_no_1
                                    I_reason_code,        -- I_ref_no_2
                                    null,                 -- tsf_source_location
                                    null,                 -- tsf_source_loc_type
                                    null,                 -- old_unit_retail
                                    null,                 -- new_unit_retail
                                    null,                 -- source_dept
                                    null,                 -- source_class
                                    null,                 -- source_subclass
                                    'rmssub_cogs_sql',
                                    null) = FALSE then   -- gl_ref_no
      return FALSE;
   end if;

   open  C_GET_UNIT_RETAIL;
   fetch C_GET_UNIT_RETAIL into L_unit_retail;
   if C_GET_UNIT_RETAIL%NOTFOUND then
      o_error_message := SQL_LIB.CREATE_MSG('NO_REC_ITEM_LOC_LOCT', I_item,
      to_char(I_store), 'S');
      CLOSE C_GET_UNIT_RETAIL;
      return FALSE;
   end if;
   close C_GET_UNIT_RETAIL;

   L_total_retail := (I_units * L_unit_retail);
   L_total_cost   := null;
   L_tran_code := 13;

   if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                    I_item,
                                    L_dept,
                                    L_class,
                                    L_subclass,
                                    I_store,
                                    'S',
                                    I_trans_date,
                                    L_tran_code,          -- tran_code
                                    null,                 -- adj_code
                                    0,                    -- units
                                    L_total_cost,
                                    L_total_retail,
                                    L_concat_media,       -- I_ref_no_1
                                    I_reason_code,        -- I_ref_no_2
                                    null,                 -- tsf_source_location
                                    null,                 -- tsf_source_loc_type
                                    L_unit_retail,        -- old_unit_retail
                                    0,                    -- new_unit_retail
                                    null,                 -- source_dept
                                    null,                 -- source_class
                                    null,                 -- source_subclass
                                    'rmssub_cogs_sql',
                                    null) = FALSE then   -- gl_ref_no
      return FALSE;
   end if;

   -- Increment/decrement the store stock on hand for replacement-in/out.
   if I_reason_code = '1' then
      update item_loc_soh
         set stock_on_hand = (stock_on_hand + (-1 * I_units)),
             soh_update_datetime = sysdate,
             last_update_datetime = sysdate,
             last_update_id = get_user
       where loc = to_number(I_store)
         and item = I_item;
   elsif I_reason_code = '2' then
      update item_loc_soh
         set stock_on_hand = (stock_on_hand - I_units),
             soh_update_datetime = sysdate,
             last_update_datetime = sysdate,
             last_update_id = get_user
       where loc = to_number(I_store)
         and item = I_item;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_COGS_ADJ;
--------------------------------------------------------------------------------

END RMSSUB_COGS_SQL;
/
