create or replace PACKAGE BODY coresvc_sa_rounding_rule
is
   Type RR_dtl_temp_tab is
   TABLE OF SVC_SA_ROUNDING_RULE_DETAIL%rowtype;
   Type errors_tab_typ
   is
   TABLE OF SVC_ADMIN_UPLD_ER%rowtype;
   Lp_errors_tab errors_tab_typ;
   
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang       LANG.LANG%TYPE;
   
   CURSOR c_svc_SA_RR_HEAD(I_process_id NUMBER,I_chunk_id NUMBER)
   is
   select
       PK_SA_ROUNDING_RULE_HEAD.rowid  AS PK_SA_ROUNDING_RULE_HEAD_rid,
       UK_SA_ROUNDING_RULE_HEAD.rowid  AS UK_SA_ROUNDING_RULE_HEAD_rid,
   PK_SA_ROUNDING_RULE_HEAD.start_business_date as prev_date,
       st.STATUS,
       st.START_BUSINESS_DATE,
       st.COUNTRY_ID,
       st.CURRENCY_CODE,
       st.ROUNDING_RULE_NAME,
       st.ROUNDING_RULE_ID,
       st.PROCESS_ID,
       st.ROW_SEQ,
       upper(st.ACTION) AS action,
       st.PROCESS$STATUS,
   PK_SA_ROUNDING_RULE_HEAD.country_id prev_country_id,
   PK_SA_ROUNDING_RULE_HEAD.currency_code prev_currency_code
    from SVC_SA_ROUNDING_RULE_HEAD st,
       SA_ROUNDING_RULE_HEAD PK_SA_ROUNDING_RULE_HEAD,
       SA_ROUNDING_RULE_HEAD UK_SA_ROUNDING_RULE_HEAD,
       dual
    where st.process_id = I_process_id
    and st.chunk_id     = I_chunk_id
    and st.ROUNDING_RULE_ID         = PK_SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID (+)
    and st.COUNTRY_ID         = UK_SA_ROUNDING_RULE_HEAD.COUNTRY_ID (+)
    and st.CURRENCY_CODE         = UK_SA_ROUNDING_RULE_HEAD.CURRENCY_CODE (+)
    and st.action      is NOT NULL;
   c_svc_SA_RR_HEAD_rec c_svc_SA_RR_HEAD%rowtype;
   
     CURSOR c_svc_SA_ROUNDING_RULE_DETAIL(I_process_id NUMBER,I_chunk_id NUMBER)
   is
   select
       SRD_SRH_FK.rowid    AS SRD_SRH_FK_rid,
       UK_SA_ROUNDING_RULE_DETAIL.rowid  AS UK_SA_ROUNDING_RULE_DETAIL_rid,
       st.ROUND_AMT,
       st.HIGH_ENDING_AMT,
       st.LOW_ENDING_AMT,
       st.ROUNDING_RULE_ID,
       st.PROCESS_ID,
       st.ROW_SEQ,
       upper(st.ACTION) AS action,
       st.PROCESS$STATUS
    from SVC_SA_ROUNDING_RULE_DETAIL st,
       SA_ROUNDING_RULE_HEAD SRD_SRH_FK,
       SA_ROUNDING_RULE_DETAIL UK_SA_ROUNDING_RULE_DETAIL,
       dual
    where st.process_id = I_process_id
    and st.chunk_id     = I_chunk_id
    and st.ROUNDING_RULE_ID        = SRD_SRH_FK.ROUNDING_RULE_ID (+)
    and st.rounding_rule_id=UK_SA_ROUNDING_RULE_DETAIL.rounding_rule_id(+)
    and st.low_ending_amt=UK_SA_ROUNDING_RULE_DETAIL.low_ending_amt(+)
    and st.high_ending_amt=UK_SA_ROUNDING_RULE_DETAIL.high_ending_amt(+)
    and st.action      is NOT NULL order by st.rounding_rule_id,st.row_seq;
   c_svc_SA_RR_DETAIL_rec c_svc_SA_ROUNDING_RULE_DETAIL%rowtype;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------------- 

 PROCEDURE WRITE_ERROR(I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
-------------------------------------------------------------------------------- 
---  Name: exec_SA_ROUNDING_RULE_HEAD_ins
---  Purpose: FOR inserting data in sa_rounding_rule_head
-------------------------------------------------------------------------------- 
PROCEDURE exec_SA_ROUNDING_RULE_HEAD_ins
(
 I_ins_tab IN SA_RR_HEAD_rec_tab
)
is
BEGIN

IF I_ins_tab     IS NOT NULL AND I_ins_tab.count>0 THEN
  FORALL i IN INDICES OF I_ins_tab
     insert INTO SA_ROUNDING_RULE_HEAD VALUES I_ins_tab
             ( i
             );
end if;
end exec_SA_ROUNDING_RULE_HEAD_ins;

-------------------------------------------------------------------------------- 
---  Name: exec_SA_ROUNDING_RULE_HEAD_upd
---  Purpose: FOR updating data in sa_rounding_rule_head
-------------------------------------------------------------------------------- 
PROCEDURE exec_SA_ROUNDING_RULE_HEAD_upd
(
 I_upd_tab IN SA_RR_HEAD_rec_tab
)
is
BEGIN
    FORall i IN I_upd_tab.first..I_upd_tab.last
    update SA_ROUNDING_RULE_HEAD SET row = I_upd_tab(i) where 1 = 1
    and ROUNDING_RULE_ID = I_upd_tab(i).ROUNDING_RULE_ID;

end exec_SA_ROUNDING_RULE_HEAD_upd;
-------------------------------------------------------------------------------- 
---  Name: exec_SA_ROUNDING_RULE_HEAD_del
---  Purpose: FOR deleting data in sa_rounding_rule_head
-------------------------------------------------------------------------------- 
PROCEDURE exec_SA_ROUNDING_RULE_HEAD_del(
 I_del_tab IN SA_RR_HEAD_rec_tab )
is
BEGIN
IF I_del_tab     IS NOT NULL AND I_del_tab.count>0 THEN
  FORALL i IN INDICES OF I_del_tab
     delete from sa_rounding_rule_detail 
        where 1 = 1
          and rounding_rule_id = I_del_tab(i).ROUNDING_RULE_ID;
              
  FORALL i IN INDICES OF I_del_tab       
     delete from SA_ROUNDING_RULE_HEAD_TL 
        where 1 = 1
         and ROUNDING_RULE_ID = I_del_tab(i).ROUNDING_RULE_ID;
      
  FORALL i IN INDICES OF I_del_tab
     delete from SA_ROUNDING_RULE_HEAD 
        where 1 = 1
         and ROUNDING_RULE_ID = I_del_tab(i).ROUNDING_RULE_ID;

end if;
end exec_SA_ROUNDING_RULE_HEAD_del;
-------------------------------------------------------------------------------- 
---  Name: EXEC_SA_RR_HEAD_TL_INS
---  Purpose: This procedure inserts data into the sa_rounding_rule_head_tl table
-------------------------------------------------------------------------------- 
PROCEDURE EXEC_SA_RR_HEAD_TL_INS(I_ins_tab   IN   SA_RR_HEAD_TL_REC_TAB) is
BEGIN
   if I_ins_tab is NOT NULL and I_ins_tab.count > 0 then
      FORALL i IN 1..I_ins_tab.count()
         insert into sa_rounding_rule_head_tl
              values I_ins_tab(i);
   end if;

end EXEC_SA_RR_HEAD_TL_INS;

-------------------------------------------------------------------------------- 
---  Name: EXEC_SA_RR_HEAD_TL_UPD
---  Purpose: This function updates data in the sa_rounding_rule_head_tl table
-------------------------------------------------------------------------------- 
FUNCTION EXEC_SA_RR_HEAD_TL_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_upd_tab                IN       SA_RR_HEAD_TL_REC_TAB,
                                I_sa_rr_head_tl_upd_rst  IN       ROW_SEQ_TAB,
                                I_process_id             IN       SVC_SA_ROUNDING_RULE_HEAD_TL.PROCESS_ID%TYPE,
                                I_chunk_id               IN       SVC_SA_ROUNDING_RULE_HEAD_TL.CHUNK_ID%TYPE) 
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SA_ROUNDING_RULE.EXEC_SA_RR_HEAD_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ROUNDING_RULE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(50) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;
   
    --Cursor to lock the record
   cursor C_LOCK_SARRHD_TL_UPD(I_rounding_rule_id  SA_ROUNDING_RULE_HEAD_TL.ROUNDING_RULE_ID%TYPE,
                               I_lang              SA_ROUNDING_RULE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from sa_rounding_rule_head_tl
       where rounding_rule_id = I_rounding_rule_id
         and lang = I_lang
         for update nowait;
         
BEGIN
   if I_upd_tab is NOT NULL and I_upd_tab.count > 0 then
      for i in I_upd_tab.FIRST..I_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_upd_tab(i).lang);
            L_key_val2 := 'Rounding Rule ID: '||to_char(I_upd_tab(i).rounding_rule_id);
            open C_LOCK_SARRHD_TL_UPD(I_upd_tab(i).rounding_rule_id,
                                      I_upd_tab(i).lang);
            close C_LOCK_SARRHD_TL_UPD;
            
            update sa_rounding_rule_head_tl
               set rounding_rule_name = I_upd_tab(i).rounding_rule_name,
                   last_update_id = I_upd_tab(i).last_update_id,
                   last_update_datetime = I_upd_tab(i).last_update_datetime
             where rounding_rule_id = I_upd_tab(i).rounding_rule_id
               and lang = I_upd_tab(i).lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SA_ROUNDING_RULE_HEAD_TL',
                           I_sa_rr_head_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_LOCK_SARRHD_TL_UPD%ISOPEN then
         close C_LOCK_SARRHD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END EXEC_SA_RR_HEAD_TL_UPD;
-------------------------------------------------------------------------------
---  Name: EXEC_SA_RR_HEAD_TL_DEL
---  Purpose: This function deletes data from the sa_rounding_rule_head_tl table
-------------------------------------------------------------------------------- 
FUNCTION EXEC_SA_RR_HEAD_TL_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_del_tab                IN       SA_RR_HEAD_TL_REC_TAB,
                                I_sa_rr_head_tl_del_rst  IN       ROW_SEQ_TAB,
                                I_process_id             IN       SVC_SA_ROUNDING_RULE_HEAD_TL.PROCESS_ID%TYPE,
                                I_chunk_id               IN       SVC_SA_ROUNDING_RULE_HEAD_TL.CHUNK_ID%TYPE) 
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SA_ROUNDING_RULE.EXEC_SA_RR_HEAD_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ROUNDING_RULE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(50) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;
   
    --Cursor to lock the record
   cursor C_LOCK_SARRHD_TL_DEL(I_rounding_rule_id  SA_ROUNDING_RULE_HEAD_TL.ROUNDING_RULE_ID%TYPE,
                               I_lang              SA_ROUNDING_RULE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from sa_rounding_rule_head_tl
       where rounding_rule_id = I_rounding_rule_id
         and lang = I_lang
         for update nowait;
         
BEGIN
   if I_del_tab is NOT NULL and I_del_tab.count > 0 then
      for i in I_del_tab.FIRST..I_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_del_tab(i).lang);
            L_key_val2 := 'Rounding Rule ID: '||to_char(I_del_tab(i).rounding_rule_id);
            open C_LOCK_SARRHD_TL_DEL(I_del_tab(i).rounding_rule_id,
                                      I_del_tab(i).lang);
            close C_LOCK_SARRHD_TL_DEL;
            
            delete
              from sa_rounding_rule_head_tl
             where rounding_rule_id = I_del_tab(i).rounding_rule_id
               and lang = I_del_tab(i).lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SA_ROUNDING_RULE_HEAD_TL',
                           I_sa_rr_head_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_LOCK_SARRHD_TL_DEL%ISOPEN then
         close C_LOCK_SARRHD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END EXEC_SA_RR_HEAD_TL_DEL;
-------------------------------------------------------------------------------
---  Name: check_detail
--- Purpose: To check and remove all header records without any detail
-------------------------------------------------------------------------------- 
PROCEDURE check_detail( O_sa_rr_head_ins_rec        IN OUT SA_RR_HEAD_rec_tab  ,
                            O_sa_rr_detail_del_rec IN OUT SA_RR_DETAIL_rec_tab,
                            I_process_id IN SVC_SA_ROUNDING_RULE_HEAD.PROCESS_ID%TYPE,
                            I_chunk_id   IN SVC_SA_ROUNDING_RULE_HEAD.CHUNK_ID%TYPE)
is


CURSOR c_chk_invalid_dtl(p_rounding_rule_id SVC_SA_ROUNDING_RULE_HEAD.rounding_rule_id%type)
is
    select row_seq
    from SVC_SA_ROUNDING_RULE_HEAD srh
    where srh.process_id =I_process_id
    and srh.chunk_id   =I_chunk_id
    and srh.action= stg_svc_SA_ROUNDING_RULE.action_new
    and srh.rounding_rule_id=p_rounding_rule_id
    and srh.process$status='IP'
    and not exists ( select 1 from SVC_SA_ROUNDING_RULE_DETAIL srd
                            where srd.process_id =srh.process_id
                              and srd.chunk_id   =srh.chunk_id
                              and srd.action= stg_svc_SA_ROUNDING_RULE.action_new
                              and srd.process$status='IP'
                              and srh.rounding_rule_id=srd.rounding_rule_id);
    
cursor c_get_distinct_rule_id
is
    select distinct srd.rounding_rule_id from SVC_SA_ROUNDING_RULE_detail srd
     where srd.process_id=I_process_id
        and srd.chunk_id=I_chunk_id
        and srd.process$status='IP' 
        and srd.action=stg_svc_SA_ROUNDING_RULE.action_del
        and not exists( select 1 from SVC_SA_ROUNDING_RULE_head srh
                                        where srd.rounding_rule_id=srh.rounding_rule_id
                                          and srd.process_id=srh.process_id
                                          and srd.chunk_id=srh.chunk_id
                                          and srh.process$status='IP' 
                                          and srh.action=stg_svc_SA_ROUNDING_RULE.action_del);


cursor c_get_total_records(p_rounding_rule_id SA_ROUNDING_RULE_detail.rounding_rule_id%type) is
select 
      (select count(*) from SA_ROUNDING_RULE_detail
        where rounding_rule_id=p_rounding_rule_id)
      +
      (select count(*) from SVC_SA_ROUNDING_RULE_detail
         where rounding_rule_id=p_rounding_rule_id  
         and process_id=I_process_id
         and chunk_id=I_chunk_id
         and process$status='IP' 
         and action=stg_svc_SA_ROUNDING_RULE.action_new)
      as total from dual;


cursor c_get_row_seq(p_rounding_rule_id SA_ROUNDING_RULE_detail.rounding_rule_id%type,
                        p_low_ending_amt SA_ROUNDING_RULE_detail.low_ending_amt%type,
                        p_high_ending_amt SA_ROUNDING_RULE_detail.high_ending_amt%type)
is
    select row_seq from SVC_SA_ROUNDING_RULE_detail
    where rounding_rule_id=p_rounding_rule_id  
     and process_id=I_process_id
     and chunk_id=I_chunk_id
     and low_ending_amt=p_low_ending_amt
     and high_ending_amt=p_high_ending_amt;

l_row_seq svc_sa_rounding_rule_head.row_seq%type;
l_dtl_count number(8);                            
l_del_count number(8); 
BEGIN

---remove invalid NEW  head records that do not have corresponding valid detail records
    FOR i IN 1..O_sa_rr_head_ins_rec.count()
    LOOP
        l_row_seq:=null;
        SQL_LIB.SET_MARK('OPEN','C_CHK_INVALID_DTL','SVC_SA_ROUNDING_RULE_HEAD',NULL);
        open c_chk_invalid_dtl(O_sa_rr_head_ins_rec(i).rounding_rule_id);
        SQL_LIB.SET_MARK('FETCH','C_CHK_INVALID_DTL','SVC_SA_ROUNDING_RULE_HEAD',NULL);
        fetch c_chk_invalid_dtl into l_row_seq;
       SQL_LIB.SET_MARK('CLOSE','C_CHK_INVALID_DTL','SVC_SA_ROUNDING_RULE_HEAD',NULL);
        close c_chk_invalid_dtl;

        if l_row_seq is not null then-- no details exist from the head record
             write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,'SA_ROUNDING_RULE_HEAD',l_row_seq,NULL,'DETAIL_REC');
             O_sa_rr_head_ins_rec.delete(i);
          
        end if;

    end LOOP;

    FOR rec in c_get_distinct_rule_id
    loop
    l_dtl_count:=0;l_del_count:=0;
    -- get the dtl count
    SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_RECORDS','SA_ROUNDING_RULE_DETAIL',NULL);
    open c_get_total_records(rec.rounding_rule_id);
    SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_RECORDS','SA_ROUNDING_RULE_DETAIL',NULL);
    fetch c_get_total_records into l_dtl_count;
    SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_RECORDS','SA_ROUNDING_RULE_DETAIL',NULL);
    close c_get_total_records;

        FOR i in 1..O_sa_rr_detail_del_rec.count()
        loop
            if O_sa_rr_detail_del_rec(i).rounding_rule_id=rec.rounding_rule_id then
                l_del_count:=l_del_count+1;
                if l_del_count>=l_dtl_count then
                  --fetch the row_seq
                  l_row_seq:=null;
                    SQL_LIB.SET_MARK('OPEN','C_GET_ROW_SEQ','SVC_SA_ROUNDING_RULE_DETAIL',NULL);
                    open c_get_row_seq(O_sa_rr_detail_del_rec(i).rounding_rule_id,O_sa_rr_detail_del_rec(i).low_ending_amt,O_sa_rr_detail_del_rec(i).high_ending_amt);
                    SQL_LIB.SET_MARK('OPEN','C_GET_ROW_SEQ','SVC_SA_ROUNDING_RULE_DETAIL',NULL);
                    fetch c_get_row_seq into l_row_seq;
                    SQL_LIB.SET_MARK('OPEN','C_GET_ROW_SEQ','SVC_SA_ROUNDING_RULE_DETAIL',NULL);
                    close c_get_row_seq;
                    
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,'SA_ROUNDING_RULE_DETAIL',l_row_seq,NULL,'DETAIL_REC');
                    O_sa_rr_detail_del_rec.delete(i);    
                end if; 
            end if; 
        end loop;
   end loop;

end check_detail;
-------------------------------------------------------------------------------
---  Name: validate_sequence
---  Purpose: Check if the high/low ending amt sequence is maintained
--             This is with assumption that user would have entered the data in excel rows sequenctially
-------------------------------------------------------------------------------- 

PROCEDURE validate_sequence( O_error OUT rtk_errors.rtk_text%type,I_rec c_svc_SA_ROUNDING_RULE_DETAIL%rowtype )
is

cursor c_get_detail_rec is
select * from 
((select r.rounding_rule_id,r.low_ending_amt,r.high_ending_amt   
    from sa_rounding_rule_detail r
  where r.rounding_rule_id=I_rec.rounding_rule_id
and not exists(select 1 from svc_sa_rounding_rule_detail
                             where  process_id=I_rec.process_id  
                                and chunk_id=1 
                                and row_seq<I_rec.row_seq
              and rounding_rule_id=I_rec.rounding_rule_id
              and r.low_ending_amt=low_ending_amt
              and r.high_ending_amt=high_ending_amt
              and process$status='IP'
              and action=stg_svc_SA_ROUNDING_RULE.action_del)
union all
 select  rounding_rule_id,low_ending_amt,high_ending_amt   
    from svc_sa_rounding_rule_detail where  process_id=I_rec.process_id  
     and chunk_id=1 
 and rounding_rule_id=I_rec.rounding_rule_id
     and row_seq<I_rec.row_seq
 and process$status='IP'
     and action=stg_svc_SA_ROUNDING_RULE.action_new 
 union all
 select  rounding_rule_id,low_ending_amt,high_ending_amt   
    from svc_sa_rounding_rule_detail where  process_id=I_rec.process_id  
     and chunk_id=1 
 and rounding_rule_id=I_rec.rounding_rule_id
     and row_seq=I_rec.row_seq
     and action=stg_svc_SA_ROUNDING_RULE.action_new) 
 minus
 (select  rounding_rule_id,low_ending_amt,high_ending_amt  
 from svc_sa_rounding_rule_detail
                             where  process_id=I_rec.process_id  
                                and chunk_id=1 
              and rounding_rule_id=I_rec.rounding_rule_id
                                and row_seq=I_rec.row_seq
                                and action=stg_svc_SA_ROUNDING_RULE.action_del))
order by low_ending_amt ;

l_prev_high sa_rounding_rule_detail.high_ending_amt%type;
l_count number(8);
BEGIN
    l_count:=0;
    FOR rec in  c_get_detail_rec
    LOOP
         l_count:=l_count+1;
         if l_count<>1 and rec.low_ending_amt<>(l_prev_high+0.0001)  then
            O_error:=SQL_LIB.CREATE_MSG('NEXT_LOW_RANGE_VALUE', to_char(l_prev_high+0.0001));
            exit;
         end if;
         l_prev_high:=rec.high_ending_amt;
    end LOOP;
end validate_sequence;
-------------------------------------------------------------------------------
---  Name: process_SA_ROUNDING_RULE_HEAD
---  Purpose: Load data to staging table SVC_SA_ROUNDING_RULE_HEAD
-------------------------------------------------------------------------------- 
PROCEDURE process_SA_ROUNDING_RULE_HEAD(
 I_process_id IN SVC_SA_ROUNDING_RULE_HEAD.PROCESS_ID%TYPE,
 I_chunk_id   IN SVC_SA_ROUNDING_RULE_HEAD.CHUNK_ID%TYPE,
 O_sa_rr_head_ins_rec IN OUT SA_RR_HEAD_rec_tab, 
 O_sa_rr_head_upd_rec IN OUT SA_RR_HEAD_rec_tab,
 O_sa_rr_head_del_rec IN OUT SA_RR_HEAD_rec_tab)
is
l_error BOOLEAN;
SA_ROUNDING_RULE_HEAD_temp_rec SA_ROUNDING_RULE_HEAD%rowtype;
l_table VARCHAR2(255)    :='SA_ROUNDING_RULE_HEAD';
L_desc         CURRENCIES.CURRENCY_DESC%TYPE;
L_currency_dec       currencies.currency_rtl_dec%TYPE  := NULL; 
L_currency_cost_fmt  currencies.currency_cost_fmt%TYPE := NULL;
L_currency_cost_dec  currencies.currency_cost_dec%TYPE := NULL;
L_error_message      VARCHAR2(255)                     := NULL;
L_currency_rtl_fmt      CURRENCIES.CURRENCY_RTL_FMT%TYPE;

   CURSOR C_country_EXISTS(p_country_id country.country_id%type)
   is
    select 'Y'
     from country
     where country_id = p_country_id;
 
   cursor c_chk_uk( p_country_id country.country_id%type ,p_currency_code currencies.currency_code%type,p_row_seq svc_sa_rounding_rule_head.row_seq%type)
   is
   select 'Y' from svc_sa_rounding_rule_head
    where nvl(country_id,'dummy')=nvl(p_country_id,'dummy' )
      and currency_code=p_currency_code
      and process$status='IP' and row_seq<p_row_seq and action<>stg_svc_SA_ROUNDING_RULE.action_del
    union
   select 'Y' from sa_rounding_rule_head
    where nvl(country_id,'dummy')=nvl(p_country_id,'dummy' )
      and currency_code=p_currency_code;

   L_exists VARCHAR2(1) := 'N';
   L_uk_exists VARCHAR2(1) := 'N';
BEGIN
    FOR rec IN c_svc_SA_RR_HEAD(I_process_id,I_chunk_id)
    LOOP
        l_error       := False ;
        if rec.action is NULL then
            WRITE_ERROR(I_process_id, svc_admin_upld_er_seq.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'ACTION', 'FIELD_NOT_NULL');
            L_error := TRUE;
        end if;
        if rec.action is NOT NULL and rec.action NOT IN (stg_svc_SA_ROUNDING_RULE.action_new,stg_svc_SA_ROUNDING_RULE.action_mod,stg_svc_SA_ROUNDING_RULE.action_del) THEN
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ACTION','INV_ACT');
            l_error :=true;
        end if;
        if rec.action = stg_svc_SA_ROUNDING_RULE.action_new and rec.PK_SA_ROUNDING_RULE_HEAD_rid is NOT NULL THEN
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUNDING_RULE_ID','REC_EXIST');
            l_error :=true;
        end if;
        if rec.action IN (stg_svc_SA_ROUNDING_RULE.action_mod,stg_svc_SA_ROUNDING_RULE.action_del) and rec.PK_SA_ROUNDING_RULE_HEAD_rid is NULL THEN
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUNDING_RULE_ID','RR_ID_NOT_EXIST');
            l_error :=true;
        end if;
  
        if rec.action in (stg_svc_SA_ROUNDING_RULE.action_new)   AND rec.PK_SA_ROUNDING_RULE_HEAD_rid is  NULL THEN
    if  rec.UK_SA_ROUNDING_RULE_HEAD_rid is NOT NULL then
      write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'ROUNDING_RULE_EXISTS');
      l_error :=true;
       
    else
    l_uk_exists:='N';
      SQL_LIB.SET_MARK('OPEN','C_CHK_UK','SVC_SA_ROUNDING_RULE_HEAD',NULL);
      open c_chk_uk(rec.country_id,rec.currency_code,rec.row_seq);
      SQL_LIB.SET_MARK('FETCH','C_CHK_UK','SVC_SA_ROUNDING_RULE_HEAD',NULL);
      fetch c_chk_uk into l_uk_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHK_UK','SVC_SA_ROUNDING_RULE_HEAD',NULL);
      close c_chk_uk;
      
      if nvl(l_uk_exists,'N')='Y' then
      
        write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'ROUNDING_RULE_EXISTS');
        l_error :=true;
      end if;
      end if;
        end if;
  if rec.action in (stg_svc_SA_ROUNDING_RULE.action_mod)   AND rec.PK_SA_ROUNDING_RULE_HEAD_rid is NOT NULL and  nvl(rec.prev_country_id,'dummy')<>nvl(rec.country_id,'dummy') and rec.prev_currency_code<>rec.currency_code then
     if rec.UK_SA_ROUNDING_RULE_HEAD_rid is not null THEN
       
        write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'ROUNDING_RULE_EXISTS');
        l_error :=true;
     else
      l_uk_exists:='N';
      SQL_LIB.SET_MARK('OPEN','C_CHK_UK','SVC_SA_ROUNDING_RULE_HEAD',NULL);
      open c_chk_uk(rec.country_id,rec.currency_code,rec.row_seq);
      SQL_LIB.SET_MARK('FETCH','C_CHK_UK','SVC_SA_ROUNDING_RULE_HEAD',NULL);
      fetch c_chk_uk into l_uk_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHK_UK','SVC_SA_ROUNDING_RULE_HEAD',NULL);
      close c_chk_uk;
      
      if nvl(l_uk_exists,'N')='Y' then
      
        write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'ROUNDING_RULE_EXISTS');
        l_error :=true;
      end if;
     end if;
  end if;
        if NOT(  rec.ROUNDING_RULE_ID  is NOT NULL ) THEN
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUNDING_RULE_ID','ROUNDING_RULE_ID_REQ');
            l_error :=true;
        end if;
        if rec.action<>stg_svc_SA_ROUNDING_RULE.action_del then
            if NOT(  rec.ROUNDING_RULE_NAME  is NOT NULL ) THEN
                 write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUNDING_RULE_NAME','ENTER_DESC');
                 l_error :=true;
            end if;
            if NOT(  rec.CURRENCY_CODE  is NOT NULL ) THEN
                 write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'CURRENCY_CODE','CURR_CODE_REQUIRED');
                 l_error :=true;
            end if;
            if NOT(  rec.STATUS  is NOT NULL ) THEN
                 write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STATUS','ROUNDING_RULE_STATUS_REQ');
                 l_error :=true;
            end if;
            if rec.STATUS  not in ('I','A') THEN
                 write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STATUS','INV_VALUE');
                 l_error :=true;
            end if;
        end if;
        ----additional validations
        if rec.action in (stg_svc_SA_ROUNDING_RULE.action_new ,stg_svc_SA_ROUNDING_RULE.action_mod)then
        ---VALIDATE_CURRENCY---
            if rec.currency_code is not null then
                if CURRENCY_SQL.GET_NAME(L_error_message,
                                            rec.currency_code,
                                            L_desc) = FALSE then
                  write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'CURRENCY_CODE',L_error_message);
                  l_error :=true;
                elsif CURRENCY_SQL.GET_FORMAT(L_error_message,
                                              rec.currency_code,
                                             L_currency_rtl_fmt,
                                              L_currency_dec,
                                              L_currency_cost_fmt,
                                              L_currency_cost_dec) = FALSE then
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'CURRENCY_CODE',L_error_message);
                    l_error :=true; 
                end if;
            end if;
        ---VALIDATE_COUNTRY---
            if rec.country_id is not NULL then
                SQL_LIB.SET_MARK('OPEN','C_COUNTRY_EXISTS','COUNTRY',NULL);
                open C_country_EXISTS(rec.country_id);
                SQL_LIB.SET_MARK('FETCH','C_COUNTRY_EXISTS','COUNTRY',NULL);
                fetch C_country_EXISTS into L_exists;
                SQL_LIB.SET_MARK('CLOSE','C_COUNTRY_EXISTS','COUNTRY',NULL);
                close C_country_EXISTS;
                 
                if nvl(L_exists,'N')='N' then
                  
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'COUNTRY_ID','INV_COUNTRY');
                    l_error :=true;
                end if;
            end if;
         --validate date
            if rec.start_business_date is not null and rec.start_business_date<GET_VDATE  
               and rec.start_business_date <> nvl(rec.prev_date,rec.start_business_date+1) then
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'START_BUSINESS_DATE','DATE_S_NOT_BEFORE_TODAY');
                l_error :=true;
            end if;
        end if;


        if NOT l_error THEN
            --update process to IP FOR the valid records.This is used in the check_detail PROCEDURE
            update svc_sa_rounding_rule_head 
                set process$status='IP' 
              where process_id=rec.process_id 
                 and chunk_id=I_chunk_id 
                 and row_seq=rec.row_seq;
          
            SA_ROUNDING_RULE_HEAD_temp_rec.ROUNDING_RULE_ID              := rec.ROUNDING_RULE_ID;
            SA_ROUNDING_RULE_HEAD_temp_rec.ROUNDING_RULE_NAME              := rec.ROUNDING_RULE_NAME;
            SA_ROUNDING_RULE_HEAD_temp_rec.CURRENCY_CODE              := rec.CURRENCY_CODE;
            SA_ROUNDING_RULE_HEAD_temp_rec.COUNTRY_ID              := rec.COUNTRY_ID;
            SA_ROUNDING_RULE_HEAD_temp_rec.START_BUSINESS_DATE              := rec.START_BUSINESS_DATE;
            SA_ROUNDING_RULE_HEAD_temp_rec.STATUS              := rec.STATUS;
            if rec.action                    = stg_svc_SA_ROUNDING_RULE.action_new THEN
              O_sa_rr_head_ins_rec.extend;
              O_sa_rr_head_ins_rec(O_sa_rr_head_ins_rec.count()):=SA_ROUNDING_RULE_HEAD_temp_rec;
            end if;
            if rec.action = stg_svc_SA_ROUNDING_RULE.action_mod THEN
              O_sa_rr_head_upd_rec.extend;
              O_sa_rr_head_upd_rec(O_sa_rr_head_upd_rec.count()):=SA_ROUNDING_RULE_HEAD_temp_rec;
            end if;
            if rec.action = stg_svc_SA_ROUNDING_RULE.action_del THEN
              O_sa_rr_head_del_rec.extend;
              O_sa_rr_head_del_rec(O_sa_rr_head_del_rec.count()):=SA_ROUNDING_RULE_HEAD_temp_rec;
            end if;
        end if;
    end LOOP;

end process_SA_ROUNDING_RULE_HEAD;
-------------------------------------------------------------------------------
---  Name: PROCESS_SA_RR_HEAD_TL
---  Purpose: Validate and load data from staging table SVC_SA_ROUNDING_RULE_HEAD_TL
---           to SA_ROUNDING_RULE_HEAD_TL
-------------------------------------------------------------------------------- 

FUNCTION PROCESS_SA_RR_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_SA_ROUNDING_RULE_HEAD_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_SA_ROUNDING_RULE_HEAD_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                      VARCHAR2(64) := 'CORESVC_SA_ROUNDING_RULE_HEAD.PROCESS_SA_RR_HEAD_TL';
   L_table                        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SA_ROUNDING_RULE_HEAD_TL';
   L_trans_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ROUNDING_RULE_HEAD_TL';
   L_parent_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ROUNDING_RULE_HEAD';
   L_error                        BOOLEAN;
   L_process_error                BOOLEAN := FALSE;
   L_error_message                RTK_ERRORS.RTK_TEXT%TYPE;
   L_sa_rr_head_tl_rec            SA_ROUNDING_RULE_HEAD_TL%ROWTYPE;
   L_sa_rr_head_tl_upd_rst        ROW_SEQ_TAB;
   L_sa_rr_head_tl_del_rst        ROW_SEQ_TAB;

   cursor C_SVC_SA_RR_HEAD_TL(I_process_id NUMBER,
                              I_chunk_id NUMBER) is
      select pk_sa_rounding_rule_head_tl.rowid  as pk_sa_rr_head_tl_rid,
             fk_sa_rounding_rule_head.rowid     as fk_sa_rr_head_rid,
             fk_lang.rowid                      as fk_lang_rid,
             st.lang,
             st.rounding_rule_id,
             st.rounding_rule_name,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             upper(st.action)        as action,
             st.process$status
        from svc_sa_rounding_rule_head_tl  st,
             sa_rounding_rule_head         fk_sa_rounding_rule_head,
             sa_rounding_rule_head_tl      pk_sa_rounding_rule_head_tl,
             lang                          fk_lang
       where st.process_id  =  i_process_id
         and st.chunk_id    =  i_chunk_id
         and st.rounding_rule_id  =  fk_sa_rounding_rule_head.rounding_rule_id (+)
         and st.lang              =  pk_sa_rounding_rule_head_tl.lang (+)
         and st.rounding_rule_id  =  pk_sa_rounding_rule_head_tl.rounding_rule_id (+)
         and st.lang              =  fk_lang.lang (+);
         
   TYPE SVC_SA_RR_HEAD_TL_TAB is TABLE OF C_SVC_SA_RR_HEAD_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_sa_rr_head_tab        SVC_SA_RR_HEAD_TL_TAB;
   
   L_svc_sa_rr_head_tl_ins_tab         SA_RR_HEAD_TL_REC_TAB         := NEW SA_RR_HEAD_TL_REC_TAB();
   L_svc_sa_rr_head_tl_upd_tab         SA_RR_HEAD_TL_REC_TAB         := NEW SA_RR_HEAD_TL_REC_TAB();
   L_svc_sa_rr_head_tl_del_tab         SA_RR_HEAD_TL_REC_TAB         := NEW SA_RR_HEAD_TL_REC_TAB();

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   if C_SVC_SA_RR_HEAD_TL%ISOPEN then
      close C_SVC_SA_RR_HEAD_TL;
   end if;
   
   open C_SVC_SA_RR_HEAD_TL(I_process_id,
                            I_chunk_id);
   LOOP 
      fetch C_SVC_SA_RR_HEAD_TL bulk collect into L_svc_sa_rr_head_tab limit LP_bulk_fetch_limit;
      if L_svc_sa_rr_head_tab.COUNT > 0 then
         FOR i in L_svc_sa_rr_head_tab.FIRST..L_svc_sa_rr_head_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_sa_rr_head_tab(i).lang = LP_primary_lang and L_svc_sa_rr_head_tab(i).action = stg_svc_sa_rounding_rule.action_new then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_rr_head_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if action is valid
            if L_svc_sa_rr_head_tab(i).action is NULL
               or L_svc_sa_rr_head_tab(i).action NOT IN (stg_svc_sa_rounding_rule.action_new, stg_svc_sa_rounding_rule.action_mod, stg_svc_sa_rounding_rule.action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_rr_head_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_sa_rr_head_tab(i).action = stg_svc_sa_rounding_rule.action_new 
               and L_svc_sa_rr_head_tab(i).pk_sa_rr_head_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_rr_head_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_sa_rr_head_tab(i).action IN (stg_svc_sa_rounding_rule.action_mod, stg_svc_sa_rounding_rule.action_del) 
               and L_svc_sa_rr_head_tab(i).lang is NOT NULL
               and L_svc_sa_rr_head_tab(i).rounding_rule_id is NOT NULL 
               and L_svc_sa_rr_head_tab(i).pk_sa_rr_head_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_sa_rr_head_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_sa_rr_head_tab(i).action = stg_svc_sa_rounding_rule.action_new 
               and L_svc_sa_rr_head_tab(i).rounding_rule_id is NOT NULL
               and L_svc_sa_rr_head_tab(i).fk_sa_rr_head_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_parent_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_sa_rr_head_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_sa_rr_head_tab(i).action = stg_svc_sa_rounding_rule.action_new 
               and L_svc_sa_rr_head_tab(i).lang is NOT NULL
               and L_svc_sa_rr_head_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_rr_head_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_sa_rr_head_tab(i).action in (stg_svc_sa_rounding_rule.action_new, stg_svc_sa_rounding_rule.action_mod) then
               if L_svc_sa_rr_head_tab(i).rounding_rule_name is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_sa_rr_head_tab(i).row_seq,
                              'ROUNDING_RULE_NAME',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_sa_rr_head_tab(i).rounding_rule_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_rr_head_tab(i).row_seq,
                           'ROUNDING_RULE_ID',
                           'FIELD_NOT_NULL');
               L_error :=TRUE;
            end if;

            if L_svc_sa_rr_head_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_rr_head_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_sa_rr_head_tl_rec.lang := L_svc_sa_rr_head_tab(i).lang;
               L_sa_rr_head_tl_rec.rounding_rule_id := L_svc_sa_rr_head_tab(i).rounding_rule_id;
               L_sa_rr_head_tl_rec.rounding_rule_name := L_svc_sa_rr_head_tab(i).rounding_rule_name;
               L_sa_rr_head_tl_rec.create_datetime := SYSDATE;
               L_sa_rr_head_tl_rec.create_id := GET_USER;
               L_sa_rr_head_tl_rec.last_update_datetime := SYSDATE;
               L_sa_rr_head_tl_rec.last_update_id := GET_USER;
            
               if L_svc_sa_rr_head_tab(i).action = stg_svc_sa_rounding_rule.action_new then
                  L_svc_sa_rr_head_tl_ins_tab.extend;
                  L_svc_sa_rr_head_tl_ins_tab(L_svc_sa_rr_head_tl_ins_tab.count()) := L_sa_rr_head_tl_rec;
               end if; 

               if L_svc_sa_rr_head_tab(i).action = stg_svc_sa_rounding_rule.action_mod then
                  L_svc_sa_rr_head_tl_upd_tab.extend;
                  L_svc_sa_rr_head_tl_upd_tab(L_svc_sa_rr_head_tl_upd_tab.count()) := L_sa_rr_head_tl_rec;
                  L_sa_rr_head_tl_upd_rst(L_svc_sa_rr_head_tl_upd_tab.count()) := L_svc_sa_rr_head_tab(i).row_seq;
               end if; 

               if L_svc_sa_rr_head_tab(i).action = stg_svc_sa_rounding_rule.action_del then
                  L_svc_sa_rr_head_tl_del_tab.extend;
                  L_svc_sa_rr_head_tl_del_tab(L_svc_sa_rr_head_tl_del_tab.count()) := L_sa_rr_head_tl_rec;
                  L_sa_rr_head_tl_del_rst(L_svc_sa_rr_head_tl_del_tab.count()) := L_svc_sa_rr_head_tab(i).row_seq;
               end if; 
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_SA_RR_HEAD_TL%NOTFOUND;
   END LOOP;
   close C_SVC_SA_RR_HEAD_TL;

   EXEC_SA_RR_HEAD_TL_INS(L_svc_sa_rr_head_tl_ins_tab);
   
   if EXEC_SA_RR_HEAD_TL_UPD(O_error_message,
                             L_svc_sa_rr_head_tl_upd_tab,
                             L_sa_rr_head_tl_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_SA_RR_HEAD_TL_DEL(O_error_message,
                             L_svc_sa_rr_head_tl_del_tab,
                             L_sa_rr_head_tl_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SA_RR_HEAD_TL;
-------------------------------------------------------------------------------
---  Name: exec_SA_RR_DETAIL_ins
--- Purpose: inserts into SA_ROUNDING_RULE_DETAIL
-------------------------------------------------------------------------------- 
PROCEDURE exec_SA_RR_DETAIL_ins
(
 I_ins_tab IN SA_RR_DETAIL_rec_tab
)
is
BEGIN

    FORall i IN I_ins_tab.first..I_ins_tab.last
    insert INTO SA_ROUNDING_RULE_DETAIL VALUES I_ins_tab
     ( i
     );
end exec_SA_RR_DETAIL_ins;
-------------------------------------------------------------------------------
---  Name: exec_SA_RR_DETAIL_ins
---  Purpose: deletes from  SA_ROUNDING_RULE_DETAIL
-------------------------------------------------------------------------------- 
PROCEDURE exec_SA_RR_DETAIL_del(
 I_del_tab IN SA_RR_DETAIL_rec_tab )
is
BEGIN

   IF I_del_tab     IS NOT NULL AND I_del_tab.count>0 THEN
    FORALL i IN INDICES OF I_del_tab
     
        delete from SA_ROUNDING_RULE_DETAIL 
              where 1 = 1
                 and ROUNDING_RULE_ID = I_del_tab(i).ROUNDING_RULE_ID
                  and low_ending_amt=I_del_tab(i).low_ending_amt
                  and high_ending_amt=I_del_tab(i).high_ending_amt
        ;
         
END IF;
end exec_SA_RR_DETAIL_del;
-------------------------------------------------------------------------------
---  Name: process_SA_RR_DETAIL
---  Purpose: deletes from  SA_ROUNDING_RULE_DETAIL
-------------------------------------------------------------------------------
PROCEDURE process_SA_RR_DETAIL(
     I_process_id IN SVC_SA_ROUNDING_RULE_DETAIL.PROCESS_ID%TYPE,
     I_chunk_id   IN SVC_SA_ROUNDING_RULE_DETAIL.CHUNK_ID%TYPE,
     O_sa_rr_detail_ins_rec IN OUT SA_RR_DETAIL_rec_tab,
     O_sa_rr_detail_upd_rec IN OUT SA_RR_DETAIL_rec_tab,
     O_sa_rr_detail_del_rec IN  OUT SA_RR_DETAIL_rec_tab)
    is
    l_error BOOLEAN;
    SA_RR_DETAIL_temp_rec SA_ROUNDING_RULE_DETAIL%rowtype;
    SA_RR_DETAIL_ins_rec SA_RR_DETAIL_rec_tab:=NEW SA_RR_DETAIL_rec_tab();
    l_table VARCHAR2(255)    :='SA_ROUNDING_RULE_DETAIL';
    t_RR_dtl_temp_tab RR_dtl_temp_tab:=NEW RR_dtl_temp_tab();
    l_error_msg rtk_errors.rtk_text%type;
    cursor c_chk_head(p_rule_id svc_sa_rounding_rule_head.rounding_rule_id%type)
    is
    select 'Y' from svc_sa_rounding_rule_head 
    where process_id=I_process_id
    and chunk_id=I_chunk_id
    and rounding_rule_id=p_rule_id
    and process$status='IP';
    l_rr_head_exists varchar2(1);
    BEGIN
        FOR rec IN c_svc_SA_ROUNDING_RULE_DETAIL(I_process_id,I_chunk_id)
        LOOP
            l_error       := False;
    if rec.action is NULL then
     WRITE_ERROR(I_process_id, svc_admin_upld_er_seq.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'ACTION', 'FIELD_NOT_NULL');
     L_error := TRUE;
   end if;
            if rec.action is NOT NULL and rec.action NOT IN (stg_svc_SA_ROUNDING_RULE.action_new,stg_svc_SA_ROUNDING_RULE.action_del) THEN
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ACTION','INV_ACT');
                l_error :=true;
            end if;
            if rec.SRD_SRH_FK_rid is NULL  then
      
                    l_rr_head_exists:=null;
                      --check if present in current upload
                    SQL_LIB.SET_MARK('OPEN','C_CHK_HEAD','SVC_SA_ROUNDING_RULE_HEAD',NULL);
                    open c_chk_head(rec.rounding_rule_id);
                    SQL_LIB.SET_MARK('FETCH','C_CHK_HEAD','SVC_SA_ROUNDING_RULE_HEAD',NULL);
                    fetch c_chk_head into l_rr_head_exists;
                    SQL_LIB.SET_MARK('CLOSE','C_CHK_HEAD','SVC_SA_ROUNDING_RULE_HEAD',NULL);
                    close c_chk_head;
                    
                    if nvl(l_rr_head_exists,'N')='N' then
                        write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUNDING_RULE_ID','RR_ID_NOT_EXIST');
                        l_error :=true; 
                    end if;
      
            end if;

            if rec.action = stg_svc_SA_ROUNDING_RULE.action_new and rec.UK_SA_ROUNDING_RULE_DETAIL_rid is NOT NULL THEN

                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'UK_SA_RR_DETAIL');
                l_error :=true;
            end if;
                if rec.action =stg_svc_SA_ROUNDING_RULE.action_del and rec.UK_SA_ROUNDING_RULE_DETAIL_rid is NULL THEN
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'UK_SA_RR_DETAIL_MISSING');
                l_error :=true;
            end if;
            if NOT(  rec.ROUNDING_RULE_ID  is NOT NULL ) THEN
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUNDING_RULE_ID','VALUES_FOR_RANGE');
                l_error :=true;
            end if;
    
            if NOT(  rec.LOW_ENDING_AMT  is NOT NULL ) THEN
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'LOW_ENDING_AMT','VALUES_FOR_RANGE');
                l_error :=true;
            elsif rec.action=stg_svc_SA_ROUNDING_RULE.action_new then
      
              if  rec.LOW_ENDING_AMT <0 then
                 write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'LOW_ENDING_AMT','NOT_NEG');
                 l_error :=true;
              elsif  rec.LOW_ENDING_AMT >1 then
                 write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'LOW_ENDING_AMT',SQL_LIB.CREATE_MSG('EXCEEDS_HIGHEST', '1'));
                 l_error :=true;
              elsif  length(substr(rec.LOW_ENDING_AMT, instr(rec.LOW_ENDING_AMT,'.',1)+1)) >4 then-- only upto 4 desimal places allowed
                 write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'LOW_ENDING_AMT',sql_lib.create_msg('FRMT_GENERIC','9999999999999999D9999'));
                 l_error :=true;
              end if;
            end if;
            if NOT(  rec.HIGH_ENDING_AMT  is NOT NULL ) THEN
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'HIGH_ENDING_AMT','VALUES_FOR_RANGE');
                l_error :=true;
            elsif rec.action=stg_svc_SA_ROUNDING_RULE.action_new then
                if  rec.HIGH_ENDING_AMT <0 then
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'HIGH_ENDING_AMT','NOT_NEG');
                    l_error :=true;
                elsif  rec.HIGH_ENDING_AMT >1 then
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'HIGH_ENDING_AMT',SQL_LIB.CREATE_MSG('EXCEEDS_HIGHEST', '1'));
                    l_error :=true;
                elsif  length(substr(rec.HIGH_ENDING_AMT, instr(rec.HIGH_ENDING_AMT,'.',1)+1)) >4 then-- only upto 4 desimal places allowed
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'HIGH_ENDING_AMT',sql_lib.create_msg('FRMT_GENERIC','9999999999999999D9999'));
                    l_error :=true;
                end if;

            end if;

            if rec.action =stg_svc_SA_ROUNDING_RULE.action_new then
           if NOT(  rec.ROUND_AMT  is NOT NULL )  THEN
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUND_AMT','VALUES_FOR_RANGE');
                    l_error :=true;
               elsif  rec.ROUND_AMT <0 then
                   write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUND_AMT','NOT_NEG');
                   l_error :=true;
                elsif  rec.ROUND_AMT >1 then
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUND_AMT',SQL_LIB.CREATE_MSG('EXCEEDS_HIGHEST', '1'));
                    l_error :=true;
                elsif  length(substr(rec.ROUND_AMT, instr(rec.ROUND_AMT,'.',1)+1)) >2 then-- only upto 2 desimal places allowed
                    write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUND_AMT',sql_lib.create_msg('FRMT_GENERIC','9999999999999D99'));
                    l_error :=true;
                end if;
            end if;
      
            ---additional validations
            if rec.action=stg_svc_SA_ROUNDING_RULE.action_new then
                if rec.HIGH_ENDING_AMT is not null and rec.LOW_ENDING_AMT is not null then
                     if rec.high_ending_amt <= rec.low_ending_amt then
                         write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,null,'CURR_RANGE_VALUE');
                         l_error :=true;
                     end if;
                     
                     if rec.round_amt is not null then
                          if rec.round_amt < rec.low_ending_amt or rec.round_amt > rec.high_ending_amt then
                          write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROUND_AMT','ROUNDING_RULE_OUT_RANGE');
                         l_error :=true;
                         end if;
                     end if;
                 
                end if;


            end if;

            ---- check if the present record breaks the sequence(excel records are processed in order of entry here)
            if NOT l_error then
               validate_sequence(l_error_msg,rec);
               if l_error_msg is not null then
                   write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,null,l_error_msg);
                   l_error :=true;
               end if;
            end if;
            -------------------------------------------------------------------------
            if NOT l_error THEN

            update svc_sa_rounding_rule_detail 
            set process$status='IP' 
            where process_id=rec.process_id 
            and chunk_id=I_chunk_id 
            and row_seq=rec.row_seq;

            SA_RR_DETAIL_temp_rec.ROUNDING_RULE_ID              := rec.ROUNDING_RULE_ID;
            SA_RR_DETAIL_temp_rec.LOW_ENDING_AMT              := rec.LOW_ENDING_AMT;
            SA_RR_DETAIL_temp_rec.HIGH_ENDING_AMT              := rec.HIGH_ENDING_AMT;
            SA_RR_DETAIL_temp_rec.ROUND_AMT              := rec.ROUND_AMT;

            t_RR_dtl_temp_tab.extend;
            t_RR_dtl_temp_tab(t_RR_dtl_temp_tab.count()).action:=rec.action;
            t_RR_dtl_temp_tab(t_RR_dtl_temp_tab.count()).rounding_rule_id:=rec.rounding_rule_id;
            t_RR_dtl_temp_tab(t_RR_dtl_temp_tab.count()).LOW_ENDING_AMT:=rec.LOW_ENDING_AMT;
            t_RR_dtl_temp_tab(t_RR_dtl_temp_tab.count()).HIGH_ENDING_AMT:=rec.HIGH_ENDING_AMT;
            t_RR_dtl_temp_tab(t_RR_dtl_temp_tab.count()).ROUND_AMT:=rec.ROUND_AMT;
            t_RR_dtl_temp_tab(t_RR_dtl_temp_tab.count()).PROCESS$STATUS:='N';

            -----------------------------
            if rec.action                    = stg_svc_SA_ROUNDING_RULE.action_new THEN
              O_sa_rr_detail_ins_rec.extend;
              O_sa_rr_detail_ins_rec(O_sa_rr_detail_ins_rec.count()):=SA_RR_DETAIL_temp_rec;
            end if;
            if rec.action = stg_svc_SA_ROUNDING_RULE.action_del THEN
              O_sa_rr_detail_del_rec.extend;
              O_sa_rr_detail_del_rec(O_sa_rr_detail_del_rec.count()):=SA_RR_DETAIL_temp_rec;
            end if;
     end if;
   end LOOP;
end process_SA_RR_DETAIL;
-------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_TABLE(I_process_id IN SVC_PROCESS_TRACKER.PROCESS_ID%TYPE) IS
BEGIN
   delete
     from svc_sa_rounding_rule_detail
    where process_id = I_process_id;
   
   delete
     from svc_sa_rounding_rule_head_tl
    where process_id = I_process_id;
   
   delete
     from svc_sa_rounding_rule_head
    where process_id = I_process_id;

END CLEAR_STAGING_TABLE;
-------------------------------------------------------------------------------
--Name: process
--Purpose: this is called from UI to load data from stg to core table
-------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_process_id    IN     NUMBER,
                 O_error_count   OUT    NUMBER) RETURN BOOLEAN IS
                 
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';
   
   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
         
   L_program VARCHAR2(255):='SA_ROUNDING_RULE.PROCESS';
   O_sa_rr_head_ins_rec     SA_RR_HEAD_REC_TAB;--:=NEW SA_RR_HEAD_rec_tab();
   O_sa_rr_head_upd_rec     SA_RR_HEAD_REC_TAB;--:=NEW SA_RR_HEAD_rec_tab();
   O_sa_rr_head_del_rec     SA_RR_HEAD_REC_TAB;--:=NEW SA_RR_HEAD_rec_tab();
   O_sa_rr_detail_ins_rec   SA_RR_DETAIL_REC_TAB := NEW SA_RR_DETAIL_REC_TAB();
   O_sa_rr_detail_upd_rec   SA_RR_DETAIL_REC_TAB := NEW SA_RR_DETAIL_REC_TAB();
   O_sa_rr_detail_del_rec   SA_RR_DETAIL_REC_TAB := NEW SA_RR_DETAIL_REC_TAB();
   L_chunk_id               NUMBER := 1;
   L_process_status         SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   
BEGIN
    LP_primary_lang      := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
    O_sa_rr_head_ins_rec := NEW SA_RR_HEAD_REC_TAB();
    O_sa_rr_head_upd_rec := NEW SA_RR_HEAD_REC_TAB();
    O_sa_rr_head_del_rec := NEW SA_RR_HEAD_REC_TAB();
    Lp_errors_tab        := NEW ERRORS_TAB_TYP();
    
    PROCESS_SA_ROUNDING_RULE_HEAD(I_process_id,
                                  L_chunk_id,
                                  O_sa_rr_head_ins_rec,
                                  O_sa_rr_head_upd_rec,
                                  O_sa_rr_head_del_rec);
    PROCESS_SA_RR_DETAIL(I_process_id,
                         L_chunk_id,
                         O_sa_rr_detail_ins_rec,
                         O_sa_rr_detail_upd_rec,
                         O_sa_rr_detail_del_rec); 
    --verify if detail record is present FOR the  head records
    CHECK_DETAIL(O_sa_rr_head_ins_rec,
                 O_sa_rr_detail_del_rec,
                 I_process_id,
                 L_chunk_id);

    EXEC_SA_ROUNDING_RULE_HEAD_INS(O_sa_rr_head_ins_rec);
    EXEC_SA_ROUNDING_RULE_HEAD_UPD(O_sa_rr_head_upd_rec);
    EXEC_SA_ROUNDING_RULE_HEAD_DEL(O_sa_rr_head_del_rec);
    EXEC_SA_RR_DETAIL_INS(O_sa_rr_detail_ins_rec);
    EXEC_SA_RR_DETAIL_DEL(O_sa_rr_detail_del_rec);
    
    if PROCESS_SA_RR_HEAD_TL(O_error_message,
                             I_process_id,
                             L_chunk_id) = FALSE then
       return FALSE;
    end if;

    O_error_count := Lp_errors_tab.count();
    FORALL i IN 1..O_error_count
        insert into svc_admin_upld_er 
             values Lp_errors_tab(i);
    Lp_errors_tab := NEW errors_tab_typ();
    
    open  C_GET_ERR_COUNT;
    fetch C_GET_ERR_COUNT into L_err_count;
    close C_GET_ERR_COUNT;
    
    open  C_GET_WARN_COUNT;
    fetch C_GET_WARN_COUNT into L_warn_count;
    close C_GET_WARN_COUNT;
    
    if L_err_count is NOT NULL then
       L_process_status := 'PE';
    elsif L_warn_count is NOT NULL then
       L_process_status := 'PW';
    else
       L_process_status := 'PS';
    end if;
   
    update svc_process_tracker
       set status = (CASE
                        when status = 'PE'
                        then 'PE'
                        else L_process_status
                     END),
           action_date =sysdate
     where process_id = I_process_id;
     
    CLEAR_STAGING_TABLE(I_process_id);
    COMMIT;
    return TRUE;
     
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_TABLE(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-------------------------------------------------------------------------------
end coresvc_sa_rounding_rule;
/ 