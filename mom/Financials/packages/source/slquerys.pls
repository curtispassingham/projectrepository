CREATE OR REPLACE PACKAGE STKLEDGR_QUERY_SQL AUTHID CURRENT_USER AS
   v_purch_cost                  NUMBER(20,4)   := 0;
   v_freight_cost                NUMBER(20,4)   := 0;
   v_rtv_cost                    NUMBER(20,4)   := 0;
   v_net_sales_cost              NUMBER(20,4)   := 0;
   v_cls_stk_cost                NUMBER(20,4)   := 0;
   v_purch_retail                NUMBER(20,4)   := 0;
   v_rtv_retail                  NUMBER(20,4)   := 0;
   v_markup_retail               NUMBER(20,4)   := 0;
   v_markup_can_retail           NUMBER(20,4)   := 0;
   v_clear_markdown_retail       NUMBER(20,4)   := 0;
   v_perm_markdown_retail        NUMBER(20,4)   := 0;
   v_prom_markdown_retail        NUMBER(20,4)   := 0;
   v_weight_variance_retail      NUMBER(20,4)   := 0;
   v_markdown_can_retail         NUMBER(20,4)   := 0;
   v_net_sales_retail            NUMBER(20,4)   := 0;
   v_net_sales_retail_ex_vat     NUMBER(20,4)   := 0;
   v_cls_stk_retail              NUMBER(20,4)   := 0;
   v_empl_disc_retail            NUMBER(20,4)   := 0;
   v_stock_adj_cost              NUMBER(20,4)   := 0;
   v_stock_adj_retail            NUMBER(20,4)   := 0;
   v_stock_adj_cogs_cost         NUMBER(20,4)   := 0;
   v_stock_adj_cogs_retail       NUMBER(20,4)   := 0;
   v_up_chrg_amt_profit          NUMBER(20,4)   := 0;
   v_up_chrg_amt_exp             NUMBER(20,4)   := 0;
   v_tsf_in_cost                 NUMBER(20,4)   := 0;
   v_tsf_in_retail               NUMBER(20,4)   := 0;
   v_tsf_in_book_cost            NUMBER(20,4)   := 0;
   v_tsf_in_book_retail          NUMBER(20,4)   := 0;
   v_tsf_out_cost                NUMBER(20,4)   := 0;
   v_tsf_out_retail              NUMBER(20,4)   := 0;
   v_tsf_out_book_cost           NUMBER(20,4)   := 0;
   v_tsf_out_book_retail         NUMBER(20,4)   := 0;
   v_reclass_in_cost             NUMBER(20,4)   := 0;
   v_reclass_in_retail           NUMBER(20,4)   := 0;
   v_reclass_out_cost            NUMBER(20,4)   := 0;
   v_reclass_out_retail          NUMBER(20,4)   := 0;
   v_returns_cost                NUMBER(20,4)   := 0;
   v_returns_retail              NUMBER(20,4)   := 0;
   v_shrinkage_cost              NUMBER(20,4)   := 0;
   v_shrinkage_retail            NUMBER(20,4)   := 0;
   v_workroom_amt                NUMBER(20,4)   := 0;
   v_cash_disc_amt               NUMBER(20,4)   := 0;
   v_gross_margin_amt            NUMBER(20,4)   := 0;
   v_sales_units                 NUMBER(20,4)   := 0;
   v_cost_variance_amt           NUMBER(20,4)   := 0;
   v_htd_gafs_cost               NUMBER(20,4)   := 0;
   v_htd_gafs_retail             NUMBER(20,4)   := 0;
   v_freight_claim_cost          NUMBER(20,4)   := 0;
   v_freight_claim_retail        NUMBER(20,4)   := 0;
   v_eom_date                    DATE           := NULL;
   v_eow_date                    DATE           := NULL;
   v_intercompany_in_retail      NUMBER(20,4)   := 0;
   v_intercompany_in_cost        NUMBER(20,4)   := 0;
   v_intercompany_out_retail     NUMBER(20,4)   := 0;
   v_intercompany_out_cost       NUMBER(20,4)   := 0;
   v_intercompany_markup         NUMBER(20,4)   := 0;
   v_intercompany_markdown       NUMBER(20,4)   := 0;
   v_wo_activity_upd_inv         NUMBER(20,4)   := 0;
   v_wo_activity_post_fin        NUMBER(20,4)   := 0;
   v_deal_income_sales           NUMBER(20,4)   := 0;
   v_deal_income_purch           NUMBER(20,4)   := 0;
   v_restocking_fee              NUMBER(20,4)   := 0;
   v_margin_cost_variance        NUMBER(20,4)   := 0;
   v_retail_cost_variance        NUMBER(20,4)   := 0;
   v_rec_cost_adj_variance       NUMBER(20,4)   := 0;
   v_franchise_sales_cost        NUMBER(20,4)   := 0;
   v_franchise_returns_cost      NUMBER(20,4)   := 0;
   v_franchise_returns_retail    NUMBER(20,4)   := 0;
   v_franchise_markdown_retail   NUMBER(20,4)   := 0;
   v_franchise_sales_retail      NUMBER(20,4)   := 0;
   v_franchise_markup_retail     NUMBER(20,4)   := 0;
   v_franchise_restocking_fee    NUMBER(20,4)   := 0;
   v_intercompany_margin         NUMBER(20,4)   := 0;
   v_net_sales_non_inv_retail    NUMBER(20,4)   := 0;
   v_net_sales_non_inv_cost      NUMBER(20,4)   := 0;
   v_net_sales_non_inv_rtl_ex_vat    NUMBER(20,4)   := 0;

-- record definition for week data pl/sql table
TYPE week_data_rec is RECORD(half_no                   WEEK_DATA.HALF_NO%TYPE,
                             week_no                   WEEK_DATA.WEEK_NO%TYPE,
                             eow_date                  WEEK_DATA.EOW_DATE%TYPE,
                             opn_stk_cost              WEEK_DATA.OPN_STK_COST%TYPE,
                             opn_stk_retail            WEEK_DATA.OPN_STK_RETAIL%TYPE,
                             purch_cost                WEEK_DATA.PURCH_COST%TYPE,
                             freight_cost              WEEK_DATA.FREIGHT_COST%TYPE,
                             rtv_cost                  WEEK_DATA.RTV_COST%TYPE,
                             net_sales_cost            WEEK_DATA.NET_SALES_COST%TYPE,
                             cls_stk_cost              WEEK_DATA.CLS_STK_COST%TYPE,
                             purch_retail              WEEK_DATA.PURCH_RETAIL%TYPE,
                             rtv_retail                WEEK_DATA.RTV_RETAIL%TYPE,
                             markup_retail             WEEK_DATA.MARKUP_RETAIL%TYPE,
                             markup_can_retail         WEEK_DATA.MARKUP_CAN_RETAIL%TYPE,
                             clear_markdown_retail     WEEK_DATA.CLEAR_MARKDOWN_RETAIL%TYPE,
                             perm_markdown_retail      WEEK_DATA.PERM_MARKDOWN_RETAIL%TYPE,
                             prom_markdown_retail      WEEK_DATA.PROM_MARKDOWN_RETAIL%TYPE,
                             weight_variance_retail    WEEK_DATA.WEIGHT_VARIANCE_RETAIL%TYPE,
                             markdown_can_retail       WEEK_DATA.MARKDOWN_CAN_RETAIL%TYPE,
                             net_sales_retail          WEEK_DATA.NET_SALES_RETAIL%TYPE,
                             net_sales_retail_ex_vat   WEEK_DATA.NET_SALES_RETAIL_EX_VAT%TYPE,
                             cls_stk_retail            WEEK_DATA.CLS_STK_RETAIL%TYPE,
                             empl_disc_retail          WEEK_DATA.EMPL_DISC_RETAIL%TYPE,
                             stock_adj_cost            WEEK_DATA.STOCK_ADJ_COST%TYPE,
                             stock_adj_retail          WEEK_DATA.STOCK_ADJ_RETAIL%TYPE,
                             stock_adj_cogs_cost       WEEK_DATA.STOCK_ADJ_COGS_COST%TYPE,
                             stock_adj_cogs_retail     WEEK_DATA.STOCK_ADJ_COGS_RETAIL%TYPE,
                             up_chrg_amt_profit        WEEK_DATA.UP_CHRG_AMT_PROFIT%TYPE,
                             up_chrg_amt_exp           WEEK_DATA.UP_CHRG_AMT_EXP%TYPE,
                             tsf_in_cost               WEEK_DATA.TSF_IN_COST%TYPE,
                             tsf_in_retail             WEEK_DATA.TSF_IN_RETAIL%TYPE,
                             tsf_in_book_cost          WEEK_DATA.TSF_IN_BOOK_COST%TYPE,
                             tsf_in_book_retail        WEEK_DATA.TSF_IN_BOOK_RETAIL%TYPE,
                             tsf_out_cost              WEEK_DATA.TSF_OUT_COST%TYPE,
                             tsf_out_retail            WEEK_DATA.TSF_OUT_RETAIL%TYPE,
                             tsf_out_book_cost         WEEK_DATA.TSF_OUT_BOOK_COST%TYPE,
                             tsf_out_book_retail       WEEK_DATA.TSF_OUT_BOOK_RETAIL%TYPE,
                             reclass_in_cost           WEEK_DATA.RECLASS_IN_COST%TYPE,
                             reclass_in_retail         WEEK_DATA.RECLASS_IN_RETAIL%TYPE,
                             reclass_out_cost          WEEK_DATA.RECLASS_OUT_COST%TYPE,
                             reclass_out_retail        WEEK_DATA.RECLASS_OUT_RETAIL%TYPE,
                             returns_cost              WEEK_DATA.RETURNS_COST%TYPE,
                             returns_retail            WEEK_DATA.RETURNS_RETAIL%TYPE,
                             shrinkage_cost            WEEK_DATA.SHRINKAGE_COST%TYPE,
                             shrinkage_retail          WEEK_DATA.SHRINKAGE_RETAIL%TYPE,
                             workroom_amt              WEEK_DATA.WORKROOM_AMT%TYPE,
                             cash_disc_amt             WEEK_DATA.CASH_DISC_AMT%TYPE,
                             gross_margin_amt          WEEK_DATA.GROSS_MARGIN_AMT%TYPE,
                             sales_units               WEEK_DATA.SALES_UNITS%TYPE,
                             cost_variance_amt         WEEK_DATA.COST_VARIANCE_AMT%TYPE,
                             htd_gafs_cost             WEEK_DATA.HTD_GAFS_COST%TYPE,
                             htd_gafs_retail           WEEK_DATA.HTD_GAFS_RETAIL%TYPE,
                             freight_claim_cost        WEEK_DATA.FREIGHT_CLAIM_COST%TYPE,
                             freight_claim_retail      WEEK_DATA.FREIGHT_CLAIM_RETAIL%TYPE,
                             intercompany_in_retail    WEEK_DATA.INTERCOMPANY_IN_RETAIL%TYPE,
                             intercompany_in_cost      WEEK_DATA.INTERCOMPANY_IN_COST%TYPE,
                             intercompany_out_retail   WEEK_DATA.INTERCOMPANY_OUT_RETAIL%TYPE,
                             intercompany_out_cost     WEEK_DATA.INTERCOMPANY_OUT_COST%TYPE,
                             intercompany_markup       WEEK_DATA.INTERCOMPANY_MARKUP%TYPE,
                             intercompany_markdown     WEEK_DATA.INTERCOMPANY_MARKDOWN%TYPE,
                             wo_activity_upd_inv       WEEK_DATA.WO_ACTIVITY_UPD_INV%TYPE,
                             wo_activity_post_fin      WEEK_DATA.WO_ACTIVITY_POST_FIN%TYPE,
                             deal_income_sales         WEEK_DATA.DEAL_INCOME_SALES%TYPE,
                             deal_income_purch         WEEK_DATA.DEAL_INCOME_PURCH%TYPE,
                             restocking_fee            WEEK_DATA.RESTOCKING_FEE%TYPE,
                             retail_cost_variance      WEEK_DATA.RETAIL_COST_VARIANCE%TYPE,
                             margin_cost_variance      WEEK_DATA.MARGIN_COST_VARIANCE%TYPE,
                             rec_cost_adj_variance     WEEK_DATA.REC_COST_ADJ_VARIANCE%TYPE,
                             franchise_sales_retail    WEEK_DATA.FRANCHISE_SALES_RETAIL%TYPE,
                             franchise_sales_cost      WEEK_DATA.FRANCHISE_SALES_COST%TYPE,
                             franchise_returns_retail  WEEK_DATA.FRANCHISE_RETURNS_RETAIL%TYPE,
                             franchise_returns_cost    WEEK_DATA.FRANCHISE_RETURNS_COST%TYPE,
                             franchise_markup_retail   WEEK_DATA.FRANCHISE_MARKUP_RETAIL%TYPE,
                             franchise_markdown_retail WEEK_DATA.FRANCHISE_MARKDOWN_RETAIL%TYPE,
                             franchise_restocking_fee  WEEK_DATA.FRANCHISE_RESTOCKING_FEE%TYPE,
                             vat_in                    WEEK_DATA.VAT_IN%TYPE,
                             vat_out                   WEEK_DATA.VAT_IN%TYPE,
                             recoverable_tax           WEEK_DATA.RECOVERABLE_TAX%TYPE,
                             intercompany_margin       WEEK_DATA.INTERCOMPANY_MARGIN%TYPE,
                             net_sales_non_inv_retail  WEEK_DATA.NET_SALES_NON_INV_RETAIL%TYPE,
                             net_sales_non_inv_cost    WEEK_DATA.NET_SALES_NON_INV_COST%TYPE,
                             net_sales_non_inv_rtl_ex_vat WEEK_DATA.NET_SALES_NON_INV_RTL_EX_VAT%TYPE);

-- table type declaration for week data pl/sql table
TYPE week_data_tbl_type is TABLE of week_data_rec INDEX BY BINARY_INTEGER;
week_data_tbl   week_data_tbl_type;

-- record definition for week data primary currency pl/sql table
TYPE week_data_rec_primary is RECORD(half_no                   WEEK_DATA.HALF_NO%TYPE,
                                     week_no                   WEEK_DATA.WEEK_NO%TYPE,
                                     eow_date                  WEEK_DATA.EOW_DATE%TYPE,
                                     opn_stk_cost              WEEK_DATA.OPN_STK_COST%TYPE,
                                     opn_stk_retail            WEEK_DATA.OPN_STK_RETAIL%TYPE,
                                     purch_cost                WEEK_DATA.PURCH_COST%TYPE,
                                     freight_cost              WEEK_DATA.FREIGHT_COST%TYPE,
                                     rtv_cost                  WEEK_DATA.RTV_COST%TYPE,
                                     net_sales_cost            WEEK_DATA.NET_SALES_COST%TYPE,
                                     cls_stk_cost              WEEK_DATA.CLS_STK_COST%TYPE,
                                     purch_retail              WEEK_DATA.PURCH_RETAIL%TYPE,
                                     rtv_retail                WEEK_DATA.RTV_RETAIL%TYPE,
                                     markup_retail             WEEK_DATA.MARKUP_RETAIL%TYPE,
                                     markup_can_retail         WEEK_DATA.MARKUP_CAN_RETAIL%TYPE,
                                     clear_markdown_retail     WEEK_DATA.CLEAR_MARKDOWN_RETAIL%TYPE,
                                     perm_markdown_retail      WEEK_DATA.PERM_MARKDOWN_RETAIL%TYPE,
                                     prom_markdown_retail      WEEK_DATA.PROM_MARKDOWN_RETAIL%TYPE,
                                     weight_variance_retail    WEEK_DATA.WEIGHT_VARIANCE_RETAIL%TYPE,
                                     markdown_can_retail       WEEK_DATA.MARKDOWN_CAN_RETAIL%TYPE,
                                     net_sales_retail          WEEK_DATA.NET_SALES_RETAIL%TYPE,
                                     net_sales_retail_ex_vat   WEEK_DATA.NET_SALES_RETAIL_EX_VAT%TYPE,
                                     cls_stk_retail            WEEK_DATA.CLS_STK_RETAIL%TYPE,
                                     empl_disc_retail          WEEK_DATA.EMPL_DISC_RETAIL%TYPE,
                                     stock_adj_cost            WEEK_DATA.STOCK_ADJ_COST%TYPE,
                                     stock_adj_retail          WEEK_DATA.STOCK_ADJ_RETAIL%TYPE,
                                     stock_adj_cogs_cost       WEEK_DATA.STOCK_ADJ_COGS_COST%TYPE,
                                     stock_adj_cogs_retail     WEEK_DATA.STOCK_ADJ_COGS_RETAIL%TYPE,
                                     up_chrg_amt_profit        WEEK_DATA.UP_CHRG_AMT_PROFIT%TYPE,
                                     up_chrg_amt_exp           WEEK_DATA.UP_CHRG_AMT_EXP%TYPE,
                                     tsf_in_cost               WEEK_DATA.TSF_IN_COST%TYPE,
                                     tsf_in_retail             WEEK_DATA.TSF_IN_RETAIL%TYPE,
                                     tsf_in_book_cost          WEEK_DATA.TSF_IN_BOOK_COST%TYPE,
                                     tsf_in_book_retail        WEEK_DATA.TSF_IN_BOOK_RETAIL%TYPE,
                                     tsf_out_cost              WEEK_DATA.TSF_OUT_COST%TYPE,
                                     tsf_out_retail            WEEK_DATA.TSF_OUT_RETAIL%TYPE,
                                     tsf_out_book_cost         WEEK_DATA.TSF_OUT_BOOK_COST%TYPE,
                                     tsf_out_book_retail       WEEK_DATA.TSF_OUT_BOOK_RETAIL%TYPE,
                                     reclass_in_cost           WEEK_DATA.RECLASS_IN_COST%TYPE,
                                     reclass_in_retail         WEEK_DATA.RECLASS_IN_RETAIL%TYPE,
                                     reclass_out_cost          WEEK_DATA.RECLASS_OUT_COST%TYPE,
                                     reclass_out_retail        WEEK_DATA.RECLASS_OUT_RETAIL%TYPE,
                                     returns_cost              WEEK_DATA.RETURNS_COST%TYPE,
                                     returns_retail            WEEK_DATA.RETURNS_RETAIL%TYPE,
                                     shrinkage_cost            WEEK_DATA.SHRINKAGE_COST%TYPE,
                                     shrinkage_retail          WEEK_DATA.SHRINKAGE_RETAIL%TYPE,
                                     workroom_amt              WEEK_DATA.WORKROOM_AMT%TYPE,
                                     cash_disc_amt             WEEK_DATA.CASH_DISC_AMT%TYPE,
                                     gross_margin_amt          WEEK_DATA.GROSS_MARGIN_AMT%TYPE,
                                     sales_units               WEEK_DATA.SALES_UNITS%TYPE,
                                     cost_variance_amt         WEEK_DATA.COST_VARIANCE_AMT%TYPE,
                                     htd_gafs_cost             WEEK_DATA.HTD_GAFS_COST%TYPE,
                                     htd_gafs_retail           WEEK_DATA.HTD_GAFS_RETAIL%TYPE,
                                     freight_claim_cost        WEEK_DATA.FREIGHT_CLAIM_COST%TYPE,
                                     freight_claim_retail      WEEK_DATA.FREIGHT_CLAIM_RETAIL%TYPE,
                                     intercompany_in_retail    WEEK_DATA.INTERCOMPANY_IN_RETAIL%TYPE,
                                     intercompany_in_cost      WEEK_DATA.INTERCOMPANY_IN_COST%TYPE,
                                     intercompany_out_retail   WEEK_DATA.INTERCOMPANY_OUT_RETAIL%TYPE,
                                     intercompany_out_cost     WEEK_DATA.INTERCOMPANY_OUT_COST%TYPE,
                                     intercompany_markup       WEEK_DATA.INTERCOMPANY_MARKUP%TYPE,
                                     intercompany_markdown     WEEK_DATA.INTERCOMPANY_MARKDOWN%TYPE,
                                     wo_activity_upd_inv       WEEK_DATA.WO_ACTIVITY_UPD_INV%TYPE,
                                     wo_activity_post_fin      WEEK_DATA.WO_ACTIVITY_POST_FIN%TYPE,
                                     deal_income_sales         WEEK_DATA.DEAL_INCOME_SALES%TYPE,
                                     deal_income_purch         WEEK_DATA.DEAL_INCOME_PURCH%TYPE,
                                     restocking_fee            WEEK_DATA.RESTOCKING_FEE%TYPE,
                                     retail_cost_variance      WEEK_DATA.RETAIL_COST_VARIANCE%TYPE,
                                     margin_cost_variance      WEEK_DATA.MARGIN_COST_VARIANCE%TYPE,
                                     rec_cost_adj_variance     WEEK_DATA.REC_COST_ADJ_VARIANCE%TYPE,
                                     opn_stk_cost_local        WEEK_DATA.OPN_STK_COST%TYPE,
                                     opn_stk_retail_local      WEEK_DATA.OPN_STK_RETAIL%TYPE,
                                     cls_stk_cost_local        WEEK_DATA.CLS_STK_COST%TYPE,
                                     cls_stk_retail_local      WEEK_DATA.CLS_STK_RETAIL%TYPE,
                                     shrinkage_cost_local      WEEK_DATA.SHRINKAGE_COST%TYPE,
                                     shrinkage_retail_local    WEEK_DATA.SHRINKAGE_RETAIL%TYPE,
                                     gross_margin_amt_local    WEEK_DATA.GROSS_MARGIN_AMT%TYPE,
                                     htd_gafs_cost_local       WEEK_DATA.HTD_GAFS_COST%TYPE,
                                     htd_gafs_retail_local     WEEK_DATA.HTD_GAFS_RETAIL%TYPE,
                                     franchise_sales_retail    WEEK_DATA.FRANCHISE_SALES_RETAIL%TYPE,
                                     franchise_sales_cost      WEEK_DATA.FRANCHISE_SALES_COST%TYPE,
                                     franchise_returns_retail  WEEK_DATA.FRANCHISE_RETURNS_RETAIL%TYPE,
                                     franchise_returns_cost    WEEK_DATA.FRANCHISE_RETURNS_COST%TYPE,
                                     franchise_markup_retail   WEEK_DATA.FRANCHISE_MARKUP_RETAIL%TYPE,
                                     franchise_markdown_retail WEEK_DATA.FRANCHISE_MARKDOWN_RETAIL%TYPE,
                                     franchise_restocking_fee  WEEK_DATA.FRANCHISE_RESTOCKING_FEE%TYPE,
                                     intercompany_margin       WEEK_DATA.INTERCOMPANY_MARGIN%TYPE,
                                     net_sales_non_inv_retail  WEEK_DATA.NET_SALES_NON_INV_RETAIL%TYPE,
                                     net_sales_non_inv_cost    WEEK_DATA.NET_SALES_NON_INV_COST%TYPE,
                                     net_sales_non_inv_rtl_ex_vat WEEK_DATA.NET_SALES_NON_INV_RTL_EX_VAT%TYPE);


-- table type declaration for week data primary currency pl/sql table
TYPE week_data_primary_tbl_type is TABLE of week_data_rec_primary INDEX BY BINARY_INTEGER;
week_data_primary_tbl   week_data_primary_tbl_type;

-- record definition for month data pl/sql table
TYPE month_data_rec is RECORD(half_no                   MONTH_DATA.HALF_NO%TYPE,
                              month_no                  MONTH_DATA.MONTH_NO%TYPE,
                              eom_date                  MONTH_DATA.EOM_DATE%TYPE,
                              opn_stk_cost              MONTH_DATA.OPN_STK_COST%TYPE,
                              opn_stk_retail            MONTH_DATA.OPN_STK_RETAIL%TYPE,
                              purch_cost                MONTH_DATA.PURCH_COST%TYPE,
                              freight_cost              MONTH_DATA.FREIGHT_COST%TYPE,
                              rtv_cost                  MONTH_DATA.RTV_COST%TYPE,
                              net_sales_cost            MONTH_DATA.NET_SALES_COST%TYPE,
                              cls_stk_cost              MONTH_DATA.CLS_STK_COST%TYPE,
                              purch_retail              MONTH_DATA.PURCH_RETAIL%TYPE,
                              rtv_retail                MONTH_DATA.RTV_RETAIL%TYPE,
                              markup_retail             MONTH_DATA.MARKUP_RETAIL%TYPE,
                              markup_can_retail         MONTH_DATA.MARKUP_CAN_RETAIL%TYPE,
                              clear_markdown_retail     MONTH_DATA.CLEAR_MARKDOWN_RETAIL%TYPE,
                              perm_markdown_retail      MONTH_DATA.PERM_MARKDOWN_RETAIL%TYPE,
                              prom_markdown_retail      MONTH_DATA.PROM_MARKDOWN_RETAIL%TYPE,
                              weight_variance_retail    MONTH_DATA.WEIGHT_VARIANCE_RETAIL%TYPE,
                              markdown_can_retail       MONTH_DATA.MARKDOWN_CAN_RETAIL%TYPE,
                              net_sales_retail          MONTH_DATA.NET_SALES_RETAIL%TYPE,
                              net_sales_retail_ex_vat   MONTH_DATA.NET_SALES_RETAIL_EX_VAT%TYPE,
                              cls_stk_retail            MONTH_DATA.CLS_STK_RETAIL%TYPE,
                              empl_disc_retail          MONTH_DATA.EMPL_DISC_RETAIL%TYPE,
                              stock_adj_cost            MONTH_DATA.STOCK_ADJ_COST%TYPE,
                              stock_adj_retail          MONTH_DATA.STOCK_ADJ_RETAIL%TYPE,
                              stock_adj_cogs_cost       MONTH_DATA.STOCK_ADJ_COGS_COST%TYPE,
                              stock_adj_cogs_retail     MONTH_DATA.STOCK_ADJ_COGS_RETAIL%TYPE,
                              up_chrg_amt_profit        MONTH_DATA.UP_CHRG_AMT_PROFIT%TYPE,
                              up_chrg_amt_exp           MONTH_DATA.UP_CHRG_AMT_EXP%TYPE,
                              tsf_in_cost               MONTH_DATA.TSF_IN_COST%TYPE,
                              tsf_in_retail             MONTH_DATA.TSF_IN_RETAIL%TYPE,
                              tsf_in_book_cost          MONTH_DATA.TSF_IN_BOOK_COST%TYPE,
                              tsf_in_book_retail        MONTH_DATA.TSF_IN_BOOK_RETAIL%TYPE,
                              tsf_out_cost              MONTH_DATA.TSF_OUT_COST%TYPE,
                              tsf_out_retail            MONTH_DATA.TSF_OUT_RETAIL%TYPE,
                              tsf_out_book_cost         MONTH_DATA.TSF_OUT_BOOK_COST%TYPE,
                              tsf_out_book_retail       MONTH_DATA.TSF_OUT_BOOK_RETAIL%TYPE,
                              reclass_in_cost           MONTH_DATA.RECLASS_IN_COST%TYPE,
                              reclass_in_retail         MONTH_DATA.RECLASS_IN_RETAIL%TYPE,
                              reclass_out_cost          MONTH_DATA.RECLASS_OUT_COST%TYPE,
                              reclass_out_retail        MONTH_DATA.RECLASS_OUT_RETAIL%TYPE,
                              returns_cost              MONTH_DATA.RETURNS_COST%TYPE,
                              returns_retail            MONTH_DATA.RETURNS_RETAIL%TYPE,
                              shrinkage_cost            MONTH_DATA.SHRINKAGE_COST%TYPE,
                              shrinkage_retail          MONTH_DATA.SHRINKAGE_RETAIL%TYPE,
                              workroom_amt              MONTH_DATA.WORKROOM_AMT%TYPE,
                              cash_disc_amt             MONTH_DATA.CASH_DISC_AMT%TYPE,
                              gross_margin_amt          MONTH_DATA.GROSS_MARGIN_AMT%TYPE,
                              sales_units               MONTH_DATA.SALES_UNITS%TYPE,
                              cost_variance_amt         MONTH_DATA.COST_VARIANCE_AMT%TYPE,
                              htd_gafs_cost             MONTH_DATA.HTD_GAFS_COST%TYPE,
                              htd_gafs_retail           MONTH_DATA.HTD_GAFS_RETAIL%TYPE,
                              freight_claim_cost        MONTH_DATA.FREIGHT_CLAIM_COST%TYPE,
                              freight_claim_retail      MONTH_DATA.FREIGHT_CLAIM_RETAIL%TYPE,
                              intercompany_in_retail    MONTH_DATA.INTERCOMPANY_IN_RETAIL%TYPE,
                              intercompany_in_cost      MONTH_DATA.INTERCOMPANY_IN_COST%TYPE,
                              intercompany_out_retail   MONTH_DATA.INTERCOMPANY_OUT_RETAIL%TYPE,
                              intercompany_out_cost     MONTH_DATA.INTERCOMPANY_OUT_COST%TYPE,
                              intercompany_markup       MONTH_DATA.INTERCOMPANY_MARKUP%TYPE,
                              intercompany_markdown     MONTH_DATA.INTERCOMPANY_MARKDOWN%TYPE,
                              wo_activity_upd_inv       MONTH_DATA.WO_ACTIVITY_UPD_INV%TYPE,
                              wo_activity_post_fin      MONTH_DATA.WO_ACTIVITY_POST_FIN%TYPE,
                              deal_income_sales         WEEK_DATA.DEAL_INCOME_SALES%TYPE,
                              deal_income_purch         WEEK_DATA.DEAL_INCOME_PURCH%TYPE,
                              restocking_fee            WEEK_DATA.RESTOCKING_FEE%TYPE,
                              retail_cost_variance      WEEK_DATA.RETAIL_COST_VARIANCE%TYPE,
                              margin_cost_variance      WEEK_DATA.MARGIN_COST_VARIANCE%TYPE,
                              rec_cost_adj_variance     WEEK_DATA.REC_COST_ADJ_VARIANCE%TYPE,
                              franchise_sales_retail    WEEK_DATA.FRANCHISE_SALES_RETAIL%TYPE,
                              franchise_sales_cost      WEEK_DATA.FRANCHISE_SALES_COST%TYPE,
                              franchise_returns_retail  WEEK_DATA.FRANCHISE_RETURNS_RETAIL%TYPE,
                              franchise_returns_cost    WEEK_DATA.FRANCHISE_RETURNS_COST%TYPE,
                              franchise_markup_retail   WEEK_DATA.FRANCHISE_MARKUP_RETAIL%TYPE,
                              franchise_markdown_retail WEEK_DATA.FRANCHISE_MARKDOWN_RETAIL%TYPE,
                              franchise_restocking_fee  WEEK_DATA.FRANCHISE_RESTOCKING_FEE%TYPE,
                              vat_in                    WEEK_DATA.VAT_IN%TYPE,
                              vat_out                   WEEK_DATA.VAT_IN%TYPE,
                              recoverable_tax           MONTH_DATA.RECOVERABLE_TAX%TYPE,
                              intercompany_margin       MONTH_DATA.INTERCOMPANY_MARGIN%TYPE,
                              net_sales_non_inv_retail  MONTH_DATA.NET_SALES_NON_INV_RETAIL%TYPE,
                              net_sales_non_inv_cost    MONTH_DATA.NET_SALES_NON_INV_COST%TYPE,
                              net_sales_non_inv_rtl_ex_vat MONTH_DATA.NET_SALES_NON_INV_RTL_EX_VAT%TYPE);

-- table type declaration for month data pl/sql table
TYPE month_data_tbl_type is TABLE of month_data_rec INDEX BY BINARY_INTEGER;
month_data_tbl   month_data_tbl_type;

-- record definition for calendar daily data pl/sql table
TYPE daily_data_cal_rec is RECORD(half_no                   DAILY_DATA.HALF_NO%TYPE,
                                  data_date                 DAILY_DATA.DATA_DATE%TYPE,
                                  day_no                    DAILY_DATA.DAY_NO%TYPE,
                                  purch_cost                DAILY_DATA.PURCH_COST%TYPE,
                                  freight_cost              DAILY_DATA.FREIGHT_COST%TYPE,
                                  rtv_cost                  DAILY_DATA.RTV_COST%TYPE,
                                  net_sales_cost            DAILY_DATA.NET_SALES_COST%TYPE,
                                  purch_retail              DAILY_DATA.PURCH_RETAIL%TYPE,
                                  rtv_retail                DAILY_DATA.RTV_RETAIL%TYPE,
                                  markup_retail             DAILY_DATA.MARKUP_RETAIL%TYPE,
                                  markup_can_retail         DAILY_DATA.MARKUP_CAN_RETAIL%TYPE,
                                  clear_markdown_retail     DAILY_DATA.CLEAR_MARKDOWN_RETAIL%TYPE,
                                  perm_markdown_retail      DAILY_DATA.PERM_MARKDOWN_RETAIL%TYPE,
                                  prom_markdown_retail      DAILY_DATA.PROM_MARKDOWN_RETAIL%TYPE,
                                  weight_variance_retail    DAILY_DATA.WEIGHT_VARIANCE_RETAIL%TYPE,
                                  markdown_can_retail       DAILY_DATA.MARKDOWN_CAN_RETAIL%TYPE,
                                  net_sales_retail          DAILY_DATA.NET_SALES_RETAIL%TYPE,
                                  net_sales_retail_ex_vat   DAILY_DATA.NET_SALES_RETAIL_EX_VAT%TYPE,
                                  empl_disc_retail          DAILY_DATA.EMPL_DISC_RETAIL%TYPE,
                                  stock_adj_cost            DAILY_DATA.STOCK_ADJ_COST%TYPE,
                                  stock_adj_retail          DAILY_DATA.STOCK_ADJ_RETAIL%TYPE,
                                  stock_adj_cogs_cost       DAILY_DATA.STOCK_ADJ_COGS_COST%TYPE,
                                  stock_adj_cogs_retail     DAILY_DATA.STOCK_ADJ_COGS_RETAIL%TYPE,
                                  up_chrg_amt_profit        DAILY_DATA.UP_CHRG_AMT_PROFIT%TYPE,
                                  up_chrg_amt_exp           DAILY_DATA.UP_CHRG_AMT_EXP%TYPE,
                                  tsf_in_cost               DAILY_DATA.TSF_IN_COST%TYPE,
                                  tsf_in_retail             DAILY_DATA.TSF_IN_RETAIL%TYPE,
                                  tsf_in_book_cost          DAILY_DATA.TSF_IN_BOOK_COST%TYPE,
                                  tsf_in_book_retail        DAILY_DATA.TSF_IN_BOOK_RETAIL%TYPE,
                                  tsf_out_cost              DAILY_DATA.TSF_OUT_COST%TYPE,
                                  tsf_out_retail            DAILY_DATA.TSF_OUT_RETAIL%TYPE,
                                  tsf_out_book_cost         DAILY_DATA.TSF_OUT_BOOK_COST%TYPE,
                                  tsf_out_book_retail       DAILY_DATA.TSF_OUT_BOOK_RETAIL%TYPE,
                                  reclass_in_cost           DAILY_DATA.RECLASS_IN_COST%TYPE,
                                  reclass_in_retail         DAILY_DATA.RECLASS_IN_RETAIL%TYPE,
                                  reclass_out_cost          DAILY_DATA.RECLASS_OUT_COST%TYPE,
                                  reclass_out_retail        DAILY_DATA.RECLASS_OUT_RETAIL%TYPE,
                                  returns_cost              DAILY_DATA.RETURNS_COST%TYPE,
                                  returns_retail            DAILY_DATA.RETURNS_RETAIL%TYPE,
                                  workroom_amt              DAILY_DATA.WORKROOM_AMT%TYPE,
                                  cash_disc_amt             DAILY_DATA.CASH_DISC_AMT%TYPE,
                                  sales_units               DAILY_DATA.SALES_UNITS%TYPE,
                                  cost_variance_amt         DAILY_DATA.COST_VARIANCE_AMT%TYPE,
                                  freight_claim_cost        DAILY_DATA.FREIGHT_CLAIM_COST%TYPE,
                                  freight_claim_retail      DAILY_DATA.FREIGHT_CLAIM_RETAIL%TYPE,
                                  intercompany_in_retail    DAILY_DATA.INTERCOMPANY_IN_RETAIL%TYPE,
                                  intercompany_in_cost      DAILY_DATA.INTERCOMPANY_IN_COST%TYPE,
                                  intercompany_out_retail   DAILY_DATA.INTERCOMPANY_OUT_RETAIL%TYPE,
                                  intercompany_out_cost     DAILY_DATA.INTERCOMPANY_OUT_COST%TYPE,
                                  intercompany_markup       DAILY_DATA.INTERCOMPANY_MARKUP%TYPE,
                                  intercompany_markdown     DAILY_DATA.INTERCOMPANY_MARKDOWN%TYPE,
                                  wo_activity_upd_inv       DAILY_DATA.WO_ACTIVITY_UPD_INV%TYPE,
                                  wo_activity_post_fin      DAILY_DATA.WO_ACTIVITY_POST_FIN%TYPE,
                                  deal_income_sales         WEEK_DATA.DEAL_INCOME_SALES%TYPE,
                                  deal_income_purch         WEEK_DATA.DEAL_INCOME_PURCH%TYPE,
                                  restocking_fee            WEEK_DATA.RESTOCKING_FEE%TYPE,
                                  retail_cost_variance      WEEK_DATA.RETAIL_COST_VARIANCE%TYPE,
                                  margin_cost_variance      WEEK_DATA.MARGIN_COST_VARIANCE%TYPE,
                                  rec_cost_adj_variance     WEEK_DATA.REC_COST_ADJ_VARIANCE%TYPE,
                                  franchise_sales_retail    WEEK_DATA.FRANCHISE_SALES_RETAIL%TYPE,
                                  franchise_sales_cost      WEEK_DATA.FRANCHISE_SALES_COST%TYPE,
                                  franchise_returns_retail  WEEK_DATA.FRANCHISE_RETURNS_RETAIL%TYPE,
                                  franchise_returns_cost    WEEK_DATA.FRANCHISE_RETURNS_COST%TYPE,
                                  franchise_markup_retail   WEEK_DATA.FRANCHISE_MARKUP_RETAIL%TYPE,
                                  franchise_markdown_retail WEEK_DATA.FRANCHISE_MARKDOWN_RETAIL%TYPE,
                                  franchise_restocking_fee  WEEK_DATA.FRANCHISE_RESTOCKING_FEE%TYPE,
                                  vat_in                    WEEK_DATA.VAT_IN%TYPE,
                                  vat_out                   WEEK_DATA.VAT_IN%TYPE,
                                  recoverable_tax           DAILY_DATA.RECOVERABLE_TAX%TYPE,
                                  net_sales_non_inv_retail  DAILY_DATA.NET_SALES_NON_INV_RETAIL%TYPE,
                                  net_sales_non_inv_cost    DAILY_DATA.NET_SALES_NON_INV_COST%TYPE,
                                  net_sales_non_inv_rtl_ex_vat DAILY_DATA.NET_SALES_NON_INV_RTL_EX_VAT%TYPE);

-- table type declaration for daily data pl/sql table
TYPE daily_data_cal_tbl_type is TABLE of daily_data_cal_rec INDEX BY BINARY_INTEGER;
daily_data_cal_tbl   daily_data_cal_tbl_type;

-- record definition for 454 daily data pl/sql table
TYPE daily_data_454_rec is RECORD(half_no                   DAILY_DATA.HALF_NO%TYPE,
                                  data_date                 DAILY_DATA.DATA_DATE%TYPE,
                                  day_no                    DAILY_DATA.DAY_NO%TYPE,
                                  purch_cost                DAILY_DATA.PURCH_COST%TYPE,
                                  freight_cost              DAILY_DATA.FREIGHT_COST%TYPE,
                                  rtv_cost                  DAILY_DATA.RTV_COST%TYPE,
                                  net_sales_cost            DAILY_DATA.NET_SALES_COST%TYPE,
                                  purch_retail              DAILY_DATA.PURCH_RETAIL%TYPE,
                                  rtv_retail                DAILY_DATA.RTV_RETAIL%TYPE,
                                  markup_retail             DAILY_DATA.MARKUP_RETAIL%TYPE,
                                  markup_can_retail         DAILY_DATA.MARKUP_CAN_RETAIL%TYPE,
                                  clear_markdown_retail     DAILY_DATA.CLEAR_MARKDOWN_RETAIL%TYPE,
                                  perm_markdown_retail      DAILY_DATA.PERM_MARKDOWN_RETAIL%TYPE,
                                  prom_markdown_retail      DAILY_DATA.PROM_MARKDOWN_RETAIL%TYPE,
                                  weight_variance_retail    DAILY_DATA.WEIGHT_VARIANCE_RETAIL%TYPE,
                                  markdown_can_retail       DAILY_DATA.MARKDOWN_CAN_RETAIL%TYPE,
                                  net_sales_retail          DAILY_DATA.NET_SALES_RETAIL%TYPE,
                                  net_sales_retail_ex_vat   DAILY_DATA.NET_SALES_RETAIL_EX_VAT%TYPE,
                                  empl_disc_retail          DAILY_DATA.EMPL_DISC_RETAIL%TYPE,
                                  stock_adj_cost            DAILY_DATA.STOCK_ADJ_COST%TYPE,
                                  stock_adj_retail          DAILY_DATA.STOCK_ADJ_RETAIL%TYPE,
                                  stock_adj_cogs_cost       DAILY_DATA.STOCK_ADJ_COGS_COST%TYPE,
                                  stock_adj_cogs_retail     DAILY_DATA.STOCK_ADJ_COGS_RETAIL%TYPE,
                                  up_chrg_amt_profit        DAILY_DATA.UP_CHRG_AMT_PROFIT%TYPE,
                                  up_chrg_amt_exp           DAILY_DATA.UP_CHRG_AMT_EXP%TYPE,
                                  tsf_in_cost               DAILY_DATA.TSF_IN_COST%TYPE,
                                  tsf_in_retail             DAILY_DATA.TSF_IN_RETAIL%TYPE,
                                  tsf_in_book_cost          DAILY_DATA.TSF_IN_BOOK_COST%TYPE,
                                  tsf_in_book_retail        DAILY_DATA.TSF_IN_BOOK_RETAIL%TYPE,
                                  tsf_out_cost              DAILY_DATA.TSF_OUT_COST%TYPE,
                                  tsf_out_retail            DAILY_DATA.TSF_OUT_RETAIL%TYPE,
                                  tsf_out_book_cost         DAILY_DATA.TSF_OUT_BOOK_COST%TYPE,
                                  tsf_out_book_retail       DAILY_DATA.TSF_OUT_BOOK_RETAIL%TYPE,
                                  reclass_in_cost           DAILY_DATA.RECLASS_IN_COST%TYPE,
                                  reclass_in_retail         DAILY_DATA.RECLASS_IN_RETAIL%TYPE,
                                  reclass_out_cost          DAILY_DATA.RECLASS_OUT_COST%TYPE,
                                  reclass_out_retail        DAILY_DATA.RECLASS_OUT_RETAIL%TYPE,
                                  returns_cost              DAILY_DATA.RETURNS_COST%TYPE,
                                  returns_retail            DAILY_DATA.RETURNS_RETAIL%TYPE,
                                  workroom_amt              DAILY_DATA.WORKROOM_AMT%TYPE,
                                  cash_disc_amt             DAILY_DATA.CASH_DISC_AMT%TYPE,
                                  sales_units               DAILY_DATA.SALES_UNITS%TYPE,
                                  cost_variance_amt         DAILY_DATA.COST_VARIANCE_AMT%TYPE,
                                  freight_claim_cost        DAILY_DATA.FREIGHT_CLAIM_COST%TYPE,
                                  freight_claim_retail      DAILY_DATA.FREIGHT_CLAIM_RETAIL%TYPE,
                                  intercompany_in_retail    DAILY_DATA.INTERCOMPANY_IN_RETAIL%TYPE,
                                  intercompany_in_cost      DAILY_DATA.INTERCOMPANY_IN_COST%TYPE,
                                  intercompany_out_retail   DAILY_DATA.INTERCOMPANY_OUT_RETAIL%TYPE,
                                  intercompany_out_cost     DAILY_DATA.INTERCOMPANY_OUT_COST%TYPE,
                                  intercompany_markup       DAILY_DATA.INTERCOMPANY_MARKUP%TYPE,
                                  intercompany_markdown     DAILY_DATA.INTERCOMPANY_MARKDOWN%TYPE,
                                  wo_activity_upd_inv       DAILY_DATA.WO_ACTIVITY_UPD_INV%TYPE,
                                  wo_activity_post_fin      DAILY_DATA.WO_ACTIVITY_POST_FIN%TYPE,
                                  deal_income_sales         WEEK_DATA.DEAL_INCOME_SALES%TYPE,
                                  deal_income_purch         WEEK_DATA.DEAL_INCOME_PURCH%TYPE,
                                  restocking_fee            WEEK_DATA.RESTOCKING_FEE%TYPE,
                                  retail_cost_variance      WEEK_DATA.RETAIL_COST_VARIANCE%TYPE,
                                  margin_cost_variance      WEEK_DATA.MARGIN_COST_VARIANCE%TYPE,
                                  rec_cost_adj_variance     WEEK_DATA.REC_COST_ADJ_VARIANCE%TYPE,
                                  franchise_sales_retail    WEEK_DATA.FRANCHISE_SALES_RETAIL%TYPE,
                                  franchise_sales_cost      WEEK_DATA.FRANCHISE_SALES_COST%TYPE,
                                  franchise_returns_retail  WEEK_DATA.FRANCHISE_RETURNS_RETAIL%TYPE,
                                  franchise_returns_cost    WEEK_DATA.FRANCHISE_RETURNS_COST%TYPE,
                                  franchise_markup_retail   WEEK_DATA.FRANCHISE_MARKUP_RETAIL%TYPE,
                                  franchise_markdown_retail WEEK_DATA.FRANCHISE_MARKDOWN_RETAIL%TYPE,
                                  franchise_restocking_fee  WEEK_DATA.FRANCHISE_RESTOCKING_FEE%TYPE,
                                  vat_in                    WEEK_DATA.VAT_IN%TYPE,
                                  vat_out                   WEEK_DATA.VAT_IN%TYPE,
                                  recoverable_tax           DAILY_DATA.RECOVERABLE_TAX%TYPE,
                                  net_sales_non_inv_retail  DAILY_DATA.NET_SALES_NON_INV_RETAIL%TYPE,
                                  net_sales_non_inv_cost    DAILY_DATA.NET_SALES_NON_INV_COST%TYPE,
                                  net_sales_non_inv_rtl_ex_vat DAILY_DATA.NET_SALES_NON_INV_RTL_EX_VAT%TYPE);

-- table type declaration for daily data pl/sql table
TYPE daily_data_454_tbl_type is TABLE of daily_data_454_rec INDEX BY BINARY_INTEGER;
daily_data_454_tbl   daily_data_454_tbl_type;

--------------------------------------------------------------------------------
-- Procedure: FETCH_MONTH_DATA_BULK
-- Called by: stkldgrv.fmb
-- Created by: Amy Selby
-- Created on: July 31, 1996
-- Purpose: To fetch month_data records and sum then to compute the
--          totals columns on the form stkldgrv.fmb.
--------------------------------------------------------------------------------
FUNCTION FETCH_MONTH_DATA_BULK(I_dept                       IN       NUMBER,
                               I_class                      IN       NUMBER,
                               I_subclass                   IN       NUMBER,
                               I_year_ind                   IN       VARCHAR2,
                               I_half                       IN       NUMBER,
                               I_loc_type                   IN       VARCHAR2,
                               I_location                   IN       NUMBER,
                               I_thousands                  IN       VARCHAR2,
                               I_currency_ind               IN       VARCHAR2,
                               I_set_of_books_id            IN       MONTH_DATA.SET_OF_BOOKS_ID%TYPE,
                               IO_month_data                IN OUT   OBJ_MONTH_DATA_TBL,
                               O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Procedure: FETCH_WEEK_DATA_BULK
-- Called by: stkldgrv.fmb
-- Created by: Amy Selby
-- Created on: July 31, 1996
-- Purpose: To fetch week_data records and sum then to compute the
--          totals columns on the form stkldgrv.fmb.
--------------------------------------------------------------------------------
FUNCTION FETCH_WEEK_DATA_BULK(I_dept                       IN       NUMBER,
                              I_class                      IN       NUMBER,
                              I_subclass                   IN       NUMBER,
                              I_year_ind                   IN       VARCHAR2,
                              I_month_date                 IN       DATE,
                              I_loc_type                   IN       VARCHAR2,
                              I_location                   IN       NUMBER,
                              I_thousands                  IN       VARCHAR2,
                              I_currency_ind               IN       VARCHAR2,
                              I_set_of_books_id            IN       WEEK_DATA.SET_OF_BOOKS_ID%TYPE,
                              IO_week_data                 IN OUT   OBJ_WEEK_DATA_TBL,
                              O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Procedure: FETCH_DAILY_DATA_CAL_BULK
-- Called by: stkldgrv.fmb
-- Created by: Amy Selby
-- Created on: July 31, 1996
-- Purpose: To fetch daily_data records and sum then to compute the
--          totals columns on the form stkldgrv.fmb.
--------------------------------------------------------------------------------
FUNCTION FETCH_DAILY_DATA_CAL_BULK(I_dept                       IN       NUMBER,
                                   I_class                      IN       NUMBER,
                                   I_subclass                   IN       NUMBER,
                                   I_year_ind                   IN       VARCHAR2,
                                   I_week_date                  IN       DATE,
                                   I_loc_type                   IN       VARCHAR2,
                                   I_location                   IN       NUMBER,
                                   I_thousands                  IN       VARCHAR2,
                                   I_currency_ind               IN       VARCHAR2,
                                   I_set_of_books_id            IN       DAILY_DATA.SET_OF_BOOKS_ID%TYPE,
                                   IO_daily_data_cal            IN OUT   OBJ_DAILY_DATA_TBL,
                                   O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Procedure: FETCH_DAILY_DATA_454_BULK
-- Called by: stkldgrv.fmb
-- Created by: Amy Selby
-- Created on: July 31, 1996
-- Purpose: To fetch daily_data records and sum then to compute the
--          totals columns on the form stkldgrv.fmb.
--------------------------------------------------------------------------------
FUNCTION FETCH_DAILY_DATA_454_BULK(I_dept                      IN     NUMBER,
                                   I_class                     IN     NUMBER,
                                   I_subclass                  IN     NUMBER,
                                   I_year_ind                  IN     VARCHAR2,
                                   I_week_date                 IN     DATE,
                                   I_loc_type                  IN     VARCHAR2,
                                   I_location                  IN     NUMBER,
                                   I_thousands                 IN     VARCHAR2,
                                   I_currency_ind              IN     VARCHAR2,
                                   I_set_of_books_id           IN     DAILY_DATA.SET_OF_BOOKS_ID%TYPE,
                                   IO_daily_data_454           IN OUT OBJ_DAILY_DATA_TBL,
                                   O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function: GET_CUM_MARKON_PCT
--  Purpose: This function retrieves the cumulative markon percentage for a
--           dept/class/subclass/loc/date. It checks the week_data first; if not
--           found, it checks the month_data. O_cum_markon_pct is defaulted to 0.
----------------------------------------------------------------------------------
FUNCTION GET_CUM_MARKON_PCT(O_error_message     IN OUT VARCHAR2,
                            O_cum_markon_pct    IN OUT week_data.cum_markon_pct%TYPE,
                            I_dept              IN     deps.dept%TYPE,
                            I_class             IN     class.class%TYPE,
                            I_subclass          IN     subclass.subclass%TYPE,
                            I_location          IN     item_loc.loc%TYPE,
                            I_loc_type          IN     item_loc.loc_type%TYPE,
                            I_tran_date         IN     DATE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END;
/
