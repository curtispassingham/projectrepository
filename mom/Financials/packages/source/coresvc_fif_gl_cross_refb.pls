create or replace PACKAGE BODY CORESVC_FIF_GL_CROSS_REF AS                                   
-----------------------------------------------------------------------
   cursor C_SVC_FIF_GL_CROSS_REF(I_process_id NUMBER,
                                 I_chunk_id NUMBER) is
      select uk_fif_gl_cross_ref.rowid                as uk_fif_gl_cross_ref_rid,
             uk_fif_gl_cross_ref.fif_gl_cross_ref_id  as uk_fif_gl_cross_ref_id,
             st.rowid                                 as st_rid,
             foc_tdc_fk.rowid                         as foc_tdc_fk_rid,
             foc_fltx_fk.rowid                        as foc_fltx_fk_rid,
             foc_vc_fk.rowid                          as foc_vc_fk_rid,
             foc_wact_fk.rowid                        as foc_wact_fk_rid,
             foc_fog_fk.rowid                         as foc_fog_fk_rid,
             foc_wact_fk.cost_type                    as foc_wact_fk_cost_type,
             st.set_of_books_id,
             st.dept,
             st.class,
             st.subclass,
             st.location,
             st.tran_code,
             UPPER(DECODE(st.tran_code,21,st.line_type,'ITEM'))  as LP_line_type,
             st.cost_Retail_flag,
             st.tran_ref_no,
             st.dr_ccid,
             st.dr_sequence1,
             st.dr_sequence2,
             st.dr_sequence3,
             st.dr_sequence4,
             st.dr_sequence5,
             st.dr_sequence6,
             st.dr_sequence7,
             st.dr_sequence8,
             st.dr_sequence9,
             st.dr_sequence10,
             st.cr_ccid,
             st.cr_sequence1,
             st.cr_sequence2,
             st.cr_sequence3,
             st.cr_sequence4,
             st.cr_sequence5,
             st.cr_sequence6,
             st.cr_sequence7,
             st.cr_sequence8,
             st.cr_sequence9,
             st.cr_sequence10,
             st.dr_sequence11,
             st.dr_sequence12,
             st.dr_sequence13,
             st.dr_sequence14,
             st.dr_sequence15,
             st.dr_sequence16,
             st.dr_sequence17,
             st.dr_sequence18,
             st.dr_sequence19,
             st.dr_sequence20,
             st.cr_sequence11,
             st.cr_sequence12,
             st.cr_sequence13,
             st.cr_sequence14,
             st.cr_sequence15,
             st.cr_sequence16,
             st.cr_sequence17,
             st.cr_sequence18,
             st.cr_sequence19,
             st.cr_sequence20,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action) as action,
             st.process$status
        from svc_fif_gl_cross_ref st,
             fif_gl_cross_ref          uk_fif_gl_cross_ref,
             tran_data_codes           foc_tdc_fk,
             fif_line_type_xref        foc_fltx_fk,
             vat_codes                 foc_vc_fk,
             wo_activity               foc_wact_fk,
             fif_gl_setup              foc_fog_fk
       where st.process_id                                          = I_process_id
             and st.chunk_id                                        = I_chunk_id
             and st.set_of_books_id                                 = uk_fif_gl_cross_ref.set_of_books_id (+)
             and st.dept                                            = uk_fif_gl_cross_ref.dept (+)
             and st.class                                           = uk_fif_gl_cross_ref.class (+)
             and st.subclass                                        = uk_fif_gl_cross_ref.subclass (+)
             and st.tran_code                                       = uk_fif_gl_cross_ref.tran_code (+)
             and st.location                                        = uk_fif_gl_cross_ref.location (+)
             and st.cost_retail_flag                                = uk_fif_gl_cross_ref.cost_retail_flag (+)
             and UPPER(DECODE(st.tran_code,21,st.line_type,'ITEM')) = uk_fif_gl_cross_ref.line_type (+)
             and nvl(st.tran_ref_no,-1)                             = nvl(uk_fif_gl_cross_ref.tran_ref_no(+),-1)
             and st.set_of_books_id                                 = foc_fog_fk.set_of_books_id (+)
             and st.tran_code                                       = foc_tdc_fk.code (+)
             and st.tran_ref_no                                     = to_char(foc_wact_fk.activity_id (+))
             and st.tran_ref_no                                     = foc_vc_fk.vat_code (+)
             and UPPER(DECODE(st.tran_code,21,st.line_type,'ITEM')) = foc_fltx_fk.rms_line_type (+);

   TYPE LP_errors_tab_typ     IS      TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   TYPE LP_column_type        IS      TABLE OF VARCHAR2(50) INDEX BY PLS_INTEGER;
   LP_dr_col_name                     LP_column_type;
   LP_cr_col_name                     LP_column_type;
   LP_errors_tab                      LP_errors_tab_typ;
   TYPE LP_s9t_errors_tab_typ IS      TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab                  LP_s9t_errors_tab_typ;
   LP_system_options_rec              SYSTEM_OPTIONS%ROWTYPE;
   LP_dr_acc_status                   VARCHAR2(1) := 'N';
   LP_cr_acc_status                   VARCHAR2(1) := 'N';
   LP_line_type SVC_FIF_GL_CROSS_REF.LINE_TYPE%TYPE;

----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;
END WRITE_S9T_ERROR;
----------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   L_fif_gl_cross_ref_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                                  :=s9t_pkg.get_sheet_names(I_file_id);
   L_fif_gl_cross_ref_cols                   :=s9t_pkg.get_col_names(I_file_id,FIF_GL_CROSS_REF_sheet);
   FIF_GL_CROSS_REF$Action                   := L_fif_gl_cross_ref_cols('ACTION');
   FIF_GL_CROSS_REF$SET_OF_BKS_ID            := L_fif_gl_cross_ref_cols('SET_OF_BOOKS_ID');
   FIF_GL_CROSS_REF$DEPT                     := L_fif_gl_cross_ref_cols('DEPT');
   FIF_GL_CROSS_REF$CLASS                    := L_fif_gl_cross_ref_cols('CLASS');
   FIF_GL_CROSS_REF$SUBCLASS                 := L_fif_gl_cross_ref_cols('SUBCLASS');
   FIF_GL_CROSS_REF$LOCATION                 := L_fif_gl_cross_ref_cols('LOCATION');
   FIF_GL_CROSS_REF$TRAN_CODE                := L_fif_gl_cross_ref_cols('TRAN_CODE');
   FIF_GL_CROSS_REF$CST_RTL_FLG              := L_fif_gl_cross_ref_cols('COST_RETAIL_FLAG');
   FIF_GL_CROSS_REF$LINE_TYPE                := L_fif_gl_cross_ref_cols('LINE_TYPE');
   FIF_GL_CROSS_REF$TRAN_REF_NO              := L_fif_gl_cross_ref_cols('TRAN_REF_NO');
   FIF_GL_CROSS_REF$DR_CCID                  := L_fif_gl_cross_ref_cols('DR_CCID');
   FIF_GL_CROSS_REF$DR_SEQUENCE1             := L_fif_gl_cross_ref_cols('DR_SEQUENCE1');
   FIF_GL_CROSS_REF$DR_SEQUENCE2             := L_fif_gl_cross_ref_cols('DR_SEQUENCE2');
   FIF_GL_CROSS_REF$DR_SEQUENCE3             := L_fif_gl_cross_ref_cols('DR_SEQUENCE3');
   FIF_GL_CROSS_REF$DR_SEQUENCE4             := L_fif_gl_cross_ref_cols('DR_SEQUENCE4');
   FIF_GL_CROSS_REF$DR_SEQUENCE5             := L_fif_gl_cross_ref_cols('DR_SEQUENCE5');
   FIF_GL_CROSS_REF$DR_SEQUENCE6             := L_fif_gl_cross_ref_cols('DR_SEQUENCE6');
   FIF_GL_CROSS_REF$DR_SEQUENCE7             := L_fif_gl_cross_ref_cols('DR_SEQUENCE7');
   FIF_GL_CROSS_REF$DR_SEQUENCE8             := L_fif_gl_cross_ref_cols('DR_SEQUENCE8');
   FIF_GL_CROSS_REF$DR_SEQUENCE9             := L_fif_gl_cross_ref_cols('DR_SEQUENCE9');
   FIF_GL_CROSS_REF$DR_SEQUENCE10            := L_fif_gl_cross_ref_cols('DR_SEQUENCE10');
   FIF_GL_CROSS_REF$CR_CCID                  := L_fif_gl_cross_ref_cols('CR_CCID');
   FIF_GL_CROSS_REF$CR_SEQUENCE1             := L_fif_gl_cross_ref_cols('CR_SEQUENCE1');
   FIF_GL_CROSS_REF$CR_SEQUENCE2             := L_fif_gl_cross_ref_cols('CR_SEQUENCE2');
   FIF_GL_CROSS_REF$CR_SEQUENCE3             := L_fif_gl_cross_ref_cols('CR_SEQUENCE3');
   FIF_GL_CROSS_REF$CR_SEQUENCE4             := L_fif_gl_cross_ref_cols('CR_SEQUENCE4');
   FIF_GL_CROSS_REF$CR_SEQUENCE5             := L_fif_gl_cross_ref_cols('CR_SEQUENCE5');
   FIF_GL_CROSS_REF$CR_SEQUENCE6             := L_fif_gl_cross_ref_cols('CR_SEQUENCE6');
   FIF_GL_CROSS_REF$CR_SEQUENCE7             := L_fif_gl_cross_ref_cols('CR_SEQUENCE7');
   FIF_GL_CROSS_REF$CR_SEQUENCE8             := L_fif_gl_cross_ref_cols('CR_SEQUENCE8');
   FIF_GL_CROSS_REF$CR_SEQUENCE9             := L_fif_gl_cross_ref_cols('CR_SEQUENCE9');
   FIF_GL_CROSS_REF$CR_SEQUENCE10            := L_fif_gl_cross_ref_cols('CR_SEQUENCE10');
   FIF_GL_CROSS_REF$DR_SEQUENCE11            := L_fif_gl_cross_ref_cols('DR_SEQUENCE11');
   FIF_GL_CROSS_REF$DR_SEQUENCE12            := L_fif_gl_cross_ref_cols('DR_SEQUENCE12');
   FIF_GL_CROSS_REF$DR_SEQUENCE13            := L_fif_gl_cross_ref_cols('DR_SEQUENCE13');
   FIF_GL_CROSS_REF$DR_SEQUENCE14            := L_fif_gl_cross_ref_cols('DR_SEQUENCE14');
   FIF_GL_CROSS_REF$DR_SEQUENCE15            := L_fif_gl_cross_ref_cols('DR_SEQUENCE15');
   FIF_GL_CROSS_REF$DR_SEQUENCE16            := L_fif_gl_cross_ref_cols('DR_SEQUENCE16');
   FIF_GL_CROSS_REF$DR_SEQUENCE17            := L_fif_gl_cross_ref_cols('DR_SEQUENCE17');
   FIF_GL_CROSS_REF$DR_SEQUENCE18            := L_fif_gl_cross_ref_cols('DR_SEQUENCE18');
   FIF_GL_CROSS_REF$DR_SEQUENCE19            := L_fif_gl_cross_ref_cols('DR_SEQUENCE19');
   FIF_GL_CROSS_REF$DR_SEQUENCE20            := L_fif_gl_cross_ref_cols('DR_SEQUENCE20');
   FIF_GL_CROSS_REF$CR_SEQUENCE11            := L_fif_gl_cross_ref_cols('CR_SEQUENCE11');
   FIF_GL_CROSS_REF$CR_SEQUENCE12            := L_fif_gl_cross_ref_cols('CR_SEQUENCE12');
   FIF_GL_CROSS_REF$CR_SEQUENCE13            := L_fif_gl_cross_ref_cols('CR_SEQUENCE13');
   FIF_GL_CROSS_REF$CR_SEQUENCE14            := L_fif_gl_cross_ref_cols('CR_SEQUENCE14');
   FIF_GL_CROSS_REF$CR_SEQUENCE15            := L_fif_gl_cross_ref_cols('CR_SEQUENCE15');
   FIF_GL_CROSS_REF$CR_SEQUENCE16            := L_fif_gl_cross_ref_cols('CR_SEQUENCE16');
   FIF_GL_CROSS_REF$CR_SEQUENCE17            := L_fif_gl_cross_ref_cols('CR_SEQUENCE17');
   FIF_GL_CROSS_REF$CR_SEQUENCE18            := L_fif_gl_cross_ref_cols('CR_SEQUENCE18');
   FIF_GL_CROSS_REF$CR_SEQUENCE19            := L_fif_gl_cross_ref_cols('CR_SEQUENCE19');
   FIF_GL_CROSS_REF$CR_SEQUENCE20            := L_fif_gl_cross_ref_cols('CR_SEQUENCE20');

   
END POPULATE_NAMES;
--------------------------------------------------------------------------
PROCEDURE POPULATE_FIF_GL_CROSS_REF( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE( select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = FIF_GL_CROSS_REF_sheet )
                      select s9t_row(s9t_cells(CORESVC_FIF_GL_CROSS_REF.action_mod ,
                                     set_of_books_id,
                                     dept,
                                     class,
                                     subclass,
                                     location,
                                     tran_code,
                                     cost_retail_flag,
                                     line_type,
                                     tran_ref_no,
                                     dr_ccid,
                                     dr_sequence1,
                                     dr_sequence2,
                                     dr_sequence3,
                                     dr_sequence4,
                                     dr_sequence5,
                                     dr_sequence6,
                                     dr_sequence7,
                                     dr_sequence8,
                                     dr_sequence9,
                                     dr_sequence10,
                                     cr_ccid,
                                     cr_sequence1,
                                     cr_sequence2,
                                     cr_sequence3,
                                     cr_sequence4,
                                     cr_sequence5,
                                     cr_sequence6,
                                     cr_sequence7,
                                     cr_sequence8,
                                     cr_sequence9,
                                     cr_sequence10,
                                     dr_sequence11,
                                     dr_sequence12,
                                     dr_sequence13,
                                     dr_sequence14,
                                     dr_sequence15,
                                     dr_sequence16,
                                     dr_sequence17,
                                     dr_sequence18,
                                     dr_sequence19,
                                     dr_sequence20,
                                     cr_sequence11,
                                     cr_sequence12,
                                     cr_sequence13,
                                     cr_sequence14,
                                     cr_sequence15,
                                     cr_sequence16,
                                     cr_sequence17,
                                     cr_sequence18,
                                     cr_sequence19,
                                     cr_sequence20))
                                from fif_gl_cross_ref ;
END POPULATE_FIF_GL_CROSS_REF;
--------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang := GET_USER_LANG;
   
   L_file.add_sheet(FIF_GL_CROSS_REF_sheet);
   L_file.sheets(L_file.get_sheet_index(FIF_GL_CROSS_REF_sheet)).column_headers := s9t_cells('ACTION',
                                                                                             'SET_OF_BOOKS_ID',
                                                                                             'DEPT',
                                                                                             'CLASS',
                                                                                             'SUBCLASS',
                                                                                             'LOCATION',
                                                                                             'TRAN_CODE',
                                                                                             'COST_RETAIL_FLAG',
                                                                                             'LINE_TYPE',
                                                                                             'TRAN_REF_NO',
                                                                                             'DR_CCID',
                                                                                             'DR_SEQUENCE1',
                                                                                             'DR_SEQUENCE2',
                                                                                             'DR_SEQUENCE3',
                                                                                             'DR_SEQUENCE4',
                                                                                             'DR_SEQUENCE5',
                                                                                             'DR_SEQUENCE6',
                                                                                             'DR_SEQUENCE7',
                                                                                             'DR_SEQUENCE8',
                                                                                             'DR_SEQUENCE9',
                                                                                             'DR_SEQUENCE10',
                                                                                             'CR_CCID',
                                                                                             'CR_SEQUENCE1',
                                                                                             'CR_SEQUENCE2',
                                                                                             'CR_SEQUENCE3',
                                                                                             'CR_SEQUENCE4',
                                                                                             'CR_SEQUENCE5',
                                                                                             'CR_SEQUENCE6',
                                                                                             'CR_SEQUENCE7',
                                                                                             'CR_SEQUENCE8',
                                                                                             'CR_SEQUENCE9',
                                                                                             'CR_SEQUENCE10',
                                                                                             'DR_SEQUENCE11',
                                                                                             'DR_SEQUENCE12',
                                                                                             'DR_SEQUENCE13',
                                                                                             'DR_SEQUENCE14',
                                                                                             'DR_SEQUENCE15',
                                                                                             'DR_SEQUENCE16',
                                                                                             'DR_SEQUENCE17',
                                                                                             'DR_SEQUENCE18',
                                                                                             'DR_SEQUENCE19',
                                                                                             'DR_SEQUENCE20',
                                                                                             'CR_SEQUENCE11',
                                                                                             'CR_SEQUENCE12',
                                                                                             'CR_SEQUENCE13',
                                                                                             'CR_SEQUENCE14',
                                                                                             'CR_SEQUENCE15',
                                                                                             'CR_SEQUENCE16',
                                                                                             'CR_SEQUENCE17',
                                                                                             'CR_SEQUENCE18',
                                                                                             'CR_SEQUENCE19',
                                                                                             'CR_SEQUENCE20');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file    s9t_file;
   L_program VARCHAR2(64):='CORESVC_FIF_GL_CROSS_REF.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;


   if I_template_only_ind = 'N' then
      POPULATE_FIF_GL_CROSS_REF(O_file_id);
      COMMIT;
   end if;
   
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FIF_GL_CROSS_REF( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                        I_process_id IN   SVC_FIF_GL_CROSS_REF.PROCESS_ID%TYPE) IS

   TYPE L_svc_fif_gl_cross_ref_col_typ          IS      TABLE OF SVC_FIF_GL_CROSS_REF%ROWTYPE;
   L_temp_rec                                           SVC_FIF_GL_CROSS_REF%ROWTYPE;
   L_svc_fif_gl_cross_ref_col                           L_svc_fif_gl_cross_ref_col_typ        :=NEW L_svc_fif_gl_cross_ref_col_typ();
   L_process_id                                         SVC_FIF_GL_CROSS_REF.PROCESS_ID%TYPE;
   L_error                                              BOOLEAN                               :=FALSE;
   L_default_rec                                        SVC_FIF_GL_CROSS_REF%ROWTYPE;
   cursor C_MANDATORY_IND is
      select SET_OF_BOOKS_ID_mi,
             DEPT_mi,
             CLASS_mi,
             SUBCLASS_mi,
             LOCATION_mi,
             TRAN_CODE_mi,
             COST_RETAIL_FLAG_mi,
             LINE_TYPE_mi,
             TRAN_REF_NO_mi,
             DR_CCID_mi,
             DR_SEQUENCE1_mi,
             DR_SEQUENCE2_mi,
             DR_SEQUENCE3_mi,
             DR_SEQUENCE4_mi,
             DR_SEQUENCE5_mi,
             DR_SEQUENCE6_mi,
             DR_SEQUENCE7_mi,
             DR_SEQUENCE8_mi,
             DR_SEQUENCE9_mi,
             DR_SEQUENCE10_mi,
             CR_CCID_mi,
             CR_SEQUENCE1_mi,
             CR_SEQUENCE2_mi,
             CR_SEQUENCE3_mi,
             CR_SEQUENCE4_mi,
             CR_SEQUENCE5_mi,
             CR_SEQUENCE6_mi,
             CR_SEQUENCE7_mi,
             CR_SEQUENCE8_mi,
             CR_SEQUENCE9_mi,
             CR_SEQUENCE10_mi,
             DR_SEQUENCE11_mi,
             DR_SEQUENCE12_mi,
             DR_SEQUENCE13_mi,
             DR_SEQUENCE14_mi,
             DR_SEQUENCE15_mi,
             DR_SEQUENCE16_mi,
             DR_SEQUENCE17_mi,
             DR_SEQUENCE18_mi,
             DR_SEQUENCE19_mi,
             DR_SEQUENCE20_mi,
             CR_SEQUENCE11_mi,
             CR_SEQUENCE12_mi,
             CR_SEQUENCE13_mi,
             CR_SEQUENCE14_mi,
             CR_SEQUENCE15_mi,
             CR_SEQUENCE16_mi,
             CR_SEQUENCE17_mi,
             CR_SEQUENCE18_mi,
             CR_SEQUENCE19_mi,
             CR_SEQUENCE20_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'FIF_GL_CROSS_REF'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('set_of_books_id'        as set_of_books_id,
                                            'dept'                   as dept,
                                            'class'                  as class,
                                            'subclass'               as subclass,
                                            'location'               as location,
                                            'tran_code'              as tran_code,
                                            'cost_retail_flag'       as cost_retail_flag,
                                            'line_type'              as line_type,
                                            'tran_ref_no'            as tran_ref_no,
                                            'dr_ccid'                as dr_ccid,
                                            'dr_sequence1'           as dr_sequence1,
                                            'dr_sequence2'           as dr_sequence2,
                                            'dr_sequence3'           as dr_sequence3,
                                            'dr_sequence4'           as dr_sequence4,
                                            'dr_sequence5'           as dr_sequence5,
                                            'dr_sequence6'           as dr_sequence6,
                                            'dr_sequence7'           as dr_sequence7,
                                            'dr_sequence8'           as dr_sequence8,
                                            'dr_sequence9'           as dr_sequence9,
                                            'dr_sequence10'          as dr_sequence10,
                                            'cr_ccid'                as cr_ccid,
                                            'cr_sequence1'           as cr_sequence1,
                                            'cr_sequence2'           as cr_sequence2,
                                            'cr_sequence3'           as cr_sequence3,
                                            'cr_sequence4'           as cr_sequence4,
                                            'cr_sequence5'           as cr_sequence5,
                                            'cr_sequence6'           as cr_sequence6,
                                            'cr_sequence7'           as cr_sequence7,
                                            'cr_sequence8'           as cr_sequence8,
                                            'cr_sequence9'           as cr_sequence9,
                                            'cr_sequence10'          as cr_sequence10,
                                             null                    as dummy,
                                            'dr_sequence11'          as dr_sequence11,
                                            'dr_sequence12'          as dr_sequence12,
                                            'dr_sequence13'          as dr_sequence13,
                                            'dr_sequence14'          as dr_sequence14,
                                            'dr_sequence15'          as dr_sequence15,
                                            'dr_sequence16'          as dr_sequence16,
                                            'dr_sequence17'          as dr_sequence17,
                                            'dr_sequence18'          as dr_sequence18,
                                            'dr_sequence19'          as dr_sequence19,
                                            'dr_sequence20'          as dr_sequence20,
                                            'cr_sequence11'          as cr_sequence11,
                                            'cr_sequence12'          as cr_sequence12,
                                            'cr_sequence13'          as cr_sequence13,
                                            'cr_sequence14'          as cr_sequence14,
                                            'cr_sequence15'          as cr_sequence15,
                                            'cr_sequence16'          as cr_sequence16,
                                            'cr_sequence17'          as cr_sequence17,
                                            'cr_sequence18'          as cr_sequence18,
                                            'cr_sequence19'          as cr_sequence19,
                                            'cr_sequence20'          as cr_sequence20));
   L_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_FIF_GL_CROSS_REF';
   L_pk_columns    VARCHAR2(255)  := 'Set Of Books,Department,Class,Subclass,Tran Code,Location,Cost/Retail,Line Type';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;      
BEGIN
  -- Get default values.
   FOR rec IN (select  SET_OF_BOOKS_ID_dv,
                       DEPT_dv,
                       CLASS_dv,
                       SUBCLASS_dv,
                       LOCATION_dv,
                       TRAN_CODE_dv,
                       COST_RETAIL_FLAG_dv,
                       LINE_TYPE_dv,
                       TRAN_REF_NO_dv,
                       DR_CCID_dv,
                       DR_SEQUENCE1_dv,
                       DR_SEQUENCE2_dv,
                       DR_SEQUENCE3_dv,
                       DR_SEQUENCE4_dv,
                       DR_SEQUENCE5_dv,
                       DR_SEQUENCE6_dv,
                       DR_SEQUENCE7_dv,
                       DR_SEQUENCE8_dv,
                       DR_SEQUENCE9_dv,
                       DR_SEQUENCE10_dv,
                       CR_CCID_dv,
                       CR_SEQUENCE1_dv,
                       CR_SEQUENCE2_dv,
                       CR_SEQUENCE3_dv,
                       CR_SEQUENCE4_dv,
                       CR_SEQUENCE5_dv,
                       CR_SEQUENCE6_dv,
                       CR_SEQUENCE7_dv,
                       CR_SEQUENCE8_dv,
                       CR_SEQUENCE9_dv,
                       CR_SEQUENCE10_dv,
                       DR_SEQUENCE11_dv,
                       DR_SEQUENCE12_dv,
                       DR_SEQUENCE13_dv,
                       DR_SEQUENCE14_dv,
                       DR_SEQUENCE15_dv,
                       DR_SEQUENCE16_dv,
                       DR_SEQUENCE17_dv,
                       DR_SEQUENCE18_dv,
                       DR_SEQUENCE19_dv,
                       DR_SEQUENCE20_dv,
                       CR_SEQUENCE11_dv,
                       CR_SEQUENCE12_dv,
                       CR_SEQUENCE13_dv,
                       CR_SEQUENCE14_dv,
                       CR_SEQUENCE15_dv,
                       CR_SEQUENCE16_dv,
                       CR_SEQUENCE17_dv,
                       CR_SEQUENCE18_dv,
                       CR_SEQUENCE19_dv,
                       CR_SEQUENCE20_dv,
                       NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                     = 'FIF_GL_CROSS_REF'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'set_of_books_id'    as set_of_books_id,
                                                      'dept'               as dept,
                                                      'class'              as class,
                                                      'subclass'           as subclass,
                                                      'location'           as location,
                                                      'tran_code'          as tran_code,
                                                      'cost_retail_flag'   as cost_retail_flag,
                                                      'line_type'          as line_type,
                                                      'tran_ref_no'        as tran_ref_no,
                                                      'dr_ccid'            as dr_ccid,
                                                      'dr_sequence1'       as dr_sequence1,
                                                      'dr_sequence2'       as dr_sequence2,
                                                      'dr_sequence3'       as dr_sequence3,
                                                      'dr_sequence4'       as dr_sequence4,
                                                      'dr_sequence5'       as dr_sequence5,
                                                      'dr_sequence6'       as dr_sequence6,
                                                      'dr_sequence7'       as dr_sequence7,
                                                      'dr_sequence8'       as dr_sequence8,
                                                      'dr_sequence9'       as dr_sequence9,
                                                      'dr_sequence10'      as dr_sequence10,
                                                      'cr_ccid'            as cr_ccid,
                                                      'cr_sequence1'       as cr_sequence1,
                                                      'cr_sequence2'       as cr_sequence2,
                                                      'cr_sequence3'       as cr_sequence3,
                                                      'cr_sequence4'       as cr_sequence4,
                                                      'cr_sequence5'       as cr_sequence5,
                                                      'cr_sequence6'       as cr_sequence6,
                                                      'cr_sequence7'       as cr_sequence7,
                                                      'cr_sequence8'       as cr_sequence8,
                                                      'cr_sequence9'       as cr_sequence9,
                                                      'cr_sequence10'      as cr_sequence10,
                                                      'dr_sequence11'       as dr_sequence11,
                                                      'dr_sequence12'       as dr_sequence12,
                                                      'dr_sequence13'       as dr_sequence13,
                                                      'dr_sequence14'       as dr_sequence14,
                                                      'dr_sequence15'       as dr_sequence15,
                                                      'dr_sequence16'       as dr_sequence16,
                                                      'dr_sequence17'       as dr_sequence17,
                                                      'dr_sequence18'       as dr_sequence18,
                                                      'dr_sequence19'       as dr_sequence19,
                                                      'dr_sequence20'       as dr_sequence20,
                                                      'cr_sequence11'       as cr_sequence11,
                                                      'cr_sequence12'       as cr_sequence12,
                                                      'cr_sequence13'       as cr_sequence13,
                                                      'cr_sequence14'       as cr_sequence14,
                                                      'cr_sequence15'       as cr_sequence15,
                                                      'cr_sequence16'       as cr_sequence16,
                                                      'cr_sequence17'       as cr_sequence17,
                                                      'cr_sequence18'       as cr_sequence18,
                                                      'cr_sequence19'       as cr_sequence19,
                                                      'cr_sequence20'       as cr_sequence20,
                                                      null                 as dummy)))
   LOOP
      BEGIN
         L_default_rec.SET_OF_BOOKS_ID := rec.SET_OF_BOOKS_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'SET_OF_BOOKS_ID ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DEPT := rec.DEPT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DEPT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CLASS := rec.CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CLASS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.SUBCLASS := rec.SUBCLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'SUBCLASS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LOCATION := rec.LOCATION_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'LOCATION ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.TRAN_CODE := rec.TRAN_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'TRAN_CODE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.COST_RETAIL_FLAG := rec.COST_RETAIL_FLAG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'COST_RETAIL_FLAG ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LINE_TYPE := rec.LINE_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'LINE_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.TRAN_REF_NO := rec.TRAN_REF_NO_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'TRAN_REF_NO ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_CCID := rec.DR_CCID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_CCID ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE1 := rec.DR_SEQUENCE1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE1 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE2 := rec.DR_SEQUENCE2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE2 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE3 := rec.DR_SEQUENCE3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE3 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE4 := rec.DR_SEQUENCE4_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE4 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE5 := rec.DR_SEQUENCE5_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE5 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE6 := rec.DR_SEQUENCE6_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE6 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE7 := rec.DR_SEQUENCE7_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE7 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE8 := rec.DR_SEQUENCE8_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE8 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE9 := rec.DR_SEQUENCE9_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE9 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE10 := rec.DR_SEQUENCE10_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE10 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_CCID := rec.CR_CCID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_CCID ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE1 := rec.CR_SEQUENCE1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE1 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE2 := rec.CR_SEQUENCE2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE2 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE3 := rec.CR_SEQUENCE3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE3 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE4 := rec.CR_SEQUENCE4_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE4 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE5 := rec.CR_SEQUENCE5_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE5 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE6 := rec.CR_SEQUENCE6_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE6 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE7 := rec.CR_SEQUENCE7_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE7 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE8 := rec.CR_SEQUENCE8_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE8 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE9 := rec.CR_SEQUENCE9_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE9 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE10 := rec.CR_SEQUENCE10_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE10 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE11 := rec.DR_SEQUENCE11_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE11 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE12 := rec.DR_SEQUENCE12_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE12 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE13 := rec.DR_SEQUENCE13_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE13 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE14 := rec.DR_SEQUENCE14_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE14 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE15 := rec.DR_SEQUENCE15_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE15 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE16 := rec.DR_SEQUENCE16_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE16 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE17 := rec.DR_SEQUENCE17_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE17 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE18 := rec.DR_SEQUENCE18_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE18 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE19 := rec.DR_SEQUENCE19_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE19 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DR_SEQUENCE20 := rec.DR_SEQUENCE20_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'DR_SEQUENCE20 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE11 := rec.CR_SEQUENCE11_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE11 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE12 := rec.CR_SEQUENCE12_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE12 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE13 := rec.CR_SEQUENCE13_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE13 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE14 := rec.CR_SEQUENCE14_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE14 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE15 := rec.CR_SEQUENCE15_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE15 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE16 := rec.CR_SEQUENCE16_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE16 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE17 := rec.CR_SEQUENCE17_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE17 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE18 := rec.CR_SEQUENCE18_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE18 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE19 := rec.CR_SEQUENCE19_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE19 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CR_SEQUENCE20 := rec.CR_SEQUENCE20_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_GL_CROSS_REF ' ,
                            NULL,
                           'CR_SEQUENCE20 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select UPPER(r.get_cell(FIF_GL_CROSS_REF$Action))             as Action,
                     r.get_cell(FIF_GL_CROSS_REF$SET_OF_BKS_ID)             as SET_OF_BOOKS_ID,
                     r.get_cell(FIF_GL_CROSS_REF$DEPT)                      as DEPT,
                     r.get_cell(FIF_GL_CROSS_REF$CLASS)                     as CLASS,
                     r.get_cell(FIF_GL_CROSS_REF$SUBCLASS)                  as SUBCLASS,
                     r.get_cell(FIF_GL_CROSS_REF$LOCATION)                  as LOCATION,
                     r.get_cell(FIF_GL_CROSS_REF$TRAN_CODE)                 as TRAN_CODE,
                     r.get_cell(FIF_GL_CROSS_REF$CST_RTL_FLG)               as COST_RETAIL_FLAG,
                     UPPER(r.get_cell(FIF_GL_CROSS_REF$LINE_TYPE))          as LINE_TYPE,
                     r.get_cell(FIF_GL_CROSS_REF$TRAN_REF_NO)               as TRAN_REF_NO,
                     r.get_cell(FIF_GL_CROSS_REF$DR_CCID)                   as DR_CCID,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE1)              as DR_SEQUENCE1,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE2)              as DR_SEQUENCE2,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE3)              as DR_SEQUENCE3,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE4)              as DR_SEQUENCE4,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE5)              as DR_SEQUENCE5,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE6)              as DR_SEQUENCE6,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE7)              as DR_SEQUENCE7,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE8)              as DR_SEQUENCE8,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE9)              as DR_SEQUENCE9,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE10)             as DR_SEQUENCE10,
                     r.get_cell(FIF_GL_CROSS_REF$CR_CCID)                   as CR_CCID,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE1)              as CR_SEQUENCE1,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE2)              as CR_SEQUENCE2,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE3)              as CR_SEQUENCE3,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE4)              as CR_SEQUENCE4,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE5)              as CR_SEQUENCE5,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE6)              as CR_SEQUENCE6,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE7)              as CR_SEQUENCE7,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE8)              as CR_SEQUENCE8,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE9)              as CR_SEQUENCE9,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE10)             as CR_SEQUENCE10,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE11)             as DR_SEQUENCE11,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE12)             as DR_SEQUENCE12,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE13)             as DR_SEQUENCE13,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE14)             as DR_SEQUENCE14,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE15)             as DR_SEQUENCE15,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE16)             as DR_SEQUENCE16,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE17)             as DR_SEQUENCE17,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE18)             as DR_SEQUENCE18,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE19)             as DR_SEQUENCE19,
                     r.get_cell(FIF_GL_CROSS_REF$DR_SEQUENCE20)             as DR_SEQUENCE20,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE11)             as CR_SEQUENCE11,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE12)             as CR_SEQUENCE12,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE13)             as CR_SEQUENCE13,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE14)             as CR_SEQUENCE14,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE15)             as CR_SEQUENCE15,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE16)             as CR_SEQUENCE16,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE17)             as CR_SEQUENCE17,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE18)             as CR_SEQUENCE18,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE19)             as CR_SEQUENCE19,
                     r.get_cell(FIF_GL_CROSS_REF$CR_SEQUENCE20)             as CR_SEQUENCE20,
                     r.get_row_seq()                                        as ROW_SEQ
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(FIF_GL_CROSS_REF_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.ACTION := rec.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SET_OF_BOOKS_ID := rec.SET_OF_BOOKS_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'SET_OF_BOOKS_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEPT := rec.DEPT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DEPT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CLASS := rec.CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SUBCLASS := rec.SUBCLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'SUBCLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LOCATION := rec.LOCATION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'LOCATION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TRAN_CODE := rec.TRAN_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'TRAN_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COST_RETAIL_FLAG := rec.COST_RETAIL_FLAG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'COST_RETAIL_FLAG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LINE_TYPE := rec.LINE_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'LINE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TRAN_REF_NO := rec.TRAN_REF_NO;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'TRAN_REF_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_CCID := rec.DR_CCID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_CCID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE1 := rec.DR_SEQUENCE1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE2 := rec.DR_SEQUENCE2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE3 := rec.DR_SEQUENCE3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE3',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE4 := rec.DR_SEQUENCE4;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE4',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE5 := rec.DR_SEQUENCE5;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE5',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE6 := rec.DR_SEQUENCE6;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE6',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE7 := rec.DR_SEQUENCE7;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE7',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE8 := rec.DR_SEQUENCE8;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE8',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE9 := rec.DR_SEQUENCE9;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE9',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE10 := rec.DR_SEQUENCE10;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE10',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_CCID := rec.CR_CCID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_CCID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE1 := rec.CR_SEQUENCE1;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE2 := rec.CR_SEQUENCE2;
    
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE3 := rec.CR_SEQUENCE3;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE3',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE4 := rec.CR_SEQUENCE4;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE4',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE5 := rec.CR_SEQUENCE5;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE5',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE6 := rec.CR_SEQUENCE6;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE6',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE7 := rec.CR_SEQUENCE7;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE7',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE8 := rec.CR_SEQUENCE8;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE8',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE9 := rec.CR_SEQUENCE9;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE9',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE10 := rec.CR_SEQUENCE10;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE10',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE11 := rec.DR_SEQUENCE11;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE11',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE12 := rec.DR_SEQUENCE12;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE12',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE13 := rec.DR_SEQUENCE13;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE13',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE14 := rec.DR_SEQUENCE14;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE14',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE15 := rec.DR_SEQUENCE15;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE15',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE16 := rec.DR_SEQUENCE16;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE16',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE17 := rec.DR_SEQUENCE17;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE17',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE18 := rec.DR_SEQUENCE18;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE18',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE19 := rec.DR_SEQUENCE19;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE19',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DR_SEQUENCE20 := rec.DR_SEQUENCE20;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'DR_SEQUENCE20',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE11 := rec.CR_SEQUENCE11;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE11',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE12 := rec.CR_SEQUENCE12;
    
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE12',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE13 := rec.CR_SEQUENCE13;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE13',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE14 := rec.CR_SEQUENCE14;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE14',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE15 := rec.CR_SEQUENCE15;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE15',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE16 := rec.CR_SEQUENCE16;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE16',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE17 := rec.CR_SEQUENCE17;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE17',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE18 := rec.CR_SEQUENCE18;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE18',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE19 := rec.CR_SEQUENCE19;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE19',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CR_SEQUENCE20 := rec.CR_SEQUENCE20;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_GL_CROSS_REF_sheet,
                            rec.row_seq,
                            'CR_SEQUENCE20',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_FIF_GL_CROSS_REF.action_new then
         L_temp_rec.SET_OF_BOOKS_ID  := NVL( L_temp_rec.SET_OF_BOOKS_ID,L_default_rec.SET_OF_BOOKS_ID);
         L_temp_rec.DEPT             := NVL( L_temp_rec.DEPT,L_default_rec.DEPT);
         L_temp_rec.CLASS            := NVL( L_temp_rec.CLASS,L_default_rec.CLASS);
         L_temp_rec.SUBCLASS         := NVL( L_temp_rec.SUBCLASS,L_default_rec.SUBCLASS);
         L_temp_rec.LOCATION         := NVL( L_temp_rec.LOCATION,L_default_rec.LOCATION);
         L_temp_rec.TRAN_CODE        := NVL( L_temp_rec.TRAN_CODE,L_default_rec.TRAN_CODE);
         L_temp_rec.COST_RETAIL_FLAG := NVL( L_temp_rec.COST_RETAIL_FLAG,L_default_rec.COST_RETAIL_FLAG);
         L_temp_rec.LINE_TYPE        := NVL( L_temp_rec.LINE_TYPE,L_default_rec.LINE_TYPE);
         L_temp_rec.TRAN_REF_NO      := NVL( L_temp_rec.TRAN_REF_NO,L_default_rec.TRAN_REF_NO);
         L_temp_rec.DR_CCID          := NVL( L_temp_rec.DR_CCID,L_default_rec.DR_CCID);
         L_temp_rec.DR_SEQUENCE1     := NVL( L_temp_rec.DR_SEQUENCE1,L_default_rec.DR_SEQUENCE1);
         L_temp_rec.DR_SEQUENCE2     := NVL( L_temp_rec.DR_SEQUENCE2,L_default_rec.DR_SEQUENCE2);
         L_temp_rec.DR_SEQUENCE3     := NVL( L_temp_rec.DR_SEQUENCE3,L_default_rec.DR_SEQUENCE3);
         L_temp_rec.DR_SEQUENCE4     := NVL( L_temp_rec.DR_SEQUENCE4,L_default_rec.DR_SEQUENCE4);
         L_temp_rec.DR_SEQUENCE5     := NVL( L_temp_rec.DR_SEQUENCE5,L_default_rec.DR_SEQUENCE5);
         L_temp_rec.DR_SEQUENCE6     := NVL( L_temp_rec.DR_SEQUENCE6,L_default_rec.DR_SEQUENCE6);
         L_temp_rec.DR_SEQUENCE7     := NVL( L_temp_rec.DR_SEQUENCE7,L_default_rec.DR_SEQUENCE7);
         L_temp_rec.DR_SEQUENCE8     := NVL( L_temp_rec.DR_SEQUENCE8,L_default_rec.DR_SEQUENCE8);
         L_temp_rec.DR_SEQUENCE9     := NVL( L_temp_rec.DR_SEQUENCE9,L_default_rec.DR_SEQUENCE9);
         L_temp_rec.DR_SEQUENCE10    := NVL( L_temp_rec.DR_SEQUENCE10,L_default_rec.DR_SEQUENCE10);
         L_temp_rec.CR_CCID          := NVL( L_temp_rec.CR_CCID,L_default_rec.CR_CCID);
         L_temp_rec.CR_SEQUENCE1     := NVL( L_temp_rec.CR_SEQUENCE1,L_default_rec.CR_SEQUENCE1);
         L_temp_rec.CR_SEQUENCE2     := NVL( L_temp_rec.CR_SEQUENCE2,L_default_rec.CR_SEQUENCE2);
         L_temp_rec.CR_SEQUENCE3     := NVL( L_temp_rec.CR_SEQUENCE3,L_default_rec.CR_SEQUENCE3);
         L_temp_rec.CR_SEQUENCE4     := NVL( L_temp_rec.CR_SEQUENCE4,L_default_rec.CR_SEQUENCE4);
         L_temp_rec.CR_SEQUENCE5     := NVL( L_temp_rec.CR_SEQUENCE5,L_default_rec.CR_SEQUENCE5);
         L_temp_rec.CR_SEQUENCE6     := NVL( L_temp_rec.CR_SEQUENCE6,L_default_rec.CR_SEQUENCE6);
         L_temp_rec.CR_SEQUENCE7     := NVL( L_temp_rec.CR_SEQUENCE7,L_default_rec.CR_SEQUENCE7);
         L_temp_rec.CR_SEQUENCE8     := NVL( L_temp_rec.CR_SEQUENCE8,L_default_rec.CR_SEQUENCE8);
         L_temp_rec.CR_SEQUENCE9     := NVL( L_temp_rec.CR_SEQUENCE9,L_default_rec.CR_SEQUENCE9);
         L_temp_rec.CR_SEQUENCE10    := NVL( L_temp_rec.CR_SEQUENCE10,L_default_rec.CR_SEQUENCE10);
         L_temp_rec.DR_SEQUENCE11    := NVL( L_temp_rec.DR_SEQUENCE11,L_default_rec.DR_SEQUENCE11);
         L_temp_rec.DR_SEQUENCE12    := NVL( L_temp_rec.DR_SEQUENCE12,L_default_rec.DR_SEQUENCE12);
         L_temp_rec.DR_SEQUENCE13    := NVL( L_temp_rec.DR_SEQUENCE13,L_default_rec.DR_SEQUENCE13);
         L_temp_rec.DR_SEQUENCE14    := NVL( L_temp_rec.DR_SEQUENCE14,L_default_rec.DR_SEQUENCE14);
         L_temp_rec.DR_SEQUENCE15    := NVL( L_temp_rec.DR_SEQUENCE15,L_default_rec.DR_SEQUENCE15);
         L_temp_rec.DR_SEQUENCE16    := NVL( L_temp_rec.DR_SEQUENCE16,L_default_rec.DR_SEQUENCE16);
         L_temp_rec.DR_SEQUENCE17    := NVL( L_temp_rec.DR_SEQUENCE17,L_default_rec.DR_SEQUENCE17);
         L_temp_rec.DR_SEQUENCE18    := NVL( L_temp_rec.DR_SEQUENCE18,L_default_rec.DR_SEQUENCE18);
         L_temp_rec.DR_SEQUENCE19    := NVL( L_temp_rec.DR_SEQUENCE19,L_default_rec.DR_SEQUENCE19);
         L_temp_rec.DR_SEQUENCE20    := NVL( L_temp_rec.DR_SEQUENCE20,L_default_rec.DR_SEQUENCE20);
         L_temp_rec.CR_SEQUENCE11    := NVL( L_temp_rec.CR_SEQUENCE11,L_default_rec.CR_SEQUENCE11);
         L_temp_rec.CR_SEQUENCE12    := NVL( L_temp_rec.CR_SEQUENCE12,L_default_rec.CR_SEQUENCE12);
         L_temp_rec.CR_SEQUENCE13    := NVL( L_temp_rec.CR_SEQUENCE13,L_default_rec.CR_SEQUENCE13);
         L_temp_rec.CR_SEQUENCE14    := NVL( L_temp_rec.CR_SEQUENCE14,L_default_rec.CR_SEQUENCE14);
         L_temp_rec.CR_SEQUENCE15    := NVL( L_temp_rec.CR_SEQUENCE15,L_default_rec.CR_SEQUENCE15);
         L_temp_rec.CR_SEQUENCE16    := NVL( L_temp_rec.CR_SEQUENCE16,L_default_rec.CR_SEQUENCE16);
         L_temp_rec.CR_SEQUENCE17    := NVL( L_temp_rec.CR_SEQUENCE17,L_default_rec.CR_SEQUENCE17);
         L_temp_rec.CR_SEQUENCE18    := NVL( L_temp_rec.CR_SEQUENCE18,L_default_rec.CR_SEQUENCE18);
         L_temp_rec.CR_SEQUENCE19    := NVL( L_temp_rec.CR_SEQUENCE19,L_default_rec.CR_SEQUENCE19);
         L_temp_rec.CR_SEQUENCE20    := NVL( L_temp_rec.CR_SEQUENCE20,L_default_rec.CR_SEQUENCE20);		 
      end if;

      if not (L_temp_rec.set_of_books_id is NOT NULL and
              L_temp_rec.dept is NOT NULL and
              L_temp_rec.class is NOT NULL and
              L_temp_rec.subclass is NOT NULL and
              L_temp_rec.tran_code is NOT NULL and
              L_temp_rec.location is NOT NULL and
              L_temp_rec.cost_retail_flag is NOT NULL and
              L_temp_rec.line_type is NOT NULL)then
              WRITE_S9T_ERROR(I_file_id,
                              FIF_GL_CROSS_REF_sheet,
                              rec.row_seq,
                              NULL,
                              NULL,
                              SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
              L_error := TRUE;
      end if;
      
      if NOT L_error then
         L_svc_fif_gl_cross_ref_col.extend();
         L_svc_fif_gl_cross_ref_col(L_svc_fif_gl_cross_ref_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..L_svc_fif_gl_cross_ref_col.COUNT SAVE EXCEPTIONS
      merge into SVC_FIF_GL_CROSS_REF st
      using(select
                  (case
                   when l_mi_rec.SET_OF_BOOKS_ID_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.set_of_books_id is NULL
                   then mt.set_of_books_id
                   else s1.set_of_books_id
                   end) as set_of_books_id,
                  (case
                   when l_mi_rec.DEPT_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dept is NULL
                   then mt.dept
                   else s1.dept
                   end) as dept,
                  (case
                   when l_mi_rec.CLASS_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.class is NULL
                   then mt.class
                   else s1.class
                   end) as class,
                  (case
                   when l_mi_rec.SUBCLASS_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.subclass is NULL
                   then mt.subclass
                   else s1.subclass
                   end) as subclass,
                  (case
                   when l_mi_rec.LOCATION_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.location is NULL
                   then mt.location
                   else s1.location
                   end) as location,
                  (case
                   when l_mi_rec.TRAN_CODE_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.tran_code is NULL
                   then mt.tran_code
                   else s1.tran_code
                   end) as tran_code,
                  (case
                   when l_mi_rec.COST_RETAIL_FLAG_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cost_retail_flag is NULL
                   then mt.cost_retail_flag
                   else s1.cost_retail_flag
                   end) as cost_retail_flag,
                  (case
                   when l_mi_rec.LINE_TYPE_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.line_type is NULL
                   then mt.line_type
                   else s1.line_type
                   end) as line_type,
                  (case
                   when l_mi_rec.TRAN_REF_NO_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.tran_ref_no is NULL
                   then mt.tran_ref_no
                   else s1.tran_ref_no
                   end) as tran_ref_no,
                  (case
                   when l_mi_rec.DR_CCID_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_ccid is NULL
                   then mt.dr_ccid
                   else s1.dr_ccid
                   end) as dr_ccid,
                  (case
                   when l_mi_rec.DR_SEQUENCE1_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence1 is NULL
                   then mt.dr_sequence1
                   else s1.dr_sequence1
                   end) as dr_sequence1,
                  (case
                   when l_mi_rec.DR_SEQUENCE2_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence2 is NULL
                   then mt.dr_sequence2
                   else s1.dr_sequence2
                   end) as dr_sequence2,
                  (case
                   when l_mi_rec.DR_SEQUENCE3_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence3 is NULL
                   then mt.dr_sequence3
                   else s1.dr_sequence3
                   end) as dr_sequence3,
                  (case
                   when l_mi_rec.DR_SEQUENCE4_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence4 is NULL
                   then mt.dr_sequence4
                   else s1.dr_sequence4
                   end) as dr_sequence4,
                  (case
                   when l_mi_rec.DR_SEQUENCE5_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence5 is NULL
                   then mt.dr_sequence5
                   else s1.dr_sequence5
                   end) as dr_sequence5,
                  (case
                   when l_mi_rec.DR_SEQUENCE6_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence6 is NULL
                   then mt.dr_sequence6
                   else s1.dr_sequence6
                   end) as dr_sequence6,
                  (case
                   when l_mi_rec.DR_SEQUENCE7_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence7 is NULL
                   then mt.dr_sequence7
                   else s1.dr_sequence7
                   end) as dr_sequence7,
                  (case
                   when l_mi_rec.DR_SEQUENCE8_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence8 is NULL
                   then mt.dr_sequence8
                   else s1.dr_sequence8
                   end) as dr_sequence8,
                  (case
                   when l_mi_rec.DR_SEQUENCE9_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence9 is NULL
                   then mt.dr_sequence9
                   else s1.dr_sequence9
                   end) as dr_sequence9,
                  (case
                   when l_mi_rec.DR_SEQUENCE10_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence10 is NULL
                   then mt.dr_sequence10
                   else s1.dr_sequence10
                   end) as dr_sequence10,
                  (case
                   when l_mi_rec.CR_CCID_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_ccid is NULL
                   then mt.cr_ccid
                   else s1.cr_ccid
                   end) as cr_ccid,
                  (case
                   when l_mi_rec.CR_SEQUENCE1_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence1 is NULL
                   then mt.cr_sequence1
                   else s1.cr_sequence1
                   end) as cr_sequence1,
                  (case
                   when l_mi_rec.CR_SEQUENCE2_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence2 is NULL
                   then mt.cr_sequence2
                   else s1.cr_sequence2
                   end) as cr_sequence2,
                  (case
                   when l_mi_rec.CR_SEQUENCE3_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence3 is NULL
                   then mt.cr_sequence3
                   else s1.cr_sequence3
                   end) as cr_sequence3,
                  (case
                   when l_mi_rec.CR_SEQUENCE4_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence4 is NULL
                   then mt.cr_sequence4
                   else s1.cr_sequence4
                   end) as cr_sequence4,
                  (case
                   when l_mi_rec.CR_SEQUENCE5_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence5 is NULL
                   then mt.cr_sequence5
                   else s1.cr_sequence5
                   end) as cr_sequence5,
                  (case
                   when l_mi_rec.CR_SEQUENCE6_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence6 is NULL
                   then mt.cr_sequence6
                   else s1.cr_sequence6
                   end) as cr_sequence6,
                  (case
                   when l_mi_rec.CR_SEQUENCE7_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence7 is NULL
                   then mt.cr_sequence7
                   else s1.cr_sequence7
                   end) as cr_sequence7,
                  (case
                   when l_mi_rec.CR_SEQUENCE8_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence8 is NULL
                   then mt.cr_sequence8
                   else s1.cr_sequence8
                   end) as cr_sequence8,
                  (case
                   when l_mi_rec.CR_SEQUENCE9_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence9 is NULL
                   then mt.cr_sequence9
                   else s1.cr_sequence9
                   end) as cr_sequence9,
                  (case
                   when l_mi_rec.CR_SEQUENCE10_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence10 is NULL
                   then mt.cr_sequence10
                   else s1.cr_sequence10
                   end) as cr_sequence10,
                  (case
                   when l_mi_rec.DR_SEQUENCE11_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence11 is NULL
                   then mt.dr_sequence11
                   else s1.dr_sequence11
                   end) as dr_sequence11,
                  (case
                   when l_mi_rec.DR_SEQUENCE12_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence12 is NULL
                   then mt.dr_sequence12
                   else s1.dr_sequence12
                   end) as dr_sequence12,
                  (case
                   when l_mi_rec.DR_SEQUENCE13_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence13 is NULL
                   then mt.dr_sequence13
                   else s1.dr_sequence13
                   end) as dr_sequence13,
                  (case
                   when l_mi_rec.DR_SEQUENCE14_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence14 is NULL
                   then mt.dr_sequence14
                   else s1.dr_sequence14
                   end) as dr_sequence14,
                  (case
                   when l_mi_rec.DR_SEQUENCE15_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence15 is NULL
                   then mt.dr_sequence15
                   else s1.dr_sequence15
                   end) as dr_sequence15,
                  (case
                   when l_mi_rec.DR_SEQUENCE16_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence16 is NULL
                   then mt.dr_sequence16
                   else s1.dr_sequence16
                   end) as dr_sequence16,
                  (case
                   when l_mi_rec.DR_SEQUENCE17_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence17 is NULL
                   then mt.dr_sequence17
                   else s1.dr_sequence17
                   end) as dr_sequence17,
                  (case
                   when l_mi_rec.DR_SEQUENCE18_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence18 is NULL
                   then mt.dr_sequence18
                   else s1.dr_sequence18
                   end) as dr_sequence18,
                  (case
                   when l_mi_rec.DR_SEQUENCE19_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence19 is NULL
                   then mt.dr_sequence19
                   else s1.dr_sequence19
                   end) as dr_sequence19,
                  (case
                   when l_mi_rec.DR_SEQUENCE20_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.dr_sequence20 is NULL
                   then mt.dr_sequence20
                   else s1.dr_sequence20
                   end) as dr_sequence20,
                  (case
                   when l_mi_rec.CR_SEQUENCE11_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence11 is NULL
                   then mt.cr_sequence11
                   else s1.cr_sequence11
                   end) as cr_sequence11,
                  (case
                   when l_mi_rec.CR_SEQUENCE12_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence12 is NULL
                   then mt.cr_sequence12
                   else s1.cr_sequence12
                   end) as cr_sequence12,
                  (case
                   when l_mi_rec.CR_SEQUENCE13_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence13 is NULL
                   then mt.cr_sequence13
                   else s1.cr_sequence13
                   end) as cr_sequence13,
                  (case
                   when l_mi_rec.CR_SEQUENCE14_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence14 is NULL
                   then mt.cr_sequence14
                   else s1.cr_sequence14
                   end) as cr_sequence14,
                  (case
                   when l_mi_rec.CR_SEQUENCE15_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence15 is NULL
                   then mt.cr_sequence15
                   else s1.cr_sequence15
                   end) as cr_sequence15,
                  (case
                   when l_mi_rec.CR_SEQUENCE16_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence16 is NULL
                   then mt.cr_sequence16
                   else s1.cr_sequence16
                   end) as cr_sequence16,
                  (case
                   when l_mi_rec.CR_SEQUENCE17_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence17 is NULL
                   then mt.cr_sequence17
                   else s1.cr_sequence17
                   end) as cr_sequence17,
                  (case
                   when l_mi_rec.CR_SEQUENCE18_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence18 is NULL
                   then mt.cr_sequence18
                   else s1.cr_sequence18
                   end) as cr_sequence18,
                  (case
                   when l_mi_rec.CR_SEQUENCE19_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence19 is NULL
                   then mt.cr_sequence19
                   else s1.cr_sequence19
                   end) as cr_sequence19,
                  (case
                   when l_mi_rec.CR_SEQUENCE20_mi    = 'N'
                    and L_svc_fif_gl_cross_ref_col(i).action = CORESVC_FIF_GL_CROSS_REF.action_mod
                    and s1.cr_sequence20 is NULL
                   then mt.cr_sequence20
                   else s1.cr_sequence20
                   end) as cr_sequence20,
                  NULL as dummy
              from (select L_svc_fif_gl_cross_ref_col(i).set_of_books_id  as set_of_books_id,
                           L_svc_fif_gl_cross_ref_col(i).dept             as dept,
                           L_svc_fif_gl_cross_ref_col(i).class            as class,
                           L_svc_fif_gl_cross_ref_col(i).subclass         as subclass,
                           L_svc_fif_gl_cross_ref_col(i).location         as location,
                           L_svc_fif_gl_cross_ref_col(i).tran_code        as tran_code,
                           L_svc_fif_gl_cross_ref_col(i).cost_retail_flag as cost_retail_flag,
                           L_svc_fif_gl_cross_ref_col(i).line_type        as line_type,
                           L_svc_fif_gl_cross_ref_col(i).tran_ref_no      as tran_ref_no,
                           L_svc_fif_gl_cross_ref_col(i).dr_ccid          as dr_ccid,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence1     as dr_sequence1,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence2     as dr_sequence2,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence3     as dr_sequence3,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence4     as dr_sequence4,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence5     as dr_sequence5,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence6     as dr_sequence6,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence7     as dr_sequence7,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence8     as dr_sequence8,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence9     as dr_sequence9,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence10    as dr_sequence10,
                           L_svc_fif_gl_cross_ref_col(i).cr_ccid          as cr_ccid,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence1     as cr_sequence1,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence2     as cr_sequence2,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence3     as cr_sequence3,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence4     as cr_sequence4,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence5     as cr_sequence5,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence6     as cr_sequence6,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence7     as cr_sequence7,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence8     as cr_sequence8,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence9     as cr_sequence9,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence10    as cr_sequence10,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence11    as dr_sequence11,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence12    as dr_sequence12,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence13    as dr_sequence13,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence14    as dr_sequence14,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence15    as dr_sequence15,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence16    as dr_sequence16,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence17    as dr_sequence17,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence18    as dr_sequence18,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence19    as dr_sequence19,
                           L_svc_fif_gl_cross_ref_col(i).dr_sequence20    as dr_sequence20,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence11    as cr_sequence11,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence12    as cr_sequence12,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence13    as cr_sequence13,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence14    as cr_sequence14,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence15    as cr_sequence15,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence16    as cr_sequence16,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence17    as cr_sequence17,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence18    as cr_sequence18,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence19    as cr_sequence19,
                           L_svc_fif_gl_cross_ref_col(i).cr_sequence20    as cr_sequence20,
                           NULL as dummy
                      from dual ) s1,
                           FIF_GL_CROSS_REF mt
                     where mt.SET_OF_BOOKS_ID (+)     = s1.SET_OF_BOOKS_ID   
                       and mt.DEPT (+)                = s1.DEPT
                       and mt.CLASS (+)               = s1.CLASS   
                       and mt.SUBCLASS (+)            = s1.SUBCLASS   
                       and mt.TRAN_CODE (+)           = s1.TRAN_CODE   
                       and mt.LOCATION (+)            = s1.LOCATION   
                       and mt.COST_RETAIL_FLAG (+)    = s1.COST_RETAIL_FLAG   
                       and mt.LINE_TYPE (+)           = s1.LINE_TYPE   
                       and mt.TRAN_REF_NO (+)        =  s1.TRAN_REF_NO )sq
        on (st.set_of_books_id      = sq.set_of_books_id and
            st.dept                 = sq.dept and
            st.class                = sq.class and
            st.subclass             = sq.subclass and
            st.tran_code            = sq.tran_code and
            st.location             = sq.location and
            st.cost_retail_flag     = sq.cost_retail_flag and
            st.line_type            = sq.line_type and
            st.tran_ref_no          = sq.tran_ref_no and
            L_svc_fif_gl_cross_ref_col(i).ACTION IN (CORESVC_FIF_GL_CROSS_REF.action_mod,
                                                     CORESVC_FIF_GL_CROSS_REF.action_del))
      when matched then
      update
         set process_id             = L_svc_fif_gl_cross_ref_col(i).process_id ,
             chunk_id               = L_svc_fif_gl_cross_ref_col(i).chunk_id ,
             row_seq                = L_svc_fif_gl_cross_ref_col(i).row_seq ,
             action                 = L_svc_fif_gl_cross_ref_col(i).action ,
             process$status         = L_svc_fif_gl_cross_ref_col(i).process$status ,
             dr_sequence8           = sq.dr_sequence8 ,
             dr_sequence1           = sq.dr_sequence1 ,
             dr_sequence7           = sq.dr_sequence7 ,
             dr_sequence10          = sq.dr_sequence10 ,
             cr_sequence7           = sq.cr_sequence7 ,
             cr_sequence4           = sq.cr_sequence4 ,
             cr_sequence9           = sq.cr_sequence9 ,
             dr_sequence2           = sq.dr_sequence2 ,
             dr_sequence6           = sq.dr_sequence6 ,
             cr_sequence8           = sq.cr_sequence8 ,
             cr_sequence3           = sq.cr_sequence3 ,
             dr_sequence9           = sq.dr_sequence9 ,
             cr_sequence2           = sq.cr_sequence2 ,
             dr_sequence3           = sq.dr_sequence3 ,
             cr_sequence1           = sq.cr_sequence1 ,
             cr_sequence6           = sq.cr_sequence6 ,
             dr_sequence4           = sq.dr_sequence4 ,
             cr_sequence5           = sq.cr_sequence5 ,
             dr_sequence18          = sq.dr_sequence18 ,
             dr_sequence11          = sq.dr_sequence11 ,
             dr_sequence17          = sq.dr_sequence17 ,
             dr_sequence20          = sq.dr_sequence20 ,
             cr_sequence17          = sq.cr_sequence17 ,
             cr_sequence14          = sq.cr_sequence14 ,
             cr_sequence19          = sq.cr_sequence19 ,
             dr_sequence12          = sq.dr_sequence12 ,
             dr_sequence16          = sq.dr_sequence16 ,
             cr_sequence18          = sq.cr_sequence18 ,
             cr_sequence13          = sq.cr_sequence13 ,
             dr_sequence19          = sq.dr_sequence19 ,
             cr_sequence12          = sq.cr_sequence12 ,
             dr_sequence13          = sq.dr_sequence13 ,
             cr_sequence11          = sq.cr_sequence11 ,
             cr_sequence16          = sq.cr_sequence16 ,
             dr_sequence14          = sq.dr_sequence14 ,
             cr_sequence15          = sq.cr_sequence15 ,			 
             dr_ccid                = sq.dr_ccid ,
             cr_ccid                = sq.cr_ccid ,
             cr_sequence10          = sq.cr_sequence10 ,
             dr_sequence5           = sq.dr_sequence5 ,
             cr_sequence20          = sq.cr_sequence20 ,
             dr_sequence15          = sq.dr_sequence15 ,
             create_id              = L_svc_fif_gl_cross_ref_col(i).create_id ,
             create_datetime        = L_svc_fif_gl_cross_ref_col(i).create_datetime ,
             last_upd_id            = L_svc_fif_gl_cross_ref_col(i).last_upd_id ,
             last_upd_datetime      = L_svc_fif_gl_cross_ref_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             set_of_books_id ,
             dept ,
             class ,
             subclass ,
             location ,
             tran_code ,
             cost_retail_flag ,
             line_type ,
             tran_ref_no ,
             dr_ccid ,
             dr_sequence1 ,
             dr_sequence2 ,
             dr_sequence3 ,
             dr_sequence4 ,
             dr_sequence5 ,
             dr_sequence6 ,
             dr_sequence7 ,
             dr_sequence8 ,
             dr_sequence9 ,
             dr_sequence10 ,
             cr_ccid ,
             cr_sequence1 ,
             cr_sequence2 ,
             cr_sequence3 ,
             cr_sequence4 ,
             cr_sequence5 ,
             cr_sequence6 ,
             cr_sequence7 ,
             cr_sequence8 ,
             cr_sequence9 ,
             cr_sequence10 ,
             dr_sequence11 ,
             dr_sequence12 ,
             dr_sequence13 ,
             dr_sequence14 ,
             dr_sequence15 ,
             dr_sequence16 ,
             dr_sequence17 ,
             dr_sequence18 ,
             dr_sequence19 ,
             dr_sequence20 ,
             cr_sequence11 ,
             cr_sequence12 ,
             cr_sequence13 ,
             cr_sequence14 ,
             cr_sequence15 ,
             cr_sequence16 ,
             cr_sequence17 ,
             cr_sequence18 ,
             cr_sequence19 ,
             cr_sequence20 ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(L_svc_fif_gl_cross_ref_col(i).process_id ,
             L_svc_fif_gl_cross_ref_col(i).chunk_id ,
             L_svc_fif_gl_cross_ref_col(i).row_seq ,
             L_svc_fif_gl_cross_ref_col(i).action ,
             L_svc_fif_gl_cross_ref_col(i).process$status ,
             sq.set_of_books_id ,
             sq.dept ,
             sq.class ,
             sq.subclass ,
             sq.location ,
             sq.tran_code ,
             sq.cost_retail_flag ,
             sq.line_type ,
             sq.tran_ref_no ,
             sq.dr_ccid ,
             sq.dr_sequence1 ,
             sq.dr_sequence2 ,
             sq.dr_sequence3 ,
             sq.dr_sequence4 ,
             sq.dr_sequence5 ,
             sq.dr_sequence6 ,
             sq.dr_sequence7 ,
             sq.dr_sequence8 ,
             sq.dr_sequence9 ,
             sq.dr_sequence10 ,
             sq.cr_ccid ,
             sq.cr_sequence1 ,
             sq.cr_sequence2 ,
             sq.cr_sequence3 ,
             sq.cr_sequence4 ,
             sq.cr_sequence5 ,
             sq.cr_sequence6 ,
             sq.cr_sequence7 ,
             sq.cr_sequence8 ,
             sq.cr_sequence9 ,
             sq.cr_sequence10 ,
             sq.dr_sequence11 ,
             sq.dr_sequence12 ,
             sq.dr_sequence13 ,
             sq.dr_sequence14 ,
             sq.dr_sequence15 ,
             sq.dr_sequence16 ,
             sq.dr_sequence17 ,
             sq.dr_sequence18 ,
             sq.dr_sequence19 ,
             sq.dr_sequence20 ,
             sq.cr_sequence11 ,
             sq.cr_sequence12 ,
             sq.cr_sequence13 ,
             sq.cr_sequence14 ,
             sq.cr_sequence15 ,
             sq.cr_sequence16 ,
             sq.cr_sequence17 ,
             sq.cr_sequence18 ,
             sq.cr_sequence19 ,
             sq.cr_sequence20 ,
             L_svc_fif_gl_cross_ref_col(i).create_id ,
             L_svc_fif_gl_cross_ref_col(i).create_datetime ,
             L_svc_fif_gl_cross_ref_col(i).last_upd_id ,
             L_svc_fif_gl_cross_ref_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             FIF_GL_CROSS_REF_sheet,
                             L_svc_fif_gl_cross_ref_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);                            
         END LOOP;
   END;
END PROCESS_S9T_FIF_GL_CROSS_REF;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count     IN OUT   NUMBER,
                      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_FIF_GL_CROSS_REF.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	    EXCEPTION;
   PRAGMA	    EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);

   if s9t_pkg.validate_template(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_FIF_GL_CROSS_REF(I_file_id,I_process_id);
   end if;

   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;
      return FALSE;
      
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
      
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error           IN OUT   BOOLEAN,
                           I_rec             IN       C_SVC_FIF_GL_CROSS_REF%ROWTYPE,
                           I_Ind             IN       NUMBER)
RETURN BOOLEAN IS   
   L_program            VARCHAR2(64)                      := 'CORESVC_FIF_GL_CROSS_REF.VALIDATE_LOCATION';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FIF_GL_CROSS_REF';
   L_loc_type           ITEM_LOC.LOC_TYPE%TYPE;
   L_set_of_books_id    FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE;
   L_loc_type_sob       ITEM_LOC.LOC_TYPE%TYPE;
   L_set_of_books_desc  FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE;
   L_stockholding_ind   STORE.STOCKHOLDING_IND%TYPE;
   L_cr_ccid            FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE;
   L_dr_ccid            FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE;
   L_acc_validate_tbl   ACC_VALIDATE_API.ACC_VALIDATE_TBL;
BEGIN
   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_loc_type,
                                   I_rec.location) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'LOCATION',
                  O_error_message);
      O_error:=TRUE;
   else
      if I_ind = 2 then
         if L_loc_type IN ('R','M','X') then
            L_loc_type_sob:='W';
         else
            L_loc_type_sob:=L_loc_type;
         end if;

         if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                         L_set_of_books_desc,
                                         L_set_of_books_id,
                                         I_rec.location,
                                         L_loc_type_sob) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'LOCATION',
                        O_error_message);
            O_error:=TRUE;
         end if;

         if I_rec.set_of_books_id is NOT NULL and
            I_rec.set_of_books_id <> L_set_of_books_id then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Location,Set of Books',
                        'LOC_NOT_IN_TSF_SOB');
            O_error:=TRUE;
         end if;
      end if;
   
      if L_loc_type = 'W' then
         if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_error_message,
                                                   L_stockholding_ind,
                                                   I_rec.location,
                                                   L_loc_type) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'LOCATION',
                        O_error_message);
            O_error:=TRUE;
         
         elsif L_stockholding_ind != 'Y' then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'LOCATION',
                        'NON_STOCKHOLDING_WH');
            O_error:=TRUE;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
   
END VALIDATE_LOCATION;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_FIF_GL_CROSS_REF_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error           IN OUT   BOOLEAN,
                                      I_rec             IN OUT   C_SVC_FIF_GL_CROSS_REF%ROWTYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      := 'CORESVC_FIF_GL_CROSS_REF.PROCESS_FIF_GL_CROSS_REF_VAL';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FIF_GL_CROSS_REF';
   L_valid              BOOLEAN;
   L_dept_name          V_DEPS.DEPT_NAME%TYPE;
   L_class_name         V_CLASS.CLASS_NAME%TYPE;
   L_subclass_name      V_SUBCLASS.SUB_NAME%TYPE;
   L_desc               FIF_GL_ACCT.DESCRIPTION1%TYPE;
   L_dr_ccid            FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE;
   L_cr_ccid            FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE;
   L_acc_validate_tbl   ACC_VALIDATE_API.ACC_VALIDATE_TBL;
   L_desc1              INV_ADJ_REASON_TL.REASON_DESC%TYPE;
   L_cogs_ind           INV_ADJ_REASON.COGS_IND%TYPE;
   L_dr                 BOOLEAN;
   L_cr                 BOOLEAN;
   L_loc_type           ITEM_LOC.LOC_TYPE%TYPE;
   L_tran_ref_no        FIF_GL_CROSS_REF.TRAN_REF_NO%TYPE;
   L_stockhold_ind      WH.STOCKHOLDING_IND%TYPE          := 'N';

BEGIN
   L_tran_ref_no := I_rec.tran_ref_no;
   
   if I_rec.tran_code IN (22,23) and I_rec.tran_ref_no is NOT NULL then
      if LENGTH(TRIM(TRANSLATE(I_rec.tran_ref_no, ' +-.0123456789', ' '))) is NOT NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TRAN_REF_NO',
                     'INVAL_REASON');
         O_error :=TRUE;
      else
         if INVADJ_SQL.GET_REASON_INFO(O_error_message,
                                       L_desc1,
                                       L_cogs_ind,
                                       TO_NUMBER(L_tran_ref_no)) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'TRAN_REF_NO',
                        O_error_message);
            O_error:=TRUE;
         else
            if I_rec.tran_code = 22 and L_cogs_ind = 'Y' then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'TRAN_REF_NO',
                           'REASON_BAD_TRAN_CODE');
               O_error:=TRUE;
            end if;
            if I_Rec.tran_code = 23 and L_cogs_ind = 'N' then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'TRAN_REF_NO',
                           'REASON_BAD_TRAN_CODE');
               O_error:=TRUE;
            end if;
         end if;
      end if;
   end if;
   
   if I_rec.tran_code IN (37,38) and I_rec.tran_ref_no is NOT NULL then

      if LENGTH(TRIM(TRANSLATE(I_rec.tran_ref_no, ' +-.0123456789', ' '))) is NOT NULL then
          WRITE_ERROR(I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'TRAN_REF_NO',
                      'INVALID_LOC');
         O_error:=TRUE;
      else
         if I_rec.tran_code NOT IN ('-1') then
            if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                            L_loc_type,
                                            TO_NUMBER(L_tran_ref_no)) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'TRAN_REF_NO',
                           O_error_message);
               O_error:=TRUE;
            else
               if L_loc_type = 'W' then
                  if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_error_message,
                                                            L_stockhold_ind,
                                                            L_tran_ref_no,
                                                            L_loc_type) = FALSE then
                     WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'TRAN_REF_NO',
                           O_error_message);
                     O_error:=TRUE;  
                  end if;
   
                  if L_stockhold_ind != 'Y' then
                     WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'TRAN_REF_NO',
                           'NON_STOCKHOLDING_WH');
                     O_error:=TRUE;
                  end if;
               end if;
            end if;
         end if;
      end if;
   end if;

   if I_rec.tran_code IN (63,64) and I_rec.tran_ref_no is NOT NULL then
      
      if I_rec.tran_ref_no NOT IN ('-1') and I_rec.foc_wact_fk_rid is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TRAN_REF_NO',
                     'INV_ACTIVITY');
         O_error:=TRUE;
      end if;
      
      if I_rec.foc_wact_fk_rid is NOT NULL and I_rec.foc_wact_fk_cost_type = 'P' and I_rec.tran_code = 63 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TRAN_REF_NO',
                     'ACTIVITY_NOT_VALID_63');
         O_error:=TRUE;
      end if;
      if I_rec.foc_wact_fk_rid is NOT NULL and I_rec.foc_wact_fk_cost_type = 'U' and I_rec.tran_code = 64 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TRAN_REF_NO',
                     'ACTIVITY_NOT_VALID_64');
         O_error:=TRUE;            
      end if;
   end if;    

   if I_rec.tran_code IN (87,88)
      and I_rec.tran_ref_no is NOT NULL
      and I_rec.tran_ref_no NOT IN ('-1')
      and I_rec.foc_vc_fk_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'TRAN_REF_NO',
                  'INV_VAT_CODE');
      O_error:=TRUE;
   end if;

   if I_rec.location is NOT NULL and I_rec.location NOT IN (-1) then
      if VALIDATE_LOCATION(O_error_message,
                           O_error,
                           I_rec,
                           2) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error:=TRUE;
      end if;
   end if;
   
   if I_rec.dept is NOT NULL
      and I_rec.dept NOT IN (-1) then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_DEPS(O_error_message,
                                               L_valid,
                                               L_dept_name,
                                               NULL,
                                               NULL,
                                               I_rec.dept) = FALSE
         or L_valid = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DEPT',
                     O_error_message);
         O_error:=TRUE;
      end if;
   end if;   

   if LP_system_options_rec.gl_rollup = 'D' 
      OR I_rec.dept = -1 then
      I_rec.class:=-1;
   elsif L_valid then
      if I_rec.class is NOT NULL
         and I_rec.class NOT IN (-1) then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS(O_error_message,
                                                   L_valid,
                                                   L_class_name,
                                                   I_rec.dept,
                                                   I_rec.class) = FALSE
            or L_valid = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Department,Class',
                        O_error_message);
            O_error:=TRUE;
         end if;
      end if;
   end if;

   if LP_system_options_rec.gl_rollup IN ('D','C') 
      or I_rec.class = -1 then
      I_rec.subclass:=-1;
   elsif L_valid then
      if I_rec.subclass is NOT NULL
         and I_rec.subclass NOT IN (-1) then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUBCLASS(O_error_message,
                                                      L_valid,
                                                      L_subclass_name,
                                                      I_rec.dept,
                                                      I_rec.class,
                                                      I_rec.subclass) = FALSE
            or L_valid = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Department,Class,Subclass',
                        O_error_message);
            O_error:=TRUE;
         end if;
      end if;
   end if;

   if NVL(LP_system_options_rec.financial_ap,'-1') != 'A' then
      for i IN 1..10
      LOOP
         if NVL(LP_dr_col_name(i),'-1') <> '-1' then
            if RMS2FIN.GET_SEG_DESC(O_error_message,
                                    L_desc,
                                    I_rec.set_of_books_id,
                                    LP_dr_col_name(i),
                                    TO_CHAR(i)) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'Debit Segment'||i,
                           O_error_message);
               O_error:=TRUE;
            end if;
         end if;
      END LOOP;   

      for i IN 1..10
      LOOP
         if NVL(LP_cr_col_name(i),'-1') <> '-1' then
            if RMS2FIN.GET_SEG_DESC(O_error_message,
                                    L_desc,
                                    I_rec.set_of_books_id,
                                    LP_cr_col_name(i),
                                    TO_CHAR(i)) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'Credit Segment'||i,
                           O_error_message);
               O_error:=TRUE;
            end if;
         end if;
      END LOOP;
   end if;

   if I_rec.dr_ccid is NOT NULL
      and NVL(LP_system_options_rec.financial_ap,'-1') != 'A' then
      
      if RMS2FIN.GET_ACCT(O_error_message,
                          L_dr_ccid,
                          I_rec.set_of_books_id,
                          I_rec.dr_sequence1,
                          I_rec.dr_sequence2,
                          I_rec.dr_sequence3,
                          I_rec.dr_sequence4,
                          I_rec.dr_sequence5,
                          I_rec.dr_sequence6,
                          I_rec.dr_sequence7,
                          I_rec.dr_sequence8,
                          I_rec.dr_sequence9,
                          I_rec.dr_sequence10) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Set of Books,Debit Segment',
                     O_error_message);
         O_error:=TRUE;
      end if;
      I_rec.dr_ccid := substr(L_dr_ccid, 1, 25);
   else
      if I_rec.action IN (action_new, action_mod) and 
         I_rec.dr_sequence1  is NULL and
         I_rec.dr_sequence2  is NULL and
         I_rec.dr_sequence3  is NULL and
         I_rec.dr_sequence4  is NULL and
         I_rec.dr_sequence5  is NULL and
         I_rec.dr_sequence6  is NULL and
         I_rec.dr_sequence7  is NULL and
         I_rec.dr_sequence8  is NULL and
         I_rec.dr_sequence9  is NULL and
         I_rec.dr_sequence10 is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Debit Segment',
                     'NO_SEGMENT_AVAIL');
         O_error:=TRUE;
      end if;

      if NOT O_error then
         L_acc_validate_tbl(1).requesting_system := 'RMS';
         L_acc_validate_tbl(1).set_of_books_id   := I_rec.set_of_books_id;    
         L_acc_validate_tbl(1).ccid              := NULL;
         L_acc_validate_tbl(1).segment_1         := I_rec.dr_sequence1;
         L_acc_validate_tbl(1).segment_2         := I_rec.dr_sequence2;
         L_acc_validate_tbl(1).segment_3         := I_rec.dr_sequence3;
         L_acc_validate_tbl(1).segment_4         := I_rec.dr_sequence4;
         L_acc_validate_tbl(1).segment_5         := I_rec.dr_sequence5;
         L_acc_validate_tbl(1).segment_6         := I_rec.dr_sequence6;
         L_acc_validate_tbl(1).segment_7         := I_rec.dr_sequence7;
         L_acc_validate_tbl(1).segment_8         := I_rec.dr_sequence8;
         L_acc_validate_tbl(1).segment_9         := I_rec.dr_sequence9;
         L_acc_validate_tbl(1).segment_10        := I_rec.dr_sequence10;
         
         if I_rec.action IN (action_new,action_mod) then
         
      	 	  if ACC_VALIDATE_API.VALIDATE_ACC(O_error_message,
      			                  							  L_acc_validate_tbl)= FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'Set of Books,Debit Segment',
                           O_error_message);
               O_error:=TRUE;
      		  end if;
      
            if L_acc_validate_tbl(1).account_status = 'invalid' then
               LP_dr_acc_status := 'N';
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'DR_CCID',
                           'INV_COMB_SEQ');
               O_error:=TRUE;
            else
               LP_dr_acc_status := 'Y';            
            end if;
      	 end if;        
      end if;
   end if;

   if I_rec.cr_ccid is NOT NULL
      and NVL(LP_system_options_rec.financial_ap,'-1') != 'A' then
      if RMS2FIN.GET_ACCT(O_error_message,
                          L_cr_ccid,
                          I_rec.set_of_books_id,
                          I_rec.cr_sequence1,
                          I_rec.cr_sequence2,
                          I_rec.cr_sequence3,
                          I_rec.cr_sequence4,
                          I_rec.cr_sequence5,
                          I_rec.cr_sequence6,
                          I_rec.cr_sequence7,
                          I_rec.cr_sequence8,
                          I_rec.cr_sequence9,
                          I_rec.cr_sequence10) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Set of Books,Credit Segment',
                     O_error_message);
         O_error:=TRUE;
      end if;
      I_rec.cr_ccid := substr(L_cr_ccid, 1, 25);
   else
      if I_rec.action IN (action_new, action_mod) and
         I_rec.cr_sequence1  is NULL and
         I_rec.cr_sequence2  is NULL and
         I_rec.cr_sequence3  is NULL and
         I_rec.cr_sequence4  is NULL and
         I_rec.cr_sequence5  is NULL and
         I_rec.cr_sequence6  is NULL and
         I_rec.cr_sequence7  is NULL and
         I_rec.cr_sequence8  is NULL and
         I_rec.cr_sequence9  is NULL and
         I_rec.cr_sequence10 is NULL then        
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Credit Segment',
                     'NO_SEGMENT_AVAIL');
         O_error:=TRUE;
      end if;
      if NOT O_error then
         L_acc_validate_tbl(1).requesting_system := 'RMS';
         L_acc_validate_tbl(1).set_of_books_id   := I_rec.set_of_books_id;    
         L_acc_validate_tbl(1).ccid              := NULL;
         L_acc_validate_tbl(1).segment_1         := I_rec.cr_sequence1;
         L_acc_validate_tbl(1).segment_2         := I_rec.cr_sequence2;
         L_acc_validate_tbl(1).segment_3         := I_rec.cr_sequence3;
         L_acc_validate_tbl(1).segment_4         := I_rec.cr_sequence4;
         L_acc_validate_tbl(1).segment_5         := I_rec.cr_sequence5;
         L_acc_validate_tbl(1).segment_6         := I_rec.cr_sequence6;
         L_acc_validate_tbl(1).segment_7         := I_rec.cr_sequence7;
         L_acc_validate_tbl(1).segment_8         := I_rec.cr_sequence8;
         L_acc_validate_tbl(1).segment_9         := I_rec.cr_sequence9;
         L_acc_validate_tbl(1).segment_10        := I_rec.cr_sequence10;
   
         if I_rec.action IN (action_new , action_mod) then
            if ACC_VALIDATE_API.VALIDATE_ACC(O_error_message,
                                             L_acc_validate_tbl)= FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'Set of Books,Credit Segment',
                           O_error_message);
               O_error:=TRUE;
            end if;
            if L_acc_validate_tbl(1).account_status = 'invalid' then
               LP_cr_acc_status := 'N';
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'CR_CCID',
                           'INV_COMB_SEQ');
               O_error:=TRUE;
            else
               LP_cr_acc_status := 'Y';
            end if;
         end if;
      end if;
   end if;      
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FIF_GL_CROSS_REF_VAL;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_FIF_GL_CROSS_REF_INS( O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_fif_gl_cross_ref_temp_rec   IN       FIF_GL_CROSS_REF%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_FIF_GL_CROSS_REF.EXEC_FIF_GL_CROSS_REF_INS';
BEGIN
   insert
     into fif_gl_cross_ref
   values I_fif_gl_cross_ref_temp_rec;
   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FIF_GL_CROSS_REF_INS;
----------------------------------------------------------------------------------
FUNCTION EXEC_FIF_GL_CROSS_REF_UPD( O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_fif_gl_cross_ref_temp_rec   IN       FIF_GL_CROSS_REF%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_FIF_GL_CROSS_REF.EXEC_FIF_GL_CROSS_REF_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FIF_GL_CROSS_REF';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_FIF_GL_CROSS_REF_LOCK is 
      select 'X'
        from FIF_GL_CROSS_REF
       where set_of_books_id      = I_fif_gl_cross_ref_temp_rec.set_of_books_id
             and dept             = I_fif_gl_cross_ref_temp_rec.dept
             and class            = I_fif_gl_cross_ref_temp_rec.class
             and subclass         = I_fif_gl_cross_ref_temp_rec.subclass
             and tran_code        = I_fif_gl_cross_ref_temp_rec.tran_code
             and location         = I_fif_gl_cross_ref_temp_rec.location
             and cost_retail_flag = I_fif_gl_cross_ref_temp_rec.cost_retail_flag
             and line_type        = I_fif_gl_cross_ref_temp_rec.line_type
             and (tran_code = -1
                  or (tran_code = I_fif_gl_cross_ref_temp_rec.tran_code
                      and (tran_ref_no in (I_fif_gl_cross_ref_temp_rec.tran_ref_no, '-1')
                           or I_fif_gl_cross_ref_temp_rec.tran_ref_no = '-1'
                           or (tran_ref_no is NULL and I_fif_gl_cross_ref_temp_rec.tran_ref_no is NULL))))
         for update nowait;
BEGIN
   open C_FIF_GL_CROSS_REF_LOCK;

   close C_FIF_GL_CROSS_REF_LOCK;


   update fif_gl_cross_ref
      set dr_ccid      =I_fif_gl_cross_ref_temp_rec.dr_ccid,
          dr_sequence1 =I_fif_gl_cross_ref_temp_rec.dr_sequence1,
          dr_sequence2 =I_fif_gl_cross_ref_temp_rec.dr_sequence2,
          dr_sequence3 =I_fif_gl_cross_ref_temp_rec.dr_sequence3,
          dr_sequence4 =I_fif_gl_cross_ref_temp_rec.dr_sequence4,
          dr_sequence5 =I_fif_gl_cross_ref_temp_rec.dr_sequence5,
          dr_sequence6 =I_fif_gl_cross_ref_temp_rec.dr_sequence6,
          dr_sequence7 =I_fif_gl_cross_ref_temp_rec.dr_sequence7,
          dr_sequence8 =I_fif_gl_cross_ref_temp_rec.dr_sequence8,
          dr_sequence9 =I_fif_gl_cross_ref_temp_rec.dr_sequence9,
          dr_sequence10=I_fif_gl_cross_ref_temp_rec.dr_sequence10,
          cr_ccid      =I_fif_gl_cross_ref_temp_rec.cr_ccid,
          cr_sequence1 =I_fif_gl_cross_ref_temp_rec.cr_sequence1,
          cr_sequence2 =I_fif_gl_cross_ref_temp_rec.cr_sequence2,
          cr_sequence3 =I_fif_gl_cross_ref_temp_rec.cr_sequence3,
          cr_sequence4 =I_fif_gl_cross_ref_temp_rec.cr_sequence4,
          cr_sequence5 =I_fif_gl_cross_ref_temp_rec.cr_sequence5,
          cr_sequence6 =I_fif_gl_cross_ref_temp_rec.cr_sequence6,
          cr_sequence7 =I_fif_gl_cross_ref_temp_rec.cr_sequence7,
          cr_sequence8 =I_fif_gl_cross_ref_temp_rec.cr_sequence8,
          cr_sequence9 =I_fif_gl_cross_ref_temp_rec.cr_sequence9,
          cr_sequence10=I_fif_gl_cross_ref_temp_rec.cr_sequence10,
          dr_sequence11 =I_fif_gl_cross_ref_temp_rec.dr_sequence11,
          dr_sequence12 =I_fif_gl_cross_ref_temp_rec.dr_sequence12,
          dr_sequence13 =I_fif_gl_cross_ref_temp_rec.dr_sequence13,
          dr_sequence14 =I_fif_gl_cross_ref_temp_rec.dr_sequence14,
          dr_sequence15 =I_fif_gl_cross_ref_temp_rec.dr_sequence15,
          dr_sequence16 =I_fif_gl_cross_ref_temp_rec.dr_sequence16,
          dr_sequence17 =I_fif_gl_cross_ref_temp_rec.dr_sequence17,
          dr_sequence18 =I_fif_gl_cross_ref_temp_rec.dr_sequence18,
          dr_sequence19 =I_fif_gl_cross_ref_temp_rec.dr_sequence19,
          dr_sequence20 =I_fif_gl_cross_ref_temp_rec.dr_sequence20,
          cr_sequence11 =I_fif_gl_cross_ref_temp_rec.cr_sequence11,
          cr_sequence12 =I_fif_gl_cross_ref_temp_rec.cr_sequence12,
          cr_sequence13 =I_fif_gl_cross_ref_temp_rec.cr_sequence13,
          cr_sequence14 =I_fif_gl_cross_ref_temp_rec.cr_sequence14,
          cr_sequence15 =I_fif_gl_cross_ref_temp_rec.cr_sequence15,
          cr_sequence16 =I_fif_gl_cross_ref_temp_rec.cr_sequence16,
          cr_sequence17 =I_fif_gl_cross_ref_temp_rec.cr_sequence17,
          cr_sequence18 =I_fif_gl_cross_ref_temp_rec.cr_sequence18,
          cr_sequence19 =I_fif_gl_cross_ref_temp_rec.cr_sequence19,
          cr_sequence20 =I_fif_gl_cross_ref_temp_rec.cr_sequence20
    where set_of_books_id  = I_fif_gl_cross_ref_temp_rec.set_of_books_id
      and dept             = I_fif_gl_cross_ref_temp_rec.dept
      and class            = I_fif_gl_cross_ref_temp_rec.class
      and subclass         = I_fif_gl_cross_ref_temp_rec.subclass
      and location         = I_fif_gl_cross_ref_temp_rec.location
      and cost_retail_flag = I_fif_gl_cross_ref_temp_rec.cost_retail_flag
      and line_type        = I_fif_gl_cross_ref_temp_rec.line_type
      and tran_code        = I_fif_gl_cross_ref_temp_rec.tran_code
      and (tran_code = -1
           or (tran_code = I_fif_gl_cross_ref_temp_rec.tran_code
                and (tran_ref_no in (I_fif_gl_cross_ref_temp_rec.tran_ref_no, '-1')
                     or I_fif_gl_cross_ref_temp_rec.tran_ref_no = '-1'
                     or (tran_ref_no is NULL and I_fif_gl_cross_ref_temp_rec.tran_ref_no is NULL))));
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_fif_gl_cross_ref_temp_rec.set_of_books_id,
                                                               I_fif_gl_cross_ref_temp_rec.dept);
      return FALSE;

   when OTHERS then
      if C_FIF_GL_CROSS_REF_LOCK%ISOPEN then
         CLOSE C_FIF_GL_CROSS_REF_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FIF_GL_CROSS_REF_UPD;
------------------------------------------------------------------------------------
FUNCTION EXEC_FIF_GL_CROSS_REF_DEL( O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_fif_gl_cross_ref_temp_rec   IN       FIF_GL_CROSS_REF%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_FIF_GL_CROSS_REF.EXEC_FIF_GL_CROSS_REF_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FIF_GL_CROSS_REF';   
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_FIF_GL_CROSS_REF_LOCK is 
      select 'X'
        from FIF_GL_CROSS_REF
       where set_of_books_id      = I_fif_gl_cross_ref_temp_rec.set_of_books_id
             and dept             = I_fif_gl_cross_ref_temp_rec.dept
             and class            = I_fif_gl_cross_ref_temp_rec.class
             and subclass         = I_fif_gl_cross_ref_temp_rec.subclass
             and tran_code        = I_fif_gl_cross_ref_temp_rec.tran_code
             and location         = I_fif_gl_cross_ref_temp_rec.location
             and cost_retail_flag = I_fif_gl_cross_ref_temp_rec.cost_retail_flag
             and line_type        = I_fif_gl_cross_ref_temp_rec.line_type
             and (tran_code = -1
                  or (tran_code = I_fif_gl_cross_ref_temp_rec.tran_code
                      and (tran_ref_no in (I_fif_gl_cross_ref_temp_rec.tran_ref_no, '-1')
                           or I_fif_gl_cross_ref_temp_rec.tran_ref_no = '-1'
                           or (tran_ref_no is NULL and I_fif_gl_cross_ref_temp_rec.tran_ref_no is NULL))))
         for update nowait;
BEGIN
   open C_FIF_GL_CROSS_REF_LOCK;

   close C_FIF_GL_CROSS_REF_LOCK;

   delete
     from fif_gl_cross_ref
    where set_of_books_id  = I_fif_gl_cross_ref_temp_rec.set_of_books_id
      and dept             = I_fif_gl_cross_ref_temp_rec.dept
      and class            = I_fif_gl_cross_ref_temp_rec.class
      and subclass         = I_fif_gl_cross_ref_temp_rec.subclass
      and tran_code        = I_fif_gl_cross_ref_temp_rec.tran_code
      and location         = I_fif_gl_cross_ref_temp_rec.location
      and cost_retail_flag = I_fif_gl_cross_ref_temp_rec.cost_retail_flag
      and line_type        = I_fif_gl_cross_ref_temp_rec.line_type
      and (tran_code = -1
           or (tran_code = I_fif_gl_cross_ref_temp_rec.tran_code
           and (tran_ref_no in (I_fif_gl_cross_ref_temp_rec.tran_ref_no, '-1')
                or I_fif_gl_cross_ref_temp_rec.tran_ref_no = '-1'
                or (tran_ref_no is NULL and I_fif_gl_cross_ref_temp_rec.tran_ref_no is NULL))));
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_fif_gl_cross_ref_temp_rec.set_of_books_id,
                                                               I_fif_gl_cross_ref_temp_rec.dept);
      return FALSE;

   when OTHERS then
      if C_FIF_GL_CROSS_REF_LOCK%ISOPEN then
         CLOSE C_FIF_GL_CROSS_REF_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FIF_GL_CROSS_REF_DEL;
------------------------------------------------------------------------------
FUNCTION PROCESS_FIF_GL_CROSS_REF( O_error_message IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                                   I_process_id    IN     SVC_FIF_GL_CROSS_REF.PROCESS_ID%TYPE,
                                   I_chunk_id      IN     SVC_FIF_GL_CROSS_REF.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64)                      :='CORESVC_FIF_GL_CROSS_REF.PROCESS_FIF_GL_CROSS_REF';
   L_error                     BOOLEAN;
   L_process_error             BOOLEAN                           := FALSE;
   L_fif_gl_cross_ref_temp_rec FIF_GL_CROSS_REF%ROWTYPE;
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FIF_GL_CROSS_REF';
   L_exists                    BOOLEAN                           := FALSE;
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   FOR rec IN C_SVC_FIF_GL_CROSS_REF(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
       if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                LP_system_options_rec) = FALSE then
          WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
         L_error :=TRUE;
      end if;
      
      LP_dr_col_name(1) :=rec.dr_sequence1;
      LP_dr_col_name(2) :=rec.dr_sequence2;
      LP_dr_col_name(3) :=rec.dr_sequence3;
      LP_dr_col_name(4) :=rec.dr_sequence4;
      LP_dr_col_name(5) :=rec.dr_sequence5;
      LP_dr_col_name(6) :=rec.dr_sequence6;
      LP_dr_col_name(7) :=rec.dr_sequence7;
      LP_dr_col_name(8) :=rec.dr_sequence8;
      LP_dr_col_name(9) :=rec.dr_sequence9;
      LP_dr_col_name(10):=rec.dr_sequence10;
      
      LP_cr_col_name(1) :=rec.cr_sequence1;
      LP_cr_col_name(2) :=rec.cr_sequence2;
      LP_cr_col_name(3) :=rec.cr_sequence3;
      LP_cr_col_name(4) :=rec.cr_sequence4;
      LP_cr_col_name(5) :=rec.cr_sequence5;
      LP_cr_col_name(6) :=rec.cr_sequence6;
      LP_cr_col_name(7) :=rec.cr_sequence7;
      LP_cr_col_name(8) :=rec.cr_sequence8;
      LP_cr_col_name(9) :=rec.cr_sequence9;
      LP_cr_col_name(10):=rec.cr_sequence10;
	  
      LP_dr_col_name(11) :=rec.dr_sequence11;
      LP_dr_col_name(12) :=rec.dr_sequence12;
      LP_dr_col_name(13) :=rec.dr_sequence13;
      LP_dr_col_name(14) :=rec.dr_sequence14;
      LP_dr_col_name(15) :=rec.dr_sequence15;
      LP_dr_col_name(16) :=rec.dr_sequence16;
      LP_dr_col_name(17) :=rec.dr_sequence17;
      LP_dr_col_name(18) :=rec.dr_sequence18;
      LP_dr_col_name(19) :=rec.dr_sequence19;
      LP_dr_col_name(20) :=rec.dr_sequence20;
                                          
      LP_cr_col_name(11) :=rec.cr_sequence11;
      LP_cr_col_name(12) :=rec.cr_sequence12;
      LP_cr_col_name(13) :=rec.cr_sequence13;
      LP_cr_col_name(14) :=rec.cr_sequence14;
      LP_cr_col_name(15) :=rec.cr_sequence15;
      LP_cr_col_name(16) :=rec.cr_sequence16;
      LP_cr_col_name(17) :=rec.cr_sequence17;
      LP_cr_col_name(18) :=rec.cr_sequence18;
      LP_cr_col_name(19) :=rec.cr_sequence19;
      LP_cr_col_name(20) :=rec.cr_sequence20;	  

      
      if rec.action is NULL
         or rec.action NOT IN (action_new,
                               action_mod,
                               action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action in (action_new, action_mod, action_del) then
         if RMS2FIN.FIF_VALIDATION(L_error_message,
                                   L_exists,
                                   rec.set_of_books_id,
                                   rec.dept,
                                   rec.class,
                                   rec.subclass,
                                   rec.location,
                                   rec.tran_code,
                                   rec.tran_ref_no,
                                   rec.cost_retail_flag,
                                   rec.LP_line_type) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        L_error_message);
            L_error :=TRUE;
         end if;

         if L_exists = TRUE and rec.action = action_new then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'Set Of Books,Department,Class,Subclass,Tran Code,Location,Cost/Retail,Line Type',
                        'DUP_RECORD');
            L_error :=TRUE;
         end if;
         if NOT L_exists and rec.action in (action_mod, action_del)
            and rec.set_of_books_id is NOT NULL
            and rec.dept is NOT NULL
            and rec.class is NOT NULL
            and rec.subclass is NOT NULL
            and rec.LP_line_type is NOT NULL
            and rec.location is NOT NULL
            and rec.tran_code is NOT NULL
            and rec.cost_retail_flag is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'Set Of Books,Department,Class,Subclass,Tran Code,Location,Cost/Retail,Line Type',
                        'NO_RECORD');
            L_error :=TRUE;
         end if;        
      end if;
      
      if rec.TRAN_CODE is NOT NULL
         and rec.TRAN_CODE != -1
         and rec.foc_tdc_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TRAN_CODE',
                     'INV_TRAN_CODE');
         L_error :=TRUE;
      end if;
      
      if rec.tran_code =21
         and rec.LP_line_type is NOT NULL
         and rec.foc_fltx_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LINE_TYPE',
                     'INV_LINE_TYPE');
         L_error :=TRUE;
      end if;
      if rec.set_of_books_id is NOT NULL
         and rec.foc_fog_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SET_OF_BOOKS_ID',
                     'INV_SET_OF_BOOKS_ID');
         L_error :=TRUE;
      end if;

      if NOT((rec.tran_code IN ( 37,38,63,64,87,88 )  
              and rec.tran_ref_no is NOT NULL )  
               or rec.tran_code IN  ( 22,23 )  
               or (rec.tran_code NOT IN ( 22,23,37,38,63,64,87,88 )  
              and rec.tran_ref_no is NULL )) then
          WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'Tran Code,Tran Ref No',
                       'INV_TRAN_REF_NO');   
          L_error :=TRUE;
      end if;
      
      if NOT( rec.cost_retail_flag IN  ( 'C','R' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COST_RETAIL_FLAG',
                     'INV_COST_RETAIL_FLAG');
         L_error :=TRUE;
      end if;

      if NVL(LP_system_options_rec.financial_ap,'-1') != 'A' then
         if rec.action IN (action_new, action_mod) 
		        and rec.dr_ccid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DR_CCID',
                        'ENT_DR_ACCT');
            L_error :=TRUE;
         end if;

         if rec.action IN (action_new, action_mod) 
		        and rec.cr_ccid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CR_CCID',
                        'ENT_CR_ACCT');
            L_error :=TRUE;
         end if;
      end if;               

      if PROCESS_FIF_GL_CROSS_REF_VAL(O_error_message,
                                      L_error,
                                      rec)=FALSE then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
      
      if NOT L_error then
         if rec.action=action_new then
            if RMS2FIN.GET_NEXT_ID (O_error_message,
                                    rec.uk_fif_gl_cross_ref_id) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_error :=TRUE;
            end if;
            L_fif_gl_cross_ref_temp_rec.fif_gl_cross_ref_id       := rec.uk_fif_gl_cross_ref_id;
         end if;
         
         if rec.action IN (action_mod,action_del) then
            L_fif_gl_cross_ref_temp_rec.fif_gl_cross_ref_id          := rec.uk_fif_gl_cross_ref_id;
         end if;
         L_fif_gl_cross_ref_temp_rec.set_of_books_id              := rec.set_of_books_id;
         L_fif_gl_cross_ref_temp_rec.dept                         := rec.dept;
         L_fif_gl_cross_ref_temp_rec.class                        := rec.class;
         L_fif_gl_cross_ref_temp_rec.subclass                     := rec.subclass;
         L_fif_gl_cross_ref_temp_rec.location                     := rec.location;
         L_fif_gl_cross_ref_temp_rec.tran_code                    := rec.tran_code;
         L_fif_gl_cross_ref_temp_rec.cost_retail_flag             := rec.cost_retail_flag;
         L_fif_gl_cross_ref_temp_rec.line_type                    := rec.LP_line_type;
         L_fif_gl_cross_ref_temp_rec.tran_ref_no                  := rec.tran_ref_no;
         L_fif_gl_cross_ref_temp_rec.dr_ccid                      := rec.dr_ccid;
         L_fif_gl_cross_ref_temp_rec.dr_sequence1                 := rec.dr_sequence1;
         L_fif_gl_cross_ref_temp_rec.dr_sequence2                 := rec.dr_sequence2;
         L_fif_gl_cross_ref_temp_rec.dr_sequence3                 := rec.dr_sequence3;
         L_fif_gl_cross_ref_temp_rec.dr_sequence4                 := rec.dr_sequence4;
         L_fif_gl_cross_ref_temp_rec.dr_sequence5                 := rec.dr_sequence5;
         L_fif_gl_cross_ref_temp_rec.dr_sequence6                 := rec.dr_sequence6;
         L_fif_gl_cross_ref_temp_rec.dr_sequence7                 := rec.dr_sequence7;
         L_fif_gl_cross_ref_temp_rec.dr_sequence8                 := rec.dr_sequence8;
         L_fif_gl_cross_ref_temp_rec.dr_sequence9                 := rec.dr_sequence9;
         L_fif_gl_cross_ref_temp_rec.dr_sequence10                := rec.dr_sequence10;
         L_fif_gl_cross_ref_temp_rec.cr_ccid                      := rec.cr_ccid;
         L_fif_gl_cross_ref_temp_rec.cr_sequence1                 := rec.cr_sequence1;
         L_fif_gl_cross_ref_temp_rec.cr_sequence2                 := rec.cr_sequence2;
         L_fif_gl_cross_ref_temp_rec.cr_sequence3                 := rec.cr_sequence3;
         L_fif_gl_cross_ref_temp_rec.cr_sequence4                 := rec.cr_sequence4;
         L_fif_gl_cross_ref_temp_rec.cr_sequence5                 := rec.cr_sequence5;
         L_fif_gl_cross_ref_temp_rec.cr_sequence6                 := rec.cr_sequence6;
         L_fif_gl_cross_ref_temp_rec.cr_sequence7                 := rec.cr_sequence7;
         L_fif_gl_cross_ref_temp_rec.cr_sequence8                 := rec.cr_sequence8;
         L_fif_gl_cross_ref_temp_rec.cr_sequence9                 := rec.cr_sequence9;
         L_fif_gl_cross_ref_temp_rec.cr_sequence10                := rec.cr_sequence10;
         L_fif_gl_cross_ref_temp_rec.dr_sequence11                := rec.dr_sequence11;
         L_fif_gl_cross_ref_temp_rec.dr_sequence12                := rec.dr_sequence12;
         L_fif_gl_cross_ref_temp_rec.dr_sequence13                := rec.dr_sequence13;
         L_fif_gl_cross_ref_temp_rec.dr_sequence14                := rec.dr_sequence14;
         L_fif_gl_cross_ref_temp_rec.dr_sequence15                := rec.dr_sequence15;
         L_fif_gl_cross_ref_temp_rec.dr_sequence16                := rec.dr_sequence16;
         L_fif_gl_cross_ref_temp_rec.dr_sequence17                := rec.dr_sequence17;
         L_fif_gl_cross_ref_temp_rec.dr_sequence18                := rec.dr_sequence18;
         L_fif_gl_cross_ref_temp_rec.dr_sequence19                := rec.dr_sequence19;
         L_fif_gl_cross_ref_temp_rec.dr_sequence20                := rec.dr_sequence20;
         L_fif_gl_cross_ref_temp_rec.cr_sequence11                := rec.cr_sequence11;
         L_fif_gl_cross_ref_temp_rec.cr_sequence12                := rec.cr_sequence12;
         L_fif_gl_cross_ref_temp_rec.cr_sequence13                := rec.cr_sequence13;
         L_fif_gl_cross_ref_temp_rec.cr_sequence14                := rec.cr_sequence14;
         L_fif_gl_cross_ref_temp_rec.cr_sequence15                := rec.cr_sequence15;
         L_fif_gl_cross_ref_temp_rec.cr_sequence16                := rec.cr_sequence16;
         L_fif_gl_cross_ref_temp_rec.cr_sequence17                := rec.cr_sequence17;
         L_fif_gl_cross_ref_temp_rec.cr_sequence18                := rec.cr_sequence18;
         L_fif_gl_cross_ref_temp_rec.cr_sequence19                := rec.cr_sequence19;
         L_fif_gl_cross_ref_temp_rec.cr_sequence20                := rec.cr_sequence20;		 

         if rec.action = action_new then
            if EXEC_FIF_GL_CROSS_REF_INS( O_error_message,
                                          L_fif_gl_cross_ref_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_FIF_GL_CROSS_REF_UPD( O_error_message,
                                          L_fif_gl_cross_ref_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_FIF_GL_CROSS_REF_DEL( O_error_message,
                                          L_fif_gl_cross_ref_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_FIF_GL_CROSS_REF%ISOPEN then
         close C_SVC_FIF_GL_CROSS_REF;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FIF_GL_CROSS_REF;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete 
     from svc_fif_gl_cross_ref 
    where process_id = I_process_id;
END;
-------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_FIF_GL_CROSS_REF.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE   :='PS';     
BEGIN
   LP_errors_tab := NEW LP_errors_tab_typ();

   if PROCESS_FIF_GL_CROSS_REF(O_error_message,
                               I_process_id,
                               I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();

   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);

   LP_errors_tab := NEW LP_errors_tab_typ();
   
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
	else
	   L_process_status := 'PE';
	end if;
		
   update svc_process_tracker
		  set status = (CASE
			              when status = 'PE'
			              then 'PE'
                    else L_process_status
                    END),
		      action_date = SYSDATE
		where process_id = I_process_id;

   CLEAR_STAGING_DATA(I_process_id); 
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-----------------------------------------------------------------------------------------
END CORESVC_FIF_GL_CROSS_REF;
/