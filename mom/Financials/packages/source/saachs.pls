
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_ACH_SQL AUTHID CURRENT_USER AS 
-- Created By:  Niloy Ray, 03-SEP-99.
-------------------------------------------------------------------------------
-- Function: MANUAL_ACH_ADJ_EXISTS
-- Purpose : Checks if a given Bank ACH record with a Manual ACH Adjustment
-- 		 amount exists on the Bank ACH table (sa_bank_ach).
------------------------------------------------------------------------------- 
FUNCTION MANUAL_ACH_ADJ_EXISTS(O_error_message   IN OUT VARCHAR2,
                               O_exists          IN OUT BOOLEAN,
                               I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
                               I_business_date   IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: BANK_ACH_EXIST
-- Purpose : Checks if a given Bank ACH record exists on the Bank ACH table 
-- 		 (sa_bank_ach).  
--  		record is found.
------------------------------------------------------------------------------- 
FUNCTION BANK_ACH_EXIST(O_error_message   IN OUT VARCHAR2,
                        O_exists          IN OUT BOOLEAN,
                        I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
			      I_bank_acct_no    IN     SA_BANK_STORE.BANK_ACCT_NO%TYPE,
                        I_business_date   IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: BANK_ACCT_NO_EXISTS
-- Purpose : Checks for the existence of a Partner/Account Number combination on
-- 		 the Bank Store table (sa_bank_store), if the Partner is passed in.  
--  		 Otherwise, checks for the existence of an account number on the Bank
--           Store table.
-------------------------------------------------------------------------------
FUNCTION BANK_ACCT_NO_EXISTS(O_error_message   IN OUT VARCHAR2,
                             O_exists          IN OUT BOOLEAN,
                             I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
                             I_bank_acct_no    IN     SA_BANK_STORE.BANK_ACCT_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    MANUAL_DEPOSIT_ADJ_EXISTS
-- Purpose: Checks if a given Store ACH record with a Manual Deposit Adjustment
-- 		amount exists on the Store ACH table (sa_store_ach).
------------------------------------------------------------------------------- 
FUNCTION MANUAL_DEPOSIT_ADJ_EXISTS(O_error_message  IN OUT VARCHAR2,
                                   O_exists         IN OUT BOOLEAN,
                                   I_partner_id     IN     PARTNER.PARTNER_ID%TYPE,
                                   I_business_date  IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    STORE_ACH_EXIST
-- Purpose: Checks if a given Store ACH record exists on the Store ACH table
--         (sa_store_ach).
------------------------------------------------------------------------------- 
FUNCTION STORE_ACH_EXIST(O_error_message   IN OUT VARCHAR2,
                         O_exists          IN OUT BOOLEAN,
		             I_store           IN     STORE.STORE%TYPE,
                         I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
                         I_business_date   IN     SA_STORE_DAY.BUSINESS_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    GET_MAX_BUS_DATE
-- Purpose: Retrieves the maximum business date on the Store ACH table (sa_store_ach) 
--          where the estimated deposit column has a value.
------------------------------------------------------------------------------- 
FUNCTION GET_MAX_BUS_DATE(O_error_message   IN OUT VARCHAR2,
                          O_business_date   IN OUT SA_STORE_DAY.BUSINESS_DATE%TYPE,
	                    I_store           IN     STORE.STORE%TYPE,
                          I_partner_id      IN     PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    GET_MAX_BUS_DATE
-- Purpose: Retrieves the maximum business date on the Bank ACH table (sa_bank_ach) 
--          where the ACH Amount column has a value.
------------------------------------------------------------------------------- 
FUNCTION GET_MAX_BUS_DATE_BANK(O_error_message   IN OUT VARCHAR2,
                               O_business_date   IN OUT SA_BANK_ACH.BUSINESS_DATE%TYPE,
	                         I_bank_acct_no    IN     SA_BANK_ACH.BANK_ACCT_NO%TYPE,
                               I_partner_id      IN     SA_BANK_ACH.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    CHECK_STORE_EXISTS
-- Purpose: Checks if a given Store exists on the Bank Store ACH table
--          (sa_bank_store).
------------------------------------------------------------------------------- 
FUNCTION CHECK_STORE_EXISTS (O_error_message   IN OUT VARCHAR2,
                             O_exists          IN OUT BOOLEAN,
                             I_store           IN     STORE.STORE%TYPE,
                             I_partner_id      IN     SA_BANK_ACH.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    CHECK_BANK_EXISTS
-- Purpose: Checks if a given Bank exists on the Bank Store ACH table
--          (sa_bank_store).
------------------------------------------------------------------------------- 
FUNCTION CHECK_BANK_EXISTS (O_error_message   IN OUT VARCHAR2,
                            O_exists          IN OUT BOOLEAN,
                            I_store           IN     STORE.STORE%TYPE,
                            I_partner_id      IN     SA_BANK_ACH.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
END;
/
