



CREATE OR REPLACE PACKAGE BODY SA_ITEM_SQL AS
---------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_CHANGE(O_error_message        IN OUT VARCHAR2,
                          I_store                IN     STORE.STORE%TYPE,
                          I_business_date        IN     SA_STORE_DAY.BUSINESS_DATE%TYPE,
                          I_orig_item            IN     SA_TRAN_ITEM.ITEM%TYPE,
                          I_orig_item_type       IN     SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                          I_new_item             IN     ITEM_MASTER.ITEM%TYPE,
                          I_new_item_type        IN     SA_TRAN_ITEM.ITEM_TYPE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program                     VARCHAR2(60) := 'SA_ITEM_SQL.MASS_ITEM_CHANGE';
   L_table                       VARCHAR2(30);
   L_tran_seq_no                 SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_qty                         SA_TRAN_ITEM.QTY%TYPE;
   L_status                      ITEM_LOC.STATUS%TYPE;
   L_item_desc                   ITEM_MASTER.ITEM_DESC%TYPE;
   L_new_item_level              ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_new_tran_level              ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_pack_ind                    ITEM_MASTER.PACK_IND%TYPE;
   L_new_item_parent             ITEM_MASTER.ITEM_PARENT%TYPE;
   L_item_grandparent            ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_short_desc                  ITEM_MASTER.SHORT_DESC%TYPE;
   L_new_dept                    ITEM_MASTER.DEPT%TYPE;
   L_dept_name                   DEPS.DEPT_NAME%TYPE;
   L_new_class                   ITEM_MASTER.CLASS%TYPE;
   L_class_name                  CLASS.CLASS_NAME%TYPE;
   L_new_subclass                ITEM_MASTER.SUBCLASS%TYPE;
   L_subclass_name               SUBCLASS.SUB_NAME%TYPE;   
   L_sellable_ind                ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind               ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type                   ITEM_MASTER.PACK_TYPE%TYPE;
   L_simple_pack_ind             ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
   L_new_waste_type              ITEM_MASTER.WASTE_TYPE%TYPE;
   L_new_waste_pct               ITEM_MASTER.WASTE_PCT%TYPE;
   L_default_waste_pct           ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;
   L_orig_item_level             ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_orig_tran_level             ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_item_loc_exists             BOOLEAN;
   L_new_item_loc_status         ITEM_LOC.STATUS%TYPE;
   L_new_taxable_ind             ITEM_LOC.TAXABLE_IND%TYPE := NULL;
   L_new_standard_uom            UOM_CLASS.UOM%TYPE;
   L_standard_class              UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor                 ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_uom_conversion_factor       ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_new_standard_unit_disc_amt  SA_TRAN_DISC.STANDARD_UNIT_DISC_AMT%TYPE;
   L_new_standard_disc_qty       SA_TRAN_DISC.STANDARD_QTY%TYPE;
   L_standard_uom                UOM_CLASS.UOM%TYPE;
   L_item_seq_no                 SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE;
   L_selling_uom                 SA_TRAN_ITEM.SELLING_UOM%TYPE;
   L_unit_retail                 SA_TRAN_ITEM.UNIT_RETAIL%TYPE;
   L_orig_standard_qty           SA_TRAN_ITEM.QTY%TYPE;
   L_new_standard_qty            SA_TRAN_ITEM.QTY%TYPE;
   L_orig_standard_uom           SA_TRAN_ITEM.STANDARD_UOM%TYPE;
   L_orig_standard_unit_retail   SA_TRAN_ITEM.STANDARD_UNIT_RETAIL%TYPE;
   L_new_standard_unit_retail    SA_TRAN_ITEM.STANDARD_UNIT_RETAIL%TYPE;
   L_discount_seq_no             SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE;
   L_discount_uom                SA_TRAN_ITEM.STANDARD_UOM%TYPE;
   L_unit_discount_amt           SA_TRAN_DISC.UNIT_DISCOUNT_AMT%TYPE;
   L_disc_qty                    SA_TRAN_DISC.QTY%TYPE;
   L_orig_standard_disc_uom      SA_TRAN_ITEM.STANDARD_UOM%TYPE;
   L_new_standard_disc_uom       SA_TRAN_ITEM.STANDARD_UOM%TYPE;
   L_orig_standard_unit_disc_amt SA_TRAN_DISC.STANDARD_UNIT_DISC_AMT%TYPE;
   L_orig_standard_disc_qty      SA_TRAN_DISC.STANDARD_QTY%TYPE;
   L_rev_no                      SA_TRAN_HEAD.REV_NO%TYPE;
   L_store                       SA_STORE_DAY.STORE%TYPE := I_store;
   L_day                         SA_STORE_DAY.DAY%TYPE;
   ---
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = L_store
         and day            = L_day
         for update nowait;

   cursor C_LOCK_ITEM(C_tran_seq_no SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_item
       where tran_seq_no  = C_tran_seq_no
         and store        = L_store
         and day          = L_day
         and (   (I_orig_item_type  = 'ITEM' and item           = I_orig_item)
              or (I_orig_item_type  = 'REF'  and ref_item       = I_orig_item)
              or (I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM') and non_merch_item = I_orig_item))
         for update nowait;

   cursor C_GET_ITEM_RECS(C_tran_seq_no SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE) is
      select item_seq_no,
             selling_uom,
             unit_retail,
             qty,
             standard_uom,
             standard_unit_retail,
             standard_qty
        from sa_tran_item
       where tran_seq_no  = C_tran_seq_no
         and store        = L_store
         and day          = L_day
         and (   (I_orig_item_type  = 'ITEM' and item           = I_orig_item)
              or (I_orig_item_type  = 'REF'  and ref_item       = I_orig_item)
              or (I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM')  and non_merch_item = I_orig_item));

   cursor C_TRAN_SEQ_REV_NO is
      select th.tran_seq_no,
             th.rev_no,
             th.store,
             th.day
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = I_store
         and sd.business_date    = I_business_date
         and exists (select 'x'
                       from sa_tran_item ti
                      where th.tran_seq_no = ti.tran_seq_no
                        and th.store       = ti.store
                        and th.day         = ti.day
                        and (   (ti.item           = I_orig_item and I_orig_item_type = 'ITEM')
                             or (ti.ref_item       = I_orig_item and I_orig_item_type = 'REF')
                             or (ti.non_merch_item = I_orig_item and I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM'))));

   cursor C_LOCK_DISCOUNTS(C_tran_seq_no SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                           C_item_seq_no SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_disc
       where tran_seq_no = C_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = C_item_seq_no
         for update nowait;

   cursor C_GET_DISCOUNT_RECS(C_tran_seq_no SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                              C_item_seq_no SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE) is
      select sd.discount_seq_no,
             sd.unit_discount_amt,
             sd.qty
        from sa_tran_disc sd,
             sa_tran_item si
       where sd.tran_seq_no = C_tran_seq_no
         and sd.store       = si.store
         and sd.day         = si.day
         and si.store       = L_store
         and si.day         = L_day
         and sd.item_seq_no = C_item_seq_no
         and sd.tran_seq_no = si.tran_seq_no
         and sd.item_seq_no = si.item_seq_no;

BEGIN
   ---
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_STORE',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_business_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_BUSINESS_DATE',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_orig_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ORIG_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_orig_item_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ORIG_ITEM_TYPE',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_new_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_NEW_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_new_item_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_NEW_ITEM_TYPE',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_new_item_type in ('ITEM', 'REF') then
      if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                  L_item_desc,
                                  L_new_item_level,
                                  L_new_tran_level,
                                  L_status,
                                  L_pack_ind,
                                  L_new_dept,
                                  L_dept_name,
                                  L_new_class,
                                  L_class_name,
                                  L_new_subclass,
                                  L_subclass_name,                                  
                                  L_sellable_ind,
                                  L_orderable_ind,
                                  L_pack_type,
                                  L_simple_pack_ind,
                                  L_new_waste_type,
                                  L_new_item_parent,
                                  L_item_grandparent,
                                  L_short_desc,
                                  L_new_waste_pct,
                                  L_default_waste_pct,
                                  I_new_item) = FALSE then
         return FALSE;
      end if;
      if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                          L_new_standard_uom,
                                          L_standard_class,
                                          L_conv_factor,
                                          I_new_item,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      if (L_new_item_level < L_new_tran_level) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TRAN_LEVEL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      ---
      if (L_new_item_level = L_new_tran_level) then
         if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                              I_new_item,
                                              I_store,
                                              L_item_loc_exists) = FALSE then
            return FALSE;
         end if;
         if L_item_loc_exists = TRUE then
            if ITEMLOC_ATTRIB_SQL.GET_STATUS_TAXABLE(O_error_message,
                                                     L_new_item_loc_status,
                                                     L_new_taxable_ind,
                                                     I_new_item,
                                                     I_store) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if (L_new_item_level > L_new_tran_level) then
         if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                              L_new_item_parent,
                                              I_store,
                                              L_item_loc_exists) = FALSE then
            return FALSE;
         end if;
         if L_item_loc_exists = TRUE then
            if ITEMLOC_ATTRIB_SQL.GET_STATUS_TAXABLE(O_error_message,
                                                     L_new_item_loc_status,
                                                     L_new_taxable_ind,
                                                     L_new_item_parent,
                                                     I_store) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   end if; -- item type is ITEM or REF
   ---
   if SA_ITEM_SQL.MASS_ERROR_UPDATE(O_error_message,
                                    I_store,
                                    I_business_date,
                                    I_orig_item,
                                    I_orig_item_type) = FALSE then
      return FALSE;
   end if;
   ---
   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no := rec.tran_seq_no;
      L_rev_no      := rec.rev_no;
      L_store       := rec.store;
      L_day         := rec.day;
      ---
      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;
      ---
      L_table := 'SA_TRAN_HEAD';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_TRAN_HEAD','SA_TRAN_HEAD',NULL);
      open C_LOCK_TRAN_HEAD(L_tran_seq_no);
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_TRAN_HEAD','SA_TRAN_HEAD',NULL);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1)
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;
      ---
      L_table := 'SA_TRAN_ITEM';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM', 'SA_TRAN_ITEM', NULL);
      open C_LOCK_ITEM(L_tran_seq_no);
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM', 'SA_TRAN_ITEM', NULL);
      close C_LOCK_ITEM;
      ---
      for rec in C_GET_ITEM_RECS(L_tran_seq_no) loop
         L_item_seq_no               := rec.item_seq_no;
         L_selling_uom               := rec.selling_uom;
         L_unit_retail               := rec.unit_retail;
         L_qty                       := rec.qty;
         L_orig_standard_uom         := rec.standard_uom;
         L_orig_standard_unit_retail := rec.standard_unit_retail;
         L_orig_standard_qty         := rec.standard_qty;
         ---
         if I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM') and
               I_new_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM') then
            -- Original non-merchandise item and new non-merchandise item
            L_new_standard_uom         := L_orig_standard_uom;
            L_new_standard_unit_retail := L_orig_standard_unit_retail;
            L_new_standard_qty         := L_orig_standard_qty;
         elsif I_orig_item_type in ('ITEM', 'REF') and I_new_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM') then
            -- Original merchandise item and new non-merchandise item
            L_new_standard_uom         := L_selling_uom;
            L_new_standard_unit_retail := L_unit_retail;
            L_new_standard_qty         := L_qty;
            ---
            SQL_LIB.SET_MARK('OPEN','C_LOCK_DISCOUNTS','SA_TRAN_DISC',NULL);
            open C_LOCK_DISCOUNTS(L_tran_seq_no,
                                  L_item_seq_no);
            SQL_LIB.SET_MARK('CLOSE','C_LOCK_DISCOUNTS','SA_TRAN_DISC',NULL);
            close C_LOCK_DISCOUNTS;
            ---
            for rec in C_GET_DISCOUNT_RECS(L_tran_seq_no,
                                           L_item_seq_no) loop
               L_discount_seq_no             := rec.discount_seq_no;
               L_unit_discount_amt           := rec.unit_discount_amt;
               L_disc_qty                    := rec.qty;
               ---
               L_new_standard_unit_disc_amt := L_unit_discount_amt;
               L_new_standard_disc_qty      := L_disc_qty;
               ---
               L_table := 'SA_TRAN_DISC';
               update sa_tran_disc
                  set standard_unit_disc_amt = L_new_standard_unit_disc_amt,
                      standard_qty           = L_new_standard_disc_qty
                where tran_seq_no     = L_tran_seq_no
                  and store           = L_store
                  and day             = L_day
                  and item_seq_no     = L_item_seq_no
                  and discount_seq_no = L_discount_seq_no;
            end loop;
         elsif I_new_item_type in ('ITEM', 'REF') then
            -- Any original item type and new merchandise item
            if L_orig_standard_uom = L_new_standard_uom then
               L_new_standard_unit_retail := L_orig_standard_unit_retail;
               L_new_standard_qty         := L_orig_standard_qty;
            else
               if UOM_SQL.CONVERT(O_error_message,
                                  L_uom_conversion_factor,
                                  L_new_standard_uom,
                                  1,
                                  L_selling_uom,
                                  I_new_item,
                                  NULL,
                                  NULL) = FALSE then
                  return FALSE;
               end if;
               if (L_uom_conversion_factor = 0) then
                  O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                        L_orig_standard_uom,
                                                        L_new_standard_uom,
                                                        NULL);
                  return FALSE;
               else
                  L_new_standard_unit_retail := (L_unit_retail / L_uom_conversion_factor);
                  L_new_standard_qty         := (L_qty * L_uom_conversion_factor);
               end if;
               ---
               SQL_LIB.SET_MARK('OPEN','C_LOCK_DISCOUNTS','SA_TRAN_DISC',NULL);
               open C_LOCK_DISCOUNTS(L_tran_seq_no,
                                     L_item_seq_no);
               SQL_LIB.SET_MARK('CLOSE','C_LOCK_DISCOUNTS','SA_TRAN_DISC',NULL);
               close C_LOCK_DISCOUNTS;
               ---
               for rec in C_GET_DISCOUNT_RECS(L_tran_seq_no,
                                              L_item_seq_no) loop
                  L_discount_seq_no             := rec.discount_seq_no;
                  L_unit_discount_amt           := rec.unit_discount_amt;
                  L_disc_qty                    := rec.qty;
                  ---
                  L_new_standard_unit_disc_amt := (L_unit_discount_amt / L_uom_conversion_factor);
                  L_new_standard_disc_qty      := (L_disc_qty * L_uom_conversion_factor);
                  ---
                  L_table := 'SA_TRAN_DISC';
                  update sa_tran_disc
                     set standard_unit_disc_amt = L_new_standard_unit_disc_amt,
                         standard_qty           = L_new_standard_disc_qty
                   where tran_seq_no     = L_tran_seq_no
                     and store           = L_store
                     and day             = L_day
                     and item_seq_no     = L_item_seq_no
                     and discount_seq_no = L_discount_seq_no;
               end loop;
            end if;
         end if;
         ---
         L_table := 'SA_TRAN_ITEM';
         ---
         if I_new_item_type = 'ITEM' then
            update sa_tran_item
               set item                 = I_new_item,
                   ref_item             = NULL,
                   non_merch_item       = NULL,
                   item_type            = I_new_item_type,
                   dept                 = L_new_dept,
                   class                = L_new_class,
                   subclass             = L_new_subclass,
                   tax_ind              = NVL(L_new_taxable_ind, tax_ind),
                   waste_type           = L_new_waste_type,
                   waste_pct            = L_new_waste_pct,
                   standard_uom         = L_new_standard_uom,
                   standard_unit_retail = L_new_standard_unit_retail,
                   standard_qty         = L_new_standard_qty
             where tran_seq_no          = L_tran_seq_no
               and item_seq_no          = L_item_seq_no
               and store                = L_store
               and day                  = L_day;
         elsif I_new_item_type = 'REF' then
            update sa_tran_item
               set ref_item             = I_new_item,
                   item                 = L_new_item_parent,
                   non_merch_item       = NULL,
                   item_type            = I_new_item_type,
                   dept                 = L_new_dept,
                   class                = L_new_class,
                   subclass             = L_new_subclass,
                   tax_ind              = NVL(L_new_taxable_ind, tax_ind),
                   waste_type           = L_new_waste_type,
                   waste_pct            = L_new_waste_pct,
                   standard_uom         = L_new_standard_uom,
                   standard_unit_retail = L_new_standard_unit_retail,
                   standard_qty         = L_new_standard_qty
             where tran_seq_no          = L_tran_seq_no
               and item_seq_no          = L_item_seq_no
               and store                = L_store
               and day                  = L_day;
         elsif I_new_item_type NOT in ('GCN', 'ERR', 'TERM') then -- Non-merchandise item
            update sa_tran_item
               set item                 = NULL,
                   ref_item             = NULL,
                   non_merch_item       = I_new_item,
                   item_type            = I_new_item_type,
                   dept                 = L_new_dept,
                   class                = L_new_class,
                   subclass             = L_new_subclass,
                   tax_ind              = NVL(L_new_taxable_ind, tax_ind),
                   waste_type           = L_new_waste_type,
                   waste_pct            = L_new_waste_pct,
                   standard_uom         = L_new_standard_uom,
                   standard_unit_retail = L_new_standard_unit_retail,
                   standard_qty         = L_new_standard_qty
             where tran_seq_no          = L_tran_seq_no
               and item_seq_no          = L_item_seq_no
               and store                = L_store
               and day                  = L_day;
         end if;
      end loop; -- C_GET_ITEM_RECS
   end loop; -- C_TRAN_SEQ_REV_NO
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(L_tran_seq_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_ITEM_CHANGE;
---------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_DISCOUNTS(O_error_message  IN OUT VARCHAR2,
                               O_total_disc_amt IN OUT SA_TRAN_DISC.UNIT_DISCOUNT_AMT%TYPE,
                               I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                               I_item_seq_no    IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                               I_store          IN     SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                               I_day            IN     SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'SA_ITEM_SQL.DELETE_ITEM_DISCOUNTS';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_SUM_DISCOUNTS is
      select SUM(NVL(unit_discount_amt,0)) * SUM(NVL(qty,0))
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no;

   cursor C_LOCK_TRAN_DISC is
      select 'x'
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no
      for update nowait;

   cursor C_LOCK_ERROR is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and key_value_1 = I_item_seq_no
         and rec_type = 'IDISC'
      for update nowait;
BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   if I_tran_seq_no is NOT NULL and I_item_seq_no is NOT NULL then

      open C_SUM_DISCOUNTS;
      fetch C_SUM_DISCOUNTS into O_total_disc_amt;
      close C_SUM_DISCOUNTS;
      ---
      open C_LOCK_TRAN_DISC;
      close C_LOCK_TRAN_DISC;
      ---
      delete from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no;
      ---
      open C_LOCK_ERROR;
      close C_LOCK_ERROR;
      ---
      delete from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and key_value_1 = I_item_seq_no
         and rec_type = 'IDISC';
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ITEM_DISCOUNTS;
---------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_IGTAX(O_error_message  IN OUT VARCHAR2,
                           I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_item_seq_no    IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                           I_store          IN     SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                           I_day            IN     SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program VARCHAR2(64)            := 'SA_ITEM_SQL.DELETE_ITEM_IGTAX';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_LOCK_TRAN_IGTAX is
      select 'x'
        from sa_tran_igtax
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no
         for update nowait;

   cursor C_LOCK_ERROR is
      select 'x'
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and key_value_1 = I_item_seq_no
         and rec_type    = 'IGTAX'
         for update nowait;
BEGIN
   if I_tran_seq_no is  NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_program,
                                             I_tran_seq_no,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_item_seq_no is  NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_program,
                                             I_item_seq_no,
                                             NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_TRAN_IGTAX',
                    'SA_TRAN_IGTAX',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)); 
   open C_LOCK_TRAN_IGTAX;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_TRAN_IGTAX',
                    'SA_TRAN_IGTAX',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)); 
   close C_LOCK_TRAN_IGTAX;
   ---
   delete from sa_tran_igtax
    where tran_seq_no = I_tran_seq_no
      and store       = L_store
      and day         = L_day
      and item_seq_no = I_item_seq_no;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ERROR',
                    'SA_ERROR',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)); 
   open C_LOCK_ERROR;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ERROR',
                    'SA_ERROR',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)); 
   close C_LOCK_ERROR;
   ---
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store       = L_store
      and day         = L_day
      and key_value_1 = I_item_seq_no
      and rec_type = 'IGTAX';
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ITEM_IGTAX;
-----------------------------------------------------------------------------------------
FUNCTION MASS_ERROR_UPDATE(O_error_message         IN OUT VARCHAR2,
                           I_store                 IN     SA_STORE_DAY.STORE%TYPE,
                           I_business_date         IN     SA_STORE_DAY.BUSINESS_DATE%TYPE,
                           I_orig_item             IN     SA_TRAN_ITEM.ITEM%TYPE,
                           I_orig_item_type        IN     SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                           I_day                   IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS
   ---
   L_table             VARCHAR2(30) := 'SA_ERROR';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_locked, -54);
   L_program           VARCHAR2(50) := 'SA_ITEM_SQL.MASS_ERROR_UPDATE';
   L_store_day_seq_no  SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE;
   L_exists            BOOLEAN;
   L_store             SA_STORE_DAY.STORE%TYPE := I_store;
   L_day               SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_LOCK_ERROR is
      select 'x'
        from sa_error
       where store_day_seq_no = L_store_day_seq_no
         and store            = L_store
         and day              = L_day
         and error_code in('SKU_NOT_FOUND','UPC_NOT_FOUND','NON_MERCH_ITEM_NOT_FOUND','INVLD_SELLING_UOM','INVLD_DISCOUNT_UOM')
         and (   rec_type = 'TITEM'
              or rec_type = 'IDISC')
         for update nowait;

   cursor C_LOCK_TRAN_ITEM is
      select 'x'
        from sa_tran_item ti
       where exists (select 'x'
                       from sa_error se
                      where se.tran_seq_no      = ti.tran_seq_no
                        and se.store            = ti.store
                        and se.day              = ti.day
                        and se.key_value_1      = ti.item_seq_no
                        and se.store_day_seq_no = L_store_day_seq_no
                        and se.store            = L_store
                        and se.day              = L_day
                        and se.orig_value       = I_orig_item
                        and se.rec_type         = 'TITEM'
                        and se.error_code in ('SKU_NOT_FOUND','UPC_NOT_FOUND','NON_MERCH_ITEM_NOT_FOUND','INVLD_SELLING_UOM'))
         and (   (I_orig_item_type  = 'ITEM' and ti.item           = I_orig_item)
              or (I_orig_item_type  = 'REF'  and ti.ref_item       = I_orig_item)
              or (I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM') and ti.non_merch_item = I_orig_item))
         for update nowait;              


   cursor C_LOCK_TRAN_DISC is
      select 'x'
        from sa_tran_disc td
       where exists (select 'x'
                       from sa_tran_item ti
                      where td.tran_seq_no = ti.tran_seq_no
                        and td.store       = ti.store
                        and td.day         = ti.day
                        and td.item_seq_no = ti.item_seq_no
                        and (   (ti.item           = I_orig_item and I_orig_item_type in ('ITEM', 'REF'))
                             or (ti.ref_item       = I_orig_item and I_orig_item_type in ('ITEM', 'REF'))
                             or (ti.non_merch_item = I_orig_item and I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM')))
                        and exists (select 'x'
                                      from sa_error se
                                     where se.tran_seq_no      = ti.tran_seq_no
                                       and se.store            = ti.store
                                       and se.day              = ti.day
                                       and se.key_value_1      = ti.item_seq_no
                                       and se.store_day_seq_no = L_store_day_seq_no
                                       and se.store            = L_store
                                       and se.day              = L_day
                                       and se.orig_value       = I_orig_item
                                       and se.rec_type         = 'TITEM'
                                       and se.error_code in ('SKU_NOT_FOUND','UPC_NOT_FOUND','NON_MERCH_ITEM_NOT_FOUND','INVLD_SELLING_UOM'))
                        and exists (select 'x'
                                      from sa_error se
                                     where se.tran_seq_no      = td.tran_seq_no
                                       and se.store            = td.store
                                       and se.day              = td.day
                                       and se.key_value_1      = td.item_seq_no
                                       and se.key_value_2      = td.discount_seq_no
                                       and se.store_day_seq_no = L_store_day_seq_no
                                       and se.store            = L_store
                                       and se.day              = L_day
                                       and se.rec_type         = 'IDISC'
                                       and se.error_code       = 'INVLD_DISCOUNT_UOM'))
         for update nowait;
BEGIN
   if STORE_DAY_SQL.GET_STORE_DAY_SEQ(O_error_message,
                                      L_store_day_seq_no,
                                      L_exists,
                                      I_store,
                                      I_business_date) = FALSE then
      return FALSE;
   else
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           L_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ERROR','SA_ERROR',NULL);
   open C_LOCK_ERROR;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ERROR','SA_ERROR',NULL);
   close C_LOCK_ERROR;

   SQL_LIB.SET_MARK('OPEN','C_LOCK_TRAN_DISC','SA_TRAN_DISC',NULL);
   open C_LOCK_TRAN_DISC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TRAN_DISC','SA_TRAN_DISC',NULL);
   close C_LOCK_TRAN_DISC;

   update sa_tran_disc td
      set error_ind = 'N'
    where exists (select 'x'
                    from sa_tran_item ti
                   where td.tran_seq_no = ti.tran_seq_no
                     and td.store       = ti.store
                     and td.day         = ti.day
                     and ti.store       = L_store
                     and ti.day         = L_day
                     and td.item_seq_no = ti.item_seq_no
                     and (   (ti.item           = I_orig_item and I_orig_item_type in ('ITEM', 'REF'))
                          or (ti.ref_item       = I_orig_item and I_orig_item_type in ('ITEM', 'REF'))
                          or (ti.non_merch_item = I_orig_item and I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM')))
                     and exists (select 'x'
                                   from sa_error se
                                  where se.tran_seq_no      = ti.tran_seq_no
                                    and se.store            = ti.store
                                    and se.day              = ti.day
                                    and se.key_value_1      = ti.item_seq_no
                                    and se.store_day_seq_no = L_store_day_seq_no
                                    and se.orig_value       = I_orig_item
                                    and se.rec_type         = 'TITEM'
                                    and se.error_code in ('SKU_NOT_FOUND','UPC_NOT_FOUND','NON_MERCH_ITEM_NOT_FOUND','INVLD_SELLING_UOM'))
                     and exists (select 'x'
                                   from sa_error se
                                  where se.tran_seq_no      = td.tran_seq_no
                                    and se.store            = td.store
                                    and se.day              = td.day
                                    and se.key_value_1      = td.item_seq_no
                                    and se.key_value_2      = td.discount_seq_no
                                    and se.store_day_seq_no = L_store_day_seq_no
                                    and se.rec_type         = 'IDISC'
                                    and se.error_code       = 'INVLD_DISCOUNT_UOM'));

   SQL_LIB.SET_MARK('OPEN','C_LOCK_TRAN_ITEM','SA_TRAN_ITEM',NULL);
   open C_LOCK_TRAN_ITEM;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TRAN_ITEM','SA_TRAN_ITEM',NULL);
   close C_LOCK_TRAN_ITEM;

   update sa_tran_item sti
      set error_ind = 'N'
    where exists (select 'x'
                    from sa_error se
                   where se.tran_seq_no      = sti.tran_seq_no
                     and se.store            = sti.store
                     and se.day              = sti.day
                     and se.key_value_1      = sti.item_seq_no
                     and se.store_day_seq_no = L_store_day_seq_no
                     and se.orig_value       = I_orig_item
                     and se.rec_type         = 'TITEM'
                     and se.error_code in ('SKU_NOT_FOUND','UPC_NOT_FOUND','NON_MERCH_ITEM_NOT_FOUND','INVLD_SELLING_UOM'))
         and (   (I_orig_item_type  = 'ITEM' and sti.item           = I_orig_item)
              or (I_orig_item_type  = 'REF'  and sti.ref_item       = I_orig_item)
              or (I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM') and sti.non_merch_item = I_orig_item));

   delete
     from sa_error s1
    where s1.error_code = 'INVLD_DISCOUNT_UOM'
      and s1.rec_type   = 'IDISC'
      and exists (select 'x'
                    from sa_error s2
                   where s2.store_day_seq_no = s1.store_day_seq_no
                     and s1.store            = s2.store
                     and s1.day              = s2.day
                     and s2.key_value_1      = s1.key_value_1
                     and s2.store_day_seq_no = L_store_day_seq_no
                     and s2.store            = L_store
                     and s2.day              = L_day
                     and s2.error_code in ('SKU_NOT_FOUND','UPC_NOT_FOUND','NON_MERCH_ITEM_NOT_FOUND','INVLD_SELLING_UOM')
                     and s2.rec_type         = 'TITEM'
                     and s2.orig_value       = I_orig_item
                     and exists (select 'x'
                                   from sa_tran_item sti
                                  where s2.tran_seq_no      = sti.tran_seq_no
                                    and s2.store            = sti.store
                                    and s2.day              = sti.day
                                    and s2.key_value_1      = sti.item_seq_no
                                    and s2.store_day_seq_no = L_store_day_seq_no
                                    and (   (I_orig_item_type  = 'ITEM' and sti.item           = I_orig_item)
                                         or (I_orig_item_type  = 'REF'  and sti.ref_item       = I_orig_item)
                                         or (I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM') and sti.non_merch_item = I_orig_item))
                                        )                     
                  );

   delete
     from sa_error se
    where se.store_day_seq_no = L_store_day_seq_no
      and se.store            = L_store
      and se.day              = L_day
      and se.error_code in('SKU_NOT_FOUND','UPC_NOT_FOUND','NON_MERCH_ITEM_NOT_FOUND','INVLD_SELLING_UOM')
      and se.rec_type   = 'TITEM'
      and se.orig_value = I_orig_item
      and exists (select 'x'
                    from sa_tran_item sti
                   where se.tran_seq_no      = sti.tran_seq_no
                     and se.store            = sti.store
                     and se.day              = sti.day
                     and se.key_value_1      = sti.item_seq_no
                     and se.store_day_seq_no = L_store_day_seq_no
                     and (   (I_orig_item_type  = 'ITEM' and sti.item           = I_orig_item)
                          or (I_orig_item_type  = 'REF'  and sti.ref_item       = I_orig_item)
                          or (I_orig_item_type NOT in ('ITEM', 'REF', 'GCN', 'ERR', 'TERM') and sti.non_merch_item = I_orig_item))
                         );

   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(L_store_day_seq_no),
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                              to_char(SQLCODE));
      return FALSE;
END MASS_ERROR_UPDATE;
-----------------------------------------------------------------------------------------
FUNCTION VALID_TRAN_ITEM(O_error_message IN OUT VARCHAR2,
                         O_valid         IN OUT BOOLEAN,
                         I_store         IN     STORE.STORE%TYPE,
                         I_business_date IN     SA_STORE_DAY.BUSINESS_DATE%TYPE,
                         I_tran_seq_no   IN     SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                         I_item_type     IN     SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                         I_item          IN     SA_TRAN_ITEM.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program    VARCHAR2(50) := 'SA_ITEM_SQL.VALID_TRAN_ITEM';
   L_desc       ITEM_MASTER.ITEM_DESC%TYPE;
   L_status     ITEM_MASTER.STATUS%TYPE;
   L_item_level ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_dummy      VARCHAR2(1) := NULL;
   ---
   cursor C_VALID_ITEM is
      select 'x'
        from sa_tran_item ti,
             sa_tran_head th,
             sa_store_day sd
       where (   (I_item_type  = 'ITEM' and I_item = ti.item)
              or (I_item_type  = 'REF'  and I_item = ti.ref_item)
              or (I_item_type NOT in ('GCN', 'ERR', 'TERM') and I_item = ti.non_merch_item))
         and (   I_tran_seq_no is NULL
              or I_tran_seq_no = ti.tran_seq_no)
         and ti.tran_seq_no      = th.tran_seq_no
         and ti.store            = th.store
         and ti.day              = th.day
         and th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and (   I_store is NULL
              or I_store = sd.store)
         and (   I_business_date is NULL
              or I_business_date = sd.business_date);
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'I_ITEM',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   if I_item_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'I_ITEM_TYPE',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item_type in ('ITEM', 'REF') then
      if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                  L_desc,
                                  L_status,
                                  L_item_level,
                                  L_tran_level,
                                  I_item) = FALSE then
         return FALSE;
      end if;
      ---
      if (L_status != 'A') then
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_STATUS',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      ---
      if (L_item_level < L_tran_level) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TRAN_LEVEL',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_VALID_ITEM','SA_TRAN_ITEM, SA_TRAN_HEAD, SA_STORE_DAY', NULL);
   open C_VALID_ITEM;
   SQL_LIB.SET_MARK('FETCH','C_VALID_ITEM','SA_TRAN_ITEM, SA_TRAN_HEAD, SA_STORE_DAY', NULL);
   fetch C_VALID_ITEM into L_dummy;
   if C_VALID_ITEM%NOTFOUND then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRAN_ITEM',
                                            I_item,
                                            NULL,
                                            NULL);
   else
      O_valid := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_VALID_ITEM','SA_TRAN_ITEM, SA_TRAN_HEAD, SA_STORE_DAY', NULL);
   close C_VALID_ITEM;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_TRAN_ITEM;
-----------------------------------------------------------------------------------------
FUNCTION UPDATE_DISCOUNT_UOM(O_error_message     IN OUT VARCHAR2,
                             O_records_updated   IN OUT BOOLEAN,
                             I_tran_seq_no       IN     SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                             I_item_seq_no       IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                             I_selling_uom       IN     SA_TRAN_ITEM.SELLING_UOM%TYPE,
                             I_conversion_factor IN     NUMBER,
                             I_store             IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day               IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   ---
   L_program    VARCHAR2(50) := 'SA_ITEM_SQL.UPDATE_DISCOUNT_UOM';
   L_table                      VARCHAR2(30) := 'sa_tran_disc';
   RECORD_LOCKED                EXCEPTION;
   PRAGMA                       EXCEPTION_INIT(Record_locked, -54);
   L_new_standard_unit_disc_amt SA_TRAN_DISC.UNIT_DISCOUNT_AMT%TYPE;
   L_discount_seq_no            SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE;
   L_new_standard_qty           SA_TRAN_DISC.STANDARD_QTY%TYPE;
   L_store                      SA_STORE_DAY.STORE%TYPE := I_store;
   L_day                        SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_LOCK_DISCOUNT_INFO is
      select 'x'
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no
         for update nowait;

   cursor C_GET_DISCOUNT_INFO is
      select discount_seq_no,
             qty,
             unit_discount_amt,
             standard_qty,
             standard_unit_disc_amt
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no;
   ---

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_tran_seq_no is NULL or
      I_item_seq_no is NULL or
      I_selling_uom is NULL or
      I_conversion_factor is NULL then

      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tran_seq_no
                                             I_item_seq_no
                                             I_selling_uom
                                             I_conversion_factor',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;

   end if;
   ---

   SQL_LIB.SET_MARK('OPEN','C_LOCK_DISCOUNT_INFO','SA_TRAN_DISC', NULL);
   open C_LOCK_DISCOUNT_INFO;

   SQL_LIB.SET_MARK('CLOSE','C_LOCK_DISCOUNT_INFO','SA_TRAN_DISC', NULL);
   close C_LOCK_DISCOUNT_INFO;

   O_records_updated := FALSE;

   for rec in C_GET_DISCOUNT_INFO loop

      L_new_standard_unit_disc_amt := (rec.unit_discount_amt / I_conversion_factor);
      L_new_standard_qty           := (rec.qty * I_conversion_factor);
      L_discount_seq_no            := rec.discount_seq_no;

      update sa_tran_disc
         set standard_unit_disc_amt = L_new_standard_unit_disc_amt,
             standard_qty           = L_new_standard_qty
       where tran_seq_no            = I_tran_seq_no
         and store                  = L_store
         and day                    = L_day
         and item_seq_no            = I_item_seq_no
         and discount_seq_no        = L_discount_seq_no;

      ---
      if O_records_updated = FALSE then
         O_records_updated := TRUE;
      end if;
      ---

   end loop;

return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_tran_seq_no,
                                            I_item_seq_no);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_DISCOUNT_UOM;
-----------------------------------------------------------------------------------------
FUNCTION GET_UOM(O_error_message     IN OUT VARCHAR2,
                 O_standard_uom      IN OUT SA_TRAN_ITEM.STANDARD_UOM%TYPE,
                 O_selling_uom       IN OUT SA_TRAN_ITEM.SELLING_UOM%TYPE,
                 I_tran_seq_no       IN     SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                 I_item_seq_no       IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                 I_store             IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                 I_day               IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)

   RETURN BOOLEAN IS
   ---
   L_program    VARCHAR2(50) := 'SA_ITEM_SQL.GET_UOM';
   L_store      SA_STORE_DAY.STORE%TYPE := I_store;
   L_day        SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_UOM_INFO is
      select standard_uom, selling_uom
        from sa_tran_item
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no;

BEGIN
   if I_tran_seq_no is NULL or I_item_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tran_seq_no
                                             I_item_seq_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   else
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_UOM_INFO','SA_TRAN_ITEM', NULL);
   open C_GET_UOM_INFO;

   SQL_LIB.SET_MARK('FETCH','C_GET_UOM_INFO','SA_TRAN_ITEM', NULL);
   fetch C_GET_UOM_INFO into O_standard_uom, O_selling_uom;

   SQL_LIB.SET_MARK('CLOSE','C_GET_UOM_INFO','SA_TRAN_ITEM', NULL);
   close C_GET_UOM_INFO;

   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_UOM;
-----------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_IGTAX(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_total_igtax       IN OUT SA_TRAN_IGTAX.TOTAL_IGTAX_AMT%TYPE,
                         I_tran_seq_no       IN     SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                         I_item_seq_no       IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                         I_store             IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                         I_day               IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   ---
   L_program    VARCHAR2(64) := 'SA_ITEM_SQL.GET_TOTAL_IGTAX';
   L_store      SA_STORE_DAY.STORE%TYPE := I_store;
   L_day        SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_TOTAL_TAX is
      select NVL(SUM(total_igtax_amt),0)
        from sa_tran_igtax
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no;

BEGIN
     ---
   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_item_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TOTAL_TAX',
                    'SA_TRAN_IGTAX',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no));
   open C_GET_TOTAL_TAX;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TOTAL_TAX',
                    'SA_TRAN_IGTAX',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no));
   fetch C_GET_TOTAL_TAX into O_total_igtax;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TOTAL_TAX',
                    'SA_TRAN_IGTAX',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no));
   close C_GET_TOTAL_TAX;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_IGTAX;
-----------------------------------------------------------------------------------------
END SA_ITEM_SQL;
/
