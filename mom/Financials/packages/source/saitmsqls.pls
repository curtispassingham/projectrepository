



CREATE OR REPLACE PACKAGE SA_ITEM_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------
-- Function Name: MASS_ITEM_CHANGE
-- Purpose:       This function will be called to perform a mass changed of an item across
--                a store day.
---------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_CHANGE(O_error_message        IN OUT VARCHAR2,
                          I_store                IN     STORE.STORE%TYPE,
                          I_business_date        IN     SA_STORE_DAY.BUSINESS_DATE%TYPE,
                          I_orig_item            IN     SA_TRAN_ITEM.ITEM%TYPE,
                          I_orig_item_type       IN     SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                          I_new_item             IN     ITEM_MASTER.ITEM%TYPE,
                          I_new_item_type        IN     SA_TRAN_ITEM.ITEM_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function Name: DELETE_ITEM_DISCOUNTS
--- Purpose      : Deletes discount records that correspond to the passed item record.
----------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_DISCOUNTS(O_error_message  IN OUT VARCHAR2,
                               O_total_disc_amt IN OUT SA_TRAN_DISC.UNIT_DISCOUNT_AMT%TYPE,
                               I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                               I_item_seq_no    IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                               I_store          IN     SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                               I_day            IN     SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function Name: DELETE_ITEM_IGTAX
--- Purpose      : Deletes IGTAX records that correspond to the passed item record.
----------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_IGTAX(O_error_message  IN OUT VARCHAR2,
                           I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           I_item_seq_no    IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                           I_store          IN     SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                           I_day            IN     SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--- Function Name: MASS_ERROR_UPDATE
--- Purpose      : Deletes all 'Invalid SKU', 'Invalid UPC', 'Invalid selling UOM', and 'Invalid
---                discount UOM' errors from the sa_error table for the item once the user
---                re-validates the item or changes the item.
------------------------------------------------------------------------------------------
FUNCTION MASS_ERROR_UPDATE(O_error_message         IN OUT VARCHAR2,
                           I_store                 IN     SA_STORE_DAY.STORE%TYPE,
                           I_business_date         IN     SA_STORE_DAY.BUSINESS_DATE%TYPE,
                           I_orig_item             IN     SA_TRAN_ITEM.ITEM%TYPE,
                           I_orig_item_type        IN     SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                           I_day                   IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--- Function Name:  VALID_TRAN_ITEM
--- Purpose:        Determines if an inputted item exists on a transaction.
------------------------------------------------------------------------------------------
FUNCTION VALID_TRAN_ITEM(O_error_message IN OUT VARCHAR2,
                         O_valid         IN OUT BOOLEAN,
                         I_store         IN     STORE.STORE%TYPE,
                         I_business_date IN     SA_STORE_DAY.BUSINESS_DATE%TYPE,
                         I_tran_seq_no   IN     SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                         I_item_type     IN     SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                         I_item          IN     SA_TRAN_ITEM.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--- Function Name:  UPDATE_DISCOUNT_UOM
--- Purpose:        Updates discount uom on sa_tran_disc
------------------------------------------------------------------------------------------

FUNCTION UPDATE_DISCOUNT_UOM(O_error_message     IN OUT VARCHAR2,
                             O_records_updated   IN OUT BOOLEAN,
                             I_tran_seq_no       IN     SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                             I_item_seq_no       IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                             I_selling_uom       IN     SA_TRAN_ITEM.SELLING_UOM%TYPE,
                             I_conversion_factor IN     NUMBER,
                             I_store             IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day               IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
RETURN BOOLEAN;

------------------------------------------------------------------------------------------
--- Function Name:  GET_UOM
--- Purpose:        Gets the standard and selling uom.
------------------------------------------------------------------------------------------
FUNCTION GET_UOM(O_error_message     IN OUT VARCHAR2,
                 O_standard_uom      IN OUT SA_TRAN_ITEM.STANDARD_UOM%TYPE,
                 O_selling_uom       IN OUT SA_TRAN_ITEM.SELLING_UOM%TYPE,
                 I_tran_seq_no       IN     SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                 I_item_seq_no       IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                 I_store             IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                 I_day               IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)

   RETURN BOOLEAN;

------------------------------------------------------------------------------------------
--- Function Name:  GET_TOTAL_IGTAX
--- Purpose:        Gets the Total IGTax for the item.
------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_IGTAX(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_total_igtax       IN OUT SA_TRAN_IGTAX.TOTAL_IGTAX_AMT%TYPE,
                         I_tran_seq_no       IN     SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                         I_item_seq_no       IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                         I_store             IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                         I_day               IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)

   RETURN BOOLEAN;
------------------------------------------------------------------------------------------

END SA_ITEM_SQL;
/
