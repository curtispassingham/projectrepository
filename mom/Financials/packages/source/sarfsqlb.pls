CREATE OR REPLACE PACKAGE BODY SA_REF_SQL AS
-----------------------------------------------------------------------------------------
FUNCTION GET_REF_LABELS(
                  O_error_message   IN OUT VARCHAR2,
         O_ref_no1      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no2      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no3      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no4      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no5      IN OUT CODE_DETAIL.CODE_DESC%TYPE,     
         O_ref_no6      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no7      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no8      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no9      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no10     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no11     IN OUT CODE_DETAIL.CODE_DESC%TYPE,        
         O_ref_no12     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no13     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no14     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no15     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no16     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                  O_ref_no17     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no18     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no19     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no20     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no21     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no22     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no23     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no24     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no25     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no26     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no27     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                  I_tran_type    IN     SA_REFERENCE.TRAN_TYPE%TYPE,
         I_sub_tran_type   IN     SA_REFERENCE.SUB_TRAN_TYPE%TYPE,
         I_reason_code  IN     SA_REFERENCE.REASON_CODE%TYPE)
    RETURN BOOLEAN IS
    -------
    L_program     VARCHAR2(60) := 'SA_REF_SQL.GET_REF_LABELS';
    L_ref_no      SA_REFERENCE.REF_NO%TYPE;
    L_ref_label_code SA_REFERENCE.REF_LABEL_CODE%TYPE;
    L_ref_desc          CODE_DETAIL.CODE_DESC%TYPE;
      
    cursor C_GET_LABEL is
       select ref_label_code,
              ref_no
    from sa_reference
   where tran_type = I_tran_type
     and ((sub_tran_type = I_sub_tran_type
                and I_sub_tran_type is not NULL
                and sub_tran_type   is not NULL)
              or (I_sub_tran_type is NULL
                  and sub_tran_type   is NULL))
     and ((reason_code = I_reason_code
                and I_reason_code is not NULL
                and reason_code   is not NULL)
              or (I_reason_code is NULL
                  and reason_code is NULL));
BEGIN
   if I_tran_type is not NULL then 
      for C_rec in C_GET_LABEL loop
         L_ref_label_code := C_rec.ref_label_code;
         L_ref_no         := C_rec.ref_no;
         ---
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        L_ref_label_code,                        
                                        L_ref_desc) = FALSE then
            return FALSE;
         end if;
         ---
         if L_ref_no = 1 then
            O_ref_no1 := L_ref_desc;
         elsif L_ref_no = 2 then
            O_ref_no2 := L_ref_desc;
         elsif L_ref_no = 3 then
            O_ref_no3 := L_ref_desc;
         elsif L_ref_no = 4 then
            O_ref_no4 := L_ref_desc;
         elsif L_ref_no = 5 then
            O_ref_no5 := L_ref_desc;
         elsif L_ref_no = 6 then
            O_ref_no6 := L_ref_desc;
    elsif L_ref_no = 7 then
            O_ref_no7 := L_ref_desc;
         elsif L_ref_no = 8 then
            O_ref_no8 := L_ref_desc;
         elsif L_ref_no = 9 then
            O_ref_no9 := L_ref_desc;
         elsif L_ref_no = 10 then
            O_ref_no10 := L_ref_desc;
         elsif L_ref_no = 11 then
            O_ref_no11 := L_ref_desc;
         elsif L_ref_no = 12 then
            O_ref_no12 := L_ref_desc;
    elsif L_ref_no = 13 then
            O_ref_no13 := L_ref_desc;
         elsif L_ref_no = 14 then
            O_ref_no14 := L_ref_desc;
         elsif L_ref_no = 15 then
            O_ref_no15 := L_ref_desc;
         elsif L_ref_no = 16 then
            O_ref_no16 := L_ref_desc;
         elsif L_ref_no = 17 then
            O_ref_no17 := L_ref_desc;
         elsif L_ref_no = 18 then
            O_ref_no18 := L_ref_desc;
         elsif L_ref_no = 19 then
            O_ref_no19 := L_ref_desc;
         elsif L_ref_no = 20 then
            O_ref_no20 := L_ref_desc;
         elsif L_ref_no = 21 then
            O_ref_no21 := L_ref_desc;
         elsif L_ref_no = 22 then
            O_ref_no22 := L_ref_desc;
         elsif L_ref_no = 23 then
            O_ref_no23 := L_ref_desc;
         elsif L_ref_no = 24 then
            O_ref_no24 := L_ref_desc;
         elsif L_ref_no = 25 then
            O_ref_no25 := L_ref_desc;
         elsif L_ref_no = 26 then
            O_ref_no26 := L_ref_desc;
         elsif L_ref_no = 27 then
            O_ref_no27 := L_ref_desc;
         end if;
      end loop;
      ---
      if O_ref_no1 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '1',                        
                                        O_ref_no1) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no2 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '2',                        
                                        O_ref_no2) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no3 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '3',                        
                                        O_ref_no3) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no4 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '4',                        
                                        O_ref_no4) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no5 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '5',                        
                                        O_ref_no5) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no6 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '6',                        
                                        O_ref_no6) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no7 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '7',                        
                                        O_ref_no7) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no8 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '8',                        
                                        O_ref_no8) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no9 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '9',                        
                                        O_ref_no9) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no10 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '10',                        
                                        O_ref_no10) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no11 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '11',                        
                                        O_ref_no11) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no12 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '12',                        
                                        O_ref_no12) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no13 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '13',                        
                                        O_ref_no13) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no14 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '14',                        
                                        O_ref_no14) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no15 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '15',                        
                                        O_ref_no15) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no16 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '16',                        
                                        O_ref_no16) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no17 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '17',                        
                                        O_ref_no17) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no18 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '18',                        
                                        O_ref_no18) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no19 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '19',                        
                                        O_ref_no19) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no20 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '20',                        
                                        O_ref_no20) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no21 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '21',                        
                                        O_ref_no21) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no22 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '22',                        
                                        O_ref_no22) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no23 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '23',                        
                                        O_ref_no23) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no24 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '24',                        
                                        O_ref_no24) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no25 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '25',                        
                                        O_ref_no25) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no26 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '26',                        
                                        O_ref_no26) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if O_ref_no27 is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                        'REFL',
                                        '27',                        
                                        O_ref_no27) = FALSE then
            return FALSE;
         end if;
      end if;

   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
       return FALSE;
   end if;
   ---
   return TRUE;   

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE; 
END GET_REF_LABELS;
---------------------------------------------------------------------------------------------------
FUNCTION REF_EXISTS(O_error_message  IN OUT  VARCHAR2,
          O_exists         IN OUT  BOOLEAN,
                    I_tran_type      IN      SA_REFERENCE.TRAN_TYPE%TYPE,
                    I_sub_tran_type  IN      SA_REFERENCE.SUB_TRAN_TYPE%TYPE,
                    I_reason_code    IN      SA_REFERENCE.REASON_CODE%TYPE,
                    I_ref_no         IN      SA_REFERENCE.REF_NO%TYPE)
   RETURN BOOLEAN IS
   ---
   L_exists   VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'SA_REF_SQL.REF_EXISTS';

   cursor C_EXIST is
   select 'Y'
   from sa_reference
   where tran_type              = I_tran_type
          and ((sub_tran_type        = I_sub_tran_type
                and sub_tran_type   is not NULL
                and I_sub_tran_type is not NULL)
           or (sub_tran_type        is NULL
               and I_sub_tran_type  is NULL))
          and ((reason_code          = I_reason_code
                and reason_code     is not NULL
                and I_reason_code   is not NULL)
           or (reason_code          is NULL
               and I_reason_code    is NULL))
          and ref_no                 = to_number(I_ref_no);          
   
BEGIN
   if I_tran_type is NULL or I_ref_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN','C_EXIST','SA_REFERENCE',NULL);
      open C_EXIST;
      SQL_LIB.SET_MARK('FETCH','C_EXIST','SA_REFERENCE',NULL);
      fetch C_EXIST into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_EXIST','SA_REFERENCE',NULL);
      close C_EXIST;
      --- 
      if L_exists = 'Y' then
    O_exists := TRUE;         
      else
    O_exists := FALSE;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END REF_EXISTS;
-----------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_REF_SEQ(O_error_message IN OUT VARCHAR2,
                          O_ref_seq_no    IN OUT SA_REFERENCE.REF_SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)                    := 'SA_REF_SQL.GET_NEXT_REF_SEQ';
   L_first_time   VARCHAR2(1)                     := 'Y';
   L_exists       VARCHAR2(1)                     := 'N';
   L_wrap_number  SA_REFERENCE.REF_SEQ_NO%TYPE;

   cursor C_GET_NEXT is
      select sa_ref_sequence.NEXTVAL
        from dual;

   cursor C_CHECK_REF is
      select 'Y'
        from sa_reference
       where ref_seq_no = O_ref_seq_no;

BEGIN
   LOOP
      --- Retrieve sequence number
      SQL_LIB.SET_MARK('OPEN','C_GET_NEXT','DUAL',NULL);
      open C_GET_NEXT;
      SQL_LIB.SET_MARK('FETCH','C_GET_NEXT','DUAL',NULL);
      fetch C_GET_NEXT into O_ref_seq_no;
      ---
      if C_GET_NEXT%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT','DUAL',NULL);
         close C_GET_NEXT;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT','DUAL',NULL);
      close C_GET_NEXT;
      ---
      if L_first_time = 'Y' then
         L_wrap_number := O_ref_seq_no;
         L_first_time  := 'N';
      elsif O_ref_seq_no = L_wrap_number then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      --- Check key existence
      SQL_LIB.SET_MARK('OPEN','C_CHECK_REF','SA_REFERENCE','REF_SEQ_NO: '||to_char(O_ref_seq_no));
      open C_CHECK_REF;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_REF','SA_REFERENCE','REF_SEQ_NO: '||to_char(O_ref_seq_no));
      fetch C_CHECK_REF into L_exists;
      ---
      if C_CHECK_REF%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_REF','SA_REFERENCE','REF_SEQ_NO: '||to_char(O_ref_seq_no));
         close C_CHECK_REF;
         EXIT;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_REF','SA_REFERENCE','REF_SEQ_NO: '||to_char(O_ref_seq_no));
      close C_CHECK_REF;
   END LOOP;   
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_REF_SEQ;
--------------------------------------------------------------------------------------
END SA_REF_SQL;
/
