
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE INVC_ATTRIB_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
--Function Name:   GET_CURRENCY_RATE
--Purpose:         This function will accept an invoice number and retrieve the currency
--                 code and exchange rate from the invc_head table.  If I_invc_id is NULL
--                 or invalid the function will return FALSE.
--Created:         NOV-98    Rebecca Dobosh
-------------------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_RATE(O_error_message  IN OUT  VARCHAR2,
                           O_currency_code  IN OUT  invc_head.currency_code%TYPE,
                           O_exchange_rate  IN OUT  invc_head.exchange_rate%TYPE,
                           I_invc_id        IN      invc_head.invc_id%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:   GET_INFO
--Purpose:         This function will accept an invoice number and retrieve the invoice 
--                 header details from invc_head table.  If I_invc_id is NULL or invalid
--                 the function will return FALSE.
--Created:         NOV-98    Rebecca Dobosh
-------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message          IN OUT VARCHAR2,
                  O_supplier               IN OUT invc_head.supplier%TYPE,
                  O_partner_type           IN OUT invc_head.partner_type%TYPE,
                  O_partner_id             IN OUT invc_head.partner_id%TYPE,
                  O_currency_code          IN OUT invc_head.currency_code%TYPE,
                  O_invc_date              IN OUT invc_head.invc_date%TYPE,
                  O_status                 IN OUT invc_head.status%TYPE,
                  O_total_merch_cost_invc  IN OUT invc_head.total_merch_cost%TYPE,
                  O_total_qty              IN OUT invc_head.total_qty%TYPE,
                  O_invc_type              IN OUT invc_head.invc_type%TYPE,
                  O_ref_invc_id            IN OUT invc_head.ref_invc_id%TYPE,
                  O_ref_rtv_order_no       IN OUT invc_head.ref_rtv_order_no%TYPE,
                  O_create_id              IN OUT invc_head.create_id%TYPE,
                  O_ext_ref_no             IN OUT invc_head.ext_ref_no%TYPE,
                  O_exchange_rate          IN OUT invc_head.exchange_rate%TYPE,
                  I_invc_id                IN     invc_head.invc_id%TYPE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:   TOTAL_NON_MERCH_COST
--Purpose:         This function will sum an invoice's non-merchandise amounts from the  
--                 invc_non_merch table and pass out the total non-merchandise cost for invoice.
--                 If I_dscnt_appl_ind = 'Y' the terms_dscnt_pct will be applied to the 
--                 non-merchandise costs and their vat codes.
--Created:         NOV-98    Rebecca Dobosh
-------------------------------------------------------------------------------------------
FUNCTION TOTAL_NON_MERCH_COST(O_error_message          IN OUT VARCHAR2,
                              O_total_non_merch_cost   IN OUT invc_non_merch.non_merch_amt%TYPE,
                              I_invc_id                IN     invc_non_merch.invc_id%TYPE,
                              I_dscnt_appl_ind         IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:   TOTAL_INVC_MERCH_VAT
--Purpose:         This function will calculate and sum an invoice's merchandise VAT amounts
--                 from the invc_vat table.
--Created:         NOV-98    Rebecca Dobosh
-------------------------------------------------------------------------------------------
FUNCTION TOTAL_INVC_MERCH_VAT(O_error_message          IN OUT VARCHAR2,
                              O_total_vat              IN OUT invc_head.total_merch_cost%TYPE,
                              I_invc_id                IN     invc_head.invc_id%TYPE,
                              I_dscnt_appl_ind         IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:   TOTAL_INVC_DSCNT  
--Purpose:         This function will retrieve an invoice's total discount across all the
--                 invoice line items.
--Created:         NOV-98    Rebecca Dobosh
-------------------------------------------------------------------------------------------
FUNCTION GET_MATCH_RCPT_VAT_RATE(O_error_message  IN OUT VARCHAR2,
                                 O_rcpt_vat       IN OUT shipsku.unit_cost%TYPE,
                                 I_invc_id        IN     invc_head.invc_id%TYPE,
                                 I_invc_date      IN     invc_head.invc_date%TYPE,
                                 I_match_rcpt     IN     shipsku.shipment%TYPE,
                                 I_item           IN     shipsku.item%TYPE,
                                 I_vat_region     IN     vat_item.vat_region%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:   GET_MATCH_RCPT_TOTALS
--Purpose:         This function will retrieve an invoice match receipt's total cost, 
--                 VAT, discount and quantity.
--Created:         NOV-98    Rebecca Dobosh
-------------------------------------------------------------------------------------------
FUNCTION GET_MATCH_RCPT_TOTALS(O_error_message    IN OUT VARCHAR2,
                               O_total_qty        IN OUT shipsku.qty_received%TYPE,
                               O_total_cost_rcpt  IN OUT shipsku.unit_cost%TYPE,
                               O_total_vat_rcpt   IN OUT shipsku.unit_cost%TYPE,
                               I_invc_id          IN     invc_head.invc_id%TYPE,
                               I_match_rcpt       IN     shipsku.shipment%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--Function Name:   TOTAL_INVC_COST
--Purpose:         This function will calculate an invoice's total cost.  If I_mrch_dscnt_appl_ind is
--                 passed in as 'Y' the terms dicount percent will be applied to the merchandise
--                 costs and merchandise VAT.  If I_non_mrch_dscnt_appl_ind is passed in as
--                 'Y' then the terms discount percent will be applied to non-merchandise
--                 costs and non-merchandise VAT.
--Created:         08-Feb-99
-------------------------------------------------------------------------------------------
FUNCTION TOTAL_INVC_COST(O_error_message              IN OUT VARCHAR2,
                         O_total_invc_cost            IN OUT invc_head.total_merch_cost%TYPE,
                         I_invc_id                    IN     invc_head.invc_id%TYPE,
                         I_mrch_dscnt_appl_ind        IN     VARCHAR2,
                         I_non_mrch_dscnt_appl_ind    IN     VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--Function Name:  GET_DBT_CRDT_INFO
--Purpose:        This function will retreive the invoice number, type and status of an 
--                debit memo or credit note request that has just been written and referenced
--                to I_ref_invc_id.
--Created:        24-Feb-99
-------------------------------------------------------------------------------------------
FUNCTION GET_DBT_CRDT_INFO(O_error_message              IN OUT VARCHAR2,
                           O_invc_id                    IN OUT invc_head.invc_id%TYPE,
                           O_invc_type                  IN OUT invc_head.invc_type%TYPE,
                           O_invc_status                IN OUT invc_head.status%TYPE,
                           O_total_invc_cost            IN OUT invc_head.total_merch_cost%TYPE,
                           I_ref_invc_id                IN     invc_head.ref_invc_id%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--Function Name:  GET_NMERCH_CODE_INFO
--Purpose:        This function will validate non_merchandise codes on invoices and return the 
--                translated non_merchandise description.
--Created:        12-Jan-00
----------------------------------------------------------------------------------------------------------
FUNCTION GET_NMERCH_CODE_INFO(O_error_message  IN OUT VARCHAR2,
                              O_code_desc      IN OUT NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE_DESC%TYPE,
                              O_service_ind    IN OUT NON_MERCH_CODE_HEAD.SERVICE_IND%TYPE,
                              I_code           IN     NON_MERCH_CODE_HEAD.NON_MERCH_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------
--Function Name:  TOTAL_TRAN_DSCNT
--Purpose:        This function will summarize the transaction-level discounts listed on the
--                invc_discount table.
--Created:        2-Feb-00
-------------------------------------------------------------------------------------------------
FUNCTION TOTAL_TRAN_DSCNT(O_error_message      IN OUT VARCHAR2,
                          O_total_tran_dscnt   IN OUT INVC_DISCOUNT.APPLIES_TO_AMT%TYPE,
                          I_invc_id            IN     INVC_DISCOUNT.INVC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--Function Name:  MATCH_TO_COST_QTY
--Purpose:        This function will retrieve a 'match to' cost and 'match to' quantity for
--                a shipment item.  If I_single_cost_out is passed in as TRUE, a match to cost
--                and quantity will only be returned if a single cost exists for the item.
--Created:        2-Feb-00
-------------------------------------------------------------------------------------------------
FUNCTION MATCH_TO_COST_QTY(O_error_message      IN OUT VARCHAR2,
                           O_match_to_cost      IN OUT INVC_MATCH_WKSHT.MATCH_TO_COST%TYPE,
                           O_match_to_qty       IN OUT INVC_MATCH_WKSHT.MATCH_TO_QTY%TYPE,
                           I_invc_id            IN     INVC_MATCH_WKSHT.INVC_ID%TYPE,
                           I_item               IN     ITEM_MASTER.ITEM%TYPE,
                           I_shipment           IN     INVC_MATCH_WKSHT.SHIPMENT%TYPE,
                           I_carton             IN     INVC_MATCH_WKSHT.CARTON%TYPE,
                           I_invc_cost          IN     INVC_MATCH_WKSHT.INVC_UNIT_COST%TYPE,
                           I_invc_qty           IN     INVC_MATCH_WKSHT.INVC_MATCH_QTY%TYPE,
                           I_single_cost_out    IN     BOOLEAN)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--Function Name:  CHECK_REASON_CODE_TYPE
--Purpose:        This function will check whether I_code is defined on CODE_DETAIL
--                or on NON_MERCH_CODE_HEAD.  I_type_ind is returned as either 'C' (exists on
--                CODE_DETAIL), 'N' (exists on NON_MERCH_CODE_HEAD) or 'I' (invalid, i.e. does not
--                exist on either).
--Created:        5-APR-00
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_REASON_CODE_TYPE (O_error_message   IN OUT VARCHAR2,
                                 O_type_ind        IN OUT VARCHAR2,
                                 I_code            IN     CODE_DETAIL.CODE_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END INVC_ATTRIB_SQL;
/

