CREATE OR REPLACE PACKAGE TRAN_DATA_IMPORT_SQL AUTHID CURRENT_USER AS 
-------------------------------------------------------------------------------------------------
   -- Process/chunk status
   NEW_RECORD   CONSTANT  VARCHAR2(30) := 'N';
   ERROR        CONSTANT  VARCHAR2(30) := 'E';
   PROCESSED    CONSTANT  VARCHAR2(30) := 'P';
-------------------------------------------------------------------------------------------------
-- Function: IMPORT_CHUNK
-- Description: This function acts as the wrapper function to the main processing functions
-------------------------------------------------------------------------------------------------
FUNCTION IMPORT_CHUNK(I_chunk_id     IN       STAGE_EXT_TRAN_DATA_CHUNK.CHUNK_ID%TYPE,
                      O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END TRAN_DATA_IMPORT_SQL;
/
