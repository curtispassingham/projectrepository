CREATE OR REPLACE PACKAGE BODY CORESVC_REFERENCE_FIELD
AS

   CURSOR c_svc_SA_REFERENCE(I_process_id NUMBER,I_chunk_id NUMBER)IS
      SELECT st.REF_LABEL_CODE,
             st.REF_NO,
             st.REASON_CODE,
             st.SUB_TRAN_TYPE,
             st.TRAN_TYPE,
             st.PROCESS_ID,
             st.ROW_SEQ,
             upper(st.ACTION) AS action,
             st.PROCESS$STATUS
        FROM SVC_SA_REFERENCE st,
             SA_REFERENCE UK_SA_REFERENCE,
             dual
       WHERE st.process_id = I_process_id
         AND st.chunk_id     = I_chunk_id
         AND st.TRAN_TYPE    = UK_SA_REFERENCE.TRAN_TYPE (+)
         AND st.REF_NO         = UK_SA_REFERENCE.REF_NO (+)
         AND st.REASON_CODE    = UK_SA_REFERENCE.REASON_CODE (+)
         AND st.SUB_TRAN_TYPE  = UK_SA_REFERENCE.SUB_TRAN_TYPE (+)
         AND st.action      IS NOT NULL;
        
   c_svc_SA_REFERENCE_rec C_SVC_SA_REFERENCE%ROWTYPE;
 
   Type errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   Lp_errors_tab errors_tab_typ;
   
  
----------------------------------------------------------------------------------
PROCEDURE write_error(I_process_id  IN SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
		      I_error_seq   IN SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
		      I_chunk_id    IN SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
		      I_table_name  IN SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
		      I_row_seq     IN SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
		      I_column_name IN SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
		      I_error_msg   IN SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)
IS
BEGIN
  Lp_errors_tab.extend();
  Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
  Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
  Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
  Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
  Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
  Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
  Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
END write_error;
--------------------------------------------------------------------------------------------
PROCEDURE exec_SA_REFERENCE_ins(I_ins_tab IN SA_REFERENCE_rec_tab)
IS

   L_error_msg      VARCHAR2(255);
BEGIN
   forall  i IN 1..I_ins_tab.count() 
      INSERT INTO SA_REFERENCE(REF_SEQ_NO,
                               TRAN_TYPE,
                               SUB_TRAN_TYPE,
                               REASON_CODE,
                               REF_NO,
                               REF_LABEL_CODE
                              ) VALUES 
                              (I_ins_tab(i).REF_SEQ_NO,
                               I_ins_tab(i).TRAN_TYPE,
                               I_ins_tab(i).SUB_TRAN_TYPE,
                               I_ins_tab(i).REASON_CODE,
                               I_ins_tab(i).REF_NO,
                               I_ins_tab(i).REF_LABEL_CODE
                              );
                    
END exec_SA_REFERENCE_ins;
--------------------------------------------------------------------------------------------
PROCEDURE exec_SA_REFERENCE_upd(I_upd_tab IN SA_REFERENCE_rec_tab) IS
BEGIN
   forall i IN 1..I_upd_tab.count()
      UPDATE SA_REFERENCE 
         SET REF_LABEL_CODE = I_upd_tab(i).REF_LABEL_CODE
      WHERE 1 = 1
        AND TRAN_TYPE = I_upd_tab(i).TRAN_TYPE
        AND NVL(SUB_TRAN_TYPE,'x') = NVL(I_upd_tab(i).SUB_TRAN_TYPE,'x')
        AND NVL(REASON_CODE,'x') = NVL(I_upd_tab(i).REASON_CODE,'x')
        AND REF_NO = I_upd_tab(i).REF_NO;
      
END exec_SA_REFERENCE_upd;
--------------------------------------------------------------------------------------------
PROCEDURE exec_SA_REFERENCE_del(I_del_tab IN SA_REFERENCE_rec_tab )
IS
BEGIN

  forall i IN 1..I_del_tab.count()
  DELETE FROM SA_REFERENCE
     WHERE 1 = 1
       AND TRAN_TYPE = I_del_tab(i).TRAN_TYPE
       AND NVL(SUB_TRAN_TYPE,'x') = NVL(I_del_tab(i).SUB_TRAN_TYPE,'x')
       AND NVL(REASON_CODE,'x') = NVL(I_del_tab(i).REASON_CODE,'x')
       AND REF_NO = I_del_tab(i).REF_NO;
       
END exec_SA_REFERENCE_del;
--------------------------------------------------------------------------------------------
PROCEDURE process_SA_REFERENCE(I_process_id IN SVC_SA_REFERENCE.PROCESS_ID%TYPE,
                               I_chunk_id   IN SVC_SA_REFERENCE.CHUNK_ID%TYPE )
IS
  
  
  L_error                 BOOLEAN;
  SA_REFERENCE_temp_rec   SA_REFERENCE%ROWTYPE;
  SA_REFERENCE_ins_rec    SA_REFERENCE_rec_tab:=NEW SA_REFERENCE_rec_tab();
  SA_REFERENCE_upd_rec    SA_REFERENCE_rec_tab:=NEW SA_REFERENCE_rec_tab();
  SA_REFERENCE_del_rec    SA_REFERENCE_rec_tab:=NEW SA_REFERENCE_rec_tab();
  L_table                 VARCHAR2(255)    :='SVC_SA_REFERENCE';
  L_error_msg		  VARCHAR2(255);
  L_exists		  BOOLEAN;
  L_ref_seq_no            SA_REFERENCE.REF_SEQ_NO%TYPE;	 
  L_code_desc             CODE_DETAIL.CODE_DESC%TYPE;
  
BEGIN
  FOR rec IN c_svc_SA_REFERENCE(I_process_id,I_chunk_id)
  LOOP
    L_error       := False;
    if rec.action is NULL then

        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id,
                    L_table,
                    rec.row_seq,
                    'ACTION',
                    'FIELD_NOT_NULL');
       L_error := TRUE;
    end if;
    if rec.action IS NOT NULL and 
       UPPER(rec.action) NOT IN (action_new,action_mod,action_del) then
      
       WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'ACTION',
                  'INV_ACT');
      L_error :=true;
    end if;
    if rec.TRAN_TYPE IS NULL then
        L_error_msg := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Tran. Type');
        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id,
                    l_table,
                    rec.row_seq,
                    'TRAN_TYPE',
                    L_error_msg);
       L_error :=true;
     end if;
     if rec.REF_NO IS NULL then
    
        L_error_msg := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Ref No.');
        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id,
                    l_table,
                    rec.row_seq,
                    'REF_NO',
                    L_error_msg);
       L_error :=true;
     end if;
     if rec.REF_LABEL_CODE IS NULL then
        L_error_msg := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Ref Label Code');
        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id,
                    l_table,
                    rec.row_seq,
                    'REF_LABEL_CODE',
                    L_error_msg);
       L_error :=true;
     end if;
     /* check the entered values are existing in code_detail table*/
     if rec.TRAN_TYPE IS NOT NULL then
        if LANGUAGE_SQL.GET_CODE_DESC(L_error_msg,
		    	              'TRAT',
		    	              rec.TRAN_TYPE,
		    	              L_code_desc) = FALSE then
	    WRITE_ERROR(I_process_id, SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'TRAN_TYPE', 'INV_VALUE');
	   L_error :=true;
        end if;
      end if;  
      if rec.SUB_TRAN_TYPE IS NOT NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(L_error_msg,
     		       	               'TRAS',
     		    	               rec.SUB_TRAN_TYPE,
     		    	               L_code_desc) = FALSE then
     	    WRITE_ERROR(I_process_id,SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,I_chunk_id,L_table,rec.row_seq,'SUB_TRAN_TYPE','INV_VALUE');
            L_error :=true;
         end if;
      end if;   
      if rec.REASON_CODE IS NOT NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(L_error_msg,
     			               'REAC',
     		    	               rec.REASON_CODE,
     		    	               L_code_desc) = FALSE then
     	    WRITE_ERROR(I_process_id, SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'REASON_CODE', 'INV_VALUE');
     	    L_error :=true;
         end if;
      end if;   
      if rec.REF_NO IS NOT NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(L_error_msg,
     		      	               'REFN',
     		    	               rec.REF_NO,
     		    	               L_code_desc) = FALSE then
     	    WRITE_ERROR(I_process_id, SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'REF_NO', 'INV_VALUE');
     	    L_error :=true;
         end if;
      end if;   
      if rec.REF_LABEL_CODE IS NOT NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(L_error_msg,
       		    	               'REFL',
     		    	               rec.REF_LABEL_CODE,
     		    	               L_code_desc) = FALSE then
     	    WRITE_ERROR(I_process_id, SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'REF_LABEL_CODE', 'INV_VALUE');
     	    L_error :=true;
         end if;
     end if;    
     if rec.action in( action_new,action_mod,action_del) and rec.tran_type is not null and rec.ref_no is not null THEN
        if SA_REF_SQL.REF_EXISTS(L_error_msg,
	    	                 L_exists, 
	 	                 rec.tran_type,
	 	                 rec.sub_tran_type,
	 	                 rec.reason_code,
	 	                 rec.ref_no) = FALSE then
	   WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       NULL,
                       L_error_msg); 
                       L_error :=true;
        end if;
        if L_exists = true then 
           if rec.action = action_new then 	    
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                          NULL,
                          'SA_REFER_EXISTS'); 	
              L_error :=true;
           end if;
        else
           if rec.action = action_new then
              if SA_REF_SQL.GET_NEXT_REF_SEQ(L_error_msg,
	     	                             L_ref_seq_no) = FALSE  then
	  	 WRITE_ERROR(I_process_id,
	                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
	                     I_chunk_id,
	                     L_table,
	                     rec.row_seq,
	                     NULL,
	                     L_error_msg); 
	         L_error :=true;   
	      else
	        SA_REFERENCE_temp_rec.REF_SEQ_NO := L_ref_seq_no; 
	      end if;	 	
	   end if;
	   if rec.action = action_mod then
	       WRITE_ERROR(I_process_id,
			   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
			   I_chunk_id,
			   L_table,
			   rec.row_seq,
			   NULL,
			  'MODIFY_REF_LABEL_CODE'); 	 
	      L_error :=true;        
	   end if;
	   if rec.action = action_del then
	      WRITE_ERROR(I_process_id,
	                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
	                  I_chunk_id,
	                  L_table,
	                  rec.row_seq,
	                  NULL,
	                 'NO_RECORD_FOR_DELETE'); 	 
	      L_error :=true;                      
	  end if;
       
        end if;
       
    END IF; 
    
    IF NOT L_error THEN
      SA_REFERENCE_temp_rec.TRAN_TYPE              := rec.TRAN_TYPE;
      SA_REFERENCE_temp_rec.SUB_TRAN_TYPE          := rec.SUB_TRAN_TYPE;
      SA_REFERENCE_temp_rec.REASON_CODE            := rec.REASON_CODE;
      SA_REFERENCE_temp_rec.REF_NO                 := rec.REF_NO;
      SA_REFERENCE_temp_rec.REF_LABEL_CODE         := rec.REF_LABEL_CODE;
    
      IF rec.action                    = action_new THEN
        SA_REFERENCE_ins_rec.extend;
        SA_REFERENCE_ins_rec(SA_REFERENCE_ins_rec.count()):=SA_REFERENCE_temp_rec;
      END IF;
      IF rec.action = action_mod THEN
        SA_REFERENCE_upd_rec.extend;
        SA_REFERENCE_upd_rec(SA_REFERENCE_upd_rec.count()):=SA_REFERENCE_temp_rec;
      END IF;
      IF rec.action = action_del THEN
        SA_REFERENCE_del_rec.extend;
        SA_REFERENCE_del_rec(SA_REFERENCE_del_rec.count()):=SA_REFERENCE_temp_rec;
      END IF;
    END IF;
  END LOOP;
  exec_SA_REFERENCE_ins(SA_REFERENCE_ins_rec);
  exec_SA_REFERENCE_upd(SA_REFERENCE_upd_rec);
  exec_SA_REFERENCE_del(SA_REFERENCE_del_rec);
END process_SA_REFERENCE;
-------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
  BEGIN
    delete from svc_sa_reference where process_id=I_process_id;

END CLEAR_STAGING_DATA;
------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message IN OUT rtk_errors.rtk_text%type,
                 I_process_id    IN Number,
                 O_error_count OUT NUMBER)
  RETURN BOOLEAN
IS
  L_program         VARCHAR2(255)     :='CORESVC_REFERENCE_FIELD.PROCESS';
  L_error_count     NUMBER       :=0;
  L_total_errors    NUMBER       :=0;
  L_chunk_id        NUMBER       :=1;
  L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
BEGIN
  Lp_errors_tab := NEW errors_tab_typ();
  process_SA_REFERENCE(I_process_id,L_chunk_id);
  O_error_count := Lp_errors_tab.count();
  forall i IN 1..O_error_count
  INSERT INTO SVC_ADMIN_UPLD_ER VALUES Lp_errors_tab
    (i
    );
  Lp_errors_tab := NEW errors_tab_typ();
  IF O_error_count    = 0 THEN
     L_process_status := 'PS';
  ELSE
     L_process_status := 'PE';
  END IF;
  UPDATE svc_process_tracker
         set status = (CASE when status = 'PE' then 'PE'
  			    else L_process_status
  	               END),
  	     action_date =sysdate
       where process_id = I_process_id;
    
  CLEAR_STAGING_DATA(I_process_id);
  commit;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END process;
--------------------------------------------------------------------------------
END CORESVC_REFERENCE_FIELD;
/