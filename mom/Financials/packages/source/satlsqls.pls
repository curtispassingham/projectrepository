
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_TOTAL_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------
-- Function: GET_TOTAL_DESC
-- Purpose : Returns the appropriate total description for the
--           given total.
-----------------------------------------------------------------------
FUNCTION GET_TOTAL_DESC(O_error_message IN OUT VARCHAR2,
                        O_total_desc    IN OUT SA_TOTAL_HEAD.TOTAL_DESC%TYPE,
                        I_total_id      IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                        I_total_rev_no  IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
    RETURN BOOLEAN;
----------------------------------------------------------------------
-- Function: GET_OS_VALUE
-- Purpose : Retrieves the Over/Short Value for the Store Day or
--           Store Day/Balance Group combination.
------------------------------------------------------------------------
FUNCTION GET_OS_VALUE(O_error_message     IN OUT VARCHAR2,
                      O_os_value          IN OUT SA_SYS_VALUE.SYS_VALUE%TYPE,
                      I_os_level          IN     VARCHAR2,
                      I_store_day_seq_no  IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                      I_bal_group_seq_no  IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                      I_store             IN     SA_STORE_DAY.STORE%TYPE,
                      I_day               IN     SA_STORE_DAY.DAY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------
-- Function: GET_TOTAL_HEADER
-- Purpose : Returns all the relavant data from the
--           SA_TOTAL_HEAD table for a particular total_id
------------------------------------------------------------------------
FUNCTION GET_TOTAL_HEADER(O_error_message      IN OUT VARCHAR2,
                          O_total_desc         IN OUT SA_TOTAL_HEAD.TOTAL_DESC%TYPE,
                          O_total_cat          IN OUT SA_TOTAL_HEAD.TOTAL_CAT%TYPE,
                          O_store_update_ind   IN OUT SA_TOTAL_HEAD.STORE_UPDATE_IND%TYPE,
                          O_hq_update_ind      IN OUT SA_TOTAL_HEAD.HQ_UPDATE_IND%TYPE,
                          O_comb_total_ind     IN OUT SA_TOTAL_HEAD.COMB_TOTAL_IND%TYPE,
                          I_total_id           IN     SA_TOTAL.TOTAL_ID%TYPE,
                          I_total_rev_no       IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------
-- Function: GET_TOTAL_VALUES
-- Purpose : Returns the values (pos_value,sys_value,etc)
--           from SA_POS_VALUE, SA_SYS_VALUE, SA_STORE_VALUE, SA_HQ_VALUE tables
--           for a particular total_seq_no.  the last_value is the last reported
--           value (of the 4 tables, which ever value has the highest value_rev_no
--           for the specified total_seq_no)
------------------------------------------------------------------------
FUNCTION GET_TOTAL_VALUES(O_error_message IN OUT VARCHAR2,
                          O_pos_value     IN OUT SA_POS_VALUE.POS_VALUE%TYPE,
                          O_sys_value     IN OUT SA_SYS_VALUE.SYS_VALUE%TYPE,
                          O_store_value   IN OUT SA_STORE_VALUE.STORE_VALUE%TYPE,
                          O_hq_value      IN OUT SA_HQ_VALUE.HQ_VALUE%TYPE,
                          O_last_value    IN OUT SA_HQ_VALUE.HQ_VALUE%TYPE,
                          I_total_seq_no  IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                          I_store         IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                          I_day           IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Function: TOTAL_REV_EXIST
-- Purpose : This function will check if the xxx_values
--           (sys,pos,store,hq) have been revised.  (ie, there exists
--           more than one record on one of the sa_xxx_values for
--           the given total_seq_no)
------------------------------------------------------------------------
FUNCTION TOTAL_REV_EXIST(O_error_message IN OUT VARCHAR2,
                         O_rev_exist     IN OUT BOOLEAN,
                         I_total_seq_no  IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         I_store         IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                         I_day           IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function: GET_TOTAL_INFO
-- Purpose : This function will return total information for a passed in
--           total sequence number
----------------------------------------------------------------------------------
FUNCTION GET_TOTAL_INFO(O_error_message      IN OUT VARCHAR2,
                        O_total_id           IN OUT SA_TOTAL.TOTAL_ID%TYPE,
                        O_total_desc         IN OUT SA_TOTAL_HEAD.TOTAL_DESC%TYPE,
                        O_total_cat          IN OUT SA_TOTAL_HEAD.TOTAL_CAT%TYPE,
                        O_total_cat_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_pos_value          IN OUT SA_POS_VALUE.POS_VALUE%TYPE,
                        O_sys_value          IN OUT SA_SYS_VALUE.SYS_VALUE%TYPE,
                        O_store_value        IN OUT SA_STORE_VALUE.STORE_VALUE%TYPE,
                        O_hq_value           IN OUT SA_HQ_VALUE.HQ_VALUE%TYPE,
                        O_comb_total_ind     IN OUT SA_TOTAL_HEAD.COMB_TOTAL_IND%TYPE,
                        O_store_update_ind   IN OUT SA_TOTAL_HEAD.STORE_UPDATE_IND%TYPE,
                        O_hq_update_ind      IN OUT SA_TOTAL_HEAD.HQ_UPDATE_IND%TYPE,
                        I_total_rev_no       IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                        I_total_status       IN     SA_TOTAL_HEAD.STATUS%TYPE,
                        I_total_seq_no       IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                        I_store              IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                        I_day                IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function: INSERT_REVISION
-- Purpose : This function will insert a new value record when one has been
--           updated.
----------------------------------------------------------------------------------
FUNCTION INSERT_REVISION(O_error_message     IN OUT VARCHAR2,
                         I_total_seq_no      IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         I_store_day_seq_no  IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                         I_value             IN     SA_STORE_VALUE.STORE_VALUE%TYPE,
                         I_update_value      IN     VARCHAR2,
                         I_update_id         IN     SA_STORE_VALUE.UPDATE_ID%TYPE,
                         I_store             IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                         I_day               IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function: UPDATE_ID_EXISTS
-- Purpose : Validates the existance of the entered Update ID
----------------------------------------------------------------------------------
FUNCTION UPDATE_ID_EXISTS(O_error_message   IN OUT  VARCHAR2,
                          O_exists          IN OUT  BOOLEAN,
                          I_update_id       IN      SA_TOTAL_HEAD.UPDATE_ID%TYPE,
                          I_store           IN      SA_TOTAL.STORE%TYPE DEFAULT NULL,
                          I_day             IN      SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function: GET_COMB_TOTAL_INFO
-- Purpose : This function will return all the necessary information
--           for the Combined Total Detail (sacombtl) form.
----------------------------------------------------------------------------------
FUNCTION GET_COMB_TOTAL_INFO(O_error_message    IN OUT   VARCHAR2,
                             O_total_desc       IN OUT   SA_TOTAL_HEAD.TOTAL_DESC%TYPE,
                             O_total_cat        IN OUT   SA_TOTAL_HEAD.TOTAL_CAT%TYPE,
                             O_total_cat_desc   IN OUT   CODE_DETAIL.CODE_DESC%TYPE,
                             I_detail_total_id  IN       SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function: UPDATE_EXP_LOG
-- Purpose : This function will update the export log when a total changes so the
--           updated version of the total will get exported appropriately, but will
--           not update for the given source system.  If a NULL is passed as the source
--           all systems are updated.
------------------------------------------------------------------------------------
FUNCTION UPDATE_EXP_LOG(O_error_message    IN OUT VARCHAR2,
                        I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                        I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                        I_source           IN     SA_EXPORT_LOG.SYSTEM_CODE%TYPE DEFAULT NULL,
			I_store		   IN	  SA_EXPORT_LOG.STORE%TYPE DEFAULT NULL,
			I_day		   IN	  SA_EXPORT_LOG.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: GET_OS_OPERATOR
-- Purpose : This function will return the OS_operator value from the sa_total_head table
--            for a passed total_id
--------------------------------------------------------------------------------------
FUNCTION GET_OS_OPERATOR(O_error_message  IN OUT VARCHAR2,
                         O_os_operator    IN OUT SA_TOTAL_HEAD.OS_OPERATOR%TYPE,
                         I_total_rev_no   IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                         I_total_seq_no   IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         I_store          IN     SA_TOTAL.STORE%TYPE DEFAULT NULL,
                         I_day            IN     SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function: GET_REF_LABELS
-- Purpose: This function will get the rollup levels and rollup level reference labels
-- for a given total id's
-- rollup levels.
---------------------------------------------------------------------------------------
FUNCTION GET_REF_LABELS(O_error_message  IN OUT VARCHAR2,
                        O_ref_label_code_1 IN OUT SA_TOTAL_HEAD.REF_LABEL_CODE_1%TYPE,
                        O_ref_label_desc_1 IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_ref_label_code_2 IN OUT SA_TOTAL_HEAD.REF_LABEL_CODE_1%TYPE,
                        O_ref_label_desc_2 IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_ref_label_code_3 IN OUT SA_TOTAL_HEAD.REF_LABEL_CODE_1%TYPE,
                        O_ref_label_desc_3 IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: TOTAL_LOC_EXISTS
-- Purpose: This function will check that a given location is valid
-- for a given total id.
---------------------------------------------------------------------------------------
FUNCTION TOTAL_LOC_EXISTS(O_error_message  IN OUT VARCHAR2,
                          O_exists         IN OUT BOOLEAN,
                          I_total_id       IN SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                          I_location       IN STORE.STORE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: TOTAL_USAGE_EXISTS
-- Purpose: This function will check whether a given total id is valid
-- for export to Oracle Financials.
---------------------------------------------------------------------------------------
FUNCTION TOTAL_USAGE_EXISTS(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_total_id       IN SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                            I_usage_type     IN SA_TOTAL_USAGE.USAGE_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: VALIDATE_UPDATE_ID
-- Purpose: This function will check that a given update id is valid
-- for a given total id.
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_UPDATE_ID (O_ERROR_MESSAGE IN OUT   VARCHAR2,
                             I_UPDATE_ID  IN SA_TOTAL_HEAD.UPDATE_ID%TYPE,
                             I_TOTAL_ID      IN SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: GET_PRECEDENCE
-- Purpose: This function will return the precedence value and value_rev_no
-- of the total value that has the highest precedence, for the given total_seq_no.
--------------------------------------------------------------------------------------------
FUNCTION GET_PRECEDENCE (O_ERROR_MESSAGE IN OUT   VARCHAR2,
                         I_total_seq_no  IN       SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                         O_precedence    IN OUT   V_SA_GET_TOTAL.PRECEDENCE%TYPE,
                         O_value_rev_no  IN OUT   V_SA_GET_TOTAL.VALUE_REV_NO%TYPE,
                         I_store         IN       SA_TOTAL.STORE%TYPE DEFAULT NULL,
                         I_day           IN       SA_TOTAL.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------

END SA_TOTAL_SQL;
/
