CREATE or REPLACE PACKAGE BODY SA_TOTAL_DEF_SQL AS
   LP_INSTANCE_ID SA_realm.instance_id%type := 1;
   FUNCTION INSERT_METADATA (O_error_message     IN OUT VARCHAR2,
                             I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                             I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                             I_rollup1_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                             I_rollup1_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE,
                             I_rollup2_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                             I_rollup2_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE,
                             I_rollup3_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                             I_rollup3_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE)
      RETURN BOOLEAN;
   FUNCTION BUILD_TOTAL_VIEW (O_error_message     IN OUT VARCHAR2,
                              I_realm_id          IN     SA_REALM.REALM_ID%TYPE,
                              I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                              I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
      RETURN BOOLEAN;   
----------------------------------------------------------------
FUNCTION CHECK_TOTAL_COMPATABILITY
      (O_error_message   IN OUT  VARCHAR2,
       O_compatible      IN OUT  BOOLEAN,
       I_base_total      IN      SA_TOTAL.TOTAL_ID%TYPE,
       I_total_to_check  IN      SA_TOTAL.TOTAL_ID%TYPE)
   return BOOLEAN is
   ---
   L_program                VARCHAR2(60)   := 'SA_TOTAL_DEF_SQL.CHECK_TOTAL_COMPATABILITY';
   L_total_id               SA_TOTAL_HEAD.TOTAL_ID%TYPE;
   L_total_realm            V_SA_PARM_INFO.REALM_NAME%TYPE;
   L_realm_id               V_SA_PARM_INFO.REALM_ID%TYPE;
   L_dummy                  VARCHAR2(1);
   ---
   L_base_count_sum_ind     SA_TOTAL_HEAD.COUNT_SUM_IND%TYPE;
   L_base_bal_level         SA_TOTAL_HEAD.BAL_LEVEL%TYPE;
   L_base_total_groupings   NUMBER(1) := 0;
   L_base_realm_id          V_SA_PARM_INFO.REALM_ID%TYPE;
   ---
   L_check_count_sum_ind    SA_TOTAL_HEAD.COUNT_SUM_IND%TYPE;
   L_check_bal_level        SA_TOTAL_HEAD.BAL_LEVEL%TYPE;
   L_check_parm_type_group  VARCHAR2(100);
   L_check_total_groupings  NUMBER(1) := 0;
   L_check_realm_id         V_SA_PARM_INFO.REALM_ID%TYPE;
   ---
   cursor C_GET_HEAD_INFO is
      select TH.bal_level,
             TH.count_sum_ind
        from sa_total_head TH
       where total_id = L_total_id
         and total_rev_no = (
         select max (total_rev_no)
           from sa_total_head
          where total_id = L_total_id) ;
   ---
   cursor c_check_view is
      select r.realm_id
        from v_SA_parm_info p, v_SA_realm_info r
       where r.realm_name = L_total_realm
         and p.realm_id = r.realm_id
         and p.realm_key_ind = 'Y'
         and p.parm_name = 'STORE_DAY_SEQ_NO';
   ---
   cursor c_count is
      select count(*)
        from v_SA_parm_info
       where realm_id = L_realm_id
         and realm_key_ind = 'Y'
         and parm_name not in ('STORE_DAY_SEQ_NO', 'REGISTER', 'CASHIER');
   ---
   cursor c_base_groups is
      select sa_total_def_sql.get_parm_type_group (parm_type_id) parm_type_group
        from v_SA_parm_info
       where realm_id = L_base_realm_id
         and realm_key_ind = 'Y'
         and parm_name not in ('STORE_DAY_SEQ_NO', 'REGISTER', 'CASHIER')
    order by 1, parm_id;
   ---
   cursor c_check_groups is
      select sa_total_def_sql.get_parm_type_group (parm_type_id)
        from v_SA_parm_info
       where realm_id = L_check_realm_id
         and realm_key_ind = 'Y'
         and parm_name not in ('STORE_DAY_SEQ_NO', 'REGISTER', 'CASHIER')
    order by 1, parm_id;

BEGIN
   if I_base_total is NULL or I_total_to_check is NULL then
      O_error_message := 'INV_INPUT_GENERIC';
      return FALSE;
   end if;
   O_compatible := TRUE;
   ---
   --- Get the base infomation for the total.
   L_total_id := I_base_total;
      open  C_GET_HEAD_INFO;
      fetch C_GET_HEAD_INFO into L_base_bal_level,
                                 L_base_count_sum_ind;
      if C_GET_HEAD_INFO%NOTFOUND then
         close C_GET_HEAD_INFO;
         o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_TOT_INFO',
                                                    l_program,
                                                    'I_base_total',
                                                    NULL);
                                                    
         return false;
      end if;
      close C_GET_HEAD_INFO;
   ---
   --- Get the information for the total to check.
   L_total_id := I_total_to_check;
      open  C_GET_HEAD_INFO;
      fetch C_GET_HEAD_INFO into L_check_bal_level,
                                 L_check_count_sum_ind;
      if C_GET_HEAD_INFO%NOTFOUND then
         close C_GET_HEAD_INFO;
         o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_TOT_INFO',
                                                    l_program,
                                                    'I_total_to_check',
                                                    NULL);
                                                    
         return false;
      end if;
      close C_GET_HEAD_INFO;
   ---
   --- Check if compatible based on the count sum indicator.
   if L_base_count_sum_ind != L_check_count_sum_ind then
      O_compatible := FALSE;
      return TRUE;
   end if;
   ---
   --- Check if compatible based on the balance level.
   if L_base_bal_level != L_check_bal_level then
      O_compatible := FALSE;
      return TRUE;
   end if;

   --- Check if metadata exists for both totals
   L_total_realm := get_total_view_name (I_base_total);
      open  C_CHECK_VIEW;
      fetch C_CHECK_VIEW into L_base_realm_id;
      if C_CHECK_VIEW%NOTFOUND then
         close C_CHECK_VIEW;
         O_compatible := FALSE;
         return true;
      end if;
      close C_CHECK_VIEW;
   L_total_realm := get_total_view_name (I_total_to_check);
      open  C_CHECK_VIEW;
      fetch C_CHECK_VIEW into L_check_realm_id;
      if C_CHECK_VIEW%NOTFOUND then
         close C_CHECK_VIEW;
         O_compatible := FALSE;
         return true;
      end if;
      close C_CHECK_VIEW;
   ---
   --- Check if compatible based on the number of grouping criteria.
   ---
   L_realm_id := L_base_realm_id;
      open  C_COUNT;
      fetch C_COUNT into L_base_total_groupings;
      close C_COUNT;

   L_realm_id := L_check_realm_id;
      open  C_COUNT;
      fetch C_COUNT into L_check_total_groupings;
      close C_COUNT;

   if L_base_total_groupings != L_check_total_groupings then
      O_compatible := FALSE;
      return TRUE;
   end if;
   ---
   --- We compare the parm_types from the two totals in order
   open  C_CHECK_GROUPS;
   for rec in c_base_groups loop
      fetch C_CHECK_GROUPS into L_check_parm_type_group;
      if rec.parm_type_group != L_check_parm_type_group then
         close C_CHECK_GROUPS;
         O_compatible := FALSE;
         return true;
      end if;
   end loop;
   close C_CHECK_GROUPS;

   --- If all requirements were passed then o_compatible is still true
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
      return FALSE;
   ---
END CHECK_TOTAL_COMPATABILITY;
----------------------------------------------------------------

   FUNCTION GET_PARM_TYPE_GROUP (I_parm_type_id      IN SA_PARM.PARM_TYPE_ID%TYPE)
            RETURN VARCHAR2
   IS
      cursor c_parm_type_name is
         select parm_type_name
           from SA_parm_type
          where parm_type_id = I_parm_type_id;

      v_parm_type_name SA_parm_type.parm_type_name%type;

   begin
      for rec in c_parm_type_name loop
         if rec.parm_type_name in ('loc','store','wh') then
            return 'LOC';
         elsif rec.parm_type_name in ('sku','fashsku','staplesku','packsku') then
            return 'SKU';
         else
            return upper (rec.parm_type_name);
         end if;
      end loop;
      return null;
   exception
      when others then
         return null;
   end;



----------------------------------------------------------------------------------------------------

   FUNCTION ORDER_GROUP_PARMS (O_error_message       IN OUT VARCHAR2,
                               O_changed_order       IN OUT BOOLEAN,
                               O_group_seq_no1       IN OUT SA_VR_PARMS.VR_PARM_SEQ_NO%TYPE,
                               O_label1              IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                               O_group_seq_no2       IN OUT SA_VR_PARMS.VR_PARM_SEQ_NO%TYPE,
                               O_label2              IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                               O_group_seq_no3       IN OUT SA_VR_PARMS.VR_PARM_SEQ_NO%TYPE,
                               O_label3              IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                               I_vr_id               IN     SA_TOTAL_HEAD.VR_ID%TYPE,
                               I_vr_rev_no           IN     SA_TOTAL_HEAD.VR_REV_NO%TYPE)
            RETURN BOOLEAN
   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.ORDER_GROUP_PARMS';
      v_parm_type_group1 varchar2(100);
      v_parm_type_group2 varchar2(100);
      v_parm_type_group3 varchar2(100);

      cursor c_group_parm (n_group_seq_no sa_vr_parms.vr_parm_seq_no%type) is
         select sa_total_def_sql.get_parm_type_group (p.parm_type_id)
           from sa_vr_parms svp,
                SA_parm p
          where svp.vr_id = I_vr_id
            and svp.vr_rev_no = I_vr_rev_no
            and svp.vr_parm_seq_no = n_group_seq_no
            and p.parm_id = svp.parm_id;

   BEGIN

      if O_group_seq_no1 is not null then
         open c_group_parm (O_group_seq_no1);
         fetch c_group_parm into v_parm_type_group1;
         if c_group_parm%notfound then
            close c_group_parm;
            o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_PARM_INFO',
                                                       v_program,
                                                       'O_group_seq_no1',
                                                       NULL);
                                                       
            return false;
         end if;
         close c_group_parm;
      end if;
      if O_group_seq_no2 is not null then
         open c_group_parm (O_group_seq_no2);
         fetch c_group_parm into v_parm_type_group2;
         if c_group_parm%notfound then
            close c_group_parm;
            o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_PARM_INFO',
                                                       v_program,
                                                       'O_group_seq_no2',
                                                       NULL);
                                                       
            return false;
         end if;
         close c_group_parm;
      end if;
      if O_group_seq_no3 is not null then
         open c_group_parm (O_group_seq_no3);
         fetch c_group_parm into v_parm_type_group3;
         if c_group_parm%notfound then
            close c_group_parm;
            o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_PARM_INFO',
                                                       v_program,
                                                       'O_group_seq_no3',
                                                       NULL);
                                                       
            return false;
         end if;
         close c_group_parm;
      end if;
      
      return true;

   EXCEPTION
      when others then
         if c_group_parm%isopen then
            close c_group_parm;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;
   end;


----------------------------------------------------------------------------------------------------

   FUNCTION ORDER_GROUP_PARMS (O_error_message     IN OUT VARCHAR2,
                               O_changed_order     IN OUT BOOLEAN,
                               O_rollup1_name      IN OUT SA_PARM.PHYSICAL_NAME%TYPE,
                               O_rollup1_parm_type IN OUT SA_PARM.PARM_TYPE_ID%TYPE,
                               O_rollup2_name      IN OUT SA_PARM.PHYSICAL_NAME%TYPE,
                               O_rollup2_parm_type IN OUT SA_PARM.PARM_TYPE_ID%TYPE,
                               O_rollup3_name      IN OUT SA_PARM.PHYSICAL_NAME%TYPE,
                               O_rollup3_parm_type IN OUT SA_PARM.PARM_TYPE_ID%TYPE)
            RETURN BOOLEAN
   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.ORDER_GROUP_PARMS';
      v_parm_type_group1 varchar2(100);
      v_parm_type_group2 varchar2(100);
      v_parm_type_group3 varchar2(100);

   BEGIN

      if O_rollup1_parm_type is not null then
         v_parm_type_group1 := get_parm_type_group (O_rollup1_parm_type);
         if v_parm_type_group1 is null then
            o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_PARM_TYPE',
                                                       v_program,
                                                       'O_rollup1_parm_type',
                                                       NULL);
                                                       
            return false;
         end if;
      end if;
      if O_rollup2_parm_type is not null then
         v_parm_type_group2 := get_parm_type_group (O_rollup2_parm_type);
         if v_parm_type_group2 is null then
            o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_PARM_TYPE',
                                                       v_program,
                                                       'O_rollup2_parm_type',
                                                       NULL);
                                                      
            return false;
         end if;
      end if;
      if O_rollup3_parm_type is not null then
         v_parm_type_group3 := get_parm_type_group (O_rollup3_parm_type);
         if v_parm_type_group3 is null then
            o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_PARM_TYPE',
                                                       v_program,
                                                       'O_rollup3_parm_type',
                                                       NULL);
                                                      
            return false;
         end if;
      end if;
      
      return true;

   EXCEPTION
      when others then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;
   end;


----------------------------------------------------------------------------------------------------

   FUNCTION CHECK_ARGS (O_error_message    IN OUT VARCHAR2,
                        I_rollup_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                        I_rollup_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE)
            return boolean is
      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.CHECK_TOTAL_DATA';
   begin
      if I_rollup_name is null and
         I_rollup_parm_type is not null then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               v_program,
                                               'Rollup Name',
                                               'NULL');
         return false;
      end if;
      if I_rollup_name is not null and
         I_rollup_parm_type is null then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               v_program,
                                               'Rollup Parm Type',
                                               'NULL');
         return false;
      end if;
      return true;

   EXCEPTION
      when others then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;
   end;

----------------------------------------------------------------------------------------------------

   FUNCTION CREATE_TOTAL_DATA (O_error_message     IN OUT VARCHAR2,
                               I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                               I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                               I_rollup1_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                               I_rollup1_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE,
                               I_rollup2_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                               I_rollup2_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE,
                               I_rollup3_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                               I_rollup3_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE)
            RETURN BOOLEAN
   IS
      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.CHECK_TOTAL_DATA';
      b_changed_order boolean;
      n_seq_1         number := null;
      n_seq_2         number := null;
      n_seq_3         number := null;
      v_rollup1_name      SA_PARM.PHYSICAL_NAME%TYPE := I_rollup1_name;
      v_rollup1_parm_type SA_PARM.PARM_TYPE_ID%TYPE := I_rollup1_parm_type;
      v_rollup2_name      SA_PARM.PHYSICAL_NAME%TYPE := I_rollup2_name;
      v_rollup2_parm_type SA_PARM.PARM_TYPE_ID%TYPE := I_rollup2_parm_type;
      v_rollup3_name      SA_PARM.PHYSICAL_NAME%TYPE := I_rollup3_name;
      v_rollup3_parm_type SA_PARM.PARM_TYPE_ID%TYPE := I_rollup3_parm_type;
   BEGIN
      if not check_args (O_error_message,
                         I_rollup1_name,
                         I_rollup1_parm_type) then
         return false;
      end if;
      if not check_args (O_error_message,
                         I_rollup2_name,
                         I_rollup2_parm_type) then
         return false;
      end if;
      if not check_args (O_error_message,
                         I_rollup3_name,
                         I_rollup3_parm_type) then
         return false;
      end if;
      if not order_group_parms (O_error_message,
                                b_changed_order,
                                v_rollup1_name,
                                v_rollup1_parm_type,
                                v_rollup2_name,
                                v_rollup2_parm_type,
                                v_rollup3_name,
                                v_rollup3_parm_type) then
         return false;
      end if;
      if b_changed_order then
         O_error_message := SQL_LIB.CREATE_MSG('GROUP_PARM_ORDER',
                                               v_rollup1_name,
                                               v_rollup2_name,
                                               v_rollup3_name);
         return false;
      end if;

      if not insert_metadata (O_error_message,
                              I_total_id,
                              I_total_rev_no,
                              v_rollup1_name,
                              v_rollup1_parm_type,
                              v_rollup2_name,
                              v_rollup2_parm_type,
                              v_rollup3_name,
                              v_rollup3_parm_type) then
         return false;
      end if;
      return true;
   EXCEPTION
      when others then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;
   END;


----------------------------------------------------------------------------------------------------

   FUNCTION GET_TOTAL_VIEW_NAME (I_total_id       IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
            RETURN VARCHAR2
   IS
   BEGIN
      return 'V_TOTAL_' || I_TOTAL_ID;
   END;

----------------------------------------------------------------------------------------------------

   FUNCTION FETCH_VR_PARM (O_error_message       IN OUT VARCHAR2,
                           O_parm_type_id        IN OUT SA_PARM.PARM_TYPE_ID%TYPE,
                           O_vr_parm_alias       IN OUT SA_VR_PARMS.VR_PARM_ALIAS%TYPE,
                           I_vr_id               IN     SA_VR_PARMS.VR_ID%TYPE,
                           I_vr_rev_no           IN     SA_VR_PARMS.VR_REV_NO%TYPE,
                           I_vr_parm_seq_no      IN     SA_VR_PARMS.VR_PARM_SEQ_NO%TYPE)
            RETURN BOOLEAN
   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.FETCH_VR_PARM';
      cursor c_vr_parms is
         select p.parm_type_id,
                svp.vr_parm_alias
           from SA_parm p,
                sa_vr_parms svp
          where svp.vr_parm_seq_no = I_vr_parm_seq_no
            and svp.vr_id = I_vr_id
            and svp.vr_rev_no = I_vr_rev_no
            and p.parm_id = svp.parm_id;
   BEGIN

      open c_vr_parms;
      fetch c_vr_parms into o_parm_type_id,
                              o_vr_parm_alias;
      if c_vr_parms%notfound then
         close c_vr_parms;
         o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_PARM_ID',
                                                    v_program,
                                                    'C_VR_PARMS',
                                                    NULL);
                                                    
         return false;
      end if;
      close c_vr_parms;
      return true;

   EXCEPTION
      when others then
         if c_vr_parms%isopen then
            close c_vr_parms;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;
   END;

----------------------------------------------------------------------------------------------------

   FUNCTION FETCH_PARM_TYPE_ID (O_error_message       IN OUT VARCHAR2,
                                O_parm_type_id        IN OUT SA_PARM.PARM_TYPE_ID%TYPE,
                                I_realm_name          IN     SA_REALM.PHYSICAL_NAME%TYPE,
                                I_parm_name           IN     SA_PARM.PHYSICAL_NAME%TYPE)
            RETURN BOOLEAN
   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.FETCH_PARM_TYPE_ID';
      cursor parm_type_id is
         select parm_type_id
           from v_SA_parm_info p, v_SA_realm_info r
          where r.realm_name = I_realm_name
            and p.realm_id = r.realm_id
            and p.parm_name = I_parm_name;

   BEGIN

      open parm_type_id;
      fetch parm_type_id into O_parm_type_id;
      if parm_type_id%notfound then
         close parm_type_id;
         o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_PARM_ID',
                                                    v_program,
                                                    'PARM_TYPE_ID',
                                                    NULL);
                                                    
         return false;
      end if;
      close parm_type_id;
      return true;

   EXCEPTION
      when others then
         if parm_type_id%isopen then
            close parm_type_id;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;
   END;

----------------------------------------------------------------------------------------------------

   FUNCTION INSERT_REALM (O_error_message     IN OUT VARCHAR2,
                          I_realm_id          IN     SA_REALM.REALM_ID%TYPE,
                          I_realm_name        IN     SA_REALM.PHYSICAL_NAME%TYPE)
            RETURN BOOLEAN
   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.INSERT_REALM';
   BEGIN

      update SA_realm
         set active_ind = 'N'
       where physical_name = I_realm_name;

      insert into SA_realm
               (realm_id,
                realm_type_id,
                physical_name,
                seq_no,
                instance_id,
                active_ind,
                display_name,
                multiselect_avail_ind)
         select I_realm_id,
                'TOTAL',
                I_realm_name,
                nvl (max (seq_no), 0) + 1,
                LP_INSTANCE_ID,
                'Y',
                I_realm_name,
                'N'
           from SA_realm
         where physical_name = NVL(I_realm_name, PHYSICAL_NAME);
          ---where physical_name = I_realm_name;
      return true;
   EXCEPTION
      when others then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;
   END;

--------------------------------------------------------------------------------

function make_unique_parm_name (
                                 v_error_message   out     varchar2,
                                 v_display_name    out     SA_parm.display_name%TYPE,
                                 v_realm_id        in      SA_parm.realm_id%TYPE,
                                 v_parm_name       in      varchar2)
         return boolean
is

   v_program   varchar2(50)  := 'SA_TOTAL_DEF_SQL.MAKE_UNIQUE_PARM_NAME' ;
   v_dummy     varchar2(1);
   n_alias_no  number := 1;
   v_temp_name SA_parm.display_name%TYPE := upper (v_parm_name);

   cursor c_parm_name is
      select null
        from SA_parm p
       where p.realm_id       = v_realm_id
         and upper (p.physical_name) = upper (v_temp_name);

begin

   open c_parm_name ;
   fetch c_parm_name into v_dummy ;
   if c_parm_name%found or
      upper (v_parm_name) in ('STORE_DAY_SEQ_NO', 'REGISTER', 'CASHIER',
                              'SYS_VALUE', 'STORE_VALUE', 'POS_VALUE', 'HQ_VALUE') then
      close c_parm_name ;
      loop
         v_temp_name := rtrim(substrb (upper (v_parm_name), 1, 117)) || to_char (n_alias_no);
         open c_parm_name ;
         fetch c_parm_name into v_dummy ;
         exit when c_parm_name%notfound;
         close c_parm_name ;
         n_alias_no := n_alias_no + 1;
         if n_alias_no > 999 then
            v_error_message := SQL_LIB.CREATE_MSG('no available parm names',
                                                  null,
                                                  null,
                                                  null);
            return false;
         end if;
      end loop;
   end if;
   close c_parm_name ;
   v_display_name := v_temp_name ;
   return true;

exception

   when others then
      if c_parm_name%isopen then
         close c_parm_name ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
----------------------------------------------------------------------------------------------------

   FUNCTION INSERT_PARM (O_error_message     IN OUT VARCHAR2,
                         I_realm_id          IN     SA_PARM.REALM_ID%TYPE,
                         I_parm_type_id      IN     SA_PARM.PARM_TYPE_ID%TYPE,
                         I_parm_name         IN     SA_PARM.PHYSICAL_NAME%TYPE,
                         I_realm_key_ind     IN     SA_PARM.REALM_KEY_IND%TYPE,
                         I_sys_parm          IN     BOOLEAN)
            RETURN BOOLEAN
   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.INSERT_PARM';
      v_parm_id  SA_parm.parm_id%type;
      v_realm_id SA_parm.realm_id%type;
      v_parm_name SA_parm.display_name%type;

   BEGIN
      if not sa_parm_sql.get_next_id(o_error_message, v_parm_id) then
         return false;
      end if;

      if I_sys_parm then
         v_parm_name := I_parm_name;
      else
         if not make_unique_parm_name (o_error_message, v_parm_name, I_realm_id, I_parm_name) then
            return false;
         end if;
      end if;

      insert into SA_parm
               (parm_id,
                realm_id,
                parm_type_id,
                physical_name,
                seq_no,
                realm_key_ind,
                active_ind,
                display_name,
                override_ind,
                lookup_ind,
                group_lookup_ind,
                error_ind,
                comment_ind,
                current_user_ind)
         values(v_parm_id,
                I_REALM_ID,
                I_PARM_TYPE_ID,
                v_PARM_NAME,
                1,
                I_REALM_KEY_IND,
                'Y',
                v_PARM_NAME,
                'N',
                'N',
                'N',
                'N',
                'N',
                'N');
      return true;
   EXCEPTION
      when others then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;
   END;

----------------------------------------------------------------------------------------------------

   FUNCTION CREATE_COMB_TOTAL_DATA (O_error_message     IN OUT VARCHAR2,
                                    I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                    I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
      RETURN BOOLEAN

   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.CREATE_COMB_TOTAL_DATA';
      v_detail_total_id  sa_comb_total.detail_total_id%type;
      v_detail_realm     v_SA_realm_info.realm_name%type;
      v_total_realm      v_SA_realm_info.realm_name%type;
      v_realm_id         v_SA_realm_info.realm_id%type;

      cursor c_total_info is
         select c1.detail_total_id
           from sa_comb_total c1
          where c1.total_id      = I_total_id
            and c1.total_rev_no  = I_total_rev_no
            and c1.detail_seq_no = (select min(c2.detail_seq_no)
                                      from sa_comb_total c2
                                     where c2.total_id     = c1.total_id
                                       and c2.total_rev_no = c1.total_rev_no);

      cursor c_detail_parms is
         select parm_name,
                parm_type_id,
                realm_key_ind
           from v_SA_parm_info p, v_SA_realm_info r
          where r.realm_name = v_detail_realm
            and p.realm_id   = r.realm_id
       order by p.parm_id;

   BEGIN

      open c_total_info;
      fetch c_total_info into v_detail_total_id;
      if c_total_info%notfound then
         close c_total_info;
         o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_DETAIL',
                                                v_program,
                                                'C_TOTAL_INFO',
                                                NULL);
                                                
         return false;
      end if;
      close c_total_info;

      v_detail_realm := get_total_view_name (v_detail_total_id);
      v_total_realm  := get_total_view_name (I_total_id);

      if not sa_realm_sql.get_next_id (o_error_message, v_realm_id) then
         return false;
      end if;

      if not insert_realm (o_error_message, v_realm_id, v_total_realm) then
         return false;
      end if;

      for rec in c_detail_parms loop
         if not insert_parm (o_error_message, v_realm_id, rec.parm_type_id, rec.parm_name, rec.realm_key_ind, true) then
            return false;
         end if;
      end loop;

      if not build_total_view (O_error_message,
                               v_realm_id,
                               I_total_id,
                               I_total_rev_no) then
         return false;
      end if;
      return true;
   EXCEPTION
      when others then
         if c_total_info%isopen then
            close c_total_info;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;

   END;

----------------------------------------------------------------------------------------------------

   FUNCTION CREATE_WIZ_TOTAL_DATA (O_error_message     IN OUT VARCHAR2,
                                   I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                   I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
      RETURN BOOLEAN

   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.CREATE_WIZ_TOTAL_DATA';
      v_vr_id             sa_vr_head.vr_id%type;
      n_vr_rev_no         sa_vr_head.vr_rev_no%type;
      n_group_seq_no1     sa_vr_parms.vr_parm_seq_no%type;
      n_group_seq_no2     sa_vr_parms.vr_parm_seq_no%type;
      n_group_seq_no3     sa_vr_parms.vr_parm_seq_no%type;

      v_rollup1_name      SA_PARM.PHYSICAL_NAME%TYPE := null;
      v_rollup1_parm_type SA_PARM.PARM_TYPE_ID%TYPE := null;
      v_rollup2_name      SA_PARM.PHYSICAL_NAME%TYPE := null;
      v_rollup2_parm_type SA_PARM.PARM_TYPE_ID%TYPE := null;
      v_rollup3_name      SA_PARM.PHYSICAL_NAME%TYPE := null;
      v_rollup3_parm_type SA_PARM.PARM_TYPE_ID%TYPE := null;

      cursor c_total_info is
         select vr_id,
                vr_rev_no,
                group_seq_no1,
                group_seq_no2,
                group_seq_no3
           from sa_total_head
          where total_id = I_total_id
            and total_rev_no = I_total_rev_no;

   BEGIN

      open c_total_info;
      fetch c_total_info into v_vr_id,
                              n_vr_rev_no,
                              n_group_seq_no1,
                              n_group_seq_no2,
                              n_group_seq_no3;
      if c_total_info%notfound then
         close c_total_info;
         o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_TOTAL',
                                                    v_program,
                                                    'C_TOTAL_INFO',
                                                    NULL);
                                                  
         return false;
      end if;
      close c_total_info;

      if n_group_seq_no1 is not null then
         if not fetch_vr_parm (o_error_message, v_rollup1_parm_type, v_rollup1_name,
                               v_vr_id, n_vr_rev_no, n_group_seq_no1) then
            return false;
         end if;
      end if;
      if n_group_seq_no2 is not null then
         if not fetch_vr_parm (o_error_message, v_rollup2_parm_type, v_rollup2_name,
                               v_vr_id, n_vr_rev_no, n_group_seq_no2) then
            return false;
         end if;
      end if;
      if n_group_seq_no3 is not null then
         if not fetch_vr_parm (o_error_message, v_rollup3_parm_type, v_rollup3_name,
                               v_vr_id, n_vr_rev_no, n_group_seq_no3) then
            return false;
         end if;
      end if;

      if not create_total_data (O_error_message,
                                I_total_id,
                                I_total_rev_no,
                                v_rollup1_name,
                                v_rollup1_parm_type,
                                v_rollup2_name,
                                v_rollup2_parm_type,
                                v_rollup3_name,
                                v_rollup3_parm_type) then
         return false;
      end if;
      return true;
   EXCEPTION
      when others then
         if c_total_info%isopen then
            close c_total_info;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;

   END;

----------------------------------------------------------------------------------------------------

   FUNCTION INSERT_METADATA (O_error_message     IN OUT VARCHAR2,
                             I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                             I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                             I_rollup1_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                             I_rollup1_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE,
                             I_rollup2_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                             I_rollup2_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE,
                             I_rollup3_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                             I_rollup3_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE)
      RETURN BOOLEAN

   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.INSERT_METADATA';
      v_realm_id          v_SA_realm_info.realm_id%TYPE;
      v_cur_parm          v_SA_parm_info.parm_name%TYPE;
      v_view_name         v_SA_realm_info.realm_name%TYPE := get_total_view_name (I_total_id);
      v_bal_level         sa_total_head.bal_level%type;
      v_parm_type_id      SA_PARM.PARM_TYPE_ID%TYPE;

      cursor c_bal_level is
         select bal_level
           from sa_total_head
          where total_id = I_total_id
            and total_rev_no = I_total_rev_no;

   BEGIN

      open c_bal_level;
      fetch c_bal_level into v_bal_level;
      if c_bal_level%notfound then
         close c_bal_level;
         o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_TOTAL',
                                                    v_program,
                                                    'C_BAL_LEVEL',
                                                    NULL);
                                              
         return false;
      end if;
      close c_bal_level;

      if not sa_realm_sql.get_next_id (o_error_message, v_realm_id) then
         return false;
      end if;

      if not insert_realm (o_error_message, v_realm_id, v_view_name) then
         return false;
      end if;

      v_cur_parm := 'STORE_DAY_SEQ_NO';
      if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_STORE_DAY', v_cur_parm) then
         return false;
      end if;
      if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'Y', true) then
         return false;
      end if;
      --
      v_cur_parm := 'STORE';
      if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_STORE_DAY', v_cur_parm) then
         return false;
      end if;
      if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'Y', true) then
         return false;
      end if;
      --
      v_cur_parm := 'DAY';
      if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_STORE_DAY', v_cur_parm) then
         return false;
      end if;
      if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'Y', true) then
         return false;
      end if;
      --
      if v_bal_level in ('R', 'C') then
         if v_bal_level = 'R' then
            v_cur_parm := 'REGISTER';
         else
            v_cur_parm := 'CASHIER';
         end if;
         if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_BALANCE_GROUP', v_cur_parm) then
            return false;
         end if;
         if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'Y', true) then
            return false;
         end if;
      end if;
      if I_rollup1_name is not null then
         if not insert_parm (o_error_message, v_realm_id, I_rollup1_parm_type, I_rollup1_name, 'Y', false) then
            return false;
         end if;
         if I_rollup2_name is not null then
            if not insert_parm (o_error_message, v_realm_id, I_rollup2_parm_type, I_rollup2_name, 'Y', false) then
               return false;
            end if;
            if I_rollup3_name is not null then
               if not insert_parm (o_error_message, v_realm_id, I_rollup3_parm_type, I_rollup3_name, 'Y', false) then
                  return false;
               end if;
            end if;
         end if;
      end if;

      v_cur_parm := 'TOTAL_SEQ_NO';
      if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_SYS_VALUE', v_cur_parm) then
         return false;
      end if;
      if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'N', true) then
         return false;
      end if;

      v_cur_parm := 'SYS_VALUE';
      if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_SYS_VALUE', v_cur_parm) then
         return false;
      end if;
      if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'N', true) then
         return false;
      end if;

      v_cur_parm := 'STORE_VALUE';
      if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_STORE_VALUE', v_cur_parm) then
         return false;
      end if;
      if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'N', true) then
         return false;
      end if;

      v_cur_parm := 'HQ_VALUE';
      if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_HQ_VALUE', v_cur_parm) then
         return false;
      end if;
      if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'N', true) then
         return false;
      end if;

      v_cur_parm := 'POS_VALUE';
      if not fetch_parm_type_id (o_error_message, v_parm_type_id, 'SA_POS_VALUE', v_cur_parm) then
         return false;
      end if;
      if not insert_parm (o_error_message, v_realm_id, v_parm_type_id, v_cur_parm, 'N', true) then
         return false;
      end if;
      ---     
      if not build_total_view (O_error_message,
                               v_realm_id,
                               I_total_id,
                               I_total_rev_no) then
         return false;
      end if;
      return true;
   EXCEPTION
      when others then
         if c_bal_level%isopen then
            close c_bal_level;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;

   END;
----------------------------------------------------------------------------------------------------

   FUNCTION BUILD_TOTAL_VIEW (O_error_message     IN OUT VARCHAR2,
                              I_realm_id          IN     SA_REALM.REALM_ID%TYPE,
                              I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                              I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
      RETURN BOOLEAN

   IS

      v_program          varchar2(65) := 'SA_TOTAL_DEF_SQL.BUILD_TOTAL_VIEW';
      v_create            sa_total_common.v_code_array := sa_total_common.empty_v_out;
      v_realm_id          v_SA_realm_info.realm_id%TYPE;
      v_parm_type_id      v_SA_parm_info.parm_type_id%TYPE;
      v_cur_parm          v_SA_parm_info.parm_name%TYPE;
      v_view_name         v_SA_realm_info.realm_name%TYPE := get_total_view_name (I_total_id);
      v_dummy             varchar2(1);
      b_bal_group         boolean;
      v_sql               DBMS_SQL.VARCHAR2s; /* dynamic sql variable */
      cursor_handle       INTEGER; /* The DBMS cursor handle */
      feedback            INTEGER; /* integer result capture variable */
      v_status            all_objects.status%TYPE;
      v_total_status      sa_total_head.status%TYPE;
      b_first_time        boolean := true;
      n_ref_no            number := 1;

      cursor c_check_compile is
         select status
           from all_objects
          where object_name = v_view_name
            and object_type = 'VIEW';

      cursor c_bal_group is
         select parm_name
           from v_SA_parm_info
          where realm_id = I_realm_id
            and parm_name in ('REGISTER', 'CASHIER');

      cursor c_parms is
         select vsp.parm_name
           from v_sa_parm_info vsp,
                sa_total_head soh,
                sa_vr_parms svp
          where vsp.realm_id = I_realm_id
            and vsp.realm_key_ind = 'Y'
            and vsp.parm_name NOT IN ('STORE_DAY_SEQ_NO', 'STORE', 'DAY', 'REGISTER', 'CASHIER')
            and soh.total_id = I_total_id
            and soh.total_rev_no = I_total_rev_no 
            and soh.vr_id = svp.vr_id
            and soh.vr_rev_no = svp.vr_rev_no
            and vsp.parm_type_name = UPPER(svp.vr_parm_alias)
            and (svp.vr_parm_seq_no = soh.total_parm_seq_no or
                 svp.vr_parm_seq_no = soh.group_seq_no1 or
                 svp.vr_parm_seq_no = soh.group_seq_no2 or
                 svp.vr_parm_seq_no = soh.group_seq_no3)            
          order by vsp.parm_id;


      cursor c_total_status is
         select status
           from sa_total_head
          where total_id = I_total_id
            and total_rev_no = I_total_rev_no;

   BEGIN
      open c_total_status;
      fetch c_total_status INTO v_total_status;
      close c_total_status;

      v_create(v_create.count + 1) := 'CREATE OR REPLACE VIEW ' ||
                sa_total_common.internal_schema_name || '.' || v_view_name || ' AS' ;
      v_create(v_create.count + 1) := '   SELECT ST.TOTAL_SEQ_NO,' ;
      v_create(v_create.count + 1) := '          ST.STORE_DAY_SEQ_NO,' ;
      v_create(v_create.count + 1) := '          ST.STORE,' ;
      v_create(v_create.count + 1) := '          ST.DAY,' ;
      for rec in c_bal_group loop
         v_create(v_create.count + 1) := '          VBG.' || rec.parm_name || ',' ;
      end loop;

      for rec in c_parms loop
         v_create(v_create.count + 1) := '          ST.' || 'REF_NO' || n_ref_no || ' ' || rec.parm_name || ',' ;
         n_ref_no := n_ref_no + 1 ;
      end loop;

      v_create(v_create.count + 1) := '          SSV.SYS_VALUE,' ;
      v_create(v_create.count + 1) := '          STV.STORE_VALUE,' ;
      v_create(v_create.count + 1) := '          SHV.HQ_VALUE,' ;
      v_create(v_create.count + 1) := '          SPV.POS_VALUE' ;

      v_create(v_create.count + 1) := '     FROM SA_TOTAL ST,' ;
      for rec in c_bal_group loop
         v_create(v_create.count + 1) := '          V_SA_BALANCE_GROUP_' || rec.parm_name || ' VBG,' ;
      end loop;
      v_create(v_create.count + 1) := '          SA_STORE_VALUE STV,' ;
      v_create(v_create.count + 1) := '          SA_HQ_VALUE SHV,' ;
      -- if not in Worksheet status, then get sys and pos values from the worksheet tables
      if v_total_status = 'W' then
         v_create(v_create.count + 1) := '          SA_POS_VALUE_WKSHT SPV,' ;
         v_create(v_create.count + 1) := '          SA_SYS_VALUE_WKSHT SSV' ;
      -- else get values from approved tables
      else
         v_create(v_create.count + 1) := '          SA_POS_VALUE SPV,' ;
         v_create(v_create.count + 1) := '          SA_SYS_VALUE SSV' ;
      end if;

      v_create(v_create.count + 1) := '    WHERE ST.TOTAL_ID = ''' || I_TOTAL_ID || '''' ;

      for rec in c_bal_group loop
         v_create(v_create.count + 1) := '      AND VBG.BAL_GROUP_SEQ_NO = ST.BAL_GROUP_SEQ_NO' ;
      end loop;

      v_create(v_create.count + 1) := '      AND SSV.TOTAL_SEQ_NO (+)= ST.TOTAL_SEQ_NO' ;
      v_create(v_create.count + 1) := '      AND SSV.STORE (+)= ST.STORE' ;
      v_create(v_create.count + 1) := '      AND SSV.DAY (+)= ST.DAY' ;
      v_create(v_create.count + 1) := '      AND (  SSV.VALUE_REV_NO IS NULL OR' ;
      v_create(v_create.count + 1) := '             SSV.VALUE_REV_NO = (' ;
      v_create(v_create.count + 1) := '         SELECT MAX (VALUE_REV_NO)' ;
      if v_total_status = 'W' then
         v_create(v_create.count + 1) := '           FROM SA_SYS_VALUE_WKSHT' ;
      else
         v_create(v_create.count + 1) := '           FROM SA_SYS_VALUE' ;
      end if;
      v_create(v_create.count + 1) := '          WHERE TOTAL_SEQ_NO = ST.TOTAL_SEQ_NO' ;
      v_create(v_create.count + 1) := '            AND STORE = ST.STORE' ;
      v_create(v_create.count + 1) := '            AND DAY = ST.DAY )  )' ;

      v_create(v_create.count + 1) := '      AND STV.TOTAL_SEQ_NO (+)= ST.TOTAL_SEQ_NO' ;
      v_create(v_create.count + 1) := '      AND STV.STORE (+)= ST.STORE' ;
      v_create(v_create.count + 1) := '      AND STV.DAY (+)= ST.DAY' ;
      v_create(v_create.count + 1) := '      AND (  STV.VALUE_REV_NO IS NULL OR' ;
      v_create(v_create.count + 1) := '             STV.VALUE_REV_NO = (' ;
      v_create(v_create.count + 1) := '         SELECT MAX (VALUE_REV_NO)' ;
      v_create(v_create.count + 1) := '           FROM SA_STORE_VALUE' ;
      v_create(v_create.count + 1) := '          WHERE TOTAL_SEQ_NO = ST.TOTAL_SEQ_NO' ;
      v_create(v_create.count + 1) := '            AND STORE = ST.STORE' ;
      v_create(v_create.count + 1) := '            AND DAY = ST.DAY )  )' ;

      v_create(v_create.count + 1) := '      AND SHV.TOTAL_SEQ_NO (+)= ST.TOTAL_SEQ_NO' ;
      v_create(v_create.count + 1) := '      AND SHV.STORE (+)= ST.STORE' ;
      v_create(v_create.count + 1) := '      AND SHV.DAY (+)= ST.DAY' ;
      v_create(v_create.count + 1) := '      AND (  SHV.VALUE_REV_NO IS NULL OR' ;
      v_create(v_create.count + 1) := '             SHV.VALUE_REV_NO = (' ;
      v_create(v_create.count + 1) := '         SELECT MAX (VALUE_REV_NO)' ;
      v_create(v_create.count + 1) := '           FROM SA_HQ_VALUE' ;
      v_create(v_create.count + 1) := '          WHERE TOTAL_SEQ_NO = ST.TOTAL_SEQ_NO' ;
      v_create(v_create.count + 1) := '            AND STORE = ST.STORE' ;
      v_create(v_create.count + 1) := '            AND DAY = ST.DAY )  )' ;

      v_create(v_create.count + 1) := '      AND SPV.TOTAL_SEQ_NO (+)= ST.TOTAL_SEQ_NO' ;
      v_create(v_create.count + 1) := '      AND SPV.STORE (+)= ST.STORE' ;
      v_create(v_create.count + 1) := '      AND SPV.DAY (+)= ST.DAY' ;
      v_create(v_create.count + 1) := '      AND (  SPV.VALUE_REV_NO IS NULL OR' ;
      v_create(v_create.count + 1) := '             SPV.VALUE_REV_NO = (' ;
      v_create(v_create.count + 1) := '         SELECT MAX (VALUE_REV_NO)' ;
      if v_total_status = 'W' then
         v_create(v_create.count + 1) := '           FROM SA_POS_VALUE_WKSHT' ;
      else
         v_create(v_create.count + 1) := '           FROM SA_POS_VALUE' ;
      end if;
      v_create(v_create.count + 1) := '          WHERE TOTAL_SEQ_NO = ST.TOTAL_SEQ_NO' ;
      v_create(v_create.count + 1) := '            AND STORE = ST.STORE' ;
      v_create(v_create.count + 1) := '            AND DAY = ST.DAY )  )' ;

      ---      
      /* load the DBMS_SQL.VARCHAR2 variable with the v_create data */
      for n_count IN 1..v_create.count
      loop
          v_sql(v_sql.count+1) := v_create(n_count);
      end loop;

      /* open the DBMS_SQL cursor */
      cursor_handle := DBMS_SQL.OPEN_CURSOR;

      /* pass in the dynamic function code for execution */
      DBMS_SQL.PARSE (cursor_handle, v_sql, v_sql.FIRST,
                      v_sql.LAST, TRUE, DBMS_SQL.NATIVE);

      /* execute the dynamic function code */
      feedback := DBMS_SQL.EXECUTE(cursor_handle);

      /* close cursor */
      DBMS_SQL.CLOSE_CURSOR(cursor_handle);

      /***************************************************************\
      |                                                               |
      |   This will determine if the code compiled successfully       |
      |                                                               |
      \***************************************************************/

      open c_check_compile;
      fetch c_check_compile INTO v_status;
      close c_check_compile;

      if v_status = 'INVALID' then
          /* Compile did not work */
         o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_VIEW',
                                                    v_program,
                                                    'CREATE_VIEW',
                                                    NULL);
                                                    
         RETURN false;
      end if;
      RETURN true;

   EXCEPTION
      when others then
         if c_check_compile%isopen then
            close c_check_compile;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               v_program,
                                                to_char(SQLCODE));
         return false;

   END;
----------------------------------------------------------------------------------------------------

   FUNCTION DROP_TOTAL_VIEW (O_error_message     IN OUT VARCHAR2,
                             I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
      RETURN BOOLEAN

   IS

      v_program           varchar2(65) := 'SA_TOTAL_DEF_SQL.DROP_TOTAL_VIEW';
      v_create            varchar2(255);
      v_view_name         v_SA_realm_info.realm_name%TYPE := get_total_view_name (I_total_id);
      cursor_handle       INTEGER; /* The DBMS cursor handle */
      feedback            INTEGER; /* integer result capture variable */
      v_status            all_objects.status%TYPE;

      cursor c_check_compile is
         select 'x'
           from all_objects
          where object_name = v_view_name
            and object_type = 'VIEW'
            and owner       = sa_total_common.internal_schema_name;

   BEGIN
      --- Check if the view exists to drop.
      SQL_LIB.SET_MARK('OPEN','C_CHECK_COMPILE','ALL_OBJECTS',NULL);
      open c_check_compile;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_COMPILE','ALL_OBJECTS',NULL);
      fetch c_check_compile INTO v_status;
      if c_check_compile%found then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_COMPILE','ALL_OBJECTS',NULL);
         close c_check_compile;
         ---
         --- Drop the view.
         v_create := 'DROP VIEW ' || sa_total_common.internal_schema_name || '.' || v_view_name ;
         /* open the DBMS_SQL cursor */
         cursor_handle := DBMS_SQL.OPEN_CURSOR;
         ---
         /* pass in the dynamic function code for execution */
         DBMS_SQL.PARSE (cursor_handle, v_create, DBMS_SQL.NATIVE);
         ---
         /* execute the dynamic function code */
         feedback := DBMS_SQL.EXECUTE(cursor_handle);
         ---
         /* close cursor */
         DBMS_SQL.CLOSE_CURSOR(cursor_handle);
         ---
         /***************************************************************\
         |                                                               |
         |   This will determine if the code compiled successfully       |
         |                                                               |
         \***************************************************************/
         ---
         SQL_LIB.SET_MARK('OPEN','C_CHECK_COMPILE','ALL_OBJECTS',NULL);
         open c_check_compile;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_COMPILE','ALL_OBJECTS',NULL);
         fetch c_check_compile INTO v_status;
         if c_check_compile%found then
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_COMPILE','ALL_OBJECTS',NULL);
            close c_check_compile;
            o_error_message := SQL_LIB.CREATE_MSG ('PROG_UNIT_ERR_DROP',
                                                    v_program,
                                                    'DROP_VIEW',
                                                    NULL);
                                                    
            RETURN false;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_COMPILE','ALL_OBJECTS',NULL);
         close c_check_compile;
      else --- View to drop was not found.
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_COMPILE','ALL_OBJECTS',NULL);
         close c_check_compile;
      end if;
      RETURN true;

   EXCEPTION
      when others then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                v_program,
                                                to_char(SQLCODE));
         return false;
   END;
----------------------------------------------------------------------------------------------------
FUNCTION APPROVED_TOTAL_EXISTS(O_error_message     IN OUT  VARCHAR2,
                               O_approved_exists   IN OUT  VARCHAR2,
                               I_total_id          IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(50)   := 'SA_TOTAL_DEF_SQL.APPROVED_TOTAL_EXISTS';
   L_exists       VARCHAR2(1)    := 'N';
   cursor C_CHECK_EXISTS is
      select 'Y'
        from sa_total_head
       where total_id = I_total_id
         and status = 'A'; --- Approved status
BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','SA_TOTAL_DEF_SQL',NULL);
   open  C_CHECK_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','SA_TOTAL_DEF_SQL',NULL);
   fetch C_CHECK_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','SA_TOTAL_DEF_SQL',NULL);
   close C_CHECK_EXISTS;
   ---
   if L_exists = 'Y' then
      O_approved_exists := 'Y';
   else
      O_approved_exists := 'N';
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END APPROVED_TOTAL_EXISTS;
----------------------------------------------------------------------------------------------------
FUNCTION VALID_MULTI_VERSIONS(O_ERROR_MESSAGE   IN OUT   VARCHAR2,
                   O_valid_multi_rev_ind  IN OUT  VARCHAR2,
                   I_total_id             IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                   I_total_rev_no         IN      SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                   I_start_date           IN      SA_TOTAL_HEAD.START_BUSINESS_DATE%TYPE,
                   I_end_date             IN      SA_TOTAL_HEAD.END_BUSINESS_DATE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program_name  VARCHAR2(60) := 'SA_TOTAL_DEF_SQL.VALID_MULTI_VERSIONS';
   L_function_name       VARCHAR2(30) := NULL;
   L_valid_multi_rev_ind VARCHAR2(1)  := 'Y';
   L_start_date          VARCHAR2(6);
   L_end_date            VARCHAR2(6);
   L_dates               VARCHAR2(14);
  ---
   cursor C_CHECK_TOTAL_DATE_RANGE is
      select to_char(start_business_date,'YYMMDD'),
             to_char(end_business_date,'YYMMDD')
        from sa_total_head
       where total_id      = I_total_id
         and total_rev_no != I_total_rev_no
         and (   (start_business_date <= I_start_date and
                  end_business_date   >= I_start_date and
                  end_business_date   is NOT NULL )
              or (start_business_date <= I_start_date and
                  end_business_date   is NULL )
              or (start_business_date >= I_start_date and
                  I_end_date          is NULL )
              or (start_business_date >= I_start_date and
                  end_business_date   <= I_end_date and
                  end_business_date   is NOT NULL)
              or (start_business_date <= I_end_date and
                  end_business_date   >= I_end_date and
                  end_business_date   is NOT NULL )
              or (start_business_date <= I_end_date and
                  end_business_date   is NULL )
             );
   cursor C_CHECK_VALID_FUNCTION is
      select 'N'
        from all_objects
       where object_name = 'SA_T_' || upper(I_total_id) || L_dates
         and object_type = 'FUNCTION'
         and lower(owner) = lower(sa_total_common.internal_schema_name)
         and status = 'VALID';

BEGIN
   if I_total_id is NULL or
         I_total_rev_no is NULL or
         I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   --- check for the total date range.
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_total_head', NULL);
   open C_CHECK_TOTAL_DATE_RANGE;
      LOOP
         L_dates      := NULL;
         L_start_date := NULL;
         L_end_date   := NULL;
         SQL_LIB.SET_MARK('FETCH', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_total_head', NULL);
         fetch C_CHECK_TOTAL_DATE_RANGE into L_start_date,
                                             L_end_date;
         EXIT WHEN C_CHECK_TOTAL_DATE_RANGE%NOTFOUND;
         BEGIN
            if L_end_date is NOT NULL then
               L_dates := '_' || L_start_date || '_' || L_end_date;
            else
               L_dates := '_' || L_start_date;
            end if;
            SQL_LIB.SET_MARK('OPEN', 'C_CHECK_VALID_FUNCTION', 'all_objects', NULL);
            open C_CHECK_VALID_FUNCTION;
            SQL_LIB.SET_MARK('FETCH', 'C_CHECK_VALID_FUNCTION', 'all_objects', NULL);
            fetch C_CHECK_VALID_FUNCTION into L_valid_multi_rev_ind;
            SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_VALID_FUNCTION', 'all_objects', NULL);
            close C_CHECK_VALID_FUNCTION;
         END;
      end LOOP;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_total_head', NULL);
   close C_CHECK_TOTAL_DATE_RANGE;
   ---
   O_valid_multi_rev_ind := L_valid_multi_rev_ind;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_MULTI_VERSIONS;
----------------------------------------------------------------------------------------------------
FUNCTION DROP_INVALID_MULTI_VERSIONS
      (O_ERROR_MESSAGE          IN OUT    VARCHAR2,
         I_total_id             IN        SA_TOTAL_HEAD.TOTAL_ID%TYPE,
         I_total_rev_no         IN        SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
         I_start_date           IN        SA_TOTAL_HEAD.START_BUSINESS_DATE%TYPE,
         I_end_date             IN        SA_TOTAL_HEAD.END_BUSINESS_DATE%TYPE)
        return BOOLEAN is
   ---
   L_program_name  VARCHAR2(60) := 'SA_TOTAL_DEF_SQL.DROP_INVALID_MULTI_VERSIONS';
   L_function_name VARCHAR2(30) := NULL;
   L_total_rev_no  SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE;
  ---
   cursor C_GET_INVALID_VERSIONS is
      select total_rev_no
        from sa_total_head
       where total_id      = I_total_id
         and total_rev_no != I_total_rev_no
         and (   (start_business_date <= I_start_date and
                  end_business_date   >= I_start_date and
                  end_business_date   is NOT NULL )
              or (start_business_date <= I_start_date and
                  end_business_date   is NULL )
              or (start_business_date >= I_start_date and
                  I_end_date          is NULL )
              or (start_business_date >= I_start_date and
                  end_business_date   <= I_end_date and
                  end_business_date   is NOT NULL)
              or (start_business_date <= I_end_date and
                  end_business_date   >= I_end_date and
                  end_business_date   is NOT NULL )
              or (start_business_date <= I_end_date and
                  end_business_date   is NULL )
             );
BEGIN
   if I_total_id is NULL or
         I_total_rev_no is NULL or
         I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   --- check for the total date range.
   SQL_LIB.SET_MARK('OPEN', 'C_GET_INVALID_VERSIONS', 'sa_total_head', NULL);
   open C_GET_INVALID_VERSIONS;
      LOOP
         SQL_LIB.SET_MARK('FETCH', 'C_GET_INVALID_VERSIONS', 'sa_total_head', NULL);
         fetch C_GET_INVALID_VERSIONS into L_total_rev_no;
         EXIT WHEN C_GET_INVALID_VERSIONS%NOTFOUND;
         BEGIN
            if SA_BUILD_TOTAL_SQL.DROP_TOTAL_FUNCTION(O_error_message,
                                                      I_total_id,
                                                      I_total_rev_no
                                                      ) != 0 then
               SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_total_head', NULL);
               close C_GET_INVALID_VERSIONS;
               return FALSE;
            end if;
         END;
      end LOOP;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_total_head', NULL);
   close C_GET_INVALID_VERSIONS;
   ---
return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END DROP_INVALID_MULTI_VERSIONS;
-----------------------------------------------------------------------
FUNCTION DELETE_TOTAL(O_error_message     IN OUT  VARCHAR2,
                      I_total_id          IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN IS
   L_program         VARCHAR2(50)                    := 'SA_TOTAL_DEF_SQL.DELETE_TOTAL';
   v_view_name       v_SA_realm_info.realm_name%TYPE := get_total_view_name (I_total_id);
   v_vr_id           sa_vr_head.vr_id%TYPE           := NULL;
   v_realm_id        sa_realm.realm_id%type          := NULL;
   v_parm_id         sa_parm.parm_id%type            := NULL;
   b_bal_group       boolean;

   cursor c_vr_id is
      select vr.vr_id,
             vr.realm_id
        from sa_vr_realm vr,
             v_sa_realm_info vsri
       where vsri.realm_name = v_view_name
         and vsri.realm_id = vr.realm_id
      UNION
      select h.vr_id,
             h.driving_realm_id
        from sa_vr_head h
       where h.driving_realm_id in (select r.realm_id
                                      from sa_realm r
                                     where r.physical_name = v_view_name);

   cursor C_GET_REALM_ID is
      select DISTINCT realm_id
        from sa_realm 
       where physical_name = v_view_name;

   cursor C_GET_PARM_ID is
      select DISTINCT parm_id
        from sa_parm 
       where realm_id = v_realm_id;

   cursor c_total_seq_no is
      select total_seq_no
        from sa_total
       where total_id = I_total_id;
BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   for vr_rec in C_VR_ID LOOP
      v_vr_id    := vr_rec.vr_id;
      v_realm_id := vr_rec.realm_id;
      ---
      if v_vr_id is NOT NULL then
         if NOT sa_audit_rules_sql.delete_vr(O_error_message,
                                             v_vr_id) then
            return FALSE;
         end if;
      end if;
      ---
      update sa_total_head
         set vr_id             = NULL, 
             vr_rev_no         = NULL, 
             total_parm_seq_no = -1,  --- Seq. No. of required parm (store_day_seq_no)
             group_seq_no1     = NULL,
             group_seq_no2     = NULL, 
             group_seq_no3     = NULL       
       where vr_id = v_vr_id;
   end LOOP;
   ---
   delete from sa_total_usage
   where total_id = I_total_id;
   ---
   delete from sa_total_loc_trait
   where total_id = I_total_id;
   ---
   delete from sa_total_restrictions
   where total_id = I_total_id;
   ---
   delete from sa_comb_total
   where total_id = I_total_id;
   ---
   delete from sa_total_head_tl
      where total_id = I_total_id;
   ---      
   delete from sa_total_head
   where total_id = I_total_id;
   ---      
   for L_realm_rec in C_GET_REALM_ID LOOP
      v_realm_id := L_realm_rec.realm_id;
      ---
      for L_parm_rec in C_GET_PARM_ID LOOP
         v_parm_id := L_parm_rec.parm_id;
         ---
         delete from sa_vr_parms
          where parm_id = v_parm_id;
         ---
         delete from sa_vr_links
          where link_to_parm_id = v_parm_id;
         ---
      end LOOP;
      ---      
      delete from sa_parm
       where realm_id = v_realm_id;
      ---
      delete from sa_vr_realm
       where realm_id = v_realm_id;
      ---
      delete from sa_vr_head
       where driving_realm_id = v_realm_id;
      ---
      delete from sa_realm
       where realm_id = v_realm_id;
   end LOOP;
   ---
   for rec in c_total_seq_no loop
      delete from sa_pos_value_wksht
      where total_seq_no = rec.total_seq_no;
      ---
      delete from sa_sys_value_wksht
      where total_seq_no = rec.total_seq_no;
      ---
      delete from sa_store_value
      where total_seq_no = rec.total_seq_no;
      ---
      delete from sa_hq_value
      where total_seq_no = rec.total_seq_no;
      ---
      delete from sa_total
      where total_seq_no = rec.total_seq_no;
      ---
   end loop;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_TOTAL;
------------------------------------------------------------------
FUNCTION BUILD_NON_SYS_TOTAL(O_error_message     IN OUT  VARCHAR2,
                             I_total_id          IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                             I_total_rev_no      IN      SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
   RETURN BOOLEAN IS
   ---
   v_program      VARCHAR2(50)   := 'SA_TOTAL_DEF_SQL.BUILD_NON_SYS_TOTAL';
   v_view_name    V_SA_REALM_INFO.REALM_NAME%TYPE := GET_TOTAL_VIEW_NAME(I_total_id);
   v_realm_id     SA_REALM.REALM_ID%TYPE := NULL;

   cursor C_REALM_EXISTS is 
      select realm_id
        from sa_realm
       where physical_name = v_view_name
         and active_ind    = 'Y';

BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            v_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_REALM_EXISTS',
                    'SA_REALM',
                    v_view_name);
   open C_REALM_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_REALM_EXISTS',
                    'SA_REALM',
                    v_view_name);
   fetch C_REALM_EXISTS into v_realm_id;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_REALM_EXISTS',
                    'SA_REALM',
                    v_view_name);
   close C_REALM_EXISTS;
   ---
   if v_realm_id is NOT NULL then
      --- Metadata exists. Only need to build/rebuild the view.
      if not build_total_view (O_error_message,
                               v_realm_id,
                               I_total_id,
                               I_total_rev_no) then
         return false;
      end if;      
   else 
      --- Create the metadata and build/rebuild the view.
      if not insert_metadata (O_error_message,
                              I_total_id,
                              I_total_rev_no,
                              NULL, --- v_rollup1_name
                              NULL, --- v_rollup1_parm_type
                              NULL, --- v_rollup2_name
                              NULL, --- v_rollup2_parm_type
                              NULL, --- v_rollup3_name
                              NULL  --- v_rollup3_parm_type
                              ) then
         return false;
      end if;   
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return FALSE;
END BUILD_NON_SYS_TOTAL;
------------------------------------------------------------------
END SA_TOTAL_DEF_SQL;
/
