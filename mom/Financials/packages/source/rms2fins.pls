CREATE OR REPLACE PACKAGE RMS2FIN AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------
-- Function    : get_segments
-- Purpose     : To retrieve segment values and segment labels
--               given CCID(Oracle Financial account code).
--               It will be called by the glcrossr and saglcros forms.
-- Created     : Feb 14, 2000
---------------------------------------------------------------------------------------
FUNCTION GET_SEGMENTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_ccid             IN      FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE,
                      I_set_of_books_id  IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                      O_segment1         IN OUT  FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                      O_segment2         IN OUT  FIF_GL_ACCT.ATTRIBUTE2%TYPE,
                      O_segment3         IN OUT  FIF_GL_ACCT.ATTRIBUTE3%TYPE,
                      O_segment4         IN OUT  FIF_GL_ACCT.ATTRIBUTE4%TYPE,
                      O_segment5         IN OUT  FIF_GL_ACCT.ATTRIBUTE5%TYPE,
                      O_segment6         IN OUT  FIF_GL_ACCT.ATTRIBUTE6%TYPE,
                      O_segment7         IN OUT  FIF_GL_ACCT.ATTRIBUTE7%TYPE,
                      O_segment8         IN OUT  FIF_GL_ACCT.ATTRIBUTE8%TYPE,
                      O_segment9         IN OUT  FIF_GL_ACCT.ATTRIBUTE9%TYPE,
                      O_segment10        IN OUT  FIF_GL_ACCT.ATTRIBUTE10%TYPE,
                      O_segment1_label   IN OUT  FIF_GL_ACCT.DESCRIPTION1%TYPE,
                      O_segment2_label   IN OUT  FIF_GL_ACCT.DESCRIPTION2%TYPE,
                      O_segment3_label   IN OUT  FIF_GL_ACCT.DESCRIPTION3%TYPE,
                      O_segment4_label   IN OUT  FIF_GL_ACCT.DESCRIPTION4%TYPE,
                      O_segment5_label   IN OUT  FIF_GL_ACCT.DESCRIPTION5%TYPE,
                      O_segment6_label   IN OUT  FIF_GL_ACCT.DESCRIPTION6%TYPE,
                      O_segment7_label   IN OUT  FIF_GL_ACCT.DESCRIPTION7%TYPE,
                      O_segment8_label   IN OUT  FIF_GL_ACCT.DESCRIPTION8%TYPE,
                      O_segment9_label   IN OUT  FIF_GL_ACCT.DESCRIPTION9%TYPE,
                      O_segment10_label  IN OUT  FIF_GL_ACCT.DESCRIPTION10%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--Function Name:  GET_CCID
--Purpose:  This function retrieves the CCID (code_combination_id) from the
--          table gl_code_combinations (Oracle) table.  This function is Oracle
--          specific and will be used in the 7.0 Financial Interface Project.
--Date Created: 4-May-98
------------------------------------------------------------------------
FUNCTION GET_CCID(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_ccid            IN OUT  FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE,
                  I_set_of_books_id IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                  I_sequence1       IN      FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                  I_sequence2       IN      FIF_GL_ACCT.ATTRIBUTE2%TYPE,
                  I_sequence3       IN      FIF_GL_ACCT.ATTRIBUTE3%TYPE,
                  I_sequence4       IN      FIF_GL_ACCT.ATTRIBUTE4%TYPE,
                  I_sequence5       IN      FIF_GL_ACCT.ATTRIBUTE5%TYPE,
                  I_sequence6       IN      FIF_GL_ACCT.ATTRIBUTE6%TYPE,
                  I_sequence7       IN      FIF_GL_ACCT.ATTRIBUTE7%TYPE,
                  I_sequence8       IN      FIF_GL_ACCT.ATTRIBUTE8%TYPE,
                  I_sequence9       IN      FIF_GL_ACCT.ATTRIBUTE9%TYPE,
                  I_sequence10      IN      FIF_GL_ACCT.ATTRIBUTE10%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
--  Function    :  sa_fif_validation
--  Purpose     :  This function will check whether a record is
--                 acceptable before it gets inserted into the
--                 SA_FIF_GL_cross_ref table.'Acceptable' implies
--                 that first,it is not a duplicate of an existing record
--                (only one account code can be assigned to one rollup level
--                 1,2,3,store and total_id combination) and that if a more
--                 general record exists, a more specific one will not be
--                 inserted , and likewise ,if a specific record exists, a
--                 general one encompassing it isn't inserted.
--  Created     :  24 Feb 2000
---------------------------------------------------------------------------------------
FUNCTION SA_FIF_VALIDATION( O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT  BOOLEAN,
                            I_set_of_books_id   IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                            I_store             IN      SA_FIF_GL_CROSS_REF.STORE%TYPE,
                            I_total_id          IN      SA_FIF_GL_CROSS_REF.TOTAL_ID%TYPE,
                            I_roll1             IN      SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_1%TYPE,
                            I_roll2             IN      SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_2%TYPE,
                            I_roll3             IN      SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_3%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--Function Name:  FIF_VALIDATION
--Purpose:  The validation process should verify that on the table fif_gl_cross_ref,
-- no two rows with the same dept, class, location, etc may also have a dept, class,
-- location ,etc that has an identical value. Additionally, there should exist no conflicts
-- where, for example, a record is written for Dept 1000, class 1000, subclass -1
-- (the given values in combination with all possible subclasses) and another record is
-- written for dept 1000, class 1000, subclass 1001.  The second record would be in conflict
-- with the wildcard nature of the first.  This function is Oracle specific and is
-- used in the 7.0 Financial Interface Project.
--Date Created: 4-May-98
---------------------------------------------------------------------------------------
FUNCTION FIF_VALIDATION(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists            IN OUT   BOOLEAN,
                        I_set_of_books_id   IN       FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                        I_dept              IN       FIF_GL_CROSS_REF.DEPT%TYPE,
                        I_class             IN       FIF_GL_CROSS_REF.CLASS%TYPE,
                        I_subclass          IN       FIF_GL_CROSS_REF.SUBCLASS%TYPE,
                        I_location          IN       FIF_GL_CROSS_REF.LOCATION%TYPE,
                        I_tran_code         IN       FIF_GL_CROSS_REF.TRAN_CODE%TYPE,
                        I_tran_ref_no       IN       FIF_GL_CROSS_REF.TRAN_REF_NO%TYPE,
                        I_cost_retail_flag  IN       FIF_GL_CROSS_REF.COST_RETAIL_FLAG%TYPE,
                        I_line_type         IN       FIF_GL_CROSS_REF.LINE_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--Function Name:  GET_LINE_TYPE_DESC
--Purpose:  This function retrieves the decode values (descriptions) from the
--          table fif_line_type_xref.  This function is Oracle specific
--          and will be used in the 7.0 Financial Interface Project.
--Date Created: 4-May-98
---------------------------------------------------------------------------------------
FUNCTION GET_LINE_TYPE_DESC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_desc           IN OUT   FIF_LINE_TYPE_XREF.RMS_LINE_TYPE_DESC%TYPE,
                            I_line_type      IN       FIF_LINE_TYPE_XREF.RMS_LINE_TYPE %TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--Function Name:  GET_SEG_DESC
--Purpose:  This function retrieves a segment description from the fif_gl_acct table based
--          on the passed in segment.
------------------------------------------------------------------------------------------
FUNCTION GET_SEG_DESC(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_desc             IN OUT  FIF_GL_ACCT.DESCRIPTION1%TYPE,
                      I_set_of_books_id  IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                      I_segment          IN      FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                      I_segment_id       IN      VARCHAR2)
                      RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:  GET_ACCT
--Purpose:  This function retrieves the primary_account from fif_gl_acct table based
--          on the passed in segments.  The segments have to be in the same sequence as on the table.
-----------------------------------------------------------------------------------------------------
FUNCTION GET_ACCT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_prim_acct        IN OUT  FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE,
                  I_set_of_books_id  IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                  I_segment1         IN      FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                  I_segment2         IN      FIF_GL_ACCT.ATTRIBUTE2%TYPE,
                  I_segment3         IN      FIF_GL_ACCT.ATTRIBUTE3%TYPE,
                  I_segment4         IN      FIF_GL_ACCT.ATTRIBUTE4%TYPE,
                  I_segment5         IN      FIF_GL_ACCT.ATTRIBUTE5%TYPE,
                  I_segment6         IN      FIF_GL_ACCT.ATTRIBUTE6%TYPE,
                  I_segment7         IN      FIF_GL_ACCT.ATTRIBUTE7%TYPE,
                  I_segment8         IN      FIF_GL_ACCT.ATTRIBUTE8%TYPE,
                  I_segment9         IN      FIF_GL_ACCT.ATTRIBUTE9%TYPE,
                  I_segment10        IN      FIF_GL_ACCT.ATTRIBUTE10%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--Function Name:  GET_ACCT_DESC
--Purpose:  This function retrieves segment descriptions from the fif_gl_acct table based
--          on the passed in primary account.
------------------------------------------------------------------------------------------
FUNCTION GET_ACCT_DESCS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_desc1            IN OUT  FIF_GL_ACCT.DESCRIPTION1%TYPE,
                        O_desc2            IN OUT  FIF_GL_ACCT.DESCRIPTION2%TYPE,
                        O_desc3            IN OUT  FIF_GL_ACCT.DESCRIPTION3%TYPE,
                        O_desc4            IN OUT  FIF_GL_ACCT.DESCRIPTION4%TYPE,
                        O_desc5            IN OUT  FIF_GL_ACCT.DESCRIPTION5%TYPE,
                        O_desc6            IN OUT  FIF_GL_ACCT.DESCRIPTION6%TYPE,
                        O_desc7            IN OUT  FIF_GL_ACCT.DESCRIPTION7%TYPE,
                        O_desc8            IN OUT  FIF_GL_ACCT.DESCRIPTION8%TYPE,
                        O_desc9            IN OUT  FIF_GL_ACCT.DESCRIPTION9%TYPE,
                        O_desc10           IN OUT  FIF_GL_ACCT.DESCRIPTION10%TYPE,
                        I_set_of_books_id  IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE,
                        I_prim_acct        IN      FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--Function Name:  GET_NEXT_ID
--Purpose:  This function retrieves the next valid fif_gl_cross_ref_id in sequence
------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ID (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_id_number     IN OUT FIF_GL_CROSS_REF.FIF_GL_CROSS_REF_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function    : GET_SEQUENCE_LABELS
-- Purpose     : To retrieve segment labels from the FIF_GL_SETUP table.
---------------------------------------------------------------------------------------
FUNCTION GET_SEQUENCE_LABELS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_sequence1_desc  IN OUT  FIF_GL_SETUP.SEQUENCE1_DESC%TYPE,
                             O_sequence2_desc  IN OUT  FIF_GL_SETUP.SEQUENCE2_DESC%TYPE,
                             O_sequence3_desc  IN OUT  FIF_GL_SETUP.SEQUENCE3_DESC%TYPE,
                             O_sequence4_desc  IN OUT  FIF_GL_SETUP.SEQUENCE4_DESC%TYPE,
                             O_sequence5_desc  IN OUT  FIF_GL_SETUP.SEQUENCE5_DESC%TYPE,
                             O_sequence6_desc  IN OUT  FIF_GL_SETUP.SEQUENCE6_DESC%TYPE,
                             O_sequence7_desc  IN OUT  FIF_GL_SETUP.SEQUENCE7_DESC%TYPE,
                             O_sequence8_desc  IN OUT  FIF_GL_SETUP.SEQUENCE8_DESC%TYPE,
                             O_sequence9_desc  IN OUT  FIF_GL_SETUP.SEQUENCE9_DESC%TYPE,
                             O_sequence10_desc IN OUT  FIF_GL_SETUP.SEQUENCE10_DESC%TYPE,
                             I_set_of_books_id IN      FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function    : GET_FIN_REF_KEY
-- Purpose     : To retrieve the reference_trace_id from the KEY_MAP_GL table against the
--               8 input parameters of the TRAN_DATA_HISTORY table.
------------------------------------------------------------------------------------------
FUNCTION GET_FIN_REF_KEY(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_reference_trace_id   IN OUT  KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE,
                         I_iftd_rowid           IN      KEY_MAP_GL.IFTD_ROWID%TYPE,
                         I_item                 IN      KEY_MAP_GL.ITEM%TYPE,
                         I_dept                 IN      KEY_MAP_GL.DEPT%TYPE,
                         I_class                IN      KEY_MAP_GL.CLASS%TYPE,
                         I_subclass             IN      KEY_MAP_GL.SUBCLASS%TYPE,
                         I_procesed_date        IN      KEY_MAP_GL.PROCESSED_DATE%TYPE,
                         I_tran_code            IN      KEY_MAP_GL.TRAN_CODE%TYPE,
                         I_location             IN      KEY_MAP_GL.LOCATION%TYPE,
                         I_loc_type             IN      KEY_MAP_GL.LOC_TYPE%TYPE,
                         I_reference_trace_type IN      KEY_MAP_GL.REFERENCE_TRACE_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function    : GET_KEY_MAP_GL
-- Purpose     : To retrieve the data from the KEY_MAP_GL table against the
--               input parameter reference_trace_id.
------------------------------------------------------------------------------------------
FUNCTION GET_KEY_MAP_GL(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_reference_trace_type  IN OUT  KEY_MAP_GL.REFERENCE_TRACE_TYPE%TYPE,
                        O_iftd_rowid            IN OUT  KEY_MAP_GL.IFTD_ROWID%TYPE,
                        O_item                  IN OUT  KEY_MAP_GL.ITEM%TYPE,
                        O_dept                  IN OUT  KEY_MAP_GL.DEPT%TYPE,
                        O_class                 IN OUT  KEY_MAP_GL.CLASS%TYPE,
                        O_subclass              IN OUT  KEY_MAP_GL.SUBCLASS%TYPE,
                        O_procesed_date         IN OUT  KEY_MAP_GL.PROCESSED_DATE%TYPE,
                        O_tran_code             IN OUT  KEY_MAP_GL.TRAN_CODE%TYPE,
                        O_location              IN OUT  KEY_MAP_GL.LOCATION%TYPE,
                        O_loc_type              IN OUT  KEY_MAP_GL.LOC_TYPE%TYPE,
                        I_reference_trace_id    IN      KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function    : VALIDATE_STORE_DAY_SEQ_NO
-- Purpose     : To validate store_day_seq_no against both SA_STORE_DAY and SA_GL_REF_DATA tables.
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE_DAY_SEQ_NO(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists           IN OUT  BOOLEAN,
                                   I_store_day_seq_no IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function    : VALIDATE_TOTAL_ID
-- Purpose     : To validate total_id against both SA_TOTAL_HEAD and SA_GL_REF_DATA tables.
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TOTAL_ID(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists           IN OUT  BOOLEAN,
                           I_total_id         IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function    : GET_SYSTEM_VARIABLES
-- Purpose     : This function returns all system_variables in one rowtype variable.
-------------------------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_VARIABLES(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_system_variables_row    IN OUT  SYSTEM_VARIABLES%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END RMS2FIN;
/
