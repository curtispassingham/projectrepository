CREATE OR REPLACE PACKAGE CORESVC_SA_ERROR_CODES AUTHID CURRENT_USER
is

	FUNCTION process(
	 O_error_message IN OUT rtk_errors.rtk_text%type,
	 I_process_id    IN Number,
	 O_error_count OUT NUMBER) RETURN BOOLEAN;
	 
	Type SA_ERROR_IMPACT_rec_tab
	IS
	TABLE OF SA_ERROR_IMPACT%rowtype;   
END coresvc_sa_error_codes;
/ 