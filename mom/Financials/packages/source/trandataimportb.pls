CREATE OR REPLACE PACKAGE BODY TRAN_DATA_IMPORT_SQL AS
----------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS SPEC
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
--    Function : POPULATE_GTT
-- Description : Retrieves the records from STG_EXT_TRAN_DATA and populates
--               GTT_STG_EXT_TRAN_DATA table for the Chunk ID passed in.
----------------------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(I_chunk_id     IN       STAGE_EXT_TRAN_DATA_CHUNK.CHUNK_ID%TYPE,
                      O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--    Function : VALIDATE
-- Description : This function validates the records on GTT_STG_EXT_TRAN_DATA
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE(O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--    Function : RECALC_WAC
-- Description : This function recalculates the WAC for valid records w/ wac_recalc_ind = 'Y'.
----------------------------------------------------------------------------------------------
FUNCTION RECALC_WAC(O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--    Function : INSERT_TRAN_DATA
-- Description : This function inserts records into TRAN_DATA.
----------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA(O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--    Function : MERGE_ERROR_STATUS
-- Description : This function merges back the records from GTT_STG_EXT_TRAN_DATA to
--               STG_EXT_TRAN_DATA and updates the status to 'E'rror or 'P'rocessed.
----------------------------------------------------------------------------------------------
FUNCTION MERGE_ERROR_STATUS(I_chunk_id     IN       STAGE_EXT_TRAN_DATA_CHUNK.CHUNK_ID%TYPE,
                            O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS BODY
----------------------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(I_chunk_id     IN       STAGE_EXT_TRAN_DATA_CHUNK.CHUNK_ID%TYPE,
                      O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(64) := 'TRAN_DATA_IMPORT_SQL.POPULATE_GTT';
   L_chunk_status   STAGE_EXT_TRAN_DATA_CHUNK.CHUNK_STATUS%TYPE;

BEGIN

   -- Validate required inputs
   if I_chunk_id is NULL then
      O_error_code := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                         'I_chunk_id',
                                          L_program,
                                          NULL);
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('INSERT',
                    'GTT_STAGE_EXT_TRAN_DATA',
                    NULL,
                    NULL);

   insert into gtt_stage_ext_tran_data (tran_id,
                                        item,
                                        dept,
                                        class,
                                        subclass,
                                        loc_type,
                                        location,
                                        tran_date,
                                        tran_code,
                                        adj_code,
                                        units,
                                        total_cost,
                                        total_retail,
                                        ref_no_1,
                                        ref_no_2,
                                        gl_ref_no,
                                        old_unit_retail,
                                        new_unit_retail,
                                        pgm_name,
                                        sales_type,
                                        vat_rate,
                                        av_cost,
                                        ref_pack_no,
                                        total_cost_excl_elc,
                                        wac_recalc_ind,
                                        status,
                                        err_msg)
                                (select distinct source.tran_id,
                                        source.item,
                                        source.dept,
                                        source.class,
                                        source.subclass,
                                        source.loc_type,
                                        source.location,
                                        source.tran_date,
                                        source.tran_code,
                                        source.adj_code,
                                        source.units,
                                        source.total_cost,
                                        source.total_retail,
                                        source.ref_no_1,
                                        source.ref_no_2,
                                        source.gl_ref_no,
                                        source.old_unit_retail,
                                        source.new_unit_retail,
                                        source.pgm_name,
                                        source.sales_type,
                                        source.vat_rate,
                                        source.av_cost,
                                        source.ref_pack_no,
                                        source.total_cost_excl_elc,
                                        source.wac_recalc_ind,
                                        source.status,
                                        source.err_msg
                                   from stage_ext_tran_data_chunk chunk,
                                        stage_ext_tran_data source
                                  where chunk.chunk_id = I_chunk_id
                                    and chunk.location = source.location
                                    and chunk.subclass = source.subclass
                                    and NVL(chunk.item,'-999') = NVL(source.item,'-999')
                                    and chunk.chunk_status = 'N');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_code := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_GTT;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE(O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'TRAN_DATA_IMPORT_SQL.VALIDATE';

   L_inv_store_err                   RTK_ERRORS.RTK_TEXT%TYPE;
   L_inv_wh_err                      RTK_ERRORS.RTK_TEXT%TYPE;
   L_inv_tran_code_err               RTK_ERRORS.RTK_TEXT%TYPE;
   L_inv_org_err                     RTK_ERRORS.RTK_TEXT%TYPE;
   L_null_item_err                   RTK_ERRORS.RTK_TEXT%TYPE;
   L_inv_item_err                    RTK_ERRORS.RTK_TEXT%TYPE;
   L_inv_item_tran_err               RTK_ERRORS.RTK_TEXT%TYPE;
   L_inv_item_org_err                RTK_ERRORS.RTK_TEXT%TYPE;
   L_inv_item_loc_err                RTK_ERRORS.RTK_TEXT%TYPE;
   L_pack_item_err                   RTK_ERRORS.RTK_TEXT%TYPE;
   L_null_item_cost_err              RTK_ERRORS.RTK_TEXT%TYPE;
   L_tran_cost_err                   RTK_ERRORS.RTK_TEXT%TYPE;
   L_cost_retail_err                 RTK_ERRORS.RTK_TEXT%TYPE;
   L_gl_ref_no_err                   RTK_ERRORS.RTK_TEXT%TYPE;
   L_old_new_unit_retail_err         RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   L_inv_store_err := SQL_LIB.GET_MESSAGE_TEXT('INVALID_STORE',
                                                NULL,
                                                NULL,
                                                NULL);

   L_inv_wh_err := SQL_LIB.GET_MESSAGE_TEXT('INV_WH',
                                             NULL,
                                             NULL,
                                             NULL);

   L_inv_tran_code_err := SQL_LIB.GET_MESSAGE_TEXT('INV_TRAN_CODE',
                                                    NULL,
                                                    NULL,
                                                    NULL);

   L_inv_org_err := SQL_LIB.GET_MESSAGE_TEXT('INV_SUBCLASS',
                                              NULL,
                                              NULL,
                                              NULL);

   L_null_item_err := SQL_LIB.GET_MESSAGE_TEXT('COST_DEPT_NO_ITEM',
                                                NULL,
                                                NULL,
                                                NULL);

   L_inv_item_err := SQL_LIB.GET_MESSAGE_TEXT('INVALID_ITEM',
                                               NULL,
                                               NULL,
                                               NULL);

   L_inv_item_tran_err := SQL_LIB.GET_MESSAGE_TEXT('TRAN_AND_APPROVED',
                                                    NULL,
                                                    NULL,
                                                    NULL);

   L_inv_item_org_err := SQL_LIB.GET_MESSAGE_TEXT('INV_ITEM_ORG_HIER',
                                                   NULL,
                                                   NULL,
                                                   NULL);

   L_pack_item_err := SQL_LIB.GET_MESSAGE_TEXT('NO_PACK_ITEM',
                                                NULL,
                                                NULL,
                                                NULL);

   L_inv_item_loc_err := SQL_LIB.GET_MESSAGE_TEXT('NO_ITEM_LOC_RELATIONSHIP',
                                                   NULL,
                                                   NULL,
                                                   NULL);

   L_null_item_cost_err := SQL_LIB.GET_MESSAGE_TEXT('WAC_NO_ITEM_TOTAL_COST',
                                                     NULL,
                                                     NULL,
                                                     NULL);

   L_tran_cost_err := SQL_LIB.GET_MESSAGE_TEXT('NO_TOTAL_COST_RETAIL_COST',
                                                NULL,
                                                NULL,
                                                NULL);

   L_cost_retail_err := SQL_LIB.GET_MESSAGE_TEXT('STKLEDGR_PRICING',
                                                  L_program,
                                                  NULL,
                                                  NULL);

   L_gl_ref_no_err := SQL_LIB.GET_MESSAGE_TEXT('NO_GL_REF_NO',
                                                NULL,
                                                NULL,
                                                NULL);

   L_old_new_unit_retail_err := SQL_LIB.GET_MESSAGE_TEXT('STKLEDGR_RETAIL',
                                                          L_program,
                                                          NULL,
                                                          NULL);

   ----------------------------------------------------------------------
   -- LOCATION and LOC_TYPE should be validated against Store, WH tables
   ----------------------------------------------------------------------
   merge into gtt_stage_ext_tran_data target
   using (select xtd.tran_id,
                 xtd.loc_type,
                 xtd.location,
                 xtd.status,
                 xtd.err_msg
            from gtt_stage_ext_tran_data xtd
           where not exists (select 1
                               from v_location v
                              where v.location_id = xtd.location)) source
      on (target.tran_id = source.tran_id)
    when matched then
  update set target.err_msg = DECODE(source.loc_type,
                                     'W', target.err_msg || L_inv_wh_err || ';',
                                     'S', target.err_msg || L_inv_store_err || ';'),
             target.status  = TRAN_DATA_IMPORT_SQL.ERROR; --error out

   ----------------------------------------------------------------------
   -- TRAN_CODE should be validated against TRAN_DATA_CODES table
   ----------------------------------------------------------------------
   merge into gtt_stage_ext_tran_data target
   using (select xtd.tran_id,
                 xtd.tran_code,
                 xtd.err_msg
            from gtt_stage_ext_tran_data xtd
           where not exists (select 1
                               from tran_data_codes tdc
                              where tdc.code = xtd.tran_code)) source
      on (target.tran_id = source.tran_id)
    when matched then
  update set target.err_msg = target.err_msg || L_inv_tran_code_err || ';',
              target.status = TRAN_DATA_IMPORT_SQL.ERROR; --error out

   ----------------------------------------------------------------------
   -- DEPT, CLASS, SUBCLASS should be validated against SUBCLASS table
   ----------------------------------------------------------------------
   merge into gtt_stage_ext_tran_data target
   using (select xtd.tran_id,
                 xtd.dept,
                 xtd.class,
                 xtd.subclass,
                 xtd.err_msg
            from gtt_stage_ext_tran_data xtd
           where not exists (select 1
                               from subclass s
                              where s.dept = xtd.dept
                                and s.class = xtd.class
                                and s.subclass = xtd.subclass)) source
      on (target.tran_id = source.tran_id)
    when matched then
  update set target.err_msg = target.err_msg || L_inv_org_err || ';',
              target.status = TRAN_DATA_IMPORT_SQL.ERROR; --error out

   -------------------------------------------------------------
   -- If item is not NULL validate against ITEM_MASTER.
   -------------------------------------------------------------
   merge into gtt_stage_ext_tran_data target
   using (select xtd.tran_id,
                 xtd.item,
                 xtd.err_msg
            from gtt_stage_ext_tran_data xtd
           where xtd.item is NOT NULL
             and not exists (select 1
                               from item_master im
                              where im.item = xtd.item)) source
      on (target.tran_id = source.tran_id)
    when matched then
  update set target.err_msg = target.err_msg||L_inv_item_err||';',
             target.status = TRAN_DATA_IMPORT_SQL.ERROR; --error out

   ----------------------------------------------------------------------
   -- Validate ITEM_MASTER
   ----------------------------------------------------------------------
   merge into gtt_stage_ext_tran_data target
   using (select xtd.tran_id,
                 xtd.item,
                 xtd.dept,
                 xtd.class,
                 xtd.subclass,
                 xtd.sales_type,
                 xtd.wac_recalc_ind,
                 xtd.status,
                 xtd.err_msg,
                 -------------------------------------------------------------
                 -- If item is not NULL, validate if the item exists, at tran level and in 'A'pproved status
                 -------------------------------------------------------------
                 case
                    when (im.item_level <> im.tran_level or im.status <> 'A')
                       then L_inv_item_tran_err
                    else NULL
                 end as inv_item_tran_err,
                 -------------------------------------------------------------
                 -- If Item is not NULL validate if the item belongs to the dept/class/subclass
                 -------------------------------------------------------------
                 case
                    when (im.subclass != xtd.subclass or
                          im.dept != xtd.dept or
                          im.class != xtd.class)
                       then L_inv_item_org_err
                    else NULL
                 end as inv_org_err,
                 -------------------------------------------------------------
                 -- if item not NULL and is a pack item then the record should be rejected
                 -------------------------------------------------------------
                 case
                    when im.pack_ind = 'Y'
                       then L_pack_item_err
                    else NULL
                 end as pack_item_err,
                 -------------------------------------------------------------
                 -- For tran_code 1, 4, 20, 24, 27, 30, 31, 37, and 38, the record should be rejected
                 -- if total cost is NULL OR (total_retail is NULL and sellable_ind is 'Y').
                 -------------------------------------------------------------
                 case
                    when xtd.tran_code IN (1, 4, 20, 24, 27, 30, 31, 37, 38) and (xtd.total_cost IS NULL or (xtd.total_retail IS NULL and im.sellable_ind ='Y'))
                       then L_cost_retail_err
                    else NULL
                 end as no_cost_retail_sellable_err
            from gtt_stage_ext_tran_data xtd,
                 item_master im
           where xtd.item = im.item
             and xtd.item is NOT NULL) source
      on (target.tran_id = source.tran_id)
    when matched then
  update set target.err_msg = target.err_msg ||
                              source.inv_item_tran_err || DECODE(source.inv_item_tran_err, NULL, NULL, ';') ||
                              source.inv_org_err || DECODE(source.inv_org_err, NULL, NULL, ';') ||
                              source.pack_item_err || DECODE(source.pack_item_err, NULL, NULL, ';') ||
                              source.no_cost_retail_sellable_err || DECODE(source.no_cost_retail_sellable_err, NULL, NULL, ';'),
              target.status = DECODE(source.inv_item_tran_err||source.inv_org_err||source.pack_item_err||source.no_cost_retail_sellable_err,
                              NULL, target.status, TRAN_DATA_IMPORT_SQL.ERROR); --error out  

   ----------------------------------------------------------------------
   -- If item is not NULL validate if it is ranged to the location.
   ----------------------------------------------------------------------
   merge into gtt_stage_ext_tran_data target
   using (select xtd.tran_id,
                 xtd.item,
                 xtd.location,
                 xtd.loc_type,
                 xtd.err_msg
            from gtt_stage_ext_tran_data xtd
           where xtd.item is NOT NULL
             and not exists (select 1
                               from item_loc il
                              where il.loc = xtd.location
                                and il.loc_type = xtd.loc_type
                                and il.item = xtd.item)) source
      on (target.tran_id = source.tran_id)
    when matched then
  update set target.err_msg = target.err_msg || L_inv_item_loc_err || ';',
             target.status  = TRAN_DATA_IMPORT_SQL.ERROR; --error out

   ----------------------------------------------------------------------
   -- Validate stage table
   ----------------------------------------------------------------------
   merge into gtt_stage_ext_tran_data target
   using (select xtd.tran_id,
                 xtd.item,
                 xtd.tran_code,
                 xtd.total_cost,
                 xtd.total_retail,
                 xtd.wac_recalc_ind,
                 xtd.status,
                 -------------------------------------------------------------
                 -- For dept as cost accounting, item should be mandatory.
                 -- For retail based dept, items can be NULL.
                 -------------------------------------------------------------
                 case
                    when d.profit_calc_type = 1 and xtd.item is NULL
                       then L_null_item_err
                    else NULL
                 end as no_item_err,
                 -------------------------------------------------------------
                 -- When RECAL_WAC_IND = 'Y', ITEM and TOTAL_COST should not be NULL.
                 -------------------------------------------------------------
                 case
                    when xtd.wac_recalc_ind = 'Y' and (xtd.total_cost is NULL or xtd.item is NULL)
                      then L_null_item_cost_err
                      else NULL
                 end as no_total_cost_item_err,
                 -------------------------------------------------------------
                 -- total_cost and total_retail are required.
                 -------------------------------------------------------------
                 case
                    when xtd.total_cost IS NULL and xtd.total_retail IS NULL
                       then L_tran_cost_err
                    else NULL
                 end as no_total_cost_retail_cost_err,
                 -------------------------------------------------------------
                 -- For tran_code in (37,38,63,64), if gl_ref_no is NULL then records should be rejected.
                 -------------------------------------------------------------
                 case
                    when xtd.tran_code IN (37,38,63,64) and xtd.gl_ref_no IS NULL
                       then L_gl_ref_no_err
                    else NULL
                 end as no_gl_ref_no_err,
                 -------------------------------------------------------------
                 -- For tran code in (22,23) if total_cost is NULL then records should be rejected.
                 -- For tran_code in (26,70,80) if (total_cost is NULL or total retail is NOT NULL) then records should be rejected.
                 -- For tran code in (11,12,13,14,15,16,60,81) if (total retail is NULL or total cost is NOT NULL) then records should be rejected.
                 -------------------------------------------------------------
                 case
                    when (xtd.tran_code IN (22,23) and xtd.total_cost IS NULL) or
                         (xtd.tran_code IN (26,70,80) and (xtd.total_cost IS NULL or xtd.total_retail IS NOT NULL)) or
                         (xtd.tran_code IN (11,12,13,14,15,16,60,81) and
                         (xtd.total_retail IS NULL or xtd.total_cost IS NOT NULL))
                       then L_cost_retail_err
                    else NULL
                 end as no_total_cost_total_retail_err,
                 -------------------------------------------------------------
                 -- For tran code in (11,12,13,14,15,16), if old or new unit retail is NULL then records should be rejected.
                 -------------------------------------------------------------
                 case
                    when xtd.tran_code IN (11,12,13,14,15,16) and
                         (xtd.old_unit_retail IS NULL or xtd.new_unit_retail IS NULL)
                       then L_old_new_unit_retail_err
                    else NULL
                 end as no_old_new_unit_retail_err
            from gtt_stage_ext_tran_data xtd,
                 deps d
           where xtd.dept = d.dept) source
      on (target.tran_id = source.tran_id)
    when matched then
  update set target.err_msg = target.err_msg||
                              source.no_item_err || DECODE(source.no_item_err, NULL, NULL, ';') ||
                              source.no_total_cost_item_err || DECODE(source.no_total_cost_item_err, NULL, NULL, ';') ||
                              source.no_total_cost_retail_cost_err || DECODE(source.no_total_cost_retail_cost_err, NULL, NULL, ';') ||
                              source.no_gl_ref_no_err || DECODE(source.no_gl_ref_no_err, NULL, NULL, ';') ||
                              source.no_old_new_unit_retail_err || DECODE(source.no_old_new_unit_retail_err, NULL, NULL, ';') ||
                              source.no_total_cost_total_retail_err || DECODE(source.no_total_cost_total_retail_err, NULL, NULL, ';'),
              target.status = DECODE(source.no_item_err||source.no_total_cost_item_err||source.no_total_cost_retail_cost_err||source.no_gl_ref_no_err||source.no_old_new_unit_retail_err||source.no_total_cost_total_retail_err,
                                     NULL, target.status, TRAN_DATA_IMPORT_SQL.ERROR); --error out  

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_code := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE;
----------------------------------------------------------------------------------------------
FUNCTION RECALC_WAC(O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'TRAN_DATA_IMPORT_SQL.RECALC_WAC';
   L_vdate        PERIOD.VDATE%TYPE;

BEGIN

   -- Get vdate
   select vdate
     into L_vdate
     from period;

   --Calculate WAC for ILS records having SOH > 0 and store calculated values on GTT

   SQL_LIB.SET_MARK('INSERT',
                    'GTT_STAGE_EXT_TRAN_DATA_CALC',
                    NULL,
                    NULL);

   insert into gtt_stage_ext_tran_data_calc (item,
                                             location,
                                             sum_units,
                                             sum_total_cost,
                                             wac_calc)
                                      select ils.item,
                                             xtd.location,
                                             sum_units,
                                             sum_total_cost,
                                             ils.av_cost +
                                                (xtd.sum_total_cost/
                                                (ils.stock_on_hand + ils.in_transit_qty + ils.pack_comp_intran + ils.pack_comp_soh)) avg_cost
                                        from (select item, 
                                                     location,
                                                     sum(units) sum_units,
                                                     sum(total_cost) sum_total_cost
                                                from gtt_stage_ext_tran_data td
                                               where td.wac_recalc_ind = 'Y'
                                                 and td.status = 'N'
                                                 and td.item IS NOT NULL
                                               group by item, location) xtd,
                                             item_loc_soh ils
                                       where xtd.item = ils.item
                                         and xtd.location = ils.loc
                                         and (ils.stock_on_hand + ils.in_transit_qty + ils.pack_comp_intran + ils.pack_comp_soh) > 0;

   -- Case 1. SOH > 0 and New_WAC > 0

   SQL_LIB.SET_MARK('MERGE',
                    'ITEM_LOC_SOH',
                    NULL,
                    NULL);

   merge into item_loc_soh target
   using (select xtdc.item,
                 xtdc.location,
                 xtdc.wac_calc
            from gtt_stage_ext_tran_data_calc xtdc
           where xtdc.wac_calc >= 0) source 
      on (target.item = source.item and
          target.loc  = source.location)
    when matched then
  update set target.av_cost = source.wac_calc,
             target.last_update_datetime = sysdate,
             target.last_update_id = get_user;

   -- Case 2. SOH > 0 and New_WAC <= 0
   -- Insert a variance record (Tran code 70) to the GTT table for that particular
   -- item/loc with sum_total_cost from gtt_stage_ext_tran_data_calc.

   SQL_LIB.SET_MARK('INSERT',
                    'GTT_STAGE_EXT_TRAN_DATA',
                    NULL,
                    NULL);

   insert into gtt_stage_ext_tran_data (item,
                                        dept,
                                        class,
                                        subclass,
                                        loc_type,
                                        location,
                                        units,
                                        tran_date,
                                        tran_code,
                                        total_cost,
                                        ref_pack_no,
                                        status)
                                 select xtd.item,
                                        xtd.dept,
                                        xtd.class,
                                        xtd.subclass,
                                        xtd.loc_type,
                                        xtd.location,
                                        xtdc.sum_units,
                                        L_vdate,
                                        70,
                                        xtdc.sum_total_cost, 
                                        xtd.ref_pack_no,
                                        'N'
                                   from gtt_stage_ext_tran_data_calc xtdc,
                                        gtt_stage_ext_tran_data xtd
                                  where xtd.item = xtdc.item
                                    and xtd.location = xtdc.location
                                    and xtdc.wac_calc <= 0;

   -- Case 3. SOH <= 0
   -- Do not update WAC on ITEM_LOC_SOH.
   -- Instead insert a variance record (Tran code 70) on the GTT for the item/loc
   -- where total_cost is the SUM of the total cost for all transactions with
   -- wac_recalc_ind = 'Y' on GTT for the item/loc.

   SQL_LIB.SET_MARK('INSERT',
                    'GTT_STAGE_EXT_TRAN_DATA',
                    NULL,
                    NULL);

   insert into gtt_stage_ext_tran_data (item,
                                        dept,
                                        class,
                                        subclass,
                                        loc_type,
                                        location,
                                        units,
                                        tran_date,
                                        tran_code,
                                        total_cost,
                                        ref_pack_no,
                                        status)
                                (select xtd.item,
                                        xtd.dept,
                                        xtd.class,
                                        xtd.subclass,
                                        xtd.loc_type,
                                        xtd.location,
                                        SUM(xtd.units),
                                        L_vdate,
                                        70,
                                        SUM(xtd.total_cost) sum_total_cost,
                                        xtd.ref_pack_no,
                                        'N'
                                   from gtt_stage_ext_tran_data xtd,
                                        item_loc_soh ils
                                  where xtd.item = ils.item
                                    and xtd.location = ils.loc
                                    and (ils.stock_on_hand + ils.in_transit_qty + ils.pack_comp_intran + ils.pack_comp_soh) <= 0

                                    and xtd.wac_recalc_ind = 'Y'
                                    and xtd.status = 'N'
                                    and xtd.item IS NOT NULL 
                               group by xtd.item,
                                        xtd.dept,
                                        xtd.class,
                                        xtd.subclass,
                                        xtd.loc_type,
                                        xtd.location,
                                        xtd.ref_pack_no);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_code := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RECALC_WAC;
----------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA(O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TRAN_DATA_IMPORT_SQL.INSERT_TRAN_DATA';

BEGIN

   SQL_LIB.SET_MARK('INSERT',
                    'TRAN_DATA',
                    NULL,
                    NULL);

   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
                   select source.item,
                          source.dept,
                          source.class,
                          source.subclass,
                          'N',
                          source.loc_type,
                          source.location,
                          source.tran_date,
                          source.tran_code,
                          source.adj_code,
                          source.units,
                          source.total_cost,
                          source.total_retail,
                          source.ref_no_1,
                          source.ref_no_2,
                          source.gl_ref_no,
                          source.old_unit_retail,
                          source.new_unit_retail,
                          DECODE(source.pgm_name,null,'TRAN_DATA_IMPORT_SQL',source.pgm_name||'|TRAN_DATA_IMPORT_SQL'),
                          source.sales_type,
                          source.vat_rate,
                          source.av_cost,
                          sysdate,
                          source.ref_pack_no,
                          source.total_cost_excl_elc
                     from gtt_stage_ext_tran_data source
                    where source.status = 'N'
                      and source.err_msg IS NULL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_code := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_TRAN_DATA;
----------------------------------------------------------------------------------------------
FUNCTION MERGE_ERROR_STATUS(I_chunk_id     IN       STAGE_EXT_TRAN_DATA_CHUNK.CHUNK_ID%TYPE,
                            O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TRAN_DATA_IMPORT_SQL.MERGE_ERROR_STATUS';

BEGIN

   SQL_LIB.SET_MARK('MERGE',
                    'STAGE_EXT_TRAN_DATA',
                    NULL,
                    NULL);

   merge into stage_ext_tran_data target
   using gtt_stage_ext_tran_data source
      on (target.tran_id = source.tran_id)
    when matched then
  update set target.status = DECODE(source.status,TRAN_DATA_IMPORT_SQL.ERROR,source.status,TRAN_DATA_IMPORT_SQL.PROCESSED),
             target.err_msg = source.err_msg,
             target.last_updated_timestamp = sysdate;

   update stage_ext_tran_data_chunk
      set chunk_status = TRAN_DATA_IMPORT_SQL.PROCESSED
    where chunk_id = I_chunk_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_code := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return FALSE;

END MERGE_ERROR_STATUS;

----------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION
----------------------------------------------------------------------------------------------
FUNCTION IMPORT_CHUNK(I_chunk_id     IN       STAGE_EXT_TRAN_DATA_CHUNK.CHUNK_ID%TYPE,
                      O_error_code   IN OUT   RTK_ERRORS.RTK_KEY%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TRAN_DATA_IMPORT_SQL.IMPORT_CHUNK';

BEGIN

   -- Validate required inputs
   if I_chunk_id is NULL then
      O_error_code := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                         'I_chunk_id',
                                          L_program,
                                          NULL);
      return FALSE;
   end if;

   if POPULATE_GTT(I_chunk_id,
                   O_error_code) = FALSE then
      return FALSE;
   end if;

   if VALIDATE(O_error_code) = FALSE then
      return FALSE;
   end if;

   if RECALC_WAC(O_error_code) = FALSE then
      return FALSE;
   end if;

   if INSERT_TRAN_DATA(O_error_code) = FALSE then
      return FALSE;
   end if;

   if MERGE_ERROR_STATUS(I_chunk_id,
                         O_error_code) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_code := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return FALSE;

END IMPORT_CHUNK;
----------------------------------------------------------------------------------------------
END TRAN_DATA_IMPORT_SQL;
/
