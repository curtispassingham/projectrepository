CREATE OR REPLACE PACKAGE BODY SA_TRANDATA_SQL AS
--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE
--- Purpose:        This procedure will query SA_GL_REF_DATA table and populate the above declared 
---                 table of records that will be used as the 'base table' in the Sales Audit Transaction Data From.
--------------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_sa_trandata_tbl      IN OUT   SA_TRANDATA_SQL.SA_TRANDATA_TBL,
                           I_store                 IN       SA_GL_REF_DATA.STORE%TYPE,
                           I_processed_date_start  IN       SA_GL_REF_DATA.PROCESSED_DATE%TYPE,
                           I_processed_date_end    IN       SA_GL_REF_DATA.PROCESSED_DATE%TYPE,
                           I_set_of_books_id       IN       SA_GL_REF_DATA.SET_OF_BOOKS_ID%TYPE,
                           I_acct_date_start       IN       SA_GL_REF_DATA.ACCT_DATE%TYPE,
                           I_acct_date_end         IN       SA_GL_REF_DATA.ACCT_DATE%TYPE,
                           I_total_type            IN       SA_GL_REF_DATA.TOTAL_ID%TYPE,
                           I_store_day_seq_no      IN       SA_GL_REF_DATA.STORE_DAY_SEQ_NO%TYPE,
                           I_reference_trace_id    IN       SA_GL_REF_DATA.REFERENCE_TRACE_ID%TYPE)
IS

   cursor C_SA_GL_REF_DATA is
      select store_day_seq_no,
             store,
             day,
             acct_date,
             processed_date,
             currency_code,
             set_of_books_id,
             update_id,
             total_value,
             total_id,
             attrib1,
             attrib2,
             period,
             reference_trace_id,
             null,
             null
        from sa_gl_ref_data
       where store = NVL(I_store, store)
         and (processed_date between NVL(I_processed_date_start, processed_date) and NVL(I_processed_date_end, processed_date))
         and (acct_date between NVL(I_acct_date_start, acct_date) and NVL(I_acct_date_end, acct_date))
         and total_id = NVL(I_total_type, total_id)
         and store_day_seq_no = NVL(I_store_day_seq_no, store_day_seq_no)
         and reference_trace_id = NVL(I_reference_trace_id, reference_trace_id);

BEGIN

   open C_SA_GL_REF_DATA;
   fetch C_SA_GL_REF_DATA BULK COLLECT into IO_sa_trandata_tbl;
   close C_SA_GL_REF_DATA;

EXCEPTION
   when OTHERS then
      IO_sa_trandata_tbl(1).return_code   := 'FALSE';
      IO_sa_trandata_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 'SA_TRANDATA_SQL.QUERY_PROCEDURE',
                                                                 to_char(SQLCODE));
END QUERY_PROCEDURE;

--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: GET_MIN_DATE
--- Purpose:        This procedure will query SA_GL_REF_DATA table to get the minimum possible date for 
---                 processed_date or acct_date
--------------------------------------------------------------------------------------------------------------------
FUNCTION GET_MIN_DATE (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_min_date              IN OUT   SA_GL_REF_DATA.PROCESSED_DATE%TYPE,
                       I_param                 IN       VARCHAR2)
RETURN BOOLEAN IS

   cursor C_MIN_PROCESSED_DATE is
      select min(processed_date)
        from sa_gl_ref_data;

   cursor C_MIN_ACCT_DATE is
      select min(acct_date)
        from sa_gl_ref_data;
BEGIN

   if I_param = 'PROCESSED_DATE' then
      -- Get the minimum processed_date from sa_gl_ref_data table.
      open C_MIN_PROCESSED_DATE;
      fetch C_MIN_PROCESSED_DATE into O_min_date;
      close C_MIN_PROCESSED_DATE;
   elsif I_param = 'ACCT_DATE' then
      -- Get the minimum acct_date from sa_gl_ref_data table.
      open C_MIN_ACCT_DATE;
      fetch C_MIN_ACCT_DATE into O_min_date;
      close C_MIN_ACCT_DATE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SA_TRANDATA_SQL.GET_MIN_DATE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_MIN_DATE;

--------------------------------------------------------------------------------------------------------------------
END SA_TRANDATA_SQL;
/