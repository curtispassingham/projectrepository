CREATE OR REPLACE PACKAGE BODY CORESVC_STKLDGRV AS
--------------------------------------------------------------------------------
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   
   LP_c_month_data_tbl      OBJ_COST_MONTH_DATA_TBL;
   LP_c_week_data_tbl       OBJ_COST_WEEK_DATA_TBL;
   LP_c_daily_data_tbl      OBJ_COST_DAILY_DATA_TBL;
   LP_r_month_data_tbl      OBJ_RETAIL_MONTH_DATA_TBL;
   LP_r_week_data_tbl       OBJ_RETAIL_WEEK_DATA_TBL;
   LP_r_daily_data_tbl      OBJ_RETAIL_DAILY_DATA_TBL;
   LP_cr_month_data_tbl     OBJ_CR_MONTH_DATA_TBL;
   LP_cr_week_data_tbl      OBJ_CR_WEEK_DATA_TBL;
   LP_cr_daily_data_tbl     OBJ_CR_DAILY_DATA_TBL;
   LP_loc_type              MONTH_DATA.LOC_TYPE%TYPE;
   LP_location              STORE.STORE%TYPE;
   LP_currency_ind          VARCHAR2(1);
   LP_set_of_books_id       NUMBER        := NULL;
   LP_system_options_row    SYSTEM_OPTIONS%ROWTYPE;
   LP_legal_entity          WH.STOCKHOLDING_IND%TYPE    := 'N';
   LP_loc_type_wh           WH.ORG_ENTITY_TYPE%TYPE;
   LP_valid                 BOOLEAN;
   LP_wh_row                V_WH%ROWTYPE;
   LP_location_name         V_EXTERNAL_FINISHER.FINISHER_DESC%TYPE;
   LP_cal_454               SYSTEM_OPTIONS.CALENDAR_454_IND%TYPE;
   
--------------------------------------------------------------------------------
FUNCTION BUILD_MAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id          OUT SVC_PROCESS_TRACKER.FILE_ID%TYPE,
                    I_sch_crit_rec  IN     STKLDGRV_SCH_CRIT_TYP)
RETURN BOOLEAN IS
   L_program VARCHAR2(75):= 'CORESVC_STKLDGRV.BUILD_MAIN';

BEGIN
   if I_sch_crit_rec.mwd_view = 'M' then
      if I_sch_crit_rec.template = 'CTMPL' then
         template_key := 'STKLDGR_C_MONTH_DATA';
      elsif I_sch_crit_rec.template = 'RTMPL' then
         template_key := 'STKLDGR_R_MONTH_DATA';
      else
         template_key := 'STKLDGR_CR_MONTH_DATA';
      end if;
   end if;
   if I_sch_crit_rec.mwd_view = 'W' then
      if I_sch_crit_rec.template = 'CTMPL' then
         template_key := 'STKLDGR_C_WEEK_DATA';
      elsif I_sch_crit_rec.template = 'RTMPL' then
         template_key := 'STKLDGR_R_WEEK_DATA';
      else
         template_key := 'STKLDGR_CR_WEEK_DATA';
      end if;
   end if;
   if I_sch_crit_rec.mwd_view = 'D' then
      if I_sch_crit_rec.template = 'CTMPL' then
         template_key := 'STKLDGR_C_DAILY_DATA';
      elsif I_sch_crit_rec.template = 'RTMPL' then
         template_key := 'STKLDGR_R_DAILY_DATA';
      else
         template_key := 'STKLDGR_CR_DAILY_DATA';
      end if;
   end if;
   
   LP_sch_crit_rec := NEW STKLDGRV_SCH_CRIT_TYP(I_sch_crit_rec.dept,
                                                I_sch_crit_rec.class,
                                                I_sch_crit_rec.subclass,
                                                I_sch_crit_rec.currency,
                                                I_sch_crit_rec.loc_type,
                                                I_sch_crit_rec.location,
                                                I_sch_crit_rec.mwd_view,
                                                I_sch_crit_rec.period_no,
                                                I_sch_crit_rec.period_date,
                                                I_sch_crit_rec.template);
   
   
   if CORESVC_STKLDGRV.CREATE_S9T(O_error_message,
                                      O_file_id,
                                      'N') = FALSE THEN
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END BUILD_MAIN;
--------------------------------------------------------------------------------
FUNCTION BUILD_MAIN_WRP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_file_id          OUT SVC_PROCESS_TRACKER.FILE_ID%TYPE,
                        I_sch_crit_rec  IN     STKLDGRV_SCH_CRIT_TYP)
RETURN NUMBER IS 
   L_program   VARCHAR2(75):= 'CORESVC_STKLDGRV.BUILD_MAIN_WRP';
BEGIN
    if CORESVC_STKLDGRV.BUILD_MAIN(O_error_message,
                                   O_file_id,
                                   I_sch_crit_rec) = FALSE then
      return 0;
   end if;
   
   return 1;   
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END BUILD_MAIN_WRP;
--------------------------------------------------------------------------------
FUNCTION GET_VARIABLES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_sch_crit_rec  IN     STKLDGRV_SCH_CRIT_TYP)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75):= 'CORESVC_STKLDGRV.GET_VARIABLES';
   L_dummy1    NUMBER;
   L_dummy2    NUMBER;
   
BEGIN
   
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;
   
   if SYSTEM_CALENDAR_SQL.GET_CAL_OPTS(O_error_message,
                                       LP_cal_454,
                                       L_dummy1,
                                       L_dummy2) = FALSE then
      return FALSE;
   end if;
   
   if I_sch_crit_rec.location is NOT NULL then
      if I_sch_crit_rec.loc_type = 'W' then
         if LOCATION_ATTRIB_SQL.GET_TYPE (O_error_message,
                                          LP_loc_type_wh,
                                          I_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;
         if LP_loc_type_wh = 'W' then
            if FILTER_LOV_VALIDATE_SQL.VALIDATE_WH(O_error_message,
                                                   LP_valid,
                                                   LP_wh_row,
                                                   I_sch_crit_rec.location) = FALSE
               or LP_valid = FALSE then
               return FALSE;
            end if;
         else
            if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                      I_sch_crit_rec.location,
                                      LP_location_name) = FALSE then
               return FALSE;
            end if;
            LP_legal_entity := 'Y';
         end if;
      end if;
   end if;
   
   if I_sch_crit_rec.loc_type = 'I' then
      LP_loc_type := 'W';
      LP_location := LP_sch_crit_rec.location;
   elsif I_sch_crit_rec.loc_type = 'M' then
      LP_loc_type := NULL;
      LP_location := NULL;
      LP_set_of_books_id := I_sch_crit_rec.location;
   elsif I_sch_crit_rec.loc_type = 'W' and LP_legal_entity = 'Y' then
      LP_loc_type := 'I';
      LP_location := I_sch_crit_rec.location;
   elsif I_sch_crit_rec.loc_type is NULL then
      LP_loc_type := NULL;
      LP_location := NULL;
   else
      LP_loc_type := I_sch_crit_rec.loc_type;
      LP_location := I_sch_crit_rec.location;
   end if;
   LP_currency_ind :=   I_sch_crit_rec.currency;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END GET_VARIABLES;
--------------------------------------------------------------------------------
FUNCTION FETCH_C_MONTH_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(75) :=   'CORESVC_STKLDGRV.FETCH_C_MONTH_DATA';
   L_tbl_c_month_data_ty    OBJ_MONTH_DATA_TBL;
   L_tbl_c_month_data_ly    OBJ_MONTH_DATA_TBL;
   L_add_cost_ly            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;   
   L_net_tsf_cost_ly        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   MONTH_DATA.TSF_IN_COST%TYPE;
   L_shrink_cost_pct_ly     NUMBER(12,4);
   L_shrink_cost_pct_ty     NUMBER(12,4);
   L_red_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_red_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ly    MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ty    MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty MONTH_DATA.TSF_IN_COST%TYPE;
   L_cum_markon_pct_ly      NUMBER(12,4);
   L_cum_markon_pct_ty      NUMBER(12,4);
   
   cursor C_COST_MONTH_DATA_INFO is
      select OBJ_COST_MONTH_DATA_REC(  outer_query.dept,
                                       outer_query.dept_name,
                                       outer_query.class,
                                       outer_query.class_name,
                                       outer_query.subclass,
                                       outer_query.subclass_name,
                                       outer_query.loc_type,
                                       outer_query.location,
                                       outer_query.loc_name,
                                       outer_query.currency_code,
                                       outer_query.half_no,
                                       outer_query.month_no,
                                       outer_query.opn_stk_cost_ly,
                                       outer_query.opn_stk_cost_ty,
                                       outer_query.purch_cost_ly,
                                       outer_query.purch_cost_ty,
                                       outer_query.tsf_in_cost_ly,
                                       outer_query.tsf_in_cost_ty,
                                       outer_query.tsf_in_retail_ly,
                                       outer_query.tsf_in_retail_ty,
                                       outer_query.tsf_in_book_cost_ly,
                                       outer_query.tsf_in_book_cost_ty,
                                       outer_query.intercompany_in_cost_ly,
                                       outer_query.intercompany_in_cost_ty,
                                       outer_query.reclass_in_cost_ly,
                                       outer_query.reclass_in_cost_ty,
                                       outer_query.franchise_returns_cost_ly,
                                       outer_query.franchise_returns_cost_ty,
                                       outer_query.up_chrg_amt_profit_ly,
                                       outer_query.up_chrg_amt_profit_ty,
                                       outer_query.up_chrg_amt_exp_ly,
                                       outer_query.up_chrg_amt_exp_ty,
                                       outer_query.wo_activity_upd_inv_ly,
                                       outer_query.wo_activity_upd_inv_ty,
                                       outer_query.recoverable_tax_ly,
                                       outer_query.recoverable_tax_ty,
                                       outer_query.rtv_cost_ly,
                                       outer_query.rtv_cost_ty,
                                       outer_query.tsf_out_cost_ly,
                                       outer_query.tsf_out_cost_ty,
                                       outer_query.tsf_out_retail_ly,
                                       outer_query.tsf_out_retail_ty,
                                       outer_query.tsf_out_book_cost_ly,
                                       outer_query.tsf_out_book_cost_ty,
                                       outer_query.intercompany_out_cost_ly,
                                       outer_query.intercompany_out_cost_ty,
                                       outer_query.reclass_out_cost_ly,
                                       outer_query.reclass_out_cost_ty,
                                       outer_query.net_sales_cost_ly,
                                       outer_query.net_sales_cost_ty,
                                       outer_query.returns_cost_ly,
                                       outer_query.returns_cost_ty,
                                       outer_query.franchise_sales_retail_ly,
                                       outer_query.franchise_sales_retail_ty,
                                       outer_query.franchise_sales_cost_ly,
                                       outer_query.franchise_sales_cost_ty,
                                       outer_query.franchise_returns_retail_ly,
                                       outer_query.franchise_returns_retail_ty,
                                       outer_query.htd_gafs_retail_ly,
                                       outer_query.htd_gafs_retail_ty,
                                       outer_query.freight_claim_cost_ly,
                                       outer_query.freight_claim_cost_ty,
                                       outer_query.stock_adj_cogs_cost_ly,
                                       outer_query.stock_adj_cogs_cost_ty,
                                       outer_query.stock_adj_cost_ly,
                                       outer_query.stock_adj_cost_ty,
                                       outer_query.shrinkage_cost_ly,
                                       outer_query.shrinkage_cost_ty,
                                       outer_query.shrinkage_retail_ly,
                                       outer_query.shrinkage_retail_ty,
                                       outer_query.cost_variance_amt_ly,
                                       outer_query.cost_variance_amt_ty,
                                       outer_query.htd_gafs_cost_ly,
                                       outer_query.htd_gafs_cost_ty,
                                       outer_query.margin_cost_variance_ly,
                                       outer_query.margin_cost_variance_ty,
                                       outer_query.cls_stk_cost_ly,
                                       outer_query.cls_stk_cost_ty,
                                       outer_query.purch_retail_ly,
                                       outer_query.purch_retail_ty,
                                       outer_query.rtv_retail_ly,
                                       outer_query.rtv_retail_ty,
                                       outer_query.add_cost_ly,
                                       outer_query.add_cost_ty,
                                       outer_query.add_retail_ly,
                                       outer_query.add_retail_ty,
                                       outer_query.shrink_cost_pct_ly,
                                       outer_query.shrink_cost_pct_ty,
                                       outer_query.red_retail_ly,
                                       outer_query.red_retail_ty,
                                       outer_query.deal_income_sales_ly,
                                       outer_query.deal_income_sales_ty,
                                       outer_query.deal_income_purch_ly,
                                       outer_query.deal_income_purch_ty,
                                       outer_query.vat_in_ly,
                                       outer_query.vat_in_ty,
                                       outer_query.vat_out_ly,
                                       outer_query.vat_out_ty,
                                       outer_query.wo_activity_post_fin_ly,
                                       outer_query.wo_activity_post_fin_ty,
                                       outer_query.restocking_fee_ly,
                                       outer_query.restocking_fee_ty,
                                       outer_query.freight_cost_ly,
                                       outer_query.freight_cost_ty,
                                       outer_query.rec_cost_adj_variance_ly,
                                       outer_query.rec_cost_adj_variance_ty,
                                       outer_query.workroom_amt_ly,
                                       outer_query.workroom_amt_ty,
                                       outer_query.cash_disc_amt_ly,
                                       outer_query.cash_disc_amt_ty,
                                       outer_query.clear_markdown_retail_ly,
                                       outer_query.clear_markdown_retail_ty,
                                       outer_query.empl_disc_retail_ly,
                                       outer_query.empl_disc_retail_ty,
                                       outer_query.markup_can_retail_ly,
                                       outer_query.markup_can_retail_ty,
                                       outer_query.markup_retail_ly,
                                       outer_query.markup_retail_ty,
                                       outer_query.perm_markdown_retail_ly,
                                       outer_query.perm_markdown_retail_ty,
                                       outer_query.prom_markdown_retail_ly,
                                       outer_query.prom_markdown_retail_ty,
                                       outer_query.weight_variance_retail_ly,
                                       outer_query.weight_variance_retail_ty,
                                       outer_query.franchise_markdown_retail_ly,
                                       outer_query.franchise_markdown_retail_ty,
                                       outer_query.franchise_markup_retail_ly,
                                       outer_query.franchise_markup_retail_ty,
                                       outer_query.franchise_restocking_fee_ly,
                                       outer_query.franchise_restocking_fee_ty,
                                       outer_query.intercompany_markdown_ly,
                                       outer_query.intercompany_markdown_ty,
                                       outer_query.intercompany_markup_ly,
                                       outer_query.intercompany_markup_ty,
                                       outer_query.intercompany_margin_ly,
                                       outer_query.intercompany_margin_ty,
                                       outer_query.markdown_can_retail_ly,
                                       outer_query.markdown_can_retail_ty,
                                       outer_query.net_sales_retail_ly,
                                       outer_query.net_sales_retail_ty,
                                       outer_query.net_markup_ly,
                                       outer_query.net_markup_ty,
                                       outer_query.net_reclass_cost_ly,
                                       outer_query.net_reclass_cost_ty,
                                       outer_query.net_tsf_cost_ly,
                                       outer_query.net_tsf_cost_ty,
                                       outer_query.sales_units_ly,
                                       outer_query.sales_units_ty,
                                       outer_query.cum_markon_pct_ly,
                                       outer_query.cum_markon_pct_ty,
                                       outer_query.gross_margin_amt_ly,
                                       outer_query.gross_margin_amt_ty,
                                       outer_query.net_sales_non_inv_cost_ly,
                                       outer_query.net_sales_non_inv_cost_ty)
        from (select NVL(tbl_cmd_ly.dept,tbl_cmd_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_cmd_ly.class,tbl_cmd_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_cmd_ly.subclass,tbl_cmd_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_cmd_ly.loc_type,tbl_cmd_ty.loc_type) as loc_type,
                     NVL(tbl_cmd_ly.location,tbl_cmd_ty.location) as location,
                     NULL                                         as loc_name, --loc_name
                     NULL                                         as currency_code,
                     NVL(tbl_cmd_ly.half_no,tbl_cmd_ty.half_no)   as half_no,
                     NVL(tbl_cmd_ly.month_no,tbl_cmd_ty.month_no) as month_no,
                     tbl_cmd_ly.opn_stk_cost                      as opn_stk_cost_ly,    
                     tbl_cmd_ty.opn_stk_cost                      as opn_stk_cost_ty,
                     tbl_cmd_ly.purch_cost                        as purch_cost_ly,
                     tbl_cmd_ty.purch_cost                        as purch_cost_ty,
                     tbl_cmd_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_cmd_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_cmd_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_cmd_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_cmd_ly.tsf_in_book_cost                  as tsf_in_book_cost_ly,
                     tbl_cmd_ty.tsf_in_book_cost                  as tsf_in_book_cost_ty,
                     tbl_cmd_ly.intercompany_in_cost              as intercompany_in_cost_ly,
                     tbl_cmd_ty.intercompany_in_cost              as intercompany_in_cost_ty,
                     tbl_cmd_ly.reclass_in_cost                   as reclass_in_cost_ly,
                     tbl_cmd_ty.reclass_in_cost                   as reclass_in_cost_ty,
                     tbl_cmd_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_cmd_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_cmd_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_cmd_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_cmd_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_cmd_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_cmd_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_cmd_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_cmd_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_cmd_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_cmd_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_cmd_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_cmd_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_cmd_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_cmd_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_cmd_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_cmd_ly.tsf_out_book_cost                 as tsf_out_book_cost_ly,
                     tbl_cmd_ty.tsf_out_book_cost                 as tsf_out_book_cost_ty,
                     tbl_cmd_ly.intercompany_out_cost             as intercompany_out_cost_ly,
                     tbl_cmd_ty.intercompany_out_cost             as intercompany_out_cost_ty,
                     tbl_cmd_ly.reclass_out_cost                  as reclass_out_cost_ly,
                     tbl_cmd_ty.reclass_out_cost                  as reclass_out_cost_ty,
                     tbl_cmd_ly.net_sales_cost                    as net_sales_cost_ly,
                     tbl_cmd_ty.net_sales_cost                    as net_sales_cost_ty,
                     tbl_cmd_ly.returns_cost                      as returns_cost_ly,
                     tbl_cmd_ty.returns_cost                      as returns_cost_ty,
                     tbl_cmd_ly.franchise_sales_retail            as franchise_sales_retail_ly,
                     tbl_cmd_ty.franchise_sales_retail            as franchise_sales_retail_ty,
                     tbl_cmd_ly.franchise_sales_cost              as franchise_sales_cost_ly,
                     tbl_cmd_ty.franchise_sales_cost              as franchise_sales_cost_ty,
                     tbl_cmd_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_cmd_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_cmd_ly.htd_gafs_retail                   as htd_gafs_retail_ly,
                     tbl_cmd_ty.htd_gafs_retail                   as htd_gafs_retail_ty,
                     tbl_cmd_ly.freight_claim_cost                as freight_claim_cost_ly,
                     tbl_cmd_ty.freight_claim_cost                as freight_claim_cost_ty,
                     tbl_cmd_ly.stock_adj_cogs_cost               as stock_adj_cogs_cost_ly,
                     tbl_cmd_ty.stock_adj_cogs_cost               as stock_adj_cogs_cost_ty,
                     tbl_cmd_ly.stock_adj_cost                    as stock_adj_cost_ly,
                     tbl_cmd_ty.stock_adj_cost                    as stock_adj_cost_ty,
                     tbl_cmd_ly.shrinkage_cost                    as shrinkage_cost_ly,
                     tbl_cmd_ty.shrinkage_cost                    as shrinkage_cost_ty,
                     tbl_cmd_ly.shrinkage_retail                  as shrinkage_retail_ly,
                     tbl_cmd_ty.shrinkage_retail                  as shrinkage_retail_ty,
                     tbl_cmd_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_cmd_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_cmd_ly.htd_gafs_cost                     as htd_gafs_cost_ly,
                     tbl_cmd_ty.htd_gafs_cost                     as htd_gafs_cost_ty,
                     tbl_cmd_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_cmd_ty.margin_cost_variance              as margin_cost_variance_ty,
                     tbl_cmd_ly.cls_stk_cost                      as cls_stk_cost_ly,
                     tbl_cmd_ty.cls_stk_cost                      as cls_stk_cost_ty,
                     tbl_cmd_ly.purch_retail                      as purch_retail_ly,
                     tbl_cmd_ty.purch_retail                      as purch_retail_ty,
                     tbl_cmd_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_cmd_ty.rtv_retail                        as rtv_retail_ty,
                     NULL                                         as add_cost_ly, --tbl_cmd_ly.add_cost                    as add_cost_ly,
                     NULL                                         as add_cost_ty, --tbl_cmd_ty.add_cost                    as add_cost_ty,
                     NULL                                         as add_retail_ly, --tbl_cmd_ly.add_retail                  as add_retail_ly,
                     NULL                                         as add_retail_ty, --tbl_cmd_ty.add_retail                  as add_retail_ty,
                     NULL                                         as shrink_cost_pct_ly, --tbl_cmd_ly.shrink_cost_pct             as shrink_cost_pct_ly,
                     NULL                                         as shrink_cost_pct_ty, --tbl_cmd_ty.shrink_cost_pct             as shrink_cost_pct_ty,
                     NULL                                         as red_retail_ly, --tbl_cmd_ly.red_retail                  as red_retail_ly,
                     NULL                                         as red_retail_ty, --tbl_cmd_ty.red_retail                  as red_retail_ty,
                     tbl_cmd_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_cmd_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_cmd_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_cmd_ty.deal_income_purch                 as deal_income_purch_ty,
                     tbl_cmd_ly.vat_in                            as vat_in_ly,
                     tbl_cmd_ty.vat_in                            as vat_in_ty,
                     tbl_cmd_ly.vat_out                           as vat_out_ly,
                     tbl_cmd_ty.vat_out                           as vat_out_ty,
                     tbl_cmd_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_cmd_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_cmd_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_cmd_ty.restocking_fee                    as restocking_fee_ty,
                     tbl_cmd_ly.freight_cost                      as freight_cost_ly,
                     tbl_cmd_ty.freight_cost                      as freight_cost_ty,
                     tbl_cmd_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_cmd_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     tbl_cmd_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_cmd_ty.workroom_amt                      as workroom_amt_ty,
                     tbl_cmd_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_cmd_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_cmd_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_cmd_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_cmd_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_cmd_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_cmd_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_cmd_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_cmd_ly.markup_retail                     as markup_retail_ly,
                     tbl_cmd_ty.markup_retail                     as markup_retail_ty,
                     tbl_cmd_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_cmd_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_cmd_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_cmd_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_cmd_ly.weight_variance_retail            as weight_variance_retail_ly,
                     tbl_cmd_ty.weight_variance_retail            as weight_variance_retail_ty,
                     tbl_cmd_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_cmd_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_cmd_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_cmd_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_cmd_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_cmd_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_cmd_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_cmd_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_cmd_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_cmd_ty.intercompany_markup               as intercompany_markup_ty,
                     tbl_cmd_ly.intercompany_margin               as intercompany_margin_ly,
                     tbl_cmd_ty.intercompany_margin               as intercompany_margin_ty,
                     tbl_cmd_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_cmd_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_cmd_ly.net_sales_retail                  as net_sales_retail_ly,
                     tbl_cmd_ty.net_sales_retail                  as net_sales_retail_ty,
                     NULL                                         as net_markup_ly, --tbl_cmd_ly.net_markup                  as net_markup_ly,
                     NULL                                         as net_markup_ty, --tbl_cmd_ty.net_markup                  as net_markup_ty,
                     NULL                                         as net_reclass_cost_ly, --tbl_cmd_ly.net_reclass_cost            as net_reclass_cost_ly,
                     NULL                                         as net_reclass_cost_ty, --tbl_cmd_ty.net_reclass_cost            as net_reclass_cost_ty,
                     NULL                                         as net_tsf_cost_ly, --tbl_cmd_ly.net_tsf_cost                as net_tsf_cost_ly,
                     NULL                                         as net_tsf_cost_ty, --tbl_cmd_ty.net_tsf_cost                as net_tsf_cost_ty,
                     tbl_cmd_ly.sales_units                       as sales_units_ly,
                     tbl_cmd_ty.sales_units                       as sales_units_ty,
                     NULL                                         as cum_markon_pct_ly, --tbl_cmd_ly.cum_markon_pct              as cum_markon_pct_ly,
                     NULL                                         as cum_markon_pct_ty, --tbl_cmd_ty.cum_markon_pct              as cum_markon_pct_ty,
                     tbl_cmd_ly.gross_margin_amt                  as gross_margin_amt_ly,
                     tbl_cmd_ty.gross_margin_amt                  as gross_margin_amt_ty,
                     tbl_cmd_ly.net_sales_non_inv_cost            as net_sales_non_inv_cost_ly,
                     tbl_cmd_ty.net_sales_non_inv_cost            as net_sales_non_inv_cost_ty
                from table(cast(L_tbl_c_month_data_ly as OBJ_MONTH_DATA_TBL)) tbl_cmd_ly 
                full outer join table(cast(L_tbl_c_month_data_ty as OBJ_MONTH_DATA_TBL)) tbl_cmd_ty 
                  ON tbl_cmd_ly.dept         =  tbl_cmd_ty.dept
                     and tbl_cmd_ly.class    =  tbl_cmd_ty.class 
                     and tbl_cmd_ly.subclass =  tbl_cmd_ty.subclass
                     and tbl_cmd_ly.loc_type =  tbl_cmd_ty.loc_type
                     and tbl_cmd_ly.location =  tbl_cmd_ty.location
                     and tbl_cmd_ly.half_no  =  tbl_cmd_ty.half_no
                     and tbl_cmd_ly.month_no =  tbl_cmd_ty.month_no
                     and tbl_cmd_ly.eom_date =  tbl_cmd_ty.eom_date) outer_query;
BEGIN
   LP_c_month_data_tbl   :=  new OBJ_COST_MONTH_DATA_TBL();
   L_tbl_c_month_data_ty :=  new OBJ_MONTH_DATA_TBL();
   L_tbl_c_month_data_ly :=  new OBJ_MONTH_DATA_TBL();

   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;                       
   if STKLEDGR_QUERY_SQL.FETCH_MONTH_DATA_BULK(LP_sch_crit_rec.dept,
                                               LP_sch_crit_rec.class,
                                               LP_sch_crit_rec.subclass,
                                               'T',
                                               LP_sch_crit_rec.period_no,
                                               LP_loc_type,
                                               LP_location,
                                               'Y',      --:B_search.CB_thousands
                                               LP_currency_ind,
                                               LP_set_of_books_id,
                                               L_tbl_c_month_data_ty,
                                               O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if STKLEDGR_QUERY_SQL.FETCH_MONTH_DATA_BULK(LP_sch_crit_rec.dept,
                                               LP_sch_crit_rec.class,
                                               LP_sch_crit_rec.subclass,
                                               'L',
                                               LP_sch_crit_rec.period_no,
                                               LP_loc_type,
                                               LP_location,
                                               'Y',      --:B_search.CB_thousands
                                               LP_currency_ind,
                                               LP_set_of_books_id,
                                               L_tbl_c_month_data_ly,
                                               O_error_message) = FALSE then
      return FALSE;
   end if;
   
   open C_COST_MONTH_DATA_INFO;
   fetch C_COST_MONTH_DATA_INFO BULK COLLECT into LP_c_month_data_tbl;
   close C_COST_MONTH_DATA_INFO;
  
   FOR i in 1..LP_c_month_data_tbl.count
   LOOP
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_c_month_data_tbl(i).dept,
                                  LP_c_month_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_c_month_data_tbl(i).dept,
                                   LP_c_month_data_tbl(i).class,
                                   LP_c_month_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_c_month_data_tbl(i).dept,
                                       LP_c_month_data_tbl(i).class,
                                       LP_c_month_data_tbl(i).subclass,
                                       LP_c_month_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;     
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_c_month_data_tbl(i).loc_name,
                                       LP_c_month_data_tbl(i).location,
                                       LP_c_month_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_c_month_data_tbl(i).loc_type,
                                    LP_c_month_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_c_month_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_c_month_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_c_month_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_c_month_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;      
      -- CALCULATE_FIELDS
      L_net_tsf_cost_ly := LP_c_month_data_tbl(i).tsf_in_cost_ly - LP_c_month_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_c_month_data_tbl(i).tsf_in_cost_ty - LP_c_month_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_c_month_data_tbl(i).tsf_in_retail_ly - LP_c_month_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_c_month_data_tbl(i).tsf_in_retail_ty - LP_c_month_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_c_month_data_tbl(i).markup_retail_ly - LP_c_month_data_tbl(i).markup_can_retail_ly + LP_c_month_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_c_month_data_tbl(i).markup_retail_ty - LP_c_month_data_tbl(i).markup_can_retail_ty + LP_c_month_data_tbl(i).franchise_markup_retail_ty;
                   
      L_add_cost_ly := LP_c_month_data_tbl(i).purch_cost_ly - LP_c_month_data_tbl(i).rtv_cost_ly + LP_c_month_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_c_month_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_c_month_data_tbl(i).purch_cost_ty - LP_c_month_data_tbl(i).rtv_cost_ty + LP_c_month_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_c_month_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_c_month_data_tbl(i).purch_retail_ly - LP_c_month_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_c_month_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_c_month_data_tbl(i).purch_retail_ty - LP_c_month_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_c_month_data_tbl(i).franchise_returns_retail_ty;
      
      
      if (LP_c_month_data_tbl(i).net_sales_cost_ly is NULL and LP_c_month_data_tbl(i).franchise_sales_cost_ly is NULL and LP_c_month_data_tbl(i).franchise_returns_cost_ly is NULL) 
          or (LP_c_month_data_tbl(i).net_sales_cost_ly = 0 and (LP_c_month_data_tbl(i).franchise_sales_cost_ly - LP_c_month_data_tbl(i).franchise_returns_cost_ly) = 0) then
         L_shrink_cost_pct_ly := 0;
      else
         L_shrink_cost_pct_ly := ((LP_c_month_data_tbl(i).shrinkage_cost_ly / (LP_c_month_data_tbl(i).net_sales_cost_ly + LP_c_month_data_tbl(i).franchise_sales_cost_ly - LP_c_month_data_tbl(i).franchise_returns_cost_ly))*100);
      end if;
      
      if (LP_c_month_data_tbl(i).net_sales_cost_ty is NULL and LP_c_month_data_tbl(i).franchise_sales_cost_ty is NULL and LP_c_month_data_tbl(i).franchise_returns_cost_ty is NULL) 
            or (LP_c_month_data_tbl(i).net_sales_cost_ty = 0 and (LP_c_month_data_tbl(i).franchise_sales_cost_ty - LP_c_month_data_tbl(i).franchise_returns_cost_ty) = 0) then
         L_shrink_cost_pct_ty := 0;
      else
         L_shrink_cost_pct_ty := ((LP_c_month_data_tbl(i).shrinkage_cost_ty / (LP_c_month_data_tbl(i).net_sales_cost_ty + LP_c_month_data_tbl(i).franchise_sales_cost_ty - LP_c_month_data_tbl(i).franchise_returns_cost_ty))*100);
      end if;
      
      L_net_markdown_retail_ly := LP_c_month_data_tbl(i).clear_markdown_retail_ly + LP_c_month_data_tbl(i).perm_markdown_retail_ly + LP_c_month_data_tbl(i).prom_markdown_retail_ly - LP_c_month_data_tbl(i).markdown_can_retail_ly + LP_c_month_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_c_month_data_tbl(i).clear_markdown_retail_ty + LP_c_month_data_tbl(i).perm_markdown_retail_ty + LP_c_month_data_tbl(i).prom_markdown_retail_ty - LP_c_month_data_tbl(i).markdown_can_retail_ty + LP_c_month_data_tbl(i).franchise_markdown_retail_ty;
      
      L_red_retail_ly   := LP_c_month_data_tbl(i).net_sales_retail_ly + LP_c_month_data_tbl(i).shrinkage_retail_ly + L_net_markdown_retail_ly + LP_c_month_data_tbl(i).empl_disc_retail_ly + LP_c_month_data_tbl(i).weight_variance_retail_ly + LP_c_month_data_tbl(i).franchise_markdown_retail_ly + LP_c_month_data_tbl(i).franchise_sales_retail_ly - LP_c_month_data_tbl(i).franchise_returns_retail_ly;
      L_red_retail_ty   := LP_c_month_data_tbl(i).net_sales_retail_ty + LP_c_month_data_tbl(i).shrinkage_retail_ty + L_net_markdown_retail_ty + LP_c_month_data_tbl(i).empl_disc_retail_ty + LP_c_month_data_tbl(i).weight_variance_retail_ty + LP_c_month_data_tbl(i).franchise_markdown_retail_ty + LP_c_month_data_tbl(i).franchise_sales_retail_ty - LP_c_month_data_tbl(i).franchise_returns_retail_ty;

      L_net_reclass_cost_ly   := LP_c_month_data_tbl(i).reclass_in_cost_ly - LP_c_month_data_tbl(i).reclass_out_cost_ly;          
      L_net_reclass_cost_ty   := LP_c_month_data_tbl(i).reclass_in_cost_ty - LP_c_month_data_tbl(i).reclass_out_cost_ty;         
      
      if LP_c_month_data_tbl(i).htd_gafs_retail_ly is NULL or LP_c_month_data_tbl(i).htd_gafs_retail_ly = 0 then
         L_cum_markon_pct_ly := 0;
      else
         L_cum_markon_pct_ly := ((1 - (LP_c_month_data_tbl(i).htd_gafs_cost_ly / LP_c_month_data_tbl(i).htd_gafs_retail_ly)) * 100);
      end if;

      if LP_c_month_data_tbl(i).htd_gafs_retail_ty is NULL or LP_c_month_data_tbl(i).htd_gafs_retail_ty = 0 then
         L_cum_markon_pct_ty := 0;
      else
         L_cum_markon_pct_ty := ((1 - (LP_c_month_data_tbl(i).htd_gafs_cost_ty / LP_c_month_data_tbl(i).htd_gafs_retail_ty)) * 100);
      end if; 
      
      LP_c_month_data_tbl(i).add_cost_ly             := L_add_cost_ly; 
      LP_c_month_data_tbl(i).add_cost_ty             := L_add_cost_ty;
      LP_c_month_data_tbl(i).add_retail_ly           := L_add_retail_ly;
      LP_c_month_data_tbl(i).add_retail_ty           := L_add_retail_ty;
      LP_c_month_data_tbl(i).shrink_cost_pct_ly      := L_shrink_cost_pct_ly;
      LP_c_month_data_tbl(i).shrink_cost_pct_ty      := L_shrink_cost_pct_ty;
      LP_c_month_data_tbl(i).red_retail_ly           := L_red_retail_ly;
      LP_c_month_data_tbl(i).red_retail_ty           := L_red_retail_ty;
      LP_c_month_data_tbl(i).net_markup_ly           := L_net_markup_retail_ly;
      LP_c_month_data_tbl(i).net_markup_ty           := L_net_markup_retail_ty;
      LP_c_month_data_tbl(i).net_reclass_cost_ly     := L_net_reclass_cost_ly;
      LP_c_month_data_tbl(i).net_reclass_cost_ty     := L_net_reclass_cost_ty;
      LP_c_month_data_tbl(i).net_tsf_cost_ly         := L_net_tsf_cost_ly;
      LP_c_month_data_tbl(i).net_tsf_cost_ty         := L_net_tsf_cost_ty; 
      LP_c_month_data_tbl(i).cum_markon_pct_ly       := L_cum_markon_pct_ly;
      LP_c_month_data_tbl(i).cum_markon_pct_ty       := L_cum_markon_pct_ty;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_COST_MONTH_DATA_INFO%ISOPEN then
         close C_COST_MONTH_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_C_MONTH_DATA;
--------------------------------------------------------------------------------
FUNCTION FETCH_C_WEEK_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   
   L_program                VARCHAR2(75) :=   'CORESVC_STKLDGRV.FETCH_C_WEEK_DATA';
   L_tbl_c_week_data_ty     OBJ_WEEK_DATA_TBL;
   L_tbl_c_week_data_ly     OBJ_WEEK_DATA_TBL;
   L_add_cost_ly            WEEK_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            WEEK_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          WEEK_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          WEEK_DATA.TSF_IN_COST%TYPE;   
   L_net_tsf_cost_ly        WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   WEEK_DATA.TSF_IN_COST%TYPE;
   L_shrink_cost_pct_ly     NUMBER(12,4);
   L_shrink_cost_pct_ty     NUMBER(12,4);
   L_red_retail_ly          WEEK_DATA.TSF_IN_COST%TYPE;
   L_red_retail_ty          WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ly    WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ty    WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly WEEK_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty WEEK_DATA.TSF_IN_COST%TYPE;
   L_cum_markon_pct_ly      NUMBER(12,4);
   L_cum_markon_pct_ty      NUMBER(12,4);

   cursor C_COST_WEEK_DATA_INFO is
      select OBJ_COST_WEEK_DATA_REC(  outer_query.dept,
                                       outer_query.dept_name,
                                       outer_query.class,
                                       outer_query.class_name,
                                       outer_query.subclass,
                                       outer_query.subclass_name,
                                       outer_query.loc_type,
                                       outer_query.location,
                                       outer_query.loc_name,
                                       outer_query.currency_code,
                                       outer_query.eom_date,
                                       outer_query.week_no,
                                       outer_query.opn_stk_cost_ly,
                                       outer_query.opn_stk_cost_ty,
                                       outer_query.purch_cost_ly,
                                       outer_query.purch_cost_ty,
                                       outer_query.tsf_in_cost_ly,
                                       outer_query.tsf_in_cost_ty,
                                       outer_query.tsf_in_retail_ly,
                                       outer_query.tsf_in_retail_ty,
                                       outer_query.tsf_in_book_cost_ly,
                                       outer_query.tsf_in_book_cost_ty,
                                       outer_query.intercompany_in_cost_ly,
                                       outer_query.intercompany_in_cost_ty,
                                       outer_query.reclass_in_cost_ly,
                                       outer_query.reclass_in_cost_ty,
                                       outer_query.franchise_returns_cost_ly,
                                       outer_query.franchise_returns_cost_ty,
                                       outer_query.up_chrg_amt_profit_ly,
                                       outer_query.up_chrg_amt_profit_ty,
                                       outer_query.up_chrg_amt_exp_ly,
                                       outer_query.up_chrg_amt_exp_ty,
                                       outer_query.wo_activity_upd_inv_ly,
                                       outer_query.wo_activity_upd_inv_ty,
                                       outer_query.recoverable_tax_ly,
                                       outer_query.recoverable_tax_ty,
                                       outer_query.rtv_cost_ly,
                                       outer_query.rtv_cost_ty,
                                       outer_query.tsf_out_cost_ly,
                                       outer_query.tsf_out_cost_ty,
                                       outer_query.tsf_out_retail_ly,
                                       outer_query.tsf_out_retail_ty,
                                       outer_query.tsf_out_book_cost_ly,
                                       outer_query.tsf_out_book_cost_ty,
                                       outer_query.intercompany_out_cost_ly,
                                       outer_query.intercompany_out_cost_ty,
                                       outer_query.reclass_out_cost_ly,
                                       outer_query.reclass_out_cost_ty,
                                       outer_query.net_sales_cost_ly,
                                       outer_query.net_sales_cost_ty,
                                       outer_query.returns_cost_ly,
                                       outer_query.returns_cost_ty,
                                       outer_query.franchise_sales_retail_ly,
                                       outer_query.franchise_sales_retail_ty,
                                       outer_query.franchise_sales_cost_ly,
                                       outer_query.franchise_sales_cost_ty,
                                       outer_query.franchise_returns_retail_ly,
                                       outer_query.franchise_returns_retail_ty,
                                       outer_query.htd_gafs_retail_ly,
                                       outer_query.htd_gafs_retail_ty,
                                       outer_query.freight_claim_cost_ly,
                                       outer_query.freight_claim_cost_ty,
                                       outer_query.stock_adj_cogs_cost_ly,
                                       outer_query.stock_adj_cogs_cost_ty,
                                       outer_query.stock_adj_cost_ly,
                                       outer_query.stock_adj_cost_ty,
                                       outer_query.shrinkage_cost_ly,
                                       outer_query.shrinkage_cost_ty,
                                       outer_query.shrinkage_retail_ly,
                                       outer_query.shrinkage_retail_ty,
                                       outer_query.cost_variance_amt_ly,
                                       outer_query.cost_variance_amt_ty,
                                       outer_query.htd_gafs_cost_ly,
                                       outer_query.htd_gafs_cost_ty,
                                       outer_query.margin_cost_variance_ly,
                                       outer_query.margin_cost_variance_ty,
                                       outer_query.cls_stk_cost_ly,
                                       outer_query.cls_stk_cost_ty,
                                       outer_query.purch_retail_ly,
                                       outer_query.purch_retail_ty,
                                       outer_query.rtv_retail_ly,
                                       outer_query.rtv_retail_ty,
                                       outer_query.add_cost_ly,
                                       outer_query.add_cost_ty,
                                       outer_query.add_retail_ly,
                                       outer_query.add_retail_ty,
                                       outer_query.shrink_cost_pct_ly,
                                       outer_query.shrink_cost_pct_ty,
                                       outer_query.red_retail_ly,
                                       outer_query.red_retail_ty,
                                       outer_query.deal_income_sales_ly,
                                       outer_query.deal_income_sales_ty,
                                       outer_query.deal_income_purch_ly,
                                       outer_query.deal_income_purch_ty,
                                       outer_query.vat_in_ly,
                                       outer_query.vat_in_ty,
                                       outer_query.vat_out_ly,
                                       outer_query.vat_out_ty,
                                       outer_query.wo_activity_post_fin_ly,
                                       outer_query.wo_activity_post_fin_ty,
                                       outer_query.restocking_fee_ly,
                                       outer_query.restocking_fee_ty,
                                       outer_query.freight_cost_ly,
                                       outer_query.freight_cost_ty,
                                       outer_query.rec_cost_adj_variance_ly,
                                       outer_query.rec_cost_adj_variance_ty,
                                       outer_query.workroom_amt_ly,
                                       outer_query.workroom_amt_ty,
                                       outer_query.cash_disc_amt_ly,
                                       outer_query.cash_disc_amt_ty,
                                       outer_query.clear_markdown_retail_ly,
                                       outer_query.clear_markdown_retail_ty,
                                       outer_query.empl_disc_retail_ly,
                                       outer_query.empl_disc_retail_ty,
                                       outer_query.markup_can_retail_ly,
                                       outer_query.markup_can_retail_ty,
                                       outer_query.markup_retail_ly,
                                       outer_query.markup_retail_ty,
                                       outer_query.perm_markdown_retail_ly,
                                       outer_query.perm_markdown_retail_ty,
                                       outer_query.prom_markdown_retail_ly,
                                       outer_query.prom_markdown_retail_ty,
                                       outer_query.weight_variance_retail_ly,
                                       outer_query.weight_variance_retail_ty,
                                       outer_query.franchise_markdown_retail_ly,
                                       outer_query.franchise_markdown_retail_ty,
                                       outer_query.franchise_markup_retail_ly,
                                       outer_query.franchise_markup_retail_ty,
                                       outer_query.franchise_restocking_fee_ly,
                                       outer_query.franchise_restocking_fee_ty,
                                       outer_query.intercompany_markdown_ly,
                                       outer_query.intercompany_markdown_ty,
                                       outer_query.intercompany_markup_ly,
                                       outer_query.intercompany_markup_ty,
                                       outer_query.intercompany_margin_ly,
                                       outer_query.intercompany_margin_ty,
                                       outer_query.markdown_can_retail_ly,
                                       outer_query.markdown_can_retail_ty,
                                       outer_query.net_sales_retail_ly,
                                       outer_query.net_sales_retail_ty,
                                       outer_query.net_markup_ly,
                                       outer_query.net_markup_ty,
                                       outer_query.net_reclass_cost_ly,
                                       outer_query.net_reclass_cost_ty,
                                       outer_query.net_tsf_cost_ly,
                                       outer_query.net_tsf_cost_ty,
                                       outer_query.sales_units_ly,
                                       outer_query.sales_units_ty,
                                       outer_query.cum_markon_pct_ly,
                                       outer_query.cum_markon_pct_ty,
                                       outer_query.gross_margin_amt_ly,
                                       outer_query.gross_margin_amt_ty,
                                       outer_query.net_sales_non_inv_cost_ly,
                                       outer_query.net_sales_non_inv_cost_ty)
        from (select NVL(tbl_cwd_ly.dept,tbl_cwd_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_cwd_ly.class,tbl_cwd_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_cwd_ly.subclass,tbl_cwd_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_cwd_ly.loc_type,tbl_cwd_ty.loc_type) as loc_type,
                     NVL(tbl_cwd_ly.location,tbl_cwd_ty.location) as location,
                     NULL                                         as loc_name, --loc_name
                     NULL                                         as currency_code,
                     NULL                                         as eom_date,
                     NVL(tbl_cwd_ly.week_no,tbl_cwd_ty.week_no)   as week_no,
                     tbl_cwd_ly.opn_stk_cost                      as opn_stk_cost_ly,    
                     tbl_cwd_ty.opn_stk_cost                      as opn_stk_cost_ty,
                     tbl_cwd_ly.purch_cost                        as purch_cost_ly,
                     tbl_cwd_ty.purch_cost                        as purch_cost_ty,
                     tbl_cwd_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_cwd_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_cwd_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_cwd_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_cwd_ly.tsf_in_book_cost                  as tsf_in_book_cost_ly,
                     tbl_cwd_ty.tsf_in_book_cost                  as tsf_in_book_cost_ty,
                     tbl_cwd_ly.intercompany_in_cost              as intercompany_in_cost_ly,
                     tbl_cwd_ty.intercompany_in_cost              as intercompany_in_cost_ty,
                     tbl_cwd_ly.reclass_in_cost                   as reclass_in_cost_ly,
                     tbl_cwd_ty.reclass_in_cost                   as reclass_in_cost_ty,
                     tbl_cwd_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_cwd_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_cwd_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_cwd_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_cwd_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_cwd_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_cwd_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_cwd_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_cwd_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_cwd_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_cwd_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_cwd_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_cwd_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_cwd_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_cwd_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_cwd_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_cwd_ly.tsf_out_book_cost                 as tsf_out_book_cost_ly,
                     tbl_cwd_ty.tsf_out_book_cost                 as tsf_out_book_cost_ty,
                     tbl_cwd_ly.intercompany_out_cost             as intercompany_out_cost_ly,
                     tbl_cwd_ty.intercompany_out_cost             as intercompany_out_cost_ty,
                     tbl_cwd_ly.reclass_out_cost                  as reclass_out_cost_ly,
                     tbl_cwd_ty.reclass_out_cost                  as reclass_out_cost_ty,
                     tbl_cwd_ly.net_sales_cost                    as net_sales_cost_ly,
                     tbl_cwd_ty.net_sales_cost                    as net_sales_cost_ty,
                     tbl_cwd_ly.returns_cost                      as returns_cost_ly,
                     tbl_cwd_ty.returns_cost                      as returns_cost_ty,
                     tbl_cwd_ly.franchise_sales_retail            as franchise_sales_retail_ly,
                     tbl_cwd_ty.franchise_sales_retail            as franchise_sales_retail_ty,
                     tbl_cwd_ly.franchise_sales_cost              as franchise_sales_cost_ly,
                     tbl_cwd_ty.franchise_sales_cost              as franchise_sales_cost_ty,
                     tbl_cwd_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_cwd_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_cwd_ly.htd_gafs_retail                   as htd_gafs_retail_ly,
                     tbl_cwd_ty.htd_gafs_retail                   as htd_gafs_retail_ty,
                     tbl_cwd_ly.freight_claim_cost                as freight_claim_cost_ly,
                     tbl_cwd_ty.freight_claim_cost                as freight_claim_cost_ty,
                     tbl_cwd_ly.stock_adj_cogs_cost               as stock_adj_cogs_cost_ly,
                     tbl_cwd_ty.stock_adj_cogs_cost               as stock_adj_cogs_cost_ty,
                     tbl_cwd_ly.stock_adj_cost                    as stock_adj_cost_ly,
                     tbl_cwd_ty.stock_adj_cost                    as stock_adj_cost_ty,
                     tbl_cwd_ly.shrinkage_cost                    as shrinkage_cost_ly,
                     tbl_cwd_ty.shrinkage_cost                    as shrinkage_cost_ty,
                     tbl_cwd_ly.shrinkage_retail                  as shrinkage_retail_ly,
                     tbl_cwd_ty.shrinkage_retail                  as shrinkage_retail_ty,
                     tbl_cwd_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_cwd_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_cwd_ly.htd_gafs_cost                     as htd_gafs_cost_ly,
                     tbl_cwd_ty.htd_gafs_cost                     as htd_gafs_cost_ty,
                     tbl_cwd_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_cwd_ty.margin_cost_variance              as margin_cost_variance_ty,
                     tbl_cwd_ly.cls_stk_cost                      as cls_stk_cost_ly,
                     tbl_cwd_ty.cls_stk_cost                      as cls_stk_cost_ty,
                     tbl_cwd_ly.purch_retail                      as purch_retail_ly,
                     tbl_cwd_ty.purch_retail                      as purch_retail_ty,
                     tbl_cwd_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_cwd_ty.rtv_retail                        as rtv_retail_ty,
                     NULL                                         as add_cost_ly, --tbl_cwd_ly.add_cost                    as add_cost_ly,
                     NULL                                         as add_cost_ty, --tbl_cwd_ty.add_cost                    as add_cost_ty,
                     NULL                                         as add_retail_ly, --tbl_cwd_ly.add_retail                  as add_retail_ly,
                     NULL                                         as add_retail_ty, --tbl_cwd_ty.add_retail                  as add_retail_ty,
                     NULL                                         as shrink_cost_pct_ly, --tbl_cwd_ly.shrink_cost_pct             as shrink_cost_pct_ly,
                     NULL                                         as shrink_cost_pct_ty, --tbl_cwd_ty.shrink_cost_pct             as shrink_cost_pct_ty,
                     NULL                                         as red_retail_ly, --tbl_cwd_ly.red_retail                  as red_retail_ly,
                     NULL                                         as red_retail_ty, --tbl_cwd_ty.red_retail                  as red_retail_ty,
                     tbl_cwd_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_cwd_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_cwd_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_cwd_ty.deal_income_purch                 as deal_income_purch_ty,
                     tbl_cwd_ly.vat_in                            as vat_in_ly,
                     tbl_cwd_ty.vat_in                            as vat_in_ty,
                     tbl_cwd_ly.vat_out                           as vat_out_ly,
                     tbl_cwd_ty.vat_out                           as vat_out_ty,
                     tbl_cwd_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_cwd_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_cwd_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_cwd_ty.restocking_fee                    as restocking_fee_ty,
                     tbl_cwd_ly.freight_cost                      as freight_cost_ly,
                     tbl_cwd_ty.freight_cost                      as freight_cost_ty,
                     tbl_cwd_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_cwd_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     tbl_cwd_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_cwd_ty.workroom_amt                      as workroom_amt_ty,
                     tbl_cwd_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_cwd_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_cwd_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_cwd_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_cwd_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_cwd_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_cwd_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_cwd_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_cwd_ly.markup_retail                     as markup_retail_ly,
                     tbl_cwd_ty.markup_retail                     as markup_retail_ty,
                     tbl_cwd_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_cwd_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_cwd_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_cwd_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_cwd_ly.weight_variance_retail            as weight_variance_retail_ly,
                     tbl_cwd_ty.weight_variance_retail            as weight_variance_retail_ty,
                     tbl_cwd_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_cwd_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_cwd_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_cwd_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_cwd_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_cwd_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_cwd_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_cwd_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_cwd_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_cwd_ty.intercompany_markup               as intercompany_markup_ty,
                     tbl_cwd_ly.intercompany_margin               as intercompany_margin_ly,
                     tbl_cwd_ty.intercompany_margin               as intercompany_margin_ty,
                     tbl_cwd_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_cwd_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_cwd_ly.net_sales_retail                  as net_sales_retail_ly,
                     tbl_cwd_ty.net_sales_retail                  as net_sales_retail_ty,
                     NULL                                         as net_markup_ly, --tbl_cwd_ly.net_markup                  as net_markup_ly,
                     NULL                                         as net_markup_ty, --tbl_cwd_ty.net_markup                  as net_markup_ty,
                     NULL                                         as net_reclass_cost_ly, --tbl_cwd_ly.net_reclass_cost            as net_reclass_cost_ly,
                     NULL                                         as net_reclass_cost_ty, --tbl_cwd_ty.net_reclass_cost            as net_reclass_cost_ty,
                     NULL                                         as net_tsf_cost_ly, --tbl_cwd_ly.net_tsf_cost                as net_tsf_cost_ly,
                     NULL                                         as net_tsf_cost_ty, --tbl_cwd_ty.net_tsf_cost                as net_tsf_cost_ty,
                     tbl_cwd_ly.sales_units                       as sales_units_ly,
                     tbl_cwd_ty.sales_units                       as sales_units_ty,
                     NULL                                         as cum_markon_pct_ly, --tbl_cwd_ly.cum_markon_pct              as cum_markon_pct_ly,
                     NULL                                         as cum_markon_pct_ty, --tbl_cwd_ty.cum_markon_pct              as cum_markon_pct_ty,
                     tbl_cwd_ly.gross_margin_amt                  as gross_margin_amt_ly,
                     tbl_cwd_ty.gross_margin_amt                  as gross_margin_amt_ty,
                     tbl_cwd_ly.net_sales_non_inv_cost            as net_sales_non_inv_cost_ly,
                     tbl_cwd_ty.net_sales_non_inv_cost            as net_sales_non_inv_cost_ty
                from table(cast(L_tbl_c_week_data_ly as OBJ_WEEK_DATA_TBL)) tbl_cwd_ly 
                full outer join table(cast(L_tbl_c_week_data_ty as OBJ_WEEK_DATA_TBL)) tbl_cwd_ty 
                  ON tbl_cwd_ly.dept         =  tbl_cwd_ty.dept
                     and tbl_cwd_ly.class    =  tbl_cwd_ty.class 
                     and tbl_cwd_ly.subclass =  tbl_cwd_ty.subclass
                     and tbl_cwd_ly.loc_type =  tbl_cwd_ty.loc_type
                     and tbl_cwd_ly.location =  tbl_cwd_ty.location
                     and tbl_cwd_ly.half_no  =  tbl_cwd_ty.half_no
                     and tbl_cwd_ly.week_no  =  tbl_cwd_ty.week_no
                     and tbl_cwd_ly.eow_date =  tbl_cwd_ty.eow_date) outer_query;    
BEGIN
   LP_c_week_data_tbl   := new OBJ_COST_WEEK_DATA_TBL();
   L_tbl_c_week_data_ty := new OBJ_WEEK_DATA_TBL();
   L_tbl_c_week_data_ly := new OBJ_WEEK_DATA_TBL();

   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;  

   if STKLEDGR_QUERY_SQL.FETCH_WEEK_DATA_BULK(LP_sch_crit_rec.dept,
                                              LP_sch_crit_rec.class,
                                              LP_sch_crit_rec.subclass,
                                              'T',
                                              LP_sch_crit_rec.period_date,
                                              LP_loc_type,
                                              LP_location,
                                              'Y', --:B_search.CB_thousands,
                                              LP_currency_ind,
                                              LP_set_of_books_id,
                                              L_tbl_c_week_data_ty,
                                              O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if STKLEDGR_QUERY_SQL.FETCH_WEEK_DATA_BULK(LP_sch_crit_rec.dept,
                                              LP_sch_crit_rec.class,
                                              LP_sch_crit_rec.subclass,
                                              'L',
                                              LP_sch_crit_rec.period_date,
                                              LP_loc_type,
                                              LP_location,
                                              'Y', --:B_search.CB_thousands,
                                              LP_currency_ind,
                                              LP_set_of_books_id,
                                              L_tbl_c_week_data_ly,
                                              O_error_message) = FALSE then
      return FALSE;
   end if;
   
   open C_COST_WEEK_DATA_INFO;
   fetch C_COST_WEEK_DATA_INFO BULK COLLECT into LP_c_week_data_tbl;
   close C_COST_WEEK_DATA_INFO;
  
   FOR i in 1..LP_c_week_data_tbl.count
   LOOP
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_c_week_data_tbl(i).dept,
                                  LP_c_week_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_c_week_data_tbl(i).dept,
                                   LP_c_week_data_tbl(i).class,
                                   LP_c_week_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_c_week_data_tbl(i).dept,
                                       LP_c_week_data_tbl(i).class,
                                       LP_c_week_data_tbl(i).subclass,
                                       LP_c_week_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;     
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_c_week_data_tbl(i).loc_name,
                                       LP_c_week_data_tbl(i).location,
                                       LP_c_week_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_c_week_data_tbl(i).loc_type,
                                    LP_c_week_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_c_week_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_c_week_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_c_week_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_c_week_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;      
      LP_c_week_data_tbl(i).eom_date := LP_sch_crit_rec.period_date;
      -- CALCULATE_FIELDS
      L_net_tsf_cost_ly := LP_c_week_data_tbl(i).tsf_in_cost_ly - LP_c_week_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_c_week_data_tbl(i).tsf_in_cost_ty - LP_c_week_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_c_week_data_tbl(i).tsf_in_retail_ly - LP_c_week_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_c_week_data_tbl(i).tsf_in_retail_ty - LP_c_week_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_c_week_data_tbl(i).markup_retail_ly - LP_c_week_data_tbl(i).markup_can_retail_ly + LP_c_week_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_c_week_data_tbl(i).markup_retail_ty - LP_c_week_data_tbl(i).markup_can_retail_ty + LP_c_week_data_tbl(i).franchise_markup_retail_ty;
      
      L_add_cost_ly := LP_c_week_data_tbl(i).purch_cost_ly - LP_c_week_data_tbl(i).rtv_cost_ly + LP_c_week_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_c_week_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_c_week_data_tbl(i).purch_cost_ty - LP_c_week_data_tbl(i).rtv_cost_ty + LP_c_week_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_c_week_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_c_week_data_tbl(i).purch_retail_ly - LP_c_week_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_c_week_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_c_week_data_tbl(i).purch_retail_ty - LP_c_week_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_c_week_data_tbl(i).franchise_returns_retail_ty;
      
      
      if (LP_c_week_data_tbl(i).net_sales_cost_ly is NULL and LP_c_week_data_tbl(i).franchise_sales_cost_ly is NULL and LP_c_week_data_tbl(i).franchise_returns_cost_ly is NULL) 
         or (LP_c_week_data_tbl(i).net_sales_cost_ly = 0 and (LP_c_week_data_tbl(i).franchise_sales_cost_ly - LP_c_week_data_tbl(i).franchise_returns_cost_ly) = 0) then
         L_shrink_cost_pct_ly := 0;
      else
         L_shrink_cost_pct_ly := ((LP_c_week_data_tbl(i).shrinkage_cost_ly / (LP_c_week_data_tbl(i).net_sales_cost_ly + LP_c_week_data_tbl(i).franchise_sales_cost_ly - LP_c_week_data_tbl(i).franchise_returns_cost_ly))*100);
      end if;
      
      if (LP_c_week_data_tbl(i).net_sales_cost_ty is NULL and LP_c_week_data_tbl(i).franchise_sales_cost_ty is NULL and LP_c_week_data_tbl(i).franchise_returns_cost_ty is NULL) 
         or (LP_c_week_data_tbl(i).net_sales_cost_ty = 0 and (LP_c_week_data_tbl(i).franchise_sales_cost_ty - LP_c_week_data_tbl(i).franchise_returns_cost_ty) = 0) then
         L_shrink_cost_pct_ty := 0;
      else
         L_shrink_cost_pct_ty := ((LP_c_week_data_tbl(i).shrinkage_cost_ty / (LP_c_week_data_tbl(i).net_sales_cost_ty + LP_c_week_data_tbl(i).franchise_sales_cost_ty - LP_c_week_data_tbl(i).franchise_returns_cost_ty))*100);
      end if;
      
      L_net_markdown_retail_ly := LP_c_week_data_tbl(i).clear_markdown_retail_ly + LP_c_week_data_tbl(i).perm_markdown_retail_ly + LP_c_week_data_tbl(i).prom_markdown_retail_ly - LP_c_week_data_tbl(i).markdown_can_retail_ly + LP_c_week_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_c_week_data_tbl(i).clear_markdown_retail_ty + LP_c_week_data_tbl(i).perm_markdown_retail_ty + LP_c_week_data_tbl(i).prom_markdown_retail_ty - LP_c_week_data_tbl(i).markdown_can_retail_ty + LP_c_week_data_tbl(i).franchise_markdown_retail_ty;
      
      L_red_retail_ly   := LP_c_week_data_tbl(i).net_sales_retail_ly + LP_c_week_data_tbl(i).shrinkage_retail_ly + L_net_markdown_retail_ly + LP_c_week_data_tbl(i).empl_disc_retail_ly + LP_c_week_data_tbl(i).weight_variance_retail_ly + LP_c_week_data_tbl(i).franchise_markdown_retail_ly + LP_c_week_data_tbl(i).franchise_sales_retail_ly - LP_c_week_data_tbl(i).franchise_returns_retail_ly;
      L_red_retail_ty   := LP_c_week_data_tbl(i).net_sales_retail_ty + LP_c_week_data_tbl(i).shrinkage_retail_ty + L_net_markdown_retail_ty + LP_c_week_data_tbl(i).empl_disc_retail_ty + LP_c_week_data_tbl(i).weight_variance_retail_ty + LP_c_week_data_tbl(i).franchise_markdown_retail_ty + LP_c_week_data_tbl(i).franchise_sales_retail_ty - LP_c_week_data_tbl(i).franchise_returns_retail_ty;

      L_net_reclass_cost_ly   := LP_c_week_data_tbl(i).reclass_in_cost_ly - LP_c_week_data_tbl(i).reclass_out_cost_ly;
      L_net_reclass_cost_ty   := LP_c_week_data_tbl(i).reclass_in_cost_ty - LP_c_week_data_tbl(i).reclass_out_cost_ty;
      
      if LP_c_week_data_tbl(i).htd_gafs_retail_ly is NULL or LP_c_week_data_tbl(i).htd_gafs_retail_ly = 0 then
         L_cum_markon_pct_ly := 0;
      else
         L_cum_markon_pct_ly := ((1 - (LP_c_week_data_tbl(i).htd_gafs_cost_ly / LP_c_week_data_tbl(i).htd_gafs_retail_ly)) * 100);
      end if;

      if LP_c_week_data_tbl(i).htd_gafs_retail_ty is NULL or LP_c_week_data_tbl(i).htd_gafs_retail_ty = 0 then
         L_cum_markon_pct_ty := 0;
      else
         L_cum_markon_pct_ty := ((1 - (LP_c_week_data_tbl(i).htd_gafs_cost_ty / LP_c_week_data_tbl(i).htd_gafs_retail_ty)) * 100);
      end if; 
      
      LP_c_week_data_tbl(i).add_cost_ly                := L_add_cost_ly; 
      LP_c_week_data_tbl(i).add_cost_ty                := L_add_cost_ty;                
      LP_c_week_data_tbl(i).add_retail_ly              := L_add_retail_ly;                
      LP_c_week_data_tbl(i).add_retail_ty              := L_add_retail_ty;                
      LP_c_week_data_tbl(i).shrink_cost_pct_ly         := L_shrink_cost_pct_ly;           
      LP_c_week_data_tbl(i).shrink_cost_pct_ty         := L_shrink_cost_pct_ty;           
      LP_c_week_data_tbl(i).red_retail_ly              := L_red_retail_ly;                
      LP_c_week_data_tbl(i).red_retail_ty              := L_red_retail_ty;
      LP_c_week_data_tbl(i).net_markup_ly              := L_net_markup_retail_ly;               
      LP_c_week_data_tbl(i).net_markup_ty              := L_net_markup_retail_ty;                
      LP_c_week_data_tbl(i).net_reclass_cost_ly        := L_net_reclass_cost_ly;          
      LP_c_week_data_tbl(i).net_reclass_cost_ty        := L_net_reclass_cost_ty;        
      LP_c_week_data_tbl(i).net_tsf_cost_ly            := L_net_tsf_cost_ly;              
      LP_c_week_data_tbl(i).net_tsf_cost_ty            := L_net_tsf_cost_ty;  
      LP_c_week_data_tbl(i).cum_markon_pct_ly          := L_cum_markon_pct_ly;            
      LP_c_week_data_tbl(i).cum_markon_pct_ty          := L_cum_markon_pct_ty;                                     
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_COST_WEEK_DATA_INFO%ISOPEN then
         close C_COST_WEEK_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_C_WEEK_DATA;
--------------------------------------------------------------------------------
FUNCTION FETCH_C_DAILY_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   
   L_program                VARCHAR2(75) :=   'CORESVC_STKLDGRV.FETCH_C_DAILY_DATA';
   L_tbl_c_daily_data_ty    OBJ_DAILY_DATA_TBL;
   L_tbl_c_daily_data_ly    OBJ_DAILY_DATA_TBL;
   L_add_cost_ly            DAILY_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            DAILY_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          DAILY_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          DAILY_DATA.TSF_IN_COST%TYPE;   
   L_net_tsf_cost_ly        DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ly    DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ty    DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly DAILY_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty DAILY_DATA.TSF_IN_COST%TYPE;


   cursor C_COST_DAILY_DATA_INFO is
      select OBJ_COST_DAILY_DATA_REC(  outer_query.dept,
                                       outer_query.dept_name,
                                       outer_query.class,
                                       outer_query.class_name,
                                       outer_query.subclass,
                                       outer_query.subclass_name,
                                       outer_query.loc_type,
                                       outer_query.location,
                                       outer_query.loc_name,
                                       outer_query.currency_code,
                                       outer_query.eow_date,
                                       outer_query.day_no,
                                       outer_query.purch_cost_ly,
                                       outer_query.purch_cost_ty,
                                       outer_query.tsf_in_cost_ly,
                                       outer_query.tsf_in_cost_ty,
                                       outer_query.tsf_in_retail_ly,
                                       outer_query.tsf_in_retail_ty,
                                       outer_query.tsf_in_book_cost_ly,
                                       outer_query.tsf_in_book_cost_ty,
                                       outer_query.intercompany_in_cost_ly,
                                       outer_query.intercompany_in_cost_ty,
                                       outer_query.reclass_in_cost_ly,
                                       outer_query.reclass_in_cost_ty,
                                       outer_query.franchise_returns_cost_ly,
                                       outer_query.franchise_returns_cost_ty,
                                       outer_query.up_chrg_amt_profit_ly,
                                       outer_query.up_chrg_amt_profit_ty,
                                       outer_query.up_chrg_amt_exp_ly,
                                       outer_query.up_chrg_amt_exp_ty,
                                       outer_query.wo_activity_upd_inv_ly,
                                       outer_query.wo_activity_upd_inv_ty,
                                       outer_query.wo_activity_post_fin_ly,
                                       outer_query.wo_activity_post_fin_ty,
                                       outer_query.recoverable_tax_ly,
                                       outer_query.recoverable_tax_ty,
                                       outer_query.rtv_cost_ly,
                                       outer_query.rtv_cost_ty,
                                       outer_query.tsf_out_cost_ly,
                                       outer_query.tsf_out_cost_ty,
                                       outer_query.tsf_out_retail_ly,
                                       outer_query.tsf_out_retail_ty,
                                       outer_query.tsf_out_book_cost_ly,
                                       outer_query.tsf_out_book_cost_ty,
                                       outer_query.intercompany_out_cost_ly,
                                       outer_query.intercompany_out_cost_ty,
                                       outer_query.reclass_out_cost_ly,
                                       outer_query.reclass_out_cost_ty,
                                       outer_query.net_sales_cost_ly,
                                       outer_query.net_sales_cost_ty,
                                       outer_query.returns_cost_ly,
                                       outer_query.returns_cost_ty,
                                       outer_query.franchise_sales_cost_ly,
                                       outer_query.franchise_sales_cost_ty,
                                       outer_query.franchise_returns_retail_ly,
                                       outer_query.franchise_returns_retail_ty,
                                       outer_query.freight_claim_cost_ly,
                                       outer_query.freight_claim_cost_ty,
                                       outer_query.stock_adj_cogs_cost_ly,
                                       outer_query.stock_adj_cogs_cost_ty,
                                       outer_query.stock_adj_cost_ly,
                                       outer_query.stock_adj_cost_ty,
                                       outer_query.cost_variance_amt_ly,
                                       outer_query.cost_variance_amt_ty,
                                       outer_query.margin_cost_variance_ly,
                                       outer_query.margin_cost_variance_ty,
                                       outer_query.purch_retail_ly,
                                       outer_query.purch_retail_ty,
                                       outer_query.rtv_retail_ly,
                                       outer_query.rtv_retail_ty,
                                       outer_query.vat_in_ly,
                                       outer_query.vat_in_ty,
                                       outer_query.vat_out_ly,
                                       outer_query.vat_out_ty,
                                       outer_query.deal_income_sales_ly,
                                       outer_query.deal_income_sales_ty,
                                       outer_query.deal_income_purch_ly,
                                       outer_query.deal_income_purch_ty,
                                       outer_query.restocking_fee_ly,
                                       outer_query.restocking_fee_ty,
                                       outer_query.franchise_restocking_fee_ly,
                                       outer_query.franchise_restocking_fee_ty,
                                       outer_query.net_sales_non_inv_cost_ly,
                                       outer_query.net_sales_non_inv_cost_ty,
                                       outer_query.add_cost_ly,
                                       outer_query.add_cost_ty,
                                       outer_query.add_retail_ly,
                                       outer_query.add_retail_ty,
                                       outer_query.cash_disc_amt_ly,
                                       outer_query.cash_disc_amt_ty,
                                       outer_query.clear_markdown_retail_ly,
                                       outer_query.clear_markdown_retail_ty,
                                       outer_query.empl_disc_retail_ly,
                                       outer_query.empl_disc_retail_ty,
                                       outer_query.franchise_markdown_retail_ly,
                                       outer_query.franchise_markdown_retail_ty,
                                       outer_query.franchise_markup_retail_ly,
                                       outer_query.franchise_markup_retail_ty,
                                       outer_query.freight_cost_ly,
                                       outer_query.freight_cost_ty,
                                       outer_query.intercompany_markdown_ly,
                                       outer_query.intercompany_markdown_ty,
                                       outer_query.intercompany_markup_ly,
                                       outer_query.intercompany_markup_ty,
                                       outer_query.net_markdown_ly,
                                       outer_query.net_markdown_ty,
                                       outer_query.markdown_can_retail_ly,
                                       outer_query.markdown_can_retail_ty,
                                       outer_query.markup_can_retail_ly,
                                       outer_query.markup_can_retail_ty,
                                       outer_query.markup_retail_ly,
                                       outer_query.markup_retail_ty,
                                       outer_query.perm_markdown_retail_ly,
                                       outer_query.perm_markdown_retail_ty,
                                       outer_query.prom_markdown_retail_ly,
                                       outer_query.prom_markdown_retail_ty,
                                       outer_query.rec_cost_adj_variance_ly,
                                       outer_query.rec_cost_adj_variance_ty,
                                       outer_query.sales_units_ly,
                                       outer_query.sales_units_ty,
                                       outer_query.workroom_amt_ly,
                                       outer_query.workroom_amt_ty,
                                       outer_query.net_reclass_cost_ly,
                                       outer_query.net_reclass_cost_ty,
                                       outer_query.net_tsf_cost_ly,
                                       outer_query.net_tsf_cost_ty,
                                       outer_query.net_markup_ly,
                                       outer_query.net_markup_ty)
        from (select NVL(tbl_cdd_ly.dept,tbl_cdd_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_cdd_ly.class,tbl_cdd_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_cdd_ly.subclass,tbl_cdd_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_cdd_ly.loc_type,tbl_cdd_ty.loc_type) as loc_type,
                     NVL(tbl_cdd_ly.location,tbl_cdd_ty.location) as location,
                     NULL                                         as loc_name, --loc_name
                     NULL                                         as currency_code,
                     NULL                                         as eow_date, -- Week Ending
                     NVL(tbl_cdd_ly.day_no,tbl_cdd_ty.day_no)     as day_no,
                     tbl_cdd_ly.purch_cost                        as purch_cost_ly,
                     tbl_cdd_ty.purch_cost                        as purch_cost_ty,
                     tbl_cdd_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_cdd_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_cdd_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_cdd_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_cdd_ly.tsf_in_book_cost                  as tsf_in_book_cost_ly,
                     tbl_cdd_ty.tsf_in_book_cost                  as tsf_in_book_cost_ty,
                     tbl_cdd_ly.intercompany_in_cost              as intercompany_in_cost_ly,
                     tbl_cdd_ty.intercompany_in_cost              as intercompany_in_cost_ty,
                     tbl_cdd_ly.reclass_in_cost                   as reclass_in_cost_ly,
                     tbl_cdd_ty.reclass_in_cost                   as reclass_in_cost_ty,
                     tbl_cdd_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_cdd_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_cdd_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_cdd_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_cdd_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_cdd_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_cdd_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_cdd_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_cdd_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_cdd_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_cdd_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_cdd_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_cdd_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_cdd_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_cdd_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_cdd_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_cdd_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_cdd_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_cdd_ly.tsf_out_book_cost                 as tsf_out_book_cost_ly,
                     tbl_cdd_ty.tsf_out_book_cost                 as tsf_out_book_cost_ty,
                     tbl_cdd_ly.intercompany_out_cost             as intercompany_out_cost_ly,
                     tbl_cdd_ty.intercompany_out_cost             as intercompany_out_cost_ty,
                     tbl_cdd_ly.reclass_out_cost                  as reclass_out_cost_ly,
                     tbl_cdd_ty.reclass_out_cost                  as reclass_out_cost_ty,
                     tbl_cdd_ly.net_sales_cost                    as net_sales_cost_ly,
                     tbl_cdd_ty.net_sales_cost                    as net_sales_cost_ty,
                     tbl_cdd_ly.returns_cost                      as returns_cost_ly,
                     tbl_cdd_ty.returns_cost                      as returns_cost_ty,
                     tbl_cdd_ly.franchise_sales_cost              as franchise_sales_cost_ly,
                     tbl_cdd_ty.franchise_sales_cost              as franchise_sales_cost_ty,
                     tbl_cdd_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_cdd_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_cdd_ly.freight_claim_cost                as freight_claim_cost_ly,
                     tbl_cdd_ty.freight_claim_cost                as freight_claim_cost_ty,
                     tbl_cdd_ly.stock_adj_cogs_cost               as stock_adj_cogs_cost_ly,
                     tbl_cdd_ty.stock_adj_cogs_cost               as stock_adj_cogs_cost_ty,
                     tbl_cdd_ly.stock_adj_cost                    as stock_adj_cost_ly,
                     tbl_cdd_ty.stock_adj_cost                    as stock_adj_cost_ty,
                     tbl_cdd_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_cdd_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_cdd_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_cdd_ty.margin_cost_variance              as margin_cost_variance_ty,
                     tbl_cdd_ly.purch_retail                      as purch_retail_ly,
                     tbl_cdd_ty.purch_retail                      as purch_retail_ty,
                     tbl_cdd_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_cdd_ty.rtv_retail                        as rtv_retail_ty,
                     tbl_cdd_ly.vat_in                            as vat_in_ly,
                     tbl_cdd_ty.vat_in                            as vat_in_ty,
                     tbl_cdd_ly.vat_out                           as vat_out_ly,
                     tbl_cdd_ty.vat_out                           as vat_out_ty,
                     tbl_cdd_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_cdd_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_cdd_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_cdd_ty.deal_income_purch                 as deal_income_purch_ty,
                     tbl_cdd_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_cdd_ty.restocking_fee                    as restocking_fee_ty,
                     tbl_cdd_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_cdd_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_cdd_ly.net_sales_non_inv_cost            as net_sales_non_inv_cost_ly,
                     tbl_cdd_ty.net_sales_non_inv_cost            as net_sales_non_inv_cost_ty,
                     NULL                                         as add_cost_ly, --tbl_cdd_ly.add_cost                    as add_cost_ly,
                     NULL                                         as add_cost_ty, --tbl_cdd_ty.add_cost                    as add_cost_ty,
                     NULL                                         as add_retail_ly, --tbl_cdd_ly.add_retail                  as add_retail_ly,
                     NULL                                         as add_retail_ty, --tbl_cdd_ty.add_retail                  as add_retail_ty,
                     tbl_cdd_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_cdd_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_cdd_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_cdd_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_cdd_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_cdd_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_cdd_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_cdd_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_cdd_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_cdd_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_cdd_ly.freight_cost                      as freight_cost_ly,
                     tbl_cdd_ty.freight_cost                      as freight_cost_ty,
                     tbl_cdd_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_cdd_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_cdd_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_cdd_ty.intercompany_markup               as intercompany_markup_ty,
                     NULL                                         as net_markdown_ly, -- tbl_cdd_ly.net_markdown  
                     NULL                                         as net_markdown_ty, -- tbl_cdd_ty.net_markdown
                     tbl_cdd_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_cdd_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_cdd_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_cdd_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_cdd_ly.markup_retail                     as markup_retail_ly,
                     tbl_cdd_ty.markup_retail                     as markup_retail_ty,
                     tbl_cdd_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_cdd_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_cdd_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_cdd_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_cdd_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_cdd_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     tbl_cdd_ly.sales_units                       as sales_units_ly,
                     tbl_cdd_ty.sales_units                       as sales_units_ty,
                     tbl_cdd_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_cdd_ty.workroom_amt                      as workroom_amt_ty,
                     NULL                                         as net_reclass_cost_ly,--tbl_cdd_ly.net_reclass_cost 
                     NULL                                         as net_reclass_cost_ty,--tbl_cdd_ty.net_reclass_cost 
                     NULL                                         as net_tsf_cost_ly, --tbl_cdd_ly.net_tsf_cost         
                     NULL                                         as net_tsf_cost_ty, --tbl_cdd_ty.net_tsf_cost         
                     NULL                                         as net_markup_ly, --tbl_cdd_ly.net_markup             
                     NULL                                         as net_markup_ty --tbl_cdd_ty.net_markup              
                from table(cast(L_tbl_c_daily_data_ly as OBJ_DAILY_DATA_TBL)) tbl_cdd_ly 
                full outer join table(cast(L_tbl_c_daily_data_ty as OBJ_DAILY_DATA_TBL)) tbl_cdd_ty 
                  on tbl_cdd_ly.dept          =  tbl_cdd_ty.dept
                     and tbl_cdd_ly.class     =  tbl_cdd_ty.class 
                     and tbl_cdd_ly.subclass  =  tbl_cdd_ty.subclass
                     and tbl_cdd_ly.loc_type  =  tbl_cdd_ty.loc_type
                     and tbl_cdd_ly.location  =  tbl_cdd_ty.location
                     and tbl_cdd_ly.half_no   =  tbl_cdd_ty.half_no
                     and tbl_cdd_ly.data_date =  tbl_cdd_ty.data_date
                     and tbl_cdd_ly.day_no    =  tbl_cdd_ty.day_no) outer_query;
BEGIN
   LP_c_daily_data_tbl   := new OBJ_COST_DAILY_DATA_TBL();
   L_tbl_c_daily_data_ty := new  OBJ_DAILY_DATA_TBL();
   L_tbl_c_daily_data_ly := new  OBJ_DAILY_DATA_TBL();
   
   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;  

   if LP_cal_454 = 'C' then
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_CAL_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'T',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y', --:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_c_daily_data_ty,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
      
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_CAL_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'L',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_c_daily_data_ly,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
   else
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_454_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'T',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_c_daily_data_ty,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
      
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_454_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'L',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_c_daily_data_ly,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   
   open C_COST_DAILY_DATA_INFO;
   fetch C_COST_DAILY_DATA_INFO BULK COLLECT into LP_c_daily_data_tbl;
   close C_COST_DAILY_DATA_INFO;
  
   FOR i in 1..LP_c_daily_data_tbl.count
   LOOP
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_c_daily_data_tbl(i).dept,
                                  LP_c_daily_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_c_daily_data_tbl(i).dept,
                                   LP_c_daily_data_tbl(i).class,
                                   LP_c_daily_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_c_daily_data_tbl(i).dept,
                                       LP_c_daily_data_tbl(i).class,
                                       LP_c_daily_data_tbl(i).subclass,
                                       LP_c_daily_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;     
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_c_daily_data_tbl(i).loc_name,
                                       LP_c_daily_data_tbl(i).location,
                                       LP_c_daily_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_c_daily_data_tbl(i).loc_type,
                                    LP_c_daily_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_c_daily_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_c_daily_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_c_daily_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_c_daily_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;      
      
      LP_c_daily_data_tbl(i).eow_date := LP_sch_crit_rec.period_date;
      -- CALCULATE_FIELDS
      L_net_tsf_cost_ly := LP_c_daily_data_tbl(i).tsf_in_cost_ly - LP_c_daily_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_c_daily_data_tbl(i).tsf_in_cost_ty - LP_c_daily_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_c_daily_data_tbl(i).tsf_in_retail_ly - LP_c_daily_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_c_daily_data_tbl(i).tsf_in_retail_ty - LP_c_daily_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_c_daily_data_tbl(i).markup_retail_ly - LP_c_daily_data_tbl(i).markup_can_retail_ly + LP_c_daily_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_c_daily_data_tbl(i).markup_retail_ty - LP_c_daily_data_tbl(i).markup_can_retail_ty + LP_c_daily_data_tbl(i).franchise_markup_retail_ty;
                   
      L_add_cost_ly := LP_c_daily_data_tbl(i).purch_cost_ly - LP_c_daily_data_tbl(i).rtv_cost_ly + LP_c_daily_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_c_daily_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_c_daily_data_tbl(i).purch_cost_ty - LP_c_daily_data_tbl(i).rtv_cost_ty + LP_c_daily_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_c_daily_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_c_daily_data_tbl(i).purch_retail_ly - LP_c_daily_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_c_daily_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_c_daily_data_tbl(i).purch_retail_ty - LP_c_daily_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_c_daily_data_tbl(i).franchise_returns_retail_ty;
            
      L_net_markdown_retail_ly := LP_c_daily_data_tbl(i).clear_markdown_retail_ly + LP_c_daily_data_tbl(i).perm_markdown_retail_ly + LP_c_daily_data_tbl(i).prom_markdown_retail_ly - LP_c_daily_data_tbl(i).markdown_can_retail_ly + LP_c_daily_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_c_daily_data_tbl(i).clear_markdown_retail_ty + LP_c_daily_data_tbl(i).perm_markdown_retail_ty + LP_c_daily_data_tbl(i).prom_markdown_retail_ty - LP_c_daily_data_tbl(i).markdown_can_retail_ty + LP_c_daily_data_tbl(i).franchise_markdown_retail_ty;
      
      L_net_reclass_cost_ly   := LP_c_daily_data_tbl(i).reclass_in_cost_ly - LP_c_daily_data_tbl(i).reclass_out_cost_ly;          
      L_net_reclass_cost_ty   := LP_c_daily_data_tbl(i).reclass_in_cost_ty - LP_c_daily_data_tbl(i).reclass_out_cost_ty;         
      
      LP_c_daily_data_tbl(i).add_cost_ly              := L_add_cost_ly; 
      LP_c_daily_data_tbl(i).add_cost_ty              := L_add_cost_ty;
      LP_c_daily_data_tbl(i).add_retail_ly            := L_add_retail_ly;
      LP_c_daily_data_tbl(i).add_retail_ty            := L_add_retail_ty;
      LP_c_daily_data_tbl(i).net_markdown_ly          := L_net_markdown_retail_ly;
      LP_c_daily_data_tbl(i).net_markdown_ty          := L_net_markdown_retail_ty;
      LP_c_daily_data_tbl(i).net_reclass_cost_ly      := L_net_reclass_cost_ly;
      LP_c_daily_data_tbl(i).net_reclass_cost_ty      := L_net_reclass_cost_ty;
      LP_c_daily_data_tbl(i).net_tsf_cost_ly          := L_net_tsf_cost_ly;
      LP_c_daily_data_tbl(i).net_tsf_cost_ty          := L_net_tsf_cost_ty;
      LP_c_daily_data_tbl(i).net_markup_ly            := L_net_markup_retail_ly;
      LP_c_daily_data_tbl(i).net_markup_ty            := L_net_markup_retail_ty;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_COST_DAILY_DATA_INFO%ISOPEN then
         close C_COST_DAILY_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_C_DAILY_DATA;
--------------------------------------------------------------------------------
FUNCTION FETCH_R_MONTH_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   
   L_program                VARCHAR2(75) :=   'CORESVC_STKLDGRV.FETCH_R_MONTH_DATA';
   L_tbl_r_month_data_ty    OBJ_MONTH_DATA_TBL;
   L_tbl_r_month_data_ly    OBJ_MONTH_DATA_TBL;
   L_add_cost_ly            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;   
   L_net_tsf_cost_ly        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   MONTH_DATA.TSF_IN_COST%TYPE;
   L_shrink_retail_pct_ly   NUMBER(12,4);
   L_shrink_retail_pct_ty   NUMBER(12,4);
   L_red_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_red_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ly  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ty  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty MONTH_DATA.TSF_IN_COST%TYPE;
   L_cum_markon_pct_ly      NUMBER(12,4);
   L_cum_markon_pct_ty      NUMBER(12,4);
   
   cursor C_RETAIL_MONTH_DATA_INFO is
      select OBJ_RETAIL_MONTH_DATA_REC(outer_query.dept,
                                       outer_query.dept_name,
                                       outer_query.class,
                                       outer_query.class_name,
                                       outer_query.subclass,
                                       outer_query.subclass_name,
                                       outer_query.loc_type,
                                       outer_query.location,
                                       outer_query.loc_name,
                                       outer_query.currency_code,
                                       outer_query.half_no,
                                       outer_query.month_no,
                                       outer_query.opn_stk_retail_ly,
                                       outer_query.opn_stk_retail_ty,
                                       outer_query.purch_retail_ly,
                                       outer_query.purch_retail_ty,
                                       outer_query.purch_cost_ly,
                                       outer_query.purch_cost_ty,
                                       outer_query.tsf_in_cost_ly,
                                       outer_query.tsf_in_cost_ty,
                                       outer_query.tsf_in_retail_ly,
                                       outer_query.tsf_in_retail_ty,
                                       outer_query.tsf_in_book_retail_ly,
                                       outer_query.tsf_in_book_retail_ty,
                                       outer_query.intercompany_in_retail_ly,
                                       outer_query.intercompany_in_retail_ty,
                                       outer_query.reclass_in_retail_ly,
                                       outer_query.reclass_in_retail_ty,
                                       outer_query.franchise_returns_retail_ly,
                                       outer_query.franchise_returns_retail_ty,
                                       outer_query.markup_retail_ly,
                                       outer_query.markup_retail_ty,
                                       outer_query.franchise_markup_retail_ly,
                                       outer_query.franchise_markup_retail_ty,
                                       outer_query.intercompany_markup_ly,
                                       outer_query.intercompany_markup_ty,
                                       outer_query.markup_can_retail_ly,
                                       outer_query.markup_can_retail_ty,
                                       outer_query.rtv_retail_ly,
                                       outer_query.rtv_retail_ty,
                                       outer_query.rtv_cost_ly,
                                       outer_query.rtv_cost_ty,
                                       outer_query.tsf_out_cost_ly,
                                       outer_query.tsf_out_cost_ty,
                                       outer_query.tsf_out_retail_ly,
                                       outer_query.tsf_out_retail_ty,
                                       outer_query.tsf_out_book_retail_ly,
                                       outer_query.tsf_out_book_retail_ty,
                                       outer_query.intercompany_out_retail_ly,
                                       outer_query.intercompany_out_retail_ty,
                                       outer_query.reclass_out_retail_ly,
                                       outer_query.reclass_out_retail_ty,
                                       outer_query.net_sales_retail_ly,
                                       outer_query.net_sales_retail_ty,
                                       outer_query.returns_retail_ly,
                                       outer_query.returns_retail_ty,
                                       outer_query.weight_variance_retail_ly,
                                       outer_query.weight_variance_retail_ty,
                                       outer_query.empl_disc_retail_ly,
                                       outer_query.empl_disc_retail_ty,
                                       outer_query.franchise_sales_retail_ly,
                                       outer_query.franchise_sales_retail_ty,
                                       outer_query.freight_claim_retail_ly,
                                       outer_query.freight_claim_retail_ty,
                                       outer_query.stock_adj_cogs_retail_ly,
                                       outer_query.stock_adj_cogs_retail_ty,
                                       outer_query.stock_adj_retail_ly,
                                       outer_query.stock_adj_retail_ty,
                                       outer_query.shrinkage_retail_ly,
                                       outer_query.shrinkage_retail_ty,
                                       outer_query.perm_markdown_retail_ly,
                                       outer_query.perm_markdown_retail_ty,
                                       outer_query.prom_markdown_retail_ly,
                                       outer_query.prom_markdown_retail_ty,
                                       outer_query.clear_markdown_retail_ly,
                                       outer_query.clear_markdown_retail_ty,
                                       outer_query.intercompany_markdown_ly,
                                       outer_query.intercompany_markdown_ty,
                                       outer_query.franchise_markdown_retail_ly,
                                       outer_query.franchise_markdown_retail_ty,
                                       outer_query.markdown_can_retail_ly,
                                       outer_query.markdown_can_retail_ty,
                                       outer_query.cls_stk_retail_ly,
                                       outer_query.cls_stk_retail_ty,
                                       outer_query.add_retail_ly,
                                       outer_query.add_retail_ty,
                                       outer_query.red_retail_ly,
                                       outer_query.red_retail_ty,
                                       outer_query.add_cost_ly,
                                       outer_query.add_cost_ty,
                                       outer_query.htd_gafs_retail_ly,
                                       outer_query.htd_gafs_retail_ty,
                                       outer_query.htd_gafs_cost_ly,
                                       outer_query.htd_gafs_cost_ty,
                                       outer_query.cum_markon_pct_ly,
                                       outer_query.cum_markon_pct_ty,
                                       outer_query.gross_margin_amt_ly,
                                       outer_query.gross_margin_amt_ty,
                                       outer_query.vat_in_ly,
                                       outer_query.vat_in_ty,
                                       outer_query.vat_out_ly,
                                       outer_query.vat_out_ty,
                                       outer_query.franchise_returns_cost_ly,
                                       outer_query.franchise_returns_cost_ty,
                                       outer_query.up_chrg_amt_profit_ly,
                                       outer_query.up_chrg_amt_profit_ty,
                                       outer_query.up_chrg_amt_exp_ly,
                                       outer_query.up_chrg_amt_exp_ty,
                                       outer_query.wo_activity_upd_inv_ly,
                                       outer_query.wo_activity_upd_inv_ty,
                                       outer_query.wo_activity_post_fin_ly,
                                       outer_query.wo_activity_post_fin_ty,
                                       outer_query.freight_cost_ly,
                                       outer_query.freight_cost_ty,
                                       outer_query.workroom_amt_ly,
                                       outer_query.workroom_amt_ty,
                                       outer_query.cash_disc_amt_ly,
                                       outer_query.cash_disc_amt_ty,
                                       outer_query.cost_variance_amt_ly,
                                       outer_query.cost_variance_amt_ty,
                                       outer_query.rec_cost_adj_variance_ly,
                                       outer_query.rec_cost_adj_variance_ty,
                                       outer_query.retail_cost_variance_ly,
                                       outer_query.retail_cost_variance_ty,
                                       outer_query.deal_income_purch_ly,
                                       outer_query.deal_income_purch_ty,
                                       outer_query.deal_income_sales_ly,
                                       outer_query.deal_income_sales_ty,
                                       outer_query.franchise_restocking_fee_ly,
                                       outer_query.franchise_restocking_fee_ty,
                                       outer_query.intercompany_margin_ly,
                                       outer_query.intercompany_margin_ty,
                                       outer_query.margin_cost_variance_ly,
                                       outer_query.margin_cost_variance_ty,
                                       outer_query.net_markdown_ly,
                                       outer_query.net_markdown_ty,
                                       outer_query.net_markup_ly,
                                       outer_query.net_markup_ty,
                                       outer_query.net_reclass_retail_ly,
                                       outer_query.net_reclass_retail_ty,
                                       outer_query.net_sales_retail_ex_vat_ly,
                                       outer_query.net_sales_retail_ex_vat_ty,
                                       outer_query.net_sale_noninv_r_exvat_ly,
                                       outer_query.net_sale_noninv_r_exvat_ly,
                                       outer_query.net_tsf_retail_ly,
                                       outer_query.net_tsf_retail_ty,
                                       outer_query.net_tsf_cost_ly,
                                       outer_query.net_tsf_cost_ty,
                                       outer_query.recoverable_tax_ly,
                                       outer_query.recoverable_tax_ty,
                                       outer_query.restocking_fee_ly,
                                       outer_query.restocking_fee_ty,
                                       outer_query.shrink_retail_pct_ly,
                                       outer_query.shrink_retail_pct_ty,
                                       outer_query.sales_units_ly,
                                       outer_query.sales_units_ty,
                                       outer_query.net_sales_noninv_retail_ly,
                                       outer_query.net_sales_noninv_retail_ty)
        from (select NVL(tbl_rmd_ly.dept,tbl_rmd_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_rmd_ly.class,tbl_rmd_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_rmd_ly.subclass,tbl_rmd_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_rmd_ly.loc_type,tbl_rmd_ty.loc_type) as loc_type,
                     NVL(tbl_rmd_ly.location,tbl_rmd_ty.location) as location,
                     NULL                                         as loc_name, --loc_name
                     NULL                                         as currency_code,
                     NVL(tbl_rmd_ly.half_no,tbl_rmd_ty.half_no)   as half_no,
                     NVL(tbl_rmd_ly.month_no,tbl_rmd_ty.month_no) as month_no,
                     tbl_rmd_ly.opn_stk_retail                    as opn_stk_retail_ly,
                     tbl_rmd_ty.opn_stk_retail                    as opn_stk_retail_ty,
                     tbl_rmd_ly.purch_retail                      as purch_retail_ly,
                     tbl_rmd_ty.purch_retail                      as purch_retail_ty,
                     tbl_rmd_ly.purch_cost                        as purch_cost_ly,
                     tbl_rmd_ty.purch_cost                        as purch_cost_ty,
                     tbl_rmd_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_rmd_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_rmd_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_rmd_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_rmd_ly.tsf_in_book_retail                as tsf_in_book_retail_ly,
                     tbl_rmd_ty.tsf_in_book_retail                as tsf_in_book_retail_ty,
                     tbl_rmd_ly.intercompany_in_retail            as intercompany_in_retail_ly,
                     tbl_rmd_ty.intercompany_in_retail            as intercompany_in_retail_ty,
                     tbl_rmd_ly.reclass_in_retail                 as reclass_in_retail_ly,
                     tbl_rmd_ty.reclass_in_retail                 as reclass_in_retail_ty,
                     tbl_rmd_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_rmd_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_rmd_ly.markup_retail                     as markup_retail_ly,
                     tbl_rmd_ty.markup_retail                     as markup_retail_ty,
                     tbl_rmd_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_rmd_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_rmd_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_rmd_ty.intercompany_markup               as intercompany_markup_ty,
                     tbl_rmd_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_rmd_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_rmd_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_rmd_ty.rtv_retail                        as rtv_retail_ty,
                     tbl_rmd_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_rmd_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_rmd_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_rmd_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_rmd_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_rmd_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_rmd_ly.tsf_out_book_retail               as tsf_out_book_retail_ly,
                     tbl_rmd_ty.tsf_out_book_retail               as tsf_out_book_retail_ty,
                     tbl_rmd_ly.intercompany_out_retail           as intercompany_out_retail_ly,
                     tbl_rmd_ty.intercompany_out_retail           as intercompany_out_retail_ty,
                     tbl_rmd_ly.reclass_out_retail                as reclass_out_retail_ly,
                     tbl_rmd_ty.reclass_out_retail                as reclass_out_retail_ty,
                     tbl_rmd_ly.net_sales_retail                  as net_sales_retail_ly,
                     tbl_rmd_ty.net_sales_retail                  as net_sales_retail_ty,
                     tbl_rmd_ly.returns_retail                    as returns_retail_ly,
                     tbl_rmd_ty.returns_retail                    as returns_retail_ty,
                     tbl_rmd_ly.weight_variance_retail            as weight_variance_retail_ly,
                     tbl_rmd_ty.weight_variance_retail            as weight_variance_retail_ty,
                     tbl_rmd_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_rmd_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_rmd_ly.franchise_sales_retail            as franchise_sales_retail_ly,
                     tbl_rmd_ty.franchise_sales_retail            as franchise_sales_retail_ty,
                     tbl_rmd_ly.freight_claim_retail              as freight_claim_retail_ly,
                     tbl_rmd_ty.freight_claim_retail              as freight_claim_retail_ty,
                     tbl_rmd_ly.stock_adj_cogs_retail             as stock_adj_cogs_retail_ly,
                     tbl_rmd_ty.stock_adj_cogs_retail             as stock_adj_cogs_retail_ty,
                     tbl_rmd_ly.stock_adj_retail                  as stock_adj_retail_ly,
                     tbl_rmd_ty.stock_adj_retail                  as stock_adj_retail_ty,
                     tbl_rmd_ly.shrinkage_retail                  as shrinkage_retail_ly,
                     tbl_rmd_ty.shrinkage_retail                  as shrinkage_retail_ty,
                     tbl_rmd_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_rmd_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_rmd_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_rmd_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_rmd_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_rmd_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_rmd_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_rmd_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_rmd_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_rmd_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_rmd_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_rmd_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_rmd_ly.cls_stk_retail                    as cls_stk_retail_ly,
                     tbl_rmd_ty.cls_stk_retail                    as cls_stk_retail_ty,
                     NULL                                         as add_retail_ly,-- tbl_rmd_ly.add_retail                       
                     NULL                                         as add_retail_ty,--tbl_rmd_ty.add_retail                       
                     NULL                                         as red_retail_ly,--tbl_rmd_ly.red_retail                       
                     NULL                                         as red_retail_ty,--tbl_rmd_ty.red_retail
                     NULL                                         as add_cost_ly,--tbl_rmd_ly.add_cost
                     NULL                                         as add_cost_ty,--tbl_rmd_ty.add_cost
                     tbl_rmd_ly.htd_gafs_retail                   as htd_gafs_retail_ly,
                     tbl_rmd_ty.htd_gafs_retail                   as htd_gafs_retail_ty,
                     tbl_rmd_ly.htd_gafs_cost                     as htd_gafs_cost_ly,
                     tbl_rmd_ty.htd_gafs_cost                     as htd_gafs_cost_ty,
                     NULL                                         as cum_markon_pct_ly,--tbl_rmd_ly.cum_markon_pct
                     NULL                                         as cum_markon_pct_ty,--tbl_rmd_ty.cum_markon_pct
                     tbl_rmd_ly.gross_margin_amt                  as gross_margin_amt_ly,
                     tbl_rmd_ty.gross_margin_amt                  as gross_margin_amt_ty,
                     tbl_rmd_ly.vat_in                            as vat_in_ly,
                     tbl_rmd_ty.vat_in                            as vat_in_ty,
                     tbl_rmd_ly.vat_out                           as vat_out_ly,
                     tbl_rmd_ty.vat_out                           as vat_out_ty,
                     tbl_rmd_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_rmd_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_rmd_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_rmd_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_rmd_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_rmd_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_rmd_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_rmd_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_rmd_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_rmd_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_rmd_ly.freight_cost                      as freight_cost_ly,
                     tbl_rmd_ty.freight_cost                      as freight_cost_ty,
                     tbl_rmd_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_rmd_ty.workroom_amt                      as workroom_amt_ty,
                     tbl_rmd_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_rmd_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_rmd_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_rmd_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_rmd_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_rmd_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     tbl_rmd_ly.retail_cost_variance              as retail_cost_variance_ly,
                     tbl_rmd_ty.retail_cost_variance              as retail_cost_variance_ty,
                     tbl_rmd_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_rmd_ty.deal_income_purch                 as deal_income_purch_ty,
                     tbl_rmd_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_rmd_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_rmd_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_rmd_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_rmd_ly.intercompany_margin               as intercompany_margin_ly,
                     tbl_rmd_ty.intercompany_margin               as intercompany_margin_ty,
                     tbl_rmd_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_rmd_ty.margin_cost_variance              as margin_cost_variance_ty,
                     NULL                                         as net_markdown_ly,--tbl_rmd_ly.net_markdown
                     NULL                                         as net_markdown_ty,--tbl_rmd_ty.net_markdown
                     NULL                                         as net_markup_ly,--tbl_rmd_ly.net_markup
                     NULL                                         as net_markup_ty,--tbl_rmd_ty.net_markup
                     NULL                                         as net_reclass_retail_ly,--tbl_rmd_ly.net_reclass_retail
                     NULL                                         as net_reclass_retail_ty,--tbl_rmd_ty.net_reclass_retail
                     tbl_rmd_ly.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ly,
                     tbl_rmd_ty.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ty,
                     tbl_rmd_ly.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ly,
                     tbl_rmd_ty.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ty,
                     NULL                                         as net_tsf_retail_ly,--tbl_rmd_ly.net_tsf_retail
                     NULL                                         as net_tsf_retail_ty,--tbl_rmd_ty.net_tsf_retail
                     NULL                                         as net_tsf_cost_ly,--tbl_rmd_ly.net_tsf_cost
                     NULL                                         as net_tsf_cost_ty,--tbl_rmd_ty.net_tsf_cost
                     tbl_rmd_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_rmd_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_rmd_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_rmd_ty.restocking_fee                    as restocking_fee_ty,
                     NULL                                         as shrink_retail_pct_ly,--tbl_rmd_ly.shrink_retail_pct
                     NULL                                         as shrink_retail_pct_ty,--tbl_rmd_ty.shrink_retail_pct
                     tbl_rmd_ly.sales_units                       as sales_units_ly,
                     tbl_rmd_ty.sales_units                       as sales_units_ty,
                     tbl_rmd_ly.net_sales_non_inv_retail          as net_sales_noninv_retail_ly,
                     tbl_rmd_ty.net_sales_non_inv_retail          as net_sales_noninv_retail_ty
                from table(cast(L_tbl_r_month_data_ly as OBJ_MONTH_DATA_TBL)) tbl_rmd_ly 
                full outer join table(cast(L_tbl_r_month_data_ty as OBJ_MONTH_DATA_TBL)) tbl_rmd_ty 
                  ON tbl_rmd_ly.dept         =  tbl_rmd_ty.dept
                     and tbl_rmd_ly.class    =  tbl_rmd_ty.class 
                     and tbl_rmd_ly.subclass =  tbl_rmd_ty.subclass
                     and tbl_rmd_ly.loc_type =  tbl_rmd_ty.loc_type
                     and tbl_rmd_ly.location =  tbl_rmd_ty.location
                     and tbl_rmd_ly.half_no  =  tbl_rmd_ty.half_no
                     and tbl_rmd_ly.month_no =  tbl_rmd_ty.month_no
                     and tbl_rmd_ly.eom_date =  tbl_rmd_ty.eom_date) outer_query;    
BEGIN
   LP_r_month_data_tbl   :=  new OBJ_RETAIL_MONTH_DATA_TBL();
   L_tbl_r_month_data_ty := new  OBJ_MONTH_DATA_TBL();
   L_tbl_r_month_data_ly := new  OBJ_MONTH_DATA_TBL();
   
   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;
   if STKLEDGR_QUERY_SQL.FETCH_MONTH_DATA_BULK(LP_sch_crit_rec.dept,
                                               LP_sch_crit_rec.class,
                                               LP_sch_crit_rec.subclass,
                                               'T',
                                               LP_sch_crit_rec.period_no,
                                               LP_loc_type,
                                               LP_location,
                                               'Y',      --:B_search.CB_thousands
                                               LP_currency_ind,
                                               LP_set_of_books_id,
                                               L_tbl_r_month_data_ty,
                                               O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if STKLEDGR_QUERY_SQL.FETCH_MONTH_DATA_BULK(LP_sch_crit_rec.dept,
                                               LP_sch_crit_rec.class,
                                               LP_sch_crit_rec.subclass,
                                               'L',
                                               LP_sch_crit_rec.period_no,
                                               LP_loc_type,
                                               LP_location,
                                               'Y',      --:B_search.CB_thousands
                                               LP_currency_ind,
                                               LP_set_of_books_id,
                                               L_tbl_r_month_data_ly,
                                               O_error_message) = FALSE then
      return FALSE;
   end if;
   
   open C_RETAIL_MONTH_DATA_INFO;
   fetch C_RETAIL_MONTH_DATA_INFO BULK COLLECT into LP_r_month_data_tbl;
   close C_RETAIL_MONTH_DATA_INFO;
  
   FOR i in 1..LP_r_month_data_tbl.count
   LOOP
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_r_month_data_tbl(i).dept,
                                  LP_r_month_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_r_month_data_tbl(i).dept,
                                   LP_r_month_data_tbl(i).class,
                                   LP_r_month_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_r_month_data_tbl(i).dept,
                                       LP_r_month_data_tbl(i).class,
                                       LP_r_month_data_tbl(i).subclass,
                                       LP_r_month_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;
      
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_r_month_data_tbl(i).loc_name,
                                       LP_r_month_data_tbl(i).location,
                                       LP_r_month_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_r_month_data_tbl(i).loc_type,
                                    LP_r_month_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_r_month_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_r_month_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_r_month_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_r_month_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;      
      -- CALCULATE_FIELDS
      L_net_tsf_cost_ly := LP_r_month_data_tbl(i).tsf_in_cost_ly - LP_r_month_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_r_month_data_tbl(i).tsf_in_cost_ty - LP_r_month_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_r_month_data_tbl(i).tsf_in_retail_ly - LP_r_month_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_r_month_data_tbl(i).tsf_in_retail_ty - LP_r_month_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_r_month_data_tbl(i).markup_retail_ly - LP_r_month_data_tbl(i).markup_can_retail_ly + LP_r_month_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_r_month_data_tbl(i).markup_retail_ty - LP_r_month_data_tbl(i).markup_can_retail_ty + LP_r_month_data_tbl(i).franchise_markup_retail_ty;
                   
      L_add_cost_ly := LP_r_month_data_tbl(i).purch_cost_ly - LP_r_month_data_tbl(i).rtv_cost_ly + LP_r_month_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_r_month_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_r_month_data_tbl(i).purch_cost_ty - LP_r_month_data_tbl(i).rtv_cost_ty + LP_r_month_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_r_month_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_r_month_data_tbl(i).purch_retail_ly - LP_r_month_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_r_month_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_r_month_data_tbl(i).purch_retail_ty - LP_r_month_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_r_month_data_tbl(i).franchise_returns_retail_ty;
      
      L_net_markdown_retail_ly := LP_r_month_data_tbl(i).clear_markdown_retail_ly + LP_r_month_data_tbl(i).perm_markdown_retail_ly + LP_r_month_data_tbl(i).prom_markdown_retail_ly - LP_r_month_data_tbl(i).markdown_can_retail_ly + LP_r_month_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_r_month_data_tbl(i).clear_markdown_retail_ty + LP_r_month_data_tbl(i).perm_markdown_retail_ty + LP_r_month_data_tbl(i).prom_markdown_retail_ty - LP_r_month_data_tbl(i).markdown_can_retail_ty + LP_r_month_data_tbl(i).franchise_markdown_retail_ty;
      
      L_red_retail_ly   := LP_r_month_data_tbl(i).net_sales_retail_ly + LP_r_month_data_tbl(i).shrinkage_retail_ly + L_net_markdown_retail_ly + LP_r_month_data_tbl(i).empl_disc_retail_ly + LP_r_month_data_tbl(i).weight_variance_retail_ly + LP_r_month_data_tbl(i).franchise_markdown_retail_ly + LP_r_month_data_tbl(i).franchise_sales_retail_ly - LP_r_month_data_tbl(i).franchise_returns_retail_ly;
      L_red_retail_ty   := LP_r_month_data_tbl(i).net_sales_retail_ty + LP_r_month_data_tbl(i).shrinkage_retail_ty + L_net_markdown_retail_ty + LP_r_month_data_tbl(i).empl_disc_retail_ty + LP_r_month_data_tbl(i).weight_variance_retail_ty + LP_r_month_data_tbl(i).franchise_markdown_retail_ty + LP_r_month_data_tbl(i).franchise_sales_retail_ty - LP_r_month_data_tbl(i).franchise_returns_retail_ty;
      
      L_net_reclass_retail_ly := LP_r_month_data_tbl(i).reclass_in_retail_ly - LP_r_month_data_tbl(i).reclass_out_retail_ly;
      L_net_reclass_retail_ty := LP_r_month_data_tbl(i).reclass_in_retail_ty - LP_r_month_data_tbl(i).reclass_out_retail_ty;  
      
      if (LP_r_month_data_tbl(i).net_sales_retail_ly is NULL and LP_r_month_data_tbl(i).franchise_sales_retail_ly is NULL and LP_r_month_data_tbl(i).franchise_returns_retail_ly is NULL) 
   	      or (LP_r_month_data_tbl(i).net_sales_retail_ly = 0 and (LP_r_month_data_tbl(i).franchise_sales_retail_ly - LP_r_month_data_tbl(i).franchise_returns_retail_ly) = 0) then
          L_shrink_retail_pct_ly := 0;
      else
          L_shrink_retail_pct_ly := ((LP_r_month_data_tbl(i).shrinkage_retail_ly / (LP_r_month_data_tbl(i).net_sales_retail_ly + LP_r_month_data_tbl(i).franchise_sales_retail_ly - LP_r_month_data_tbl(i).franchise_returns_retail_ly))*100);
      end if;
     
      if (LP_r_month_data_tbl(i).net_sales_retail_ty is NULL and LP_r_month_data_tbl(i).franchise_sales_retail_ty is NULL and LP_r_month_data_tbl(i).franchise_returns_retail_ty is NULL) 
   	      or (LP_r_month_data_tbl(i).net_sales_retail_ty = 0 and (LP_r_month_data_tbl(i).franchise_sales_retail_ty - LP_r_month_data_tbl(i).franchise_returns_retail_ty) = 0) then
          L_shrink_retail_pct_ty := 0;
      else
          L_shrink_retail_pct_ty := ((LP_r_month_data_tbl(i).shrinkage_retail_ty / (LP_r_month_data_tbl(i).net_sales_retail_ty + LP_r_month_data_tbl(i).franchise_sales_retail_ty - LP_r_month_data_tbl(i).franchise_returns_retail_ty))*100);
      end if;  
      
      if LP_r_month_data_tbl(i).htd_gafs_retail_ly is NULL or LP_r_month_data_tbl(i).htd_gafs_retail_ly = 0 then
         L_cum_markon_pct_ly := 0;
      else
         L_cum_markon_pct_ly := ((1 - (LP_r_month_data_tbl(i).htd_gafs_cost_ly / LP_r_month_data_tbl(i).htd_gafs_retail_ly)) * 100);
      end if;

      if LP_r_month_data_tbl(i).htd_gafs_retail_ty is NULL or LP_r_month_data_tbl(i).htd_gafs_retail_ty = 0 then
         L_cum_markon_pct_ty := 0;
      else
         L_cum_markon_pct_ty := ((1 - (LP_r_month_data_tbl(i).htd_gafs_cost_ty / LP_r_month_data_tbl(i).htd_gafs_retail_ty)) * 100);
      end if; 
      
      LP_r_month_data_tbl(i).add_retail_ly              := L_add_retail_ly;
      LP_r_month_data_tbl(i).add_retail_ty              := L_add_retail_ty;
      LP_r_month_data_tbl(i).red_retail_ly              := L_red_retail_ly;
      LP_r_month_data_tbl(i).red_retail_ty              := L_red_retail_ty;
      LP_r_month_data_tbl(i).add_cost_ly                := L_add_cost_ly;
      LP_r_month_data_tbl(i).add_cost_ty                := L_add_cost_ty;
      LP_r_month_data_tbl(i).cum_markon_pct_ly          := L_cum_markon_pct_ly;
      LP_r_month_data_tbl(i).cum_markon_pct_ty          := L_cum_markon_pct_ty;
      LP_r_month_data_tbl(i).net_markdown_ly            := L_net_markdown_retail_ly;
      LP_r_month_data_tbl(i).net_markdown_ty            := L_net_markdown_retail_ty;
      LP_r_month_data_tbl(i).net_markup_ly              := L_net_markup_retail_ly;
      LP_r_month_data_tbl(i).net_markup_ty              := L_net_markup_retail_ty;
      LP_r_month_data_tbl(i).net_reclass_retail_ly      := L_net_reclass_retail_ly;
      LP_r_month_data_tbl(i).net_reclass_retail_ty      := L_net_reclass_retail_ty;
      LP_r_month_data_tbl(i).net_tsf_retail_ly          := L_net_tsf_retail_ly;
      LP_r_month_data_tbl(i).net_tsf_retail_ty          := L_net_tsf_retail_ty;
      LP_r_month_data_tbl(i).net_tsf_cost_ly            := L_net_tsf_cost_ly;
      LP_r_month_data_tbl(i).net_tsf_cost_ty            := L_net_tsf_cost_ty;
      LP_r_month_data_tbl(i).shrink_retail_pct_ly       := L_shrink_retail_pct_ly;
      LP_r_month_data_tbl(i).shrink_retail_pct_ty       := L_shrink_retail_pct_ty;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_RETAIL_MONTH_DATA_INFO%ISOPEN then
         close C_RETAIL_MONTH_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_R_MONTH_DATA;
--------------------------------------------------------------------------------
FUNCTION FETCH_R_WEEK_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   
   L_program                VARCHAR2(75) := 'CORESVC_STKLDGRV.FETCH_R_WEEK_DATA';
   L_tbl_r_week_data_ty     OBJ_WEEK_DATA_TBL;
   L_tbl_r_week_data_ly     OBJ_WEEK_DATA_TBL;
   L_add_cost_ly            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;   
   L_net_tsf_cost_ly        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   MONTH_DATA.TSF_IN_COST%TYPE;
   L_shrink_retail_pct_ly   NUMBER(12,4);
   L_shrink_retail_pct_ty   NUMBER(12,4);
   L_red_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_red_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ly  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ty  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty MONTH_DATA.TSF_IN_COST%TYPE;
   L_cum_markon_pct_ly      NUMBER(12,4);
   L_cum_markon_pct_ty      NUMBER(12,4);
   
   cursor C_RETAIL_WEEK_DATA_INFO is
      select OBJ_RETAIL_WEEK_DATA_REC(outer_query.dept,
                                       outer_query.dept_name,
                                       outer_query.class,
                                       outer_query.class_name,
                                       outer_query.subclass,
                                       outer_query.subclass_name,
                                       outer_query.loc_type,
                                       outer_query.location,
                                       outer_query.loc_name,
                                       outer_query.currency_code,
                                       outer_query.eom_date,
                                       outer_query.week_no,
                                       outer_query.opn_stk_retail_ly,
                                       outer_query.opn_stk_retail_ty,
                                       outer_query.purch_retail_ly,
                                       outer_query.purch_retail_ty,
                                       outer_query.purch_cost_ly,
                                       outer_query.purch_cost_ty,
                                       outer_query.tsf_in_cost_ly,
                                       outer_query.tsf_in_cost_ty,
                                       outer_query.tsf_in_retail_ly,
                                       outer_query.tsf_in_retail_ty,
                                       outer_query.tsf_in_book_retail_ly,
                                       outer_query.tsf_in_book_retail_ty,
                                       outer_query.intercompany_in_retail_ly,
                                       outer_query.intercompany_in_retail_ty,
                                       outer_query.reclass_in_retail_ly,
                                       outer_query.reclass_in_retail_ty,
                                       outer_query.franchise_returns_retail_ly,
                                       outer_query.franchise_returns_retail_ty,
                                       outer_query.markup_retail_ly,
                                       outer_query.markup_retail_ty,
                                       outer_query.franchise_markup_retail_ly,
                                       outer_query.franchise_markup_retail_ty,
                                       outer_query.intercompany_markup_ly,
                                       outer_query.intercompany_markup_ty,
                                       outer_query.markup_can_retail_ly,
                                       outer_query.markup_can_retail_ty,
                                       outer_query.rtv_retail_ly,
                                       outer_query.rtv_retail_ty,
                                       outer_query.rtv_cost_ly,
                                       outer_query.rtv_cost_ty,
                                       outer_query.tsf_out_cost_ly,
                                       outer_query.tsf_out_cost_ty,
                                       outer_query.tsf_out_retail_ly,
                                       outer_query.tsf_out_retail_ty,
                                       outer_query.tsf_out_book_retail_ly,
                                       outer_query.tsf_out_book_retail_ty,
                                       outer_query.intercompany_out_retail_ly,
                                       outer_query.intercompany_out_retail_ty,
                                       outer_query.reclass_out_retail_ly,
                                       outer_query.reclass_out_retail_ty,
                                       outer_query.net_sales_retail_ly,
                                       outer_query.net_sales_retail_ty,
                                       outer_query.returns_retail_ly,
                                       outer_query.returns_retail_ty,
                                       outer_query.weight_variance_retail_ly,
                                       outer_query.weight_variance_retail_ty,
                                       outer_query.empl_disc_retail_ly,
                                       outer_query.empl_disc_retail_ty,
                                       outer_query.franchise_sales_retail_ly,
                                       outer_query.franchise_sales_retail_ty,
                                       outer_query.freight_claim_retail_ly,
                                       outer_query.freight_claim_retail_ty,
                                       outer_query.stock_adj_cogs_retail_ly,
                                       outer_query.stock_adj_cogs_retail_ty,
                                       outer_query.stock_adj_retail_ly,
                                       outer_query.stock_adj_retail_ty,
                                       outer_query.shrinkage_retail_ly,
                                       outer_query.shrinkage_retail_ty,
                                       outer_query.perm_markdown_retail_ly,
                                       outer_query.perm_markdown_retail_ty,
                                       outer_query.prom_markdown_retail_ly,
                                       outer_query.prom_markdown_retail_ty,
                                       outer_query.clear_markdown_retail_ly,
                                       outer_query.clear_markdown_retail_ty,
                                       outer_query.intercompany_markdown_ly,
                                       outer_query.intercompany_markdown_ty,
                                       outer_query.franchise_markdown_retail_ly,
                                       outer_query.franchise_markdown_retail_ty,
                                       outer_query.markdown_can_retail_ly,
                                       outer_query.markdown_can_retail_ty,
                                       outer_query.cls_stk_retail_ly,
                                       outer_query.cls_stk_retail_ty,
                                       outer_query.add_retail_ly,
                                       outer_query.add_retail_ty,
                                       outer_query.red_retail_ly,
                                       outer_query.red_retail_ty,
                                       outer_query.add_cost_ly,
                                       outer_query.add_cost_ty,
                                       outer_query.htd_gafs_retail_ly,
                                       outer_query.htd_gafs_retail_ty,
                                       outer_query.htd_gafs_cost_ly,
                                       outer_query.htd_gafs_cost_ty,
                                       outer_query.cum_markon_pct_ly,
                                       outer_query.cum_markon_pct_ty,
                                       outer_query.gross_margin_amt_ly,
                                       outer_query.gross_margin_amt_ty,
                                       outer_query.vat_in_ly,
                                       outer_query.vat_in_ty,
                                       outer_query.vat_out_ly,
                                       outer_query.vat_out_ty,
                                       outer_query.franchise_returns_cost_ly,
                                       outer_query.franchise_returns_cost_ty,
                                       outer_query.up_chrg_amt_profit_ly,
                                       outer_query.up_chrg_amt_profit_ty,
                                       outer_query.up_chrg_amt_exp_ly,
                                       outer_query.up_chrg_amt_exp_ty,
                                       outer_query.wo_activity_upd_inv_ly,
                                       outer_query.wo_activity_upd_inv_ty,
                                       outer_query.wo_activity_post_fin_ly,
                                       outer_query.wo_activity_post_fin_ty,
                                       outer_query.freight_cost_ly,
                                       outer_query.freight_cost_ty,
                                       outer_query.workroom_amt_ly,
                                       outer_query.workroom_amt_ty,
                                       outer_query.cash_disc_amt_ly,
                                       outer_query.cash_disc_amt_ty,
                                       outer_query.cost_variance_amt_ly,
                                       outer_query.cost_variance_amt_ty,
                                       outer_query.rec_cost_adj_variance_ly,
                                       outer_query.rec_cost_adj_variance_ty,
                                       outer_query.retail_cost_variance_ly,
                                       outer_query.retail_cost_variance_ty,
                                       outer_query.deal_income_purch_ly,
                                       outer_query.deal_income_purch_ty,
                                       outer_query.deal_income_sales_ly,
                                       outer_query.deal_income_sales_ty,
                                       outer_query.franchise_restocking_fee_ly,
                                       outer_query.franchise_restocking_fee_ty,
                                       outer_query.intercompany_margin_ly,
                                       outer_query.intercompany_margin_ty,
                                       outer_query.margin_cost_variance_ly,
                                       outer_query.margin_cost_variance_ty,
                                       outer_query.net_markdown_ly,
                                       outer_query.net_markdown_ty,
                                       outer_query.net_markup_ly,
                                       outer_query.net_markup_ty,
                                       outer_query.net_reclass_retail_ly,
                                       outer_query.net_reclass_retail_ty,
                                       outer_query.net_sales_retail_ex_vat_ly,
                                       outer_query.net_sales_retail_ex_vat_ty,
                                       outer_query.net_sale_noninv_r_exvat_ly,
                                       outer_query.net_sale_noninv_r_exvat_ly,
                                       outer_query.net_tsf_retail_ly,
                                       outer_query.net_tsf_retail_ty,
                                       outer_query.net_tsf_cost_ly,
                                       outer_query.net_tsf_cost_ty,
                                       outer_query.recoverable_tax_ly,
                                       outer_query.recoverable_tax_ty,
                                       outer_query.restocking_fee_ly,
                                       outer_query.restocking_fee_ty,
                                       outer_query.shrink_retail_pct_ly,
                                       outer_query.shrink_retail_pct_ty,
                                       outer_query.sales_units_ly,
                                       outer_query.sales_units_ty,
                                       outer_query.net_sales_noninv_retail_ly,
                                       outer_query.net_sales_noninv_retail_ty)
        from (select NVL(tbl_rwd_ly.dept,tbl_rwd_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_rwd_ly.class,tbl_rwd_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_rwd_ly.subclass,tbl_rwd_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_rwd_ly.loc_type,tbl_rwd_ty.loc_type) as loc_type,
                     NVL(tbl_rwd_ly.location,tbl_rwd_ty.location) as location,
                     NULL                                         as loc_name, --loc_name
                     NULL                                         as currency_code,
                     NULL                                         as eom_date,
                     NVL(tbl_rwd_ly.week_no,tbl_rwd_ty.week_no)   as week_no,
                     tbl_rwd_ly.opn_stk_retail                    as opn_stk_retail_ly,
                     tbl_rwd_ty.opn_stk_retail                    as opn_stk_retail_ty,
                     tbl_rwd_ly.purch_retail                      as purch_retail_ly,
                     tbl_rwd_ty.purch_retail                      as purch_retail_ty,
                     tbl_rwd_ly.purch_cost                        as purch_cost_ly,
                     tbl_rwd_ty.purch_cost                        as purch_cost_ty,
                     tbl_rwd_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_rwd_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_rwd_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_rwd_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_rwd_ly.tsf_in_book_retail                as tsf_in_book_retail_ly,
                     tbl_rwd_ty.tsf_in_book_retail                as tsf_in_book_retail_ty,
                     tbl_rwd_ly.intercompany_in_retail            as intercompany_in_retail_ly,
                     tbl_rwd_ty.intercompany_in_retail            as intercompany_in_retail_ty,
                     tbl_rwd_ly.reclass_in_retail                 as reclass_in_retail_ly,
                     tbl_rwd_ty.reclass_in_retail                 as reclass_in_retail_ty,
                     tbl_rwd_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_rwd_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_rwd_ly.markup_retail                     as markup_retail_ly,
                     tbl_rwd_ty.markup_retail                     as markup_retail_ty,
                     tbl_rwd_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_rwd_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_rwd_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_rwd_ty.intercompany_markup               as intercompany_markup_ty,
                     tbl_rwd_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_rwd_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_rwd_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_rwd_ty.rtv_retail                        as rtv_retail_ty,
                     tbl_rwd_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_rwd_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_rwd_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_rwd_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_rwd_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_rwd_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_rwd_ly.tsf_out_book_retail               as tsf_out_book_retail_ly,
                     tbl_rwd_ty.tsf_out_book_retail               as tsf_out_book_retail_ty,
                     tbl_rwd_ly.intercompany_out_retail           as intercompany_out_retail_ly,
                     tbl_rwd_ty.intercompany_out_retail           as intercompany_out_retail_ty,
                     tbl_rwd_ly.reclass_out_retail                as reclass_out_retail_ly,
                     tbl_rwd_ty.reclass_out_retail                as reclass_out_retail_ty,
                     tbl_rwd_ly.net_sales_retail                  as net_sales_retail_ly,
                     tbl_rwd_ty.net_sales_retail                  as net_sales_retail_ty,
                     tbl_rwd_ly.returns_retail                    as returns_retail_ly,
                     tbl_rwd_ty.returns_retail                    as returns_retail_ty,
                     tbl_rwd_ly.weight_variance_retail            as weight_variance_retail_ly,
                     tbl_rwd_ty.weight_variance_retail            as weight_variance_retail_ty,
                     tbl_rwd_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_rwd_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_rwd_ly.franchise_sales_retail            as franchise_sales_retail_ly,
                     tbl_rwd_ty.franchise_sales_retail            as franchise_sales_retail_ty,
                     tbl_rwd_ly.freight_claim_retail              as freight_claim_retail_ly,
                     tbl_rwd_ty.freight_claim_retail              as freight_claim_retail_ty,
                     tbl_rwd_ly.stock_adj_cogs_retail             as stock_adj_cogs_retail_ly,
                     tbl_rwd_ty.stock_adj_cogs_retail             as stock_adj_cogs_retail_ty,
                     tbl_rwd_ly.stock_adj_retail                  as stock_adj_retail_ly,
                     tbl_rwd_ty.stock_adj_retail                  as stock_adj_retail_ty,
                     tbl_rwd_ly.shrinkage_retail                  as shrinkage_retail_ly,
                     tbl_rwd_ty.shrinkage_retail                  as shrinkage_retail_ty,
                     tbl_rwd_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_rwd_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_rwd_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_rwd_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_rwd_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_rwd_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_rwd_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_rwd_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_rwd_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_rwd_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_rwd_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_rwd_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_rwd_ly.cls_stk_retail                    as cls_stk_retail_ly,
                     tbl_rwd_ty.cls_stk_retail                    as cls_stk_retail_ty,
                     NULL                                         as add_retail_ly,-- tbl_rwd_ly.add_retail                       
                     NULL                                         as add_retail_ty,--tbl_rwd_ty.add_retail                       
                     NULL                                         as red_retail_ly,--tbl_rwd_ly.red_retail                       
                     NULL                                         as red_retail_ty,--tbl_rwd_ty.red_retail
                     NULL                                         as add_cost_ly,--tbl_rwd_ly.add_cost
                     NULL                                         as add_cost_ty,--tbl_rwd_ty.add_cost
                     tbl_rwd_ly.htd_gafs_retail                   as htd_gafs_retail_ly,
                     tbl_rwd_ty.htd_gafs_retail                   as htd_gafs_retail_ty,
                     tbl_rwd_ly.htd_gafs_cost                     as htd_gafs_cost_ly,
                     tbl_rwd_ty.htd_gafs_cost                     as htd_gafs_cost_ty,
                     NULL                                         as cum_markon_pct_ly,--tbl_rwd_ly.cum_markon_pct
                     NULL                                         as cum_markon_pct_ty,--tbl_rwd_ty.cum_markon_pct
                     tbl_rwd_ly.gross_margin_amt                  as gross_margin_amt_ly,
                     tbl_rwd_ty.gross_margin_amt                  as gross_margin_amt_ty,
                     tbl_rwd_ly.vat_in                            as vat_in_ly,
                     tbl_rwd_ty.vat_in                            as vat_in_ty,
                     tbl_rwd_ly.vat_out                           as vat_out_ly,
                     tbl_rwd_ty.vat_out                           as vat_out_ty,
                     tbl_rwd_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_rwd_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_rwd_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_rwd_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_rwd_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_rwd_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_rwd_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_rwd_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_rwd_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_rwd_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_rwd_ly.freight_cost                      as freight_cost_ly,
                     tbl_rwd_ty.freight_cost                      as freight_cost_ty,
                     tbl_rwd_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_rwd_ty.workroom_amt                      as workroom_amt_ty,
                     tbl_rwd_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_rwd_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_rwd_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_rwd_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_rwd_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_rwd_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     tbl_rwd_ly.retail_cost_variance              as retail_cost_variance_ly,
                     tbl_rwd_ty.retail_cost_variance              as retail_cost_variance_ty,
                     tbl_rwd_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_rwd_ty.deal_income_purch                 as deal_income_purch_ty,
                     tbl_rwd_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_rwd_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_rwd_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_rwd_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_rwd_ly.intercompany_margin               as intercompany_margin_ly,
                     tbl_rwd_ty.intercompany_margin               as intercompany_margin_ty,
                     tbl_rwd_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_rwd_ty.margin_cost_variance              as margin_cost_variance_ty,
                     NULL                                         as net_markdown_ly,--tbl_rwd_ly.net_markdown
                     NULL                                         as net_markdown_ty,--tbl_rwd_ty.net_markdown
                     NULL                                         as net_markup_ly,--tbl_rwd_ly.net_markup
                     NULL                                         as net_markup_ty,--tbl_rwd_ty.net_markup
                     NULL                                         as net_reclass_retail_ly,--tbl_rwd_ly.net_reclass_retail
                     NULL                                         as net_reclass_retail_ty,--tbl_rwd_ty.net_reclass_retail
                     tbl_rwd_ly.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ly,
                     tbl_rwd_ty.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ty,
                     tbl_rwd_ly.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ly,
                     tbl_rwd_ty.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ty,
                     NULL                                         as net_tsf_retail_ly,--tbl_rwd_ly.net_tsf_retail
                     NULL                                         as net_tsf_retail_ty,--tbl_rwd_ty.net_tsf_retail
                     NULL                                         as net_tsf_cost_ly,--tbl_rwd_ly.net_tsf_cost
                     NULL                                         as net_tsf_cost_ty,--tbl_rwd_ty.net_tsf_cost
                     tbl_rwd_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_rwd_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_rwd_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_rwd_ty.restocking_fee                    as restocking_fee_ty,
                     NULL                                         as shrink_retail_pct_ly,--tbl_rwd_ly.shrink_retail_pct
                     NULL                                         as shrink_retail_pct_ty,--tbl_rwd_ty.shrink_retail_pct
                     tbl_rwd_ly.sales_units                       as sales_units_ly,
                     tbl_rwd_ty.sales_units                       as sales_units_ty,
                     tbl_rwd_ly.net_sales_non_inv_retail          as net_sales_noninv_retail_ly,
                     tbl_rwd_ty.net_sales_non_inv_retail          as net_sales_noninv_retail_ty
                from table(cast(L_tbl_r_week_data_ly as OBJ_WEEK_DATA_TBL)) tbl_rwd_ly 
                full outer join table(cast(L_tbl_r_week_data_ty as OBJ_WEEK_DATA_TBL)) tbl_rwd_ty 
                  ON tbl_rwd_ly.dept         =  tbl_rwd_ty.dept
                     and tbl_rwd_ly.class    =  tbl_rwd_ty.class 
                     and tbl_rwd_ly.subclass =  tbl_rwd_ty.subclass
                     and tbl_rwd_ly.loc_type =  tbl_rwd_ty.loc_type
                     and tbl_rwd_ly.location =  tbl_rwd_ty.location
                     and tbl_rwd_ly.half_no  =  tbl_rwd_ty.half_no
                     and tbl_rwd_ly.week_no  =  tbl_rwd_ty.week_no
                     and tbl_rwd_ly.eow_date =  tbl_rwd_ty.eow_date) outer_query;    
BEGIN
   LP_r_week_data_tbl   := new OBJ_RETAIL_WEEK_DATA_TBL();
   L_tbl_r_week_data_ty := new OBJ_WEEK_DATA_TBL();
   L_tbl_r_week_data_ly := new OBJ_WEEK_DATA_TBL();
   
   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;
   if STKLEDGR_QUERY_SQL.FETCH_WEEK_DATA_BULK(LP_sch_crit_rec.dept,
                                              LP_sch_crit_rec.class,
                                              LP_sch_crit_rec.subclass,
                                              'T',
                                              LP_sch_crit_rec.period_date,
                                              LP_loc_type,
                                              LP_location,
                                              'Y', --:B_search.CB_thousands,
                                              LP_currency_ind,
                                              LP_set_of_books_id,
                                              L_tbl_r_week_data_ty,
                                              O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if STKLEDGR_QUERY_SQL.FETCH_WEEK_DATA_BULK(LP_sch_crit_rec.dept,
                                              LP_sch_crit_rec.class,
                                              LP_sch_crit_rec.subclass,
                                              'L',
                                              LP_sch_crit_rec.period_date,
                                              LP_loc_type,
                                              LP_location,
                                              'Y', --:B_search.CB_thousands,
                                              LP_currency_ind,
                                              LP_set_of_books_id,
                                              L_tbl_r_week_data_ly,
                                              O_error_message) = FALSE then
      return FALSE;
   end if;
   
   open C_RETAIL_WEEK_DATA_INFO;
   fetch C_RETAIL_WEEK_DATA_INFO BULK COLLECT into LP_r_week_data_tbl;
   close C_RETAIL_WEEK_DATA_INFO;
  
   FOR i in 1..LP_r_week_data_tbl.count
   LOOP
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_r_week_data_tbl(i).dept,
                                  LP_r_week_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_r_week_data_tbl(i).dept,
                                   LP_r_week_data_tbl(i).class,
                                   LP_r_week_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_r_week_data_tbl(i).dept,
                                       LP_r_week_data_tbl(i).class,
                                       LP_r_week_data_tbl(i).subclass,
                                       LP_r_week_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;
      
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_r_week_data_tbl(i).loc_name,
                                       LP_r_week_data_tbl(i).location,
                                       LP_r_week_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_r_week_data_tbl(i).loc_type,
                                    LP_r_week_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_r_week_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_r_week_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_r_week_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_r_week_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;      
      
      LP_r_week_data_tbl(i).eom_date := LP_sch_crit_rec.period_date;
      -- CALCULATE_FIELDS
      L_net_tsf_cost_ly := LP_r_week_data_tbl(i).tsf_in_cost_ly - LP_r_week_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_r_week_data_tbl(i).tsf_in_cost_ty - LP_r_week_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_r_week_data_tbl(i).tsf_in_retail_ly - LP_r_week_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_r_week_data_tbl(i).tsf_in_retail_ty - LP_r_week_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_r_week_data_tbl(i).markup_retail_ly - LP_r_week_data_tbl(i).markup_can_retail_ly + LP_r_week_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_r_week_data_tbl(i).markup_retail_ty - LP_r_week_data_tbl(i).markup_can_retail_ty + LP_r_week_data_tbl(i).franchise_markup_retail_ty;
                   
      L_add_cost_ly := LP_r_week_data_tbl(i).purch_cost_ly - LP_r_week_data_tbl(i).rtv_cost_ly + LP_r_week_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_r_week_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_r_week_data_tbl(i).purch_cost_ty - LP_r_week_data_tbl(i).rtv_cost_ty + LP_r_week_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_r_week_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_r_week_data_tbl(i).purch_retail_ly - LP_r_week_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_r_week_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_r_week_data_tbl(i).purch_retail_ty - LP_r_week_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_r_week_data_tbl(i).franchise_returns_retail_ty;
      
      L_net_markdown_retail_ly := LP_r_week_data_tbl(i).clear_markdown_retail_ly + LP_r_week_data_tbl(i).perm_markdown_retail_ly + LP_r_week_data_tbl(i).prom_markdown_retail_ly - LP_r_week_data_tbl(i).markdown_can_retail_ly + LP_r_week_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_r_week_data_tbl(i).clear_markdown_retail_ty + LP_r_week_data_tbl(i).perm_markdown_retail_ty + LP_r_week_data_tbl(i).prom_markdown_retail_ty - LP_r_week_data_tbl(i).markdown_can_retail_ty + LP_r_week_data_tbl(i).franchise_markdown_retail_ty;
      
      L_red_retail_ly   := LP_r_week_data_tbl(i).net_sales_retail_ly + LP_r_week_data_tbl(i).shrinkage_retail_ly + L_net_markdown_retail_ly + LP_r_week_data_tbl(i).empl_disc_retail_ly + LP_r_week_data_tbl(i).weight_variance_retail_ly + LP_r_week_data_tbl(i).franchise_markdown_retail_ly + LP_r_week_data_tbl(i).franchise_sales_retail_ly - LP_r_week_data_tbl(i).franchise_returns_retail_ly;
      L_red_retail_ty   := LP_r_week_data_tbl(i).net_sales_retail_ty + LP_r_week_data_tbl(i).shrinkage_retail_ty + L_net_markdown_retail_ty + LP_r_week_data_tbl(i).empl_disc_retail_ty + LP_r_week_data_tbl(i).weight_variance_retail_ty + LP_r_week_data_tbl(i).franchise_markdown_retail_ty + LP_r_week_data_tbl(i).franchise_sales_retail_ty - LP_r_week_data_tbl(i).franchise_returns_retail_ty;
      
      L_net_reclass_retail_ly := LP_r_week_data_tbl(i).reclass_in_retail_ly - LP_r_week_data_tbl(i).reclass_out_retail_ly;
      L_net_reclass_retail_ty := LP_r_week_data_tbl(i).reclass_in_retail_ty - LP_r_week_data_tbl(i).reclass_out_retail_ty;  
      
      if (LP_r_week_data_tbl(i).net_sales_retail_ly is NULL and LP_r_week_data_tbl(i).franchise_sales_retail_ly is NULL and LP_r_week_data_tbl(i).franchise_returns_retail_ly is NULL) 
   	    or (LP_r_week_data_tbl(i).net_sales_retail_ly = 0 and (LP_r_week_data_tbl(i).franchise_sales_retail_ly - LP_r_week_data_tbl(i).franchise_returns_retail_ly) = 0) then
          L_shrink_retail_pct_ly := 0;
      else
          L_shrink_retail_pct_ly := ((LP_r_week_data_tbl(i).shrinkage_retail_ly / (LP_r_week_data_tbl(i).net_sales_retail_ly + LP_r_week_data_tbl(i).franchise_sales_retail_ly - LP_r_week_data_tbl(i).franchise_returns_retail_ly))*100);
      end if;
     
      if (LP_r_week_data_tbl(i).net_sales_retail_ty is NULL and LP_r_week_data_tbl(i).franchise_sales_retail_ty is NULL and LP_r_week_data_tbl(i).franchise_returns_retail_ty is NULL) 
   	    or (LP_r_week_data_tbl(i).net_sales_retail_ty = 0 and (LP_r_week_data_tbl(i).franchise_sales_retail_ty - LP_r_week_data_tbl(i).franchise_returns_retail_ty) = 0) then
          L_shrink_retail_pct_ty := 0;
      else
          L_shrink_retail_pct_ty := ((LP_r_week_data_tbl(i).shrinkage_retail_ty / (LP_r_week_data_tbl(i).net_sales_retail_ty + LP_r_week_data_tbl(i).franchise_sales_retail_ty - LP_r_week_data_tbl(i).franchise_returns_retail_ty))*100);
      end if;  
      
      if LP_r_week_data_tbl(i).htd_gafs_retail_ly is NULL or LP_r_week_data_tbl(i).htd_gafs_retail_ly = 0 then
         L_cum_markon_pct_ly := 0;
      else
         L_cum_markon_pct_ly := ((1 - (LP_r_week_data_tbl(i).htd_gafs_cost_ly / LP_r_week_data_tbl(i).htd_gafs_retail_ly)) * 100);
      end if;

      if LP_r_week_data_tbl(i).htd_gafs_retail_ty is NULL or LP_r_week_data_tbl(i).htd_gafs_retail_ty = 0 then
         L_cum_markon_pct_ty := 0;
      else
         L_cum_markon_pct_ty := ((1 - (LP_r_week_data_tbl(i).htd_gafs_cost_ty / LP_r_week_data_tbl(i).htd_gafs_retail_ty)) * 100);
      end if; 
      
      LP_r_week_data_tbl(i).add_retail_ly              := L_add_retail_ly;
      LP_r_week_data_tbl(i).add_retail_ty              := L_add_retail_ty;
      LP_r_week_data_tbl(i).red_retail_ly              := L_red_retail_ly;
      LP_r_week_data_tbl(i).red_retail_ty              := L_red_retail_ty;
      LP_r_week_data_tbl(i).add_cost_ly                := L_add_cost_ly;
      LP_r_week_data_tbl(i).add_cost_ty                := L_add_cost_ty;
      LP_r_week_data_tbl(i).cum_markon_pct_ly          := L_cum_markon_pct_ly;
      LP_r_week_data_tbl(i).cum_markon_pct_ty          := L_cum_markon_pct_ty;
      LP_r_week_data_tbl(i).net_markdown_ly            := L_net_markdown_retail_ly;
      LP_r_week_data_tbl(i).net_markdown_ty            := L_net_markdown_retail_ty;
      LP_r_week_data_tbl(i).net_markup_ly              := L_net_markup_retail_ly;
      LP_r_week_data_tbl(i).net_markup_ty              := L_net_markup_retail_ty;
      LP_r_week_data_tbl(i).net_reclass_retail_ly      := L_net_reclass_retail_ly;
      LP_r_week_data_tbl(i).net_reclass_retail_ty      := L_net_reclass_retail_ty;
      LP_r_week_data_tbl(i).net_tsf_retail_ly          := L_net_tsf_retail_ly;
      LP_r_week_data_tbl(i).net_tsf_retail_ty          := L_net_tsf_retail_ty;
      LP_r_week_data_tbl(i).net_tsf_cost_ly            := L_net_tsf_cost_ly;
      LP_r_week_data_tbl(i).net_tsf_cost_ty            := L_net_tsf_cost_ty;
      LP_r_week_data_tbl(i).shrink_retail_pct_ly       := L_shrink_retail_pct_ly;
      LP_r_week_data_tbl(i).shrink_retail_pct_ty       := L_shrink_retail_pct_ty;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_RETAIL_WEEK_DATA_INFO%ISOPEN then
         close C_RETAIL_WEEK_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_R_WEEK_DATA;
--------------------------------------------------------------------------------
FUNCTION FETCH_R_DAILY_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   
   L_program                VARCHAR2(75) := 'CORESVC_STKLDGRV.FETCH_R_DAILY_DATA';
   L_tbl_r_daily_data_ty    OBJ_DAILY_DATA_TBL;
   L_tbl_r_daily_data_ly    OBJ_DAILY_DATA_TBL;
   L_add_cost_ly            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;   
   L_net_tsf_cost_ly        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   MONTH_DATA.TSF_IN_COST%TYPE;
   L_shrink_retail_pct_ly   NUMBER(12,4);
   L_shrink_retail_pct_ty   NUMBER(12,4);
   L_net_reclass_retail_ly  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ty  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty MONTH_DATA.TSF_IN_COST%TYPE;
   L_cum_markon_pct_ly      NUMBER(12,4);
   L_cum_markon_pct_ty      NUMBER(12,4);
   
   cursor C_RETAIL_DAILY_DATA_INFO is
      select OBJ_RETAIL_DAILY_DATA_REC(outer_query.dept,                               
                                       outer_query.dept_name,
                                       outer_query.class,
                                       outer_query.class_name,
                                       outer_query.subclass,
                                       outer_query.subclass_name,
                                       outer_query.loc_type,
                                       outer_query.location,
                                       outer_query.loc_name,
                                       outer_query.currency_code,
                                       outer_query.eow_date,
                                       outer_query.day_no,
                                       outer_query.purch_retail_ly,
                                       outer_query.purch_retail_ty,
                                       outer_query.purch_cost_ly,
                                       outer_query.purch_cost_ty,
                                       outer_query.tsf_in_cost_ly,
                                       outer_query.tsf_in_cost_ty,
                                       outer_query.tsf_in_retail_ly,
                                       outer_query.tsf_in_retail_ty,
                                       outer_query.tsf_in_book_retail_ly,
                                       outer_query.tsf_in_book_retail_ty,
                                       outer_query.intercompany_in_retail_ly,
                                       outer_query.intercompany_in_retail_ty,
                                       outer_query.reclass_in_retail_ly,
                                       outer_query.reclass_in_retail_ty,
                                       outer_query.franchise_returns_retail_ly,
                                       outer_query.franchise_returns_retail_ty,
                                       outer_query.markup_retail_ly,
                                       outer_query.markup_retail_ty,
                                       outer_query.franchise_markup_retail_ly,
                                       outer_query.franchise_markup_retail_ty,
                                       outer_query.intercompany_markup_ly,
                                       outer_query.intercompany_markup_ty,
                                       outer_query.markup_can_retail_ly,
                                       outer_query.markup_can_retail_ty,
                                       outer_query.rtv_retail_ly,
                                       outer_query.rtv_retail_ty,
                                       outer_query.rtv_cost_ly,
                                       outer_query.rtv_cost_ty,
                                       outer_query.tsf_out_cost_ly,
                                       outer_query.tsf_out_cost_ty,
                                       outer_query.tsf_out_retail_ly,
                                       outer_query.tsf_out_retail_ty,
                                       outer_query.tsf_out_book_retail_ly,
                                       outer_query.tsf_out_book_retail_ty,
                                       outer_query.intercompany_out_retail_ly,
                                       outer_query.intercompany_out_retail_ty,
                                       outer_query.reclass_out_retail_ly,
                                       outer_query.reclass_out_retail_ty,
                                       outer_query.net_sales_retail_ly,
                                       outer_query.net_sales_retail_ty,
                                       outer_query.returns_retail_ly,
                                       outer_query.returns_retail_ty,
                                       outer_query.weight_variance_retail_ly,
                                       outer_query.weight_variance_retail_ty,
                                       outer_query.empl_disc_retail_ly,
                                       outer_query.empl_disc_retail_ty,
                                       outer_query.franchise_sales_retail_ly,
                                       outer_query.franchise_sales_retail_ty,
                                       outer_query.freight_claim_retail_ly,
                                       outer_query.freight_claim_retail_ty,
                                       outer_query.stock_adj_cogs_retail_ly,
                                       outer_query.stock_adj_cogs_retail_ty,
                                       outer_query.stock_adj_retail_ly,
                                       outer_query.stock_adj_retail_ty,
                                       outer_query.perm_markdown_retail_ly,
                                       outer_query.perm_markdown_retail_ty,
                                       outer_query.prom_markdown_retail_ly,
                                       outer_query.prom_markdown_retail_ty,
                                       outer_query.clear_markdown_retail_ly,
                                       outer_query.clear_markdown_retail_ty,
                                       outer_query.intercompany_markdown_ly,
                                       outer_query.intercompany_markdown_ty,
                                       outer_query.franchise_markdown_retail_ly,
                                       outer_query.franchise_markdown_retail_ty,
                                       outer_query.markdown_can_retail_ly,
                                       outer_query.markdown_can_retail_ty,
                                       outer_query.vat_in_ly,
                                       outer_query.vat_in_ty,
                                       outer_query.vat_out_ly,
                                       outer_query.vat_out_ty,
                                       outer_query.franchise_returns_cost_ly,
                                       outer_query.franchise_returns_cost_ty,
                                       outer_query.up_chrg_amt_profit_ly,
                                       outer_query.up_chrg_amt_profit_ty,
                                       outer_query.up_chrg_amt_exp_ly,
                                       outer_query.up_chrg_amt_exp_ty,
                                       outer_query.wo_activity_upd_inv_ly,
                                       outer_query.wo_activity_upd_inv_ty,
                                       outer_query.wo_activity_post_fin_ly,
                                       outer_query.wo_activity_post_fin_ty,
                                       outer_query.freight_cost_ly,
                                       outer_query.freight_cost_ty,
                                       outer_query.workroom_amt_ly,
                                       outer_query.workroom_amt_ty,
                                       outer_query.cash_disc_amt_ly,
                                       outer_query.cash_disc_amt_ty,
                                       outer_query.cost_variance_amt_ly,
                                       outer_query.cost_variance_amt_ty,
                                       outer_query.retail_cost_variance_ly,
                                       outer_query.retail_cost_variance_ty,
                                       outer_query.rec_cost_adj_variance_ly,
                                       outer_query.rec_cost_adj_variance_ty,
                                       outer_query.add_cost_ly,
                                       outer_query.add_cost_ty,
                                       outer_query.add_retail_ly,
                                       outer_query.add_retail_ty,
                                       outer_query.deal_income_purch_ly,
                                       outer_query.deal_income_purch_ty,
                                       outer_query.deal_income_sales_ly,
                                       outer_query.deal_income_sales_ty,
                                       outer_query.restocking_fee_ly,
                                       outer_query.restocking_fee_ty,
                                       outer_query.franchise_restocking_fee_ly,
                                       outer_query.franchise_restocking_fee_ty,
                                       outer_query.margin_cost_variance_ly,
                                       outer_query.margin_cost_variance_ty,
                                       outer_query.net_markdown_ly,
                                       outer_query.net_markdown_ty,
                                       outer_query.net_markup_ly,
                                       outer_query.net_markup_ty,
                                       outer_query.net_reclass_retail_ly,
                                       outer_query.net_reclass_retail_ty,
                                       outer_query.net_sales_retail_ex_vat_ly,
                                       outer_query.net_sales_retail_ex_vat_ty,
                                       outer_query.net_sale_noninv_r_exvat_ly,
                                       outer_query.net_sale_noninv_r_exvat_ly,
                                       outer_query.net_sales_noninv_retail_ly,
                                       outer_query.net_sales_noninv_retail_ty,
                                       outer_query.net_tsf_retail_ly,
                                       outer_query.net_tsf_retail_ty,
                                       --outer_query.net_tsf_cost_ly,
                                       --outer_query.net_tsf_cost_ty,
                                       outer_query.recoverable_tax_ly,
                                       outer_query.recoverable_tax_ty,
                                       outer_query.sales_units_ly,
                                       outer_query.sales_units_ty)
        from (select NVL(tbl_rdd_ly.dept,tbl_rdd_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_rdd_ly.class,tbl_rdd_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_rdd_ly.subclass,tbl_rdd_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_rdd_ly.loc_type,tbl_rdd_ty.loc_type) as loc_type,
                     NVL(tbl_rdd_ly.location,tbl_rdd_ty.location) as location,
                     NULL                                         as loc_name, --loc_name
                     NULL                                         as currency_code,
                     NULL                                         as eow_date,
                     NVL(tbl_rdd_ly.day_no,tbl_rdd_ty.day_no)     as day_no,
                     tbl_rdd_ly.purch_retail                      as purch_retail_ly,
                     tbl_rdd_ty.purch_retail                      as purch_retail_ty,
                     tbl_rdd_ly.purch_cost                        as purch_cost_ly,
                     tbl_rdd_ty.purch_cost                        as purch_cost_ty,
                     tbl_rdd_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_rdd_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_rdd_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_rdd_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_rdd_ly.tsf_in_book_retail                as tsf_in_book_retail_ly,
                     tbl_rdd_ty.tsf_in_book_retail                as tsf_in_book_retail_ty,
                     tbl_rdd_ly.intercompany_in_retail            as intercompany_in_retail_ly,
                     tbl_rdd_ty.intercompany_in_retail            as intercompany_in_retail_ty,
                     tbl_rdd_ly.reclass_in_retail                 as reclass_in_retail_ly,
                     tbl_rdd_ty.reclass_in_retail                 as reclass_in_retail_ty,
                     tbl_rdd_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_rdd_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_rdd_ly.markup_retail                     as markup_retail_ly,
                     tbl_rdd_ty.markup_retail                     as markup_retail_ty,
                     tbl_rdd_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_rdd_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_rdd_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_rdd_ty.intercompany_markup               as intercompany_markup_ty,
                     tbl_rdd_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_rdd_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_rdd_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_rdd_ty.rtv_retail                        as rtv_retail_ty,
                     tbl_rdd_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_rdd_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_rdd_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_rdd_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_rdd_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_rdd_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_rdd_ly.tsf_out_book_retail               as tsf_out_book_retail_ly,
                     tbl_rdd_ty.tsf_out_book_retail               as tsf_out_book_retail_ty,
                     tbl_rdd_ly.intercompany_out_retail           as intercompany_out_retail_ly,
                     tbl_rdd_ty.intercompany_out_retail           as intercompany_out_retail_ty,
                     tbl_rdd_ly.reclass_out_retail                as reclass_out_retail_ly,
                     tbl_rdd_ty.reclass_out_retail                as reclass_out_retail_ty,
                     tbl_rdd_ly.net_sales_retail                  as net_sales_retail_ly,
                     tbl_rdd_ty.net_sales_retail                  as net_sales_retail_ty,
                     tbl_rdd_ly.returns_retail                    as returns_retail_ly,
                     tbl_rdd_ty.returns_retail                    as returns_retail_ty,
                     tbl_rdd_ly.weight_variance_retail            as weight_variance_retail_ly,
                     tbl_rdd_ty.weight_variance_retail            as weight_variance_retail_ty,
                     tbl_rdd_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_rdd_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_rdd_ly.franchise_sales_retail            as franchise_sales_retail_ly,
                     tbl_rdd_ty.franchise_sales_retail            as franchise_sales_retail_ty,
                     tbl_rdd_ly.freight_claim_retail              as freight_claim_retail_ly,
                     tbl_rdd_ty.freight_claim_retail              as freight_claim_retail_ty,
                     tbl_rdd_ly.stock_adj_cogs_retail             as stock_adj_cogs_retail_ly,
                     tbl_rdd_ty.stock_adj_cogs_retail             as stock_adj_cogs_retail_ty,
                     tbl_rdd_ly.stock_adj_retail                  as stock_adj_retail_ly,
                     tbl_rdd_ty.stock_adj_retail                  as stock_adj_retail_ty,
                     tbl_rdd_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_rdd_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_rdd_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_rdd_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_rdd_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_rdd_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_rdd_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_rdd_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_rdd_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_rdd_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_rdd_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_rdd_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_rdd_ly.vat_in                            as vat_in_ly,
                     tbl_rdd_ty.vat_in                            as vat_in_ty,
                     tbl_rdd_ly.vat_out                           as vat_out_ly,
                     tbl_rdd_ty.vat_out                           as vat_out_ty,
                     tbl_rdd_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_rdd_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_rdd_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_rdd_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_rdd_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_rdd_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_rdd_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_rdd_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_rdd_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_rdd_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_rdd_ly.freight_cost                      as freight_cost_ly,
                     tbl_rdd_ty.freight_cost                      as freight_cost_ty,
                     tbl_rdd_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_rdd_ty.workroom_amt                      as workroom_amt_ty,
                     tbl_rdd_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_rdd_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_rdd_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_rdd_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_rdd_ly.retail_cost_variance              as retail_cost_variance_ly,
                     tbl_rdd_ty.retail_cost_variance              as retail_cost_variance_ty,
                     tbl_rdd_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_rdd_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     NULL                                         as add_cost_ly,--tbl_rdd_ly.add_cost
                     NULL                                         as add_cost_ty,--tbl_rdd_ty.add_cost
                     NULL                                         as add_retail_ly,-- tbl_rdd_ly.add_retail                       
                     NULL                                         as add_retail_ty,--tbl_rdd_ty.add_retail                       
                     tbl_rdd_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_rdd_ty.deal_income_purch                 as deal_income_purch_ty,
                     tbl_rdd_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_rdd_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_rdd_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_rdd_ty.restocking_fee                    as restocking_fee_ty,
                     tbl_rdd_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_rdd_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_rdd_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_rdd_ty.margin_cost_variance              as margin_cost_variance_ty,
                     NULL                                         as net_markdown_ly,--tbl_rdd_ly.net_markdown
                     NULL                                         as net_markdown_ty,--tbl_rdd_ty.net_markdown
                     NULL                                         as net_markup_ly,--tbl_rdd_ly.net_markup
                     NULL                                         as net_markup_ty,--tbl_rdd_ty.net_markup
                     NULL                                         as net_reclass_retail_ly,--tbl_rdd_ly.net_reclass_retail
                     NULL                                         as net_reclass_retail_ty,--tbl_rdd_ty.net_reclass_retail
                     tbl_rdd_ly.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ly,
                     tbl_rdd_ty.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ty,
                     tbl_rdd_ly.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ly,
                     tbl_rdd_ty.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ty,
                     tbl_rdd_ly.net_sales_non_inv_retail          as net_sales_noninv_retail_ly,
                     tbl_rdd_ty.net_sales_non_inv_retail          as net_sales_noninv_retail_ty,
                     NULL                                         as net_tsf_retail_ly,--tbl_rdd_ly.net_tsf_retail
                     NULL                                         as net_tsf_retail_ty,--tbl_rdd_ty.net_tsf_retail
                     tbl_rdd_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_rdd_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_rdd_ly.sales_units                       as sales_units_ly,
                     tbl_rdd_ty.sales_units                       as sales_units_ty
                from table(cast(L_tbl_r_daily_data_ly as OBJ_DAILY_DATA_TBL)) tbl_rdd_ly 
                full outer join table(cast(L_tbl_r_daily_data_ty as OBJ_DAILY_DATA_TBL)) tbl_rdd_ty 
                  ON tbl_rdd_ly.dept          =  tbl_rdd_ty.dept
                     and tbl_rdd_ly.class     =  tbl_rdd_ty.class 
                     and tbl_rdd_ly.subclass  =  tbl_rdd_ty.subclass
                     and tbl_rdd_ly.loc_type  =  tbl_rdd_ty.loc_type
                     and tbl_rdd_ly.location  =  tbl_rdd_ty.location
                     and tbl_rdd_ly.half_no   =  tbl_rdd_ty.half_no
                     and tbl_rdd_ly.data_date =  tbl_rdd_ty.data_date
                     and tbl_rdd_ly.day_no    =  tbl_rdd_ty.day_no) outer_query;    
BEGIN
   LP_r_daily_data_tbl   := new OBJ_RETAIL_DAILY_DATA_TBL();
   L_tbl_r_daily_data_ty := new  OBJ_DAILY_DATA_TBL();
   L_tbl_r_daily_data_ly := new  OBJ_DAILY_DATA_TBL();
   
   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;
   
   if LP_cal_454 = 'C' then
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_CAL_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'T',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y', --:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_r_daily_data_ty,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
      
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_CAL_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'L',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_r_daily_data_ly,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
   else
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_454_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'T',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_r_daily_data_ty,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
      
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_454_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'L',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_r_daily_data_ly,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   
   
   open C_RETAIL_DAILY_DATA_INFO;
   fetch C_RETAIL_DAILY_DATA_INFO BULK COLLECT into LP_r_daily_data_tbl;
   close C_RETAIL_DAILY_DATA_INFO;
  
   FOR i in 1..LP_r_daily_data_tbl.count
   LOOP
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_r_daily_data_tbl(i).dept,
                                  LP_r_daily_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_r_daily_data_tbl(i).dept,
                                   LP_r_daily_data_tbl(i).class,
                                   LP_r_daily_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_r_daily_data_tbl(i).dept,
                                       LP_r_daily_data_tbl(i).class,
                                       LP_r_daily_data_tbl(i).subclass,
                                       LP_r_daily_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;
      
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_r_daily_data_tbl(i).loc_name,
                                       LP_r_daily_data_tbl(i).location,
                                       LP_r_daily_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_r_daily_data_tbl(i).loc_type,
                                    LP_r_daily_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_r_daily_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_r_daily_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_r_daily_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_r_daily_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;      
      
      LP_r_daily_data_tbl(i).eow_date := LP_sch_crit_rec.period_date;
      -- CALCULATE_FIELDS
      L_net_tsf_cost_ly := LP_r_daily_data_tbl(i).tsf_in_cost_ly - LP_r_daily_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_r_daily_data_tbl(i).tsf_in_cost_ty - LP_r_daily_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_r_daily_data_tbl(i).tsf_in_retail_ly - LP_r_daily_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_r_daily_data_tbl(i).tsf_in_retail_ty - LP_r_daily_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_r_daily_data_tbl(i).markup_retail_ly - LP_r_daily_data_tbl(i).markup_can_retail_ly + LP_r_daily_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_r_daily_data_tbl(i).markup_retail_ty - LP_r_daily_data_tbl(i).markup_can_retail_ty + LP_r_daily_data_tbl(i).franchise_markup_retail_ty;
                   
      L_add_cost_ly := LP_r_daily_data_tbl(i).purch_cost_ly - LP_r_daily_data_tbl(i).rtv_cost_ly + LP_r_daily_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_r_daily_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_r_daily_data_tbl(i).purch_cost_ty - LP_r_daily_data_tbl(i).rtv_cost_ty + LP_r_daily_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_r_daily_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_r_daily_data_tbl(i).purch_retail_ly - LP_r_daily_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_r_daily_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_r_daily_data_tbl(i).purch_retail_ty - LP_r_daily_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_r_daily_data_tbl(i).franchise_returns_retail_ty;
      
      L_net_markdown_retail_ly := LP_r_daily_data_tbl(i).clear_markdown_retail_ly + LP_r_daily_data_tbl(i).perm_markdown_retail_ly + LP_r_daily_data_tbl(i).prom_markdown_retail_ly - LP_r_daily_data_tbl(i).markdown_can_retail_ly + LP_r_daily_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_r_daily_data_tbl(i).clear_markdown_retail_ty + LP_r_daily_data_tbl(i).perm_markdown_retail_ty + LP_r_daily_data_tbl(i).prom_markdown_retail_ty - LP_r_daily_data_tbl(i).markdown_can_retail_ty + LP_r_daily_data_tbl(i).franchise_markdown_retail_ty;

      L_net_reclass_retail_ly := LP_r_daily_data_tbl(i).reclass_in_retail_ly - LP_r_daily_data_tbl(i).reclass_out_retail_ly;
      L_net_reclass_retail_ty := LP_r_daily_data_tbl(i).reclass_in_retail_ty - LP_r_daily_data_tbl(i).reclass_out_retail_ty;  

      LP_r_daily_data_tbl(i).add_cost_ly                   := L_add_cost_ly; 
      LP_r_daily_data_tbl(i).add_cost_ty                   := L_add_cost_ty;
      LP_r_daily_data_tbl(i).add_retail_ly                 := L_add_retail_ly;
      LP_r_daily_data_tbl(i).add_retail_ty                 := L_add_retail_ty;
      LP_r_daily_data_tbl(i).net_markdown_ly               := L_net_markdown_retail_ly;
      LP_r_daily_data_tbl(i).net_markdown_ty               := L_net_markdown_retail_ty;
      LP_r_daily_data_tbl(i).net_markup_ly                 := L_net_markup_retail_ly;
      LP_r_daily_data_tbl(i).net_markup_ty                 := L_net_markup_retail_ty;
      LP_r_daily_data_tbl(i).net_reclass_retail_ly         := L_net_reclass_retail_ly;
      LP_r_daily_data_tbl(i).net_reclass_retail_ty         := L_net_reclass_retail_ty;
      LP_r_daily_data_tbl(i).net_tsf_retail_ly             := L_net_tsf_retail_ly;
      LP_r_daily_data_tbl(i).net_tsf_retail_ty             := L_net_tsf_retail_ty;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_RETAIL_DAILY_DATA_INFO%ISOPEN then
         close C_RETAIL_DAILY_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_R_DAILY_DATA;
--------------------------------------------------------------------------------
FUNCTION FETCH_CR_MONTH_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   
   L_program                VARCHAR2(75) :=   'CORESVC_STKLDGRV.FETCH_CR_MONTH_DATA';
   L_tbl_cr_month_data_ty   OBJ_MONTH_DATA_TBL;
   L_tbl_cr_month_data_ly   OBJ_MONTH_DATA_TBL;
   L_add_cost_ly            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ly        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty MONTH_DATA.TSF_IN_COST%TYPE;
   L_red_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_red_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ly    MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ty    MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ly  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ty  MONTH_DATA.TSF_IN_COST%TYPE;
   L_shrink_cost_pct_ly     NUMBER(12,4);
   L_shrink_cost_pct_ty     NUMBER(12,4);
   L_shrink_retail_pct_ly   NUMBER(12,4);
   L_shrink_retail_pct_ty   NUMBER(12,4);
   L_cum_markon_pct_ly      NUMBER(12,4);
   L_cum_markon_pct_ty      NUMBER(12,4);
   
   cursor C_CR_MONTH_DATA_INFO is
      select OBJ_CR_MONTH_DATA_REC( outer_query.dept,
                                    outer_query.dept_name,
                                    outer_query.class,
                                    outer_query.class_name,
                                    outer_query.subclass,
                                    outer_query.subclass_name,
                                    outer_query.loc_type,
                                    outer_query.location,
                                    outer_query.loc_name,
                                    outer_query.currency_code,
                                    outer_query.half_no,
                                    outer_query.month_no,
                                    outer_query.opn_stk_cost_ly,
                                    outer_query.opn_stk_cost_ty,
                                    outer_query.opn_stk_retail_ly,
                                    outer_query.opn_stk_retail_ty,
                                    outer_query.purch_cost_ly,
                                    outer_query.purch_cost_ty,
                                    outer_query.purch_retail_ly,
                                    outer_query.purch_retail_ty,
                                    outer_query.tsf_in_cost_ly,
                                    outer_query.tsf_in_cost_ty,
                                    outer_query.tsf_in_retail_ly,
                                    outer_query.tsf_in_retail_ty,
                                    outer_query.tsf_in_book_cost_ly,
                                    outer_query.tsf_in_book_cost_ty,
                                    outer_query.tsf_in_book_retail_ly,
                                    outer_query.tsf_in_book_retail_ty,
                                    outer_query.intercompany_in_cost_ly,
                                    outer_query.intercompany_in_cost_ty,
                                    outer_query.intercompany_in_retail_ly,
                                    outer_query.intercompany_in_retail_ty,
                                    outer_query.reclass_in_cost_ly,
                                    outer_query.reclass_in_cost_ty,
                                    outer_query.reclass_in_retail_ly,
                                    outer_query.reclass_in_retail_ty,
                                    outer_query.franchise_returns_cost_ly,
                                    outer_query.franchise_returns_cost_ty,
                                    outer_query.franchise_returns_retail_ly,
                                    outer_query.franchise_returns_retail_ty,
                                    outer_query.up_chrg_amt_profit_ly,
                                    outer_query.up_chrg_amt_profit_ty,
                                    outer_query.up_chrg_amt_exp_ly,
                                    outer_query.up_chrg_amt_exp_ty,
                                    outer_query.wo_activity_upd_inv_ly,
                                    outer_query.wo_activity_upd_inv_ty,
                                    outer_query.wo_activity_post_fin_ly,
                                    outer_query.wo_activity_post_fin_ty,
                                    outer_query.recoverable_tax_ly,
                                    outer_query.recoverable_tax_ty,
                                    outer_query.markup_retail_ly,
                                    outer_query.markup_retail_ty,
                                    outer_query.franchise_markup_retail_ly,
                                    outer_query.franchise_markup_retail_ty,
                                    outer_query.intercompany_markup_ly,
                                    outer_query.intercompany_markup_ty,
                                    outer_query.markup_can_retail_ly,
                                    outer_query.markup_can_retail_ty,
                                    outer_query.rtv_cost_ly,
                                    outer_query.rtv_cost_ty,
                                    outer_query.rtv_retail_ly,
                                    outer_query.rtv_retail_ty,
                                    outer_query.tsf_out_cost_ly,
                                    outer_query.tsf_out_cost_ty,
                                    outer_query.tsf_out_retail_ly,
                                    outer_query.tsf_out_retail_ty,
                                    outer_query.tsf_out_book_cost_ly,
                                    outer_query.tsf_out_book_cost_ty,
                                    outer_query.tsf_out_book_retail_ly,
                                    outer_query.tsf_out_book_retail_ty,
                                    outer_query.intercompany_out_cost_ly,
                                    outer_query.intercompany_out_cost_ty,
                                    outer_query.intercompany_out_retail_ly,
                                    outer_query.intercompany_out_retail_ty,
                                    outer_query.reclass_out_cost_ly,
                                    outer_query.reclass_out_cost_ty,
                                    outer_query.reclass_out_retail_ly,
                                    outer_query.reclass_out_retail_ty,
                                    outer_query.net_sales_cost_ly,
                                    outer_query.net_sales_cost_ty,
                                    outer_query.net_sales_retail_ly,
                                    outer_query.net_sales_retail_ty,
                                    outer_query.returns_cost_ly,
                                    outer_query.returns_cost_ty,
                                    outer_query.returns_retail_ly,
                                    outer_query.returns_retail_ty,
                                    outer_query.weight_variance_retail_ly,
                                    outer_query.weight_variance_retail_ty,
                                    outer_query.empl_disc_retail_ly,
                                    outer_query.empl_disc_retail_ty,
                                    outer_query.franchise_sales_cost_ly,
                                    outer_query.franchise_sales_cost_ty,
                                    outer_query.franchise_sales_retail_ly,
                                    outer_query.franchise_sales_retail_ty,
                                    outer_query.freight_claim_cost_ly,
                                    outer_query.freight_claim_cost_ty,
                                    outer_query.freight_claim_retail_ly,
                                    outer_query.freight_claim_retail_ty,
                                    outer_query.stock_adj_cogs_cost_ly,
                                    outer_query.stock_adj_cogs_cost_ty,
                                    outer_query.stock_adj_cogs_retail_ly,
                                    outer_query.stock_adj_cogs_retail_ty,
                                    outer_query.stock_adj_cost_ly,
                                    outer_query.stock_adj_cost_ty,
                                    outer_query.stock_adj_retail_ly,
                                    outer_query.stock_adj_retail_ty,
                                    outer_query.shrinkage_cost_ly,
                                    outer_query.shrinkage_cost_ty,
                                    outer_query.shrinkage_retail_ly,
                                    outer_query.shrinkage_retail_ty,
                                    outer_query.cost_variance_amt_ly,
                                    outer_query.cost_variance_amt_ty,
                                    outer_query.margin_cost_variance_ly,
                                    outer_query.margin_cost_variance_ty,
                                    outer_query.perm_markdown_retail_ly,
                                    outer_query.perm_markdown_retail_ty,
                                    outer_query.prom_markdown_retail_ly,
                                    outer_query.prom_markdown_retail_ty,
                                    outer_query.clear_markdown_retail_ly,
                                    outer_query.clear_markdown_retail_ty,
                                    outer_query.intercompany_markdown_ly,
                                    outer_query.intercompany_markdown_ty,
                                    outer_query.franchise_markdown_retail_ly,
                                    outer_query.franchise_markdown_retail_ty,
                                    outer_query.markdown_can_retail_ly,
                                    outer_query.markdown_can_retail_ty,
                                    outer_query.cls_stk_cost_ly,
                                    outer_query.cls_stk_cost_ty,
                                    outer_query.cls_stk_retail_ly,
                                    outer_query.cls_stk_retail_ty,
                                    outer_query.add_cost_ly,
                                    outer_query.add_cost_ty,
                                    outer_query.add_retail_ly,
                                    outer_query.add_retail_ty,
                                    outer_query.shrink_cost_pct_ly,
                                    outer_query.shrink_cost_pct_ty,
                                    outer_query.htd_gafs_retail_ly,
                                    outer_query.htd_gafs_retail_ty,
                                    outer_query.htd_gafs_cost_ly,
                                    outer_query.htd_gafs_cost_ty,
                                    outer_query.red_retail_ly,
                                    outer_query.red_retail_ty,
                                    outer_query.deal_income_sales_ly,
                                    outer_query.deal_income_sales_ty,
                                    outer_query.deal_income_purch_ly,
                                    outer_query.deal_income_purch_ty,
                                    outer_query.cum_markon_pct_ly,
                                    outer_query.cum_markon_pct_ty,
                                    outer_query.gross_margin_amt_ly,
                                    outer_query.gross_margin_amt_ty,
                                    outer_query.vat_in_ly,
                                    outer_query.vat_in_ty,
                                    outer_query.vat_out_ly,
                                    outer_query.vat_out_ty,
                                    outer_query.restocking_fee_ly,
                                    outer_query.restocking_fee_ty,
                                    outer_query.freight_cost_ly,
                                    outer_query.freight_cost_ty,
                                    outer_query.rec_cost_adj_variance_ly,
                                    outer_query.rec_cost_adj_variance_ty,
                                    outer_query.workroom_amt_ly,
                                    outer_query.workroom_amt_ty,
                                    outer_query.cash_disc_amt_ly,
                                    outer_query.cash_disc_amt_ty,
                                    outer_query.retail_cost_variance_ly,
                                    outer_query.retail_cost_variance_ty,
                                    outer_query.franchise_restocking_fee_ly,
                                    outer_query.franchise_restocking_fee_ty,
                                    outer_query.intercompany_margin_ly,
                                    outer_query.intercompany_margin_ty,
                                    outer_query.net_markdown_ly,
                                    outer_query.net_markdown_ty,
                                    outer_query.net_markup_ly,
                                    outer_query.net_markup_ty,
                                    outer_query.net_reclass_cost_ly,
                                    outer_query.net_reclass_cost_ty,
                                    outer_query.net_reclass_retail_ly,
                                    outer_query.net_reclass_retail_ty,
                                    outer_query.net_sales_retail_ex_vat_ly,
                                    outer_query.net_sales_retail_ex_vat_ty,
                                    outer_query.net_sale_noninv_r_exvat_ly,
                                    outer_query.net_sale_noninv_r_exvat_ty,
                                    outer_query.net_tsf_cost_ly,
                                    outer_query.net_tsf_cost_ty,
                                    outer_query.net_tsf_retail_ly,
                                    outer_query.net_tsf_retail_ty,
                                    outer_query.shrink_retail_pct_ly,
                                    outer_query.shrink_retail_pct_ty,
                                    outer_query.sales_units_ly,
                                    outer_query.sales_units_ty,
                                    outer_query.net_sales_non_inv_cost_ly,
                                    outer_query.net_sales_non_inv_cost_ty,
                                    outer_query.net_sales_noninv_retail_ly,
                                    outer_query.net_sales_noninv_retail_ty)
        from (select NVL(tbl_crm_ly.dept,tbl_crm_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_crm_ly.class,tbl_crm_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_crm_ly.subclass,tbl_crm_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_crm_ly.loc_type,tbl_crm_ty.loc_type) as loc_type,
                     NVL(tbl_crm_ly.location,tbl_crm_ty.location) as location,
                     NULL                                         as loc_name,--loc_name
                     NULL                                         as currency_code,
                     NVL(tbl_crm_ly.half_no,tbl_crm_ty.half_no)   as half_no,
                     NVL(tbl_crm_ly.month_no,tbl_crm_ty.month_no) as month_no,
                     tbl_crm_ly.opn_stk_cost                      as opn_stk_cost_ly,
                     tbl_crm_ty.opn_stk_cost                      as opn_stk_cost_ty,
                     tbl_crm_ly.opn_stk_retail                    as opn_stk_retail_ly,
                     tbl_crm_ty.opn_stk_retail                    as opn_stk_retail_ty,
                     tbl_crm_ly.purch_cost                        as purch_cost_ly,
                     tbl_crm_ty.purch_cost                        as purch_cost_ty,
                     tbl_crm_ly.purch_retail                      as purch_retail_ly,
                     tbl_crm_ty.purch_retail                      as purch_retail_ty,
                     tbl_crm_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_crm_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_crm_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_crm_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_crm_ly.tsf_in_book_cost                  as tsf_in_book_cost_ly,
                     tbl_crm_ty.tsf_in_book_cost                  as tsf_in_book_cost_ty,
                     tbl_crm_ly.tsf_in_book_retail                as tsf_in_book_retail_ly,
                     tbl_crm_ty.tsf_in_book_retail                as tsf_in_book_retail_ty,
                     tbl_crm_ly.intercompany_in_cost              as intercompany_in_cost_ly,
                     tbl_crm_ty.intercompany_in_cost              as intercompany_in_cost_ty,
                     tbl_crm_ly.intercompany_in_retail            as intercompany_in_retail_ly,
                     tbl_crm_ty.intercompany_in_retail            as intercompany_in_retail_ty,
                     tbl_crm_ly.reclass_in_cost                   as reclass_in_cost_ly,
                     tbl_crm_ty.reclass_in_cost                   as reclass_in_cost_ty,
                     tbl_crm_ly.reclass_in_retail                 as reclass_in_retail_ly,
                     tbl_crm_ty.reclass_in_retail                 as reclass_in_retail_ty,
                     tbl_crm_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_crm_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_crm_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_crm_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_crm_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_crm_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_crm_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_crm_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_crm_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_crm_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_crm_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_crm_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_crm_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_crm_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_crm_ly.markup_retail                     as markup_retail_ly,
                     tbl_crm_ty.markup_retail                     as markup_retail_ty,
                     tbl_crm_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_crm_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_crm_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_crm_ty.intercompany_markup               as intercompany_markup_ty,
                     tbl_crm_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_crm_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_crm_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_crm_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_crm_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_crm_ty.rtv_retail                        as rtv_retail_ty,
                     tbl_crm_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_crm_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_crm_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_crm_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_crm_ly.tsf_out_book_cost                 as tsf_out_book_cost_ly,
                     tbl_crm_ty.tsf_out_book_cost                 as tsf_out_book_cost_ty,
                     tbl_crm_ly.tsf_out_book_retail               as tsf_out_book_retail_ly,
                     tbl_crm_ty.tsf_out_book_retail               as tsf_out_book_retail_ty,
                     tbl_crm_ly.intercompany_out_cost             as intercompany_out_cost_ly,
                     tbl_crm_ty.intercompany_out_cost             as intercompany_out_cost_ty,
                     tbl_crm_ly.intercompany_out_retail           as intercompany_out_retail_ly,
                     tbl_crm_ty.intercompany_out_retail           as intercompany_out_retail_ty,
                     tbl_crm_ly.reclass_out_cost                  as reclass_out_cost_ly,
                     tbl_crm_ty.reclass_out_cost                  as reclass_out_cost_ty,
                     tbl_crm_ly.reclass_out_retail                as reclass_out_retail_ly,
                     tbl_crm_ty.reclass_out_retail                as reclass_out_retail_ty,
                     tbl_crm_ly.net_sales_cost                    as net_sales_cost_ly,
                     tbl_crm_ty.net_sales_cost                    as net_sales_cost_ty,
                     tbl_crm_ly.net_sales_retail                  as net_sales_retail_ly,
                     tbl_crm_ty.net_sales_retail                  as net_sales_retail_ty,
                     tbl_crm_ly.returns_cost                      as returns_cost_ly,
                     tbl_crm_ty.returns_cost                      as returns_cost_ty,
                     tbl_crm_ly.returns_retail                    as returns_retail_ly,
                     tbl_crm_ty.returns_retail                    as returns_retail_ty,
                     tbl_crm_ly.weight_variance_retail            as weight_variance_retail_ly,
                     tbl_crm_ty.weight_variance_retail            as weight_variance_retail_ty,
                     tbl_crm_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_crm_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_crm_ly.franchise_sales_cost              as franchise_sales_cost_ly,
                     tbl_crm_ty.franchise_sales_cost              as franchise_sales_cost_ty,
                     tbl_crm_ly.franchise_sales_retail            as franchise_sales_retail_ly,
                     tbl_crm_ty.franchise_sales_retail            as franchise_sales_retail_ty,
                     tbl_crm_ly.freight_claim_cost                as freight_claim_cost_ly,
                     tbl_crm_ty.freight_claim_cost                as freight_claim_cost_ty,
                     tbl_crm_ly.freight_claim_retail              as freight_claim_retail_ly,
                     tbl_crm_ty.freight_claim_retail              as freight_claim_retail_ty,
                     tbl_crm_ly.stock_adj_cogs_cost               as stock_adj_cogs_cost_ly,
                     tbl_crm_ty.stock_adj_cogs_cost               as stock_adj_cogs_cost_ty,
                     tbl_crm_ly.stock_adj_cogs_retail             as stock_adj_cogs_retail_ly,
                     tbl_crm_ty.stock_adj_cogs_retail             as stock_adj_cogs_retail_ty,
                     tbl_crm_ly.stock_adj_cost                    as stock_adj_cost_ly,
                     tbl_crm_ty.stock_adj_cost                    as stock_adj_cost_ty,
                     tbl_crm_ly.stock_adj_retail                  as stock_adj_retail_ly,
                     tbl_crm_ty.stock_adj_retail                  as stock_adj_retail_ty,
                     tbl_crm_ly.shrinkage_cost                    as shrinkage_cost_ly,
                     tbl_crm_ty.shrinkage_cost                    as shrinkage_cost_ty,
                     tbl_crm_ly.shrinkage_retail                  as shrinkage_retail_ly,
                     tbl_crm_ty.shrinkage_retail                  as shrinkage_retail_ty,
                     tbl_crm_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_crm_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_crm_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_crm_ty.margin_cost_variance              as margin_cost_variance_ty,
                     tbl_crm_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_crm_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_crm_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_crm_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_crm_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_crm_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_crm_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_crm_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_crm_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_crm_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_crm_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_crm_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_crm_ly.cls_stk_cost                      as cls_stk_cost_ly,
                     tbl_crm_ty.cls_stk_cost                      as cls_stk_cost_ty,
                     tbl_crm_ly.cls_stk_retail                    as cls_stk_retail_ly,
                     tbl_crm_ty.cls_stk_retail                    as cls_stk_retail_ty,
                     NULL                                         as add_cost_ly, -- tbl_crm_ly.add_cost
                     NULL                                         as add_cost_ty, -- tbl_crm_ty.add_cost
                     NULL                                         as add_retail_ly,--tbl_crm_ly.add_retail
                     NULL                                         as add_retail_ty, --tbl_crm_ty.add_retail 
                     NULL                                         as shrink_cost_pct_ly, --tbl_crm_ly.shrink_cost_pct
                     NULL                                         as shrink_cost_pct_ty, --tbl_crm_ty.shrink_cost_pct
                     tbl_crm_ly.htd_gafs_retail                   as htd_gafs_retail_ly,
                     tbl_crm_ty.htd_gafs_retail                   as htd_gafs_retail_ty,
                     tbl_crm_ly.htd_gafs_cost                     as htd_gafs_cost_ly,
                     tbl_crm_ty.htd_gafs_cost                     as htd_gafs_cost_ty,
                     NULL                                         as red_retail_ly, --tbl_crm_ly.red_retail
                     NULL                                         as red_retail_ty, --tbl_crm_ty.red_retail
                     tbl_crm_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_crm_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_crm_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_crm_ty.deal_income_purch                 as deal_income_purch_ty,
                     NULL                                         as cum_markon_pct_ly, --tbl_crm_ly.cum_markon_pct
                     NULL                                         as cum_markon_pct_ty, --tbl_crm_ty.cum_markon_pct
                     tbl_crm_ly.gross_margin_amt                  as gross_margin_amt_ly,
                     tbl_crm_ty.gross_margin_amt                  as gross_margin_amt_ty,
                     tbl_crm_ly.vat_in                            as vat_in_ly,
                     tbl_crm_ty.vat_in                            as vat_in_ty,
                     tbl_crm_ly.vat_out                           as vat_out_ly,
                     tbl_crm_ty.vat_out                           as vat_out_ty,
                     tbl_crm_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_crm_ty.restocking_fee                    as restocking_fee_ty,
                     tbl_crm_ly.freight_cost                      as freight_cost_ly,
                     tbl_crm_ty.freight_cost                      as freight_cost_ty,
                     tbl_crm_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_crm_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     tbl_crm_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_crm_ty.workroom_amt                      as workroom_amt_ty,
                     tbl_crm_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_crm_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_crm_ly.retail_cost_variance              as retail_cost_variance_ly,
                     tbl_crm_ty.retail_cost_variance              as retail_cost_variance_ty,
                     tbl_crm_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_crm_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_crm_ly.intercompany_margin               as intercompany_margin_ly,
                     tbl_crm_ty.intercompany_margin               as intercompany_margin_ty,
                     NULL                                         as net_markdown_ly, --tbl_crm_ly.net_markdown
                     NULL                                         as net_markdown_ty, --tbl_crm_ty.net_markdown
                     NULL                                         as net_markup_ly, --tbl_crm_ly.net_markup
                     NULL                                         as net_markup_ty, --tbl_crm_ty.net_markup
                     NULL                                         as net_reclass_cost_ly, --tbl_crm_ly.net_reclass_cost
                     NULL                                         as net_reclass_cost_ty, --tbl_crm_ty.net_reclass_cost
                     NULL                                         as net_reclass_retail_ly, --tbl_crm_ly.net_reclass_retail
                     NULL                                         as net_reclass_retail_ty, --tbl_crm_ty.net_reclass_retail
                     tbl_crm_ly.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ly,
                     tbl_crm_ty.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ty,
                     tbl_crm_ly.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ly,
                     tbl_crm_ty.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ty,
                     NULL                                         as net_tsf_cost_ly, --tbl_crm_ly.net_tsf_cost
                     NULL                                         as net_tsf_cost_ty, --tbl_crm_ty.net_tsf_cost
                     NULL                                         as net_tsf_retail_ly, --tbl_crm_ly.net_tsf_retail
                     NULL                                         as net_tsf_retail_ty, --tbl_crm_ty.net_tsf_retail
                     NULL                                         as shrink_retail_pct_ly, --tbl_crm_ly.shrink_retail_pct
                     NULL                                         as shrink_retail_pct_ty, --tbl_crm_ty.shrink_retail_pct
                     tbl_crm_ly.sales_units                       as sales_units_ly,
                     tbl_crm_ty.sales_units                       as sales_units_ty,
                     tbl_crm_ly.net_sales_non_inv_cost            as net_sales_non_inv_cost_ly,
                     tbl_crm_ty.net_sales_non_inv_cost            as net_sales_non_inv_cost_ty,
                     tbl_crm_ly.net_sales_non_inv_retail          as net_sales_noninv_retail_ly,
                     tbl_crm_ty.net_sales_non_inv_retail          as net_sales_noninv_retail_ty
                from table(cast(L_tbl_cr_month_data_ly as OBJ_MONTH_DATA_TBL)) tbl_crm_ly 
                full outer join table(cast(L_tbl_cr_month_data_ty as OBJ_MONTH_DATA_TBL)) tbl_crm_ty 
                  ON tbl_crm_ly.dept         =  tbl_crm_ty.dept
                     and tbl_crm_ly.class    =  tbl_crm_ty.class 
                     and tbl_crm_ly.subclass =  tbl_crm_ty.subclass
                     and tbl_crm_ly.loc_type =  tbl_crm_ty.loc_type
                     and tbl_crm_ly.location =  tbl_crm_ty.location
                     and tbl_crm_ly.half_no  =  tbl_crm_ty.half_no
                     and tbl_crm_ly.month_no =  tbl_crm_ty.month_no
                     and tbl_crm_ly.eom_date =  tbl_crm_ty.eom_date) outer_query;    
BEGIN
   LP_cr_month_data_tbl   := new OBJ_CR_MONTH_DATA_TBL();
   L_tbl_cr_month_data_ty := new OBJ_MONTH_DATA_TBL();
   L_tbl_cr_month_data_ly := new OBJ_MONTH_DATA_TBL();
   
   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;                       
   if STKLEDGR_QUERY_SQL.FETCH_MONTH_DATA_BULK(LP_sch_crit_rec.dept,
                                               LP_sch_crit_rec.class,
                                               LP_sch_crit_rec.subclass,
                                               'T',
                                               LP_sch_crit_rec.period_no,
                                               LP_loc_type,
                                               LP_location,
                                               'Y',      --:B_search.CB_thousands
                                               LP_currency_ind,
                                               LP_set_of_books_id,
                                               L_tbl_cr_month_data_ty,
                                               O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if STKLEDGR_QUERY_SQL.FETCH_MONTH_DATA_BULK(LP_sch_crit_rec.dept,
                                               LP_sch_crit_rec.class,
                                               LP_sch_crit_rec.subclass,
                                               'L',
                                               LP_sch_crit_rec.period_no,
                                               LP_loc_type,
                                               LP_location,
                                               'Y',      --:B_search.CB_thousands
                                               LP_currency_ind,
                                               LP_set_of_books_id,
                                               L_tbl_cr_month_data_ly,
                                               O_error_message) = FALSE then
      return FALSE;
   end if;
   
   open C_CR_MONTH_DATA_INFO;
   fetch C_CR_MONTH_DATA_INFO BULK COLLECT into LP_cr_month_data_tbl;
   close C_CR_MONTH_DATA_INFO;
  
   FOR i in 1..LP_cr_month_data_tbl.count
   LOOP
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_cr_month_data_tbl(i).dept,
                                  LP_cr_month_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_cr_month_data_tbl(i).dept,
                                   LP_cr_month_data_tbl(i).class,
                                   LP_cr_month_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_cr_month_data_tbl(i).dept,
                                       LP_cr_month_data_tbl(i).class,
                                       LP_cr_month_data_tbl(i).subclass,
                                       LP_cr_month_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;     
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_cr_month_data_tbl(i).loc_name,
                                       LP_cr_month_data_tbl(i).location,
                                       LP_cr_month_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_cr_month_data_tbl(i).loc_type,
                                    LP_cr_month_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_cr_month_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_cr_month_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_cr_month_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_cr_month_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;      
      -- CALCULATE_FIELDS
      L_net_tsf_cost_ly := LP_cr_month_data_tbl(i).tsf_in_cost_ly - LP_cr_month_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_cr_month_data_tbl(i).tsf_in_cost_ty - LP_cr_month_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_cr_month_data_tbl(i).tsf_in_retail_ly - LP_cr_month_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_cr_month_data_tbl(i).tsf_in_retail_ty - LP_cr_month_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_cr_month_data_tbl(i).markup_retail_ly - LP_cr_month_data_tbl(i).markup_can_retail_ly + LP_cr_month_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_cr_month_data_tbl(i).markup_retail_ty - LP_cr_month_data_tbl(i).markup_can_retail_ty + LP_cr_month_data_tbl(i).franchise_markup_retail_ty;
      
      L_net_markdown_retail_ly := LP_cr_month_data_tbl(i).clear_markdown_retail_ly + LP_cr_month_data_tbl(i).perm_markdown_retail_ly + LP_cr_month_data_tbl(i).prom_markdown_retail_ly - LP_cr_month_data_tbl(i).markdown_can_retail_ly + LP_cr_month_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_cr_month_data_tbl(i).clear_markdown_retail_ty + LP_cr_month_data_tbl(i).perm_markdown_retail_ty + LP_cr_month_data_tbl(i).prom_markdown_retail_ty - LP_cr_month_data_tbl(i).markdown_can_retail_ty + LP_cr_month_data_tbl(i).franchise_markdown_retail_ty;
            
      L_add_cost_ly := LP_cr_month_data_tbl(i).purch_cost_ly - LP_cr_month_data_tbl(i).rtv_cost_ly + LP_cr_month_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_cr_month_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_cr_month_data_tbl(i).purch_cost_ty - LP_cr_month_data_tbl(i).rtv_cost_ty + LP_cr_month_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_cr_month_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_cr_month_data_tbl(i).purch_retail_ly - LP_cr_month_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_cr_month_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_cr_month_data_tbl(i).purch_retail_ty - LP_cr_month_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_cr_month_data_tbl(i).franchise_returns_retail_ty;
      
      L_red_retail_ly   := LP_cr_month_data_tbl(i).net_sales_retail_ly + LP_cr_month_data_tbl(i).shrinkage_retail_ly + L_net_markdown_retail_ly + LP_cr_month_data_tbl(i).empl_disc_retail_ly + LP_cr_month_data_tbl(i).weight_variance_retail_ly + LP_cr_month_data_tbl(i).franchise_markdown_retail_ly + LP_cr_month_data_tbl(i).franchise_sales_retail_ly - LP_cr_month_data_tbl(i).franchise_returns_retail_ly;
      L_red_retail_ty   := LP_cr_month_data_tbl(i).net_sales_retail_ty + LP_cr_month_data_tbl(i).shrinkage_retail_ty + L_net_markdown_retail_ty + LP_cr_month_data_tbl(i).empl_disc_retail_ty + LP_cr_month_data_tbl(i).weight_variance_retail_ty + LP_cr_month_data_tbl(i).franchise_markdown_retail_ty + LP_cr_month_data_tbl(i).franchise_sales_retail_ty - LP_cr_month_data_tbl(i).franchise_returns_retail_ty;

      L_net_reclass_cost_ly   := LP_cr_month_data_tbl(i).reclass_in_cost_ly - LP_cr_month_data_tbl(i).reclass_out_cost_ly;          
      L_net_reclass_cost_ty   := LP_cr_month_data_tbl(i).reclass_in_cost_ty - LP_cr_month_data_tbl(i).reclass_out_cost_ty;

      L_net_reclass_retail_ly := LP_cr_month_data_tbl(i).reclass_in_retail_ly - LP_cr_month_data_tbl(i).reclass_out_retail_ly;    
      L_net_reclass_retail_ty := LP_cr_month_data_tbl(i).reclass_in_retail_ty - LP_cr_month_data_tbl(i).reclass_out_retail_ty;

      if (LP_cr_month_data_tbl(i).net_sales_cost_ly is NULL and LP_cr_month_data_tbl(i).franchise_sales_cost_ly is NULL and LP_cr_month_data_tbl(i).franchise_returns_cost_ly is NULL) 
         or (LP_cr_month_data_tbl(i).net_sales_cost_ly = 0 and (LP_cr_month_data_tbl(i).franchise_sales_cost_ly - LP_cr_month_data_tbl(i).franchise_returns_cost_ly) = 0) then
         L_shrink_cost_pct_ly := 0;
      else
         L_shrink_cost_pct_ly := ((LP_cr_month_data_tbl(i).shrinkage_cost_ly / (LP_cr_month_data_tbl(i).net_sales_cost_ly + LP_cr_month_data_tbl(i).franchise_sales_cost_ly - LP_cr_month_data_tbl(i).franchise_returns_cost_ly))*100);
      end if;
      
      if (LP_cr_month_data_tbl(i).net_sales_cost_ty is NULL and LP_cr_month_data_tbl(i).franchise_sales_cost_ty is NULL and LP_cr_month_data_tbl(i).franchise_returns_cost_ty is NULL) 
         or (LP_cr_month_data_tbl(i).net_sales_cost_ty = 0 and (LP_cr_month_data_tbl(i).franchise_sales_cost_ty - LP_cr_month_data_tbl(i).franchise_returns_cost_ty) = 0) then
         L_shrink_cost_pct_ty := 0;
      else
         L_shrink_cost_pct_ty := ((LP_cr_month_data_tbl(i).shrinkage_cost_ty / (LP_cr_month_data_tbl(i).net_sales_cost_ty + LP_cr_month_data_tbl(i).franchise_sales_cost_ty - LP_cr_month_data_tbl(i).franchise_returns_cost_ty))*100);
      end if;

      if (LP_cr_month_data_tbl(i).net_sales_retail_ly is NULL and LP_cr_month_data_tbl(i).franchise_sales_retail_ly is NULL and LP_cr_month_data_tbl(i).franchise_returns_retail_ly is NULL) 
   	   or (LP_cr_month_data_tbl(i).net_sales_retail_ly = 0 and (LP_cr_month_data_tbl(i).franchise_sales_retail_ly - LP_cr_month_data_tbl(i).franchise_returns_retail_ly) = 0) then
         L_shrink_retail_pct_ly := 0;
      else
         L_shrink_retail_pct_ly := ((LP_cr_month_data_tbl(i).shrinkage_retail_ly / (LP_cr_month_data_tbl(i).net_sales_retail_ly + LP_cr_month_data_tbl(i).franchise_sales_retail_ly - LP_cr_month_data_tbl(i).franchise_returns_retail_ly))*100);
      end if;
     
      if (LP_cr_month_data_tbl(i).net_sales_retail_ty is NULL and LP_cr_month_data_tbl(i).franchise_sales_retail_ty is NULL and LP_cr_month_data_tbl(i).franchise_returns_retail_ty is NULL) 
   	   or (LP_cr_month_data_tbl(i).net_sales_retail_ty = 0 and (LP_cr_month_data_tbl(i).franchise_sales_retail_ty - LP_cr_month_data_tbl(i).franchise_returns_retail_ty) = 0) then
         L_shrink_retail_pct_ty := 0;
      else
          L_shrink_retail_pct_ty := ((LP_cr_month_data_tbl(i).shrinkage_retail_ty / (LP_cr_month_data_tbl(i).net_sales_retail_ty + LP_cr_month_data_tbl(i).franchise_sales_retail_ty - LP_cr_month_data_tbl(i).franchise_returns_retail_ty))*100);
      end if;      
      
      if LP_cr_month_data_tbl(i).htd_gafs_retail_ly is NULL or LP_cr_month_data_tbl(i).htd_gafs_retail_ly = 0 then
         L_cum_markon_pct_ly := 0;
      else
         L_cum_markon_pct_ly := ((1 - (LP_cr_month_data_tbl(i).htd_gafs_cost_ly / LP_cr_month_data_tbl(i).htd_gafs_retail_ly)) * 100);
      end if;

      if LP_cr_month_data_tbl(i).htd_gafs_retail_ty is NULL or LP_cr_month_data_tbl(i).htd_gafs_retail_ty = 0 then
         L_cum_markon_pct_ty := 0;
      else
         L_cum_markon_pct_ty := ((1 - (LP_cr_month_data_tbl(i).htd_gafs_cost_ty / LP_cr_month_data_tbl(i).htd_gafs_retail_ty)) * 100);
      end if; 
      
      LP_cr_month_data_tbl(i).add_cost_ly                   := L_add_cost_ly;
      LP_cr_month_data_tbl(i).add_cost_ty                   := L_add_cost_ty;
      LP_cr_month_data_tbl(i).add_retail_ly                 := L_add_retail_ly;
      LP_cr_month_data_tbl(i).add_retail_ty                 := L_add_retail_ty;
      LP_cr_month_data_tbl(i).red_retail_ly                 := L_red_retail_ly;
      LP_cr_month_data_tbl(i).red_retail_ty                 := L_red_retail_ty;
      LP_cr_month_data_tbl(i).shrink_cost_pct_ly            := L_shrink_cost_pct_ly;
      LP_cr_month_data_tbl(i).shrink_cost_pct_ty            := L_shrink_cost_pct_ty;
      LP_cr_month_data_tbl(i).shrink_retail_pct_ly          := L_shrink_retail_pct_ly;
      LP_cr_month_data_tbl(i).shrink_retail_pct_ty          := L_shrink_retail_pct_ty;
      LP_cr_month_data_tbl(i).net_markdown_ly               := L_net_markdown_retail_ly;
      LP_cr_month_data_tbl(i).net_markdown_ty               := L_net_markdown_retail_ty;
      LP_cr_month_data_tbl(i).net_markup_ly                 := L_net_markup_retail_ly;
      LP_cr_month_data_tbl(i).net_markup_ty                 := L_net_markup_retail_ty;
      LP_cr_month_data_tbl(i).net_reclass_cost_ly           := L_net_reclass_cost_ly;
      LP_cr_month_data_tbl(i).net_reclass_cost_ty           := L_net_reclass_cost_ty;
      LP_cr_month_data_tbl(i).net_reclass_retail_ly         := L_net_reclass_retail_ly;
      LP_cr_month_data_tbl(i).net_reclass_retail_ty         := L_net_reclass_retail_ty;
      LP_cr_month_data_tbl(i).net_tsf_cost_ly               := L_net_tsf_cost_ly;
      LP_cr_month_data_tbl(i).net_tsf_cost_ty               := L_net_tsf_cost_ty;
      LP_cr_month_data_tbl(i).net_tsf_retail_ly             := L_net_tsf_retail_ly;
      LP_cr_month_data_tbl(i).net_tsf_retail_ty             := L_net_tsf_retail_ty;
      LP_cr_month_data_tbl(i).cum_markon_pct_ly             := L_cum_markon_pct_ly;
      LP_cr_month_data_tbl(i).cum_markon_pct_ty             := L_cum_markon_pct_ty;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CR_MONTH_DATA_INFO%ISOPEN then
         close C_CR_MONTH_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_CR_MONTH_DATA;
--------------------------------------------------------------------------------
FUNCTION FETCH_CR_WEEK_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   
   L_program                VARCHAR2(75) := 'CORESVC_STKLDGRV.FETCH_CR_WEEK_DATA';
   L_tbl_cr_week_data_ty    OBJ_WEEK_DATA_TBL;
   L_tbl_cr_week_data_ly    OBJ_WEEK_DATA_TBL;

   L_add_cost_ly            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ly        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty MONTH_DATA.TSF_IN_COST%TYPE;
   L_red_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_red_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ly    MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ty    MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ly  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ty  MONTH_DATA.TSF_IN_COST%TYPE;
   L_shrink_cost_pct_ly     NUMBER(12,4);
   L_shrink_cost_pct_ty     NUMBER(12,4);
   L_shrink_retail_pct_ly   NUMBER(12,4);
   L_shrink_retail_pct_ty   NUMBER(12,4);
   L_cum_markon_pct_ly      NUMBER(12,4);
   L_cum_markon_pct_ty      NUMBER(12,4);
   
   cursor C_CR_WEEK_DATA_INFO is
      select OBJ_CR_WEEK_DATA_REC(  outer_query.dept,
                                    outer_query.dept_name,
                                    outer_query.class,
                                    outer_query.class_name,
                                    outer_query.subclass,
                                    outer_query.subclass_name,
                                    outer_query.loc_type,
                                    outer_query.location,
                                    outer_query.loc_name,
                                    outer_query.currency_code,
                                    outer_query.eom_date,
                                    outer_query.week_no,
                                    outer_query.opn_stk_cost_ly,
                                    outer_query.opn_stk_cost_ty,
                                    outer_query.opn_stk_retail_ly,
                                    outer_query.opn_stk_retail_ty,
                                    outer_query.purch_cost_ly,
                                    outer_query.purch_cost_ty,
                                    outer_query.purch_retail_ly,
                                    outer_query.purch_retail_ty,
                                    outer_query.tsf_in_cost_ly,
                                    outer_query.tsf_in_cost_ty,
                                    outer_query.tsf_in_retail_ly,
                                    outer_query.tsf_in_retail_ty,
                                    outer_query.tsf_in_book_cost_ly,
                                    outer_query.tsf_in_book_cost_ty,
                                    outer_query.tsf_in_book_retail_ly,
                                    outer_query.tsf_in_book_retail_ty,
                                    outer_query.intercompany_in_cost_ly,
                                    outer_query.intercompany_in_cost_ty,
                                    outer_query.intercompany_in_retail_ly,
                                    outer_query.intercompany_in_retail_ty,
                                    outer_query.reclass_in_cost_ly,
                                    outer_query.reclass_in_cost_ty,
                                    outer_query.reclass_in_retail_ly,
                                    outer_query.reclass_in_retail_ty,
                                    outer_query.franchise_returns_cost_ly,
                                    outer_query.franchise_returns_cost_ty,
                                    outer_query.franchise_returns_retail_ly,
                                    outer_query.franchise_returns_retail_ty,
                                    outer_query.up_chrg_amt_profit_ly,
                                    outer_query.up_chrg_amt_profit_ty,
                                    outer_query.up_chrg_amt_exp_ly,
                                    outer_query.up_chrg_amt_exp_ty,
                                    outer_query.wo_activity_upd_inv_ly,
                                    outer_query.wo_activity_upd_inv_ty,
                                    outer_query.wo_activity_post_fin_ly,
                                    outer_query.wo_activity_post_fin_ty,
                                    outer_query.recoverable_tax_ly,
                                    outer_query.recoverable_tax_ty,
                                    outer_query.markup_retail_ly,
                                    outer_query.markup_retail_ty,
                                    outer_query.franchise_markup_retail_ly,
                                    outer_query.franchise_markup_retail_ty,
                                    outer_query.intercompany_markup_ly,
                                    outer_query.intercompany_markup_ty,
                                    outer_query.markup_can_retail_ly,
                                    outer_query.markup_can_retail_ty,
                                    outer_query.rtv_cost_ly,
                                    outer_query.rtv_cost_ty,
                                    outer_query.rtv_retail_ly,
                                    outer_query.rtv_retail_ty,
                                    outer_query.tsf_out_cost_ly,
                                    outer_query.tsf_out_cost_ty,
                                    outer_query.tsf_out_retail_ly,
                                    outer_query.tsf_out_retail_ty,
                                    outer_query.tsf_out_book_cost_ly,
                                    outer_query.tsf_out_book_cost_ty,
                                    outer_query.tsf_out_book_retail_ly,
                                    outer_query.tsf_out_book_retail_ty,
                                    outer_query.intercompany_out_cost_ly,
                                    outer_query.intercompany_out_cost_ty,
                                    outer_query.intercompany_out_retail_ly,
                                    outer_query.intercompany_out_retail_ty,
                                    outer_query.reclass_out_cost_ly,
                                    outer_query.reclass_out_cost_ty,
                                    outer_query.reclass_out_retail_ly,
                                    outer_query.reclass_out_retail_ty,
                                    outer_query.net_sales_cost_ly,
                                    outer_query.net_sales_cost_ty,
                                    outer_query.net_sales_retail_ly,
                                    outer_query.net_sales_retail_ty,
                                    outer_query.returns_cost_ly,
                                    outer_query.returns_cost_ty,
                                    outer_query.returns_retail_ly,
                                    outer_query.returns_retail_ty,
                                    outer_query.weight_variance_retail_ly,
                                    outer_query.weight_variance_retail_ty,
                                    outer_query.empl_disc_retail_ly,
                                    outer_query.empl_disc_retail_ty,
                                    outer_query.franchise_sales_cost_ly,
                                    outer_query.franchise_sales_cost_ty,
                                    outer_query.franchise_sales_retail_ly,
                                    outer_query.franchise_sales_retail_ty,
                                    outer_query.freight_claim_cost_ly,
                                    outer_query.freight_claim_cost_ty,
                                    outer_query.freight_claim_retail_ly,
                                    outer_query.freight_claim_retail_ty,
                                    outer_query.stock_adj_cogs_cost_ly,
                                    outer_query.stock_adj_cogs_cost_ty,
                                    outer_query.stock_adj_cogs_retail_ly,
                                    outer_query.stock_adj_cogs_retail_ty,
                                    outer_query.stock_adj_cost_ly,
                                    outer_query.stock_adj_cost_ty,
                                    outer_query.stock_adj_retail_ly,
                                    outer_query.stock_adj_retail_ty,
                                    outer_query.shrinkage_cost_ly,
                                    outer_query.shrinkage_cost_ty,
                                    outer_query.shrinkage_retail_ly,
                                    outer_query.shrinkage_retail_ty,
                                    outer_query.cost_variance_amt_ly,
                                    outer_query.cost_variance_amt_ty,
                                    outer_query.margin_cost_variance_ly,
                                    outer_query.margin_cost_variance_ty,
                                    outer_query.perm_markdown_retail_ly,
                                    outer_query.perm_markdown_retail_ty,
                                    outer_query.prom_markdown_retail_ly,
                                    outer_query.prom_markdown_retail_ty,
                                    outer_query.clear_markdown_retail_ly,
                                    outer_query.clear_markdown_retail_ty,
                                    outer_query.intercompany_markdown_ly,
                                    outer_query.intercompany_markdown_ty,
                                    outer_query.franchise_markdown_retail_ly,
                                    outer_query.franchise_markdown_retail_ty,
                                    outer_query.markdown_can_retail_ly,
                                    outer_query.markdown_can_retail_ty,
                                    outer_query.cls_stk_cost_ly,
                                    outer_query.cls_stk_cost_ty,
                                    outer_query.cls_stk_retail_ly,
                                    outer_query.cls_stk_retail_ty,
                                    outer_query.add_cost_ly,
                                    outer_query.add_cost_ty,
                                    outer_query.add_retail_ly,
                                    outer_query.add_retail_ty,
                                    outer_query.shrink_cost_pct_ly,
                                    outer_query.shrink_cost_pct_ty,
                                    outer_query.htd_gafs_retail_ly,
                                    outer_query.htd_gafs_retail_ty,
                                    outer_query.htd_gafs_cost_ly,
                                    outer_query.htd_gafs_cost_ty,
                                    outer_query.red_retail_ly,
                                    outer_query.red_retail_ty,
                                    outer_query.deal_income_sales_ly,
                                    outer_query.deal_income_sales_ty,
                                    outer_query.deal_income_purch_ly,
                                    outer_query.deal_income_purch_ty,
                                    outer_query.cum_markon_pct_ly,
                                    outer_query.cum_markon_pct_ty,
                                    outer_query.gross_margin_amt_ly,
                                    outer_query.gross_margin_amt_ty,
                                    outer_query.vat_in_ly,
                                    outer_query.vat_in_ty,
                                    outer_query.vat_out_ly,
                                    outer_query.vat_out_ty,
                                    outer_query.restocking_fee_ly,
                                    outer_query.restocking_fee_ty,
                                    outer_query.freight_cost_ly,
                                    outer_query.freight_cost_ty,
                                    outer_query.rec_cost_adj_variance_ly,
                                    outer_query.rec_cost_adj_variance_ty,
                                    outer_query.workroom_amt_ly,
                                    outer_query.workroom_amt_ty,
                                    outer_query.cash_disc_amt_ly,
                                    outer_query.cash_disc_amt_ty,
                                    outer_query.retail_cost_variance_ly,
                                    outer_query.retail_cost_variance_ty,
                                    outer_query.franchise_restocking_fee_ly,
                                    outer_query.franchise_restocking_fee_ty,
                                    outer_query.intercompany_margin_ly,
                                    outer_query.intercompany_margin_ty,
                                    outer_query.net_markdown_ly,
                                    outer_query.net_markdown_ty,
                                    outer_query.net_markup_ly,
                                    outer_query.net_markup_ty,
                                    outer_query.net_reclass_cost_ly,
                                    outer_query.net_reclass_cost_ty,
                                    outer_query.net_reclass_retail_ly,
                                    outer_query.net_reclass_retail_ty,
                                    outer_query.net_sales_retail_ex_vat_ly,
                                    outer_query.net_sales_retail_ex_vat_ty,
                                    outer_query.net_sale_noninv_r_exvat_ly,
                                    outer_query.net_sale_noninv_r_exvat_ty,
                                    outer_query.net_tsf_cost_ly,
                                    outer_query.net_tsf_cost_ty,
                                    outer_query.net_tsf_retail_ly,
                                    outer_query.net_tsf_retail_ty,
                                    outer_query.shrink_retail_pct_ly,
                                    outer_query.shrink_retail_pct_ty,
                                    outer_query.sales_units_ly,
                                    outer_query.sales_units_ty,
                                    outer_query.net_sales_non_inv_cost_ly,
                                    outer_query.net_sales_non_inv_cost_ty,
                                    outer_query.net_sales_noninv_retail_ly,
                                    outer_query.net_sales_noninv_retail_ty)
        from (select NVL(tbl_crw_ly.dept,tbl_crw_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_crw_ly.class,tbl_crw_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_crw_ly.subclass,tbl_crw_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_crw_ly.loc_type,tbl_crw_ty.loc_type) as loc_type,
                     NVL(tbl_crw_ly.location,tbl_crw_ty.location) as location,
                     NULL                                         as loc_name,--loc_name
                     NULL                                         as currency_code,
                     NULL                                         as eom_date,
                     NVL(tbl_crw_ly.week_no,tbl_crw_ty.week_no)   as week_no,
                     tbl_crw_ly.opn_stk_cost                      as opn_stk_cost_ly,
                     tbl_crw_ty.opn_stk_cost                      as opn_stk_cost_ty,
                     tbl_crw_ly.opn_stk_retail                    as opn_stk_retail_ly,
                     tbl_crw_ty.opn_stk_retail                    as opn_stk_retail_ty,
                     tbl_crw_ly.purch_cost                        as purch_cost_ly,
                     tbl_crw_ty.purch_cost                        as purch_cost_ty,
                     tbl_crw_ly.purch_retail                      as purch_retail_ly,
                     tbl_crw_ty.purch_retail                      as purch_retail_ty,
                     tbl_crw_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_crw_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_crw_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_crw_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_crw_ly.tsf_in_book_cost                  as tsf_in_book_cost_ly,
                     tbl_crw_ty.tsf_in_book_cost                  as tsf_in_book_cost_ty,
                     tbl_crw_ly.tsf_in_book_retail                as tsf_in_book_retail_ly,
                     tbl_crw_ty.tsf_in_book_retail                as tsf_in_book_retail_ty,
                     tbl_crw_ly.intercompany_in_cost              as intercompany_in_cost_ly,
                     tbl_crw_ty.intercompany_in_cost              as intercompany_in_cost_ty,
                     tbl_crw_ly.intercompany_in_retail            as intercompany_in_retail_ly,
                     tbl_crw_ty.intercompany_in_retail            as intercompany_in_retail_ty,
                     tbl_crw_ly.reclass_in_cost                   as reclass_in_cost_ly,
                     tbl_crw_ty.reclass_in_cost                   as reclass_in_cost_ty,
                     tbl_crw_ly.reclass_in_retail                 as reclass_in_retail_ly,
                     tbl_crw_ty.reclass_in_retail                 as reclass_in_retail_ty,
                     tbl_crw_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_crw_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_crw_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_crw_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_crw_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_crw_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_crw_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_crw_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_crw_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_crw_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_crw_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_crw_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_crw_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_crw_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_crw_ly.markup_retail                     as markup_retail_ly,
                     tbl_crw_ty.markup_retail                     as markup_retail_ty,
                     tbl_crw_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_crw_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_crw_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_crw_ty.intercompany_markup               as intercompany_markup_ty,
                     tbl_crw_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_crw_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_crw_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_crw_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_crw_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_crw_ty.rtv_retail                        as rtv_retail_ty,
                     tbl_crw_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_crw_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_crw_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_crw_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_crw_ly.tsf_out_book_cost                 as tsf_out_book_cost_ly,
                     tbl_crw_ty.tsf_out_book_cost                 as tsf_out_book_cost_ty,
                     tbl_crw_ly.tsf_out_book_retail               as tsf_out_book_retail_ly,
                     tbl_crw_ty.tsf_out_book_retail               as tsf_out_book_retail_ty,
                     tbl_crw_ly.intercompany_out_cost             as intercompany_out_cost_ly,
                     tbl_crw_ty.intercompany_out_cost             as intercompany_out_cost_ty,
                     tbl_crw_ly.intercompany_out_retail           as intercompany_out_retail_ly,
                     tbl_crw_ty.intercompany_out_retail           as intercompany_out_retail_ty,
                     tbl_crw_ly.reclass_out_cost                  as reclass_out_cost_ly,
                     tbl_crw_ty.reclass_out_cost                  as reclass_out_cost_ty,
                     tbl_crw_ly.reclass_out_retail                as reclass_out_retail_ly,
                     tbl_crw_ty.reclass_out_retail                as reclass_out_retail_ty,
                     tbl_crw_ly.net_sales_cost                    as net_sales_cost_ly,
                     tbl_crw_ty.net_sales_cost                    as net_sales_cost_ty,
                     tbl_crw_ly.net_sales_retail                  as net_sales_retail_ly,
                     tbl_crw_ty.net_sales_retail                  as net_sales_retail_ty,
                     tbl_crw_ly.returns_cost                      as returns_cost_ly,
                     tbl_crw_ty.returns_cost                      as returns_cost_ty,
                     tbl_crw_ly.returns_retail                    as returns_retail_ly,
                     tbl_crw_ty.returns_retail                    as returns_retail_ty,
                     tbl_crw_ly.weight_variance_retail            as weight_variance_retail_ly,
                     tbl_crw_ty.weight_variance_retail            as weight_variance_retail_ty,
                     tbl_crw_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_crw_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_crw_ly.franchise_sales_cost              as franchise_sales_cost_ly,
                     tbl_crw_ty.franchise_sales_cost              as franchise_sales_cost_ty,
                     tbl_crw_ly.franchise_sales_retail            as franchise_sales_retail_ly,
                     tbl_crw_ty.franchise_sales_retail            as franchise_sales_retail_ty,
                     tbl_crw_ly.freight_claim_cost                as freight_claim_cost_ly,
                     tbl_crw_ty.freight_claim_cost                as freight_claim_cost_ty,
                     tbl_crw_ly.freight_claim_retail              as freight_claim_retail_ly,
                     tbl_crw_ty.freight_claim_retail              as freight_claim_retail_ty,
                     tbl_crw_ly.stock_adj_cogs_cost               as stock_adj_cogs_cost_ly,
                     tbl_crw_ty.stock_adj_cogs_cost               as stock_adj_cogs_cost_ty,
                     tbl_crw_ly.stock_adj_cogs_retail             as stock_adj_cogs_retail_ly,
                     tbl_crw_ty.stock_adj_cogs_retail             as stock_adj_cogs_retail_ty,
                     tbl_crw_ly.stock_adj_cost                    as stock_adj_cost_ly,
                     tbl_crw_ty.stock_adj_cost                    as stock_adj_cost_ty,
                     tbl_crw_ly.stock_adj_retail                  as stock_adj_retail_ly,
                     tbl_crw_ty.stock_adj_retail                  as stock_adj_retail_ty,
                     tbl_crw_ly.shrinkage_cost                    as shrinkage_cost_ly,
                     tbl_crw_ty.shrinkage_cost                    as shrinkage_cost_ty,
                     tbl_crw_ly.shrinkage_retail                  as shrinkage_retail_ly,
                     tbl_crw_ty.shrinkage_retail                  as shrinkage_retail_ty,
                     tbl_crw_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_crw_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_crw_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_crw_ty.margin_cost_variance              as margin_cost_variance_ty,
                     tbl_crw_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_crw_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_crw_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_crw_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_crw_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_crw_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_crw_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_crw_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_crw_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_crw_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_crw_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_crw_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_crw_ly.cls_stk_cost                      as cls_stk_cost_ly,
                     tbl_crw_ty.cls_stk_cost                      as cls_stk_cost_ty,
                     tbl_crw_ly.cls_stk_retail                    as cls_stk_retail_ly,
                     tbl_crw_ty.cls_stk_retail                    as cls_stk_retail_ty,
                     NULL                                         as add_cost_ly, -- tbl_crw_ly.add_cost
                     NULL                                         as add_cost_ty, -- tbl_crw_ty.add_cost
                     NULL                                         as add_retail_ly,--tbl_crw_ly.add_retail
                     NULL                                         as add_retail_ty, --tbl_crw_ty.add_retail 
                     NULL                                         as shrink_cost_pct_ly, --tbl_crw_ly.shrink_cost_pct
                     NULL                                         as shrink_cost_pct_ty, --tbl_crw_ty.shrink_cost_pct
                     tbl_crw_ly.htd_gafs_retail                   as htd_gafs_retail_ly,
                     tbl_crw_ty.htd_gafs_retail                   as htd_gafs_retail_ty,
                     tbl_crw_ly.htd_gafs_cost                     as htd_gafs_cost_ly,
                     tbl_crw_ty.htd_gafs_cost                     as htd_gafs_cost_ty,
                     NULL                                         as red_retail_ly, --tbl_crw_ly.red_retail
                     NULL                                         as red_retail_ty, --tbl_crw_ty.red_retail
                     tbl_crw_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_crw_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_crw_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_crw_ty.deal_income_purch                 as deal_income_purch_ty,
                     NULL                                         as cum_markon_pct_ly, --tbl_crw_ly.cum_markon_pct
                     NULL                                         as cum_markon_pct_ty, --tbl_crw_ty.cum_markon_pct
                     tbl_crw_ly.gross_margin_amt                  as gross_margin_amt_ly,
                     tbl_crw_ty.gross_margin_amt                  as gross_margin_amt_ty,
                     tbl_crw_ly.vat_in                            as vat_in_ly,
                     tbl_crw_ty.vat_in                            as vat_in_ty,
                     tbl_crw_ly.vat_out                           as vat_out_ly,
                     tbl_crw_ty.vat_out                           as vat_out_ty,
                     tbl_crw_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_crw_ty.restocking_fee                    as restocking_fee_ty,
                     tbl_crw_ly.freight_cost                      as freight_cost_ly,
                     tbl_crw_ty.freight_cost                      as freight_cost_ty,
                     tbl_crw_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_crw_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     tbl_crw_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_crw_ty.workroom_amt                      as workroom_amt_ty,
                     tbl_crw_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_crw_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_crw_ly.retail_cost_variance              as retail_cost_variance_ly,
                     tbl_crw_ty.retail_cost_variance              as retail_cost_variance_ty,
                     tbl_crw_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_crw_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_crw_ly.intercompany_margin               as intercompany_margin_ly,
                     tbl_crw_ty.intercompany_margin               as intercompany_margin_ty,
                     NULL                                         as net_markdown_ly, --tbl_crw_ly.net_markdown
                     NULL                                         as net_markdown_ty, --tbl_crw_ty.net_markdown
                     NULL                                         as net_markup_ly, --tbl_crw_ly.net_markup
                     NULL                                         as net_markup_ty, --tbl_crw_ty.net_markup
                     NULL                                         as net_reclass_cost_ly, --tbl_crw_ly.net_reclass_cost
                     NULL                                         as net_reclass_cost_ty, --tbl_crw_ty.net_reclass_cost
                     NULL                                         as net_reclass_retail_ly, --tbl_crw_ly.net_reclass_retail
                     NULL                                         as net_reclass_retail_ty, --tbl_crw_ty.net_reclass_retail
                     tbl_crw_ly.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ly,
                     tbl_crw_ty.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ty,
                     tbl_crw_ly.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ly,
                     tbl_crw_ty.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ty,
                     NULL                                         as net_tsf_cost_ly, --tbl_crw_ly.net_tsf_cost
                     NULL                                         as net_tsf_cost_ty, --tbl_crw_ty.net_tsf_cost
                     NULL                                         as net_tsf_retail_ly, --tbl_crw_ly.net_tsf_retail
                     NULL                                         as net_tsf_retail_ty, --tbl_crw_ty.net_tsf_retail
                     NULL                                         as shrink_retail_pct_ly, --tbl_crw_ly.shrink_retail_pct
                     NULL                                         as shrink_retail_pct_ty, --tbl_crw_ty.shrink_retail_pct
                     tbl_crw_ly.sales_units                       as sales_units_ly,
                     tbl_crw_ty.sales_units                       as sales_units_ty,
                     tbl_crw_ly.net_sales_non_inv_cost            as net_sales_non_inv_cost_ly,
                     tbl_crw_ty.net_sales_non_inv_cost            as net_sales_non_inv_cost_ty,
                     tbl_crw_ly.net_sales_non_inv_retail          as net_sales_noninv_retail_ly,
                     tbl_crw_ty.net_sales_non_inv_retail          as net_sales_noninv_retail_ty
                from table(cast(L_tbl_cr_week_data_ly as OBJ_WEEK_DATA_TBL)) tbl_crw_ly 
                full outer join table(cast(L_tbl_cr_week_data_ty as OBJ_WEEK_DATA_TBL)) tbl_crw_ty 
                  ON tbl_crw_ly.dept         =  tbl_crw_ty.dept
                     and tbl_crw_ly.class    =  tbl_crw_ty.class 
                     and tbl_crw_ly.subclass =  tbl_crw_ty.subclass
                     and tbl_crw_ly.loc_type =  tbl_crw_ty.loc_type
                     and tbl_crw_ly.location =  tbl_crw_ty.location
                     and tbl_crw_ly.half_no  =  tbl_crw_ty.half_no
                     and tbl_crw_ly.week_no  =  tbl_crw_ty.week_no
                     and tbl_crw_ly.eow_date =  tbl_crw_ty.eow_date) outer_query;    
BEGIN
   LP_cr_week_data_tbl   := new OBJ_CR_WEEK_DATA_TBL();
   L_tbl_cr_week_data_ty := new OBJ_WEEK_DATA_TBL();
   L_tbl_cr_week_data_ly := new OBJ_WEEK_DATA_TBL();
   
   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;                       
   if STKLEDGR_QUERY_SQL.FETCH_WEEK_DATA_BULK(LP_sch_crit_rec.dept,
                                              LP_sch_crit_rec.class,
                                              LP_sch_crit_rec.subclass,
                                              'T',
                                              LP_sch_crit_rec.period_date,
                                              LP_loc_type,
                                              LP_location,
                                              'Y', --:B_search.CB_thousands,
                                              LP_currency_ind,
                                              LP_set_of_books_id,
                                              L_tbl_cr_week_data_ty,
                                              O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if STKLEDGR_QUERY_SQL.FETCH_WEEK_DATA_BULK(LP_sch_crit_rec.dept,
                                              LP_sch_crit_rec.class,
                                              LP_sch_crit_rec.subclass,
                                              'L',
                                              LP_sch_crit_rec.period_date,
                                              LP_loc_type,
                                              LP_location,
                                              'Y', --:B_search.CB_thousands,
                                              LP_currency_ind,
                                              LP_set_of_books_id,
                                              L_tbl_cr_week_data_ly,
                                              O_error_message) = FALSE then
      return FALSE;
   end if;
   
   open  C_CR_WEEK_DATA_INFO;
   fetch C_CR_WEEK_DATA_INFO BULK COLLECT into LP_cr_week_data_tbl;
   close C_CR_WEEK_DATA_INFO;
  
   for i in 1..LP_cr_week_data_tbl.count
   loop
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_cr_week_data_tbl(i).dept,
                                  LP_cr_week_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_cr_week_data_tbl(i).dept,
                                   LP_cr_week_data_tbl(i).class,
                                   LP_cr_week_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_cr_week_data_tbl(i).dept,
                                       LP_cr_week_data_tbl(i).class,
                                       LP_cr_week_data_tbl(i).subclass,
                                       LP_cr_week_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;     
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_cr_week_data_tbl(i).loc_name,
                                       LP_cr_week_data_tbl(i).location,
                                       LP_cr_week_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_cr_week_data_tbl(i).loc_type,
                                    LP_cr_week_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_cr_week_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_cr_week_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_cr_week_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_cr_week_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;
      LP_cr_week_data_tbl(i).eom_date := LP_sch_crit_rec.period_date;      
     -- CALCULATE_FIELDS
      L_net_tsf_cost_ly := LP_cr_week_data_tbl(i).tsf_in_cost_ly - LP_cr_week_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_cr_week_data_tbl(i).tsf_in_cost_ty - LP_cr_week_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_cr_week_data_tbl(i).tsf_in_retail_ly - LP_cr_week_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_cr_week_data_tbl(i).tsf_in_retail_ty - LP_cr_week_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_cr_week_data_tbl(i).markup_retail_ly - LP_cr_week_data_tbl(i).markup_can_retail_ly + LP_cr_week_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_cr_week_data_tbl(i).markup_retail_ty - LP_cr_week_data_tbl(i).markup_can_retail_ty + LP_cr_week_data_tbl(i).franchise_markup_retail_ty;
      
      L_net_markdown_retail_ly := LP_cr_week_data_tbl(i).clear_markdown_retail_ly + LP_cr_week_data_tbl(i).perm_markdown_retail_ly + LP_cr_week_data_tbl(i).prom_markdown_retail_ly - LP_cr_week_data_tbl(i).markdown_can_retail_ly + LP_cr_week_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_cr_week_data_tbl(i).clear_markdown_retail_ty + LP_cr_week_data_tbl(i).perm_markdown_retail_ty + LP_cr_week_data_tbl(i).prom_markdown_retail_ty - LP_cr_week_data_tbl(i).markdown_can_retail_ty + LP_cr_week_data_tbl(i).franchise_markdown_retail_ty;
            
      L_add_cost_ly := LP_cr_week_data_tbl(i).purch_cost_ly - LP_cr_week_data_tbl(i).rtv_cost_ly + LP_cr_week_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_cr_week_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_cr_week_data_tbl(i).purch_cost_ty - LP_cr_week_data_tbl(i).rtv_cost_ty + LP_cr_week_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_cr_week_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_cr_week_data_tbl(i).purch_retail_ly - LP_cr_week_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_cr_week_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_cr_week_data_tbl(i).purch_retail_ty - LP_cr_week_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_cr_week_data_tbl(i).franchise_returns_retail_ty;
      
      L_red_retail_ly   := LP_cr_week_data_tbl(i).net_sales_retail_ly + LP_cr_week_data_tbl(i).shrinkage_retail_ly + L_net_markdown_retail_ly + LP_cr_week_data_tbl(i).empl_disc_retail_ly + LP_cr_week_data_tbl(i).weight_variance_retail_ly + LP_cr_week_data_tbl(i).franchise_markdown_retail_ly + LP_cr_week_data_tbl(i).franchise_sales_retail_ly - LP_cr_week_data_tbl(i).franchise_returns_retail_ly;
      L_red_retail_ty   := LP_cr_week_data_tbl(i).net_sales_retail_ty + LP_cr_week_data_tbl(i).shrinkage_retail_ty + L_net_markdown_retail_ty + LP_cr_week_data_tbl(i).empl_disc_retail_ty + LP_cr_week_data_tbl(i).weight_variance_retail_ty + LP_cr_week_data_tbl(i).franchise_markdown_retail_ty + LP_cr_week_data_tbl(i).franchise_sales_retail_ty - LP_cr_week_data_tbl(i).franchise_returns_retail_ty;

      L_net_reclass_cost_ly   := LP_cr_week_data_tbl(i).reclass_in_cost_ly - LP_cr_week_data_tbl(i).reclass_out_cost_ly;          
      L_net_reclass_cost_ty   := LP_cr_week_data_tbl(i).reclass_in_cost_ty - LP_cr_week_data_tbl(i).reclass_out_cost_ty;

      L_net_reclass_retail_ly := LP_cr_week_data_tbl(i).reclass_in_retail_ly - LP_cr_week_data_tbl(i).reclass_out_retail_ly;    
      L_net_reclass_retail_ty := LP_cr_week_data_tbl(i).reclass_in_retail_ty - LP_cr_week_data_tbl(i).reclass_out_retail_ty;

      if (LP_cr_week_data_tbl(i).net_sales_cost_ly is NULL and LP_cr_week_data_tbl(i).franchise_sales_cost_ly is NULL and LP_cr_week_data_tbl(i).franchise_returns_cost_ly is NULL) 
          or (LP_cr_week_data_tbl(i).net_sales_cost_ly = 0 and (LP_cr_week_data_tbl(i).franchise_sales_cost_ly - LP_cr_week_data_tbl(i).franchise_returns_cost_ly) = 0) then
         L_shrink_cost_pct_ly := 0;
      else
         L_shrink_cost_pct_ly := ((LP_cr_week_data_tbl(i).shrinkage_cost_ly / (LP_cr_week_data_tbl(i).net_sales_cost_ly + LP_cr_week_data_tbl(i).franchise_sales_cost_ly - LP_cr_week_data_tbl(i).franchise_returns_cost_ly))*100);
      end if;
      
      if (LP_cr_week_data_tbl(i).net_sales_cost_ty is NULL and LP_cr_week_data_tbl(i).franchise_sales_cost_ty is NULL and LP_cr_week_data_tbl(i).franchise_returns_cost_ty is NULL) 
         or (LP_cr_week_data_tbl(i).net_sales_cost_ty = 0 and (LP_cr_week_data_tbl(i).franchise_sales_cost_ty - LP_cr_week_data_tbl(i).franchise_returns_cost_ty) = 0) then
         L_shrink_cost_pct_ty := 0;
      else
         L_shrink_cost_pct_ty := ((LP_cr_week_data_tbl(i).shrinkage_cost_ty / (LP_cr_week_data_tbl(i).net_sales_cost_ty + LP_cr_week_data_tbl(i).franchise_sales_cost_ty - LP_cr_week_data_tbl(i).franchise_returns_cost_ty))*100);
      end if;

      if (LP_cr_week_data_tbl(i).net_sales_retail_ly is NULL and LP_cr_week_data_tbl(i).franchise_sales_retail_ly is NULL and LP_cr_week_data_tbl(i).franchise_returns_retail_ly is NULL) 
   	   or (LP_cr_week_data_tbl(i).net_sales_retail_ly = 0 and (LP_cr_week_data_tbl(i).franchise_sales_retail_ly - LP_cr_week_data_tbl(i).franchise_returns_retail_ly) = 0) then
         L_shrink_retail_pct_ly := 0;
      else
         L_shrink_retail_pct_ly := ((LP_cr_week_data_tbl(i).shrinkage_retail_ly / (LP_cr_week_data_tbl(i).net_sales_retail_ly + LP_cr_week_data_tbl(i).franchise_sales_retail_ly - LP_cr_week_data_tbl(i).franchise_returns_retail_ly))*100);
      end if;
     
      if (LP_cr_week_data_tbl(i).net_sales_retail_ty is NULL and LP_cr_week_data_tbl(i).franchise_sales_retail_ty is NULL and LP_cr_week_data_tbl(i).franchise_returns_retail_ty is NULL) 
   	   or (LP_cr_week_data_tbl(i).net_sales_retail_ty = 0 and (LP_cr_week_data_tbl(i).franchise_sales_retail_ty - LP_cr_week_data_tbl(i).franchise_returns_retail_ty) = 0) then
         L_shrink_retail_pct_ty := 0;
      else
         L_shrink_retail_pct_ty := ((LP_cr_week_data_tbl(i).shrinkage_retail_ty / (LP_cr_week_data_tbl(i).net_sales_retail_ty + LP_cr_week_data_tbl(i).franchise_sales_retail_ty - LP_cr_week_data_tbl(i).franchise_returns_retail_ty))*100);
      end if;      
      
      if LP_cr_week_data_tbl(i).htd_gafs_retail_ly is NULL or LP_cr_week_data_tbl(i).htd_gafs_retail_ly = 0 then
         L_cum_markon_pct_ly := 0;
      else
         L_cum_markon_pct_ly := ((1 - (LP_cr_week_data_tbl(i).htd_gafs_cost_ly / LP_cr_week_data_tbl(i).htd_gafs_retail_ly)) * 100);
      end if;

      if LP_cr_week_data_tbl(i).htd_gafs_retail_ty is NULL or LP_cr_week_data_tbl(i).htd_gafs_retail_ty = 0 then
         L_cum_markon_pct_ty := 0;
      else
         L_cum_markon_pct_ty := ((1 - (LP_cr_week_data_tbl(i).htd_gafs_cost_ty / LP_cr_week_data_tbl(i).htd_gafs_retail_ty)) * 100);
      end if; 
      
      LP_cr_week_data_tbl(i).add_cost_ly                   := L_add_cost_ly; 
      LP_cr_week_data_tbl(i).add_cost_ty                   := L_add_cost_ty;
      LP_cr_week_data_tbl(i).add_retail_ly                 := L_add_retail_ly;
      LP_cr_week_data_tbl(i).add_retail_ty                 := L_add_retail_ty;
      LP_cr_week_data_tbl(i).red_retail_ly                 := L_red_retail_ly;
      LP_cr_week_data_tbl(i).red_retail_ty                 := L_red_retail_ty;
      LP_cr_week_data_tbl(i).shrink_cost_pct_ly            := L_shrink_cost_pct_ly;
      LP_cr_week_data_tbl(i).shrink_cost_pct_ty            := L_shrink_cost_pct_ty;
      LP_cr_week_data_tbl(i).shrink_retail_pct_ly          := L_shrink_retail_pct_ly;
      LP_cr_week_data_tbl(i).shrink_retail_pct_ty          := L_shrink_retail_pct_ty;
      LP_cr_week_data_tbl(i).net_markdown_ly               := L_net_markdown_retail_ly;
      LP_cr_week_data_tbl(i).net_markdown_ty               := L_net_markdown_retail_ty;
      LP_cr_week_data_tbl(i).net_markup_ly                 := L_net_markup_retail_ly;
      LP_cr_week_data_tbl(i).net_markup_ty                 := L_net_markup_retail_ty;
      LP_cr_week_data_tbl(i).net_reclass_cost_ly           := L_net_reclass_cost_ly;
      LP_cr_week_data_tbl(i).net_reclass_cost_ty           := L_net_reclass_cost_ty;
      LP_cr_week_data_tbl(i).net_reclass_retail_ly         := L_net_reclass_retail_ly;
      LP_cr_week_data_tbl(i).net_reclass_retail_ty         := L_net_reclass_retail_ty;
      LP_cr_week_data_tbl(i).net_tsf_cost_ly               := L_net_tsf_cost_ly;
      LP_cr_week_data_tbl(i).net_tsf_cost_ty               := L_net_tsf_cost_ty;
      LP_cr_week_data_tbl(i).net_tsf_retail_ly             := L_net_tsf_retail_ly;
      LP_cr_week_data_tbl(i).net_tsf_retail_ty             := L_net_tsf_retail_ty;
      LP_cr_week_data_tbl(i).cum_markon_pct_ly             := L_cum_markon_pct_ly;
      LP_cr_week_data_tbl(i).cum_markon_pct_ty             := L_cum_markon_pct_ty;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CR_WEEK_DATA_INFO%ISOPEN then
         close C_CR_WEEK_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_CR_WEEK_DATA;
--------------------------------------------------------------------------------
FUNCTION FETCH_CR_DAILY_DATA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   
   L_program                VARCHAR2(75) := 'CORESVC_STKLDGRV.FETCH_CR_DAILY_DATA';
   L_tbl_cr_daily_data_ty   OBJ_DAILY_DATA_TBL;
   L_tbl_cr_daily_data_ly   OBJ_DAILY_DATA_TBL;
   L_add_cost_ly            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_cost_ty            MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ly          MONTH_DATA.TSF_IN_COST%TYPE;
   L_add_retail_ty          MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ly        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_cost_ty        MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ly      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_tsf_retail_ty      MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ly   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markup_retail_ty   MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ly MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_markdown_retail_ty MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ly    MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_cost_ty    MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ly  MONTH_DATA.TSF_IN_COST%TYPE;
   L_net_reclass_retail_ty  MONTH_DATA.TSF_IN_COST%TYPE;
   
   cursor C_CR_DAILY_DATA_INFO is
      select OBJ_CR_DAILY_DATA_REC( outer_query.dept,
                                    outer_query.dept_name,
                                    outer_query.class,
                                    outer_query.class_name,
                                    outer_query.subclass,
                                    outer_query.subclass_name,
                                    outer_query.loc_type,
                                    outer_query.location,
                                    outer_query.loc_name,
                                    outer_query.currency_code,
                                    outer_query.eow_date,
                                    outer_query.day_no,
                                    outer_query.purch_cost_ly,
                                    outer_query.purch_cost_ty,
                                    outer_query.purch_retail_ly,
                                    outer_query.purch_retail_ty,
                                    outer_query.tsf_in_cost_ly,
                                    outer_query.tsf_in_cost_ty,
                                    outer_query.tsf_in_retail_ly,
                                    outer_query.tsf_in_retail_ty,
                                    outer_query.tsf_in_book_cost_ly,
                                    outer_query.tsf_in_book_cost_ty,
                                    outer_query.tsf_in_book_retail_ly,
                                    outer_query.tsf_in_book_retail_ty,
                                    outer_query.intercompany_in_cost_ly,
                                    outer_query.intercompany_in_cost_ty,
                                    outer_query.intercompany_in_retail_ly,
                                    outer_query.intercompany_in_retail_ty,
                                    outer_query.reclass_in_cost_ly,
                                    outer_query.reclass_in_cost_ty,
                                    outer_query.reclass_in_retail_ly,
                                    outer_query.reclass_in_retail_ty,
                                    outer_query.franchise_returns_cost_ly,
                                    outer_query.franchise_returns_cost_ty,
                                    outer_query.franchise_returns_retail_ly,
                                    outer_query.franchise_returns_retail_ty,
                                    outer_query.up_chrg_amt_profit_ly,
                                    outer_query.up_chrg_amt_profit_ty,
                                    outer_query.up_chrg_amt_exp_ly,
                                    outer_query.up_chrg_amt_exp_ty,
                                    outer_query.wo_activity_upd_inv_ly,
                                    outer_query.wo_activity_upd_inv_ty,
                                    outer_query.wo_activity_post_fin_ly,
                                    outer_query.wo_activity_post_fin_ty,
                                    outer_query.recoverable_tax_ly,
                                    outer_query.recoverable_tax_ty,
                                    outer_query.rtv_cost_ly,
                                    outer_query.rtv_cost_ty,
                                    outer_query.rtv_retail_ly,
                                    outer_query.rtv_retail_ty,
                                    outer_query.tsf_out_cost_ly,
                                    outer_query.tsf_out_cost_ty,
                                    outer_query.tsf_out_retail_ly,
                                    outer_query.tsf_out_retail_ty,
                                    outer_query.tsf_out_book_cost_ly,
                                    outer_query.tsf_out_book_cost_ty,
                                    outer_query.tsf_out_book_retail_ly,
                                    outer_query.tsf_out_book_retail_ty,
                                    outer_query.intercompany_out_cost_ly,
                                    outer_query.intercompany_out_cost_ty,
                                    outer_query.intercompany_out_retail_ly,
                                    outer_query.intercompany_out_retail_ty,
                                    outer_query.reclass_out_cost_ly,
                                    outer_query.reclass_out_cost_ty,
                                    outer_query.reclass_out_retail_ly,
                                    outer_query.reclass_out_retail_ty,
                                    outer_query.net_sales_cost_ly,
                                    outer_query.net_sales_cost_ty,
                                    outer_query.net_sales_retail_ly,
                                    outer_query.net_sales_retail_ty,
                                    outer_query.returns_cost_ly,
                                    outer_query.returns_cost_ty,
                                    outer_query.returns_retail_ly,
                                    outer_query.returns_retail_ty,
                                    outer_query.franchise_sales_cost_ly,
                                    outer_query.franchise_sales_cost_ty,
                                    outer_query.franchise_sales_retail_ly,
                                    outer_query.franchise_sales_retail_ty,
                                    outer_query.weight_variance_retail_ly,
                                    outer_query.weight_variance_retail_ty,
                                    outer_query.freight_claim_cost_ly,
                                    outer_query.freight_claim_cost_ty,
                                    outer_query.freight_claim_retail_ly,
                                    outer_query.freight_claim_retail_ty,
                                    outer_query.stock_adj_cogs_cost_ly,
                                    outer_query.stock_adj_cogs_cost_ty,
                                    outer_query.stock_adj_cogs_retail_ly,
                                    outer_query.stock_adj_cogs_retail_ty,
                                    outer_query.stock_adj_cost_ly,
                                    outer_query.stock_adj_cost_ty,
                                    outer_query.stock_adj_retail_ly,
                                    outer_query.stock_adj_retail_ty,
                                    outer_query.cost_variance_amt_ly,
                                    outer_query.cost_variance_amt_ty,
                                    outer_query.retail_cost_variance_ly,
                                    outer_query.retail_cost_variance_ty,
                                    outer_query.margin_cost_variance_ly,
                                    outer_query.margin_cost_variance_ty,
                                    outer_query.vat_in_ly,
                                    outer_query.vat_in_ty,
                                    outer_query.vat_out_ly,
                                    outer_query.vat_out_ty,
                                    outer_query.deal_income_sales_ly,
                                    outer_query.deal_income_sales_ty,
                                    outer_query.deal_income_purch_ly,
                                    outer_query.deal_income_purch_ty,
                                    outer_query.restocking_fee_ly,
                                    outer_query.restocking_fee_ty,
                                    outer_query.franchise_restocking_fee_ly,
                                    outer_query.franchise_restocking_fee_ty,
                                    outer_query.net_sales_non_inv_cost_ly,
                                    outer_query.net_sales_non_inv_cost_ty,
                                    outer_query.add_cost_ly,
                                    outer_query.add_cost_ty,
                                    outer_query.add_retail_ly,
                                    outer_query.add_retail_ty,
                                    outer_query.cash_disc_amt_ly,
                                    outer_query.cash_disc_amt_ty,
                                    outer_query.clear_markdown_retail_ly,
                                    outer_query.clear_markdown_retail_ty,
                                    outer_query.empl_disc_retail_ly,
                                    outer_query.empl_disc_retail_ty,
                                    outer_query.franchise_markdown_retail_ly,
                                    outer_query.franchise_markdown_retail_ty,
                                    outer_query.franchise_markup_retail_ly,
                                    outer_query.franchise_markup_retail_ty,
                                    outer_query.freight_cost_ly,
                                    outer_query.freight_cost_ty,
                                    outer_query.intercompany_markdown_ly,
                                    outer_query.intercompany_markdown_ty,
                                    outer_query.intercompany_markup_ly,
                                    outer_query.intercompany_markup_ty,
                                    outer_query.net_markdown_ly,
                                    outer_query.net_markdown_ty,
                                    outer_query.markdown_can_retail_ly,
                                    outer_query.markdown_can_retail_ty, 
                                    outer_query.markup_can_retail_ly,
                                    outer_query.markup_can_retail_ty,
                                    outer_query.markup_retail_ly,
                                    outer_query.markup_retail_ty,
                                    outer_query.perm_markdown_retail_ly,
                                    outer_query.perm_markdown_retail_ty,
                                    outer_query.prom_markdown_retail_ly,
                                    outer_query.prom_markdown_retail_ty,
                                    outer_query.rec_cost_adj_variance_ly,
                                    outer_query.rec_cost_adj_variance_ty,
                                    outer_query.sales_units_ly,
                                    outer_query.sales_units_ty,
                                    outer_query.workroom_amt_ly,
                                    outer_query.workroom_amt_ty, 
                                    outer_query.net_reclass_cost_ly,
                                    outer_query.net_reclass_cost_ty,
                                    outer_query.net_reclass_retail_ly,
                                    outer_query.net_reclass_retail_ty,
                                    outer_query.net_tsf_cost_ly,
                                    outer_query.net_tsf_cost_ty, 
                                    outer_query.net_markup_ly,
                                    outer_query.net_markup_ty,
                                    outer_query.net_sales_retail_ex_vat_ly,
                                    outer_query.net_sales_retail_ex_vat_ty,
                                    outer_query.net_sale_noninv_r_exvat_ly,
                                    outer_query.net_sale_noninv_r_exvat_ty,
                                    outer_query.net_sales_noninv_retail_ly,
                                    outer_query.net_sales_noninv_retail_ty,
                                    outer_query.net_tsf_retail_ly,
                                    outer_query.net_tsf_retail_ty)
        from (select NVL(tbl_crd_ly.dept,tbl_crd_ty.dept)         as dept,
                     NULL                                         as dept_name, -- dept_name
                     NVL(tbl_crd_ly.class,tbl_crd_ty.class)       as class,
                     NULL                                         as class_name, -- class_name
                     NVL(tbl_crd_ly.subclass,tbl_crd_ty.subclass) as subclass,
                     NULL                                         as subclass_name, -- subclass_name
                     NVL(tbl_crd_ly.loc_type,tbl_crd_ty.loc_type) as loc_type,
                     NVL(tbl_crd_ly.location,tbl_crd_ty.location) as location,
                     NULL                                         as loc_name,--loc_name
                     NULL                                         as currency_code,
                     NULL                                         as eow_date,
                     NVL(tbl_crd_ly.day_no,tbl_crd_ty.day_no)   as day_no,
                     tbl_crd_ly.purch_cost                        as purch_cost_ly,
                     tbl_crd_ty.purch_cost                        as purch_cost_ty,
                     tbl_crd_ly.purch_retail                      as purch_retail_ly,
                     tbl_crd_ty.purch_retail                      as purch_retail_ty,
                     tbl_crd_ly.tsf_in_cost                       as tsf_in_cost_ly,
                     tbl_crd_ty.tsf_in_cost                       as tsf_in_cost_ty,
                     tbl_crd_ly.tsf_in_retail                     as tsf_in_retail_ly,
                     tbl_crd_ty.tsf_in_retail                     as tsf_in_retail_ty,
                     tbl_crd_ly.tsf_in_book_cost                  as tsf_in_book_cost_ly,
                     tbl_crd_ty.tsf_in_book_cost                  as tsf_in_book_cost_ty,
                     tbl_crd_ly.tsf_in_book_retail                as tsf_in_book_retail_ly,
                     tbl_crd_ty.tsf_in_book_retail                as tsf_in_book_retail_ty,
                     tbl_crd_ly.intercompany_in_cost              as intercompany_in_cost_ly,
                     tbl_crd_ty.intercompany_in_cost              as intercompany_in_cost_ty,
                     tbl_crd_ly.intercompany_in_retail            as intercompany_in_retail_ly,
                     tbl_crd_ty.intercompany_in_retail            as intercompany_in_retail_ty,
                     tbl_crd_ly.reclass_in_cost                   as reclass_in_cost_ly,
                     tbl_crd_ty.reclass_in_cost                   as reclass_in_cost_ty,
                     tbl_crd_ly.reclass_in_retail                 as reclass_in_retail_ly,
                     tbl_crd_ty.reclass_in_retail                 as reclass_in_retail_ty,
                     tbl_crd_ly.franchise_returns_cost            as franchise_returns_cost_ly,
                     tbl_crd_ty.franchise_returns_cost            as franchise_returns_cost_ty,
                     tbl_crd_ly.franchise_returns_retail          as franchise_returns_retail_ly,
                     tbl_crd_ty.franchise_returns_retail          as franchise_returns_retail_ty,
                     tbl_crd_ly.up_chrg_amt_profit                as up_chrg_amt_profit_ly,
                     tbl_crd_ty.up_chrg_amt_profit                as up_chrg_amt_profit_ty,
                     tbl_crd_ly.up_chrg_amt_exp                   as up_chrg_amt_exp_ly,
                     tbl_crd_ty.up_chrg_amt_exp                   as up_chrg_amt_exp_ty,
                     tbl_crd_ly.wo_activity_upd_inv               as wo_activity_upd_inv_ly,
                     tbl_crd_ty.wo_activity_upd_inv               as wo_activity_upd_inv_ty,
                     tbl_crd_ly.wo_activity_post_fin              as wo_activity_post_fin_ly,
                     tbl_crd_ty.wo_activity_post_fin              as wo_activity_post_fin_ty,
                     tbl_crd_ly.recoverable_tax                   as recoverable_tax_ly,
                     tbl_crd_ty.recoverable_tax                   as recoverable_tax_ty,
                     tbl_crd_ly.rtv_cost                          as rtv_cost_ly,
                     tbl_crd_ty.rtv_cost                          as rtv_cost_ty,
                     tbl_crd_ly.rtv_retail                        as rtv_retail_ly,
                     tbl_crd_ty.rtv_retail                        as rtv_retail_ty,
                     tbl_crd_ly.tsf_out_cost                      as tsf_out_cost_ly,
                     tbl_crd_ty.tsf_out_cost                      as tsf_out_cost_ty,
                     tbl_crd_ly.tsf_out_retail                    as tsf_out_retail_ly,
                     tbl_crd_ty.tsf_out_retail                    as tsf_out_retail_ty,
                     tbl_crd_ly.tsf_out_book_cost                 as tsf_out_book_cost_ly,
                     tbl_crd_ty.tsf_out_book_cost                 as tsf_out_book_cost_ty,
                     tbl_crd_ly.tsf_out_book_retail               as tsf_out_book_retail_ly,
                     tbl_crd_ty.tsf_out_book_retail               as tsf_out_book_retail_ty,
                     tbl_crd_ly.intercompany_out_cost             as intercompany_out_cost_ly,
                     tbl_crd_ty.intercompany_out_cost             as intercompany_out_cost_ty,
                     tbl_crd_ly.intercompany_out_retail           as intercompany_out_retail_ly,
                     tbl_crd_ty.intercompany_out_retail           as intercompany_out_retail_ty,
                     tbl_crd_ly.reclass_out_cost                  as reclass_out_cost_ly,
                     tbl_crd_ty.reclass_out_cost                  as reclass_out_cost_ty,
                     tbl_crd_ly.reclass_out_retail                as reclass_out_retail_ly,
                     tbl_crd_ty.reclass_out_retail                as reclass_out_retail_ty,
                     tbl_crd_ly.net_sales_cost                    as net_sales_cost_ly,
                     tbl_crd_ty.net_sales_cost                    as net_sales_cost_ty,
                     tbl_crd_ly.net_sales_retail                  as net_sales_retail_ly,
                     tbl_crd_ty.net_sales_retail                  as net_sales_retail_ty,
                     tbl_crd_ly.returns_cost                      as returns_cost_ly,
                     tbl_crd_ty.returns_cost                      as returns_cost_ty,
                     tbl_crd_ly.returns_retail                    as returns_retail_ly,
                     tbl_crd_ty.returns_retail                    as returns_retail_ty,
                     tbl_crd_ly.franchise_sales_cost              as franchise_sales_cost_ly,
                     tbl_crd_ty.franchise_sales_cost              as franchise_sales_cost_ty,
                     tbl_crd_ly.franchise_sales_retail            as franchise_sales_retail_ly,
                     tbl_crd_ty.franchise_sales_retail            as franchise_sales_retail_ty,
                     tbl_crd_ly.weight_variance_retail            as weight_variance_retail_ly,
                     tbl_crd_ty.weight_variance_retail            as weight_variance_retail_ty,
                     tbl_crd_ly.freight_claim_cost                as freight_claim_cost_ly,
                     tbl_crd_ty.freight_claim_cost                as freight_claim_cost_ty,
                     tbl_crd_ly.freight_claim_retail              as freight_claim_retail_ly,
                     tbl_crd_ty.freight_claim_retail              as freight_claim_retail_ty,
                     tbl_crd_ly.stock_adj_cogs_cost               as stock_adj_cogs_cost_ly,
                     tbl_crd_ty.stock_adj_cogs_cost               as stock_adj_cogs_cost_ty,
                     tbl_crd_ly.stock_adj_cogs_retail             as stock_adj_cogs_retail_ly,
                     tbl_crd_ty.stock_adj_cogs_retail             as stock_adj_cogs_retail_ty,
                     tbl_crd_ly.stock_adj_cost                    as stock_adj_cost_ly,
                     tbl_crd_ty.stock_adj_cost                    as stock_adj_cost_ty,
                     tbl_crd_ly.stock_adj_retail                  as stock_adj_retail_ly,
                     tbl_crd_ty.stock_adj_retail                  as stock_adj_retail_ty,
                     tbl_crd_ly.cost_variance_amt                 as cost_variance_amt_ly,
                     tbl_crd_ty.cost_variance_amt                 as cost_variance_amt_ty,
                     tbl_crd_ly.retail_cost_variance              as retail_cost_variance_ly,
                     tbl_crd_ty.retail_cost_variance              as retail_cost_variance_ty,
                     tbl_crd_ly.margin_cost_variance              as margin_cost_variance_ly,
                     tbl_crd_ty.margin_cost_variance              as margin_cost_variance_ty,
                     tbl_crd_ly.vat_in                            as vat_in_ly,
                     tbl_crd_ty.vat_in                            as vat_in_ty,
                     tbl_crd_ly.vat_out                           as vat_out_ly,
                     tbl_crd_ty.vat_out                           as vat_out_ty,
                     tbl_crd_ly.deal_income_sales                 as deal_income_sales_ly,
                     tbl_crd_ty.deal_income_sales                 as deal_income_sales_ty,
                     tbl_crd_ly.deal_income_purch                 as deal_income_purch_ly,
                     tbl_crd_ty.deal_income_purch                 as deal_income_purch_ty,
                     tbl_crd_ly.restocking_fee                    as restocking_fee_ly,
                     tbl_crd_ty.restocking_fee                    as restocking_fee_ty,
                     tbl_crd_ly.franchise_restocking_fee          as franchise_restocking_fee_ly,
                     tbl_crd_ty.franchise_restocking_fee          as franchise_restocking_fee_ty,
                     tbl_crd_ly.net_sales_non_inv_cost            as net_sales_non_inv_cost_ly,
                     tbl_crd_ty.net_sales_non_inv_cost            as net_sales_non_inv_cost_ty,
                     NULL                                         as add_cost_ly, -- tbl_crd_ly.add_cost
                     NULL                                         as add_cost_ty, -- tbl_crd_ty.add_cost
                     NULL                                         as add_retail_ly,--tbl_crd_ly.add_retail
                     NULL                                         as add_retail_ty, --tbl_crd_ty.add_retail 
                     tbl_crd_ly.cash_disc_amt                     as cash_disc_amt_ly,
                     tbl_crd_ty.cash_disc_amt                     as cash_disc_amt_ty,
                     tbl_crd_ly.clear_markdown_retail             as clear_markdown_retail_ly,
                     tbl_crd_ty.clear_markdown_retail             as clear_markdown_retail_ty,
                     tbl_crd_ly.empl_disc_retail                  as empl_disc_retail_ly,
                     tbl_crd_ty.empl_disc_retail                  as empl_disc_retail_ty,
                     tbl_crd_ly.franchise_markdown_retail         as franchise_markdown_retail_ly,
                     tbl_crd_ty.franchise_markdown_retail         as franchise_markdown_retail_ty,
                     tbl_crd_ly.franchise_markup_retail           as franchise_markup_retail_ly,
                     tbl_crd_ty.franchise_markup_retail           as franchise_markup_retail_ty,
                     tbl_crd_ly.freight_cost                      as freight_cost_ly,
                     tbl_crd_ty.freight_cost                      as freight_cost_ty,
                     tbl_crd_ly.intercompany_markdown             as intercompany_markdown_ly,
                     tbl_crd_ty.intercompany_markdown             as intercompany_markdown_ty,
                     tbl_crd_ly.intercompany_markup               as intercompany_markup_ly,
                     tbl_crd_ty.intercompany_markup               as intercompany_markup_ty,
                     NULL                                         as net_markdown_ly, --tbl_crd_ly.net_markdown
                     NULL                                         as net_markdown_ty, --tbl_crd_ty.net_markdown
                     tbl_crd_ly.markdown_can_retail               as markdown_can_retail_ly,
                     tbl_crd_ty.markdown_can_retail               as markdown_can_retail_ty,
                     tbl_crd_ly.markup_can_retail                 as markup_can_retail_ly,
                     tbl_crd_ty.markup_can_retail                 as markup_can_retail_ty,
                     tbl_crd_ly.markup_retail                     as markup_retail_ly,
                     tbl_crd_ty.markup_retail                     as markup_retail_ty,
                     tbl_crd_ly.perm_markdown_retail              as perm_markdown_retail_ly,
                     tbl_crd_ty.perm_markdown_retail              as perm_markdown_retail_ty,
                     tbl_crd_ly.prom_markdown_retail              as prom_markdown_retail_ly,
                     tbl_crd_ty.prom_markdown_retail              as prom_markdown_retail_ty,
                     tbl_crd_ly.rec_cost_adj_variance             as rec_cost_adj_variance_ly,
                     tbl_crd_ty.rec_cost_adj_variance             as rec_cost_adj_variance_ty,
                     tbl_crd_ly.sales_units                       as sales_units_ly,
                     tbl_crd_ty.sales_units                       as sales_units_ty,
                     tbl_crd_ly.workroom_amt                      as workroom_amt_ly,
                     tbl_crd_ty.workroom_amt                      as workroom_amt_ty,
                     NULL                                         as net_reclass_cost_ly, --tbl_crd_ly.net_reclass_cost
                     NULL                                         as net_reclass_cost_ty, --tbl_crd_ty.net_reclass_cost
                     NULL                                         as net_reclass_retail_ly, --tbl_crd_ly.net_reclass_retail
                     NULL                                         as net_reclass_retail_ty, --tbl_crd_ty.net_reclass_retail
                     NULL                                         as net_tsf_cost_ly, --tbl_crd_ly.net_tsf_cost
                     NULL                                         as net_tsf_cost_ty, --tbl_crd_ty.net_tsf_cost
                     NULL                                         as net_markup_ly, --tbl_crd_ly.net_markup
                     NULL                                         as net_markup_ty, --tbl_crd_ty.net_markup
                     tbl_crd_ly.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ly,
                     tbl_crd_ty.net_sales_retail_ex_vat           as net_sales_retail_ex_vat_ty,
                     tbl_crd_ly.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ly,
                     tbl_crd_ty.net_sales_non_inv_rtl_ex_vat      as net_sale_noninv_r_exvat_ty,
                     tbl_crd_ly.net_sales_non_inv_retail          as net_sales_noninv_retail_ly,
                     tbl_crd_ty.net_sales_non_inv_retail          as net_sales_noninv_retail_ty,
                     NULL                                         as net_tsf_retail_ly, --tbl_crd_ly.net_tsf_retail
                     NULL                                         as net_tsf_retail_ty --tbl_crd_ty.net_tsf_retail
                from table(cast(L_tbl_cr_daily_data_ly as OBJ_DAILY_DATA_TBL)) tbl_crd_ly 
                full outer join table(cast(L_tbl_cr_daily_data_ty as OBJ_DAILY_DATA_TBL)) tbl_crd_ty 
                  ON tbl_crd_ly.dept          =  tbl_crd_ty.dept
                     and tbl_crd_ly.class     =  tbl_crd_ty.class 
                     and tbl_crd_ly.subclass  =  tbl_crd_ty.subclass
                     and tbl_crd_ly.loc_type  =  tbl_crd_ty.loc_type
                     and tbl_crd_ly.location  =  tbl_crd_ty.location
                     and tbl_crd_ly.half_no   =  tbl_crd_ty.half_no
                     and tbl_crd_ly.data_date =  tbl_crd_ty.data_date
                     and tbl_crd_ly.day_no    =  tbl_crd_ty.day_no) outer_query;    
BEGIN
   LP_cr_daily_data_tbl   := new OBJ_CR_DAILY_DATA_TBL();
   L_tbl_cr_daily_data_ty := new OBJ_DAILY_DATA_TBL();
   L_tbl_cr_daily_data_ly := new OBJ_DAILY_DATA_TBL();
   
   if GET_VARIABLES(O_error_message,
                    LP_sch_crit_rec) = FALSE then
      return FALSE;
   end if;
                          
   if LP_cal_454 = 'C' then
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_CAL_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'T',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y', --:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_cr_daily_data_ty,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
      
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_CAL_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'L',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_cr_daily_data_ly,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;

   else
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_454_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'T',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_cr_daily_data_ty,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
      
      if STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_454_BULK(LP_sch_crit_rec.dept,
                                                      LP_sch_crit_rec.class,
                                                      LP_sch_crit_rec.subclass,
                                                      'L',
                                                      LP_sch_crit_rec.period_date,
                                                      LP_loc_type,
                                                      LP_location,
                                                      'Y',--:B_search.CB_thousands,
                                                      LP_currency_ind,
                                                      LP_set_of_books_id,
                                                      L_tbl_cr_daily_data_ly,
                                                      O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   
   open  C_CR_DAILY_DATA_INFO;
   fetch C_CR_DAILY_DATA_INFO BULK COLLECT into LP_cr_daily_data_tbl;
   close C_CR_DAILY_DATA_INFO;
  
   FOR i in 1..LP_cr_daily_data_tbl.count
   LOOP
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  LP_cr_daily_data_tbl(i).dept,
                                  LP_cr_daily_data_tbl(i).dept_name) = FALSE then
		     return FALSE;
      end if;
     
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   LP_cr_daily_data_tbl(i).dept,
                                   LP_cr_daily_data_tbl(i).class,
                                   LP_cr_daily_data_tbl(i).class_name) = FALSE then
		     return FALSE;
      end if;
     
      if SUBCLASS_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_cr_daily_data_tbl(i).dept,
                                       LP_cr_daily_data_tbl(i).class,
                                       LP_cr_daily_data_tbl(i).subclass,
                                       LP_cr_daily_data_tbl(i).subclass_name) = FALSE then
         return FALSE;
      end if;     
      if LOCATION_ATTRIB_SQL.GET_NAME (O_error_message,
                                       LP_cr_daily_data_tbl(i).loc_name,
                                       LP_cr_daily_data_tbl(i).location,
                                       LP_cr_daily_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
       
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LTPM',
                                    LP_cr_daily_data_tbl(i).loc_type,
                                    LP_cr_daily_data_tbl(i).loc_type) = FALSE then
         return FALSE;
      end if;
      
      if (LP_sch_crit_rec.loc_type is NULL or LP_currency_ind = 'P') then
         LP_cr_daily_data_tbl(i).currency_code := LP_system_options_row.currency_code;
      elsif LP_sch_crit_rec.loc_type = 'I' then
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       'W',
      			                       NULL,
      			                       LP_cr_daily_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;
      elsif LP_sch_crit_rec.loc_type = 'M' then
         if SET_OF_BOOKS_SQL.GET_CURRENCY_CODE(O_error_message,
         	                                   LP_cr_daily_data_tbl(i).currency_code,
      			                                LP_sch_crit_rec.location) = FALSE then
            return FALSE;
         end if;  
      else   
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
      			                       LP_sch_crit_rec.location,
      			                       LP_sch_crit_rec.loc_type,
      			                       NULL,
      			                       LP_cr_daily_data_tbl(i).currency_code) = FALSE then
            return FALSE;
         end if;   
      end if;      
      
      LP_cr_daily_data_tbl(i).eow_date := LP_sch_crit_rec.period_date;
      -- CALCULATE_FIELDS
      
      L_net_tsf_cost_ly := LP_cr_daily_data_tbl(i).tsf_in_cost_ly - LP_cr_daily_data_tbl(i).tsf_out_cost_ly;
      L_net_tsf_cost_ty := LP_cr_daily_data_tbl(i).tsf_in_cost_ty - LP_cr_daily_data_tbl(i).tsf_out_cost_ty;
      
      L_net_tsf_retail_ly := LP_cr_daily_data_tbl(i).tsf_in_retail_ly - LP_cr_daily_data_tbl(i).tsf_out_retail_ly;
      L_net_tsf_retail_ty := LP_cr_daily_data_tbl(i).tsf_in_retail_ty - LP_cr_daily_data_tbl(i).tsf_out_retail_ty;
      
      L_net_markup_retail_ly := LP_cr_daily_data_tbl(i).markup_retail_ly - LP_cr_daily_data_tbl(i).markup_can_retail_ly + LP_cr_daily_data_tbl(i).franchise_markup_retail_ly;
      L_net_markup_retail_ty := LP_cr_daily_data_tbl(i).markup_retail_ty - LP_cr_daily_data_tbl(i).markup_can_retail_ty + LP_cr_daily_data_tbl(i).franchise_markup_retail_ty;
      
      L_net_markdown_retail_ly := LP_cr_daily_data_tbl(i).clear_markdown_retail_ly + LP_cr_daily_data_tbl(i).perm_markdown_retail_ly + LP_cr_daily_data_tbl(i).prom_markdown_retail_ly - LP_cr_daily_data_tbl(i).markdown_can_retail_ly + LP_cr_daily_data_tbl(i).franchise_markdown_retail_ly;
      L_net_markdown_retail_ty := LP_cr_daily_data_tbl(i).clear_markdown_retail_ty + LP_cr_daily_data_tbl(i).perm_markdown_retail_ty + LP_cr_daily_data_tbl(i).prom_markdown_retail_ty - LP_cr_daily_data_tbl(i).markdown_can_retail_ty + LP_cr_daily_data_tbl(i).franchise_markdown_retail_ty;
            
      L_add_cost_ly := LP_cr_daily_data_tbl(i).purch_cost_ly - LP_cr_daily_data_tbl(i).rtv_cost_ly + LP_cr_daily_data_tbl(i).freight_cost_ly + L_net_tsf_cost_ly + LP_cr_daily_data_tbl(i).franchise_returns_cost_ly;
      L_add_cost_ty := LP_cr_daily_data_tbl(i).purch_cost_ty - LP_cr_daily_data_tbl(i).rtv_cost_ty + LP_cr_daily_data_tbl(i).freight_cost_ty + L_net_tsf_cost_ty + LP_cr_daily_data_tbl(i).franchise_returns_cost_ty;
      
      L_add_retail_ly := LP_cr_daily_data_tbl(i).purch_retail_ly - LP_cr_daily_data_tbl(i).rtv_retail_ly + L_net_tsf_retail_ly + L_net_markup_retail_ly + LP_cr_daily_data_tbl(i).franchise_returns_retail_ly;
      L_add_retail_ty := LP_cr_daily_data_tbl(i).purch_retail_ty - LP_cr_daily_data_tbl(i).rtv_retail_ty + L_net_tsf_retail_ty + L_net_markup_retail_ty + LP_cr_daily_data_tbl(i).franchise_returns_retail_ty;
      
      L_net_reclass_cost_ly   := LP_cr_daily_data_tbl(i).reclass_in_cost_ly - LP_cr_daily_data_tbl(i).reclass_out_cost_ly;          
      L_net_reclass_cost_ty   := LP_cr_daily_data_tbl(i).reclass_in_cost_ty - LP_cr_daily_data_tbl(i).reclass_out_cost_ty;

      L_net_reclass_retail_ly := LP_cr_daily_data_tbl(i).reclass_in_retail_ly - LP_cr_daily_data_tbl(i).reclass_out_retail_ly;    
      L_net_reclass_retail_ty := LP_cr_daily_data_tbl(i).reclass_in_retail_ty - LP_cr_daily_data_tbl(i).reclass_out_retail_ty;

      LP_cr_daily_data_tbl(i).add_cost_ly                   := L_add_cost_ly;
      LP_cr_daily_data_tbl(i).add_cost_ty                   := L_add_cost_ty;
      LP_cr_daily_data_tbl(i).add_retail_ly                 := L_add_retail_ly;
      LP_cr_daily_data_tbl(i).add_retail_ty                 := L_add_retail_ty;
      LP_cr_daily_data_tbl(i).net_markdown_ly               := L_net_markdown_retail_ly;
      LP_cr_daily_data_tbl(i).net_markdown_ty               := L_net_markdown_retail_ty;
      LP_cr_daily_data_tbl(i).net_markup_ly                 := L_net_markup_retail_ly;
      LP_cr_daily_data_tbl(i).net_markup_ty                 := L_net_markup_retail_ty;
      LP_cr_daily_data_tbl(i).net_reclass_cost_ly           := L_net_reclass_cost_ly;
      LP_cr_daily_data_tbl(i).net_reclass_cost_ty           := L_net_reclass_cost_ty;
      LP_cr_daily_data_tbl(i).net_reclass_retail_ly         := L_net_reclass_retail_ly;
      LP_cr_daily_data_tbl(i).net_reclass_retail_ty         := L_net_reclass_retail_ty;
      LP_cr_daily_data_tbl(i).net_tsf_cost_ly               := L_net_tsf_cost_ly;
      LP_cr_daily_data_tbl(i).net_tsf_cost_ty               := L_net_tsf_cost_ty;
      LP_cr_daily_data_tbl(i).net_tsf_retail_ly             := L_net_tsf_retail_ly;
      LP_cr_daily_data_tbl(i).net_tsf_retail_ty             := L_net_tsf_retail_ty;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CR_DAILY_DATA_INFO%ISOPEN then
         close C_CR_DAILY_DATA_INFO;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END FETCH_CR_DAILY_DATA;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   CMD_cols s9t_pkg.names_map_typ;
   CWD_cols s9t_pkg.names_map_typ;
   CDD_cols s9t_pkg.names_map_typ;
   RMD_cols s9t_pkg.names_map_typ;
   RWD_cols s9t_pkg.names_map_typ;
   RDD_cols s9t_pkg.names_map_typ;
   CRM_cols s9t_pkg.names_map_typ;
   CRW_cols s9t_pkg.names_map_typ;
   CRD_cols s9t_pkg.names_map_typ;
   
BEGIN
   -- Cost Month Columns Begin --
   CMD_cols                              := s9t_pkg.get_col_names(I_file_id,CMD_sheet);
   CMD$DEPT                              := CMD_cols('DEPT');
   CMD$DEPT_NAME                         := CMD_cols('DEPT_NAME');
   CMD$CLASS                             := CMD_cols('CLASS');
   CMD$CLASS_NAME                        := CMD_cols('CLASS_NAME');
   CMD$SUBCLASS                          := CMD_cols('SUBCLASS');
   CMD$SUBCLASS_NAME                     := CMD_cols('SUBCLASS_NAME');
   CMD$LOC_TYPE                          := CMD_cols('LOC_TYPE');
   CMD$LOCATION                          := CMD_cols('LOCATION');
   CMD$LOCATION_NAME                     := CMD_cols('LOC_NAME');
   CMD$CURRENCY                          := CMD_cols('CURRENCY');
   CMD$HALF_NO                           := CMD_cols('HALF_NO');
   CMD$MONTH_NO                          := CMD_cols('MONTH_NO');
   CMD$OPN_STK_COST_LY                   := CMD_cols('OPN_STK_COST_LY');
   CMD$OPN_STK_COST_TY                   := CMD_cols('OPN_STK_COST_TY');
   CMD$PURCH_COST_LY                     := CMD_cols('PURCH_COST_LY');
   CMD$PURCH_COST_TY                     := CMD_cols('PURCH_COST_TY');
   CMD$TSF_IN_COST_LY                    := CMD_cols('TSF_IN_COST_LY');
   CMD$TSF_IN_COST_TY                    := CMD_cols('TSF_IN_COST_TY');
   CMD$TSF_IN_BOOK_COST_LY               := CMD_cols('TSF_IN_BOOK_COST_LY');
   CMD$TSF_IN_BOOK_COST_TY               := CMD_cols('TSF_IN_BOOK_COST_TY');
   CMD$INTERCOMPANY_IN_COST_LY           := CMD_cols('INTERCOMPANY_IN_COST_LY');
   CMD$INTERCOMPANY_IN_COST_TY           := CMD_cols('INTERCOMPANY_IN_COST_TY'); 
   CMD$RECLASS_IN_COST_LY                := CMD_cols('RECLASS_IN_COST_LY');
   CMD$RECLASS_IN_COST_TY                := CMD_cols('RECLASS_IN_COST_TY');
   CMD$FRANCHISE_RETURNS_COST_LY         := CMD_cols('FRANCHISE_RETURNS_COST_LY');
   CMD$FRANCHISE_RETURNS_COST_TY         := CMD_cols('FRANCHISE_RETURNS_COST_TY'); 
   CMD$UP_CHRG_AMT_PROFIT_LY             := CMD_cols('UP_CHRG_AMT_PROFIT_LY');
   CMD$UP_CHRG_AMT_PROFIT_TY             := CMD_cols('UP_CHRG_AMT_PROFIT_TY');
   CMD$UP_CHRG_AMT_EXP_LY                := CMD_cols('UP_CHRG_AMT_EXP_LY');
   CMD$UP_CHRG_AMT_EXP_TY                := CMD_cols('UP_CHRG_AMT_EXP_TY');
   CMD$WO_ACTIVITY_UPD_INV_LY            := CMD_cols('WO_ACTIVITY_UPD_INV_LY');
   CMD$WO_ACTIVITY_UPD_INV_TY            := CMD_cols('WO_ACTIVITY_UPD_INV_TY');
   CMD$RECOVERABLE_TAX_LY                := CMD_cols('RECOVERABLE_TAX_LY');
   CMD$RECOVERABLE_TAX_TY                := CMD_cols('RECOVERABLE_TAX_TY');
   CMD$RTV_COST_LY                       := CMD_cols('RTV_COST_LY');
   CMD$RTV_COST_TY                       := CMD_cols('RTV_COST_TY');
   CMD$TSF_OUT_COST_LY                   := CMD_cols('TSF_OUT_COST_LY');
   CMD$TSF_OUT_COST_TY                   := CMD_cols('TSF_OUT_COST_TY');
   CMD$TSF_OUT_BOOK_COST_LY              := CMD_cols('TSF_OUT_BOOK_COST_LY');
   CMD$TSF_OUT_BOOK_COST_TY              := CMD_cols('TSF_OUT_BOOK_COST_TY');
   CMD$INTERCOMPANY_OUT_COST_LY          := CMD_cols('INTERCOMPANY_OUT_COST_LY');
   CMD$INTERCOMPANY_OUT_COST_TY          := CMD_cols('INTERCOMPANY_OUT_COST_TY');
   CMD$RECLASS_OUT_COST_LY               := CMD_cols('RECLASS_OUT_COST_LY');
   CMD$RECLASS_OUT_COST_TY               := CMD_cols('RECLASS_OUT_COST_TY');
   CMD$NET_SALES_COST_LY                 := CMD_cols('NET_SALES_COST_LY');
   CMD$NET_SALES_COST_TY                 := CMD_cols('NET_SALES_COST_TY');
   CMD$RETURNS_COST_LY                   := CMD_cols('RETURNS_COST_LY');
   CMD$RETURNS_COST_TY                   := CMD_cols('RETURNS_COST_TY');
   CMD$FRANCHISE_SALES_COST_LY           := CMD_cols('FRANCHISE_SALES_COST_LY');
   CMD$FRANCHISE_SALES_COST_TY           := CMD_cols('FRANCHISE_SALES_COST_TY');
   CMD$FREIGHT_CLAIM_COST_LY             := CMD_cols('FREIGHT_CLAIM_COST_LY');
   CMD$FREIGHT_CLAIM_COST_TY             := CMD_cols('FREIGHT_CLAIM_COST_TY');
   CMD$STOCK_ADJ_COGS_COST_LY            := CMD_cols('STOCK_ADJ_COGS_COST_LY');
   CMD$STOCK_ADJ_COGS_COST_TY            := CMD_cols('STOCK_ADJ_COGS_COST_TY');
   CMD$STOCK_ADJ_COST_LY                 := CMD_cols('STOCK_ADJ_COST_LY');
   CMD$STOCK_ADJ_COST_TY                 := CMD_cols('STOCK_ADJ_COST_TY');
   CMD$SHRINKAGE_COST_LY                 := CMD_cols('SHRINKAGE_COST_LY');
   CMD$SHRINKAGE_COST_TY                 := CMD_cols('SHRINKAGE_COST_TY');
   CMD$COST_VARIANCE_AMT_LY              := CMD_cols('COST_VARIANCE_AMT_LY');
   CMD$COST_VARIANCE_AMT_TY              := CMD_cols('COST_VARIANCE_AMT_TY');
   CMD$MARGIN_COST_VARIANCE_LY           := CMD_cols('MARGIN_COST_VARIANCE_LY');
   CMD$MARGIN_COST_VARIANCE_TY           := CMD_cols('MARGIN_COST_VARIANCE_TY');
   CMD$CLS_STK_COST_LY                   := CMD_cols('CLS_STK_COST_LY');
   CMD$CLS_STK_COST_TY                   := CMD_cols('CLS_STK_COST_TY');
   CMD$ADD_COST_LY                       := CMD_cols('ADD_COST_LY');
   CMD$ADD_COST_TY                       := CMD_cols('ADD_COST_TY');
   CMD$ADD_RETAIL_LY                     := CMD_cols('ADD_RETAIL_LY');
   CMD$ADD_RETAIL_TY                     := CMD_cols('ADD_RETAIL_TY');
   CMD$SHRINK_COST_PCT_LY                := CMD_cols('SHRINK_COST_PCT_LY');
   CMD$SHRINK_COST_PCT_TY                := CMD_cols('SHRINK_COST_PCT_TY');
   CMD$RED_RETAIL_LY                     := CMD_cols('RED_RETAIL_LY');
   CMD$RED_RETAIL_TY                     := CMD_cols('RED_RETAIL_TY');
   CMD$DEAL_INCOME_SALES_LY              := CMD_cols('DEAL_INCOME_SALES_LY');
   CMD$DEAL_INCOME_SALES_TY              := CMD_cols('DEAL_INCOME_SALES_TY');
   CMD$DEAL_INCOME_PURCH_LY              := CMD_cols('DEAL_INCOME_PURCH_LY');
   CMD$DEAL_INCOME_PURCH_TY              := CMD_cols('DEAL_INCOME_PURCH_TY');
   CMD$VAT_IN_LY                         := CMD_cols('VAT_IN_LY');
   CMD$VAT_IN_TY                         := CMD_cols('VAT_IN_TY');
   CMD$VAT_OUT_LY                        := CMD_cols('VAT_OUT_LY');
   CMD$VAT_OUT_TY                        := CMD_cols('VAT_OUT_TY');
   CMD$WO_ACTIVITY_POST_FIN_LY           := CMD_cols('WO_ACTIVITY_POST_FIN_LY');
   CMD$WO_ACTIVITY_POST_FIN_TY           := CMD_cols('WO_ACTIVITY_POST_FIN_TY');
   CMD$RESTOCKING_FEE_LY                 := CMD_cols('RESTOCKING_FEE_LY');
   CMD$RESTOCKING_FEE_TY                 := CMD_cols('RESTOCKING_FEE_TY');
   CMD$FREIGHT_COST_LY                   := CMD_cols('FREIGHT_COST_LY');
   CMD$FREIGHT_COST_TY                   := CMD_cols('FREIGHT_COST_TY');
   CMD$REC_COST_ADJ_VARIANCE_LY          := CMD_cols('REC_COST_ADJ_VARIANCE_LY');
   CMD$REC_COST_ADJ_VARIANCE_TY          := CMD_cols('REC_COST_ADJ_VARIANCE_TY');
   CMD$WORKROOM_AMT_LY                   := CMD_cols('WORKROOM_AMT_LY');
   CMD$WORKROOM_AMT_TY                   := CMD_cols('WORKROOM_AMT_TY');
   CMD$CASH_DISC_AMT_LY                  := CMD_cols('CASH_DISC_AMT_LY');
   CMD$CASH_DISC_AMT_TY                  := CMD_cols('CASH_DISC_AMT_TY');
   CMD$CLEAR_MARKDOWN_RETAIL_LY          := CMD_cols('CLEAR_MARKDOWN_RETAIL_LY');
   CMD$CLEAR_MARKDOWN_RETAIL_TY          := CMD_cols('CLEAR_MARKDOWN_RETAIL_TY');
   CMD$EMPL_DISC_RETAIL_LY               := CMD_cols('EMPL_DISC_RETAIL_LY');
   CMD$EMPL_DISC_RETAIL_TY               := CMD_cols('EMPL_DISC_RETAIL_TY');
   CMD$MARKUP_CAN_RETAIL_LY              := CMD_cols('MARKUP_CAN_RETAIL_LY');
   CMD$MARKUP_CAN_RETAIL_TY              := CMD_cols('MARKUP_CAN_RETAIL_TY');
   CMD$MARKUP_RETAIL_LY                  := CMD_cols('MARKUP_RETAIL_LY');
   CMD$MARKUP_RETAIL_TY                  := CMD_cols('MARKUP_RETAIL_TY');
   CMD$PERM_MARKDOWN_RETAIL_LY           := CMD_cols('PERM_MARKDOWN_RETAIL_LY');
   CMD$PERM_MARKDOWN_RETAIL_TY           := CMD_cols('PERM_MARKDOWN_RETAIL_TY');
   CMD$PROM_MARKDOWN_RETAIL_LY           := CMD_cols('PROM_MARKDOWN_RETAIL_LY');
   CMD$PROM_MARKDOWN_RETAIL_TY           := CMD_cols('PROM_MARKDOWN_RETAIL_TY');
   CMD$FRN_MARKDOWN_RETAIL_LY            := CMD_cols('FRN_MARKDOWN_RETAIL_LY');
   CMD$FRN_MARKDOWN_RETAIL_TY            := CMD_cols('FRN_MARKDOWN_RETAIL_TY');
   CMD$FRN_MARKUP_RETAIL_LY              := CMD_cols('FRN_MARKUP_RETAIL_LY');
   CMD$FRN_MARKUP_RETAIL_TY              := CMD_cols('FRN_MARKUP_RETAIL_TY');
   CMD$FRN_RESTOCKING_FEE_LY             := CMD_cols('FRN_RESTOCKING_FEE_LY');
   CMD$FRN_RESTOCKING_FEE_TY             := CMD_cols('FRN_RESTOCKING_FEE_TY');
   CMD$INTERCOMPANY_MARKDOWN_LY          := CMD_cols('INTERCOMPANY_MARKDOWN_LY');
   CMD$INTERCOMPANY_MARKDOWN_TY          := CMD_cols('INTERCOMPANY_MARKDOWN_TY');
   CMD$INTERCOMPANY_MARKUP_LY            := CMD_cols('INTERCOMPANY_MARKUP_LY');
   CMD$INTERCOMPANY_MARKUP_TY            := CMD_cols('INTERCOMPANY_MARKUP_TY');
   CMD$INTERCOMPANY_MARGIN_LY            := CMD_cols('INTERCOMPANY_MARGIN_LY');
   CMD$INTERCOMPANY_MARGIN_TY            := CMD_cols('INTERCOMPANY_MARGIN_TY');
   CMD$MARKDOWN_CAN_RETAIL_LY            := CMD_cols('MARKDOWN_CAN_RETAIL_LY');
   CMD$MARKDOWN_CAN_RETAIL_TY            := CMD_cols('MARKDOWN_CAN_RETAIL_TY');
   CMD$NET_MARKUP_LY                     := CMD_cols('NET_MARKUP_LY');
   CMD$NET_MARKUP_TY                     := CMD_cols('NET_MARKUP_TY');
   CMD$NET_RECLASS_COST_LY               := CMD_cols('NET_RECLASS_COST_LY');
   CMD$NET_RECLASS_COST_TY               := CMD_cols('NET_RECLASS_COST_TY');
   CMD$NET_TSF_COST_LY                   := CMD_cols('NET_TSF_COST_LY');
   CMD$NET_TSF_COST_TY                   := CMD_cols('NET_TSF_COST_TY');
   CMD$SALES_UNITS_LY                    := CMD_cols('SALES_UNITS_LY');
   CMD$SALES_UNITS_TY                    := CMD_cols('SALES_UNITS_TY');
   CMD$CUM_MARKON_PCT_LY                 := CMD_cols('CUM_MARKON_PCT_LY');
   CMD$CUM_MARKON_PCT_TY                 := CMD_cols('CUM_MARKON_PCT_TY');
   CMD$GROSS_MARGIN_AMT_LY               := CMD_cols('GROSS_MARGIN_AMT_LY');
   CMD$GROSS_MARGIN_AMT_TY               := CMD_cols('GROSS_MARGIN_AMT_TY');
   CMD$NET_SALES_NON_INV_COST_LY         := CMD_cols('NET_SALES_NON_INV_COST_LY');
   CMD$NET_SALES_NON_INV_COST_TY         := CMD_cols('NET_SALES_NON_INV_COST_TY');
   -- Cost Month Columns End --
   -- Cost Week Columns Begin --
   CWD_cols                              := s9t_pkg.get_col_names(I_file_id,CWD_sheet);
   CWD$DEPT                              := CWD_cols('DEPT');
   CWD$DEPT_NAME                         := CWD_cols('DEPT_NAME');          
   CWD$CLASS                             := CWD_cols('CLASS');
   CWD$CLASS_NAME                        := CWD_cols('CLASS_NAME');         
   CWD$SUBCLASS                          := CWD_cols('SUBCLASS');
   CWD$SUBCLASS_NAME                     := CWD_cols('SUBCLASS_NAME'); 
   CWD$LOC_TYPE                          := CWD_cols('LOC_TYPE');
   CWD$LOCATION                          := CWD_cols('LOCATION');
   CWD$LOCATION_NAME                     := CWD_cols('LOC_NAME');    
   CWD$CURRENCY                          := CWD_cols('CURRENCY');    
   CWD$EOM_DATE                          := CWD_cols('EOM_DATE');
   CWD$WEEK_NO                           := CWD_cols('WEEK_NO');
   CWD$OPN_STK_COST_LY                   := CWD_cols('OPN_STK_COST_LY');
   CWD$OPN_STK_COST_TY                   := CWD_cols('OPN_STK_COST_TY');
   CWD$PURCH_COST_LY                     := CWD_cols('PURCH_COST_LY');
   CWD$PURCH_COST_TY                     := CWD_cols('PURCH_COST_TY');
   CWD$TSF_IN_COST_LY                    := CWD_cols('TSF_IN_COST_LY');
   CWD$TSF_IN_COST_TY                    := CWD_cols('TSF_IN_COST_TY');
   CWD$TSF_IN_BOOK_COST_LY               := CWD_cols('TSF_IN_BOOK_COST_LY');
   CWD$TSF_IN_BOOK_COST_TY               := CWD_cols('TSF_IN_BOOK_COST_TY');
   CWD$INTERCOMPANY_IN_COST_LY           := CWD_cols('INTERCOMPANY_IN_COST_LY');
   CWD$INTERCOMPANY_IN_COST_TY           := CWD_cols('INTERCOMPANY_IN_COST_TY'); 
   CWD$RECLASS_IN_COST_LY                := CWD_cols('RECLASS_IN_COST_LY');
   CWD$RECLASS_IN_COST_TY                := CWD_cols('RECLASS_IN_COST_TY');
   CWD$FRANCHISE_RETURNS_COST_LY         := CWD_cols('FRANCHISE_RETURNS_COST_LY');
   CWD$FRANCHISE_RETURNS_COST_TY         := CWD_cols('FRANCHISE_RETURNS_COST_TY'); 
   CWD$UP_CHRG_AMT_PROFIT_LY             := CWD_cols('UP_CHRG_AMT_PROFIT_LY');
   CWD$UP_CHRG_AMT_PROFIT_TY             := CWD_cols('UP_CHRG_AMT_PROFIT_TY');
   CWD$UP_CHRG_AMT_EXP_LY                := CWD_cols('UP_CHRG_AMT_EXP_LY');
   CWD$UP_CHRG_AMT_EXP_TY                := CWD_cols('UP_CHRG_AMT_EXP_TY');
   CWD$WO_ACTIVITY_UPD_INV_LY            := CWD_cols('WO_ACTIVITY_UPD_INV_LY');
   CWD$WO_ACTIVITY_UPD_INV_TY            := CWD_cols('WO_ACTIVITY_UPD_INV_TY');
   CWD$RECOVERABLE_TAX_LY                := CWD_cols('RECOVERABLE_TAX_LY');
   CWD$RECOVERABLE_TAX_TY                := CWD_cols('RECOVERABLE_TAX_TY');
   CWD$RTV_COST_LY                       := CWD_cols('RTV_COST_LY');
   CWD$RTV_COST_TY                       := CWD_cols('RTV_COST_TY');
   CWD$TSF_OUT_COST_LY                   := CWD_cols('TSF_OUT_COST_LY');
   CWD$TSF_OUT_COST_TY                   := CWD_cols('TSF_OUT_COST_TY');
   CWD$TSF_OUT_BOOK_COST_LY              := CWD_cols('TSF_OUT_BOOK_COST_LY');
   CWD$TSF_OUT_BOOK_COST_TY              := CWD_cols('TSF_OUT_BOOK_COST_TY');
   CWD$INTERCOMPANY_OUT_COST_LY          := CWD_cols('INTERCOMPANY_OUT_COST_LY');
   CWD$INTERCOMPANY_OUT_COST_TY          := CWD_cols('INTERCOMPANY_OUT_COST_TY');
   CWD$RECLASS_OUT_COST_LY               := CWD_cols('RECLASS_OUT_COST_LY');
   CWD$RECLASS_OUT_COST_TY               := CWD_cols('RECLASS_OUT_COST_TY');
   CWD$NET_SALES_COST_LY                 := CWD_cols('NET_SALES_COST_LY');
   CWD$NET_SALES_COST_TY                 := CWD_cols('NET_SALES_COST_TY');
   CWD$RETURNS_COST_LY                   := CWD_cols('RETURNS_COST_LY');
   CWD$RETURNS_COST_TY                   := CWD_cols('RETURNS_COST_TY');
   CWD$FRANCHISE_SALES_COST_LY           := CWD_cols('FRANCHISE_SALES_COST_LY');
   CWD$FRANCHISE_SALES_COST_TY           := CWD_cols('FRANCHISE_SALES_COST_TY');
   CWD$FREIGHT_CLAIM_COST_LY             := CWD_cols('FREIGHT_CLAIM_COST_LY');
   CWD$FREIGHT_CLAIM_COST_TY             := CWD_cols('FREIGHT_CLAIM_COST_TY');
   CWD$STOCK_ADJ_COGS_COST_LY            := CWD_cols('STOCK_ADJ_COGS_COST_LY');
   CWD$STOCK_ADJ_COGS_COST_TY            := CWD_cols('STOCK_ADJ_COGS_COST_TY');
   CWD$STOCK_ADJ_COST_LY                 := CWD_cols('STOCK_ADJ_COST_LY');
   CWD$STOCK_ADJ_COST_TY                 := CWD_cols('STOCK_ADJ_COST_TY');
   CWD$SHRINKAGE_COST_LY                 := CWD_cols('SHRINKAGE_COST_LY');
   CWD$SHRINKAGE_COST_TY                 := CWD_cols('SHRINKAGE_COST_TY');
   CWD$COST_VARIANCE_AMT_LY              := CWD_cols('COST_VARIANCE_AMT_LY');
   CWD$COST_VARIANCE_AMT_TY              := CWD_cols('COST_VARIANCE_AMT_TY');
   CWD$MARGIN_COST_VARIANCE_LY           := CWD_cols('MARGIN_COST_VARIANCE_LY');
   CWD$MARGIN_COST_VARIANCE_TY           := CWD_cols('MARGIN_COST_VARIANCE_TY');
   CWD$CLS_STK_COST_LY                   := CWD_cols('CLS_STK_COST_LY');
   CWD$CLS_STK_COST_TY                   := CWD_cols('CLS_STK_COST_TY');
   CWD$ADD_COST_LY                       := CWD_cols('ADD_COST_LY');
   CWD$ADD_COST_TY                       := CWD_cols('ADD_COST_TY');
   CWD$ADD_RETAIL_LY                     := CWD_cols('ADD_RETAIL_LY');
   CWD$ADD_RETAIL_TY                     := CWD_cols('ADD_RETAIL_TY');
   CWD$SHRINK_COST_PCT_LY                := CWD_cols('SHRINK_COST_PCT_LY');
   CWD$SHRINK_COST_PCT_TY                := CWD_cols('SHRINK_COST_PCT_TY');
   CWD$RED_RETAIL_LY                     := CWD_cols('RED_RETAIL_LY');
   CWD$RED_RETAIL_TY                     := CWD_cols('RED_RETAIL_TY');
   CWD$DEAL_INCOME_SALES_LY              := CWD_cols('DEAL_INCOME_SALES_LY');
   CWD$DEAL_INCOME_SALES_TY              := CWD_cols('DEAL_INCOME_SALES_TY');
   CWD$DEAL_INCOME_PURCH_LY              := CWD_cols('DEAL_INCOME_PURCH_LY');
   CWD$DEAL_INCOME_PURCH_TY              := CWD_cols('DEAL_INCOME_PURCH_TY');
   CWD$VAT_IN_LY                         := CWD_cols('VAT_IN_LY');
   CWD$VAT_IN_TY                         := CWD_cols('VAT_IN_TY');
   CWD$VAT_OUT_LY                        := CWD_cols('VAT_OUT_LY');
   CWD$VAT_OUT_TY                        := CWD_cols('VAT_OUT_TY');
   CWD$WO_ACTIVITY_POST_FIN_LY           := CWD_cols('WO_ACTIVITY_POST_FIN_LY');
   CWD$WO_ACTIVITY_POST_FIN_TY           := CWD_cols('WO_ACTIVITY_POST_FIN_TY');
   CWD$RESTOCKING_FEE_LY                 := CWD_cols('RESTOCKING_FEE_LY');
   CWD$RESTOCKING_FEE_TY                 := CWD_cols('RESTOCKING_FEE_TY');
   CWD$FREIGHT_COST_LY                   := CWD_cols('FREIGHT_COST_LY');
   CWD$FREIGHT_COST_TY                   := CWD_cols('FREIGHT_COST_TY');
   CWD$REC_COST_ADJ_VARIANCE_LY          := CWD_cols('REC_COST_ADJ_VARIANCE_LY');
   CWD$REC_COST_ADJ_VARIANCE_TY          := CWD_cols('REC_COST_ADJ_VARIANCE_TY');
   CWD$WORKROOM_AMT_LY                   := CWD_cols('WORKROOM_AMT_LY');
   CWD$WORKROOM_AMT_TY                   := CWD_cols('WORKROOM_AMT_TY');
   CWD$CASH_DISC_AMT_LY                  := CWD_cols('CASH_DISC_AMT_LY');
   CWD$CASH_DISC_AMT_TY                  := CWD_cols('CASH_DISC_AMT_TY');
   CWD$CLEAR_MARKDOWN_RETAIL_LY          := CWD_cols('CLEAR_MARKDOWN_RETAIL_LY');
   CWD$CLEAR_MARKDOWN_RETAIL_TY          := CWD_cols('CLEAR_MARKDOWN_RETAIL_TY');
   CWD$EMPL_DISC_RETAIL_LY               := CWD_cols('EMPL_DISC_RETAIL_LY');
   CWD$EMPL_DISC_RETAIL_TY               := CWD_cols('EMPL_DISC_RETAIL_TY');
   CWD$MARKUP_CAN_RETAIL_LY              := CWD_cols('MARKUP_CAN_RETAIL_LY');
   CWD$MARKUP_CAN_RETAIL_TY              := CWD_cols('MARKUP_CAN_RETAIL_TY');
   CWD$MARKUP_RETAIL_LY                  := CWD_cols('MARKUP_RETAIL_LY');
   CWD$MARKUP_RETAIL_TY                  := CWD_cols('MARKUP_RETAIL_TY');
   CWD$PERM_MARKDOWN_RETAIL_LY           := CWD_cols('PERM_MARKDOWN_RETAIL_LY');
   CWD$PERM_MARKDOWN_RETAIL_TY           := CWD_cols('PERM_MARKDOWN_RETAIL_TY');
   CWD$PROM_MARKDOWN_RETAIL_LY           := CWD_cols('PROM_MARKDOWN_RETAIL_LY');
   CWD$PROM_MARKDOWN_RETAIL_TY           := CWD_cols('PROM_MARKDOWN_RETAIL_TY');
   CWD$FRN_MARKDOWN_RETAIL_LY            := CWD_cols('FRN_MARKDOWN_RETAIL_LY');
   CWD$FRN_MARKDOWN_RETAIL_TY            := CWD_cols('FRN_MARKDOWN_RETAIL_TY');
   CWD$FRN_MARKUP_RETAIL_LY              := CWD_cols('FRN_MARKUP_RETAIL_LY');
   CWD$FRN_MARKUP_RETAIL_TY              := CWD_cols('FRN_MARKUP_RETAIL_TY');
   CWD$FRN_RESTOCKING_FEE_LY             := CWD_cols('FRN_RESTOCKING_FEE_LY');
   CWD$FRN_RESTOCKING_FEE_TY             := CWD_cols('FRN_RESTOCKING_FEE_TY');
   CWD$INTERCOMPANY_MARKDOWN_LY          := CWD_cols('INTERCOMPANY_MARKDOWN_LY');
   CWD$INTERCOMPANY_MARKDOWN_TY          := CWD_cols('INTERCOMPANY_MARKDOWN_TY');
   CWD$INTERCOMPANY_MARKUP_LY            := CWD_cols('INTERCOMPANY_MARKUP_LY');
   CWD$INTERCOMPANY_MARKUP_TY            := CWD_cols('INTERCOMPANY_MARKUP_TY');
   CWD$INTERCOMPANY_MARGIN_LY            := CWD_cols('INTERCOMPANY_MARGIN_LY');
   CWD$INTERCOMPANY_MARGIN_TY            := CWD_cols('INTERCOMPANY_MARGIN_TY');
   CWD$MARKDOWN_CAN_RETAIL_LY            := CWD_cols('MARKDOWN_CAN_RETAIL_LY');
   CWD$MARKDOWN_CAN_RETAIL_TY            := CWD_cols('MARKDOWN_CAN_RETAIL_TY');   
   CWD$NET_MARKUP_LY                     := CWD_cols('NET_MARKUP_LY');
   CWD$NET_MARKUP_TY                     := CWD_cols('NET_MARKUP_TY');
   CWD$NET_RECLASS_COST_LY               := CWD_cols('NET_RECLASS_COST_LY');
   CWD$NET_RECLASS_COST_TY               := CWD_cols('NET_RECLASS_COST_TY');
   CWD$NET_TSF_COST_LY                   := CWD_cols('NET_TSF_COST_LY');
   CWD$NET_TSF_COST_TY                   := CWD_cols('NET_TSF_COST_TY');
   CWD$SALES_UNITS_LY                    := CWD_cols('SALES_UNITS_LY');
   CWD$SALES_UNITS_TY                    := CWD_cols('SALES_UNITS_TY');
   CWD$CUM_MARKON_PCT_LY                 := CWD_cols('CUM_MARKON_PCT_LY');
   CWD$CUM_MARKON_PCT_TY                 := CWD_cols('CUM_MARKON_PCT_TY');
   CWD$GROSS_MARGIN_AMT_LY               := CWD_cols('GROSS_MARGIN_AMT_LY');
   CWD$GROSS_MARGIN_AMT_TY               := CWD_cols('GROSS_MARGIN_AMT_TY');
   CWD$NET_SALES_NON_INV_COST_LY         := CWD_cols('NET_SALES_NON_INV_COST_LY');
   CWD$NET_SALES_NON_INV_COST_TY         := CWD_cols('NET_SALES_NON_INV_COST_TY');
   -- Cost Week Columns End --
   -- Cost Daily Columns Begin --
   CDD_cols                              := s9t_pkg.get_col_names(I_file_id,CDD_sheet);
   CDD$DEPT                              := CDD_Cols('DEPT');               
   CDD$DEPT_NAME                         := CDD_Cols('DEPT_NAME');
   CDD$CLASS                             := CDD_Cols('CLASS');              
   CDD$CLASS_NAME                        := CDD_Cols('CLASS_NAME');         
   CDD$SUBCLASS                          := CDD_Cols('SUBCLASS');           
   CDD$SUBCLASS_NAME                     := CDD_Cols('SUBCLASS_NAME');     
   CDD$LOC_TYPE                          := CDD_Cols('LOC_TYPE');           	
   CDD$LOCATION                          := CDD_Cols('LOCATION');            
   CDD$LOC_NAME                          := CDD_Cols('LOC_NAME');       
   CDD$CURRENCY                          := CDD_Cols('CURRENCY');            
   CDD$EOW_DATE                          := CDD_Cols('EOW_DATE');           
   CDD$DAY_NO                            := CDD_Cols('DAY_NO');              
   CDD$PURCH_COST_LY                     := CDD_Cols('PURCH_COST_LY');       
   CDD$PURCH_COST_TY                     := CDD_Cols('PURCH_COST_TY');       
   CDD$TSF_IN_COST_LY                    := CDD_Cols('TSF_IN_COST_LY');      
   CDD$TSF_IN_COST_TY          	        := CDD_Cols('TSF_IN_COST_TY');          	
   CDD$TSF_IN_BOOK_COST_LY     	        := CDD_Cols('TSF_IN_BOOK_COST_LY');     	
   CDD$TSF_IN_BOOK_COST_TY     	        := CDD_Cols('TSF_IN_BOOK_COST_TY');     	
   CDD$INTERCOMPANY_IN_COST_LY 	        := CDD_Cols('INTERCOMPANY_IN_COST_LY'); 	
   CDD$INTERCOMPANY_IN_COST_TY 	        := CDD_Cols('INTERCOMPANY_IN_COST_TY'); 	
   CDD$RECLASS_IN_COST_LY      	        := CDD_Cols('RECLASS_IN_COST_LY');      	
   CDD$RECLASS_IN_COST_TY      	        := CDD_Cols('RECLASS_IN_COST_TY');      	
   CDD$FRANCHISE_RETURNS_COST_LY         := CDD_Cols('FRANCHISE_RETURNS_COST_LY'); 
   CDD$FRANCHISE_RETURNS_COST_TY         := CDD_Cols('FRANCHISE_RETURNS_COST_TY'); 
   CDD$UP_CHRG_AMT_PROFIT_LY             := CDD_Cols('UP_CHRG_AMT_PROFIT_LY');     
   CDD$UP_CHRG_AMT_PROFIT_TY             := CDD_Cols('UP_CHRG_AMT_PROFIT_TY');     
   CDD$UP_CHRG_AMT_EXP_LY                := CDD_Cols('UP_CHRG_AMT_EXP_LY');        
   CDD$UP_CHRG_AMT_EXP_TY                := CDD_Cols('UP_CHRG_AMT_EXP_TY');        
   CDD$WO_ACTIVITY_UPD_INV_LY            := CDD_Cols('WO_ACTIVITY_UPD_INV_LY');    
   CDD$WO_ACTIVITY_UPD_INV_TY            := CDD_Cols('WO_ACTIVITY_UPD_INV_TY');    
   CDD$WO_ACTIVITY_POST_FIN_LY 	        := CDD_Cols('WO_ACTIVITY_POST_FIN_LY'); 	
   CDD$WO_ACTIVITY_POST_FIN_TY 	        := CDD_Cols('WO_ACTIVITY_POST_FIN_TY'); 	
   CDD$RECOVERABLE_TAX_LY                := CDD_Cols('RECOVERABLE_TAX_LY');        
   CDD$RECOVERABLE_TAX_TY                := CDD_Cols('RECOVERABLE_TAX_TY');        
   CDD$RTV_COST_LY             	        := CDD_Cols('RTV_COST_LY');             	
   CDD$RTV_COST_TY             	        := CDD_Cols('RTV_COST_TY');             	
   CDD$TSF_OUT_COST_LY         	        := CDD_Cols('TSF_OUT_COST_LY');         	
   CDD$TSF_OUT_COST_TY         	        := CDD_Cols('TSF_OUT_COST_TY');         	
   CDD$TSF_OUT_BOOK_COST_LY    	        := CDD_Cols('TSF_OUT_BOOK_COST_LY');    	
   CDD$TSF_OUT_BOOK_COST_TY    	        := CDD_Cols('TSF_OUT_BOOK_COST_TY');    	
   CDD$INTERCOMPANY_OUT_COST_LY	        := CDD_Cols('INTERCOMPANY_OUT_COST_LY');	
   CDD$INTERCOMPANY_OUT_COST_TY          := CDD_Cols('INTERCOMPANY_OUT_COST_TY');	
   CDD$RECLASS_OUT_COST_LY     	        := CDD_Cols('RECLASS_OUT_COST_LY');     	
   CDD$RECLASS_OUT_COST_TY     	        := CDD_Cols('RECLASS_OUT_COST_TY');     	
   CDD$NET_SALES_COST_LY       	        := CDD_Cols('NET_SALES_COST_LY');       	
   CDD$NET_SALES_COST_TY       	        := CDD_Cols('NET_SALES_COST_TY');       	
   CDD$RETURNS_COST_LY         	        := CDD_Cols('RETURNS_COST_LY');         	
   CDD$RETURNS_COST_TY         	        := CDD_Cols('RETURNS_COST_TY');         	
   CDD$FRANCHISE_SALES_COST_LY 	        := CDD_Cols('FRANCHISE_SALES_COST_LY'); 	
   CDD$FRANCHISE_SALES_COST_TY 	        := CDD_Cols('FRANCHISE_SALES_COST_TY'); 	
   CDD$FREIGHT_CLAIM_COST_LY   	        := CDD_Cols('FREIGHT_CLAIM_COST_LY');   	
   CDD$FREIGHT_CLAIM_COST_TY   	        := CDD_Cols('FREIGHT_CLAIM_COST_TY');   	
   CDD$STOCK_ADJ_COGS_COST_LY  	        := CDD_Cols('STOCK_ADJ_COGS_COST_LY');  	
   CDD$STOCK_ADJ_COGS_COST_TY  	        := CDD_Cols('STOCK_ADJ_COGS_COST_TY');  	
   CDD$STOCK_ADJ_COST_LY       	        := CDD_Cols('STOCK_ADJ_COST_LY');       	
   CDD$STOCK_ADJ_COST_TY       	        := CDD_Cols('STOCK_ADJ_COST_TY');       	
   CDD$COST_VARIANCE_AMT_LY    	        := CDD_Cols('COST_VARIANCE_AMT_LY');    	
   CDD$COST_VARIANCE_AMT_TY    	        := CDD_Cols('COST_VARIANCE_AMT_TY');    	
   CDD$MARGIN_COST_VARIANCE_LY 	        := CDD_Cols('MARGIN_COST_VARIANCE_LY'); 	
   CDD$MARGIN_COST_VARIANCE_TY 	        := CDD_Cols('MARGIN_COST_VARIANCE_TY'); 	
   CDD$VAT_IN_LY              	        := CDD_Cols('VAT_IN_LY');              	
   CDD$VAT_IN_TY                         := CDD_Cols('VAT_IN_TY');                 
   CDD$VAT_OUT_LY              	        := CDD_Cols('VAT_OUT_LY');              	
   CDD$VAT_OUT_TY              	        := CDD_Cols('VAT_OUT_TY');              	
   CDD$DEAL_INCOME_SALES_LY    	        := CDD_Cols('DEAL_INCOME_SALES_LY');    	
   CDD$DEAL_INCOME_SALES_TY    	        := CDD_Cols('DEAL_INCOME_SALES_TY');    	
   CDD$DEAL_INCOME_PURCH_LY    	        := CDD_Cols('DEAL_INCOME_PURCH_LY');    	
   CDD$DEAL_INCOME_PURCH_TY    	        := CDD_Cols('DEAL_INCOME_PURCH_TY');    	
   CDD$RESTOCKING_FEE_LY                 := CDD_Cols('RESTOCKING_FEE_LY');         
   CDD$RESTOCKING_FEE_TY                 := CDD_Cols('RESTOCKING_FEE_TY');         
   CDD$FRN_RESTOCKING_FEE_LY 	           := CDD_Cols('FRN_RESTOCKING_FEE_LY'); 	
   CDD$FRN_RESTOCKING_FEE_TY 	           := CDD_Cols('FRN_RESTOCKING_FEE_TY'); 	
   CDD$NET_SALES_NON_INV_COST_LY         := CDD_Cols('NET_SALES_NON_INV_COST_LY'); 
   CDD$NET_SALES_NON_INV_COST_TY         := CDD_Cols('NET_SALES_NON_INV_COST_TY'); 
   CDD$ADD_COST_LY             	        := CDD_Cols('ADD_COST_LY');             	
   CDD$ADD_COST_TY             	        := CDD_Cols('ADD_COST_TY');             	
   CDD$ADD_RETAIL_LY           	        := CDD_Cols('ADD_RETAIL_LY');           	
   CDD$ADD_RETAIL_TY           	        := CDD_Cols('ADD_RETAIL_TY');           	
   CDD$CASH_DISC_AMT_LY                  := CDD_Cols('CASH_DISC_AMT_LY');          
   CDD$CASH_DISC_AMT_TY                  := CDD_Cols('CASH_DISC_AMT_TY');          
   CDD$CLEAR_MARKDOWN_RETAIL_LY          := CDD_Cols('CLEAR_MARKDOWN_RETAIL_LY');  
   CDD$CLEAR_MARKDOWN_RETAIL_TY          := CDD_Cols('CLEAR_MARKDOWN_RETAIL_TY');  
   CDD$EMPL_DISC_RETAIL_LY               := CDD_Cols('EMPL_DISC_RETAIL_LY');       
   CDD$EMPL_DISC_RETAIL_TY               := CDD_Cols('EMPL_DISC_RETAIL_TY');    
   CDD$FRN_MARKDOWN_RETAIL_LY	           := CDD_Cols('FRN_MARKDOWN_RETAIL_LY');	
   CDD$FRN_MARKDOWN_RETAIL_TY	           := CDD_Cols('FRN_MARKDOWN_RETAIL_TY');	
   CDD$FRN_MARKUP_RETAIL_LY  	           := CDD_Cols('FRN_MARKUP_RETAIL_LY');  	
   CDD$FRN_MARKUP_RETAIL_TY  	           := CDD_Cols('FRN_MARKUP_RETAIL_TY');  	
   CDD$FREIGHT_COST_LY                   := CDD_Cols('FREIGHT_COST_LY');         
   CDD$FREIGHT_COST_TY                   := CDD_Cols('FREIGHT_COST_TY');         
   CDD$INTERCOMPANY_MARKDOWN_LY          := CDD_Cols('INTERCOMPANY_MARKDOWN_LY');
   CDD$INTERCOMPANY_MARKDOWN_TY          := CDD_Cols('INTERCOMPANY_MARKDOWN_TY');
   CDD$INTERCOMPANY_MARKUP_LY            := CDD_Cols('INTERCOMPANY_MARKUP_LY');  
   CDD$INTERCOMPANY_MARKUP_TY            := CDD_Cols('INTERCOMPANY_MARKUP_TY');  
   CDD$NET_MARKDOWN_LY                   := CDD_Cols('NET_MARKDOWN_LY');         
   CDD$NET_MARKDOWN_TY                   := CDD_Cols('NET_MARKDOWN_TY');         
   CDD$MARKDOWN_CAN_RETAIL_LY            := CDD_Cols('MARKDOWN_CAN_RETAIL_LY');  
   CDD$MARKDOWN_CAN_RETAIL_TY            := CDD_Cols('MARKDOWN_CAN_RETAIL_TY');  
   CDD$MARKUP_CAN_RETAIL_LY              := CDD_Cols('MARKUP_CAN_RETAIL_LY');    
   CDD$MARKUP_CAN_RETAIL_TY              := CDD_Cols('MARKUP_CAN_RETAIL_TY');    
   CDD$MARKUP_RETAIL_LY                  := CDD_Cols('MARKUP_RETAIL_LY');        
   CDD$MARKUP_RETAIL_TY                  := CDD_Cols('MARKUP_RETAIL_TY');        
   CDD$PERM_MARKDOWN_RETAIL_LY           := CDD_Cols('PERM_MARKDOWN_RETAIL_LY'); 
   CDD$PERM_MARKDOWN_RETAIL_TY           := CDD_Cols('PERM_MARKDOWN_RETAIL_TY'); 
   CDD$PROM_MARKDOWN_RETAIL_LY           := CDD_Cols('PROM_MARKDOWN_RETAIL_LY'); 
   CDD$PROM_MARKDOWN_RETAIL_TY           := CDD_Cols('PROM_MARKDOWN_RETAIL_TY'); 
   CDD$REC_COST_ADJ_VARIANCE_LY          := CDD_Cols('REC_COST_ADJ_VARIANCE_LY');
   CDD$REC_COST_ADJ_VARIANCE_TY          := CDD_Cols('REC_COST_ADJ_VARIANCE_TY');
   CDD$SALES_UNITS_LY                    := CDD_Cols('SALES_UNITS_LY');          
   CDD$SALES_UNITS_TY                    := CDD_Cols('SALES_UNITS_TY');          
   CDD$WORKROOM_AMT_LY                   := CDD_Cols('WORKROOM_AMT_LY');         
   CDD$WORKROOM_AMT_TY                   := CDD_Cols('WORKROOM_AMT_TY');         
   CDD$NET_RECLASS_COST_LY               := CDD_Cols('NET_RECLASS_COST_LY');     
   CDD$NET_RECLASS_COST_TY               := CDD_Cols('NET_RECLASS_COST_TY');     
   CDD$NET_TSF_COST_LY                   := CDD_Cols('NET_TSF_COST_LY');         
   CDD$NET_TSF_COST_TY                   := CDD_Cols('NET_TSF_COST_TY');         
   CDD$NET_MARKUP_LY                     := CDD_Cols('NET_MARKUP_LY');           
   CDD$NET_MARKUP_TY                     := CDD_Cols('NET_MARKUP_TY');             
   -- Cost Daily Columns End --
   -- Retail Month Data Columns Begin
   RMD_Cols                              := s9t_pkg.get_col_names(I_file_id,RMD_sheet);
   RMD$DEPT                              := RMD_Cols('DEPT');
   RMD$DEPT_NAME                         := RMD_Cols('DEPT_NAME');
   RMD$CLASS                             := RMD_Cols('CLASS');
   RMD$CLASS_NAME                        := RMD_Cols('CLASS_NAME');
   RMD$SUBCLASS                          := RMD_Cols('SUBCLASS');
   RMD$SUBCLASS_NAME                     := RMD_Cols('SUBCLASS_NAME');
   RMD$LOC_TYPE                          := RMD_Cols('LOC_TYPE');
   RMD$LOCATION                          := RMD_Cols('LOCATION');
   RMD$LOC_NAME                          := RMD_Cols('LOC_NAME');
   RMD$CURRENCY                          := RMD_Cols('CURRENCY');
   RMD$HALF_NO                           := RMD_Cols('HALF_NO');
   RMD$MONTH_NO                          := RMD_Cols('MONTH_NO');
   RMD$OPN_STK_RETAIL_LY                 := RMD_Cols('OPN_STK_RETAIL_LY');
   RMD$OPN_STK_RETAIL_TY                 := RMD_Cols('OPN_STK_RETAIL_TY');
   RMD$PURCH_RETAIL_LY                   := RMD_Cols('PURCH_RETAIL_LY');
   RMD$PURCH_RETAIL_TY                   := RMD_Cols('PURCH_RETAIL_TY');
   RMD$TSF_IN_RETAIL_LY                  := RMD_Cols('TSF_IN_RETAIL_LY');
   RMD$TSF_IN_RETAIL_TY                  := RMD_Cols('TSF_IN_RETAIL_TY');
   RMD$TSF_IN_BOOK_RETAIL_LY             := RMD_Cols('TSF_IN_BOOK_RETAIL_LY');
   RMD$TSF_IN_BOOK_RETAIL_TY             := RMD_Cols('TSF_IN_BOOK_RETAIL_TY');
   RMD$INTERCOMPANY_IN_RETAIL_LY         := RMD_Cols('INTERCOMPANY_IN_RETAIL_LY');
   RMD$INTERCOMPANY_IN_RETAIL_TY         := RMD_Cols('INTERCOMPANY_IN_RETAIL_TY');
   RMD$RECLASS_IN_RETAIL_LY              := RMD_Cols('RECLASS_IN_RETAIL_LY');
   RMD$RECLASS_IN_RETAIL_TY              := RMD_Cols('RECLASS_IN_RETAIL_TY');
   RMD$FRN_RETURNS_RETAIL_LY             := RMD_Cols('FRN_RETURNS_RETAIL_LY');
   RMD$FRN_RETURNS_RETAIL_TY             := RMD_Cols('FRN_RETURNS_RETAIL_TY');
   RMD$MARKUP_RETAIL_LY                  := RMD_Cols('MARKUP_RETAIL_LY');
   RMD$MARKUP_RETAIL_TY                  := RMD_Cols('MARKUP_RETAIL_TY');
   RMD$FRN_MARKUP_RETAIL_LY              := RMD_Cols('FRN_MARKUP_RETAIL_LY');
   RMD$FRN_MARKUP_RETAIL_TY              := RMD_Cols('FRN_MARKUP_RETAIL_TY');
   RMD$INTERCOMPANY_MARKUP_LY            := RMD_Cols('INTERCOMPANY_MARKUP_LY');
   RMD$INTERCOMPANY_MARKUP_TY            := RMD_Cols('INTERCOMPANY_MARKUP_TY');
   RMD$MARKUP_CAN_RETAIL_LY              := RMD_Cols('MARKUP_CAN_RETAIL_LY');
   RMD$MARKUP_CAN_RETAIL_TY              := RMD_Cols('MARKUP_CAN_RETAIL_TY');
   RMD$RTV_RETAIL_LY                     := RMD_Cols('RTV_RETAIL_LY');
   RMD$RTV_RETAIL_TY                     := RMD_Cols('RTV_RETAIL_TY');
   RMD$TSF_OUT_RETAIL_LY                 := RMD_Cols('TSF_OUT_RETAIL_LY');
   RMD$TSF_OUT_RETAIL_TY                 := RMD_Cols('TSF_OUT_RETAIL_TY');
   RMD$TSF_OUT_BOOK_RETAIL_LY            := RMD_Cols('TSF_OUT_BOOK_RETAIL_LY');
   RMD$TSF_OUT_BOOK_RETAIL_TY            := RMD_Cols('TSF_OUT_BOOK_RETAIL_TY');
   RMD$INTERCOMPANY_OUT_RETAIL_LY        := RMD_Cols('INTERCOMPANY_OUT_RETAIL_LY');
   RMD$INTERCOMPANY_OUT_RETAIL_TY        := RMD_Cols('INTERCOMPANY_OUT_RETAIL_TY');
   RMD$RECLASS_OUT_RETAIL_LY             := RMD_Cols('RECLASS_OUT_RETAIL_LY');
   RMD$RECLASS_OUT_RETAIL_TY             := RMD_Cols('RECLASS_OUT_RETAIL_TY');
   RMD$NET_SALES_RETAIL_LY               := RMD_Cols('NET_SALES_RETAIL_LY');
   RMD$NET_SALES_RETAIL_TY               := RMD_Cols('NET_SALES_RETAIL_TY');
   RMD$RETURNS_RETAIL_LY                 := RMD_Cols('RETURNS_RETAIL_LY');
   RMD$RETURNS_RETAIL_TY                 := RMD_Cols('RETURNS_RETAIL_TY');
   RMD$WEIGHT_VARIANCE_RETAIL_LY         := RMD_Cols('WEIGHT_VARIANCE_RETAIL_LY');
   RMD$WEIGHT_VARIANCE_RETAIL_TY         := RMD_Cols('WEIGHT_VARIANCE_RETAIL_TY');
   RMD$EMPL_DISC_RETAIL_LY               := RMD_Cols('EMPL_DISC_RETAIL_LY');
   RMD$EMPL_DISC_RETAIL_TY               := RMD_Cols('EMPL_DISC_RETAIL_TY');
   RMD$FRANCHISE_SALES_RETAIL_LY         := RMD_Cols('FRANCHISE_SALES_RETAIL_LY');
   RMD$FRANCHISE_SALES_RETAIL_TY         := RMD_Cols('FRANCHISE_SALES_RETAIL_TY');
   RMD$FREIGHT_CLAIM_RETAIL_LY           := RMD_Cols('FREIGHT_CLAIM_RETAIL_LY');
   RMD$FREIGHT_CLAIM_RETAIL_TY           := RMD_Cols('FREIGHT_CLAIM_RETAIL_TY');
   RMD$STOCK_ADJ_COGS_RETAIL_LY          := RMD_Cols('STOCK_ADJ_COGS_RETAIL_LY');
   RMD$STOCK_ADJ_COGS_RETAIL_TY          := RMD_Cols('STOCK_ADJ_COGS_RETAIL_TY');
   RMD$STOCK_ADJ_RETAIL_LY               := RMD_Cols('STOCK_ADJ_RETAIL_LY');
   RMD$STOCK_ADJ_RETAIL_TY               := RMD_Cols('STOCK_ADJ_RETAIL_TY');
   RMD$SHRINKAGE_RETAIL_LY               := RMD_Cols('SHRINKAGE_RETAIL_LY');
   RMD$SHRINKAGE_RETAIL_TY               := RMD_Cols('SHRINKAGE_RETAIL_TY');
   RMD$PERM_MARKDOWN_RETAIL_LY           := RMD_Cols('PERM_MARKDOWN_RETAIL_LY');
   RMD$PERM_MARKDOWN_RETAIL_TY           := RMD_Cols('PERM_MARKDOWN_RETAIL_TY');
   RMD$PROM_MARKDOWN_RETAIL_LY           := RMD_Cols('PROM_MARKDOWN_RETAIL_LY');
   RMD$PROM_MARKDOWN_RETAIL_TY           := RMD_Cols('PROM_MARKDOWN_RETAIL_TY');
   RMD$CLEAR_MARKDOWN_RETAIL_LY          := RMD_Cols('CLEAR_MARKDOWN_RETAIL_LY');
   RMD$CLEAR_MARKDOWN_RETAIL_TY          := RMD_Cols('CLEAR_MARKDOWN_RETAIL_TY');
   RMD$INTERCOMPANY_MARKDOWN_LY          := RMD_Cols('INTERCOMPANY_MARKDOWN_LY');
   RMD$INTERCOMPANY_MARKDOWN_TY          := RMD_Cols('INTERCOMPANY_MARKDOWN_TY');
   RMD$FRN_MARKDOWN_RETAIL_LY            := RMD_Cols('FRN_MARKDOWN_RETAIL_LY');
   RMD$FRN_MARKDOWN_RETAIL_TY            := RMD_Cols('FRN_MARKDOWN_RETAIL_TY');
   RMD$MARKDOWN_CAN_RETAIL_LY            := RMD_Cols('MARKDOWN_CAN_RETAIL_LY');
   RMD$MARKDOWN_CAN_RETAIL_TY            := RMD_Cols('MARKDOWN_CAN_RETAIL_TY');
   RMD$CLS_STK_RETAIL_LY                 := RMD_Cols('CLS_STK_RETAIL_LY');
   RMD$CLS_STK_RETAIL_TY                 := RMD_Cols('CLS_STK_RETAIL_TY');
   RMD$ADD_RETAIL_LY                     := RMD_Cols('ADD_RETAIL_LY');
   RMD$ADD_RETAIL_TY                     := RMD_Cols('ADD_RETAIL_TY');
   RMD$RED_RETAIL_LY                     := RMD_Cols('RED_RETAIL_LY');
   RMD$RED_RETAIL_TY                     := RMD_Cols('RED_RETAIL_TY');
   RMD$ADD_COST_LY                       := RMD_Cols('ADD_COST_LY');
   RMD$ADD_COST_TY                       := RMD_Cols('ADD_COST_TY');
   RMD$HTD_GAFS_RETAIL_LY                := RMD_Cols('HTD_GAFS_RETAIL_LY');
   RMD$HTD_GAFS_RETAIL_TY                := RMD_Cols('HTD_GAFS_RETAIL_TY');
   RMD$HTD_GAFS_COST_LY                  := RMD_Cols('HTD_GAFS_COST_LY');
   RMD$HTD_GAFS_COST_TY                  := RMD_Cols('HTD_GAFS_COST_TY');
   RMD$CUM_MARKON_PCT_LY                 := RMD_Cols('CUM_MARKON_PCT_LY');
   RMD$CUM_MARKON_PCT_TY                 := RMD_Cols('CUM_MARKON_PCT_TY');
   RMD$GROSS_MARGIN_AMT_LY               := RMD_Cols('GROSS_MARGIN_AMT_LY');
   RMD$GROSS_MARGIN_AMT_TY               := RMD_Cols('GROSS_MARGIN_AMT_TY');
   RMD$VAT_IN_LY                         := RMD_Cols('VAT_IN_LY');
   RMD$VAT_IN_TY                         := RMD_Cols('VAT_IN_TY');
   RMD$VAT_OUT_LY                        := RMD_Cols('VAT_OUT_LY');
   RMD$VAT_OUT_TY                        := RMD_Cols('VAT_OUT_TY');
   RMD$UP_CHRG_AMT_PROFIT_LY             := RMD_Cols('UP_CHRG_AMT_PROFIT_LY');
   RMD$UP_CHRG_AMT_PROFIT_TY             := RMD_Cols('UP_CHRG_AMT_PROFIT_TY');
   RMD$UP_CHRG_AMT_EXP_LY                := RMD_Cols('UP_CHRG_AMT_EXP_LY');
   RMD$UP_CHRG_AMT_EXP_TY                := RMD_Cols('UP_CHRG_AMT_EXP_TY');
   RMD$WO_ACTIVITY_UPD_INV_LY            := RMD_Cols('WO_ACTIVITY_UPD_INV_LY');
   RMD$WO_ACTIVITY_UPD_INV_TY            := RMD_Cols('WO_ACTIVITY_UPD_INV_TY');
   RMD$WO_ACTIVITY_POST_FIN_LY           := RMD_Cols('WO_ACTIVITY_POST_FIN_LY');
   RMD$WO_ACTIVITY_POST_FIN_TY           := RMD_Cols('WO_ACTIVITY_POST_FIN_TY');
   RMD$FREIGHT_COST_LY                   := RMD_Cols('FREIGHT_COST_LY');
   RMD$FREIGHT_COST_TY                   := RMD_Cols('FREIGHT_COST_TY');
   RMD$WORKROOM_AMT_LY                   := RMD_Cols('WORKROOM_AMT_LY');
   RMD$WORKROOM_AMT_TY                   := RMD_Cols('WORKROOM_AMT_TY');
   RMD$CASH_DISC_AMT_LY                  := RMD_Cols('CASH_DISC_AMT_LY');
   RMD$CASH_DISC_AMT_TY                  := RMD_Cols('CASH_DISC_AMT_TY');
   RMD$COST_VARIANCE_AMT_LY              := RMD_Cols('COST_VARIANCE_AMT_LY');
   RMD$COST_VARIANCE_AMT_TY              := RMD_Cols('COST_VARIANCE_AMT_TY');
   RMD$REC_COST_ADJ_VARIANCE_LY          := RMD_Cols('REC_COST_ADJ_VARIANCE_LY');
   RMD$REC_COST_ADJ_VARIANCE_TY          := RMD_Cols('REC_COST_ADJ_VARIANCE_TY');
   RMD$RETAIL_COST_VARIANCE_LY           := RMD_Cols('RETAIL_COST_VARIANCE_LY');
   RMD$RETAIL_COST_VARIANCE_TY           := RMD_Cols('RETAIL_COST_VARIANCE_TY');
   RMD$DEAL_INCOME_PURCH_LY              := RMD_Cols('DEAL_INCOME_PURCH_LY');
   RMD$DEAL_INCOME_PURCH_TY              := RMD_Cols('DEAL_INCOME_PURCH_TY');
   RMD$DEAL_INCOME_SALES_LY              := RMD_Cols('DEAL_INCOME_SALES_LY');
   RMD$DEAL_INCOME_SALES_TY              := RMD_Cols('DEAL_INCOME_SALES_TY');
   RMD$FRN_RESTOCKING_FEE_LY             := RMD_Cols('FRN_RESTOCKING_FEE_LY');
   RMD$FRN_RESTOCKING_FEE_TY             := RMD_Cols('FRN_RESTOCKING_FEE_TY');
   RMD$INTERCOMPANY_MARGIN_LY            := RMD_Cols('INTERCOMPANY_MARGIN_LY');
   RMD$INTERCOMPANY_MARGIN_TY            := RMD_Cols('INTERCOMPANY_MARGIN_TY');
   RMD$MARGIN_COST_VARIANCE_LY           := RMD_Cols('MARGIN_COST_VARIANCE_LY');
   RMD$MARGIN_COST_VARIANCE_TY           := RMD_Cols('MARGIN_COST_VARIANCE_TY');
   RMD$NET_MARKDOWN_LY                   := RMD_Cols('NET_MARKDOWN_LY');
   RMD$NET_MARKDOWN_TY                   := RMD_Cols('NET_MARKDOWN_TY');
   RMD$NET_MARKUP_LY                     := RMD_Cols('NET_MARKUP_LY');
   RMD$NET_MARKUP_TY                     := RMD_Cols('NET_MARKUP_TY');
   RMD$NET_RECLASS_RETAIL_LY             := RMD_Cols('NET_RECLASS_RETAIL_LY');
   RMD$NET_RECLASS_RETAIL_TY             := RMD_Cols('NET_RECLASS_RETAIL_TY');
   RMD$NET_SALES_RETAIL_EX_VAT_LY        := RMD_Cols('NET_SALES_RETAIL_EX_VAT_LY');
   RMD$NET_SALES_RETAIL_EX_VAT_TY        := RMD_Cols('NET_SALES_RETAIL_EX_VAT_TY');
   RMD$NET_SALE_NONINV_R_EXVAT_LY        := RMD_Cols('NET_SALE_NONINV_R_EXVAT_LY');
   RMD$NET_SALE_NONINV_R_EXVAT_TY        := RMD_Cols('NET_SALE_NONINV_R_EXVAT_TY');
   RMD$NET_TSF_RETAIL_LY                 := RMD_Cols('NET_TSF_RETAIL_LY');
   RMD$NET_TSF_RETAIL_TY                 := RMD_Cols('NET_TSF_RETAIL_TY');
   RMD$NET_TSF_COST_LY                   := RMD_Cols('NET_TSF_COST_LY');
   RMD$NET_TSF_COST_TY                   := RMD_Cols('NET_TSF_COST_TY');
   RMD$RECOVERABLE_TAX_LY                := RMD_Cols('RECOVERABLE_TAX_LY');
   RMD$RECOVERABLE_TAX_TY                := RMD_Cols('RECOVERABLE_TAX_TY');
   RMD$RESTOCKING_FEE_LY                 := RMD_Cols('RESTOCKING_FEE_LY');
   RMD$RESTOCKING_FEE_TY                 := RMD_Cols('RESTOCKING_FEE_TY');
   RMD$SHRINK_RETAIL_PCT_LY              := RMD_Cols('SHRINK_RETAIL_PCT_LY');
   RMD$SHRINK_RETAIL_PCT_TY              := RMD_Cols('SHRINK_RETAIL_PCT_TY');
   RMD$SALES_UNITS_LY                    := RMD_Cols('SALES_UNITS_LY');
   RMD$SALES_UNITS_TY                    := RMD_Cols('SALES_UNITS_TY');
   RMD$NET_SALES_NONINV_RETAIL_LY        := RMD_Cols('NET_SALES_NONINV_RETAIL_LY');
   RMD$NET_SALES_NONINV_RETAIL_TY        := RMD_Cols('NET_SALES_NONINV_RETAIL_TY');
   -- Retail Month Data Columns End
   
   -- Retail Week Data Columns Begin
   RWD_Cols                              := s9t_pkg.get_col_names(I_file_id,RWD_sheet);
   RWD$DEPT                              := RWD_Cols('DEPT');
   RWD$DEPT_NAME                         := RWD_Cols('DEPT_NAME');
   RWD$CLASS                             := RWD_Cols('CLASS');
   RWD$CLASS_NAME                        := RWD_Cols('CLASS_NAME');
   RWD$SUBCLASS                          := RWD_Cols('SUBCLASS');
   RWD$SUBCLASS_NAME                     := RWD_Cols('SUBCLASS_NAME');
   RWD$LOC_TYPE                          := RWD_Cols('LOC_TYPE');
   RWD$LOCATION                          := RWD_Cols('LOCATION');
   RWD$LOC_NAME                          := RWD_Cols('LOC_NAME');
   RWD$CURRENCY                          := RWD_Cols('CURRENCY');
   RWD$EOM_DATE                          := RWD_Cols('EOM_DATE');
   RWD$WEEK_NO                           := RWD_Cols('WEEK_NO');
   RWD$OPN_STK_RETAIL_LY                 := RWD_Cols('OPN_STK_RETAIL_LY');
   RWD$OPN_STK_RETAIL_TY                 := RWD_Cols('OPN_STK_RETAIL_TY');
   RWD$PURCH_RETAIL_LY                   := RWD_Cols('PURCH_RETAIL_LY');
   RWD$PURCH_RETAIL_TY                   := RWD_Cols('PURCH_RETAIL_TY');
   RWD$TSF_IN_RETAIL_LY                  := RWD_Cols('TSF_IN_RETAIL_LY');
   RWD$TSF_IN_RETAIL_TY                  := RWD_Cols('TSF_IN_RETAIL_TY');
   RWD$TSF_IN_BOOK_RETAIL_LY             := RWD_Cols('TSF_IN_BOOK_RETAIL_LY');
   RWD$TSF_IN_BOOK_RETAIL_TY             := RWD_Cols('TSF_IN_BOOK_RETAIL_TY');
   RWD$INTERCOMPANY_IN_RETAIL_LY         := RWD_Cols('INTERCOMPANY_IN_RETAIL_LY');
   RWD$INTERCOMPANY_IN_RETAIL_TY         := RWD_Cols('INTERCOMPANY_IN_RETAIL_TY');
   RWD$RECLASS_IN_RETAIL_LY              := RWD_Cols('RECLASS_IN_RETAIL_LY');
   RWD$RECLASS_IN_RETAIL_TY              := RWD_Cols('RECLASS_IN_RETAIL_TY');
   RWD$FRN_RETURNS_RETAIL_LY             := RWD_Cols('FRN_RETURNS_RETAIL_LY');
   RWD$FRN_RETURNS_RETAIL_TY             := RWD_Cols('FRN_RETURNS_RETAIL_TY');
   RWD$MARKUP_RETAIL_LY                  := RWD_Cols('MARKUP_RETAIL_LY');
   RWD$MARKUP_RETAIL_TY                  := RWD_Cols('MARKUP_RETAIL_TY');
   RWD$FRN_MARKUP_RETAIL_LY              := RWD_Cols('FRN_MARKUP_RETAIL_LY');
   RWD$FRN_MARKUP_RETAIL_TY              := RWD_Cols('FRN_MARKUP_RETAIL_TY');
   RWD$INTERCOMPANY_MARKUP_LY            := RWD_Cols('INTERCOMPANY_MARKUP_LY');
   RWD$INTERCOMPANY_MARKUP_TY            := RWD_Cols('INTERCOMPANY_MARKUP_TY');
   RWD$MARKUP_CAN_RETAIL_LY              := RWD_Cols('MARKUP_CAN_RETAIL_LY');
   RWD$MARKUP_CAN_RETAIL_TY              := RWD_Cols('MARKUP_CAN_RETAIL_TY');
   RWD$RTV_RETAIL_LY                     := RWD_Cols('RTV_RETAIL_LY');
   RWD$RTV_RETAIL_TY                     := RWD_Cols('RTV_RETAIL_TY');
   RWD$TSF_OUT_RETAIL_LY                 := RWD_Cols('TSF_OUT_RETAIL_LY');
   RWD$TSF_OUT_RETAIL_TY                 := RWD_Cols('TSF_OUT_RETAIL_TY');
   RWD$TSF_OUT_BOOK_RETAIL_LY            := RWD_Cols('TSF_OUT_BOOK_RETAIL_LY');
   RWD$TSF_OUT_BOOK_RETAIL_TY            := RWD_Cols('TSF_OUT_BOOK_RETAIL_TY');
   RWD$INTERCOMPANY_OUT_RETAIL_LY        := RWD_Cols('INTERCOMPANY_OUT_RETAIL_LY');
   RWD$INTERCOMPANY_OUT_RETAIL_TY        := RWD_Cols('INTERCOMPANY_OUT_RETAIL_TY');
   RWD$RECLASS_OUT_RETAIL_LY             := RWD_Cols('RECLASS_OUT_RETAIL_LY');
   RWD$RECLASS_OUT_RETAIL_TY             := RWD_Cols('RECLASS_OUT_RETAIL_TY');
   RWD$NET_SALES_RETAIL_LY               := RWD_Cols('NET_SALES_RETAIL_LY');
   RWD$NET_SALES_RETAIL_TY               := RWD_Cols('NET_SALES_RETAIL_TY');
   RWD$RETURNS_RETAIL_LY                 := RWD_Cols('RETURNS_RETAIL_LY');
   RWD$RETURNS_RETAIL_TY                 := RWD_Cols('RETURNS_RETAIL_TY');
   RWD$WEIGHT_VARIANCE_RETAIL_LY         := RWD_Cols('WEIGHT_VARIANCE_RETAIL_LY');
   RWD$WEIGHT_VARIANCE_RETAIL_TY         := RWD_Cols('WEIGHT_VARIANCE_RETAIL_TY');
   RWD$EMPL_DISC_RETAIL_LY               := RWD_Cols('EMPL_DISC_RETAIL_LY');
   RWD$EMPL_DISC_RETAIL_TY               := RWD_Cols('EMPL_DISC_RETAIL_TY');
   RWD$FRANCHISE_SALES_RETAIL_LY         := RWD_Cols('FRANCHISE_SALES_RETAIL_LY');
   RWD$FRANCHISE_SALES_RETAIL_TY         := RWD_Cols('FRANCHISE_SALES_RETAIL_TY');
   RWD$FREIGHT_CLAIM_RETAIL_LY           := RWD_Cols('FREIGHT_CLAIM_RETAIL_LY');
   RWD$FREIGHT_CLAIM_RETAIL_TY           := RWD_Cols('FREIGHT_CLAIM_RETAIL_TY');
   RWD$STOCK_ADJ_COGS_RETAIL_LY          := RWD_Cols('STOCK_ADJ_COGS_RETAIL_LY');
   RWD$STOCK_ADJ_COGS_RETAIL_TY          := RWD_Cols('STOCK_ADJ_COGS_RETAIL_TY');
   RWD$STOCK_ADJ_RETAIL_LY               := RWD_Cols('STOCK_ADJ_RETAIL_LY');
   RWD$STOCK_ADJ_RETAIL_TY               := RWD_Cols('STOCK_ADJ_RETAIL_TY');
   RWD$SHRINKAGE_RETAIL_LY               := RWD_Cols('SHRINKAGE_RETAIL_LY');
   RWD$SHRINKAGE_RETAIL_TY               := RWD_Cols('SHRINKAGE_RETAIL_TY');
   RWD$PERM_MARKDOWN_RETAIL_LY           := RWD_Cols('PERM_MARKDOWN_RETAIL_LY');
   RWD$PERM_MARKDOWN_RETAIL_TY           := RWD_Cols('PERM_MARKDOWN_RETAIL_TY');
   RWD$PROM_MARKDOWN_RETAIL_LY           := RWD_Cols('PROM_MARKDOWN_RETAIL_LY');
   RWD$PROM_MARKDOWN_RETAIL_TY           := RWD_Cols('PROM_MARKDOWN_RETAIL_TY');
   RWD$CLEAR_MARKDOWN_RETAIL_LY          := RWD_Cols('CLEAR_MARKDOWN_RETAIL_LY');
   RWD$CLEAR_MARKDOWN_RETAIL_TY          := RWD_Cols('CLEAR_MARKDOWN_RETAIL_TY');
   RWD$INTERCOMPANY_MARKDOWN_LY          := RWD_Cols('INTERCOMPANY_MARKDOWN_LY');
   RWD$INTERCOMPANY_MARKDOWN_TY          := RWD_Cols('INTERCOMPANY_MARKDOWN_TY');
   RWD$FRN_MARKDOWN_RETAIL_LY            := RWD_Cols('FRN_MARKDOWN_RETAIL_LY');
   RWD$FRN_MARKDOWN_RETAIL_TY            := RWD_Cols('FRN_MARKDOWN_RETAIL_TY');
   RWD$MARKDOWN_CAN_RETAIL_LY            := RWD_Cols('MARKDOWN_CAN_RETAIL_LY');
   RWD$MARKDOWN_CAN_RETAIL_TY            := RWD_Cols('MARKDOWN_CAN_RETAIL_TY');
   RWD$CLS_STK_RETAIL_LY                 := RWD_Cols('CLS_STK_RETAIL_LY');
   RWD$CLS_STK_RETAIL_TY                 := RWD_Cols('CLS_STK_RETAIL_TY');
   RWD$ADD_RETAIL_LY                     := RWD_Cols('ADD_RETAIL_LY');
   RWD$ADD_RETAIL_TY                     := RWD_Cols('ADD_RETAIL_TY');
   RWD$RED_RETAIL_LY                     := RWD_Cols('RED_RETAIL_LY');
   RWD$RED_RETAIL_TY                     := RWD_Cols('RED_RETAIL_TY');
   RWD$ADD_COST_LY                       := RWD_Cols('ADD_COST_LY');
   RWD$ADD_COST_TY                       := RWD_Cols('ADD_COST_TY');
   RWD$HTD_GAFS_RETAIL_LY                := RWD_Cols('HTD_GAFS_RETAIL_LY');
   RWD$HTD_GAFS_RETAIL_TY                := RWD_Cols('HTD_GAFS_RETAIL_TY');
   RWD$HTD_GAFS_COST_LY                  := RWD_Cols('HTD_GAFS_COST_LY');
   RWD$HTD_GAFS_COST_TY                  := RWD_Cols('HTD_GAFS_COST_TY');
   RWD$CUM_MARKON_PCT_LY                 := RWD_Cols('CUM_MARKON_PCT_LY');
   RWD$CUM_MARKON_PCT_TY                 := RWD_Cols('CUM_MARKON_PCT_TY');
   RWD$GROSS_MARGIN_AMT_LY               := RWD_Cols('GROSS_MARGIN_AMT_LY');
   RWD$GROSS_MARGIN_AMT_TY               := RWD_Cols('GROSS_MARGIN_AMT_TY');
   RWD$VAT_IN_LY                         := RWD_Cols('VAT_IN_LY');
   RWD$VAT_IN_TY                         := RWD_Cols('VAT_IN_TY');
   RWD$VAT_OUT_LY                        := RWD_Cols('VAT_OUT_LY');
   RWD$VAT_OUT_TY                        := RWD_Cols('VAT_OUT_TY');
   RWD$UP_CHRG_AMT_PROFIT_LY             := RWD_Cols('UP_CHRG_AMT_PROFIT_LY');
   RWD$UP_CHRG_AMT_PROFIT_TY             := RWD_Cols('UP_CHRG_AMT_PROFIT_TY');
   RWD$UP_CHRG_AMT_EXP_LY                := RWD_Cols('UP_CHRG_AMT_EXP_LY');
   RWD$UP_CHRG_AMT_EXP_TY                := RWD_Cols('UP_CHRG_AMT_EXP_TY');
   RWD$WO_ACTIVITY_UPD_INV_LY            := RWD_Cols('WO_ACTIVITY_UPD_INV_LY');
   RWD$WO_ACTIVITY_UPD_INV_TY            := RWD_Cols('WO_ACTIVITY_UPD_INV_TY');
   RWD$WO_ACTIVITY_POST_FIN_LY           := RWD_Cols('WO_ACTIVITY_POST_FIN_LY');
   RWD$WO_ACTIVITY_POST_FIN_TY           := RWD_Cols('WO_ACTIVITY_POST_FIN_TY');
   RWD$FREIGHT_COST_LY                   := RWD_Cols('FREIGHT_COST_LY');
   RWD$FREIGHT_COST_TY                   := RWD_Cols('FREIGHT_COST_TY');
   RWD$WORKROOM_AMT_LY                   := RWD_Cols('WORKROOM_AMT_LY');
   RWD$WORKROOM_AMT_TY                   := RWD_Cols('WORKROOM_AMT_TY');
   RWD$CASH_DISC_AMT_LY                  := RWD_Cols('CASH_DISC_AMT_LY');
   RWD$CASH_DISC_AMT_TY                  := RWD_Cols('CASH_DISC_AMT_TY');
   RWD$COST_VARIANCE_AMT_LY              := RWD_Cols('COST_VARIANCE_AMT_LY');
   RWD$COST_VARIANCE_AMT_TY              := RWD_Cols('COST_VARIANCE_AMT_TY');
   RWD$REC_COST_ADJ_VARIANCE_LY          := RWD_Cols('REC_COST_ADJ_VARIANCE_LY');
   RWD$REC_COST_ADJ_VARIANCE_TY          := RWD_Cols('REC_COST_ADJ_VARIANCE_TY');
   RWD$RETAIL_COST_VARIANCE_LY           := RWD_Cols('RETAIL_COST_VARIANCE_LY');
   RWD$RETAIL_COST_VARIANCE_TY           := RWD_Cols('RETAIL_COST_VARIANCE_TY');
   RWD$DEAL_INCOME_PURCH_LY              := RWD_Cols('DEAL_INCOME_PURCH_LY');
   RWD$DEAL_INCOME_PURCH_TY              := RWD_Cols('DEAL_INCOME_PURCH_TY');
   RWD$DEAL_INCOME_SALES_LY              := RWD_Cols('DEAL_INCOME_SALES_LY');
   RWD$DEAL_INCOME_SALES_TY              := RWD_Cols('DEAL_INCOME_SALES_TY');
   RWD$FRN_RESTOCKING_FEE_LY             := RWD_Cols('FRN_RESTOCKING_FEE_LY');
   RWD$FRN_RESTOCKING_FEE_TY             := RWD_Cols('FRN_RESTOCKING_FEE_TY');
   RWD$INTERCOMPANY_MARGIN_LY            := RWD_Cols('INTERCOMPANY_MARGIN_LY');
   RWD$INTERCOMPANY_MARGIN_TY            := RWD_Cols('INTERCOMPANY_MARGIN_TY');
   RWD$MARGIN_COST_VARIANCE_LY           := RWD_Cols('MARGIN_COST_VARIANCE_LY');
   RWD$MARGIN_COST_VARIANCE_TY           := RWD_Cols('MARGIN_COST_VARIANCE_TY');
   RWD$NET_MARKDOWN_LY                   := RWD_Cols('NET_MARKDOWN_LY');
   RWD$NET_MARKDOWN_TY                   := RWD_Cols('NET_MARKDOWN_TY');
   RWD$NET_MARKUP_LY                     := RWD_Cols('NET_MARKUP_LY');
   RWD$NET_MARKUP_TY                     := RWD_Cols('NET_MARKUP_TY');
   RWD$NET_RECLASS_RETAIL_LY             := RWD_Cols('NET_RECLASS_RETAIL_LY');
   RWD$NET_RECLASS_RETAIL_TY             := RWD_Cols('NET_RECLASS_RETAIL_TY');
   RWD$NET_SALES_RETAIL_EX_VAT_LY        := RWD_Cols('NET_SALES_RETAIL_EX_VAT_LY');
   RWD$NET_SALES_RETAIL_EX_VAT_TY        := RWD_Cols('NET_SALES_RETAIL_EX_VAT_TY');
   RWD$NET_SALE_NONINV_R_EXVAT_LY        := RWD_Cols('NET_SALE_NONINV_R_EXVAT_LY');
   RWD$NET_SALE_NONINV_R_EXVAT_TY        := RWD_Cols('NET_SALE_NONINV_R_EXVAT_TY');
   RWD$NET_TSF_RETAIL_LY                 := RWD_Cols('NET_TSF_RETAIL_LY');
   RWD$NET_TSF_RETAIL_TY                 := RWD_Cols('NET_TSF_RETAIL_TY');
   RWD$NET_TSF_COST_LY                   := RWD_Cols('NET_TSF_COST_LY');
   RWD$NET_TSF_COST_TY                   := RWD_Cols('NET_TSF_COST_TY');
   RWD$RECOVERABLE_TAX_LY                := RWD_Cols('RECOVERABLE_TAX_LY');
   RWD$RECOVERABLE_TAX_TY                := RWD_Cols('RECOVERABLE_TAX_TY');
   RWD$RESTOCKING_FEE_LY                 := RWD_Cols('RESTOCKING_FEE_LY');
   RWD$RESTOCKING_FEE_TY                 := RWD_Cols('RESTOCKING_FEE_TY');
   RWD$SHRINK_RETAIL_PCT_LY              := RWD_Cols('SHRINK_RETAIL_PCT_LY');
   RWD$SHRINK_RETAIL_PCT_TY              := RWD_Cols('SHRINK_RETAIL_PCT_TY');
   RWD$SALES_UNITS_LY                    := RWD_Cols('SALES_UNITS_LY');
   RWD$SALES_UNITS_TY                    := RWD_Cols('SALES_UNITS_TY');
   RWD$NET_SALES_NONINV_RETAIL_LY        := RWD_Cols('NET_SALES_NONINV_RETAIL_LY');
   RWD$NET_SALES_NONINV_RETAIL_TY        := RWD_Cols('NET_SALES_NONINV_RETAIL_TY');
   -- Retail Week Data Columns End
   
   -- Retail Daily Data Columns Begin
   RDD_Cols                              := s9t_pkg.get_col_names(I_file_id,RDD_sheet);
   RDD$DEPT                              := RDD_Cols('DEPT');
   RDD$DEPT_NAME                         := RDD_Cols('DEPT_NAME');
   RDD$CLASS                             := RDD_Cols('CLASS');
   RDD$CLASS_NAME                        := RDD_Cols('CLASS_NAME');
   RDD$SUBCLASS                          := RDD_Cols('SUBCLASS');
   RDD$SUBCLASS_NAME                     := RDD_Cols('SUBCLASS_NAME');
   RDD$LOC_TYPE                          := RDD_Cols('LOC_TYPE');
   RDD$LOCATION                          := RDD_Cols('LOCATION');
   RDD$LOC_NAME                          := RDD_Cols('LOC_NAME');
   RDD$CURRENCY                          := RDD_Cols('CURRENCY');
   RDD$EOW_DATE                          := RDD_Cols('EOW_DATE');
   RDD$DAY_NO                            := RDD_Cols('DAY_NO');
   RDD$PURCH_RETAIL_LY                   := RDD_Cols('PURCH_RETAIL_LY');
   RDD$PURCH_RETAIL_TY                   := RDD_Cols('PURCH_RETAIL_TY');
   RDD$TSF_IN_RETAIL_LY                  := RDD_Cols('TSF_IN_RETAIL_LY');
   RDD$TSF_IN_RETAIL_TY                  := RDD_Cols('TSF_IN_RETAIL_TY');
   RDD$TSF_IN_BOOK_RETAIL_LY             := RDD_Cols('TSF_IN_BOOK_RETAIL_LY');
   RDD$TSF_IN_BOOK_RETAIL_TY             := RDD_Cols('TSF_IN_BOOK_RETAIL_TY');
   RDD$INTERCOMPANY_IN_RETAIL_LY         := RDD_Cols('INTERCOMPANY_IN_RETAIL_LY');
   RDD$INTERCOMPANY_IN_RETAIL_TY         := RDD_Cols('INTERCOMPANY_IN_RETAIL_TY');
   RDD$RECLASS_IN_RETAIL_LY              := RDD_Cols('RECLASS_IN_RETAIL_LY');
   RDD$RECLASS_IN_RETAIL_TY              := RDD_Cols('RECLASS_IN_RETAIL_TY');
   RDD$FRN_RETURNS_RETAIL_LY             := RDD_Cols('FRN_RETURNS_RETAIL_LY');
   RDD$FRN_RETURNS_RETAIL_TY             := RDD_Cols('FRN_RETURNS_RETAIL_TY');
   RDD$MARKUP_RETAIL_LY                  := RDD_Cols('MARKUP_RETAIL_LY');
   RDD$MARKUP_RETAIL_TY                  := RDD_Cols('MARKUP_RETAIL_TY');
   RDD$FRN_MARKUP_RETAIL_LY              := RDD_Cols('FRN_MARKUP_RETAIL_LY');
   RDD$FRN_MARKUP_RETAIL_TY              := RDD_Cols('FRN_MARKUP_RETAIL_TY');
   RDD$INTERCOMPANY_MARKUP_LY            := RDD_Cols('INTERCOMPANY_MARKUP_LY');
   RDD$INTERCOMPANY_MARKUP_TY            := RDD_Cols('INTERCOMPANY_MARKUP_TY');
   RDD$MARKUP_CAN_RETAIL_LY              := RDD_Cols('MARKUP_CAN_RETAIL_LY');
   RDD$MARKUP_CAN_RETAIL_TY              := RDD_Cols('MARKUP_CAN_RETAIL_TY');
   RDD$RTV_RETAIL_LY                     := RDD_Cols('RTV_RETAIL_LY');
   RDD$RTV_RETAIL_TY                     := RDD_Cols('RTV_RETAIL_TY');
   RDD$TSF_OUT_RETAIL_LY                 := RDD_Cols('TSF_OUT_RETAIL_LY');
   RDD$TSF_OUT_RETAIL_TY                 := RDD_Cols('TSF_OUT_RETAIL_TY');
   RDD$TSF_OUT_BOOK_RETAIL_LY            := RDD_Cols('TSF_OUT_BOOK_RETAIL_LY');
   RDD$TSF_OUT_BOOK_RETAIL_TY            := RDD_Cols('TSF_OUT_BOOK_RETAIL_TY');
   RDD$INTERCOMPANY_OUT_RETAIL_LY        := RDD_Cols('INTERCOMPANY_OUT_RETAIL_LY');
   RDD$INTERCOMPANY_OUT_RETAIL_TY        := RDD_Cols('INTERCOMPANY_OUT_RETAIL_TY');
   RDD$RECLASS_OUT_RETAIL_LY             := RDD_Cols('RECLASS_OUT_RETAIL_LY');
   RDD$RECLASS_OUT_RETAIL_TY             := RDD_Cols('RECLASS_OUT_RETAIL_TY');
   RDD$NET_SALES_RETAIL_LY               := RDD_Cols('NET_SALES_RETAIL_LY');
   RDD$NET_SALES_RETAIL_TY               := RDD_Cols('NET_SALES_RETAIL_TY');
   RDD$RETURNS_RETAIL_LY                 := RDD_Cols('RETURNS_RETAIL_LY');
   RDD$RETURNS_RETAIL_TY                 := RDD_Cols('RETURNS_RETAIL_TY');
   RDD$WEIGHT_VARIANCE_RETAIL_LY         := RDD_Cols('WEIGHT_VARIANCE_RETAIL_LY');
   RDD$WEIGHT_VARIANCE_RETAIL_TY         := RDD_Cols('WEIGHT_VARIANCE_RETAIL_TY');
   RDD$EMPL_DISC_RETAIL_LY               := RDD_Cols('EMPL_DISC_RETAIL_LY');
   RDD$EMPL_DISC_RETAIL_TY               := RDD_Cols('EMPL_DISC_RETAIL_TY');
   RDD$FRANCHISE_SALES_RETAIL_LY         := RDD_Cols('FRANCHISE_SALES_RETAIL_LY');
   RDD$FRANCHISE_SALES_RETAIL_TY         := RDD_Cols('FRANCHISE_SALES_RETAIL_TY');
   RDD$FREIGHT_CLAIM_RETAIL_LY           := RDD_Cols('FREIGHT_CLAIM_RETAIL_LY');
   RDD$FREIGHT_CLAIM_RETAIL_TY           := RDD_Cols('FREIGHT_CLAIM_RETAIL_TY');
   RDD$STOCK_ADJ_COGS_RETAIL_LY          := RDD_Cols('STOCK_ADJ_COGS_RETAIL_LY');
   RDD$STOCK_ADJ_COGS_RETAIL_TY          := RDD_Cols('STOCK_ADJ_COGS_RETAIL_TY');
   RDD$STOCK_ADJ_RETAIL_LY               := RDD_Cols('STOCK_ADJ_RETAIL_LY');
   RDD$STOCK_ADJ_RETAIL_TY               := RDD_Cols('STOCK_ADJ_RETAIL_TY');
   RDD$PERM_MARKDOWN_RETAIL_LY           := RDD_Cols('PERM_MARKDOWN_RETAIL_LY');
   RDD$PERM_MARKDOWN_RETAIL_TY           := RDD_Cols('PERM_MARKDOWN_RETAIL_TY');
   RDD$PROM_MARKDOWN_RETAIL_LY           := RDD_Cols('PROM_MARKDOWN_RETAIL_LY');
   RDD$PROM_MARKDOWN_RETAIL_TY           := RDD_Cols('PROM_MARKDOWN_RETAIL_TY');
   RDD$CLEAR_MARKDOWN_RETAIL_LY          := RDD_Cols('CLEAR_MARKDOWN_RETAIL_LY');
   RDD$CLEAR_MARKDOWN_RETAIL_TY          := RDD_Cols('CLEAR_MARKDOWN_RETAIL_TY');
   RDD$INTERCOMPANY_MARKDOWN_LY          := RDD_Cols('INTERCOMPANY_MARKDOWN_LY');
   RDD$INTERCOMPANY_MARKDOWN_TY          := RDD_Cols('INTERCOMPANY_MARKDOWN_TY');
   RDD$FRN_MARKDOWN_RETAIL_LY            := RDD_Cols('FRN_MARKDOWN_RETAIL_LY');
   RDD$FRN_MARKDOWN_RETAIL_TY            := RDD_Cols('FRN_MARKDOWN_RETAIL_TY');
   RDD$MARKDOWN_CAN_RETAIL_LY            := RDD_Cols('MARKDOWN_CAN_RETAIL_LY');
   RDD$MARKDOWN_CAN_RETAIL_TY            := RDD_Cols('MARKDOWN_CAN_RETAIL_TY');
   RDD$VAT_IN_LY                         := RDD_Cols('VAT_IN_LY');
   RDD$VAT_IN_TY                         := RDD_Cols('VAT_IN_TY');
   RDD$VAT_OUT_LY                        := RDD_Cols('VAT_OUT_LY');
   RDD$VAT_OUT_TY                        := RDD_Cols('VAT_OUT_TY');
   RDD$UP_CHRG_AMT_PROFIT_LY             := RDD_Cols('UP_CHRG_AMT_PROFIT_LY');
   RDD$UP_CHRG_AMT_PROFIT_TY             := RDD_Cols('UP_CHRG_AMT_PROFIT_TY');
   RDD$UP_CHRG_AMT_EXP_LY                := RDD_Cols('UP_CHRG_AMT_EXP_LY');
   RDD$UP_CHRG_AMT_EXP_TY                := RDD_Cols('UP_CHRG_AMT_EXP_TY');
   RDD$WO_ACTIVITY_UPD_INV_LY            := RDD_Cols('WO_ACTIVITY_UPD_INV_LY');
   RDD$WO_ACTIVITY_UPD_INV_TY            := RDD_Cols('WO_ACTIVITY_UPD_INV_TY');
   RDD$WO_ACTIVITY_POST_FIN_LY           := RDD_Cols('WO_ACTIVITY_POST_FIN_LY');
   RDD$WO_ACTIVITY_POST_FIN_TY           := RDD_Cols('WO_ACTIVITY_POST_FIN_TY');
   RDD$FREIGHT_COST_LY                   := RDD_Cols('FREIGHT_COST_LY');
   RDD$FREIGHT_COST_TY                   := RDD_Cols('FREIGHT_COST_TY');
   RDD$WORKROOM_AMT_LY                   := RDD_Cols('WORKROOM_AMT_LY');
   RDD$WORKROOM_AMT_TY                   := RDD_Cols('WORKROOM_AMT_TY');
   RDD$CASH_DISC_AMT_LY                  := RDD_Cols('CASH_DISC_AMT_LY');
   RDD$CASH_DISC_AMT_TY                  := RDD_Cols('CASH_DISC_AMT_TY');
   RDD$COST_VARIANCE_AMT_LY              := RDD_Cols('COST_VARIANCE_AMT_LY');
   RDD$COST_VARIANCE_AMT_TY              := RDD_Cols('COST_VARIANCE_AMT_TY');
   RDD$RETAIL_COST_VARIANCE_LY           := RDD_Cols('RETAIL_COST_VARIANCE_LY');
   RDD$RETAIL_COST_VARIANCE_TY           := RDD_Cols('RETAIL_COST_VARIANCE_TY');
   RDD$REC_COST_ADJ_VARIANCE_LY          := RDD_Cols('REC_COST_ADJ_VARIANCE_LY');
   RDD$REC_COST_ADJ_VARIANCE_TY          := RDD_Cols('REC_COST_ADJ_VARIANCE_TY');
   RDD$ADD_COST_LY                       := RDD_Cols('ADD_COST_LY');
   RDD$ADD_COST_TY                       := RDD_Cols('ADD_COST_TY');
   RDD$ADD_RETAIL_LY                     := RDD_Cols('ADD_RETAIL_LY');
   RDD$ADD_RETAIL_TY                     := RDD_Cols('ADD_RETAIL_TY');
   RDD$DEAL_INCOME_PURCH_LY              := RDD_Cols('DEAL_INCOME_PURCH_LY');
   RDD$DEAL_INCOME_PURCH_TY              := RDD_Cols('DEAL_INCOME_PURCH_TY');
   RDD$DEAL_INCOME_SALES_LY              := RDD_Cols('DEAL_INCOME_SALES_LY');
   RDD$DEAL_INCOME_SALES_TY              := RDD_Cols('DEAL_INCOME_SALES_TY');
   RDD$RESTOCKING_FEE_LY                 := RDD_Cols('RESTOCKING_FEE_LY');
   RDD$RESTOCKING_FEE_TY                 := RDD_Cols('RESTOCKING_FEE_TY');
   RDD$FRN_RESTOCKING_FEE_LY             := RDD_Cols('FRN_RESTOCKING_FEE_LY');
   RDD$FRN_RESTOCKING_FEE_TY             := RDD_Cols('FRN_RESTOCKING_FEE_TY');
   RDD$MARGIN_COST_VARIANCE_LY           := RDD_Cols('MARGIN_COST_VARIANCE_LY');
   RDD$MARGIN_COST_VARIANCE_TY           := RDD_Cols('MARGIN_COST_VARIANCE_TY');
   RDD$NET_MARKDOWN_LY                   := RDD_Cols('NET_MARKDOWN_LY');
   RDD$NET_MARKDOWN_TY                   := RDD_Cols('NET_MARKDOWN_TY');
   RDD$NET_MARKUP_LY                     := RDD_Cols('NET_MARKUP_LY');
   RDD$NET_MARKUP_TY                     := RDD_Cols('NET_MARKUP_TY');
   RDD$NET_RECLASS_RETAIL_LY             := RDD_Cols('NET_RECLASS_RETAIL_LY');
   RDD$NET_RECLASS_RETAIL_TY             := RDD_Cols('NET_RECLASS_RETAIL_TY');
   RDD$NET_SALES_RETAIL_EX_VAT_LY        := RDD_Cols('NET_SALES_RETAIL_EX_VAT_LY');
   RDD$NET_SALES_RETAIL_EX_VAT_TY        := RDD_Cols('NET_SALES_RETAIL_EX_VAT_TY');
   RDD$NET_SALE_NONINV_R_EXVAT_LY        := RDD_Cols('NET_SALE_NONINV_R_EXVAT_LY');
   RDD$NET_SALE_NONINV_R_EXVAT_TY        := RDD_Cols('NET_SALE_NONINV_R_EXVAT_TY');
   RDD$NET_SALES_NONINV_RETAIL_LY        := RDD_Cols('NET_SALES_NONINV_RETAIL_LY');
   RDD$NET_SALES_NONINV_RETAIL_TY        := RDD_Cols('NET_SALES_NONINV_RETAIL_TY');
   RDD$NET_TSF_RETAIL_LY                 := RDD_Cols('NET_TSF_RETAIL_LY');
   RDD$NET_TSF_RETAIL_TY                 := RDD_Cols('NET_TSF_RETAIL_TY');
   RDD$RECOVERABLE_TAX_LY                := RDD_Cols('RECOVERABLE_TAX_LY');
   RDD$RECOVERABLE_TAX_TY                := RDD_Cols('RECOVERABLE_TAX_TY');
   RDD$SALES_UNITS_LY                    := RDD_Cols('SALES_UNITS_LY');
   RDD$SALES_UNITS_TY                    := RDD_Cols('SALES_UNITS_TY');
   -- Retail Daily Data Columns End
   -- Cost-Retail Month Data Columns Begin
   CRM_Cols                              := s9t_pkg.get_col_names(I_file_id,CRM_sheet);
   CRM$DEPT                              := CRM_Cols('DEPT');
   CRM$DEPT_NAME                         := CRM_Cols('DEPT_NAME');
   CRM$CLASS                             := CRM_Cols('CLASS');
   CRM$CLASS_NAME                        := CRM_Cols('CLASS_NAME');
   CRM$SUBCLASS                          := CRM_Cols('SUBCLASS');
   CRM$SUBCLASS_NAME                     := CRM_Cols('SUBCLASS_NAME');
   CRM$LOC_TYPE                          := CRM_Cols('LOC_TYPE');
   CRM$LOCATION                          := CRM_Cols('LOCATION');
   CRM$LOC_NAME                          := CRM_Cols('LOC_NAME');
   CRM$CURRENCY                          := CRM_Cols('CURRENCY');
   CRM$HALF_NO                           := CRM_Cols('HALF_NO');
   CRM$MONTH_NO                          := CRM_Cols('MONTH_NO');
   CRM$OPN_STK_COST_LY                   := CRM_Cols('OPN_STK_COST_LY');
   CRM$OPN_STK_COST_TY                   := CRM_Cols('OPN_STK_COST_TY');
   CRM$OPN_STK_RETAIL_LY                 := CRM_Cols('OPN_STK_RETAIL_LY');
   CRM$OPN_STK_RETAIL_TY                 := CRM_Cols('OPN_STK_RETAIL_TY');
   CRM$PURCH_COST_LY                     := CRM_Cols('PURCH_COST_LY');
   CRM$PURCH_COST_TY                     := CRM_Cols('PURCH_COST_TY');
   CRM$PURCH_RETAIL_LY                   := CRM_Cols('PURCH_RETAIL_LY');
   CRM$PURCH_RETAIL_TY                   := CRM_Cols('PURCH_RETAIL_TY');
   CRM$TSF_IN_COST_LY                    := CRM_Cols('TSF_IN_COST_LY');
   CRM$TSF_IN_COST_TY                    := CRM_Cols('TSF_IN_COST_TY');
   CRM$TSF_IN_RETAIL_LY                  := CRM_Cols('TSF_IN_RETAIL_LY');
   CRM$TSF_IN_RETAIL_TY                  := CRM_Cols('TSF_IN_RETAIL_TY');
   CRM$TSF_IN_BOOK_COST_LY               := CRM_Cols('TSF_IN_BOOK_COST_LY');
   CRM$TSF_IN_BOOK_COST_TY               := CRM_Cols('TSF_IN_BOOK_COST_TY');
   CRM$TSF_IN_BOOK_RETAIL_LY             := CRM_Cols('TSF_IN_BOOK_RETAIL_LY');
   CRM$TSF_IN_BOOK_RETAIL_TY             := CRM_Cols('TSF_IN_BOOK_RETAIL_TY');
   CRM$INTERCOMPANY_IN_COST_LY           := CRM_Cols('INTERCOMPANY_IN_COST_LY');
   CRM$INTERCOMPANY_IN_COST_TY           := CRM_Cols('INTERCOMPANY_IN_COST_TY');
   CRM$INTERCOMPANY_IN_RETAIL_LY         := CRM_Cols('INTERCOMPANY_IN_RETAIL_LY');
   CRM$INTERCOMPANY_IN_RETAIL_TY         := CRM_Cols('INTERCOMPANY_IN_RETAIL_TY');
   CRM$RECLASS_IN_COST_LY                := CRM_Cols('RECLASS_IN_COST_LY');
   CRM$RECLASS_IN_COST_TY                := CRM_Cols('RECLASS_IN_COST_TY');
   CRM$RECLASS_IN_RETAIL_LY              := CRM_Cols('RECLASS_IN_RETAIL_LY');
   CRM$RECLASS_IN_RETAIL_TY              := CRM_Cols('RECLASS_IN_RETAIL_TY');
   CRM$FRANCHISE_RETURNS_COST_LY         := CRM_Cols('FRANCHISE_RETURNS_COST_LY');
   CRM$FRANCHISE_RETURNS_COST_TY         := CRM_Cols('FRANCHISE_RETURNS_COST_TY');
   CRM$FRN_RETURNS_RETAIL_LY             := CRM_Cols('FRN_RETURNS_RETAIL_LY');
   CRM$FRN_RETURNS_RETAIL_TY             := CRM_Cols('FRN_RETURNS_RETAIL_TY');
   CRM$UP_CHRG_AMT_PROFIT_LY             := CRM_Cols('UP_CHRG_AMT_PROFIT_LY');
   CRM$UP_CHRG_AMT_PROFIT_TY             := CRM_Cols('UP_CHRG_AMT_PROFIT_TY');
   CRM$UP_CHRG_AMT_EXP_LY                := CRM_Cols('UP_CHRG_AMT_EXP_LY');
   CRM$UP_CHRG_AMT_EXP_TY                := CRM_Cols('UP_CHRG_AMT_EXP_TY');
   CRM$WO_ACTIVITY_UPD_INV_LY            := CRM_Cols('WO_ACTIVITY_UPD_INV_LY');
   CRM$WO_ACTIVITY_UPD_INV_TY            := CRM_Cols('WO_ACTIVITY_UPD_INV_TY');
   CRM$WO_ACTIVITY_POST_FIN_LY           := CRM_Cols('WO_ACTIVITY_POST_FIN_LY');
   CRM$WO_ACTIVITY_POST_FIN_TY           := CRM_Cols('WO_ACTIVITY_POST_FIN_TY');
   CRM$RECOVERABLE_TAX_LY                := CRM_Cols('RECOVERABLE_TAX_LY');
   CRM$RECOVERABLE_TAX_TY                := CRM_Cols('RECOVERABLE_TAX_TY');
   CRM$MARKUP_RETAIL_LY                  := CRM_Cols('MARKUP_RETAIL_LY');
   CRM$MARKUP_RETAIL_TY                  := CRM_Cols('MARKUP_RETAIL_TY');
   CRM$FRN_MARKUP_RETAIL_LY              := CRM_Cols('FRN_MARKUP_RETAIL_LY');
   CRM$FRN_MARKUP_RETAIL_TY              := CRM_Cols('FRN_MARKUP_RETAIL_TY');
   CRM$INTERCOMPANY_MARKUP_LY            := CRM_Cols('INTERCOMPANY_MARKUP_LY');
   CRM$INTERCOMPANY_MARKUP_TY            := CRM_Cols('INTERCOMPANY_MARKUP_TY');
   CRM$MARKUP_CAN_RETAIL_LY              := CRM_Cols('MARKUP_CAN_RETAIL_LY');
   CRM$MARKUP_CAN_RETAIL_TY              := CRM_Cols('MARKUP_CAN_RETAIL_TY');
   CRM$RTV_COST_LY                       := CRM_Cols('RTV_COST_LY');
   CRM$RTV_COST_TY                       := CRM_Cols('RTV_COST_TY');
   CRM$RTV_RETAIL_LY                     := CRM_Cols('RTV_RETAIL_LY');
   CRM$RTV_RETAIL_TY                     := CRM_Cols('RTV_RETAIL_TY');
   CRM$TSF_OUT_COST_LY                   := CRM_Cols('TSF_OUT_COST_LY');
   CRM$TSF_OUT_COST_TY                   := CRM_Cols('TSF_OUT_COST_TY');
   CRM$TSF_OUT_RETAIL_LY                 := CRM_Cols('TSF_OUT_RETAIL_LY');
   CRM$TSF_OUT_RETAIL_TY                 := CRM_Cols('TSF_OUT_RETAIL_TY');
   CRM$TSF_OUT_BOOK_COST_LY              := CRM_Cols('TSF_OUT_BOOK_COST_LY');
   CRM$TSF_OUT_BOOK_COST_TY              := CRM_Cols('TSF_OUT_BOOK_COST_TY');
   CRM$TSF_OUT_BOOK_RETAIL_LY            := CRM_Cols('TSF_OUT_BOOK_RETAIL_LY');
   CRM$TSF_OUT_BOOK_RETAIL_TY            := CRM_Cols('TSF_OUT_BOOK_RETAIL_TY');
   CRM$INTERCOMPANY_OUT_COST_LY          := CRM_Cols('INTERCOMPANY_OUT_COST_LY');
   CRM$INTERCOMPANY_OUT_COST_TY          := CRM_Cols('INTERCOMPANY_OUT_COST_TY');
   CRM$INTERCOMPANY_OUT_RETAIL_LY        := CRM_Cols('INTERCOMPANY_OUT_RETAIL_LY');
   CRM$INTERCOMPANY_OUT_RETAIL_TY        := CRM_Cols('INTERCOMPANY_OUT_RETAIL_TY');
   CRM$RECLASS_OUT_COST_LY               := CRM_Cols('RECLASS_OUT_COST_LY');
   CRM$RECLASS_OUT_COST_TY               := CRM_Cols('RECLASS_OUT_COST_TY');
   CRM$RECLASS_OUT_RETAIL_LY             := CRM_Cols('RECLASS_OUT_RETAIL_LY');
   CRM$RECLASS_OUT_RETAIL_TY             := CRM_Cols('RECLASS_OUT_RETAIL_TY');
   CRM$NET_SALES_COST_LY                 := CRM_Cols('NET_SALES_COST_LY');
   CRM$NET_SALES_COST_TY                 := CRM_Cols('NET_SALES_COST_TY');
   CRM$NET_SALES_RETAIL_LY               := CRM_Cols('NET_SALES_RETAIL_LY');
   CRM$NET_SALES_RETAIL_TY               := CRM_Cols('NET_SALES_RETAIL_TY');
   CRM$RETURNS_COST_LY                   := CRM_Cols('RETURNS_COST_LY');
   CRM$RETURNS_COST_TY                   := CRM_Cols('RETURNS_COST_TY');
   CRM$RETURNS_RETAIL_LY                 := CRM_Cols('RETURNS_RETAIL_LY');
   CRM$RETURNS_RETAIL_TY                 := CRM_Cols('RETURNS_RETAIL_TY');
   CRM$WEIGHT_VARIANCE_RETAIL_LY         := CRM_Cols('WEIGHT_VARIANCE_RETAIL_LY');
   CRM$WEIGHT_VARIANCE_RETAIL_TY         := CRM_Cols('WEIGHT_VARIANCE_RETAIL_TY');
   CRM$EMPL_DISC_RETAIL_LY               := CRM_Cols('EMPL_DISC_RETAIL_LY');
   CRM$EMPL_DISC_RETAIL_TY               := CRM_Cols('EMPL_DISC_RETAIL_TY');
   CRM$FRANCHISE_SALES_COST_LY           := CRM_Cols('FRANCHISE_SALES_COST_LY');
   CRM$FRANCHISE_SALES_COST_TY           := CRM_Cols('FRANCHISE_SALES_COST_TY');
   CRM$FRANCHISE_SALES_RETAIL_LY         := CRM_Cols('FRANCHISE_SALES_RETAIL_LY');
   CRM$FRANCHISE_SALES_RETAIL_TY         := CRM_Cols('FRANCHISE_SALES_RETAIL_TY');
   CRM$FREIGHT_CLAIM_COST_LY             := CRM_Cols('FREIGHT_CLAIM_COST_LY');
   CRM$FREIGHT_CLAIM_COST_TY             := CRM_Cols('FREIGHT_CLAIM_COST_TY');
   CRM$FREIGHT_CLAIM_RETAIL_LY           := CRM_Cols('FREIGHT_CLAIM_RETAIL_LY');
   CRM$FREIGHT_CLAIM_RETAIL_TY           := CRM_Cols('FREIGHT_CLAIM_RETAIL_TY');
   CRM$STOCK_ADJ_COGS_COST_LY            := CRM_Cols('STOCK_ADJ_COGS_COST_LY');
   CRM$STOCK_ADJ_COGS_COST_TY            := CRM_Cols('STOCK_ADJ_COGS_COST_TY');
   CRM$STOCK_ADJ_COGS_RETAIL_LY          := CRM_Cols('STOCK_ADJ_COGS_RETAIL_LY');
   CRM$STOCK_ADJ_COGS_RETAIL_TY          := CRM_Cols('STOCK_ADJ_COGS_RETAIL_TY');
   CRM$STOCK_ADJ_COST_LY                 := CRM_Cols('STOCK_ADJ_COST_LY');
   CRM$STOCK_ADJ_COST_TY                 := CRM_Cols('STOCK_ADJ_COST_TY');
   CRM$STOCK_ADJ_RETAIL_LY               := CRM_Cols('STOCK_ADJ_RETAIL_LY');
   CRM$STOCK_ADJ_RETAIL_TY               := CRM_Cols('STOCK_ADJ_RETAIL_TY');
   CRM$SHRINKAGE_COST_LY                 := CRM_Cols('SHRINKAGE_COST_LY');
   CRM$SHRINKAGE_COST_TY                 := CRM_Cols('SHRINKAGE_COST_TY');
   CRM$SHRINKAGE_RETAIL_LY               := CRM_Cols('SHRINKAGE_RETAIL_LY');
   CRM$SHRINKAGE_RETAIL_TY               := CRM_Cols('SHRINKAGE_RETAIL_TY');
   CRM$COST_VARIANCE_AMT_LY              := CRM_Cols('COST_VARIANCE_AMT_LY');
   CRM$COST_VARIANCE_AMT_TY              := CRM_Cols('COST_VARIANCE_AMT_TY');
   CRM$MARGIN_COST_VARIANCE_LY           := CRM_Cols('MARGIN_COST_VARIANCE_LY');
   CRM$MARGIN_COST_VARIANCE_TY           := CRM_Cols('MARGIN_COST_VARIANCE_TY');
   CRM$PERM_MARKDOWN_RETAIL_LY           := CRM_Cols('PERM_MARKDOWN_RETAIL_LY');
   CRM$PERM_MARKDOWN_RETAIL_TY           := CRM_Cols('PERM_MARKDOWN_RETAIL_TY');
   CRM$PROM_MARKDOWN_RETAIL_LY           := CRM_Cols('PROM_MARKDOWN_RETAIL_LY');
   CRM$PROM_MARKDOWN_RETAIL_TY           := CRM_Cols('PROM_MARKDOWN_RETAIL_TY');
   CRM$CLEAR_MARKDOWN_RETAIL_LY          := CRM_Cols('CLEAR_MARKDOWN_RETAIL_LY');
   CRM$CLEAR_MARKDOWN_RETAIL_TY          := CRM_Cols('CLEAR_MARKDOWN_RETAIL_TY');
   CRM$INTERCOMPANY_MARKDOWN_LY          := CRM_Cols('INTERCOMPANY_MARKDOWN_LY');
   CRM$INTERCOMPANY_MARKDOWN_TY          := CRM_Cols('INTERCOMPANY_MARKDOWN_TY');
   CRM$FRN_MARKDOWN_RETAIL_LY            := CRM_Cols('FRN_MARKDOWN_RETAIL_LY');
   CRM$FRN_MARKDOWN_RETAIL_TY            := CRM_Cols('FRN_MARKDOWN_RETAIL_TY');
   CRM$MARKDOWN_CAN_RETAIL_LY            := CRM_Cols('MARKDOWN_CAN_RETAIL_LY');
   CRM$MARKDOWN_CAN_RETAIL_TY            := CRM_Cols('MARKDOWN_CAN_RETAIL_TY');
   CRM$CLS_STK_COST_LY                   := CRM_Cols('CLS_STK_COST_LY');
   CRM$CLS_STK_COST_TY                   := CRM_Cols('CLS_STK_COST_TY');
   CRM$CLS_STK_RETAIL_LY                 := CRM_Cols('CLS_STK_RETAIL_LY');
   CRM$CLS_STK_RETAIL_TY                 := CRM_Cols('CLS_STK_RETAIL_TY');
   CRM$ADD_COST_LY                       := CRM_Cols('ADD_COST_LY');
   CRM$ADD_COST_TY                       := CRM_Cols('ADD_COST_TY');
   CRM$ADD_RETAIL_LY                     := CRM_Cols('ADD_RETAIL_LY');
   CRM$ADD_RETAIL_TY                     := CRM_Cols('ADD_RETAIL_TY');
   CRM$SHRINK_COST_PCT_LY                := CRM_Cols('SHRINK_COST_PCT_LY');
   CRM$SHRINK_COST_PCT_TY                := CRM_Cols('SHRINK_COST_PCT_TY');
   CRM$HTD_GAFS_RETAIL_LY                := CRM_Cols('HTD_GAFS_RETAIL_LY');
   CRM$HTD_GAFS_RETAIL_TY                := CRM_Cols('HTD_GAFS_RETAIL_TY');
   CRM$HTD_GAFS_COST_LY                  := CRM_Cols('HTD_GAFS_COST_LY');
   CRM$HTD_GAFS_COST_TY                  := CRM_Cols('HTD_GAFS_COST_TY');
   CRM$RED_RETAIL_LY                     := CRM_Cols('RED_RETAIL_LY');
   CRM$RED_RETAIL_TY                     := CRM_Cols('RED_RETAIL_TY');
   CRM$DEAL_INCOME_SALES_LY              := CRM_Cols('DEAL_INCOME_SALES_LY');
   CRM$DEAL_INCOME_SALES_TY              := CRM_Cols('DEAL_INCOME_SALES_TY');
   CRM$DEAL_INCOME_PURCH_LY              := CRM_Cols('DEAL_INCOME_PURCH_LY');
   CRM$DEAL_INCOME_PURCH_TY              := CRM_Cols('DEAL_INCOME_PURCH_TY');
   CRM$CUM_MARKON_PCT_LY                 := CRM_Cols('CUM_MARKON_PCT_LY');
   CRM$CUM_MARKON_PCT_TY                 := CRM_Cols('CUM_MARKON_PCT_TY');
   CRM$GROSS_MARGIN_AMT_LY               := CRM_Cols('GROSS_MARGIN_AMT_LY');
   CRM$GROSS_MARGIN_AMT_TY               := CRM_Cols('GROSS_MARGIN_AMT_TY');
   CRM$VAT_IN_LY                         := CRM_Cols('VAT_IN_LY');
   CRM$VAT_IN_TY                         := CRM_Cols('VAT_IN_TY');
   CRM$VAT_OUT_LY                        := CRM_Cols('VAT_OUT_LY');
   CRM$VAT_OUT_TY                        := CRM_Cols('VAT_OUT_TY');
   CRM$RESTOCKING_FEE_LY                 := CRM_Cols('RESTOCKING_FEE_LY');
   CRM$RESTOCKING_FEE_TY                 := CRM_Cols('RESTOCKING_FEE_TY');
   CRM$FREIGHT_COST_LY                   := CRM_Cols('FREIGHT_COST_LY');
   CRM$FREIGHT_COST_TY                   := CRM_Cols('FREIGHT_COST_TY');
   CRM$REC_COST_ADJ_VARIANCE_LY          := CRM_Cols('REC_COST_ADJ_VARIANCE_LY');
   CRM$REC_COST_ADJ_VARIANCE_TY          := CRM_Cols('REC_COST_ADJ_VARIANCE_TY');
   CRM$WORKROOM_AMT_LY                   := CRM_Cols('WORKROOM_AMT_LY');
   CRM$WORKROOM_AMT_TY                   := CRM_Cols('WORKROOM_AMT_TY');
   CRM$CASH_DISC_AMT_LY                  := CRM_Cols('CASH_DISC_AMT_LY');
   CRM$CASH_DISC_AMT_TY                  := CRM_Cols('CASH_DISC_AMT_TY');
   CRM$RETAIL_COST_VARIANCE_LY           := CRM_Cols('RETAIL_COST_VARIANCE_LY');
   CRM$RETAIL_COST_VARIANCE_TY           := CRM_Cols('RETAIL_COST_VARIANCE_TY');
   CRM$FRN_RESTOCKING_FEE_LY             := CRM_Cols('FRN_RESTOCKING_FEE_LY');
   CRM$FRN_RESTOCKING_FEE_TY             := CRM_Cols('FRN_RESTOCKING_FEE_TY');
   CRM$INTERCOMPANY_MARGIN_LY            := CRM_Cols('INTERCOMPANY_MARGIN_LY');
   CRM$INTERCOMPANY_MARGIN_TY            := CRM_Cols('INTERCOMPANY_MARGIN_TY');
   CRM$NET_MARKDOWN_LY                   := CRM_Cols('NET_MARKDOWN_LY');
   CRM$NET_MARKDOWN_TY                   := CRM_Cols('NET_MARKDOWN_TY');
   CRM$NET_MARKUP_LY                     := CRM_Cols('NET_MARKUP_LY');
   CRM$NET_MARKUP_TY                     := CRM_Cols('NET_MARKUP_TY');
   CRM$NET_RECLASS_COST_LY               := CRM_Cols('NET_RECLASS_COST_LY');
   CRM$NET_RECLASS_COST_TY               := CRM_Cols('NET_RECLASS_COST_TY');
   CRM$NET_RECLASS_RETAIL_LY             := CRM_Cols('NET_RECLASS_RETAIL_LY');
   CRM$NET_RECLASS_RETAIL_TY             := CRM_Cols('NET_RECLASS_RETAIL_TY');
   CRM$NET_SALES_RETAIL_EX_VAT_LY        := CRM_Cols('NET_SALES_RETAIL_EX_VAT_LY');
   CRM$NET_SALES_RETAIL_EX_VAT_TY        := CRM_Cols('NET_SALES_RETAIL_EX_VAT_TY');
   CRM$NET_SALE_NONINV_R_EXVAT_LY        := CRM_Cols('NET_SALE_NONINV_R_EXVAT_LY');
   CRM$NET_SALE_NONINV_R_EXVAT_TY        := CRM_Cols('NET_SALE_NONINV_R_EXVAT_TY');
   CRM$NET_TSF_COST_LY                   := CRM_Cols('NET_TSF_COST_LY');
   CRM$NET_TSF_COST_TY                   := CRM_Cols('NET_TSF_COST_TY');
   CRM$NET_TSF_RETAIL_LY                 := CRM_Cols('NET_TSF_RETAIL_LY');
   CRM$NET_TSF_RETAIL_TY                 := CRM_Cols('NET_TSF_RETAIL_TY');
   CRM$SHRINK_RETAIL_PCT_LY              := CRM_Cols('SHRINK_RETAIL_PCT_LY');
   CRM$SHRINK_RETAIL_PCT_TY              := CRM_Cols('SHRINK_RETAIL_PCT_TY');
   CRM$SALES_UNITS_LY                    := CRM_Cols('SALES_UNITS_LY');
   CRM$SALES_UNITS_TY                    := CRM_Cols('SALES_UNITS_TY');
   CRM$NET_SALES_NON_INV_COST_LY         := CRM_Cols('NET_SALES_NON_INV_COST_LY');
   CRM$NET_SALES_NON_INV_COST_TY         := CRM_Cols('NET_SALES_NON_INV_COST_TY');
   CRM$NET_SALES_NONINV_RETAIL_LY        := CRM_Cols('NET_SALES_NONINV_RETAIL_LY');
   CRM$NET_SALES_NONINV_RETAIL_TY        := CRM_Cols('NET_SALES_NONINV_RETAIL_TY');
   -- Cost-Retail Month Data Columns End
   -- Cost-Retail Week Data Columns Begin
   CRW_Cols                              := s9t_pkg.get_col_names(I_file_id,CRW_sheet);
   CRW$DEPT                              := CRW_Cols('DEPT');
   CRW$DEPT_NAME                         := CRW_Cols('DEPT_NAME');
   CRW$CLASS                             := CRW_Cols('CLASS');
   CRW$CLASS_NAME                        := CRW_Cols('CLASS_NAME');
   CRW$SUBCLASS                          := CRW_Cols('SUBCLASS');
   CRW$SUBCLASS_NAME                     := CRW_Cols('SUBCLASS_NAME');
   CRW$LOC_TYPE                          := CRW_Cols('LOC_TYPE');
   CRW$LOCATION                          := CRW_Cols('LOCATION');
   CRW$LOC_NAME                          := CRW_Cols('LOC_NAME');
   CRW$CURRENCY                          := CRW_Cols('CURRENCY');
   CRW$EOM_DATE                          := CRW_Cols('EOM_DATE');
   CRW$WEEK_NO                           := CRW_Cols('WEEK_NO');
   CRW$OPN_STK_COST_LY                   := CRW_Cols('OPN_STK_COST_LY');
   CRW$OPN_STK_COST_TY                   := CRW_Cols('OPN_STK_COST_TY');
   CRW$OPN_STK_RETAIL_LY                 := CRW_Cols('OPN_STK_RETAIL_LY');
   CRW$OPN_STK_RETAIL_TY                 := CRW_Cols('OPN_STK_RETAIL_TY');
   CRW$PURCH_COST_LY                     := CRW_Cols('PURCH_COST_LY');
   CRW$PURCH_COST_TY                     := CRW_Cols('PURCH_COST_TY');
   CRW$PURCH_RETAIL_LY                   := CRW_Cols('PURCH_RETAIL_LY');
   CRW$PURCH_RETAIL_TY                   := CRW_Cols('PURCH_RETAIL_TY');
   CRW$TSF_IN_COST_LY                    := CRW_Cols('TSF_IN_COST_LY');
   CRW$TSF_IN_COST_TY                    := CRW_Cols('TSF_IN_COST_TY');
   CRW$TSF_IN_RETAIL_LY                  := CRW_Cols('TSF_IN_RETAIL_LY');
   CRW$TSF_IN_RETAIL_TY                  := CRW_Cols('TSF_IN_RETAIL_TY');
   CRW$TSF_IN_BOOK_COST_LY               := CRW_Cols('TSF_IN_BOOK_COST_LY');
   CRW$TSF_IN_BOOK_COST_TY               := CRW_Cols('TSF_IN_BOOK_COST_TY');
   CRW$TSF_IN_BOOK_RETAIL_LY             := CRW_Cols('TSF_IN_BOOK_RETAIL_LY');
   CRW$TSF_IN_BOOK_RETAIL_TY             := CRW_Cols('TSF_IN_BOOK_RETAIL_TY');
   CRW$INTERCOMPANY_IN_COST_LY           := CRW_Cols('INTERCOMPANY_IN_COST_LY');
   CRW$INTERCOMPANY_IN_COST_TY           := CRW_Cols('INTERCOMPANY_IN_COST_TY');
   CRW$INTERCOMPANY_IN_RETAIL_LY         := CRW_Cols('INTERCOMPANY_IN_RETAIL_LY');
   CRW$INTERCOMPANY_IN_RETAIL_TY         := CRW_Cols('INTERCOMPANY_IN_RETAIL_TY');
   CRW$RECLASS_IN_COST_LY                := CRW_Cols('RECLASS_IN_COST_LY');
   CRW$RECLASS_IN_COST_TY                := CRW_Cols('RECLASS_IN_COST_TY');
   CRW$RECLASS_IN_RETAIL_LY              := CRW_Cols('RECLASS_IN_RETAIL_LY');
   CRW$RECLASS_IN_RETAIL_TY              := CRW_Cols('RECLASS_IN_RETAIL_TY');
   CRW$FRANCHISE_RETURNS_COST_LY         := CRW_Cols('FRANCHISE_RETURNS_COST_LY');
   CRW$FRANCHISE_RETURNS_COST_TY         := CRW_Cols('FRANCHISE_RETURNS_COST_TY');
   CRW$FRN_RETURNS_RETAIL_LY             := CRW_Cols('FRN_RETURNS_RETAIL_LY');
   CRW$FRN_RETURNS_RETAIL_TY             := CRW_Cols('FRN_RETURNS_RETAIL_TY');
   CRW$UP_CHRG_AMT_PROFIT_LY             := CRW_Cols('UP_CHRG_AMT_PROFIT_LY');
   CRW$UP_CHRG_AMT_PROFIT_TY             := CRW_Cols('UP_CHRG_AMT_PROFIT_TY');
   CRW$UP_CHRG_AMT_EXP_LY                := CRW_Cols('UP_CHRG_AMT_EXP_LY');
   CRW$UP_CHRG_AMT_EXP_TY                := CRW_Cols('UP_CHRG_AMT_EXP_TY');
   CRW$WO_ACTIVITY_UPD_INV_LY            := CRW_Cols('WO_ACTIVITY_UPD_INV_LY');
   CRW$WO_ACTIVITY_UPD_INV_TY            := CRW_Cols('WO_ACTIVITY_UPD_INV_TY');
   CRW$WO_ACTIVITY_POST_FIN_LY           := CRW_Cols('WO_ACTIVITY_POST_FIN_LY');
   CRW$WO_ACTIVITY_POST_FIN_TY           := CRW_Cols('WO_ACTIVITY_POST_FIN_TY');
   CRW$RECOVERABLE_TAX_LY                := CRW_Cols('RECOVERABLE_TAX_LY');
   CRW$RECOVERABLE_TAX_TY                := CRW_Cols('RECOVERABLE_TAX_TY');
   CRW$MARKUP_RETAIL_LY                  := CRW_Cols('MARKUP_RETAIL_LY');
   CRW$MARKUP_RETAIL_TY                  := CRW_Cols('MARKUP_RETAIL_TY');
   CRW$FRN_MARKUP_RETAIL_LY              := CRW_Cols('FRN_MARKUP_RETAIL_LY');
   CRW$FRN_MARKUP_RETAIL_TY              := CRW_Cols('FRN_MARKUP_RETAIL_TY');
   CRW$INTERCOMPANY_MARKUP_LY            := CRW_Cols('INTERCOMPANY_MARKUP_LY');
   CRW$INTERCOMPANY_MARKUP_TY            := CRW_Cols('INTERCOMPANY_MARKUP_TY');
   CRW$MARKUP_CAN_RETAIL_LY              := CRW_Cols('MARKUP_CAN_RETAIL_LY');
   CRW$MARKUP_CAN_RETAIL_TY              := CRW_Cols('MARKUP_CAN_RETAIL_TY');
   CRW$RTV_COST_LY                       := CRW_Cols('RTV_COST_LY');
   CRW$RTV_COST_TY                       := CRW_Cols('RTV_COST_TY');
   CRW$RTV_RETAIL_LY                     := CRW_Cols('RTV_RETAIL_LY');
   CRW$RTV_RETAIL_TY                     := CRW_Cols('RTV_RETAIL_TY');
   CRW$TSF_OUT_COST_LY                   := CRW_Cols('TSF_OUT_COST_LY');
   CRW$TSF_OUT_COST_TY                   := CRW_Cols('TSF_OUT_COST_TY');
   CRW$TSF_OUT_RETAIL_LY                 := CRW_Cols('TSF_OUT_RETAIL_LY');
   CRW$TSF_OUT_RETAIL_TY                 := CRW_Cols('TSF_OUT_RETAIL_TY');
   CRW$TSF_OUT_BOOK_COST_LY              := CRW_Cols('TSF_OUT_BOOK_COST_LY');
   CRW$TSF_OUT_BOOK_COST_TY              := CRW_Cols('TSF_OUT_BOOK_COST_TY');
   CRW$TSF_OUT_BOOK_RETAIL_LY            := CRW_Cols('TSF_OUT_BOOK_RETAIL_LY');
   CRW$TSF_OUT_BOOK_RETAIL_TY            := CRW_Cols('TSF_OUT_BOOK_RETAIL_TY');
   CRW$INTERCOMPANY_OUT_COST_LY          := CRW_Cols('INTERCOMPANY_OUT_COST_LY');
   CRW$INTERCOMPANY_OUT_COST_TY          := CRW_Cols('INTERCOMPANY_OUT_COST_TY');
   CRW$INTERCOMPANY_OUT_RETAIL_LY        := CRW_Cols('INTERCOMPANY_OUT_RETAIL_LY');
   CRW$INTERCOMPANY_OUT_RETAIL_TY        := CRW_Cols('INTERCOMPANY_OUT_RETAIL_TY');
   CRW$RECLASS_OUT_COST_LY               := CRW_Cols('RECLASS_OUT_COST_LY');
   CRW$RECLASS_OUT_COST_TY               := CRW_Cols('RECLASS_OUT_COST_TY');
   CRW$RECLASS_OUT_RETAIL_LY             := CRW_Cols('RECLASS_OUT_RETAIL_LY');
   CRW$RECLASS_OUT_RETAIL_TY             := CRW_Cols('RECLASS_OUT_RETAIL_TY');
   CRW$NET_SALES_COST_LY                 := CRW_Cols('NET_SALES_COST_LY');
   CRW$NET_SALES_COST_TY                 := CRW_Cols('NET_SALES_COST_TY');
   CRW$NET_SALES_RETAIL_LY               := CRW_Cols('NET_SALES_RETAIL_LY');
   CRW$NET_SALES_RETAIL_TY               := CRW_Cols('NET_SALES_RETAIL_TY');
   CRW$RETURNS_COST_LY                   := CRW_Cols('RETURNS_COST_LY');
   CRW$RETURNS_COST_TY                   := CRW_Cols('RETURNS_COST_TY');
   CRW$RETURNS_RETAIL_LY                 := CRW_Cols('RETURNS_RETAIL_LY');
   CRW$RETURNS_RETAIL_TY                 := CRW_Cols('RETURNS_RETAIL_TY');
   CRW$WEIGHT_VARIANCE_RETAIL_LY         := CRW_Cols('WEIGHT_VARIANCE_RETAIL_LY');
   CRW$WEIGHT_VARIANCE_RETAIL_TY         := CRW_Cols('WEIGHT_VARIANCE_RETAIL_TY');
   CRW$EMPL_DISC_RETAIL_LY               := CRW_Cols('EMPL_DISC_RETAIL_LY');
   CRW$EMPL_DISC_RETAIL_TY               := CRW_Cols('EMPL_DISC_RETAIL_TY');
   CRW$FRANCHISE_SALES_COST_LY           := CRW_Cols('FRANCHISE_SALES_COST_LY');
   CRW$FRANCHISE_SALES_COST_TY           := CRW_Cols('FRANCHISE_SALES_COST_TY');
   CRW$FRANCHISE_SALES_RETAIL_LY         := CRW_Cols('FRANCHISE_SALES_RETAIL_LY');
   CRW$FRANCHISE_SALES_RETAIL_TY         := CRW_Cols('FRANCHISE_SALES_RETAIL_TY');
   CRW$FREIGHT_CLAIM_COST_LY             := CRW_Cols('FREIGHT_CLAIM_COST_LY');
   CRW$FREIGHT_CLAIM_COST_TY             := CRW_Cols('FREIGHT_CLAIM_COST_TY');
   CRW$FREIGHT_CLAIM_RETAIL_LY           := CRW_Cols('FREIGHT_CLAIM_RETAIL_LY');
   CRW$FREIGHT_CLAIM_RETAIL_TY           := CRW_Cols('FREIGHT_CLAIM_RETAIL_TY');
   CRW$STOCK_ADJ_COGS_COST_LY            := CRW_Cols('STOCK_ADJ_COGS_COST_LY');
   CRW$STOCK_ADJ_COGS_COST_TY            := CRW_Cols('STOCK_ADJ_COGS_COST_TY');
   CRW$STOCK_ADJ_COGS_RETAIL_LY          := CRW_Cols('STOCK_ADJ_COGS_RETAIL_LY');
   CRW$STOCK_ADJ_COGS_RETAIL_TY          := CRW_Cols('STOCK_ADJ_COGS_RETAIL_TY');
   CRW$STOCK_ADJ_COST_LY                 := CRW_Cols('STOCK_ADJ_COST_LY');
   CRW$STOCK_ADJ_COST_TY                 := CRW_Cols('STOCK_ADJ_COST_TY');
   CRW$STOCK_ADJ_RETAIL_LY               := CRW_Cols('STOCK_ADJ_RETAIL_LY');
   CRW$STOCK_ADJ_RETAIL_TY               := CRW_Cols('STOCK_ADJ_RETAIL_TY');
   CRW$SHRINKAGE_COST_LY                 := CRW_Cols('SHRINKAGE_COST_LY');
   CRW$SHRINKAGE_COST_TY                 := CRW_Cols('SHRINKAGE_COST_TY');
   CRW$SHRINKAGE_RETAIL_LY               := CRW_Cols('SHRINKAGE_RETAIL_LY');
   CRW$SHRINKAGE_RETAIL_TY               := CRW_Cols('SHRINKAGE_RETAIL_TY');
   CRW$COST_VARIANCE_AMT_LY              := CRW_Cols('COST_VARIANCE_AMT_LY');
   CRW$COST_VARIANCE_AMT_TY              := CRW_Cols('COST_VARIANCE_AMT_TY');
   CRW$MARGIN_COST_VARIANCE_LY           := CRW_Cols('MARGIN_COST_VARIANCE_LY');
   CRW$MARGIN_COST_VARIANCE_TY           := CRW_Cols('MARGIN_COST_VARIANCE_TY');
   CRW$PERM_MARKDOWN_RETAIL_LY           := CRW_Cols('PERM_MARKDOWN_RETAIL_LY');
   CRW$PERM_MARKDOWN_RETAIL_TY           := CRW_Cols('PERM_MARKDOWN_RETAIL_TY');
   CRW$PROM_MARKDOWN_RETAIL_LY           := CRW_Cols('PROM_MARKDOWN_RETAIL_LY');
   CRW$PROM_MARKDOWN_RETAIL_TY           := CRW_Cols('PROM_MARKDOWN_RETAIL_TY');
   CRW$CLEAR_MARKDOWN_RETAIL_LY          := CRW_Cols('CLEAR_MARKDOWN_RETAIL_LY');
   CRW$CLEAR_MARKDOWN_RETAIL_TY          := CRW_Cols('CLEAR_MARKDOWN_RETAIL_TY');
   CRW$INTERCOMPANY_MARKDOWN_LY          := CRW_Cols('INTERCOMPANY_MARKDOWN_LY');
   CRW$INTERCOMPANY_MARKDOWN_TY          := CRW_Cols('INTERCOMPANY_MARKDOWN_TY');
   CRW$FRN_MARKDOWN_RETAIL_LY            := CRW_Cols('FRN_MARKDOWN_RETAIL_LY');
   CRW$FRN_MARKDOWN_RETAIL_TY            := CRW_Cols('FRN_MARKDOWN_RETAIL_TY');
   CRW$MARKDOWN_CAN_RETAIL_LY            := CRW_Cols('MARKDOWN_CAN_RETAIL_LY');
   CRW$MARKDOWN_CAN_RETAIL_TY            := CRW_Cols('MARKDOWN_CAN_RETAIL_TY');
   CRW$CLS_STK_COST_LY                   := CRW_Cols('CLS_STK_COST_LY');
   CRW$CLS_STK_COST_TY                   := CRW_Cols('CLS_STK_COST_TY');
   CRW$CLS_STK_RETAIL_LY                 := CRW_Cols('CLS_STK_RETAIL_LY');
   CRW$CLS_STK_RETAIL_TY                 := CRW_Cols('CLS_STK_RETAIL_TY');
   CRW$ADD_COST_LY                       := CRW_Cols('ADD_COST_LY');
   CRW$ADD_COST_TY                       := CRW_Cols('ADD_COST_TY');
   CRW$ADD_RETAIL_LY                     := CRW_Cols('ADD_RETAIL_LY');
   CRW$ADD_RETAIL_TY                     := CRW_Cols('ADD_RETAIL_TY');
   CRW$SHRINK_COST_PCT_LY                := CRW_Cols('SHRINK_COST_PCT_LY');
   CRW$SHRINK_COST_PCT_TY                := CRW_Cols('SHRINK_COST_PCT_TY');
   CRW$HTD_GAFS_RETAIL_LY                := CRW_Cols('HTD_GAFS_RETAIL_LY');
   CRW$HTD_GAFS_RETAIL_TY                := CRW_Cols('HTD_GAFS_RETAIL_TY');
   CRW$HTD_GAFS_COST_LY                  := CRW_Cols('HTD_GAFS_COST_LY');
   CRW$HTD_GAFS_COST_TY                  := CRW_Cols('HTD_GAFS_COST_TY');
   CRW$RED_RETAIL_LY                     := CRW_Cols('RED_RETAIL_LY');
   CRW$RED_RETAIL_TY                     := CRW_Cols('RED_RETAIL_TY');
   CRW$DEAL_INCOME_SALES_LY              := CRW_Cols('DEAL_INCOME_SALES_LY');
   CRW$DEAL_INCOME_SALES_TY              := CRW_Cols('DEAL_INCOME_SALES_TY');
   CRW$DEAL_INCOME_PURCH_LY              := CRW_Cols('DEAL_INCOME_PURCH_LY');
   CRW$DEAL_INCOME_PURCH_TY              := CRW_Cols('DEAL_INCOME_PURCH_TY');
   CRW$CUM_MARKON_PCT_LY                 := CRW_Cols('CUM_MARKON_PCT_LY');
   CRW$CUM_MARKON_PCT_TY                 := CRW_Cols('CUM_MARKON_PCT_TY');
   CRW$GROSS_MARGIN_AMT_LY               := CRW_Cols('GROSS_MARGIN_AMT_LY');
   CRW$GROSS_MARGIN_AMT_TY               := CRW_Cols('GROSS_MARGIN_AMT_TY');
   CRW$VAT_IN_LY                         := CRW_Cols('VAT_IN_LY');
   CRW$VAT_IN_TY                         := CRW_Cols('VAT_IN_TY');
   CRW$VAT_OUT_LY                        := CRW_Cols('VAT_OUT_LY');
   CRW$VAT_OUT_TY                        := CRW_Cols('VAT_OUT_TY');
   CRW$RESTOCKING_FEE_LY                 := CRW_Cols('RESTOCKING_FEE_LY');
   CRW$RESTOCKING_FEE_TY                 := CRW_Cols('RESTOCKING_FEE_TY');
   CRW$FREIGHT_COST_LY                   := CRW_Cols('FREIGHT_COST_LY');
   CRW$FREIGHT_COST_TY                   := CRW_Cols('FREIGHT_COST_TY');
   CRW$REC_COST_ADJ_VARIANCE_LY          := CRW_Cols('REC_COST_ADJ_VARIANCE_LY');
   CRW$REC_COST_ADJ_VARIANCE_TY          := CRW_Cols('REC_COST_ADJ_VARIANCE_TY');
   CRW$WORKROOM_AMT_LY                   := CRW_Cols('WORKROOM_AMT_LY');
   CRW$WORKROOM_AMT_TY                   := CRW_Cols('WORKROOM_AMT_TY');
   CRW$CASH_DISC_AMT_LY                  := CRW_Cols('CASH_DISC_AMT_LY');
   CRW$CASH_DISC_AMT_TY                  := CRW_Cols('CASH_DISC_AMT_TY');
   CRW$RETAIL_COST_VARIANCE_LY           := CRW_Cols('RETAIL_COST_VARIANCE_LY');
   CRW$RETAIL_COST_VARIANCE_TY           := CRW_Cols('RETAIL_COST_VARIANCE_TY');
   CRW$FRN_RESTOCKING_FEE_LY             := CRW_Cols('FRN_RESTOCKING_FEE_LY');
   CRW$FRN_RESTOCKING_FEE_TY             := CRW_Cols('FRN_RESTOCKING_FEE_TY');
   CRW$INTERCOMPANY_MARGIN_LY            := CRW_Cols('INTERCOMPANY_MARGIN_LY');
   CRW$INTERCOMPANY_MARGIN_TY            := CRW_Cols('INTERCOMPANY_MARGIN_TY');
   CRW$NET_MARKDOWN_LY                   := CRW_Cols('NET_MARKDOWN_LY');
   CRW$NET_MARKDOWN_TY                   := CRW_Cols('NET_MARKDOWN_TY');
   CRW$NET_MARKUP_LY                     := CRW_Cols('NET_MARKUP_LY');
   CRW$NET_MARKUP_TY                     := CRW_Cols('NET_MARKUP_TY');
   CRW$NET_RECLASS_COST_LY               := CRW_Cols('NET_RECLASS_COST_LY');
   CRW$NET_RECLASS_COST_TY               := CRW_Cols('NET_RECLASS_COST_TY');
   CRW$NET_RECLASS_RETAIL_LY             := CRW_Cols('NET_RECLASS_RETAIL_LY');
   CRW$NET_RECLASS_RETAIL_TY             := CRW_Cols('NET_RECLASS_RETAIL_TY');
   CRW$NET_SALES_RETAIL_EX_VAT_LY        := CRW_Cols('NET_SALES_RETAIL_EX_VAT_LY');
   CRW$NET_SALES_RETAIL_EX_VAT_TY        := CRW_Cols('NET_SALES_RETAIL_EX_VAT_TY');
   CRW$NET_SALE_NONINV_R_EXVAT_LY        := CRW_Cols('NET_SALE_NONINV_R_EXVAT_LY');
   CRW$NET_SALE_NONINV_R_EXVAT_TY        := CRW_Cols('NET_SALE_NONINV_R_EXVAT_TY');
   CRW$NET_TSF_COST_LY                   := CRW_Cols('NET_TSF_COST_LY');
   CRW$NET_TSF_COST_TY                   := CRW_Cols('NET_TSF_COST_TY');
   CRW$NET_TSF_RETAIL_LY                 := CRW_Cols('NET_TSF_RETAIL_LY');
   CRW$NET_TSF_RETAIL_TY                 := CRW_Cols('NET_TSF_RETAIL_TY');
   CRW$SHRINK_RETAIL_PCT_LY              := CRW_Cols('SHRINK_RETAIL_PCT_LY');
   CRW$SHRINK_RETAIL_PCT_TY              := CRW_Cols('SHRINK_RETAIL_PCT_TY');
   CRW$SALES_UNITS_LY                    := CRW_Cols('SALES_UNITS_LY');
   CRW$SALES_UNITS_TY                    := CRW_Cols('SALES_UNITS_TY');
   CRW$NET_SALES_NON_INV_COST_LY         := CRW_Cols('NET_SALES_NON_INV_COST_LY');
   CRW$NET_SALES_NON_INV_COST_TY         := CRW_Cols('NET_SALES_NON_INV_COST_TY');
   CRW$NET_SALES_NONINV_RETAIL_LY        := CRW_Cols('NET_SALES_NONINV_RETAIL_LY');
   CRW$NET_SALES_NONINV_RETAIL_TY        := CRW_Cols('NET_SALES_NONINV_RETAIL_TY');
   -- Cost-Retail Week Data Columns End
   
   -- Cost-Retail Daily Data Columns Begin
   CRD_Cols                              := s9t_pkg.get_col_names(I_file_id,CRD_sheet);
   CRD$DEPT                              := CRD_Cols('DEPT');
   CRD$DEPT_NAME                         := CRD_Cols('DEPT_NAME');
   CRD$CLASS                             := CRD_Cols('CLASS');
   CRD$CLASS_NAME                        := CRD_Cols('CLASS_NAME');
   CRD$SUBCLASS                          := CRD_Cols('SUBCLASS');
   CRD$SUBCLASS_NAME                     := CRD_Cols('SUBCLASS_NAME');
   CRD$LOC_TYPE                          := CRD_Cols('LOC_TYPE');
   CRD$LOCATION                          := CRD_Cols('LOCATION');
   CRD$LOC_NAME                          := CRD_Cols('LOC_NAME');
   CRD$CURRENCY                          := CRD_Cols('CURRENCY');
   CRD$EOW_DATE                          := CRD_Cols('EOW_DATE');
   CRD$DAY_NO                            := CRD_Cols('DAY_NO');
   CRD$PURCH_COST_LY                     := CRD_Cols('PURCH_COST_LY');
   CRD$PURCH_COST_TY                     := CRD_Cols('PURCH_COST_TY');
   CRD$PURCH_RETAIL_LY                   := CRD_Cols('PURCH_RETAIL_LY');
   CRD$PURCH_RETAIL_TY                   := CRD_Cols('PURCH_RETAIL_TY');
   CRD$TSF_IN_COST_LY                    := CRD_Cols('TSF_IN_COST_LY');
   CRD$TSF_IN_COST_TY                    := CRD_Cols('TSF_IN_COST_TY');
   CRD$TSF_IN_RETAIL_LY                  := CRD_Cols('TSF_IN_RETAIL_LY');
   CRD$TSF_IN_RETAIL_TY                  := CRD_Cols('TSF_IN_RETAIL_TY');
   CRD$TSF_IN_BOOK_COST_LY               := CRD_Cols('TSF_IN_BOOK_COST_LY');
   CRD$TSF_IN_BOOK_COST_TY               := CRD_Cols('TSF_IN_BOOK_COST_TY');
   CRD$TSF_IN_BOOK_RETAIL_LY             := CRD_Cols('TSF_IN_BOOK_RETAIL_LY');
   CRD$TSF_IN_BOOK_RETAIL_TY             := CRD_Cols('TSF_IN_BOOK_RETAIL_TY');
   CRD$INTERCOMPANY_IN_COST_LY           := CRD_Cols('INTERCOMPANY_IN_COST_LY');
   CRD$INTERCOMPANY_IN_COST_TY           := CRD_Cols('INTERCOMPANY_IN_COST_TY');
   CRD$INTERCOMPANY_IN_RETAIL_LY         := CRD_Cols('INTERCOMPANY_IN_RETAIL_LY');
   CRD$INTERCOMPANY_IN_RETAIL_TY         := CRD_Cols('INTERCOMPANY_IN_RETAIL_TY');
   CRD$RECLASS_IN_COST_LY                := CRD_Cols('RECLASS_IN_COST_LY');
   CRD$RECLASS_IN_COST_TY                := CRD_Cols('RECLASS_IN_COST_TY');
   CRD$RECLASS_IN_RETAIL_LY              := CRD_Cols('RECLASS_IN_RETAIL_LY');
   CRD$RECLASS_IN_RETAIL_TY              := CRD_Cols('RECLASS_IN_RETAIL_TY');
   CRD$FRANCHISE_RETURNS_COST_LY         := CRD_Cols('FRANCHISE_RETURNS_COST_LY');
   CRD$FRANCHISE_RETURNS_COST_TY         := CRD_Cols('FRANCHISE_RETURNS_COST_TY');
   CRD$FRN_RETURNS_RETAIL_LY             := CRD_Cols('FRN_RETURNS_RETAIL_LY');
   CRD$FRN_RETURNS_RETAIL_TY             := CRD_Cols('FRN_RETURNS_RETAIL_TY');
   CRD$UP_CHRG_AMT_PROFIT_LY             := CRD_Cols('UP_CHRG_AMT_PROFIT_LY');
   CRD$UP_CHRG_AMT_PROFIT_TY             := CRD_Cols('UP_CHRG_AMT_PROFIT_TY');
   CRD$UP_CHRG_AMT_EXP_LY                := CRD_Cols('UP_CHRG_AMT_EXP_LY');
   CRD$UP_CHRG_AMT_EXP_TY                := CRD_Cols('UP_CHRG_AMT_EXP_TY');
   CRD$WO_ACTIVITY_UPD_INV_LY            := CRD_Cols('WO_ACTIVITY_UPD_INV_LY');
   CRD$WO_ACTIVITY_UPD_INV_TY            := CRD_Cols('WO_ACTIVITY_UPD_INV_TY');
   CRD$WO_ACTIVITY_POST_FIN_LY           := CRD_Cols('WO_ACTIVITY_POST_FIN_LY');
   CRD$WO_ACTIVITY_POST_FIN_TY           := CRD_Cols('WO_ACTIVITY_POST_FIN_TY');
   CRD$RECOVERABLE_TAX_LY                := CRD_Cols('RECOVERABLE_TAX_LY');
   CRD$RECOVERABLE_TAX_TY                := CRD_Cols('RECOVERABLE_TAX_TY');
   CRD$RTV_COST_LY                       := CRD_Cols('RTV_COST_LY');
   CRD$RTV_COST_TY                       := CRD_Cols('RTV_COST_TY');
   CRD$RTV_RETAIL_LY                     := CRD_Cols('RTV_RETAIL_LY');
   CRD$RTV_RETAIL_TY                     := CRD_Cols('RTV_RETAIL_TY');
   CRD$TSF_OUT_COST_LY                   := CRD_Cols('TSF_OUT_COST_LY');
   CRD$TSF_OUT_COST_TY                   := CRD_Cols('TSF_OUT_COST_TY');
   CRD$TSF_OUT_RETAIL_LY                 := CRD_Cols('TSF_OUT_RETAIL_LY');
   CRD$TSF_OUT_RETAIL_TY                 := CRD_Cols('TSF_OUT_RETAIL_TY');
   CRD$TSF_OUT_BOOK_COST_LY              := CRD_Cols('TSF_OUT_BOOK_COST_LY');
   CRD$TSF_OUT_BOOK_COST_TY              := CRD_Cols('TSF_OUT_BOOK_COST_TY');
   CRD$TSF_OUT_BOOK_RETAIL_LY            := CRD_Cols('TSF_OUT_BOOK_RETAIL_LY');
   CRD$TSF_OUT_BOOK_RETAIL_TY            := CRD_Cols('TSF_OUT_BOOK_RETAIL_TY');
   CRD$INTERCOMPANY_OUT_COST_LY          := CRD_Cols('INTERCOMPANY_OUT_COST_LY');
   CRD$INTERCOMPANY_OUT_COST_TY          := CRD_Cols('INTERCOMPANY_OUT_COST_TY');
   CRD$INTERCOMPANY_OUT_RETAIL_LY        := CRD_Cols('INTERCOMPANY_OUT_RETAIL_LY');
   CRD$INTERCOMPANY_OUT_RETAIL_TY        := CRD_Cols('INTERCOMPANY_OUT_RETAIL_TY');
   CRD$RECLASS_OUT_COST_LY               := CRD_Cols('RECLASS_OUT_COST_LY');
   CRD$RECLASS_OUT_COST_TY               := CRD_Cols('RECLASS_OUT_COST_TY');
   CRD$RECLASS_OUT_RETAIL_LY             := CRD_Cols('RECLASS_OUT_RETAIL_LY');
   CRD$RECLASS_OUT_RETAIL_TY             := CRD_Cols('RECLASS_OUT_RETAIL_TY');
   CRD$NET_SALES_COST_LY                 := CRD_Cols('NET_SALES_COST_LY');
   CRD$NET_SALES_COST_TY                 := CRD_Cols('NET_SALES_COST_TY');
   CRD$NET_SALES_RETAIL_LY               := CRD_Cols('NET_SALES_RETAIL_LY');
   CRD$NET_SALES_RETAIL_TY               := CRD_Cols('NET_SALES_RETAIL_TY');
   CRD$RETURNS_COST_LY                   := CRD_Cols('RETURNS_COST_LY');
   CRD$RETURNS_COST_TY                   := CRD_Cols('RETURNS_COST_TY');
   CRD$RETURNS_RETAIL_LY                 := CRD_Cols('RETURNS_RETAIL_LY');
   CRD$RETURNS_RETAIL_TY                 := CRD_Cols('RETURNS_RETAIL_TY');
   CRD$FRANCHISE_SALES_COST_LY           := CRD_Cols('FRANCHISE_SALES_COST_LY');
   CRD$FRANCHISE_SALES_COST_TY           := CRD_Cols('FRANCHISE_SALES_COST_TY');
   CRD$FRANCHISE_SALES_RETAIL_LY         := CRD_Cols('FRANCHISE_SALES_RETAIL_LY');
   CRD$FRANCHISE_SALES_RETAIL_TY         := CRD_Cols('FRANCHISE_SALES_RETAIL_TY');
   CRD$WEIGHT_VARIANCE_RETAIL_LY         := CRD_Cols('WEIGHT_VARIANCE_RETAIL_LY');
   CRD$WEIGHT_VARIANCE_RETAIL_TY         := CRD_Cols('WEIGHT_VARIANCE_RETAIL_TY');
   CRD$FREIGHT_CLAIM_COST_LY             := CRD_Cols('FREIGHT_CLAIM_COST_LY');
   CRD$FREIGHT_CLAIM_COST_TY             := CRD_Cols('FREIGHT_CLAIM_COST_TY');
   CRD$FREIGHT_CLAIM_RETAIL_LY           := CRD_Cols('FREIGHT_CLAIM_RETAIL_LY');
   CRD$FREIGHT_CLAIM_RETAIL_TY           := CRD_Cols('FREIGHT_CLAIM_RETAIL_TY');
   CRD$STOCK_ADJ_COGS_COST_LY            := CRD_Cols('STOCK_ADJ_COGS_COST_LY');
   CRD$STOCK_ADJ_COGS_COST_TY            := CRD_Cols('STOCK_ADJ_COGS_COST_TY');
   CRD$STOCK_ADJ_COGS_RETAIL_LY          := CRD_Cols('STOCK_ADJ_COGS_RETAIL_LY');
   CRD$STOCK_ADJ_COGS_RETAIL_TY          := CRD_Cols('STOCK_ADJ_COGS_RETAIL_TY');
   CRD$STOCK_ADJ_COST_LY                 := CRD_Cols('STOCK_ADJ_COST_LY');
   CRD$STOCK_ADJ_COST_TY                 := CRD_Cols('STOCK_ADJ_COST_TY');
   CRD$STOCK_ADJ_RETAIL_LY               := CRD_Cols('STOCK_ADJ_RETAIL_LY');
   CRD$STOCK_ADJ_RETAIL_TY               := CRD_Cols('STOCK_ADJ_RETAIL_TY');
   CRD$COST_VARIANCE_AMT_LY              := CRD_Cols('COST_VARIANCE_AMT_LY');
   CRD$COST_VARIANCE_AMT_TY              := CRD_Cols('COST_VARIANCE_AMT_TY');
   CRD$RETAIL_COST_VARIANCE_LY           := CRD_Cols('RETAIL_COST_VARIANCE_LY');
   CRD$RETAIL_COST_VARIANCE_TY           := CRD_Cols('RETAIL_COST_VARIANCE_TY');
   CRD$MARGIN_COST_VARIANCE_LY           := CRD_Cols('MARGIN_COST_VARIANCE_LY');
   CRD$MARGIN_COST_VARIANCE_TY           := CRD_Cols('MARGIN_COST_VARIANCE_TY');
   CRD$VAT_IN_LY                         := CRD_Cols('VAT_IN_LY');
   CRD$VAT_IN_TY                         := CRD_Cols('VAT_IN_TY');
   CRD$VAT_OUT_LY                        := CRD_Cols('VAT_OUT_LY');
   CRD$VAT_OUT_TY                        := CRD_Cols('VAT_OUT_TY');
   CRD$DEAL_INCOME_SALES_LY              := CRD_Cols('DEAL_INCOME_SALES_LY');
   CRD$DEAL_INCOME_SALES_TY              := CRD_Cols('DEAL_INCOME_SALES_TY');
   CRD$DEAL_INCOME_PURCH_LY              := CRD_Cols('DEAL_INCOME_PURCH_LY');
   CRD$DEAL_INCOME_PURCH_TY              := CRD_Cols('DEAL_INCOME_PURCH_TY');
   CRD$RESTOCKING_FEE_LY                 := CRD_Cols('RESTOCKING_FEE_LY');
   CRD$RESTOCKING_FEE_TY                 := CRD_Cols('RESTOCKING_FEE_TY');
   CRD$FRN_RESTOCKING_FEE_LY             := CRD_Cols('FRN_RESTOCKING_FEE_LY');
   CRD$FRN_RESTOCKING_FEE_TY             := CRD_Cols('FRN_RESTOCKING_FEE_TY');
   CRD$NET_SALES_NON_INV_COST_LY         := CRD_Cols('NET_SALES_NON_INV_COST_LY');
   CRD$NET_SALES_NON_INV_COST_TY         := CRD_Cols('NET_SALES_NON_INV_COST_TY');
   CRD$ADD_COST_LY                       := CRD_Cols('ADD_COST_LY');
   CRD$ADD_COST_TY                       := CRD_Cols('ADD_COST_TY');
   CRD$ADD_RETAIL_LY                     := CRD_Cols('ADD_RETAIL_LY');
   CRD$ADD_RETAIL_TY                     := CRD_Cols('ADD_RETAIL_TY');
   CRD$CASH_DISC_AMT_LY                  := CRD_Cols('CASH_DISC_AMT_LY');
   CRD$CASH_DISC_AMT_TY                  := CRD_Cols('CASH_DISC_AMT_TY');
   CRD$CLEAR_MARKDOWN_RETAIL_LY          := CRD_Cols('CLEAR_MARKDOWN_RETAIL_LY');
   CRD$CLEAR_MARKDOWN_RETAIL_TY          := CRD_Cols('CLEAR_MARKDOWN_RETAIL_TY');
   CRD$EMPL_DISC_RETAIL_LY               := CRD_Cols('EMPL_DISC_RETAIL_LY');
   CRD$EMPL_DISC_RETAIL_TY               := CRD_Cols('EMPL_DISC_RETAIL_TY');
   CRD$FRN_MARKDOWN_RETAIL_LY            := CRD_Cols('FRN_MARKDOWN_RETAIL_LY');
   CRD$FRN_MARKDOWN_RETAIL_TY            := CRD_Cols('FRN_MARKDOWN_RETAIL_TY');
   CRD$FRN_MARKUP_RETAIL_LY              := CRD_Cols('FRN_MARKUP_RETAIL_LY');
   CRD$FRN_MARKUP_RETAIL_TY              := CRD_Cols('FRN_MARKUP_RETAIL_TY');
   CRD$FREIGHT_COST_LY                   := CRD_Cols('FREIGHT_COST_LY');
   CRD$FREIGHT_COST_TY                   := CRD_Cols('FREIGHT_COST_TY');
   CRD$INTERCOMPANY_MARKDOWN_LY          := CRD_Cols('INTERCOMPANY_MARKDOWN_LY');
   CRD$INTERCOMPANY_MARKDOWN_TY          := CRD_Cols('INTERCOMPANY_MARKDOWN_TY');
   CRD$INTERCOMPANY_MARKUP_LY            := CRD_Cols('INTERCOMPANY_MARKUP_LY');
   CRD$INTERCOMPANY_MARKUP_TY            := CRD_Cols('INTERCOMPANY_MARKUP_TY');
   CRD$NET_MARKDOWN_LY                   := CRD_Cols('NET_MARKDOWN_LY');
   CRD$NET_MARKDOWN_TY                   := CRD_Cols('NET_MARKDOWN_TY');
   CRD$MARKDOWN_CAN_RETAIL_LY            := CRD_Cols('MARKDOWN_CAN_RETAIL_LY');
   CRD$MARKDOWN_CAN_RETAIL_TY            := CRD_Cols('MARKDOWN_CAN_RETAIL_TY');
   CRD$MARKUP_CAN_RETAIL_LY              := CRD_Cols('MARKUP_CAN_RETAIL_LY');
   CRD$MARKUP_CAN_RETAIL_TY              := CRD_Cols('MARKUP_CAN_RETAIL_TY');
   CRD$MARKUP_RETAIL_LY                  := CRD_Cols('MARKUP_RETAIL_LY');
   CRD$MARKUP_RETAIL_TY                  := CRD_Cols('MARKUP_RETAIL_TY');
   CRD$PERM_MARKDOWN_RETAIL_LY           := CRD_Cols('PERM_MARKDOWN_RETAIL_LY');
   CRD$PERM_MARKDOWN_RETAIL_TY           := CRD_Cols('PERM_MARKDOWN_RETAIL_TY');
   CRD$PROM_MARKDOWN_RETAIL_LY           := CRD_Cols('PROM_MARKDOWN_RETAIL_LY');
   CRD$PROM_MARKDOWN_RETAIL_TY           := CRD_Cols('PROM_MARKDOWN_RETAIL_TY');
   CRD$REC_COST_ADJ_VARIANCE_LY          := CRD_Cols('REC_COST_ADJ_VARIANCE_LY');
   CRD$REC_COST_ADJ_VARIANCE_TY          := CRD_Cols('REC_COST_ADJ_VARIANCE_TY');
   CRD$SALES_UNITS_LY                    := CRD_Cols('SALES_UNITS_LY');
   CRD$SALES_UNITS_TY                    := CRD_Cols('SALES_UNITS_TY');
   CRD$WORKROOM_AMT_LY                   := CRD_Cols('WORKROOM_AMT_LY');
   CRD$WORKROOM_AMT_TY                   := CRD_Cols('WORKROOM_AMT_TY');
   CRD$NET_RECLASS_COST_LY               := CRD_Cols('NET_RECLASS_COST_LY');
   CRD$NET_RECLASS_COST_TY               := CRD_Cols('NET_RECLASS_COST_TY');
   CRD$NET_RECLASS_RETAIL_LY             := CRD_Cols('NET_RECLASS_RETAIL_LY');
   CRD$NET_RECLASS_RETAIL_TY             := CRD_Cols('NET_RECLASS_RETAIL_TY');
   CRD$NET_TSF_COST_LY                   := CRD_Cols('NET_TSF_COST_LY');
   CRD$NET_TSF_COST_TY                   := CRD_Cols('NET_TSF_COST_TY');
   CRD$NET_MARKUP_LY                     := CRD_Cols('NET_MARKUP_LY');
   CRD$NET_MARKUP_TY                     := CRD_Cols('NET_MARKUP_TY');
   CRD$NET_SALES_RETAIL_EX_VAT_LY        := CRD_Cols('NET_SALES_RETAIL_EX_VAT_LY');
   CRD$NET_SALES_RETAIL_EX_VAT_TY        := CRD_Cols('NET_SALES_RETAIL_EX_VAT_TY');
   CRD$NET_SALE_NONINV_R_EXVAT_LY        := CRD_Cols('NET_SALE_NONINV_R_EXVAT_LY');
   CRD$NET_SALE_NONINV_R_EXVAT_TY        := CRD_Cols('NET_SALE_NONINV_R_EXVAT_TY');
   CRD$NET_SALES_NONINV_RETAIL_LY        := CRD_Cols('NET_SALES_NONINV_RETAIL_LY');
   CRD$NET_SALES_NONINV_RETAIL_TY        := CRD_Cols('NET_SALES_NONINV_RETAIL_TY');
   CRD$NET_TSF_RETAIL_LY                 := CRD_Cols('NET_TSF_RETAIL_LY');
   CRD$NET_TSF_RETAIL_TY                 := CRD_Cols('NET_TSF_RETAIL_TY');
   -- Cost-Retail Daily Data Columns End
END POPULATE_NAMES;
PROCEDURE POPULATE_C_MONTH_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_c_month_data_tbl.FIRST..LP_c_month_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = CMD_sheet)
   select s9t_row(s9t_cells(
                           LP_c_month_data_tbl(j).dept,
                           LP_c_month_data_tbl(j).dept_name,
                           LP_c_month_data_tbl(j).class,
                           LP_c_month_data_tbl(j).class_name,
                           LP_c_month_data_tbl(j).subclass,
                           LP_c_month_data_tbl(j).subclass_name,
                           LP_c_month_data_tbl(j).loc_type,
                           LP_c_month_data_tbl(j).location,
                           LP_c_month_data_tbl(j).loc_name,
                           LP_c_month_data_tbl(j).currency_code,
                           LP_c_month_data_tbl(j).half_no,
                           LP_c_month_data_tbl(j).month_no,
                           nvl(LP_c_month_data_tbl(j).opn_stk_cost_ly,0.0),
                           nvl(LP_c_month_data_tbl(j).opn_stk_cost_ty,0.0),
                           nvl(LP_c_month_data_tbl(j).purch_cost_ly,0.0),
                           nvl(LP_c_month_data_tbl(j).purch_cost_ty,0.0),
                           nvl(LP_c_month_data_tbl(j).tsf_in_cost_ly,0.0),
                           nvl(LP_c_month_data_tbl(j).tsf_in_cost_ty,0.0),
                           nvl(LP_c_month_data_tbl(j).tsf_in_book_cost_ly,0.0),
                           nvl(LP_c_month_data_tbl(j).tsf_in_book_cost_ty,0.0),
                           nvl(LP_c_month_data_tbl(j).intercompany_in_cost_ly,0.0),
                           nvl(LP_c_month_data_tbl(j).intercompany_in_cost_ty,0.0),
                           nvl(LP_c_month_data_tbl(j).reclass_in_cost_ly,0.0),
                           nvl(LP_c_month_data_tbl(j).reclass_in_cost_ty,0.0),
                           nvl(LP_c_month_data_tbl(j).franchise_returns_cost_ly,0.0),    
                           nvl(LP_c_month_data_tbl(j).franchise_returns_cost_ty,0.0),    
                           nvl(LP_c_month_data_tbl(j).up_chrg_amt_profit_ly,0.0),        
                           nvl(LP_c_month_data_tbl(j).up_chrg_amt_profit_ty,0.0),        
                           nvl(LP_c_month_data_tbl(j).up_chrg_amt_exp_ly,0.0),           
                           nvl(LP_c_month_data_tbl(j).up_chrg_amt_exp_ty,0.0),           
                           nvl(LP_c_month_data_tbl(j).wo_activity_upd_inv_ly,0.0),       
                           nvl(LP_c_month_data_tbl(j).wo_activity_upd_inv_ty,0.0),       
                           nvl(LP_c_month_data_tbl(j).recoverable_tax_ly,0.0),           
                           nvl(LP_c_month_data_tbl(j).recoverable_tax_ty,0.0),           
                           nvl(LP_c_month_data_tbl(j).rtv_cost_ly,0.0),                  
                           nvl(LP_c_month_data_tbl(j).rtv_cost_ty,0.0),                  
                           nvl(LP_c_month_data_tbl(j).tsf_out_cost_ly,0.0),              
                           nvl(LP_c_month_data_tbl(j).tsf_out_cost_ty,0.0),              
                           nvl(LP_c_month_data_tbl(j).tsf_out_book_cost_ly,0.0),         
                           nvl(LP_c_month_data_tbl(j).tsf_out_book_cost_ty,0.0),         
                           nvl(LP_c_month_data_tbl(j).intercompany_out_cost_ly,0.0),     
                           nvl(LP_c_month_data_tbl(j).intercompany_out_cost_ty,0.0),     
                           nvl(LP_c_month_data_tbl(j).reclass_out_cost_ly,0.0),          
                           nvl(LP_c_month_data_tbl(j).reclass_out_cost_ty,0.0),          
                           nvl(LP_c_month_data_tbl(j).net_sales_cost_ly,0.0),            
                           nvl(LP_c_month_data_tbl(j).net_sales_cost_ty,0.0),            
                           nvl(LP_c_month_data_tbl(j).returns_cost_ly,0.0),              
                           nvl(LP_c_month_data_tbl(j).returns_cost_ty,0.0),              
                           nvl(LP_c_month_data_tbl(j).franchise_sales_cost_ly,0.0),      
                           nvl(LP_c_month_data_tbl(j).franchise_sales_cost_ty,0.0),      
                           nvl(LP_c_month_data_tbl(j).freight_claim_cost_ly,0.0),        
                           nvl(LP_c_month_data_tbl(j).freight_claim_cost_ty,0.0),        
                           nvl(LP_c_month_data_tbl(j).stock_adj_cogs_cost_ly,0.0),       
                           nvl(LP_c_month_data_tbl(j).stock_adj_cogs_cost_ty,0.0),
                           nvl(LP_c_month_data_tbl(j).stock_adj_cost_ly,0.0),            
                           nvl(LP_c_month_data_tbl(j).stock_adj_cost_ty,0.0),       
                           nvl(LP_c_month_data_tbl(j).shrinkage_cost_ly,0.0),            
                           nvl(LP_c_month_data_tbl(j).shrinkage_cost_ty,0.0),            
                           nvl(LP_c_month_data_tbl(j).cost_variance_amt_ly,0.0),         
                           nvl(LP_c_month_data_tbl(j).cost_variance_amt_ty,0.0),         
                           nvl(LP_c_month_data_tbl(j).margin_cost_variance_ly,0.0),      
                           nvl(LP_c_month_data_tbl(j).margin_cost_variance_ty,0.0),      
                           nvl(LP_c_month_data_tbl(j).cls_stk_cost_ly,0.0),              
                           nvl(LP_c_month_data_tbl(j).cls_stk_cost_ty,0.0),              
                           nvl(LP_c_month_data_tbl(j).add_cost_ly,0.0),                  
                           nvl(LP_c_month_data_tbl(j).add_cost_ty,0.0),                  
                           nvl(LP_c_month_data_tbl(j).add_retail_ly,0.0),                
                           nvl(LP_c_month_data_tbl(j).add_retail_ty,0.0),                
                           nvl(LP_c_month_data_tbl(j).shrink_cost_pct_ly,0.0),           
                           nvl(LP_c_month_data_tbl(j).shrink_cost_pct_ty,0.0),           
                           nvl(LP_c_month_data_tbl(j).red_retail_ly,0.0),                
                           nvl(LP_c_month_data_tbl(j).red_retail_ty,0.0),                
                           nvl(LP_c_month_data_tbl(j).deal_income_sales_ly,0.0),         
                           nvl(LP_c_month_data_tbl(j).deal_income_sales_ty,0.0),         
                           nvl(LP_c_month_data_tbl(j).deal_income_purch_ly,0.0),         
                           nvl(LP_c_month_data_tbl(j).deal_income_purch_ty,0.0),         
                           nvl(LP_c_month_data_tbl(j).vat_in_ly,0.0),                    
                           nvl(LP_c_month_data_tbl(j).vat_in_ty,0.0),                    
                           nvl(LP_c_month_data_tbl(j).vat_out_ly,0.0),                   
                           nvl(LP_c_month_data_tbl(j).vat_out_ty,0.0),                   
                           nvl(LP_c_month_data_tbl(j).wo_activity_post_fin_ly,0.0),      
                           nvl(LP_c_month_data_tbl(j).wo_activity_post_fin_ty,0.0),      
                           nvl(LP_c_month_data_tbl(j).restocking_fee_ly,0.0),            
                           nvl(LP_c_month_data_tbl(j).restocking_fee_ty,0.0),            
                           nvl(LP_c_month_data_tbl(j).freight_cost_ly,0.0),              
                           nvl(LP_c_month_data_tbl(j).freight_cost_ty,0.0),              
                           nvl(LP_c_month_data_tbl(j).rec_cost_adj_variance_ly,0.0),     
                           nvl(LP_c_month_data_tbl(j).rec_cost_adj_variance_ty,0.0),     
                           nvl(LP_c_month_data_tbl(j).workroom_amt_ly,0.0),              
                           nvl(LP_c_month_data_tbl(j).workroom_amt_ty,0.0),              
                           nvl(LP_c_month_data_tbl(j).cash_disc_amt_ly,0.0),             
                           nvl(LP_c_month_data_tbl(j).cash_disc_amt_ty,0.0),             
                           nvl(LP_c_month_data_tbl(j).clear_markdown_retail_ly,0.0),     
                           nvl(LP_c_month_data_tbl(j).clear_markdown_retail_ty,0.0),     
                           nvl(LP_c_month_data_tbl(j).empl_disc_retail_ly,0.0),          
                           nvl(LP_c_month_data_tbl(j).empl_disc_retail_ty,0.0),          
                           nvl(LP_c_month_data_tbl(j).markup_can_retail_ly,0.0),         
                           nvl(LP_c_month_data_tbl(j).markup_can_retail_ty,0.0),         
                           nvl(LP_c_month_data_tbl(j).markup_retail_ly,0.0),             
                           nvl(LP_c_month_data_tbl(j).markup_retail_ty,0.0),             
                           nvl(LP_c_month_data_tbl(j).perm_markdown_retail_ly,0.0),      
                           nvl(LP_c_month_data_tbl(j).perm_markdown_retail_ty,0.0),      
                           nvl(LP_c_month_data_tbl(j).prom_markdown_retail_ly,0.0),      
                           nvl(LP_c_month_data_tbl(j).prom_markdown_retail_ty,0.0),      
                           nvl(LP_c_month_data_tbl(j).franchise_markdown_retail_ly,0.0), 
                           nvl(LP_c_month_data_tbl(j).franchise_markdown_retail_ty,0.0), 
                           nvl(LP_c_month_data_tbl(j).franchise_markup_retail_ly,0.0),   
                           nvl(LP_c_month_data_tbl(j).franchise_markup_retail_ty,0.0),   
                           nvl(LP_c_month_data_tbl(j).franchise_restocking_fee_ly,0.0),  
                           nvl(LP_c_month_data_tbl(j).franchise_restocking_fee_ty,0.0),  
                           nvl(LP_c_month_data_tbl(j).intercompany_markdown_ly,0.0),     
                           nvl(LP_c_month_data_tbl(j).intercompany_markdown_ty,0.0),     
                           nvl(LP_c_month_data_tbl(j).intercompany_markup_ly,0.0),       
                           nvl(LP_c_month_data_tbl(j).intercompany_markup_ty,0.0),       
                           nvl(LP_c_month_data_tbl(j).intercompany_margin_ly,0.0),       
                           nvl(LP_c_month_data_tbl(j).intercompany_margin_ty,0.0),       
                           nvl(LP_c_month_data_tbl(j).markdown_can_retail_ly,0.0),       
                           nvl(LP_c_month_data_tbl(j).markdown_can_retail_ty,0.0),       
                           nvl(LP_c_month_data_tbl(j).net_markup_ly,0.0),                
                           nvl(LP_c_month_data_tbl(j).net_markup_ty,0.0),                
                           nvl(LP_c_month_data_tbl(j).net_reclass_cost_ly,0.0),          
                           nvl(LP_c_month_data_tbl(j).net_reclass_cost_ty,0.0),          
                           nvl(LP_c_month_data_tbl(j).net_tsf_cost_ly,0.0),              
                           nvl(LP_c_month_data_tbl(j).net_tsf_cost_ty,0.0),              
                           nvl(LP_c_month_data_tbl(j).sales_units_ly,0.0),               
                           nvl(LP_c_month_data_tbl(j).sales_units_ty,0.0),               
                           nvl(LP_c_month_data_tbl(j).cum_markon_pct_ly,0.0),            
                           nvl(LP_c_month_data_tbl(j).cum_markon_pct_ty,0.0),            
                           nvl(LP_c_month_data_tbl(j).gross_margin_amt_ly,0.0),          
                           nvl(LP_c_month_data_tbl(j).gross_margin_amt_ty,0.0),          
                           nvl(LP_c_month_data_tbl(j).net_sales_non_inv_cost_ly,0.0),    
                           nvl(LP_c_month_data_tbl(j).net_sales_non_inv_cost_ty,0.0)))
                      from dual;

END POPULATE_C_MONTH_DATA;
PROCEDURE POPULATE_C_WEEK_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_c_week_data_tbl.FIRST..LP_c_week_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = CWD_sheet)
   select s9t_row(s9t_cells(
                           LP_c_week_data_tbl(j).dept,
                           LP_c_week_data_tbl(j).dept_name,
                           LP_c_week_data_tbl(j).class,
                           LP_c_week_data_tbl(j).class_name,
                           LP_c_week_data_tbl(j).subclass,
                           LP_c_week_data_tbl(j).subclass_name,
                           LP_c_week_data_tbl(j).loc_type,
                           LP_c_week_data_tbl(j).location,
                           LP_c_week_data_tbl(j).loc_name,
                           LP_c_week_data_tbl(j).currency_code,
                           LP_c_week_data_tbl(j).eom_date,
                           LP_c_week_data_tbl(j).week_no,
                           nvl(LP_c_week_data_tbl(j).opn_stk_cost_ly,0.0),              
                           nvl(LP_c_week_data_tbl(j).opn_stk_cost_ty,0.0),              
                           nvl(LP_c_week_data_tbl(j).purch_cost_ly,0.0),                
                           nvl(LP_c_week_data_tbl(j).purch_cost_ty,0.0),                
                           nvl(LP_c_week_data_tbl(j).tsf_in_cost_ly,0.0),               
                           nvl(LP_c_week_data_tbl(j).tsf_in_cost_ty,0.0),               
                           nvl(LP_c_week_data_tbl(j).tsf_in_book_cost_ly,0.0),          
                           nvl(LP_c_week_data_tbl(j).tsf_in_book_cost_ty,0.0),          
                           nvl(LP_c_week_data_tbl(j).intercompany_in_cost_ly,0.0),      
                           nvl(LP_c_week_data_tbl(j).intercompany_in_cost_ty,0.0),      
                           nvl(LP_c_week_data_tbl(j).reclass_in_cost_ly,0.0),           
                           nvl(LP_c_week_data_tbl(j).reclass_in_cost_ty,0.0),           
                           nvl(LP_c_week_data_tbl(j).franchise_returns_cost_ly,0.0),    
                           nvl(LP_c_week_data_tbl(j).franchise_returns_cost_ty,0.0),    
                           nvl(LP_c_week_data_tbl(j).up_chrg_amt_profit_ly,0.0),        
                           nvl(LP_c_week_data_tbl(j).up_chrg_amt_profit_ty,0.0),        
                           nvl(LP_c_week_data_tbl(j).up_chrg_amt_exp_ly,0.0),           
                           nvl(LP_c_week_data_tbl(j).up_chrg_amt_exp_ty,0.0),           
                           nvl(LP_c_week_data_tbl(j).wo_activity_upd_inv_ly,0.0),       
                           nvl(LP_c_week_data_tbl(j).wo_activity_upd_inv_ty,0.0),       
                           nvl(LP_c_week_data_tbl(j).recoverable_tax_ly,0.0),           
                           nvl(LP_c_week_data_tbl(j).recoverable_tax_ty,0.0),           
                           nvl(LP_c_week_data_tbl(j).rtv_cost_ly,0.0),                  
                           nvl(LP_c_week_data_tbl(j).rtv_cost_ty,0.0),                  
                           nvl(LP_c_week_data_tbl(j).tsf_out_cost_ly,0.0),              
                           nvl(LP_c_week_data_tbl(j).tsf_out_cost_ty,0.0),              
                           nvl(LP_c_week_data_tbl(j).tsf_out_book_cost_ly,0.0),         
                           nvl(LP_c_week_data_tbl(j).tsf_out_book_cost_ty,0.0),         
                           nvl(LP_c_week_data_tbl(j).intercompany_out_cost_ly,0.0),     
                           nvl(LP_c_week_data_tbl(j).intercompany_out_cost_ty,0.0),     
                           nvl(LP_c_week_data_tbl(j).reclass_out_cost_ly,0.0),          
                           nvl(LP_c_week_data_tbl(j).reclass_out_cost_ty,0.0),          
                           nvl(LP_c_week_data_tbl(j).net_sales_cost_ly,0.0),            
                           nvl(LP_c_week_data_tbl(j).net_sales_cost_ty,0.0),            
                           nvl(LP_c_week_data_tbl(j).returns_cost_ly,0.0),              
                           nvl(LP_c_week_data_tbl(j).returns_cost_ty,0.0),              
                           nvl(LP_c_week_data_tbl(j).franchise_sales_cost_ly,0.0),      
                           nvl(LP_c_week_data_tbl(j).franchise_sales_cost_ty,0.0),      
                           nvl(LP_c_week_data_tbl(j).freight_claim_cost_ly,0.0),        
                           nvl(LP_c_week_data_tbl(j).freight_claim_cost_ty,0.0),        
                           nvl(LP_c_week_data_tbl(j).stock_adj_cogs_cost_ly,0.0),       
                           nvl(LP_c_week_data_tbl(j).stock_adj_cogs_cost_ty,0.0),
                           nvl(LP_c_week_data_tbl(j).stock_adj_cost_ly,0.0),            
                           nvl(LP_c_week_data_tbl(j).stock_adj_cost_ty,0.0),
                           nvl(LP_c_week_data_tbl(j).shrinkage_cost_ly,0.0),            
                           nvl(LP_c_week_data_tbl(j).shrinkage_cost_ty,0.0),            
                           nvl(LP_c_week_data_tbl(j).cost_variance_amt_ly,0.0),         
                           nvl(LP_c_week_data_tbl(j).cost_variance_amt_ty,0.0),         
                           nvl(LP_c_week_data_tbl(j).margin_cost_variance_ly,0.0),      
                           nvl(LP_c_week_data_tbl(j).margin_cost_variance_ty,0.0),      
                           nvl(LP_c_week_data_tbl(j).cls_stk_cost_ly,0.0),              
                           nvl(LP_c_week_data_tbl(j).cls_stk_cost_ty,0.0),              
                           nvl(LP_c_week_data_tbl(j).add_cost_ly,0.0),                  
                           nvl(LP_c_week_data_tbl(j).add_cost_ty,0.0),                  
                           nvl(LP_c_week_data_tbl(j).add_retail_ly,0.0),                
                           nvl(LP_c_week_data_tbl(j).add_retail_ty,0.0),                
                           nvl(LP_c_week_data_tbl(j).shrink_cost_pct_ly,0.0),           
                           nvl(LP_c_week_data_tbl(j).shrink_cost_pct_ty,0.0),           
                           nvl(LP_c_week_data_tbl(j).red_retail_ly,0.0),                
                           nvl(LP_c_week_data_tbl(j).red_retail_ty,0.0),                
                           nvl(LP_c_week_data_tbl(j).deal_income_sales_ly,0.0),         
                           nvl(LP_c_week_data_tbl(j).deal_income_sales_ty,0.0),         
                           nvl(LP_c_week_data_tbl(j).deal_income_purch_ly,0.0),         
                           nvl(LP_c_week_data_tbl(j).deal_income_purch_ty,0.0),         
                           nvl(LP_c_week_data_tbl(j).vat_in_ly,0.0),                    
                           nvl(LP_c_week_data_tbl(j).vat_in_ty,0.0),                    
                           nvl(LP_c_week_data_tbl(j).vat_out_ly,0.0),                   
                           nvl(LP_c_week_data_tbl(j).vat_out_ty,0.0),                   
                           nvl(LP_c_week_data_tbl(j).wo_activity_post_fin_ly,0.0),      
                           nvl(LP_c_week_data_tbl(j).wo_activity_post_fin_ty,0.0),      
                           nvl(LP_c_week_data_tbl(j).restocking_fee_ly,0.0),            
                           nvl(LP_c_week_data_tbl(j).restocking_fee_ty,0.0),            
                           nvl(LP_c_week_data_tbl(j).freight_cost_ly,0.0),              
                           nvl(LP_c_week_data_tbl(j).freight_cost_ty,0.0),              
                           nvl(LP_c_week_data_tbl(j).rec_cost_adj_variance_ly,0.0),     
                           nvl(LP_c_week_data_tbl(j).rec_cost_adj_variance_ty,0.0),     
                           nvl(LP_c_week_data_tbl(j).workroom_amt_ly,0.0),              
                           nvl(LP_c_week_data_tbl(j).workroom_amt_ty,0.0),              
                           nvl(LP_c_week_data_tbl(j).cash_disc_amt_ly,0.0),             
                           nvl(LP_c_week_data_tbl(j).cash_disc_amt_ty,0.0),             
                           nvl(LP_c_week_data_tbl(j).clear_markdown_retail_ly,0.0),     
                           nvl(LP_c_week_data_tbl(j).clear_markdown_retail_ty,0.0),     
                           nvl(LP_c_week_data_tbl(j).empl_disc_retail_ly,0.0),          
                           nvl(LP_c_week_data_tbl(j).empl_disc_retail_ty,0.0),          
                           nvl(LP_c_week_data_tbl(j).markup_can_retail_ly,0.0),         
                           nvl(LP_c_week_data_tbl(j).markup_can_retail_ty,0.0),         
                           nvl(LP_c_week_data_tbl(j).markup_retail_ly,0.0),             
                           nvl(LP_c_week_data_tbl(j).markup_retail_ty,0.0),             
                           nvl(LP_c_week_data_tbl(j).perm_markdown_retail_ly,0.0),      
                           nvl(LP_c_week_data_tbl(j).perm_markdown_retail_ty,0.0),      
                           nvl(LP_c_week_data_tbl(j).prom_markdown_retail_ly,0.0),      
                           nvl(LP_c_week_data_tbl(j).prom_markdown_retail_ty,0.0),      
                           nvl(LP_c_week_data_tbl(j).franchise_markdown_retail_ly,0.0), 
                           nvl(LP_c_week_data_tbl(j).franchise_markdown_retail_ty,0.0), 
                           nvl(LP_c_week_data_tbl(j).franchise_markup_retail_ly,0.0),   
                           nvl(LP_c_week_data_tbl(j).franchise_markup_retail_ty,0.0),   
                           nvl(LP_c_week_data_tbl(j).franchise_restocking_fee_ly,0.0),  
                           nvl(LP_c_week_data_tbl(j).franchise_restocking_fee_ty,0.0),  
                           nvl(LP_c_week_data_tbl(j).intercompany_markdown_ly,0.0),     
                           nvl(LP_c_week_data_tbl(j).intercompany_markdown_ty,0.0),     
                           nvl(LP_c_week_data_tbl(j).intercompany_markup_ly,0.0),       
                           nvl(LP_c_week_data_tbl(j).intercompany_markup_ty,0.0),       
                           nvl(LP_c_week_data_tbl(j).intercompany_margin_ly,0.0),       
                           nvl(LP_c_week_data_tbl(j).intercompany_margin_ty,0.0),       
                           nvl(LP_c_week_data_tbl(j).markdown_can_retail_ly,0.0),       
                           nvl(LP_c_week_data_tbl(j).markdown_can_retail_ty,0.0),       
                           nvl(LP_c_week_data_tbl(j).net_markup_ly,0.0),                
                           nvl(LP_c_week_data_tbl(j).net_markup_ty,0.0),                
                           nvl(LP_c_week_data_tbl(j).net_reclass_cost_ly,0.0),          
                           nvl(LP_c_week_data_tbl(j).net_reclass_cost_ty,0.0),          
                           nvl(LP_c_week_data_tbl(j).net_tsf_cost_ly,0.0),              
                           nvl(LP_c_week_data_tbl(j).net_tsf_cost_ty,0.0),              
                           nvl(LP_c_week_data_tbl(j).sales_units_ly,0.0),               
                           nvl(LP_c_week_data_tbl(j).sales_units_ty,0.0),               
                           nvl(LP_c_week_data_tbl(j).cum_markon_pct_ly,0.0),            
                           nvl(LP_c_week_data_tbl(j).cum_markon_pct_ty,0.0),            
                           nvl(LP_c_week_data_tbl(j).gross_margin_amt_ly,0.0),          
                           nvl(LP_c_week_data_tbl(j).gross_margin_amt_ty,0.0),          
                           nvl(LP_c_week_data_tbl(j).net_sales_non_inv_cost_ly,0.0),    
                           nvl(LP_c_week_data_tbl(j).net_sales_non_inv_cost_ty,0.0)))
                      from dual;

END POPULATE_C_WEEK_DATA;
PROCEDURE POPULATE_C_DAILY_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_c_daily_data_tbl.FIRST..LP_c_daily_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = CDD_sheet)
   select s9t_row(s9t_cells(
                           LP_c_daily_data_tbl(j).dept,
                           LP_c_daily_data_tbl(j).dept_name,
                           LP_c_daily_data_tbl(j).class,
                           LP_c_daily_data_tbl(j).class_name,
                           LP_c_daily_data_tbl(j).subclass,
                           LP_c_daily_data_tbl(j).subclass_name,
                           LP_c_daily_data_tbl(j).loc_type,
                           LP_c_daily_data_tbl(j).location,
                           LP_c_daily_data_tbl(j).loc_name,
                           LP_c_daily_data_tbl(j).currency_code,
                           LP_c_daily_data_tbl(j).eow_date,
                           LP_c_daily_data_tbl(j).day_no,
                           nvl(LP_c_daily_data_tbl(j).purch_cost_ly,0.0),                
                           nvl(LP_c_daily_data_tbl(j).purch_cost_ty,0.0),                
                           nvl(LP_c_daily_data_tbl(j).tsf_in_cost_ly,0.0),               
                           nvl(LP_c_daily_data_tbl(j).tsf_in_cost_ty,0.0),               
                           nvl(LP_c_daily_data_tbl(j).tsf_in_book_cost_ly,0.0),          
                           nvl(LP_c_daily_data_tbl(j).tsf_in_book_cost_ty,0.0),          
                           nvl(LP_c_daily_data_tbl(j).intercompany_in_cost_ly,0.0),      
                           nvl(LP_c_daily_data_tbl(j).intercompany_in_cost_ty,0.0),      
                           nvl(LP_c_daily_data_tbl(j).reclass_in_cost_ly,0.0),           
                           nvl(LP_c_daily_data_tbl(j).reclass_in_cost_ty,0.0),           
                           nvl(LP_c_daily_data_tbl(j).franchise_returns_cost_ly,0.0),    
                           nvl(LP_c_daily_data_tbl(j).franchise_returns_cost_ty,0.0),    
                           nvl(LP_c_daily_data_tbl(j).up_chrg_amt_profit_ly,0.0),        
                           nvl(LP_c_daily_data_tbl(j).up_chrg_amt_profit_ty,0.0),        
                           nvl(LP_c_daily_data_tbl(j).up_chrg_amt_exp_ly,0.0),           
                           nvl(LP_c_daily_data_tbl(j).up_chrg_amt_exp_ty,0.0),           
                           nvl(LP_c_daily_data_tbl(j).wo_activity_upd_inv_ly,0.0),       
                           nvl(LP_c_daily_data_tbl(j).wo_activity_upd_inv_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).wo_activity_post_fin_ly,0.0),      
                           nvl(LP_c_daily_data_tbl(j).wo_activity_post_fin_ty,0.0),     
                           nvl(LP_c_daily_data_tbl(j).recoverable_tax_ly,0.0),           
                           nvl(LP_c_daily_data_tbl(j).recoverable_tax_ty,0.0),           
                           nvl(LP_c_daily_data_tbl(j).rtv_cost_ly,0.0),                  
                           nvl(LP_c_daily_data_tbl(j).rtv_cost_ty,0.0),                  
                           nvl(LP_c_daily_data_tbl(j).tsf_out_cost_ly,0.0),              
                           nvl(LP_c_daily_data_tbl(j).tsf_out_cost_ty,0.0),              
                           nvl(LP_c_daily_data_tbl(j).tsf_out_book_cost_ly,0.0),         
                           nvl(LP_c_daily_data_tbl(j).tsf_out_book_cost_ty,0.0),         
                           nvl(LP_c_daily_data_tbl(j).intercompany_out_cost_ly,0.0),     
                           nvl(LP_c_daily_data_tbl(j).intercompany_out_cost_ty,0.0),     
                           nvl(LP_c_daily_data_tbl(j).reclass_out_cost_ly,0.0),          
                           nvl(LP_c_daily_data_tbl(j).reclass_out_cost_ty,0.0),          
                           nvl(LP_c_daily_data_tbl(j).net_sales_cost_ly,0.0),            
                           nvl(LP_c_daily_data_tbl(j).net_sales_cost_ty,0.0),            
                           nvl(LP_c_daily_data_tbl(j).returns_cost_ly,0.0),              
                           nvl(LP_c_daily_data_tbl(j).returns_cost_ty,0.0),              
                           nvl(LP_c_daily_data_tbl(j).franchise_sales_cost_ly,0.0),      
                           nvl(LP_c_daily_data_tbl(j).franchise_sales_cost_ty,0.0),      
                           nvl(LP_c_daily_data_tbl(j).freight_claim_cost_ly,0.0),        
                           nvl(LP_c_daily_data_tbl(j).freight_claim_cost_ty,0.0),        
                           nvl(LP_c_daily_data_tbl(j).stock_adj_cogs_cost_ly,0.0),       
                           nvl(LP_c_daily_data_tbl(j).stock_adj_cogs_cost_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).stock_adj_cost_ly,0.0),            
                           nvl(LP_c_daily_data_tbl(j).stock_adj_cost_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).cost_variance_amt_ly,0.0),         
                           nvl(LP_c_daily_data_tbl(j).cost_variance_amt_ty,0.0),         
                           nvl(LP_c_daily_data_tbl(j).margin_cost_variance_ly,0.0),      
                           nvl(LP_c_daily_data_tbl(j).margin_cost_variance_ty,0.0),      
                           nvl(LP_c_daily_data_tbl(j).vat_in_ly,0.0),                    
                           nvl(LP_c_daily_data_tbl(j).vat_in_ty,0.0),                    
                           nvl(LP_c_daily_data_tbl(j).vat_out_ly,0.0),                   
                           nvl(LP_c_daily_data_tbl(j).vat_out_ty,0.0), 
                           nvl(LP_c_daily_data_tbl(j).deal_income_sales_ly,0.0),         
                           nvl(LP_c_daily_data_tbl(j).deal_income_sales_ty,0.0),         
                           nvl(LP_c_daily_data_tbl(j).deal_income_purch_ly,0.0),         
                           nvl(LP_c_daily_data_tbl(j).deal_income_purch_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).restocking_fee_ly,0.0),            
                           nvl(LP_c_daily_data_tbl(j).restocking_fee_ty,0.0),  
                           nvl(LP_c_daily_data_tbl(j).franchise_restocking_fee_ly,0.0),  
                           nvl(LP_c_daily_data_tbl(j).franchise_restocking_fee_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).net_sales_non_inv_cost_ly,0.0),    
                           nvl(LP_c_daily_data_tbl(j).net_sales_non_inv_cost_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).add_cost_ly,0.0),                  
                           nvl(LP_c_daily_data_tbl(j).add_cost_ty,0.0),                  
                           nvl(LP_c_daily_data_tbl(j).add_retail_ly,0.0),                
                           nvl(LP_c_daily_data_tbl(j).add_retail_ty,0.0),                
                           nvl(LP_c_daily_data_tbl(j).cash_disc_amt_ly,0.0),             
                           nvl(LP_c_daily_data_tbl(j).cash_disc_amt_ty,0.0),             
                           nvl(LP_c_daily_data_tbl(j).clear_markdown_retail_ly,0.0),     
                           nvl(LP_c_daily_data_tbl(j).clear_markdown_retail_ty,0.0),     
                           nvl(LP_c_daily_data_tbl(j).empl_disc_retail_ly,0.0),          
                           nvl(LP_c_daily_data_tbl(j).empl_disc_retail_ty,0.0),          
                           nvl(LP_c_daily_data_tbl(j).franchise_markdown_retail_ly,0.0), 
                           nvl(LP_c_daily_data_tbl(j).franchise_markdown_retail_ty,0.0), 
                           nvl(LP_c_daily_data_tbl(j).franchise_markup_retail_ly,0.0),   
                           nvl(LP_c_daily_data_tbl(j).franchise_markup_retail_ty,0.0),   
                           nvl(LP_c_daily_data_tbl(j).freight_cost_ly,0.0),              
                           nvl(LP_c_daily_data_tbl(j).freight_cost_ty,0.0),              
                           nvl(LP_c_daily_data_tbl(j).intercompany_markdown_ly,0.0),     
                           nvl(LP_c_daily_data_tbl(j).intercompany_markdown_ty,0.0),     
                           nvl(LP_c_daily_data_tbl(j).intercompany_markup_ly,0.0),       
                           nvl(LP_c_daily_data_tbl(j).intercompany_markup_ty,0.0),       
                           nvl(LP_c_daily_data_tbl(j).net_markdown_ly,0.0),       
                           nvl(LP_c_daily_data_tbl(j).net_markdown_ty,0.0), 
                           nvl(LP_c_daily_data_tbl(j).markdown_can_retail_ly,0.0),       
                           nvl(LP_c_daily_data_tbl(j).markdown_can_retail_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).markup_can_retail_ly,0.0),         
                           nvl(LP_c_daily_data_tbl(j).markup_can_retail_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).markup_retail_ly,0.0),             
                           nvl(LP_c_daily_data_tbl(j).markup_retail_ty,0.0), 
                           nvl(LP_c_daily_data_tbl(j).perm_markdown_retail_ly,0.0),      
                           nvl(LP_c_daily_data_tbl(j).perm_markdown_retail_ty,0.0),      
                           nvl(LP_c_daily_data_tbl(j).prom_markdown_retail_ly,0.0),      
                           nvl(LP_c_daily_data_tbl(j).prom_markdown_retail_ty,0.0), 
                           nvl(LP_c_daily_data_tbl(j).rec_cost_adj_variance_ly,0.0),     
                           nvl(LP_c_daily_data_tbl(j).rec_cost_adj_variance_ty,0.0),
                           nvl(LP_c_daily_data_tbl(j).sales_units_ly,0.0),               
                           nvl(LP_c_daily_data_tbl(j).sales_units_ty,0.0),      
                           nvl(LP_c_daily_data_tbl(j).workroom_amt_ly,0.0),              
                           nvl(LP_c_daily_data_tbl(j).workroom_amt_ty,0.0),              
                           nvl(LP_c_daily_data_tbl(j).net_reclass_cost_ly,0.0),          
                           nvl(LP_c_daily_data_tbl(j).net_reclass_cost_ty,0.0),          
                           nvl(LP_c_daily_data_tbl(j).net_tsf_cost_ly,0.0),              
                           nvl(LP_c_daily_data_tbl(j).net_tsf_cost_ty,0.0),        
                           nvl(LP_c_daily_data_tbl(j).net_markup_ly,0.0),                
                           nvl(LP_c_daily_data_tbl(j).net_markup_ty,0.0)))
                      from dual;
END POPULATE_C_DAILY_DATA;
PROCEDURE POPULATE_R_MONTH_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_r_month_data_tbl.FIRST..LP_r_month_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = RMD_sheet)
   select s9t_row(s9t_cells(
                           LP_r_month_data_tbl(j).dept,
                           LP_r_month_data_tbl(j).dept_name,
                           LP_r_month_data_tbl(j).class,
                           LP_r_month_data_tbl(j).class_name,
                           LP_r_month_data_tbl(j).subclass,
                           LP_r_month_data_tbl(j).subclass_name,
                           LP_r_month_data_tbl(j).loc_type,
                           LP_r_month_data_tbl(j).location,
                           LP_r_month_data_tbl(j).loc_name,
                           LP_r_month_data_tbl(j).currency_code,
                           LP_r_month_data_tbl(j).half_no,
                           LP_r_month_data_tbl(j).month_no,
                           nvl(LP_r_month_data_tbl(j).opn_stk_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).opn_stk_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).purch_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).purch_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).tsf_in_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).tsf_in_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).tsf_in_book_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).tsf_in_book_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_in_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_in_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).reclass_in_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).reclass_in_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_returns_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_returns_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).markup_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).markup_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_markup_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_markup_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_markup_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_markup_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).markup_can_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).markup_can_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).rtv_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).rtv_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).tsf_out_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).tsf_out_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).tsf_out_book_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).tsf_out_book_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_out_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_out_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).reclass_out_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).reclass_out_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_sales_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_sales_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).returns_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).returns_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).weight_variance_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).weight_variance_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).empl_disc_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).empl_disc_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_sales_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_sales_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).freight_claim_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).freight_claim_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).stock_adj_cogs_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).stock_adj_cogs_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).stock_adj_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).stock_adj_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).shrinkage_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).shrinkage_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).perm_markdown_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).perm_markdown_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).prom_markdown_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).prom_markdown_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).clear_markdown_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).clear_markdown_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_markdown_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_markdown_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_markdown_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_markdown_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).markdown_can_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).markdown_can_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).cls_stk_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).cls_stk_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).add_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).add_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).red_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).red_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).add_cost_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).add_cost_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).htd_gafs_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).htd_gafs_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).htd_gafs_cost_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).htd_gafs_cost_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).cum_markon_pct_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).cum_markon_pct_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).gross_margin_amt_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).gross_margin_amt_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).vat_in_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).vat_in_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).vat_out_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).vat_out_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).up_chrg_amt_profit_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).up_chrg_amt_profit_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).up_chrg_amt_exp_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).up_chrg_amt_exp_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).wo_activity_upd_inv_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).wo_activity_upd_inv_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).wo_activity_post_fin_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).wo_activity_post_fin_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).freight_cost_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).freight_cost_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).workroom_amt_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).workroom_amt_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).cash_disc_amt_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).cash_disc_amt_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).cost_variance_amt_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).cost_variance_amt_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).rec_cost_adj_variance_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).rec_cost_adj_variance_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).retail_cost_variance_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).retail_cost_variance_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).deal_income_purch_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).deal_income_purch_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).deal_income_sales_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).deal_income_sales_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_restocking_fee_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).franchise_restocking_fee_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_margin_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).intercompany_margin_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).margin_cost_variance_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).margin_cost_variance_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_markdown_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_markdown_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_markup_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_markup_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_reclass_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_reclass_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_sales_retail_ex_vat_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_sales_retail_ex_vat_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_sale_noninv_r_exvat_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_sale_noninv_r_exvat_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_tsf_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_tsf_retail_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_tsf_cost_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_tsf_cost_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).recoverable_tax_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).recoverable_tax_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).restocking_fee_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).restocking_fee_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).shrink_retail_pct_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).shrink_retail_pct_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).sales_units_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).sales_units_ty,0.0),
                           nvl(LP_r_month_data_tbl(j).net_sales_non_inv_retail_ly,0.0),
                           nvl(LP_r_month_data_tbl(j).net_sales_non_inv_retail_ty,0.0)))
                      from dual;
END POPULATE_R_MONTH_DATA;
PROCEDURE POPULATE_R_WEEK_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_r_week_data_tbl.FIRST..LP_r_week_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = RWD_sheet)
   select s9t_row(s9t_cells(
                           LP_r_week_data_tbl(j).dept,
                           LP_r_week_data_tbl(j).dept_name,
                           LP_r_week_data_tbl(j).class,
                           LP_r_week_data_tbl(j).class_name,
                           LP_r_week_data_tbl(j).subclass,
                           LP_r_week_data_tbl(j).subclass_name,
                           LP_r_week_data_tbl(j).loc_type,
                           LP_r_week_data_tbl(j).location,
                           LP_r_week_data_tbl(j).loc_name,
                           LP_r_week_data_tbl(j).currency_code,
                           LP_r_week_data_tbl(j).eom_date,
                           LP_r_week_data_tbl(j).week_no,
                           nvl(LP_r_week_data_tbl(j).opn_stk_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).opn_stk_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).purch_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).purch_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).tsf_in_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).tsf_in_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).tsf_in_book_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).tsf_in_book_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_in_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_in_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).reclass_in_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).reclass_in_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_returns_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_returns_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).markup_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).markup_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_markup_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_markup_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_markup_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_markup_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).markup_can_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).markup_can_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).rtv_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).rtv_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).tsf_out_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).tsf_out_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).tsf_out_book_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).tsf_out_book_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_out_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_out_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).reclass_out_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).reclass_out_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_sales_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_sales_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).returns_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).returns_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).weight_variance_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).weight_variance_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).empl_disc_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).empl_disc_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_sales_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_sales_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).freight_claim_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).freight_claim_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).stock_adj_cogs_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).stock_adj_cogs_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).stock_adj_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).stock_adj_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).shrinkage_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).shrinkage_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).perm_markdown_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).perm_markdown_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).prom_markdown_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).prom_markdown_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).clear_markdown_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).clear_markdown_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_markdown_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_markdown_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_markdown_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_markdown_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).markdown_can_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).markdown_can_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).cls_stk_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).cls_stk_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).add_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).add_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).red_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).red_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).add_cost_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).add_cost_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).htd_gafs_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).htd_gafs_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).htd_gafs_cost_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).htd_gafs_cost_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).cum_markon_pct_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).cum_markon_pct_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).gross_margin_amt_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).gross_margin_amt_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).vat_in_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).vat_in_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).vat_out_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).vat_out_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).up_chrg_amt_profit_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).up_chrg_amt_profit_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).up_chrg_amt_exp_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).up_chrg_amt_exp_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).wo_activity_upd_inv_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).wo_activity_upd_inv_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).wo_activity_post_fin_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).wo_activity_post_fin_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).freight_cost_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).freight_cost_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).workroom_amt_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).workroom_amt_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).cash_disc_amt_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).cash_disc_amt_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).cost_variance_amt_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).cost_variance_amt_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).rec_cost_adj_variance_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).rec_cost_adj_variance_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).retail_cost_variance_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).retail_cost_variance_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).deal_income_purch_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).deal_income_purch_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).deal_income_sales_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).deal_income_sales_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_restocking_fee_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).franchise_restocking_fee_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_margin_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).intercompany_margin_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).margin_cost_variance_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).margin_cost_variance_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_markdown_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_markdown_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_markup_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_markup_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_reclass_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_reclass_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_sales_retail_ex_vat_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_sales_retail_ex_vat_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_sale_noninv_r_exvat_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_sale_noninv_r_exvat_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_tsf_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_tsf_retail_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_tsf_cost_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_tsf_cost_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).recoverable_tax_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).recoverable_tax_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).restocking_fee_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).restocking_fee_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).shrink_retail_pct_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).shrink_retail_pct_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).sales_units_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).sales_units_ty,0.0),
                           nvl(LP_r_week_data_tbl(j).net_sales_non_inv_retail_ly,0.0),
                           nvl(LP_r_week_data_tbl(j).net_sales_non_inv_retail_ty,0.0)))
                      from dual;
END POPULATE_R_WEEK_DATA;
PROCEDURE POPULATE_R_DAILY_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_r_daily_data_tbl.FIRST..LP_r_daily_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = RDD_sheet)
   select s9t_row(s9t_cells(
                           LP_r_daily_data_tbl(j).dept,
                           LP_r_daily_data_tbl(j).dept_name,
                           LP_r_daily_data_tbl(j).class,
                           LP_r_daily_data_tbl(j).class_name,
                           LP_r_daily_data_tbl(j).subclass,
                           LP_r_daily_data_tbl(j).subclass_name,
                           LP_r_daily_data_tbl(j).loc_type,
                           LP_r_daily_data_tbl(j).location,
                           LP_r_daily_data_tbl(j).loc_name,
                           LP_r_daily_data_tbl(j).currency_code,
                           LP_r_daily_data_tbl(j).eow_date,
                           LP_r_daily_data_tbl(j).day_no,
                           nvl(LP_r_daily_data_tbl(j).purch_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).purch_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).tsf_in_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).tsf_in_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).tsf_in_book_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).tsf_in_book_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).intercompany_in_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).intercompany_in_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).reclass_in_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).reclass_in_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_returns_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_returns_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).markup_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).markup_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_markup_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_markup_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).intercompany_markup_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).intercompany_markup_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).markup_can_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).markup_can_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).rtv_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).rtv_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).tsf_out_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).tsf_out_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).tsf_out_book_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).tsf_out_book_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).intercompany_out_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).intercompany_out_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).reclass_out_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).reclass_out_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_sales_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_sales_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).returns_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).returns_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).weight_variance_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).weight_variance_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).empl_disc_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).empl_disc_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_sales_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_sales_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).freight_claim_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).freight_claim_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).stock_adj_cogs_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).stock_adj_cogs_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).stock_adj_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).stock_adj_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).perm_markdown_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).perm_markdown_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).prom_markdown_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).prom_markdown_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).clear_markdown_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).clear_markdown_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).intercompany_markdown_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).intercompany_markdown_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_markdown_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_markdown_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).markdown_can_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).markdown_can_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).vat_in_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).vat_in_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).vat_out_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).vat_out_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).up_chrg_amt_profit_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).up_chrg_amt_profit_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).up_chrg_amt_exp_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).up_chrg_amt_exp_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).wo_activity_upd_inv_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).wo_activity_upd_inv_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).wo_activity_post_fin_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).wo_activity_post_fin_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).freight_cost_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).freight_cost_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).workroom_amt_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).workroom_amt_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).cash_disc_amt_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).cash_disc_amt_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).cost_variance_amt_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).cost_variance_amt_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).retail_cost_variance_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).retail_cost_variance_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).rec_cost_adj_variance_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).rec_cost_adj_variance_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).add_cost_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).add_cost_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).add_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).add_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).deal_income_purch_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).deal_income_purch_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).deal_income_sales_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).deal_income_sales_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).restocking_fee_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).restocking_fee_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_restocking_fee_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).franchise_restocking_fee_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).margin_cost_variance_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).margin_cost_variance_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_markdown_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_markdown_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_markup_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_markup_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_reclass_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_reclass_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_sales_retail_ex_vat_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_sales_retail_ex_vat_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_sale_noninv_r_exvat_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_sale_noninv_r_exvat_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_sales_non_inv_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_sales_non_inv_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_tsf_retail_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).net_tsf_retail_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).recoverable_tax_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).recoverable_tax_ty,0.0),
                           nvl(LP_r_daily_data_tbl(j).sales_units_ly,0.0),
                           nvl(LP_r_daily_data_tbl(j).sales_units_ty,0.0)))
                      from dual;
END POPULATE_R_DAILY_DATA;
PROCEDURE POPULATE_CR_MONTH_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_cr_month_data_tbl.FIRST..LP_cr_month_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = CRM_sheet)
   select s9t_row(s9t_cells(
                           LP_cr_month_data_tbl(j).dept,
                           LP_cr_month_data_tbl(j).dept_name,
                           LP_cr_month_data_tbl(j).class,
                           LP_cr_month_data_tbl(j).class_name,
                           LP_cr_month_data_tbl(j).subclass,
                           LP_cr_month_data_tbl(j).subclass_name,
                           LP_cr_month_data_tbl(j).loc_type,
                           LP_cr_month_data_tbl(j).location,
                           LP_cr_month_data_tbl(j).loc_name,
                           LP_cr_month_data_tbl(j).currency_code,
                           LP_cr_month_data_tbl(j).half_no,
                           LP_cr_month_data_tbl(j).month_no,
                           nvl(LP_cr_month_data_tbl(j).opn_stk_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).opn_stk_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).opn_stk_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).opn_stk_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).purch_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).purch_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).purch_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).purch_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_in_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_in_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_in_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_in_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_in_book_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_in_book_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_in_book_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_in_book_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_in_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_in_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_in_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_in_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).reclass_in_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).reclass_in_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).reclass_in_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).reclass_in_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_returns_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_returns_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_returns_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_returns_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).up_chrg_amt_profit_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).up_chrg_amt_profit_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).up_chrg_amt_exp_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).up_chrg_amt_exp_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).wo_activity_upd_inv_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).wo_activity_upd_inv_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).wo_activity_post_fin_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).wo_activity_post_fin_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).recoverable_tax_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).recoverable_tax_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).markup_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).markup_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_markup_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_markup_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_markup_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_markup_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).markup_can_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).markup_can_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).rtv_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).rtv_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).rtv_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).rtv_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_out_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_out_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_out_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_out_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_out_book_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_out_book_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_out_book_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).tsf_out_book_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_out_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_out_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_out_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_out_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).reclass_out_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).reclass_out_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).reclass_out_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).reclass_out_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).returns_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).returns_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).returns_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).returns_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).weight_variance_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).weight_variance_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).empl_disc_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).empl_disc_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_sales_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_sales_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_sales_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_sales_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).freight_claim_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).freight_claim_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).freight_claim_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).freight_claim_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).stock_adj_cogs_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).stock_adj_cogs_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).stock_adj_cogs_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).stock_adj_cogs_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).stock_adj_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).stock_adj_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).stock_adj_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).stock_adj_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).shrinkage_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).shrinkage_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).shrinkage_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).shrinkage_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).cost_variance_amt_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).cost_variance_amt_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).margin_cost_variance_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).margin_cost_variance_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).perm_markdown_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).perm_markdown_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).prom_markdown_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).prom_markdown_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).clear_markdown_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).clear_markdown_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_markdown_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_markdown_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_markdown_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_markdown_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).markdown_can_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).markdown_can_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).cls_stk_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).cls_stk_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).cls_stk_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).cls_stk_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).add_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).add_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).add_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).add_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).shrink_cost_pct_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).shrink_cost_pct_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).htd_gafs_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).htd_gafs_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).htd_gafs_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).htd_gafs_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).red_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).red_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).deal_income_sales_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).deal_income_sales_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).deal_income_purch_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).deal_income_purch_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).cum_markon_pct_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).cum_markon_pct_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).gross_margin_amt_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).gross_margin_amt_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).vat_in_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).vat_in_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).vat_out_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).vat_out_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).restocking_fee_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).restocking_fee_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).freight_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).freight_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).rec_cost_adj_variance_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).rec_cost_adj_variance_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).workroom_amt_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).workroom_amt_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).cash_disc_amt_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).cash_disc_amt_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).retail_cost_variance_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).retail_cost_variance_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_restocking_fee_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).franchise_restocking_fee_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_margin_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).intercompany_margin_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_markdown_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_markdown_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_markup_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_markup_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_reclass_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_reclass_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_reclass_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_reclass_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_retail_ex_vat_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_retail_ex_vat_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sale_noninv_r_exvat_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sale_noninv_r_exvat_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_tsf_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_tsf_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_tsf_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_tsf_retail_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).shrink_retail_pct_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).shrink_retail_pct_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).sales_units_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).sales_units_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_non_inv_cost_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_non_inv_cost_ty,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_noninv_retail_ly,0.0),
                           nvl(LP_cr_month_data_tbl(j).net_sales_noninv_retail_ty,0.0)))
                      from dual;
END POPULATE_CR_MONTH_DATA;
PROCEDURE POPULATE_CR_WEEK_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_cr_week_data_tbl.FIRST..LP_cr_week_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = CRW_sheet)
   select s9t_row(s9t_cells(
                           LP_cr_week_data_tbl(j).dept,
                           LP_cr_week_data_tbl(j).dept_name,
                           LP_cr_week_data_tbl(j).class,
                           LP_cr_week_data_tbl(j).class_name,
                           LP_cr_week_data_tbl(j).subclass,
                           LP_cr_week_data_tbl(j).subclass_name,
                           LP_cr_week_data_tbl(j).loc_type,
                           LP_cr_week_data_tbl(j).location,
                           LP_cr_week_data_tbl(j).loc_name,
                           LP_cr_week_data_tbl(j).currency_code,
                           LP_cr_week_data_tbl(j).eom_date,
                           LP_cr_week_data_tbl(j).week_no,
                           nvl(LP_cr_week_data_tbl(j).opn_stk_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).opn_stk_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).opn_stk_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).opn_stk_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).purch_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).purch_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).purch_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).purch_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_in_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_in_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_in_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_in_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_in_book_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_in_book_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_in_book_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_in_book_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_in_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_in_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_in_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_in_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).reclass_in_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).reclass_in_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).reclass_in_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).reclass_in_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_returns_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_returns_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_returns_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_returns_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).up_chrg_amt_profit_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).up_chrg_amt_profit_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).up_chrg_amt_exp_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).up_chrg_amt_exp_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).wo_activity_upd_inv_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).wo_activity_upd_inv_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).wo_activity_post_fin_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).wo_activity_post_fin_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).recoverable_tax_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).recoverable_tax_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).markup_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).markup_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_markup_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_markup_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_markup_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_markup_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).markup_can_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).markup_can_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).rtv_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).rtv_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).rtv_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).rtv_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_out_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_out_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_out_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_out_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_out_book_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_out_book_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_out_book_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).tsf_out_book_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_out_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_out_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_out_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_out_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).reclass_out_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).reclass_out_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).reclass_out_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).reclass_out_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).returns_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).returns_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).returns_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).returns_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).weight_variance_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).weight_variance_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).empl_disc_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).empl_disc_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_sales_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_sales_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_sales_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_sales_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).freight_claim_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).freight_claim_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).freight_claim_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).freight_claim_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).stock_adj_cogs_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).stock_adj_cogs_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).stock_adj_cogs_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).stock_adj_cogs_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).stock_adj_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).stock_adj_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).stock_adj_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).stock_adj_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).shrinkage_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).shrinkage_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).shrinkage_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).shrinkage_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).cost_variance_amt_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).cost_variance_amt_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).margin_cost_variance_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).margin_cost_variance_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).perm_markdown_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).perm_markdown_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).prom_markdown_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).prom_markdown_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).clear_markdown_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).clear_markdown_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_markdown_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_markdown_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_markdown_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_markdown_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).markdown_can_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).markdown_can_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).cls_stk_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).cls_stk_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).cls_stk_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).cls_stk_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).add_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).add_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).add_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).add_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).shrink_cost_pct_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).shrink_cost_pct_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).htd_gafs_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).htd_gafs_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).htd_gafs_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).htd_gafs_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).red_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).red_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).deal_income_sales_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).deal_income_sales_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).deal_income_purch_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).deal_income_purch_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).cum_markon_pct_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).cum_markon_pct_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).gross_margin_amt_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).gross_margin_amt_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).vat_in_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).vat_in_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).vat_out_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).vat_out_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).restocking_fee_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).restocking_fee_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).freight_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).freight_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).rec_cost_adj_variance_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).rec_cost_adj_variance_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).workroom_amt_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).workroom_amt_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).cash_disc_amt_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).cash_disc_amt_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).retail_cost_variance_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).retail_cost_variance_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_restocking_fee_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).franchise_restocking_fee_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_margin_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).intercompany_margin_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_markdown_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_markdown_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_markup_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_markup_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_reclass_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_reclass_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_reclass_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_reclass_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_retail_ex_vat_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_retail_ex_vat_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sale_noninv_r_exvat_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sale_noninv_r_exvat_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_tsf_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_tsf_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_tsf_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_tsf_retail_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).shrink_retail_pct_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).shrink_retail_pct_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).sales_units_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).sales_units_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_non_inv_cost_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_non_inv_cost_ty,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_noninv_retail_ly,0.0),
                           nvl(LP_cr_week_data_tbl(j).net_sales_noninv_retail_ty,0.0)))
                      from dual;
END POPULATE_CR_WEEK_DATA;
PROCEDURE POPULATE_CR_DAILY_DATA( I_file_id   IN   NUMBER ) IS
BEGIN
   FORALL j IN LP_cr_daily_data_tbl.FIRST..LP_cr_daily_data_tbl.LAST
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = CRD_sheet)
   select s9t_row(s9t_cells(
                           LP_cr_daily_data_tbl(j).dept,
                           LP_cr_daily_data_tbl(j).dept_name,
                           LP_cr_daily_data_tbl(j).class,
                           LP_cr_daily_data_tbl(j).class_name,
                           LP_cr_daily_data_tbl(j).subclass,
                           LP_cr_daily_data_tbl(j).subclass_name,
                           LP_cr_daily_data_tbl(j).loc_type,
                           LP_cr_daily_data_tbl(j).location,
                           LP_cr_daily_data_tbl(j).loc_name,
                           LP_cr_daily_data_tbl(j).currency_code,
                           LP_cr_daily_data_tbl(j).eow_date,
                           LP_cr_daily_data_tbl(j).day_no,
                           nvl(LP_cr_daily_data_tbl(j).purch_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).purch_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).purch_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).purch_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_in_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_in_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_in_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_in_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_in_book_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_in_book_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_in_book_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_in_book_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_in_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_in_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_in_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_in_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).reclass_in_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).reclass_in_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).reclass_in_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).reclass_in_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_returns_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_returns_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_returns_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_returns_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).up_chrg_amt_profit_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).up_chrg_amt_profit_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).up_chrg_amt_exp_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).up_chrg_amt_exp_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).wo_activity_upd_inv_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).wo_activity_upd_inv_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).wo_activity_post_fin_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).wo_activity_post_fin_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).recoverable_tax_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).recoverable_tax_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).rtv_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).rtv_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).rtv_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).rtv_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_out_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_out_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_out_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_out_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_out_book_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_out_book_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_out_book_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).tsf_out_book_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_out_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_out_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_out_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_out_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).reclass_out_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).reclass_out_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).reclass_out_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).reclass_out_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).returns_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).returns_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).returns_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).returns_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_sales_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_sales_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_sales_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_sales_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).weight_variance_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).weight_variance_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).freight_claim_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).freight_claim_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).freight_claim_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).freight_claim_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).stock_adj_cogs_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).stock_adj_cogs_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).stock_adj_cogs_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).stock_adj_cogs_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).stock_adj_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).stock_adj_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).stock_adj_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).stock_adj_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).cost_variance_amt_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).cost_variance_amt_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).retail_cost_variance_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).retail_cost_variance_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).margin_cost_variance_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).margin_cost_variance_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).vat_in_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).vat_in_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).vat_out_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).vat_out_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).deal_income_sales_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).deal_income_sales_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).deal_income_purch_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).deal_income_purch_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).restocking_fee_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).restocking_fee_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_restocking_fee_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_restocking_fee_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_non_inv_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_non_inv_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).add_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).add_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).add_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).add_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).cash_disc_amt_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).cash_disc_amt_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).clear_markdown_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).clear_markdown_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).empl_disc_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).empl_disc_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_markdown_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_markdown_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_markup_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).franchise_markup_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).freight_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).freight_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_markdown_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_markdown_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_markup_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).intercompany_markup_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_markdown_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_markdown_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).markdown_can_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).markdown_can_retail_ty,0.0), 
                           nvl(LP_cr_daily_data_tbl(j).markup_can_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).markup_can_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).markup_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).markup_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).perm_markdown_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).perm_markdown_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).prom_markdown_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).prom_markdown_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).rec_cost_adj_variance_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).rec_cost_adj_variance_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).sales_units_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).sales_units_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).workroom_amt_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).workroom_amt_ty,0.0), 
                           nvl(LP_cr_daily_data_tbl(j).net_reclass_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_reclass_cost_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_reclass_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_reclass_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_tsf_cost_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_tsf_cost_ty,0.0), 
                           nvl(LP_cr_daily_data_tbl(j).net_markup_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_markup_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_retail_ex_vat_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_retail_ex_vat_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sale_noninv_r_exvat_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sale_noninv_r_exvat_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_noninv_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_sales_noninv_retail_ty,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_tsf_retail_ly,0.0),
                           nvl(LP_cr_daily_data_tbl(j).net_tsf_retail_ty,0.0)))
                      from dual;
END POPULATE_CR_DAILY_DATA;
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
 
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   
   if l_file.user_lang IS NULL then
      L_file.user_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   end if;
   
   L_file.add_sheet(CMD_sheet);
   L_file.sheets(l_file.get_sheet_index(CMD_sheet)).column_headers := s9t_cells('DEPT',
                                                                                'DEPT_NAME',
                                                                                'CLASS',
                                                                                'CLASS_NAME',
                                                                                'SUBCLASS',
                                                                                'SUBCLASS_NAME',
                                                                                'LOC_TYPE',
                                                                                'LOCATION',
                                                                                'LOC_NAME',
                                                                                'CURRENCY',
                                                                                'HALF_NO',
                                                                                'MONTH_NO',
                                                                                'OPN_STK_COST_LY',
                                                                                'OPN_STK_COST_TY',
                                                                                'PURCH_COST_LY',
                                                                                'PURCH_COST_TY',
                                                                                'TSF_IN_COST_LY',
                                                                                'TSF_IN_COST_TY',
                                                                                'TSF_IN_BOOK_COST_LY',
                                                                                'TSF_IN_BOOK_COST_TY',
                                                                                'INTERCOMPANY_IN_COST_LY',
                                                                                'INTERCOMPANY_IN_COST_TY',
                                                                                'RECLASS_IN_COST_LY',
                                                                                'RECLASS_IN_COST_TY',
                                                                                'FRANCHISE_RETURNS_COST_LY',
                                                                                'FRANCHISE_RETURNS_COST_TY',
                                                                                'UP_CHRG_AMT_PROFIT_LY',
                                                                                'UP_CHRG_AMT_PROFIT_TY',
                                                                                'UP_CHRG_AMT_EXP_LY',
                                                                                'UP_CHRG_AMT_EXP_TY',
                                                                                'WO_ACTIVITY_UPD_INV_LY',
                                                                                'WO_ACTIVITY_UPD_INV_TY',
                                                                                'RECOVERABLE_TAX_LY',
                                                                                'RECOVERABLE_TAX_TY',
                                                                                'RTV_COST_LY',
                                                                                'RTV_COST_TY',
                                                                                'TSF_OUT_COST_LY',
                                                                                'TSF_OUT_COST_TY',
                                                                                'TSF_OUT_BOOK_COST_LY',
                                                                                'TSF_OUT_BOOK_COST_TY',
                                                                                'INTERCOMPANY_OUT_COST_LY',
                                                                                'INTERCOMPANY_OUT_COST_TY',
                                                                                'RECLASS_OUT_COST_LY',
                                                                                'RECLASS_OUT_COST_TY',
                                                                                'NET_SALES_COST_LY',
                                                                                'NET_SALES_COST_TY',
                                                                                'RETURNS_COST_LY',
                                                                                'RETURNS_COST_TY',
                                                                                'FRANCHISE_SALES_COST_LY',
                                                                                'FRANCHISE_SALES_COST_TY',
                                                                                'FREIGHT_CLAIM_COST_LY',
                                                                                'FREIGHT_CLAIM_COST_TY',
                                                                                'STOCK_ADJ_COGS_COST_LY',
                                                                                'STOCK_ADJ_COGS_COST_TY',
                                                                                'STOCK_ADJ_COST_LY',
                                                                                'STOCK_ADJ_COST_TY',
                                                                                'SHRINKAGE_COST_LY',
                                                                                'SHRINKAGE_COST_TY',
                                                                                'COST_VARIANCE_AMT_LY',
                                                                                'COST_VARIANCE_AMT_TY',
                                                                                'MARGIN_COST_VARIANCE_LY',
                                                                                'MARGIN_COST_VARIANCE_TY',
                                                                                'CLS_STK_COST_LY',
                                                                                'CLS_STK_COST_TY',
                                                                                'ADD_COST_LY',
                                                                                'ADD_COST_TY',
                                                                                'ADD_RETAIL_LY',
                                                                                'ADD_RETAIL_TY',
                                                                                'SHRINK_COST_PCT_LY',
                                                                                'SHRINK_COST_PCT_TY',
                                                                                'RED_RETAIL_LY',
                                                                                'RED_RETAIL_TY',
                                                                                'DEAL_INCOME_SALES_LY',
                                                                                'DEAL_INCOME_SALES_TY',
                                                                                'DEAL_INCOME_PURCH_LY',
                                                                                'DEAL_INCOME_PURCH_TY',
                                                                                'VAT_IN_LY',
                                                                                'VAT_IN_TY',
                                                                                'VAT_OUT_LY',
                                                                                'VAT_OUT_TY',
                                                                                'WO_ACTIVITY_POST_FIN_LY',
                                                                                'WO_ACTIVITY_POST_FIN_TY',
                                                                                'RESTOCKING_FEE_LY',
                                                                                'RESTOCKING_FEE_TY',
                                                                                'FREIGHT_COST_LY',
                                                                                'FREIGHT_COST_TY',
                                                                                'REC_COST_ADJ_VARIANCE_LY',
                                                                                'REC_COST_ADJ_VARIANCE_TY',
                                                                                'WORKROOM_AMT_LY',
                                                                                'WORKROOM_AMT_TY',
                                                                                'CASH_DISC_AMT_LY',
                                                                                'CASH_DISC_AMT_TY',
                                                                                'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                'EMPL_DISC_RETAIL_LY',
                                                                                'EMPL_DISC_RETAIL_TY',
                                                                                'MARKUP_CAN_RETAIL_LY',
                                                                                'MARKUP_CAN_RETAIL_TY',
                                                                                'MARKUP_RETAIL_LY',
                                                                                'MARKUP_RETAIL_TY',
                                                                                'PERM_MARKDOWN_RETAIL_LY',
                                                                                'PERM_MARKDOWN_RETAIL_TY',
                                                                                'PROM_MARKDOWN_RETAIL_LY',
                                                                                'PROM_MARKDOWN_RETAIL_TY',
                                                                                'FRN_MARKDOWN_RETAIL_LY',
                                                                                'FRN_MARKDOWN_RETAIL_TY',
                                                                                'FRN_MARKUP_RETAIL_LY',
                                                                                'FRN_MARKUP_RETAIL_TY',
                                                                                'FRN_RESTOCKING_FEE_LY',
                                                                                'FRN_RESTOCKING_FEE_TY',
                                                                                'INTERCOMPANY_MARKDOWN_LY',
                                                                                'INTERCOMPANY_MARKDOWN_TY',
                                                                                'INTERCOMPANY_MARKUP_LY',
                                                                                'INTERCOMPANY_MARKUP_TY',
                                                                                'INTERCOMPANY_MARGIN_LY',
                                                                                'INTERCOMPANY_MARGIN_TY',
                                                                                'MARKDOWN_CAN_RETAIL_LY',
                                                                                'MARKDOWN_CAN_RETAIL_TY',
                                                                                'NET_MARKUP_LY',
                                                                                'NET_MARKUP_TY',
                                                                                'NET_RECLASS_COST_LY',
                                                                                'NET_RECLASS_COST_TY',
                                                                                'NET_TSF_COST_LY',
                                                                                'NET_TSF_COST_TY',
                                                                                'SALES_UNITS_LY',
                                                                                'SALES_UNITS_TY',
                                                                                'CUM_MARKON_PCT_LY',
                                                                                'CUM_MARKON_PCT_TY',
                                                                                'GROSS_MARGIN_AMT_LY',
                                                                                'GROSS_MARGIN_AMT_TY',
                                                                                'NET_SALES_NON_INV_COST_LY',
                                                                                'NET_SALES_NON_INV_COST_TY');
   L_file.add_sheet(CWD_sheet);
   L_file.sheets(l_file.get_sheet_index(CWD_sheet)).column_headers := s9t_cells('DEPT',
                                                                                'DEPT_NAME',
                                                                                'CLASS',
                                                                                'CLASS_NAME',
                                                                                'SUBCLASS',
                                                                                'SUBCLASS_NAME',
                                                                                'LOC_TYPE',
                                                                                'LOCATION',
                                                                                'LOC_NAME',
                                                                                'CURRENCY',
                                                                                'EOM_DATE',
                                                                                'WEEK_NO',
                                                                                'OPN_STK_COST_LY',
                                                                                'OPN_STK_COST_TY',
                                                                                'PURCH_COST_LY',
                                                                                'PURCH_COST_TY',
                                                                                'TSF_IN_COST_LY',
                                                                                'TSF_IN_COST_TY',
                                                                                'TSF_IN_BOOK_COST_LY',
                                                                                'TSF_IN_BOOK_COST_TY',
                                                                                'INTERCOMPANY_IN_COST_LY',
                                                                                'INTERCOMPANY_IN_COST_TY',
                                                                                'RECLASS_IN_COST_LY',
                                                                                'RECLASS_IN_COST_TY',
                                                                                'FRANCHISE_RETURNS_COST_LY',
                                                                                'FRANCHISE_RETURNS_COST_TY',
                                                                                'UP_CHRG_AMT_PROFIT_LY',
                                                                                'UP_CHRG_AMT_PROFIT_TY',
                                                                                'UP_CHRG_AMT_EXP_LY',
                                                                                'UP_CHRG_AMT_EXP_TY',
                                                                                'WO_ACTIVITY_UPD_INV_LY',
                                                                                'WO_ACTIVITY_UPD_INV_TY',
                                                                                'RECOVERABLE_TAX_LY',
                                                                                'RECOVERABLE_TAX_TY',
                                                                                'RTV_COST_LY',
                                                                                'RTV_COST_TY',
                                                                                'TSF_OUT_COST_LY',
                                                                                'TSF_OUT_COST_TY',
                                                                                'TSF_OUT_BOOK_COST_LY',
                                                                                'TSF_OUT_BOOK_COST_TY',
                                                                                'INTERCOMPANY_OUT_COST_LY',
                                                                                'INTERCOMPANY_OUT_COST_TY',
                                                                                'RECLASS_OUT_COST_LY',
                                                                                'RECLASS_OUT_COST_TY',
                                                                                'NET_SALES_COST_LY',
                                                                                'NET_SALES_COST_TY',
                                                                                'RETURNS_COST_LY',
                                                                                'RETURNS_COST_TY',
                                                                                'FRANCHISE_SALES_COST_LY',
                                                                                'FRANCHISE_SALES_COST_TY',
                                                                                'FREIGHT_CLAIM_COST_LY',
                                                                                'FREIGHT_CLAIM_COST_TY',
                                                                                'STOCK_ADJ_COGS_COST_LY',
                                                                                'STOCK_ADJ_COGS_COST_TY',
                                                                                'STOCK_ADJ_COST_LY',
                                                                                'STOCK_ADJ_COST_TY',
                                                                                'SHRINKAGE_COST_LY',
                                                                                'SHRINKAGE_COST_TY',
                                                                                'COST_VARIANCE_AMT_LY',
                                                                                'COST_VARIANCE_AMT_TY',
                                                                                'MARGIN_COST_VARIANCE_LY',
                                                                                'MARGIN_COST_VARIANCE_TY',
                                                                                'CLS_STK_COST_LY',
                                                                                'CLS_STK_COST_TY',
                                                                                'ADD_COST_LY',
                                                                                'ADD_COST_TY',
                                                                                'ADD_RETAIL_LY',
                                                                                'ADD_RETAIL_TY',
                                                                                'SHRINK_COST_PCT_LY',
                                                                                'SHRINK_COST_PCT_TY',
                                                                                'RED_RETAIL_LY',
                                                                                'RED_RETAIL_TY',
                                                                                'DEAL_INCOME_SALES_LY',
                                                                                'DEAL_INCOME_SALES_TY',
                                                                                'DEAL_INCOME_PURCH_LY',
                                                                                'DEAL_INCOME_PURCH_TY',
                                                                                'VAT_IN_LY',
                                                                                'VAT_IN_TY',
                                                                                'VAT_OUT_LY',
                                                                                'VAT_OUT_TY',
                                                                                'WO_ACTIVITY_POST_FIN_LY',
                                                                                'WO_ACTIVITY_POST_FIN_TY',
                                                                                'RESTOCKING_FEE_LY',
                                                                                'RESTOCKING_FEE_TY',
                                                                                'FREIGHT_COST_LY',
                                                                                'FREIGHT_COST_TY',
                                                                                'REC_COST_ADJ_VARIANCE_LY',
                                                                                'REC_COST_ADJ_VARIANCE_TY',
                                                                                'WORKROOM_AMT_LY',
                                                                                'WORKROOM_AMT_TY',
                                                                                'CASH_DISC_AMT_LY',
                                                                                'CASH_DISC_AMT_TY',
                                                                                'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                'EMPL_DISC_RETAIL_LY',
                                                                                'EMPL_DISC_RETAIL_TY',
                                                                                'MARKUP_CAN_RETAIL_LY',
                                                                                'MARKUP_CAN_RETAIL_TY',
                                                                                'MARKUP_RETAIL_LY',
                                                                                'MARKUP_RETAIL_TY',
                                                                                'PERM_MARKDOWN_RETAIL_LY',
                                                                                'PERM_MARKDOWN_RETAIL_TY',
                                                                                'PROM_MARKDOWN_RETAIL_LY',
                                                                                'PROM_MARKDOWN_RETAIL_TY',
                                                                                'FRN_MARKDOWN_RETAIL_LY',
                                                                                'FRN_MARKDOWN_RETAIL_TY',
                                                                                'FRN_MARKUP_RETAIL_LY',
                                                                                'FRN_MARKUP_RETAIL_TY',
                                                                                'FRN_RESTOCKING_FEE_LY',
                                                                                'FRN_RESTOCKING_FEE_TY',
                                                                                'INTERCOMPANY_MARKDOWN_LY',
                                                                                'INTERCOMPANY_MARKDOWN_TY',
                                                                                'INTERCOMPANY_MARKUP_LY',
                                                                                'INTERCOMPANY_MARKUP_TY',
                                                                                'INTERCOMPANY_MARGIN_LY',
                                                                                'INTERCOMPANY_MARGIN_TY',
                                                                                'MARKDOWN_CAN_RETAIL_LY',
                                                                                'MARKDOWN_CAN_RETAIL_TY',
                                                                                'NET_MARKUP_LY',
                                                                                'NET_MARKUP_TY',
                                                                                'NET_RECLASS_COST_LY',
                                                                                'NET_RECLASS_COST_TY',
                                                                                'NET_TSF_COST_LY',
                                                                                'NET_TSF_COST_TY',
                                                                                'SALES_UNITS_LY',
                                                                                'SALES_UNITS_TY',
                                                                                'CUM_MARKON_PCT_LY',
                                                                                'CUM_MARKON_PCT_TY',
                                                                                'GROSS_MARGIN_AMT_LY',
                                                                                'GROSS_MARGIN_AMT_TY',
                                                                                'NET_SALES_NON_INV_COST_LY',
                                                                                'NET_SALES_NON_INV_COST_TY');

   L_file.add_sheet(CDD_sheet);
   L_file.sheets(l_file.get_sheet_index(CDD_sheet)).column_headers := s9t_cells( 'DEPT',                        
                                                                                 'DEPT_NAME',
                                                                                 'CLASS',                 
                                                                                 'CLASS_NAME',
                                                                                 'SUBCLASS',
                                                                                 'SUBCLASS_NAME',
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'LOC_NAME',
                                                                                 'CURRENCY',
                                                                                 'EOW_DATE',
                                                                                 'DAY_NO',
                                                                                 'PURCH_COST_LY',
                                                                                 'PURCH_COST_TY',
                                                                                 'TSF_IN_COST_LY',
                                                                                 'TSF_IN_COST_TY',
                                                                                 'TSF_IN_BOOK_COST_LY',
                                                                                 'TSF_IN_BOOK_COST_TY',
                                                                                 'INTERCOMPANY_IN_COST_LY',
                                                                                 'INTERCOMPANY_IN_COST_TY',
                                                                                 'RECLASS_IN_COST_LY',
                                                                                 'RECLASS_IN_COST_TY',
                                                                                 'FRANCHISE_RETURNS_COST_LY',
                                                                                 'FRANCHISE_RETURNS_COST_TY',
                                                                                 'UP_CHRG_AMT_PROFIT_LY',
                                                                                 'UP_CHRG_AMT_PROFIT_TY',
                                                                                 'UP_CHRG_AMT_EXP_LY',
                                                                                 'UP_CHRG_AMT_EXP_TY',
                                                                                 'WO_ACTIVITY_UPD_INV_LY',
                                                                                 'WO_ACTIVITY_UPD_INV_TY',
                                                                                 'WO_ACTIVITY_POST_FIN_LY',
                                                                                 'WO_ACTIVITY_POST_FIN_TY',
                                                                                 'RECOVERABLE_TAX_LY',
                                                                                 'RECOVERABLE_TAX_TY',
                                                                                 'RTV_COST_LY',
                                                                                 'RTV_COST_TY',
                                                                                 'TSF_OUT_COST_LY',
                                                                                 'TSF_OUT_COST_TY',
                                                                                 'TSF_OUT_BOOK_COST_LY',
                                                                                 'TSF_OUT_BOOK_COST_TY',
                                                                                 'INTERCOMPANY_OUT_COST_LY',
                                                                                 'INTERCOMPANY_OUT_COST_TY',
                                                                                 'RECLASS_OUT_COST_LY',
                                                                                 'RECLASS_OUT_COST_TY',
                                                                                 'NET_SALES_COST_LY',
                                                                                 'NET_SALES_COST_TY',
                                                                                 'RETURNS_COST_LY',
                                                                                 'RETURNS_COST_TY',
                                                                                 'FRANCHISE_SALES_COST_LY',
                                                                                 'FRANCHISE_SALES_COST_TY',
                                                                                 'FREIGHT_CLAIM_COST_LY',
                                                                                 'FREIGHT_CLAIM_COST_TY',
                                                                                 'STOCK_ADJ_COGS_COST_LY',
                                                                                 'STOCK_ADJ_COGS_COST_TY',
                                                                                 'STOCK_ADJ_COST_LY',
                                                                                 'STOCK_ADJ_COST_TY',
                                                                                 'COST_VARIANCE_AMT_LY',
                                                                                 'COST_VARIANCE_AMT_TY',
                                                                                 'MARGIN_COST_VARIANCE_LY',
                                                                                 'MARGIN_COST_VARIANCE_TY',
                                                                                 'VAT_IN_LY',
                                                                                 'VAT_IN_TY',
                                                                                 'VAT_OUT_LY',
                                                                                 'VAT_OUT_TY',
                                                                                 'DEAL_INCOME_SALES_LY',
                                                                                 'DEAL_INCOME_SALES_TY',
                                                                                 'DEAL_INCOME_PURCH_LY',
                                                                                 'DEAL_INCOME_PURCH_TY',
                                                                                 'RESTOCKING_FEE_LY',
                                                                                 'RESTOCKING_FEE_TY',
                                                                                 'FRN_RESTOCKING_FEE_LY',
                                                                                 'FRN_RESTOCKING_FEE_TY',
                                                                                 'NET_SALES_NON_INV_COST_LY',
                                                                                 'NET_SALES_NON_INV_COST_TY',
                                                                                 'ADD_COST_LY',
                                                                                 'ADD_COST_TY',
                                                                                 'ADD_RETAIL_LY',
                                                                                 'ADD_RETAIL_TY',
                                                                                 'CASH_DISC_AMT_LY',
                                                                                 'CASH_DISC_AMT_TY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                 'EMPL_DISC_RETAIL_LY',
                                                                                 'EMPL_DISC_RETAIL_TY',
                                                                                 'FRN_MARKDOWN_RETAIL_LY',
                                                                                 'FRN_MARKDOWN_RETAIL_TY',
                                                                                 'FRN_MARKUP_RETAIL_LY',
                                                                                 'FRN_MARKUP_RETAIL_TY',
                                                                                 'FREIGHT_COST_LY',
                                                                                 'FREIGHT_COST_TY',
                                                                                 'INTERCOMPANY_MARKDOWN_LY',
                                                                                 'INTERCOMPANY_MARKDOWN_TY',
                                                                                 'INTERCOMPANY_MARKUP_LY',
                                                                                 'INTERCOMPANY_MARKUP_TY',
                                                                                 'NET_MARKDOWN_LY',
                                                                                 'NET_MARKDOWN_TY',
                                                                                 'MARKDOWN_CAN_RETAIL_LY',
                                                                                 'MARKDOWN_CAN_RETAIL_TY',
                                                                                 'MARKUP_CAN_RETAIL_LY',
                                                                                 'MARKUP_CAN_RETAIL_TY',
                                                                                 'MARKUP_RETAIL_LY',
                                                                                 'MARKUP_RETAIL_TY',
                                                                                 'PERM_MARKDOWN_RETAIL_LY',
                                                                                 'PERM_MARKDOWN_RETAIL_TY',
                                                                                 'PROM_MARKDOWN_RETAIL_LY',
                                                                                 'PROM_MARKDOWN_RETAIL_TY',
                                                                                 'REC_COST_ADJ_VARIANCE_LY',
                                                                                 'REC_COST_ADJ_VARIANCE_TY',
                                                                                 'SALES_UNITS_LY',
                                                                                 'SALES_UNITS_TY',
                                                                                 'WORKROOM_AMT_LY',
                                                                                 'WORKROOM_AMT_TY',
                                                                                 'NET_RECLASS_COST_LY',
                                                                                 'NET_RECLASS_COST_TY',
                                                                                 'NET_TSF_COST_LY',
                                                                                 'NET_TSF_COST_TY',
                                                                                 'NET_MARKUP_LY',
                                                                                 'NET_MARKUP_TY');
   L_file.add_sheet(RMD_sheet);
   L_file.sheets(l_file.get_sheet_index(RMD_sheet)).column_headers := s9t_cells( 'DEPT',
                                                                                 'DEPT_NAME',
                                                                                 'CLASS',
                                                                                 'CLASS_NAME',
                                                                                 'SUBCLASS',
                                                                                 'SUBCLASS_NAME',
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'LOC_NAME',
                                                                                 'CURRENCY',
                                                                                 'HALF_NO',
                                                                                 'MONTH_NO',
                                                                                 'OPN_STK_RETAIL_LY',
                                                                                 'OPN_STK_RETAIL_TY',
                                                                                 'PURCH_RETAIL_LY',
                                                                                 'PURCH_RETAIL_TY',
                                                                                 'TSF_IN_RETAIL_LY',
                                                                                 'TSF_IN_RETAIL_TY',
                                                                                 'TSF_IN_BOOK_RETAIL_LY',
                                                                                 'TSF_IN_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_IN_RETAIL_LY',
                                                                                 'INTERCOMPANY_IN_RETAIL_TY',
                                                                                 'RECLASS_IN_RETAIL_LY',
                                                                                 'RECLASS_IN_RETAIL_TY',
                                                                                 'FRN_RETURNS_RETAIL_LY',
                                                                                 'FRN_RETURNS_RETAIL_TY',
                                                                                 'MARKUP_RETAIL_LY',
                                                                                 'MARKUP_RETAIL_TY',
                                                                                 'FRN_MARKUP_RETAIL_LY',
                                                                                 'FRN_MARKUP_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKUP_LY',
                                                                                 'INTERCOMPANY_MARKUP_TY',
                                                                                 'MARKUP_CAN_RETAIL_LY',
                                                                                 'MARKUP_CAN_RETAIL_TY',
                                                                                 'RTV_RETAIL_LY',
                                                                                 'RTV_RETAIL_TY',
                                                                                 'TSF_OUT_RETAIL_LY',
                                                                                 'TSF_OUT_RETAIL_TY',
                                                                                 'TSF_OUT_BOOK_RETAIL_LY',
                                                                                 'TSF_OUT_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_LY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_TY',
                                                                                 'RECLASS_OUT_RETAIL_LY',
                                                                                 'RECLASS_OUT_RETAIL_TY',
                                                                                 'NET_SALES_RETAIL_LY',
                                                                                 'NET_SALES_RETAIL_TY',
                                                                                 'RETURNS_RETAIL_LY',
                                                                                 'RETURNS_RETAIL_TY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_LY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_TY',
                                                                                 'EMPL_DISC_RETAIL_LY',
                                                                                 'EMPL_DISC_RETAIL_TY',
                                                                                 'FRANCHISE_SALES_RETAIL_LY',
                                                                                 'FRANCHISE_SALES_RETAIL_TY',
                                                                                 'FREIGHT_CLAIM_RETAIL_LY',
                                                                                 'FREIGHT_CLAIM_RETAIL_TY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_LY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_TY',
                                                                                 'STOCK_ADJ_RETAIL_LY',
                                                                                 'STOCK_ADJ_RETAIL_TY',
                                                                                 'SHRINKAGE_RETAIL_LY',
                                                                                 'SHRINKAGE_RETAIL_TY',
                                                                                 'PERM_MARKDOWN_RETAIL_LY',
                                                                                 'PERM_MARKDOWN_RETAIL_TY',
                                                                                 'PROM_MARKDOWN_RETAIL_LY',
                                                                                 'PROM_MARKDOWN_RETAIL_TY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKDOWN_LY',
                                                                                 'INTERCOMPANY_MARKDOWN_TY',
                                                                                 'FRN_MARKDOWN_RETAIL_LY',
                                                                                 'FRN_MARKDOWN_RETAIL_TY',
                                                                                 'MARKDOWN_CAN_RETAIL_LY', 
                                                                                 'MARKDOWN_CAN_RETAIL_TY', 
                                                                                 'CLS_STK_RETAIL_LY',
                                                                                 'CLS_STK_RETAIL_TY',
                                                                                 'ADD_RETAIL_LY',
                                                                                 'ADD_RETAIL_TY',
                                                                                 'RED_RETAIL_LY',
                                                                                 'RED_RETAIL_TY',
                                                                                 'ADD_COST_LY',
                                                                                 'ADD_COST_TY',
                                                                                 'HTD_GAFS_RETAIL_LY',
                                                                                 'HTD_GAFS_RETAIL_TY',
                                                                                 'HTD_GAFS_COST_LY',
                                                                                 'HTD_GAFS_COST_TY',
                                                                                 'CUM_MARKON_PCT_LY',
                                                                                 'CUM_MARKON_PCT_TY',
                                                                                 'GROSS_MARGIN_AMT_LY',
                                                                                 'GROSS_MARGIN_AMT_TY',
                                                                                 'VAT_IN_LY',
                                                                                 'VAT_IN_TY',
                                                                                 'VAT_OUT_LY',
                                                                                 'VAT_OUT_TY',
                                                                                 'UP_CHRG_AMT_PROFIT_LY',
                                                                                 'UP_CHRG_AMT_PROFIT_TY',
                                                                                 'UP_CHRG_AMT_EXP_LY',
                                                                                 'UP_CHRG_AMT_EXP_TY',
                                                                                 'WO_ACTIVITY_UPD_INV_LY',
                                                                                 'WO_ACTIVITY_UPD_INV_TY',
                                                                                 'WO_ACTIVITY_POST_FIN_LY',
                                                                                 'WO_ACTIVITY_POST_FIN_TY',
                                                                                 'FREIGHT_COST_LY',
                                                                                 'FREIGHT_COST_TY',
                                                                                 'WORKROOM_AMT_LY',
                                                                                 'WORKROOM_AMT_TY',
                                                                                 'CASH_DISC_AMT_LY',
                                                                                 'CASH_DISC_AMT_TY',
                                                                                 'COST_VARIANCE_AMT_LY',
                                                                                 'COST_VARIANCE_AMT_TY',
                                                                                 'REC_COST_ADJ_VARIANCE_LY',
                                                                                 'REC_COST_ADJ_VARIANCE_TY',
                                                                                 'RETAIL_COST_VARIANCE_LY',
                                                                                 'RETAIL_COST_VARIANCE_TY',
                                                                                 'DEAL_INCOME_PURCH_LY',
                                                                                 'DEAL_INCOME_PURCH_TY',
                                                                                 'DEAL_INCOME_SALES_LY',
                                                                                 'DEAL_INCOME_SALES_TY',
                                                                                 'FRN_RESTOCKING_FEE_LY',
                                                                                 'FRN_RESTOCKING_FEE_TY',
                                                                                 'INTERCOMPANY_MARGIN_LY',
                                                                                 'INTERCOMPANY_MARGIN_TY',
                                                                                 'MARGIN_COST_VARIANCE_LY',
                                                                                 'MARGIN_COST_VARIANCE_TY',
                                                                                 'NET_MARKDOWN_LY',
                                                                                 'NET_MARKDOWN_TY',
                                                                                 'NET_MARKUP_LY',
                                                                                 'NET_MARKUP_TY',
                                                                                 'NET_RECLASS_RETAIL_LY',
                                                                                 'NET_RECLASS_RETAIL_TY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_LY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_TY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_LY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_TY',
                                                                                 'NET_TSF_RETAIL_LY',
                                                                                 'NET_TSF_RETAIL_TY',
                                                                                 'NET_TSF_COST_LY',
                                                                                 'NET_TSF_COST_TY',
                                                                                 'RECOVERABLE_TAX_LY',
                                                                                 'RECOVERABLE_TAX_TY',
                                                                                 'RESTOCKING_FEE_LY',
                                                                                 'RESTOCKING_FEE_TY',
                                                                                 'SHRINK_RETAIL_PCT_LY',
                                                                                 'SHRINK_RETAIL_PCT_TY',
                                                                                 'SALES_UNITS_LY',
                                                                                 'SALES_UNITS_TY',
                                                                                 'NET_SALES_NONINV_RETAIL_LY',
                                                                                 'NET_SALES_NONINV_RETAIL_TY');
    
   L_file.add_sheet(RWD_sheet);
   L_file.sheets(l_file.get_sheet_index(RWD_sheet)).column_headers := s9t_cells( 'DEPT',
                                                                                 'DEPT_NAME',
                                                                                 'CLASS',
                                                                                 'CLASS_NAME',
                                                                                 'SUBCLASS',
                                                                                 'SUBCLASS_NAME',
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'LOC_NAME',
                                                                                 'CURRENCY',
                                                                                 'EOM_DATE',
                                                                                 'WEEK_NO',
                                                                                 'OPN_STK_RETAIL_LY',
                                                                                 'OPN_STK_RETAIL_TY',
                                                                                 'PURCH_RETAIL_LY',
                                                                                 'PURCH_RETAIL_TY',
                                                                                 'TSF_IN_RETAIL_LY',
                                                                                 'TSF_IN_RETAIL_TY',
                                                                                 'TSF_IN_BOOK_RETAIL_LY',
                                                                                 'TSF_IN_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_IN_RETAIL_LY',
                                                                                 'INTERCOMPANY_IN_RETAIL_TY',
                                                                                 'RECLASS_IN_RETAIL_LY',
                                                                                 'RECLASS_IN_RETAIL_TY',
                                                                                 'FRN_RETURNS_RETAIL_LY',
                                                                                 'FRN_RETURNS_RETAIL_TY',
                                                                                 'MARKUP_RETAIL_LY',
                                                                                 'MARKUP_RETAIL_TY',
                                                                                 'FRN_MARKUP_RETAIL_LY',
                                                                                 'FRN_MARKUP_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKUP_LY',
                                                                                 'INTERCOMPANY_MARKUP_TY',
                                                                                 'MARKUP_CAN_RETAIL_LY',
                                                                                 'MARKUP_CAN_RETAIL_TY',
                                                                                 'RTV_RETAIL_LY',
                                                                                 'RTV_RETAIL_TY',
                                                                                 'TSF_OUT_RETAIL_LY',
                                                                                 'TSF_OUT_RETAIL_TY',
                                                                                 'TSF_OUT_BOOK_RETAIL_LY',
                                                                                 'TSF_OUT_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_LY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_TY',
                                                                                 'RECLASS_OUT_RETAIL_LY',
                                                                                 'RECLASS_OUT_RETAIL_TY',
                                                                                 'NET_SALES_RETAIL_LY',
                                                                                 'NET_SALES_RETAIL_TY',
                                                                                 'RETURNS_RETAIL_LY',
                                                                                 'RETURNS_RETAIL_TY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_LY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_TY',
                                                                                 'EMPL_DISC_RETAIL_LY',
                                                                                 'EMPL_DISC_RETAIL_TY',
                                                                                 'FRANCHISE_SALES_RETAIL_LY',
                                                                                 'FRANCHISE_SALES_RETAIL_TY',
                                                                                 'FREIGHT_CLAIM_RETAIL_LY',
                                                                                 'FREIGHT_CLAIM_RETAIL_TY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_LY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_TY',
                                                                                 'STOCK_ADJ_RETAIL_LY',
                                                                                 'STOCK_ADJ_RETAIL_TY',
                                                                                 'SHRINKAGE_RETAIL_LY',
                                                                                 'SHRINKAGE_RETAIL_TY',
                                                                                 'PERM_MARKDOWN_RETAIL_LY',
                                                                                 'PERM_MARKDOWN_RETAIL_TY',
                                                                                 'PROM_MARKDOWN_RETAIL_LY',
                                                                                 'PROM_MARKDOWN_RETAIL_TY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKDOWN_LY',
                                                                                 'INTERCOMPANY_MARKDOWN_TY',
                                                                                 'FRN_MARKDOWN_RETAIL_LY',
                                                                                 'FRN_MARKDOWN_RETAIL_TY',
                                                                                 'MARKDOWN_CAN_RETAIL_LY', 
                                                                                 'MARKDOWN_CAN_RETAIL_TY', 
                                                                                 'CLS_STK_RETAIL_LY',
                                                                                 'CLS_STK_RETAIL_TY',
                                                                                 'ADD_RETAIL_LY',
                                                                                 'ADD_RETAIL_TY',
                                                                                 'RED_RETAIL_LY',
                                                                                 'RED_RETAIL_TY',
                                                                                 'ADD_COST_LY',
                                                                                 'ADD_COST_TY',
                                                                                 'HTD_GAFS_RETAIL_LY',
                                                                                 'HTD_GAFS_RETAIL_TY',
                                                                                 'HTD_GAFS_COST_LY',
                                                                                 'HTD_GAFS_COST_TY',
                                                                                 'CUM_MARKON_PCT_LY',
                                                                                 'CUM_MARKON_PCT_TY',
                                                                                 'GROSS_MARGIN_AMT_LY',
                                                                                 'GROSS_MARGIN_AMT_TY',
                                                                                 'VAT_IN_LY',
                                                                                 'VAT_IN_TY',
                                                                                 'VAT_OUT_LY',
                                                                                 'VAT_OUT_TY',
                                                                                 'UP_CHRG_AMT_PROFIT_LY',
                                                                                 'UP_CHRG_AMT_PROFIT_TY',
                                                                                 'UP_CHRG_AMT_EXP_LY',
                                                                                 'UP_CHRG_AMT_EXP_TY',
                                                                                 'WO_ACTIVITY_UPD_INV_LY',
                                                                                 'WO_ACTIVITY_UPD_INV_TY',
                                                                                 'WO_ACTIVITY_POST_FIN_LY',
                                                                                 'WO_ACTIVITY_POST_FIN_TY',
                                                                                 'FREIGHT_COST_LY',
                                                                                 'FREIGHT_COST_TY',
                                                                                 'WORKROOM_AMT_LY',
                                                                                 'WORKROOM_AMT_TY',
                                                                                 'CASH_DISC_AMT_LY',
                                                                                 'CASH_DISC_AMT_TY',
                                                                                 'COST_VARIANCE_AMT_LY',
                                                                                 'COST_VARIANCE_AMT_TY',
                                                                                 'REC_COST_ADJ_VARIANCE_LY',
                                                                                 'REC_COST_ADJ_VARIANCE_TY',
                                                                                 'RETAIL_COST_VARIANCE_LY',
                                                                                 'RETAIL_COST_VARIANCE_TY',
                                                                                 'DEAL_INCOME_PURCH_LY',
                                                                                 'DEAL_INCOME_PURCH_TY',
                                                                                 'DEAL_INCOME_SALES_LY',
                                                                                 'DEAL_INCOME_SALES_TY',
                                                                                 'FRN_RESTOCKING_FEE_LY',
                                                                                 'FRN_RESTOCKING_FEE_TY',
                                                                                 'INTERCOMPANY_MARGIN_LY',
                                                                                 'INTERCOMPANY_MARGIN_TY',
                                                                                 'MARGIN_COST_VARIANCE_LY',
                                                                                 'MARGIN_COST_VARIANCE_TY',
                                                                                 'NET_MARKDOWN_LY',
                                                                                 'NET_MARKDOWN_TY',
                                                                                 'NET_MARKUP_LY',
                                                                                 'NET_MARKUP_TY',
                                                                                 'NET_RECLASS_RETAIL_LY',
                                                                                 'NET_RECLASS_RETAIL_TY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_LY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_TY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_LY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_TY',
                                                                                 'NET_TSF_RETAIL_LY',
                                                                                 'NET_TSF_RETAIL_TY',
                                                                                 'NET_TSF_COST_LY',
                                                                                 'NET_TSF_COST_TY',
                                                                                 'RECOVERABLE_TAX_LY',
                                                                                 'RECOVERABLE_TAX_TY',
                                                                                 'RESTOCKING_FEE_LY',
                                                                                 'RESTOCKING_FEE_TY',
                                                                                 'SHRINK_RETAIL_PCT_LY',
                                                                                 'SHRINK_RETAIL_PCT_TY',
                                                                                 'SALES_UNITS_LY',
                                                                                 'SALES_UNITS_TY',
                                                                                 'NET_SALES_NONINV_RETAIL_LY',
                                                                                 'NET_SALES_NONINV_RETAIL_TY');
   
   L_file.add_sheet(RDD_sheet);
   L_file.sheets(l_file.get_sheet_index(RDD_sheet)).column_headers := s9t_cells( 'DEPT',
                                                                                 'DEPT_NAME',
                                                                                 'CLASS',
                                                                                 'CLASS_NAME',
                                                                                 'SUBCLASS',
                                                                                 'SUBCLASS_NAME',
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'LOC_NAME',
                                                                                 'CURRENCY',
                                                                                 'EOW_DATE',
                                                                                 'DAY_NO',
                                                                                 'PURCH_RETAIL_LY',
                                                                                 'PURCH_RETAIL_TY',
                                                                                 'TSF_IN_RETAIL_LY',
                                                                                 'TSF_IN_RETAIL_TY',
                                                                                 'TSF_IN_BOOK_RETAIL_LY',
                                                                                 'TSF_IN_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_IN_RETAIL_LY',
                                                                                 'INTERCOMPANY_IN_RETAIL_TY',
                                                                                 'RECLASS_IN_RETAIL_LY',
                                                                                 'RECLASS_IN_RETAIL_TY',
                                                                                 'FRN_RETURNS_RETAIL_LY',
                                                                                 'FRN_RETURNS_RETAIL_TY',
                                                                                 'MARKUP_RETAIL_LY',
                                                                                 'MARKUP_RETAIL_TY',
                                                                                 'FRN_MARKUP_RETAIL_LY',
                                                                                 'FRN_MARKUP_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKUP_LY',
                                                                                 'INTERCOMPANY_MARKUP_TY',
                                                                                 'MARKUP_CAN_RETAIL_LY',
                                                                                 'MARKUP_CAN_RETAIL_TY',
                                                                                 'RTV_RETAIL_LY',
                                                                                 'RTV_RETAIL_TY',
                                                                                 'TSF_OUT_RETAIL_LY',
                                                                                 'TSF_OUT_RETAIL_TY',
                                                                                 'TSF_OUT_BOOK_RETAIL_LY',
                                                                                 'TSF_OUT_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_LY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_TY',
                                                                                 'RECLASS_OUT_RETAIL_LY',
                                                                                 'RECLASS_OUT_RETAIL_TY',
                                                                                 'NET_SALES_RETAIL_LY',
                                                                                 'NET_SALES_RETAIL_TY',
                                                                                 'RETURNS_RETAIL_LY',
                                                                                 'RETURNS_RETAIL_TY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_LY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_TY',
                                                                                 'EMPL_DISC_RETAIL_LY',
                                                                                 'EMPL_DISC_RETAIL_TY',
                                                                                 'FRANCHISE_SALES_RETAIL_LY',
                                                                                 'FRANCHISE_SALES_RETAIL_TY',
                                                                                 'FREIGHT_CLAIM_RETAIL_LY',
                                                                                 'FREIGHT_CLAIM_RETAIL_TY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_LY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_TY',
                                                                                 'STOCK_ADJ_RETAIL_LY',
                                                                                 'STOCK_ADJ_RETAIL_TY',
                                                                                 'PERM_MARKDOWN_RETAIL_LY',
                                                                                 'PERM_MARKDOWN_RETAIL_TY',
                                                                                 'PROM_MARKDOWN_RETAIL_LY',
                                                                                 'PROM_MARKDOWN_RETAIL_TY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKDOWN_LY',
                                                                                 'INTERCOMPANY_MARKDOWN_TY',
                                                                                 'FRN_MARKDOWN_RETAIL_LY',
                                                                                 'FRN_MARKDOWN_RETAIL_TY',
                                                                                 'MARKDOWN_CAN_RETAIL_LY',
                                                                                 'MARKDOWN_CAN_RETAIL_TY',
                                                                                 'VAT_IN_LY',
                                                                                 'VAT_IN_TY',
                                                                                 'VAT_OUT_LY',
                                                                                 'VAT_OUT_TY',
                                                                                 'UP_CHRG_AMT_PROFIT_LY',
                                                                                 'UP_CHRG_AMT_PROFIT_TY',
                                                                                 'UP_CHRG_AMT_EXP_LY',
                                                                                 'UP_CHRG_AMT_EXP_TY',
                                                                                 'WO_ACTIVITY_UPD_INV_LY',
                                                                                 'WO_ACTIVITY_UPD_INV_TY',
                                                                                 'WO_ACTIVITY_POST_FIN_LY',
                                                                                 'WO_ACTIVITY_POST_FIN_TY',
                                                                                 'FREIGHT_COST_LY',
                                                                                 'FREIGHT_COST_TY',
                                                                                 'WORKROOM_AMT_LY',
                                                                                 'WORKROOM_AMT_TY',
                                                                                 'CASH_DISC_AMT_LY',
                                                                                 'CASH_DISC_AMT_TY',
                                                                                 'COST_VARIANCE_AMT_LY',
                                                                                 'COST_VARIANCE_AMT_TY',
                                                                                 'RETAIL_COST_VARIANCE_LY',
                                                                                 'RETAIL_COST_VARIANCE_TY',
                                                                                 'REC_COST_ADJ_VARIANCE_LY',
                                                                                 'REC_COST_ADJ_VARIANCE_TY',
                                                                                 'ADD_COST_LY',
                                                                                 'ADD_COST_TY',
                                                                                 'ADD_RETAIL_LY',
                                                                                 'ADD_RETAIL_TY',
                                                                                 'DEAL_INCOME_PURCH_LY',
                                                                                 'DEAL_INCOME_PURCH_TY',
                                                                                 'DEAL_INCOME_SALES_LY',
                                                                                 'DEAL_INCOME_SALES_TY',
                                                                                 'RESTOCKING_FEE_LY',
                                                                                 'RESTOCKING_FEE_TY',
                                                                                 'FRN_RESTOCKING_FEE_LY',
                                                                                 'FRN_RESTOCKING_FEE_TY',
                                                                                 'MARGIN_COST_VARIANCE_LY',
                                                                                 'MARGIN_COST_VARIANCE_TY',
                                                                                 'NET_MARKDOWN_LY',
                                                                                 'NET_MARKDOWN_TY',
                                                                                 'NET_MARKUP_LY',
                                                                                 'NET_MARKUP_TY',
                                                                                 'NET_RECLASS_RETAIL_LY',
                                                                                 'NET_RECLASS_RETAIL_TY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_LY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_TY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_LY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_TY',
                                                                                 'NET_SALES_NONINV_RETAIL_LY',
                                                                                 'NET_SALES_NONINV_RETAIL_TY',
                                                                                 'NET_TSF_RETAIL_LY',
                                                                                 'NET_TSF_RETAIL_TY',
                                                                                 'RECOVERABLE_TAX_LY',
                                                                                 'RECOVERABLE_TAX_TY',
                                                                                 'SALES_UNITS_LY',
                                                                                 'SALES_UNITS_TY');
   L_file.add_sheet(CRM_sheet);
   L_file.sheets(l_file.get_sheet_index(CRM_sheet)).column_headers := s9t_cells( 'DEPT',
                                                                                 'DEPT_NAME',
                                                                                 'CLASS',
                                                                                 'CLASS_NAME',
                                                                                 'SUBCLASS',
                                                                                 'SUBCLASS_NAME',
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'LOC_NAME',
                                                                                 'CURRENCY',
                                                                                 'HALF_NO',
                                                                                 'MONTH_NO',
                                                                                 'OPN_STK_COST_LY',
                                                                                 'OPN_STK_COST_TY',
                                                                                 'OPN_STK_RETAIL_LY',
                                                                                 'OPN_STK_RETAIL_TY',
                                                                                 'PURCH_COST_LY',
                                                                                 'PURCH_COST_TY',
                                                                                 'PURCH_RETAIL_LY',
                                                                                 'PURCH_RETAIL_TY',
                                                                                 'TSF_IN_COST_LY',
                                                                                 'TSF_IN_COST_TY',
                                                                                 'TSF_IN_RETAIL_LY',
                                                                                 'TSF_IN_RETAIL_TY',
                                                                                 'TSF_IN_BOOK_COST_LY',
                                                                                 'TSF_IN_BOOK_COST_TY',
                                                                                 'TSF_IN_BOOK_RETAIL_LY',
                                                                                 'TSF_IN_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_IN_COST_LY',
                                                                                 'INTERCOMPANY_IN_COST_TY',
                                                                                 'INTERCOMPANY_IN_RETAIL_LY',
                                                                                 'INTERCOMPANY_IN_RETAIL_TY',
                                                                                 'RECLASS_IN_COST_LY',
                                                                                 'RECLASS_IN_COST_TY',
                                                                                 'RECLASS_IN_RETAIL_LY',
                                                                                 'RECLASS_IN_RETAIL_TY',
                                                                                 'FRANCHISE_RETURNS_COST_LY',
                                                                                 'FRANCHISE_RETURNS_COST_TY',
                                                                                 'FRN_RETURNS_RETAIL_LY',
                                                                                 'FRN_RETURNS_RETAIL_TY',
                                                                                 'UP_CHRG_AMT_PROFIT_LY',
                                                                                 'UP_CHRG_AMT_PROFIT_TY',
                                                                                 'UP_CHRG_AMT_EXP_LY',
                                                                                 'UP_CHRG_AMT_EXP_TY',
                                                                                 'WO_ACTIVITY_UPD_INV_LY',
                                                                                 'WO_ACTIVITY_UPD_INV_TY',
                                                                                 'WO_ACTIVITY_POST_FIN_LY',
                                                                                 'WO_ACTIVITY_POST_FIN_TY',
                                                                                 'RECOVERABLE_TAX_LY',
                                                                                 'RECOVERABLE_TAX_TY',
                                                                                 'MARKUP_RETAIL_LY',
                                                                                 'MARKUP_RETAIL_TY',
                                                                                 'FRN_MARKUP_RETAIL_LY',
                                                                                 'FRN_MARKUP_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKUP_LY',
                                                                                 'INTERCOMPANY_MARKUP_TY',
                                                                                 'MARKUP_CAN_RETAIL_LY',
                                                                                 'MARKUP_CAN_RETAIL_TY',
                                                                                 'RTV_COST_LY',
                                                                                 'RTV_COST_TY',
                                                                                 'RTV_RETAIL_LY',
                                                                                 'RTV_RETAIL_TY',
                                                                                 'TSF_OUT_COST_LY',
                                                                                 'TSF_OUT_COST_TY',
                                                                                 'TSF_OUT_RETAIL_LY',
                                                                                 'TSF_OUT_RETAIL_TY',
                                                                                 'TSF_OUT_BOOK_COST_LY',
                                                                                 'TSF_OUT_BOOK_COST_TY',
                                                                                 'TSF_OUT_BOOK_RETAIL_LY',
                                                                                 'TSF_OUT_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_OUT_COST_LY',
                                                                                 'INTERCOMPANY_OUT_COST_TY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_LY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_TY',
                                                                                 'RECLASS_OUT_COST_LY',
                                                                                 'RECLASS_OUT_COST_TY',
                                                                                 'RECLASS_OUT_RETAIL_LY',
                                                                                 'RECLASS_OUT_RETAIL_TY',
                                                                                 'NET_SALES_COST_LY',
                                                                                 'NET_SALES_COST_TY',
                                                                                 'NET_SALES_RETAIL_LY',
                                                                                 'NET_SALES_RETAIL_TY',
                                                                                 'RETURNS_COST_LY',
                                                                                 'RETURNS_COST_TY',
                                                                                 'RETURNS_RETAIL_LY',
                                                                                 'RETURNS_RETAIL_TY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_LY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_TY',
                                                                                 'EMPL_DISC_RETAIL_LY',
                                                                                 'EMPL_DISC_RETAIL_TY',
                                                                                 'FRANCHISE_SALES_COST_LY',
                                                                                 'FRANCHISE_SALES_COST_TY',
                                                                                 'FRANCHISE_SALES_RETAIL_LY',
                                                                                 'FRANCHISE_SALES_RETAIL_TY',
                                                                                 'FREIGHT_CLAIM_COST_LY',
                                                                                 'FREIGHT_CLAIM_COST_TY',
                                                                                 'FREIGHT_CLAIM_RETAIL_LY',
                                                                                 'FREIGHT_CLAIM_RETAIL_TY',
                                                                                 'STOCK_ADJ_COGS_COST_LY',
                                                                                 'STOCK_ADJ_COGS_COST_TY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_LY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_TY',
                                                                                 'STOCK_ADJ_COST_LY',
                                                                                 'STOCK_ADJ_COST_TY',
                                                                                 'STOCK_ADJ_RETAIL_LY',
                                                                                 'STOCK_ADJ_RETAIL_TY',
                                                                                 'SHRINKAGE_COST_LY',
                                                                                 'SHRINKAGE_COST_TY',
                                                                                 'SHRINKAGE_RETAIL_LY',
                                                                                 'SHRINKAGE_RETAIL_TY',
                                                                                 'COST_VARIANCE_AMT_LY',
                                                                                 'COST_VARIANCE_AMT_TY',
                                                                                 'MARGIN_COST_VARIANCE_LY',
                                                                                 'MARGIN_COST_VARIANCE_TY',
                                                                                 'PERM_MARKDOWN_RETAIL_LY',
                                                                                 'PERM_MARKDOWN_RETAIL_TY',
                                                                                 'PROM_MARKDOWN_RETAIL_LY',
                                                                                 'PROM_MARKDOWN_RETAIL_TY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKDOWN_LY',
                                                                                 'INTERCOMPANY_MARKDOWN_TY',
                                                                                 'FRN_MARKDOWN_RETAIL_LY',
                                                                                 'FRN_MARKDOWN_RETAIL_TY',
                                                                                 'MARKDOWN_CAN_RETAIL_LY',
                                                                                 'MARKDOWN_CAN_RETAIL_TY',
                                                                                 'CLS_STK_COST_LY',
                                                                                 'CLS_STK_COST_TY',
                                                                                 'CLS_STK_RETAIL_LY',
                                                                                 'CLS_STK_RETAIL_TY',
                                                                                 'ADD_COST_LY',
                                                                                 'ADD_COST_TY',
                                                                                 'ADD_RETAIL_LY',
                                                                                 'ADD_RETAIL_TY',
                                                                                 'SHRINK_COST_PCT_LY',
                                                                                 'SHRINK_COST_PCT_TY',
                                                                                 'HTD_GAFS_RETAIL_LY',
                                                                                 'HTD_GAFS_RETAIL_TY',
                                                                                 'HTD_GAFS_COST_LY',
                                                                                 'HTD_GAFS_COST_TY',
                                                                                 'RED_RETAIL_LY',
                                                                                 'RED_RETAIL_TY',
                                                                                 'DEAL_INCOME_SALES_LY',
                                                                                 'DEAL_INCOME_SALES_TY',
                                                                                 'DEAL_INCOME_PURCH_LY',
                                                                                 'DEAL_INCOME_PURCH_TY',
                                                                                 'CUM_MARKON_PCT_LY',
                                                                                 'CUM_MARKON_PCT_TY',
                                                                                 'GROSS_MARGIN_AMT_LY',
                                                                                 'GROSS_MARGIN_AMT_TY',
                                                                                 'VAT_IN_LY',
                                                                                 'VAT_IN_TY',
                                                                                 'VAT_OUT_LY',
                                                                                 'VAT_OUT_TY',
                                                                                 'RESTOCKING_FEE_LY',
                                                                                 'RESTOCKING_FEE_TY',
                                                                                 'FREIGHT_COST_LY',
                                                                                 'FREIGHT_COST_TY',
                                                                                 'REC_COST_ADJ_VARIANCE_LY',
                                                                                 'REC_COST_ADJ_VARIANCE_TY',
                                                                                 'WORKROOM_AMT_LY',
                                                                                 'WORKROOM_AMT_TY',
                                                                                 'CASH_DISC_AMT_LY',
                                                                                 'CASH_DISC_AMT_TY',
                                                                                 'RETAIL_COST_VARIANCE_LY',
                                                                                 'RETAIL_COST_VARIANCE_TY',
                                                                                 'FRN_RESTOCKING_FEE_LY',
                                                                                 'FRN_RESTOCKING_FEE_TY',
                                                                                 'INTERCOMPANY_MARGIN_LY',
                                                                                 'INTERCOMPANY_MARGIN_TY',
                                                                                 'NET_MARKDOWN_LY',
                                                                                 'NET_MARKDOWN_TY',
                                                                                 'NET_MARKUP_LY',
                                                                                 'NET_MARKUP_TY',
                                                                                 'NET_RECLASS_COST_LY',
                                                                                 'NET_RECLASS_COST_TY',
                                                                                 'NET_RECLASS_RETAIL_LY',
                                                                                 'NET_RECLASS_RETAIL_TY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_LY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_TY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_LY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_TY',
                                                                                 'NET_TSF_COST_LY',
                                                                                 'NET_TSF_COST_TY',
                                                                                 'NET_TSF_RETAIL_LY',
                                                                                 'NET_TSF_RETAIL_TY',
                                                                                 'SHRINK_RETAIL_PCT_LY',
                                                                                 'SHRINK_RETAIL_PCT_TY',
                                                                                 'SALES_UNITS_LY',
                                                                                 'SALES_UNITS_TY',
                                                                                 'NET_SALES_NON_INV_COST_LY',
                                                                                 'NET_SALES_NON_INV_COST_TY',
                                                                                 'NET_SALES_NONINV_RETAIL_LY',
                                                                                 'NET_SALES_NONINV_RETAIL_TY');
   
   L_file.add_sheet(CRW_sheet);
   L_file.sheets(l_file.get_sheet_index(CRW_sheet)).column_headers := s9t_cells( 'DEPT',
                                                                                 'DEPT_NAME',
                                                                                 'CLASS',
                                                                                 'CLASS_NAME',
                                                                                 'SUBCLASS',
                                                                                 'SUBCLASS_NAME',
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'LOC_NAME',
                                                                                 'CURRENCY',
                                                                                 'EOM_DATE',
                                                                                 'WEEK_NO',
                                                                                 'OPN_STK_COST_LY',
                                                                                 'OPN_STK_COST_TY',
                                                                                 'OPN_STK_RETAIL_LY',
                                                                                 'OPN_STK_RETAIL_TY',
                                                                                 'PURCH_COST_LY',
                                                                                 'PURCH_COST_TY',
                                                                                 'PURCH_RETAIL_LY',
                                                                                 'PURCH_RETAIL_TY',
                                                                                 'TSF_IN_COST_LY',
                                                                                 'TSF_IN_COST_TY',
                                                                                 'TSF_IN_RETAIL_LY',
                                                                                 'TSF_IN_RETAIL_TY',
                                                                                 'TSF_IN_BOOK_COST_LY',
                                                                                 'TSF_IN_BOOK_COST_TY',
                                                                                 'TSF_IN_BOOK_RETAIL_LY',
                                                                                 'TSF_IN_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_IN_COST_LY',
                                                                                 'INTERCOMPANY_IN_COST_TY',
                                                                                 'INTERCOMPANY_IN_RETAIL_LY',
                                                                                 'INTERCOMPANY_IN_RETAIL_TY',
                                                                                 'RECLASS_IN_COST_LY',
                                                                                 'RECLASS_IN_COST_TY',
                                                                                 'RECLASS_IN_RETAIL_LY',
                                                                                 'RECLASS_IN_RETAIL_TY',
                                                                                 'FRANCHISE_RETURNS_COST_LY',
                                                                                 'FRANCHISE_RETURNS_COST_TY',
                                                                                 'FRN_RETURNS_RETAIL_LY',
                                                                                 'FRN_RETURNS_RETAIL_TY',
                                                                                 'UP_CHRG_AMT_PROFIT_LY',
                                                                                 'UP_CHRG_AMT_PROFIT_TY',
                                                                                 'UP_CHRG_AMT_EXP_LY',
                                                                                 'UP_CHRG_AMT_EXP_TY',
                                                                                 'WO_ACTIVITY_UPD_INV_LY',
                                                                                 'WO_ACTIVITY_UPD_INV_TY',
                                                                                 'WO_ACTIVITY_POST_FIN_LY',
                                                                                 'WO_ACTIVITY_POST_FIN_TY',
                                                                                 'RECOVERABLE_TAX_LY',
                                                                                 'RECOVERABLE_TAX_TY',
                                                                                 'MARKUP_RETAIL_LY',
                                                                                 'MARKUP_RETAIL_TY',
                                                                                 'FRN_MARKUP_RETAIL_LY',
                                                                                 'FRN_MARKUP_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKUP_LY',
                                                                                 'INTERCOMPANY_MARKUP_TY',
                                                                                 'MARKUP_CAN_RETAIL_LY',
                                                                                 'MARKUP_CAN_RETAIL_TY',
                                                                                 'RTV_COST_LY',
                                                                                 'RTV_COST_TY',
                                                                                 'RTV_RETAIL_LY',
                                                                                 'RTV_RETAIL_TY',
                                                                                 'TSF_OUT_COST_LY',
                                                                                 'TSF_OUT_COST_TY',
                                                                                 'TSF_OUT_RETAIL_LY',
                                                                                 'TSF_OUT_RETAIL_TY',
                                                                                 'TSF_OUT_BOOK_COST_LY',
                                                                                 'TSF_OUT_BOOK_COST_TY',
                                                                                 'TSF_OUT_BOOK_RETAIL_LY',
                                                                                 'TSF_OUT_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_OUT_COST_LY',
                                                                                 'INTERCOMPANY_OUT_COST_TY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_LY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_TY',
                                                                                 'RECLASS_OUT_COST_LY',
                                                                                 'RECLASS_OUT_COST_TY',
                                                                                 'RECLASS_OUT_RETAIL_LY',
                                                                                 'RECLASS_OUT_RETAIL_TY',
                                                                                 'NET_SALES_COST_LY',
                                                                                 'NET_SALES_COST_TY',
                                                                                 'NET_SALES_RETAIL_LY',
                                                                                 'NET_SALES_RETAIL_TY',
                                                                                 'RETURNS_COST_LY',
                                                                                 'RETURNS_COST_TY',
                                                                                 'RETURNS_RETAIL_LY',
                                                                                 'RETURNS_RETAIL_TY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_LY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_TY',
                                                                                 'EMPL_DISC_RETAIL_LY',
                                                                                 'EMPL_DISC_RETAIL_TY',
                                                                                 'FRANCHISE_SALES_COST_LY',
                                                                                 'FRANCHISE_SALES_COST_TY',
                                                                                 'FRANCHISE_SALES_RETAIL_LY',
                                                                                 'FRANCHISE_SALES_RETAIL_TY',
                                                                                 'FREIGHT_CLAIM_COST_LY',
                                                                                 'FREIGHT_CLAIM_COST_TY',
                                                                                 'FREIGHT_CLAIM_RETAIL_LY',
                                                                                 'FREIGHT_CLAIM_RETAIL_TY',
                                                                                 'STOCK_ADJ_COGS_COST_LY',
                                                                                 'STOCK_ADJ_COGS_COST_TY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_LY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_TY',
                                                                                 'STOCK_ADJ_COST_LY',
                                                                                 'STOCK_ADJ_COST_TY',
                                                                                 'STOCK_ADJ_RETAIL_LY',
                                                                                 'STOCK_ADJ_RETAIL_TY',
                                                                                 'SHRINKAGE_COST_LY',
                                                                                 'SHRINKAGE_COST_TY',
                                                                                 'SHRINKAGE_RETAIL_LY',
                                                                                 'SHRINKAGE_RETAIL_TY',
                                                                                 'COST_VARIANCE_AMT_LY',
                                                                                 'COST_VARIANCE_AMT_TY',
                                                                                 'MARGIN_COST_VARIANCE_LY',
                                                                                 'MARGIN_COST_VARIANCE_TY',
                                                                                 'PERM_MARKDOWN_RETAIL_LY',
                                                                                 'PERM_MARKDOWN_RETAIL_TY',
                                                                                 'PROM_MARKDOWN_RETAIL_LY',
                                                                                 'PROM_MARKDOWN_RETAIL_TY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                 'INTERCOMPANY_MARKDOWN_LY',
                                                                                 'INTERCOMPANY_MARKDOWN_TY',
                                                                                 'FRN_MARKDOWN_RETAIL_LY',
                                                                                 'FRN_MARKDOWN_RETAIL_TY',
                                                                                 'MARKDOWN_CAN_RETAIL_LY',
                                                                                 'MARKDOWN_CAN_RETAIL_TY',
                                                                                 'CLS_STK_COST_LY',
                                                                                 'CLS_STK_COST_TY',
                                                                                 'CLS_STK_RETAIL_LY',
                                                                                 'CLS_STK_RETAIL_TY',
                                                                                 'ADD_COST_LY',
                                                                                 'ADD_COST_TY',
                                                                                 'ADD_RETAIL_LY',
                                                                                 'ADD_RETAIL_TY',
                                                                                 'SHRINK_COST_PCT_LY',
                                                                                 'SHRINK_COST_PCT_TY',
                                                                                 'HTD_GAFS_RETAIL_LY',
                                                                                 'HTD_GAFS_RETAIL_TY',
                                                                                 'HTD_GAFS_COST_LY',
                                                                                 'HTD_GAFS_COST_TY',
                                                                                 'RED_RETAIL_LY',
                                                                                 'RED_RETAIL_TY',
                                                                                 'DEAL_INCOME_SALES_LY',
                                                                                 'DEAL_INCOME_SALES_TY',
                                                                                 'DEAL_INCOME_PURCH_LY',
                                                                                 'DEAL_INCOME_PURCH_TY',
                                                                                 'CUM_MARKON_PCT_LY',
                                                                                 'CUM_MARKON_PCT_TY',
                                                                                 'GROSS_MARGIN_AMT_LY',
                                                                                 'GROSS_MARGIN_AMT_TY',
                                                                                 'VAT_IN_LY',
                                                                                 'VAT_IN_TY',
                                                                                 'VAT_OUT_LY',
                                                                                 'VAT_OUT_TY',
                                                                                 'RESTOCKING_FEE_LY',
                                                                                 'RESTOCKING_FEE_TY',
                                                                                 'FREIGHT_COST_LY',
                                                                                 'FREIGHT_COST_TY',
                                                                                 'REC_COST_ADJ_VARIANCE_LY',
                                                                                 'REC_COST_ADJ_VARIANCE_TY',
                                                                                 'WORKROOM_AMT_LY',
                                                                                 'WORKROOM_AMT_TY',
                                                                                 'CASH_DISC_AMT_LY',
                                                                                 'CASH_DISC_AMT_TY',
                                                                                 'RETAIL_COST_VARIANCE_LY',
                                                                                 'RETAIL_COST_VARIANCE_TY',
                                                                                 'FRN_RESTOCKING_FEE_LY',
                                                                                 'FRN_RESTOCKING_FEE_TY',
                                                                                 'INTERCOMPANY_MARGIN_LY',
                                                                                 'INTERCOMPANY_MARGIN_TY',
                                                                                 'NET_MARKDOWN_LY',
                                                                                 'NET_MARKDOWN_TY',
                                                                                 'NET_MARKUP_LY',
                                                                                 'NET_MARKUP_TY',
                                                                                 'NET_RECLASS_COST_LY',
                                                                                 'NET_RECLASS_COST_TY',
                                                                                 'NET_RECLASS_RETAIL_LY',
                                                                                 'NET_RECLASS_RETAIL_TY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_LY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_TY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_LY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_TY',
                                                                                 'NET_TSF_COST_LY',
                                                                                 'NET_TSF_COST_TY',
                                                                                 'NET_TSF_RETAIL_LY',
                                                                                 'NET_TSF_RETAIL_TY',
                                                                                 'SHRINK_RETAIL_PCT_LY',
                                                                                 'SHRINK_RETAIL_PCT_TY',
                                                                                 'SALES_UNITS_LY',
                                                                                 'SALES_UNITS_TY',
                                                                                 'NET_SALES_NON_INV_COST_LY',
                                                                                 'NET_SALES_NON_INV_COST_TY',
                                                                                 'NET_SALES_NONINV_RETAIL_LY',
                                                                                 'NET_SALES_NONINV_RETAIL_TY');
   
   L_file.add_sheet(CRD_sheet);
   L_file.sheets(l_file.get_sheet_index(CRD_sheet)).column_headers := s9t_cells( 'DEPT',
                                                                                 'DEPT_NAME',
                                                                                 'CLASS',
                                                                                 'CLASS_NAME',
                                                                                 'SUBCLASS',
                                                                                 'SUBCLASS_NAME',
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'LOC_NAME',
                                                                                 'CURRENCY',
                                                                                 'EOW_DATE',
                                                                                 'DAY_NO',
                                                                                 'PURCH_COST_LY',
                                                                                 'PURCH_COST_TY',
                                                                                 'PURCH_RETAIL_LY',
                                                                                 'PURCH_RETAIL_TY',
                                                                                 'TSF_IN_COST_LY',
                                                                                 'TSF_IN_COST_TY',
                                                                                 'TSF_IN_RETAIL_LY',
                                                                                 'TSF_IN_RETAIL_TY',
                                                                                 'TSF_IN_BOOK_COST_LY',
                                                                                 'TSF_IN_BOOK_COST_TY',
                                                                                 'TSF_IN_BOOK_RETAIL_LY',
                                                                                 'TSF_IN_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_IN_COST_LY',
                                                                                 'INTERCOMPANY_IN_COST_TY',
                                                                                 'INTERCOMPANY_IN_RETAIL_LY',
                                                                                 'INTERCOMPANY_IN_RETAIL_TY',
                                                                                 'RECLASS_IN_COST_LY',
                                                                                 'RECLASS_IN_COST_TY',
                                                                                 'RECLASS_IN_RETAIL_LY',
                                                                                 'RECLASS_IN_RETAIL_TY',
                                                                                 'FRANCHISE_RETURNS_COST_LY',
                                                                                 'FRANCHISE_RETURNS_COST_TY',
                                                                                 'FRN_RETURNS_RETAIL_LY',
                                                                                 'FRN_RETURNS_RETAIL_TY',
                                                                                 'UP_CHRG_AMT_PROFIT_LY',
                                                                                 'UP_CHRG_AMT_PROFIT_TY',
                                                                                 'UP_CHRG_AMT_EXP_LY',
                                                                                 'UP_CHRG_AMT_EXP_TY',
                                                                                 'WO_ACTIVITY_UPD_INV_LY',
                                                                                 'WO_ACTIVITY_UPD_INV_TY',
                                                                                 'WO_ACTIVITY_POST_FIN_LY',
                                                                                 'WO_ACTIVITY_POST_FIN_TY',
                                                                                 'RECOVERABLE_TAX_LY',
                                                                                 'RECOVERABLE_TAX_TY',
                                                                                 'RTV_COST_LY',
                                                                                 'RTV_COST_TY',
                                                                                 'RTV_RETAIL_LY',
                                                                                 'RTV_RETAIL_TY',
                                                                                 'TSF_OUT_COST_LY',
                                                                                 'TSF_OUT_COST_TY',
                                                                                 'TSF_OUT_RETAIL_LY',
                                                                                 'TSF_OUT_RETAIL_TY',
                                                                                 'TSF_OUT_BOOK_COST_LY',
                                                                                 'TSF_OUT_BOOK_COST_TY',
                                                                                 'TSF_OUT_BOOK_RETAIL_LY',
                                                                                 'TSF_OUT_BOOK_RETAIL_TY',
                                                                                 'INTERCOMPANY_OUT_COST_LY',
                                                                                 'INTERCOMPANY_OUT_COST_TY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_LY',
                                                                                 'INTERCOMPANY_OUT_RETAIL_TY',
                                                                                 'RECLASS_OUT_COST_LY',
                                                                                 'RECLASS_OUT_COST_TY',
                                                                                 'RECLASS_OUT_RETAIL_LY',
                                                                                 'RECLASS_OUT_RETAIL_TY',
                                                                                 'NET_SALES_COST_LY',
                                                                                 'NET_SALES_COST_TY',
                                                                                 'NET_SALES_RETAIL_LY',
                                                                                 'NET_SALES_RETAIL_TY',
                                                                                 'RETURNS_COST_LY',
                                                                                 'RETURNS_COST_TY',
                                                                                 'RETURNS_RETAIL_LY',
                                                                                 'RETURNS_RETAIL_TY',
                                                                                 'FRANCHISE_SALES_COST_LY',
                                                                                 'FRANCHISE_SALES_COST_TY',
                                                                                 'FRANCHISE_SALES_RETAIL_LY',
                                                                                 'FRANCHISE_SALES_RETAIL_TY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_LY',
                                                                                 'WEIGHT_VARIANCE_RETAIL_TY',
                                                                                 'FREIGHT_CLAIM_COST_LY',
                                                                                 'FREIGHT_CLAIM_COST_TY',
                                                                                 'FREIGHT_CLAIM_RETAIL_LY',
                                                                                 'FREIGHT_CLAIM_RETAIL_TY',
                                                                                 'STOCK_ADJ_COGS_COST_LY',
                                                                                 'STOCK_ADJ_COGS_COST_TY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_LY',
                                                                                 'STOCK_ADJ_COGS_RETAIL_TY',
                                                                                 'STOCK_ADJ_COST_LY',
                                                                                 'STOCK_ADJ_COST_TY',
                                                                                 'STOCK_ADJ_RETAIL_LY',
                                                                                 'STOCK_ADJ_RETAIL_TY',
                                                                                 'COST_VARIANCE_AMT_LY',
                                                                                 'COST_VARIANCE_AMT_TY',
                                                                                 'RETAIL_COST_VARIANCE_LY',
                                                                                 'RETAIL_COST_VARIANCE_TY',
                                                                                 'MARGIN_COST_VARIANCE_LY',
                                                                                 'MARGIN_COST_VARIANCE_TY',
                                                                                 'VAT_IN_LY',
                                                                                 'VAT_IN_TY',
                                                                                 'VAT_OUT_LY',
                                                                                 'VAT_OUT_TY',
                                                                                 'DEAL_INCOME_SALES_LY',
                                                                                 'DEAL_INCOME_SALES_TY',
                                                                                 'DEAL_INCOME_PURCH_LY',
                                                                                 'DEAL_INCOME_PURCH_TY',
                                                                                 'RESTOCKING_FEE_LY',
                                                                                 'RESTOCKING_FEE_TY',
                                                                                 'FRN_RESTOCKING_FEE_LY',
                                                                                 'FRN_RESTOCKING_FEE_TY',
                                                                                 'NET_SALES_NON_INV_COST_LY',
                                                                                 'NET_SALES_NON_INV_COST_TY',
                                                                                 'ADD_COST_LY',
                                                                                 'ADD_COST_TY',
                                                                                 'ADD_RETAIL_LY',
                                                                                 'ADD_RETAIL_TY',
                                                                                 'CASH_DISC_AMT_LY',
                                                                                 'CASH_DISC_AMT_TY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_LY',
                                                                                 'CLEAR_MARKDOWN_RETAIL_TY',
                                                                                 'EMPL_DISC_RETAIL_LY',
                                                                                 'EMPL_DISC_RETAIL_TY',
                                                                                 'FRN_MARKDOWN_RETAIL_LY',
                                                                                 'FRN_MARKDOWN_RETAIL_TY',
                                                                                 'FRN_MARKUP_RETAIL_LY',
                                                                                 'FRN_MARKUP_RETAIL_TY',
                                                                                 'FREIGHT_COST_LY',
                                                                                 'FREIGHT_COST_TY',
                                                                                 'INTERCOMPANY_MARKDOWN_LY',
                                                                                 'INTERCOMPANY_MARKDOWN_TY',
                                                                                 'INTERCOMPANY_MARKUP_LY',
                                                                                 'INTERCOMPANY_MARKUP_TY',
                                                                                 'NET_MARKDOWN_LY',
                                                                                 'NET_MARKDOWN_TY',
                                                                                 'MARKDOWN_CAN_RETAIL_LY',
                                                                                 'MARKDOWN_CAN_RETAIL_TY',
                                                                                 'MARKUP_CAN_RETAIL_LY',
                                                                                 'MARKUP_CAN_RETAIL_TY',
                                                                                 'MARKUP_RETAIL_LY',
                                                                                 'MARKUP_RETAIL_TY',
                                                                                 'PERM_MARKDOWN_RETAIL_LY',
                                                                                 'PERM_MARKDOWN_RETAIL_TY',
                                                                                 'PROM_MARKDOWN_RETAIL_LY',
                                                                                 'PROM_MARKDOWN_RETAIL_TY',
                                                                                 'REC_COST_ADJ_VARIANCE_LY',
                                                                                 'REC_COST_ADJ_VARIANCE_TY',
                                                                                 'SALES_UNITS_LY',
                                                                                 'SALES_UNITS_TY',
                                                                                 'WORKROOM_AMT_LY',
                                                                                 'WORKROOM_AMT_TY',
                                                                                 'NET_RECLASS_COST_LY',
                                                                                 'NET_RECLASS_COST_TY',
                                                                                 'NET_RECLASS_RETAIL_LY',
                                                                                 'NET_RECLASS_RETAIL_TY',
                                                                                 'NET_TSF_COST_LY',
                                                                                 'NET_TSF_COST_TY',
                                                                                 'NET_MARKUP_LY',
                                                                                 'NET_MARKUP_TY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_LY',
                                                                                 'NET_SALES_RETAIL_EX_VAT_TY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_LY',
                                                                                 'NET_SALE_NONINV_R_EXVAT_TY',
                                                                                 'NET_SALES_NONINV_RETAIL_LY',
                                                                                 'NET_SALES_NONINV_RETAIL_TY',
                                                                                 'NET_TSF_RETAIL_LY',
                                                                                 'NET_TSF_RETAIL_TY');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_STKLDGRV.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if I_template_only_ind = 'N' then
      if template_key = 'STKLDGR_C_MONTH_DATA' then 
         if FETCH_C_MONTH_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_C_MONTH_DATA(O_file_id);
      end if;
      if template_key = 'STKLDGR_C_WEEK_DATA' then
         if FETCH_C_WEEK_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_C_WEEK_DATA(O_file_id);
      end if;
      if template_key = 'STKLDGR_C_DAILY_DATA' then
         if FETCH_C_DAILY_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_C_DAILY_DATA(O_file_id);
      end if;
      if template_key = 'STKLDGR_R_MONTH_DATA' then 
         if FETCH_R_MONTH_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_R_MONTH_DATA(O_file_id);
      end if;
      if template_key = 'STKLDGR_R_WEEK_DATA' then 
         if FETCH_R_WEEK_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_R_WEEK_DATA(O_file_id);
      end if;
      if template_key = 'STKLDGR_R_DAILY_DATA' then 
         if FETCH_R_DAILY_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_R_DAILY_DATA(O_file_id);
      end if;
      if template_key = 'STKLDGR_CR_MONTH_DATA' then 
         if FETCH_CR_MONTH_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_CR_MONTH_DATA(O_file_id);
      end if;
      if template_key = 'STKLDGR_CR_WEEK_DATA' then 
         if FETCH_CR_WEEK_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_CR_WEEK_DATA(O_file_id);
      end if;
      if template_key = 'STKLDGR_CR_DAILY_DATA' then 
         if FETCH_CR_DAILY_DATA(O_error_message) = FALSE then
            return FALSE;
         end if;
         POPULATE_CR_DAILY_DATA(O_file_id);
      end if;
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
END CORESVC_STKLDGRV;
/
