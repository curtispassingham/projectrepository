CREATE OR REPLACE PACKAGE CORESVC_SA_CONSTANTS AUTHID CURRENT_USER AS
   template_key                    CONSTANT VARCHAR2(255) := 'SA_CONSTANTS_DATA';
   template_category               CODE_DETAIL.CODE%TYPE  := 'RSACD';
   
   action_new                      VARCHAR2(25)           := 'NEW';
   action_mod                      VARCHAR2(25)           := 'MOD';
   action_del                      VARCHAR2(25)           := 'DEL';
   
   SA_CONSTANTS_sheet              VARCHAR2(255)          := 'SA_CONSTANTS';
   SA_CONSTANTS$Action             NUMBER                 := 1;
   SA_CONSTANTS$CONSTANT_ID        NUMBER                 := 2;
   SA_CONSTANTS$CONSTANT_NAME      NUMBER                 := 3;
   SA_CONSTANTS$CONSTANT_VALUE     NUMBER                 := 4;
   SA_CONSTANTS$VALUE_DATA_TYPE    NUMBER                 := 5;
   SA_CONSTANTS$DELETE_IND         NUMBER                 := 6;
   
   SA_CONSTANTS_TL_sheet           VARCHAR2(255)         := 'SA_CONSTANTS_TL';
   SA_CONSTANTS_TL$Action          NUMBER                := 1;
   SA_CONSTANTS_TL$LANG            NUMBER                := 2;
   SA_CONSTANTS_TL$CONSTANT_ID     NUMBER                := 3;
   SA_CONSTANTS_TL$CONSTANT_NAME   NUMBER                := 4;
   
   sheet_name_trans                S9T_PKG.trans_map_typ;
   action_column                   VARCHAR2(255)          := 'ACTION';
----------------------------------------------------------------------------------   
   TYPE SA_CONSTANTS_rec_tab IS TABLE OF SA_CONSTANTS%ROWTYPE;
----------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
----------------------------------------------------------------------------------   
END CORESVC_SA_CONSTANTS;
/
