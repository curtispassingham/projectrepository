
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_COMMENTS_SQL AS
-------------------------------------------------------------------------------------------
FUNCTION GET_NEW_COMMENT_SEQ_NO(O_error_message    IN OUT  VARCHAR2,
                                O_comment_seq_no   IN OUT  SA_COMMENTS.COMMENT_SEQ_NO%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program         VARCHAR2(50)                   := 'SA_COMMENTS_SQL.GET_NEW_COMMENT_SEQ_NO';   
   ---
   cursor C_GET_NEXT is
      select sa_comment_seq_no_sequence.NEXTVAL
        from dual;   
   ---
BEGIN
   --- Retrieve sequence number
   SQL_LIB.SET_MARK('OPEN','C_GET_NEXT','DUAL',NULL);
   open C_GET_NEXT;
   SQL_LIB.SET_MARK('FETCH','C_GET_NEXT','DUAL',NULL);
   fetch C_GET_NEXT into O_comment_seq_no;
   ---
   if C_GET_NEXT%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT','DUAL',NULL);
      close C_GET_NEXT;
      O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT','DUAL',NULL);
   close C_GET_NEXT;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEW_COMMENT_SEQ_NO;
----------------------------------------------------------------------------
FUNCTION KEY_DECODE(O_error_message     IN OUT  VARCHAR2,
                    O_comment_key_desc  IN OUT  VARCHAR2,
                    I_comment_type      IN      SA_COMMENTS.COMMENT_TYPE%TYPE,
                    I_comment_key       IN      SA_COMMENTS.COMMENT_KEY%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program          VARCHAR2(64)                        := 'SA_COMMENT_SQL.KEY_DECODE';
   L_store_day_seq_no SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE  := NULL;
   L_store            STORE.STORE%TYPE                    := NULL;
   L_store_NAME       STORE.STORE_NAME%TYPE               := NULL;
   L_business_date    SA_STORE_DAY.BUSINESS_DATE%TYPE     := NULL;
   L_cashier          SA_BALANCE_GROUP.CASHIER%TYPE       := NULL;
   L_register         SA_BALANCE_GROUP.REGISTER%TYPE      := NULL;
   L_total_id         SA_TOTAL_HEAD.TOTAL_ID%TYPE         := NULL;
   L_total_desc       SA_TOTAL_HEAD.TOTAL_DESC%TYPE       := NULL;
   L_total_rev_no     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE     := NULL;
   L_update_datetime  SA_TOTAL_HEAD.UPDATE_DATETIME%TYPE  := NULL;
   ---
   cursor C_GET_TOTAL_INFO is
      select total_id,
             total_desc,
             total_rev_no,
             update_datetime
        from sa_total_head
       where total_id = (select total_id
                           from sa_total
                           where total_seq_no = I_comment_key);
   cursor C_GET_BALANCE_INFO is
      select store_day_seq_no,
             register,
             cashier
        from sa_balance_group
       where bal_group_seq_no = I_comment_key;
BEGIN
   if I_comment_type is NULL or I_comment_key is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_comment_type = 'SASD' then
      if STORE_DAY_SQL.GET_STORE_DAY(O_error_message,
                                     L_store,
                                     L_business_date,
                                     I_comment_key) = FALSE then
         return FALSE;
      end if;
      if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                   L_store,
                                   L_store_name) = FALSE then
         return FALSE;
      end if;
      O_comment_key_desc := L_store || ', ' || L_store_name || ', ' || to_char(L_business_date,'DD-MON-YYYY');
      ---
   elsif I_comment_type = 'SATR' then
      O_comment_key_desc := I_comment_key;
      ---
   elsif I_comment_type = 'SATL' then
      SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_INFO','SA_COMMENTS_SQL',NULL);
      open C_GET_TOTAL_INFO;
      SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_INFO','SA_COMMENTS_SQL',NULL);
      fetch C_GET_TOTAL_INFO into L_total_id,
                                  L_total_desc,
                                  L_total_rev_no,
                                  L_update_datetime;
      if C_GET_TOTAL_INFO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_INFO','SA_COMMENTS_SQL',NULL);
         close C_GET_TOTAL_INFO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_DATA',NULL,NULL,NULL);
         return FALSE;
      end if;                                       
      SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_INFO','SA_COMMENTS_SQL',NULL);
      close C_GET_TOTAL_INFO;
      ---
      O_comment_key_desc := L_total_desc || ', ' || L_total_rev_no || ', ' || to_char(L_update_datetime, 'DD-MON-YYYY HH24:MI:SS');
      ---
   elsif I_comment_type = 'SABG' then
      SQL_LIB.SET_MARK('OPEN','C_GET_BALANCE_INFO','SA_COMMENTS_SQL',NULL);
      open C_GET_BALANCE_INFO;
      SQL_LIB.SET_MARK('FETCH','C_GET_BALANCE_INFO','SA_COMMENTS_SQL',NULL);
      fetch C_GET_BALANCE_INFO into L_store_day_seq_no,
                                    L_register,
                                    L_cashier;
      if C_GET_BALANCE_INFO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_BALANCE_INFO','SA_COMMENTS_SQL',NULL);
         close C_GET_BALANCE_INFO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_DATA',NULL,NULL,NULL);
         return FALSE;
      end if;                                                                           
      SQL_LIB.SET_MARK('CLOSE','C_GET_BALANCE_INFO','SA_COMMENTS_SQL',NULL);
      close C_GET_BALANCE_INFO;
      ---
      if STORE_DAY_SQL.GET_STORE_DAY(O_error_message,
                                     L_store,
                                     L_business_date,
                                     L_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                   L_store,
                                   L_store_name) = FALSE then
         return FALSE;
      end if;
      ---
      if L_cashier is NOT NULL and L_register is NOT NULL then
         O_comment_key_desc := L_store || ', ' || L_store_name || ', ' || L_cashier ||
                               ', ' || L_register || ', ' || To_Char(L_business_date, 'DD-MON-YYYY');
      elsif L_cashier is NOT NULL then
         O_comment_key_desc := L_store || ', ' || L_store_name || ', ' || L_cashier ||
                               ', ' || To_Char(L_business_date, 'DD-MON-YYYY');
      elsif L_register is NOT NULL then
         O_comment_key_desc := L_store || ', ' || L_store_name || ', ' || L_register ||
                               ', ' || To_Char(L_business_date, 'DD-MON-YYYY');
      else
         O_comment_key_desc := L_store || ', ' || L_store_name || ', ' || To_Char(L_business_date, 'DD-MON-YYYY');
      end if;
      ---
   else 
      O_comment_key_desc := I_comment_key;   
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END KEY_DECODE;
-------------------------------------------------------------------------------------
END SA_COMMENTS_SQL;
/