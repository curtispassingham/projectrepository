
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TENDER_TYPE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--- FUNCTION  : GET_TENDER_TYPE_DESC
--- PURPOSE   : Returns the tender type description from the pos_tender_type_head
---             table for a passed tender_type_group and tender_type_id
--------------------------------------------------------------------------------
FUNCTION GET_TENDER_TYPE_DESC(O_ERROR_MESSAGE     IN OUT VARCHAR2,
                              O_TENDER_TYPE_DESC  IN OUT POS_TENDER_TYPE_HEAD.TENDER_TYPE_DESC%TYPE,
                              I_TENDER_TYPE_GROUP IN     POS_TENDER_TYPE_HEAD.TENDER_TYPE_GROUP%TYPE,
                              I_TENDER_TYPE_ID    IN     POS_TENDER_TYPE_HEAD.TENDER_TYPE_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
END TENDER_TYPE_SQL;
/
                              