
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_ROLES_SQL AS
--------------------------------------------------------------------
FUNCTION ROLE_EXISTS(O_error_message  IN OUT VARCHAR2,
                     O_exists         IN OUT BOOLEAN,
                     I_role           IN     SA_ROLE_FIELD.ROLE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.ROLE_EXISTS';
   L_exists  VARCHAR2(1)  := 'N';
   ---
   cursor C_EXISTS is
      select 'Y'
        from sa_role_field
       where role = I_role;
   ---
BEGIN
   if I_role is NOT NULL then
      O_exists := FALSE;
      SQL_LIB.SET_MARK('OPEN','C_EXISTS','SA_ROLE_FIELD','ROLE '||I_role);
      open C_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS','SA_ROLE_FIELD','ROLE '||I_role);
      fetch C_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS','SA_ROLE_FIELD','ROLE '||I_role);
      close C_EXISTS;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   return TRUE;
   ---
EXCEPTION
      when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ROLE_EXISTS;
-----------------------------------------------------------------
FUNCTION DBA_ROLE_EXISTS(O_error_message   IN OUT VARCHAR2,
                         O_exists          IN OUT BOOLEAN,
                         I_role            IN     DBA_ROLES.ROLE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.DBA_ROLE_EXISTS';
   L_exists  VARCHAR2(1)  := 'N';
   ---
   cursor C_EXISTS is
      select 'Y'
        from dba_roles
       where role = I_role;
BEGIN
   if I_role is NOT NULL then
      O_exists := FALSE;
      SQL_LIB.SET_MARK('OPEN','C_EXISTS','DBA_ROLES','ROLE '||I_role);
      open C_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS','DBA_ROLES','ROLE '||I_role);
      fetch C_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS','DBA_ROLES','ROLE '||I_role);
      close C_EXISTS;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if; 
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
      when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DBA_ROLE_EXISTS;
--------------------------------------------------------------------
FUNCTION ROLE_FIELD_EXISTS(O_error_message   IN OUT VARCHAR2,
                           O_exists          IN OUT BOOLEAN,
                           I_role            IN     SA_ROLE_FIELD.ROLE%TYPE,
                           I_field           IN     SA_ROLE_FIELD.FIELD%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.ROLE_FIELD_EXISTS';
   L_exists  VARCHAR2(1)  := 'N';
   ---
   L_role  SA_ROLE_FIELD.ROLE%TYPE;
   cursor C_EXISTS is
      select 'Y'
        from sa_role_field
       where role  = I_role
         and field = I_field;
BEGIN
   if I_role is NOT NULL and I_field is NOT NULL then
      O_exists := FALSE;
      SQL_LIB.SET_MARK('OPEN','C_EXISTS','SA_ROLE_FIELD',NULL);
      open C_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS','SA_ROLE_FIELD',NULL);
      fetch C_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS','SA_ROLE_FIELD',NULL);
      close C_EXISTS;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
      when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ROLE_FIELD_EXISTS;
-----------------------------------------------------------------
FUNCTION CHECK_FIELD_DELETE(O_error_message   IN OUT VARCHAR2,
                            O_delete_ind      IN OUT VARCHAR2,
                            I_field           IN     SA_ROLE_FIELD.FIELD%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.CHECK_FIELD_DELETE';
   ---
   L_required_ind   CODE_DETAIL.REQUIRED_IND%TYPE;
   ---
   cursor C_CHECK_REQUIRED is
      select required_ind
        from code_detail
       where code_type = 'FLDA'
         and code      = I_Field;
BEGIN
   if I_field is NOT NULL then
      SQL_LIB.SET_MARK('OPEN','C_CHECK_REQUIRED','CODE_DETAIL',NULL);
      open C_CHECK_REQUIRED;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_REQUIRED','CODE_DETAIL',NULL);
      fetch C_CHECK_REQUIRED into L_required_ind;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_REQUIRED','CODE_DETAIL',NULL);      
      close C_CHECK_REQUIRED;
      ---
      if L_required_ind = 'Y' then
         O_delete_ind := 'N';
      else
         O_delete_ind := 'Y';
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_FIELD_DELETE;
-----------------------------------------------------------------
FUNCTION INSERT_ROLE_FIELDS(O_error_message IN OUT VARCHAR2,
                            I_role          IN     SA_ROLE_FIELD.ROLE%TYPE,
                            I_enable_ind    IN     SA_ROLE_FIELD.ENABLE_IND%TYPE,
                            I_display_ind   IN     SA_ROLE_FIELD.DISPLAY_IND%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.INSERT_ROLE_FIELDS';
   ---
BEGIN
   if I_role is NOT NULL then
      insert into sa_role_field
             (role,
              field,
              enable_ind,
              display_ind)
       select I_role,
              code,
              decode(code,'OSA','N','OST','N','BOS','N',I_enable_ind),
              I_display_ind
        from code_detail
       where code_type    = 'FLDA'
         and required_ind = 'Y';
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ROLE_FIELDS;
-----------------------------------------------------------------
FUNCTION DELETE_ROLE(O_error_message   IN OUT VARCHAR2,
                     I_role            IN     SA_ROLE_FIELD.ROLE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program       VARCHAR2(40) := 'SA_ROLES_SQL.DELETE_ROLE';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_SA_ROLE_FIELD is
      select 'x'
        from sa_role_field
       where role = I_role
         for update nowait;
BEGIN
   if I_role is NOT NULL then
      ---
      L_table := 'SA_ROLE_FIELD';
      open  C_LOCK_SA_ROLE_FIELD;
      close C_LOCK_SA_ROLE_FIELD;
      ---
      delete from sa_role_field
      where role = I_role;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_role,
                                             NULL);
      return FALSE;  
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ROLE;
-----------------------------------------------------------------
FUNCTION GET_OS_FIELD_ACCESS(O_error_message        IN OUT  VARCHAR2,
                             O_act_os_display       IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                             O_trial_os_display     IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                             O_store_value_display  IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                             O_store_value_enable   IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                             O_hq_value_display     IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                             O_hq_value_enable      IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                             I_user_id              IN      VARCHAR2 )
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.GET_OS_FIELD_ACCESS';
   ---
   L_field    SA_ROLE_FIELD.FIELD%TYPE;
   L_enable   SA_ROLE_FIELD.ENABLE_IND%TYPE;
   L_display  SA_ROLE_FIELD.DISPLAY_IND%TYPE;
   ---
   cursor C_GET_ENABLE_IND is
      select 'Y'
        from sa_role_field f,
             dba_role_privs d
       where d.granted_role = f.role
         and d.grantee      = I_user_id
         and f.field        = L_field
         and f.enable_ind   = 'Y';
   ---
   cursor C_GET_DISPLAY_IND is
      select 'Y'
        from sa_role_field f,
             dba_role_privs d
       where d.granted_role = f.role
         and d.grantee      = I_user_id
         and f.field        = L_field
         and f.display_ind = 'Y';
BEGIN
   if I_user_id  is NOT NULL then
      O_act_os_display      := 'N';
      O_trial_os_display    := 'N';
      O_store_value_display := 'N';
      O_store_value_enable  := 'N';
      O_hq_value_display    := 'N';
      O_hq_value_enable     := 'N';
      ---
      L_field   := 'OSA';
      SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      open C_GET_DISPLAY_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_DISPLAY_IND into O_act_os_display;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      close C_GET_DISPLAY_IND;
      ---
      L_field   := 'OST';
      open C_GET_DISPLAY_IND;
      fetch C_GET_DISPLAY_IND into O_trial_os_display;
      close C_GET_DISPLAY_IND;
      ---
      L_field   := 'OSS';
      SQL_LIB.SET_MARK('OPEN','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      open C_GET_ENABLE_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_ENABLE_IND into O_store_value_enable;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      close C_GET_ENABLE_IND;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      open C_GET_DISPLAY_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_DISPLAY_IND into O_store_value_display;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      close C_GET_DISPLAY_IND;
      ---
      L_field   := 'OSH';
      SQL_LIB.SET_MARK('OPEN','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      open C_GET_ENABLE_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_ENABLE_IND into O_hq_value_enable;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      close C_GET_ENABLE_IND;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      open C_GET_DISPLAY_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_DISPLAY_IND into O_hq_value_display;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      close C_GET_DISPLAY_IND;
      ---
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
  END GET_OS_FIELD_ACCESS;
-----------------------------------------------------------------
FUNCTION GET_MISC_FIELD_ACCESS(O_error_message        IN OUT  VARCHAR2,
                               O_store_value_display  IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                               O_store_value_enable   IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                               O_hq_value_display     IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                               O_hq_value_enable      IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                               I_user_id              IN      VARCHAR2 )
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.GET_MISC_FIELD_ACCESS';
   L_field    SA_ROLE_FIELD.FIELD%TYPE;
   ---
  cursor C_GET_ENABLE_IND is
      select 'Y'
        from sa_role_field f,
             dba_role_privs d
       where d.granted_role = f.role
         and d.grantee      = I_user_id
         and f.field        = L_field
         and f.enable_ind   = 'Y';
   ---
   cursor C_GET_DISPLAY_IND is
      select 'Y'
        from sa_role_field f,
             dba_role_privs d
       where d.granted_role = f.role
         and d.grantee      = I_user_id
         and f.field        = L_field
         and f.display_ind = 'Y';
BEGIN
   if I_user_id is NOT NULL then
      O_store_value_display := 'N';
      O_store_value_enable  := 'N';
      O_hq_value_display    := 'N';
      O_hq_value_enable     := 'N';
      ---
      L_field   := 'MS';
      SQL_LIB.SET_MARK('OPEN','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      open C_GET_ENABLE_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_ENABLE_IND into O_store_value_enable;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      close C_GET_ENABLE_IND;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      open C_GET_DISPLAY_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_DISPLAY_IND into O_store_value_display;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      close C_GET_DISPLAY_IND;
      ---
      L_field   := 'MH';
      SQL_LIB.SET_MARK('OPEN','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      open C_GET_ENABLE_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_ENABLE_IND into O_hq_value_enable;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      close C_GET_ENABLE_IND;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      open C_GET_DISPLAY_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_DISPLAY_IND into O_hq_value_display;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      close C_GET_DISPLAY_IND;
      ---
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_MISC_FIELD_ACCESS;
-----------------------------------------------------------------
FUNCTION GET_ERR_FIELD_ACCESS(O_error_message        IN OUT  VARCHAR2, 
                              O_store_over_display   IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                              O_store_over_enable    IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                              O_hq_over_display      IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                              O_hq_over_enable       IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                              I_user_id              IN      VARCHAR2 )
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.GET_ERR_FIELD_ACCESS';
   L_field    SA_ROLE_FIELD.FIELD%TYPE;
   ---
   cursor C_GET_ENABLE_IND is
      select 'Y'
        from sa_role_field f,
             dba_role_privs d
       where d.granted_role = f.role
         and d.grantee      = I_user_id
         and f.field        = L_field
         and f.enable_ind   = 'Y';
   ---
   cursor C_GET_DISPLAY_IND is
      select 'Y'
        from sa_role_field f,
             dba_role_privs d
       where d.granted_role = f.role
         and d.grantee      = I_user_id
         and f.field        = L_field
         and f.display_ind = 'Y';
BEGIN
   if I_user_id is NOT NULL then
      O_store_over_display := 'N';
      O_store_over_enable  := 'N';
      O_hq_over_display    := 'N';
      O_hq_over_enable     := 'N';
      ---
      L_field    := 'ESO';
      SQL_LIB.SET_MARK('OPEN','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      open C_GET_ENABLE_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_ENABLE_IND into O_store_over_enable;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      close C_GET_ENABLE_IND;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      open C_GET_DISPLAY_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_DISPLAY_IND into O_store_over_display;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      close C_GET_DISPLAY_IND;
      ---
      L_field    := 'EHO';
      SQL_LIB.SET_MARK('OPEN','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      open C_GET_ENABLE_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_ENABLE_IND into O_hq_over_enable;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      close C_GET_ENABLE_IND;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      open C_GET_DISPLAY_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_DISPLAY_IND into O_hq_over_display;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      close C_GET_DISPLAY_IND;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
   ---
END GET_ERR_FIELD_ACCESS;
-----------------------------------------------------------------
FUNCTION GET_BAL_FIELD_ACCESS(O_error_message IN OUT  VARCHAR2,
                              O_os_display    IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                              O_os_enable     IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                              I_user_id       IN      VARCHAR2 )
   RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(40) := 'SA_ROLES_SQL.GET_BAL_FIELD_ACCESS';
   L_field    SA_ROLE_FIELD.FIELD%TYPE;
   ---
   cursor C_GET_ENABLE_IND is
      select 'Y'
        from sa_role_field f,
             dba_role_privs d
       where d.granted_role = f.role
         and d.grantee      = I_user_id
         and f.field        = L_field
         and f.enable_ind   = 'Y';
   ---
   cursor C_GET_DISPLAY_IND is
      select 'Y'
        from sa_role_field f,
             dba_role_privs d
       where d.granted_role = f.role
         and d.grantee      = I_user_id
         and f.field        = L_field
         and f.display_ind = 'Y';
BEGIN
   if I_user_id is NOT NULL then
      O_os_display := 'N';
      O_os_enable  := 'N';
      ---
      L_field    := 'BOS';
      SQL_LIB.SET_MARK('OPEN','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      open C_GET_ENABLE_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_ENABLE_IND into O_os_enable;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ENABLE_IND','SA_ROLE_FIELD',NULL);
      close C_GET_ENABLE_IND;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      open C_GET_DISPLAY_IND;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      fetch C_GET_DISPLAY_IND into O_os_display;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISPLAY_IND','SA_ROLE_FIELD',NULL);
      close C_GET_DISPLAY_IND;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_BAL_FIELD_ACCESS;
-------------------------------------------------------------
END SA_ROLES_SQL;
/
