CREATE OR REPLACE PACKAGE SA_REF_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------
--Function: GET_REF_LABELS
--Purpose : Retrieve the reference labels of each transaction reference field and pass them out
--     of the function which eventually retrieve the reference label code and call the
--     LANGUAGE_SQL.GET_CODE_DESC function.  This function gets the reference label for each
--     reference number(1-27) and pass the labels out of the function.
------------------------------------------------------------------------------------------------
FUNCTION GET_REF_LABELS(O_error_message      IN OUT VARCHAR2,
         O_ref_no1      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no2      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no3      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no4      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no5      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no6      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no7      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no8      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no9      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no10     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no11     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no12     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no13     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no14     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no15     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no16     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no17     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no18     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no19     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no20     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no21     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no22     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no23     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no24     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no25     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no26     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         O_ref_no27     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
         I_tran_type    IN     SA_REFERENCE.TRAN_TYPE%TYPE,
         I_sub_tran_type      IN     SA_REFERENCE.SUB_TRAN_TYPE%TYPE,
         I_reason_code     IN     SA_REFERENCE.REASON_CODE%TYPE)
    RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function: REF_EXISTS
-- Purpose : This function will check to see if the transaction type, sub
--           trasaction type, reason code, ref. no. relationship exists
--           on the SA_REFERENCE table
-------------------------------------------------------------------------------------
FUNCTION REF_EXISTS(O_error_message  IN OUT  VARCHAR2,
          O_exists         IN OUT  BOOLEAN,
                    I_tran_type      IN      SA_REFERENCE.TRAN_TYPE%TYPE,
                    I_sub_tran_type  IN      SA_REFERENCE.SUB_TRAN_TYPE%TYPE,
                    I_reason_code    IN      SA_REFERENCE.REASON_CODE%TYPE,
                    I_ref_no         IN      SA_REFERENCE.REF_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function: GET_NEXT_REF_SEQ
-- Purpose : This function will retrieve the next available reference sequence number.
-------------------------------------------------------------------------------------
FUNCTION GET_NEXT_REF_SEQ(O_error_message IN OUT VARCHAR2,
                          O_ref_seq_no    IN OUT SA_REFERENCE.REF_SEQ_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
END SA_REF_SQL;
/
