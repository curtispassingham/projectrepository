CREATE OR REPLACE PACKAGE CORESVC_TERMS AUTHID CURRENT_USER AS

   template_key                     CONSTANT VARCHAR2(255) := 'TERMS_DATA';
   template_category                CODE_DETAIL.CODE%TYPE  := 'RMSFIN';

   action_new                       VARCHAR2(25)           := 'NEW';
   action_mod                       VARCHAR2(25)           := 'MOD';
   action_del                       VARCHAR2(25)           := 'DEL';

   FREIGHT_TERMS_sheet              VARCHAR2(255)          := 'FREIGHT_TERMS';
   FREIGHT_TERMS$Action             NUMBER                 := 1;
   FREIGHT_TERMS$FREIGHT_TERMS      NUMBER                 := 2;
   FREIGHT_TERMS$START_DT_ACTIVE    NUMBER                 := 3;
   FREIGHT_TERMS$END_DATE_ACTIVE    NUMBER                 := 4;
   FREIGHT_TERMS$ENABLED_FLAG       NUMBER                 := 5;
   FREIGHT_TERMS$TERM_DESC          NUMBER                 := 6;

   FREIGHT_TERMS_TL_sheet           VARCHAR2(255)          := 'FREIGHT_TERMS_TL';
   FREIGHT_TERMS_TL$Action          NUMBER                 := 1;
   FREIGHT_TERMS_TL$FREIGHT_TERMS   NUMBER                 := 2;
   FREIGHT_TERMS_TL$LANG            NUMBER                 := 3;
   FREIGHT_TERMS_TL$TERM_DESC       NUMBER                 := 4;

   TERMS_HEAD_sheet                 VARCHAR2(255)          := 'TERMS_HEAD';
   TERMS_HEAD$Action                NUMBER                 := 1;
   TERMS_HEAD$TERMS                 NUMBER                 := 2;
   TERMS_HEAD$RANK                  NUMBER                 := 3;
   TERMS_HEAD$TERMS_CODE            NUMBER                 := 4;
   TERMS_HEAD$TERMS_DESC            NUMBER                 := 5;

   TERMS_DETAIL_sheet               VARCHAR2(255)          := 'TERMS_DETAIL';
   TERMS_DETAIL$Action              NUMBER                 := 1;
   TERMS_DETAIL$TERMS               NUMBER                 := 2;
   TERMS_DETAIL$TERMS_SEQ           NUMBER                 := 3;
   TERMS_DETAIL$DUEDAYS             NUMBER                 := 4;
   TERMS_DETAIL$DUE_MAX_AMOUNT      NUMBER                 := 5;
   TERMS_DETAIL$DUE_DOM             NUMBER                 := 6;
   TERMS_DETAIL$DUE_MM_FWD          NUMBER                 := 7;
   TERMS_DETAIL$DISCDAYS            NUMBER                 := 8;
   TERMS_DETAIL$PERCENT             NUMBER                 := 9;
   TERMS_DETAIL$DISC_DOM            NUMBER                 := 10;
   TERMS_DETAIL$DISC_MM_FWD         NUMBER                 := 11;
   TERMS_DETAIL$FIXED_DATE          NUMBER                 := 12;
   TERMS_DETAIL$ENABLED_FLAG        NUMBER                 := 13;
   TERMS_DETAIL$START_DATE_ACTIVE   NUMBER                 := 14;
   TERMS_DETAIL$END_DATE_ACTIVE     NUMBER                 := 15;
   TERMS_DETAIL$CUTOFF_DAY          NUMBER                 := 16;

   TERMS_HEAD_TL_sheet              VARCHAR2(255)          := 'TERMS_HEAD_TL';
   TERMS_HEAD_TL$Action             NUMBER                 := 1;
   TERMS_HEAD_TL$TERMS              NUMBER                 := 2;
   TERMS_HEAD_TL$LANG               NUMBER                 := 3;
   TERMS_HEAD_TL$TERMS_CODE         NUMBER                 := 4;
   TERMS_HEAD_TL$TERMS_DESC         NUMBER                 := 5;

   TYPE FREIGHT_TERMS_rec_tab IS TABLE OF FREIGHT_TERMS%ROWTYPE;
   TYPE TERMS_HEAD_rec_tab IS TABLE OF TERMS_HEAD%ROWTYPE;
   TYPE TERMS_DETAIL_rec_tab IS TABLE OF TERMS_DETAIL%ROWTYPE;

   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
--------------------------------------------------------------------------------   
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------   
END CORESVC_TERMS;
/
