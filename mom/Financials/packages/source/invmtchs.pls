CREATE OR REPLACE PACKAGE INVC_MATCH_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------
--- Function: CHECK_ASSOC
--- Purpose:  This function will check if the shipment being received is or should be
---           associated with any existing invoices.
--- Calls:    None
--- Created:  13-NOV-98, Sonia Wong
-----------------------------------------------------------------------------------------
FUNCTION CHECK_ASSOC(O_error_message   IN OUT   VARCHAR2,
                     I_rcpt            IN       shipment.shipment%TYPE)
                     return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: CHECK_TOLERANCE
--- Purpose:  This function will check to see if an invoice value falls within the specified
---           tolerance of a receipt value.
--- Calls:    None
--- Created:  30-OCT-98, Sonia Wong
-----------------------------------------------------------------------------------------
FUNCTION CHECK_TOLERANCE(O_error_message     IN OUT   VARCHAR2,
                         O_in_tolerance      IN OUT   BOOLEAN,
                         I_supplier          IN       sup_tolerance.supplier%TYPE,
                         I_tolerance_level   IN       sup_tolerance.tolerance_level%TYPE,
                         I_invc_value        IN       invc_head.total_merch_cost%TYPE,
                         I_rcpt_value        IN       invc_head.total_merch_cost%TYPE)
                         return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: CHECK_DETAILS
--- Purpose:  This function will check that the sums across all detail records of total
---           cost and total quantity (if total quantity is entered) equal the totals
---           entered at the header level. An invoice cannot be matched until these
--            figures are reconciled.
--- Calls:    None
--- Created:  28-OCT-98, Sonia Wong
-----------------------------------------------------------------------------------------
FUNCTION CHECK_DETAILS(O_error_message   IN OUT   VARCHAR2,
                       O_reconciled      IN OUT   BOOLEAN,
                       I_invc_id         IN       invc_head.invc_id%TYPE)
                       return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: CHECK_VAT
--- Purpose:  This function will check VAT records when summary matching.
-----------------------------------------------------------------------------------------
FUNCTION CHECK_VAT(O_error_message   IN OUT   VARCHAR2,
                   O_reconciled      IN OUT   BOOLEAN,
                   I_invc_id         IN       invc_head.invc_id%TYPE,
                   I_supplier        IN       invc_head.supplier%TYPE)
                   return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: ITEM_MATCH_ALL
--- Purpose:  This function will attempt to match all items on an invoice to the receipt that
---           has been associated with it on the INVC_MATCH_WKSHT table.
--- Calls:    INVC_MATCH_SQL.ITEM_MATCH
--- Created:  10-NOV-98, Sonia Wong
-----------------------------------------------------------------------------------------
FUNCTION ITEM_MATCH_ALL(O_error_message   IN OUT   VARCHAR2,
                        O_match           IN OUT   BOOLEAN,
                        IO_reconciled     IN OUT   BOOLEAN,
                        I_invc_id         IN       invc_head.invc_id%TYPE,
                        I_user_id         IN       VARCHAR2,
                        I_supplier        IN       invc_head.supplier%TYPE,
                        I_vat_region      IN       vat_item.vat_region%TYPE)
                        return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: ITEM_MATCH
--- Purpose:  This function will attempt to match an invoice line item.
--- Calls:    None
--- Created:  30-OCT-98, Sonia Wong
-----------------------------------------------------------------------------------------
FUNCTION ITEM_MATCH(O_error_message   IN OUT   VARCHAR2,
                    O_match           IN OUT   BOOLEAN,
                    IO_reconciled     IN OUT   BOOLEAN,
                    I_invc_id         IN       invc_match_wksht.invc_id%TYPE,
                    I_user_id         IN       VARCHAR2,
                    I_item            IN       invc_match_wksht.item%TYPE,
                    I_invc_cost       IN       invc_detail.invc_unit_cost%TYPE,
                    I_invc_qty        IN       invc_detail.invc_qty%TYPE,
                    I_supplier        IN       invc_head.supplier%TYPE,
                    I_single_call     IN       BOOLEAN,
                    I_vat_region      IN       vat_item.vat_region%TYPE,
                    I_invc_vat_rate   IN       invc_detail.invc_vat_rate%TYPE,
                    I_rcpt_vat_rate   IN       vat_item.vat_rate%TYPE)
                    return BOOLEAN;
--------------------------------------------------------------------------------------------
--- Function: UNMATCH
--- Purpose:  This function will unmatch the invoice at line item, receipt or total invoice
---           level.
--- Calls:    None
--- Created:  29-OCT-98, Sonia Wong
-----------------------------------------------------------------------------------------
FUNCTION UNMATCH(O_error_message   IN OUT   VARCHAR2,
                 O_status          IN OUT   invc_head.status%TYPE,
                 I_invc_id         IN       invc_head.invc_id%TYPE,
                 I_rcpt            IN       shipment.shipment%TYPE,
                 I_item            IN       invc_detail.item%TYPE)
                 return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: APPROVE
--- Purpose:  This function will approve an invoice for release to the financial staging
---           tables.
--- Calls:    None
--- Created:  28-OCT-98, Sonia Wong
--- Modified: 20-JAN-2000, Hien Tran
-----------------------------------------------------------------------------------------
FUNCTION APPROVE(O_error_message     IN OUT   VARCHAR2,
                 O_service_perf_fail IN OUT   BOOLEAN,
                 I_invc_id           IN       invc_head.invc_id%TYPE,
                 I_user_id           IN       VARCHAR2)
                 return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: UNAPPROVE
--- Purpose:  This function will unapprove an approved invoice.
--- Calls:    INVC_MATCH_SQL.UNMATCH
--- Created:  29-OCT-98, Sonia Wong
-----------------------------------------------------------------------------------------
FUNCTION UNAPPROVE(O_error_message   IN OUT   VARCHAR2,
                   I_invc_id         IN       invc_head.invc_id%TYPE,
                   I_unmatch         IN       BOOLEAN)
                   return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: TOTAL_MATCH
--- Purpose:  Attempts a total match for an invoice and its associated receipts.
--- Calls:    INVC_SQL.UPDATE_STATUSES, INVC_MATCH_SQL.CHECK_DETAILS
---           INVC_MATCH_SQL.CHECK_TOLERANCE
--- Created:  18-NOV-98, Ryan McNamara
--- Modified: 20-JAN-2000, Hien Tran
-----------------------------------------------------------------------------------------
FUNCTION TOTAL_MATCH(O_error_message         IN OUT   VARCHAR2,
                     O_match                 IN OUT   BOOLEAN,
                     O_reconciled            IN OUT   BOOLEAN,
                     O_invc_status           IN OUT   invc_head.status%TYPE,
                     O_service_perf_fail     IN OUT   BOOLEAN,
                     I_invc_id               IN       invc_head.invc_id%TYPE,
                     I_user_id               IN       VARCHAR2)
return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: CHECK_VAT_RATES
--- Purpose:  Validates that the invoice detail level vat rates are valid at the header level.
-----------------------------------------------------------------------------------------
FUNCTION CHECK_VAT_RATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_invc_id         IN       INVC_DETAIL.INVC_ID%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------------
--- Function: CHECK_VAT_CODE
--- Purpose:  Validates an input vat_code for an input item.
-----------------------------------------------------------------------------------------
FUNCTION CHECK_VAT_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid           IN OUT   VARCHAR2,
                        I_invc_id         IN       INVC_DETAIL.INVC_ID%TYPE,
                        I_vat_rate        IN       INVC_DETAIL.ORIG_VAT_RATE%TYPE,
                        I_invc_date       IN       INVC_HEAD.INVC_DATE%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------------
END INVC_MATCH_SQL;
/