CREATE OR REPLACE PACKAGE WF_SALES_UPLOAD_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-- Function Name: PROCESS_SALES
-- Purpose      : Public function that calls the ROLLUP_WF_SALES and CHUNK_ROLLUP
--                functions to populate the WFSLSUPLD_ROLLUP table and to 
--                divide up the data into more manageable chunks for commits. 
-------------------------------------------------------------------------------
FUNCTION PROCESS_SALES(O_error_message          IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)   
  RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: WF_PURGE_NON_STOCK_SALES_HIST
-- Purpose      : Public function that will check if the records on the 
--                WF_NON_STOCKHOLDING_SALES table have exceeded the history days
--                specified for storing the information. Any record whose report
--                date has passed the history days will be deleted. 
-------------------------------------------------------------------------------
FUNCTION WF_PURGE_NON_STOCK_SALES_HIST(O_error_message     IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: VALIDATE_DETAIL
-- Purpose      : Public function that performs the necessary validations on the 
--                ITEM, NET_SALES_QTY_UOM and TOTAL_RETAIL_AMT_CURR fields to
--                to verify that they can be used in RMS.
-------------------------------------------------------------------------------
FUNCTION VALIDATE_DETAIL(O_error_message     IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                         I_chunk_id          IN            WFSLSUPLD_STAGING.CHUNK_ID%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: CHUNK_STAGING
-- Purpose      : Public function that will divide up the detail records in the
--                WFSLSUPLD_STAGING table into more manageable chunks for 
--                commits. It will also populate the customer loc and 
--                report date fields in WFSLSUPLD_STAGING.
-------------------------------------------------------------------------------
FUNCTION CHUNK_STAGING(O_error_message     IN OUT NOCOPY     RTK_ERRORS.RTK_TEXT%TYPE,
                       I_customer_loc      IN                WFSLSUPLD_STAGING.CUSTOMER_LOC%TYPE,
                       I_report_date       IN                WFSLSUPLD_STAGING.REPORT_DATE%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: LOAD_WF_SALES
-- Purpose      : Public function that will load all the valid non-stockholding
--                franchise store sales information into the WF_NON_STOCKHOLDING_SALES
--                table. 
-------------------------------------------------------------------------------
FUNCTION LOAD_WF_SALES(O_error_message     IN OUT NOCOPY     RTK_ERRORS.RTK_TEXT%TYPE,
                       I_chunk_id          IN                WFSLSUPLD_ROLLUP.CHUNK_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: PURGE_REJECTS
-- Purpose      : This public function will delete records with errors from
--                WFSLSUPLD_STAGING table. This is after the records have 
--                been written out to a reject file.
-------------------------------------------------------------------------------
FUNCTION PURGE_REJECTS(O_error_message    IN OUT NOCOPY      RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rej_filename     IN                 VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
$if $$UTPLSQL=TRUE $then
   -------------------------------------------------------------------------------
   -- Function Name: ROLLUP_WF_SALES
   -- Purpose      : Sums up the NET_SALES_QTY and TOTAL_RETAIL_AMT values for a 
   --                loc/report date/item combination.
   -------------------------------------------------------------------------------
   FUNCTION ROLLUP_WF_SALES(O_error_message          IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)
      RETURN BOOLEAN;
   -------------------------------------------------------------------------------
   -- Function Name: CHUNK_ROLLUP
   -- Purpose      : Divides the records in the WFSLSUPLD_ROLLUP table into chunks of 
   --                data that will better accommodate commiting records. 
   -------------------------------------------------------------------------------
   FUNCTION CHUNK_ROLLUP(O_error_message     IN OUT NOCOPY     RTK_ERRORS.RTK_TEXT%TYPE)
      RETURN BOOLEAN;
   -------------------------------------------------------------------------------
$end
-------------------------------------------------------------------------------
END WF_SALES_UPLOAD_SQL;
/
