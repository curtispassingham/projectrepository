CREATE OR REPLACE PACKAGE BODY SA_CC_VALIDATE_SQL AS
-----------------------------------------------------------------------------------
FUNCTION CHECK_ALL_NUMBERS(O_error_message IN OUT VARCHAR2,
                           I_card_no       IN     SA_TRAN_TENDER.CC_NO%TYPE)
   RETURN BOOLEAN IS

   L_number_length NUMBER(2)    := NULL;
   L_program       VARCHAR2(50) := 'SA_CC_VALIDATE_SQL.CHECK_ALL_NUMBERS';
BEGIN

   L_number_length := LENGTH(I_card_no);

   WHILE (L_number_length > 0) LOOP
      if ((ASCII(SUBSTR(I_card_no, L_number_length, 1)) > 57 or
           ASCII(SUBSTR(I_card_no, L_number_length, 1)) < 48)) then

         O_error_message := SQL_LIB.GET_MESSAGE_TEXT('NON_NUMERIC',
                                                     NULL,
                                                     NULL,
                                                     NULL);
         return FALSE;
      end if;

      L_number_length := L_number_length -1;
   END LOOP;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ALL_NUMBERS;
-----------------------------------------------------------------------------------
FUNCTION MASK_CARD_NO(O_error_message IN OUT VARCHAR2,
                      O_mask_card_no  IN OUT SA_TRAN_TENDER.CC_NO%TYPE,
                      I_card_no       IN     SA_TRAN_TENDER.CC_NO%TYPE)
   RETURN BOOLEAN IS

   L_length          NUMBER(2) := 0;
   L_no_of_mask_char NUMBER(2) := 0;
   L_first_part      VARCHAR2(40) := NULL;
   L_mask_char       SA_SYSTEM_OPTIONS.CC_NO_MASK_CHAR%TYPE := NULL;
   L_number_length   NUMBER(2) := 0;
   L_program         VARCHAR2(50) := 'SA_CC_VALIDATE_SQL.MASK_CARD_NO';

BEGIN

   L_number_length := LENGTH(I_card_no);

   -- Get the masking character from sa_system_options
   if SA_SYSTEM_OPTIONS_SQL.GET_CC_NO_MASK_CHAR (O_error_message,
                                                 L_mask_char) = FALSE then
      return FALSE;
   end if;

   -- If the credit card number has more than 10 digits then mask the digits between
   -- the first 6 and last 4 digits
   if L_number_length > 10 then
      L_no_of_mask_char := L_number_length - 10;
      L_length := 0;
      WHILE L_length < L_no_of_mask_char
      LOOP
         if L_first_part is NULL then
            L_first_part :=  SUBSTR(I_card_no,1,6);
         end if;
         --
         L_first_part := L_first_part||L_mask_char;
         --
         L_length := L_length + 1;
      END LOOP;
      O_mask_card_no := L_first_part||SUBSTR(I_card_no,-4);
   -- If the credit card number has 10 digits then mask the 5th and 6th digits
   elsif L_number_length = 10 then
      L_no_of_mask_char := L_number_length - 8;
      L_length := 0;
      WHILE L_length < L_no_of_mask_char
      LOOP
         if L_first_part is NULL then
            L_first_part :=  SUBSTR(I_card_no,1,4);
         end if;
         --
         L_first_part := L_first_part||L_mask_char;
         --
         L_length := L_length + 1;
      END LOOP;
      O_mask_card_no := L_first_part||SUBSTR(I_card_no,-4);
   else
      O_mask_card_no := I_card_no;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASK_CARD_NO;
-----------------------------------------------------------------------------------
END SA_CC_VALIDATE_SQL;
/
