CREATE OR REPLACE PACKAGE BODY SA_MASS_ERROR_SQL AS
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_DELETE
-- Purpose: This function will be called by the MASS_UPD_DEL function if fix is to delete record/s.
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_ind          OUT   BOOLEAN,
                     I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                     I_key_value_1     IN       SA_ERROR.KEY_VALUE_1%TYPE DEFAULT NULL,
                     I_key_value_2     IN       SA_ERROR.KEY_VALUE_2%TYPE DEFAULT NULL,
                     I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                     I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                     I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                     I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                     I_table           IN       SA_ERROR_CODES.ERROR_FIX_TABLE%TYPE,
                     I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_UPDATE
-- Purpose: This function will be called by the MASS_UPD_DEL function if fix is to update a
--          certain field in a record/s.
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_ind          OUT   BOOLEAN,
                     I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                     I_key_value_1     IN       SA_ERROR.KEY_VALUE_1%TYPE DEFAULT NULL,
                     I_key_value_2     IN       SA_ERROR.KEY_VALUE_2%TYPE DEFAULT NULL,
                     I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                     I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                     I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                     I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                     I_table           IN       SA_ERROR_CODES.ERROR_FIX_TABLE%TYPE,
                     I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                     I_new_value       IN       VARCHAR2,
                     I_dependency      IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_ITEM_DELETE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_DELETE if the fix is to delete an ITEM record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                          I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                          I_store           IN       SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                          I_day             IN       SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL,
                          I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                          I_error_code      IN       SA_ERROR_CODES.ERROR_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_ITEM_DISCOUNTS_DELETE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_DELETE if the fix is to delete an
--          ITEM-DISCOUNT record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_DISCOUNTS_DELETE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_seq_no       IN       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                    I_item_seq_no       IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                    I_discount_seq_no   IN       SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE DEFAULT NULL,
                                    I_store             IN       SA_TRAN_DISC.STORE%TYPE DEFAULT NULL,
                                    I_day               IN       SA_TRAN_DISC.DAY%TYPE DEFAULT NULL,
                                    I_business_date     IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                    I_error_code        IN       SA_ERROR_CODES.ERROR_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_ITEM_IGTAX_DELETE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_DELETE if the fix is to delete an ITEM-TAX record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_IGTAX_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tran_seq_no     IN       SA_TRAN_IGTAX.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                I_item_seq_no     IN       SA_TRAN_IGTAX.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                I_igtax_seq_no    IN       SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE DEFAULT NULL,
                                I_store           IN       SA_TRAN_IGTAX.STORE%TYPE DEFAULT NULL,
                                I_day             IN       SA_TRAN_IGTAX.DAY%TYPE DEFAULT NULL,
                                I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                I_error_code      IN       SA_ERROR_CODES.ERROR_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_PAYMENT_DELETE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_DELETE if the fix is to delete an ITEM record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_PAYMENT_DELETE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tran_seq_no      IN       SA_TRAN_PAYMENT.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                             I_payment_seq_no   IN       SA_TRAN_PAYMENT.PAYMENT_SEQ_NO%TYPE DEFAULT NULL,
                             I_store            IN       SA_TRAN_PAYMENT.STORE%TYPE DEFAULT NULL,
                             I_day              IN       SA_TRAN_PAYMENT.DAY%TYPE DEFAULT NULL,
                             I_business_date    IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                             I_error_code       IN       SA_ERROR_CODES.ERROR_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_TENDER_DELETE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_DELETE if the fix is to delete a TENDER-TYPE record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TENDER_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tran_seq_no     IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                            I_tender_seq_no   IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE DEFAULT NULL,
                            I_store           IN       SA_TRAN_TENDER.STORE%TYPE DEFAULT NULL,
                            I_day             IN       SA_TRAN_TENDER.DAY%TYPE DEFAULT NULL,
                            I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                            I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_TAX_DELETE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_DELETE if the fix is to delete a TTAX record -
--          Tax Level Error
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TAX_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tran_seq_no     IN       SA_TRAN_TAX.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                         I_tax_seq_no      IN       SA_TRAN_TAX.TAX_SEQ_NO%TYPE DEFAULT NULL,
                         I_store           IN       SA_TRAN_TAX.STORE%TYPE DEFAULT NULL,
                         I_day             IN       SA_TRAN_TAX.DAY%TYPE DEFAULT NULL,
                         I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                         I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_CUSTOMER_DELETE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_DELETE if the fix is for TCUST record -
--          Customer Level Error
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_CUSTOMER_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tran_seq_no     IN       SA_CUSTOMER.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                              I_store           IN       SA_CUSTOMER.STORE%TYPE DEFAULT NULL,
                              I_day             IN       SA_CUSTOMER.DAY%TYPE DEFAULT NULL,
                              I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                              I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_CUST_ATTR_DELETE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_DELETE if the fix is for CATT record -
--          Customer Attribute
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_CUST_ATTR_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tran_seq_no     IN       SA_CUST_ATTRIB.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                               I_attrib_seq_no   IN       SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE DEFAULT NULL,
                               I_store           IN       SA_CUST_ATTRIB.STORE%TYPE DEFAULT NULL,
                               I_day             IN       SA_CUST_ATTRIB.DAY%TYPE DEFAULT NULL,
                               I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                               I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- This private function will be called by the MASS_ITEM_DELETE function for data dependencies.
----------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_MASS_ITEM_DISCOUNTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                    I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                    I_store           IN       SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                                    I_day             IN       SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- This private function will be called by the MASS_ITEM_DELETE function for data dependencies.
----------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_MASS_ITEM_IGTAX(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                I_store           IN       SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                                I_day             IN       SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_ITEM_UPDATE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_UPDATE if the fix is for a certain record in
--          an ITEM record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_ind          OUT   BOOLEAN,
                          I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                          I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                          I_store           IN       SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                          I_day             IN       SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL,
                          I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                          I_error_code      IN       SA_ERROR_CODES.ERROR_CODE%TYPE,
                          I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                          I_new_value       IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_ITEM_DISCOUNTS_UPDATE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_UPDATE if the fix is for a certain field in
--          an ITEM-DISCOUNT record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_DISCOUNTS_UPDATE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_seq_no       IN       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                    I_item_seq_no       IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                    I_discount_seq_no   IN       SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE DEFAULT NULL,
                                    I_store             IN       SA_TRAN_DISC.STORE%TYPE DEFAULT NULL,
                                    I_day               IN       SA_TRAN_DISC.DAY%TYPE DEFAULT NULL,
                                    I_business_date     IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                    I_error_code        IN       SA_ERROR_CODES.ERROR_CODE%TYPE,
                                    I_fix_column        IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                                    I_new_value         IN       VARCHAR2,
                                    I_promotion         IN       SA_TRAN_DISC.PROMOTION%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_ITEM_IGTAX_UPDATE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_UPDATE if the fix is for a certain field in
--          an ITEM-TAX record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_IGTAX_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tran_seq_no     IN       SA_TRAN_IGTAX.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                I_item_seq_no     IN       SA_TRAN_IGTAX.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                I_igtax_seq_no    IN       SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE DEFAULT NULL,
                                I_store           IN       SA_TRAN_IGTAX.STORE%TYPE DEFAULT NULL,
                                I_day             IN       SA_TRAN_IGTAX.DAY%TYPE DEFAULT NULL,
                                I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                I_error_code      IN       SA_ERROR_CODES.ERROR_CODE%TYPE,
                                I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                                I_new_value       IN       VARCHAR2)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_TENDER_UPDATE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_UPDATE if the fix is for a certain field in
--          a TENDER-TYPE record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TENDER_UPDATE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tran_seq_no         IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                            I_tender_seq_no       IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE DEFAULT NULL,
                            I_store               IN       SA_TRAN_TENDER.STORE%TYPE DEFAULT NULL,
                            I_day                 IN       SA_TRAN_TENDER.DAY%TYPE DEFAULT NULL,
                            I_business_date       IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                            I_error_code          IN       SA_ERROR.ERROR_CODE%TYPE,
                            I_fix_column          IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                            I_new_value           IN       VARCHAR2,
                            I_tender_type_group   IN   SA_TRAN_TENDER.TENDER_TYPE_GROUP%TYPE)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_TAX_UPDATE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_UPDATE if the fix is for a certain field in
--          TTAX record - Tax Level Error
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TAX_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tran_seq_no     IN       SA_TRAN_TAX.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                         I_tax_seq_no      IN       SA_TRAN_TAX.TAX_SEQ_NO%TYPE DEFAULT NULL,
                         I_store           IN       SA_TRAN_TAX.STORE%TYPE DEFAULT NULL,
                         I_day             IN       SA_TRAN_TAX.DAY%TYPE DEFAULT NULL,
                         I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                         I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                         I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                         I_new_value       IN       VARCHAR2)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_CUSTOMER_UPDATE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_UPDATE if the fix is for a certain field in
--          TCUST record - Customer Level Error
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_CUSTOMER_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tran_seq_no     IN       SA_CUSTOMER.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                              I_store           IN       SA_CUSTOMER.STORE%TYPE DEFAULT NULL,
                              I_day             IN       SA_CUSTOMER.DAY%TYPE DEFAULT NULL,
                              I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                              I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                              I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                              I_new_value       IN       VARCHAR2)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_CUST_ATTR_UPDATE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_UPDATE if the fix is for a certain field in
--          CATT record - Customer Attribute
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_CUST_ATTR_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tran_seq_no     IN       SA_CUST_ATTRIB.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                               I_attrib_seq_no   IN       SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE DEFAULT NULL,
                               I_store           IN       SA_CUST_ATTRIB.STORE%TYPE DEFAULT NULL,
                               I_day             IN       SA_CUST_ATTRIB.DAY%TYPE DEFAULT NULL,
                               I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                               I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                               I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                               I_new_value       IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: MASS_TRAN_HEAD_UPDATE
-- Purpose: This function will be called by SA_MASS_ERROR_SQL.MASS_UPDATE if the fix is for a certain field in
--          THEAD record - Header Record
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TRAN_HEAD_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                               I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                               I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                               I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                               I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                               I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                               I_new_value       IN       VARCHAR2)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ITEM_INFO
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_INFO(O_error_message   IN OUT   VARCHAR2,
                           O_error_ind          OUT   BOOLEAN,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_item_type       IN       SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                           I_sub_tran_type   IN       SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                           I_qty             IN       SA_TRAN_ITEM.QTY%TYPE,
                           I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                           I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                           I_store           IN       SA_TRAN_ITEM.STORE%TYPE,
                           I_day             IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_POST_ITEM_INFO
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_POST_ITEM_INFO(O_error_message         IN OUT   VARCHAR2,
                                I_ref_item              IN       ITEM_MASTER.ITEM%TYPE,
                                I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                I_dept                  IN       ITEM_MASTER.DEPT%TYPE,
                                I_class                 IN       ITEM_MASTER.CLASS%TYPE,
                                I_subclass              IN       ITEM_MASTER.SUBCLASS%TYPE,
                                I_waste_type            IN       ITEM_MASTER.WASTE_TYPE%TYPE,
                                I_waste_pct             IN       ITEM_MASTER.WASTE_PCT%TYPE,
                                I_taxable_ind           IN       ITEM_LOC.TAXABLE_IND%TYPE,
                                I_selling_unit_retail   IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                I_unit_retail           IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                I_selling_uom           IN       ITEM_LOC.SELLING_UOM%TYPE,
                                I_orig_item             IN       ITEM_MASTER.ITEM%TYPE,
                                I_item_type             IN       SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                                I_sub_tran_type         IN       SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                                I_qty                   IN       SA_TRAN_ITEM.QTY%TYPE,
                                I_tran_seq_no           IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                I_item_seq_no           IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                I_store                 IN       SA_TRAN_ITEM.STORE%TYPE,
                                I_day                   IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REMOVE_ERRORS
----------------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_ERRORS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store           IN       SA_TRAN_HEAD.STORE%TYPE,
                       I_day             IN       SA_TRAN_HEAD.DAY%TYPE,
                       I_tran_type       IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REFRESH_ERRORS
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                        I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                        I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                        I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                        I_tran_type          IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                        I_rtlog_orig_sys     IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                        I_tran_process_sys   IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                        I_record_name        IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REFRESH_ITEM_ERRORS
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_ITEM_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                             I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                             I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                             I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                             I_tran_type          IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                             I_rtlog_orig_sys     IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                             I_tran_process_sys   IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                             I_record_name        IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REFRESH_CUSTORD_ERRORS
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_CUSTORD_ERRORS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_store_day_seq_no     IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                I_tran_seq_no          IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                I_tran_type            IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                                I_rtlog_orig_sys       IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                                I_tran_process_sys     IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                                I_item_seq_no          IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                I_return_wh            IN       SA_TRAN_ITEM.RETURN_WH%TYPE,
                                I_sales_type           IN       SA_TRAN_ITEM.SALES_TYPE%TYPE,
                                I_item_status          IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                I_fulfill_order_no     IN       SA_TRAN_ITEM.FULFILL_ORDER_NO%TYPE,
                                I_return_disposition   IN       SA_TRAN_ITEM.RETURN_DISPOSITION%TYPE,
                                I_no_inv_ret_ind       IN       SA_TRAN_ITEM.NO_INV_RET_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REFRESH_TENDER_ERRORS
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_TENDER_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                               I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                               I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                               I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                               I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                               I_tran_type          IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                               I_record_name        IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REFRESH_TAX_ERRORS
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_TAX_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                            I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                            I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                            I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                            I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                            I_record_name        IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REFRESH_IGTAX_ERRORS
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_IGTAX_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                              I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                              I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                              I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                              I_record_name        IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REFRESH_HEADER_ERROR
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_HEADER_ERROR(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error_ind          IN OUT   SA_TRAN_HEAD.ERROR_IND%TYPE,
                              I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                              I_tran_type          IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                              I_sub_tran_type      IN       SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                              I_orig_tran_type     IN       SA_TRAN_HEAD.ORIG_TRAN_TYPE%TYPE,
                              I_reason_code        IN       SA_TRAN_HEAD.REASON_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ITEM_SALES_TYPE
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_SALES_TYPE(O_error_message      IN OUT   VARCHAR2,
                                 O_error_ind             OUT   BOOLEAN,
                                 I_sales_type         IN       SA_TRAN_ITEM.SALES_TYPE%TYPE,
                                 I_item_status        IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                 I_pos_tran_ind       IN       SA_TRAN_HEAD.POS_TRAN_IND%TYPE,
                                 I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                 I_tran_seq_no        IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                 I_item_seq_no        IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                 I_store              IN       SA_TRAN_ITEM.STORE%TYPE,
                                 I_day                IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: POST_ITEM_TYPE_CHANGED
----------------------------------------------------------------------------------------------------------------
FUNCTION POST_ITEM_TYPE_CHANGED(O_error_message   IN OUT   VARCHAR2,
                                I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                I_store           IN       SA_TRAN_ITEM.STORE%TYPE,
                                I_day             IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ITEM_RETURN_DISP
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_RETURN_DISP(O_error_message        IN OUT   VARCHAR2,
                                  O_error_ind               OUT   BOOLEAN,
                                  I_rtlog_orig_sys       IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                                  I_tran_process_sys     IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                                  I_return_disposition   IN       SA_TRAN_ITEM.RETURN_DISPOSITION%TYPE,
                                  I_sales_type           IN       SA_TRAN_ITEM.SALES_TYPE%TYPE,
                                  I_item_status          IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                  I_no_inv_ret_ind       IN       SA_TRAN_ITEM.NO_INV_RET_IND%TYPE,
                                  I_store_day_seq_no     IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                  I_tran_seq_no          IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                  I_item_seq_no          IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                  I_store                IN       SA_TRAN_ITEM.STORE%TYPE,
                                  I_day                  IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ITEM_RETURN_WH
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_RETURN_WH(O_error_message      IN OUT   VARCHAR2,
                                O_error_ind             OUT   BOOLEAN,
                                I_rtlog_orig_sys     IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                                I_tran_process_sys   IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                                I_return_wh          IN       SA_TRAN_ITEM.RETURN_WH%TYPE,
                                I_sales_type         IN       SA_TRAN_ITEM.SALES_TYPE%TYPE,
                                I_item_status        IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                I_tran_seq_no        IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                I_item_seq_no        IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                I_store              IN       SA_TRAN_ITEM.STORE%TYPE,
                                I_day                IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: TOTAL_AUDIT_CHECK
----------------------------------------------------------------------------------------------------------------
FUNCTION TOTAL_AUDIT_CHECK(O_error_message      IN OUT   VARCHAR2,
                           I_audit_status       IN       SA_STORE_DAY.AUDIT_STATUS%TYPE,
                           I_data_status        IN       SA_STORE_DAY.DATA_STATUS%TYPE,
                           I_store_day_seq_no   IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_store              IN       SA_STORE_DAY.STORE%TYPE,
                           I_day                IN       SA_STORE_DAY.DAY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function Name: REFRESH_DISCOUNT_ERRORS
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_DISCOUNT_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_ind          IN OUT   SA_TRAN_DISC.ERROR_IND%TYPE,
                                 I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                                 I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                 I_tran_seq_no        IN       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE,
                                 I_item_seq_no        IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE,
                                 I_discount_seq_no    IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE,
                                 I_store              IN       SA_TRAN_DISC.STORE%TYPE,
                                 I_day                IN       SA_TRAN_DISC.DAY%TYPE,
                                 I_disc_type          IN       SA_TRAN_DISC.DISC_TYPE%TYPE,
                                 I_coupon_no          IN       SA_TRAN_DISC.COUPON_NO%TYPE,
                                 I_rms_promo_type     IN       SA_TRAN_DISC.RMS_PROMO_TYPE%TYPE,
                                 I_promotion          IN       SA_TRAN_DISC.PROMOTION%TYPE,
                                 I_promo_comp         IN       SA_TRAN_DISC.PROMO_COMP%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_UPD_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_error_ind          OUT   BOOLEAN,
                      I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                      I_key_value_1     IN       SA_ERROR.KEY_VALUE_1%TYPE DEFAULT NULL,
                      I_key_value_2     IN       SA_ERROR.KEY_VALUE_2%TYPE DEFAULT NULL,
                      I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                      I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                      I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                      I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                      I_pop_up_type     IN       SA_ERROR_CODES.MASS_RES_POP_UP_TYPE%TYPE,
                      I_table           IN       SA_ERROR_CODES.ERROR_FIX_TABLE%TYPE,
                      I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                      I_new_value       IN       VARCHAR2,
                      I_dependency      IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_UPD_DEL';
BEGIN

   if I_pop_up_type = 'POP_UP_REPLACE' then
      if SA_MASS_ERROR_SQL.MASS_UPDATE(O_error_message,
                                       O_error_ind,
                                       I_tran_seq_no,
                                       I_key_value_1,
                                       I_key_value_2,
                                       I_store,
                                       I_day,
                                       I_business_date,
                                       I_error_code,
                                       I_table,
                                       I_fix_column,
                                       I_new_value,
                                       I_dependency) = FALSE then
         return FALSE;
      end if;
   elsif I_pop_up_type = 'POP_UP_DEL' then
      if SA_MASS_ERROR_SQL.MASS_DELETE(O_error_message,
                                       O_error_ind,
                                       I_tran_seq_no,
                                       I_key_value_1,
                                       I_key_value_2,
                                       I_store,
                                       I_day,
                                       I_business_date,
                                       I_error_code,
                                       I_table,
                                       I_fix_column) = FALSE then
         return FALSE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_UPD_DEL;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_ind          OUT   BOOLEAN,
                     I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                     I_key_value_1     IN       SA_ERROR.KEY_VALUE_1%TYPE DEFAULT NULL,
                     I_key_value_2     IN       SA_ERROR.KEY_VALUE_2%TYPE DEFAULT NULL,
                     I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                     I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                     I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                     I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                     I_table           IN       SA_ERROR_CODES.ERROR_FIX_TABLE%TYPE,
                     I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_DELETE';
BEGIN

   if I_fix_column is not NULL then
      if SA_MASS_ERROR_SQL.MASS_UPDATE(O_error_message,
                                       O_error_ind,
                                       I_tran_seq_no,
                                       I_key_value_1,
                                       I_key_value_2,
                                       I_store,
                                       I_day,
                                       I_business_date,
                                       I_error_code,
                                       I_table,
                                       I_fix_column,
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
      end if;
   else
      if I_table = 'SA_TRAN_ITEM' then
         if SA_MASS_ERROR_SQL.MASS_ITEM_DELETE(O_error_message,
                                               I_tran_seq_no,
                                               I_key_value_1,
                                               I_store,
                                               I_day,
                                               I_business_date,
                                               I_error_code) = FALSE then
            return FALSE;
         end if;
      elsif I_table = 'SA_TRAN_DISC' then
         if SA_MASS_ERROR_SQL.MASS_ITEM_DISCOUNTS_DELETE(O_error_message,
                                                         I_tran_seq_no,
                                                         I_key_value_1,
                                                         I_key_value_2,
                                                         I_store,
                                                         I_day,
                                                         I_business_date,
                                                         I_error_code) = FALSE then
            return FALSE;
         end if;
      elsif I_table = 'SA_TRAN_IGTAX' then
         if SA_MASS_ERROR_SQL.MASS_ITEM_IGTAX_DELETE(O_error_message,
                                                     I_tran_seq_no,
                                                     I_key_value_1,
                                                     I_key_value_2,
                                                     I_store,
                                                     I_day,
                                                     I_business_date,
                                                     I_error_code) = FALSE then
            return FALSE;
         end if;
      elsif I_table = 'SA_TRAN_PAYMENT' then
         if SA_MASS_ERROR_SQL.MASS_PAYMENT_DELETE(O_error_message,
                                                  I_tran_seq_no,
                                                  I_key_value_1,
                                                  I_store,
                                                  I_day,
                                                  I_business_date,
                                                  I_error_code) = FALSE then
            return FALSE;
         end if;
      elsif I_table = 'SA_TRAN_TENDER' then
         if SA_MASS_ERROR_SQL.MASS_TENDER_DELETE(O_error_message,
                                                 I_tran_seq_no,
                                                 I_key_value_1,
                                                 I_store,
                                                 I_day,
                                                 I_business_date,
                                                 I_error_code) = FALSE then
            return FALSE;
         end if;
      elsif I_table = 'SA_TRAN_TAX' then
         if SA_MASS_ERROR_SQL.MASS_TAX_DELETE(O_error_message,
                                              I_tran_seq_no,
                                              I_key_value_1,
                                              I_store,
                                              I_day,
                                              I_business_date,
                                              I_error_code) = FALSE then
            return FALSE;
         end if;
      elsif I_table = 'SA_CUSTOMER' then
         if SA_MASS_ERROR_SQL.MASS_CUSTOMER_DELETE(O_error_message,
                                                   I_tran_seq_no,
                                                   I_store,
                                                   I_day,
                                                   I_business_date,
                                                   I_error_code) = FALSE then
            return FALSE;
         end if;
      elsif I_table = 'SA_CUST_ATTRIB' then
         if SA_MASS_ERROR_SQL.MASS_CUST_ATTR_DELETE(O_error_message,
                                                    I_tran_seq_no,
                                                    I_key_value_1,
                                                    I_store,
                                                    I_day,
                                                    I_business_date,
                                                    I_error_code) = FALSE then
            return FALSE;
         end if;
      end if;
      O_error_ind := FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                          I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                          I_store           IN       SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                          I_day             IN       SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL,
                          I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                          I_error_code      IN       SA_ERROR_CODES.ERROR_CODE%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_ITEM_DELETE';

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_item_seq_no        SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE;

   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_item sti
                      where exists (select 'x'
                                      from sa_error se
                                     where se.tran_seq_no = nvl(I_tran_seq_no, sti.tran_seq_no)
                                       and se.tran_seq_no = sti.tran_seq_no
                                       and se.store = nvl(I_store, sti.store)
                                       and se.store = sti.store
                                       and se.day = nvl(I_day, sti.day)
                                       and se.day = sti.day
                                       and se.key_value_1 = nvl(I_item_seq_no, sti.item_seq_no)
                                       and se.key_value_1 = sti.item_seq_no
                                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                        from sa_store_day sa
                                                                       where sa.store = se.store
                                                                         and sa.business_date = I_business_date),
                                                                      se.store_day_seq_no)
                                       and se.rec_type = 'TITEM'
                                       and se.error_code = I_error_code)
                        and th.tran_seq_no = sti.tran_seq_no
                        and th.store       = sti.store
                        and th.day         = sti.day);


   cursor C_SA_TRAN_ITEM_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_item sti
       where exists (select 'x'
                       from sa_error se
                      where se.tran_seq_no = nvl(I_tran_seq_no, sti.tran_seq_no)
                        and se.tran_seq_no = sti.tran_seq_no
                        and se.store = nvl(I_store, sti.store)
                        and se.store = sti.store
                        and se.day = nvl(I_day, sti.day)
                        and se.day = sti.day
                        and se.key_value_1 = nvl(I_item_seq_no, sti.item_seq_no)
                        and se.key_value_1 = sti.item_seq_no
                        and se.store_day_seq_no = nvl((select store_day_seq_no
                                                         from sa_store_day sa
                                                        where sa.store = se.store
                                                          and sa.business_date = I_business_date),
                                                       se.store_day_seq_no)
                        and se.rec_type = 'TITEM'
                        and se.error_code = I_error_code)
                and sti.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_TRAN_ITEM(C_store             SA_TRAN_DISC.STORE%TYPE,
                           C_day               SA_TRAN_DISC.DAY%TYPE,
                           C_tran_seq_no       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE,
                           C_item_seq_no       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_item sti
       where sti.store = C_store
         and sti.day = C_day
         and sti.tran_seq_no = C_tran_seq_no
         and sti.item_seq_no = C_item_seq_no;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

BEGIN

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      L_tran_type        := rec.tran_type;

      ---
      for rec in C_SA_TRAN_ITEM_REC(L_tran_seq_no) loop
         L_item_seq_no     := rec.item_seq_no;

         if SA_MASS_ERROR_SQL.DELETE_MASS_ITEM_DISCOUNTS(O_error_message,
                                                         L_tran_seq_no,
                                                         L_item_seq_no,
                                                         L_store,
                                                         L_day) = FALSE then
            return FALSE;
         end if;

         if SA_MASS_ERROR_SQL.DELETE_MASS_ITEM_IGTAX(O_error_message,
                                                     L_tran_seq_no,
                                                     L_item_seq_no,
                                                     L_store,
                                                     L_day) = FALSE then
            return FALSE;
         end if;

         open C_LOCK_TRAN_ITEM(L_store, L_day, L_tran_seq_no, L_item_seq_no);
         close C_LOCK_TRAN_ITEM;

         delete
           from sa_tran_item sti
          where sti.store = L_store
            and sti.day = L_day
            and sti.tran_seq_no = L_tran_seq_no
            and sti.item_seq_no = L_item_seq_no;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       L_item_seq_no,
                                       NULL,
                                       'TITEM') = FALSE then
            return FALSE;
         end if;
      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_ITEM_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_MASS_ITEM_DISCOUNTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                    I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                    I_store           IN       SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                                    I_day             IN       SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.DELETE_MASS_ITEM_DISCOUNTS';

   cursor C_LOCK_TRAN_DISC is
      select 'x'
        from sa_tran_disc std
       where std.store = I_store
         and std.day = I_day
         and std.tran_seq_no = I_tran_seq_no
         and std.item_seq_no = I_item_seq_no
         for update nowait;

   cursor C_LOCK_ERROR is
      select 'x'
        from sa_error se
       where exists(select 'x'
                      from sa_tran_item sti
                      where sti.store = I_store
                        and sti.store = se.store
                        and sti.day = I_day
                        and sti.day = se.day
                        and sti.tran_seq_no = I_tran_seq_no
                        and sti.tran_seq_no = se.tran_seq_no
                        and sti.item_seq_no = I_item_seq_no
                        and sti.item_seq_no = se.key_value_1)
         and se.rec_type = 'IDISC'
         for update nowait;


BEGIN

   open C_LOCK_TRAN_DISC;
   close C_LOCK_TRAN_DISC;

   delete
     from sa_tran_disc std
    where std.store = I_store
      and std.day = I_day
      and std.tran_seq_no = I_tran_seq_no
      and std.item_seq_no = I_item_seq_no;

   open C_LOCK_ERROR;
   close C_LOCK_ERROR;
   delete from sa_error se
         where exists(select 'x'
                        from sa_tran_item sti
                        where sti.store = I_store
                          and sti.store = se.store
                          and sti.day = I_day
                          and sti.day = se.day
                          and sti.tran_seq_no = I_tran_seq_no
                          and sti.tran_seq_no = se.tran_seq_no
                          and sti.item_seq_no = I_item_seq_no
                          and sti.item_seq_no = se.key_value_1)
           and se.rec_type = 'IDISC';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_MASS_ITEM_DISCOUNTS;
----------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_MASS_ITEM_IGTAX(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                I_store           IN       SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                                I_day             IN       SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.DELETE_MASS_ITEM_IGTAX';

   cursor C_LOCK_TRAN_IGTAX is
      select 'x'
        from sa_tran_igtax stx
       where stx.store = I_store
         and stx.day = I_day
         and stx.tran_seq_no = I_tran_seq_no
         and stx.item_seq_no = I_item_seq_no
         for update nowait;

   cursor C_LOCK_ERROR is
      select 'x'
        from sa_error se
       where exists(select 'x'
                      from sa_tran_item sti
                      where sti.store = I_store
                        and sti.store = se.store
                        and sti.day = I_day
                        and sti.day = se.day
                        and sti.tran_seq_no = I_tran_seq_no
                        and sti.tran_seq_no = se.tran_seq_no
                        and sti.item_seq_no = I_item_seq_no
                        and sti.item_seq_no = se.key_value_1)
         and se.rec_type = 'IGTAX'
         for update nowait;

BEGIN

   open C_LOCK_TRAN_IGTAX;
   close C_LOCK_TRAN_IGTAX;

   delete
     from sa_tran_igtax stx
    where stx.store = I_store
      and stx.day = I_day
      and stx.tran_seq_no = I_tran_seq_no
      and stx.item_seq_no = I_item_seq_no;

   open C_LOCK_ERROR;
   close C_LOCK_ERROR;

   delete from sa_error se
         where exists(select 'x'
                        from sa_tran_item sti
                        where sti.store = I_store
                          and sti.store = se.store
                          and sti.day = I_day
                          and sti.day = se.day
                          and sti.tran_seq_no = I_tran_seq_no
                          and sti.tran_seq_no = se.tran_seq_no
                          and sti.item_seq_no = I_item_seq_no
                          and sti.item_seq_no = se.key_value_1)
           and se.rec_type = 'IGTAX';

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_MASS_ITEM_IGTAX;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_DISCOUNTS_DELETE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_seq_no       IN       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                    I_item_seq_no       IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                    I_discount_seq_no   IN       SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE DEFAULT NULL,
                                    I_store             IN       SA_TRAN_DISC.STORE%TYPE DEFAULT NULL,
                                    I_day               IN       SA_TRAN_DISC.DAY%TYPE DEFAULT NULL,
                                    I_business_date     IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                    I_error_code        IN       SA_ERROR_CODES.ERROR_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_ITEM_DISCOUNTS_DELETE';

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_item_seq_no        SA_TRAN_DISC.ITEM_SEQ_NO%TYPE;
   L_discount_seq_no    SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE;

   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_disc std
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, std.tran_seq_no)
                                      and se.tran_seq_no = std.tran_seq_no
                                      and se.store = nvl(I_store, std.store)
                                      and se.store = std.store
                                      and se.day = nvl(I_day, std.day)
                                      and se.day = std.day
                                      and se.key_value_1 = nvl(I_item_seq_no, std.item_seq_no)
                                      and se.key_value_1 = std.item_seq_no
                                      and se.key_value_2 = nvl(I_discount_seq_no, std.discount_seq_no)
                                      and se.key_value_2 = std.discount_seq_no
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type = 'IDISC'
                                      and se.error_code = I_error_code)
                        and th.tran_seq_no = std.tran_seq_no
                        and th.store       = std.store
                        and th.day         = std.day);

   cursor C_SA_TRAN_DISC_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_disc std
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, std.tran_seq_no)
                       and se.tran_seq_no = std.tran_seq_no
                       and se.store = nvl(I_store, std.store)
                       and se.store = std.store
                       and se.day = nvl(I_day, std.day)
                       and se.day = std.day
                       and se.key_value_1 = nvl(I_item_seq_no, std.item_seq_no)
                       and se.key_value_1 = std.item_seq_no
                       and se.key_value_2 = nvl(I_discount_seq_no, std.discount_seq_no)
                       and se.key_value_2 = std.discount_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'IDISC'
                       and se.error_code = I_error_code)
        and std.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_TRAN_IDISC(C_store             SA_TRAN_DISC.STORE%TYPE,
                            C_day               SA_TRAN_DISC.DAY%TYPE,
                            C_tran_seq_no       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE,
                            C_item_seq_no       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE,
                            C_discount_seq_no   SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_disc std
       where std.store = C_store
         and std.day = C_day
         and std.tran_seq_no = C_tran_seq_no
         and std.item_seq_no = C_item_seq_no
         and std.discount_seq_no = C_discount_seq_no;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

BEGIN



   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      L_tran_type        := rec.tran_type;

      ---
      for rec in C_SA_TRAN_DISC_REC(L_tran_seq_no) loop
         L_item_seq_no     := rec.item_seq_no;
         L_discount_seq_no := rec.discount_seq_no;

         open C_LOCK_TRAN_IDISC(L_store, L_day, L_tran_seq_no, L_item_seq_no, L_discount_seq_no);
         close C_LOCK_TRAN_IDISC;

         delete
           from sa_tran_disc std
          where std.store = L_store
            and std.day = L_day
            and std.tran_seq_no = L_tran_seq_no
            and std.item_seq_no = L_item_seq_no
            and std.discount_seq_no = L_discount_seq_no;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       L_item_seq_no,
                                       L_discount_seq_no,
                                       'IDISC') = FALSE then
            return FALSE;
         end if;
      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_ITEM_DISCOUNTS_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_IGTAX_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tran_seq_no     IN       SA_TRAN_IGTAX.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                I_item_seq_no     IN       SA_TRAN_IGTAX.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                I_igtax_seq_no    IN       SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE DEFAULT NULL,
                                I_store           IN       SA_TRAN_IGTAX.STORE%TYPE DEFAULT NULL,
                                I_day             IN       SA_TRAN_IGTAX.DAY%TYPE DEFAULT NULL,
                                I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                I_error_code      IN       SA_ERROR_CODES.ERROR_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_ITEM_IGTAX_DELETE';

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_item_seq_no        SA_TRAN_IGTAX.ITEM_SEQ_NO%TYPE;
   L_igtax_seq_no       SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE;


   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_igtax stx
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, stx.tran_seq_no)
                                      and se.tran_seq_no = stx.tran_seq_no
                                      and se.store = nvl(I_store, stx.store)
                                      and se.store = stx.store
                                      and se.day = nvl(I_day, stx.day)
                                      and se.day = stx.day
                                      and se.key_value_1 = nvl(I_item_seq_no, stx.item_seq_no)
                                      and se.key_value_1 = stx.item_seq_no
                                      and se.key_value_2 = nvl(I_igtax_seq_no, stx.igtax_seq_no)
                                      and se.key_value_2 = stx.igtax_seq_no
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type = 'IGTAX'
                                      and se.error_code = I_error_code)
                         and th.tran_seq_no = stx.tran_seq_no
                         and th.store       = stx.store
                         and th.day         = stx.day);

   cursor C_SA_TRAN_IGTAX_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_igtax stx
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, stx.tran_seq_no)
                       and se.tran_seq_no = stx.tran_seq_no
                       and se.store = nvl(I_store, stx.store)
                       and se.store = stx.store
                       and se.day = nvl(I_day, stx.day)
                       and se.day = stx.day
                       and se.key_value_1 = nvl(I_item_seq_no, stx.item_seq_no)
                       and se.key_value_1 = stx.item_seq_no
                       and se.key_value_2 = nvl(I_igtax_seq_no, stx.igtax_seq_no)
                       and se.key_value_2 = stx.igtax_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'IGTAX'
                       and se.error_code = I_error_code)
         and stx.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_TRAN_IGTAX(C_store          SA_TRAN_IGTAX.STORE%TYPE,
                            C_day            SA_TRAN_IGTAX.DAY%TYPE,
                            C_tran_seq_no    SA_TRAN_IGTAX.TRAN_SEQ_NO%TYPE,
                            C_item_seq_no    SA_TRAN_IGTAX.ITEM_SEQ_NO%TYPE,
                            C_igtax_seq_no   SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_igtax stx
       where stx.store = C_store
         and stx.day = C_day
         and stx.tran_seq_no = C_tran_seq_no
         and stx.item_seq_no = C_item_seq_no
         and stx.igtax_seq_no = C_igtax_seq_no;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;
BEGIN

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_tran_type        := rec.tran_type;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      ---
      for rec in C_SA_TRAN_IGTAX_REC(L_tran_seq_no) loop
         L_item_seq_no      := rec.item_seq_no;
         L_igtax_seq_no     := rec.igtax_seq_no;

         open C_LOCK_TRAN_IGTAX(L_store, L_day, L_tran_seq_no, L_item_seq_no, L_igtax_seq_no);
         close C_LOCK_TRAN_IGTAX;

         delete
           from sa_tran_igtax stig
          where stig.store = L_store
            and stig.day = L_day
            and stig.tran_seq_no = L_tran_seq_no
            and stig.item_seq_no = L_item_seq_no
            and stig.igtax_seq_no = L_igtax_seq_no;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       L_item_seq_no,
                                       L_igtax_seq_no,
                                       'IGTAX') = FALSE then
            return FALSE;
         end if;
      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;

   end loop;


   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_ITEM_IGTAX_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_PAYMENT_DELETE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tran_seq_no      IN       SA_TRAN_PAYMENT.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                             I_payment_seq_no   IN       SA_TRAN_PAYMENT.PAYMENT_SEQ_NO%TYPE DEFAULT NULL,
                             I_store            IN       SA_TRAN_PAYMENT.STORE%TYPE DEFAULT NULL,
                             I_day              IN       SA_TRAN_PAYMENT.DAY%TYPE DEFAULT NULL,
                             I_business_date    IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                             I_error_code       IN       SA_ERROR_CODES.ERROR_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_PAYMENT_DELETE';

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_payment_seq_no     SA_TRAN_PAYMENT.PAYMENT_SEQ_NO%TYPE;

   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists(select 'x'
                      from sa_tran_payment stp
                     where exists(select 'x'
                                    from sa_error se
                                   where se.tran_seq_no = nvl(I_tran_seq_no, stp.tran_seq_no)
                                     and se.tran_seq_no = stp.tran_seq_no
                                     and se.store = nvl(I_store, stp.store)
                                     and se.store = stp.store
                                     and se.day = nvl(I_day, stp.day)
                                     and se.day = stp.day
                                     and se.key_value_1 = nvl(I_payment_seq_no, stp.payment_seq_no)
                                     and se.key_value_1 = stp.payment_seq_no
                                     and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                      from sa_store_day sa
                                                                     where sa.store = se.store
                                                                       and sa.business_date = I_business_date),
                                                                    se.store_day_seq_no)
                                     and se.rec_type = 'TPYMT'
                                     and se.error_code = I_error_code)
                       and th.tran_seq_no = stp.tran_seq_no
                       and th.store       = stp.store
                       and th.day         = stp.day);

   cursor C_SA_TRAN_PAYMENT_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_payment stp
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, stp.tran_seq_no)
                       and se.tran_seq_no = stp.tran_seq_no
                       and se.store = nvl(I_store, stp.store)
                       and se.store = stp.store
                       and se.day = nvl(I_day, stp.day)
                       and se.day = stp.day
                       and se.key_value_1 = nvl(I_payment_seq_no, stp.payment_seq_no)
                       and se.key_value_1 = stp.payment_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'TPYMT'
                       and se.error_code = I_error_code)
        and stp.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_TRAN_PAYMENT(C_store            SA_TRAN_PAYMENT.STORE%TYPE,
                              C_day              SA_TRAN_PAYMENT.DAY%TYPE,
                              C_tran_seq_no      SA_TRAN_PAYMENT.TRAN_SEQ_NO%TYPE,
                              C_payment_seq_no   SA_TRAN_PAYMENT.PAYMENT_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_payment stp
       where stp.store = C_store
         and stp.day = C_day
         and stp.tran_seq_no = C_tran_seq_no
         and stp.payment_seq_no = C_payment_seq_no;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;
BEGIN

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      L_tran_type        := rec.tran_type;

      ---
      for rec in C_SA_TRAN_PAYMENT_REC(L_tran_seq_no) loop
         L_payment_seq_no     := rec.payment_seq_no;

         open C_LOCK_TRAN_PAYMENT(L_store, L_day, L_tran_seq_no, L_payment_seq_no);
         close C_LOCK_TRAN_PAYMENT;

         delete
           from sa_tran_payment stp
          where stp.store = L_store
            and stp.day = L_day
            and stp.tran_seq_no = L_tran_seq_no
            and stp.payment_seq_no = L_payment_seq_no;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       L_payment_seq_no,
                                       NULL,
                                       'TPYMT') = FALSE then
            return FALSE;
         end if;
      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_PAYMENT_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TENDER_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tran_seq_no     IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                            I_tender_seq_no   IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE DEFAULT NULL,
                            I_store           IN       SA_TRAN_TENDER.STORE%TYPE DEFAULT NULL,
                            I_day             IN       SA_TRAN_TENDER.DAY%TYPE DEFAULT NULL,
                            I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                            I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_TENDER_DELETE';

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_tender_seq_no      SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE;

   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists(select 'x'
                      from sa_tran_tender stt
                     where exists (select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, stt.tran_seq_no)
                                      and se.tran_seq_no = stt.tran_seq_no
                                      and se.store = nvl(I_store, stt.store)
                                      and se.store = stt.store
                                      and se.day = nvl(I_day, stt.day)
                                      and se.day = stt.day
                                      and se.key_value_1 = nvl(I_tender_seq_no, stt.tender_seq_no)
                                      and se.key_value_1 = stt.tender_seq_no
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type = 'TTEND'
                                      and se.error_code = I_error_code)
                       and th.tran_seq_no = stt.tran_seq_no
                       and th.store       = stt.store
                       and th.day         = stt.day);

   cursor C_SA_TRAN_TENDER_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_tender stt
       where exists (select 'x'
                       from sa_error se
                      where se.tran_seq_no = nvl(I_tran_seq_no, stt.tran_seq_no)
                        and se.tran_seq_no = stt.tran_seq_no
                        and se.store = nvl(I_store, stt.store)
                        and se.store = stt.store
                        and se.day = nvl(I_day, stt.day)
                        and se.day = stt.day
                        and se.key_value_1 = nvl(I_tender_seq_no, stt.tender_seq_no)
                        and se.key_value_1 = stt.tender_seq_no
                        and se.store_day_seq_no = nvl((select store_day_seq_no
                                                         from sa_store_day sa
                                                        where sa.store = se.store
                                                          and sa.business_date = I_business_date),
                                                       se.store_day_seq_no)
                        and se.rec_type = 'TTEND'
                        and se.error_code = I_error_code)
        and stt.tran_seq_no = C_tran_seq_no;


   cursor C_LOCK_TRAN_TENDER(C_store           SA_TRAN_TENDER.STORE%TYPE,
                             C_day             SA_TRAN_TENDER.DAY%TYPE,
                             C_tran_seq_no     SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE,
                             C_tender_seq_no   SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_tender std
       where std.store = C_store
         and std.day = C_day
         and std.tran_seq_no = C_tran_seq_no
         and std.tender_seq_no = C_tender_seq_no;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_LOCK_VOUCHER(C_store           SA_TRAN_TENDER.STORE%TYPE,
                         C_day             SA_TRAN_TENDER.DAY%TYPE,
                         C_tran_seq_no     SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE,
                         C_tender_seq_no   SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE) is
      select 'x'
        from sa_voucher sv
       where exists(select 'x'
                      from sa_tran_tender stt
                     where stt.store = C_store
                       and stt.day = C_day
                       and stt.tran_seq_no = C_tran_seq_no
                       and stt.tender_seq_no = C_tender_seq_no
                       and stt.voucher_no = sv.voucher_no
                       and stt.voucher_no is not null
                       and stt.tender_type_group = 'VOUCH')
         for update nowait;

BEGIN

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      L_tran_type        := rec.tran_type;

      ---
      for rec in C_SA_TRAN_TENDER_REC(L_tran_seq_no) loop
         L_tender_seq_no     := rec.tender_seq_no;

         open C_LOCK_VOUCHER(L_store, L_day, L_tran_seq_no, L_tender_seq_no);
         close C_LOCK_VOUCHER;

         update sa_voucher sv
            set sv.status = nvl2(sv.ass_date, 'A',
                                          nvl2(sv.iss_tran_seq_no, 'I',
                                                                   nvl2(sv.red_tran_seq_no, 'R', sv.status)))
          where exists(select 'x'
                         from sa_tran_tender stt
                        where stt.store = L_store
                          and stt.day = L_day
                          and stt.tran_seq_no = L_tran_seq_no
                          and stt.tender_seq_no = L_tender_seq_no
                          and stt.voucher_no = sv.voucher_no
                          and stt.voucher_no is not null
                          and stt.tender_type_group = 'VOUCH');

         update sa_voucher sv
            set iss_date             = NULL,
                iss_store            = NULL,
                iss_register         = NULL,
                iss_cashier          = NULL,
                iss_tran_seq_no      = NULL,
                iss_tender_seq_no    = NULL,
                iss_item_seq_no      = NULL,
                iss_amt              = NULL,
                iss_cust_name        = NULL,
                iss_cust_addr1       = NULL,
                iss_cust_addr2       = NULL,
                iss_cust_city        = NULL,
                iss_cust_state       = NULL,
                iss_cust_postal_code = NULL,
                iss_cust_country     = NULL,
                recipient_name       = NULL,
                recipient_state      = NULL,
                recipient_country    = NULL
          where exists(select 'x'
                         from sa_tran_tender stt,
                              sa_tran_head sth
                        where stt.store = L_store
                          and sth.store = stt.store
                          and stt.day = L_day
                          and sth.day = stt.day
                          and stt.tran_seq_no = L_tran_seq_no
                          and sth.tran_seq_no = stt.tran_seq_no
                          and stt.tender_seq_no = L_tender_seq_no
                          and stt.voucher_no = sv.voucher_no
                          and stt.voucher_no is not null
                          and sth.tran_seq_no = stt.tran_seq_no
                          and sth.tran_type = 'PAIDOU'
                          and stt.tender_type_group = 'VOUCH');

         update sa_voucher sv
            set red_date          = NULL,
                red_store         = NULL,
                red_register      = NULL,
                red_cashier       = NULL,
                red_tran_seq_no   = NULL,
                red_tender_seq_no = NULL,
                red_amt           = NULL
          where exists(select 'x'
                         from sa_tran_tender stt,
                              sa_tran_head sth
                        where stt.store = L_store
                          and sth.store = stt.store
                          and stt.day = L_day
                          and sth.day = stt.day
                          and stt.tran_seq_no = L_tran_seq_no
                          and sth.tran_seq_no = stt.tran_seq_no
                          and stt.tender_seq_no = L_tender_seq_no
                          and stt.voucher_no = sv.voucher_no
                          and stt.voucher_no is not null
                          and sth.tran_seq_no = stt.tran_seq_no
                          and sth.tran_type != 'PAIDOU'
                          and stt.tender_type_group = 'VOUCH');

         open C_LOCK_TRAN_TENDER(L_store, L_day, L_tran_seq_no, L_tender_seq_no);
         close C_LOCK_TRAN_TENDER;

         delete
           from sa_tran_tender stt
          where stt.store = L_store
            and stt.day = L_day
            and stt.tran_seq_no = L_tran_seq_no
            and stt.tender_seq_no = L_tender_seq_no;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       L_tender_seq_no,
                                       NULL,
                                       'TTEND') = FALSE then
            return FALSE;
         end if;
      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_TENDER_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TAX_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tran_seq_no     IN       SA_TRAN_TAX.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                         I_tax_seq_no      IN       SA_TRAN_TAX.TAX_SEQ_NO%TYPE DEFAULT NULL,
                         I_store           IN       SA_TRAN_TAX.STORE%TYPE DEFAULT NULL,
                         I_day             IN       SA_TRAN_TAX.DAY%TYPE DEFAULT NULL,
                         I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                         I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_TAX_DELETE';

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_tax_seq_no         SA_TRAN_TAX.TAX_SEQ_NO%TYPE;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_tax sttx
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, sttx.tran_seq_no)
                                      and se.tran_seq_no = sttx.tran_seq_no
                                      and se.store = nvl(I_store, sttx.store)
                                      and se.store =  sttx.store
                                      and se.day = nvl(I_day, sttx.day)
                                      and se.day = sttx.day
                                      and se.key_value_1 = nvl(I_tax_seq_no, sttx.tax_seq_no)
                                      and se.key_value_1 = sttx.tax_seq_no
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type = 'TTAX'
                                      and se.error_code = I_error_code)
                         and th.tran_seq_no = sttx.tran_seq_no
                         and th.store       = sttx.store
                         and th.day         = sttx.day);

   cursor C_SA_TRAN_TAX_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_tax sttx
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sttx.tran_seq_no)
                       and se.tran_seq_no = sttx.tran_seq_no
                       and se.store = nvl(I_store, sttx.store)
                       and se.store =  sttx.store
                       and se.day = nvl(I_day, sttx.day)
                       and se.day = sttx.day
                       and se.key_value_1 = nvl(I_tax_seq_no, sttx.tax_seq_no)
                       and se.key_value_1 = sttx.tax_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'TTAX'
                       and se.error_code = I_error_code)
         and sttx.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_TRAN_TAX(C_store        SA_TRAN_TAX.STORE%TYPE,
                          C_day          SA_TRAN_TAX.DAY%TYPE,
                          C_tran_seq_no  SA_TRAN_TAX.TRAN_SEQ_NO%TYPE,
                          C_tax_seq_no   SA_TRAN_TAX.TAX_SEQ_NO%TYPE) is
      select 'x'
        from sa_tran_tax sttx
       where sttx.store = C_store
         and sttx.day = C_day
         and sttx.tran_seq_no = C_tran_seq_no
         and sttx.tax_seq_no = C_tax_seq_no;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

BEGIN

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_tran_type        := rec.tran_type;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      ---
      for rec in C_SA_TRAN_TAX_REC(L_tran_seq_no) loop
         L_tax_seq_no     := rec.tax_seq_no;

         open C_LOCK_TRAN_TAX(L_store, L_day, L_tran_seq_no, L_tax_seq_no);
         close C_LOCK_TRAN_TAX;

         delete
           from sa_tran_tax sttx
          where sttx.store = L_store
            and sttx.day = L_day
            and sttx.tran_seq_no = L_tran_seq_no
            and sttx.tax_seq_no = L_tax_seq_no;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       L_tax_seq_no,
                                       NULL,
                                       'TTAX') = FALSE then
            return FALSE;
         end if;
      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;

   end loop;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_TAX_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_CUSTOMER_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tran_seq_no     IN       SA_CUSTOMER.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                              I_store           IN       SA_CUSTOMER.STORE%TYPE DEFAULT NULL,
                              I_day             IN       SA_CUSTOMER.DAY%TYPE DEFAULT NULL,
                              I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                              I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_CUSTOMER_DELETE';

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_customer sc
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, sc.tran_seq_no)
                                      and se.tran_seq_no = sc.tran_seq_no
                                      and se.store = nvl(I_store, sc.store)
                                      and se.store = sc.store
                                      and se.day = nvl(I_day, sc.day)
                                      and se.day = sc.day
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type in ('TCUST','TCTYPE')
                                      and se.error_code = I_error_code)
                        and th.tran_seq_no = sc.tran_seq_no
                        and th.store       = sc.store
                        and th.day         = sc.day);

   cursor C_SA_TRAN_CUST_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_customer sc
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sc.tran_seq_no)
                       and se.tran_seq_no = sc.tran_seq_no
                       and se.store = nvl(I_store, sc.store)
                       and se.store = sc.store
                       and se.day = nvl(I_day, sc.day)
                       and se.day = sc.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type in ('TCUST','TCTYPE')
                       and se.error_code = I_error_code)
         and sc.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_CUST_ATTRIB(C_store           SA_CUSTOMER.STORE%TYPE,
                             C_day             SA_CUSTOMER.DAY%TYPE,
                             C_tran_seq_no     SA_CUSTOMER.TRAN_SEQ_NO%TYPE) is
      select 'x'
        from sa_cust_attrib sca
       where sca.store = C_store
         and sca.day = C_day
         and sca.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_CUSTOMER(C_store           SA_CUST_ATTRIB.STORE%TYPE,
                          C_day             SA_CUST_ATTRIB.DAY%TYPE,
                          C_tran_seq_no     SA_CUST_ATTRIB.TRAN_SEQ_NO%TYPE) is
      select 'x'
        from sa_customer sc
       where sc.store = C_store
         and sc.day = C_day
         and sc.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;
BEGIN

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no        := rec.tran_seq_no;
      L_rev_no             := rec.rev_no;
      L_store              := rec.store;
      L_day                := rec.day;
      L_store_day_seq_no   := rec.store_day_seq_no;
      L_audit_status       := rec.audit_status;
      L_data_status        := rec.data_status;
      L_tran_type          := rec.tran_type;
      
      for rec in C_SA_TRAN_CUST_REC(L_tran_seq_no) loop

         open C_LOCK_CUST_ATTRIB(L_store, L_day, L_tran_seq_no);
         close C_LOCK_CUST_ATTRIB;

         delete
           from sa_cust_attrib sca
          where sca.store = L_store
            and sca.day = L_day
            and sca.tran_seq_no = L_tran_seq_no;

         open C_LOCK_CUSTOMER(L_store, L_day, L_tran_seq_no);
         close C_LOCK_CUSTOMER;

         delete from sa_customer sc
          where sc.store = L_store
            and sc.day = L_day 
            and sc.tran_seq_no = L_tran_seq_no;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       NULL,
                                       NULL,
                                       'TCUST') = FALSE then
            return FALSE;
         end if;
      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_CUSTOMER_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_CUST_ATTR_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tran_seq_no     IN       SA_CUST_ATTRIB.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                               I_attrib_seq_no   IN       SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE DEFAULT NULL,
                               I_store           IN       SA_CUST_ATTRIB.STORE%TYPE DEFAULT NULL,
                               I_day             IN       SA_CUST_ATTRIB.DAY%TYPE DEFAULT NULL,
                               I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                               I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_CUST_ATTR_DELETE';

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_attrib_seq_no      SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_cust_attrib sca
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, sca.tran_seq_no)
                                      and se.tran_seq_no = sca.tran_seq_no
                                      and se.key_value_1 = nvl(I_attrib_seq_no, sca.attrib_seq_no)
                                      and se.key_value_1 = sca.attrib_seq_no
                                      and se.store = nvl(I_store, sca.store)
                                      and se.store = sca.store
                                      and se.day = nvl(I_day, sca.day)
                                      and se.day = sca.day
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type = 'CATT'
                                      and se.error_code = I_error_code)
                        and th.tran_seq_no = sca.tran_seq_no
                        and th.store       = sca.store
                        and th.day         = sca.day);

   cursor C_SA_TRAN_CATT_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_cust_attrib sca
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sca.tran_seq_no)
                       and se.tran_seq_no = sca.tran_seq_no
                       and se.key_value_1 = nvl(I_attrib_seq_no, sca.attrib_seq_no)
                       and se.key_value_1 = sca.attrib_seq_no
                       and se.store = nvl(I_store, sca.store)
                       and se.store = sca.store
                       and se.day = nvl(I_day, sca.day)
                       and se.day = sca.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'CATT'
                       and se.error_code = I_error_code)
         and sca.tran_seq_no = C_tran_seq_no;

   cursor C_LOCK_CUST_ATTRIB(C_store           SA_CUST_ATTRIB.STORE%TYPE,
                             C_day             SA_CUST_ATTRIB.DAY%TYPE,
                             C_tran_seq_no     SA_CUST_ATTRIB.TRAN_SEQ_NO%TYPE,
                             C_attrib_seq_no   SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE) is
      select 'x'
        from sa_cust_attrib sca
       where sca.store = C_store
         and sca.day = C_day
         and sca.tran_seq_no = C_tran_seq_no
         and sca.attrib_seq_no = C_attrib_seq_no;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;
BEGIN

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no        := rec.tran_seq_no;
      L_rev_no             := rec.rev_no;
      L_store              := rec.store;
      L_day                := rec.day;
      L_store_day_seq_no   := rec.store_day_seq_no;
      L_audit_status       := rec.audit_status;
      L_data_status        := rec.data_status;
      L_tran_type          := rec.tran_type;

      for rec in C_SA_TRAN_CATT_REC(L_tran_seq_no) loop

         L_attrib_seq_no := rec.attrib_seq_no;

         open C_LOCK_CUST_ATTRIB(L_store, L_day, L_tran_seq_no, L_attrib_seq_no);
         close C_LOCK_CUST_ATTRIB;

         delete
           from sa_cust_attrib sca
          where sca.store = L_store
            and sca.day = L_day
            and sca.tran_seq_no = L_tran_seq_no
            and sca.attrib_seq_no = L_attrib_seq_no;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       L_attrib_seq_no,
                                       NULL,
                                       'CATT') = FALSE then
            return FALSE;
         end if;
      end loop;
      
      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_CUST_ATTR_DELETE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_OVERRIDE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                       I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                       I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                       I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                       I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_OVERRIDE';

BEGIN

   update sa_error se
      set hq_override_ind = 'Y',
          update_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER),
          update_datetime = SYSDATE
    where se.tran_seq_no = nvl(I_tran_seq_no, se.tran_seq_no)
      and se.store = nvl(I_store, se.store)
      and se.day = nvl(I_day, se.day)
      and se.store_day_seq_no = nvl((select store_day_seq_no
                                       from sa_store_day sa
                                      where sa.store = se.store
                                        and sa.business_date = I_business_date), se.store_day_seq_no)
      and se.error_code = I_error_code;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_OVERRIDE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TOTAL_TRANSACTIONS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_total_transactions      OUT   NUMBER,
                                 I_tran_seq_no          IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                 I_key_value_1          IN       SA_ERROR.KEY_VALUE_1%TYPE DEFAULT NULL,
                                 I_key_value_2          IN       SA_ERROR.KEY_VALUE_2%TYPE DEFAULT NULL,
                                 I_store                IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                                 I_day                  IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                                 I_business_date        IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                 I_error_code           IN       SA_ERROR.ERROR_CODE%TYPE,
                                 I_table                IN       SA_ERROR_CODES.ERROR_FIX_TABLE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_TOTAL_TRANSACTIONS';

   cursor C_TOTAL_ITEM is
      select count('x')
        from sa_tran_item sti
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sti.tran_seq_no)
                       and se.tran_seq_no = sti.tran_seq_no
                       and se.store = nvl(I_store, sti.store)
                       and se.store = sti.store
                       and se.day = nvl(I_day, sti.day)
                       and se.day = sti.day
                       and se.key_value_1 = nvl(I_key_value_1, sti.item_seq_no)
                       and se.key_value_1 = sti.item_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'TITEM'
                       and se.error_code = I_error_code);

   cursor C_TOTAL_DISC is
      select count('x')
        from sa_tran_disc std
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, std.tran_seq_no)
                       and se.tran_seq_no = std.tran_seq_no
                       and se.store = nvl(I_store, std.store)
                       and se.store = std.store
                       and se.day = nvl(I_day, std.day)
                       and se.day = std.day
                       and se.key_value_1 = nvl(I_key_value_1, std.item_seq_no)
                       and se.key_value_1 = std.item_seq_no
                       and se.key_value_2 = nvl(I_key_value_2, std.discount_seq_no)
                       and se.key_value_2 = std.discount_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'IDISC'
                       and se.error_code = I_error_code);

   cursor C_TOTAL_IGTAX is
      select count('x')
        from sa_tran_igtax stx
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, stx.tran_seq_no)
                       and se.tran_seq_no = stx.tran_seq_no
                       and se.store = nvl(I_store, stx.store)
                       and se.store = stx.store
                       and se.day = nvl(I_day, stx.day)
                       and se.day = stx.day
                       and se.key_value_1 = nvl(I_key_value_1, stx.item_seq_no)
                       and se.key_value_1 = stx.item_seq_no
                       and se.key_value_2 = nvl(I_key_value_2, stx.igtax_seq_no)
                       and se.key_value_2 = stx.igtax_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'IGTAX'
                       and se.error_code = I_error_code);

   cursor C_TOTAL_TENDER is
      select count('x')
        from sa_tran_tender stt
       where exists (select 'x'
                       from sa_error se
                      where se.tran_seq_no = nvl(I_tran_seq_no, stt.tran_seq_no)
                        and se.tran_seq_no = stt.tran_seq_no
                        and se.store = nvl(I_store, stt.store)
                        and se.store = stt.store
                        and se.day = nvl(I_day, stt.day)
                        and se.day = stt.day
                        and se.key_value_1 = nvl(I_key_value_1, stt.tender_seq_no)
                        and se.key_value_1 = stt.tender_seq_no
                        and se.store_day_seq_no = nvl((select store_day_seq_no
                                                         from sa_store_day sa
                                                        where sa.store = se.store
                                                          and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                        and se.rec_type = 'TTEND'
                        and se.error_code = I_error_code);

   cursor C_TOTAL_TAX is
      select count('x')
        from sa_tran_tax sttx
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sttx.tran_seq_no)
                       and se.tran_seq_no = sttx.tran_seq_no
                       and se.store = nvl(I_store, sttx.store)
                       and se.store =  sttx.store
                       and se.day = nvl(I_day, sttx.day)
                       and se.day = sttx.day
                       and se.key_value_1 = nvl(I_key_value_1, sttx.tax_seq_no)
                       and se.key_value_1 = sttx.tax_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                     se.store_day_seq_no)
                       and se.rec_type = 'TTAX'
                       and se.error_code = I_error_code);

   cursor C_TOTAL_CUSTOMER is
      select count('x')
        from sa_customer sc
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sc.tran_seq_no)
                       and se.tran_seq_no = sc.tran_seq_no
                       and se.store = nvl(I_store, sc.store)
                       and se.store = sc.store
                       and se.day = nvl(I_day, sc.day)
                       and se.day = sc.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type in ('TCUST','TCTYPE')
                       and se.error_code = I_error_code);

   cursor C_TOTAL_CUST_ATTRIB is
      select count('x')
        from sa_cust_attrib sca
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sca.tran_seq_no)
                       and se.tran_seq_no = sca.tran_seq_no
                       and se.key_value_1 = nvl(I_key_value_1, sca.attrib_seq_no)
                       and se.key_value_1 = sca.attrib_seq_no
                       and se.store = nvl(I_store, sca.store)
                       and se.store = sca.store
                       and se.day = nvl(I_day, sca.day)
                       and se.day = sca.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'CATT'
                       and se.error_code = I_error_code);

   cursor C_TOTAL_PAYMENT is
      select count('x')
        from sa_tran_payment stp
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, stp.tran_seq_no)
                       and se.tran_seq_no = stp.tran_seq_no
                       and se.store = nvl(I_store, stp.store)
                       and se.store = stp.store
                       and se.day = nvl(I_day, stp.day)
                       and se.day = stp.day
                       and se.key_value_1 = nvl(I_key_value_1, stp.payment_seq_no)
                       and se.key_value_1 = stp.payment_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'TPYMT'
                       and se.error_code = I_error_code);

   cursor C_TOTAL_TRAN_HEAD is
      select count('x')
        from sa_tran_head sth
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sth.tran_seq_no)
                       and se.tran_seq_no = sth.tran_seq_no
                       and se.store = nvl(I_store, sth.store)
                       and se.store = sth.store
                       and se.day = nvl(I_day, sth.day)
                       and se.day = sth.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type in ('THEAD','THREAC','FHEAD','THSUBT','THTRAT')
                       and se.error_code = I_error_code);

BEGIN
   if I_table = 'SA_TRAN_ITEM' then
      open C_TOTAL_ITEM;
      fetch C_TOTAL_ITEM into O_total_transactions;
      close C_TOTAL_ITEM;
   elsif I_table = 'SA_TRAN_DISC' then
      open C_TOTAL_DISC;
      fetch C_TOTAL_DISC into O_total_transactions;
      close C_TOTAL_DISC;
   elsif I_table = 'SA_TRAN_IGTAX' then
      open C_TOTAL_IGTAX;
      fetch C_TOTAL_IGTAX into O_total_transactions;
      close C_TOTAL_IGTAX;
   elsif I_table = 'SA_TRAN_TENDER' then
      open C_TOTAL_TENDER;
      fetch C_TOTAL_TENDER into O_total_transactions;
      close C_TOTAL_TENDER;
   elsif I_table = 'SA_TRAN_TAX' then
      open C_TOTAL_TAX;
      fetch C_TOTAL_TAX into O_total_transactions;
      close C_TOTAL_TAX;
   elsif I_table = 'SA_CUSTOMER' then
      open C_TOTAL_CUSTOMER;
      fetch C_TOTAL_CUSTOMER into O_total_transactions;
      close C_TOTAL_CUSTOMER;
   elsif I_table = 'SA_CUST_ATTRIB' then
      open C_TOTAL_CUST_ATTRIB;
      fetch C_TOTAL_CUST_ATTRIB into O_total_transactions;
      close C_TOTAL_CUST_ATTRIB;
   elsif I_table = 'SA_TRAN_HEAD' then
      open C_TOTAL_TRAN_HEAD;
      fetch C_TOTAL_TRAN_HEAD into O_total_transactions;
      close C_TOTAL_TRAN_HEAD;
   elsif I_table = 'SA_TRAN_PAYMENT' then
      open C_TOTAL_PAYMENT;
      fetch C_TOTAL_PAYMENT into O_total_transactions;
      close C_TOTAL_PAYMENT;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_TOTAL_TRANSACTIONS;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_ind          OUT   BOOLEAN,
                     I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                     I_key_value_1     IN       SA_ERROR.KEY_VALUE_1%TYPE DEFAULT NULL,
                     I_key_value_2     IN       SA_ERROR.KEY_VALUE_2%TYPE DEFAULT NULL,
                     I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                     I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                     I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                     I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                     I_table           IN       SA_ERROR_CODES.ERROR_FIX_TABLE%TYPE,
                     I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                     I_new_value       IN       VARCHAR2,
                     I_dependency      IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_UPDATE';
   L_error_ind   BOOLEAN   := FALSE;

BEGIN
   if I_table = 'SA_TRAN_ITEM' then
      if SA_MASS_ERROR_SQL.MASS_ITEM_UPDATE(O_error_message,
                                            L_error_ind,
                                            I_tran_seq_no,
                                            I_key_value_1,
                                            I_store,
                                            I_day,
                                            I_business_date,
                                            I_error_code,
                                            I_fix_column,
                                            I_new_value) = FALSE then
         return FALSE;
      end if;
   elsif I_table = 'SA_TRAN_DISC' then
      if SA_MASS_ERROR_SQL.MASS_ITEM_DISCOUNTS_UPDATE(O_error_message,
                                                      I_tran_seq_no,
                                                      I_key_value_1,
                                                      I_key_value_2,
                                                      I_store,
                                                      I_day,
                                                      I_business_date,
                                                      I_error_code,
                                                      I_fix_column,
                                                      I_new_value,
                                                      I_dependency) = FALSE then
         return FALSE;
      end if;
   elsif I_table = 'SA_TRAN_IGTAX' then
      if SA_MASS_ERROR_SQL.MASS_ITEM_IGTAX_UPDATE(O_error_message,
                                                  I_tran_seq_no,
                                                  I_key_value_1,
                                                  I_key_value_2,
                                                  I_store,
                                                  I_day,
                                                  I_business_date,
                                                  I_error_code,
                                                  I_fix_column,
                                                  I_new_value) = FALSE then
         return FALSE;
      end if;
   elsif I_table = 'SA_TRAN_TENDER' then
      if SA_MASS_ERROR_SQL.MASS_TENDER_UPDATE(O_error_message,
                                              I_tran_seq_no,
                                              I_key_value_1,
                                              I_store,
                                              I_day,
                                              I_business_date,
                                              I_error_code,
                                              I_fix_column,
                                              I_new_value,
                                              I_dependency) = FALSE then
         return FALSE;
      end if;
   elsif I_table = 'SA_TRAN_TAX' then
      if SA_MASS_ERROR_SQL.MASS_TAX_UPDATE(O_error_message,
                                           I_tran_seq_no,
                                           I_key_value_1,
                                           I_store,
                                           I_day,
                                           I_business_date,
                                           I_error_code,
                                           I_fix_column,
                                           I_new_value) = FALSE then
         return FALSE;
      end if;
   elsif I_table = 'SA_CUSTOMER' then
      if SA_MASS_ERROR_SQL.MASS_CUSTOMER_UPDATE(O_error_message,
                                                I_tran_seq_no,
                                                I_store,
                                                I_day,
                                                I_business_date,
                                                I_error_code,
                                                I_fix_column,
                                                I_new_value) = FALSE then
         return FALSE;
      end if;
   elsif I_table = 'SA_CUST_ATTRIB' then
      if SA_MASS_ERROR_SQL.MASS_CUST_ATTR_UPDATE(O_error_message,
                                                 I_tran_seq_no,
                                                 I_key_value_1,
                                                 I_store,
                                                 I_day,
                                                 I_business_date,
                                                 I_error_code,
                                                 I_fix_column,
                                                 I_new_value) = FALSE then
         return FALSE;
      end if;
    elsif I_table = 'SA_TRAN_HEAD' then
      if SA_MASS_ERROR_SQL.MASS_TRAN_HEAD_UPDATE(O_error_message,
                                                 I_tran_seq_no,
                                                 I_store,
                                                 I_day,
                                                 I_business_date,
                                                 I_error_code,
                                                 I_fix_column,
                                                 I_new_value) = FALSE then
         return FALSE;
      end if;
   end if;

   O_error_ind := L_error_ind;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_ind          OUT   BOOLEAN,
                          I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                          I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                          I_store           IN       SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                          I_day             IN       SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL,
                          I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                          I_error_code      IN       SA_ERROR_CODES.ERROR_CODE%TYPE,
                          I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                          I_new_value       IN       VARCHAR2)

RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_ITEM_UPDATE';
   L_update_statement   VARCHAR2(1000);

   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;
   L_sub_tran_type      SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_rtlog_orig_sys     SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE;
   L_tran_process_sys   SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE;
   L_pos_tran_ind       SA_TRAN_HEAD.POS_TRAN_IND%TYPE;

   L_audit_status   SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status    SA_STORE_DAY.DATA_STATUS%TYPE;

   L_item_type            SA_TRAN_ITEM.ITEM_TYPE%TYPE;
   L_catchweight_ind      SA_TRAN_ITEM.CATCHWEIGHT_IND%TYPE;
   L_standard_uom         SA_TRAN_ITEM.STANDARD_UOM%TYPE;
   L_qty                  SA_TRAN_ITEM.QTY%TYPE;
   L_item                 SA_TRAN_ITEM.ITEM%TYPE;
   L_item_seq_no          SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE;
   L_sales_type           SA_TRAN_ITEM.SALES_TYPE%TYPE;
   L_return_wh            SA_TRAN_ITEM.RETURN_WH%TYPE;
   L_return_disposition   SA_TRAN_ITEM.RETURN_DISPOSITION%TYPE;
   L_item_status          SA_TRAN_ITEM.ITEM_STATUS%TYPE;
   L_no_inv_ret_ind       SA_TRAN_ITEM.NO_INV_RET_IND%TYPE;

   L_update_ind   BOOLEAN := FALSE;
   L_error_ind    BOOLEAN := FALSE;

   cursor C_LOCK_TRAN_ITEM is
      select 'x'
        from sa_tran_item sti
       where exists (select 'x'
                       from sa_error se
                      where se.tran_seq_no = nvl(I_tran_seq_no, sti.tran_seq_no)
                        and se.tran_seq_no = sti.tran_seq_no
                        and se.store = nvl(I_store, sti.store)
                        and se.store = sti.store
                        and se.day = nvl(I_day, sti.day)
                        and se.day = sti.day
                        and se.key_value_1 = nvl(I_item_seq_no, sti.item_seq_no)
                        and se.key_value_1 = sti.item_seq_no
                        and se.store_day_seq_no = nvl((select store_day_seq_no
                                                         from sa_store_day sa
                                                        where sa.store = se.store
                                                          and sa.business_date = I_business_date),
                                                       se.store_day_seq_no)
                        and se.rec_type = 'TITEM'
                        and se.error_code = I_error_code)
         for update nowait;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_item sti
                      where exists (select 'x'
                                      from sa_error se
                                     where se.tran_seq_no = nvl(I_tran_seq_no, sti.tran_seq_no)
                                       and se.tran_seq_no = sti.tran_seq_no
                                       and se.store = nvl(I_store, sti.store)
                                       and se.store = sti.store
                                       and se.day = nvl(I_day, sti.day)
                                       and se.day = sti.day
                                       and se.key_value_1 = nvl(I_item_seq_no, sti.item_seq_no)
                                       and se.key_value_1 = sti.item_seq_no
                                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                        from sa_store_day sa
                                                                       where sa.store = se.store
                                                                         and sa.business_date = I_business_date),
                                                                      se.store_day_seq_no)
                                       and se.rec_type = 'TITEM'
                                       and se.error_code = I_error_code)
                        and th.tran_seq_no = sti.tran_seq_no
                        and th.store       = sti.store
                        and th.day         = sti.day);

   cursor C_SA_TRAN_ITEM_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_item sti
       where exists (select 'x'
                       from sa_error se
                      where se.tran_seq_no = nvl(I_tran_seq_no, sti.tran_seq_no)
                        and se.tran_seq_no = sti.tran_seq_no
                        and se.store = nvl(I_store, sti.store)
                        and se.store = sti.store
                        and se.day = nvl(I_day, sti.day)
                        and se.day = sti.day
                        and se.key_value_1 = nvl(I_item_seq_no, sti.item_seq_no)
                        and se.key_value_1 = sti.item_seq_no
                        and se.store_day_seq_no = nvl((select store_day_seq_no
                                                         from sa_store_day sa
                                                        where sa.store = se.store
                                                          and sa.business_date = I_business_date),
                                                       se.store_day_seq_no)
                        and se.rec_type = 'TITEM'
                        and se.error_code = I_error_code)
                and sti.tran_seq_no = C_tran_seq_no;

BEGIN

   open C_LOCK_TRAN_ITEM;
   close C_LOCK_TRAN_ITEM;

   L_update_statement :=   'update sa_tran_item sti'
                         ||'   set '||I_fix_column||' = :I_new_value'
                         ||' where sti.store = :L_store '
                         ||'   and sti.day = :L_day '
                         ||'   and sti.tran_seq_no = :L_tran_seq_no'
                         ||'   and sti.item_seq_no = :L_item_seq_no';

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_tran_type        := rec.tran_type;
      L_sub_tran_type    := rec.sub_tran_type;
      L_update_ind       := FALSE;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_rtlog_orig_sys   := rec.rtlog_orig_sys;
      L_tran_process_sys := rec.tran_process_sys;
      L_pos_tran_ind     := rec.pos_tran_ind;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      ---
      for rec in C_SA_TRAN_ITEM_REC(L_tran_seq_no) loop
         L_item_type        := rec.item_type;
         L_catchweight_ind  := rec.catchweight_ind;
         L_standard_uom     := rec.standard_uom;
         L_qty              := rec.qty;
         L_item_seq_no      := rec.item_seq_no;
         L_sales_type       := rec.sales_type;
         L_item_status      := rec.item_status;
         L_no_inv_ret_ind   := rec.no_inv_ret_ind;

         if (I_fix_column in ('ITEM', 'REF_ITEM')) then

            L_item             := I_new_value;

            if PROCESS_ITEM_INFO(O_error_message,
                                 O_error_ind,
                                 L_item,
                                 L_item_type,
                                 L_sub_tran_type,
                                 L_qty,
                                 L_tran_seq_no,
                                 L_item_seq_no,
                                 L_store,
                                 L_day) = FALSE then
               return FALSE;
            end if;
         elsif(I_fix_column in ('SALES_TYPE')) then

            L_sales_type       := I_new_value;

            if PROCESS_ITEM_SALES_TYPE(O_error_message,
                                       O_error_ind,
                                       L_sales_type,
                                       L_item_status,
                                       L_pos_tran_ind,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       L_item_seq_no,
                                       L_store,
                                       L_day) = FALSE then
               return FALSE;
            end if;
         elsif(I_fix_column in ('RETURN_WH')) then

            L_return_wh       := I_new_value;

            if PROCESS_ITEM_RETURN_WH(O_error_message,
                                      O_error_ind,
                                      L_rtlog_orig_sys,
                                      L_tran_process_sys,
                                      L_return_wh,
                                      L_sales_type,
                                      L_item_status,
                                      L_store_day_seq_no,
                                      L_tran_seq_no,
                                      L_item_seq_no,
                                      L_store,
                                      L_day) = FALSE then
               return FALSE;
            end if;
         elsif(I_fix_column in ('RETURN_DISPOSITION')) then

            L_return_disposition   := I_new_value;

            if PROCESS_ITEM_RETURN_DISP(O_error_message,
                                        O_error_ind,
                                        L_rtlog_orig_sys,
                                        L_tran_process_sys,
                                        L_return_disposition,
                                        L_sales_type,
                                        L_item_status,
                                        L_no_inv_ret_ind,
                                        L_store_day_seq_no,
                                        L_tran_seq_no,
                                        L_item_seq_no,
                                        L_store,
                                        L_day) = FALSE then
               return FALSE;
            end if;
         else
            O_error_ind := FALSE;
         end if;

         if O_error_ind = FALSE then
            L_update_ind := TRUE;
            EXECUTE IMMEDIATE L_update_statement
            USING I_new_value,L_store, L_day, L_tran_seq_no, L_item_seq_no;

            if I_fix_column in ('ITEM_STATUS') then
               if POST_ITEM_TYPE_CHANGED (O_error_message,
                                          L_tran_seq_no,
                                          L_item_seq_no,
                                          L_store,
                                          L_day) then
                  return FALSE;
               end if;
            end if;
         else
            L_error_ind := TRUE;
         end if;
      end loop;

      /* REFRESH ERRORS */
      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if REFRESH_ERRORS(O_error_message,
                        I_fix_column,
                        L_store_day_seq_no,
                        L_tran_seq_no,
                        L_store,
                        L_day,
                        L_tran_type,
                        L_rtlog_orig_sys,
                        L_tran_process_sys,
                        'SA_TRAN_ITEM') = FALSE then
         return FALSE;
      end if;
      ---
      if L_update_ind = TRUE then
         if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                             L_tran_seq_no,
                                             L_rev_no,
                                             L_store,
                                             L_day) = FALSE then
            return FALSE;
         end if;

         open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
         close C_LOCK_TRAN_HEAD;
         ---
         update sa_tran_head
            set rev_no      = (L_rev_no + 1),
                update_id = get_user,
                update_datetime = sysdate
          where tran_seq_no = L_tran_seq_no
            and store       = L_store
            and day         = L_day;

         if TOTAL_AUDIT_CHECK(O_error_message,
                              L_audit_status,
                              L_data_status,
                              L_store_day_seq_no,
                              L_store,
                              L_day) = FALSE then
            return FALSE;
         end if;

      end if;

   end loop;

   O_error_ind := L_error_ind;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_ITEM_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_DISCOUNTS_UPDATE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_seq_no       IN       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                    I_item_seq_no       IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                    I_discount_seq_no   IN       SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE DEFAULT NULL,
                                    I_store             IN       SA_TRAN_DISC.STORE%TYPE DEFAULT NULL,
                                    I_day               IN       SA_TRAN_DISC.DAY%TYPE DEFAULT NULL,
                                    I_business_date     IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                    I_error_code        IN       SA_ERROR_CODES.ERROR_CODE%TYPE,
                                    I_fix_column        IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                                    I_new_value         IN       VARCHAR2,
                                    I_promotion         IN       SA_TRAN_DISC.PROMOTION%TYPE)

RETURN BOOLEAN IS

   L_program              VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_ITEM_DISCOUNTS_UPDATE';
   L_update_statement     VARCHAR2(1200);
   L_update_statement_2   VARCHAR2(1200);
   L_tran_seq_no          SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no               SA_TRAN_HEAD.REV_NO%TYPE;
   L_store                SA_STORE_DAY.STORE%TYPE;
   L_day                  SA_STORE_DAY.DAY%TYPE;
   L_audit_status         SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status          SA_STORE_DAY.DATA_STATUS%TYPE;
   L_update_ind           BOOLEAN := FALSE;
   L_store_day_seq_no     SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_item_seq_no          SA_TRAN_DISC.ITEM_SEQ_NO%TYPE;
   L_discount_seq_no      SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE;
   L_disc_type            SA_TRAN_DISC.DISC_TYPE%TYPE;
   L_coupon_no            SA_TRAN_DISC.COUPON_NO%TYPE;
   L_rms_promo_type       SA_TRAN_DISC.RMS_PROMO_TYPE%TYPE;
   L_promotion            SA_TRAN_DISC.PROMOTION%TYPE;
   L_promo_comp           SA_TRAN_DISC.PROMO_COMP%TYPE;
   L_error_ind            SA_TRAN_DISC.ERROR_IND%TYPE;
   L_tran_type            SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_LOCK_TRAN_DISC is
      select 'x'
        from sa_tran_disc std
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, std.tran_seq_no)
                       and se.tran_seq_no = std.tran_seq_no
                       and se.store = nvl(I_store, std.store)
                       and se.store = std.store
                       and se.day = nvl(I_day, std.day)
                       and se.day = std.day
                       and se.key_value_1 = nvl(I_item_seq_no, std.item_seq_no)
                       and se.key_value_1 = std.item_seq_no
                       and se.key_value_2 = nvl(I_discount_seq_no, std.discount_seq_no)
                       and se.key_value_2 = std.discount_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'IDISC'
                       and se.error_code = I_error_code)
         for update nowait;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_disc std
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, std.tran_seq_no)
                                      and se.tran_seq_no = std.tran_seq_no
                                      and se.store = nvl(I_store, std.store)
                                      and se.store = std.store
                                      and se.day = nvl(I_day, std.day)
                                      and se.day = std.day
                                      and se.key_value_1 = nvl(I_item_seq_no, std.item_seq_no)
                                      and se.key_value_1 = std.item_seq_no
                                      and se.key_value_2 = nvl(I_discount_seq_no, std.discount_seq_no)
                                      and se.key_value_2 = std.discount_seq_no
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type = 'IDISC'
                                      and se.error_code = I_error_code)
                        and th.tran_seq_no = std.tran_seq_no
                        and th.store       = std.store
                        and th.day         = std.day);

   cursor C_SA_TRAN_DISC_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_disc std
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, std.tran_seq_no)
                       and se.tran_seq_no = std.tran_seq_no
                       and se.store = nvl(I_store, std.store)
                       and se.store = std.store
                       and se.day = nvl(I_day, std.day)
                       and se.day = std.day
                       and se.key_value_1 = nvl(I_item_seq_no, std.item_seq_no)
                       and se.key_value_1 = std.item_seq_no
                       and se.key_value_2 = nvl(I_discount_seq_no, std.discount_seq_no)
                       and se.key_value_2 = std.discount_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'IDISC'
                       and se.error_code = I_error_code)
        and std.tran_seq_no = C_tran_seq_no;
BEGIN

   open C_LOCK_TRAN_DISC;
   close C_LOCK_TRAN_DISC;

   L_update_statement :=   'update sa_tran_disc std'
                         ||'   set '||I_fix_column||' = :I_new_value'
                         ||' where std.store = :L_store '
                         ||'   and std.day = :L_day '
                         ||'   and std.tran_seq_no = :L_tran_seq_no'
                         ||'   and std.item_seq_no = :L_item_seq_no'
                         ||'   and std.discount_seq_no = :L_discount_seq_no';

   L_update_statement_2 :=   'update sa_tran_disc std'
                           ||'   set '||I_fix_column||' = :I_new_value'
                           ||' where std.store = :L_store '
                           ||'   and std.day = :L_day '
                           ||'   and std.tran_seq_no = :L_tran_seq_no'
                           ||'   and std.item_seq_no = :L_item_seq_no'
                           ||'   and std.discount_seq_no = :L_discount_seq_no'
                           ||'   and std.promotion = :I_promotion';



   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_update_ind       := FALSE;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      L_tran_type        := rec.tran_type;

      ---
      for rec in C_SA_TRAN_DISC_REC(L_tran_seq_no) loop
         L_item_seq_no     := rec.item_seq_no;
         L_discount_seq_no := rec.discount_seq_no;
         L_disc_type       := rec.disc_type;
         L_coupon_no       := rec.coupon_no;
         L_rms_promo_type  := rec.rms_promo_type;
         L_promotion       := rec.promotion;
         L_promo_comp      := rec.promo_comp;
         L_error_ind       := rec.error_ind;
         
         if I_fix_column in ('PROMOTION') then
            L_promotion := I_new_value;
         elsif I_fix_column in ('PROMO_COMP') then
            L_promo_comp := I_new_value;
         elsif I_fix_column in ('RMS_PROMO_TYPE') then
            L_rms_promo_type := I_new_value;
         elsif I_fix_column in ('DISC_TYPE') then
            L_disc_type := I_new_value;
         end if;

         if I_fix_column in ('PROMO_COMP') then
            EXECUTE IMMEDIATE L_update_statement_2
            USING I_new_value, L_store, L_day, L_tran_seq_no, L_item_seq_no, L_discount_seq_no, I_promotion;
         else
            EXECUTE IMMEDIATE L_update_statement
            USING I_new_value, L_store, L_day, L_tran_seq_no, L_item_seq_no, L_discount_seq_no;
         end if;
         L_update_ind := TRUE;


         if REFRESH_DISCOUNT_ERRORS(O_error_message,
                                    L_error_ind,
                                    I_fix_column,
                                    L_store_day_seq_no,
                                    L_tran_seq_no,
                                    L_item_seq_no,
                                    L_discount_seq_no,
                                    L_store,
                                    L_day,
                                    L_disc_type,
                                    L_coupon_no,
                                    L_rms_promo_type,
                                    L_promotion,
                                    L_promo_comp) = FALSE then
            return FALSE;
         end if;
         
         update sa_tran_disc std
            set error_ind = L_error_ind
          where std.store = L_store
            and std.day = L_day
            and std.tran_seq_no = L_tran_seq_no
            and std.item_seq_no = L_item_seq_no
            and std.discount_seq_no = L_discount_seq_no;

      end loop;

      if L_update_ind = TRUE then
         if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                             L_tran_seq_no,
                                             L_rev_no,
                                             L_store,
                                             L_day) = FALSE then
            return FALSE;
         end if;

         open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
         close C_LOCK_TRAN_HEAD;
         ---
         update sa_tran_head
            set rev_no      = (L_rev_no + 1),
                update_id = get_user,
                update_datetime = sysdate
          where tran_seq_no = L_tran_seq_no
            and store       = L_store
            and day         = L_day;
            
         if TOTAL_AUDIT_CHECK(O_error_message,
                              L_audit_status,
                              L_data_status,
                              L_store_day_seq_no,
                              L_store,
                              L_day) = FALSE then
            return FALSE;
         end if;
      end if;

   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_ITEM_DISCOUNTS_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_ITEM_IGTAX_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tran_seq_no     IN       SA_TRAN_IGTAX.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                                I_item_seq_no     IN       SA_TRAN_IGTAX.ITEM_SEQ_NO%TYPE DEFAULT NULL,
                                I_igtax_seq_no    IN       SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE DEFAULT NULL,
                                I_store           IN       SA_TRAN_IGTAX.STORE%TYPE DEFAULT NULL,
                                I_day             IN       SA_TRAN_IGTAX.DAY%TYPE DEFAULT NULL,
                                I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                                I_error_code      IN       SA_ERROR_CODES.ERROR_CODE%TYPE,
                                I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                                I_new_value       IN       VARCHAR2)

RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_ITEM_IGTAX_UPDATE';
   L_update_statement   VARCHAR2(1200);
   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;
   L_sub_tran_type      SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_rtlog_orig_sys     SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE;
   L_tran_process_sys   SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE;
   L_item_seq_no        SA_TRAN_IGTAX.ITEM_SEQ_NO%TYPE;
   L_igtax_seq_no       SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE;
   L_update_ind         BOOLEAN := FALSE;

   cursor C_LOCK_TRAN_IGTAX is
      select 'x'
        from sa_tran_igtax stx
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, stx.tran_seq_no)
                       and se.tran_seq_no = stx.tran_seq_no
                       and se.store = nvl(I_store, stx.store)
                       and se.store = stx.store
                       and se.day = nvl(I_day, stx.day)
                       and se.day = stx.day
                       and se.key_value_1 = nvl(I_item_seq_no, stx.item_seq_no)
                       and se.key_value_1 = stx.item_seq_no
                       and se.key_value_2 = nvl(I_igtax_seq_no, stx.igtax_seq_no)
                       and se.key_value_2 = stx.igtax_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'IGTAX'
                       and se.error_code = I_error_code)
         for update nowait;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_igtax stx
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, stx.tran_seq_no)
                                      and se.tran_seq_no = stx.tran_seq_no
                                      and se.store = nvl(I_store, stx.store)
                                      and se.store = stx.store
                                      and se.day = nvl(I_day, stx.day)
                                      and se.day = stx.day
                                      and se.key_value_1 = nvl(I_item_seq_no, stx.item_seq_no)
                                      and se.key_value_1 = stx.item_seq_no
                                      and se.key_value_2 = nvl(I_igtax_seq_no, stx.igtax_seq_no)
                                      and se.key_value_2 = stx.igtax_seq_no
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type = 'IGTAX'
                                      and se.error_code = I_error_code)
                         and th.tran_seq_no = stx.tran_seq_no
                         and th.store       = stx.store
                         and th.day         = stx.day);

   cursor C_SA_TRAN_IGTAX_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_igtax stx
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, stx.tran_seq_no)
                       and se.tran_seq_no = stx.tran_seq_no
                       and se.store = nvl(I_store, stx.store)
                       and se.store = stx.store
                       and se.day = nvl(I_day, stx.day)
                       and se.day = stx.day
                       and se.key_value_1 = nvl(I_item_seq_no, stx.item_seq_no)
                       and se.key_value_1 = stx.item_seq_no
                       and se.key_value_2 = nvl(I_igtax_seq_no, stx.igtax_seq_no)
                       and se.key_value_2 = stx.igtax_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'IGTAX'
                       and se.error_code = I_error_code)
         and stx.tran_seq_no = C_tran_seq_no;
BEGIN

   open C_LOCK_TRAN_IGTAX;
   close C_LOCK_TRAN_IGTAX;

   L_update_statement :=   'update sa_tran_igtax stx'
                         ||'   set '||I_fix_column||' = :I_new_value'
                         ||' where stx.store = :L_store '
                         ||'   and stx.day = :L_day '
                         ||'   and stx.tran_seq_no = :L_tran_seq_no'
                         ||'   and stx.item_seq_no = :L_item_seq_no'
                         ||'   and stx.igtax_seq_no = :L_igtax_seq_no';

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_tran_type        := rec.tran_type;
      L_sub_tran_type    := rec.sub_tran_type;
      L_update_ind       := FALSE;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_rtlog_orig_sys   := rec.rtlog_orig_sys;
      L_tran_process_sys := rec.tran_process_sys;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      ---
      for rec in C_SA_TRAN_IGTAX_REC(L_tran_seq_no) loop
         L_item_seq_no      := rec.item_seq_no;
         L_igtax_seq_no     := rec.igtax_seq_no;

         L_update_ind := TRUE;
         EXECUTE IMMEDIATE L_update_statement
         USING I_new_value,L_store, L_day, L_tran_seq_no, L_item_seq_no, L_igtax_seq_no;

      end loop;

      /* REFRESH ERRORS */
      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if REFRESH_ERRORS(O_error_message,
                        I_fix_column,
                        L_store_day_seq_no,
                        L_tran_seq_no,
                        L_store,
                        L_day,
                        L_tran_type,
                        L_rtlog_orig_sys,
                        L_tran_process_sys,
                        'SA_TRAN_IGTAX') = FALSE then
         return FALSE;
      end if;
      ---
      if L_update_ind = TRUE then
         if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                             L_tran_seq_no,
                                             L_rev_no,
                                             L_store,
                                             L_day) = FALSE then
            return FALSE;
         end if;

         open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
         close C_LOCK_TRAN_HEAD;
         ---
         update sa_tran_head
            set rev_no      = (L_rev_no + 1),
                update_id = get_user,
                update_datetime = sysdate
          where tran_seq_no = L_tran_seq_no
            and store       = L_store
            and day         = L_day;
            
         if TOTAL_AUDIT_CHECK(O_error_message,
                              L_audit_status,
                              L_data_status,
                              L_store_day_seq_no,
                              L_store,
                              L_day) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_ITEM_IGTAX_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TENDER_UPDATE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tran_seq_no         IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                            I_tender_seq_no       IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE DEFAULT NULL,
                            I_store               IN       SA_TRAN_TENDER.STORE%TYPE DEFAULT NULL,
                            I_day                 IN       SA_TRAN_TENDER.DAY%TYPE DEFAULT NULL,
                            I_business_date       IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                            I_error_code          IN       SA_ERROR.ERROR_CODE%TYPE,
                            I_fix_column          IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                            I_new_value           IN       VARCHAR2,
                            I_tender_type_group   IN   SA_TRAN_TENDER.TENDER_TYPE_GROUP%TYPE)

RETURN BOOLEAN IS

   L_program              VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_TENDER_UPDATE';
   L_update_statement     VARCHAR2(1200);
   L_update_statement_2   VARCHAR2(1200);
   L_tran_seq_no          SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no               SA_TRAN_HEAD.REV_NO%TYPE;
   L_store                SA_STORE_DAY.STORE%TYPE;
   L_day                  SA_STORE_DAY.DAY%TYPE;
   L_audit_status         SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status          SA_STORE_DAY.DATA_STATUS%TYPE;
   L_tran_type            SA_TRAN_HEAD.TRAN_TYPE%TYPE;
   L_sub_tran_type        SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE;
   L_store_day_seq_no     SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_rtlog_orig_sys       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE;
   L_tran_process_sys     SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE;
   L_tender_seq_no        SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE;
   L_update_ind           BOOLEAN := FALSE;

   cursor C_LOCK_TENDER is
      select 'x'
        from sa_tran_tender stt
       where exists (select 'x'
                       from sa_error se
                      where se.tran_seq_no = nvl(I_tran_seq_no, stt.tran_seq_no)
                        and se.tran_seq_no = stt.tran_seq_no
                        and se.store = nvl(I_store, stt.store)
                        and se.store = stt.store
                        and se.day = nvl(I_day, stt.day)
                        and se.day = stt.day
                        and se.key_value_1 = nvl(I_tender_seq_no, stt.tender_seq_no)
                        and se.key_value_1 = stt.tender_seq_no
                        and se.store_day_seq_no = nvl((select store_day_seq_no
                                                         from sa_store_day sa
                                                        where sa.store = se.store
                                                          and sa.business_date = I_business_date),
                                                       se.store_day_seq_no)
                        and se.rec_type = 'TTEND'
                        and se.error_code = I_error_code)
         for update nowait;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_tender stt
                      where exists (select 'x'
                                      from sa_error se
                                     where se.tran_seq_no = nvl(I_tran_seq_no, stt.tran_seq_no)
                                       and se.tran_seq_no = stt.tran_seq_no
                                       and se.store = nvl(I_store, stt.store)
                                       and se.store = stt.store
                                       and se.day = nvl(I_day, stt.day)
                                       and se.day = stt.day
                                       and se.key_value_1 = nvl(I_tender_seq_no, stt.tender_seq_no)
                                       and se.key_value_1 = stt.tender_seq_no
                                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                        from sa_store_day sa
                                                                       where sa.store = se.store
                                                                         and sa.business_date = I_business_date),
                                                                      se.store_day_seq_no)
                                       and se.rec_type = 'TTEND'
                                       and se.error_code = I_error_code)
                        and th.tran_seq_no = stt.tran_seq_no
                        and th.store       = stt.store
                        and th.day         = stt.day);

   cursor C_SA_TRAN_TENDER_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_tender stt
       where exists (select 'x'
                       from sa_error se
                      where se.tran_seq_no = nvl(I_tran_seq_no, stt.tran_seq_no)
                        and se.tran_seq_no = stt.tran_seq_no
                        and se.store = nvl(I_store, stt.store)
                        and se.store = stt.store
                        and se.day = nvl(I_day, stt.day)
                        and se.day = stt.day
                        and se.key_value_1 = nvl(I_tender_seq_no, stt.tender_seq_no)
                        and se.key_value_1 = stt.tender_seq_no
                        and se.store_day_seq_no = nvl((select store_day_seq_no
                                                         from sa_store_day sa
                                                        where sa.store = se.store
                                                          and sa.business_date = I_business_date),
                                                       se.store_day_seq_no)
                        and se.rec_type = 'TTEND'
                        and se.error_code = I_error_code)
         and stt.tran_seq_no = C_tran_seq_no;
BEGIN

   open C_LOCK_TENDER;
   close C_LOCK_TENDER;

   L_update_statement :=   'update sa_tran_tender stt'
                         ||'   set '||I_fix_column||' = :I_new_value'
                         ||' where stt.store = :L_store '
                         ||'   and stt.day = :L_day '
                         ||'   and stt.tran_seq_no = :L_tran_seq_no'
                         ||'   and stt.tender_seq_no = :L_tender_seq_no';
                         
   L_update_statement_2 :=   'update sa_tran_tender stt'
                           ||'   set '||I_fix_column||' = :I_new_value'
                           ||' where stt.store = :L_store '
                           ||'   and stt.day = :L_day '
                           ||'   and stt.tran_seq_no = :L_tran_seq_no'
                           ||'   and stt.tender_seq_no = :L_tender_seq_no'
                           ||'   and stt.tender_type_group = :I_tender_type_group';

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_tran_type        := rec.tran_type;
      L_sub_tran_type    := rec.sub_tran_type;
      L_update_ind       := FALSE;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_rtlog_orig_sys   := rec.rtlog_orig_sys;
      L_tran_process_sys := rec.tran_process_sys;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      ---
      for rec in C_SA_TRAN_TENDER_REC(L_tran_seq_no) loop
         L_tender_seq_no      := rec.tender_seq_no;
         L_update_ind := TRUE;

         if I_fix_column in ('TENDER_TYPE_ID') then
            EXECUTE IMMEDIATE L_update_statement_2
            USING I_new_value, L_store, L_day, L_tran_seq_no, L_tender_seq_no, I_tender_type_group;
         else
            EXECUTE IMMEDIATE L_update_statement
            USING I_new_value, L_store, L_day, L_tran_seq_no, L_tender_seq_no;
         end if;

      end loop;

      /* REFRESH ERRORS */
      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if REFRESH_ERRORS(O_error_message,
                        I_fix_column,
                        L_store_day_seq_no,
                        L_tran_seq_no,
                        L_store,
                        L_day,
                        L_tran_type,
                        L_rtlog_orig_sys,
                        L_tran_process_sys,
                        'SA_TRAN_TENDER') = FALSE then
         return FALSE;
      end if;
      ---
      if L_update_ind = TRUE then
         if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                             L_tran_seq_no,
                                             L_rev_no,
                                             L_store,
                                             L_day) = FALSE then
            return FALSE;
         end if;

         open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
         close C_LOCK_TRAN_HEAD;
         ---
         update sa_tran_head
            set rev_no      = (L_rev_no + 1),
                update_id = get_user,
                update_datetime = sysdate
          where tran_seq_no = L_tran_seq_no
            and store       = L_store
            and day         = L_day;

         if TOTAL_AUDIT_CHECK(O_error_message,
                              L_audit_status,
                              L_data_status,
                              L_store_day_seq_no,
                              L_store,
                              L_day) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_TENDER_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TAX_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tran_seq_no     IN       SA_TRAN_TAX.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                         I_tax_seq_no      IN       SA_TRAN_TAX.TAX_SEQ_NO%TYPE DEFAULT NULL,
                         I_store           IN       SA_TRAN_TAX.STORE%TYPE DEFAULT NULL,
                         I_day             IN       SA_TRAN_TAX.DAY%TYPE DEFAULT NULL,
                         I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                         I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                         I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                         I_new_value       IN       VARCHAR2)

RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_TAX_UPDATE';
   L_update_statement   VARCHAR2(1200);
   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;
   L_sub_tran_type      SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_rtlog_orig_sys     SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE;
   L_tran_process_sys   SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE;
   L_tax_seq_no         SA_TRAN_TAX.TAX_SEQ_NO%TYPE;
   L_update_ind         BOOLEAN := FALSE;

   cursor C_LOCK_TAX is
      select 'x'
        from sa_tran_tax sttx
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sttx.tran_seq_no)
                       and se.tran_seq_no = sttx.tran_seq_no
                       and se.store = nvl(I_store, sttx.store)
                       and se.store =  sttx.store
                       and se.day = nvl(I_day, sttx.day)
                       and se.day = sttx.day
                       and se.key_value_1 = nvl(I_tax_seq_no, sttx.tax_seq_no)
                       and se.key_value_1 = sttx.tax_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'TTAX'
                       and se.error_code = I_error_code)
         for update nowait;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_tax sttx
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, sttx.tran_seq_no)
                                      and se.tran_seq_no = sttx.tran_seq_no
                                      and se.store = nvl(I_store, sttx.store)
                                      and se.store =  sttx.store
                                      and se.day = nvl(I_day, sttx.day)
                                      and se.day = sttx.day
                                      and se.key_value_1 = nvl(I_tax_seq_no, sttx.tax_seq_no)
                                      and se.key_value_1 = sttx.tax_seq_no
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type = 'TTAX'
                                      and se.error_code = I_error_code)
                        and th.tran_seq_no = sttx.tran_seq_no
                        and th.store       = sttx.store
                        and th.day         = sttx.day);

   cursor C_SA_TRAN_TAX_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_tran_tax sttx
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sttx.tran_seq_no)
                       and se.tran_seq_no = sttx.tran_seq_no
                       and se.store = nvl(I_store, sttx.store)
                       and se.store =  sttx.store
                       and se.day = nvl(I_day, sttx.day)
                       and se.day = sttx.day
                       and se.key_value_1 = nvl(I_tax_seq_no, sttx.tax_seq_no)
                       and se.key_value_1 = sttx.tax_seq_no
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'TTAX'
                       and se.error_code = I_error_code)
         and sttx.tran_seq_no = C_tran_seq_no;
BEGIN

   open C_LOCK_TAX;
   close C_LOCK_TAX;

   L_update_statement :=   'update sa_tran_tax sttx'
                         ||'   set '||I_fix_column||' = :I_new_value'
                         ||' where sttx.store = :L_store '
                         ||'   and sttx.day = :L_day '
                         ||'   and sttx.tran_seq_no = :L_tran_seq_no'
                         ||'   and sttx.tax_seq_no = :L_tax_seq_no';

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_tran_type        := rec.tran_type;
      L_sub_tran_type    := rec.sub_tran_type;
      L_update_ind       := FALSE;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_rtlog_orig_sys   := rec.rtlog_orig_sys;
      L_tran_process_sys := rec.tran_process_sys;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      ---
      for rec in C_SA_TRAN_TAX_REC(L_tran_seq_no) loop
         L_tax_seq_no      := rec.tax_seq_no;
         L_update_ind := TRUE;

         EXECUTE IMMEDIATE L_update_statement
         USING I_new_value,L_store, L_day, L_tran_seq_no, L_tax_seq_no;

      end loop;

      /* REFRESH ERRORS */
      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if REFRESH_ERRORS(O_error_message,
                        I_fix_column,
                        L_store_day_seq_no,
                        L_tran_seq_no,
                        L_store,
                        L_day,
                        L_tran_type,
                        L_rtlog_orig_sys,
                        L_tran_process_sys,
                        'SA_TRAN_TAX') = FALSE then
         return FALSE;
      end if;
      ---
      if L_update_ind = TRUE then
         if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                             L_tran_seq_no,
                                             L_rev_no,
                                             L_store,
                                             L_day) = FALSE then
            return FALSE;
         end if;

         open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
         close C_LOCK_TRAN_HEAD;
         ---
         update sa_tran_head
            set rev_no      = (L_rev_no + 1),
                update_id = get_user,
                update_datetime = sysdate
          where tran_seq_no = L_tran_seq_no
            and store       = L_store
            and day         = L_day;
         
         if TOTAL_AUDIT_CHECK(O_error_message,
                              L_audit_status,
                              L_data_status,
                              L_store_day_seq_no,
                              L_store,
                              L_day) = FALSE then
            return FALSE;
         end if;
      end if;

   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_TAX_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_CUSTOMER_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tran_seq_no     IN       SA_CUSTOMER.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                              I_store           IN       SA_CUSTOMER.STORE%TYPE DEFAULT NULL,
                              I_day             IN       SA_CUSTOMER.DAY%TYPE DEFAULT NULL,
                              I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                              I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                              I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                              I_new_value       IN       VARCHAR2)

RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_CUSTOMER_UPDATE';
   L_update_statement   VARCHAR2(1200);
   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_cust_id_type       SA_CUSTOMER.CUST_ID_TYPE%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_LOCK_CUSTOMER is
      select 'x'
        from sa_customer sc
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sc.tran_seq_no)
                       and se.tran_seq_no = sc.tran_seq_no
                       and se.store = nvl(I_store, sc.store)
                       and se.store = sc.store
                       and se.day = nvl(I_day, sc.day)
                       and se.day = sc.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type in ('TCUST','TCTYPE')
                       and se.error_code = I_error_code)
         for update nowait;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_customer sc
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, sc.tran_seq_no)
                                      and se.tran_seq_no = sc.tran_seq_no
                                      and se.store = nvl(I_store, sc.store)
                                      and se.store = sc.store
                                      and se.day = nvl(I_day, sc.day)
                                      and se.day = sc.day
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type in ('TCUST','TCTYPE')
                                      and se.error_code = I_error_code)
                        and th.tran_seq_no = sc.tran_seq_no
                        and th.store       = sc.store
                        and th.day         = sc.day);

   cursor C_SA_TRAN_CUST_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_customer sc
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sc.tran_seq_no)
                       and se.tran_seq_no = sc.tran_seq_no
                       and se.store = nvl(I_store, sc.store)
                       and se.store = sc.store
                       and se.day = nvl(I_day, sc.day)
                       and se.day = sc.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type in ('TCUST','TCTYPE')
                       and se.error_code = I_error_code)
         and sc.tran_seq_no = C_tran_seq_no;

BEGIN

   open C_LOCK_CUSTOMER;
   close C_LOCK_CUSTOMER;

   L_update_statement :=   'update sa_customer sc'
                         ||'   set '||I_fix_column||' = :I_new_value'
                         ||' where sc.store = :L_store '
                         ||'   and sc.day = :L_day '
                         ||'   and sc.tran_seq_no = :L_tran_seq_no';

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no      := rec.tran_seq_no;
      L_rev_no           := rec.rev_no;
      L_store            := rec.store;
      L_day              := rec.day;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      L_tran_type          := rec.tran_type;
      ---
      for rec in C_SA_TRAN_CUST_REC(L_tran_seq_no) loop
         if I_fix_column = 'CUST_ID_TYPE' then
            L_cust_id_type := I_new_value;
         else
            L_cust_id_type := rec.cust_id_type;
         end if;

         EXECUTE IMMEDIATE L_update_statement
         USING I_new_value,L_store, L_day, L_tran_seq_no;

         if L_cust_id_type != 'ERR' then
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          L_store_day_seq_no,
                                          L_tran_seq_no,
                                          'INVLD_CIDT',
                                          NULL,
                                          NULL,
                                          'TCTYPE') = FALSE then
               return FALSE;
            end if;
         end if;
      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_CUSTOMER_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_CUST_ATTR_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tran_seq_no     IN       SA_CUST_ATTRIB.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                               I_attrib_seq_no   IN       SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE DEFAULT NULL,
                               I_store           IN       SA_CUST_ATTRIB.STORE%TYPE DEFAULT NULL,
                               I_day             IN       SA_CUST_ATTRIB.DAY%TYPE DEFAULT NULL,
                               I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                               I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                               I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                               I_new_value       IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_CUST_ATTR_UPDATE';
   L_update_statement   VARCHAR2(1200);
   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_attrib_type        SA_CUST_ATTRIB.ATTRIB_TYPE%TYPE;
   L_attrib_seq_no      SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;

   cursor C_LOCK_CUST_ATTRIB is
      select 'x'
        from sa_cust_attrib sca
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sca.tran_seq_no)
                       and se.tran_seq_no = sca.tran_seq_no
                       and se.key_value_1 = nvl(I_attrib_seq_no, sca.attrib_seq_no)
                       and se.key_value_1 = sca.attrib_seq_no
                       and se.store = nvl(I_store, sca.store)
                       and se.store = sca.store
                       and se.day = nvl(I_day, sca.day)
                       and se.day = sca.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'CATT'
                       and se.error_code = I_error_code)
         for update nowait;

   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_cust_attrib sca
                       where exists(select 'x'
                                      from sa_error se
                                     where se.tran_seq_no = nvl(I_tran_seq_no, sca.tran_seq_no)
                                       and se.tran_seq_no = sca.tran_seq_no
                                       and se.key_value_1 = nvl(I_attrib_seq_no, sca.attrib_seq_no)
                                       and se.key_value_1 = sca.attrib_seq_no
                                       and se.store = nvl(I_store, sca.store)
                                       and se.store = sca.store
                                       and se.day = nvl(I_day, sca.day)
                                       and se.day = sca.day
                                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                        from sa_store_day sa
                                                                       where sa.store = se.store
                                                                         and sa.business_date = I_business_date),
                                                                      se.store_day_seq_no)
                                       and se.rec_type = 'CATT'
                                       and se.error_code = I_error_code)
                        and th.tran_seq_no = sca.tran_seq_no
                        and th.store       = sca.store
                        and th.day         = sca.day);

   cursor C_SA_TRAN_CATT_REC(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE) is
      select *
        from sa_cust_attrib sca
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sca.tran_seq_no)
                       and se.tran_seq_no = sca.tran_seq_no
                       and se.key_value_1 = nvl(I_attrib_seq_no, sca.attrib_seq_no)
                       and se.key_value_1 = sca.attrib_seq_no
                       and se.store = nvl(I_store, sca.store)
                       and se.store = sca.store
                       and se.day = nvl(I_day, sca.day)
                       and se.day = sca.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type = 'CATT'
                       and se.error_code = I_error_code)
         and sca.tran_seq_no = C_tran_seq_no;
BEGIN

   open C_LOCK_CUST_ATTRIB;
   close C_LOCK_CUST_ATTRIB;

   L_update_statement :=   'update sa_cust_attrib sca'
                         ||'   set '||I_fix_column||' = :I_new_value'
                         ||' where sca.store = :L_store '
                         ||'   and sca.day = :L_day '
                         ||'   and sca.tran_seq_no = :L_tran_seq_no'
                         ||'   and sca.attrib_seq_no = :L_attrib_seq_no';

   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no := rec.tran_seq_no;
      L_rev_no      := rec.rev_no;
      L_store       := rec.store;
      L_day         := rec.day;
      L_store_day_seq_no := rec.store_day_seq_no;
      L_audit_status     := rec.audit_status;
      L_data_status      := rec.data_status;
      L_tran_type          := rec.tran_type;

      ---
      for rec in C_SA_TRAN_CATT_REC(L_tran_seq_no) loop

         L_attrib_seq_no := rec.attrib_seq_no;

         if I_fix_column in ('ATTRIB_TYPE') then
            L_attrib_type := I_new_value;
         else
            L_attrib_type := rec.attrib_type;
         end if;

         EXECUTE IMMEDIATE L_update_statement
         USING I_new_value,L_store, L_day, L_tran_seq_no, L_attrib_seq_no;


         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       L_store_day_seq_no,
                                       L_tran_seq_no,
                                       I_error_code,
                                       L_attrib_seq_no,
                                       NULL,
                                       'CATT') = FALSE then
            return FALSE;
            end if;

      end loop;

      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_CUST_ATTR_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION MASS_TRAN_HEAD_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE DEFAULT NULL,
                               I_store           IN       SA_TRAN_HEAD.STORE%TYPE DEFAULT NULL,
                               I_day             IN       SA_TRAN_HEAD.DAY%TYPE DEFAULT NULL,
                               I_business_date   IN       SA_STORE_DAY.BUSINESS_DATE%TYPE DEFAULT NULL,
                               I_error_code      IN       SA_ERROR.ERROR_CODE%TYPE,
                               I_fix_column      IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                               I_new_value       IN       VARCHAR2)

RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.MASS_TRAN_HEAD_UPDATE';
   L_update_statement   VARCHAR2(1200);
   L_tran_seq_no        SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE;
   L_rev_no             SA_TRAN_HEAD.REV_NO%TYPE;
   L_store              SA_STORE_DAY.STORE%TYPE;
   L_day                SA_STORE_DAY.DAY%TYPE;
   L_store_day_seq_no   SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE;
   L_tran_type          SA_TRAN_HEAD.TRAN_TYPE%TYPE;
   L_sub_tran_type      SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE;
   L_orig_tran_type     SA_TRAN_HEAD.ORIG_TRAN_TYPE%TYPE;
   L_reason_code        SA_TRAN_HEAD.REASON_CODE%TYPE;
   L_error_ind          SA_TRAN_HEAD.ERROR_IND%TYPE;
   L_audit_status       SA_STORE_DAY.AUDIT_STATUS%TYPE;
   L_data_status        SA_STORE_DAY.DATA_STATUS%TYPE;


   cursor C_LOCK_HEAD is
      select 'x'
        from sa_tran_head sth
       where exists(select 'x'
                      from sa_error se
                     where se.tran_seq_no = nvl(I_tran_seq_no, sth.tran_seq_no)
                       and se.tran_seq_no = sth.tran_seq_no
                       and se.store = nvl(I_store, sth.store)
                       and se.store = sth.store
                       and se.day = nvl(I_day, sth.day)
                       and se.day = sth.day
                       and se.store_day_seq_no = nvl((select store_day_seq_no
                                                        from sa_store_day sa
                                                       where sa.store = se.store
                                                         and sa.business_date = I_business_date),
                                                      se.store_day_seq_no)
                       and se.rec_type in ('THEAD','THREAC','FHEAD','THSUBT','THTRAT')
                       and se.error_code = I_error_code)
         for update nowait;


   cursor C_LOCK_TRAN_HEAD(C_tran_seq_no   SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                           C_store         SA_TRAN_HEAD.STORE%TYPE,
                           C_day           SA_TRAN_HEAD.DAY%TYPE) is
      select 'x'
        from sa_tran_head
       where tran_seq_no    = C_tran_seq_no
         and store          = C_store
         and day            = C_day
         for update nowait;

   cursor C_TRAN_SEQ_REV_NO is
      select th.*,
             sd.audit_status,
             sd.data_status
        from sa_tran_head th,
             sa_store_day sd
       where th.store_day_seq_no = sd.store_day_seq_no
         and th.store            = sd.store
         and th.day              = sd.day
         and sd.store            = nvl(I_store, sd.store)
         and sd.business_date    = nvl(I_business_date, sd.business_date)
         and exists (select 'x'
                       from sa_tran_head sth
                      where exists(select 'x'
                                     from sa_error se
                                    where se.tran_seq_no = nvl(I_tran_seq_no, sth.tran_seq_no)
                                      and se.tran_seq_no = sth.tran_seq_no
                                      and se.store = nvl(I_store, sth.store)
                                      and se.store = sth.store
                                      and se.day = nvl(I_day, sth.day)
                                      and se.day = sth.day
                                      and se.store_day_seq_no = nvl((select store_day_seq_no
                                                                       from sa_store_day sa
                                                                      where sa.store = se.store
                                                                        and sa.business_date = I_business_date),
                                                                     se.store_day_seq_no)
                                      and se.rec_type in ('THEAD','THREAC','FHEAD','THSUBT','THTRAT')
                                      and se.error_code = I_error_code)
                        and th.tran_seq_no = sth.tran_seq_no
                        and th.store       = sth.store
                        and th.day         = sth.day);

  cursor C_LOCK_ERROR(C_tran_seq_no SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      C_store SA_TRAN_HEAD.STORE%TYPE,
                      C_day SA_TRAN_HEAD.DAY%TYPE,
                      C_store_day_seq_no SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE) is
      select 'x'
        from sa_error se
       where se.tran_seq_no = C_tran_seq_no
         and se.store = C_store
         and se.day = C_day
         and se.store_day_seq_no = C_store_day_seq_no
         and se.rec_type in ('THEAD','THREAC','FHEAD','THSUBT','THTRAT')
         and se.error_code = I_error_code;
BEGIN

   open C_LOCK_HEAD;
   close C_LOCK_HEAD;

   L_update_statement :=   'update sa_tran_head sth'
                         ||'   set '||I_fix_column||' = :I_new_value'
                         ||' where sth.store = :L_store '
                         ||'   and sth.day = :L_day '
                         ||'   and sth.tran_seq_no = :L_tran_seq_no'
                         ||'   and sth.store_day_seq_no = :L_store_day_seq_no';


   for rec in C_TRAN_SEQ_REV_NO loop
      L_tran_seq_no        := rec.tran_seq_no;
      L_rev_no             := rec.rev_no;
      L_store              := rec.store;
      L_day                := rec.day;
      L_store_day_seq_no   := rec.store_day_seq_no;
      L_tran_type          := rec.tran_type;
      L_sub_tran_type      := rec.sub_tran_type;
      L_orig_tran_type     := rec.orig_tran_type;
      L_reason_code        := rec.reason_code;
      L_error_ind          := rec.error_ind;
      L_audit_status       := rec.audit_status;
      L_data_status        := rec.data_status;

      EXECUTE IMMEDIATE L_update_statement
      USING I_new_value,L_store, L_day, L_tran_seq_no, L_store_day_seq_no;

      open C_LOCK_ERROR(L_tran_seq_no, L_store, L_day, L_store_day_seq_no);
      close C_LOCK_ERROR;

      delete from sa_error se
       where se.tran_seq_no = L_tran_seq_no
         and se.store = L_store
         and se.day = L_day
         and se.store_day_seq_no = L_store_day_seq_no
         and se.rec_type in ('THEAD','THREAC','FHEAD','THSUBT','THTRAT')
         and se.error_code = I_error_code;

      if REFRESH_HEADER_ERROR(O_error_message,
                              L_error_ind,
                              L_tran_seq_no,
                              L_store_day_seq_no,
                              L_tran_type,
                              L_sub_tran_type,
                              L_orig_tran_type,
                              L_reason_code) = FALSE then
         return FALSE;
      end if;

      update sa_tran_head sth
         set error_ind = L_error_ind
       where sth.tran_seq_no = L_tran_seq_no
         and sth.store = L_store
         and sth.day = L_day
         and sth.store_day_seq_no = L_store_day_seq_no;
      ---
      if TRANSACTION_SQL.CREATE_REVISIONS(O_error_message,
                                          L_tran_seq_no,
                                          L_rev_no,
                                          L_store,
                                          L_day) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_TRAN_HEAD(L_tran_seq_no, L_store, L_day);
      close C_LOCK_TRAN_HEAD;
      ---
      update sa_tran_head
         set rev_no      = (L_rev_no + 1),
             update_id = get_user,
             update_datetime = sysdate
       where tran_seq_no = L_tran_seq_no
         and store       = L_store
         and day         = L_day;

      if REMOVE_ERRORS(O_error_message,
                       L_tran_seq_no,
                       L_store,
                       L_day,
                       L_tran_type) = FALSE then
         return FALSE;
      end if;

      if TOTAL_AUDIT_CHECK(O_error_message,
                           L_audit_status,
                           L_data_status,
                           L_store_day_seq_no,
                           L_store,
                           L_day) = FALSE then
         return FALSE;
      end if;

   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_TRAN_HEAD_UPDATE;
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_INFO(O_error_message   IN OUT   VARCHAR2,
                           O_error_ind          OUT   BOOLEAN,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_item_type       IN       SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                           I_sub_tran_type   IN       SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                           I_qty             IN       SA_TRAN_ITEM.QTY%TYPE,
                           I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                           I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                           I_store           IN       SA_TRAN_ITEM.STORE%TYPE,
                           I_day             IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.PROCESS_ITEM_INFO';

   L_item                      ITEM_MASTER.ITEM%TYPE   := I_item;
   L_item_desc                 ITEM_MASTER.ITEM_DESC%TYPE;
   L_item_level                ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level                ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_status                    ITEM_MASTER.STATUS%TYPE;
   L_pack_ind                  ITEM_MASTER.PACK_IND%TYPE;
   L_dept                      ITEM_MASTER.DEPT%TYPE;
   L_dept_name                 DEPS.DEPT_NAME%TYPE;
   L_class                     ITEM_MASTER.CLASS%TYPE;
   L_class_name                CLASS.CLASS_NAME%TYPE;
   L_subclass                  ITEM_MASTER.SUBCLASS%TYPE;
   L_subclass_name             SUBCLASS.SUB_NAME%TYPE;
   L_sellable_ind              ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind             ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type                 ITEM_MASTER.PACK_TYPE%TYPE;
   L_simple_pack_ind           ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
   L_waste_type                ITEM_MASTER.WASTE_TYPE%TYPE;
   L_item_parent               ITEM_MASTER.ITEM_PARENT%TYPE;
   L_item_grandparent          ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_short_desc                ITEM_MASTER.SHORT_DESC%TYPE;
   L_waste_pct                 ITEM_MASTER.WASTE_PCT%TYPE;
   L_default_waste_pct         ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;

   L_item_parent_loc           ITEM_LOC.ITEM_PARENT%TYPE;
   L_item_grandparent_loc      ITEM_LOC.ITEM_GRANDPARENT%TYPE;
   L_loc_type                  ITEM_LOC.LOC_TYPE%TYPE;
   L_unit_retail               ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom               ITEM_LOC.SELLING_UOM%TYPE;
   L_clear_ind                 ITEM_LOC.CLEAR_IND%TYPE;
   L_taxable_ind               ITEM_LOC.TAXABLE_IND%TYPE;
   L_local_item_desc           ITEM_LOC.LOCAL_ITEM_DESC%TYPE;
   L_local_short_desc          ITEM_LOC.LOCAL_SHORT_DESC%TYPE;
   L_ti                        ITEM_LOC.TI%TYPE;
   L_hi                        ITEM_LOC.HI%TYPE;
   L_store_ord_mult            ITEM_LOC.STORE_ORD_MULT%TYPE;
   L_itemloc_status            ITEM_LOC.STATUS%TYPE;
   L_status_update_date        ITEM_LOC.STATUS_UPDATE_DATE%TYPE;
   L_daily_waste_pct           ITEM_LOC.DAILY_WASTE_PCT%TYPE;
   L_meas_of_each              ITEM_LOC.MEAS_OF_EACH%TYPE;
   L_meas_of_price             ITEM_LOC.MEAS_OF_PRICE%TYPE;
   L_uom_of_price              ITEM_LOC.UOM_OF_PRICE%TYPE;
   L_primary_variant           ITEM_LOC.PRIMARY_VARIANT%TYPE;
   L_primary_supp              ITEM_LOC.PRIMARY_SUPP%TYPE;
   L_primary_cntry             ITEM_LOC.PRIMARY_CNTRY%TYPE;
   L_primary_cost_pack         ITEM_LOC.PRIMARY_COST_PACK%TYPE;

   L_selling_unit_retail_loc   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_loc           ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_loc           ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_loc     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_loc     ITEM_LOC.MULTI_SELLING_UOM%TYPE;

   L_ref_item                  ITEM_MASTER.ITEM%TYPE;
   L_ref_item_desc             ITEM_MASTER.ITEM_DESC%TYPE;

   L_exists              BOOLEAN;
   L_error_ind           BOOLEAN := FALSE;

BEGIN
   if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                               L_item_desc,
                               L_item_level,
                               L_tran_level,
                               L_status,
                               L_pack_ind,
                               L_dept,
                               L_dept_name,
                               L_class,
                               L_class_name,
                               L_subclass,
                               L_subclass_name,
                               L_sellable_ind,
                               L_orderable_ind,
                               L_pack_type,
                               L_simple_pack_ind,
                               L_waste_type,
                               L_item_parent,
                               L_item_grandparent,
                               L_short_desc,
                               L_waste_pct,
                               L_default_waste_pct,
                               L_item ) = FALSE then
      return FALSE;
   end if;

   if(L_tran_level - L_item_level = 1) then
      if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                           L_item,
                                           I_store,
                                           L_exists) = FALSE then
         return FALSE;
      end if;

      if (not(L_exists)) then
         O_error_ind := TRUE;
         return TRUE;
      end if;

      if L_exists then
          if ITEM_LOC_SQL.GET_ITEM_LOC(O_error_message,
                                       L_item_parent_loc,
                                       L_item_grandparent_loc,
                                       L_loc_type,
                                       L_unit_retail,
                                       L_selling_unit_retail,
                                       L_selling_uom,
                                       L_clear_ind,
                                       L_taxable_ind,
                                       L_local_item_desc,
                                       L_local_short_desc,
                                       L_ti,
                                       L_hi,
                                       L_store_ord_mult,
                                       L_itemloc_status,
                                       L_status_update_date,
                                       L_daily_waste_pct,
                                       L_meas_of_each,
                                       L_meas_of_price,
                                       L_uom_of_price,
                                       L_primary_variant,
                                       L_primary_supp,
                                       L_primary_cntry,
                                       L_primary_cost_pack,
                                       L_item,
                                       I_store) = FALSE then
             return FALSE;
          end if;

          if L_primary_variant is NULL then
             O_error_ind := TRUE;
             return TRUE;
          end if;

          if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                      L_item_desc,
                                      L_item_level,
                                      L_tran_level,
                                      L_status,
                                      L_pack_ind,
                                      L_dept,
                                      L_dept_name,
                                      L_class,
                                      L_class_name,
                                      L_subclass,
                                      L_subclass_name,
                                      L_sellable_ind,
                                      L_orderable_ind,
                                      L_pack_type,
                                      L_simple_pack_ind,
                                      L_waste_type,
                                      L_item_parent,
                                      L_item_grandparent,
                                      L_short_desc,
                                      L_waste_pct,
                                      L_default_waste_pct,
                                      L_primary_variant) = FALSE then
             return FALSE;
          end if;

          L_item := L_primary_variant;
      end if;
   elsif L_item_level > L_tran_level then
      if (L_status != 'A' or L_sellable_ind != 'Y') then
         O_error_ind := TRUE;
         return TRUE;
      end if;
      L_ref_item := L_item;
      L_ref_item_desc := L_item_desc;
      L_item := L_item_parent;

      if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                  L_item_desc,
                                  L_item_level,
                                  L_tran_level,
                                  L_status,
                                  L_pack_ind,
                                  L_dept,
                                  L_dept_name,
                                  L_class,
                                  L_class_name,
                                  L_subclass,
                                  L_subclass_name,
                                  L_sellable_ind,
                                  L_orderable_ind,
                                  L_pack_type,
                                  L_simple_pack_ind,
                                  L_waste_type,
                                  L_item_parent,
                                  L_item_grandparent,
                                  L_short_desc,
                                  L_waste_pct,
                                  L_default_waste_pct,
                                  L_item_parent) = FALSE then
         return FALSE;
      end if;
   end if;

   if(L_item_level != L_tran_level or
      L_status != 'A'              or
      L_sellable_ind != 'Y') then
      O_error_ind := TRUE;
      return TRUE;
   end if;

   if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                        L_item,
                                        I_store,
                                        L_exists) = FALSE then
      return FALSE;
   end if;

   if L_exists then
      if ITEM_LOC_SQL.GET_ITEM_LOC(O_error_message,
                                   L_item_parent_loc,
                                   L_item_grandparent_loc,
                                   L_loc_type,
                                   L_unit_retail,
                                   L_selling_unit_retail,
                                   L_selling_uom,
                                   L_clear_ind,
                                   L_taxable_ind,
                                   L_local_item_desc,
                                   L_local_short_desc,
                                   L_ti,
                                   L_hi,
                                   L_store_ord_mult,
                                   L_itemloc_status,
                                   L_status_update_date,
                                   L_daily_waste_pct,
                                   L_meas_of_each,
                                   L_meas_of_price,
                                   L_uom_of_price,
                                   L_primary_variant,
                                   L_primary_supp,
                                   L_primary_cntry,
                                   L_primary_cost_pack,
                                   L_item,
                                   I_store) = FALSE then
         return FALSE;
      end if;
   else

      if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                    L_unit_retail,
                                    L_selling_unit_retail_loc,
                                    L_selling_uom_loc,
                                    L_multi_units_loc,
                                    L_multi_unit_retail_loc,
                                    L_multi_selling_uom_loc,
                                    L_item,
                                    'S',
                                    I_store) = FALSE then
          return FALSE;
       end if;
   end if;

   if PROCESS_POST_ITEM_INFO(O_error_message,
                             L_ref_item,
                             L_item,
                             L_dept,
                             L_class,
                             L_subclass,
                             L_waste_type,
                             L_waste_pct,
                             L_taxable_ind,
                             L_selling_unit_retail,
                             L_unit_retail,
                             L_selling_uom,
                             I_item,
                             I_item_type,
                             I_sub_tran_type,
                             I_qty,
                             I_tran_seq_no,
                             I_item_seq_no,
                             I_store,
                             I_day) = FALSE then
      return FALSE;
   end if;

   O_error_ind := L_error_ind;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_ITEM_INFO;
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_POST_ITEM_INFO(O_error_message        IN OUT   VARCHAR2,
                                I_ref_item             IN       ITEM_MASTER.ITEM%TYPE,
                                I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                                I_dept                 IN       ITEM_MASTER.DEPT%TYPE,
                                I_class                IN       ITEM_MASTER.CLASS%TYPE,
                                I_subclass             IN       ITEM_MASTER.SUBCLASS%TYPE,
                                I_waste_type           IN       ITEM_MASTER.WASTE_TYPE%TYPE,
                                I_waste_pct            IN       ITEM_MASTER.WASTE_PCT%TYPE,
                                I_taxable_ind          IN       ITEM_LOC.TAXABLE_IND%TYPE,
                                I_selling_unit_retail  IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                I_unit_retail          IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                I_selling_uom          IN       ITEM_LOC.SELLING_UOM%TYPE,
                                I_orig_item            IN       ITEM_MASTER.ITEM%TYPE,
                                I_item_type            IN       SA_TRAN_ITEM.ITEM_TYPE%TYPE,
                                I_sub_tran_type        IN       SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                                I_qty                  IN       SA_TRAN_ITEM.QTY%TYPE,
                                I_tran_seq_no          IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                I_item_seq_no          IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                I_store                IN       SA_TRAN_ITEM.STORE%TYPE,
                                I_day                  IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.PROCESS_POST_ITEM_INFO';
   L_item               ITEM_MASTER.ITEM%TYPE := I_item;
   L_standard_uom       UOM_CLASS.UOM%TYPE;
   L_standard_class     UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor        ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_catch_weight_ind   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE;
   L_unit_retail        ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_uom        ITEM_LOC.SELLING_UOM%TYPE := I_selling_uom;
   L_item_type          SA_TRAN_ITEM.ITEM_TYPE%TYPE := I_item_type;
   L_qty                SA_TRAN_ITEM.QTY%TYPE := I_qty;
BEGIN

   if (I_item_type != 'REF') then
      L_item := I_orig_item;
   end if;

   if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                       L_standard_uom,
                                       L_standard_class,
                                       L_conv_factor,
                                       L_item,
                                       'N') = FALSE then
      return FALSE;
   end if;

   if(I_item_type = 'ITEM' and I_ref_item is not null) then
      L_item_type := 'REF';
   elsif(I_item_type = 'REF' and I_ref_item is null) then
      L_item_type := 'ITEM';
   end if;

   if ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_ATTRIB(O_error_message,
                                              L_catch_weight_ind,
                                              L_item) = FALSE then
      return FALSE;
   end if;

   if(L_catch_weight_ind = 'Y' and L_standard_uom = 'EA') then
      if(not(I_sub_tran_type in ('ORDINT','ORDCMP'))) then
         L_unit_retail := I_selling_unit_retail;
      end if;
   else
     if (not(I_sub_tran_type in ('ORDINT','ORDCMP'))) then
         L_unit_retail := I_unit_retail;
     end if;
     L_qty := I_qty;
   end if;

   if(I_selling_uom is null) then
      L_selling_uom := L_standard_uom;
   end if;

    update sa_tran_item sti
       set item = L_item,
           ref_item = I_ref_item,
           voucher_no = null,
           dept = I_dept,
           class = I_class,
           subclass = I_subclass,
           waste_type = I_waste_type,
           waste_pct = I_waste_pct,
           tax_ind = I_taxable_ind,
           standard_uom = L_standard_uom,
           selling_uom = nvl(selling_uom, L_selling_uom),
           unit_retail = L_unit_retail,
           uom_quantity = nvl(L_qty, uom_quantity),
           catchweight_ind = L_catch_weight_ind
     where sti.store = I_store
       and sti.day = I_day
       and sti.tran_seq_no = I_tran_seq_no
       and sti.item_seq_no = I_item_seq_no;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_POST_ITEM_INFO;
----------------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_ERRORS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store           IN       SA_TRAN_HEAD.STORE%TYPE,
                       I_day             IN       SA_TRAN_HEAD.DAY%TYPE,
                       I_tran_type       IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.REMOVE_ERRORS';
   L_total_errors   NUMBER(10)     := 0;

BEGIN

   if SA_ERROR_SQL.COUNT_ERRORS(O_error_message,
                                L_total_errors,
                                I_tran_seq_no,
                                I_store,
                                I_day) = FALSE then
      return FALSE;
   end if;

   if L_total_errors > 0 then
      if I_tran_type = 'CLOSE' then
         if TRAN_ERROR_SQL.PROCESS_CLOSE(O_error_message,
                                         I_tran_seq_no,
                                         I_store,
                                         I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'COND' then
         if TRAN_ERROR_SQL.PROCESS_COND(O_error_message,
                                        I_tran_seq_no,
                                        I_store,
                                        I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'DCLOSE' then
         if TRAN_ERROR_SQL.PROCESS_DCLOSE(O_error_message,
                                          I_tran_seq_no,
                                          I_store,
                                          I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'LOAN' then
         if TRAN_ERROR_SQL.PROCESS_LOAN(O_error_message,
                                        I_tran_seq_no,
                                        I_store,
                                        I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'METER' then
         if TRAN_ERROR_SQL.PROCESS_METER(O_error_message,
                                         I_tran_seq_no,
                                         I_store,
                                         I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'NOSALE' then
         if TRAN_ERROR_SQL.PROCESS_NOSALE(O_error_message,
                                          I_tran_seq_no,
                                          I_store,
                                          I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'OPEN' then
         if TRAN_ERROR_SQL.PROCESS_OPEN(O_error_message,
                                        I_tran_seq_no,
                                        I_store,
                                        I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'PAIDIN' then
         if TRAN_ERROR_SQL.PROCESS_PAIDIN(O_error_message,
                                          I_tran_seq_no,
                                          I_store,
                                          I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'PAIDOU' then
         if TRAN_ERROR_SQL.PROCESS_PAIDOU(O_error_message,
                                          I_tran_seq_no,
                                          I_store,
                                          I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'PULL' then
         if TRAN_ERROR_SQL.PROCESS_PULL(O_error_message,
                                        I_tran_seq_no,
                                        I_store,
                                        I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'PUMPT' then
         if TRAN_ERROR_SQL.PROCESS_PUMPT(O_error_message,
                                         I_tran_seq_no,
                                         I_store,
                                         I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'PVOID' then
         if TRAN_ERROR_SQL.PROCESS_PVOID(O_error_message,
                                         I_tran_seq_no,
                                         I_store,
                                         I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'REFUND' then
         if TRAN_ERROR_SQL.PROCESS_REFUND(O_error_message,
                                          I_tran_seq_no,
                                          I_store,
                                          I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'TANKDP' then
         if TRAN_ERROR_SQL.PROCESS_TANKDP(O_error_message,
                                          I_tran_seq_no,
                                          I_store,
                                          I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'TERM' then
         if TRAN_ERROR_SQL.PROCESS_TERM(O_error_message,
                                        I_tran_seq_no,
                                        I_store,
                                        I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'TOTAL' then
         if TRAN_ERROR_SQL.PROCESS_TOTAL(O_error_message,
                                         I_tran_seq_no,
                                         I_store,
                                         I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type = 'EEXCH' then
         if TRAN_ERROR_SQL.PROCESS_EEXCH(O_error_message,
                                         I_tran_seq_no,
                                         I_store,
                                         I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_tran_type in ('SALE','RETURN','SPLORD') then
         if TRAN_ERROR_SQL.PROCESS_SALE_RETURN(O_error_message,
                                               I_tran_seq_no,
                                               I_store,
                                               I_day) = FALSE then
            return FALSE;
         end if;
      end if;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REMOVE_ERRORS;
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                        I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                        I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                        I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                        I_tran_type          IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                        I_rtlog_orig_sys     IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                        I_tran_process_sys   IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                        I_record_name        IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'SA_MASS_ERROR_SQL.REFRESH_ERRORS';
   L_error_ind        SA_TRAN_PAYMENT.ERROR_IND%TYPE;
   L_payment_seq_no   SA_TRAN_PAYMENT.PAYMENT_SEQ_NO%TYPE;

   cursor C_SA_PAYMENT_REC is
    select *
      from sa_tran_payment stp
       where stp.tran_seq_no = I_tran_seq_no
         and stp.store = I_store
         and stp.day = I_day;
BEGIN
   if REFRESH_ITEM_ERRORS(O_error_message,
                          I_fix_column,
                          I_store_day_seq_no,
                          I_tran_seq_no,
                          I_store,
                          I_day,
                          I_tran_type,
                          I_rtlog_orig_sys,
                          I_tran_process_sys,
                          I_record_name) = FALSE then
      return FALSE;
   end if;

   if REFRESH_TENDER_ERRORS(O_error_message,
                            I_fix_column,
                            I_store_day_seq_no,
                            I_tran_seq_no,
                            I_store,
                            I_day,
                            I_tran_type,
                            I_record_name) = FALSE then
      return FALSE;
   end if;

   if REFRESH_TAX_ERRORS(O_error_message,
                         I_fix_column,
                         I_store_day_seq_no,
                         I_tran_seq_no,
                         I_store,
                         I_day,
                         I_record_name) = FALSE then
      return FALSE;
   end if;

   if REFRESH_IGTAX_ERRORS(O_error_message,
                           I_fix_column,
                           I_store_day_seq_no,
                           I_tran_seq_no,
                           I_store,
                           I_day,
                           I_record_name) = FALSE then
      return FALSE;
   end if;

   for rec in C_SA_PAYMENT_REC loop
      L_error_ind      := rec.error_ind;
      L_payment_seq_no := rec.payment_seq_no;
      if L_error_ind = 'N' and I_tran_type in ('SALE', 'SPLORD', 'ERR') then
         if TRANSACTION_SQL.DELETE_TRAN_PAYMENT(O_error_message,
                                                L_payment_seq_no,
                                                I_tran_seq_no,
                                                I_store,
                                                I_day) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_ERRORS;
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_ITEM_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                             I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                             I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                             I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                             I_tran_type          IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                             I_rtlog_orig_sys     IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                             I_tran_process_sys   IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                             I_record_name        IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.REFRESH_ITEM_ERRORS';
   L_error_ind            SA_TRAN_ITEM.ERROR_IND%TYPE;
   L_item_seq_no          SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE;
   L_return_wh            SA_TRAN_ITEM.RETURN_WH%TYPE;
   L_sales_type           SA_TRAN_ITEM.SALES_TYPE%TYPE;
   L_item_status          SA_TRAN_ITEM.ITEM_STATUS%TYPE;
   L_fulfill_order_no     SA_TRAN_ITEM.FULFILL_ORDER_NO%TYPE;
   L_return_disposition   SA_TRAN_ITEM.RETURN_DISPOSITION%TYPE;
   L_no_inv_ret_ind       SA_TRAN_ITEM.NO_INV_RET_IND%TYPE;
   L_qty                  SA_TRAN_ITEM.QTY%TYPE;
   L_item                 SA_TRAN_ITEM.ITEM%TYPE;
   L_selling_uom          SA_TRAN_ITEM.SELLING_UOM%TYPE;
   L_standard_uom         SA_TRAN_ITEM.STANDARD_UOM%TYPE;

   L_item_type_changed_ind         VARCHAR2(1) := 'N';
   L_item_changed_ind              VARCHAR2(1) := 'N';
   L_unit_retail_changed_ind       VARCHAR2(1) := 'N';
   L_orig_ur_changed_ind           VARCHAR2(1) := 'N';
   L_override_reason_changed_ind   VARCHAR2(1) := 'N';
   L_qty_changed_ind               VARCHAR2(1) := 'N';

   cursor C_SA_ITEM_REC is
      select *
        from sa_tran_item sti
       where sti.tran_seq_no = I_tran_seq_no
         and sti.store = I_store
         and sti.day = day;
BEGIN
   if I_fix_column in ('ITEM','REF_ITEM','NON_MERCH_ITEM') then
      L_item_changed_ind := 'Y';
   elsif I_fix_column = 'ITEM_TYPE' then
      L_item_type_changed_ind := 'Y';
   elsif I_fix_column = 'UNIT_RETAIL' then
      L_unit_retail_changed_ind := 'Y';
   elsif I_fix_column = 'OVERRIDE_REASON' then
      L_override_reason_changed_ind := 'Y';
   elsif I_fix_column = 'QTY' then
      L_qty_changed_ind := 'Y';
   end if;
   for rec in C_SA_ITEM_REC loop
      L_error_ind          := rec.error_ind;
      L_item_seq_no        := rec.item_seq_no;
      L_return_wh          := rec.return_wh;
      L_sales_type         := rec.sales_type;
      L_item_status        := rec.item_status;
      L_fulfill_order_no   := rec.fulfill_order_no;
      L_return_disposition := rec.return_disposition;
      L_no_inv_ret_ind     := rec.no_inv_ret_ind;
      L_qty                := rec.qty;


      if L_error_ind = 'Y' then
         if I_tran_type in ('SPLORD','SALE', 'RETURN') then
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'TOTAL_IGTAX_AMT_GT_TOTRET',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'INVLD_ITEM_SWIPED_IND',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'INVLD_SYSTEM_IND',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'INVLD_TAX_IND',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'TITEM_CLS_STIN',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'TITEM_DEP_STIN',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'TITEM_SBC_STIN',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'INVLD_SELLING_UOM',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
         end if;
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'SET_DROP_SHIP',
                                       L_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       I_store,
                                       I_day) = FALSE then
            return FALSE;
         end if;
      end if; -- end of ERROR_IND = 'Y'

      if REFRESH_CUSTORD_ERRORS(O_error_message,
                                I_store_day_seq_no,
                                I_tran_seq_no,
                                I_tran_type,
                                I_rtlog_orig_sys,
                                I_tran_process_sys,
                                L_item_seq_no,
                                L_return_wh,
                                L_sales_type,
                                L_item_status,
                                L_fulfill_order_no,
                                L_return_disposition,
                                L_no_inv_ret_ind) = FALSE then
         return FALSE;
      end if;

      if L_error_ind = 'Y' and I_record_name = 'SA_TRAN_ITEM' then
         if I_tran_type in ('SPLORD','SALE', 'RETURN') then
            if I_fix_column = 'RETURN_REASON_CODE' then
               if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                             I_store_day_seq_no,
                                             I_tran_seq_no,
                                             'INVLD_SARR',
                                             L_item_seq_no,
                                             NULL,
                                             'TITEM',
                                             I_store,
                                             I_day) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_SASI',
                                       L_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       I_store,
                                       I_day) = FALSE then
            return FALSE;
         end if;
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'ITEM_STATUS_REQ',
                                       L_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       I_store,
                                       I_day) = FALSE then
            return FALSE;
         end if;
         if ((L_item_status in ('S', 'ORI', 'ORD', 'LIN', 'LCO') and L_qty > 0) or
             (L_item_status in ('R', 'ORC', 'LCA') and L_qty < 0)) then
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'TITEM_QTY_SIGN',
                                          L_item_seq_no,
                                          NULL,
                                          'TITEM',
                                          I_store,
                                          I_day) = FALSE then
               return FALSE;
            end if;
         end if;
         if TRAN_ERROR_SQL.REFRESH_ITEM_ERRORS(O_error_message,
                                               L_error_ind,
                                               I_store_day_seq_no,
                                               I_tran_seq_no,
                                               L_item_seq_no,
                                               L_item,
                                               L_selling_uom,
                                               L_standard_uom,
                                               L_item_type_changed_ind,
                                               L_item_changed_ind,
                                               L_unit_retail_changed_ind,
                                               L_orig_ur_changed_ind,
                                               L_override_reason_changed_ind,
                                               L_qty_changed_ind,
                                               I_store,
                                               I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      update sa_tran_item sti
         set error_ind = L_error_ind
       where sti.tran_seq_no = I_tran_seq_no
         and sti.store = I_store
         and sti.day = I_day
         and sti.item_seq_no = L_item_seq_no;

   end loop;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_ITEM_ERRORS;
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_CUSTORD_ERRORS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_store_day_seq_no     IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                I_tran_seq_no          IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                I_tran_type            IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                                I_rtlog_orig_sys       IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                                I_tran_process_sys     IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                                I_item_seq_no          IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                I_return_wh            IN       SA_TRAN_ITEM.RETURN_WH%TYPE,
                                I_sales_type           IN       SA_TRAN_ITEM.SALES_TYPE%TYPE,
                                I_item_status          IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                I_fulfill_order_no     IN       SA_TRAN_ITEM.FULFILL_ORDER_NO%TYPE,
                                I_return_disposition   IN       SA_TRAN_ITEM.RETURN_DISPOSITION%TYPE,
                                I_no_inv_ret_ind       IN       SA_TRAN_ITEM.NO_INV_RET_IND%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.REFRESH_CUSTORD_ERRORS';

BEGIN
   if I_return_wh is not NULL and I_sales_type = 'E' then
      if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                    I_store_day_seq_no,
                                    I_tran_seq_no,
                                    'RETWH_EXT_ORD',
                                    I_item_seq_no,
                                    NULL,
                                    'TITEM') = FALSE then
         return FALSE;
      end if;
   end if;

   if I_item_status = 'ORD' then
      if I_sales_type != 'E' and I_fulfill_order_no is NULL then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'FULFILL_ORDER_NO_REQ',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM') = FALSE then
            return FALSE;
         end if;
      end if;
      if I_fulfill_order_no is NULL and I_sales_type = 'E' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'FULFILL_ORDER_NO_ERR',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM') = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if I_return_disposition is not NULL then
      if I_no_inv_ret_ind = 'N' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'RETDISP_INV_RET',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM') = FALSE then
            return FALSE;
         end if;
      end if;

      if I_sales_type = 'E' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'RETDISP_EXT_ORD',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM') = FALSE then
            return FALSE;
         end if;
      end if;
   else --I_return_disposition is null
      if ((I_sales_type = 'E' and I_no_inv_ret_ind = 'Y') or
           I_sales_type != 'E' and I_no_inv_ret_ind = 'N') then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'RETURN_DISP_REQ',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM') = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if I_rtlog_orig_sys = 'OMS' and I_tran_process_sys = 'OMS' and I_item_status = 'R' then
      if ((I_sales_type = 'E' and I_return_wh is not NULL) or
           I_sales_type != 'E' and I_return_wh is NULL) then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'RETWH_REQ',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM') = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_CUSTORD_ERRORS;
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_TENDER_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                               I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                               I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                               I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                               I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                               I_tran_type          IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                               I_record_name        IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.REFRESH_TENDER_ERRORS';

   L_tend_type_group_changed_ind   VARCHAR2(1) := 'N';
   L_tender_type_id_changed_ind    VARCHAR2(1) := 'N';
   L_tender_no_changed_ind         VARCHAR2(1) := 'N';
   L_tender_amt_changed_ind        VARCHAR2(1) := 'N';
   L_cc_no_changed_ind             VARCHAR2(1) := 'N';
   L_cc_exp_date_changed_ind       VARCHAR2(1) := 'N';
   L_cc_auth_src_changed_ind       VARCHAR2(1) := 'N';
   L_cc_card_verf_changed_ind      VARCHAR2(1) := 'N';
   L_cc_spec_cond_changed_ind      VARCHAR2(1) := 'N';
   L_cc_entry_mode_changed_ind     VARCHAR2(1) := 'N';
   L_add_details_changed_ind       VARCHAR2(1) := 'N';
   L_error_ind                     SA_TRAN_TENDER.ERROR_IND%TYPE;
   L_tender_seq_no                 SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE;
   L_tender_type_group             SA_TRAN_TENDER.TENDER_TYPE_GROUP%TYPE;
   L_coupon_no                     SA_TRAN_TENDER.COUPON_NO%TYPE;

   cursor C_SA_TENDER_REC is
      select *
        from sa_tran_tender stt
       where stt.tran_seq_no = I_tran_seq_no
         and stt.store = I_store
         and stt.day = I_day;
BEGIN

   if I_fix_column = 'TENDER_TYPE_GROUP' then
      L_tend_type_group_changed_ind := 'Y';
   elsif I_fix_column = 'TENDER_TYPE_ID' then
      L_tender_type_id_changed_ind := 'Y';
   elsif I_fix_column in ('IDENTI_METHOD') then
      L_add_details_changed_ind := 'Y';
   end if;

   for rec in C_SA_TENDER_REC loop
      L_error_ind          := rec.error_ind;
      L_tender_seq_no      := rec.tender_seq_no;
      L_tender_type_group  := rec.tender_type_group;
      L_coupon_no          := rec.coupon_no;

      if L_error_ind = 'Y' and I_record_name = 'SA_TRAN_TENDER' then
         if I_tran_type in ('SPLORD','SALE', 'RETURN') then
            if L_tender_type_group = 'COUPON' and L_coupon_no is not NULL then
               if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                             I_store_day_seq_no,
                                             I_tran_seq_no,
                                             'TTEND_COUPON_NO_REQ',
                                             L_tender_seq_no,
                                             NULL,
                                             'TTEND',
                                             I_store,
                                             I_day) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;

         if TRAN_ERROR_SQL.REFRESH_TENDER_ERRORS(O_error_message,
                                                 L_error_ind,
                                                 I_store_day_seq_no,
                                                 I_tran_seq_no,
                                                 L_tender_seq_no,
                                                 L_tend_type_group_changed_ind,
                                                 L_tender_type_id_changed_ind,
                                                 L_tender_no_changed_ind,
                                                 L_tender_amt_changed_ind,
                                                 L_cc_no_changed_ind,
                                                 L_cc_exp_date_changed_ind,
                                                 L_cc_auth_src_changed_ind,
                                                 L_cc_card_verf_changed_ind,
                                                 L_cc_spec_cond_changed_ind,
                                                 L_cc_entry_mode_changed_ind,
                                                 L_add_details_changed_ind,
                                                 I_store,
                                                 I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      update sa_tran_tender stt
         set error_ind = L_error_ind
       where stt.tran_seq_no = I_tran_seq_no
         and stt.store = I_store
         and stt.day = I_day
         and stt.tender_seq_no = L_tender_seq_no;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_TENDER_ERRORS;
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_TAX_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                            I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                            I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                            I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                            I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                            I_record_name        IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.REFRESH_TAX_ERRORS';

   L_tax_code_changed_ind   VARCHAR2(1) := 'N';
   L_tax_amt_changed_ind    VARCHAR2(1) := 'N';
   L_error_ind              SA_TRAN_TAX.ERROR_IND%TYPE;
   L_tax_seq_no             SA_TRAN_TAX.TAX_SEQ_NO%TYPE;

   cursor C_SA_TAX_REC is
      select *
        from sa_tran_tax stx
       where stx.tran_seq_no = I_tran_seq_no
         and stx.store = I_store
         and stx.day = I_day;
BEGIN

   if I_fix_column = 'TAX_CODE' then
      L_tax_code_changed_ind := 'Y';
   end if;

   for rec in C_SA_TAX_REC loop
      L_error_ind    := rec.error_ind;
      L_tax_seq_no   := rec.tax_seq_no;

      if L_error_ind = 'Y' and I_record_name = 'SA_TRAN_TAX' then
         if TRAN_ERROR_SQL.REFRESH_TAX_ERRORS(O_error_message,
                                              L_error_ind,
                                              I_store_day_seq_no,
                                              I_tran_seq_no,
                                              L_tax_seq_no,
                                              L_tax_code_changed_ind,
                                              L_tax_amt_changed_ind,
                                              I_store,
                                              I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      update sa_tran_tax stx
         set error_ind = L_error_ind
       where stx.tran_seq_no = I_tran_seq_no
         and stx.store = I_store
         and stx.day = I_day
         and stx.tax_seq_no = L_tax_seq_no;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_TAX_ERRORS;
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_IGTAX_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                              I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                              I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store              IN       SA_TRAN_HEAD.STORE%TYPE,
                              I_day                IN       SA_TRAN_HEAD.DAY%TYPE,
                              I_record_name        IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.REFRESH_IGTAX_ERRORS';

   L_error_ind                   SA_TRAN_IGTAX.ERROR_IND%TYPE;
   L_item_seq_no                 SA_TRAN_IGTAX.ITEM_SEQ_NO%TYPE;
   L_igtax_seq_no                SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE;
   L_tax_authority_changed_ind   VARCHAR2(1) := 'N';
   L_tax_code_changed_ind        VARCHAR2(1) := 'N';
   L_tax_amt_changed_ind         VARCHAR2(1) := 'N';
   L_tax_rate_changed_ind        VARCHAR2(1) := 'N';

   cursor C_SA_IGTAX_REC is
      select *
        from sa_tran_igtax stig
       where stig.tran_seq_no = I_tran_seq_no
         and stig.store = I_store
         and stig.day = I_day;
BEGIN

   if I_fix_column = 'TAX_AUTHORITY' then
      L_tax_authority_changed_ind := 'Y';
   elsif I_fix_column in ('TAX_CODE','IGTAX_CODE') then
      L_tax_code_changed_ind := 'Y';
   end if;


   for rec in C_SA_IGTAX_REC loop
      L_error_ind    := rec.error_ind;
      L_item_seq_no   := rec.item_seq_no;
      L_igtax_seq_no   := rec.igtax_seq_no;

      if L_error_ind = 'Y' and I_record_name = 'SA_TRAN_IGTAX' then
         if TRAN_ERROR_SQL.REFRESH_IGTAX_ERRORS(O_error_message,
                                                L_error_ind,
                                                I_store_day_seq_no,
                                                I_tran_seq_no,
                                                L_item_seq_no,
                                                L_igtax_seq_no,
                                                L_tax_authority_changed_ind,
                                                L_tax_code_changed_ind,
                                                L_tax_amt_changed_ind,
                                                L_tax_rate_changed_ind,
                                                I_store,
                                                I_day) = FALSE then
            return FALSE;
         end if;
      end if;

      update sa_tran_igtax stig
         set error_ind = L_error_ind
       where stig.tran_seq_no = I_tran_seq_no
         and stig.store = I_store
         and stig.day = I_day
         and stig.item_seq_no = L_item_seq_no
         and stig.igtax_seq_no = L_igtax_seq_no;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_IGTAX_ERRORS;
----------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_HEADER_ERROR(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error_ind          IN OUT   SA_TRAN_HEAD.ERROR_IND%TYPE,
                              I_tran_seq_no        IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                              I_tran_type          IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                              I_sub_tran_type      IN       SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE,
                              I_orig_tran_type     IN       SA_TRAN_HEAD.ORIG_TRAN_TYPE%TYPE,
                              I_reason_code        IN       SA_TRAN_HEAD.REASON_CODE%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.REFRESH_HEADER_ERROR';

   L_error                      BOOLEAN   := FALSE;
   L_balance_error_exists       BOOLEAN  := FALSE;
   L_tender_ending_error_exists BOOLEAN  := FALSE;
   L_ti_value_error_exists      BOOLEAN  := FALSE;
   L_date_error_exists          BOOLEAN  := FALSE;
   L_tran_no_error_exists       BOOLEAN  := FALSE;
   L_orig_tran_no_exists        BOOLEAN  := FALSE;
   L_value_error_exists         BOOLEAN  := FALSE;
   L_banner_no_error_exists     BOOLEAN  := FALSE;

BEGIN

   if O_error_ind = 'Y' then
      --- if transaction type is in error, populate flag
      if I_tran_type = 'ERR' then  --- invalid transaction type
         L_error := TRUE;
      end if;
      --- if reason code is in error, populate the flag
      if I_tran_type != 'PVOID' then
         if I_reason_code = 'ERR' then  --- invalid reason code
            L_error := TRUE;
         end if;
         --- if sub_tran_type is in error, populate the flag
         if I_sub_tran_type = 'ERR' then    --- invalid sub_tran_type
            L_error := TRUE;
         end if;
      end if;

      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_balance_error_exists,
                                   'TRAN_OUT_BAL',
                                   NULL,
                                   I_tran_seq_no,
                                   NULL,
                                   NULL,
                                   'THBALC',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_balance_error_exists = TRUE then
         L_error := TRUE;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_ti_value_error_exists,
                                   'NO_RULE_FOR_SALETOTAL',
                                   NULL,
                                   I_tran_seq_no,
                                   NULL,
                                   NULL,
                                   'THTTSA',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_ti_value_error_exists = TRUE then
         L_error := TRUE;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_tender_ending_error_exists,
                                   'INVLD_TENDER_ENDING',
                                   NULL,
                                   I_tran_seq_no,
                                   NULL,
                                   NULL,
                                   'THTEND',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;

      end if;
      ---
      if L_tender_ending_error_exists = TRUE then
         L_error := TRUE;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_date_error_exists,
                                   'INVLD_TRAN_DATETIME',
                                   NULL,
                                   I_tran_seq_no,
                                   NULL,
                                   NULL,
                                   'THDATE',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_date_error_exists = TRUE then
         L_error := TRUE;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_tran_no_error_exists,
                                   'THEAD_TRN_STIN',
                                   NULL,
                                   I_tran_seq_no,
                                   NULL,
                                   NULL,
                                   'THTRAN',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_tran_no_error_exists = TRUE then
         L_error := TRUE;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_tran_no_error_exists,
                                   'TRAN_NO_REQ',
                                   NULL,
                                   I_tran_seq_no,
                                   NULL,
                                   NULL,
                                   'THTRAN',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_tran_no_error_exists = TRUE then
         L_error := TRUE;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_banner_no_error_exists,
                                   'INVLD_BANNER_ID',
                                    NULL,
                                   I_tran_seq_no,
                                    NULL,
                                    NULL,
                                   'THEAD',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_banner_no_error_exists = FALSE then
         if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                      L_banner_no_error_exists,
                                      'THEAD_LOC_NO_BANN',
                                       NULL,
                                      I_tran_seq_no,
                                       NULL,
                                       NULL,
                                      'THEAD',
                                      I_store_day_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_banner_no_error_exists = TRUE then
         L_error := TRUE;
      end if;
      ---


      if I_tran_type = 'PVOID' then
         if I_orig_tran_type = 'ERR' then
            L_error := TRUE;
         end if;
         ---
         if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                      L_orig_tran_no_exists,
                                      'THEAD_OTN_STIN',
                                      NULL,
                                      I_tran_seq_no,
                                      NULL,
                                      NULL,
                                      'THOTRN',
                                      I_store_day_seq_no) = FALSE then
            return FALSE;
         end if;
         ---
         if L_orig_tran_no_exists = TRUE then
            L_error := TRUE;
         end if;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_value_error_exists,
                                   'THEAD_VAL_STIN',
                                   NULL,
                                   I_tran_seq_no,
                                   NULL,
                                   NULL,
                                   'THVALU',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_value_error_exists,
                                   'TOTAL_VALUE_REQ',
                                   NULL,
                                   I_tran_seq_no,
                                   NULL,
                                   NULL,
                                   'THVALU',
                                   I_store_day_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_value_error_exists = TRUE then
         L_error := TRUE;
      end if;
   end if;

   if L_error then
      O_error_ind := 'Y';
   else
      O_error_ind := 'N';
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_HEADER_ERROR;
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_SALES_TYPE(O_error_message      IN OUT   VARCHAR2,
                                 O_error_ind             OUT   BOOLEAN,
                                 I_sales_type         IN       SA_TRAN_ITEM.SALES_TYPE%TYPE,
                                 I_item_status        IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                 I_pos_tran_ind       IN       SA_TRAN_HEAD.POS_TRAN_IND%TYPE,
                                 I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                 I_tran_seq_no        IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                 I_item_seq_no        IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                 I_store              IN       SA_TRAN_ITEM.STORE%TYPE,
                                 I_day                IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN IS
   L_error_ind   BOOLEAN := FALSE;
   L_program     VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.PROCESS_ITEM_SALES_TYPE';
BEGIN
   if (I_pos_tran_ind = 'N' and I_sales_type = 'E') then
      O_error_ind := TRUE;
      return TRUE;
   end if;

   if (I_sales_type = 'E' and I_item_status not in ('ORI', 'ORD', 'ORC', 'R')) then
      O_error_ind := TRUE;
      return TRUE;
   end if;

   if (I_sales_type != 'I' and I_item_status in ('LIN', 'LCA', 'LCO')) then
      O_error_ind := TRUE;
      return TRUE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'INVLD_SASY',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;
   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'INVLD_SASY_E',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;
   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'CUST_ORD_ATTR_REQ',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   O_error_ind := L_error_ind;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_ITEM_SALES_TYPE;
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_RETURN_WH(O_error_message      IN OUT   VARCHAR2,
                                O_error_ind             OUT   BOOLEAN,
                                I_rtlog_orig_sys     IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                                I_tran_process_sys   IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                                I_return_wh          IN       SA_TRAN_ITEM.RETURN_WH%TYPE,
                                I_sales_type         IN       SA_TRAN_ITEM.SALES_TYPE%TYPE,
                                I_item_status        IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                I_tran_seq_no        IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                I_item_seq_no        IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                I_store              IN       SA_TRAN_ITEM.STORE%TYPE,
                                I_day                IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.PROCESS_ITEM_RETURN_WH';

BEGIN

   if I_return_wh is not null then
      if  I_sales_type  = 'E' then
         if((I_rtlog_orig_sys = 'OMS' and I_item_status != 'R') or
            (I_rtlog_orig_sys != 'OMS' or I_tran_process_sys != 'OMS')) then
            O_error_ind := TRUE;
            return TRUE;
         end if;
      else
         O_error_ind := TRUE;
         return TRUE;
      end if;

      if I_item_status != 'R' then
         O_error_ind := TRUE;
         return TRUE;
      end if;

   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'RETWH_OMS',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'RETWH_EXT_ORD',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'RETWH_NOT_FOUND',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'TITEM_WH_STIN',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'RETWH_REQ',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   O_error_ind := FALSE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_ITEM_RETURN_WH;
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_RETURN_DISP(O_error_message        IN OUT   VARCHAR2,
                                  O_error_ind               OUT   BOOLEAN,
                                  I_rtlog_orig_sys       IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE,
                                  I_tran_process_sys     IN       SA_TRAN_HEAD.TRAN_PROCESS_SYS%TYPE,
                                  I_return_disposition   IN       SA_TRAN_ITEM.RETURN_DISPOSITION%TYPE,
                                  I_sales_type           IN       SA_TRAN_ITEM.SALES_TYPE%TYPE,
                                  I_item_status          IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                  I_no_inv_ret_ind       IN       SA_TRAN_ITEM.NO_INV_RET_IND%TYPE,
                                  I_store_day_seq_no     IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                  I_tran_seq_no          IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                  I_item_seq_no          IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                  I_store                IN       SA_TRAN_ITEM.STORE%TYPE,
                                  I_day                  IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.PROCESS_ITEM_RETURN_DISP';

BEGIN

   if I_return_disposition is not null then
      if I_no_inv_ret_ind = 'Y' then
         O_error_ind := TRUE;
         return TRUE;
      end if;

      if I_sales_type != 'E' then
         O_error_ind := TRUE;
         return TRUE;
      end if;

      if ((I_rtlog_orig_sys != 'OMS' and I_tran_process_sys != 'OMS') or
          (I_rtlog_orig_sys = 'OMS' and I_tran_process_sys != 'OMS') or
          (I_rtlog_orig_sys != 'OMS' and I_tran_process_sys = 'OMS') or
          (I_rtlog_orig_sys = 'OMS' and I_item_status != 'R')) then
         O_error_ind := TRUE;
         return TRUE;
      end if;

   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'INVLD_RETURN_DISP',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'RETDISP_EXT_ORD',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'RETDISP_INV_RET',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'RETDISP_OMS',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                 I_store_day_seq_no,
                                 I_tran_seq_no,
                                 'RETURN_DISP_REQ',
                                 I_item_seq_no,
                                 NULL,
                                 'TITEM',
                                 I_store,
                                 I_day) = FALSE then
      return FALSE;
   end if;

   O_error_ind := FALSE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_ITEM_RETURN_DISP;
----------------------------------------------------------------------------------------------------------------
FUNCTION POST_ITEM_TYPE_CHANGED(O_error_message        IN OUT   VARCHAR2,
                                I_tran_seq_no          IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE,
                                I_item_seq_no          IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                I_store                IN       SA_TRAN_ITEM.STORE%TYPE,
                                I_day                  IN       SA_TRAN_ITEM.DAY%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.POST_ITEM_TYPE_CHANGED';

   cursor C_LOCK_TRAN_ITEM is
      select 'x'
        from sa_tran_item sti
       where sti.tran_seq_no = I_tran_seq_no
         and sti.item_seq_no = I_item_seq_no
         and sti.store = I_store
         for update nowait;
BEGIN

   open C_LOCK_TRAN_ITEM;
   close C_LOCK_TRAN_ITEM;

   update sa_tran_item sti
      set item_swiped_ind = 'N',
          item = null,
          ref_item = null,
          non_merch_item = null,
          voucher_no = null,
          qty = null,
          unit_retail = null,
          orig_unit_retail = null,
          dept = null,
          class = null,
          subclass = null,
          waste_type = null,
          waste_pct = null
    where sti.tran_seq_no = I_tran_seq_no
         and sti.item_seq_no = I_item_seq_no
         and sti.store = I_store
         and sti.day = I_day;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POST_ITEM_TYPE_CHANGED;
------------------------------------------------------------------------------------------------
FUNCTION TOTAL_AUDIT_CHECK(O_error_message         IN OUT   VARCHAR2,
                           I_audit_status          IN       SA_STORE_DAY.AUDIT_STATUS%TYPE,
                           I_data_status           IN       SA_STORE_DAY.DATA_STATUS%TYPE,
                           I_store_day_seq_no      IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_store                 IN       SA_STORE_DAY.STORE%TYPE,
                           I_day                   IN       SA_STORE_DAY.DAY%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.TOTAL_AUDIT_CHECK';

   L_process_pos_totals_ind   VARCHAR2(1);
   L_audit_after_imp_ind      SA_SYSTEM_OPTIONS.AUDIT_AFTER_IMP_IND%TYPE;
   L_user                     VARCHAR2(50) := NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER);

BEGIN
   if I_audit_status NOT in ('R', 'U') then
      -- SKIP AUDIT CHECK SINCE IT IS NOT REQUIRED
      return TRUE;
   end if;
   ---
   if SA_SYSTEM_OPTIONS_SQL.GET_AUDIT_AFTER_IMP_IND(O_error_message,
                                                    L_audit_after_imp_ind) = FALSE then
       return FALSE;
    end if;

   if L_audit_after_imp_ind = 'N' and I_data_status = 'P' then
   -- Execute the POS totaling process since the data status is not fully loaded and the import
   -- processes to not total/audit unless the data status is fully loaded with the given system
   -- option.
   ---
      L_process_pos_totals_ind := 'Y';
      ---
      if STORE_DAY_SQL.SETUP_FOR_AUDIT(O_error_message,
                                       I_store_day_seq_no,
                                       I_store,
                                       I_day) = FALSE then
       return FALSE;
      end if;
   else
      L_process_pos_totals_ind := 'N';
   end if;
   ---
   if STORE_DAY_SQL.TOTAL_AUDIT(O_error_message,
                                I_store_day_seq_no,
                                L_user,
                                L_process_pos_totals_ind,
                                I_store,
                                I_day) = FALSE then
      return FALSE;
   end if;
   ---
   if SA_LOCKING_SQL.GET_WRITE_LOCK(O_error_message,
                                    I_store_day_seq_no) = FALSE then
      return FALSE;
   end if;
 ---

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TOTAL_AUDIT_CHECK;
------------------------------------------------------------------------------------------------
FUNCTION REFRESH_DISCOUNT_ERRORS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_ind          IN OUT   SA_TRAN_DISC.ERROR_IND%TYPE,
                                 I_fix_column         IN       SA_ERROR_CODES.ERROR_FIX_COLUMN%TYPE,
                                 I_store_day_seq_no   IN       SA_TRAN_HEAD.STORE_DAY_SEQ_NO%TYPE,
                                 I_tran_seq_no        IN       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE,
                                 I_item_seq_no        IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE,
                                 I_discount_seq_no    IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE,
                                 I_store              IN       SA_TRAN_DISC.STORE%TYPE,
                                 I_day                IN       SA_TRAN_DISC.DAY%TYPE,
                                 I_disc_type          IN       SA_TRAN_DISC.DISC_TYPE%TYPE,
                                 I_coupon_no          IN       SA_TRAN_DISC.COUPON_NO%TYPE,
                                 I_rms_promo_type     IN       SA_TRAN_DISC.RMS_PROMO_TYPE%TYPE,
                                 I_promotion          IN       SA_TRAN_DISC.PROMOTION%TYPE,
                                 I_promo_comp         IN       SA_TRAN_DISC.PROMO_COMP%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50)   := 'SA_MASS_ERROR_SQL.REFRESH_DISCOUNT_ERRORS';
   L_clear_error_ind   VARCHAR2(1):= 'N';
   L_valid             BOOLEAN;
   L_promo_name        GTT_PROMO_TEMP.NAME%TYPE;
   L_promo_comp_rec    PM_API_SQL.COMPONENT_REC;

   L_item              SA_TRAN_ITEM.ITEM%TYPE;
   L_discount_uom      SA_TRAN_ITEM.SELLING_UOM%TYPE;
   L_standard_uom      SA_TRAN_ITEM.STANDARD_UOM%TYPE;

   L_discount_type_changed_ind    VARCHAR2(1) := 'N';
   L_promotion_changed_ind        VARCHAR2(1) := 'N';
   L_qty_changed_ind              VARCHAR2(1) := 'N';
   L_unit_disc_amt_changed_ind    VARCHAR2(1) := 'N';
   L_rms_promo_type_changed_ind   VARCHAR2(1) := 'N';

   cursor C_SA_ITEM_REC is
      select item,
             selling_uom,
             standard_uom
        from sa_tran_item
       where tran_seq_no = I_tran_seq_no
         and item_seq_no = I_item_seq_no
         and store = I_store
         and day = I_day;
BEGIN

   if O_error_ind = 'Y' then

      if I_fix_column = 'DISC_TYPE' then
         L_discount_type_changed_ind := 'Y';
      elsif I_fix_column in ('PROMOTION', 'RMS_PROMO_TYPE', 'PROMO_COMP') then
         L_promotion_changed_ind := 'Y';
      elsif I_fix_column = 'QTY' then
         L_qty_changed_ind := 'Y';
      elsif I_fix_column = 'UNIT_DISCOUNT_AMT' then
         L_unit_disc_amt_changed_ind := 'Y';
      elsif I_fix_column = 'RMS_PROMO_TYPE' then
         L_rms_promo_type_changed_ind := 'Y';
      end if;

      open C_SA_ITEM_REC;
      fetch C_SA_ITEM_REC into L_item,
                               L_discount_uom,
                               L_standard_uom;
      close C_SA_ITEM_REC;

      if I_disc_type = 'SCOUP'
         and I_coupon_no is NOT NULL then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'IDISC_COUPON_NO_REQ',
                                       I_item_seq_no,
                                       I_discount_seq_no,
                                       'IDISC',
                                       I_store,
                                       I_day) = FALSE then
            return FALSE;

         end if;
      end if;
      ---
      if I_rms_promo_type != 'ERR' and I_rms_promo_type != '9999' and
        I_disc_type is NOT NULL then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                      I_store_day_seq_no,
                                      I_tran_seq_no,
                                      'DISC_REASON_REQ',
                                      I_item_seq_no,
                                      I_discount_seq_no,
                                      'IDISC',
                                      I_store,
                                      I_day) = FALSE then
            return FALSE;

         end if;
         L_clear_error_ind := 'Y';
      end if;
      ---
      if (I_rms_promo_type = '9999' and I_promotion is NOT NULL) or
           (L_rms_promo_type_changed_ind = 'Y') then
           ---
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                      I_store_day_seq_no,
                                      I_tran_seq_no,
                                      'DISC_REF_NO_REQ',
                                      I_item_seq_no,
                                      I_discount_seq_no,
                                      'IDISC',
                                      I_store,
                                      I_day) = FALSE then
            return FALSE;

         end if;
         L_clear_error_ind := 'Y';
      end if;
      ---
      if (I_rms_promo_type = '9999' and I_promo_comp is NOT NULL) or
           (L_rms_promo_type_changed_ind = 'Y') then
           ---
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                      I_store_day_seq_no,
                                      I_tran_seq_no,
                                      'PROM_COMP_REQ',
                                      I_item_seq_no,
                                      I_discount_seq_no,
                                      'IDISC',
                                      I_store,
                                      I_day) = FALSE then
            return FALSE;

         end if;
         L_clear_error_ind := 'Y';
      end if;
      ---
      if I_promotion is NOT NULL then
         if PM_API_SQL.VALIDATE_PROMOTION_EXIST(O_error_message,
                                                L_valid,
                                                L_promo_name,
                                                I_promotion) = FALSE then
            return FALSE;
         end if;
         ---

         if L_valid = TRUE then
            L_clear_error_ind := 'Y';
         end if;

      end if;
      ---

         if TRAN_ERROR_SQL.REFRESH_DISC_ERRORS(O_error_message,
                                               O_error_ind,
                                               I_store_day_seq_no,
                                               I_tran_seq_no,
                                               I_item_seq_no,
                                               L_item,
                                               I_discount_seq_no,
                                               L_discount_uom,
                                               L_standard_uom,
                                               L_discount_type_changed_ind,
                                               L_promotion_changed_ind,
                                               L_qty_changed_ind,
                                               L_unit_disc_amt_changed_ind,
                                               I_store,
                                               I_day) = FALSE then
            return FALSE;

         end if;

      end if;

   ---
   if L_clear_error_ind = 'Y' then
      O_error_ind := 'N';
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_DISCOUNT_ERRORS;
------------------------------------------------------------------------------------------------
FUNCTION GET_PROMO_DETAIL(O_error_message     IN OUT  VARCHAR2,
                          O_rms_promo_type       OUT  SA_TRAN_DISC.RMS_PROMO_TYPE%TYPE,
                          O_promotion            OUT  SA_TRAN_DISC.PROMOTION%TYPE,
                          I_tran_seq_no       IN       SA_TRAN_DISC.TRAN_SEQ_NO%TYPE,
                          I_item_seq_no       IN       SA_TRAN_DISC.ITEM_SEQ_NO%TYPE,
                          I_discount_seq_no   IN       SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE,
                          I_store             IN       SA_TRAN_DISC.STORE%TYPE,
                          I_day               IN       SA_TRAN_DISC.DAY%TYPE)
RETURN BOOLEAN IS
   ---
   L_program  VARCHAR2(64) := 'SA_MASS_ERROR_SQL.GET_PROMO_DETAIL';

   cursor C_GET_PROMO_DTL is
      select rms_promo_type,
             promotion
        from sa_tran_disc std
       where tran_seq_no = I_tran_seq_no
         and item_seq_no = I_item_seq_no
         and discount_seq_no = I_discount_seq_no
         and store = I_store
         and day = I_day;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_PROMO_DTL','SA_TRAN_DISC',NULL);
   open C_GET_PROMO_DTL;
   SQL_LIB.SET_MARK('FETCH','C_GET_PROMO_DTL','SA_TRAN_DISC',NULL);
   fetch C_GET_PROMO_DTL into O_rms_promo_type, O_promotion;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PROMO_DTL','SA_TRAN_DISC',NULL);
   close C_GET_PROMO_DTL;

   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_PROMO_DETAIL;
------------------------------------------------------------------------------------------------
FUNCTION GET_TENDER_DETAIL(O_error_message       IN OUT   VARCHAR2,
                           O_tender_type_group      OUT   SA_TRAN_TENDER.TENDER_TYPE_GROUP%TYPE,
                           I_tran_seq_no         IN       SA_TRAN_TENDER.TRAN_SEQ_NO%TYPE,
                           I_item_seq_no         IN       SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE,
                           I_store               IN       SA_TRAN_TENDER.STORE%TYPE,
                           I_day                 IN       SA_TRAN_TENDER.DAY%TYPE)
RETURN BOOLEAN IS
   ---
   L_program  VARCHAR2(64) := 'SA_MASS_ERROR_SQL.GET_TENDER_DETAIL';

   cursor C_GET_TEND_DTL is
      select tender_type_group
        from sa_tran_tender stt
       where tran_seq_no = I_tran_seq_no
         and tender_seq_no = I_item_seq_no
         and store = I_store
         and day = I_day;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_TEND_DTL','SA_TRAN_TENDER',NULL);
   open C_GET_TEND_DTL;
   SQL_LIB.SET_MARK('FETCH','C_GET_TEND_DTL','SA_TRAN_TENDER',NULL);
   fetch C_GET_TEND_DTL into O_tender_type_group;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TEND_DTL','SA_TRAN_TENDER',NULL);
   close C_GET_TEND_DTL;

   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_TENDER_DETAIL;
------------------------------------------------------------------------------------------------
END SA_MASS_ERROR_SQL;
/
