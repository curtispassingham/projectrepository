



CREATE OR REPLACE PACKAGE BODY SA_TRANSACTION_SEQUENCE_SQL AS
-----------------------------------------------------------------------------------
FUNCTION GET_NEXT_ITEM_SEQ_NO(O_error_message   IN OUT VARCHAR2,
                              O_item_seq_no     IN OUT SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                              I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store           IN     SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                              I_day             IN     SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'TRANSACTION_SEQUENCE_SQL.GET_NEXT_ITEM_SEQ_NO';
   L_store    SA_STORE_DAY.STORE%TYPE := I_store;
   L_day      SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_NEXT is
      select NVL(MAX(item_seq_no),0) + 1
        from sa_tran_item
       where tran_seq_no = I_tran_seq_no
    and store = L_store 
    and day = L_day;

BEGIN

   if I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      open C_GET_NEXT;
      fetch C_GET_NEXT into O_item_seq_no;
      close C_GET_NEXT;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_ITEM_SEQ_NO;
--------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TENDER_SEQ_NO(O_error_message   IN OUT VARCHAR2,
                                O_tender_seq_no   IN OUT SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE,
                                I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                I_store           IN     SA_TRAN_TENDER.STORE%TYPE DEFAULT NULL,
                                I_day             IN     SA_TRAN_TENDER.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'TRANSACTION_SEQUENCE_SQL.GET_NEXT_TENDER_SEQ_NO';
   L_store     SA_STORE_DAY.STORE%TYPE := I_store;
   L_day       SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_NEXT_TENDER is
      select NVL(MAX(tender_seq_no),0) + 1
        from sa_tran_tender
       where tran_seq_no = I_tran_seq_no
    and store = L_store 
    and day = L_day;

BEGIN

   if I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      open C_GET_NEXT_TENDER;
      fetch C_GET_NEXT_TENDER into O_tender_seq_no;
      close C_GET_NEXT_TENDER;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_TENDER_SEQ_NO;
--------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TAX_SEQ_NO(O_error_message   IN OUT VARCHAR2,
                             O_tax_seq_no      IN OUT SA_TRAN_TAX.TAX_SEQ_NO%TYPE,
                             I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_store           IN     SA_TRAN_TAX.STORE%TYPE DEFAULT NULL,
                             I_day             IN     SA_TRAN_TAX.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(60)  := 'TRANSACTION_SEQUENCE_SQL.GET_NEXT_TAX_SEQ_NO';
   L_store       SA_STORE_DAY.STORE%TYPE := I_store;
   L_day         SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_NEXT_TAX is
      select NVL(MAX(tax_seq_no), 0) + 1
        from sa_tran_tax
       where tran_seq_no = I_tran_seq_no
    and store = L_store 
    and day = L_day;

BEGIN

   if I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      open C_GET_NEXT_TAX;
      fetch C_GET_NEXT_TAX into O_tax_seq_no;
      close C_GET_NEXT_TAX;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_TAX_SEQ_NO;
----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_IGTAX_SEQ_NO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_tax_seq_no      IN OUT SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE,
                               I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                               I_item_seq_no     IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                               I_store           IN     SA_TRAN_TAX.STORE%TYPE DEFAULT NULL,
                               I_day             IN     SA_TRAN_TAX.DAY%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)  := 'TRANSACTION_SEQUENCE_SQL.GET_NEXT_IGTAX_SEQ_NO';
   L_store       SA_STORE_DAY.STORE%TYPE := I_store;
   L_day         SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_NEXT_TAX is
      select NVL(MAX(igtax_seq_no), 0) + 1
        from sa_tran_igtax
       where tran_seq_no = I_tran_seq_no
         and item_seq_no = I_item_seq_no
         and store = L_store 
         and day   = L_day;

BEGIN

   if I_tran_seq_no is  NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_program,
                                             I_tran_seq_no,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_item_seq_no is  NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_program,
                                             I_item_seq_no,
                                             NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_NEXT_TAX',
                    'SA_TRAN_IGTAX',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)); 
   open C_GET_NEXT_TAX;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_NEXT_TAX',
                    'SA_TRAN_IGTAX',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)); 

   fetch C_GET_NEXT_TAX into O_tax_seq_no;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_NEXT_TAX',
                    'SA_TRAN_IGTAX',
                    'tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)); 

   close C_GET_NEXT_TAX;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_IGTAX_SEQ_NO;
----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DISC_SEQ_NO(O_error_message  IN OUT VARCHAR2,
                              O_disc_seq_no    IN OUT SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE,
                              I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_item_seq_no    IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                              I_store          IN     SA_TRAN_DISC.STORE%TYPE DEFAULT NULL,
                              I_day            IN     SA_TRAN_DISC.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(60)  := 'TRANSACTION_SEQUENCE_SQL.GET_NEXT_DISC_SEQ_NO';
   L_store      SA_STORE_DAY.STORE%TYPE := I_store;
   L_day        SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_NEXT_DISC is
      select NVL(MAX(discount_seq_no), 0) + 1
        from sa_tran_disc
       where tran_seq_no = I_tran_seq_no
    and store = L_store 
    and day = L_day
         and item_seq_no = I_item_seq_no;

BEGIN

   if I_tran_seq_no is NOT NULL or I_item_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      open C_GET_NEXT_DISC;
      fetch C_GET_NEXT_DISC into O_disc_seq_no;
      close C_GET_NEXT_DISC;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_DISC_SEQ_NO;
-----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_REV_NO(O_error_message  IN OUT VARCHAR2,
                         O_next_rev_no    IN OUT SA_TRAN_HEAD.REV_NO%TYPE,
                         I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_store          IN     SA_TRAN_HEAD_REV.STORE%TYPE DEFAULT NULL,
                         I_day            IN     SA_TRAN_HEAD_REV.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'TRANSACTION_SEQUENCE_SQL.GET_NEXT_REV_NO';
   L_store    SA_STORE_DAY.STORE%TYPE := I_store;
   L_day      SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_MAX_REV_NO is
      select NVL(MAX(rev_no)+1,1)
        from sa_tran_head_rev
       where tran_seq_no = I_tran_seq_no
    and store = L_store 
    and day = L_day;

BEGIN

   if I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      open C_GET_MAX_REV_NO;
      fetch C_GET_MAX_REV_NO into O_next_rev_no;
      close C_GET_MAX_REV_NO;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_REV_NO;
----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ATTRIB_SEQ_NO(O_error_message   IN OUT VARCHAR2,
                                O_attrib_seq_no   IN OUT SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE,
                                I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                I_store           IN     SA_CUST_ATTRIB.STORE%TYPE DEFAULT NULL,
                                I_day             IN     SA_CUST_ATTRIB.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(60)  := 'TRANSACTION_SEQUENCE_SQL.GET_NEXT_ATTRIB_SEQ_NO';
   L_store       SA_STORE_DAY.STORE%TYPE := I_store;
   L_day         SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_GET_NEXT_ATTRIB is
      select NVL(MAX(attrib_seq_no), 0) + 1
        from sa_cust_attrib
       where tran_seq_no = I_tran_seq_no
    and store = L_store 
    and day = L_day;

BEGIN

   if I_tran_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      open C_GET_NEXT_ATTRIB;
      fetch C_GET_NEXT_ATTRIB into O_attrib_seq_no;
      close C_GET_NEXT_ATTRIB;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_ATTRIB_SEQ_NO;
---------------------------------------------------------------------------------------
END SA_TRANSACTION_SEQUENCE_SQL;
/
