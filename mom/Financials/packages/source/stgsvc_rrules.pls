CREATE OR REPLACE PACKAGE STG_SVC_SA_ROUNDING_RULE AUTHID CURRENT_USER
AS
   template_key           CONSTANT VARCHAR2(255) :='SA_ROUNDING_RULE_DATA';
   action_new             VARCHAR2(25)           := 'NEW';
   action_mod             VARCHAR2(25)           := 'MOD';
   action_del             VARCHAR2(25)           := 'DEL';
   SA_RR_HEAD_sheet                VARCHAR2(255)         := 'SA_ROUNDING_RULE_HEAD';
   SA_RR_HEAD$Action               NUMBER                :=1;
   SA_RR_HEAD$STATUS               NUMBER                :=7;
   SA_RR_HEAD$START_BUSINESS_DATE  NUMBER                :=6;
   SA_RR_HEAD$COUNTRY_ID           NUMBER                :=5;
   SA_RR_HEAD$CURRENCY_CODE        NUMBER                :=4;
   SA_RR_HEAD$ROUNDING_RULE_NAME   NUMBER                :=3;
   SA_RR_HEAD$ROUNDING_RULE_ID     NUMBER                :=2;

   Type SA_ROUNDING_RULE_HEAD_REC_TAB IS TABLE OF SA_ROUNDING_RULE_HEAD%ROWTYPE;
   
   SA_RR_HEAD_TL_sheet                VARCHAR2(255)         := 'SA_ROUNDING_RULE_HEAD_TL';
   SA_RR_HEAD_TL$Action               NUMBER                :=1;
   SA_RR_HEAD_TL$LANG                 NUMBER                :=2;
   SA_RR_HEAD_TL$ROUNDING_RULE_ID     NUMBER                :=3;
   SA_RR_HEAD_TL$ROUNDING_RULE_NM     NUMBER                :=4;
   
   Type SA_RRULEE_HEAD_TL_REC_TAB IS TABLE OF SA_ROUNDING_RULE_HEAD_TL%ROWTYPE;
   
   SA_RR_DETAIL_sheet              VARCHAR2(255)         := 'SA_ROUNDING_RULE_DETAIL';
   SA_RR_DETAIL$Action             NUMBER                :=1;
   SA_RR_DETAIL$ROUND_AMT          NUMBER                :=5;
   SA_RR_DETAIL$HIGH_ENDING_AMT    NUMBER                :=4;
   SA_RR_DETAIL$LOW_ENDING_AMT     NUMBER                :=3;
   SA_RR_DETAIL$ROUNDING_RULE_ID   NUMBER                :=2;


   FUNCTION create_s9t(
   O_error_message     IN OUT rtk_errors.rtk_text%type,
   O_file_id           IN OUT s9t_folder.file_id%type,
   I_template_only_ind IN CHAR DEFAULT 'N') RETURN BOOLEAN;

   FUNCTION process_s9t(
   O_error_message IN OUT rtk_errors.rtk_text%type ,
   I_file_id       IN s9t_folder.file_id%type,
   I_process_id IN NUMBER,
   O_error_count OUT NUMBER)   RETURN BOOLEAN;

   sheet_name_trans s9t_pkg.trans_map_typ;

END STG_SVC_SA_ROUNDING_RULE;
/