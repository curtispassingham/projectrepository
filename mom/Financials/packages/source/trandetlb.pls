
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY TRANSACTION_DETAIL_SQL AS
--------------------------------------------------------
FUNCTION GET_STORE_BANNER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_banner_id       IN OUT   BANNER.BANNER_ID%TYPE,
                          O_banner_name     IN OUT   BANNER.BANNER_NAME%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_store           IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'TRANSACTION_DETAIL_SQL.GET_STORE_BANNER';

   cursor C_BANNER is
      select ch.banner_id,
             ba.banner_name
        from store st,
             channels ch,
             banner ba
       where st.store      = I_store
         and ch.channel_id = st.channel_id
         and ba.banner_id  = ch.banner_id;

BEGIN

   if I_store is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   O_exists  := TRUE;

   SQL_LIB.SET_MARK('OPEN', 'C_BANNER', 'CHANNELS, BANNER','I_STORE: '||to_char(I_store));
   open C_BANNER;
   SQL_LIB.SET_MARK('FETCH', 'C_BANNER', 'CHANNELS, BANNER','I_STORE: '||to_char(I_store));
   fetch C_BANNER into O_banner_id, 
                       O_banner_name;
   SQL_LIB.SET_MARK('CLOSE', 'C_BANNER', 'CHANNELS, BANNER','I_STORE: '||to_char(I_store));
   close C_BANNER;

   if O_banner_id is NULL then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_BANNER_FOR_STORE',
                                            I_store,
                                            NULL,
                                            NULL);
         return TRUE;
   end if;

   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

END GET_STORE_BANNER;
--------------------------------------------------------
FUNCTION ITEM_FILTER_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_diff            IN OUT   VARCHAR2,
                          I_tran_seq_no     IN       SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE)
RETURN BOOLEAN IS

   cursor C_COUNT_ITEM is
      select count(item)
        from sa_tran_item
       where tran_seq_no = I_tran_seq_no;

   cursor C_COUNT_VITEM is
      select count(sti.item)
        from sa_tran_item sti, 
             v_item_master vim 
       where sti.item    = vim.item
         and tran_seq_no = I_tran_seq_no;

   L_item    NUMBER(6) := -765547;
   L_vitem   NUMBER(6) := -987436;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_COUNT_ITEM','SA_TRAN_ITEM',I_tran_seq_no);
   open C_COUNT_ITEM;
   SQL_LIB.SET_MARK('FETCH','C_COUNT_ITEM','SA_TRAN_ITEM',I_tran_seq_no);
   fetch C_COUNT_ITEM into L_item;
   SQL_LIB.SET_MARK('CLOSE','C_COUNT_ITEM','SA_TRAN_ITEM',I_tran_seq_no);
   close C_COUNT_ITEM;

   SQL_LIB.SET_MARK('OPEN','C_COUNT_VITEM','SA_TRAN_ITEM',I_tran_seq_no);
   open C_COUNT_VITEM;
   SQL_LIB.SET_MARK('FETCH','C_COUNT_VITEM','SA_TRAN_ITEM',I_tran_seq_no);
   fetch C_COUNT_VITEM into L_vitem;
   SQL_LIB.SET_MARK('CLOSE','C_COUNT_VITEM','SA_TRAN_ITEM',I_tran_seq_no);
   close C_COUNT_VITEM;

   if NVL(L_vitem,0) != NVL(L_item,0) then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'TRANSACTION_DETAIL_SQL.ITEM_FILTER_LIST',
                                          to_char(SQLCODE));
    return FALSE;
END ITEM_FILTER_LIST;
-----------------------------------------------------------------------------------------------
FUNCTION GET_SUM_CONTENT_QTY (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_sum_content_qty         IN OUT   SA_TRAN_ITEM.QTY%TYPE,
                              O_sum_content_std_qty     IN OUT   SA_TRAN_ITEM.STANDARD_QTY%TYPE,
                              O_sum_content_uom_qty     IN OUT   SA_TRAN_ITEM.UOM_QUANTITY%TYPE,
                              I_tran_seq_no             IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store                   IN       SA_TRAN_HEAD.STORE%TYPE,
                              I_day                     IN       SA_TRAN_HEAD.DAY%TYPE,
                              I_container_item          IN       ITEM_MASTER.ITEM%TYPE,
                              I_container_item_status   IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE)
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(55) := 'TRANSACTION_DETAIL_SQL.GET_SUM_CONTENT_QTY';
   ---
   --- for a given Container item and a given Item Status, sum up the Qty for all sa_tran_item 
   --- Content items having that Container item.
   cursor C_SUM_CONTENT_QTY is
      select nvl(sum(sti.qty),0)            sum_content_qty,
             nvl(sum(sti.standard_qty),0)   sum_content_std_qty,
             nvl(sum(sti.uom_quantity),0)   sum_content_uom_qty
        from sa_tran_item sti, 
             item_master  im
       where sti.item          = im.item
         and sti.tran_seq_no   = I_tran_seq_no
         and sti.store         = I_store
         and sti.day           = I_day
         and im.container_item = I_container_item
         and sti.item_status   = I_container_item_status;   
   
BEGIN

   if I_tran_seq_no is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_seq_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_store is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_day is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_day',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_container_item is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_container_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_container_item_status is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_container_item_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---   
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUM_CONTENT_QTY',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' STORE: '||I_store||' DAY: '||I_day||' CONTAINER_ITEM: '||I_container_item||' ITEM_STATUS: '||I_container_item_status);
   open C_SUM_CONTENT_QTY;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUM_CONTENT_QTY',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' STORE: '||I_store||' DAY: '||I_day||' CONTAINER_ITEM: '||I_container_item||' ITEM_STATUS: '||I_container_item_status);
   fetch C_SUM_CONTENT_QTY into O_sum_content_qty, 
                                O_sum_content_std_qty, 
                                O_sum_content_uom_qty;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUM_CONTENT_QTY',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' STORE: '||I_store||' DAY: '||I_day||' CONTAINER_ITEM: '||I_container_item||' ITEM_STATUS: '||I_container_item_status);   
   close C_SUM_CONTENT_QTY;
   ---   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_SUM_CONTENT_QTY;   
-----------------------------------------------------------------------------------------------
FUNCTION GET_CONTAINER_QTY (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_container_item        IN OUT   ITEM_MASTER.ITEM%TYPE,
                            O_container_qty         IN OUT   SA_TRAN_ITEM.QTY%TYPE,
                            O_item_seq_no           IN OUT   SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                            I_tran_seq_no           IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                            I_store                 IN       SA_TRAN_HEAD.STORE%TYPE,
                            I_day                   IN       SA_TRAN_HEAD.DAY%TYPE,
                            I_content_item          IN       ITEM_MASTER.ITEM%TYPE,
                            I_content_item_status   IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE)
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(55) := 'TRANSACTION_DETAIL_SQL.GET_CONTAINER_QTY';
   ---
   --- For a given Content item/item status, get the Container item qty.
   --- This is based on the assumption that 
   --- tran_seq_no/store/day/container_item/container_item_status combination is unique.
   --- i.e. Content1 (is in ContainerA); Content2 (is in ContainerA).
   --- If we have a Sales transaction with items Content1 qty=4; Content2 qty=1,
   --- there will only be 1 line item with item ContainerA qty=5 (4+1).
   ---
   cursor C_CONTAINER_QTY is
   select im.container_item,
          sti.item_seq_no,
          sti.qty
     from sa_tran_item sti,
          item_master  im
    where im.item           = I_content_item
      and im.container_item = sti.item
      and sti.tran_seq_no   = I_tran_seq_no
      and sti.store         = I_store
      and sti.day           = I_day
      and sti.item_status   = I_content_item_status;
   
BEGIN

   if I_tran_seq_no is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_seq_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---   
   if I_store is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_day is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_day',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_content_item is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_content_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_content_item_status is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_content_item_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---   
   SQL_LIB.SET_MARK('OPEN',
                    'C_CONTAINER_QTY',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' STORE: '||I_store||' DAY: '||I_day||' CONTENT_ITEM: '||I_content_item||' ITEM_STATUS: '||I_content_item_status);
   open C_CONTAINER_QTY;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CONTAINER_QTY',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' STORE: '||I_store||' DAY: '||I_day||' CONTENT_ITEM: '||I_content_item||' ITEM_STATUS: '||I_content_item_status);
   fetch C_CONTAINER_QTY into O_container_item, 
                              O_item_seq_no,
                              O_container_qty;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CONTAINER_QTY',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' STORE: '||I_store||' DAY: '||I_day||' CONTENT_ITEM: '||I_content_item||' ITEM_STATUS: '||I_content_item_status);
   close C_CONTAINER_QTY;
   ---
   if O_container_item is NULL then
      if ITEM_ATTRIB_SQL.GET_CONTAINER_ITEM(O_error_message,
                                            O_container_item,
                                            I_content_item) = FALSE then
         return FALSE;
      end if;
      ---
      O_item_seq_no   := NULL;
      O_container_qty := 0;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_CONTAINER_QTY;
-----------------------------------------------------------------------------------------------
FUNCTION ADD_CONTAINER_ITEM_TO_TRAN (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_tran_seq_no             IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                     I_store                   IN       SA_TRAN_HEAD.STORE%TYPE,
                                     I_day                     IN       SA_TRAN_HEAD.DAY%TYPE,
                                     I_container_item          IN       ITEM_MASTER.ITEM%TYPE,
                                     I_container_item_status   IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE,
                                     I_qty                     IN       SA_TRAN_ITEM.QTY%TYPE)
   RETURN BOOLEAN IS
 
   L_program                 VARCHAR2(55)                    := 'TRANSACTION_DETAIL_SQL.ADD_CONTAINER_ITEM_TO_TRAN';
   L_container_item_seq_no   SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE;
   ---
   L_item_master             V_ITEM_MASTER%ROWTYPE;
   L_valid                   BOOLEAN;
   ---
   L_item_desc               ITEM_MASTER.ITEM_DESC%TYPE;
   L_item_level              ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level              ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_status                  ITEM_MASTER.STATUS%TYPE;
   L_pack_ind                ITEM_MASTER.PACK_IND%TYPE;
   L_dept                    ITEM_MASTER.DEPT%TYPE;
   L_dept_name               DEPS.DEPT_NAME%TYPE;
   L_class                   ITEM_MASTER.CLASS%TYPE;
   L_class_name              CLASS.CLASS_NAME%TYPE;
   L_subclass                ITEM_MASTER.SUBCLASS%TYPE;
   L_subclass_name           SUBCLASS.SUB_NAME%TYPE;   
   L_sellable_ind            ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind           ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type               ITEM_MASTER.PACK_TYPE%TYPE;
   L_simple_pack_ind         ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
   L_waste_type              ITEM_MASTER.WASTE_TYPE%TYPE;
   L_item_parent             ITEM_MASTER.ITEM_PARENT%TYPE;
   L_item_grandparent        ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_short_desc              ITEM_MASTER.SHORT_DESC%TYPE;
   L_waste_pct               ITEM_MASTER.WASTE_PCT%TYPE;
   L_default_waste_pct       ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;
   ---
   L_exists                  BOOLEAN;
   ---
   L_loc_type                ITEM_LOC.LOC_TYPE%TYPE;
   L_unit_retail             ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom             ITEM_LOC.SELLING_UOM%TYPE;
   L_clear_ind               ITEM_LOC.CLEAR_IND%TYPE;
   L_taxable_ind             ITEM_LOC.TAXABLE_IND%TYPE;
   L_local_item_desc         ITEM_LOC.LOCAL_ITEM_DESC%TYPE;
   L_local_short_desc        ITEM_LOC.LOCAL_SHORT_DESC%TYPE;
   L_ti                      ITEM_LOC.TI%TYPE;
   L_hi                      ITEM_LOC.HI%TYPE;
   L_store_ord_mult          ITEM_LOC.STORE_ORD_MULT%TYPE;
   L_itemloc_status          ITEM_LOC.STATUS%TYPE;
   L_status_update_date      ITEM_LOC.STATUS_UPDATE_DATE%TYPE;
   L_daily_waste_pct         ITEM_LOC.DAILY_WASTE_PCT%TYPE;
   L_meas_of_each            ITEM_LOC.MEAS_OF_EACH%TYPE;
   L_meas_of_price           ITEM_LOC.MEAS_OF_PRICE%TYPE;
   L_uom_of_price            ITEM_LOC.UOM_OF_PRICE%TYPE;
   L_primary_variant         ITEM_LOC.PRIMARY_VARIANT%TYPE;
   L_primary_supp            ITEM_LOC.PRIMARY_SUPP%TYPE;
   L_primary_cntry           ITEM_LOC.PRIMARY_CNTRY%TYPE;
   L_primary_cost_pack       ITEM_LOC.PRIMARY_COST_PACK%TYPE;
   ---
   L_standard_uom            UOM_CLASS.UOM%TYPE;
   L_standard_class          UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor             ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   ---
   L_qty                     NUMBER;
   L_standard_unit_retail    SA_TRAN_ITEM.STANDARD_UNIT_RETAIL%TYPE;
   L_standard_qty            SA_TRAN_ITEM.STANDARD_QTY%TYPE;
   
BEGIN   
   --- Only transaction level Container items can be associated with a Content Item.
   --- Container items are always Sellable.
   --- So there's no need to validate the transaction level and sellable indicator 
   --- for the item.   
    
   --- verify if the user has visibility of the container item
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_ITEM_MASTER(O_error_message,
                                                  L_valid,
                                                  L_item_master,
                                                  I_container_item) = FALSE
      or L_valid = FALSE then
      return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                               L_item_desc,
                               L_item_level,
                               L_tran_level,
                               L_status,
                               L_pack_ind,
                               L_dept,
                               L_dept_name,
                               L_class,
                               L_class_name,
                               L_subclass,
                               L_subclass_name,                               
                               L_sellable_ind,
                               L_orderable_ind,
                               L_pack_type,
                               L_simple_pack_ind,
                               L_waste_type,
                               L_item_parent,
                               L_item_grandparent,
                               L_short_desc,
                               L_waste_pct,
                               L_default_waste_pct,
                               I_container_item) = FALSE then
      return FALSE;
   end if;
   ---
   if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                        I_container_item,
                                        I_store,
                                        L_exists) = FALSE then
      return FALSE;
   end if;
   ---
   if L_exists  = TRUE then
      if ITEM_LOC_SQL.GET_ITEM_LOC(O_error_message,
                                   L_item_parent,
                                   L_item_grandparent,
                                   L_loc_type,
                                   L_unit_retail,
                                   L_selling_unit_retail,
                                   L_selling_uom,
                                   L_clear_ind,
                                   L_taxable_ind,
                                   L_local_item_desc,
                                   L_local_short_desc,
                                   L_ti,
                                   L_hi,
                                   L_store_ord_mult,
                                   L_itemloc_status,
                                   L_status_update_date,
                                   L_daily_waste_pct,
                                   L_meas_of_each,
                                   L_meas_of_price,
                                   L_uom_of_price,
                                   L_primary_variant,
                                   L_primary_supp,
                                   L_primary_cntry,
                                   L_primary_cost_pack,
                                   I_container_item,
                                   I_store) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                       L_standard_uom,
                                       L_standard_class,
                                       L_conv_factor,
                                       I_container_item,
                                       'N') = FALSE then
      return FALSE;
   end if;
   ---
   L_selling_uom := nvl(L_selling_uom, L_standard_uom);
   ---
   if UOM_SQL.CONVERT(O_error_message,
                      L_qty,
                      L_standard_uom,
                      1,
                      L_selling_uom,
                      I_container_item,
                      NULL,
                      NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if L_qty = 0 then 
      O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                            L_standard_uom,
                                            L_selling_uom,
                                            NULL);
      return FALSE;
   end if;
   ---
      if L_qty != 0 and L_unit_retail is NOT NULL then
         L_standard_unit_retail := L_unit_retail / L_qty;
         L_standard_qty         := I_qty * L_qty;
      end if;
   ---
   --- fetches the next sequential item_seq_no on the sa_tran_item table.
   if SA_TRANSACTION_SEQUENCE_SQL.GET_NEXT_ITEM_SEQ_NO (O_error_message,
                                                        L_container_item_seq_no,
                                                        I_tran_seq_no,
                                                        I_store,
                                                        I_day) = FALSE then
     return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'SA_TRAN_ITEM','STORE: '||to_char(I_store)||' DAY: '||to_char(I_day)||' TRAN_SEQ_NO: '||to_char(I_tran_seq_no)||' ITEM_SEQ_NO: '||to_char(L_container_item_seq_no));
   insert into sa_tran_item (store,
                             day,
                             tran_seq_no,
                             item_seq_no,
                             item_status,
                             item_type,
                             item,
                             ref_item,
                             non_merch_item,
                             voucher_no,
                             dept,
                             class,
                             subclass,
                             qty,
                             unit_retail,
                             selling_uom,
                             override_reason,
                             orig_unit_retail,
                             standard_orig_unit_retail,
                             tax_ind,
                             item_swiped_ind,
                             error_ind,
                             drop_ship_ind,
                             waste_type,
                             waste_pct,
                             pump,
                             return_reason_code,
                             salesperson,
                             expiration_date,
                             standard_qty,
                             standard_unit_retail,
                             standard_uom,
                             ref_no5,
                             ref_no6,
                             ref_no7,
                             ref_no8,
                             uom_quantity,
                             catchweight_ind,
                             selling_item,
                             customer_order_line_no,
                             media_id)
                     values (I_store,
                             I_day,
                             I_tran_seq_no,
                             L_container_item_seq_no,
                             I_container_item_status,     --- container and content must be of the same item_status
                             'ITEM',                      --- item_type
                             I_container_item,            --- item
                             NULL,                        --- ref_item
                             NULL,                        --- non_merch_item
                             NULL,                        --- voucher_no
                             L_dept,
                             L_class,
                             L_subclass,
                             I_qty,
                             L_unit_retail,
                             L_selling_uom,
                             NULL,                        --- override_reason
                             L_unit_retail,               --- orig_unit_retail
                             L_standard_unit_retail,      --- standard_orig_unit_retail
                             nvl(L_taxable_ind, 'N'),     --- tax_ind
                             'N',                         --- item_swiped_ind
                             'N',                         --- error_ind
                             'N',                         --- drop_ship_ind
                             L_waste_type,
                             L_waste_pct,
                             NULL,                        --- pump
                             NULL,                        --- return_reason_code; this could be for more than 1 returned content item with different return reason
                             NULL,                        --- salesperson
                             NULL,                        --- expiration_date
                             L_standard_qty,
                             L_standard_unit_retail,
                             L_standard_uom,              
                             NULL,                        --- ref_no5
                             NULL,                        --- ref_no6
                             NULL,                        --- ref_no7
                             NULL,                        --- ref_no8
                             I_qty,                       --- uom_quantity
                             L_item_master.catch_weight_ind,
                             NULL,                        --- selling_item
                             NULL,                        --- customer_order_line_no; this could be for more than 1 content item
                             NULL);                       --- media_id

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ADD_CONTAINER_ITEM_TO_TRAN;
-----------------------------------------------------------------------------------------------
FUNCTION DELETE_SINGLE_CONTAINER_ITEM (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_tran_seq_no             IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                       I_store                   IN       SA_TRAN_HEAD.STORE%TYPE,
                                       I_day                     IN       SA_TRAN_HEAD.DAY%TYPE,
                                       I_container_item_seq_no   IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(55) := 'TRANSACTION_DETAIL_SQL.DELETE_SINGLE_CONTAINER_ITEM';
   ---
         
BEGIN
   if I_tran_seq_no is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_seq_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---   
   if I_store is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_day is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_day',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_container_item_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_container_item_seq_no',
                                             L_program,
                                             NULL);   
   end if;   
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, 'SA_TRAN_ITEM','STORE: '||to_char(I_store)||' DAY: '||to_char(I_day)||' TRAN_SEQ_NO: '||to_char(I_tran_seq_no)||' ITEM_SEQ_NO: '||to_char(I_container_item_seq_no));
   delete from sa_tran_item sti
         where sti.tran_seq_no = I_tran_seq_no
           and sti.item_seq_no = I_container_item_seq_no
           and sti.store       = I_store
           and sti.day         = I_day;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_SINGLE_CONTAINER_ITEM;
-----------------------------------------------------------------------------------------------
FUNCTION DELETE_ORPHAN_CONTAINER_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                       I_store           IN       SA_TRAN_HEAD.STORE%TYPE,
                                       I_day             IN       SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS
   
   L_program     VARCHAR2(55) := 'TRANSACTION_DETAIL_SQL.DELETE_ORPHAN_CONTAINER_ITEM';
   L_discounts   SA_TRAN_DISC.UNIT_DISCOUNT_AMT%TYPE;  
   ---
   cursor C_ORPHAN is
      select sti_container.item_seq_no,
             sti_container.item, 
             sti_container.item_status
        from sa_tran_item sti_container,
             item_master  im
       where sti_container.tran_seq_no = I_tran_seq_no
         and sti_container.store       = I_store
         and sti_container.day         = I_day
         and im.deposit_item_type      = 'A'  -- container
         and sti_container.item        = im.item   
         and not exists (select 'x'
                           from sa_tran_item sti,
                                item_master  im2
                          where sti.item           = im2.item
                            and sti.tran_seq_no    = I_tran_seq_no
                            and sti.store          = I_store
                            and sti.day            = I_day
                            and im2.container_item = im.item
                            and sti.item_status    = sti_container.item_status);

   
BEGIN
   FOR L_orphan_rec in C_ORPHAN LOOP
      ---
      if SA_ITEM_SQL.DELETE_ITEM_DISCOUNTS(O_error_message,
                                           L_discounts,
                                           I_tran_seq_no,
                                           L_orphan_rec.item_seq_no) = FALSE then
         return FALSE;
      end if;      
      ---
      if TRANSACTION_DETAIL_SQL.DELETE_SINGLE_CONTAINER_ITEM (O_error_message,
                                                              I_tran_seq_no,
                                                              I_store,
                                                              I_day,
                                                              L_orphan_rec.item_seq_no) = FALSE then  --- container_item_seq_no
         return FALSE;
      end if;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ORPHAN_CONTAINER_ITEM;
-----------------------------------------------------------------------------------------------   
FUNCTION UPDATE_CONTAINER_ITEM_QTY (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tran_seq_no           IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                    I_store                 IN       SA_TRAN_HEAD.STORE%TYPE,
                                    I_day                   IN       SA_TRAN_HEAD.DAY%TYPE,
                                    I_content_item          IN       ITEM_MASTER.ITEM%TYPE,
                                    I_content_item_status   IN       SA_TRAN_ITEM.ITEM_STATUS%TYPE)
   RETURN BOOLEAN IS

   L_program                 VARCHAR2(55)          := 'TRANSACTION_DETAIL_SQL.UPDATE_CONTAINER_ITEM_QTY';
   L_container_item          ITEM_MASTER.ITEM%TYPE;
   L_container_qty           SA_TRAN_ITEM.QTY%TYPE;
   L_sum_content_qty         SA_TRAN_ITEM.QTY%TYPE;
   L_sum_content_std_qty     SA_TRAN_ITEM.STANDARD_QTY%TYPE;
   L_sum_content_uom_qty     SA_TRAN_ITEM.UOM_QUANTITY%TYPE;
   L_container_item_seq_no   SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE;
   
BEGIN
   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_day is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_day',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_content_item is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_content_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_content_item_status is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_content_item_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   --- get container qty
   if TRANSACTION_DETAIL_SQL.GET_CONTAINER_QTY(O_error_message,
                                               L_container_item,
                                               L_container_qty,
                                               L_container_item_seq_no,
                                               I_tran_seq_no,
                                               I_store,
                                               I_day,
                                               I_content_item,
                                               I_content_item_status) = FALSE then
      return FALSE;
   end if;
   
   --- get sum(content qty)
   if TRANSACTION_DETAIL_SQL.GET_SUM_CONTENT_QTY (O_error_message,
                                                  L_sum_content_qty,
                                                  L_sum_content_std_qty,
                                                  L_sum_content_uom_qty,
                                                  I_tran_seq_no,
                                                  I_store,
                                                  I_day,
                                                  L_container_item,
                                                  I_content_item_status) = FALSE then
      return FALSE;                                               
   end if;

   --- compare container qty with sum(content qty)      
   if L_container_qty != L_sum_content_qty then
      SQL_LIB.SET_MARK('UPDATE', NULL, 'SA_TRAN_ITEM','STORE: '||to_char(I_store)||' DAY: '||to_char(I_day)||' TRAN_SEQ_NO: '||to_char(I_tran_seq_no)||' ITEM_SEQ_NO: '||to_char(L_container_item_seq_no));
      update sa_tran_item sti
         set sti.qty           = L_sum_content_qty,
             sti.standard_qty  = L_sum_content_std_qty,
             sti.uom_quantity  = L_sum_content_uom_qty
       where sti.tran_seq_no = I_tran_seq_no
         and sti.store       = I_store
         and sti.day         = I_day
         and sti.item_seq_no = L_container_item_seq_no;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_CONTAINER_ITEM_QTY;
-----------------------------------------------------------------------------------------------   
FUNCTION POP_SINGLE_CONTAINER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_tran_seq_no     IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                   I_item_seq_no     IN       SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                                   I_store           IN       SA_TRAN_HEAD.STORE%TYPE,
                                   I_day             IN       SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS
   
   L_program                 VARCHAR2(55)          := 'TRANSACTION_DETAIL_SQL.POP_SINGLE_CONTAINER_ITEM';
   L_content_item            ITEM_MASTER.ITEM%TYPE;
   L_content_item_status     SA_TRAN_ITEM.ITEM_STATUS%TYPE;
   ---
   L_container_item          ITEM_MASTER.ITEM%TYPE;
   L_container_item_dummy    ITEM_MASTER.ITEM%TYPE; 
   L_container_item_seq_no   SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE;
   L_container_qty           SA_TRAN_ITEM.QTY%TYPE;
   ---
   L_sum_content_qty         SA_TRAN_ITEM.QTY%TYPE := 0;
   L_sum_content_std_qty     SA_TRAN_ITEM.STANDARD_QTY%TYPE;   
   L_sum_content_uom_qty     SA_TRAN_ITEM.UOM_QUANTITY%TYPE;
   ---
   cursor C_CONTENT_ITEM is
      select im.item,
             sti.item_status,
             im.container_item
        from sa_tran_item sti,
             item_master  im
       where sti.tran_seq_no      = I_tran_seq_no
         and sti.item_seq_no      = I_item_seq_no
         and sti.store            = I_store
         and sti.day              = I_day
         and nvl(sti.qty,0)      != 0
         and sti.item             = im.item
         and im.deposit_item_type = 'E'; -- Content  

BEGIN
   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_item_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_day is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_day',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --- get the content item
   SQL_LIB.SET_MARK('OPEN',
                    'C_CONTENT_ITEM',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' ITEM_SEQ_NO: '||I_item_seq_no||' STORE: '||I_store||' DAY: '||I_day);
   open C_CONTENT_ITEM;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CONTENT_ITEM',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' ITEM_SEQ_NO: '||I_item_seq_no||' STORE: '||I_store||' DAY: '||I_day);
   fetch C_CONTENT_ITEM into L_content_item, 
                             L_content_item_status, 
                             L_container_item;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CONTENT_ITEM',
                    'SA_TRAN_ITEM, ITEM_MASTER',
                    'TRAN_SEQ_NO: '||I_tran_seq_no||' ITEM_SEQ_NO: '||I_item_seq_no||' STORE: '||I_store||' DAY: '||I_day);
   close C_CONTENT_ITEM;
   ---
   if L_content_item is NOT NULL then
      --- get the content qty
      if TRANSACTION_DETAIL_SQL.GET_SUM_CONTENT_QTY (O_error_message,
                                                     L_sum_content_qty,
                                                     L_sum_content_std_qty,
                                                     L_sum_content_uom_qty,
                                                     I_tran_seq_no,
                                                     I_store,
                                                     I_day,
                                                     L_container_item,
                                                     L_content_item_status) = FALSE then
         return FALSE;
      end if;

      --- get container qty and possibly container_item_seq_no (if the container already exists)
      if TRANSACTION_DETAIL_SQL.GET_CONTAINER_QTY(O_error_message,
                                                  L_container_item_dummy,
                                                  L_container_qty,
                                                  L_container_item_seq_no,
                                                  I_tran_seq_no,
                                                  I_store,
                                                  I_day,
                                                  L_content_item,
                                                  L_content_item_status) = FALSE then
         return FALSE;
      end if;      
      ---
      --- if there are content items using the given container item, 
      --- update into sa_tran_item for the container item, else insert.
      ---
      if L_sum_content_qty != 0 then         
         ---
         --- The container is also used by other contents in the same transaction.
         --- Instead of deleting/inserting the Container to have the correct qty,
         --- just update. This way, we won't lose other related detail-records (i.e. sa_tran_disc).
         --- And the sort-order of the Container item within satrdetl.fmb B_tran_item won't change.
         ---      
         if L_container_item_seq_no is NOT NULL then
            --- update container qty
            if L_container_qty != L_sum_content_qty then
               SQL_LIB.SET_MARK('UPDATE', NULL, 'SA_TRAN_ITEM','STORE: '||to_char(I_store)||' DAY: '||to_char(I_day)||' TRAN_SEQ_NO: '||to_char(I_tran_seq_no)||' ITEM_SEQ_NO: '||to_char(L_container_item_seq_no));
               update sa_tran_item sti
                  set sti.qty           = L_sum_content_qty,
                      sti.standard_qty  = L_sum_content_std_qty,
                      sti.uom_quantity  = L_sum_content_uom_qty
                where sti.tran_seq_no = I_tran_seq_no
                  and sti.store       = I_store
                  and sti.day         = I_day
                  and sti.item_seq_no = L_container_item_seq_no;
            end if;
            ---
         else
            --- insert sa_tran_item record for the Container
            if TRANSACTION_DETAIL_SQL.ADD_CONTAINER_ITEM_TO_TRAN (O_error_message,
                                                                  I_tran_seq_no,
                                                                  I_store,
                                                                  I_day,
                                                                  L_container_item,
                                                                  L_content_item_status,
                                                                  L_sum_content_qty) = FALSE then
               return FALSE;
            end if;
            ---            
         end if;
      end if;
   end if;
   ---
   --- check for orphan containers 
   if TRANSACTION_DETAIL_SQL.DELETE_ORPHAN_CONTAINER_ITEM (O_error_message,
                                                           I_tran_seq_no,
                                                           I_store,
                                                           I_day) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END POP_SINGLE_CONTAINER_ITEM;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE_BANNER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid           IN OUT   BOOLEAN,
                               O_banner_name     IN OUT   BANNER.BANNER_NAME%TYPE,
                               I_banner_id       IN       BANNER.BANNER_ID%TYPE,
                               I_store           IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'TRANSACTION_DETAIL_SQL.VALIDATE_STORE_BANNER';
   L_banner_name   BANNER.BANNER_NAME%TYPE;

   cursor C_BANNER_NAME is
      select banner_name
        from banner bn,
             channels ch,
             store st
       where st.store      = I_store
         and st.channel_id = ch.channel_id
         and ch.banner_id  = I_banner_id
         and ch.banner_id  = bn.banner_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_BANNER_NAME',
                    'BANNER,CHANNELS',
                    'BANNER_ID: '||TO_CHAR(I_banner_id)||' STORE: '||I_store);
   open C_BANNER_NAME;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_BANNER_NAME',
                    'BANNER,CHANNELS',
                    'BANNER_ID: '||TO_CHAR(I_banner_id)||' STORE: '||I_store);
   fetch C_BANNER_NAME into L_banner_name;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_BANNER_NAME',
                    'BANNER,CHANNELS',
                    'BANNER_ID: '||TO_CHAR(I_banner_id)||' STORE: '||I_store);
   close C_BANNER_NAME;

   if L_banner_name is NOT NULL then
      O_valid         := TRUE;
      O_banner_name   := L_banner_name;
   else
      O_valid         := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_BANNER',
                                            NULL,
                                            NULL,
                                            NULL);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_STORE_BANNER;
-----------------------------------------------------------------------------------------------
END TRANSACTION_DETAIL_SQL;
/
