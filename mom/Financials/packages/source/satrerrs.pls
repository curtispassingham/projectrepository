



CREATE OR REPLACE PACKAGE TRAN_ERROR_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------
FUNCTION REFRESH_ITEM_ERRORS(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_ind                   IN OUT SA_TRAN_ITEM.ERROR_IND%TYPE,
                             I_store_day_seq_no            IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                             I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_item_seq_no                 IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                             I_item                        IN     SA_TRAN_ITEM.ITEM%TYPE,
                             I_selling_uom                 IN     SA_TRAN_ITEM.SELLING_UOM%TYPE,
                             I_standard_uom                IN     SA_TRAN_ITEM.STANDARD_UOM%TYPE,
                             I_item_type_changed_ind       IN     VARCHAR2,
                             I_item_changed_ind            IN     VARCHAR2,
                             I_unit_retail_changed_ind     IN     VARCHAR2,
                             I_orig_ur_changed_ind         IN     VARCHAR2,
                             I_override_reason_changed_ind IN     VARCHAR2,
                             I_qty_changed_ind             IN     VARCHAR2,
                             I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION REFRESH_TENDER_ERRORS(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error_ind                   IN OUT SA_TRAN_TENDER.ERROR_IND%TYPE,
                               I_store_day_seq_no            IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                               I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                               I_tender_seq_no               IN     SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE,
                               I_tend_type_group_changed_ind IN     VARCHAR2,
                               I_tender_type_id_changed_ind  IN     VARCHAR2,
                               I_tender_no_changed_ind       IN     VARCHAR2,
                               I_tender_amt_changed_ind      IN     VARCHAR2,
                               I_cc_no_changed_ind           IN     VARCHAR2,
                               I_cc_exp_date_changed_ind     IN     VARCHAR2,
                               I_cc_auth_src_changed_ind     IN     VARCHAR2,
                               I_cc_card_verf_changed_ind    IN     VARCHAR2,
                               I_cc_spec_cond_changed_ind    IN     VARCHAR2,
                               I_cc_entry_mode_changed_ind   IN     VARCHAR2,
                               I_add_details_changed_ind     IN     VARCHAR2,
                               I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                               I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION REFRESH_TAX_ERRORS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error_ind            IN OUT SA_TRAN_TAX.ERROR_IND%TYPE,
                            I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                            I_tran_seq_no          IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                            I_tax_seq_no           IN     SA_TRAN_TAX.TAX_SEQ_NO%TYPE,
                            I_tax_code_changed_ind IN     VARCHAR2,
                            I_tax_amt_changed_ind  IN     VARCHAR2,
                            I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                            I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION REFRESH_IGTAX_ERRORS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error_ind            IN OUT SA_TRAN_IGTAX.ERROR_IND%TYPE,
                              I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                              I_tran_seq_no          IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_item_seq_no          IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                              I_igtax_seq_no         IN     SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE,
                              I_tax_auth_changed_ind IN     VARCHAR2,
                              I_tax_code_changed_ind IN     VARCHAR2,
                              I_tax_amt_changed_ind  IN     VARCHAR2,
							  I_tax_rate_changed_ind IN     VARCHAR2,
                              I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                              I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION REFRESH_DISC_ERRORS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_ind                  IN OUT SA_TRAN_DISC.ERROR_IND%TYPE,
                             I_store_day_seq_no           IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                             I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_item_seq_no                IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                             I_item                       IN     SA_TRAN_ITEM.ITEM%TYPE,
                             I_discount_seq_no            IN     SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE,
                             I_discount_uom               IN     SA_TRAN_ITEM.SELLING_UOM%TYPE,
                             I_standard_uom               IN     SA_TRAN_ITEM.STANDARD_UOM%TYPE,
                             I_discount_type_changed_ind  IN     VARCHAR2,
                             I_promotion_changed_ind      IN     VARCHAR2,
                             I_qty_changed_ind            IN     VARCHAR2,
                             I_unit_disc_amt_changed_ind  IN     VARCHAR2,
                             I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CLOSE(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COND(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                      I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------- 
FUNCTION PROCESS_DCLOSE(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_LOAN(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                      I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_METER(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------     
FUNCTION PROCESS_NOSALE(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------- 
FUNCTION PROCESS_OPEN(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                      I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PAIDIN(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PAIDOU(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------  
FUNCTION PROCESS_PULL(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                      I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PUMPT(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------  
FUNCTION PROCESS_PVOID(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------- 
FUNCTION PROCESS_REFUND(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------- 
FUNCTION PROCESS_TANKDP(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------  
FUNCTION PROCESS_TERM(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TOTAL(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SALE_RETURN(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_EEXCH(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_MISS_TRAN(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_store_day_seq_no           IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                           I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Purpose: This function will update all of the transaction errors that need to be assoiciated 
--          with a different balancing group (cashier/register) in the case where the transaction
--          is modified to involve a different cashier/register balancing group.
--
FUNCTION UPDATE_BALANCE_GROUP(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_new_bal_group_seq_no  IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                              I_tran_seq_no           IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store                 IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                              I_day                   IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END TRAN_ERROR_SQL;
/
