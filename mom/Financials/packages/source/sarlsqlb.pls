CREATE OR REPLACE PACKAGE BODY SA_RULE_SQL AS

----------------------------------------------------------------------------------
FUNCTION CHECK_UNIQUE_RULE_ID(O_error_message      IN OUT VARCHAR2,
                              O_unique             IN OUT BOOLEAN,
                              I_rule_id            IN     SA_RULE_HEAD.RULE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program_name VARCHAR2(60) := 'SA_RULE_SQL.CHECK_UNIQUE_RULE_ID';
   L_dummy     VARCHAR2(1) := 'Z';

   cursor C_GET_ID is
      select 'X'
        from sa_rule_head
       where rule_id = I_rule_id;

BEGIN
   O_unique := TRUE;
   ---
   if I_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ID', 'sa_rule_head', I_rule_id);
   open C_GET_ID;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_ID', 'sa_rule_head', I_rule_id);
   fetch C_GET_ID into L_dummy;
   ---
   if L_dummy = 'X' then
      O_unique := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ID', 'sa_rule_head', I_rule_id);
   close C_GET_ID;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_UNIQUE_RULE_ID;
----------------------------------------------------------------------------------
FUNCTION GET_MAX_REV(O_error_message   IN OUT   VARCHAR2,
                     O_rule_rev_no     IN OUT   SA_RULE_HEAD.RULE_REV_NO%TYPE,
                     I_rule_id         IN SA_RULE_HEAD.RULE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program_name VARCHAR2(60) := 'SA_RULE_SQL.GET_MAX_REV';

   cursor C_GET_MAX_REV is
      select max(rule_rev_no)
        from sa_rule_head
       where rule_id = I_rule_id;

BEGIN
   ---
   if I_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_MAX_REV', 'sa_rule_head', O_rule_rev_no);
   open C_GET_MAX_REV;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_MAX_REV', 'sa_rule_head', O_rule_rev_no);
   fetch C_GET_MAX_REV into O_rule_rev_no;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_MAX_REV', 'sa_rule_head', O_rule_rev_no);
   close C_GET_MAX_REV;
   ---
   if O_rule_rev_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_RULE', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END GET_MAX_REV;
----------------------------------------------------------------------------------
FUNCTION GET_NO_ERRORS(O_error_message IN OUT   VARCHAR2,
                       O_no_errors     IN OUT   NUMBER,
                       I_rule_id       IN       SA_RULE_HEAD.RULE_ID%TYPE,
                       I_rule_rev_no   IN       SA_RULE_HEAD.RULE_REV_NO%TYPE)
   RETURN BOOLEAN IS

   L_program_name VARCHAR2(60) := 'SA_RULE_SQL.GET_NO_ERRORS';

   cursor C_GET_ERRORS is
      select count(rule_id)
        from sa_rule_errors
       where rule_id     = I_rule_id
    and rule_rev_no = I_rule_rev_no;
BEGIN
   ---
   if I_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ERRORS', 'sa_rule_errors', I_rule_id);
   open C_GET_ERRORS;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_ERRORS', 'sa_rule_errors', I_rule_id);
   fetch C_GET_ERRORS into O_no_errors;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ERRORS', 'sa_rule_errors', I_rule_id);
   close C_GET_ERRORS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NO_ERRORS;
-----------------------------------------------------------------------
FUNCTION GET_ERROR_DESC_CONCAT(O_error_message     IN OUT   VARCHAR2,
                               O_error_desc_concat IN OUT   VARCHAR2,
                               I_rule_id           IN       SA_RULE_HEAD.RULE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program_name       VARCHAR2(60) := 'SA_RULE_SQL.GET_ERROR_DESC_CONCAT';
   L_error_code         sa_rule_errors.error_code%TYPE;
   L_error_desc_concat  VARCHAR2(2000);
   L_error_desc         VARCHAR2(255);

   cursor C_GET_ERROR_CODE is
      select distinct(error_code) error_code
        from sa_rule_errors
       where rule_id = I_rule_id;
BEGIN
   ---
   if I_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   --Loop to get all error codes and associated descriptions
   FOR rec in C_GET_ERROR_CODE LOOP
      L_error_code := rec.error_code;
      ---
      if SA_ERROR_SQL.GET_ERROR_DESC(O_error_message,
                      L_error_desc,
                      L_error_code) = FALSE then
    return FALSE;
      end if;
      ---
      L_error_desc_concat := (L_error_desc_concat ||' ' || L_error_desc );
   END LOOP;
   ---
   O_error_desc_concat := RTRIM(SUBSTRB(L_error_desc_concat, 1, 2000));
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ERROR_DESC_CONCAT;
-----------------------------------------------------------------------
FUNCTION VALIDATE_REV_NO(O_ERROR_MESSAGE  IN OUT   VARCHAR2,
                         I_RULE_ID        IN       SA_RULE_HEAD.RULE_ID%TYPE,
                         I_RULE_REV_NO    IN       SA_RULE_HEAD.RULE_REV_NO%TYPE)
   RETURN BOOLEAN IS

   L_program_name VARCHAR2(60) := 'SA_RULE_SQL.VALIDATE_REV_NO';
   L_dummy              VARCHAR2(1) := NULL;

   cursor C_VAL is
      select 'X'
        from sa_rule_head
       where rule_id     = I_rule_id
         and rule_rev_no = I_rule_rev_no;
BEGIN
   ---
   if I_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_VAL', 'sa_rule_head', I_rule_id);
   open C_VAL;

   SQL_LIB.SET_MARK('FETCH', 'C_VAL', 'sa_rule_head', I_rule_id);
   fetch C_VAL into L_dummy;

   SQL_LIB.SET_MARK('CLOSE', 'C_VAL', 'sa_rule_head', I_rule_id);
   close C_VAL;
   ---
   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_RULE_REV_NO',
                                             to_char(I_rule_rev_no), I_rule_id,
                                             L_program_name);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_REV_NO;
-----------------------------------------------------------------------
FUNCTION VALIDATE_UPDATE_ID(O_ERROR_MESSAGE  IN OUT   VARCHAR2,
                            I_UPDATE_ID      IN       SA_RULE_HEAD.UPDATE_ID%TYPE,
                            I_RULE_ID        IN       SA_RULE_HEAD.RULE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program_name VARCHAR2(60) := 'SA_RULE_SQL.VALIDATE_UPDATE_ID';
   L_dummy              VARCHAR2(1) := NULL;

   cursor C_VAL is
      select 'X'
        from sa_rule_head
       where update_id = I_update_id
         and rule_id   = nvl(I_rule_id, rule_id);
BEGIN
   ---
   if I_update_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_VAL', 'sa_rule_head', I_update_id);
   open C_VAL;

   SQL_LIB.SET_MARK('FETCH', 'C_VAL', 'sa_rule_head', I_update_id);
   fetch C_VAL into L_dummy;

   SQL_LIB.SET_MARK('CLOSE', 'C_VAL', 'sa_rule_head', I_update_id);
   close C_VAL;
   ---
   if L_dummy is NULL then
      ---
      if I_rule_id is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('INVALID_UPDATE_ID',I_update_id , NULL, NULL);
      else
         O_error_message:= SQL_LIB.CREATE_MSG('INVALID_UPDATE_ID_RULE',I_update_id , I_rule_id, NULL);
      end if;
      ---
      RETURN FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_RULE_SQL.VALIDATE_UPDATE_ID',
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_UPDATE_ID;
-----------------------------------------------------------------------
FUNCTION VALID_MULTI_VERSIONS(O_ERROR_MESSAGE        IN OUT   VARCHAR2,
                              O_valid_multi_rev_ind  IN OUT   VARCHAR2,
                              I_rule_id              IN       SA_RULE_HEAD.RULE_ID%TYPE,
                              I_rule_rev_no          IN       SA_RULE_HEAD.RULE_REV_NO%TYPE,
                              I_start_date           IN       SA_RULE_HEAD.START_BUSINESS_DATE%TYPE,
                              I_end_date             IN       SA_RULE_HEAD.END_BUSINESS_DATE%TYPE)
   return BOOLEAN is
   ---
   L_program_name  VARCHAR2(60) := 'SA_RULE_SQL.VALID_MULTI_VERSIONS';
   L_function_name       VARCHAR2(30) := NULL;
   L_valid_multi_rev_ind VARCHAR2(1)  := 'Y';
   L_start_date          VARCHAR2(6);
   L_end_date            VARCHAR2(6);
   L_dates               VARCHAR2(14);
   ---
   cursor C_CHECK_TOTAL_DATE_RANGE is
      select to_char(start_business_date,'YYMMDD'),
             to_char(end_business_date,'YYMMDD')
        from sa_rule_head
       where rule_id      = I_rule_id
         and rule_rev_no != I_rule_rev_no
         and (   (start_business_date <= I_start_date and
                  end_business_date   >= I_start_date and
                  end_business_date   is NOT NULL )
              or (start_business_date <= I_start_date and
                  end_business_date   is NULL )
              or (start_business_date >= I_start_date and
                  I_end_date          is NULL )
              or (start_business_date >= I_start_date and
                  end_business_date   <= I_end_date and
                  end_business_date   is NOT NULL)
              or (start_business_date <= I_end_date and
                  end_business_date   >= I_end_date and
                  end_business_date   is NOT NULL )
              or (start_business_date <= I_end_date and
                  end_business_date   is NULL )
             );

   cursor C_CHECK_VALID_FUNCTION is
      select 'N'
        from all_objects
       where object_name = 'SA_R_' || upper(I_rule_id) || L_dates
         and object_type = 'FUNCTION'
         and lower(owner) = lower(sa_total_common.internal_schema_name)
         and status = 'VALID';

BEGIN
   if I_rule_id is NULL or
      I_rule_rev_no is NULL or
      I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   --- check for the total date range.
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_rule_head', NULL);
   open C_CHECK_TOTAL_DATE_RANGE;
      LOOP
         L_dates      := NULL;
         L_start_date := NULL;
         L_end_date   := NULL;
         SQL_LIB.SET_MARK('FETCH', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_rule_head', NULL);
         fetch C_CHECK_TOTAL_DATE_RANGE into L_start_date,
                                             L_end_date;
         EXIT WHEN C_CHECK_TOTAL_DATE_RANGE%NOTFOUND;
         BEGIN
            if L_end_date is NOT NULL then
               L_dates := '_' || L_start_date || '_' || L_end_date;
            else
               L_dates := '_' || L_start_date;
            end if;
            SQL_LIB.SET_MARK('OPEN', 'C_CHECK_VALID_FUNCTION', 'all_objects', NULL);
            open C_CHECK_VALID_FUNCTION;
            SQL_LIB.SET_MARK('FETCH', 'C_CHECK_VALID_FUNCTION', 'all_objects', NULL);
            fetch C_CHECK_VALID_FUNCTION into L_valid_multi_rev_ind;
            SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_VALID_FUNCTION', 'all_objects', NULL);
            close C_CHECK_VALID_FUNCTION;
         END;
      end LOOP;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_rule_head', NULL);
   close C_CHECK_TOTAL_DATE_RANGE;
   ---
   O_valid_multi_rev_ind := L_valid_multi_rev_ind;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_MULTI_VERSIONS;
-----------------------------------------------------------------------
FUNCTION DROP_INVALID_MULTI_VERSIONS(O_ERROR_MESSAGE   IN OUT   VARCHAR2,
                                     I_rule_id         IN       SA_RULE_HEAD.RULE_ID%TYPE,
                                     I_rule_rev_no     IN       SA_RULE_HEAD.RULE_REV_NO%TYPE,
                                     I_start_date      IN       SA_RULE_HEAD.START_BUSINESS_DATE%TYPE,
                                     I_end_date        IN       SA_RULE_HEAD.END_BUSINESS_DATE%TYPE)
        return BOOLEAN is
   ---
   L_program_name   VARCHAR2(60) := 'SA_RULE_SQL.DROP_INVALID_MULTI_VERSIONS';
   L_function_name  VARCHAR2(30) := NULL;
   L_rule_rev_no    SA_RULE_HEAD.RULE_REV_NO%TYPE;
   ---
   cursor C_GET_INVALID_VERSIONS is
      select rule_rev_no
        from sa_rule_head
       where rule_id      = I_rule_id
         and rule_rev_no != I_rule_rev_no
         and (   (start_business_date <= I_start_date and
                  end_business_date   >= I_start_date and
                  end_business_date   is NOT NULL )
              or (start_business_date <= I_start_date and
                  end_business_date   is NULL )
              or (start_business_date >= I_start_date and
                  I_end_date          is NULL )
              or (start_business_date >= I_start_date and
                  end_business_date   <= I_end_date and
                  end_business_date   is NOT NULL)
              or (start_business_date <= I_end_date and
                  end_business_date   >= I_end_date and
                  end_business_date   is NOT NULL )
              or (start_business_date <= I_end_date and
                  end_business_date   is NULL )
             );
BEGIN
   if I_rule_id is NULL or
      I_rule_rev_no is NULL or
      I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   --- check for the total date range.
   SQL_LIB.SET_MARK('OPEN', 'C_GET_INVALID_VERSIONS', 'sa_rule_head', NULL);
   open C_GET_INVALID_VERSIONS;
      LOOP
         SQL_LIB.SET_MARK('FETCH', 'C_GET_INVALID_VERSIONS', 'sa_rule_head', NULL);
         fetch C_GET_INVALID_VERSIONS into L_rule_rev_no;
         EXIT WHEN C_GET_INVALID_VERSIONS%NOTFOUND;
         BEGIN
            if SA_AUDIT_RULES_SQL.DROP_AUDIT_RULE(I_rule_id,
                                                  L_rule_rev_no,
                                                  O_error_message) != 0 then
               SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_rule_head', NULL);
               close C_GET_INVALID_VERSIONS;
               return FALSE;
            end if;
         END;
      end LOOP;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_TOTAL_DATE_RANGE', 'sa_rule_head', NULL);
   close C_GET_INVALID_VERSIONS;
   ---
return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END DROP_INVALID_MULTI_VERSIONS;
-----------------------------------------------------------------------
FUNCTION FUNCTION_STATUS(O_ERROR_MESSAGE        IN OUT   VARCHAR2,
                         O_valid_function_ind   IN OUT   VARCHAR2,
                         I_rule_id              IN       SA_RULE_HEAD.RULE_ID%TYPE,
                         I_rule_rev_no          IN       SA_RULE_HEAD.RULE_REV_NO%TYPE,
                         I_start_date           IN       SA_RULE_HEAD.START_BUSINESS_DATE%TYPE,
                         I_end_date             IN       SA_RULE_HEAD.END_BUSINESS_DATE%TYPE)
   return BOOLEAN is
   ---
   L_program_name  VARCHAR2(60) := 'SA_RULE_SQL.FUNCTION_STATUS';
   L_version_max_rev_no  SA_RULE_HEAD.RULE_REV_NO%TYPE;
   L_start_date          VARCHAR2(6)  := to_char(I_start_date,'YYMMDD');
   L_end_date            VARCHAR2(6)  := to_char(I_end_date,'YYMMDD');
   L_dates               VARCHAR2(14) := NULL;
   L_valid_function_ind  VARCHAR2(1)  := 'N';
   ---
   cursor C_CHECK_VERSION_MAX_REV is
      select max(rule_rev_no)
        from sa_rule_head
       where rule_id = I_rule_id
         and start_business_date = I_start_date
         and (
                (end_business_date is NULL and I_end_date is NULL)
                or
                (end_business_date = I_end_date and I_end_date is NOT NULL)
             );

   cursor C_CHECK_VALID_FUNCTION is
      select 'Y'
        from all_objects
       where object_name = 'SA_R_' || upper(I_rule_id) || L_dates
         and object_type = 'FUNCTION'
         and lower(owner) = lower(sa_total_common.internal_schema_name)
         and status = 'VALID';

BEGIN
   if I_rule_id is NULL or
      I_rule_rev_no is NULL or
      I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   O_valid_function_ind := 'N';
   if L_end_date is NOT NULL then
      L_dates := '_' || L_start_date || '_' || L_end_date;
   else
      L_dates := '_' || L_start_date;
   end if;
   ---
   --- check for a valid function with the passed-in date range.
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_CHECK_VALID_FUNCTION', 'ALL_OBJECTS', NULL);
   open C_CHECK_VALID_FUNCTION;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_VALID_FUNCTION', 'all_objects', NULL);
   fetch C_CHECK_VALID_FUNCTION into L_valid_function_ind;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_VALID_FUNCTION', 'all_objects', NULL);
   close C_CHECK_VALID_FUNCTION;
   ---
   if L_valid_function_ind = 'Y' then
      --- check if the valid function is for the passed-in revision.
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_VERSION_MAX_REV', 'SA_RULE_HEAD', NULL);
      open C_CHECK_VERSION_MAX_REV;
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_VERSION_MAX_REV', 'SA_RULE_HEAD', NULL);
      fetch C_CHECK_VERSION_MAX_REV into L_version_max_rev_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_VERSION_MAX_REV', 'SA_RULE_HEAD', NULL);
      close C_CHECK_VERSION_MAX_REV;
      ---
      if L_version_max_rev_no = I_rule_rev_no then
         O_valid_function_ind := 'Y';
      end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END FUNCTION_STATUS;
----------------------------------------------------------------------------------
END SA_RULE_SQL;
/
