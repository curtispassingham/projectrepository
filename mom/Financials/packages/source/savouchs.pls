
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_VOUCHER_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------
--- Function:  ISSUE_VOUCHER
--- Purpose:  Called from trandetl when a voucher is purchased.  The function updates
---           the issue fields on the SA_voucher table with the values on sa_tran_head.
---------------------------------------------------------------------------------------
FUNCTION ISSUE_VOUCHER(O_ERROR_MESSAGE        IN OUT VARCHAR2,
                       I_VOUCHER_NO           IN     SA_TRAN_ITEM.VOUCHER_NO%TYPE,
                       I_VOUCHER_TYPE         IN     SA_VOUCHER.TENDER_TYPE_ID%TYPE,
                       I_ISS_DATE             IN     SA_VOUCHER.ISS_DATE%TYPE,
                       I_ISS_STORE            IN     SA_VOUCHER.ISS_STORE%TYPE,
                       I_ISS_REGISTER         IN     SA_VOUCHER.ISS_REGISTER%TYPE,
                       I_ISS_CASHIER          IN     SA_VOUCHER.ISS_CASHIER%TYPE,
                       I_ISS_TRAN_SEQ_NO      IN     SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE,
		       I_ISS_TENDER_SEQ_NO    IN     SA_VOUCHER.ISS_TENDER_SEQ_NO%TYPE,
 		       I_ISS_ITEM_SEQ_NO      IN     SA_VOUCHER.ISS_ITEM_SEQ_NO%TYPE,
		       I_ISS_AMT	      IN     SA_VOUCHER.ISS_AMT%TYPE,
		       I_ISS_CUST_NAME        IN     SA_VOUCHER.ISS_CUST_NAME%TYPE,
 		       I_ISS_CUST_ADDR1       IN     SA_VOUCHER.ISS_CUST_ADDR1%TYPE,
   		       I_ISS_CUST_ADDR2       IN     SA_VOUCHER.ISS_CUST_ADDR2%TYPE,
		       I_ISS_CUST_CITY        IN     SA_VOUCHER.ISS_CUST_CITY%TYPE,
                       I_ISS_CUST_STATE       IN     SA_VOUCHER.ISS_CUST_STATE%TYPE,
                       I_ISS_CUST_POSTAL_CODE IN     SA_VOUCHER.ISS_CUST_POSTAL_CODE%TYPE,
		       I_ISS_CUST_COUNTRY     IN     SA_VOUCHER.ISS_CUST_COUNTRY%TYPE,
 		       I_RECIPIENT_NAME       IN     SA_VOUCHER.RECIPIENT_NAME%TYPE,
		       I_RECIPIENT_STATE      IN     SA_VOUCHER.RECIPIENT_STATE%TYPE,
		       I_RECIPIENT_COUNTRY    IN     SA_VOUCHER.RECIPIENT_COUNTRY%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function:   REDEEM_VOUCHER
--- Purpose:    Called from trandetl when a voucher is used as tender.  The function
---             inserts the redeemed fields on the sa_voucher table with the values
---             on sa_tran_head.
--------------------------------------------------------------------------------------
FUNCTION REDEEM_VOUCHER(O_ERROR_MESSAGE     IN OUT VARCHAR2,
                        I_VOUCHER_NO        IN     SA_TRAN_ITEM.VOUCHER_NO%TYPE,
                        I_VOUCHER_TYPE      IN     SA_VOUCHER.TENDER_TYPE_ID%TYPE,
                        I_RED_DATE          IN     SA_VOUCHER.RED_DATE%TYPE,
                        I_RED_STORE         IN     SA_VOUCHER.RED_STORE%TYPE,
                        I_RED_REGISTER      IN     SA_VOUCHER.RED_REGISTER%TYPE,
                        I_RED_CASHIER       IN     SA_VOUCHER.RED_CASHIER%TYPE,
                        I_RED_TRAN_SEQ_NO   IN     SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE,
			I_RED_TENDER_NO     IN     SA_VOUCHER.RED_TENDER_SEQ_NO%TYPE,
                        I_RED_AMT           IN     SA_VOUCHER.RED_AMT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function:   DELETE_VOUCHER
--- Purpose:    Called from trandetl when a voucher is deleted as an item or tender
---		from a transaction.
---------------------------------------------------------------------------------------
FUNCTION DELETE_VOUCHER(O_ERROR_MESSAGE     IN OUT VARCHAR2,
                        I_VOUCHER_NO        IN     SA_TRAN_ITEM.VOUCHER_NO%TYPE,
			I_DELETE_TYPE	    IN 	   VARCHAR2)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function:   UPDATE_TRANSACTION_INFO
--- Purpose:    Called to update all of the voucher records if a transaction contains
---		vouchers and the voucher records themselves are not changed, but the
---		transaction information changes.
---------------------------------------------------------------------------------------
FUNCTION UPDATE_TRANSACTION_INFO
		      (O_ERROR_MESSAGE        IN OUT VARCHAR2,
                       I_TRAN_SEQ_NO          IN     SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE,
                       I_TRAN_DATE            IN     SA_VOUCHER.RED_DATE%TYPE,
                       I_TRAN_STORE           IN     SA_VOUCHER.RED_STORE%TYPE,
                       I_TRAN_REGISTER        IN     SA_VOUCHER.RED_REGISTER%TYPE,
                       I_TRAN_CASHIER         IN     SA_VOUCHER.RED_CASHIER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function:   UPDATE_ISS_ITEM_INFO
--- Purpose:    Called to update detail level information for a given tran_seq_no and
---		iss_item_seq_no.
---------------------------------------------------------------------------------------
FUNCTION UPDATE_ISS_ITEM_INFO
			(O_ERROR_MESSAGE    IN OUT VARCHAR2,
                        I_ISS_TRAN_SEQ_NO   IN     SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE,
                        I_ISS_ITEM_SEQ_NO   IN     SA_VOUCHER.ISS_ITEM_SEQ_NO%TYPE,
                        I_VOUCHER_NO        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_ISS_AMOUNT        IN     SA_VOUCHER.ISS_AMT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function:   UPDATE_ISS_TENDER_INFO
--- Purpose:    Called to update detail level information for a given tran_seq_no and
---		iss_tender_seq_no.
---------------------------------------------------------------------------------------
FUNCTION UPDATE_ISS_TENDER_INFO
			(O_ERROR_MESSAGE    IN OUT VARCHAR2,
                        I_ISS_TRAN_SEQ_NO   IN     SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE,
                        I_ISS_TENDER_SEQ_NO IN     SA_VOUCHER.ISS_TENDER_SEQ_NO%TYPE,
                        I_VOUCHER_NO        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_ISS_AMOUNT        IN     SA_VOUCHER.ISS_AMT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function:   UPDATE_RED_TENDER_INFO
--- Purpose:    Called to update detail level information for a given tran_seq_no and
---		red_tender_seq_no.
---------------------------------------------------------------------------------------
FUNCTION UPDATE_RED_TENDER_INFO
			(O_ERROR_MESSAGE    IN OUT VARCHAR2,
                        I_RED_TRAN_SEQ_NO   IN     SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE,
                        I_RED_TENDER_SEQ_NO IN     SA_VOUCHER.RED_TENDER_SEQ_NO%TYPE,
                        I_VOUCHER_NO        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_RED_AMOUNT        IN     SA_VOUCHER.RED_AMT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function:   POST_VOID_VOUCHER
--- Purpose:    Called to undo the changes on the sa_voucher table when the
---             transaction has been post-voided.
---------------------------------------------------------------------------------------
FUNCTION POST_VOID_VOUCHER
			(O_ERROR_MESSAGE    IN OUT VARCHAR2,
                        I_TRAN_SEQ_NO       IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_VOUCHER_NO        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_STATUS            IN     SA_VOUCHER.STATUS%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function: POST_VOIDE_ONLINE_VOUCHER
--- Purpose:  Called on line to loop through records on a transaction and call 
---           SA_VOUCHER_SQL.POST_VOID_VOUCHER as necessary.
---------------------------------------------------------------------------------------
FUNCTION POST_VOID_ONLINE_VOUCHER(O_error_message  IN OUT   VARCHAR2,
                                  I_tran_seq_no    IN       sa_tran_head.tran_seq_no%TYPE,
                                  I_tran_type      IN       sa_tran_head.tran_type%TYPE,
                                  I_store          IN       sa_tran_head.store%TYPE,
                                  I_day            IN       sa_tran_head.day%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
END SA_VOUCHER_SQL;
/
