CREATE OR REPLACE PACKAGE TRANSACTION_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
--- Function:  TRAN_DATE
--- Purpose:  Validates to ensure that the transaction date is within allowable range.
---
FUNCTION TRAN_DATE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_valid_date     IN OUT BOOLEAN,
                   I_business_date  IN     SA_STORE_DAY.BUSINESS_DATE%TYPE,
                   I_vdate          IN     PERIOD.VDATE%TYPE,
                   I_day_post_sale  IN     SA_SYSTEM_OPTIONS.DAY_POST_SALE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--- Function: VOUCHER_STATUS
--- Purpose:  Returns the status of a voucher/gift certificate.  Valid values are: 'N'
---           for null status, meaning the voucher/gift certificate has neither been
---           assigned, issued, or redeemed.  'A' for meaning the voucher has been
---           assigned to a store.  'I' for issued, meaning the voucher was previously
---           issued to a customer.  'R' for redeemed, meaning the voucher was
---           previously issued and redeemed by a customer.
---
FUNCTION VOUCHER_STATUS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_status             IN OUT SA_VOUCHER.STATUS%TYPE,
                        O_ass_store          IN OUT SA_VOUCHER.ASS_STORE%TYPE,
                        O_iss_tran_seq_no    IN OUT SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE,
                        O_iss_item_seq_no    IN OUT SA_VOUCHER.ISS_ITEM_SEQ_NO%TYPE,
                        O_iss_tender_seq_no  IN OUT SA_VOUCHER.ISS_TENDER_SEQ_NO%TYPE,
                        O_red_tran_seq_no    IN OUT SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE,
                        O_red_tender_seq_no  IN OUT SA_VOUCHER.RED_TENDER_SEQ_NO%TYPE,
                        I_voucher_no         IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_voucher_type       IN     SA_VOUCHER.TENDER_TYPE_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--- Function: VOUCHER
--- Purpose : Returns a boolean true if a voucher has been issed, or false if the voucher
---           has not been issued.
---
FUNCTION VOUCHER(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_issued            IN OUT BOOLEAN,
                 O_issue_amt         IN OUT SA_VOUCHER.ISS_AMT%TYPE,
                 I_voucher_no        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                 I_voucher_type      IN     SA_VOUCHER.TENDER_TYPE_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function:  CHECK_DUP_TRANS
--Purpose:   Checks for duplicate transactions based on Transaction No, RTlog originating system
--           Store and Register(optional), returning TRUE if found.
---
FUNCTION CHECK_DUP_TRANS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tran_seq_no       IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_tran_no           IN       SA_TRAN_HEAD.TRAN_NO%TYPE,
                         I_store_day_seq_no  IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                         I_register          IN       SA_TRAN_HEAD.REGISTER%TYPE,
                         I_status            IN       SA_TRAN_HEAD.STATUS%TYPE,
                         O_exist_ind         IN OUT   BOOLEAN,
                         I_store             IN       SA_STORE_DAY.STORE%TYPE,
                         I_day               IN       SA_STORE_DAY.DAY%TYPE,
                         I_rtlog_orig_sys    IN       SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function:  POS_DECLARED_TOTAL
-- Purpose:   Verifies that the specified total ID set in ref_no1 is a valid
--            total ID. Also considering the balance level of the total, determines
--            if a duplicate total has already been declared.
--
FUNCTION POS_DECLARED_TOTAL
         (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
          O_valid_total      IN OUT  BOOLEAN,
          O_duplicate        IN OUT  BOOLEAN,
          I_tran_seq_no      IN      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
          I_store_day_seq_no IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
          I_register         IN      SA_TRAN_HEAD.REGISTER%TYPE,
          I_cashier          IN      SA_TRAN_HEAD.REGISTER%TYPE,
          I_total_id         IN      SA_TRAN_HEAD.REF_NO1%TYPE,
          I_store            IN      SA_STORE_DAY.STORE%TYPE,
          I_day              IN      SA_STORE_DAY.DAY%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
-- Function:  GET_PREV_VOID_ITEM_STATUS
-- Purpose:   Gets the most recent item status that is not a void or an error
--            for the specified transaction sequence and item sequenc numbers.
--            If no item status is found, then the function will set an appropriate
--            error message and return false.
--
FUNCTION GET_PREV_VOID_ITEM_STATUS
         (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
          O_prev_item_status IN OUT  SA_TRAN_ITEM.ITEM_STATUS%TYPE,
          I_tran_seq_no      IN      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
          I_item_seq_no      IN      SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
          I_store            IN      SA_STORE_DAY.STORE%TYPE,
          I_day              IN      SA_STORE_DAY.DAY%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
--Function:  TENDER_ENDING_AMT
--Purpose:   Validates if the Tender Amount has a valid ending.
--           Calls TRANSACTION_SQL.GET_ENDING_AMT and returns TRUE if the
--           ending amount is defined in SA_ROUNDING_RULES_DETAIL.round_amt
-------------------------------------------------------------------------------
FUNCTION TENDER_ENDING_AMT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid_ending       IN OUT   BOOLEAN,
                           I_tender_amt         IN       SA_TRAN_TENDER.TENDER_AMT%TYPE,
                           I_rounding_rule_id   IN       SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
--Function:  CHECK_DUP_IGTAX
--Purpose:   Validates if the I_igtax_code is duplicate for the item on the transaction
--           The IGTAX Code should be unique for a regular item being taxed.
--           For packitems the IGTAX Code and Component Item combination should be
--           unique.
-------------------------------------------------------------------------------
FUNCTION CHECK_DUP_IGTAX(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists             IN OUT  BOOLEAN,
                         I_store              IN      SA_STORE_DAY.STORE%TYPE,
                         I_day                IN      SA_STORE_DAY.DAY%TYPE,
                         I_tran_seq_no        IN      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_item_seq_no        IN      SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                         I_igtax_seq_no       IN      SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE,
                         I_igtax_code         IN      SA_TRAN_IGTAX.IGTAX_CODE%TYPE,
                         I_tax_authority      IN      SA_TRAN_IGTAX.TAX_AUTHORITY%TYPE,
                         I_component_item     IN      SA_TRAN_IGTAX.REF_NO21%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
END TRANSACTION_VALIDATE_SQL;
/
