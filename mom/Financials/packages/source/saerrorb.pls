
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_ERROR_SQL AS
----------------------------------------------------------------------
FUNCTION ERROR_SYSTEM_EXISTS(O_error_message  IN OUT  VARCHAR2,
                             O_exists         IN OUT  BOOLEAN,
                             I_system_code    IN      SA_ERROR_IMPACT.SYSTEM_CODE%TYPE,
                             I_error_code     IN      SA_ERROR_IMPACT.ERROR_CODE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_exists   VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'SA_ERROR_SQL.SYSTEM_EXISTS';

   cursor C_EXIST is
   select 'Y'
     from sa_error_impact
    where error_code  = I_error_code
           and system_code = NVL(I_system_code, system_code);

BEGIN
   if I_error_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      O_exists := FALSE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_EXIST','SA_ERROR_IMPACT','Error: '||I_error_code||
                                                          ' System: '||I_system_code);
      open C_EXIST;
      SQL_LIB.SET_MARK('FETCH','C_EXIST','SA_ERROR_IMPACT','Error: '||I_error_code||
                                                           ' System: '||I_system_code);
      fetch C_EXIST into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_EXIST','SA_ERROR_IMPACT','Error: '||I_error_code||
                                                           ' System: '||I_system_code);
      close C_EXIST;
      ---
      if L_exists = 'Y' then
    O_exists := TRUE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END ERROR_SYSTEM_EXISTS;
-------------------------------------------------------------------------------------------
FUNCTION ERROR_CODE_EXISTS(O_error_message  IN OUT  VARCHAR2,
                         O_exists         IN OUT  BOOLEAN,
                           I_error_code     IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_exists   VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'SA_ERROR_SQL.ERROR_CODE_EXISTS';

   cursor C_EXIST is
   select 'Y'
     from sa_error_codes
    where error_code = I_error_code;

BEGIN
   if I_error_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      O_exists := FALSE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_EXIST','SA_ERROR_CODES','Error: '||I_error_code);
      open C_EXIST;
      SQL_LIB.SET_MARK('FETCH','C_EXIST','SA_ERROR_CODES','Error: '||I_error_code);
      fetch C_EXIST into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_EXIST','SA_ERROR_CODES','Error: '||I_error_code);
      close C_EXIST;
      ---
      if L_exists = 'Y' then
    O_exists := TRUE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END ERROR_CODE_EXISTS;
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message  IN OUT  VARCHAR2,
                    O_delete         IN OUT  BOOLEAN,
                      I_error_code     IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_exists       VARCHAR2(1)  := 'N';
   L_rev_exists   VARCHAR2(1)  := 'N';
   L_rule_exists  VARCHAR2(1)  := 'N';
   L_program      VARCHAR2(64) := 'SA_ERROR_SQL.CHECK_DELETE';
   ---
   cursor C_EXIST_ERROR is
      select 'Y'
        from sa_error
       where error_code = I_error_code;
   ---
   cursor C_EXIST_REV is
      select 'Y'
        from sa_error_rev
       where error_code = I_error_code;
   ---
   cursor C_EXIST_RULE is
   select 'Y'
     from sa_rule_errors
    where error_code = I_error_code;

BEGIN
   if I_error_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      O_delete := TRUE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_EXIST_ERROR','SA_ERROR','Error: '||I_error_code);
      open C_EXIST_ERROR;
      SQL_LIB.SET_MARK('FETCH','C_EXIST_ERROR','SA_ERROR','Error: '||I_error_code);
      fetch C_EXIST_ERROR into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_EXIST_ERROR','SA_ERROR','Error: '||I_error_code);
      close C_EXIST_ERROR;
      ---
      if L_exists = 'Y' then
         O_delete := FALSE;
      else
         SQL_LIB.SET_MARK('OPEN','C_EXIST_REV','SA_ERROR_REV','Error: '||I_error_code);
         open C_EXIST_REV;
         SQL_LIB.SET_MARK('FETCH','C_EXIST_REV','SA_ERROR_REV','Error: '||I_error_code);
         fetch C_EXIST_REV into L_rev_exists;
         SQL_LIB.SET_MARK('CLOSE','C_EXIST_REV','SA_ERROR_REV','Error: '||I_error_code);
         close C_EXIST_REV;
         ---
         if L_rev_exists = 'Y' then
            O_delete := FALSE;
         else
            SQL_LIB.SET_MARK('OPEN','C_EXIST_RULE','SA_RULE_ERRORS','Error: '||I_error_code);
            open C_EXIST_RULE;
            SQL_LIB.SET_MARK('FETCH','C_EXIST_RULE','SA_RULE_ERRORS','Error: '||I_error_code);
            fetch C_EXIST_RULE into L_rule_exists;
            SQL_LIB.SET_MARK('CLOSE','C_EXIST_RULE','SA_RULE_ERRORS','Error: '||I_error_code);
            close C_EXIST_RULE;
            ---
            if L_rule_exists = 'Y' then
               O_delete := FALSE;
            end if;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_DELETE;
-------------------------------------------------------------------------------------------
FUNCTION LOCK_ERROR_IMPACT(O_error_message  IN OUT  VARCHAR2,
                           I_error_code     IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_table         VARCHAR2(30) := 'SA_ERROR_IMPACT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ERROR is
      select 'x'
        from sa_error_impact
       where error_code = I_error_code
         for update nowait;
BEGIN
   if I_error_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ERROR','SA_ERROR_IMPACT',I_error_code);
      open C_LOCK_ERROR;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ERROR','SA_ERROR_IMPACT',I_error_code);
      close C_LOCK_ERROR;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_error_code,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_ERROR_SQL.LOCK_ERROR_IMPACT',
                                            NULL);
      return FALSE;
END LOCK_ERROR_IMPACT;
-------------------------------------------------------------------------------------------
FUNCTION DELETE_ERROR_IMPACT(O_error_message  IN OUT  VARCHAR2,
                             I_error_code     IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN IS

BEGIN
   if I_error_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                   NULL,
                   NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('DELETE',NULL,'SA_ERROR_IMPACT',I_error_code);
      delete
        from sa_error_impact
       where error_code = I_error_code;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                     SQLERRM,
                     'SA_ERROR_SQL.DELETE_ERROR_IMPACT',
                     NULL);
   return FALSE;
END DELETE_ERROR_IMPACT;
--------------------------------------------------------------------------------------
FUNCTION GET_ERROR_INFO(O_error_message      IN OUT VARCHAR2,
                        O_error_desc         IN OUT SA_ERROR_CODES.ERROR_DESC%TYPE,
                        O_rec_solution       IN OUT SA_ERROR_CODES.REC_SOLUTION%TYPE,
                        O_orig_value         IN OUT SA_ERROR.ORIG_VALUE%TYPE,
                        O_store_override_ind IN OUT SA_ERROR_CODES.STORE_OVERRIDE_IND%TYPE,
                        O_hq_override_ind    IN OUT SA_ERROR_CODES.HQ_OVERRIDE_IND%TYPE,
                        O_target_form        IN OUT SA_ERROR_CODES.TARGET_FORM%TYPE,
                        O_target_tab         IN OUT SA_ERROR_CODES.TARGET_TAB%TYPE,
                        O_update_id          IN OUT SA_ERROR.UPDATE_ID%TYPE,
                        O_update_datetime    IN OUT SA_ERROR.UPDATE_DATETIME%TYPE,
                        O_orig_cc_no         IN OUT SA_ERROR.ORIG_CC_NO%TYPE,
                        IO_error_code        IN OUT SA_ERROR.ERROR_CODE%TYPE,
                        I_error_seq_no       IN     SA_ERROR.ERROR_SEQ_NO%TYPE,
                        I_tran_seq_no        IN     SA_ERROR.TRAN_SEQ_NO%TYPE,
                        I_total_seq_no       IN     SA_ERROR.TOTAL_SEQ_NO%TYPE,
                        I_key_value_1        IN     SA_ERROR.KEY_VALUE_1%TYPE,
                        I_key_value_2        IN     SA_ERROR.KEY_VALUE_2%TYPE,
                        I_rec_type           IN     SA_ERROR.REC_TYPE%TYPE,
                        I_store              IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                        I_day                IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(60)             := 'SA_ERROR_SQL.GET_ERROR_INFO';
   L_error_code  SA_ERROR.ERROR_CODE%TYPE;
   L_store   SA_ERROR.STORE%TYPE := I_store;
   L_day     SA_ERROR.DAY%TYPE := I_day;

   cursor C_GET_ERROR_CODE_INFO is
      select vec.error_desc,
             vec.rec_solution,
             ec.store_override_ind,
             ec.hq_override_ind,
             ec.target_form,
             ec.target_tab
        from v_sa_error_codes_tl vec,
             sa_error_codes ec
       where vec.error_code = IO_error_code
         and vec.error_code = ec.error_code;

   cursor C_GET_ERROR_INFO is
      select e.error_code,
             vec.error_desc,
             vec.rec_solution,
             ec.store_override_ind,
             ec.hq_override_ind,
             ec.target_form,
             ec.target_tab,
             e.orig_value,
             e.update_id,
             e.update_datetime,
             e.orig_cc_no
        from sa_error_codes ec,
             v_sa_error_all e,
             v_sa_error_codes_tl vec
       where vec.error_code = ec.error_code
         and ec.error_code  = e.error_code
         and (e.store      = L_store)
         and (e.day        = L_day)
         and (  (tran_seq_no       = I_tran_seq_no
                 and tran_seq_no   is not NULL
                 and I_tran_seq_no is not NULL
                 and error_seq_no  = I_error_seq_no)
             or (total_seq_no       = I_total_seq_no
                 and total_seq_no   is not NULL
                 and I_total_seq_no is not NULL
                 and error_seq_no   = I_error_seq_no))
         and ((key_value_1        = I_key_value_1
               and key_value_1    is not NULL
               and I_key_value_1  is not NULL)
              or (I_key_value_1   is NULL
                  and key_value_1 is NULL))
         and ((key_value_2         = I_key_value_2
               and key_value_2    is not NULL
               and I_key_value_2  is not NULL)
              or (I_key_value_2   is NULL
                  and key_value_2 is NULL))
         and rec_type              = I_rec_type
         and rownum = 1;

BEGIN
   if IO_error_code is NULL then
      if (I_tran_seq_no is NULL and
          I_total_seq_no is NULL) or
          I_rec_type is NULL then
          O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           I_error_seq_no,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      
      SQL_LIB.SET_MARK('OPEN','C_GET_ERROR_INFO','SA_ERROR_CODES, V_SA_ERROR_ALL',NULL);
      open C_GET_ERROR_INFO;
      SQL_LIB.SET_MARK('FETCH','C_GET_ERROR_INFO','SA_ERROR_CODES, V_SA_ERROR_ALL',NULL);
      fetch C_GET_ERROR_INFO into L_error_code,  --IO_error_code,
                                  O_error_desc,
                                  O_rec_solution,
                                  O_store_override_ind,
                                  O_hq_override_ind,
                                  O_target_form,
                                  O_target_tab,
                                  O_orig_value,
                                  O_update_id,
                                  O_update_datetime,
                                  O_orig_cc_no;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ERROR_INFO','SA_ERROR_CODES, V_SA_ERROR_ALL',NULL);
      close C_GET_ERROR_INFO;
      ---
      IO_error_code := L_error_code;
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_ERROR_CODE_INFO','SA_ERROR_CODES',NULL);
      open C_GET_ERROR_CODE_INFO;
      SQL_LIB.SET_MARK('FETCH','C_GET_ERROR_CODE_INFO','SA_ERROR_CODES',NULL);
      fetch C_GET_ERROR_CODE_INFO into O_error_desc,
                                       O_rec_solution,
                                       O_store_override_ind,
                                       O_hq_override_ind,
                                       O_target_form,
                                       O_target_tab;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ERROR_CODE_INFO','SA_ERROR_CODES',NULL);
      close C_GET_ERROR_CODE_INFO;
   end if;
   
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END GET_ERROR_INFO;
----------------------------------------------------------------------------------------
FUNCTION GET_ERROR_DESC(O_error_message IN OUT  VARCHAR2,
                        O_error_desc    IN OUT  SA_ERROR_CODES.ERROR_DESC%TYPE,
                        I_error_code    IN      SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50)    := 'SA_ERROR_SQL.GET_ERROR_DESC';
   ---
   cursor C_GET_ERROR_DESC is
      select error_desc
        from v_sa_error_codes_tl
       where error_code = I_error_code;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_ERROR_DESC','V_SA_ERROR_CODES_TL','error_code: '||I_error_code);
   open  C_GET_ERROR_DESC;
   SQL_LIB.SET_MARK('FETCH','C_GET_ERROR_DESC','V_SA_ERROR_CODES_TL','error_code: '||I_error_code);
   fetch C_GET_ERROR_DESC into O_error_desc;
   ---
   if C_GET_ERROR_DESC%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ERROR_CODE',
                                            NULL,
                                            NULL,
                                            NULL);
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_ERROR_DESC','V_SA_ERROR_CODES_TL','error_code: '||I_error_code);
      close C_GET_ERROR_DESC;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_ERROR_DESC','V_SA_ERROR_CODES_TL','error_code: '||I_error_code);
   close C_GET_ERROR_DESC;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ERROR_DESC;
------------------------------------------------------------------------------------------
FUNCTION COUNT_ERRORS(O_error_message    IN OUT VARCHAR2,
                      O_no_of_errors     IN OUT NUMBER,
                      I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                      I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(60)  := 'SA_ERROR_SQL.COUNT_ERRORS';
   L_store   SA_ERROR.STORE%TYPE := I_store;
   L_day     SA_ERROR.DAY%TYPE := I_day;

   cursor C_COUNT_ERRORS is
      select count(*)
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day;

BEGIN
   if I_tran_seq_no is not NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_no_of_errors := 0;
      ---
      SQL_LIB.SET_MARK('OPEN','C_COUNT_ERRORS','SA_ERROR','tran_seq_no: '||to_char(I_tran_seq_no));
      open C_COUNT_ERRORS;
      SQL_LIB.SET_MARK('FETCH','C_COUNT_ERRORS','SA_ERROR','tran_seq_no: '||to_char(I_tran_seq_no));
      fetch C_COUNT_ERRORS into O_no_of_errors;
      SQL_LIB.SET_MARK('CLOSE','C_COUNT_ERRORS','SA_ERROR','tran_seq_no: '||to_char(I_tran_seq_no));
      close C_COUNT_ERRORS;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END COUNT_ERRORS;
---------------------------------------------------------------------------------------
FUNCTION ERRORS_EXIST(O_error_message    IN OUT VARCHAR2,
                      O_exists           IN OUT BOOLEAN,
                      I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                      I_total_seq_no     IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                      I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_key_value_1      IN     SA_ERROR.KEY_VALUE_1%TYPE,
                      I_key_value_2      IN     SA_ERROR.KEY_VALUE_2%TYPE,
                      I_rec_type         IN     SA_ERROR.REC_TYPE%TYPE,
                      I_store_day_seq_no IN     SA_TOTAL.STORE_DAY_SEQ_NO%TYPE,
                      I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                      I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN is

   L_program  VARCHAR2(60) := 'SA_ERROR_SQL.ERRORS_EXIST';
   L_exists   VARCHAR2(1)  := 'N';
   L_store    SA_ERROR.STORE%TYPE := I_store;
   L_day      SA_ERROR.DAY%TYPE   := I_day;

   cursor C_CHECK_ERROR is
      select 'Y'
        from sa_error
       where  store        = L_store
              and day        = L_day
              and (store_day_seq_no = I_store_day_seq_no
                    and ((I_tran_seq_no is not NULL
                    and tran_seq_no is not NULL
                    and tran_seq_no = I_tran_seq_no
                    and total_seq_no is NULL
                    and I_total_seq_no is NULL)
                  or
                    (I_total_seq_no is not NULL
                     and total_seq_no is not NULL
                     and total_seq_no = I_total_seq_no
                     and tran_seq_no is NULL
                     and I_tran_seq_no is NULL)
                  or
                    (total_seq_no is NULL
                     and I_total_seq_no is NULL
                     and tran_seq_no is NULL
                     and I_tran_seq_no is NULL)))
         and error_code = nvl(I_error_code,error_code)
         and (   (key_value_1 = nvl(I_key_value_1, key_value_1) and key_value_1 is NOT NULL)
              or (I_key_value_1 is NULL and key_value_1 is NULL))
         and (   (key_value_2 = nvl(I_key_value_2, key_value_2) and key_value_1 is NOT NULL)
              or (I_key_value_2 is NULL and key_value_2 is NULL))
         and rec_type = nvl(I_rec_type, rec_type);

BEGIN
   /*if ((I_tran_seq_no is not NULL and I_total_seq_no is NULL) or
      (I_total_seq_no is not NULL and I_tran_seq_no is NULL) then
      O_exists := FALSE;*/
   if ((I_store_day_seq_no is not NULL and I_total_seq_no is NULL and I_tran_seq_no is not NULL) or
       (I_store_day_seq_no is not NULL and I_tran_seq_no is NULL and I_total_seq_no is not NULL) or
       (I_store_day_seq_no is not NULL and I_tran_seq_no is NULL and I_total_seq_no is NULL)) then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_exists := FALSE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_ERROR','SA_ERROR',NULL);
      open C_CHECK_ERROR;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_ERROR','SA_ERROR',NULL);
      fetch C_CHECK_ERROR into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_ERROR','SA_ERROR',NULL);
      close C_CHECK_ERROR;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ERRORS_EXIST;
---------------------------------------------------------------------------------------
FUNCTION ERROR_EXISTS_ON_FORM(O_error_message    IN OUT VARCHAR2,
                              O_exists           IN OUT VARCHAR2,
                              I_store_day_seq_no IN     SA_TOTAL.STORE_DAY_SEQ_NO%TYPE,
                              I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_bal_group_seq_no IN     SA_ERROR.BAL_GROUP_SEQ_NO%TYPE,
                              I_total_type       IN     SA_TOTAL_HEAD.TOTAL_TYPE%TYPE,
                              I_rev_table        IN     VARCHAR2,
                              I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                              I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN is
   ---
   L_program          VARCHAR2(50)              := 'SA_ERROR_SQL.ERROR_EXISTS_ON_FORM';
   L_exists           VARCHAR2(1)               := 'N';
   L_store            SA_ERROR.STORE%TYPE := I_store;
   L_day              SA_ERROR.DAY%TYPE   := I_day;
   ---

   cursor C_CHECK_TOTAL_ERRORS is
      select 'Y'
        from v_sa_error_all e
       where e.store = L_store
         and e.day   = L_day
         and e.store_day_seq_no = I_store_day_seq_no
         and e.total_seq_no in (select t.total_seq_no
                                  from sa_total t, sa_total_head h
                                 where t.total_id = h.total_id
                                   and t.store = e.store
                                   and t.day = e.day
                                   and h.total_type = I_total_type);

   cursor C_CHECK_BAL_TOTAL_ERRORS is
      select 'Y'
        from v_sa_error_all e
       where e.store = L_store
         and e.day   = L_day
         and e.store_day_seq_no = I_store_day_seq_no
         and e.bal_group_seq_no = I_bal_group_seq_no
         and e.total_seq_no in (select t.total_seq_no
                                  from sa_total t, sa_total_head h
                                 where t.total_id = h.total_id
                                   and t.store = e.store
                                   and t.day = e.day
                                   and h.total_type = I_total_type);

   cursor C_CHECK_TOTAL_REV_ERRORS is
      select 'Y'
        from sa_error_rev e
       where e.store = L_store
         and e.day   = L_day
         and e.store_day_seq_no = I_store_day_seq_no
         and e.total_seq_no in (select t.total_seq_no
                                  from sa_total t, sa_total_head h
                                 where t.total_id = h.total_id
                                   and t.store = e.store
                                   and t.day = e.day
                                   and h.total_type = I_total_type);

   cursor C_CHECK_BAL_TOTAL_REV_ERRORS is
      select 'Y'
        from sa_error_rev e
       where e.store = L_store
         and e.day = L_day
         and e.store_day_seq_no = I_store_day_seq_no
         and e.bal_group_seq_no = I_bal_group_seq_no
         and e.total_seq_no in (select t.total_seq_no
                                  from sa_total t, sa_total_head h
                                 where t.total_id = h.total_id
                                   and t.store = e.store
                                   and t.day = e.day
                                   and h.total_type = I_total_type);

   cursor C_CHECK_TRAN_ERRORS is
      select 'Y'
        from sa_error
       where store       = L_store
         and day         = L_day
         and tran_seq_no = I_tran_seq_no;

   cursor C_CHECK_TRAN_REV_ERRORS is
      select 'Y'
        from sa_error_rev
       where store       = L_store
         and day         = L_day
         and tran_seq_no = I_tran_seq_no;

BEGIN
   if (I_store_day_seq_no is NULL or I_total_type is NULL) and I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_rev_table = 'N' then --- Not using the revisions table
      if I_tran_seq_no is NOT NULL then --- Check for transaction errors
         SQL_LIB.SET_MARK('OPEN','C_CHECK_TRAN_ERRORS','SA_ERROR',NULL);
         open C_CHECK_TRAN_ERRORS;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_TRAN_ERRORS','SA_ERROR',NULL);
         fetch C_CHECK_TRAN_ERRORS into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_TRAN_ERRORS','SA_ERROR',NULL);
         close C_CHECK_TRAN_ERRORS;
         ---
      else --- Check for total errors
         if I_bal_group_seq_no is NOT NULL then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_BAL_TOTAL_ERRORS','SA_ERROR',NULL);
            open C_CHECK_BAL_TOTAL_ERRORS;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_BAL_TOTAL_ERRORS','SA_ERROR',NULL);
            fetch C_CHECK_BAL_TOTAL_ERRORS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_BAL_TOTAL_ERRORS','SA_ERROR',NULL);
            close C_CHECK_BAL_TOTAL_ERRORS;
         else
            SQL_LIB.SET_MARK('OPEN','C_CHECK_TOTAL_ERRORS','SA_ERROR',NULL);
            open C_CHECK_TOTAL_ERRORS;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_TOTAL_ERRORS','SA_ERROR',NULL);
            fetch C_CHECK_TOTAL_ERRORS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_TOTAL_ERRORS','SA_ERROR',NULL);
            close C_CHECK_TOTAL_ERRORS;
         end if;
      end if;
   else --- Using the revisions table
      if I_tran_seq_no is NOT NULL then --- Check for transaction errors
         SQL_LIB.SET_MARK('OPEN','C_CHECK_TRAN_REV_ERRORS','SA_ERROR',NULL);
         open C_CHECK_TRAN_REV_ERRORS;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_TRAN_REV_ERRORS','SA_ERROR',NULL);
         fetch C_CHECK_TRAN_REV_ERRORS into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_TRAN_REV_ERRORS','SA_ERROR',NULL);
         close C_CHECK_TRAN_REV_ERRORS;
         ---
      else --- Check for total errors
         if I_bal_group_seq_no is NOT NULL then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_BAL_TOTAL_REV_ERRORS','SA_ERROR',NULL);
            open C_CHECK_BAL_TOTAL_REV_ERRORS;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_BAL_TOTAL_REV_ERRORS','SA_ERROR',NULL);
            fetch C_CHECK_BAL_TOTAL_REV_ERRORS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_BAL_TOTAL_REV_ERRORS','SA_ERROR',NULL);
            close C_CHECK_BAL_TOTAL_REV_ERRORS;
         else
            SQL_LIB.SET_MARK('OPEN','C_CHECK_TOTAL_REV_ERRORS','SA_ERROR',NULL);
            open C_CHECK_TOTAL_REV_ERRORS;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_TOTAL_REV_ERRORS','SA_ERROR',NULL);
            fetch C_CHECK_TOTAL_REV_ERRORS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_TOTAL_REV_ERRORS','SA_ERROR',NULL);
            close C_CHECK_TOTAL_REV_ERRORS;
         end if;
      end if;
   end if;
   ---
   O_exists := L_exists;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ERROR_EXISTS_ON_FORM;
---------------------------------------------------------------------------------------
FUNCTION INSERT_ERROR(O_error_message    IN OUT VARCHAR2,
                      I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                      I_bal_group_seq_no IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                      I_total_seq_no     IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                      I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                      I_key_value_1      IN     SA_ERROR.KEY_VALUE_1%TYPE,
                      I_key_value_2      IN     SA_ERROR.KEY_VALUE_2%TYPE,
                      I_rec_type         IN     SA_ERROR.REC_TYPE%TYPE,
                      I_update_id        IN     SA_ERROR.UPDATE_ID%TYPE,
                      I_update_datetime  IN     SA_ERROR.UPDATE_DATETIME%TYPE,
                      I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                      I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN is

   L_program          VARCHAR2(50)    := 'SA_ERROR_SQL.INSERT_ERROR';
   L_exists           BOOLEAN         := FALSE;
   L_seq_value        SA_ERROR.ERROR_SEQ_NO%TYPE;
   L_store            SA_ERROR.STORE%TYPE := I_store;
   L_day              SA_ERROR.DAY%TYPE   := I_day;

BEGIN

   if I_store_day_seq_no is NOT NULL and
      I_error_code is NOT NULL and
      I_rec_type is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if ERRORS_EXIST(O_error_message,
                      L_exists,
                      I_error_code,
                      I_total_seq_no,
                      I_tran_seq_no,
                      I_key_value_1,
                      I_key_value_2,
                      I_rec_type,
                      I_store_day_seq_no,
                      L_store,
                      L_day) = FALSE then
         return FALSE;
      end if;
      ---

      if L_exists != TRUE then
         if SA_SEQUENCE2_SQL.GET_ERROR_SEQ(O_error_message,
                                           L_seq_value) = FALSE then
            return FALSE;
         end if;
         ---
         insert into sa_error(store,
                              day,
                              error_seq_no,
                              store_day_seq_no,
                              bal_group_seq_no,
                              total_seq_no,
                              tran_seq_no,
                              error_code,
                              key_value_1,
                              key_value_2,
                              rec_type,
                              store_override_ind,
                              hq_override_ind,
                              update_id,
                              update_datetime)
                       values(L_store,
                              L_day,
                              L_seq_value,
                              I_store_day_seq_no,
                              I_bal_group_seq_no,
                              I_total_seq_no,
                              I_tran_seq_no,
                              I_error_code,
                              I_key_value_1,
                              I_key_value_2,
                              I_rec_type,
                              'N',
                              'N',
                              I_update_id,
                              I_update_datetime);
      end if;
   else
      O_error_message := sql_lib.create_msg('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_ERROR;
------------------------------------------------------------------------------------------
FUNCTION DELETE_ERRORS(O_error_message    IN OUT VARCHAR2,
                       I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                       I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                       I_key_value_1      IN     SA_ERROR.KEY_VALUE_1%TYPE,
                       I_key_value_2      IN     SA_ERROR.KEY_VALUE_2%TYPE,
                       I_rec_type         IN     SA_ERROR.REC_TYPE%TYPE,
                       I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                       I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN is

   L_program    VARCHAR2(60) := 'SA_ERROR_SQL.DELETE_ERRORS';
   L_store      SA_ERROR.STORE%TYPE := I_store;
   L_day        SA_ERROR.DAY%TYPE   := I_day;

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_error
       where store = L_store
         and day = L_day
         and store_day_seq_no = I_store_day_seq_no
         and tran_seq_no = I_tran_seq_no
         and ((error_code = I_error_code and
              I_error_code is NOT NULL)
          or I_error_code is NULL)
         and ((key_value_1 = I_key_value_1 and
              I_key_value_1 is NOT NULL)
          or I_key_value_1 is NULL and
             key_value_1 is NULL)
         and ((key_value_2 = I_key_value_2 and
              I_key_value_2 is NOT NULL)
          or I_key_value_2 is NULL and
             key_value_2 is NULL)
         and rec_type = I_rec_type
      for update nowait;

BEGIN

   if I_store_day_seq_no is NOT NULL and
      I_tran_seq_no is NOT NULL and
      I_rec_type is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      delete from sa_error
       where store = L_store
         and day = L_day
         and store_day_seq_no = I_store_day_seq_no
         and tran_seq_no = I_tran_seq_no
         and ((error_code = I_error_code and
              I_error_code is NOT NULL)
          or I_error_code is NULL)
         and ((key_value_1 = I_key_value_1 and
              I_key_value_1 is NOT NULL)
          or I_key_value_1 is NULL and
             key_value_1 is NULL)
         and ((key_value_2 = I_key_value_2 and
              I_key_value_2 is NOT NULL)
          or I_key_value_2 is NULL and
             key_value_2 is NULL)
         and rec_type = I_rec_type;
   else
      O_error_message := sql_lib.create_msg('INV_INPUT_GENERIC',
                                             null,
                                             null,
                                             null);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ERRORS;
----------------------------------------------------------------------
FUNCTION INVALID_CHAR_SEQ(O_error_message  IN OUT  VARCHAR2,
                          O_exists         IN OUT  BOOLEAN,
                          I_error_code    IN    SA_ERROR_CODES.ERROR_CODE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_exists   VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'SA_ERROR_SQL.INVALID_CHAR_SEQ';

   cursor C_EXIST is
      select 'Y'
        from sa_error_codes s
       where I_error_code = s.error_code
         and (s.error_desc   like '%*/%' or
              s.error_desc   like '%/*%' or
              s.rec_solution like '%*/%' or
              s.rec_solution like '%/*%');

BEGIN
   if I_error_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN','C_EXIST','SA_ERROR_CODES',NULL);
      open C_EXIST;
      SQL_LIB.SET_MARK('FETCH','C_EXIST','SA_ERROR_CODES',NULL);
      fetch C_EXIST into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_EXIST','SA_ERROR_CODES',NULL);
      close C_EXIST;
      ---
      if L_exists = 'Y' then
    O_exists := TRUE;
      else
    O_exists := FALSE;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END INVALID_CHAR_SEQ;
-----------------------------------------------------------------------------------
FUNCTION UPDATE_STORE_OVERRIDE(O_error_message      IN OUT VARCHAR2,
                               I_error_seq_no       IN     SA_ERROR.ERROR_SEQ_NO%TYPE,
                               I_store_override_ind IN     SA_ERROR.STORE_OVERRIDE_IND%TYPE,
                               I_store              IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                               I_day                IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(60)          := 'SA_ERROR_SQL.UPDATE_STORE_OVERRIDE';
   L_store      SA_ERROR.STORE%TYPE := I_store;
   L_day        SA_ERROR.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        I_error_seq_no,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   update sa_error
      set store_override_ind = I_store_override_ind,
          update_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER),
          update_datetime = SYSDATE
      where error_seq_no = I_error_seq_no
        and store = L_store
        and day = L_day;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_STORE_OVERRIDE;
-----------------------------------------------------------------------------------
FUNCTION UPDATE_HQ_OVERRIDE(O_error_message   IN OUT VARCHAR2,
                            I_error_seq_no    IN     SA_ERROR.ERROR_SEQ_NO%TYPE,
                            I_hq_override_ind IN     SA_ERROR.HQ_OVERRIDE_IND%TYPE,
                            I_store           IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                            I_day             IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'SA_ERROR_SQL.UPDATE_HQ_OVERRIDE';
   L_store      SA_ERROR.STORE%TYPE := I_store;
   L_day        SA_ERROR.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        I_error_seq_no,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   update sa_error
      set hq_override_ind = I_hq_override_ind,
          update_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER),
          update_datetime = SYSDATE
      where error_seq_no = I_error_seq_no
        and store = L_store
        and day = L_day;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_HQ_OVERRIDE;
-------------------------------------------------------------------------------------------
FUNCTION REV_ERRORS_EXIST(O_error_message    IN OUT VARCHAR2,
                          O_exists           IN OUT BOOLEAN,
                          I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                          I_total_seq_no     IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
                          I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_key_value_1      IN     SA_ERROR_REV.KEY_VALUE_1%TYPE,
                          I_key_value_2      IN     SA_ERROR_REV.KEY_VALUE_2%TYPE,
                          I_rec_type         IN     SA_ERROR_REV.REC_TYPE%TYPE,
                          I_store_day_seq_no IN     SA_TOTAL.STORE_DAY_SEQ_NO%TYPE,
                          I_rev_no           IN     SA_ERROR_REV.REV_NO%TYPE,
                          I_tran_rev_no      IN     SA_ERROR_REV.TRAN_REV_NO%TYPE,
                          I_store            IN     SA_ERROR.STORE%TYPE DEFAULT NULL,
                          I_day              IN     SA_ERROR.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN is

   L_program    VARCHAR2(60)        := 'SA_ERROR_SQL.REV_ERRORS_EXIST';
   L_exists     VARCHAR2(1)         := 'N';
   L_store      SA_ERROR.STORE%TYPE := I_store;
   L_day        SA_ERROR.DAY%TYPE   := I_day;

   cursor C_CHECK_ERROR is
      select 'Y'
        from sa_error_rev
       where (store_day_seq_no = I_store_day_seq_no
              and ((I_tran_seq_no is not NULL
                    and tran_seq_no is not NULL
                    and tran_seq_no = I_tran_seq_no
                    and total_seq_no is NULL
                    and I_total_seq_no is NULL)
                  or
                    (I_total_seq_no is not NULL
                     and total_seq_no is not NULL
                     and total_seq_no = I_total_seq_no
                     and tran_seq_no is NULL
                     and I_tran_seq_no is NULL)
                  or
                    (total_seq_no is NULL
                     and I_total_seq_no is NULL
                     and tran_seq_no is NULL
                     and I_tran_seq_no is NULL)))
         and store = L_store
         and day   = L_day
         and error_code = nvl(I_error_code,error_code)
         and (   (key_value_1 = nvl(I_key_value_1, key_value_1) and key_value_1 is NOT NULL)
              or (I_key_value_1 is NULL and key_value_1 is NULL))
         and (   (key_value_2 = nvl(I_key_value_2, key_value_2) and key_value_1 is NOT NULL)
              or (I_key_value_2 is NULL and key_value_2 is NULL))
         and rec_type    = nvl(I_rec_type, rec_type)
         and rev_no      = nvl(I_rev_no, rev_no)
         and tran_rev_no = nvl(I_tran_rev_no, tran_rev_no);

BEGIN
   if ((I_store_day_seq_no is not NULL and I_total_seq_no is NULL and I_tran_seq_no is not NULL) or
       (I_store_day_seq_no is not NULL and I_tran_seq_no is NULL and I_total_seq_no is not NULL) or
       (I_store_day_seq_no is not NULL and I_tran_seq_no is NULL and I_total_seq_no is NULL)) then
      O_exists := FALSE;
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_ERROR','SA_ERROR',NULL);
      open C_CHECK_ERROR;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_ERROR','SA_ERROR',NULL);
      fetch C_CHECK_ERROR into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_ERROR','SA_ERROR',NULL);
      close C_CHECK_ERROR;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END REV_ERRORS_EXIST;
-------------------------------------------------------------------------------------------
FUNCTION DELETE_NONTRAN_ERROR(O_error_message    IN OUT VARCHAR2,
                              I_error_code       IN     SA_ERROR_CODES.ERROR_CODE%TYPE,
                              I_error_seq_no     IN     SA_ERROR.ERROR_SEQ_NO%TYPE,
                              I_store            IN     SA_ERROR.STORE%TYPE,
                              I_day              IN     SA_ERROR.DAY%TYPE,
                              I_rec_type         IN     SA_ERROR.REC_TYPE%TYPE)
   RETURN BOOLEAN is

   L_program    VARCHAR2(60) := 'SA_ERROR_SQL.DELETE_NONTRAN_ERRORS';
   L_store      SA_ERROR.STORE%TYPE := I_store;
   L_day        SA_ERROR.DAY%TYPE   := I_day;

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_error
       where error_Code = I_error_code
         and rec_type = I_rec_type
         and day = I_day
         and store = I_store
         and error_seq_no = I_error_seq_no
      for update nowait;

BEGIN

   if I_error_seq_no is NOT NULL and
      I_store        is NOT NULL and
      I_day          is NOT NULL then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECORD',
                       'sa_error',
                       'STORE '||I_store);            
      open C_LOCK_RECORD;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECORD',
                       'sa_error',
                       'STORE '||I_store);            
      close C_LOCK_RECORD;
      ---
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'sa_error',
                       'STORE '||I_store);            
      delete from sa_error
       where error_code = I_error_code
         and rec_type = I_rec_type
         and day = I_day
         and store = I_store
         and error_seq_no = I_error_seq_no;
   else
      O_error_message := sql_lib.create_msg('INV_NULL_INPUT_VAR',
                                             null,
                                             null,
                                             null);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_NONTRAN_ERROR;
--------------------------------------------------------------------------------------------
END SA_ERROR_SQL;
/
