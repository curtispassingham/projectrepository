
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY MISSING_TRAN_SQL AS
--------------------------------------------------------------------------------------
FUNCTION GET_NEW_MISS_TRAN_SEQ (O_error_message      IN OUT VARCHAR2,
                                O_new_miss_tran_seq  IN OUT SA_MISSING_TRAN.MISS_TRAN_SEQ_NO%TYPE)
   RETURN BOOLEAN IS 

   L_new_seq         SA_MISSING_TRAN.MISS_TRAN_SEQ_NO%TYPE;
   L_program         VARCHAR2(50)                   := 'MISSING_TRAN_SQL .GET_NEW_MISS_TRAN_SEQ';
   L_first_time      VARCHAR2(1)                    := 'Y';
   L_exists          VARCHAR2(1)                    := 'N';
   L_wrap_number     SA_MISSING_TRAN.MISS_TRAN_SEQ_NO%TYPE;

   cursor C_GET_NEXT is
      select sa_miss_tran_sequence.NEXTVAL
        from dual;

   cursor C_CHECK_SEQ is
      select 'Y'
        from sa_missing_tran
       where sa_missing_tran.miss_tran_seq_no = O_new_miss_tran_seq;

BEGIN
   LOOP
      --- Retrieve sequence number
      SQL_LIB.SET_MARK('OPEN','C_GET_NEXT','DUAL',NULL);
      open C_GET_NEXT;
      SQL_LIB.SET_MARK('FETCH','C_GET_NEXT','DUAL',NULL);
      fetch C_GET_NEXT into O_new_miss_tran_seq;
      ---
      if C_GET_NEXT%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT','DUAL',NULL);
         close C_GET_NEXT;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT','DUAL',NULL);
      close C_GET_NEXT;
      ---
      if (L_first_time = 'Y') then
         L_wrap_number := O_new_miss_tran_seq;
         L_first_time := 'N';
      elsif (O_new_miss_tran_seq = L_wrap_number) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',NULL,NULL,NULL);
         return FALSE;
      end if;

      --- Check key existence
      SQL_LIB.SET_MARK('OPEN','C_CHECK_KEY','SA_MISSING_TRAN','seq: '||to_char(O_new_miss_tran_seq));
      open C_CHECK_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_KEY','SA_MISSING_TRAN','seq: '||to_char(O_new_miss_tran_seq));
      fetch C_CHECK_SEQ into L_exists;
      if C_CHECK_SEQ%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_KEY','SA_MISSING_TRAN','seq: '||to_char(O_new_miss_tran_seq));
         close C_CHECK_SEQ;
         EXIT;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_KEY','SA_MISSING_TRAN','seq: '||to_char(O_new_miss_tran_seq));
      close C_CHECK_SEQ;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEW_MISS_TRAN_SEQ;
--------------------------------------------------------------------------------------
FUNCTION DELETE_ALL_MISS_TRAN (O_error_message     IN OUT VARCHAR2,
                               I_store_day_seq_no  IN     SA_MISSING_TRAN.STORE_DAY_SEQ_NO%TYPE,
			             I_register          IN     SA_MISSING_TRAN.REGISTER%TYPE,
			       	 I_POS_tran_no	   IN	    SA_MISSING_TRAN.TRAN_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'MISSING_TRAN_SQL.DELETE_ALL_MISS_TRAN';
   L_table         VARCHAR2(30) := 'SA_MISSING_TRAN';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SA_MISSING_TRAN is
      select 'x'
        from sa_missing_tran mt
       where mt.store_day_seq_no = I_store_day_seq_no
         and ((mt.tran_no        = I_POS_tran_no
               and I_POS_tran_no is NOT NULL)
          or I_POS_tran_no is NULL)
         and ((mt.register       = I_register
               and I_register is NOT NULL)
          or I_register is NULL)
         for update nowait;

BEGIN

   if I_store_day_seq_no is NOT NULL then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_SA_MISSING_TRAN','SA_MISSING_TRAN',NULL);
      open C_LOCK_SA_MISSING_TRAN;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SA_MISSING_TRAN','SA_MISSING_TRAN',NULL);
      close C_LOCK_SA_MISSING_TRAN;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'SA_MISSING_TRAN',NULL);
  update sa_missing_tran mt set status = 'D' 
            where mt.store_day_seq_no = I_store_day_seq_no
              and mt.status = 'M'
              and ((mt.tran_no          = I_POS_tran_no
                    and I_POS_tran_no is NOT NULL)
               or I_POS_tran_no is NULL)
              and ((mt.register         = I_register
                    and I_register is NOT NULL)
               or I_register is NULL);
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ALL_MISS_TRAN;
----------------------------------------------------------------------------------------
FUNCTION DELETE_ALL_MISS_TRAN (O_error_message     IN OUT VARCHAR2,
                               I_store_day_seq_no  IN     SA_MISSING_TRAN.STORE_DAY_SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'MISSING_TRAN_SQL.DELETE_ALL_MISS_TRAN';

BEGIN
   if MISSING_TRAN_SQL.DELETE_ALL_MISS_TRAN(O_error_message,
					    	 	  I_store_day_seq_no,
				   	    		  NULL,
					    		  NULL) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ALL_MISS_TRAN;
----------------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message          IN OUT  VARCHAR2,
                       I_missing_tran_seq_no    IN      SA_MISSING_TRAN.MISS_TRAN_SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program	   VARCHAR2(50) := 'MISSING_TRAN_SQL.UPDATE_STATUS';
   L_table         VARCHAR2(30) := 'SA_MISSING_TRAN';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SA_MISSING_TRAN is
      select 'x'
        from sa_missing_tran mt
       where mt.miss_tran_seq_no = I_missing_tran_seq_no
         for update nowait;

BEGIN
   if I_missing_tran_seq_no is NOT NULL then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_SA_MISSING_TRAN','SA_MISSING_TRAN',NULL);
      open C_LOCK_SA_MISSING_TRAN;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SA_MISSING_TRAN','SA_MISSING_TRAN',NULL);
      close C_LOCK_SA_MISSING_TRAN;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'SA_MISSING_TRAN',NULL);
      update sa_missing_tran mt
         set status = 'A'
       where mt.miss_tran_seq_no = I_missing_tran_seq_no;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                                SQLERRM, 
                                                L_program,
                                                to_char(SQLCODE));
         return FALSE;
END UPDATE_STATUS;
----------------------------------------------------------------------------------------
END MISSING_TRAN_SQL;
/
