
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_TOTAL_COMMON AUTHID CURRENT_USER AS

V_internal_schema_name       system_options.table_owner%TYPE;

TYPE v_code_array is table of varchar2(2000) index by BINARY_INTEGER;
v_out       sa_total_common.v_code_array ;
empty_v_out sa_total_common.v_code_array ;

/*----------------------------------------------------------------------------------------------
|| This is the full version that tests for the successful creation of an object of the given
|| type and name.
----------------------------------------------------------------------------------------------*/
function Call_Dynamic_Sql(v_spec IN sa_total_common.v_code_array, 
                          v_object_type IN varchar2,
                          v_object_name IN varchar2,
                          o_error_message OUT varchar2,
                          v_object_owner IN varchar2 := USER)
return boolean ;
----------------------------------------------------------------------------------------------------------
function Call_Dynamic_Sql(v_spec IN sa_total_common.v_code_array,
                          o_error_message OUT varchar2)
return boolean ;

function GET_TRAN_DATETIME (I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                            O_business_date    IN OUT SA_STORE_DAY.BUSINESS_DATE%TYPE,
                            O_store            IN OUT SA_STORE_DAY.STORE%TYPE,
                            O_day              IN OUT SA_STORE_DAY.DAY%TYPE,
                            O_error_message       OUT VARCHAR2)
return boolean;
-----------------------------------------------------------------------------
FUNCTION INTERNAL_SCHEMA_NAME
   return CHAR;
-----------------------------------------------------------------------------
END;
/

