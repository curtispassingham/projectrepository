CREATE OR REPLACE PACKAGE BODY STG_SVC_REFERENCE_FIELD
AS
 
   Type s9t_errors_tab_typ IS  TABLE OF S9T_ERRORS%ROWTYPE;
   Lp_s9t_errors_tab s9t_errors_tab_typ;
--------------------------------------------------------------------------------------------  
PROCEDURE WRITE_S9T_ERROR(I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
			  I_sheet   IN VARCHAR2,
			  I_row_seq IN NUMBER,
			  I_col     IN VARCHAR2,
			  I_sqlcode IN NUMBER,
			  I_sqlerrm IN VARCHAR2)
IS
BEGIN
  Lp_s9t_errors_tab.extend();
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).FILE_ID              := I_file_id;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_SEQ_NO         := s9t_errors_seq.nextval;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).TEMPLATE_KEY         := template_key;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).WKSHT_KEY            := I_sheet;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).COLUMN_KEY           := I_col;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ROW_SEQ              := I_row_seq;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            := 
  (
      CASE
         WHEN I_sqlcode IS NULL THEN
            I_sqlerrm
         ELSE
            'IIND-ORA-'||lpad(I_sqlcode,5,'0')
         END
   );
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_ID            := GET_USER;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_DATETIME      := sysdate;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_ID       := GET_USER;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_DATETIME := sysdate;
END write_s9t_error;
--------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER)
IS
  l_sheets s9t_pkg.names_map_typ;
  SA_REFERENCE_cols s9t_pkg.names_map_typ;
BEGIN
  l_sheets                       :=s9t_pkg.get_sheet_names(I_file_id);
  SA_REFERENCE_cols              :=s9t_pkg.get_col_names(I_file_id,SA_REFERENCE_sheet);
  SA_REFERENCE$Action            := SA_REFERENCE_cols('ACTION');
  SA_REFERENCE$TRAN_TYPE         := SA_REFERENCE_cols('TRAN_TYPE');
  SA_REFERENCE$SUB_TRAN_TYPE     := SA_REFERENCE_cols('SUB_TRAN_TYPE');
  SA_REFERENCE$REASON_CODE       := SA_REFERENCE_cols('REASON_CODE');
  SA_REFERENCE$REF_NO            := SA_REFERENCE_cols('REF_NO');
  SA_REFERENCE$REF_LABEL_CODE    := SA_REFERENCE_cols('REF_LABEL_CODE');
  --SA_REFERENCE$REF_SEQ_NO        := SA_REFERENCE_cols('REF_SEQ_NO');
END POPULATE_NAMES;
--------------------------------------------------------------------------------------------
PROCEDURE populate_SA_REFERENCE(I_file_id IN NUMBER)
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
        TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = SA_REFERENCE_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD'
  			    ,TRAN_TYPE
  	                    ,SUB_TRAN_TYPE
  	                    ,REASON_CODE
  	                    ,REF_NO
                            ,REF_LABEL_CODE                  
                           -- ,REF_SEQ_NO                  
                            ))
   FROM SA_REFERENCE ;
END populate_SA_REFERENCE;
--------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id IN OUT NUMBER)
IS
  l_file S9T_FILE;
  l_file_name S9T_FOLDER.FILE_NAME%TYPE;
  
BEGIN

  l_file              := NEW S9T_FILE();
  O_file_id           := S9T_FOLDER_SEQ.NEXTVAL;
  l_file.file_id      := O_file_id;
  l_file_name         := STG_SVC_REFERENCE_FIELD.template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
  l_file.file_name    := l_file_name;
  l_file.template_key := STG_SVC_REFERENCE_FIELD.TEMPLATE_KEY;
  l_file.user_lang    := GET_USER_LANG; 
  l_file.add_sheet(SA_REFERENCE_sheet);
  l_file.sheets(l_file.get_sheet_index(SA_REFERENCE_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'TRAN_TYPE'
                                                                                            ,'SUB_TRAN_TYPE'
                                                                                            ,'REASON_CODE'
                                                                                            ,'REF_NO'
                                                                                            ,'REF_LABEL_CODE'
                                                                                            );
  s9t_pkg.save_obj(l_file); 
END INIT_S9T;

--------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN CHAR DEFAULT 'N')
  RETURN BOOLEAN
IS
  l_file s9t_file;
  L_program VARCHAR2(255):='STG_SVC_REFERENCE_FIELD.CREATE_S9T';
BEGIN
  init_s9t(O_file_id);
  if S9T_PKG.populate_lists(O_error_message,
                            O_file_id,
                            'RSARF')=FALSE then
     return FALSE;
  end if;
  IF I_template_only_ind = 'N' THEN
     populate_SA_REFERENCE(O_file_id);
     commit; -- to avoid deadlock error 
  END IF;
  s9t_pkg.translate_to_user_lang(O_file_id);
  -- Apply template
  s9t_pkg.apply_template(O_file_id,template_key);
  l_file:=s9t_file(O_file_id);
  if s9t_pkg.code2desc(O_error_message,
                       'RSARF',
							         l_file)=FALSE then
     return FALSE;
  end if;
  s9t_pkg.save_obj(l_file);
  s9t_pkg.update_ods(l_file);
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END create_s9t;
--------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SA_REFERENCE (I_file_id    IN s9t_folder.file_id%type,
                                    I_process_id IN SVC_SA_REFERENCE.PROCESS_ID%type)
IS
  Type svc_SA_REFERENCE_col_typ IS TABLE OF   SVC_SA_REFERENCE%ROWTYPE;
  l_temp_rec                                  SVC_SA_REFERENCE%ROWTYPE;
  svc_SA_REFERENCE_col                        SVC_SA_REFERENCE_COL_TYP :=NEW SVC_SA_REFERENCE_COL_TYP();
  l_process_id                                SVC_SA_REFERENCE.PROCESS_ID%TYPE;
  l_error                                     BOOLEAN:=false;
  l_default_rec                               SVC_SA_REFERENCE%ROWTYPE;
  
  CURSOR C_MANDATORY_IND IS
    SELECT REF_LABEL_CODE_mi,
           REF_NO_mi,
           REASON_CODE_mi,
           SUB_TRAN_TYPE_mi,
           TRAN_TYPE_mi,
           1 as dummy
    FROM
      (SELECT column_key,
              mandatory
         FROM s9t_tmpl_cols_def
        WHERE template_key      = STG_SVC_REFERENCE_FIELD.template_key
          AND wksht_key         = 'SA_REFERENCE'
      ) PIVOT (MAX(mandatory) AS mi
      FOR (column_key) IN ('REF_LABEL_CODE' AS REF_LABEL_CODE,
                           'REF_NO' AS REF_NO,
                           'REASON_CODE' AS REASON_CODE,
                           'SUB_TRAN_TYPE' AS SUB_TRAN_TYPE,
                           'TRAN_TYPE' AS TRAN_TYPE,
                            null as dummy));
                            
  l_mi_rec C_MANDATORY_IND%ROWTYPE;
  
  CURSOR C_SA_REFERENCE is
     select TRAN_TYPE,
            SUB_TRAN_TYPE,
            REASON_CODE,
            REF_NO,
            REF_LABEL_CODE
      from SA_REFERENCE;
    L_sa_ref_rec SA_REFERENCE%ROWTYPE;  
    L_error_code   NUMBER;  
    L_pk_columns   VARCHAR2(255) := ' tran_type,ref_no';
    L_req_columns  VARCHAR2(255) := 'tran_type,ref_no,ref_label_code ';
    L_table        VARCHAR2(30) := 'SA_REFERENCE';
    L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
    dml_errors EXCEPTION;
    PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
    (SELECT
            REF_LABEL_CODE_dv,
            REF_NO_dv,
            REASON_CODE_dv,
            SUB_TRAN_TYPE_dv,
            TRAN_TYPE_dv,
            null as dummy
       FROM(SELECT column_key,
                   default_value
              FROM s9t_tmpl_cols_def
             WHERE template_key    = STG_SVC_REFERENCE_FIELD.template_key
               AND wksht_key       = 'SA_REFERENCE'
            ) PIVOT (MAX(default_value) AS dv
            FOR (column_key) IN ('REF_LABEL_CODE' AS REF_LABEL_CODE,
                                 'REF_NO' AS REF_NO,
                                 'REASON_CODE' AS REASON_CODE,
                                 'SUB_TRAN_TYPE' AS SUB_TRAN_TYPE,
                                 'TRAN_TYPE' AS TRAN_TYPE,
                                  NULL  ASdummy)
                     )
      ) LOOP  
 
           BEGIN  
             l_default_rec.REF_LABEL_CODE := rec.REF_LABEL_CODE_dv;  
           EXCEPTION  
           WHEN OTHERS THEN  
              write_s9t_error(I_file_id,'SA_REFERENCE',NULL,'REF_LABEL_CODE','INV_DEFAULT',SQLERRM);  
           END;
           
           BEGIN  
	    l_default_rec.REF_NO := rec.REF_NO_dv;  
	   EXCEPTION  
	   WHEN OTHERS THEN  
	    write_s9t_error(I_file_id,'SA_REFERENCE',NULL,'REF_NO','INV_DEFAULT',SQLERRM);  
           END;
           
           BEGIN  
             l_default_rec.REASON_CODE := rec.REASON_CODE_dv;  
           EXCEPTION  
           WHEN OTHERS THEN  
             write_s9t_error(I_file_id,'SA_REFERENCE',NULL,'REASON_CODE','INV_DEFAULT',SQLERRM);  
           END;
           
           BEGIN  
	    l_default_rec.SUB_TRAN_TYPE := rec.SUB_TRAN_TYPE_dv;  
	   EXCEPTION  
	   WHEN OTHERS THEN  
	    write_s9t_error(I_file_id,'SA_REFERENCE',NULL,'SUB_TRAN_TYPE','INV_DEFAULT',SQLERRM);  
           END;
           
           BEGIN  
	    l_default_rec.TRAN_TYPE := rec.TRAN_TYPE_dv;  
	   EXCEPTION  
	   WHEN OTHERS THEN  
	    write_s9t_error(I_file_id,'SA_REFERENCE',NULL,'TRAN_TYPE','INV_DEFAULT',SQLERRM);  
           END;
     END LOOP;

 --Get mandatory indicators
  OPEN C_MANDATORY_IND;
  FETCH C_MANDATORY_IND INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  
  FOR rec IN (SELECT r.get_cell(SA_REFERENCE$Action)            AS Action,
                     r.get_cell(SA_REFERENCE$REF_LABEL_CODE)    AS REF_LABEL_CODE,
                     r.get_cell(SA_REFERENCE$REF_NO)            AS REF_NO,
                     r.get_cell(SA_REFERENCE$REASON_CODE)       AS REASON_CODE,
                     r.get_cell(SA_REFERENCE$SUB_TRAN_TYPE)     AS SUB_TRAN_TYPE,
                     r.get_cell(SA_REFERENCE$TRAN_TYPE)         AS TRAN_TYPE,
                     r.get_row_seq()                            AS row_seq
                FROM s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               WHERE sf.file_id  = I_file_id
                 AND ss.sheet_name = sheet_name_trans(SA_REFERENCE_sheet)
             )
   LOOP
    l_temp_rec.process_id        := I_process_id;
    l_temp_rec.chunk_id          := 1;
    l_temp_rec.row_seq           := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id         := GET_USER;
    l_temp_rec.last_upd_id       := GET_USER;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,SA_REFERENCE_sheet,rec.row_seq,'ACTION_COLUMN',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.TRAN_TYPE := rec.TRAN_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,SA_REFERENCE_sheet,rec.row_seq,'TRAN_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUB_TRAN_TYPE := rec.SUB_TRAN_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,SA_REFERENCE_sheet,rec.row_seq,'SUB_TRAN_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.REASON_CODE := rec.REASON_CODE;
    EXCEPTION
    WHEN OTHERS THEN
       write_s9t_error(I_file_id,SA_REFERENCE_sheet,rec.row_seq,'REASON_CODE',SQLCODE,SQLERRM);
       l_error := true;
    END;
    BEGIN
      l_temp_rec.REF_NO := rec.REF_NO;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,SA_REFERENCE_sheet,rec.row_seq,'REF_NO',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.REF_LABEL_CODE := rec.REF_LABEL_CODE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,SA_REFERENCE_sheet,rec.row_seq,'REF_LABEL_CODE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    if rec.action = STG_SVC_REFERENCE_FIELD.ACTION_NEW    then
       l_temp_rec.REF_LABEL_CODE   := NVL( l_temp_rec.REF_LABEL_CODE,l_default_rec.REF_LABEL_CODE);
       l_temp_rec.REF_NO           := NVL( l_temp_rec.REF_NO,l_default_rec.REF_NO);
       l_temp_rec.REASON_CODE      := NVL( l_temp_rec.REASON_CODE,l_default_rec.REASON_CODE);
       l_temp_rec.SUB_TRAN_TYPE    := NVL( l_temp_rec.SUB_TRAN_TYPE,l_default_rec.SUB_TRAN_TYPE);
       l_temp_rec.TRAN_TYPE        := NVL( l_temp_rec.TRAN_TYPE,l_default_rec.TRAN_TYPE);
    end if;
    If (l_temp_rec.TRAN_TYPE is  NULL OR l_temp_rec.REF_NO IS  NULL OR l_temp_rec.REF_LABEL_CODE IS NULL ) then
       L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_req_columns);      
       write_s9t_error(I_file_id,SA_REFERENCE_sheet,rec.row_seq,NULL,NULL,L_error_msg); 
       l_error := true;
    END IF;
    IF NOT l_error THEN
      svc_SA_REFERENCE_col.extend();
      svc_SA_REFERENCE_col(svc_SA_REFERENCE_col.count()):=l_temp_rec;
    END IF;
  END LOOP;
  BEGIN
  
  FORALL i IN 1..svc_SA_REFERENCE_col.count SAVE EXCEPTIONS  Merge INTO SVC_SA_REFERENCE st USING
  (SELECT
    (CASE
        WHEN l_mi_rec.TRAN_TYPE_mi    = 'N'
        AND svc_SA_REFERENCE_col(i).action = STG_SVC_REFERENCE_FIELD.action_mod
        AND s1.TRAN_TYPE             IS NULL
        THEN mt.TRAN_TYPE
        ELSE s1.TRAN_TYPE
     END) AS TRAN_TYPE,
    (CASE
           WHEN l_mi_rec.SUB_TRAN_TYPE_mi    = 'N'
           AND svc_SA_REFERENCE_col(i).action = STG_SVC_REFERENCE_FIELD.action_mod
           AND s1.SUB_TRAN_TYPE             IS NULL
           THEN mt.SUB_TRAN_TYPE
           ELSE s1.SUB_TRAN_TYPE
     END) AS SUB_TRAN_TYPE,
    (CASE
           WHEN l_mi_rec.REASON_CODE_mi    = 'N'
           AND svc_SA_REFERENCE_col(i).action = STG_SVC_REFERENCE_FIELD.action_mod
           AND s1.REASON_CODE             IS NULL
           THEN mt.REASON_CODE
           ELSE s1.REASON_CODE
    END) AS REASON_CODE,
    (CASE
          WHEN l_mi_rec.REF_NO_mi    = 'N' 
          AND svc_SA_REFERENCE_col(i).action = STG_SVC_REFERENCE_FIELD.action_mod 
          AND s1.REF_NO IS NULL
              THEN mt.REF_NO
          ELSE s1.REF_NO
    END) AS REF_NO,
    (CASE
      WHEN l_mi_rec.REF_LABEL_CODE_mi    = 'N' AND svc_SA_REFERENCE_col(i).action = STG_SVC_REFERENCE_FIELD.action_mod
           AND s1.REF_LABEL_CODE             IS NULL
      THEN mt.REF_LABEL_CODE
      ELSE s1.REF_LABEL_CODE
    END) AS REF_LABEL_CODE,
    null as dummy
    FROM (SELECT  svc_SA_REFERENCE_col(i).REF_LABEL_CODE AS REF_LABEL_CODE,
                  svc_SA_REFERENCE_col(i).REF_NO AS REF_NO,
                  svc_SA_REFERENCE_col(i).REASON_CODE AS REASON_CODE,
                  svc_SA_REFERENCE_col(i).SUB_TRAN_TYPE AS SUB_TRAN_TYPE,
                  svc_SA_REFERENCE_col(i).TRAN_TYPE AS TRAN_TYPE,
                  null as dummy
            FROM dual) s1,
                       SA_REFERENCE mt
      WHERE  mt.TRAN_TYPE (+)     = s1.TRAN_TYPE   and
             mt.SUB_TRAN_TYPE (+)     = s1.SUB_TRAN_TYPE   and
             mt.REASON_CODE (+)     = s1.REASON_CODE   and
             mt.REF_NO (+)     = s1.REF_NO   and
             1 = 1) sq 
   ON (st.TRAN_TYPE                  = sq.TRAN_TYPE AND
       NVL(st.SUB_TRAN_TYPE,'X')    = NVL(sq.SUB_TRAN_TYPE,'X') AND
       NVL(st.REASON_CODE,'X')      = NVL(sq.REASON_CODE,'X') AND
       st.REF_NO                     = sq.REF_NO AND
       svc_SA_REFERENCE_col(i).ACTION IN (STG_SVC_REFERENCE_FIELD.ACTION_MOD,STG_SVC_REFERENCE_FIELD.ACTION_DEL)
   )
WHEN matched THEN
  UPDATE
  SET PROCESS_ID      = svc_SA_REFERENCE_col(i).PROCESS_ID ,
      CHUNK_ID          = svc_SA_REFERENCE_col(i).CHUNK_ID ,
      ROW_SEQ           = svc_SA_REFERENCE_col(i).ROW_SEQ ,
      PROCESS$STATUS    = svc_SA_REFERENCE_col(i).PROCESS$STATUS ,
      ACTION            = svc_SA_REFERENCE_col(i).ACTION ,
      REF_LABEL_CODE    = sq.REF_LABEL_CODE ,
      CREATE_ID         = svc_SA_REFERENCE_col(i).CREATE_ID ,
      CREATE_DATETIME   = svc_SA_REFERENCE_col(i).CREATE_DATETIME ,
      LAST_UPD_ID       = svc_SA_REFERENCE_col(i).LAST_UPD_ID ,
      LAST_UPD_DATETIME = svc_SA_REFERENCE_col(i).LAST_UPD_DATETIME 
 WHEN NOT matched THEN
  INSERT
    (
      PROCESS_ID ,
      CHUNK_ID ,
      ROW_SEQ ,
      ACTION ,
      PROCESS$STATUS ,
      REF_LABEL_CODE ,
      REF_NO ,
      REASON_CODE ,
      SUB_TRAN_TYPE ,
      TRAN_TYPE ,
      CREATE_ID ,
      CREATE_DATETIME ,
      LAST_UPD_ID ,
      LAST_UPD_DATETIME
    )
    VALUES
    (
      svc_SA_REFERENCE_col(i).PROCESS_ID ,
      svc_SA_REFERENCE_col(i).CHUNK_ID ,
      svc_SA_REFERENCE_col(i).ROW_SEQ ,
      svc_SA_REFERENCE_col(i).ACTION ,
      svc_SA_REFERENCE_col(i).PROCESS$STATUS ,
      sq.REF_LABEL_CODE ,
      sq.REF_NO ,
      sq.REASON_CODE ,
      sq.SUB_TRAN_TYPE ,
      sq.TRAN_TYPE ,
      svc_SA_REFERENCE_col(i).CREATE_ID ,
      svc_SA_REFERENCE_col(i).CREATE_DATETIME ,
      svc_SA_REFERENCE_col(i).LAST_UPD_ID ,
      svc_SA_REFERENCE_col(i).LAST_UPD_DATETIME
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
     L_error_code := sql%bulk_exceptions(i).error_code;
     if L_error_code = 1 then
        L_error_code := NULL;
        L_error_msg := 'SA_REFER_EXISTS';
                                                 
      end if;
      write_s9t_error
      (
        I_file_id,SA_REFERENCE_sheet,svc_SA_REFERENCE_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;
END PROCESS_S9T_SA_REFERENCE;
--------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T
  (
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
    I_process_id    IN     NUMBER,
    O_error_count   OUT    NUMBER
  )
  RETURN BOOLEAN
IS
   l_file            S9T_FILE;
   l_sheets          S9T_PKG.NAMES_MAP_TYP;
   L_program         VARCHAR2(255):='STG_SVC_REFERENCE_FIELD.PROCESS_S9T';
   l_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
   
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	    EXCEPTION;
   PRAGMA	    EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   commit;
   s9t_pkg.ods2obj(I_file_id);
   l_file            := s9t_pkg.get_obj(I_file_id);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if s9t_pkg.code2desc(O_error_message,
	                      'RSARF',-- pass Template Category code type
                        l_file,
                        true)=FALSE then
      return FALSE;
	 end if;	
	 s9t_pkg.save_obj(l_file);
   
   IF s9t_pkg.validate_template(I_file_id) = false THEN
      write_s9t_error(I_file_id,NULL,NULL,'ACTIVE_DATE',NULL,'S9T_INVALID_TEMPLATE');  
   ELSE
     populate_names(I_file_id);
     sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key,l_file.user_lang);
     /*Insert into staging table*/
     process_s9t_SA_REFERENCE(I_file_id,I_process_id);
   END IF;
  
   O_error_count := Lp_s9t_errors_tab.count();
   forall i IN 1..O_error_count
     INSERT INTO s9t_errors VALUES Lp_s9t_errors_tab(i);
   
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   IF O_error_count    = 0 THEN
       l_process_status := 'PS';
   ELSE
      l_process_status := 'PE';
   END IF;
    UPDATE svc_process_tracker
    SET status       = l_process_status,
      file_id        = I_file_id
    WHERE process_id = I_process_id;
   commit;
  RETURN true;
EXCEPTION
   WHEN INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         INSERT INTO s9t_errors VALUES Lp_s9t_errors_tab(i);
      UPDATE svc_process_tracker
         SET status       = 'PE',
             file_id        = I_file_id
       WHERE process_id = I_process_id;
   
      COMMIT;

      RETURN FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
  
END PROCESS_S9T;
--------------------------------------------------------------------------------------------
END STG_SVC_REFERENCE_FIELD;
/