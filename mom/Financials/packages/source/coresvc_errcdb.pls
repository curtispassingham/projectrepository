CREATE OR REPLACE PACKAGE BODY coresvc_sa_error_codes
is
    Type errors_tab_typ
    IS
    TABLE OF SVC_ADMIN_UPLD_ER%rowtype;
    Lp_errors_tab errors_tab_typ;
    Type SA_ERROR_CODES_rec_tab
    IS
    TABLE OF SA_ERROR_CODES%rowtype;
    
    Type SA_ERROR_CODES_TL_REC_TAB IS TABLE OF SA_ERROR_CODES_TL%ROWTYPE;
    
    CURSOR c_svc_SA_ERROR_IMPACT(I_process_id NUMBER,I_chunk_id NUMBER)
    IS
    select
        PK_SA_ERROR_IMPACT.rowid  AS PK_SA_ERROR_IMPACT_rid,
        SEI_SAC_FK.rowid    AS SEI_SAC_FK_rid,
        SEI_SAC_FK.REQUIRED_IND as SEI_SAC_FK_req,
        st.REQUIRED_IND,
        st.SYSTEM_CODE,
        st.ERROR_CODE,
        st.PROCESS_ID,
        st.ROW_SEQ,
    PK_SA_ERROR_IMPACT.required_ind  as prev_required_ind,
        upper(st.ACTION) AS action,
        st.PROCESS$STATUS
     from SVC_SA_ERROR_IMPACT st,
        SA_ERROR_IMPACT PK_SA_ERROR_IMPACT,
        SA_ERROR_CODES SEI_SAC_FK,
        dual
     where st.process_id = I_process_id
     and st.chunk_id     = I_chunk_id
     and st.SYSTEM_CODE         = PK_SA_ERROR_IMPACT.SYSTEM_CODE (+)
     and st.ERROR_CODE         = PK_SA_ERROR_IMPACT.ERROR_CODE (+)
     and st.ERROR_CODE        = SEI_SAC_FK.ERROR_CODE (+)
     and st.action      IS NOT NULL;
    c_svc_SA_ERROR_IMPACT_rec c_svc_SA_ERROR_IMPACT%rowtype;
    
    cursor C_SVC_SA_ERROR_CODES(I_process_id NUMBER,I_chunk_id NUMBER) is
       select pk_sa_error_codes.rowid  as pk_sa_error_codes_rid,
              pk_sa_error_codes.required_ind as pk_sa_error_codes_req,
              st.short_desc,
              st.required_ind,
              st.hq_override_ind,
              st.store_override_ind,
              st.rec_solution,
              st.target_tab,
              st.target_form,
              st.error_desc,
              st.error_code,
              st.process_id,
              st.row_seq,
              upper(st.action) as action,
              st.process$status
         from svc_sa_error_codes st,
              sa_error_codes pk_sa_error_codes
        where st.process_id   = I_process_id
          and st.chunk_id     = I_chunk_id
          and st.error_code   = pk_sa_error_codes.error_code (+)
          and st.action       is not null;
          c_svc_SA_ERROR_CODES_rec c_svc_SA_ERROR_CODES%rowtype;
          
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang       LANG.LANG%TYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;

    -----------------------------------------------------------
    PROCEDURE write_error(
     I_process_id  IN SVC_ADMIN_UPLD_ER.process_id%type,
     I_error_seq   IN SVC_ADMIN_UPLD_ER.error_seq%type,
     I_chunk_id    IN SVC_ADMIN_UPLD_ER.chunk_id%type,
     I_table_name  IN SVC_ADMIN_UPLD_ER.table_name%type,
     I_row_seq     IN SVC_ADMIN_UPLD_ER.row_seq%type,
     I_column_name IN SVC_ADMIN_UPLD_ER.column_name%type,
     I_error_msg   IN SVC_ADMIN_UPLD_ER.error_msg%type,
     I_error_type  IN SVC_ADMIN_UPLD_ER.error_type%type default 'E')
    IS
    BEGIN
    Lp_errors_tab.extend();
    Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
    Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
    Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
    Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
    Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
    Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
    Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
    Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
    END write_error;
-------------------------------------------------------------------------------
---  Name: exec_SA_ERROR_IMPACT_ins
--- Purpose: For inserting data in SA_ERROR_IMPACT
-------------------------------------------------------------------------------- 
    PROCEDURE exec_SA_ERROR_IMPACT_ins
    (
     I_ins_tab IN SA_ERROR_IMPACT_rec_tab
    )
    IS
    BEGIN
    forall i IN 1..I_ins_tab.count()
    INSERT INTO SA_ERROR_IMPACT VALUES I_ins_tab
     ( i
     );
    END exec_SA_ERROR_IMPACT_ins;
    -------------------------------------------------------------------------------
---  Name: exec_SA_ERROR_IMPACT_upd
--- Purpose: For updating data in SA_ERROR_IMPACT
-------------------------------------------------------------------------------- 
    PROCEDURE exec_SA_ERROR_IMPACT_upd
    (
     I_upd_tab IN SA_ERROR_IMPACT_rec_tab
    )
    IS
    BEGIN
    forall i IN 1..I_upd_tab.count()
    UPDATE SA_ERROR_IMPACT SET row = I_upd_tab(i) where 1 = 1
    and SYSTEM_CODE = I_upd_tab(i).SYSTEM_CODE
    and ERROR_CODE = I_upd_tab(i).ERROR_CODE
    ;
    END exec_SA_ERROR_IMPACT_upd;
    -------------------------------------------------------------------------------
---  Name: exec_SA_ERROR_IMPACT_del
--- Purpose: For deleting data in SA_ERROR_IMPACT
-------------------------------------------------------------------------------- 
    PROCEDURE exec_SA_ERROR_IMPACT_del(
     I_del_tab IN SA_ERROR_IMPACT_rec_tab )
    IS
    BEGIN
    forall i IN 1..I_del_tab.count()
    DELETE from SA_ERROR_IMPACT where 1 = 1
    and SYSTEM_CODE = I_del_tab(i).SYSTEM_CODE
    and ERROR_CODE = I_del_tab(i).ERROR_CODE
    ;
    END exec_SA_ERROR_IMPACT_del;
    -------------------------------------------------------------------------------
---  Name: process_SA_ERROR_IMPACT
-------------------------------------------------------------------------------- 
    PROCEDURE process_SA_ERROR_IMPACT(
     I_process_id IN SVC_SA_ERROR_IMPACT.PROCESS_ID%TYPE,
     I_chunk_id   IN SVC_SA_ERROR_IMPACT.CHUNK_ID%TYPE )
    IS
    l_error BOOLEAN;
    SA_ERROR_IMPACT_temp_rec SA_ERROR_IMPACT%rowtype;
    SA_ERROR_IMPACT_ins_rec SA_ERROR_IMPACT_rec_tab:=NEW SA_ERROR_IMPACT_rec_tab();
    SA_ERROR_IMPACT_upd_rec SA_ERROR_IMPACT_rec_tab:=NEW SA_ERROR_IMPACT_rec_tab();
    SA_ERROR_IMPACT_del_rec SA_ERROR_IMPACT_rec_tab:=NEW SA_ERROR_IMPACT_rec_tab();
    l_table VARCHAR2(255)    :='SA_ERROR_IMPACT';
    O_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
  l_system_name code_detail.code_desc%type;
    o_exists boolean;

    BEGIN
    FOR rec IN c_svc_SA_ERROR_IMPACT(I_process_id,I_chunk_id)
    LOOP
         l_error       := False;
         if rec.action is NULL then
         WRITE_ERROR(I_process_id, svc_admin_upld_er_seq.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'ACTION', 'FIELD_NOT_NULL');
         L_error := TRUE;
      end if;
         IF rec.action IS NOT NULL and rec.action NOT IN (STG_SVC_SA_ERROR_CODES.action_new,STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_del) THEN
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ACTION','INV_ACT');
            l_error :=true;
         END IF;
         
         IF rec.action IN (STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_del) and rec.PK_SA_ERROR_IMPACT_rid IS NULL THEN
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'PK_SA_EI_MISSING');
            l_error :=true;
         END IF;
         IF rec.SEI_SAC_FK_rid IS NULL THEN
            
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ERROR_CODE','INV_ERROR_CODE');
            l_error :=true;
         END IF;
         IF NOT(  rec.ERROR_CODE  IS NOT NULL ) THEN
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ERROR_CODE','MUST_ENTER_VALUE');
            l_error :=true;
         END IF;
         IF NOT(  rec.SYSTEM_CODE  IS NOT NULL ) THEN
            write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'SYSTEM_CODE','SYSTEM_NAME_REQ');
            l_error :=true;
         END IF;
     if  rec.action IN (STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_new) then
       IF NOT(  rec.REQUIRED_IND  IS NOT NULL )  THEN
        write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'REQUIRED_IND','MUST_ENTER_VALUE');
        l_error :=true;
       END IF;
       IF NOT( rec.REQUIRED_IND IN  ( 'Y','N' )  )  THEN
        write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'REQUIRED_IND','INV_Y_N_IND');
        l_error :=true;
       END IF;
       
     end if;
        --- additional validations

        IF rec.action =STG_SVC_SA_ERROR_CODES.action_new THEN
         ---check if record exist
            IF SA_ERROR_SQL.ERROR_SYSTEM_EXISTS(O_error_message, O_exists, rec.system_code, upper(rec.error_code)) = FALSE THEN
                WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'ERROR_CODE',O_error_message);
                l_error     :=TRUE;
            elsif O_exists = TRUE THEN
                WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,null,'ECODE_SYSNAME_EXISTS');
                l_error:=TRUE;
      elsif LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'SYSE',
                                       rec.system_code,
                                       l_system_name) = FALSE then
            WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'SYSTEM_CODE',O_error_message);
                    l_error:=TRUE;
          
            END IF;
            --check required_ind field
            IF rec.SEI_SAC_FK_req='N' and rec.required_ind='Y' THEN
                WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'REQUIRED_IND','INVALID');
                l_error:=TRUE;
            END IF;
         --MOD--
         
        elsif rec.action =STG_SVC_SA_ERROR_CODES.action_mod THEN
    
         --check required_ind field on parent
            IF rec.SEI_SAC_FK_req='N' and rec.required_ind='Y' THEN
                WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'REQUIRED_IND','INVALID');
                l_error                              :=TRUE;
            ELSIF rec.SEI_SAC_FK_req='Y' and rec.required_ind='N' and rec.prev_required_ind='Y' THEN--required_ind is updatable only if it is previously N
                WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'REQUIRED_IND','CANNOT_MODIFY');
                l_error:=TRUE;
            END IF;
        elsif rec.action =STG_SVC_SA_ERROR_CODES.action_del THEN
            ---cannot delete record with required_ind=Y
            IF rec.prev_required_ind='Y' THEN-- 
                WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,null,'NO_DEL_SYS_REQ');
                l_error:=TRUE;
            END IF;
        END IF;
         
        IF NOT l_error THEN
            SA_ERROR_IMPACT_temp_rec.ERROR_CODE              := rec.ERROR_CODE;
            SA_ERROR_IMPACT_temp_rec.SYSTEM_CODE              := rec.SYSTEM_CODE;
            SA_ERROR_IMPACT_temp_rec.REQUIRED_IND              := rec.REQUIRED_IND;
            IF rec.action                    = STG_SVC_SA_ERROR_CODES.action_new THEN
                SA_ERROR_IMPACT_ins_rec.extend;
                SA_ERROR_IMPACT_ins_rec(SA_ERROR_IMPACT_ins_rec.count()):=SA_ERROR_IMPACT_temp_rec;         
            END IF;
            IF rec.action = STG_SVC_SA_ERROR_CODES.action_mod THEN
                SA_ERROR_IMPACT_upd_rec.extend;
                SA_ERROR_IMPACT_upd_rec(SA_ERROR_IMPACT_upd_rec.count()):=SA_ERROR_IMPACT_temp_rec;
            END IF;
            IF rec.action = STG_SVC_SA_ERROR_CODES.action_del THEN
                SA_ERROR_IMPACT_del_rec.extend;
                SA_ERROR_IMPACT_del_rec(SA_ERROR_IMPACT_del_rec.count()):=SA_ERROR_IMPACT_temp_rec;
            END IF;
        END IF;
    END LOOP;
    exec_SA_ERROR_IMPACT_ins(SA_ERROR_IMPACT_ins_rec);
    exec_SA_ERROR_IMPACT_upd(SA_ERROR_IMPACT_upd_rec);
    exec_SA_ERROR_IMPACT_del(SA_ERROR_IMPACT_del_rec);
    END process_SA_ERROR_IMPACT;
-------------------------------------------------------------------------------
---  Name: exec_SA_ERROR_CODES_ins
--- Purpose: For inserting data in SA_ERROR_CODES
-------------------------------------------------------------------------------- 
    PROCEDURE exec_SA_ERROR_CODES_ins
    (
     I_ins_tab IN SA_ERROR_CODES_rec_tab
    )
    IS
    BEGIN
        forall i IN 1..I_ins_tab.count()
        INSERT INTO SA_ERROR_CODES VALUES I_ins_tab
         ( i
         );
    END exec_SA_ERROR_CODES_ins;
    -------------------------------------------------------------------------------
---  Name: exec_SA_ERROR_CODES_upd
--- Purpose: For UPDATING data in SA_ERROR_CODES
-------------------------------------------------------------------------------- 
    PROCEDURE exec_SA_ERROR_CODES_upd
    (
     I_upd_tab IN SA_ERROR_CODES_rec_tab
    )
    IS
    BEGIN
        forall i IN 1..I_upd_tab.count()
        UPDATE SA_ERROR_CODES SET row = I_upd_tab(i) where 1 = 1
        and ERROR_CODE = I_upd_tab(i).ERROR_CODE
        ;
    END exec_SA_ERROR_CODES_upd;
-------------------------------------------------------------------------------
---  Name: exec_SA_ERROR_CODES_del
--- Purpose: For DELETING data in SA_ERROR_CODES
-------------------------------------------------------------------------------- 
    PROCEDURE exec_SA_ERROR_CODES_del(
     I_del_tab IN SA_ERROR_CODES_rec_tab )
    IS
    BEGIN
        for i IN 1..I_del_tab.count()
        loop
            DELETE from SA_ERROR_IMPACT where 1 = 1
            and ERROR_CODE = I_del_tab(i).ERROR_CODE;

            DELETE from SA_ERROR_CODES_TL where 1 = 1
            and ERROR_CODE = I_del_tab(i).ERROR_CODE;
    
            DELETE from SA_ERROR_CODES where 1 = 1
            and ERROR_CODE = I_del_tab(i).ERROR_CODE;

        end loop;
    END exec_SA_ERROR_CODES_del;
-------------------------------------------------------------------------------
---  Name: EXEC_SA_ERROR_CODES_TL_INS
--- Purpose: For inserting data in SA_ERROR_CODES_TL
-------------------------------------------------------------------------------- 
FUNCTION EXEC_SA_ERROR_CODES_TL_INS(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_ins_tab        IN       SA_ERROR_CODES_TL_REC_TAB) 
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SA_ERROR_CODES.EXEC_SA_ERROR_CODES_TL_INS';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ERROR_CODES_TL';

BEGIN
   if I_ins_tab is NOT NULL and I_ins_tab.count > 0 then
      FORALL i IN 1..I_ins_tab.count()
         insert into sa_error_codes_tl
              values I_ins_tab(i);
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_SA_ERROR_CODES_TL_INS;
-------------------------------------------------------------------------------
---  Name: EXEC_SA_ERROR_CODES_TL_UPD
---  Purpose: For UPDATING data in SA_ERROR_CODES_TL
-------------------------------------------------------------------------------- 
FUNCTION EXEC_SA_ERROR_CODES_TL_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_upd_tab                  IN       SA_ERROR_CODES_TL_REC_TAB,
                                    I_saerr_codes_tl_upd_rst   IN       ROW_SEQ_TAB,
                                    I_process_id               IN       SVC_SA_ERROR_CODES_TL.PROCESS_ID%TYPE,
                                    I_chunk_id                 IN       SVC_SA_ERROR_CODES_TL.CHUNK_ID%TYPE) 
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SA_ERROR_CODES.EXEC_SA_ERROR_CODES_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ERROR_CODES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(50) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;
   
    --Cursor to lock the record
   cursor C_LOCK_SAERRCD_TL_UPD(I_error_code  SA_ERROR_CODES_TL.ERROR_CODE%TYPE,
                                I_lang        SA_ERROR_CODES_TL.LANG%TYPE) is
      select 'x'
        from sa_error_codes_tl
       where error_code = I_error_code
         and lang = I_lang
         for update nowait;
         
BEGIN
   if I_upd_tab is NOT NULL and I_upd_tab.count > 0 then
      for i in I_upd_tab.FIRST..I_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_upd_tab(i).lang);
            L_key_val2 := 'Error Code: '||to_char(I_upd_tab(i).error_code);
            open C_LOCK_SAERRCD_TL_UPD(I_upd_tab(i).error_code,
                                        I_upd_tab(i).lang);
            close C_LOCK_SAERRCD_TL_UPD;
            
            update sa_error_codes_tl
               set error_desc = I_upd_tab(i).error_desc,
                   rec_solution = I_upd_tab(i).rec_solution,
                   short_desc = I_upd_tab(i).short_desc,
                   last_update_id = I_upd_tab(i).last_update_id,
                   last_update_datetime = I_upd_tab(i).last_update_datetime
             where error_code = I_upd_tab(i).error_code
               and lang = I_upd_tab(i).lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SA_ERROR_CODES_TL',
                           I_saerr_codes_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_LOCK_SAERRCD_TL_UPD%ISOPEN then
         close C_LOCK_SAERRCD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END EXEC_SA_ERROR_CODES_TL_UPD;
-------------------------------------------------------------------------------
---  Name: EXEC_SA_ERROR_CODES_TL_DEL
---  Purpose: For DELETING data in SA_ERROR_CODES_TL
-------------------------------------------------------------------------------- 
FUNCTION EXEC_SA_ERROR_CODES_TL_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_del_tab                  IN       SA_ERROR_CODES_TL_REC_TAB,
                                    I_saerr_codes_tl_del_rst   IN       ROW_SEQ_TAB,
                                    I_process_id               IN       SVC_SA_ERROR_CODES_TL.PROCESS_ID%TYPE,
                                    I_chunk_id                 IN       SVC_SA_ERROR_CODES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SA_ERROR_CODES.EXEC_SA_ERROR_CODES_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ERROR_CODES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(50) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_SAERRCD_TL_DEL(I_error_code  SA_ERROR_CODES_TL.ERROR_CODE%TYPE,
                                I_lang        SA_ERROR_CODES_TL.LANG%TYPE) is
      select 'x'
        from sa_error_codes_tl
       where error_code = I_error_code
         and lang = I_lang
         for update nowait;
BEGIN
   if I_del_tab is NOT NULL and I_del_tab.count > 0 then
      for i in I_del_tab.FIRST..I_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_del_tab(i).lang);
            L_key_val2 := 'Error Code: '||to_char(I_del_tab(i).error_code);
            open C_LOCK_SAERRCD_TL_DEL(I_del_tab(i).error_code,
                                       I_del_tab(i).lang);
            close C_LOCK_SAERRCD_TL_DEL;
            
            delete
              from sa_error_codes_tl
             where error_code = I_del_tab(i).error_code
               and lang = I_del_tab(i).lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SA_ERROR_CODES_TL',
                           I_saerr_codes_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_SAERRCD_TL_DEL%ISOPEN then
         close C_LOCK_SAERRCD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_SA_ERROR_CODES_TL_DEL;
-------------------------------------------------------------------------------- 
    PROCEDURE process_SA_ERROR_CODES(
     I_process_id IN SVC_SA_ERROR_CODES.PROCESS_ID%TYPE,
     I_chunk_id   IN SVC_SA_ERROR_CODES.CHUNK_ID%TYPE )
    IS
    l_error BOOLEAN;
    SA_ERROR_CODES_temp_rec SA_ERROR_CODES%rowtype;
    SA_ERROR_CODES_ins_rec SA_ERROR_CODES_rec_tab:=NEW SA_ERROR_CODES_rec_tab();
    SA_ERROR_CODES_upd_rec SA_ERROR_CODES_rec_tab:=NEW SA_ERROR_CODES_rec_tab();
    SA_ERROR_CODES_del_rec SA_ERROR_CODES_rec_tab:=NEW SA_ERROR_CODES_rec_tab();
    l_table VARCHAR2(255)    :='SA_ERROR_CODES';
    O_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
  l_output code_detail.code_desc%type;
    O_exists boolean;
    O_delete BOOLEAN;
    BEGIN
        FOR rec IN c_svc_SA_ERROR_CODES(I_process_id,I_chunk_id)
        LOOP
            l_error       := False;
            if rec.action is NULL then
                WRITE_ERROR(I_process_id, svc_admin_upld_er_seq.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'ACTION', 'FIELD_NOT_NULL');
                L_error := TRUE;
         end if;
            IF rec.action IS NOT NULL and rec.action NOT IN (STG_SVC_SA_ERROR_CODES.action_new,STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_del) THEN
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ACTION','INV_ACT');
                l_error :=true;
            END IF;
            IF rec.action = STG_SVC_SA_ERROR_CODES.action_new and rec.PK_SA_ERROR_CODES_rid IS NOT NULL THEN

                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ERROR_CODE','ERROR_CODE_EXISTS');
                l_error :=true;
            END IF;
            IF rec.action IN (STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_del) and rec.PK_SA_ERROR_CODES_rid IS NULL and rec.ERROR_CODE  IS NOT NULL THEN
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ERROR_CODE','INV_ERROR_CODE');
                l_error :=true;
            END IF;
            IF NOT(  rec.ERROR_CODE  IS NOT NULL )  THEN
                write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ERROR_CODE','MUST_ENTER_VALUE');
                l_error :=true;
            END IF;
      if  rec.action IN (STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_new) then
        IF NOT(  rec.ERROR_DESC  IS NOT NULL )  THEN
          write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ERROR_DESC','MUST_ENTER_VALUE');
          l_error :=true;
        END IF;
        IF NOT(  rec.HQ_OVERRIDE_IND  IS NOT NULL ) THEN
          write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'HQ_OVERRIDE_IND','MUST_ENTER_VALUE');
          l_error :=true;
        END IF;
        IF NOT(  rec.REQUIRED_IND  IS NOT NULL )   THEN
          write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'REQUIRED_IND','MUST_ENTER_VALUE');
          l_error :=true;
        END IF;
        IF NOT( rec.REQUIRED_IND IN  ( 'Y','N' )  )  THEN
          write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'REQUIRED_IND','INV_Y_N_IND');
          l_error :=true;
        END IF;
        IF NOT( rec.HQ_OVERRIDE_IND IN  ( 'Y','N' )  ) THEN
          write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'HQ_OVERRIDE_IND','INV_Y_N_IND');
          l_error :=true;
        END IF;
         if rec.target_form is not null then
           if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                   'FORM',
                                   rec.target_form,
                                   l_output) = FALSE then
               write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TARGET_FORM',O_error_message);
               l_error :=true;
            end if;
        end if;
        
     end if;
            ----additional validations

            IF rec.action  =STG_SVC_SA_ERROR_CODES.action_new THEN
                IF rec.required_ind = 'Y' THEN
                    IF SA_ERROR_SQL.INVALID_CHAR_SEQ(O_error_message, O_exists,upper( rec.error_code)) = FALSE THEN
                        WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'ERROR_CODE',O_error_message);
                        l_error:=TRUE;
                    ELSIF O_exists THEN
                        WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'ERROR_CODE','INV_CHAR_SEQ');
                        l_error:=TRUE;
                    END IF;
                END IF;
        
        if rec.target_tab is not null then
               if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'TABS',
                                       rec.target_tab,
                                       l_output) = FALSE then
                   write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TARGET_TAB',O_error_message);
                   l_error :=true;
              elsif NVL(rec.target_form,'dummy')<>'TR' THEN
                -- Only if form is TR, then tab values are available
                  WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'TARGET_TAB','INVALID');
                  l_error:=TRUE;
              end if;
        end if       ; 

            

            --- validations for MOD
            ELSIF rec.action=STG_SVC_SA_ERROR_CODES.action_mod THEN

            --Required_ind cannot be updated to N when it is  set to Y previously
                IF rec.required_ind='N' and rec.PK_SA_ERROR_CODES_req ='Y' THEN
                    WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'REQUIRED_IND','CANNOT_MODIFY');
                    l_error:=TRUE;
                END IF;
            if rec.target_tab is not null then
               if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'TABS',
                                       rec.target_tab,
                                       l_output) = FALSE then
                   write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TARGET_TAB',O_error_message);
                   l_error :=true;
              elsif NVL(rec.target_form,'dummy')<>'TR' THEN
                -- Only if form is TR, then tab values are available
                  WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,'TARGET_TAB','INVALID');
                  l_error:=TRUE;
              end if;
        end if       ; 
            ELSIF rec.action=STG_SVC_SA_ERROR_CODES.action_del THEN
            ---check if record  exist
                IF SA_ERROR_SQL.CHECK_DELETE(O_error_message, O_delete,upper( rec.error_code)) = FALSE THEN
                    WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,NULL,O_error_message);
                    l_error    :=TRUE;
                elsif O_delete= FALSE THEN
                    WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,NULL,'DEL_ERROR_CODE_REQ');
                    l_error:=TRUE;
                END IF;
            END IF;
            ------------------

            IF NOT l_error THEN
            SA_ERROR_CODES_temp_rec.ERROR_CODE              := rec.ERROR_CODE;
            SA_ERROR_CODES_temp_rec.ERROR_DESC              := rec.ERROR_DESC;
            SA_ERROR_CODES_temp_rec.TARGET_FORM             := rec.TARGET_FORM;
            SA_ERROR_CODES_temp_rec.TARGET_TAB              := rec.TARGET_TAB;
            SA_ERROR_CODES_temp_rec.REC_SOLUTION            := rec.REC_SOLUTION;
            SA_ERROR_CODES_temp_rec.STORE_OVERRIDE_IND      := rec.STORE_OVERRIDE_IND;
            SA_ERROR_CODES_temp_rec.HQ_OVERRIDE_IND         := rec.HQ_OVERRIDE_IND;
            SA_ERROR_CODES_temp_rec.REQUIRED_IND            := rec.REQUIRED_IND;
            SA_ERROR_CODES_temp_rec.SHORT_DESC              := rec.SHORT_DESC;
            IF rec.action                    = STG_SVC_SA_ERROR_CODES.action_new THEN
              SA_ERROR_CODES_ins_rec.extend;
              SA_ERROR_CODES_ins_rec(SA_ERROR_CODES_ins_rec.count()):=SA_ERROR_CODES_temp_rec;
            END IF;
            IF rec.action = STG_SVC_SA_ERROR_CODES.action_mod THEN
              SA_ERROR_CODES_upd_rec.extend;
              SA_ERROR_CODES_upd_rec(SA_ERROR_CODES_upd_rec.count()):=SA_ERROR_CODES_temp_rec;
            END IF;
            IF rec.action = STG_SVC_SA_ERROR_CODES.action_del THEN
              SA_ERROR_CODES_del_rec.extend;
              SA_ERROR_CODES_del_rec(SA_ERROR_CODES_del_rec.count()):=SA_ERROR_CODES_temp_rec;
            END IF;
            END IF;
        END LOOP;
        exec_SA_ERROR_CODES_ins(SA_ERROR_CODES_ins_rec);
        exec_SA_ERROR_CODES_upd(SA_ERROR_CODES_upd_rec);
        exec_SA_ERROR_CODES_del(SA_ERROR_CODES_del_rec);
    END process_SA_ERROR_CODES;
-------------------------------------------------------------------------------
FUNCTION PROCESS_SA_ERROR_CODES_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN       SVC_SA_ERROR_CODES_TL.PROCESS_ID%TYPE,
                                   I_chunk_id        IN       SVC_SA_ERROR_CODES_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                      VARCHAR2(64) := 'CORESVC_SA_ERROR_CODES_TL.PROCESS_SA_ERROR_CODES_TL';
   L_table                        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SA_ERROR_CODES_TL';
   L_trans_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ERROR_CODES_TL';
   L_parent_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_ERROR_CODES';
   L_error                        BOOLEAN;
   L_process_error                BOOLEAN := FALSE;
   L_error_message                RTK_ERRORS.RTK_TEXT%TYPE;
   L_saerrcd_tl_temp_rec          SA_ERROR_CODES_TL%ROWTYPE;
   L_saerr_codes_tl_upd_rst       ROW_SEQ_TAB;
   L_saerr_codes_tl_del_rst       ROW_SEQ_TAB;

   cursor C_SVC_SA_ERROR_CODES_TL(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select pk_sa_error_codes_tl.rowid  as pk_sa_error_codes_tl_rid,
             fk_sa_error_codes.rowid     as fk_sa_error_codes_rid,
             fk_lang.rowid               as fk_lang_rid,
             st.lang,
             st.error_code,
             st.error_desc,
             st.rec_solution,
             st.short_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_sa_error_codes_tl  st,
             sa_error_codes         fk_sa_error_codes,
             sa_error_codes_tl      pk_sa_error_codes_tl,
             lang                   fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.error_code  =  fk_sa_error_codes.error_code (+)
         and st.lang        =  pk_sa_error_codes_tl.lang (+)
         and st.error_code  =  pk_sa_error_codes_tl.error_code (+)
         and st.lang        =  fk_lang.lang (+);
         
   TYPE SVC_sa_error_codes_TL is TABLE OF C_SVC_SA_ERROR_CODES_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_sa_error_codes_tab        SVC_SA_ERROR_CODES_TL;
   
   L_svc_saerr_codes_tl_ins_tab         sa_error_codes_tl_rec_tab         := NEW sa_error_codes_tl_rec_tab();
   L_svc_saerr_codes_tl_upd_tab         sa_error_codes_tl_rec_tab         := NEW sa_error_codes_tl_rec_tab();
   L_svc_saerr_codes_tl_del_tab         sa_error_codes_tl_rec_tab         := NEW sa_error_codes_tl_rec_tab();
   
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   if C_SVC_SA_ERROR_CODES_TL%ISOPEN then
      close C_SVC_SA_ERROR_CODES_TL;
   end if;

   open C_SVC_SA_ERROR_CODES_TL(I_process_id,
                                I_chunk_id);
   LOOP 
      fetch C_SVC_SA_ERROR_CODES_TL bulk collect into L_svc_sa_error_codes_tab limit LP_bulk_fetch_limit;
      if L_svc_sa_error_codes_tab.COUNT > 0 then
         FOR i in L_svc_sa_error_codes_tab.FIRST..L_svc_sa_error_codes_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_sa_error_codes_tab(i).lang = LP_primary_lang and L_svc_sa_error_codes_tab(i).action = stg_svc_sa_error_codes.action_new then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_error_codes_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if action is valid
            if L_svc_sa_error_codes_tab(i).action is NULL
               or L_svc_sa_error_codes_tab(i).action NOT IN (stg_svc_sa_error_codes.action_new, stg_svc_sa_error_codes.action_mod, stg_svc_sa_error_codes.action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_error_codes_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_sa_error_codes_tab(i).action = stg_svc_sa_error_codes.action_new 
               and L_svc_sa_error_codes_tab(i).pk_sa_error_codes_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_error_codes_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_sa_error_codes_tab(i).action IN (stg_svc_sa_error_codes.action_mod, stg_svc_sa_error_codes.action_del) 
               and L_svc_sa_error_codes_tab(i).lang is NOT NULL
               and L_svc_sa_error_codes_tab(i).error_code is NOT NULL 
               and L_svc_sa_error_codes_tab(i).pk_sa_error_codes_TL_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_sa_error_codes_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_sa_error_codes_tab(i).action = stg_svc_sa_error_codes.action_new 
               and L_svc_sa_error_codes_tab(i).error_code is NOT NULL
               and L_svc_sa_error_codes_tab(i).fk_sa_error_codes_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_parent_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_sa_error_codes_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_sa_error_codes_tab(i).action = stg_svc_sa_error_codes.action_new 
               and L_svc_sa_error_codes_tab(i).lang is NOT NULL
               and L_svc_sa_error_codes_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_error_codes_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_sa_error_codes_tab(i).action in (stg_svc_sa_error_codes.action_new, stg_svc_sa_error_codes.action_mod) then
               if L_svc_sa_error_codes_tab(i).error_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_sa_error_codes_tab(i).row_seq,
                              'ERROR_DESC',
                              'ENTER_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_sa_error_codes_tab(i).error_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_error_codes_tab(i).row_seq,
                           'ERROR_CODE',
                           'FIELD_NOT_NULL');
               L_error :=TRUE;
            end if;

            if L_svc_sa_error_codes_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sa_error_codes_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_saerrcd_tl_temp_rec.lang := L_svc_sa_error_codes_tab(i).lang;
               L_saerrcd_tl_temp_rec.error_code := L_svc_sa_error_codes_tab(i).error_code;
               L_saerrcd_tl_temp_rec.error_desc := L_svc_sa_error_codes_tab(i).error_desc;
               L_saerrcd_tl_temp_rec.rec_solution := L_svc_sa_error_codes_tab(i).rec_solution;
               L_saerrcd_tl_temp_rec.short_desc := L_svc_sa_error_codes_tab(i).short_desc;
               L_saerrcd_tl_temp_rec.create_datetime := SYSDATE;
               L_saerrcd_tl_temp_rec.create_id := GET_USER;
               L_saerrcd_tl_temp_rec.last_update_datetime := SYSDATE;
               L_saerrcd_tl_temp_rec.last_update_id := GET_USER;
            
               if L_svc_sa_error_codes_tab(i).action = stg_svc_sa_error_codes.action_new then
                  L_svc_saerr_codes_tl_ins_tab.extend;
                  L_svc_saerr_codes_tl_ins_tab(L_svc_saerr_codes_tl_ins_tab.count()) := L_saerrcd_tl_temp_rec;
               end if; 

               if L_svc_sa_error_codes_tab(i).action = stg_svc_sa_error_codes.action_mod then
                  L_svc_saerr_codes_tl_upd_tab.extend;
                  L_svc_saerr_codes_tl_upd_tab(L_svc_saerr_codes_tl_upd_tab.count()) := L_saerrcd_tl_temp_rec;
                  L_saerr_codes_tl_upd_rst(L_svc_saerr_codes_tl_upd_tab.count()) := L_svc_sa_error_codes_tab(i).row_seq;
               end if; 

               if L_svc_sa_error_codes_tab(i).action = stg_svc_sa_error_codes.action_del then
                  L_svc_saerr_codes_tl_del_tab.extend;
                  L_svc_saerr_codes_tl_del_tab(L_svc_saerr_codes_tl_del_tab.count()) := L_saerrcd_tl_temp_rec;
                  L_saerr_codes_tl_del_rst(L_svc_saerr_codes_tl_del_tab.count()) := L_svc_sa_error_codes_tab(i).row_seq;
               end if; 
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_SA_ERROR_CODES_TL%NOTFOUND;
   END LOOP;
   close C_SVC_SA_ERROR_CODES_TL;

   if EXEC_SA_ERROR_CODES_TL_INS(O_error_message,
                                 L_svc_saerr_codes_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_SA_ERROR_CODES_TL_UPD(O_error_message,
                                 L_svc_saerr_codes_tl_upd_tab,
                                 L_saerr_codes_tl_upd_rst,
                                 I_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_SA_ERROR_CODES_TL_DEL(O_error_message,
                                 L_svc_saerr_codes_tl_del_tab,
                                 L_saerr_codes_tl_del_rst,
                                 I_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SA_ERROR_CODES_TL;
--------------------------------------------------------------------------------
    PROCEDURE clear_staging_data(I_process_id    IN Number)
    IS
    BEGIN
        delete from svc_sa_error_impact where process_id=I_process_id;
        delete from svc_sa_error_codes_tl where process_id=I_process_id;
        delete from svc_sa_error_codes where process_id=I_process_id;
    END;
 -------------------------------------------------------------------------------
---  Name: process
--- Purpose: This called from UI to load data from staging to core tables
-------------------------------------------------------------------------------- 
FUNCTION PROCESS(O_error_message IN OUT rtk_errors.rtk_text%type,
                 I_process_id    IN     Number,
                 O_error_count   OUT    NUMBER) 
RETURN BOOLEAN IS
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

   L_program VARCHAR2(255) := 'CORESVC_SA_ERROR_CODES.PROCESS';
   l_process_status SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_chunk_id NUMBER := 1;

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   Lp_errors_tab := NEW errors_tab_typ();

   PROCESS_SA_ERROR_CODES(I_process_id,L_chunk_id);

   if PROCESS_SA_ERROR_CODES_TL(O_error_message,
                                I_process_id,
                                L_chunk_id) = FALSE then
      return FALSE;
   end if;

   PROCESS_SA_ERROR_IMPACT(I_process_id,L_chunk_id);

   O_error_count := Lp_errors_tab.count();
   forall i IN 1..O_error_count
      insert into svc_admin_upld_er 
           values LP_errors_tab(i);

   Lp_errors_tab := NEW errors_tab_typ();
   
   open  C_GET_ERR_COUNT;
   fetch C_GET_ERR_COUNT into L_err_count;
   close C_GET_ERR_COUNT;

   open  C_GET_WARN_COUNT;
   fetch C_GET_WARN_COUNT into L_warn_count;
   close C_GET_WARN_COUNT;
   
   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                       else L_process_status
                    END),
          action_date =sysdate
    where process_id = I_process_id;
   ---clear the staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   RETURN true;

EXCEPTION
WHEN OTHERS THEN
   CLEAR_STAGING_DATA(I_process_id);
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
   RETURN FALSE;
END PROCESS;

END coresvc_sa_error_codes;
/ 
