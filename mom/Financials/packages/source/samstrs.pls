
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE MISSING_TRAN_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function:  GET_NEW_MISS_TRAN_SEQ
--Purpose:   Retrieves the next missing Transaction sequence number.
-------------------------------------------------------------------------------
FUNCTION GET_NEW_MISS_TRAN_SEQ (O_error_message     IN OUT VARCHAR2,
                                O_new_miss_tran_seq IN OUT SA_MISSING_TRAN.MISS_TRAN_SEQ_NO%TYPE)
   RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function:  DELETE_ALL_MISS_TRAN
--Purpose :  Deletes SA_missing_tran records based on Store Day Seq No, Register
--           or POS Transaction Number.
-------------------------------------------------------------------------------
FUNCTION DELETE_ALL_MISS_TRAN (O_error_message     IN OUT VARCHAR2,
                               I_store_day_seq_no  IN     SA_MISSING_TRAN.STORE_DAY_SEQ_NO%TYPE,
			       	 I_register          IN     SA_MISSING_TRAN.REGISTER%TYPE,
			       	 I_POS_tran_no	   IN	    SA_MISSING_TRAN.TRAN_NO%TYPE)
   RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function:  DELETE_ALL_MISS_TRAN
--Purpose :  Old version of DELETE_ALL_MISS_TRAN, in existence to account for all 
--           current calls to the function that don't have Register or POS 
--           Transaction Number input parameters.
-------------------------------------------------------------------------------
FUNCTION DELETE_ALL_MISS_TRAN (O_error_message     IN OUT VARCHAR2,
                               I_store_day_seq_no  IN     SA_MISSING_TRAN.STORE_DAY_SEQ_NO%TYPE)
   RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function:  UPDATE_STATUS
--Purpose :  Updates sa_missing_tran.status to be 'Added'.
-------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS (O_error_message       IN OUT VARCHAR2,
 			I_missing_tran_seq_no IN     SA_MISSING_TRAN.MISS_TRAN_SEQ_NO%TYPE)
   RETURN BOOLEAN; 
-------------------------------------------------------------------------------
END MISSING_TRAN_SQL;
/
