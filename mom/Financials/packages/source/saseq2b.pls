
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_SEQUENCE2_SQL AS
   GP_table_owner                 VARCHAR2(30) := NULL;
   ---
   GP_bal_group_seq_no_remaining  NUMBER := 0;
   GP_bal_group_seq_no_value      NUMBER := NULL;
   ---
   GP_error_seq_no_remaining      NUMBER := 0;
   GP_error_seq_no_value          NUMBER := NULL;
   ---
   GP_escheat_seq_no_remaining    NUMBER := 0;
   GP_escheat_seq_no_value        NUMBER := NULL;
   ---
   GP_export_seq_no_remaining     NUMBER := 0;
   GP_export_seq_no_value         NUMBER := NULL;
   ---
   GP_miss_tran_seq_no_remaining  NUMBER := 0;
   GP_miss_tran_seq_no_value      NUMBER := NULL;
   ---
   GP_store_day_seq_no_remaining  NUMBER := 0;
   GP_store_day_seq_no_value      NUMBER := NULL;
   ---
   GP_total_seq_no_remaining      NUMBER := 0;
   GP_total_seq_no_value          NUMBER := NULL;
   ---
   GP_tran_seq_no_remaining       NUMBER := 0;
   GP_tran_seq_no_value           NUMBER := NULL;
   ---
   GP_voucher_seq_no_remaining    NUMBER := 0;
   GP_voucher_seq_no_value        NUMBER := NULL;
--------------------------------------------------------------------------------
   ---FUNCTION POPULATE_GLOBALS
   ---
   --This internal package function will populate variables global to the
   --package, including the system_options.table.
--------------------------------------------------------------------------------
FUNCTION POPULATE_GLOBALS (O_error_message IN OUT VARCHAR2)
      RETURN BOOLEAN IS

   cursor C_GET_SEQ_OWNER is
      select table_owner
        from system_options;
   ---
BEGIN
   if GP_table_owner is NULL then
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_SEQ_OWNER','system_options',NULL);
      open C_GET_SEQ_OWNER;
      SQL_LIB.SET_MARK('FETCH','C_GET_SEQ_OWNER','system_options',NULL);
      fetch C_GET_SEQ_OWNER into GP_table_owner;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SEQ_OWNER','system_options',NULL);
      close C_GET_SEQ_OWNER;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.POPULATE_GLOBALS',
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_GLOBALS;
-----------------------------------------------------------------------------------
FUNCTION GET_BAL_GROUP_SEQ(O_error_message IN OUT VARCHAR2,
                           O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_BAL_GROUP_SEQ is
      select SA_BAL_GROUP_NO_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_BAL_GROUP_NO_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_bal_group_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_BAL_GROUP_SEQ','all_sequences',NULL);
      open C_GET_BAL_GROUP_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_BAL_GROUP_SEQ','all_sequences',NULL);
      fetch C_GET_BAL_GROUP_SEQ into GP_bal_group_seq_no_value,
                                     GP_bal_group_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_bal_group_seq_no_value;
   GP_bal_group_seq_no_value := GP_bal_group_seq_no_value + 1;
   GP_bal_group_seq_no_remaining := GP_bal_group_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_BAL_GROUP_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_BAL_GROUP_SEQ;
-----------------------------------------------------------------------------------
FUNCTION GET_ERROR_SEQ(O_error_message IN OUT VARCHAR2,
                       O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_ERROR_SEQ is
      select SA_ERROR_SEQ_NO_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_ERROR_SEQ_NO_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_error_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_ERROR_SEQ','all_sequences',NULL);
      open C_GET_ERROR_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_ERROR_SEQ','all_sequences',NULL);
      fetch C_GET_ERROR_SEQ into GP_error_seq_no_value,
                                 GP_error_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_error_seq_no_value;
   GP_error_seq_no_value := GP_error_seq_no_value + 1;
   GP_error_seq_no_remaining := GP_error_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_ERROR_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_ERROR_SEQ;
-----------------------------------------------------------------------------------
FUNCTION GET_ESCHEAT_SEQ(O_error_message IN OUT VARCHAR2,
                         O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_ESCHEAT_SEQ is
      select SA_ESCHEAT_SEQ_NO_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_ESCHEAT_SEQ_NO_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_escheat_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_ESCHEAT_SEQ','all_sequences',NULL);
      open C_GET_ESCHEAT_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_ESCHEAT_SEQ','all_sequences',NULL);
      fetch C_GET_ESCHEAT_SEQ into GP_escheat_seq_no_value,
                                   GP_escheat_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_escheat_seq_no_value;
   GP_escheat_seq_no_value := GP_escheat_seq_no_value + 1;
   GP_escheat_seq_no_remaining := GP_escheat_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_ESCHEAT_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_ESCHEAT_SEQ;
-----------------------------------------------------------------------------------
FUNCTION GET_EXPORT_SEQ(O_error_message IN OUT VARCHAR2,
                        O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   -- How come 'SA_EXPORT_SEQ_NO_SEQUENCE' is not used?
   cursor C_GET_EXPORT_SEQ is
      select SA_EXPORT_SEQ_NO_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_EXPORT_SEQ_NO_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_export_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_EXPORT_SEQ','all_sequences',NULL);
      open C_GET_EXPORT_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_EXPORT_SEQ','all_sequences',NULL);
      fetch C_GET_EXPORT_SEQ into GP_export_seq_no_value,
                                  GP_export_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_export_seq_no_value;
   GP_export_seq_no_value := GP_export_seq_no_value + 1;
   GP_export_seq_no_remaining := GP_export_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_EXPORT_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_EXPORT_SEQ;
-----------------------------------------------------------------------------------
FUNCTION GET_MISS_TRAN_SEQ(O_error_message IN OUT VARCHAR2,
                           O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_MISS_TRAN_SEQ is
      select SA_MISS_TRAN_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_MISS_TRAN_SEQ_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_miss_tran_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_MISS_TRAN_SEQ','all_sequences',NULL);
      open C_GET_MISS_TRAN_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_MISS_TRAN_SEQ','all_sequences',NULL);
      fetch C_GET_MISS_TRAN_SEQ into GP_miss_tran_seq_no_value,
                                     GP_miss_tran_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_miss_tran_seq_no_value;
   GP_miss_tran_seq_no_value := GP_miss_tran_seq_no_value + 1;
   GP_miss_tran_seq_no_remaining := GP_miss_tran_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_MISS_TRAN_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_MISS_TRAN_SEQ;
-----------------------------------------------------------------------------------
FUNCTION GET_STORE_DAY_SEQ(O_error_message IN OUT VARCHAR2,
                           O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_STORE_DAY_SEQ is
      select SA_STORE_DAY_SEQ_NO_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_STORE_DAY_SEQ_NO_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_store_day_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_DAY_SEQ','all_sequences',NULL);
      open C_GET_STORE_DAY_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_DAY_SEQ','all_sequences',NULL);
      fetch C_GET_STORE_DAY_SEQ into GP_store_day_seq_no_value,
                                     GP_store_day_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_store_day_seq_no_value;
   GP_store_day_seq_no_value := GP_store_day_seq_no_value + 1;
   GP_store_day_seq_no_remaining := GP_store_day_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_STORE_DAY_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_STORE_DAY_SEQ;
-----------------------------------------------------------------------------------
FUNCTION GET_TOTAL_SEQ(O_error_message IN OUT VARCHAR2,
                       O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_TOTAL_SEQ is
      select SA_TOTAL_SEQ_NO_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_TOTAL_SEQ_NO_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_total_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_SEQ','all_sequences',NULL);
      open C_GET_TOTAL_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_SEQ','all_sequences',NULL);
      fetch C_GET_TOTAL_SEQ into GP_total_seq_no_value,
                                 GP_total_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_total_seq_no_value;
   GP_total_seq_no_value := GP_total_seq_no_value + 1;
   GP_total_seq_no_remaining := GP_total_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_TOTAL_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_TOTAL_SEQ;
-----------------------------------------------------------------------------------
FUNCTION GET_TRAN_SEQ(O_error_message IN OUT VARCHAR2,
                      O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_TRAN_SEQ is
      select SA_TRAN_SEQ_NO_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_TRAN_SEQ_NO_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_tran_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_SEQ','all_sequences',NULL);
      open C_GET_TRAN_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_TRAN_SEQ','all_sequences',NULL);
      fetch C_GET_TRAN_SEQ into GP_tran_seq_no_value,
                                GP_tran_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_tran_seq_no_value;
   GP_tran_seq_no_value := GP_tran_seq_no_value + 1;
   GP_tran_seq_no_remaining := GP_tran_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_TRAN_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_TRAN_SEQ;
-----------------------------------------------------------------------------------
FUNCTION GET_VOUCHER_SEQ(O_error_message IN OUT VARCHAR2,
                         O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_VOUCHER_SEQ is
      select SA_VOUCHER_SEQ_NO_SEQUENCE.NEXTVAL, all_sequences.increment_by
        from all_sequences
       where all_sequences.sequence_name = 'SA_VOUCHER_SEQ_NO_SEQUENCE'
         and all_sequences.sequence_owner = GP_table_owner;

BEGIN
   if POPULATE_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_voucher_seq_no_remaining = 0 then
      SQL_LIB.SET_MARK('OPEN','C_GET_VOUCHER_SEQ','all_sequences',NULL);
      open C_GET_VOUCHER_SEQ;
      SQL_LIB.SET_MARK('FETCH','C_GET_VOUCHER_SEQ','all_sequences',NULL);
      fetch C_GET_VOUCHER_SEQ into GP_voucher_seq_no_value,
                                   GP_voucher_seq_no_remaining;
   end if;
   ---
   O_seq_value := GP_voucher_seq_no_value;
   GP_voucher_seq_no_value := GP_voucher_seq_no_value + 1;
   GP_voucher_seq_no_remaining := GP_voucher_seq_no_remaining - 1;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_SEQUENCE2_SQL.GET_VOUCHER_SEQ',
                                            to_char(SQLCODE));
      O_seq_value := NULL;
      return FALSE;
END GET_VOUCHER_SEQ;
--------------------------------------------------------------------------------
END SA_SEQUENCE2_SQL;
/
