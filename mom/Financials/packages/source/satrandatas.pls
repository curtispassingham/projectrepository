CREATE OR REPLACE PACKAGE SA_TRANDATA_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------------------------------
TYPE sa_trandata_rec IS RECORD(store_day_seq_no     SA_GL_REF_DATA.STORE_DAY_SEQ_NO%TYPE,
                               store                SA_GL_REF_DATA.STORE%TYPE,
                               day                  SA_GL_REF_DATA.DAY%TYPE,
                               acct_date            SA_GL_REF_DATA.ACCT_DATE%TYPE,
                               processed_date       SA_GL_REF_DATA.PROCESSED_DATE%TYPE,
                               currency_code        SA_GL_REF_DATA.CURRENCY_CODE%TYPE,
                               set_of_books_id      SA_GL_REF_DATA.SET_OF_BOOKS_ID%TYPE,
                               update_id            SA_GL_REF_DATA.UPDATE_ID%TYPE,
                               total_value          SA_GL_REF_DATA.TOTAL_VALUE%TYPE,
                               total_id             SA_GL_REF_DATA.TOTAL_ID%TYPE,
                               attrib1              SA_GL_REF_DATA.ATTRIB1%TYPE,
                               attrib2              SA_GL_REF_DATA.ATTRIB2%TYPE,
                               period               SA_GL_REF_DATA.PERIOD%TYPE,
                               reference_trace_id   SA_GL_REF_DATA.REFERENCE_TRACE_ID%TYPE,
                               error_message        RTK_ERRORS.RTK_TEXT%TYPE,
                               return_code          VARCHAR2(5));

TYPE sa_trandata_tbl IS TABLE OF sa_trandata_rec INDEX BY BINARY_INTEGER;
--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE
--- Purpose:        This procedure will query SA_GL_REF_DATA table and populate the above declared 
---                 table of records that will be used as the 'base table' in the Sales Audit Transaction Data From.
--------------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_sa_trandata_tbl      IN OUT   SA_TRANDATA_SQL.SA_TRANDATA_TBL,
                           I_store                 IN       SA_GL_REF_DATA.STORE%TYPE,
                           I_processed_date_start  IN       SA_GL_REF_DATA.PROCESSED_DATE%TYPE,
                           I_processed_date_end    IN       SA_GL_REF_DATA.PROCESSED_DATE%TYPE,
                           I_set_of_books_id       IN       SA_GL_REF_DATA.SET_OF_BOOKS_ID%TYPE,
                           I_acct_date_start       IN       SA_GL_REF_DATA.ACCT_DATE%TYPE,
                           I_acct_date_end         IN       SA_GL_REF_DATA.ACCT_DATE%TYPE,
                           I_total_type            IN       SA_GL_REF_DATA.TOTAL_ID%TYPE,
                           I_store_day_seq_no      IN       SA_GL_REF_DATA.STORE_DAY_SEQ_NO%TYPE,
                           I_reference_trace_id    IN       SA_GL_REF_DATA.REFERENCE_TRACE_ID%TYPE);
--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: GET_MIN_DATE
--- Purpose:        This procedure will query SA_GL_REF_DATA table to get the minimum possible date for 
---                 processed_date or acct_date
--------------------------------------------------------------------------------------------------------------------
FUNCTION GET_MIN_DATE (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_min_date              IN OUT   SA_GL_REF_DATA.PROCESSED_DATE%TYPE,
                       I_param                 IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------------
END SA_TRANDATA_SQL;
/
