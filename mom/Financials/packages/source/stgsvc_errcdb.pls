create or replace PACKAGE BODY STG_SVC_SA_ERROR_CODES
AS
	Type s9t_errors_tab_typ IS TABLE OF s9t_errors%rowtype;
	Lp_s9t_errors_tab s9t_errors_tab_typ;
    LP_primary_lang    LANG.LANG%TYPE;
	-------------------------------------------------------
	FUNCTION populate_lists(
	  O_error_message IN OUT rtk_errors.rtk_text%type,
	  I_file_id       IN NUMBER)
	RETURN BOOLEAN;
	-------------------------------------------------------
	PROCEDURE write_s9t_error(
	  I_file_id IN s9t_errors.file_id%type,
	  I_sheet   IN VARCHAR2,
	  I_row_seq IN NUMBER,
	  I_col     IN VARCHAR2,
	  I_sqlcode IN NUMBER,
	  I_sqlerrm IN VARCHAR2)
	IS
	BEGIN
	Lp_s9t_errors_tab.extend();
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).FILE_ID              := I_file_id;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_SEQ_NO         := s9t_errors_seq.nextval;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).TEMPLATE_KEY         := STG_SVC_SA_ERROR_CODES.template_key;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).WKSHT_KEY            := I_sheet;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).COLUMN_KEY           := I_col;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ROW_SEQ              := I_row_seq;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_ID            := GET_USER;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_DATETIME      := sysdate;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_ID       := GET_USER;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_DATETIME := sysdate;
	END write_s9t_error;
	----------------------------------------------------------------------------------
	PROCEDURE populate_names (I_file_id NUMBER)
	IS
	 l_sheets               S9T_PKG.NAMES_MAP_TYP;
	 SA_ERROR_IMPACT_COLS   S9T_PKG.NAMES_MAP_TYP;
	 SA_ERROR_CODES_COLS    S9T_PKG.NAMES_MAP_TYP;
	 SA_ERROR_CODES_TL_COLS S9T_PKG.NAMES_MAP_TYP;
	BEGIN
	 l_sheets                         :=s9t_pkg.get_sheet_names(I_file_id); 
	 SA_ERROR_CODES_cols              :=s9t_pkg.get_col_names(I_file_id,SA_ERROR_CODES_sheet);
	 SA_ERROR_CODES$Action            := SA_ERROR_CODES_cols('ACTION');
	 SA_ERROR_CODES$REQUIRED_IND      := SA_ERROR_CODES_cols('REQUIRED_IND');
	 SA_ERROR_CODES$HQ_OVERRIDE_IND   := SA_ERROR_CODES_cols('HQ_OVERRIDE_IND');
	 SA_ERROR_CODES$REC_SOLUTION      := SA_ERROR_CODES_cols('REC_SOLUTION');
	 SA_ERROR_CODES$TARGET_TAB        := SA_ERROR_CODES_cols('TARGET_TAB');
	 SA_ERROR_CODES$TARGET_FORM       := SA_ERROR_CODES_cols('TARGET_FORM');
	 SA_ERROR_CODES$ERROR_DESC        := SA_ERROR_CODES_cols('ERROR_DESC');
	 SA_ERROR_CODES$ERROR_CODE        := SA_ERROR_CODES_cols('ERROR_CODE');
     SA_ERROR_CODES$SHORT_DESC        := SA_ERROR_CODES_cols('SHORT_DESC');
     
	 SA_ERROR_CODES_TL_cols              := s9t_pkg.get_col_names(I_file_id,SA_ERROR_CODES_TL_sheet);
	 SA_ERROR_CODES_TL$Action            := SA_ERROR_CODES_TL_cols('ACTION');
     SA_ERROR_CODES_TL$LANG              := SA_ERROR_CODES_TL_cols('LANG');
	 SA_ERROR_CODES_TL$REC_SOLUTION      := SA_ERROR_CODES_TL_cols('REC_SOLUTION');
	 SA_ERROR_CODES_TL$ERROR_DESC        := SA_ERROR_CODES_TL_cols('ERROR_DESC');
	 SA_ERROR_CODES_TL$ERROR_CODE        := SA_ERROR_CODES_TL_cols('ERROR_CODE');
	 SA_ERROR_CODES_TL$SHORT_DESC        := SA_ERROR_CODES_TL_cols('SHORT_DESC');

	 SA_ERROR_IMPACT_cols             :=s9t_pkg.get_col_names(I_file_id,SA_ERROR_IMPACT_sheet);
	 SA_ERROR_IMPACT$Action           := SA_ERROR_IMPACT_cols('ACTION');
	 SA_ERROR_IMPACT$REQUIRED_IND     := SA_ERROR_IMPACT_cols('REQUIRED_IND');
	 SA_ERROR_IMPACT$SYSTEM_CODE      := SA_ERROR_IMPACT_cols('SYSTEM_CODE');
	 SA_ERROR_IMPACT$ERROR_CODE       := SA_ERROR_IMPACT_cols('ERROR_CODE');
	END populate_names;
	-----------------------------------------------------------------------------------
	PROCEDURE populate_SA_ERROR_IMPACT(I_file_id IN NUMBER)
	IS
	BEGIN
	 insert
	 INTO TABLE
		(select ss.s9t_rows
		  from s9t_folder sf,
			 TABLE(sf.s9t_file_obj.sheets) ss
		  where sf.file_id  = I_file_id
		  and ss.sheet_name = SA_ERROR_IMPACT_sheet
		)
	 select s9t_row(s9t_cells( stg_svc_sa_error_codes.action_mod
									  ,ERROR_CODE
									  ,SYSTEM_CODE
									  ,REQUIRED_IND))
	   from SA_ERROR_IMPACT ;
	END populate_SA_ERROR_IMPACT;
	-----------------------------------------------------------------------------------
	PROCEDURE POPULATE_SA_ERROR_CODES(I_file_id IN NUMBER)
	IS
	BEGIN
	 insert
	 INTO TABLE
		(select ss.s9t_rows
		  from s9t_folder sf,
			 TABLE(sf.s9t_file_obj.sheets) ss
		  where sf.file_id  = I_file_id
		  and ss.sheet_name = SA_ERROR_CODES_sheet
		)
	 select s9t_row(s9t_cells( stg_svc_sa_error_codes.action_mod
                              ,ERROR_CODE
                              ,ERROR_DESC
                              ,SHORT_DESC
                              ,TARGET_FORM
                              ,TARGET_TAB
                              ,REC_SOLUTION
                              ,REQUIRED_IND
                              ,HQ_OVERRIDE_IND
                              ,STORE_OVERRIDE_IND ))
	 from SA_ERROR_CODES ;
	END POPULATE_SA_ERROR_CODES;
	-----------------------------------------------------------------------------------
	PROCEDURE POPULATE_SA_ERROR_CODES_TL(I_file_id IN NUMBER)
	IS
	BEGIN
	 insert
	 INTO TABLE
		(select ss.s9t_rows
		  from s9t_folder sf,
			 TABLE(sf.s9t_file_obj.sheets) ss
		  where sf.file_id  = I_file_id
		  and ss.sheet_name = SA_ERROR_CODES_TL_sheet
		)
	 select s9t_row(s9t_cells( stg_svc_sa_error_codes.action_mod
                              ,LANG
                              ,ERROR_CODE
                              ,ERROR_DESC
                              ,REC_SOLUTION
                              ,SHORT_DESC ))
	 from sa_error_codes_tl ;
	END POPULATE_SA_ERROR_CODES_TL;
	--------------------------------------------------------------------------------------
	PROCEDURE init_s9t(O_file_id IN OUT NUMBER)
	IS
		l_file s9t_file;
		l_file_name s9t_folder.file_name%type;

	BEGIN
		l_file              := NEW s9t_file();
		O_file_id           := s9t_folder_seq.nextval;
		l_file.file_id      := O_file_id;
		l_file_name         := STG_SVC_SA_ERROR_CODES.template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
		l_file.file_name    := l_file_name;
		l_file.template_key := STG_SVC_SA_ERROR_CODES.template_key;
		l_file.user_lang    := GET_USER_LANG;
    l_file.add_sheet(SA_ERROR_CODES_sheet);
		l_file.sheets(l_file.get_sheet_index(SA_ERROR_CODES_sheet)).column_headers := s9t_cells(  'ACTION'
                                                                                                 ,'ERROR_CODE'
                                                                                                 ,'ERROR_DESC'
                                                                                                 ,'SHORT_DESC'
                                                                                                 ,'TARGET_FORM'
                                                                                                 ,'TARGET_TAB'
                                                                                                 ,'REC_SOLUTION'
                                                                                                 ,'REQUIRED_IND'
                                                                                                 ,'HQ_OVERRIDE_IND'
                                                                                                 ,'STORE_OVERRIDE_IND');
                                                                                                                             
		l_file.add_sheet(SA_ERROR_CODES_TL_sheet);
		l_file.sheets(l_file.get_sheet_index(SA_ERROR_CODES_TL_sheet)).column_headers := s9t_cells(  'ACTION'
																															 ,'LANG'
                                                                                                                             ,'ERROR_CODE'
																															 ,'ERROR_DESC'
                                                                                                                             ,'REC_SOLUTION'
																															 ,'SHORT_DESC');
		l_file.add_sheet(SA_ERROR_IMPACT_sheet);
		l_file.sheets(l_file.get_sheet_index(SA_ERROR_IMPACT_sheet)).column_headers := s9t_cells( 'ACTION'
																															 ,'ERROR_CODE'
																															 ,'SYSTEM_CODE'
																															 ,'REQUIRED_IND');
		s9t_pkg.save_obj(l_file);
	END init_s9t;
	-------------------------------------------------------------------------------------------------------------------------------------
	FUNCTION create_s9t(O_error_message     IN OUT rtk_errors.rtk_text%type,
							 O_file_id           IN OUT s9t_folder.file_id%type,
							 I_template_only_ind IN CHAR DEFAULT 'N') RETURN BOOLEAN IS
	 l_file s9t_file;
	 L_program VARCHAR2(255):='STG_SVC_SA_ERROR_CODES.CREATE_S9T'; 
	BEGIN
		init_s9t(O_file_id);
		---populate the column lists
		if populate_lists(O_error_message,O_file_id)=false then
		  return FALSE;
		end if;
		if I_template_only_ind = 'N' then
		---populate the data
		   populate_SA_ERROR_IMPACT(O_file_id);
           populate_SA_ERROR_CODES_TL(O_file_id);
		   populate_SA_ERROR_CODES(O_file_id);
		   COMMIT;
		end if;
		s9t_pkg.translate_to_user_lang(O_file_id);
		-- Apply template
		s9t_pkg.apply_template(O_file_id,STG_SVC_SA_ERROR_CODES.template_key);
		l_file:=s9t_file(O_file_id);
		if s9t_pkg.code2desc(O_error_message,
		                     'RSAEC',
									       l_file)=FALSE then
       return FALSE;
		end if;
		s9t_pkg.save_obj(l_file);
		s9t_pkg.update_ods(l_file);
		COMMIT;
		return TRUE;
	EXCEPTION
	  when OTHERS then
		  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
		  return FALSE;
	END create_s9t;
	PROCEDURE process_s9t_SA_ERROR_IMPACT(I_file_id    IN s9t_folder.file_id%type,
													 I_process_id IN SVC_SA_ERROR_IMPACT.process_id%type)
	IS
	Type svc_SA_ERROR_IMPACT_col_typ
	IS
		TABLE OF SVC_SA_ERROR_IMPACT%rowtype;
		l_temp_rec SVC_SA_ERROR_IMPACT%rowtype;
		svc_SA_ERROR_IMPACT_col svc_SA_ERROR_IMPACT_col_typ :=NEW svc_SA_ERROR_IMPACT_col_typ();
		l_process_id SVC_SA_ERROR_IMPACT.process_id%type;
		l_error BOOLEAN:=FALSE;
		l_default_rec SVC_SA_ERROR_IMPACT%rowtype;
    L_table        VARCHAR2(30)  := 'SVC_SA_ERROR_IMPACT';

		cursor c_mandatory_ind IS
		  select ERROR_CODE_mi,SYSTEM_CODE_mi, REQUIRED_IND_mi,  1 as dummy
			 from (select column_key,mandatory
						from s9t_tmpl_cols_def
					  where template_key = STG_SVC_SA_ERROR_CODES.template_key
						 and wksht_key    ='SA_ERROR_IMPACT') 
			PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'ERROR_CODE' AS ERROR_CODE,'SYSTEM_CODE' AS SYSTEM_CODE,'REQUIRED_IND' AS REQUIRED_IND,
																			null as dummy));
		l_mi_rec c_mandatory_ind%rowtype;
		dml_errors EXCEPTION;
		PRAGMA exception_init(dml_errors, -24381);
		l_pk_columns varchar2(255):='error_code,system_code';
		l_error_code number;
		l_error_msg rtk_errors.rtk_text%type;
		l_count number;
	BEGIN

	 -- Get default values.
		FOR rec IN(select REQUIRED_IND_dv,SYSTEM_CODE_dv, ERROR_CODE_dv,null as dummy
					  from (select column_key,default_value from s9t_tmpl_cols_def
								where template_key = STG_SVC_SA_ERROR_CODES.template_key
								  and wksht_key    = 'SA_ERROR_IMPACT')
					 PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('ERROR_CODE' AS ERROR_CODE,'SYSTEM_CODE' AS SYSTEM_CODE,'REQUIRED_IND' AS REQUIRED_IND,
																			null as dummy)))
		LOOP  
		  BEGIN  
			  l_default_rec.REQUIRED_IND := rec.REQUIRED_IND_dv;  
		  EXCEPTION  
			  when others then  
				  write_s9t_error(I_file_id,'SA_ERROR_IMPACT',NULL,'REQUIRED_IND','INV_DEFAULT',SQLERRM);  
		  END;
		  BEGIN  
			  l_default_rec.SYSTEM_CODE := rec.SYSTEM_CODE_dv;  
		  EXCEPTION  
			  when others then  
				  write_s9t_error(I_file_id,'SA_ERROR_IMPACT',NULL,'SYSTEM_CODE','INV_DEFAULT',SQLERRM);  
		  END;
		  BEGIN  
			  l_default_rec.ERROR_CODE := rec.ERROR_CODE_dv;  
		  EXCEPTION  
			  when others then  
				  write_s9t_error(I_file_id,'SA_ERROR_IMPACT',NULL,'ERROR_CODE','INV_DEFAULT',SQLERRM);  
		  END;
		END LOOP;
		--Get mandatory indicators
		OPEN C_mandatory_ind;
		FETCH C_mandatory_ind
		INTO l_mi_rec;
		CLOSE C_mandatory_ind;

		FOR rec IN
		(select r.get_cell(SA_ERROR_IMPACT$Action)      AS Action,
		r.get_cell(SA_ERROR_IMPACT$REQUIRED_IND)              AS REQUIRED_IND,
		r.get_cell(SA_ERROR_IMPACT$SYSTEM_CODE)              AS SYSTEM_CODE,
		upper(r.get_cell(SA_ERROR_IMPACT$ERROR_CODE) )             AS ERROR_CODE,
		r.get_row_seq()                             AS row_seq
		from s9t_folder sf,
		TABLE(sf.s9t_file_obj.sheets) ss,
		TABLE(ss.s9t_rows) r
		where sf.file_id  = I_file_id
		and ss.sheet_name = sheet_name_trans(SA_ERROR_IMPACT_sheet)
		)
		LOOP

		l_temp_rec.process_id        := I_process_id;
		l_temp_rec.chunk_id          := 1;
		l_temp_rec.row_seq           := rec.row_seq;
		l_temp_rec.process$status    := 'N';
		l_temp_rec.create_id         := GET_USER;
		l_temp_rec.last_upd_id       := GET_USER;
		l_temp_rec.create_datetime   := sysdate;
		l_temp_rec.last_upd_datetime := sysdate;
		l_error := FALSE;
		BEGIN
		  l_temp_rec.Action := rec.Action;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_IMPACT_sheet,rec.row_seq,'ACTION',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.REQUIRED_IND := rec.REQUIRED_IND;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_IMPACT_sheet,rec.row_seq,'REQUIRED_IND',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.SYSTEM_CODE := rec.SYSTEM_CODE;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_IMPACT_sheet,rec.row_seq,'SYSTEM_CODE',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.ERROR_CODE := rec.ERROR_CODE;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_IMPACT_sheet,rec.row_seq,'ERROR_CODE',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		if rec.action = STG_SVC_SA_ERROR_CODES.action_new then
			l_temp_rec.REQUIRED_IND := NVL( l_temp_rec.REQUIRED_IND,l_default_rec.REQUIRED_IND);
			l_temp_rec.SYSTEM_CODE := NVL( l_temp_rec.SYSTEM_CODE,l_default_rec.SYSTEM_CODE);
			l_temp_rec.ERROR_CODE := upper(NVL( l_temp_rec.ERROR_CODE,l_default_rec.ERROR_CODE));

		end if;
		if not (
				  l_temp_rec.SYSTEM_CODE is not null and
				  l_temp_rec.ERROR_CODE is not null and
				  1 = 1
				  )then
			write_s9t_error(I_file_id,SA_ERROR_IMPACT_sheet,rec.row_seq,NULL,NULL,sql_lib.create_msg('PK_COLS_REQUIRED',l_pk_columns));
			l_error := true;
		end if;
		IF NOT l_error THEN
		  svc_SA_ERROR_IMPACT_col.extend();
		  svc_SA_ERROR_IMPACT_col(svc_SA_ERROR_IMPACT_col.count()):=l_temp_rec;
		end if;
		END LOOP;
		BEGIN
		forall i IN 1..svc_SA_ERROR_IMPACT_col.count SAVE EXCEPTIONS Merge INTO SVC_SA_ERROR_IMPACT st USING
		(select
		(CASE
		  WHEN l_mi_rec.REQUIRED_IND_mi    = 'N'
		  and svc_SA_ERROR_IMPACT_col(i).action = STG_SVC_SA_ERROR_CODES.action_mod
		  and s1.REQUIRED_IND             IS NULL
		  THEN mt.REQUIRED_IND
		  ELSE s1.REQUIRED_IND
		END) AS REQUIRED_IND,
		(CASE
		  WHEN l_mi_rec.SYSTEM_CODE_mi    = 'N'
		  and svc_SA_ERROR_IMPACT_col(i).action = STG_SVC_SA_ERROR_CODES.action_mod
		  and s1.SYSTEM_CODE             IS NULL
		  THEN mt.SYSTEM_CODE
		  ELSE s1.SYSTEM_CODE
		END) AS SYSTEM_CODE,
		(CASE
		  WHEN l_mi_rec.ERROR_CODE_mi    = 'N'
		  and svc_SA_ERROR_IMPACT_col(i).action = STG_SVC_SA_ERROR_CODES.action_mod
		  and s1.ERROR_CODE             IS NULL
		  THEN mt.ERROR_CODE
		  ELSE s1.ERROR_CODE
		END) AS ERROR_CODE,
		null as dummy
		from (select
		 svc_SA_ERROR_IMPACT_col(i).REQUIRED_IND AS REQUIRED_IND,
		 svc_SA_ERROR_IMPACT_col(i).SYSTEM_CODE AS SYSTEM_CODE,
		 svc_SA_ERROR_IMPACT_col(i).ERROR_CODE AS ERROR_CODE,
		 null as dummy
		from dual
		) s1,
		SA_ERROR_IMPACT mt
		where
		mt.SYSTEM_CODE (+)     = s1.SYSTEM_CODE   and
		mt.ERROR_CODE (+)     = s1.ERROR_CODE   and
		1 = 1
		) sq ON (
		st.SYSTEM_CODE      = sq.SYSTEM_CODE and
		st.ERROR_CODE      = sq.ERROR_CODE and
		svc_SA_ERROR_IMPACT_col(i).ACTION IN (STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_del)
		)
		WHEN matched THEN
		update
		set PROCESS_ID      = svc_SA_ERROR_IMPACT_col(i).PROCESS_ID ,
		CHUNK_ID          = svc_SA_ERROR_IMPACT_col(i).CHUNK_ID ,
		ROW_SEQ           = svc_SA_ERROR_IMPACT_col(i).ROW_SEQ ,
		ACTION            = svc_SA_ERROR_IMPACT_col(i).ACTION ,
		PROCESS$STATUS    = svc_SA_ERROR_IMPACT_col(i).PROCESS$STATUS ,
		REQUIRED_IND              = sq.REQUIRED_IND ,
		CREATE_ID         = svc_SA_ERROR_IMPACT_col(i).CREATE_ID ,
		CREATE_DATETIME   = svc_SA_ERROR_IMPACT_col(i).CREATE_DATETIME ,
		LAST_UPD_ID       = svc_SA_ERROR_IMPACT_col(i).LAST_UPD_ID ,
		LAST_UPD_DATETIME = svc_SA_ERROR_IMPACT_col(i).LAST_UPD_DATETIME WHEN NOT matched THEN
		insert
		(
		  PROCESS_ID ,
		  CHUNK_ID ,
		  ROW_SEQ ,
		  ACTION ,
		  PROCESS$STATUS ,
		  REQUIRED_IND ,
		  SYSTEM_CODE ,
		  ERROR_CODE ,
		  CREATE_ID ,
		  CREATE_DATETIME ,
		  LAST_UPD_ID ,
		  LAST_UPD_DATETIME
		)
		VALUES
		(
		  svc_SA_ERROR_IMPACT_col(i).PROCESS_ID ,
		  svc_SA_ERROR_IMPACT_col(i).CHUNK_ID ,
		  svc_SA_ERROR_IMPACT_col(i).ROW_SEQ ,
		  svc_SA_ERROR_IMPACT_col(i).ACTION ,
		  svc_SA_ERROR_IMPACT_col(i).PROCESS$STATUS ,
		  sq.REQUIRED_IND ,
		  sq.SYSTEM_CODE ,
		  sq.ERROR_CODE ,
		  svc_SA_ERROR_IMPACT_col(i).CREATE_ID ,
		  svc_SA_ERROR_IMPACT_col(i).CREATE_DATETIME ,
		  svc_SA_ERROR_IMPACT_col(i).LAST_UPD_ID ,
		  svc_SA_ERROR_IMPACT_col(i).LAST_UPD_DATETIME
		);
	 
	 EXCEPTION
		 WHEN DML_ERRORS THEN
			FOR i IN 1..sql%bulk_exceptions.count
			LOOP
				l_error_code:=sql%bulk_exceptions(i).error_code;
				if l_error_code=1 then
				  l_error_code:=null;
				  l_error_msg:=sql_lib.create_msg('DUP_REC_EXISTS',l_pk_columns,l_table);
				end if;
				write_s9t_error
				(
				 I_file_id,SA_ERROR_IMPACT_sheet,svc_SA_ERROR_IMPACT_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,l_error_code,l_error_msg
				)
				;
			END LOOP;
	 END;
	END process_s9t_SA_ERROR_IMPACT;
	PROCEDURE process_s9t_SA_ERROR_CODES
	 (
		I_file_id    IN s9t_folder.file_id%type,
		I_process_id IN SVC_SA_ERROR_CODES.process_id%type
	 )
	IS
		Type svc_SA_ERROR_CODES_col_typ
		IS
		TABLE OF SVC_SA_ERROR_CODES%rowtype;
		l_temp_rec SVC_SA_ERROR_CODES%rowtype;
		svc_SA_ERROR_CODES_col svc_SA_ERROR_CODES_col_typ :=NEW svc_SA_ERROR_CODES_col_typ();
		l_process_id SVC_SA_ERROR_CODES.process_id%type;
		l_error BOOLEAN:=FALSE;
		l_default_rec SVC_SA_ERROR_CODES%rowtype;
    L_table        VARCHAR2(30)  := 'SVC_SA_ERROR_CODES';
		cursor c_mandatory_ind
		IS
		select
		  REQUIRED_IND_mi,
		  HQ_OVERRIDE_IND_mi,
		  STORE_OVERRIDE_IND_mi,
		  REC_SOLUTION_mi,
		  TARGET_TAB_mi,
		  TARGET_FORM_mi,
		  ERROR_DESC_mi,
		  ERROR_CODE_mi,
		  SHORT_DESC_mi,
		  1 as dummy
		from
		  (select column_key,
			 mandatory
		  from s9t_tmpl_cols_def
		  where template_key                              = STG_SVC_SA_ERROR_CODES.template_key
		  and wksht_key                                   = 'SA_ERROR_CODES'
		  ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('SHORT_DESC' AS SHORT_DESC, 'ERROR_CODE' AS ERROR_CODE,'ERROR_DESC' AS ERROR_DESC,'TARGET_FORM' AS TARGET_FORM, 'TARGET_TAB' AS TARGET_TAB,'REC_SOLUTION' AS REC_SOLUTION,
								  'REQUIRED_IND' AS REQUIRED_IND,
								  'HQ_OVERRIDE_IND' AS HQ_OVERRIDE_IND,
								  'STORE_OVERRIDE_IND' AS STORE_OVERRIDE_IND,null as dummy));
		l_mi_rec c_mandatory_ind%rowtype;
		dml_errors EXCEPTION;
		PRAGMA exception_init(dml_errors, -24381);
		l_pk_columns varchar2(255) :='error_code';
		l_error_msg rtk_errors.rtk_text%type;
		l_error_code number;
		l_count number;
		BEGIN
		-- Get default values.
		FOR rec IN
		(select
			  REQUIRED_IND_dv,
			  HQ_OVERRIDE_IND_dv,
			  STORE_OVERRIDE_IND_dv,
			  REC_SOLUTION_dv,
			  TARGET_TAB_dv,
			  TARGET_FORM_dv,
			  ERROR_DESC_dv,
			  ERROR_CODE_dv,
			  SHORT_DESC_dv,
		null as dummy
		from
		(select column_key,
		  default_value
		from s9t_tmpl_cols_def
		where template_key                                  = STG_SVC_SA_ERROR_CODES.template_key
		and wksht_key                                       = 'SA_ERROR_CODES'
		) PIVOT (MAX(default_value) AS dv FOR (column_key) IN (
				'SHORT_DESC' AS SHORT_DESC, 'ERROR_CODE' AS ERROR_CODE,'ERROR_DESC' AS ERROR_DESC,'TARGET_FORM' AS TARGET_FORM, 'TARGET_TAB' AS TARGET_TAB,'REC_SOLUTION' AS REC_SOLUTION,
								  'REQUIRED_IND' AS REQUIRED_IND,
								  'HQ_OVERRIDE_IND' AS HQ_OVERRIDE_IND,
								  'STORE_OVERRIDE_IND' AS STORE_OVERRIDE_IND,null as dummy))
		)

		LOOP  
		 BEGIN  
			 l_default_rec.REQUIRED_IND := rec.REQUIRED_IND_dv;  
		 EXCEPTION  
			when others then  
				write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'REQUIRED_IND','INV_DEFAULT',SQLERRM);  
		 END;
		 BEGIN  
			 l_default_rec.HQ_OVERRIDE_IND := rec.HQ_OVERRIDE_IND_dv;  
		 EXCEPTION  
			when others then  
				write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'HQ_OVERRIDE_IND','INV_DEFAULT',SQLERRM);  
		 END;
		 BEGIN
			 l_default_rec.STORE_OVERRIDE_IND := rec.STORE_OVERRIDE_IND_dv;
		 EXCEPTION
			 when others then
				 write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'STORE_OVERRIDE_IND','INV_DEFAULT',SQLERRM);
		 END;
		 BEGIN
			 l_default_rec.REC_SOLUTION := rec.REC_SOLUTION_dv;
		 EXCEPTION
			 when others then
				 write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'REC_SOLUTION','INV_DEFAULT',SQLERRM);
		 END;
		 BEGIN
			 l_default_rec.TARGET_TAB := rec.TARGET_TAB_dv;
		 EXCEPTION
			 when others then
				 write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'TARGET_TAB','INV_DEFAULT',SQLERRM);
		 END;
		 BEGIN
			 l_default_rec.TARGET_FORM := rec.TARGET_FORM_dv;
		 EXCEPTION
			 when others then
				 write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'TARGET_FORM','INV_DEFAULT',SQLERRM);
		 END;
		 BEGIN
			 l_default_rec.ERROR_DESC := rec.ERROR_DESC_dv;
		 EXCEPTION
			 when others then
				 write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'ERROR_DESC','INV_DEFAULT',SQLERRM);
		 END;
		 BEGIN
			 l_default_rec.ERROR_CODE := rec.ERROR_CODE_dv;
		 EXCEPTION
			 when others then
				 write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'ERROR_CODE','INV_DEFAULT',SQLERRM);
		 END;
		 BEGIN
			 l_default_rec.SHORT_DESC := rec.SHORT_DESC_dv;
		 EXCEPTION
			 when others then
				 write_s9t_error(I_file_id,'SA_ERROR_CODES',NULL,'SHORT_DESC','INV_DEFAULT',SQLERRM);
		 END;
		END LOOP;

		--Get mandatory indicators
		OPEN C_mandatory_ind;
		FETCH C_mandatory_ind
		INTO l_mi_rec;
		CLOSE C_mandatory_ind;
		FOR rec IN
		(select r.get_cell(SA_ERROR_CODES$Action)      AS Action,
		r.get_cell(SA_ERROR_CODES$REQUIRED_IND)              AS REQUIRED_IND,
		r.get_cell(SA_ERROR_CODES$HQ_OVERRIDE_IND)              AS HQ_OVERRIDE_IND,
		'N'                                          AS STORE_OVERRIDE_IND,
		r.get_cell(SA_ERROR_CODES$REC_SOLUTION)              AS REC_SOLUTION,
		r.get_cell(SA_ERROR_CODES$TARGET_TAB)              AS TARGET_TAB,
		r.get_cell(SA_ERROR_CODES$TARGET_FORM)              AS TARGET_FORM,
		r.get_cell(SA_ERROR_CODES$ERROR_DESC)              AS ERROR_DESC,
		upper(r.get_cell(SA_ERROR_CODES$ERROR_CODE))              AS ERROR_CODE,
		r.get_cell(SA_ERROR_CODES$SHORT_DESC)              AS SHORT_DESC,
		r.get_row_seq()                             AS row_seq
		from s9t_folder sf,
		TABLE(sf.s9t_file_obj.sheets) ss,
		TABLE(ss.s9t_rows) r
		where sf.file_id  = I_file_id
		and ss.sheet_name = sheet_name_trans(SA_ERROR_CODES_sheet)
		)
		LOOP
		l_temp_rec.process_id        := I_process_id;
		l_temp_rec.chunk_id          := 1;
		l_temp_rec.row_seq           := rec.row_seq;
		l_temp_rec.process$status    := 'N';
		l_temp_rec.create_id         := GET_USER;
		l_temp_rec.last_upd_id       := GET_USER;
		l_temp_rec.create_datetime   := sysdate;
		l_temp_rec.last_upd_datetime := sysdate;
		l_error := false;
		BEGIN
		  l_temp_rec.Action := rec.Action;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'ACTION_COLUMN',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.REQUIRED_IND := rec.REQUIRED_IND;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'REQUIRED_IND',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.HQ_OVERRIDE_IND := rec.HQ_OVERRIDE_IND;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'HQ_OVERRIDE_IND',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.STORE_OVERRIDE_IND := rec.STORE_OVERRIDE_IND;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'STORE_OVERRIDE_IND',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.REC_SOLUTION := rec.REC_SOLUTION;
		  
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'REC_SOLUTION',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.TARGET_TAB := rec.TARGET_TAB;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'TARGET_TAB',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.TARGET_FORM := rec.TARGET_FORM;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'TARGET_FORM',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.ERROR_DESC := rec.ERROR_DESC;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'ERROR_DESC',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.ERROR_CODE := rec.ERROR_CODE;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'ERROR_CODE',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		BEGIN
		  l_temp_rec.SHORT_DESC := rec.SHORT_DESC;
		EXCEPTION
		when others then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,'SHORT_DESC',SQLCODE,SQLERRM);
		  l_error := true;
		END;
		if rec.action = STG_SVC_SA_ERROR_CODES.action_new
		then
		l_temp_rec.REQUIRED_IND := NVL( l_temp_rec.REQUIRED_IND,l_default_rec.REQUIRED_IND);
		l_temp_rec.HQ_OVERRIDE_IND := NVL( l_temp_rec.HQ_OVERRIDE_IND,l_default_rec.HQ_OVERRIDE_IND);
		l_temp_rec.STORE_OVERRIDE_IND := NVL( l_temp_rec.STORE_OVERRIDE_IND,l_default_rec.STORE_OVERRIDE_IND);
		l_temp_rec.REC_SOLUTION := NVL( l_temp_rec.REC_SOLUTION,l_default_rec.REC_SOLUTION);
		l_temp_rec.TARGET_TAB := NVL( l_temp_rec.TARGET_TAB,l_default_rec.TARGET_TAB);
		l_temp_rec.TARGET_FORM := NVL( l_temp_rec.TARGET_FORM,l_default_rec.TARGET_FORM);
		l_temp_rec.ERROR_DESC := NVL( l_temp_rec.ERROR_DESC,l_default_rec.ERROR_DESC);
		l_temp_rec.ERROR_CODE := upper(NVL( l_temp_rec.ERROR_CODE,l_default_rec.ERROR_CODE));
		l_temp_rec.SHORT_DESC := NVL( l_temp_rec.SHORT_DESC,l_default_rec.SHORT_DESC);
		end if;
		If not (
				  l_temp_rec.ERROR_CODE is not null and
				  1 = 1
				  )
		Then
		  write_s9t_error(I_file_id,SA_ERROR_CODES_sheet,rec.row_seq,NULL,NULL,sql_lib.create_msg('PK_COLS_REQUIRED',l_pk_columns));
		  l_error := true;
		end if;
		IF NOT l_error THEN
		  svc_SA_ERROR_CODES_col.extend();
		  svc_SA_ERROR_CODES_col(svc_SA_ERROR_CODES_col.count()):=l_temp_rec;
		end if;
		END LOOP;
		BEGIN
		forall i IN 1..svc_SA_ERROR_CODES_col.count SAVE EXCEPTIONS Merge INTO SVC_SA_ERROR_CODES st USING
		(select
		(CASE
		  WHEN l_mi_rec.REQUIRED_IND_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.REQUIRED_IND             IS NULL
		  THEN mt.REQUIRED_IND
		  ELSE s1.REQUIRED_IND
		END) AS REQUIRED_IND,
		(CASE
		  WHEN l_mi_rec.HQ_OVERRIDE_IND_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.HQ_OVERRIDE_IND             IS NULL
		  THEN mt.HQ_OVERRIDE_IND
		  ELSE s1.HQ_OVERRIDE_IND
		END) AS HQ_OVERRIDE_IND,
		(CASE
		  WHEN l_mi_rec.STORE_OVERRIDE_IND_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.STORE_OVERRIDE_IND             IS NULL
		  THEN mt.STORE_OVERRIDE_IND
		  ELSE s1.STORE_OVERRIDE_IND
		END) AS STORE_OVERRIDE_IND,
		(CASE
		  WHEN l_mi_rec.REC_SOLUTION_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.REC_SOLUTION             IS NULL
		  THEN mt.REC_SOLUTION
		  ELSE s1.REC_SOLUTION
		END) AS REC_SOLUTION,
		(CASE
		  WHEN l_mi_rec.TARGET_TAB_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.TARGET_TAB             IS NULL
		  THEN mt.TARGET_TAB
		  ELSE s1.TARGET_TAB
		END) AS TARGET_TAB,
		(CASE
		  WHEN l_mi_rec.TARGET_FORM_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.TARGET_FORM             IS NULL
		  THEN mt.TARGET_FORM
		  ELSE s1.TARGET_FORM
		END) AS TARGET_FORM,
		(CASE
		  WHEN l_mi_rec.ERROR_DESC_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.ERROR_DESC             IS NULL
		  THEN mt.ERROR_DESC
		  ELSE s1.ERROR_DESC
		END) AS ERROR_DESC,
		(CASE
		  WHEN l_mi_rec.ERROR_CODE_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.ERROR_CODE             IS NULL
		  THEN mt.ERROR_CODE
		  ELSE s1.ERROR_CODE
		END) AS ERROR_CODE,
		(CASE
		  WHEN l_mi_rec.SHORT_DESC_mi    = 'N'
		  and svc_SA_ERROR_CODES_col(i).action = coresvc_item.action_mod
		  and s1.SHORT_DESC             IS NULL
		  THEN mt.SHORT_DESC
		  ELSE s1.SHORT_DESC
		END) AS SHORT_DESC,
		null as dummy
		from (select
		 svc_SA_ERROR_CODES_col(i).REQUIRED_IND AS REQUIRED_IND,
		 svc_SA_ERROR_CODES_col(i).HQ_OVERRIDE_IND AS HQ_OVERRIDE_IND,
		 svc_SA_ERROR_CODES_col(i).STORE_OVERRIDE_IND AS STORE_OVERRIDE_IND,
		 svc_SA_ERROR_CODES_col(i).REC_SOLUTION AS REC_SOLUTION,
		 svc_SA_ERROR_CODES_col(i).TARGET_TAB AS TARGET_TAB,
		 svc_SA_ERROR_CODES_col(i).TARGET_FORM AS TARGET_FORM,
		 svc_SA_ERROR_CODES_col(i).ERROR_DESC AS ERROR_DESC,
		 svc_SA_ERROR_CODES_col(i).ERROR_CODE AS ERROR_CODE,
		 svc_SA_ERROR_CODES_col(i).SHORT_DESC AS SHORT_DESC,
		 null as dummy
		from dual
		) s1,
		SA_ERROR_CODES mt
		where
		mt.ERROR_CODE (+)     = s1.ERROR_CODE   and
		1 = 1
		) sq ON (
		st.ERROR_CODE      = sq.ERROR_CODE and
		svc_SA_ERROR_CODES_col(i).ACTION IN (STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_del)
		)
		WHEN matched THEN
		update
		set PROCESS_ID      = svc_SA_ERROR_CODES_col(i).PROCESS_ID ,
		CHUNK_ID          = svc_SA_ERROR_CODES_col(i).CHUNK_ID ,
		ROW_SEQ           = svc_SA_ERROR_CODES_col(i).ROW_SEQ ,
		ACTION            = svc_SA_ERROR_CODES_col(i).ACTION ,
		PROCESS$STATUS    = svc_SA_ERROR_CODES_col(i).PROCESS$STATUS ,
		REC_SOLUTION              = sq.REC_SOLUTION ,
		TARGET_TAB              = sq.TARGET_TAB ,
		ERROR_DESC              = sq.ERROR_DESC ,
		STORE_OVERRIDE_IND              = sq.STORE_OVERRIDE_IND ,
		REQUIRED_IND              = sq.REQUIRED_IND ,
		HQ_OVERRIDE_IND              = sq.HQ_OVERRIDE_IND ,
		TARGET_FORM              = sq.TARGET_FORM ,
        SHORT_DESC               = sq.SHORT_DESC ,
		CREATE_ID         = svc_SA_ERROR_CODES_col(i).CREATE_ID ,
		CREATE_DATETIME   = svc_SA_ERROR_CODES_col(i).CREATE_DATETIME ,
		LAST_UPD_ID       = svc_SA_ERROR_CODES_col(i).LAST_UPD_ID ,
		LAST_UPD_DATETIME = svc_SA_ERROR_CODES_col(i).LAST_UPD_DATETIME WHEN NOT matched THEN
		insert
		(
		  PROCESS_ID ,
		  CHUNK_ID ,
		  ROW_SEQ ,
		  ACTION ,
		  PROCESS$STATUS ,
		  REQUIRED_IND ,
		  HQ_OVERRIDE_IND ,
		  STORE_OVERRIDE_IND ,
		  REC_SOLUTION ,
		  TARGET_TAB ,
		  TARGET_FORM ,
		  ERROR_DESC ,
		  ERROR_CODE ,
		  SHORT_DESC ,
		  CREATE_ID ,
		  CREATE_DATETIME ,
		  LAST_UPD_ID ,
		  LAST_UPD_DATETIME
		)
		VALUES
		(
		  svc_SA_ERROR_CODES_col(i).PROCESS_ID ,
		  svc_SA_ERROR_CODES_col(i).CHUNK_ID ,
		  svc_SA_ERROR_CODES_col(i).ROW_SEQ ,
		  svc_SA_ERROR_CODES_col(i).ACTION ,
		  svc_SA_ERROR_CODES_col(i).PROCESS$STATUS ,
		  sq.REQUIRED_IND ,
		  sq.HQ_OVERRIDE_IND ,
		  sq.STORE_OVERRIDE_IND ,
		  sq.REC_SOLUTION ,
		  sq.TARGET_TAB ,
		  sq.TARGET_FORM ,
		  sq.ERROR_DESC ,
		  sq.ERROR_CODE ,
		  sq.SHORT_DESC ,
		  svc_SA_ERROR_CODES_col(i).CREATE_ID ,
		  svc_SA_ERROR_CODES_col(i).CREATE_DATETIME ,
		  svc_SA_ERROR_CODES_col(i).LAST_UPD_ID ,
		  svc_SA_ERROR_CODES_col(i).LAST_UPD_DATETIME
		);
	 EXCEPTION
	 WHEN DML_ERRORS THEN
		FOR i IN 1..sql%bulk_exceptions.count
		LOOP
		  l_error_code:=sql%bulk_exceptions(i).error_code;
		  if l_error_code=1 then 
			  l_error_code:=null;
			  l_error_msg:=sql_lib.create_msg('DUP_REC_EXISTS',l_pk_columns,l_table);
		  end if;
		  write_s9t_error
		  (
			 I_file_id,SA_ERROR_CODES_sheet,svc_SA_ERROR_CODES_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,l_error_code,l_error_msg
		  )
		  ;
		END LOOP;
	 END;
	END process_s9t_SA_ERROR_CODES;
    
PROCEDURE PROCESS_S9T_SA_ERROR_CODES_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                        I_process_id   IN   SVC_SA_ERROR_CODES_TL.PROCESS_ID%TYPE)
IS
   TYPE SVC_SA_ERROR_CODES_TL_COL_TYP IS TABLE OF SVC_SA_ERROR_CODES_TL%ROWTYPE;

   L_temp_rec                 SVC_SA_ERROR_CODES_TL%ROWTYPE;
   SVC_SA_ERROR_CODES_TL_COL  SVC_SA_ERROR_CODES_TL_COL_TYP := NEW SVC_SA_ERROR_CODES_TL_COL_TYP();
   L_process_id               SVC_SA_ERROR_CODES_TL.PROCESS_ID%TYPE;
   L_error                    BOOLEAN := FALSE;
   L_default_rec              SVC_SA_ERROR_CODES_TL%ROWTYPE;
   L_table                    VARCHAR2(30)  := 'SVC_SA_ERROR_CODES_TL';
   
   cursor C_MANDATORY_IND is
      select rec_solution_mi,
             error_desc_mi,
             error_code_mi,
             short_desc_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = STG_SVC_SA_ERROR_CODES.TEMPLATE_KEY
                 and wksht_key         = SA_ERROR_CODES_TL_sheet
              ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('REC_SOLUTION' AS REC_SOLUTION
                                                                ,'ERROR_DESC'   AS ERROR_DESC
                                                                ,'ERROR_CODE'   AS ERROR_CODE
                                                                ,'SHORT_DESC'   AS SHORT_DESC
                                                                ,'LANG'         AS LANG));
   L_mi_rec c_mandatory_ind%rowtype;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_pk_columns varchar2(255) := 'error_code, lang';
   L_error_msg rtk_errors.rtk_text%type;
   L_error_code number;
   l_count number;
   BEGIN
   -- Get default values.
   FOR rec IN (select REC_SOLUTION_dv,
                      ERROR_DESC_dv,
                      ERROR_CODE_dv,
                      SHORT_DESC_dv,
                      LANG_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = STG_SVC_SA_ERROR_CODES.template_key
                          and wksht_key     = SA_ERROR_CODES_TL_sheet
                       ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('ERROR_CODE'   AS ERROR_CODE,
                                                                              'ERROR_DESC'   AS ERROR_DESC,
                                                                              'REC_SOLUTION' AS REC_SOLUTION,
                                                                              'SHORT_DESC'   AS SHORT_DESC,
                                                                              'LANG'         AS LANG))
               )

   LOOP  
    BEGIN  
        l_default_rec.error_code := rec.error_code_dv;  
    EXCEPTION  
       when others then  
           WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_sheet,NULL,'ERROR_CODE','INV_DEFAULT',SQLERRM);  
    END;
    BEGIN  
        l_default_rec.error_desc := rec.error_desc_dv;  
    EXCEPTION  
       when others then  
           WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_sheet,NULL,'ERROR_DESC','INV_DEFAULT',SQLERRM);  
    END;
    BEGIN
        l_default_rec.rec_solution := rec.rec_solution_dv;
    EXCEPTION
        when others then
            WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_sheet,NULL,'REC_SOLUTION','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
        l_default_rec.short_desc := rec.short_desc_dv;
    EXCEPTION
        when others then
            WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_sheet,NULL,'SHORT_DESC','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
        l_default_rec.lang := rec.lang_dv;
    EXCEPTION
        when others then
            WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_sheet,NULL,'LANG','INV_DEFAULT',SQLERRM);
    END;
   END LOOP;

   --Get mandatory indicators
   OPEN C_MANDATORY_IND;
   FETCH C_MANDATORY_IND
   INTO L_mi_rec;
   CLOSE C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(SA_ERROR_CODES_TL$Action)         AS action,
                     UPPER(r.get_cell(SA_ERROR_CODES_TL$error_code))     AS error_code,
                     r.get_cell(SA_ERROR_CODES_TL$error_desc)     AS error_desc,
                     r.get_cell(SA_ERROR_CODES_TL$rec_solution)   AS rec_solution,
                     r.get_cell(SA_ERROR_CODES_TL$short_desc)     AS short_desc,
                     r.get_cell(SA_ERROR_CODES_TL$lang)           AS lang,
                     r.get_row_seq()                           AS row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(SA_ERROR_CODES_TL_sheet)
             )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := false;
      BEGIN
        L_temp_rec.Action := rec.Action;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_SHEET,rec.row_seq,'ACTION_COLUMN',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.LANG := rec.LANG;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_SHEET,rec.row_seq,'LANG',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.ERROR_CODE := rec.ERROR_CODE;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_SHEET,rec.row_seq,'ERROR_CODE',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.ERROR_DESC := rec.ERROR_DESC;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_SHEET,rec.row_seq,'ERROR_DESC',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.REC_SOLUTION := rec.REC_SOLUTION;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_SHEET,rec.row_seq,'REC_SOLUTION',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.SHORT_DESC := rec.SHORT_DESC;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_SHEET,rec.row_seq,'SHORT_DESC',SQLCODE,SQLERRM);
        L_error := true;
      END;
      BEGIN
        L_temp_rec.LANG := rec.LANG;
      EXCEPTION
      when others then
        WRITE_S9T_ERROR(I_file_id,SA_ERROR_CODES_TL_SHEET,rec.row_seq,'LANG',SQLCODE,SQLERRM);
        L_error := true;
      END;
      if rec.action = STG_SVC_SA_ERROR_CODES.action_new THEN
         L_temp_rec.ERROR_CODE := upper(NVL( L_temp_rec.ERROR_CODE,l_default_rec.ERROR_CODE));
         L_temp_rec.ERROR_DESC := NVL( L_temp_rec.ERROR_DESC,l_default_rec.ERROR_DESC);
         L_temp_rec.REC_SOLUTION := NVL( L_temp_rec.REC_SOLUTION,l_default_rec.REC_SOLUTION);
         L_temp_rec.SHORT_DESC := NVL( L_temp_rec.SHORT_DESC,l_default_rec.SHORT_DESC);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,l_default_rec.LANG);
      end if;
      if L_temp_rec.error_code is NULL then
        WRITE_S9T_ERROR(I_file_id,
                        SA_ERROR_CODES_TL_SHEET,
                        rec.row_seq,
                        NULL,
                        NULL,
                        sql_lib.create_msg('PK_COLS_REQUIRED',L_pk_columns));
        L_error := true;
      end if;
      if NOT L_error THEN
        SVC_SA_ERROR_CODES_TL_COL.extend();
        SVC_SA_ERROR_CODES_TL_COL(SVC_SA_ERROR_CODES_TL_COL.count()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_sa_error_codes_tl_col.count SAVE EXCEPTIONS 
         merge into svc_sa_error_codes_tl st using
            (select
                   (case
                     when L_mi_rec.rec_solution_mi    = 'N'
                      and svc_sa_error_codes_tl_col(i).action = coresvc_item.action_mod
                      and s1.rec_solution             is null
                     then mt.rec_solution
                     else s1.rec_solution
                   end) as rec_solution,
                   (case
                     when L_mi_rec.error_desc_mi    = 'N'
                      and svc_sa_error_codes_tl_col(i).action = coresvc_item.action_mod
                      and s1.error_desc             is null
                     then mt.error_desc
                     else s1.error_desc
                   end) as error_desc,
                   (case
                     when L_mi_rec.error_code_mi    = 'N'
                      and svc_sa_error_codes_tl_col(i).action = coresvc_item.action_mod
                      and s1.error_code             is null
                     then mt.error_code
                     else s1.error_code
                   end) as error_code,
                   (case
                     when L_mi_rec.short_desc_mi    = 'N'
                      and svc_sa_error_codes_tl_col(i).action = coresvc_item.action_mod
                      and s1.short_desc             is null
                     then mt.short_desc
                     else s1.short_desc
                   end) as short_desc,
                   (case
                     when L_mi_rec.lang_mi    = 'N'
                      and svc_sa_error_codes_tl_col(i).action = coresvc_item.action_mod
                      and s1.lang             is null
                     then mt.lang
                     else s1.lang
                   end) as lang,
                   null as dummy
              from (select svc_sa_error_codes_tl_col(i).rec_solution as rec_solution,
                           svc_sa_error_codes_tl_col(i).error_desc as error_desc,
                           UPPER(svc_sa_error_codes_tl_col(i).error_code) as error_code,
                           svc_sa_error_codes_tl_col(i).short_desc as short_desc,
                           svc_sa_error_codes_tl_col(i).lang as lang
                      from dual) s1,
                    sa_error_codes_tl mt
             where mt.error_code (+)     = s1.error_code   and
                   mt.lang (+)           = s1.lang
            ) sq on (
                     st.error_code      = sq.error_code and
                     st.lang            = sq.lang and
                     svc_sa_error_codes_tl_col(i).action in (stg_svc_sa_error_codes.action_mod,stg_svc_sa_error_codes.action_del)
                    )
         WHEN matched THEN
            update
               set process_id        = svc_sa_error_codes_tl_col(i).process_id ,
                   chunk_id          = svc_sa_error_codes_tl_col(i).chunk_id ,
                   row_seq           = svc_sa_error_codes_tl_col(i).row_seq ,
                   action            = svc_sa_error_codes_tl_col(i).action ,
                   process$status    = svc_sa_error_codes_tl_col(i).process$status ,
                   rec_solution      = sq.rec_solution ,
                   error_desc        = sq.error_desc ,
                   short_desc        = sq.short_desc
         WHEN NOT matched THEN
            insert
                  (
                   process_id ,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   rec_solution ,
                   error_desc ,
                   error_code ,
                   short_desc ,
                   lang
                  )
            values
                  (
                   svc_sa_error_codes_tl_col(i).process_id ,
                   svc_sa_error_codes_tl_col(i).chunk_id ,
                   svc_sa_error_codes_tl_col(i).row_seq ,
                   svc_sa_error_codes_tl_col(i).action ,
                   svc_sa_error_codes_tl_col(i).process$status ,
                   sq.rec_solution ,
                   sq.error_desc ,
                   sq.error_code ,
                   sq.short_desc ,
                   sq.lang
                  );

EXCEPTION
   WHEN DML_ERRORS THEN
      FOR i IN 1..sql%bulk_exceptions.count
      LOOP
        L_error_code:=sql%bulk_exceptions(i).error_code;
        if L_error_code=1 then 
            L_error_code:=null;
            L_error_msg:=sql_lib.create_msg('DUP_REC_EXISTS',L_pk_columns,L_table);
        end if;
        WRITE_S9T_ERROR
        (
           I_file_id,SA_ERROR_CODES_TL_SHEET,SVC_SA_ERROR_CODES_TL_COL(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
        )
        ;
      END LOOP;
   END;
END PROCESS_S9T_SA_ERROR_CODES_TL;
        
	FUNCTION process_s9t
	 (
		O_error_message IN OUT rtk_errors.rtk_text%type ,
		I_file_id       IN s9t_folder.file_id%type,
		I_process_id IN  Number,
		O_error_count OUT NUMBER
	 )
	 RETURN BOOLEAN
	IS
	  l_file s9t_file;
	  l_sheets s9t_pkg.names_map_typ;
	  L_program VARCHAR2(255):='STG_SVC_SA_ERROR_CODES.process_s9t';
	  l_process_status svc_process_tracker.status%type;
	  INVALID_FORMAT   EXCEPTION;
	  PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
          MAX_CHAR	   EXCEPTION;
          PRAGMA	   EXCEPTION_INIT(MAX_CHAR, -01706);
	BEGIN
	  commit;--to ensure that the record in s9t_folder is commited
	  s9t_pkg.ods2obj(I_file_id);
	  COMMIT;  
	  L_file := s9t_pkg.get_obj(I_file_id);
	  Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
	  if s9t_pkg.code2desc(O_error_message,
	                       'RSAEC',
                         l_file,
                         true)=FALSE then
       return FALSE;
	  end if;
	  s9t_pkg.save_obj(l_file);
	  if s9t_pkg.validate_template(I_file_id) = false THEN
		  write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'S9T_INVALID_TEMPLATE');
	  else
		  populate_names(I_file_id);
		  sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key,l_file.user_lang);
		  process_s9t_SA_ERROR_IMPACT(I_file_id,I_process_id);
		  process_s9t_SA_ERROR_CODES(I_file_id,I_process_id);
          process_s9t_SA_ERROR_CODES_TL(I_file_id,I_process_id);
	  end if;
	  O_error_count := Lp_s9t_errors_tab.count();
	  FORALL i IN 1..O_error_count 
		 insert INTO s9t_errors VALUES Lp_s9t_errors_tab(i);
		 Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
		 if O_error_count    = 0 THEN
			 l_process_status := 'PS';
		 else
			 l_process_status := 'PE';
		 end if;
		 update svc_process_tracker
			 set status       = l_process_status,
				  file_id      = I_file_id
		  where process_id   = I_process_id;
		  
		COMMIT;
		return TRUE;
	EXCEPTION
	   WHEN INVALID_FORMAT then
	      ROLLBACK;
			O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT', NULL, NULL, NULL);
			Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
			write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'INV_FILE_FORMAT');
			O_error_count := Lp_s9t_errors_tab.count();
			forall i IN 1..O_error_count
			INSERT INTO s9t_errors VALUES Lp_s9t_errors_tab
			(i
			 );
			UPDATE svc_process_tracker
				SET status       = 'PE',
					 file_id      = I_file_id
			 WHERE process_id   = I_process_id;
			COMMIT;
			return FALSE;

           when MAX_CHAR then
              ROLLBACK;
              O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
              Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
              write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
              O_error_count := Lp_s9t_errors_tab.count();
              forall i IN 1..O_error_count
	         insert into s9t_errors
	              values Lp_s9t_errors_tab(i);

              update svc_process_tracker
	         set status = 'PE',
	             file_id  = I_file_id
               where process_id = I_process_id;
      
              COMMIT;
      
              return FALSE;

	   when others then
		   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
		   RETURN FALSE;
	END process_s9t;
	FUNCTION populate_lists(O_error_message IN OUT rtk_errors.rtk_text%type,
								  I_file_id       IN NUMBER) RETURN BOOLEAN
	IS
	   L_program VARCHAR2(75) := 'STG_SVC_SA_ERROR_CODES.POPULATE_LISTS';
	   l_s9t_action s9t_cells := NEW s9t_cells(STG_SVC_SA_ERROR_CODES.action_new,STG_SVC_SA_ERROR_CODES.action_mod,STG_SVC_SA_ERROR_CODES.action_del);
	BEGIN
	   if S9T_PKG.populate_lists(O_error_message, 
		                           I_file_id,
										           'RSAEC')=FALSE then
		    return FALSE;
     end if;
	   return TRUE;
	EXCEPTION
	   when others then
		   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
		   return FALSE;
	END populate_lists;

END STG_SVC_SA_ERROR_CODES;
/ 