
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STKLEDGR_INSERTS_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------
--THESE FUNCTIONS MUST BE CALLED IN THE FOLLOWING ORDER:
--   ADD_LOC
--   ADD_DEPT_BUDGET
--   ADD_SUBCLASS
--OTHERWISE THERE WILL BE PK VIOLATIONS! 
--
--Function Name : add_loc
--Purpose : This function inserts empty rows into the tables
--          month_data, week_data (if a 454 calendar is being
--          used), half_data, month_data_budget, and 
--          half_data_budget for new stores or WHs added to 
--          the system.  One row is inserted for each
--          location/dept/class/subclass combination.
--
--Called By : salinsrt.pc
--
--Calls : CAL_TO_CAL_HALF,
--        CAL_TO_CAL_LDOM,
--        CAL_TO_454_HALF,
--        CAL_TO_454_LDOM,
--        CAL_TO_454,
--        HALF_TO_CAL_FDOH,
--        HALF_TO_454_FDOH
--
--Date Created : 31-JUL-96
--Created By : Christine Ronnback
--------------------------------------------------------------
FUNCTION ADD_LOC (I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                  I_location        IN       ITEM_LOC.LOC%TYPE,
                  I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
                  O_counter         IN OUT   NUMBER,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------
--Function Name : add_subclass
--Purpose : This function inserts empty rows into the tables
--          month_data, week_data (if a 454 calendar is being
--          used), and half_data for new subclasses added 
--          to the system.  One row is inserted for each store
--          and wh.
--
--Called By : salinsrt.pc
--
--Calls : CAL_TO_CAL_HALF,
--        CAL_TO_CAL_LDOM,
--        CAL_TO_454_HALF,
--        CAL_TO_454_LDOM,
--        CAL_TO_454
--
--Date Created : 31-JUL-96
--Created By : Christine Ronnback
--------------------------------------------------------------
FUNCTION ADD_SUBCLASS (I_dept            IN       SUBCLASS.DEPT%TYPE,
                       I_class           IN       SUBCLASS.CLASS%TYPE,
                       I_subclass        IN       SUBCLASS.SUBCLASS%TYPE,
                       O_counter         IN OUT   NUMBER,
                       O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------
--Function Name : add_dept_budget
--Purpose : This function inserts empty rows into the budget tables
--          month_data_budget and half_data_budget for 
--          new departments added to the system.  One row is 
--          inserted for each store and wh.
--
--Called By : salinsrt.pc
--
--Calls : HALF_TO_CAL_FDOH,
--        HALF_TO_454_FDOH,
--        CAL_TO_CAL_LDOM,
--        CAL_TO_454_LDOM
--
--Date Created : 31-JUL-96
--Created By : Christine Ronnback
--------------------------------------------------------------
FUNCTION ADD_DEPT_BUDGET (I_dept           IN      NUMBER,
                          O_counter        IN OUT  NUMBER,
                          O_error_message  IN OUT VARCHAR2)
                          RETURN BOOLEAN;
END;
/


