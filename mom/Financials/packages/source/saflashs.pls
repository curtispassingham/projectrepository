
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_FLASH_SALES_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
FUNCTION WRITE_FLASH_SALES (O_error_message    IN OUT VARCHAR2,
                            I_store_day_seq_no IN     SA_TOTAL.STORE_DAY_SEQ_NO%TYPE)                    
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END;
/