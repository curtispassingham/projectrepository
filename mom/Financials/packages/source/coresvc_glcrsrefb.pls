CREATE OR REPLACE PACKAGE BODY CORESVC_SA_FIF_GL_CROSS_REF

is
	CURSOR c_svc_SA_FIF_GL_CROSS_REF(I_process_id NUMBER,I_chunk_id NUMBER)
	  is
	  select
			PK_SA_FIF_GL_CROSS_REF.rowid  AS PK_SA_FIF_GL_CROSS_REF_rid,
			SFO_FOG_FK.rowid    AS SFO_FOG_FK_rid,
			st.CR_CCID,
			st.DR_SEQUENCE10,
			st.DR_SEQUENCE9,
			st.DR_SEQUENCE8,
			st.DR_SEQUENCE7,
			st.DR_SEQUENCE6,
			st.DR_SEQUENCE5,
			st.DR_SEQUENCE4,
			st.DR_SEQUENCE3,
			st.DR_SEQUENCE2,
			st.DR_SEQUENCE1,
			st.DR_CCID,
			st.SET_OF_BOOKS_ID,
			st.TOTAL_ID,
			st.CR_SEQUENCE20,
			st.ROLLUP_LEVEL_3,
			st.ROLLUP_LEVEL_2,
			st.ROLLUP_LEVEL_1,
			st.STORE,
			st.CR_SEQUENCE19,
			st.CR_SEQUENCE18,
			st.CR_SEQUENCE17,
			st.CR_SEQUENCE16,
			st.CR_SEQUENCE15,
			st.CR_SEQUENCE14,
			st.CR_SEQUENCE13,
			st.CR_SEQUENCE12,
			st.CR_SEQUENCE11,
			st.DR_SEQUENCE20,
			st.DR_SEQUENCE19,
			st.DR_SEQUENCE18,
			st.DR_SEQUENCE17,
			st.DR_SEQUENCE16,
			st.DR_SEQUENCE15,
			st.DR_SEQUENCE14,
			st.DR_SEQUENCE13,
			st.DR_SEQUENCE12,
			st.DR_SEQUENCE11,
			st.CR_SEQUENCE10,
			st.CR_SEQUENCE9,
			st.CR_SEQUENCE8,
			st.CR_SEQUENCE7,
			st.CR_SEQUENCE6,
			st.CR_SEQUENCE5,
			st.CR_SEQUENCE4,
			st.CR_SEQUENCE3,
			st.CR_SEQUENCE2,
			st.CR_SEQUENCE1,
			st.PROCESS_ID,
			st.ROW_SEQ,
			upper(st.ACTION) AS action,
			st.PROCESS$STATUS
		 from SVC_SA_FIF_GL_CROSS_REF st,
			SA_FIF_GL_CROSS_REF PK_SA_FIF_GL_CROSS_REF,
			FIF_GL_SETUP SFO_FOG_FK,
			dual
		 where st.process_id = I_process_id
		 and st.chunk_id     = I_chunk_id
		 and st.SET_OF_BOOKS_ID         = PK_SA_FIF_GL_CROSS_REF.SET_OF_BOOKS_ID (+)
		 and st.ROLLUP_LEVEL_3         = PK_SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_3 (+)
		 and st.ROLLUP_LEVEL_2         = PK_SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_2 (+)
		 and st.ROLLUP_LEVEL_1         = PK_SA_FIF_GL_CROSS_REF.ROLLUP_LEVEL_1 (+)
		 and st.TOTAL_ID         = PK_SA_FIF_GL_CROSS_REF.TOTAL_ID (+)
		 and st.STORE         = PK_SA_FIF_GL_CROSS_REF.STORE (+)
		 and st.SET_OF_BOOKS_ID        = SFO_FOG_FK.SET_OF_BOOKS_ID (+)
		 and st.action      is NOT NULL;
	  c_svc_SA_FIF_GL_CROSS_REF_rec c_svc_SA_FIF_GL_CROSS_REF%rowtype;
	Type errors_tab_typ
	is
	  TABLE OF SVC_ADMIN_UPLD_ER%rowtype;
	  Lp_errors_tab errors_tab_typ;
	-----------------------------------------  
	PROCEDURE write_error(
		 I_process_id  IN SVC_ADMIN_UPLD_ER.process_id%type,
		 I_error_seq   IN SVC_ADMIN_UPLD_ER.error_seq%type,
		 I_chunk_id    IN SVC_ADMIN_UPLD_ER.chunk_id%type,
		 I_table_name  IN SVC_ADMIN_UPLD_ER.table_name%type,
		 I_row_seq     IN SVC_ADMIN_UPLD_ER.row_seq%type,
		 I_column_name IN SVC_ADMIN_UPLD_ER.column_name%type,
		 I_error_msg   IN SVC_ADMIN_UPLD_ER.error_msg%type)
	is
	BEGIN
	  Lp_errors_tab.extend();
	  Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
	  Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
	  Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
	  Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
	  Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
	  Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
	  Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
	end write_error;
	-------------------------------------------------------------------------------
	---  Name: exec_SA_FIF_GL_CROSS_REF_ins
	--purpose: to insert data in SA_FIF_GL_CROSS_REF
	-------------------------------------------------------------------------------- 
	PROCEDURE exec_SA_FIF_GL_CROSS_REF_ins
	  (
		 I_ins_tab IN SA_FIF_GL_CROSS_REF_rec_tab
	  )
	is
	BEGIN
	  forall i IN 1..I_ins_tab.count()
	  insert INTO SA_FIF_GL_CROSS_REF VALUES I_ins_tab
		 ( i
		 );
	end exec_SA_FIF_GL_CROSS_REF_ins;
	-------------------------------------------------------------------------------
	---  Name: exec_SA_FIF_GL_CROSS_REF_upd
	--purpose: to update data in SA_FIF_GL_CROSS_REF
	-------------------------------------------------------------------------------- 
	PROCEDURE exec_SA_FIF_GL_CROSS_REF_upd
	  (
		 I_upd_tab IN SA_FIF_GL_CROSS_REF_rec_tab
	  )
	is
	BEGIN
	  forall i IN 1..I_upd_tab.count()
	  update SA_FIF_GL_CROSS_REF SET row = I_upd_tab(i) where 1 = 1
	and SET_OF_BOOKS_ID = I_upd_tab(i).SET_OF_BOOKS_ID
	and ROLLUP_LEVEL_3 = I_upd_tab(i).ROLLUP_LEVEL_3
	and ROLLUP_LEVEL_2 = I_upd_tab(i).ROLLUP_LEVEL_2
	and ROLLUP_LEVEL_1 = I_upd_tab(i).ROLLUP_LEVEL_1
	and TOTAL_ID = I_upd_tab(i).TOTAL_ID
	and STORE = I_upd_tab(i).STORE
	;
	end exec_SA_FIF_GL_CROSS_REF_upd;
	-------------------------------------------------------------------------------
	---  Name: exec_SA_FIF_GL_CROSS_REF_del
	--purpose: to delete data from SA_FIF_GL_CROSS_REF
	-------------------------------------------------------------------------------- 
	PROCEDURE exec_SA_FIF_GL_CROSS_REF_del(
		 I_del_tab IN SA_FIF_GL_CROSS_REF_rec_tab )
	is
	BEGIN
	  forall i IN 1..I_del_tab.count()
	  delete from SA_FIF_GL_CROSS_REF where 1 = 1
	and SET_OF_BOOKS_ID = I_del_tab(i).SET_OF_BOOKS_ID
	and ROLLUP_LEVEL_3 = I_del_tab(i).ROLLUP_LEVEL_3
	and ROLLUP_LEVEL_2 = I_del_tab(i).ROLLUP_LEVEL_2
	and ROLLUP_LEVEL_1 = I_del_tab(i).ROLLUP_LEVEL_1
	and TOTAL_ID = I_del_tab(i).TOTAL_ID
	and STORE = I_del_tab(i).STORE
	;
	end exec_SA_FIF_GL_CROSS_REF_del;
	-------------------------------------------------------------------------------
	---  Name: process_SA_FIF_GL_CROSS_REF
	-------------------------------------------------------------------------------- 
	PROCEDURE process_SA_FIF_GL_CROSS_REF(
		 I_process_id IN SVC_SA_FIF_GL_CROSS_REF.PROCESS_ID%TYPE,
		 I_chunk_id   IN SVC_SA_FIF_GL_CROSS_REF.CHUNK_ID%TYPE )
	is
	  l_error BOOLEAN;
	  SA_FIF_GL_CROSS_REF_temp_rec SA_FIF_GL_CROSS_REF%rowtype;
	  SA_FIF_GL_CROSS_REF_ins_rec SA_FIF_GL_CROSS_REF_rec_tab:=NEW SA_FIF_GL_CROSS_REF_rec_tab();
	  SA_FIF_GL_CROSS_REF_upd_rec SA_FIF_GL_CROSS_REF_rec_tab:=NEW SA_FIF_GL_CROSS_REF_rec_tab();
	  SA_FIF_GL_CROSS_REF_del_rec SA_FIF_GL_CROSS_REF_rec_tab:=NEW SA_FIF_GL_CROSS_REF_rec_tab();
	  l_table VARCHAR2(255)    :='SA_FIF_GL_CROSS_REF';
	  l_chk_result varchar2(1):='N';
	  L_error_message rtk_errors.rtk_text%type;
	  L_exists boolean;
	  l_desc code_detail.code_desc%type;
	  L_sa_store             BOOLEAN;
		L_store_type           STORE.STORE_TYPE%TYPE;
		L_set_of_books_id      FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE;
		L_set_of_books_desc    FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE;
		 L_ref_label_code_1       SA_TOTAL_HEAD.REF_LABEL_CODE_1%TYPE;
		L_ref_label_code_2       SA_TOTAL_HEAD.REF_LABEL_CODE_2%TYPE;
		L_ref_label_code_3       SA_TOTAL_HEAD.REF_LABEL_CODE_3%TYPE;
		L_ref_label_desc_1       CODE_DETAIL.CODE_DESC%TYPE;
		L_ref_label_desc_2       CODE_DETAIL.CODE_DESC%TYPE;
		L_ref_label_desc_3       CODE_DETAIL.CODE_DESC%TYPE;
		GP_system_options      SYSTEM_OPTIONS%ROWTYPE;
		L_acc_validate_tbl   ACC_VALIDATE_API.ACC_VALIDATE_TBL;
		L_ccid            FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE;
	 
	BEGIN
		FOR rec IN c_svc_SA_FIF_GL_CROSS_REF(I_process_id,I_chunk_id)
		LOOP
		   l_error       := False;
			if rec.action is NULL then
				WRITE_ERROR(I_process_id, svc_admin_upld_er_seq.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'ACTION', 'FIELD_NOT_NULL');
				L_error := TRUE;
			end if;
		   if rec.action is NOT NULL and rec.action NOT IN (stg_svc_sa_fif_gl_cross_ref.action_new,stg_svc_sa_fif_gl_cross_ref.action_mod,stg_svc_sa_fif_gl_cross_ref.action_del) then
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ACTION','INV_ACT');
				l_error :=true;
		   end if;
		   if rec.action = stg_svc_sa_fif_gl_cross_ref.action_new and rec.PK_SA_FIF_GL_CROSS_REF_rid is NOT NULL then
			
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'REC_EXIST');
				l_error :=true;
		   end if;
		   if rec.action IN (stg_svc_sa_fif_gl_cross_ref.action_mod,stg_svc_sa_fif_gl_cross_ref.action_del) and rec.PK_SA_FIF_GL_CROSS_REF_rid is NULL then
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'PK_SA_GL_REF_MISSING');
				l_error :=true;
		   end if;

			if NOT(  rec.STORE  is NOT NULL ) then
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STORE','MUST_ENTER_FIELD');
				l_error :=true;
			end if;
			if NOT(  rec.TOTAL_ID  is NOT NULL ) then
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TOTAL_ID','MUST_ENTER_FIELD');
				l_error :=true;
			end if;
			if NOT(  rec.ROLLUP_LEVEL_1  is NOT NULL ) then
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TOTAL_ID','MUST_ENTER_FIELD');
				l_error :=true;
			end if;
			if NOT(  rec.ROLLUP_LEVEL_2  is NOT NULL ) then
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROLLUP_LEVEL_2','MUST_ENTER_FIELD');
				l_error :=true;
			end if;
			if NOT(  rec.ROLLUP_LEVEL_3  is NOT NULL ) then
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ROLLUP_LEVEL_3','MUST_ENTER_FIELD');
				l_error :=true;
			end if;
			if NOT(  rec.SET_OF_BOOKS_ID  is NOT NULL ) then
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'SET_OF_BOOKS_ID','MUST_ENTER_FIELD');
				l_error :=true;
			end if;
		 
		 ---additional validations
			if rec.action =stg_svc_sa_fif_gl_cross_ref.action_new then

			---check if total is valid
				if rec.total_id is not null then
					if rec.total_id <>'-1' then
						if SA_TOTAL_SQL.TOTAL_USAGE_EXISTS(L_error_message, L_exists, rec.total_id, 'GL') = FALSE then
							write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TOTAL_ID',L_error_message);
							l_error     :=true;
						elsif L_exists = FALSE then
							write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TOTAL_ID','GL_TOT_NO_EXP');
							l_error :=true;
						elsif  SA_TOTAL_SQL.GET_TOTAL_DESC(L_error_message, L_desc, rec.total_id, NULL) = FALSE then
						   write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TOTAL_ID',L_error_message);
						   l_error :=true;
						
					   end if;
				   end if;
					if SA_TOTAL_SQL.GET_REF_LABELS(L_error_message, L_ref_label_code_1, L_ref_label_desc_1, L_ref_label_code_2, L_ref_label_desc_2, L_ref_label_code_3, L_ref_label_desc_3, rec.total_id) = FALSE then
						write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'TOTAL_ID',L_error_message);
						l_error :=true;
					end if;
			   end if;

		-- validate set_of_books
				if rec.set_of_books_id is not null then
				 
					if SET_OF_BOOKS_SQL.GET_DESC(L_error_message, L_desc, rec.set_of_books_id) = FALSE then
					  write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'SET_OF_BOOKS_ID',L_error_message);
					  l_error :=true;
					end if;
				end if; 

		---validate location(store)
				if rec.store   is NOT NULL then
					if rec.store <>'-1' then
						if FILTER_LOV_VALIDATE_SQL.SA_STORE(L_error_message, L_sa_store, L_desc, L_store_type, rec.store) = FALSE then
							write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STORE',L_error_message);
							l_error       :=true;
						elsif L_sa_store = FALSE then
							l_error       :=true;
							write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STORE','INV_SA_STORE');
						elsif SET_OF_BOOKS_SQL.GET_SOB_LOC(L_error_message, L_set_of_books_desc, L_set_of_books_id, rec.store, 'S') = FALSE then
						  write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STORE',L_error_message);
						  l_error :=true;
						elsif rec.set_of_books_id is NOT NULL and rec.set_of_books_id <> L_set_of_books_id then
						  write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STORE',sql_lib.create_msg('LOC_NOTIN_TSF_SOB',rec.store,rec.set_of_books_id));
						  l_error                 :=true;
						elsif rec.total_id   is NOT NULL and rec.set_of_books_id is NOT NULL then
							if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(L_error_message, L_exists, rec.store, rec.total_id, rec.set_of_books_id) = FALSE OR L_exists=FALSE then
								write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STORE',L_error_message);
								l_error :=true;
							end if;

						end if;
					end if;
				end if;--if rec.store   is NOT NULL
				if not(l_error) then
					if RMS2FIN.SA_FIF_VALIDATION(L_error_message,
											 L_exists,
											 rec.set_of_books_id,
											 rec.store,
											 rec.total_id,
											 rec.rollup_level_1,
											 rec.rollup_level_2,
											 rec.rollup_level_3) = FALSE then
					  
						write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,null,L_error_message);
						l_error :=true;
					end if;
            ---
					if L_exists = TRUE then
					 
						write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,null,'DUPLICATE_RECORD');
						l_error :=true;
					end if;
            end if;
         end if;--new validation
    
		if not(l_error) and rec.action<>stg_svc_sa_fif_gl_cross_ref.action_del then
			--validate based on system_options.financial_ap
		   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message, GP_system_options) = FALSE then
		
			  NULL;
		   end if;
			  
			if GP_system_options.financial_ap = 'A' then
			---
				if rec.dr_sequence1 is NULL and rec.dr_sequence2 is NULL and rec. dr_sequence3 is NULL and rec.dr_sequence4 is NULL and rec.dr_sequence5 is NULL and rec.dr_sequence6 is NULL and rec.dr_sequence7 is NULL and rec.dr_sequence8 is NULL and rec.dr_sequence9 is NULL and rec.dr_sequence10 is NULL and rec.dr_sequence11 is NULL and rec.dr_sequence12 is NULL and rec.dr_sequence13 is NULL and rec.dr_sequence14 is NULL and rec.dr_sequence15 is NULL and rec.dr_sequence16 is NULL and rec.dr_sequence17 is NULL and rec.dr_sequence18 is NULL and rec.dr_sequence19 is NULL and rec.dr_sequence20 is NULL then
					write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'DR_CCID','NO_SEGMENT_AVAIL');
					l_error :=true;
				else
				---
					L_acc_validate_tbl(1).requesting_system := 'RESA';
					L_acc_validate_tbl(1).set_of_books_id   := rec.set_of_books_id;
					L_acc_validate_tbl(1).ccid              := NULL;
					L_acc_validate_tbl(1).segment_1         := rec.dr_sequence1;
					L_acc_validate_tbl(1).segment_2         := rec.dr_sequence2;
					L_acc_validate_tbl(1).segment_3         := rec.dr_sequence3;
					L_acc_validate_tbl(1).segment_4         := rec.dr_sequence4;
					L_acc_validate_tbl(1).segment_5         := rec.dr_sequence5;
					L_acc_validate_tbl(1).segment_6         := rec.dr_sequence6;
					L_acc_validate_tbl(1).segment_7         := rec.dr_sequence7;
					L_acc_validate_tbl(1).segment_8         := rec.dr_sequence8;
					L_acc_validate_tbl(1).segment_9         := rec.dr_sequence9;
					L_acc_validate_tbl(1).segment_10        := rec.dr_sequence10;
					L_acc_validate_tbl(1).segment_11        := rec.dr_sequence11;
					L_acc_validate_tbl(1).segment_12        := rec.dr_sequence12;
					L_acc_validate_tbl(1).segment_13        := rec.dr_sequence13;
					L_acc_validate_tbl(1).segment_14        := rec.dr_sequence14;
					L_acc_validate_tbl(1).segment_15        := rec.dr_sequence15;
					L_acc_validate_tbl(1).segment_16        := rec.dr_sequence16;
					L_acc_validate_tbl(1).segment_17        := rec.dr_sequence17;
					L_acc_validate_tbl(1).segment_18        := rec.dr_sequence18;
					L_acc_validate_tbl(1).segment_19        := rec.dr_sequence19;
					L_acc_validate_tbl(1).segment_20        := rec.dr_sequence20;
					---
					if ACC_VALIDATE_API.VALIDATE_ACC(L_error_message, L_acc_validate_tbl)= FALSE then
						write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,L_error_message);
						l_error :=true;
					elsif L_acc_validate_tbl(1).account_status = 'invalid' then
						write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'DR_CCID','INV_COMB_SEQ');
						l_error :=true;
					end if;
				end if;
			if rec.cr_sequence1  is NULL and
			  rec.cr_sequence2  is NULL and
			  rec.cr_sequence3  is NULL and
			  rec.cr_sequence4  is NULL and
			  rec.cr_sequence5  is NULL and
			  rec.cr_sequence6  is NULL and
			  rec.cr_sequence7  is NULL and
			  rec.cr_sequence8  is NULL and
			  rec.cr_sequence9  is NULL and
			  rec.cr_sequence10 is NULL and
			  rec.cr_sequence11 is NULL and
			  rec.cr_sequence12 is NULL and 
			  rec.cr_sequence13 is NULL and
			  rec.cr_sequence14 is NULL and
			  rec.cr_sequence15 is NULL and
			  rec.cr_sequence16 is NULL and
			  rec.cr_sequence17 is NULL and
			  rec.cr_sequence18 is NULL and
			  rec.cr_sequence19 is NULL and
			  rec.cr_sequence20 is NULL then      	 
				  
				write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'CR_CCID','NO_SEGMENT_AVAIL');
			   l_error :=true;
		   else
		 ---
				L_acc_validate_tbl(1).requesting_system := 'RESA';
				L_acc_validate_tbl(1).set_of_books_id   := rec.set_of_books_id;
				L_acc_validate_tbl(1).ccid              := NULL;
				L_acc_validate_tbl(1).segment_1         := rec.cr_sequence1;
				L_acc_validate_tbl(1).segment_2         := rec.cr_sequence2;
				L_acc_validate_tbl(1).segment_3         := rec.cr_sequence3;
				L_acc_validate_tbl(1).segment_4         := rec.cr_sequence4;
				L_acc_validate_tbl(1).segment_5         := rec.cr_sequence5;
				L_acc_validate_tbl(1).segment_6         := rec.cr_sequence6;
				L_acc_validate_tbl(1).segment_7         := rec.cr_sequence7;
				L_acc_validate_tbl(1).segment_8         := rec.cr_sequence8;
				L_acc_validate_tbl(1).segment_9         := rec.cr_sequence9;
				L_acc_validate_tbl(1).segment_10        := rec.cr_sequence10;
				L_acc_validate_tbl(1).segment_11        := rec.cr_sequence11;
				L_acc_validate_tbl(1).segment_12        := rec.cr_sequence12;
				L_acc_validate_tbl(1).segment_13        := rec.cr_sequence13;
				L_acc_validate_tbl(1).segment_14        := rec.cr_sequence14;
				L_acc_validate_tbl(1).segment_15        := rec.cr_sequence15;
				L_acc_validate_tbl(1).segment_16        := rec.cr_sequence16;
				L_acc_validate_tbl(1).segment_17        := rec.cr_sequence17;
				L_acc_validate_tbl(1).segment_18        := rec.cr_sequence18;
				L_acc_validate_tbl(1).segment_19        := rec.cr_sequence19;
				L_acc_validate_tbl(1).segment_20	      := rec.cr_sequence20;
				--- 
				if ACC_VALIDATE_API.VALIDATE_ACC(L_error_message,
											L_acc_validate_tbl)= FALSE then
					write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'CR_CCID',L_error_message);
					l_error :=true;
		 
		 ---
				elsif L_acc_validate_tbl(1).account_status = 'invalid' then
				--  INTERNAL_VARIABLES.GV_CR_ACC_STATUS := 'N';
					write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,null,'INV_COMB_SEQ');
					l_error :=true;

				end if; 
		   end if;
      ELSE --GV_system_options_rec.financial_ap <> 'A'
     
			BEGIN
				 if RMS2FIN.GET_CCID(L_error_message, L_ccid, rec.set_of_books_id, rec.dr_sequence1, rec.dr_sequence2, rec.dr_sequence3, rec.dr_sequence4, rec.dr_sequence5, rec.dr_sequence6, rec.dr_sequence7, rec.dr_sequence8, rec.dr_sequence9, rec.dr_sequence10) = FALSE then
					write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'DR_CCID',L_error_message);
					l_error :=true;
				end if;
				rec.dr_ccid := to_number(SUBSTR(L_ccid,1,15));
			exception
				when VALUE_ERROR then
			  
					write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'DR_CCID','INV_ACCT_VALUE');
					l_error :=true;
			end;
			L_ccid:=null;
			begin
				if RMS2FIN.GET_CCID(L_error_message,
										  L_ccid,
										  rec.set_of_books_id,
										  rec.cr_sequence1,
										  rec.cr_sequence2,
										  rec.cr_sequence3,
										  rec.cr_sequence4,
										  rec.cr_sequence5,
										  rec.cr_sequence6,
										  rec.cr_sequence7,
										  rec.cr_sequence8,
										  rec.cr_sequence9,
										  rec.cr_sequence10) = FALSE then
					write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'CR_CCID',L_error_message);
					l_error :=true;
				end if;
				rec.cr_ccid := to_number(SUBSTR(L_ccid,1,15));
				exception
					when VALUE_ERROR then
				  
						write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'CR_CCID','INV_ACCT_VALUE');
						l_error :=true;
			end;

		end if; 
		 
	end if;  --not error
   
		 
	if NOT l_error then
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE1              := rec.CR_SEQUENCE1;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE2              := rec.CR_SEQUENCE2;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE3              := rec.CR_SEQUENCE3;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE4              := rec.CR_SEQUENCE4;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE5              := rec.CR_SEQUENCE5;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE6              := rec.CR_SEQUENCE6;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE7              := rec.CR_SEQUENCE7;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE8              := rec.CR_SEQUENCE8;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE9              := rec.CR_SEQUENCE9;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE10              := rec.CR_SEQUENCE10;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE11              := rec.DR_SEQUENCE11;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE12              := rec.DR_SEQUENCE12;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE13              := rec.DR_SEQUENCE13;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE14              := rec.DR_SEQUENCE14;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE15              := rec.DR_SEQUENCE15;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE16              := rec.DR_SEQUENCE16;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE17              := rec.DR_SEQUENCE17;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE18              := rec.DR_SEQUENCE18;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE19              := rec.DR_SEQUENCE19;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE20              := rec.DR_SEQUENCE20;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE11              := rec.CR_SEQUENCE11;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE12              := rec.CR_SEQUENCE12;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE13              := rec.CR_SEQUENCE13;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE14              := rec.CR_SEQUENCE14;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE15              := rec.CR_SEQUENCE15;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE16              := rec.CR_SEQUENCE16;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE17              := rec.CR_SEQUENCE17;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE18              := rec.CR_SEQUENCE18;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE19              := rec.CR_SEQUENCE19;
		SA_FIF_GL_CROSS_REF_temp_rec.STORE              := rec.STORE;
		SA_FIF_GL_CROSS_REF_temp_rec.ROLLUP_LEVEL_1              := rec.ROLLUP_LEVEL_1;
		SA_FIF_GL_CROSS_REF_temp_rec.ROLLUP_LEVEL_2              := rec.ROLLUP_LEVEL_2;
		SA_FIF_GL_CROSS_REF_temp_rec.ROLLUP_LEVEL_3              := rec.ROLLUP_LEVEL_3;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_SEQUENCE20              := rec.CR_SEQUENCE20;
		SA_FIF_GL_CROSS_REF_temp_rec.TOTAL_ID              := rec.TOTAL_ID;
		SA_FIF_GL_CROSS_REF_temp_rec.SET_OF_BOOKS_ID              := rec.SET_OF_BOOKS_ID;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_CCID              := rec.DR_CCID;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE1              := rec.DR_SEQUENCE1;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE2              := rec.DR_SEQUENCE2;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE3              := rec.DR_SEQUENCE3;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE4              := rec.DR_SEQUENCE4;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE5              := rec.DR_SEQUENCE5;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE6              := rec.DR_SEQUENCE6;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE7              := rec.DR_SEQUENCE7;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE8              := rec.DR_SEQUENCE8;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE9              := rec.DR_SEQUENCE9;
		SA_FIF_GL_CROSS_REF_temp_rec.DR_SEQUENCE10              := rec.DR_SEQUENCE10;
		SA_FIF_GL_CROSS_REF_temp_rec.CR_CCID              := rec.CR_CCID;
		if rec.action                    = stg_svc_sa_fif_gl_cross_ref.action_new then
			SA_FIF_GL_CROSS_REF_ins_rec.extend;
			SA_FIF_GL_CROSS_REF_ins_rec(SA_FIF_GL_CROSS_REF_ins_rec.count()):=SA_FIF_GL_CROSS_REF_temp_rec;
		end if;
		if rec.action = stg_svc_sa_fif_gl_cross_ref.action_mod then
			SA_FIF_GL_CROSS_REF_upd_rec.extend;
			SA_FIF_GL_CROSS_REF_upd_rec(SA_FIF_GL_CROSS_REF_upd_rec.count()):=SA_FIF_GL_CROSS_REF_temp_rec;
		end if;
		if rec.action = stg_svc_sa_fif_gl_cross_ref.action_del then
			SA_FIF_GL_CROSS_REF_del_rec.extend;
			SA_FIF_GL_CROSS_REF_del_rec(SA_FIF_GL_CROSS_REF_del_rec.count()):=SA_FIF_GL_CROSS_REF_temp_rec;
		end if;
	  end if;
	end LOOP;
	exec_SA_FIF_GL_CROSS_REF_ins(SA_FIF_GL_CROSS_REF_ins_rec);
	exec_SA_FIF_GL_CROSS_REF_upd(SA_FIF_GL_CROSS_REF_upd_rec);
	exec_SA_FIF_GL_CROSS_REF_del(SA_FIF_GL_CROSS_REF_del_rec);
	end process_SA_FIF_GL_CROSS_REF;
	-------------------------------------------------------------------------------
	---  Name: clear_staging_data 
	--Purpose : Clear data from stg table
	-------------------------------------------------------------------------------- 
	PROCEDURE clear_staging_data(I_process_id    IN Number)
	is
	BEGIN
		delete from SVC_SA_FIF_GL_CROSS_REF where process_id=I_process_id;
	end;
	-------------------------------------------------------------------------------
	---  Name: process 
	--Purpose : called from UI to load data ffrom stg to core tables
	-------------------------------------------------------------------------------- 
	FUNCTION process(
		 O_error_message IN OUT rtk_errors.rtk_text%type,
		 I_process_id    IN Number,
		 O_error_count OUT NUMBER)
	  RETURN BOOLEAN
	is
	  L_program VARCHAR2(255):='SA_FIF_GL_CROSS_REF.process';
	  l_process_status svc_process_tracker.status%type:='PS';
	  l_chunk_id number :=1;
	BEGIN
	  Lp_errors_tab := NEW errors_tab_typ();
	  process_SA_FIF_GL_CROSS_REF(I_process_id,l_chunk_id);
	  O_error_count := Lp_errors_tab.count();
	  forall i IN 1..O_error_count
		  insert INTO SVC_ADMIN_UPLD_ER VALUES Lp_errors_tab
			 (i
			 );
	  Lp_errors_tab := NEW errors_tab_typ();
	  if O_error_count    = 0 then
		 l_process_status := 'PS';
	  ELSE
		 l_process_status := 'PE';
	  end if;
	  update svc_process_tracker
	  SET status       =  (CASE
								  when status = 'PE'
								  then 'PE'
								  else L_process_status
							  end)
	  , action_date =sysdate
	  where process_id = I_process_id;
	  ---clear the staging tables for this process_id
	  clear_staging_data(I_process_id);
	  COMMIT;
	  RETURN true;
	EXCEPTION
		WHEN OTHERS then
		   clear_staging_data(I_process_id);
			O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
			RETURN FALSE;
	end process;
end CORESVC_SA_FIF_GL_CROSS_REF;
/ 