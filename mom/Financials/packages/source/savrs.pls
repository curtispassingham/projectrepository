
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_VR_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------

function get_signature (                                                                                 
                        v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                        n_vr_parm_seq_no   in     sa_vr_parms.vr_parm_seq_no%TYPE) 
         return varchar2 ;                                                                                     
                                                                                                    
PRAGMA RESTRICT_REFERENCES (get_signature, WNDS, WNPS) ;
                                                                                                    
--------------------------------------------------------------------------------

function available_realm (                                                                                 
                          v_error_message   out    varchar2,
                          b_avail           out    boolean,
                          v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                          n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                          n_vr_realm_seq_no in     sa_vr_realm.vr_realm_seq_no%TYPE,  
                          v_realm_id        in     sa_realm.realm_id%TYPE) 
         return boolean ;

--------------------------------------------------------------------------------

function find_parm (                                                                                 
                        v_error_message   out    varchar2,
                        n_vr_realm_seq_no out    sa_vr_realm.vr_realm_seq_no%TYPE,
                        v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,
                        v_parm_name       in     sa_parm.display_name%type) 
         return boolean ;
--------------------------------------------------------------------------------

function find_realm_id (                                                                                 
                        v_error_message   out    varchar2,
                        n_vr_realm_seq_no out    sa_vr_realm.vr_realm_seq_no%TYPE,
                        v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,
                        v_realm_id        in     sa_realm.realm_id%type) 
         return boolean ;
--------------------------------------------------------------------------------
/*
FUNCTION CHECK_DRIVING_REALMS (
                               v_error_message   out    varchar2,
                               n_vr_realm_seq_no out    sa_vr_realm.vr_realm_seq_no%TYPE,
                               v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                               n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------
*/
function insert_realm (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,
                       v_realm_id        in     sa_realm.realm_id%TYPE,
                       v_realm_name      in     sa_realm.display_name%TYPE,
                       v_outer_join_ind  in     sa_vr_realm.outer_join_ind%TYPE) 
         return boolean ;
-----------------------------------------------------------------------
/*
function insert_rollup_parms (                                                                                 
                              v_error_message   out    varchar2,
                              v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                              n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------
*/
function get_active_realm_id (                                                                                 
                              v_error_message   out    varchar2,
                              b_unique          out    boolean,
                              v_realm_id        out    sa_realm.realm_id%type,
                              v_display_name    in     sa_realm.display_name%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function get_active_parm_id (                                                                                 
                             v_error_message   out    varchar2,
                             b_unique          out    boolean,
                             v_parm_id         out    sa_parm.parm_id%type,
                             v_realm_id        in     sa_realm.realm_id%type,
                             v_display_name    in     sa_parm.display_name%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function new_link_info (                                                                                 
                        v_error_message    out    varchar2,
                        b_found            out    boolean,
                        n_vr_realm_seq_no  out    sa_vr_realm.vr_realm_seq_no%TYPE,
                        v_realm_alias      out    sa_vr_realm.realm_alias%TYPE,
                        v_parm_name        out    sa_parm.display_name%TYPE,
                        v_parm_id          out    sa_parm.parm_id%TYPE,
                        v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                        n_max_realm_seq_no in     sa_vr_realm.vr_realm_seq_no%TYPE,  
                        v_vr_parm_id       in     sa_parm.parm_id%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function get_link_info (                                                                                 
                        v_error_message   out    varchar2,
                        n_vr_realm_seq_no out    sa_vr_realm.vr_realm_seq_no%TYPE,
                        v_realm_alias     out    sa_vr_realm.realm_alias%TYPE,
                        v_parm_name       out    sa_parm.display_name%TYPE,
                        v_parm_id         out    sa_parm.parm_id%TYPE,
                        v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                        n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------
function find_link_parm (                                                                                 
                         v_error_message   out    varchar2,
                         n_vr_parm_seq_no  in out sa_vr_parms.vr_parm_seq_no%TYPE,
                         v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                         n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                         n_vr_realm_seq_no in     sa_vr_realm.vr_realm_seq_no%TYPE,
                         v_parm_id         in     sa_parm.parm_id%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function insert_link_parm (                                                                                 
                           v_error_message   out    varchar2,
                           n_vr_parm_seq_no  in out sa_vr_parms.vr_parm_seq_no%TYPE,
                           v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                           n_vr_realm_seq_no in     sa_vr_realm.vr_realm_seq_no%TYPE,
                           v_parm_id         in     sa_parm.parm_id%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function update_realm (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                       n_vr_realm_seq_no in     sa_vr_realm.vr_realm_seq_no%TYPE,
                       v_new_signature   in     sa_vr_realm.signature%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function update_signature (                                                                                 
                           v_error_message   out    varchar2,
                           v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                           n_vr_realm_seq_no in     sa_vr_realm.vr_realm_seq_no%TYPE,
                           v_new_signature   in     sa_vr_realm.signature%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function fetch_signature (                                                                                 
                          v_error_message   out    varchar2,
                          v_signature       out    sa_vr_realm.signature%TYPE,
                          v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                          n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                          n_vr_realm_seq_no in     sa_vr_realm.vr_realm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function dependants (                                                                                 
                     v_error_message    out    varchar2,
                     b_parm_dependants  out    boolean,
                     b_realm_dependants out    boolean,
                     v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                     n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                     n_vr_realm_seq_no  in     sa_vr_realm.vr_realm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_realm (                                                                                 
                       v_error_message    out    varchar2,
                       v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                       n_vr_realm_seq_no  in     sa_vr_realm.vr_realm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_linked_realm (                                                                                 
                              v_error_message    out    varchar2,
                              v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                              n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                              n_vr_realm_seq_no  in     sa_vr_realm.vr_realm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_all_realms (                                                                                 
                            v_error_message    out    varchar2,
                            v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                            n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_parms (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                       v_signature       in     sa_vr_realm.signature%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function check_realms (                                                                                 
                       v_error_message   out    varchar2,
                       b_valid           out    boolean,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_links (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                       n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function check_parm_dependants (                                                                                 
                                v_error_message   out    varchar2,
                                b_dependants      out    boolean,
                                v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_parm_dependants (                                                                                 
                                 v_error_message   out    varchar2,
                                 v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                 n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                 n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function check_rule_vr_dependants (                                                                                 
                                   v_error_message   out    varchar2,
                                   b_head            out    boolean,
                                   b_restrictions    out    boolean,
                                   v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                   n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                   n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function check_total_vr_dependants (                                                                                 
                                    v_error_message   out    varchar2,
                                    b_head            out    boolean,
                                    b_restrictions    out    boolean,
                                    v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                    n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                    n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_rule_vr_dependants (                                                                                 
                                    v_error_message   out    varchar2,
                                    v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                    n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                    n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_total_vr_dependants (                                                                                 
                                     v_error_message   out    varchar2,
                                     v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                     n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                     n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function remove_parm (                                                                                 
                      v_error_message   out    varchar2,
                      v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                      n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                      n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function insert_links (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                       v_signature       in     varchar2,
                       v_realm_id        in     sa_realm.realm_id%TYPE, 
                       n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE,
                       v_outer_join_ind  in     sa_vr_links.outer_join_ind%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function find_realm_signature (                                                                                 
                               v_error_message   out    varchar2,
                               n_vr_realm_seq_no out    sa_vr_realm.vr_realm_seq_no%TYPE,
                               v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                               n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                               v_realm_id        in     sa_vr_realm.realm_id%TYPE, 
                               v_signature       in     sa_vr_realm.signature%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------


function find_realm_alias (                                                                                 
                           v_error_message   out    varchar2,
                           n_vr_realm_seq_no out    sa_vr_realm.vr_realm_seq_no%TYPE,
                           v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                           v_realm_alias     in     sa_vr_realm.realm_alias%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function find_parm_alias (                                                                                 
                           v_error_message  out    varchar2,
                           n_vr_parm_seq_no out    sa_vr_parms.vr_parm_seq_no%TYPE,
                           v_vr_id          in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no      in     sa_vr_head.vr_rev_no%TYPE,  
                           v_parm_alias     in     sa_vr_parms.vr_parm_alias%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function find_vr_name (                                                                                 
                       v_error_message  out    varchar2,
                       v_vr_id          out    sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no      out    sa_vr_head.vr_rev_no%TYPE,  
                       v_vr_name        in     sa_vr_head.vr_name%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function fetch_realm_alias (                                                                                 
                            v_error_message   out    varchar2,
                            v_realm_alias     out    sa_vr_realm.realm_alias%TYPE, 
                            v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                            n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                            n_vr_realm_seq_no in     sa_vr_realm.vr_realm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function fetch_parm_alias (                                                                                 
                           v_error_message   out    varchar2,
                           v_parm_alias      out    sa_vr_parms.vr_parm_alias%TYPE, 
                           v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                           n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function fetch_parm_type_id (                                                                                 
                             v_error_message   out    varchar2,
                             v_parm_type_id    out    sa_parm.parm_type_id%TYPE, 
                             v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                             n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                             n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean ;
--------------------------------------------------------------------------------

function next_vr_parm_seq_no (                                                                                 
                              v_error_message   out    varchar2,
                              n_vr_parm_seq_no  out    sa_vr_parms.vr_parm_seq_no%TYPE,     
                              v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                              n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function next_vr_realm_seq_no (                                                                                 
                               v_error_message   out    varchar2,
                               n_vr_realm_seq_no out    sa_vr_realm.vr_realm_seq_no%TYPE,     
                               v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                               n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function insert_vr_head (                                                                                 
                         v_error_message   out     varchar2,
                         v_vr_id           out     sa_vr_head.vr_id%TYPE,     
                         v_vr_name         in      sa_vr_head.vr_name%TYPE, 
                         v_realm_id        in     sa_realm.realm_id%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function lock_vr_head (                                                                                 
                       v_error_message   out     varchar2,
                       v_vr_id           in      sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function update_vr_name (                                                                                 
                         v_error_message   out     varchar2,
                         v_vr_id           in out  sa_vr_head.vr_id%TYPE,
                         n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE,
                         v_vr_name         in      sa_vr_head.vr_name%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function update_driving_realm (                                                                                 
                               v_error_message   out     varchar2,
                               v_vr_id           in out  sa_vr_head.vr_id%TYPE,
                               n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE,
                               v_realm_id        in      sa_realm.realm_id%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------
 
function delete_vr_head (                                                                                 
                         v_error_message   out     varchar2,
                         v_vr_id           in out  sa_vr_head.vr_id%TYPE) 
         return boolean ;
--------------------------------------------------------------------------------

function make_unique_vr_name (                                                                                 
                                 v_error_message   out     varchar2,
                                 v_vr_name         out     sa_vr_head.vr_name%TYPE,     
                                 v_name            in      sa_vr_head.vr_name%TYPE)     
         return boolean ;
--------------------------------------------------------------------------------

function make_unique_parm_alias (                                                                                 
                                 v_error_message   out     varchar2,
                                 v_vr_parm_alias   out     sa_vr_parms.vr_parm_alias%TYPE,     
                                 v_vr_id           in      sa_vr_head.vr_id%TYPE,     
                                 n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE,
                                 v_parm_name       in      sa_vr_parms.vr_parm_alias%TYPE)     
         return boolean ;
--------------------------------------------------------------------------------

function make_unique_realm_alias (                                                                                 
                                 v_error_message   out     varchar2,
                                 v_realm_alias     out     sa_vr_realm.realm_alias%TYPE,     
                                 v_vr_id           in      sa_vr_head.vr_id%TYPE,     
                                 n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE,     
                                 v_realm_name      in      sa_vr_parms.vr_parm_alias%TYPE)     
         return boolean ;
--------------------------------------------------------------------------------
--- Purpose: This function is called in the create revision process for both
---          totals and rules to insert into the VR tables.
---
FUNCTION CREATE_VR_REVISION(O_error_message     IN OUT  VARCHAR2,
                            I_new_realm_id      IN      SA_REALM.REALM_ID%TYPE,
                            I_vr_id             IN      SA_VR_HEAD.VR_ID%TYPE,
                            I_old_vr_rev_no     IN      SA_VR_HEAD.VR_REV_NO%TYPE,
                            I_new_vr_rev_no     IN      SA_VR_HEAD.VR_REV_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
END SA_VR_SQL;
/
