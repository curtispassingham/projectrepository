
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_AUDIT_RULES_SQL IS
-------------------------------------------------------------------------
FUNCTION REBUILD_RULE   (O_error_message     IN OUT  VARCHAR2,
                         I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE,
                         I_rule_rev_no       IN      SA_RULE_HEAD.RULE_REV_NO%TYPE,
                         I_new_vr_id         IN      SA_RULE_COMP.VR_ID%TYPE,
                         I_new_vr_rev_no     IN      SA_RULE_COMP.VR_REV_NO%TYPE)
RETURN BOOLEAN IS

   L_new_rule_rev_no          SA_RULE_HEAD.RULE_REV_NO%TYPE;
   L_valid_multi_rev_ind      VARCHAR2(1);
   L_rule_status              SA_RULE_HEAD.STATUS%TYPE;
   L_start_date               SA_RULE_HEAD.START_BUSINESS_DATE%TYPE;
   L_end_date                 SA_RULE_HEAD.END_BUSINESS_DATE%TYPE;
   L_wiz_ind                  SA_RULE_HEAD.WIZ_IND%TYPE;
   ---
   cursor C_RULE_INFO is
         select start_business_date,
                end_business_date,
                wiz_ind
           from sa_rule_head
          where rule_id     = I_rule_id
            and rule_rev_no = I_rule_rev_no;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_RULE_INFO',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   open  C_RULE_INFO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_RULE_INFO',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   fetch C_RULE_INFO into L_start_date,
                          L_end_date,
                          L_wiz_ind;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_RULE_INFO',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   close C_RULE_INFO;
   ---
   if SA_AUDIT_RULES_SQL.CREATE_REVISION (O_error_message,
                                          L_new_rule_rev_no,
                                          L_rule_status,
                                          I_rule_id,
                                          I_rule_rev_no) = FALSE then
      RETURN FALSE;
   end if;
   ---
   if SA_RULE_SQL.DROP_INVALID_MULTI_VERSIONS(O_error_message,
                                              I_rule_id,
                                              L_new_rule_rev_no,
                                              L_start_date,
                                              L_end_date) = FALSE then
      return FALSE;
   end if;
   ---
   if L_wiz_ind = 'Y' then
      if SA_AUDIT_RULES_SQL.BUILD_AUDIT_RULE (O_error_message,
                                              I_rule_id,
                                              L_new_rule_rev_no,
                                              FALSE,
                                              L_rule_status) != 0 then
         --- Build function failed
         if SA_AUDIT_RULES_SQL.DROP_AUDIT_RULE(I_rule_id,
                                               L_new_rule_rev_no,
                                               O_error_message) != 0 then
            return FALSE;
         end if;
         return FALSE;
      end if;
   end if;
   ---
   RETURN TRUE;
   ---
EXCEPTION
      WHEN others THEN
         o_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                   SQLERRM,
                                                   'SA_AUDIT_RULES_SQL.REBUILD_RULE',
                                                   SQLCODE);
         RETURN FALSE;
END REBUILD_RULE;
-------------------------------------------------------------------------
FUNCTION CREATE_REVISION(O_error_message     IN OUT  VARCHAR2,
                         O_new_rule_rev_no   IN OUT  SA_RULE_HEAD.RULE_REV_NO%TYPE,
                         O_rule_status       IN OUT  SA_RULE_HEAD.STATUS%TYPE,
                         I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE,
                         I_rule_rev_no       IN      SA_RULE_HEAD.RULE_REV_NO%TYPE)
RETURN BOOLEAN IS

   L_max_rev_no         SA_RULE_HEAD.RULE_REV_NO%TYPE;
   L_vr_id              SA_RULE_COMP.vr_id%TYPE;
   L_vr_rev_no          SA_RULE_COMP.vr_rev_no%TYPE;
   L_max_vr_rev_no      SA_RULE_COMP.vr_rev_no%TYPE;
   L_original_realm_id  SA_REALM.REALM_ID%TYPE;
   L_new_realm_id       SA_REALM.REALM_ID%TYPE;
   L_program            VARCHAR2(60) := 'SA_AUDIT_RULES_SQL.CREATE_REVISIONS';
   ---
   cursor C_GET_STATUS is
      select status
        from sa_rule_head
       where rule_id = I_rule_id
         and rule_rev_no = I_rule_rev_no;
   ---
   cursor C_GET_MAX_REV is
      select max(rule_rev_no)
        from SA_rule_HEAD
       where rule_id = I_rule_id;
   ---
   cursor C_GET_VR_INFO is
      select vr_id,
             vr_rev_no
        from sa_rule_comp
       where rule_id     = I_rule_id
         and rule_rev_no = I_rule_rev_no;    -- possibly L_max_rev_no;
   ---
   cursor C_GET_MAX_VR_REV is
      select max(vr_rev_no)
        from sa_rule_comp
       where rule_id     = I_rule_id
         and rule_rev_no = I_rule_rev_no;    -- possibly L_max_rev_no;
   ---
   cursor C_GET_ORIGINAL_REALM_ID is
      select driving_realm_id
        from sa_vr_head
       where vr_id     = L_vr_id
         and vr_rev_no = L_vr_rev_no;
   ---
BEGIN
   if I_rule_id is NULL or I_rule_rev_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STATUS',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   open C_GET_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STATUS',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   fetch C_GET_STATUS into O_rule_status;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STATUS',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   close C_GET_STATUS;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_MAX_REV',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   open C_GET_MAX_REV;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_MAX_REV',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   fetch C_GET_MAX_REV into L_max_rev_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_MAX_REV',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   close C_GET_MAX_REV;
   ---
   O_new_rule_rev_no := L_max_rev_no +1;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_VR_INFO',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   open  C_GET_VR_INFO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_VR_INFO',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   fetch C_GET_VR_INFO into L_vr_id,
                            L_vr_rev_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_VR_INFO',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   close C_GET_VR_INFO;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_MAX_VR_REV',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   open  C_GET_MAX_VR_REV;
   SQL_LIB.SET_MARK('FETCH',
                    'C_MAX_VR_REV',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   fetch C_GET_MAX_VR_REV into L_max_vr_rev_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_MAX_VR_REV',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   close C_GET_MAX_VR_REV;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ORIGINAL_REALM_ID',
                    'SA_VR_HEAD',
                    'VR ID: '||L_vr_id);
   open  C_GET_ORIGINAL_REALM_ID;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ORIGINAL_REALM_ID',
                    'SA_VR_HEAD',
                    'VR ID: '||L_vr_id);
   fetch C_GET_ORIGINAL_REALM_ID into L_original_realm_id;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ORIGINAL_REALM_ID',
                    'SA_VR_HEAD',
                    'VR ID: '||L_vr_id);
   close C_GET_ORIGINAL_REALM_ID;
   ---
   --- Insert into Rule tables
   ---
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   insert into sa_rule_head
               (rule_id,
                rule_rev_no,
                rule_name,
                status,
                bal_level,
                update_datetime,
                update_id,
                total_tran_ind,
                rule_stop_ind,
                pres_abs_ind,
                wiz_ind,
                start_business_date,
                end_business_date,
                key_val_1_parm_seq_no,
                key_val_2_parm_seq_no,
                execute_order,
                rec_type)
       select   rule_id,
                O_new_rule_rev_no,
                rule_name,
                status,
                bal_level,
                sysdate,
                user,
                total_tran_ind,
                rule_stop_ind,
                pres_abs_ind,
                wiz_ind,
                start_business_date,
                end_business_date,
                key_val_1_parm_seq_no,
                key_val_2_parm_seq_no,
                execute_order,
                rec_type
         from sa_rule_head
        where rule_id     = I_rule_id
          and rule_rev_no = I_rule_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_RULE_HEAD_TL',
                    'Rule ID: '||I_rule_id);

   insert into sa_rule_head_tl
               (lang,
                rule_id,
                rule_rev_no,
                rule_name,
                create_datetime,
                create_id,
                last_update_datetime,
                last_update_id)
         select lang,
                rule_id,
                O_new_rule_rev_no,
                rule_name,
                sysdate,
                get_user(),
                sysdate,
                get_user()
           from sa_rule_head_tl
          where rule_id     = I_rule_id
            and rule_rev_no = I_rule_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   insert into sa_rule_comp
               (rule_id,
                rule_rev_no,
                rule_comp_seq_no,
                vr_id,
                vr_rev_no)
       select rule_id,
              O_new_rule_rev_no,
              rule_comp_seq_no,
              L_vr_id,
              L_max_vr_rev_no + 1
         from sa_rule_comp
        where rule_id     = I_rule_id
          and rule_rev_no = I_rule_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_RULE_COMP_RESTRICTIONS',
                    'Rule ID: '||I_rule_id);
   insert into sa_rule_comp_restrictions
               (rule_id,
                rule_rev_no,
                rule_comp_seq_no,
                res_seq_no,
                parm_seq_no_1,
                res_operator,
                parm_seq_no_2,
                parm_seq_no_3,
                res_constant_2,
                res_constant_3,
                tolerance,
                tolerance_type)
       select rule_id,
              O_new_rule_rev_no,
              rule_comp_seq_no,
              res_seq_no,
              parm_seq_no_1,
              res_operator,
              parm_seq_no_2,
              parm_seq_no_3,
              res_constant_2,
              res_constant_3,
              tolerance,
              tolerance_type
         from sa_rule_comp_restrictions
        where rule_id      = I_rule_id
          and rule_rev_no = I_rule_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_RULE_LOC_TRAIT',
                    'Rule ID: '||I_rule_id);
   insert into sa_rule_loc_trait
               (rule_id,
                rule_rev_no,
                loc_trait)
       select rule_id,
              O_new_rule_rev_no,
              loc_trait
         from sa_rule_loc_trait
        where rule_id     = I_rule_id
          and rule_rev_no = I_rule_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_RULE_ERRORS',
                    'Rule ID: '||I_rule_id);
   insert into sa_rule_errors
               (rule_id,
                rule_rev_no,
                error_code)
       select rule_id,
              O_new_rule_rev_no,
              error_code
         from sa_rule_errors
        where rule_id     = I_rule_id
          and rule_rev_no = I_rule_rev_no;
   ---
   --- Insert into VR tables
   ---
   if L_original_realm_id is NOT NULL then
      if SA_REALM_SQL.GET_MAX_REALM_ID(O_error_message,
                                       L_new_realm_id,
                                       L_original_realm_id) = FALSE then
         return FALSE;
      end if;
      ---
      if SA_VR_SQL.CREATE_VR_REVISION(O_error_message,
                                      L_new_realm_id,
                                      L_vr_id,
                                      L_vr_rev_no,
                                      L_max_vr_rev_no + 1) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   RETURN TRUE;
   ---
   EXCEPTION
      WHEN others THEN
         o_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                   SQLERRM,
                                                   L_program,
                                                   SQLCODE);
         RETURN FALSE;
   END CREATE_REVISION;
-------------------------------------------------------------------------
    FUNCTION BUILD_AUDIT_RULE
    (
        o_error_message    IN OUT VARCHAR2,
        i_rule_id          IN     sa_rule_head.rule_id%TYPE,
        i_rule_rev_no      IN     sa_rule_head.rule_rev_no%TYPE,
        i_refresh_override IN     BOOLEAN,
        i_rule_status      IN     SA_RULE_HEAD.STATUS%TYPE
    )
           RETURN INT
   /****************************************************************\
   |                                                                |
   |   build_audit_rule                                             |
   |                                                                |
   |   This function will generate a single rule function as        |
   |   defined in the sa_rule_head and corresponding tables.        |
   |                                                                |
   |    Arguments:          Description:                            |
   |    i_rule_id           Rule identifier to create.              |
   |    i_rule_rev_no       Will determine which version to create. |
   |    i_refresh_override  If value passed is 'Y' the existing     |
   |                        function (regardless of the date will   |
   |                        be deleted first.                       |
   |    o_error_message     Returned error message                  |
   |                                                                |
   |    Called from:        Sales Audit form: sa_rule.fmb           |
   |                        Sales Audit nightly batch: sarules.pc   |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
    IS
        L_program           VARCHAR2(50) := 'SA_AUDIT_RULES.BUILD_AUDIT_RULES';
        cursor_handle       INTEGER;     /* The DBMS cursor handle */
        feedback            INTEGER;     /* integer result capture variable */
        b_string            BOOLEAN      := FALSE;
        b_bal_group         BOOLEAN      := FALSE;
        b_store_day         BOOLEAN      := FALSE;
        b_select_done       BOOLEAN      := FALSE;
        b_group_by_done     BOOLEAN      := FALSE;
        b_between_op        BOOLEAN      := FALSE;
        b_do_first_and      BOOLEAN      := FALSE;
        b_print_having      BOOLEAN      := FALSE;
        b_tolerance         BOOLEAN      := FALSE;
        b_errors            boolean      := false;
        d_end_date          VARCHAR2(8);
        d_start_date        VARCHAR2(8);
        e_total_def_error   EXCEPTION;
        l_outer_join_ind    sa_vr_links.outer_join_ind%TYPE;
        l_count_sum_ind     sa_total_head.count_sum_ind%TYPE;
        l_rule_comp_seq_no  sa_rule_comp.rule_comp_seq_no%TYPE;
        n_seq_no            sa_vr_parms.vr_parm_seq_no%TYPE;
        n_sum_parm_seq_no   sa_vr_parms.vr_parm_seq_no%TYPE;
        n_counter           INTEGER := 0;
        n_bal_group         INTEGER := 0;
        n_store_day         INTEGER := 0;
        n_error_check_count INTEGER := 0;
        n_count             NUMBER; /* for loop counter */
        s_total_ind         SA_RULE_HEAD.TOTAL_TRAN_IND%TYPE;
        v_data_type         sa_parm_type.data_type%TYPE;
        v_where             VARCHAR2(2000);
        v_having            VARCHAR2(2000);
        v_object_name       VARCHAR2(61);
        v_realm_id          v_sa_realm_info.realm_id%TYPE;
        v_constant_2        SA_RULE_COMP_RESTRICTIONS.res_constant_2%TYPE;
        v_constant_3        SA_RULE_COMP_RESTRICTIONS.res_constant_3%TYPE;
        v_group_by          VARCHAR2(100);
        v_name              VARCHAR2(50); /* stores function name */
        v_bal_level         sa_total_head.bal_level%TYPE;
        v_sql               DBMS_SQL.VARCHAR2s; /* dynamic sql variable */
        v_operator          VARCHAR2(30);
        v_select            sa_total_common.v_code_array := sa_total_common.empty_v_out;
        v_realm2            SA_REALM.DISPLAY_NAME%TYPE;
        v_realm3            SA_REALM.DISPLAY_NAME%TYPE;
        v_parm2             SA_PARM.DISPLAY_NAME%TYPE;
        v_parm3             SA_PARM.DISPLAY_NAME%TYPE;
        v_rec_type          SA_RULE_HEAD.REC_TYPE%TYPE;
        v_pres_abs_ind      SA_RULE_HEAD.PRES_ABS_IND%TYPE;
        v_rule_stop_ind     SA_RULE_HEAD.RULE_STOP_IND%TYPE;
        n_tolerance         SA_RULE_COMP_RESTRICTIONS.TOLERANCE%TYPE;
        v_tolerance_type    SA_RULE_COMP_RESTRICTIONS.TOLERANCE_TYPE%TYPE;
        v_error_code        SA_ERROR.ERROR_CODE%TYPE;
        L_parm_seq_no_2     SA_RULE_COMP_RESTRICTIONS.PARM_SEQ_NO_2%TYPE;
        L_parm_seq_no_3     SA_RULE_COMP_RESTRICTIONS.PARM_SEQ_NO_3%TYPE;
        L_parm_seq_no       SA_RULE_COMP_RESTRICTIONS.PARM_SEQ_NO_2%TYPE;
        v_status            all_objects.status%TYPE;
        v_db_date_format    VARCHAR2(16) := 'YYYYMMDDHH24MISS';

        /* retrieves the realms for the FROM statement */
        CURSOR c_get_realms IS
            SELECT DISTINCT realm_id, realm_name
            FROM   v_sa_realm_info
            WHERE  realm_id IN (
                SELECT realm_id
                FROM   v_sa_parm_info
                WHERE  parm_id in (
                    SELECT svp.parm_id
                    FROM   sa_vr_parms svp, sa_rule_comp src
                    WHERE  src.rule_id = I_rule_id
                    AND    src.rule_rev_no = I_rule_rev_no
                    AND    svp.vr_rev_no = src.vr_rev_no
                    AND    svp.vr_id = src.vr_id));
        /******************************************************/
        /*                                                    */
        /* Retrieve the some info from sa_rule_head table     */
        /*                                                    */
        /******************************************************/
        CURSOR c_get_rule_head_info IS
            SELECT total_tran_ind,
                   rec_type,
                   to_char(start_business_date,'YYYYMMDD'),
                   to_char(end_business_date, 'YYYYMMDD'),
                   pres_abs_ind,
                   rule_stop_ind
             FROM  sa_rule_head
             WHERE rule_id = i_rule_id
             AND   rule_rev_no = i_rule_rev_no;
        /******************************************************/
        /*                                                    */
        /* Retrieve the select columns for the sa_error table */
        /*                                                    */
        /******************************************************/
        CURSOR c_get_select_parms IS
            SELECT  DISTINCT vp.realm_name || '.' || vp.parm_name,
                    stp.vr_parm_seq_no, pti.data_type,
                    st.bal_level, to_char(st.end_business_date,'YYYYMMDD'),
                    to_char(st.start_business_date, 'YYYYMMDD'),
                    st.total_tran_ind, st.pres_abs_ind,
                    st.rule_stop_ind, st.rec_type
            FROM    v_sa_realm_info vr, v_sa_parm_info vp, sa_vr_parms stp, sa_parm_type pti,
                    sa_rule_head st, sa_rule_comp rc
            WHERE   vr.realm_id = vp.realm_id
            AND     vp.parm_id = stp.parm_id
            AND     st.rule_id = i_rule_id
            AND     st.rule_rev_no = i_rule_rev_no
            AND     st.rule_id = rc.rule_id
            AND     st.rule_rev_no = rc.rule_rev_no
            AND     rc.vr_id = stp.vr_id
            AND     rc.vr_rev_no = stp.vr_rev_no
            AND     vp.parm_type_id = pti.parm_type_id
            AND     (stp.vr_parm_seq_no = st.key_val_1_parm_seq_no
                     OR
                     stp.vr_parm_seq_no = st.key_val_2_parm_seq_no)
            ORDER BY stp.vr_parm_seq_no DESC;
        /* Retrieves the join columns for the WHERE clause */
        cursor c_get_where_clause is
            select distinct ri.realm_name || '.' || pi.parm_name || ' = '
                   || ri1.realm_name || '.' || pi1.parm_name, tl.outer_join_ind
              from sa_vr_links tl, v_sa_realm_info ri,
                   v_sa_parm_info pi, sa_vr_parms tp1, v_sa_realm_info ri1,
                   v_sa_parm_info pi1, sa_rule_comp rc
             where ri.realm_id = pi.realm_id
               and ri1.realm_id = pi1.realm_id
               and pi.parm_id = tl.link_to_parm_id
               and pi1.parm_id = tp1.parm_id
               and tl.vr_id = rc.vr_id
               and tl.vr_rev_no = rc.vr_rev_no
               and tp1.vr_parm_seq_no = tl.link_to_parm_seq_no
               and tp1.vr_id = tl.vr_id
               and tp1.vr_rev_no = tl.vr_rev_no
               and rc.rule_id = i_rule_id
               and rc.rule_rev_no = i_rule_rev_no;
         /* In order to group the varios components restrictions
            together, I will retrieve the comp_seq_nos first,
            and for each one, retrieve the corresponding comp
            restrictions */
        /* Retrieve the Rule Components */
        CURSOR c_get_components IS
            SELECT  rule_comp_seq_no
            FROM    sa_rule_comp
            WHERE   rule_id = i_rule_id
            AND     rule_rev_no = i_rule_rev_no;
        /* retrieves the exceptions for the WHERE clause */
        CURSOR c_get_exceptions IS
            SELECT  ri.realm_name || '.' || pi.parm_name,
                    tr.res_operator, pti.data_type, tr.res_constant_2,
                    tr.res_constant_3, tr.parm_seq_no_1,
                    tr.tolerance, tr.tolerance_type,
                    tr.parm_seq_no_2, tr.parm_seq_no_3
            FROM    sa_rule_comp_restrictions tr, sa_vr_parms tp,
                    v_sa_parm_info pi, v_sa_realm_info ri, sa_parm_type pti,
                    sa_rule_comp rc
            WHERE   ri.realm_id = pi.realm_id
            AND     tp.vr_parm_seq_no = tr.parm_seq_no_1
            AND     tp.parm_id = pi.parm_id
            AND     pti.parm_type_id = pi.parm_type_id
            AND     rc.rule_id = i_rule_id
            AND     rc.rule_rev_no = i_rule_rev_no
            and     rc.rule_comp_seq_no = l_rule_comp_seq_no
            and     tr.rule_id = rc.rule_id
            and     tr.rule_rev_no = rc.rule_rev_no
            AND     tr.rule_comp_seq_no = rc.rule_comp_seq_no
            and     rc.vr_id = tp.vr_id
            and     rc.vr_rev_no = tp.vr_rev_no
            ORDER BY tr.rule_comp_seq_no;
        /* retrieve the info for each extra parameter */
        cursor c_get_parameter_info is
            select ri.realm_name, pi.parm_name
              from sa_vr_parms tp, v_sa_parm_info pi, v_sa_realm_info ri,
                   sa_rule_comp th
             where ri.realm_id = pi.realm_id
               and tp.vr_parm_seq_no = L_parm_seq_no
               and tp.parm_id = pi.parm_id
               and th.rule_id = i_rule_id
               and th.rule_rev_no = i_rule_rev_no
               and th.rule_comp_seq_no = l_rule_comp_seq_no
               and tp.vr_id = th.vr_id
               and tp.vr_rev_no = th.vr_rev_no
             ORDER BY tp.vr_parm_seq_no;
        CURSOR c_get_error_info IS
            SELECT  error_code
            FROM    sa_rule_errors
            WHERE   rule_id = i_rule_id
            AND     rule_rev_no =
                    (SELECT MAX(sa_rule_head.rule_rev_no)
                     FROM   sa_rule_head
                     WHERE  sa_rule_head.rule_id = i_rule_id);
        cursor c_check_compile is
            select status
              from all_objects
             where object_name = v_name
               and object_type = 'FUNCTION'
               and owner       = sa_total_common.internal_schema_name;
        cursor C_get_start_end_date is
           select to_char(start_business_date,'YYMMDD'),
                  to_char(end_business_date,'YYMMDD')
             from sa_rule_head
            where rule_id     = I_rule_id
              and rule_rev_no = I_rule_rev_no;
    BEGIN
        /* this function gives the user the option to maintain many
            different revisions of the same rule running.  If the user
            wishes not to do this, they can pass a 'Y' in the refresh
            indicator and any existing functions for the current rule
            would be deleted before another is created */
        IF i_refresh_override THEN
            feedback := drop_audit_rule(i_rule_id, i_rule_rev_no, o_error_message);
            IF feedback <> 0 THEN
                RETURN(feedback);
            END IF;
        END IF;
        /***************************************************************\
        |                                                               |
        |   Retrieve the start and end dates for the function.          |
        |                                                               |
        \***************************************************************/
      SQL_LIB.SET_MARK('OPEN','C_get_start_end_date','SA_RULE_HEAD',NULL);
        open  C_get_start_end_date;
        SQL_LIB.SET_MARK('FETCH','C_get_start_end_date','SA_RULE_HEAD',NULL);
        fetch C_get_start_end_date into d_start_date, d_end_date;
        SQL_LIB.SET_MARK('CLOSE','C_get_start_end_date','SA_RULE_HEAD',NULL);
        close C_get_start_end_date;
        /***************************************************************\
        |                                                               |
        |   Create the dynamic rule function                            |
        |                                                               |
        \***************************************************************/
        if d_end_date is not NULL then
           v_name := 'SA_R_' || i_rule_id || '_' || d_start_date || '_' || d_end_date;
        else
           v_name := 'SA_R_' || i_rule_id || '_' || d_start_date;
        end if;
        v_select(v_select.count+1) := 'CREATE OR REPLACE FUNCTION ' ||
                sa_total_common.internal_schema_name || '.' || v_name;
        v_select(v_select.count+1) := '   (';
        v_select(v_select.count+1) := '    i_store_day_seq_no IN   sa_store_day.store_day_seq_no%TYPE DEFAULT NULL,';
        v_select(v_select.count+1) := '    o_error_message   OUT   VARCHAR2';
        v_select(v_select.count+1) := '   )';
        v_select(v_select.count+1) := 'RETURN INT';
        v_select(v_select.count+1) := 'IS';
        v_select(v_select.count+1) := '   l_rule_id       sa_rule_head.rule_id%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_rule_rev_no   sa_rule_head.rule_rev_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_key_value1    sa_error.key_value_1%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_key_value2    sa_error.key_value_1%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_total_seq_no  SA_TOTAL.TOTAL_SEQ_NO%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_tran_seq_no   SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_error_code    sa_error_codes.error_code%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_store_day_seq_no sa_total.store_day_seq_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_bal_group_seq_no sa_total.bal_group_seq_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_tran_date     sa_store_day.business_date%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_store         sa_store_day.store%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_day           sa_store_day.day%TYPE := NULL;';
        v_select(v_select.count+1) := '   b_result        BOOLEAN;';
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := 'CURSOR c_audit_rule IS';
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*   Parameters - Select statement    */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   retrieve the parameters...select statement                  |
        |                                                               |
        \***************************************************************/
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_SELECT_PARMS',
                          'V_SA_REALM_INFO, V_SA_PARM_INFO, SA_VR_PARMS, SA_PARM_TYPE, SA_RULE_HEAD, SA_RULE_COMP',
                          'Rule ID: '||I_rule_id);
        OPEN c_get_select_parms;
        LOOP
            FETCH c_get_select_parms INTO v_object_name,
                                          n_seq_no,
                                          v_data_type,
                                          v_bal_level,
                                          d_end_date,
                                          d_start_date,
                                          s_total_ind,
                                          v_pres_abs_ind,
                                          v_rule_stop_ind,
                                          v_rec_type;
            EXIT WHEN c_get_select_parms%NOTFOUND;
            BEGIN
                IF NOT b_select_done THEN
                    IF s_total_ind = 'TR' THEN
                        v_select(v_select.count+1) := 'SELECT SA_TRAN_HEAD.TRAN_SEQ_NO T_IND, ';
                        b_select_done := TRUE;
                    ELSIF s_total_ind = 'TO' THEN
                        /* Total rec_type*/
                        v_select(v_select.count+1) := 'SELECT ' || v_object_name || ' T_IND, ';
                        b_select_done := TRUE;
                    ELSIF s_total_ind = 'ST' THEN
                        /* Store rec_type*/
                        /* Should ensure to only bring back unique store_day_seq_no */
                        v_select(v_select.count+1) := 'SELECT DISTINCT ' || v_object_name || ' T_IND, ';
                        b_select_done := TRUE;
                    ELSIF s_total_ind = 'BG' THEN
                        /* Balance Group rec_type*/
                        v_select(v_select.count+1) := 'SELECT ' || v_object_name || ' T_IND, ';
                        b_select_done := TRUE;
                    ELSE
                        /* Incorrect total tran indicator */
                        o_error_message := sql_lib.create_msg('SAAUDIT_TOTRIND',
                                                              i_rule_id,
                                                              NULL,
                                                              NULL);
                        return (-1);
                    END IF;
                    IF v_pres_abs_ind = 'P' THEN
                       /* no need to check bal_level for 'S' since
                          store_day is always implied  */
                       /* We assume that all the Balance group tables are
                           already defined in the links and parameters
                           section, therefore we can simply add them to
                           the selection statement */
                       if v_rec_type != 'BALG' then
                          if v_bal_level = 'R' then
                            v_select(v_select.count+1) := '       V_SA_BALANCE_GROUP_REGISTER.BAL_GROUP_SEQ_NO BAL_GROUP, ';
                            b_bal_group := true;
                         elsif v_bal_level = 'C' then
                            v_select(v_select.count+1) := '       V_SA_BALANCE_GROUP_CASHIER.BAL_GROUP_SEQ_NO BAL_GROUP, ';
                            b_bal_group := true;
                         end if;
                       end if;
                    END IF; /* pres_abs check */
                END IF; /* b_select not done check */

                IF v_pres_abs_ind = 'P' THEN
                    /* Add the roll-up columns as defined */
                    n_counter := n_counter + 1;

                    if s_total_ind in ('TO', 'ST', 'BG') then
                       if n_counter > 1 then
                        v_select(v_select.count+1) := '       ' || v_object_name ||
                           ' KEY_VAL' || n_counter || ', ';
                       end if;
                    else
                       IF b_select_done THEN
                          v_select(v_select.count+1) := '       ' || v_object_name ||
                             ' KEY_VAL' || n_counter || ', ';
                       ELSE
                          v_select(v_select.count+1) := 'SELECT ' || v_object_name ||
                             ' KEY_VAL' || n_counter || ', ';
                          b_select_done := TRUE;
                       END IF;

                   end if; /* total_tran_ind check */

                END IF; /* pres abs check */

            END; /* end loop */

        END LOOP;
        CLOSE c_get_select_parms;

        if s_total_ind in ('TO', 'ST', 'BG') and v_pres_abs_ind = 'P' then
           n_counter := n_counter - 1;
        end if;

        if not b_select_done then
           SQL_LIB.SET_MARK('OPEN',
                            'C_GET_RULE_HEAD_INFO',
                            'SA_RULE_HEAD',
                            'Rule ID: '||I_rule_id);
           OPEN c_get_rule_head_info;
           SQL_LIB.SET_MARK('FETCH',
                            'C_GET_RULE_HEAD_INFO',
                            'SA_RULE_HEAD',
                            'Rule ID: '||I_rule_id);
           FETCH c_get_rule_head_info INTO s_total_ind,
                                           v_rec_type,
                                           d_start_date,
                                           d_end_date,
                                           v_pres_abs_ind,
                                           v_rule_stop_ind;
           SQL_LIB.SET_MARK('CLOSE',
                            'C_GET_RULE_HEAD_INFO',
                            'SA_RULE_HEAD',
                            'Rule ID: '||I_rule_id);
           CLOSE c_get_rule_head_info;
           if s_total_ind = 'TR' and v_rec_type = 'THEAD' then
              v_select(v_select.count+1) := 'SELECT SA_TRAN_HEAD.TRAN_SEQ_NO T_IND, ';
                        b_select_done := TRUE;
           else

              /* There are no select parameters defined */
              o_error_message := sql_lib.create_msg('SAAUDIT_NOPARMS',
                                                    i_rule_id,
                                                    NULL,
                                                    NULL);
              return (-1);
           end if;

        end if;

        /* remove the final comma from the select statement */
        v_select(v_select.count) := rtrim(v_select(v_select.count), ', ');
        b_select_done := FALSE;
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*      Realms - From Clause    */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   get the realms...FROM statement                             |
        |                                                               |
        \***************************************************************/
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_REALMS',
                         'V_SA_REALM_INFO',
                         'Rule ID: '||I_rule_id);
        OPEN c_get_realms;
        LOOP
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_REALMS',
                             'V_SA_REALM_INFO',
                             'Rule ID: '||I_rule_id);
            FETCH c_get_realms INTO v_realm_id, v_object_name;
            EXIT WHEN c_get_realms%NOTFOUND;
            BEGIN
                n_error_check_count := n_error_check_count + 1;
                /* check to determine if this is the first statement in the FROM clause */
                IF NOT b_select_done THEN
                    v_select(v_select.count+1) := 'FROM ' || v_object_name || ',';
                    b_select_done := TRUE;
                ELSE
                    v_select(v_select.count+1) := '     ' || v_object_name || ', ';
                END IF;
            END;
        END LOOP;
        CLOSE c_get_realms;
        IF n_error_check_count = 0 THEN
            RAISE e_total_def_error;
        END IF;
        n_error_check_count := 0;
        v_select(v_select.count) := rtrim(v_select(v_select.count), ', ');
        b_select_done := FALSE;
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*      Links - Where Clause      */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   get the links...where statement                             |
        |                                                               |
        \***************************************************************/
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_WHERE_CLAUSE',
                         'SA_VR_LINKS, V_SA_REALM_INFO, V_SA_PARM_INFO, SA_VR_PARMS, SA_RULE_COMP',
                         'Rule ID: '||I_rule_id);
        OPEN c_get_where_clause;
        LOOP
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_WHERE_CLAUSE',
                             'SA_VR_LINKS, V_SA_REALM_INFO, V_SA_PARM_INFO, SA_VR_PARMS, SA_RULE_COMP',
                             'Rule ID: '||I_rule_id);
            FETCH c_get_where_clause INTO v_where, l_outer_join_ind;
            EXIT WHEN c_get_where_clause%NOTFOUND;
            BEGIN
                n_error_check_count := n_error_check_count + 1;
                /* check to determine if this is the first statement in the where clause */
                IF NOT b_select_done THEN
                    v_select(v_select.count+1) := 'WHERE ' || v_where;
                    b_select_done := TRUE;
                ELSE
                    v_select(v_select.count+1) := 'AND ' || v_where;
                END IF;
                /* check to see if there is an outer join for this link */
                IF l_outer_join_ind = 'Y' THEN
                    v_select(v_select.count) := v_select(v_select.count) || '(+)';
                END IF;
                /* check to see if the current parameter is store_day and set flag */
                IF instr(upper(v_where), 'STORE_DAY_SEQ_NO') > 0 THEN
                    b_store_day := TRUE;
                END IF;
                /* check to see if the current parameter is bal_group and set flag */
                IF instr(upper(v_where), 'BAL_GROUP_SEQ_NO') >0 THEN
                   if v_rec_type != 'BALG' then
                      b_bal_group := TRUE;
                   end if;
                END IF;
            END;
        END LOOP;
        CLOSE c_get_where_clause;
        IF n_error_check_count = 0 THEN
            RAISE e_total_def_error;
        END IF;
        n_error_check_count := 0;
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*      Restrictions       */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   get the restrictions...where statement                      |
        |                                                               |
        \***************************************************************/
        IF b_select_done THEN
           v_select(v_select.count+1) := 'AND (';
        ELSE
           v_select(v_select.count+1) := 'WHERE (';
           b_select_done := TRUE;
        END IF;
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_COMPONENTS',
                         'SA_RULE_COMP',
                         'Rule ID: '||I_rule_id);
        OPEN c_get_components;
        LOOP
            v_select(v_select.count+1) := '      (';
            b_do_first_and := FALSE;
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_COMPONENTS',
                             'SA_RULE_COMP',
                             'Rule ID: '||I_rule_id);
            FETCH   c_get_components INTO l_rule_comp_seq_no;
            EXIT WHEN c_get_components%NOTFOUND;
            BEGIN
                SQL_LIB.SET_MARK('OPEN',
                                 'C_GET_EXCEPTIONS',
                                 'SA_RULE_COMP_RESTRICTIONS, SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_PARM_TYPE, SA_RULE_COMP',
                                 'Rule ID: '||I_rule_id);
                OPEN c_get_exceptions;
                LOOP
                    SQL_LIB.SET_MARK('OPEN',
                                     'C_GET_EXCEPTIONS',
                                     'SA_RULE_COMP_RESTRICTIONS, SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_PARM_TYPE, SA_RULE_COMP',
                                     'Rule ID: '||I_rule_id);
                    FETCH c_get_exceptions INTO v_object_name, v_operator, v_data_type,
                          v_constant_2, v_constant_3, n_seq_no, n_tolerance, v_tolerance_type,
                          L_parm_seq_no_2, L_parm_seq_no_3;
                    EXIT WHEN c_get_exceptions%NOTFOUND;
                    BEGIN
                        n_error_check_count := n_error_check_count + 1;
                        /* determine if the operator is a Between or Not Between,
                            NULL or NOT NULL */
                        IF v_operator = 'B' THEN
                            v_operator := 'BETWEEN';
                            b_between_op := TRUE;
                        ELSIF v_operator = 'NB' THEN
                            v_operator := 'NOT BETWEEN';
                            b_between_op := TRUE;
                        ELSIF v_operator = 'NULL' THEN
                            v_operator := 'IS NULL';
                            b_between_op := FALSE;
                        ELSIF v_operator = '!NULL' THEN
                            v_operator := 'IS NOT NULL';
                            b_between_op := FALSE;
                        ELSIF v_operator = 'TOL' THEN
                            v_operator := 'BETWEEN';
                            b_tolerance := TRUE;
                            b_between_op := FALSE;
                        ELSIF v_operator = '!TOL' THEN
                            v_operator := 'NOT BETWEEN';
                            b_tolerance := TRUE;
                            b_between_op := FALSE;
                        ELSIF v_operator = 'IN' THEN
                            b_between_op := FALSE;
                        ELSIF v_operator = 'NOT IN' THEN
                            b_between_op := FALSE;
                        ELSE
                            b_between_op := FALSE;
                        END IF;
                        /* Determine if we are using parmaters or constants */
                        /* if parameters are defined */
                        if (L_parm_seq_no_2 is NOT NULL) then
                            /* get the parameter2 info */
                            L_parm_seq_no := L_parm_seq_no_2;
                            SQL_LIB.SET_MARK('OPEN',
                                             'C_GET_PARAMETER_INFO',
                                             'SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_RULE_COMP',
                                             'Rule ID: '||I_rule_id);
                            open c_get_parameter_info;
                            SQL_LIB.SET_MARK('FETCH',
                                             'C_GET_PARAMETER_INFO',
                                             'SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_RULE_COMP',
                                             'Rule ID: '||I_rule_id);
                            fetch c_get_parameter_info INTO v_realm2,
                                                            v_parm2;
                            SQL_LIB.SET_MARK('CLOSE',
                                             'C_GET_PARAMETER_INFO',
                                             'SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_RULE_COMP',
                                             'Rule ID: '||I_rule_id);
                            close c_get_parameter_info;
                            IF v_realm2 is null or v_parm2 is null THEN
                                raise e_total_def_error;
                            END IF;
                            if (L_parm_seq_no_3 is NOT NULL) then
                                /* get the parameter3 info */
                                L_parm_seq_no := L_parm_seq_no_3;
                                open c_get_parameter_info;
                                fetch c_get_parameter_info INTO v_realm3,
                                                                v_parm3;
                                close c_get_parameter_info;
                                IF v_realm3 is null or v_parm3 is null THEN
                                    raise e_total_def_error;
                                END IF;
                            END if;
                            IF b_tolerance THEN
                                IF n_tolerance IS NOT NULL THEN
                                    IF v_tolerance_type = 'P' THEN
                                        IF b_do_first_and THEN
                                            /* Percent */
                                            v_select(v_select.count+1) := 'AND ' ||
                                                v_object_name || ' ' || v_operator || ' ('
                                                || v_realm2 || '.' || v_parm2 || ' - (' ||
                                                v_realm2 || '.' || v_parm2 ||
                                                ' * (.01 * ' || n_tolerance || ')))';
                                        ELSE
                                            /* Percent */
                                            v_select(v_select.count+1) := '     ' ||
                                                v_object_name || ' ' || v_operator || ' ('
                                                || v_realm2 || '.' || v_parm2 || ' - (' ||
                                                v_realm2 || '.' || v_parm2 ||
                                                ' * (.01 * ' || n_tolerance || ')))';
                                            b_do_first_and := TRUE;
                                        END IF;
                                    ELSE
                                        IF b_do_first_and THEN
                                            /* Amount */
                                            v_select(v_select.count+1) := 'AND ' ||
                                                v_object_name || ' ' || v_operator || ' ('
                                                || v_realm2 || '.' || v_parm2 || ' - ' ||
                                                n_tolerance || ')';
                                        ELSE
                                            /* Amount */
                                            v_select(v_select.count+1) := '     ' ||
                                                v_object_name || ' ' || v_operator || ' ('
                                                || v_realm2 || '.' || v_parm2 || ' - ' ||
                                                n_tolerance || ')';
                                            b_do_first_and := TRUE;
                                        END IF;
                                    END IF;
                                ELSE
                                    RAISE e_total_def_error;
                                END IF;
                            ELSE
                                IF b_do_first_and THEN
                                   IF v_operator = '!=' or v_operator = '+' THEN
                                    v_select(v_select.count+1) := 'AND NVL(' ||
                                        v_object_name||',-999999)' || ' ' || v_operator || '  NVL(' ||
                                        v_realm2 || '.' || v_parm2||',-999999)';
                                   ELSE
                                    v_select(v_select.count+1) := 'AND ' ||
                                        v_object_name || ' ' || v_operator || ' ' ||
                                        v_realm2 || '.' || v_parm2;
                                   END IF;
                                ELSE
                                   IF v_operator = '!=' or v_operator = '+' THEN
                                    v_select(v_select.count+1) := '     NVL(' ||
                                        v_object_name||',-999999)' || ' ' || v_operator || '  NVL(' ||
                                        v_realm2 || '.' || v_parm2||',-999999)';
                                   ELSE
                                    v_select(v_select.count+1) := '     ' ||
                                        v_object_name || ' ' || v_operator || ' ' ||
                                        v_realm2 || '.' || v_parm2;
                                   END IF;
                                    b_do_first_and := TRUE;
                                END IF;
                            END IF;

                            IF b_between_op OR b_tolerance THEN

                                /* Check for Tolerance operator */
                                IF b_tolerance THEN
                                    IF n_tolerance IS NOT NULL THEN
                                        IF v_tolerance_type = 'P' THEN
                                            /* Percent */
                                            v_select(v_select.count) := v_select(v_select.count) ||
                                                ' AND (' || v_realm2 || '.' || v_parm2 ||
                                                ' + (' || v_realm2 || '.' || v_parm2 ||
                                                ' * (.01 * ' || n_tolerance || ')))';
                                            b_select_done := TRUE;
                                        ELSE
                                            /* Amount */
                                            v_select(v_select.count) := v_select(v_select.count) ||
                                                ' AND (' || v_realm2 || '.' || v_parm2 || ' + ' ||
                                                n_tolerance || ')';
                                            b_select_done := TRUE;
                                        END IF;
                                    ELSE
                                        RAISE e_total_def_error;
                                    END IF;
                                ELSE
                                    v_select(v_select.count) := v_select(v_select.count) ||
                                        ' AND ' || v_realm2 || '.' || v_parm2;
                                END IF;

                            END IF;

                        /* Constants are defined for this exception */
                        ELSIF (v_constant_2 IS NOT NULL) THEN

                           if v_operator = 'IN' or v_operator = 'NOT IN' THEN
                               v_constant_2 := '(' || v_constant_2 || ')';
                           end if;

                            /* determine if the data type of the constant is a string or numeric */
                            IF instr(v_data_type, 'VARCHAR') > 0 THEN
                                /* If the operator is IN we do not put quotes around the constants */
                                IF v_operator != 'IN' AND v_operator != 'NOT IN' THEN
                                    b_string := TRUE;
                                END IF;
                            /* If date, need to format the object name to char since the date(s) are
                               saved as varchar2 strings.*/
                            ELSIF instr(v_data_type, 'DATE') > 0 THEN
                                v_object_name := 'To_Char (' || v_object_name || ', ' || chr(39) ||
                                                  v_db_date_format || chr(39) || ')';
                                /* If the operator is IN we do not put quotes around the constants */
                                IF v_operator != 'IN' AND v_operator != 'NOT IN' THEN
                                    b_string := TRUE;
                                END IF;
                            END IF;
                            /* The constant datatype is numeric */
                            IF NOT b_string THEN
                                /* Check for Tolerance operator */
                                IF b_tolerance THEN
                                    IF n_tolerance IS NOT NULL THEN
                                        IF v_tolerance_type = 'P' THEN
                                            IF b_do_first_and THEN
                                                /* Percent */
                                                v_select(v_select.count+1) := 'AND ' ||
                                                    v_object_name || ' ' || v_operator || ' ('
                                                    || v_constant_2 || ' - (' || v_constant_2 ||
                                                    ' * (.01 * ' || n_tolerance || ')))';
                                            ELSE
                                                /* Percent */
                                                v_select(v_select.count+1) := '     ' ||
                                                    v_object_name || ' ' || v_operator || ' ('
                                                    || v_constant_2 || ' - (' || v_constant_2 ||
                                                    ' * (.01 * ' || n_tolerance || ')))';
                                                b_do_first_and := TRUE;
                                            END IF;
                                        ELSE
                                            IF b_do_first_and THEN
                                                /* Amount */
                                                v_select(v_select.count+1) := 'AND ' ||
                                                    v_object_name || ' ' || v_operator || ' ('
                                                    || v_constant_2 || ' - ' || n_tolerance || ')';
                                            ELSE
                                                /* Amount */
                                                v_select(v_select.count+1) := '     ' ||
                                                    v_object_name || ' ' || v_operator || ' ('
                                                    || v_constant_2 || ' - ' || n_tolerance || ')';
                                                b_do_first_and := TRUE;
                                            END IF;
                                        END IF;
                                    ELSE
                                        RAISE e_total_def_error;
                                    END IF;
                                ELSE
                                    IF b_do_first_and THEN
                                        v_select(v_select.count+1) := 'AND ' || v_object_name
                                            || ' ' || v_operator || ' ' || v_constant_2;
                                    ELSE
                                        v_select(v_select.count+1) := '    ' || v_object_name
                                            || ' ' || v_operator || ' ' || v_constant_2;
                                        b_do_first_and := TRUE;
                                    END IF;
                                END IF;
                                ---
                                IF b_between_op OR b_tolerance THEN
                                    /* Check for Tolerance operator */
                                    IF b_tolerance THEN
                                        IF n_tolerance IS NOT NULL THEN
                                            IF v_tolerance_type = 'P' THEN
                                                /* Percent */
                                                v_select(v_select.count) := v_select(v_select.count) ||
                                                    ' AND (' || v_constant_2 || ' + (' ||
                                                    v_constant_2 || ' * (.01 * ' ||
                                                    n_tolerance || ')))';
                                                b_select_done := TRUE;
                                            ELSE
                                                /* Amount */
                                                v_select(v_select.count) := v_select(v_select.count) ||
                                                    ' AND (' || v_constant_2 || ' + ' ||
                                                    n_tolerance || ')';
                                                b_select_done := TRUE;
                                            END IF;
                                        ELSE
                                            RAISE e_total_def_error;
                                        END IF;
                                    ELSE
                                        v_select(v_select.count) := v_select(v_select.count) ||
                                            ' AND ' || v_constant_3;
                                    END IF;
                                END IF;
                            ---
                            /* if the constant datatype is a string */
                            ELSE
                                IF b_do_first_and THEN
                                    v_select(v_select.count+1) := 'AND ' || v_object_name
                                        || ' ' || v_operator || ' ' || chr(39) || v_constant_2 || chr(39);
                                ELSE
                                    v_select(v_select.count+1) := '     ' || v_object_name
                                        || ' ' || v_operator || ' ' || chr(39) || v_constant_2 || chr(39);
                                    b_do_first_and := TRUE;
                                END IF;
                                /* if contstant 3 is not null, it must be a between statement */
                                IF v_constant_3 IS NOT NULL THEN
                                    IF b_between_op THEN
                                        v_select(v_select.count) := v_select(v_select.count) ||
                                            ' AND ' || CHR(39) || v_constant_3 || CHR(39);
                                    ELSE
                                        RAISE e_total_def_error;
                                    END IF;
                                ELSE
                                    IF b_between_op THEN
                                        RAISE e_total_def_error;
                                    END IF;
                                END IF; /* v_constant_3 is NOT NULL */
                            END IF; /* Datatype is a string */
                        /* Check for the is NULL or IS NOT NULL operators */
                        ELSIF v_operator = 'IS NULL' OR v_operator = 'IS NOT NULL' THEN
                            IF b_do_first_and THEN
                                v_select(v_select.count+1) := 'AND ' || v_object_name
                                    || ' ' || v_operator;
                            ELSE
                                v_select(v_select.count+1) := '     ' || v_object_name
                                    || ' ' || v_operator;
                                b_do_first_and := TRUE;
                            END IF;
                        ELSE
                            /* No constants or parameters are defined */
                            RAISE e_total_def_error;
                        END IF; /* check for parms or constants */
                        b_string := false;
                        b_tolerance := false;
                    END;  /* c_get_exceptions Begin */
                END LOOP;   /* c_get_exceptions Loop */
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_GET_EXCEPTIONS',
                                 'SA_RULE_COMP_RESTRICTIONS, SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_PARM_TYPE, SA_RULE_COMP',
                                 'Rule ID: '||I_rule_id);
                close c_get_exceptions;
                v_select(v_select.count+1) := '      )';
                v_select(v_select.count+1) := '      OR';
            END; /* c_get_components Begin */
        END LOOP; /* c_get_components Loop */
        CLOSE c_get_components;
        ---
        IF n_error_check_count = 0 THEN
           v_select(v_select.count-4) := '              ';
           v_select(v_select.count-3) := '              ';
           v_select(v_select.count-2) := '              ';
           v_select(v_select.count-1) := '              ';
           v_select(v_select.count) := '              ';
        ELSE
           v_select(v_select.count-1) := '    )          ';
           v_select(v_select.count) := '              ';
        END IF;
        ---
        n_error_check_count := 0;
        /* include the exception that filters out only the passed store day */
        IF NOT b_select_done THEN
            v_select(v_select.count+1) := 'WHERE ' ||
                'SA_STORE_DAY.STORE_DAY_SEQ_NO = i_store_day_seq_no';
            b_select_done := TRUE;
        ELSE
            v_select(v_select.count+1) := 'AND ' ||
                'SA_STORE_DAY.STORE_DAY_SEQ_NO = i_store_day_seq_no';
        END IF;
        v_select(v_select.count+1) := 'AND SA_STORE_DAY.STORE = l_store';
        v_select(v_select.count+1) := 'AND SA_STORE_DAY.DAY = l_day;';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := 'BEGIN';
        /* include the date constraints for starting and ending the total */
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  /* Check the date contraints */';
        v_select(v_select.count+1) := '        ';

        v_select(v_select.count+1) := '   if not sa_total_common.get_tran_datetime(i_store_day_seq_no, l_tran_date, l_store, l_day, o_error_message) then';
        v_select(v_select.count+1) := '      o_error_message := sql_lib.create_msg(' || CHR(39) || 'SATOTRUL_NOSTRDAY' || CHR(39) || ',';
        v_select(v_select.count+1) := '                                            i_store_day_seq_no, NULL, NULL);';
        v_select(v_select.count+1) := '      return(-1);';
        v_select(v_select.count+1) := '   end if;';
        v_select(v_select.count+1) := '        ';
        ---
        if d_end_date is NOT NULL then
            v_select(v_select.count+1) := '  if (l_tran_date >= TO_DATE(' || CHR(39) ||
                d_start_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD'  || CHR(39) || ')) and (l_tran_date <= TO_DATE(' || CHR(39) ||
                d_end_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD' || CHR(39) || ')) then';
        else
            v_select(v_select.count+1) := '  if l_tran_date >= TO_DATE(' || CHR(39) ||
                d_start_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD' || CHR(39) || ') then';
        end if;
        ---
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '    /* Begin Cursor loop */';
        v_select(v_select.count+1) := '        ';
        IF v_pres_abs_ind = 'P' THEN
           v_select(v_select.count+1) := '    FOR L_rec IN c_audit_rule LOOP';
           v_select(v_select.count+1) := '        ';
           IF s_total_ind = 'TR' THEN /*transaction */
              v_select(v_select.count+1) := '        l_tran_seq_no        := L_rec.T_IND;';
           ELSIF s_total_ind = 'TO' THEN /*total */
              v_select(v_select.count+1) := '        l_total_seq_no       := L_rec.T_IND;';
           ELSIF s_total_ind = 'BG' THEN /*balance group */
              v_select(v_select.count+1) := '        l_bal_group_seq_no   := L_rec.T_IND;';
           ELSE
              NULL; --- s_total_ind equals 'ST' for store level and nothing else is needed.
           END IF;
           /* if bal_group is present, include it in the fetch */
           IF b_bal_group THEN
              v_select(v_select.count+1) := '        l_bal_group_seq_no   := L_rec.BAL_GROUP;';
           END IF;
           /* include all ref_nos defined in the fetch statement */
           FOR n_count IN 1..n_counter LOOP
              v_select(v_select.count+1) := '        l_key_value' || n_count ||
                  '         := L_rec.KEY_VAL' || n_count || ';';
           END LOOP;
        ELSE /* absense error */
            v_select(v_select.count+1) := '     OPEN c_audit_rule;';
            v_select(v_select.count+1) := '        ';
            v_select(v_select.count+1) := '     FETCH c_audit_rule INTO ';
            IF s_total_ind = 'TR' THEN
                v_select(v_select.count) := v_select(v_select.count) || 'l_tran_seq_no;';
            ELSIF s_total_ind = 'TO' THEN /*total */
                v_select(v_select.count) := v_select(v_select.count) || 'l_total_seq_no;';
            ELSIF s_total_ind = 'BG' THEN /*balance group */
                v_select(v_select.count) := v_select(v_select.count) || 'l_bal_group_seq_no;';
            ELSE  -- s_total_ind equals 'ST' for store level and nothing else is needed.
                v_select(v_select.count) := v_select(v_select.count) || 'l_store_day_seq_no;';
            END IF;
            v_select(v_select.count+1) := '        ';
            v_select(v_select.count+1) := '     IF c_audit_rule%NOTFOUND THEN';
            v_select(v_select.count+1) := '        ';
        END IF;
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '        /* Insert the error into the database */';
        v_select(v_select.count+1) := '        ';
        /***************************************************************\
        |                                                               |
        |  retrieve the error codes                                     |
        |                                                               |
        \***************************************************************/
        /* Enter an insert_error for each error_code defined for
            the audit rule */
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_ERROR_INFO',
                         'SA_RULE_ERRORS',
                         'Rule ID: '||I_rule_id);
        OPEN c_get_error_info;
        LOOP
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_ERROR_INFO',
                             'SA_RULE_ERRORS',
                             'Rule ID: '||I_rule_id);
            FETCH c_get_error_info INTO v_error_code;
            EXIT WHEN c_get_error_info%NOTFOUND;
            BEGIN
                v_select(v_select.count+1) := '          b_result := SA_AUDIT_RULES_SQL.insert_error (';
                v_select(v_select.count+1) := '             o_error_message, ';
                /* We always pass the store day */
                v_select(v_select.count+1) := '             i_store_day_seq_no,';
                /* If the bal group is defined, pass variable, otherwise pass NULL */
                IF b_bal_group or v_rec_type = 'BALG' THEN
                    v_select(v_select.count+1) := '             l_bal_group_seq_no,';
                ELSE
                    v_select(v_select.count+1) := '             NULL,';
                END IF;
                v_select(v_select.count+1) := '             l_total_seq_no,';
                v_select(v_select.count+1) := '             l_tran_seq_no,';
                /* Include the ref_no variables for ref_nos defined */
                if n_counter = 2 then
                   v_select(v_select.count+1) := '             l_key_value1,';
                   v_select(v_select.count+1) := '             l_key_value2,';
                elsif n_counter = 1 then
                   v_select(v_select.count+1) := '             l_key_value1,';
                   v_select(v_select.count+1) := '             NULL,';
                else
                   v_select(v_select.count+1) := '             NULL,';
                   v_select(v_select.count+1) := '             NULL,';
                end if;
                v_select(v_select.count+1) := '             ' || chr(39) || v_error_code ||
                    chr(39) || ',';
                v_select(v_select.count+1) := '             ' || chr(39) || v_rec_type ||
                    chr(39) || ',';
                /* Include NULL for the orig_value */
                v_select(v_select.count+1) := '                 NULL,';
v_select(v_select.count+1) := '             ' || chr(39) || I_rule_status ||
                    chr(39) || ',';
                v_select(v_select.count+1) := '             l_store,';
                v_select(v_select.count+1) := '             l_day);';
                /* check the result of the insert total function */
                v_select(v_select.count+1) := '        ';
                v_select(v_select.count+1) := '          /* Check to see if the insert succeeded */';
                v_select(v_select.count+1) := '        ';
                v_select(v_select.count+1) := '          IF NOT b_result THEN';
                v_select(v_select.count+1) := '             RETURN(-1);';
                v_select(v_select.count+1) := '          END IF;';
                v_select(v_select.count+1) := '          ';
                b_errors := true;
            END;
        END LOOP; /* error_code loop */
        ---
        /* Determine if errors were defined, otherwise error out. */
        IF not b_errors THEN
            o_error_message := sql_lib.create_msg('SAAUDIT_NOERRORS',
                                                  i_rule_id,
                                                  NULL,
                                                  NULL);
            RETURN(-1);
        END IF;
        ---
        /* Determine the correct update for the error_ind according
           to the rec_type */
        IF v_pres_abs_ind = 'P' THEN
           IF v_rec_type = 'THEAD' THEN
              v_select(v_select.count+1) := '         UPDATE sa_tran_head';
              v_select(v_select.count+1) := '         SET    error_ind = ' || chr(39) ||
                   'Y' || chr(39);
              v_select(v_select.count+1) := '         WHERE  tran_seq_no = l_tran_seq_no';
              v_select(v_select.count+1) := '         AND    store = l_store';
              v_select(v_select.count+1) := '         AND    day = l_day';
              v_select(v_select.count+1) := '         AND    store_day_seq_no = i_store_day_seq_no;';
              v_select(v_select.count+1) := '          ';
           ELSIF v_rec_type = 'TITEM' THEN
              v_select(v_select.count+1) := '         UPDATE sa_tran_item';
              v_select(v_select.count+1) := '         SET    error_ind = ' || chr(39) ||
                  'Y' || chr(39);
              v_select(v_select.count+1) := '         WHERE  tran_seq_no = l_tran_seq_no';
              v_select(v_select.count+1) := '         AND    store = l_store';
              v_select(v_select.count+1) := '         AND    day = l_day';
              v_select(v_select.count+1) := '         AND    item_seq_no = l_key_value1;';
              v_select(v_select.count+1) := '          ';
           ELSIF v_rec_type = 'TTEND' THEN
              v_select(v_select.count+1) := '         UPDATE sa_tran_tender';
              v_select(v_select.count+1) := '         SET    error_ind = ' || chr(39) ||
                  'Y' || chr(39);
              v_select(v_select.count+1) := '         WHERE  tran_seq_no = l_tran_seq_no';
              v_select(v_select.count+1) := '         AND    store = l_store';
              v_select(v_select.count+1) := '         AND    day = l_day';
              v_select(v_select.count+1) := '         AND    tender_seq_no = l_key_value1;';
              v_select(v_select.count+1) := '          ';
           ELSIF v_rec_type = 'TTAX' THEN
              v_select(v_select.count+1) := '         UPDATE sa_tran_tax';
              v_select(v_select.count+1) := '         SET    error_ind = ' || chr(39) ||
                  'Y' || chr(39);
              v_select(v_select.count+1) := '         WHERE  tran_seq_no = l_tran_seq_no';
              v_select(v_select.count+1) := '         AND    store = l_store';
              v_select(v_select.count+1) := '         AND    day = l_day';
              v_select(v_select.count+1) := '         AND    tax_seq_no = l_key_value1;';
              v_select(v_select.count+1) := '          ';
           ELSIF v_rec_type = 'IDISC' THEN
              v_select(v_select.count+1) := '         UPDATE sa_tran_disc';
              v_select(v_select.count+1) := '         SET    error_ind = ' || chr(39) ||
                  'Y' || chr(39);
              v_select(v_select.count+1) := '         WHERE  tran_seq_no = l_tran_seq_no';
              v_select(v_select.count+1) := '         AND    store = l_store';
              v_select(v_select.count+1) := '         AND    day = l_day';
              v_select(v_select.count+1) := '         AND    item_seq_no = l_key_value1';
              v_select(v_select.count+1) := '         AND    discount_seq_no = l_key_value2;';
              v_select(v_select.count+1) := '          ';
           ELSIF v_rec_type = 'TOTAL' THEN
              v_select(v_select.count+1) := '         UPDATE sa_total';
              v_select(v_select.count+1) := '         SET    error_ind = ' || chr(39) ||
                  'Y' || chr(39);
              v_select(v_select.count+1) := '         WHERE  total_seq_no = l_total_seq_no';
              v_select(v_select.count+1) := '         AND    store = l_store';
              v_select(v_select.count+1) := '         AND    day = l_day';
              v_select(v_select.count+1) := '         AND    store_day_seq_no = i_store_day_seq_no;';
              v_select(v_select.count+1) := '          ';
           END IF;

           -- If rule_stop_ind is 'Y', then stop immediately after writing error
           IF v_rule_stop_ind = 'Y' THEN
              v_select(v_select.count+1) := '               ';
              v_select(v_select.count+1) := '     RETURN(1);';
           END IF;

           v_select(v_select.count+1) := '      END LOOP;';

        ELSE /* Absence rule */
           -- If rule_stop_ind is 'Y', then stop immediately after writing error
           IF v_rule_stop_ind = 'Y' THEN
              v_select(v_select.count+1) := '               ';
              v_select(v_select.count+1) := '     RETURN(1);';
           END IF;
           v_select(v_select.count+1) := '     END IF; /* not found */';
       END IF;
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  END IF; /* date check */';
        /* Add error trapping */
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  RETURN(0);';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  EXCEPTION';
        v_select(v_select.count+1) := '      WHEN OTHERS THEN';
        v_select(v_select.count+1) := '          o_error_message := sql_lib.create_msg(';
        v_select(v_select.count+1) := '              ' || chr(39) || 'PACKAGE_ERROR' || chr(39) || ',';
        v_select(v_select.count+1) := '              SQLERRM,';
        v_select(v_select.count+1) := '              ' || chr(39) || v_name || chr(39) || ',';
        v_select(v_select.count+1) := '              SQLCODE);';
        v_select(v_select.count+1) := '          RETURN(-1);';
        v_select(v_select.count+1) := 'END;';
        /* load the DBMS_SQL.VARCHAR2 variable with the v_select data */
        FOR n_count IN 1..v_select.count
        LOOP
            v_sql(v_sql.count+1) := v_select(n_count);
        END LOOP;
        /* open the DBMS_SQL cursor */
        cursor_handle := DBMS_SQL.OPEN_CURSOR;
        /* pass in the dynamic function code for execution */
        DBMS_SQL.PARSE (cursor_handle, v_sql, v_sql.FIRST,
                        v_sql.LAST, TRUE, DBMS_SQL.NATIVE);
        /* execute the dynamic function code */
        feedback := DBMS_SQL.EXECUTE(cursor_handle);
        /* close cursor */
        DBMS_SQL.CLOSE_CURSOR(cursor_handle);
        /***************************************************************\
        |                                                               |
        |   This will determine if the code compiled successfully       |
        |                                                               |
        \***************************************************************/
        SQL_LIB.SET_MARK('OPEN',
                         'C_CHECK_COMPILE',
                         'ALL_OBJECTS',
                         'Object Name: '||v_name);
        open c_check_compile;
        SQL_LIB.SET_MARK('FETCH',
                         'C_CHECK_COMPILE',
                         'ALL_OBJECTS',
                         'Object Name: '||v_name);
        fetch c_check_compile INTO v_status;
        SQL_LIB.SET_MARK('CLOSE',
                         'C_CHECK_COMPILE',
                         'ALL_OBJECTS',
                         'Object Name: '||v_name);
        close c_check_compile;
        if v_status = 'INVALID' then
            /* Compile did not work */
            o_error_message := sql_lib.create_msg('SAAUDITB_COMPILE',
                                                  i_rule_id,
                                                  NULL,
                                                  NULL);
            RETURN(-1);
        end if;
        /***************************************************************\
        |                                                               |
        |   Delete any invalid definition errors for this rule          |
        |                                                               |
        \***************************************************************/
        if SA_BUILD_TOTAL_SQL.DELETE_INVALID_DEF_ERROR(O_error_message,
                                                       NULL, --- total id
                                                       i_rule_id) = FALSE then
           return (-1);
        end if;
        ---
        RETURN(0);
    EXCEPTION
        WHEN e_total_def_error THEN
            /* We found a total that was incorrectly defined */
            o_error_message := sql_lib.create_msg('SAAUDIT_WRONGDEF',
                                                  i_rule_id,
                                                  NULL,
                                                  NULL);
            RETURN(-1);
        WHEN others THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                   SQLERRM,
                                                   'SA_AUDIT_RULES_SQL.BUILD_AUDIT_RULE',
                                                   SQLCODE);
            RETURN(-1);
    END build_audit_rule;

--------------------------------------------------------------------------

    FUNCTION drop_audit_rule
    (
        i_rule_id       IN     sa_rule_head.rule_id%TYPE,
        i_rule_rev_no   IN     SA_RULE_HEAD.RULE_REV_NO%TYPE,
        o_error_message IN OUT VARCHAR2
    )
           RETURN INT
   /****************************************************************\
   |                                                                |
   |   drop_audit_rule                                              |
   |                                                                |
   |   This function will drop a single audit rule function if the  |
   |   rev_no is passed into the function. Otherwise the function   |
   |   will drop all audit rule functions for the given rule_id.    |
   |                                                                |
   |    Arguments:          Description:                            |
   |    i_rule_id           Total identifier to create.             |
   |    o_error_message     Returned error message                  |
   |                                                                |
   |    Called from:        Sales Audit form: sa_audit.fmb          |
   |                        Sales Audit dynamic rule functions      |
   |                                                                |
   |    Created:                                                    |
   |    September 20, 1999                                          |
   |                                                                |
   \****************************************************************/
    IS
        result           BOOLEAN;
        cursor_handle    INTEGER;
        feedback         INTEGER;
        v_name           VARCHAR2(30);
        v_sql            VARCHAR2(2000);
        d_end_date       VARCHAR2(6);
        d_start_date     VARCHAR2(6);
        v_dates          VARCHAR2(18) := NULL;

        CURSOR c_get_function IS
            SELECT object_name
              FROM all_objects
             WHERE object_name = 'SA_R_' || upper(I_rule_id) || v_dates
               AND object_type = 'FUNCTION'
               AND lower(owner) = lower(sa_total_common.internal_schema_name)
               AND NOT EXISTS (
                   SELECT 'x'
                     FROM all_jobs
                     WHERE instr(UPPER(what), UPPER(object_name)) <>0);
        ---
        cursor C_GET_RULE_INFO is
          select to_char(x.start_business_date, 'YYMMDD'),
                 to_char(x.end_business_date, 'YYMMDD')
            from sa_rule_head x
           where x.rule_id = I_rule_id
             and x.status not in ('DI', 'DE')
             and not exists (select distinct 'N'
                               from sa_rule_head z
                              where z.rule_id     = x.rule_id
                                and z.rule_rev_no > x.rule_rev_no
                                and (   (z.start_business_date <= x.start_business_date and
                                         z.end_business_date   >= x.start_business_date and
                                         z.end_business_date   is NOT NULL)
                                     or (z.start_business_date <= x.start_business_date and
                                         z.end_business_date   is NULL)
                                     or (z.start_business_date >= x.start_business_date and
                                         x.end_business_date   is NULL)
                                     or (z.start_business_date >= x.start_business_date and
                                         z.end_business_date   <= x.end_business_date and
                                         z.end_business_date   is NOT NULL)
                                     or (z.start_business_date <= x.end_business_date and
                                         z.end_business_date   >= x.end_business_date and
                                         z.end_business_date   is NOT NULL)
                                     or (z.start_business_date <= x.end_business_date and
                                         z.end_business_date   is NULL)
                                  ));
       ---
        cursor C_get_start_end_date is
           select to_char(start_business_date,'YYMMDD'),
                  to_char(end_business_date,'YYMMDD')
             from sa_rule_head
            where rule_id     = I_rule_id
              and rule_rev_no = I_rule_rev_no;
       ---
    BEGIN
       if I_rule_rev_no is NOT NULL then
          SQL_LIB.SET_MARK('OPEN','C_get_start_end_date','SA_RULE_HEAD',NULL);
          open  C_get_start_end_date;
          SQL_LIB.SET_MARK('FETCH','C_get_start_end_date','SA_RULE_HEAD',NULL);
          fetch C_get_start_end_date into d_start_date, d_end_date;
          SQL_LIB.SET_MARK('CLOSE','C_get_start_end_date','SA_RULE_HEAD',NULL);
          close C_get_start_end_date;
          if d_end_date is NULL then
             v_dates := '_' || d_start_date;
          else
             v_dates := '_' || d_start_date || '_' || d_end_date;
          end if;
          ---
          SQL_LIB.SET_MARK('OPEN',
                           'C_GET_FUNCTION',
                           'ALL_OBJECTS',
                           'Object Name: SA_R_' || upper(I_rule_id) || v_dates);
          OPEN  c_get_function;
          SQL_LIB.SET_MARK('FETCH',
                           'C_GET_FUNCTION',
                           'ALL_OBJECTS',
                           'Object Name: SA_R_' || upper(I_rule_id) || v_dates);
          FETCH c_get_function INTO v_name;
          SQL_LIB.SET_MARK('CLOSE',
                           'C_GET_FUNCTION',
                           'ALL_OBJECTS',
                           'Object Name: SA_R_' || upper(I_rule_id) || v_dates);
          CLOSE c_get_function;
          if v_name is NOT NULL then
             v_sql := 'drop function ' ||
                      sa_total_common.internal_schema_name ||
                      '.' || v_name;
             cursor_handle := DBMS_SQL.OPEN_CURSOR;
             DBMS_SQL.PARSE(cursor_handle, v_sql, DBMS_SQL.NATIVE);
             feedback := DBMS_SQL.EXECUTE(cursor_handle);
             DBMS_SQL.CLOSE_CURSOR(cursor_handle);
          end if;
       else --- I_rule_rev_no is NULL
          SQL_LIB.SET_MARK('OPEN',
                           'C_GET_RULE_INFO',
                           'SA_RULE_HEAD',
                           'Rule ID: '||I_rule_id);
          open  C_GET_RULE_INFO;
          LOOP
             SQL_LIB.SET_MARK('FETCH',
                              'C_GET_RULE_INFO',
                              'SA_RULE_HEAD',
                              'Rule ID: '||I_rule_id);
             fetch C_GET_RULE_INFO into d_start_date, d_end_date;
             EXIT when C_GET_RULE_INFO%NOTFOUND;
             BEGIN
                if d_end_date is NULL then
                   v_dates := '_' || d_start_date;
                else
                   v_dates := '_' || d_start_date || '_' || d_end_date;
                end if;
                ---
                SQL_LIB.SET_MARK('OPEN',
                                 'C_GET_FUNCTION',
                                 'ALL_OBJECTS',
                                 'Object Name: SA_R_' || upper(I_rule_id) || v_dates);
                OPEN  c_get_function;
                SQL_LIB.SET_MARK('FETCH',
                                 'C_GET_FUNCTION',
                                 'ALL_OBJECTS',
                                 'Object Name: SA_R_' || upper(I_rule_id) || v_dates);
                FETCH c_get_function INTO v_name;
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_GET_FUNCTION',
                                 'ALL_OBJECTS',
                                 'Object Name: SA_R_' || upper(I_rule_id) || v_dates);
                CLOSE c_get_function;
                if v_name is NOT NULL then
                   v_sql := 'drop function ' ||
                            sa_total_common.internal_schema_name ||
                            '.' || v_name;
                   cursor_handle := DBMS_SQL.OPEN_CURSOR;
                   DBMS_SQL.PARSE(cursor_handle, v_sql, DBMS_SQL.NATIVE);
                   feedback := DBMS_SQL.EXECUTE(cursor_handle);
                   DBMS_SQL.CLOSE_CURSOR(cursor_handle);
                end if;
             END;
          end LOOP;
          SQL_LIB.SET_MARK('CLOSE',
                           'C_GET_RULE_INFO',
                           'SA_RULE_HEAD',
                           'Rule ID: '||I_rule_id);
          close C_GET_RULE_INFO;
       end if;
       ---
       RETURN(0);
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                   SQLERRM,
                                                   'SA_AUDIT_RULES_SQL.DROP_AUDIT_RULE',
                                                   SQLCODE);
            RETURN(-1);
    END drop_audit_rule;

--------------------------------------------------------------------------

    FUNCTION INSERT_ERROR
   /****************************************************************\
   |                                                                |
   |   insert_error                                                 |
   |                                                                |
   |   This function will insert a single error into the sa_error   |
   |   table.                                                       |
   |                                                                |
   |    Called from:        Sales Audit form: sarule.fmb            |
   |                        Sales Audit dynamic rule functions      |
   |                                                                |
   |    Created:                                                    |
   |    September 20, 1999                                          |
   |                                                                |
   \****************************************************************/
    (
        o_error_message        IN OUT VARCHAR2,
        i_store_day_seq_no     IN     sa_store_day.store_day_seq_no%TYPE,
        i_balance_group_seq_no IN     sa_balance_group.bal_group_seq_no%TYPE,
        i_total_seq_no         IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
        i_tran_seq_no          IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
        i_key_value1           IN     sa_error.key_value_1%TYPE,
        i_key_value2           IN     sa_error.key_value_2%TYPE,
        i_error_code           IN     sa_error_codes.error_code%TYPE,
        i_rec_type             IN     sa_rule_head.rec_type%TYPE,
        i_orig_value           IN     sa_error.orig_value%type,
        i_rule_status          IN     sa_rule_head.status%TYPE,
        i_store                IN     sa_store_day.store%TYPE DEFAULT NULL,
        i_day                  IN     sa_store_day.day%TYPE DEFAULT NULL
    )
        RETURN BOOLEAN IS

        result  INT;
        ln_error_seq_no     sa_error.error_seq_no%TYPE := NULL;
        L_store             SA_STORE_DAY.STORE%TYPE := I_store;
        L_day               sa_store_day.day%TYPE := I_day;
        L_mla_store_ind     VARCHAR2(1) := 'N';

    BEGIN
       if L_store is NULL or L_day is NULL then
          if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
             return FALSE;
          end if;
       end if;
       ---
       if SA_SEQUENCE2_SQL.GET_ERROR_SEQ(o_error_message,
                                         ln_error_seq_no) = FALSE then
           return FALSE;
       end if;
       ---
       if STORE_DATA_SQL.CHECK_MLA_STORE(o_error_message,
                                         L_mla_store_ind,
                                         L_store) = FALSE then
          return FALSE;
       end if;
       if I_rule_status = 'A' then --- rule is approved
          SQL_LIB.SET_MARK('INSERT',
                           NULL,
                           'SA_ERROR',
                           NULL);
          INSERT INTO sa_error (store,
                                day,
                                error_seq_no,
                                store_day_seq_no,
                                bal_group_seq_no,
                                total_seq_no,
                                tran_seq_no,
                                error_code,
                                key_value_1,
                                key_value_2,
                                rec_type,
                                store_override_ind,
                                hq_override_ind,
                                update_id,
                                update_datetime,
                                orig_value)
          VALUES (L_store,
                  L_day,
                  ln_error_seq_no,
                  i_store_day_seq_no,
                  i_balance_group_seq_no,
                  i_total_seq_no,
                  i_tran_seq_no,
                  i_error_code,
                  i_key_value1,
                  i_key_value2,
                  i_rec_type,
                  'N',
                  'N',
                  USER,
                  SYSDATE,
                  i_orig_value);
            /* If in multi level audit*/
          if L_mla_store_ind ='Y' then   
             /* update the audit_status to 'S' if the store status is 'W' or 'F' */
             SQL_LIB.SET_MARK('UPDATE',
                              NULL,
                              'SA_STORE_DAY',
                              NULL);
             update sa_store_day
                set audit_status = 'S',
                    audit_changed_datetime = SYSDATE
              where store_day_seq_no = i_store_day_seq_no
                and store = L_store
                and day = L_day
                and store_status in ('W', 'F');
             /* update the audit_status to 'H' if the store status is 'C' */
             SQL_LIB.SET_MARK('UPDATE',
                              NULL,
                              'SA_STORE_DAY',
                              NULL);
             update sa_store_day
                set audit_status = 'H',
                    audit_changed_datetime = SYSDATE
              where store_day_seq_no = i_store_day_seq_no
                and store = L_store
                and day = L_day
                and store_status = 'C';
          else
             /* update the audit_status to 'H' */
             SQL_LIB.SET_MARK('UPDATE',
                              NULL,
                              'SA_STORE_DAY',
                              NULL);
             update sa_store_day
                set audit_status = 'H',
                    audit_changed_datetime = SYSDATE
              where store_day_seq_no = i_store_day_seq_no
                and store = L_store
                and day = L_day;
          end if;     
           ---
       else --- rule is NOT approved
          SQL_LIB.SET_MARK('INSERT',
                           NULL,
                           'SA_ERROR_WKSHT',
                           NULL);
          INSERT INTO sa_error_wksht (error_seq_no,
                                      store_day_seq_no,
                                      store,
                                      day,
                                      bal_group_seq_no,
                                      total_seq_no,
                                      tran_seq_no,
                                      error_code,
                                      key_value_1,
                                      key_value_2,
                                      rec_type,
                                      store_override_ind,
                                      hq_override_ind,
                                      update_id,
                                      update_datetime,
                                      orig_value)
          VALUES (ln_error_seq_no,
                  i_store_day_seq_no,
                  L_store,
                  L_day,
                  i_balance_group_seq_no,
                  i_total_seq_no,
                  i_tran_seq_no,
                  i_error_code,
                  i_key_value1,
                  i_key_value2,
                  i_rec_type,
                  'N',
                  'N',
                  USER,
                  SYSDATE,
                  i_orig_value);
       end if;
       ---
       RETURN true;
    EXCEPTION
        /* This will ensure that duplicate errors will not be entered
           and we won't fail if we try to enter one. */
       WHEN DUP_VAL_ON_INDEX THEN
           RETURN true;
       WHEN others THEN
           o_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  'SA_AUDIT_RULES_SQL.INSERT_ERROR',
                                                  SQLCODE);
           RETURN false;
    END INSERT_ERROR;

--------------------------------------------------------------------------

    FUNCTION PROCESS_AUDIT_RULES
    -- Recalculates rules for a given store_day_seq_no
    (
        o_error_message     IN OUT  VARCHAR2,
        i_store_day_seq_no  IN      sa_store_day.store_day_seq_no%TYPE,
        i_user_id           IN      sa_employee.user_id%TYPE,
        I_store             IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
        I_day               IN      SA_STORE_DAY.DAY%TYPE DEFAULT NULL
    )
           RETURN BOOLEAN
   /****************************************************************\
   |                                                                |
   |   process_audit_rules                                          |
   |                                                                |
   |   This function will process all audit rules for a passed      |
   |    store_day_seq_no.                                           |
   |                                                                |
   |    Arguments:          Description:                            |
   |    i_store_day_seq_no  Which store and date the rules should  |
   |                        be recalculated for.                    |
   |    o_error_message     Returned error message                  |
   |                                                                |
   |    Called from:        Sales Audit form: sa_rule.fmb           |
   |                        Sales Audit nightly batch: sarules.pc   |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
    IS
        l_emp_type          sa_employee.emp_type%TYPE := 'S';
        l_rule_id           sa_rule_head.rule_id%TYPE;
        l_start_date        VARCHAR2(6);
        l_end_date          VARCHAR2(6);
        l_status            SA_RULE_HEAD.STATUS%TYPE;
        l_object_name       ALL_OBJECTS.OBJECT_NAME%TYPE;
        l_function_name     VARCHAR2(61);
        cursor_handle       INTEGER;
        d_update_date       SA_RULE_HEAD.UPDATE_DATETIME%TYPE;
        l_execute_order     SA_RULE_HEAD.EXECUTE_ORDER%TYPE;
        feedback            INTEGER;
        v_select            sa_total_common.v_code_array :=
                                sa_total_common.empty_v_out;
        v_sql               DBMS_SQL.VARCHAR2s;
        v_sql_blank         DBMS_SQL.VARCHAR2s;
        li_result           INT := 0;
        li_count            INT := 0;
        l_store_status      SA_STORE_DAY.STORE_STATUS%TYPE;
        l_store             SA_STORE_DAY.STORE%TYPE := I_store;
        l_day               SA_STORE_DAY.DAY%TYPE := I_day;
        L_mla_store_ind     VARCHAR2(1) := 'N';
        
        CURSOR c_get_store_status IS
            SELECT store_status
              FROM sa_store_day
             WHERE store_day_seq_no = i_store_day_seq_no
               AND store = l_store
               AND day = l_day;

        CURSOR c_get_store_errors IS
            SELECT COUNT(se.error_code)
              FROM sa_error se,
                   sa_error_codes sec
             WHERE se.store_day_seq_no    = i_store_day_seq_no
               AND se.store = l_store
               AND se.day = l_day
               AND se.store_override_ind  = 'N'
               AND se.error_code          = sec.error_code;

        CURSOR c_get_hq_errors IS
            SELECT COUNT(se.error_code)
              FROM sa_error se,
                   sa_error_codes sec
             WHERE se.store_day_seq_no = i_store_day_seq_no
               AND se.store = l_store
               AND se.day = l_day
               AND se.hq_override_ind  = 'N'
               AND se.error_code       = sec.error_code;

        /* retrieve all total_ids that are subscribed to by
            the passed store_day */
        CURSOR c_get_rule_ids IS
          SELECT h1.rule_id, h1.update_datetime, h1.execute_order,
                 to_char(h1.start_business_date, 'YYMMDD'), to_char(h1.end_business_date, 'YYMMDD'),
                 h1.status
            FROM sa_rule_head h1
           WHERE h1.status not in ('DI', 'DE')
             AND not exists (select distinct 'N'
                               from sa_rule_head h2
                              where h2.rule_id     = h1.rule_id
                                and h2.rule_rev_no > h1.rule_rev_no
                                and (   (h2.start_business_date <= h1.start_business_date and
                                         h2.end_business_date   >= h1.start_business_date and
                                         h2.end_business_date   is NOT NULL)
                                     or (h2.start_business_date <= h1.start_business_date and
                                         h2.end_business_date   is NULL)
                                     or (h2.start_business_date >= h1.start_business_date and
                                         h1.end_business_date   is NULL)
                                     or (h2.start_business_date >= h1.start_business_date and
                                         h2.end_business_date   <= h1.end_business_date and
                                         h2.end_business_date   is NOT NULL)
                                     or (h2.start_business_date <= h1.end_business_date and
                                         h2.end_business_date   >= h1.end_business_date and
                                         h2.end_business_date   is NOT NULL)
                                     or (h2.start_business_date <= h1.end_business_date and
                                         h2.end_business_date   is NULL)
                                  ))
             AND h1.rule_id IN
                (SELECT rule_id
                   FROM sa_rule_loc_trait
                  WHERE loc_trait IN
                    (SELECT loc_trait
                       FROM loc_traits_matrix
                      WHERE  STORE = l_store))
            ORDER BY to_number(h1.execute_order), h1.update_datetime;

        /* retrieve the dynamic total functions to call */
        CURSOR c_get_function_name IS
            SELECT owner || '.' || object_name
            FROM all_objects
            WHERE object_name = l_object_name
            AND object_type = 'FUNCTION'
            AND status        = 'VALID'
            AND upper(owner) = upper(sa_total_common.internal_schema_name)
            AND NOT EXISTS (
                   SELECT 'x'
                     FROM all_jobs
                    WHERE instr(UPPER(what), UPPER(object_name)) <>0);

    BEGIN
       if l_store is NULL or l_day is NULL then
          if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
             return FALSE;
          end if;
       end if;
       if STORE_DATA_SQL.CHECK_MLA_STORE(o_error_message,
                                         L_mla_store_ind,
                                         L_store) = FALSE then
          return FALSE;
       end if;
       
       /*delete previous non-approved errors*/
       SQL_LIB.SET_MARK('DELETE',
                         NULL,
                         'SA_ERROR_WKSHT',
                         NULL);
        DELETE sa_error_wksht e1
         WHERE e1.store_day_seq_no = I_store_day_seq_no
           AND e1.store = l_store
           AND e1.day = l_day
           AND e1.error_seq_no not in ( select e2.error_seq_no
                                          from sa_error_wksht e2
                                         where e2.error_code = 'INV_TOTAL_DEF'
                                           and e2.rec_type   = 'TOTERR' );
        ---
        /* keep the errors for later update */
        SQL_LIB.SET_MARK('INSERT',
                         NULL,
                         'SA_ERROR_TEMP',
                         NULL);
        INSERT INTO sa_error_temp (error_seq_no, store_day_seq_no, store, day, bal_group_seq_no, total_seq_no, tran_seq_no, error_code, key_value_1, key_value_2, rec_type, store_override_ind, hq_override_ind, update_id, update_datetime, orig_value)
           SELECT error_seq_no, store_day_seq_no, store, day, bal_group_seq_no, total_seq_no, tran_seq_no, error_code, key_value_1, key_value_2, rec_type, store_override_ind, hq_override_ind, update_id, update_datetime, orig_value from sa_error
            WHERE store_day_seq_no = I_store_day_seq_no
              AND store = l_store
              AND day = l_day
              AND (   store_override_ind = 'Y'
                   OR hq_override_ind = 'Y')
              AND error_code IN
                  (SELECT error_code
                     FROM sa_error_codes
                    WHERE required_ind = 'N');
        ---
        /* delete previous unrequired errors */
        SQL_LIB.SET_MARK('DELETE',
                         NULL,
                         'SA_ERROR',
                         NULL);
        DELETE sa_error e1
         WHERE e1.store_day_seq_no = I_store_day_seq_no
           AND e1.store = l_store
           AND e1.day = l_day
           AND (   e1.error_code IN ( SELECT e2.error_code
                                        FROM sa_error_codes e2
                                    WHERE e2.required_ind = 'N')
                or (e1.error_code = 'INV_RULE_DEF' and e1.rec_type = 'RULERR')
               );
        ---
        /* Reset the error indicator on sa_total to 'N' (No) */
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_TOTAL',
                         NULL);
        update sa_total
           set error_ind = 'N'
         where store_day_seq_no = i_store_day_seq_no
           and store = l_store
           and day = l_day;
        ---

        /* Reset the error indicator on the transaction tables to 'N'(No) for transactions
           that do not have system required errors associated with them. */
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_TRAN_HEAD',
                         NULL);
        UPDATE sa_tran_head t1
           SET t1.error_ind = 'N'
         WHERE t1.store_day_seq_no = i_store_day_seq_no
           AND t1.store = l_store
           AND t1.day = l_day
           AND t1.error_ind = 'Y'
           AND NOT EXISTS (SELECT 1
                             FROM sa_error se,
                                  sa_error_codes sec
                            WHERE se.tran_seq_no = t1.tran_seq_no
                              AND se.store = t1.store
                              AND se.day = t1.day
                              AND sec.error_code = se.error_code
                              AND se.rec_type IN ('THEAD',  'THBALC', 'THPOSI',
                                                  'THREAC', 'THSUBT', 'THTRAT',
                                                  'THDATE', 'THTRAN', 'THOREG',
                                                  'THOTRT', 'THOTRN', 'THVALU'));
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_TRAN_ITEM',
                         NULL);
        UPDATE /*+ index(t1) */ sa_tran_item t1
           SET t1.error_ind = 'N'
         WHERE t1.tran_seq_no IN (SELECT tran_seq_no
                                    FROM sa_tran_head
                                   WHERE store_day_seq_no = i_store_day_seq_no
                                     AND store = l_store
                                     AND day = l_day)
           AND t1.store = l_store
           AND t1.day = l_day
           AND t1.error_ind = 'Y'
           AND NOT EXISTS (SELECT 1
                             FROM sa_error se,
                                  sa_error_codes sec
                            WHERE se.tran_seq_no = t1.tran_seq_no
                              AND se.store = t1.store
                              AND se.day = t1.day
                              AND se.key_value_1 = t1.item_seq_no
                              AND sec.error_code = se.error_code
                              AND se.rec_type = 'TITEM');
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_TRAN_DISC',
                         NULL);
        UPDATE /*+ index(t1) */ sa_tran_disc t1
           SET t1.error_ind = 'N'
         WHERE t1.tran_seq_no IN (SELECT tran_seq_no
                                    FROM sa_tran_head
                                   WHERE store_day_seq_no = i_store_day_seq_no
                                     AND store = l_store
                                     AND day = l_day)
           AND t1.store = l_store
           AND t1.day = l_day
           AND t1.error_ind = 'Y'
           AND NOT EXISTS (SELECT 1
                             FROM sa_error se,
                                  sa_error_codes sec
                            WHERE se.tran_seq_no = t1.tran_seq_no
                              AND se.store = t1.store
                              AND se.day = t1.day
                              AND se.key_value_1 = t1.item_seq_no
                              AND se.key_value_2 = t1.discount_seq_no
                              AND sec.error_code = se.error_code
                              AND se.rec_type = 'IDISC');
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_TRAN_TENDER',
                         NULL);
        UPDATE sa_tran_tender t1
           SET t1.error_ind = 'N'
         WHERE t1.tran_seq_no IN (SELECT tran_seq_no
                                    FROM sa_tran_head
                                   WHERE store_day_seq_no = i_store_day_seq_no
                                     AND store = l_store
                                     AND day = l_day)
           AND t1.store = l_store
           AND t1.day = l_day
           AND t1.error_ind = 'Y'
           AND NOT EXISTS (SELECT 1
                             FROM sa_error se,
                                  sa_error_codes sec
                            WHERE se.tran_seq_no = t1.tran_seq_no
                              AND se.store = t1.store
                              AND se.day = t1.day
                              AND se.key_value_1 = t1.tender_seq_no
                              AND sec.error_code = se.error_code
                              AND se.rec_type = 'TTEND');
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_TRAN_TAX',
                         NULL);
        UPDATE sa_tran_tax t1
           SET t1.error_ind = 'N'
         WHERE t1.tran_seq_no IN (SELECT tran_seq_no
                                    FROM sa_tran_head
                                   WHERE store_day_seq_no = i_store_day_seq_no
                                     AND store = l_store
                                     AND day = l_day)
           AND t1.store = l_store
           AND t1.day = l_day
           AND t1.error_ind = 'Y'
           AND NOT EXISTS (SELECT 1
                             FROM sa_error se,
                                  sa_error_codes sec
                            WHERE se.tran_seq_no = t1.tran_seq_no
                              AND se.store = t1.store
                              AND se.day = t1.day
                              AND se.key_value_1 = t1.tax_seq_no
                              AND sec.error_code = se.error_code
                              AND se.rec_type = 'TTAX');

        cursor_handle := DBMS_SQL.OPEN_CURSOR;
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_RULE_IDS',
                         'SA_RULE_HEAD',
                         NULL);
        OPEN c_get_rule_ids;
        LOOP
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_RULE_IDS',
                             'SA_RULE_HEAD',
                             NULL);
            FETCH c_get_rule_ids INTO l_rule_id, d_update_date, l_execute_order,
                                      l_start_date, l_end_date, l_status;
            EXIT WHEN c_get_rule_ids%NOTFOUND;
            BEGIN
                if l_end_date is NOT NULL then
                   l_object_name := 'SA_R_' || l_rule_id || '_' || l_start_date || '_' || l_end_date;
                else
                   l_object_name := 'SA_R_' || l_rule_id || '_' || l_start_date;
                end if;
                SQL_LIB.SET_MARK('OPEN',
                                 'C_GET_FUNCTION_NAME',
                                 'ALL_OBJECTS',
                                 'Object Name: '||l_object_name);
                OPEN c_get_function_name;
                LOOP
                    SQL_LIB.SET_MARK('FETCH',
                                     'C_GET_FUNCTION_NAME',
                                     'ALL_OBJECTS',
                                     'Object Name: '||l_object_name);
                    FETCH c_get_function_name INTO l_function_name;
                    EXIT WHEN c_get_function_name%NOTFOUND;
                    BEGIN
                        v_select := sa_total_common.empty_v_out;
                        v_sql := v_sql_blank;
                        feedback := 0;
                        li_count := 1;
                        v_select(v_select.count+1) := 'DECLARE';
                        v_select(v_select.count+1) := 'os_error_message VARCHAR2(256);';
                        v_select(v_select.count+1) := 'BEGIN';
                        v_select(v_select.count+1) := ':li_result := ' ||
                            l_function_name || '(:i_store_day_seq_no, os_error_message);';
                        v_select(v_select.count+1) := 'END;';
                        FOR n_count IN 1..v_select.count LOOP
                            v_sql(v_sql.count+1) := v_select(n_count);
                        END LOOP;
                        /* Use dynamic SQl to execute functions */
                        DBMS_SQL.PARSE (cursor_handle, v_sql, v_sql.FIRST,
                                        v_sql.LAST, TRUE, DBMS_SQL.NATIVE);
                        DBMS_SQL.BIND_VARIABLE(cursor_handle, 'li_result', li_result);
                        DBMS_SQL.BIND_VARIABLE(cursor_handle, 'i_store_day_seq_no', i_store_day_seq_no);
                        feedback := DBMS_SQL.EXECUTE(cursor_handle);
                        DBMS_SQL.VARIABLE_VALUE(cursor_handle, 'li_result', li_result);
                        IF li_result < 0 THEN
                           /* could not execute the function */
                           if l_status NOT in ('W', 'A') then
                              --- Status is not approved, so insert the error into the worksheet tables.
                              l_status := 'W';
                           end if;
                           --- Insert an error indicating that the total function did not execute successfully.
                           if SA_AUDIT_RULES_SQL.INSERT_ERROR(O_error_message,
                                                              I_store_day_seq_no,
                                                              NULL,            --- bal_group_seq_no
                                                              NULL,            --- total_seq_no
                                                              NULL,            --- tran_seq_no
                                                              NULL,            --- key_value_1
                                                              NULL,            --- key_value_2
                                                              'INV_RULE_DEF',  --- error_code
                                                              'RULERR',        --- rec_type
                                                              l_rule_id,       --- orig_value
                                                              l_status,
                                                              l_store,
                                                              l_day) = FALSE then
                              return FALSE;
                           end if;
                        ELSIF li_result = 1 THEN
                           -- If rule_stop_ind is 'Y' for this rule, the function returns 1
                           -- and therefore processing must stop.
                           EXIT;
                        END IF;
                        IF feedback <> 1 THEN
                            RETURN(false);
                        END IF;
                    END;
                END LOOP;

                IF li_result = 1 THEN
                   -- If rule_stop_ind is 'Y' for this rule, the function returns 1
                   -- and therefore processing must stop.
                   EXIT;
                END IF;

                IF li_count = 0 THEN
                    o_error_message := sql_lib.create_msg('SAAUDIT_NOFOUND',
                                                          l_function_name,
                                                          NULL,
                                                          NULL);
                END IF;
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_GET_FUNCTION_NAME',
                                 'ALL_OBJECTS',
                                 'Object Name: '||l_object_name);
                CLOSE c_get_function_name;
                li_count := 0;
            END;
        END LOOP;
        IF DBMS_SQL.IS_OPEN(cursor_handle) THEN
            DBMS_SQL.CLOSE_CURSOR(cursor_handle);
        END IF;
        ---
        /* Check if dimensions for the selling uom have been defined, if so then delete the error from sa_error */
        if CHECK_UOM_ERRORS(O_error_message,
                            I_store_day_seq_no,
                            L_store,
                            L_day) = FALSE then
           return FALSE;
        end if;
        ---
        /* update store_override_ind and hq_override_ind according to the sa_error_temp table */
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_ERROR',
                         NULL);
        UPDATE sa_error se
           SET store_override_ind = 'Y'
         WHERE store_day_seq_no = I_store_day_seq_no
           AND store = l_store
           AND day = l_day
           AND EXISTS (SELECT 1
                         FROM sa_error_temp se_temp
                        WHERE se_temp.store_day_seq_no = se.store_day_seq_no
                          AND se_temp.store = se.store
                          AND se_temp.day = se.day
                          AND (se.bal_group_seq_no is NULL OR (se_temp.bal_group_seq_no = se.bal_group_seq_no
                                                               AND se.bal_group_seq_no is not NULL))
                          AND (se.total_seq_no is NULL OR (se_temp.total_seq_no = se.total_seq_no
                                                           AND se.total_seq_no is not NULL))
                          AND (se.tran_seq_no is NULL OR (se_temp.tran_seq_no = se.tran_seq_no
                                                          AND se.tran_seq_no is not NULL))
                          AND se_temp.error_code = se.error_code
                          AND (se.key_value_1 is NULL OR (se_temp.key_value_1 = se.key_value_1
                                                          AND se.key_value_1 is not NULL))
                          AND (se.key_value_2 is NULL OR (se_temp.key_value_2 = se.key_value_2
                                                          AND se.key_value_2 is not NULL))
                          AND se_temp.rec_type = se.rec_type
                          AND (se.orig_value is NULL OR (se_temp.orig_value = se.orig_value
                                                         AND se.orig_value is not NULL))
                          AND se_temp.store_override_ind = 'Y'
                          AND se_temp.hq_override_ind = 'N');
        ---
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_ERROR',
                         NULL);
        UPDATE sa_error se
           SET hq_override_ind = 'Y'
         WHERE store_day_seq_no = I_store_day_seq_no
           AND store = l_store
           AND day = l_day
           AND EXISTS (SELECT 1
                         FROM sa_error_temp se_temp
                        WHERE se_temp.store_day_seq_no = se.store_day_seq_no
                          AND se_temp.store = se.store
                          AND se_temp.day = se.day
                          AND (se.bal_group_seq_no is NULL OR (se_temp.bal_group_seq_no = se.bal_group_seq_no
                                                               AND se.bal_group_seq_no is not NULL))
                          AND (se.total_seq_no is NULL OR (se_temp.total_seq_no = se.total_seq_no
                                                           AND se.total_seq_no is not NULL))
                          AND (se.tran_seq_no is NULL OR (se_temp.tran_seq_no = se.tran_seq_no
                                                          AND se.tran_seq_no is not NULL))
                          AND se_temp.error_code = se.error_code
                          AND (se.key_value_1 is NULL OR (se_temp.key_value_1 = se.key_value_1
                                                          AND se.key_value_1 is not NULL))
                          AND (se.key_value_2 is NULL OR (se_temp.key_value_2 = se.key_value_2
                                                          AND se.key_value_2 is not NULL))
                          AND se_temp.rec_type = se.rec_type
                          AND (se.orig_value is NULL OR (se_temp.orig_value = se.orig_value
                                                         AND se.orig_value is not NULL))
                          AND se_temp.hq_override_ind = 'Y'
                          AND se_temp.store_override_ind = 'N');
        ---
        SQL_LIB.SET_MARK('UPDATE',
                         NULL,
                         'SA_ERROR',
                         NULL);
        UPDATE sa_error se
           SET store_override_ind = 'Y',
               hq_override_ind    = 'Y'
         WHERE store_day_seq_no = I_store_day_seq_no
           AND store = l_store
           AND day = l_day
           AND EXISTS (SELECT 1
                         FROM sa_error_temp se_temp
                        WHERE se_temp.store_day_seq_no = se.store_day_seq_no
                          AND se_temp.store = se.store
                          AND se_temp.day = se.day
                          AND (se.bal_group_seq_no is NULL OR (se_temp.bal_group_seq_no = se.bal_group_seq_no
                                                               AND se.bal_group_seq_no is not NULL))
                          AND (se.total_seq_no is NULL OR (se_temp.total_seq_no = se.total_seq_no
                                                           AND se.total_seq_no is not NULL))
                          AND (se.tran_seq_no is NULL OR (se_temp.tran_seq_no = se.tran_seq_no
                                                          AND se.tran_seq_no is not NULL))
                          AND se_temp.error_code = se.error_code
                          AND (se.key_value_1 is NULL OR (se_temp.key_value_1 = se.key_value_1
                                                          AND se.key_value_1 is not NULL))
                          AND (se.key_value_2 is NULL OR (se_temp.key_value_2 = se.key_value_2
                                                          AND se.key_value_2 is not NULL))
                          AND se_temp.rec_type = se.rec_type
                          AND (se.orig_value is NULL OR (se_temp.orig_value = se.orig_value
                                                         AND se.orig_value is not NULL))
                          AND se_temp.store_override_ind = 'Y'
                          AND se_temp.hq_override_ind = 'Y');
        ---
        /* delete the temporary records */
        SQL_LIB.SET_MARK('DELETE',
                         NULL,
                         'SA_ERROR_TEMP',
                         NULL);
        DELETE sa_error_temp
         WHERE store_day_seq_no = I_store_day_seq_no
           AND store = l_store
           AND day = l_day;
        ---
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_STORE_STATUS',
                         'SA_STORE_DAY',
                         'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
        OPEN c_get_store_status;
        SQL_LIB.SET_MARK('FETCH',
                         'C_GET_STORE_STATUS',
                         'SA_STORE_DAY',
                         'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
        FETCH c_get_store_status INTO l_store_status;
        ---
        /* update the audit status for the processed store_day */
        li_count := 0;
        ---
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_STORE_ERRORS',
                         'SA_ERROR, SA_ERROR_CODES',
                         'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
        OPEN c_get_store_errors;
        SQL_LIB.SET_MARK('FETCH',
                         'C_GET_STORE_ERRORS',
                         'SA_ERROR, SA_ERROR_CODES',
                         'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
        FETCH c_get_store_errors INTO li_count;
        IF (l_store_status in ('W', 'F')) AND (li_count > 0) AND L_mla_store_ind = 'Y' THEN
           SQL_LIB.SET_MARK('UPDATE',
                            NULL,
                            'SA_STORE_DAY',
                            'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
           UPDATE sa_store_day
              SET audit_status = 'S',
                  audit_changed_datetime = SYSDATE
            WHERE store_day_seq_no = I_store_day_seq_no
              AND store = l_store
              AND day = l_day;
        ELSE
           li_count := 0;
           SQL_LIB.SET_MARK('OPEN',
                            'C_GET_HQ_ERRORS',
                            'SA_ERROR, SA_ERROR_CODES',
                            'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
           OPEN c_get_hq_errors;
           SQL_LIB.SET_MARK('FETCH',
                            'C_GET_HQ_ERRORS',
                            'SA_ERROR, SA_ERROR_CODES',
                            'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
           FETCH c_get_hq_errors INTO li_count;
           IF li_count > 0 THEN
           SQL_LIB.SET_MARK('UPDATE',
                            NULL,
                            'SA_STORE_DAY',
                            'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
              UPDATE sa_store_day
                 SET audit_status = 'H',
                     audit_changed_datetime = SYSDATE
               WHERE store_day_seq_no = I_store_day_seq_no
                 AND store = l_store
                 AND day = l_day;
           ELSE
              SQL_LIB.SET_MARK('UPDATE',
                               NULL,
                               'SA_STORE_DAY',
                               'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
              UPDATE sa_store_day
                 SET audit_status = 'A',
                     audit_changed_datetime = SYSDATE
               WHERE store_day_seq_no = I_store_day_seq_no
                 AND store = l_store
                 AND day = l_day;
           END IF;
           CLOSE c_get_hq_errors;
        END IF;
        CLOSE c_get_store_errors;
        ---
        RETURN(TRUE);
        ---
EXCEPTION
    WHEN others THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                   SQLERRM,
                                                   'SA_AUDIT_RULES_SQL.PROCESS_AUDIT_RULES',
                                                   SQLCODE);
        RETURN(false);
END PROCESS_AUDIT_RULES;
------------------------------------------------------------------
FUNCTION DELETE_AUDIT_RULE(O_error_message     IN OUT  VARCHAR2,
                           I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE)
                           RETURN BOOLEAN IS
   L_program         VARCHAR2(50)                   := 'SA_AUDIT_RULES_SQL.DELETE_AUDIT_RULE';
   v_vr_id           sa_vr_head.vr_id%TYPE          := NULL;

   cursor c_vr_id is
      select vr_id
        from sa_rule_comp
       where rule_id = I_rule_id;

BEGIN
   if I_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_VR_ID',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   open c_vr_id;
   SQL_LIB.SET_MARK('FETCH',
                    'C_VR_ID',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   fetch c_vr_id into v_vr_id;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_VR_ID',
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   close c_vr_id;

   if v_vr_id is NOT NULL then
      if delete_vr(O_error_message,
                   v_vr_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_RULE_COMP_RESTRICTIONS',
                    'Rule ID: '||I_rule_id);
   delete from sa_rule_comp_restrictions
   where rule_id = I_rule_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_RULE_LOC_TRAIT',
                    'Rule ID: '||I_rule_id);
   delete from sa_rule_loc_trait
   where rule_id = I_rule_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_RULE_ERRORS',
                    'Rule ID: '||I_rule_id);
   delete from sa_rule_errors
   where rule_id = I_rule_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_RULE_COMP',
                    'Rule ID: '||I_rule_id);
   delete from sa_rule_comp
   where rule_id = I_rule_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_RULE_HEAD_TL',
                    'Rule ID: '||I_rule_id);
   delete from sa_rule_head_tl
   where rule_id = I_rule_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   delete from sa_rule_head
   where rule_id = I_rule_id;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_AUDIT_RULE;
------------------------------------------------------------------
FUNCTION APPROVED_RULE_EXISTS(O_error_message     IN OUT  VARCHAR2,
                              O_approved_exists   IN OUT  VARCHAR2,
                              I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(50)   := 'SA_AUDIT_RULES_SQL.APPROVED_RULE_EXISTS';
   L_exists       VARCHAR2(1)    := 'N';
   cursor C_CHECK_EXISTS is
      select 'Y'
        from sa_rule_head
       where rule_id = I_rule_id
         and status = 'A'; --- Approved status
BEGIN
   if I_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_EXISTS',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   open  C_CHECK_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_EXISTS',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   fetch C_CHECK_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_EXISTS',
                    'SA_RULE_HEAD',
                    'Rule ID: '||I_rule_id);
   close C_CHECK_EXISTS;
   ---
   if L_exists = 'Y' then
      O_approved_exists := 'Y';
   else
      O_approved_exists := 'N';
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END APPROVED_RULE_EXISTS;
------------------------------------------------------------------
FUNCTION DELETE_VR(O_error_message     IN OUT  VARCHAR2,
                   I_vr_id             IN      SA_VR_HEAD.VR_ID%TYPE)
   RETURN BOOLEAN IS
   L_program      VARCHAR2(50)   := 'SA_AUDIT_RULES_SQL.DELETE_VR';

BEGIN
   if I_vr_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_RULE_COMP_RESTRICTIONS',
                    'VR ID: '||I_vr_id);
   delete from sa_rule_comp_restrictions
   where (rule_id, rule_rev_no, rule_comp_seq_no) in
                  (select rule_id, rule_rev_no, rule_comp_seq_no
                     from sa_rule_comp
                    where vr_id = I_vr_id);
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_RULE_COMP',
                    'VR ID: '||I_vr_id);
   delete from sa_rule_comp
   where vr_id = I_vr_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_VR_LINKS',
                    'VR ID: '||I_vr_id);
   delete from sa_vr_links
   where vr_id = I_vr_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_VR_PARMS',
                    'VR ID: '||I_vr_id);
   delete from sa_vr_parms
   where vr_id = I_vr_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_VR_REALM',
                    'VR ID: '||I_vr_id);
   delete from sa_vr_realm
   where vr_id = I_vr_id;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SA_VR_HEAD',
                    'VR ID: '||I_vr_id);
   delete from sa_vr_head
   where vr_id = I_vr_id;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_VR;
------------------------------------------------------------------
FUNCTION MAX_RULE_REV_NO(O_error_message     IN OUT  VARCHAR2,
                         O_max_rule_rev_no   IN OUT  SA_RULE_HEAD.RULE_REV_NO%TYPE,
                         I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE)
   RETURN BOOLEAN IS
   L_program      VARCHAR2(50)   := 'SA_AUDIT_RULES_SQL.MAX_RULE_REV_NO';
   ---
   cursor C_GET_MAX_REV is
      select max(rule_rev_no)
        from sa_rule_head
       where rule_id = I_rule_id;
BEGIN
   if I_rule_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_MAX_REV','SA_AUDIT_RULES_SQL',NULL);
   open  C_GET_MAX_REV;
   SQL_LIB.SET_MARK('FETCH','C_GET_MAX_REV','SA_AUDIT_RULES_SQL',NULL);
   fetch C_GET_MAX_REV into O_max_rule_rev_no;
   SQL_LIB.SET_MARK('CLOSE','C_GET_MAX_REV','SA_AUDIT_RULES_SQL',NULL);
   close C_GET_MAX_REV;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MAX_RULE_REV_NO;
------------------------------------------------------------------
FUNCTION CHECK_UOM_ERRORS(O_error_message     IN OUT  VARCHAR2,
                          I_store_day_seq_no  IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                          I_store             IN      SA_STORE_DAY.STORE%TYPE,
                          I_day               IN      SA_STORE_DAY.DAY%TYPE)

   RETURN BOOLEAN IS
   L_program        VARCHAR2(50)   := 'SA_AUDIT_RULES_SQL.CHECK_ERRORS';
   L_selling_uom    SA_TRAN_ITEM.SELLING_UOM%TYPE;
   L_standard_uom   SA_TRAN_ITEM.STANDARD_UOM%TYPE;
   L_tran_seq_no    SA_TRAN_ITEM.TRAN_SEQ_NO%TYPE;
   L_item_seq_no    SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE;
   L_item           SA_TRAN_ITEM.ITEM%TYPE;
   L_value          SA_TRAN_ITEM.UNIT_RETAIL%TYPE;
   L_store          SA_TRAN_HEAD.STORE%TYPE        := I_store;
   L_day            SA_TRAN_HEAD.DAY%TYPE          := I_day;
   ---
   cursor C_CHECK_ERROR is
      select e.tran_seq_no, e.key_value_1, ti.selling_uom, ti.standard_uom, ti.item
        from sa_error e, sa_tran_item ti, sa_tran_head th
       where e.store_day_seq_no = I_store_day_seq_no
         and e.store            = L_store
         and e.day              = L_day
         and e.error_code       = 'INVLD_SELLING_UOM'
         and th.tran_seq_no     = ti.tran_seq_no
         and e.tran_seq_no      = ti.tran_seq_no
         and e.key_value_1      = ti.item_seq_no;

BEGIN
   if I_store_day_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        I_store_day_seq_no,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   --- 
   FOR rec_in IN C_CHECK_ERROR LOOP
      L_tran_seq_no := rec_in.tran_seq_no;
      L_item_seq_no := rec_in.key_value_1;
      L_selling_uom := rec_in.selling_uom;
      L_standard_uom := rec_in.standard_uom;
      L_item := rec_in.item;
      ---
      if UOM_SQL.CONVERT(O_error_message,
                         L_value,
                         L_standard_uom,
                         1,
                         L_selling_uom,
                         L_item,
                         NULL,
                         NULL) = FALSE then
         RETURN FALSE;
      end if;
      ---
      if L_value != 0 then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       L_tran_seq_no,
                                       'INVLD_SELLING_UOM',
                                       L_item_seq_no,
                                       NULL,
                                       'TITEM',
                                               L_store,
                                               L_day) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_UOM_ERRORS;
------------------------------------------------------------------


END SA_AUDIT_RULES_SQL;
/
