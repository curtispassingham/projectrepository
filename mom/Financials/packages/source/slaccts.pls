CREATE OR REPLACE PACKAGE STKLEDGR_ACCTING_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------
---     Name: retail_method_calc
---  Purpose: Performs calculations for retail method of accounting
---           such as closing stock, book stock, gross margin, etc.
----------------------------------------------------------------------
FUNCTION RETAIL_METHOD_CALC(IO_stk_retail                    IN OUT   NUMBER, --opening stock in, closing out
                            IO_stk_cost                      IN OUT   NUMBER, --opening stock in, closing out
                            I_purch_retail                   IN       NUMBER,
                            I_purch_cost                     IN       NUMBER,
                            I_rtv_retail                     IN       NUMBER,
                            I_rtv_cost                       IN       NUMBER,
                            I_freight_cost                   IN       NUMBER,
                            I_markup_retail                  IN       NUMBER,
                            I_markup_can_retail              IN       NUMBER,
                            I_net_sales_retail               IN       NUMBER,
                            I_net_sales_retail_ex_vat        IN       NUMBER,
                            I_net_sales_cost                 IN       NUMBER,
                            I_returns_retail                 IN       NUMBER,
                            I_returns_cost                   IN       NUMBER,
                            I_perm_markdown_retail           IN       NUMBER,
                            I_promo_markdown_retail          IN       NUMBER,
                            I_weight_variance_retail         IN       NUMBER,
                            I_clear_markdown_retail          IN       NUMBER,
                            I_markdown_can_retail            IN       NUMBER,
                            I_up_chrg_amt_profit             IN       NUMBER,
                            I_up_chrg_amt_exp                IN       NUMBER,
                            I_tsf_in_retail                  IN       NUMBER,
                            I_tsf_in_cost                    IN       NUMBER,
                            I_tsf_in_book_retail             IN       NUMBER,
                            I_tsf_in_book_cost               IN       NUMBER,
                            I_tsf_out_retail                 IN       NUMBER,
                            I_tsf_out_cost                   IN       NUMBER,
                            I_tsf_out_book_retail            IN       NUMBER,
                            I_tsf_out_book_cost              IN       NUMBER,
                            I_reclass_in_retail              IN       NUMBER,
                            I_reclass_in_cost                IN       NUMBER,
                            I_reclass_out_retail             IN       NUMBER,
                            I_reclass_out_cost               IN       NUMBER,
                            I_stock_adj_retail               IN       NUMBER,
                            I_stock_adj_cost                 IN       NUMBER,
                            I_freight_claims_retail          IN       NUMBER,
                            I_stock_adj_cogs_retail          IN       NUMBER,
                            I_bud_shrinkage_pct              IN       NUMBER,
                            I_empl_disc_retail               IN       NUMBER,
                            IO_htd_gafs_retail               IN OUT   NUMBER,
                            IO_htd_gafs_cost                 IN OUT   NUMBER,
                            I_workroom_amt                   IN       NUMBER,
                            I_cash_discount_amt              IN       NUMBER,
                            I_stocktake_adj_retail           IN       NUMBER,
                            I_stocktake_mtd_shrink_amt       IN       NUMBER,
                            I_stocktake_mtd_sales_amt        IN       NUMBER,
                            I_intercompany_in_retail         IN       NUMBER,
                            I_intercompany_in_cost           IN       NUMBER,
                            I_intercompany_out_retail        IN       NUMBER,
                            I_intercompany_out_cost          IN       NUMBER,
                            I_intercompany_markup            IN       NUMBER,
                            I_intercompany_markdown          IN       NUMBER,
                            I_wo_activity_upd_inv            IN       NUMBER,
                            I_wo_activity_post_fin           IN       NUMBER,
                            I_deal_income_sales              IN       NUMBER,
                            I_deal_income_purch              IN       NUMBER,
                            I_restocking_fee                 IN       NUMBER,
                            IO_retail_cost_variance_amount   IN OUT   NUMBER,
                            IO_margin_cost_variance_amount   IN OUT   NUMBER,
                            O_intercompany_margin            IN OUT   NUMBER,
                            O_gross_margin_amt               IN OUT   NUMBER,
                            IO_cum_markon_pct                IN OUT   NUMBER,
                            I_recalc_cum_markon_pct          IN       BOOLEAN,
                            O_shrinkage_retail               IN OUT   NUMBER,
                            I_franchise_sales_retail         IN       NUMBER,
                            I_franchise_returns_retail       IN       NUMBER,
                            I_franchise_returns_cost         IN       NUMBER,
                            I_franchise_markup_retail        IN       NUMBER,
                            I_franchise_markdown_retail      IN       NUMBER,
                            I_freight_claims_cost            IN       NUMBER,
                            O_error_message                  IN OUT   VARCHAR2)


   RETURN BOOLEAN;

----------------------------------------------------------------------
---     Name: cost_method_calc
---  Purpose: Performs calculations for cost method of accounting
---           such as closing stock, book stock, gross margin, etc.
----------------------------------------------------------------------
FUNCTION COST_METHOD_CALC(IO_stk_cost                      IN OUT   NUMBER,
                          I_purch_cost                     IN       NUMBER,
                          I_rtv_cost                       IN       NUMBER,
                          I_net_sales_cost                 IN       NUMBER,
                          I_net_sales_retail               IN       NUMBER,
                          I_up_chrg_amt_profit             IN       NUMBER,
                          I_up_chrg_amt_exp                IN       NUMBER,
                          I_tsf_in_cost                    IN       NUMBER,
                          I_tsf_in_book_cost               IN       NUMBER,
                          I_tsf_out_cost                   IN       NUMBER,
                          I_tsf_out_book_cost              IN       NUMBER,
                          I_reclass_in_cost                IN       NUMBER,
                          I_reclass_out_cost               IN       NUMBER,
                          I_stock_adj_cost                 IN       NUMBER,
                          I_freight_claims_cost            IN       NUMBER,
                          I_stock_adj_cogs_cost            IN       NUMBER,
                          I_bud_shrinkage_pct              IN       NUMBER,
                          I_stocktake_adj_cost             IN       NUMBER,
                          I_stocktake_mtd_shrink_amt       IN       NUMBER,
                          I_stocktake_mtd_sales_amt        IN       NUMBER,
                          I_cost_variance_amt              IN       NUMBER,
                          I_intercompany_in_retail         IN       NUMBER,
                          I_intercompany_in_cost           IN       NUMBER,
                          I_intercompany_out_retail        IN       NUMBER,
                          I_intercompany_out_cost          IN       NUMBER,
                          I_intercompany_markup            IN       NUMBER,
                          I_intercompany_markdown          IN       NUMBER,
                          I_wo_activity_upd_inv            IN       NUMBER,
                          I_wo_activity_post_fin           IN       NUMBER,
                          I_deal_income_sales              IN       NUMBER,
                          I_deal_income_purch              IN       NUMBER,
                          I_restocking_fee                 IN       NUMBER,
                          IO_retail_cost_variance_amount   IN OUT   NUMBER,
                          IO_margin_cost_variance_amount   IN OUT   NUMBER,
                          O_intercompany_margin            IN OUT   NUMBER,
                          O_shrinkage_cost                 IN OUT   NUMBER,
                          O_gross_margin_amt               IN OUT   NUMBER,
                          I_franchise_sales_retail         IN       NUMBER,
                          I_franchise_sales_cost           IN       NUMBER,
                          I_franchise_returns_retail       IN       NUMBER,
                          I_franchise_returns_cost         IN       NUMBER,
                          I_recoverable_tax                IN       NUMBER   DEFAULT NULL,
                          O_error_message                  IN OUT   VARCHAR2)

   RETURN BOOLEAN;

--------------------------------------------------------------------------
---     Name: cost_method_calc_retail
---  Purpose: Calculates the ending inventory value at retail for cost
---           method
---------------------------------------------------------------------------
FUNCTION COST_METHOD_CALC_RETAIL(IO_stk_retail                    IN OUT   NUMBER,
                                 I_purch_retail                   IN       NUMBER,
                                 I_rtv_retail                     IN       NUMBER,
                                 I_markup_retail                  IN       NUMBER,
                                 I_markup_can_retail              IN       NUMBER,
                                 I_net_sales_retail               IN       NUMBER,
                                 I_perm_markdown_retail           IN       NUMBER,
                                 I_promo_markdown_retail          IN       NUMBER,
                                 I_weight_variance_retail         IN       NUMBER,
                                 I_clear_markdown_retail          IN       NUMBER,
                                 I_markdown_can_retail            IN       NUMBER,
                                 I_tsf_in_retail                  IN       NUMBER,
                                 I_tsf_in_book_retail             IN       NUMBER,
                                 I_tsf_out_retail                 IN       NUMBER,
                                 I_tsf_out_book_retail            IN       NUMBER,
                                 I_stock_adj_retail               IN       NUMBER,
                                 I_freight_claims_retail          IN       NUMBER,
                                 I_stock_adj_cogs_retail          IN       NUMBER,
                                 I_empl_disc_retail               IN       NUMBER,
                                 I_stocktake_adj_retail           IN       NUMBER,
                                 I_reclass_in_retail              IN       NUMBER,
                                 I_reclass_out_retail             IN       NUMBER,
                                 I_intercompany_in_retail         IN       NUMBER,
                                 I_intercompany_in_cost           IN       NUMBER,
                                 I_intercompany_out_retail        IN       NUMBER,
                                 I_intercompany_out_cost          IN       NUMBER,
                                 I_intercompany_markup            IN       NUMBER,
                                 I_intercompany_markdown          IN       NUMBER,
                                 I_wo_activity_upd_inv            IN       NUMBER,
                                 I_wo_activity_post_fin           IN       NUMBER,
                                 I_deal_income_sales              IN       NUMBER,
                                 I_deal_income_purch              IN       NUMBER,
                                 I_restocking_fee                 IN       NUMBER,
                                 IO_retail_cost_variance_amount   IN OUT   NUMBER,
                                 IO_margin_cost_variance_amount   IN OUT   NUMBER,
                                 O_intercompany_margin            IN OUT   NUMBER,
                                 I_franchise_sales_retail         IN       NUMBER,
                                 I_franchise_returns_retail       IN       NUMBER,
                                 I_franchise_markup_retail        IN       NUMBER,
                                 I_franchise_markdown_retail      IN       NUMBER,
                                 O_error_message                  IN OUT   VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
---    Name: WAC_CALC_QTY_CHANGE
--- Purpose: Contains the formula for calculating WAC (Weighted average cost)
---          when receiving PO or stock order, or doing receiver unit adjustment.
---          I_receipt_cost is the unit cost of I_receipt_qty. I_total_cost is the
---          total cost of I_receipt_qty. If defined, I_total_cost is used in the
---          calculation.
-------------------------------------------------------------------------------------------
FUNCTION WAC_CALC_QTY_CHANGE(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_new_wac             IN OUT  ITEM_LOC_SOH.AV_COST%TYPE,
                             O_neg_soh_wac_adj_amt IN OUT  ITEM_LOC_SOH.AV_COST%TYPE,
                             I_old_wac             IN      ITEM_LOC_SOH.AV_COST%TYPE,
                             I_old_soh             IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_receipt_cost        IN      ITEM_LOC_SOH.AV_COST%TYPE,
                             I_receipt_qty         IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_total_cost          IN      ITEM_LOC_SOH.AV_COST%TYPE DEFAULT NULL,
                             I_recalc_ind          IN      BOOLEAN DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
---    Name: WAC_CALC_COST_CHANGE
--- Purpose: Contains the formula for calculating WAC (Weighted average cost)
---          when doing receiver cost adjustment for a PO
-------------------------------------------------------------------------------------------
FUNCTION WAC_CALC_COST_CHANGE(O_error_message       IN OUT  VARCHAR2,
                              O_new_wac             IN OUT  ITEM_LOC_SOH.AV_COST%TYPE,
                              O_neg_soh_wac_adj_amt IN OUT  ITEM_LOC_SOH.AV_COST%TYPE,
                              I_old_wac             IN      ITEM_LOC_SOH.AV_COST%TYPE,
                              I_soh                 IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_new_po_cost         IN      ITEM_LOC_SOH.AV_COST%TYPE,
                              I_old_po_cost         IN      ITEM_LOC_SOH.AV_COST%TYPE,
                              I_receipt_qty         IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function: ALT_WAC_CALC_COST_CHANGE
-- Purpose : This function is called by Itemloc_update_sql.upd_av_cost_change_cost to
-- calculate the new WAC, adjustment quantity and negative SOH when the FIFO/standard
-- indicator is set to FIFO
--------------------------------------------------------------------------------------------
FUNCTION ALT_WAC_CALC_COST_CHANGE (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_new_wac               IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   O_adj_qty               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   O_neg_soh_wac_adj_qty   IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   I_old_wac               IN       ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   I_soh                   IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   I_old_po_cost           IN       ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   I_po                    IN       SHIPMENT.ORDER_NO%TYPE,
                                   I_new_po_cost           IN       ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   I_location              IN       ITEM_LOC_SOH.LOC%TYPE,
                                   I_item                  IN       ITEM_LOC_SOH.ITEM%TYPE,
                                   I_pack_comp_rcvd_qty    IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT NULL,
                                   I_pack_item             IN       ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                   I_shipment              IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                   I_adj_code              IN       TRAN_DATA.ADJ_CODE%TYPE DEFAULT 'C')
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function: GET_GAFS_BOM
-- Purpose : This function will return the Beginning of Month Goods Available
--           for Sale retail and cost values.
--------------------------------------------------------------------------------------------
FUNCTION GET_GAFS_BOM(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_gafs_retail_bom   IN OUT   MONTH_DATA.HTD_GAFS_RETAIL%TYPE,
                      O_gafs_cost_bom     IN OUT   MONTH_DATA.HTD_GAFS_COST%TYPE,
                      I_dept              IN       MONTH_DATA.DEPT%TYPE,
                      I_class             IN       MONTH_DATA.CLASS%TYPE,
                      I_subclass          IN       MONTH_DATA.SUBCLASS%TYPE,
                      I_loc_type          IN       MONTH_DATA.LOC_TYPE%TYPE,
                      I_location          IN       MONTH_DATA.LOCATION%TYPE,
                      I_eom_date          IN       MONTH_DATA.EOM_DATE%TYPE,
                      I_opn_stk_retail    IN       MONTH_DATA.OPN_STK_RETAIL%TYPE,
                      I_opn_stk_cost      IN       MONTH_DATA.OPN_STK_COST%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: GET_PREV_NEXT_EOM
-- Purpose : This function will return the previous or next End of Month date based on the 
--           indicator passed in.
--------------------------------------------------------------------------------------------
FUNCTION GET_PREV_NEXT_EOM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_prev_eom_date   IN OUT   MONTH_DATA.EOM_DATE%TYPE,
                           O_next_eom_date   IN OUT   MONTH_DATA.EOM_DATE%TYPE,
                           I_eom_date        IN       MONTH_DATA.EOM_DATE%TYPE,
                           I_prev_next_ind   IN       VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END STKLEDGR_ACCTING_SQL;
/
