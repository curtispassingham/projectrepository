
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_AUDIT_RULES_SQL AS
-------------------------------------------------------------------------
FUNCTION REBUILD_RULE   (O_error_message     IN OUT  VARCHAR2,
                         I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE,
                         I_rule_rev_no       IN      SA_RULE_HEAD.RULE_REV_NO%TYPE,
                         I_new_vr_id         IN      SA_RULE_COMP.VR_ID%TYPE,
                         I_new_vr_rev_no     IN      SA_RULE_COMP.VR_REV_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION CREATE_REVISION(O_error_message     IN OUT  VARCHAR2,
                         O_new_rule_rev_no   IN OUT  SA_RULE_HEAD.RULE_REV_NO%TYPE,
                         O_rule_status       IN OUT  SA_RULE_HEAD.STATUS%TYPE,
                         I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE,
                         I_rule_rev_no       IN      SA_RULE_HEAD.RULE_REV_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
FUNCTION build_audit_rule
    -- Creates one rule function
    (
        O_error_message    IN OUT VARCHAR2,
        I_rule_id          IN     SA_RULE_HEAD.RULE_ID%TYPE,
        I_rule_rev_no      IN     SA_RULE_HEAD.RULE_REV_NO%TYPE,
        I_refresh_override IN     BOOLEAN,
        I_rule_status      IN     SA_RULE_HEAD.STATUS%TYPE
    )
RETURN INT;
-------------------------------------------------------------------------
    FUNCTION drop_audit_rule
    (
        i_rule_id       IN     sa_rule_head.rule_id%TYPE,
        i_rule_rev_no   IN     SA_RULE_HEAD.RULE_REV_NO%TYPE,
        o_error_message IN OUT VARCHAR2
    )
           RETURN INT;
-------------------------------------------------------------------------
    FUNCTION INSERT_ERROR
        -- Will do all the necessary work to insert a new rule/error
        -- calculation.
    (
        o_error_message        IN OUT VARCHAR2,
        i_store_day_seq_no     IN     sa_store_day.store_day_seq_no%TYPE,
        i_balance_group_seq_no IN     sa_balance_group.bal_group_seq_no%TYPE,
        i_total_seq_no         IN     SA_TOTAL.TOTAL_SEQ_NO%TYPE,
        i_tran_seq_no          IN     sa_tran_head.tran_seq_no%TYPE,
        i_key_value1           IN     sa_error.key_value_1%TYPE,
        i_key_value2           IN     sa_error.key_value_2%TYPE,
        i_error_code           IN     sa_error_codes.error_code%TYPE,
        i_rec_type             IN     sa_rule_head.rec_type%TYPE,
        i_orig_value           IN     sa_error.orig_value%TYPE,
        i_rule_status          IN     sa_rule_head.status%type,
        i_store                IN     sa_store_day.store%TYPE DEFAULT NULL,
        i_day                  IN     sa_store_day.day%TYPE DEFAULT NULL

    )
        RETURN BOOLEAN;
-------------------------------------------------------------------------
    FUNCTION process_audit_rules
    -- Recalculates totals for a given store_day_seq_no
    (
        o_error_message     IN OUT VARCHAR2,
        i_store_day_seq_no  IN     sa_store_day.store_day_seq_no%TYPE,
        i_user_id           IN     sa_employee.user_id%TYPE,
        I_store             IN      SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
        I_day               IN      SA_STORE_DAY.DAY%TYPE DEFAULT NULL
    )
           RETURN BOOLEAN;
------------------------------------------------------------------
--- Name:    DELETE_AUDIT_RULE
--- Purpose: Completely delete the audit rule including all revisions
---
FUNCTION DELETE_AUDIT_RULE(O_error_message     IN OUT  VARCHAR2,
                           I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------
--- Name:    APPROVED_RULE_EXISTS
--- Purpose: Check if there are any revisions of the rule where the status
---          is Approved.
---
FUNCTION APPROVED_RULE_EXISTS(O_error_message     IN OUT  VARCHAR2,
                              O_approved_exists   IN OUT  VARCHAR2,
                              I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
--- Name:    DELETE_VR
--- Purpose: Deletes the vr_id from the sa_vr_head table and its child tables.
---
FUNCTION DELETE_VR(O_error_message     IN OUT  VARCHAR2,
                   I_vr_id             IN      SA_VR_HEAD.VR_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
--- Purpose: Retrieve the most recent revision number for the passed-in
---          rule ID.
---
FUNCTION MAX_RULE_REV_NO(O_error_message     IN OUT  VARCHAR2,
                         O_max_rule_rev_no   IN OUT  SA_RULE_HEAD.RULE_REV_NO%TYPE,
                         I_rule_id           IN      SA_RULE_HEAD.RULE_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
--- Name:    CHECK_UOM_ERRORS
--- Purpose: Check if there are any errors for an invalid
---          selling uom.
---

FUNCTION CHECK_UOM_ERRORS(O_error_message     IN OUT  VARCHAR2,
                          I_store_day_seq_no  IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                          I_store             IN      SA_STORE_DAY.STORE%TYPE,
                          I_day               IN      SA_STORE_DAY.DAY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
END SA_AUDIT_RULES_SQL;
/
