
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_SEQUENCE2_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
---  These functions will return the valid unique sequence numbers to be used
---  as primary keys throught the Sales Audit Application.
-----------------------------------------------------------------------------------
FUNCTION GET_BAL_GROUP_SEQ(O_error_message IN OUT VARCHAR2,
                           O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_ERROR_SEQ(O_error_message IN OUT VARCHAR2,
                       O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_ESCHEAT_SEQ(O_error_message IN OUT VARCHAR2,
                         O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_EXPORT_SEQ(O_error_message IN OUT VARCHAR2,
                        O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_MISS_TRAN_SEQ(O_error_message IN OUT VARCHAR2,
                           O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_STORE_DAY_SEQ(O_error_message IN OUT VARCHAR2,
                           O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_TOTAL_SEQ(O_error_message IN OUT VARCHAR2,
                       O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_TRAN_SEQ(O_error_message IN OUT VARCHAR2,
                      O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_VOUCHER_SEQ(O_error_message IN OUT VARCHAR2,
                         O_seq_value     IN OUT NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END SA_SEQUENCE2_SQL;
/
