
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_TOTAL_VALIDATE_SQL AS
-------------------------------------------------------------------
FUNCTION CHECK_UNIQUE_TOTAL_ID (O_error_message    IN OUT VARCHAR2,
                                O_unique           OUT    BOOLEAN,
                                I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)                    
   RETURN BOOLEAN IS
   
   L_dummy	VARCHAR2(1);

   cursor C_UNIQUE is
      select 'x'
         from sa_total_head
       where total_id = I_total_id;

BEGIN

   if I_total_id is NULL then
      O_error_message := sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 
                                             'I_total_id', 'SA_TOTAL_VALIDATE_SQL.CHECK_UNIQUE_TOTAL_ID', null);
      return FALSE;
   else
      open C_UNIQUE;
      fetch C_UNIQUE into L_dummy;
      ---
      if C_UNIQUE%FOUND then
         O_unique := FALSE;
         O_error_message := sql_lib.create_msg('TOT_NOT_UNIQ', 
                                                I_total_id,null, null);
         return FALSE;
      else
         O_unique := TRUE;
      end if;
      ---
      close C_UNIQUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
	O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
						SQLERRM,
						'SA_TOTAL_VALIDATE_SQL.CHECK_UNIQUE_TOTAL_ID',
						to_char(SQLCODE));
	return FALSE;

END CHECK_UNIQUE_TOTAL_ID;

----------------------------------------------------------------------------------------------
FUNCTION GET_MAX_TOTAL_REV_NO (O_error_message		IN OUT VARCHAR2,
                               O_max_total_rev_no       OUT    SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                               I_total_id               IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN IS

   L_max_rev_no		SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE;	

   cursor C_MAX_REV is
      select max(total_rev_no)
         from sa_total_head
        where total_id = I_total_id;

BEGIN
   if I_total_id is NULL then
      O_error_message := sql_lib.create_msg('ARI_INV_PARM', 
                                             null, null, null);
      return FALSE;
   else
      open C_MAX_REV;
      fetch C_MAX_REV into L_max_rev_no;
      ---
      if C_MAX_REV%NOTFOUND then
         O_error_message := sql_lib.create_msg('NO_MAX_REV',
                                                I_total_id, null, null);
         return FALSE;
      else
         O_max_total_rev_no := L_max_rev_no;
      end if;
      ---
      close C_MAX_REV;
   end if;
   ---
   return TRUE;


EXCEPTION
   when OTHERS then
	O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
						SQLERRM,
						'SA_TOTAL_VALIDATE_SQL.GET_MAX_TOTAL_REV_NO',
						to_char(SQLCODE));
	return FALSE;

END GET_MAX_TOTAL_REV_NO;
   
-------------------------------------------------------------------
END SA_TOTAL_VALIDATE_SQL;
/

