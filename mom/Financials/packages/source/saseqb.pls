
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_SEQUENCE_SQL AS
   GP_table_owner                           VARCHAR2(30) := NULL;
   ---
   GP_increment_by_tran_seq_no              NUMBER := NULL;
   ---
   GP_increment_by_store_day_seq            NUMBER := NULL;
   ---
   GP_increment_by_voucher_seq_no           NUMBER := NULL;
   ---
   GP_increment_by_bal_group_seq            NUMBER := NULL;
   ---
   GP_increment_by_value_rev_no             NUMBER := NULL;
   ---
   GP_increment_by_error_no                 NUMBER := NULL;
   ---
   GP_increment_by_export_no                NUMBER := NULL;
   ---
   GP_increment_by_total_no                 NUMBER := NULL;
   ---
   GP_increment_by_miss_tran_seq            NUMBER := NULL;
   ---
   GP_increment_by_escheat_seq              NUMBER := NULL;
   ---
   GP_seq_num                               NUMBER := NULL;
   ---
   GP_seq_numleft                           NUMBER := 0;
-------------------------------------------------------------------   
---FUNCTION POPULATE_GLOBALS

   ---
   --This internal package function will determine the appropriate id to use
   --as a lock handle for the given sequence
------------------------------------------------------------------
FUNCTION GET_HANDLE(O_error_message IN OUT VARCHAR2,
                    I_seq_type      IN     VARCHAR2,
                    O_lockhandle    IN OUT NUMBER)
      RETURN BOOLEAN IS
   L_program          VARCHAR2(60) := 'SA_SEQUENCE_SQL.GET_HANDLE';

BEGIN
   O_lockhandle := NULL;
   if I_seq_type = 'SA_TRAN_SEQ_NO_SEQUENCE' then
      ---
      O_lockhandle := 151;
      ---
   elsif I_seq_type = 'SA_STORE_DAY_SEQ_NO_SEQUENCE' then
      ---
      O_lockhandle := 152;
      ---
   elsif I_seq_type = 'SA_VOUCHER_SEQ_NO_SEQUENCE' then
      ---
      O_lockhandle := 153;
      ---
   elsif I_seq_type = 'SA_BAL_GROUP_NO_SEQUENCE' then
      ---
      O_lockhandle := 154;
      ---
   elsif I_seq_type = 'SA_VALUE_REV_NO_SEQUENCE' then
      ---
      O_lockhandle := 155;
      ---
    elsif I_seq_type = 'SA_ERROR_SEQ_NO_SEQUENCE' then
      ---
      O_lockhandle := 156;
      ---
    elsif I_seq_type = 'SA_EXPORT_SEQ_NO_SEQUENCE' then
      ---
      O_lockhandle := 157;
      ---
   elsif I_seq_type = 'SA_TOTAL_SEQ_NO_SEQUENCE' then
      ---
      O_lockhandle := 158;
      ---
   elsif I_seq_type = 'SA_MISS_TRAN_SEQUENCE' then
      ---
      O_lockhandle := 159;
      ---
   elsif I_seq_type = 'SA_ESCHEAT_SEQ_NO_SEQUENCE' then
      ---
      O_lockhandle := 160;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_HANDLE;
------------------------------------------------------------------
   ---FUNCTION POPULATE_GLOBALS
   ---
   --This internal package function will populate variable global to the package,
   --including the system_options.table owner and increment_by and min_value for
   --the various sequences.
------------------------------------------------------------------
FUNCTION POPULATE_GLOBALS (O_error_message      IN OUT VARCHAR2,
                           I_seq_type           IN     VARCHAR2)
      RETURN BOOLEAN IS
   L_program          VARCHAR2(60) := 'SA_SEQUENCE_SQL.POPULATE_GLOBALS';

   cursor C_SEQ_VALS is
     select increment_by
        from all_sequences
       where all_sequences.sequence_name = I_seq_type
         and all_sequences.sequence_owner = GP_table_owner;

   cursor C_GET_SEQ_OWNER is
      select table_owner
        from system_options;
   ---
BEGIN
   if GP_table_owner is NULL then
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_SEQ_OWNER','SYSTEM_OPTIONS',NULL);
      open C_GET_SEQ_OWNER;
      SQL_LIB.SET_MARK('FETCH','C_GET_SEQ_OWNER','SYSTEM_OPTIONS',NULL);
      fetch C_GET_SEQ_OWNER into GP_table_owner;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SEQ_OWNER','SYSTEM_OPTIONS',NULL);
      close C_GET_SEQ_OWNER;
      ---
   end if;
   ---
   if I_seq_type = 'SA_TRAN_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_tran_seq_no is NULL then
         ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_tran_seq_no;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_tran_seq_no is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
   elsif I_seq_type = 'SA_STORE_DAY_SEQ_NO_SEQUENCE' then
      ---
      if  GP_increment_by_store_day_seq is NULL then
         ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_store_day_seq;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_store_day_seq is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
   elsif I_seq_type = 'SA_VOUCHER_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_voucher_seq_no is NULL then
         ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_voucher_seq_no;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_voucher_seq_no is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
   elsif I_seq_type = 'SA_BAL_GROUP_NO_SEQUENCE' then
      ---
      if GP_increment_by_bal_group_seq is NULL then
          ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_bal_group_seq;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_bal_group_seq is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
   elsif I_seq_type = 'SA_VALUE_REV_NO_SEQUENCE' then
      ---
      if GP_increment_by_value_rev_no is NULL then
          ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_value_rev_no;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_value_rev_no is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
    elsif I_seq_type = 'SA_ERROR_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_error_no is NULL then
          ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_error_no;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_error_no is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
    elsif I_seq_type = 'SA_EXPORT_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_export_no is NULL then
          ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_export_no;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_export_no is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
   elsif I_seq_type = 'SA_TOTAL_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_total_no is NULL then
          ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_total_no;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_total_no is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
   elsif I_seq_type = 'SA_MISS_TRAN_SEQUENCE' then
      ---
      if GP_increment_by_miss_tran_seq is NULL then
         ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS', 'ALL_SEQUENCES', 'sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_miss_tran_seq;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_miss_tran_seq is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
   elsif I_seq_type = 'SA_ESCHEAT_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_escheat_seq is NULL then
         ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_VALS', 'ALL_SEQUENCES', 'sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         open C_SEQ_VALS;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         fetch C_SEQ_VALS into GP_increment_by_escheat_seq;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALS','ALL_SEQUENCES','sequence_name: '||I_seq_type||', sequence_owner: '||GP_table_owner);
         close C_SEQ_VALS;
         --- if it is still NULL, something bad happened.
         if GP_increment_by_escheat_seq is NULL then
            O_error_message := sql_lib.create_msg('SEQUENCE_NOT_FOUND',
                                                  I_seq_type,
                                                  GP_table_owner,
                                                  L_program);
            return FALSE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_GLOBALS;
------------------------------------------------------------------
   ---FUNCTION GET_STARTVAL
   ---
   --This internal function is used to find the starting (nextval) of the appropriate
   --sequence.
------------------------------------------------------------------
FUNCTION GET_STARTVAL (O_error_message      IN OUT VARCHAR2,
                       O_starting_val       IN OUT NUMBER,
                       I_seq_type           IN     VARCHAR2)
      RETURN BOOLEAN IS
   L_program          VARCHAR2(60) := 'SA_SEQUENCE_SQL.GET_STARTVAL';
   cursor C_SEQ_TRAN_SEQ_NO is
      select SA_TRAN_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_STORE_DAY_SEQ_NO is
      select SA_STORE_DAY_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_VOUCHER_SEQ_NO is
      select SA_VOUCHER_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_BAL_GROUP_NO is
      select SA_BAL_GROUP_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_VALUE_REV_NO is
      select SA_VALUE_REV_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_ERROR_NO is
      select SA_ERROR_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_EXPORT_NO is
      select SA_EXPORT_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_TOTAL_NO is
      select SA_TOTAL_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_MISS_TRAN_NO is
      select SA_MISS_TRAN_SEQUENCE.nextval
        from dual;
   cursor C_ESCHEAT_NO is
      select SA_ESCHEAT_SEQ_NO_SEQUENCE.nextval
        from dual;
BEGIN
   if I_seq_type = 'SA_TRAN_SEQ_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_TRAN_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      open C_SEQ_TRAN_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_TRAN_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_SEQ_TRAN_SEQ_NO into O_starting_val;
      ---
      if C_SEQ_TRAN_SEQ_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_TRAN_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
         close C_SEQ_TRAN_SEQ_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_TRAN_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      close C_SEQ_TRAN_SEQ_NO;
      ---
   elsif I_seq_type = 'SA_STORE_DAY_SEQ_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_STORE_DAY_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      open C_SEQ_STORE_DAY_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_STORE_DAY_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_SEQ_STORE_DAY_SEQ_NO into O_starting_val;
      ---
      if C_SEQ_STORE_DAY_SEQ_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_STORE_DAY_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
         close C_SEQ_STORE_DAY_SEQ_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_STORE_DAY_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      close C_SEQ_STORE_DAY_SEQ_NO;
      ---
   elsif I_seq_type = 'SA_VOUCHER_SEQ_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_VOUCHER_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      open C_SEQ_VOUCHER_SEQ_NO ;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_VOUCHER_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_SEQ_VOUCHER_SEQ_NO into O_starting_val;
      ---
      if C_SEQ_VOUCHER_SEQ_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VOUCHER_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
         close C_SEQ_VOUCHER_SEQ_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_VOUCHER_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
      close C_SEQ_VOUCHER_SEQ_NO;
      ---
   elsif I_seq_type = 'SA_BAL_GROUP_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_BAL_GROUP_NO','DUAL','sequence_name: '||I_seq_type);
      open C_SEQ_BAL_GROUP_NO;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_BAL_GROUP_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_SEQ_BAL_GROUP_NO into O_starting_val;
      ---
      if C_SEQ_BAL_GROUP_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_BAL_GROUP_NO','DUAL','sequence_name: '||I_seq_type);
         close C_SEQ_BAL_GROUP_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---      
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_BAL_GROUP_NO','DUAL','sequence_name: '||I_seq_type);
      close C_SEQ_BAL_GROUP_NO;
      ---
   elsif I_seq_type = 'SA_VALUE_REV_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_VALUE_REV_NO','DUAL','sequence_name: '||I_seq_type);
      open C_SEQ_VALUE_REV_NO;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_VALUE_REV_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_SEQ_VALUE_REV_NO into O_starting_val;
      ---
      if C_SEQ_VALUE_REV_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALUE_REV_NO','DUAL','sequence_name: '||I_seq_type);
         close C_SEQ_VALUE_REV_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---     
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_VALUE_REV_NO','DUAL','sequence_name: '||I_seq_type);
      close C_SEQ_VALUE_REV_NO;
      ---
   elsif I_seq_type = 'SA_ERROR_SEQ_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_ERROR_NO','DUAL','sequence_name: '||I_seq_type);
      open C_SEQ_ERROR_NO;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_ERROR_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_SEQ_ERROR_NO into O_starting_val;    
      ---
      if C_SEQ_ERROR_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_ERROR_NO','DUAL','sequence_name: '||I_seq_type);
         close C_SEQ_ERROR_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---     
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_ERROR_NO','DUAL','sequence_name: '||I_seq_type);
      close C_SEQ_ERROR_NO;
      ---
   elsif I_seq_type = 'SA_EXPORT_SEQ_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_EXPORT_NO','DUAL','sequence_name: '||I_seq_type);
      open C_SEQ_EXPORT_NO;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_EXPORT_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_SEQ_EXPORT_NO into O_starting_val;
      ---
      if C_SEQ_EXPORT_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_EXPORT_NO','DUAL','sequence_name: '||I_seq_type);
         close C_SEQ_EXPORT_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      --- 
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_EXPORT_NO','DUAL','sequence_name: '||I_seq_type);
      close C_SEQ_EXPORT_NO;
      ---
   elsif I_seq_type = 'SA_TOTAL_SEQ_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_TOTAL_NO','DUAL','sequence_name: '||I_seq_type);
      open C_SEQ_TOTAL_NO;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_TOTAL_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_SEQ_TOTAL_NO into O_starting_val;
      ---
      if C_SEQ_TOTAL_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_TOTAL_NO','DUAL','sequence_name: '||I_seq_type);
         close C_SEQ_TOTAL_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---       
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_TOTAL_NO','DUAL','sequence_name: '||I_seq_type);
      close C_SEQ_TOTAL_NO;
      ---
   elsif I_seq_type = 'SA_MISS_TRAN_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_MISS_TRAN_NO','DUAL','sequence_name: '||I_seq_type);
      open C_MISS_TRAN_NO;
      SQL_LIB.SET_MARK('FETCH','C_MISS_TRAN_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_MISS_TRAN_NO into O_starting_val;
      ---
      if C_MISS_TRAN_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_MISS_TRAN_NO','DUAL','sequence_name: '||I_seq_type);
         close C_MISS_TRAN_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---       
      SQL_LIB.SET_MARK('CLOSE','C_MISS_TRAN_NO','DUAL','sequence_name: '||I_seq_type);
      close C_MISS_TRAN_NO;
      ---
   elsif I_seq_type = 'SA_ESCHEAT_SEQ_NO_SEQUENCE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_ESCHEAT_NO','DUAL','sequence_name: '||I_seq_type);
      open C_ESCHEAT_NO;
      SQL_LIB.SET_MARK('FETCH','C_ESCHEAT_NO','DUAL','sequence_name: '||I_seq_type);
      fetch C_ESCHEAT_NO into O_starting_val;
      ---
      if C_ESCHEAT_NO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_ESCHEAT_NO','DUAL','sequence_name: '||I_seq_type);
         close C_ESCHEAT_NO;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---             
      SQL_LIB.SET_MARK('CLOSE','C_ESCHEAT_NO','DUAL','sequence_name: '||I_seq_type);
      close C_ESCHEAT_NO;
   ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STARTVAL;
------------------------------------------------------------------
   ---FUNCTION ERROR_HANDLING
   ---
   --This internal function is used to evaluate the success of the call to
   --DBMS_LOCK.REQUEST.  If the function returns 0, it has suceeded.  Otherwise
   --it has failed.  If the function returns 1, it has timed out.  If the function
   --returns 2, it has encountered a deadlock.  If the function returns a 3, it has
   --been passed invalid inout parameters.  If the function returns a 4, the process
   --does not own the lock handle it is requesting a lock on.  If the function returns
   --a 5, the lock handle is illegal.
------------------------------------------------------------------
FUNCTION ERROR_HANDLING (O_error_message      IN OUT VARCHAR2,
                         I_seq_type           IN     VARCHAR2,
                         I_call_status        IN     VARCHAR2)
      RETURN BOOLEAN IS
   L_program          VARCHAR2(60) := 'SA_SEQUENCE_SQL.ERROR_HANDLING';
BEGIN
   ---
   if I_call_status != 0 then
      ---
      if I_call_status = 1 then
         O_error_message := sql_lib.create_msg('TIME_OUT', I_seq_type,
                                                null, null);
      elsif I_call_status = 2 then
         O_error_message := sql_lib.create_msg('DEADLOCK', I_seq_type,
                                                null, null);
      elsif I_call_status = 3 then
         O_error_message := sql_lib.create_msg('INV_PARAM_PROG_UNIT', 'DBMS_LOCK.REQUEST',
                                                null, null);
      elsif I_call_status = 4 then
         O_error_message := sql_lib.create_msg('NO_OWN_LOCK', I_seq_type,
                                                null, null);
      elsif I_call_status = 5 then
         O_error_message := sql_lib.create_msg('ILL_LOCKHANDLE', null,
                                                null, null);
      end if;
      ---
      return FALSE;
   else
      return TRUE;
   end if;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ERROR_HANDLING;
------------------------------------------------------------------
   ---FUNCTION RESERVE_REST
   ---
   --This internal function reserves any other values needed beyond the sequences
   --increment by and calculates the value reserved through.
------------------------------------------------------------------
FUNCTION RESERVE_REST (O_error_message      IN OUT VARCHAR2,
                       O_reserved_through   IN OUT NUMBER,
                       I_seq_type           IN     VARCHAR2,
                       I_starting_value     IN     NUMBER,
                       I_values_needed      IN     NUMBER)
      RETURN BOOLEAN IS
   cursor C_SEQ_TRAN_SEQ_NO is
      select SA_TRAN_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_STORE_DAY_SEQ_NO is
      select SA_STORE_DAY_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_VOUCHER_SEQ_NO is
      select SA_VOUCHER_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_BAL_GROUP_NO is
      select SA_BAL_GROUP_NO_SEQUENCE.nextval
        from dual;
   cursor C_VALUE_REV_NO is
      select SA_VALUE_REV_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_ERROR_NO is
      select SA_ERROR_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_EXPORT_NO is
      select SA_EXPORT_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_SEQ_TOTAL_NO is
      select SA_TOTAL_SEQ_NO_SEQUENCE.nextval
        from dual;
   cursor C_MISS_TRAN_NO is
      select SA_MISS_TRAN_SEQUENCE.nextval
        from dual;
   cursor C_ESCHEAT_NO is
      select SA_ESCHEAT_SEQ_NO_SEQUENCE.nextval
        from dual;
   L_program          VARCHAR2(60) := 'SA_SEQUENCE_SQL.RESERVE_REST';
   i                  NUMBER := 2;
   L_factor           NUMBER;
   L_currval          NUMBER := I_starting_value;
   ---
   --L_currval gets I_starting value so that the reserved through value can
   --be calculated if the increment_by is not less than the values needed.
   ---
BEGIN
   if I_seq_type = 'SA_TRAN_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_tran_seq_no < I_values_needed then
      ---
         L_factor := ceil(I_values_needed /GP_increment_by_tran_seq_no);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_SEQ_TRAN_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
            open C_SEQ_TRAN_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH','C_SEQ_TRAN_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
            fetch C_SEQ_TRAN_SEQ_NO into L_currval;
            ---
            if C_SEQ_TRAN_SEQ_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_SEQ_TRAN_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
               close C_SEQ_TRAN_SEQ_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---           
            SQL_LIB.SET_MARK('CLOSE','C_SEQ_TRAN_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
            close C_SEQ_TRAN_SEQ_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      --The value reserved through will be one less than the next starting value.
      ---
      O_reserved_through := L_currval + (GP_increment_by_tran_seq_no - 1);
      ---
   elsif I_seq_type = 'SA_STORE_DAY_SEQ_NO_SEQUENCE' then
       ---
      if GP_increment_by_store_day_seq < I_values_needed then
      ---
         L_factor := ceil(I_values_needed /GP_increment_by_store_day_seq);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_SEQ_STORE_DAY_SEQ','DUAL','sequence_name: '||I_seq_type);
            open C_SEQ_STORE_DAY_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH','C_SEQ_STORE_DAY_SEQ','DUAL','sequence_name: '||I_seq_type);
            fetch C_SEQ_STORE_DAY_SEQ_NO into L_currval;
            ---
            if C_SEQ_STORE_DAY_SEQ_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_SEQ_STORE_DAY_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
               close C_SEQ_STORE_DAY_SEQ_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE','C_SEQ_STORE_DAY_SEQ','DUAL','sequence_name: '||I_seq_type);
            close C_SEQ_STORE_DAY_SEQ_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      --The value reserved through will be one less than the next starting value.
      ---
      O_reserved_through := L_currval + (GP_increment_by_store_day_seq - 1);
      ---
   elsif I_seq_type = 'SA_VOUCHER_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_voucher_seq_no < I_values_needed then
         ---
         L_factor := ceil(I_values_needed /GP_increment_by_voucher_seq_no);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_SEQ_VOUCHER_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
            open C_SEQ_VOUCHER_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH','C_SEQ_VOUCHER_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
            fetch C_SEQ_VOUCHER_SEQ_NO into L_currval;
            ---
            if C_SEQ_VOUCHER_SEQ_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_SEQ_VOUCHER_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
               close C_SEQ_VOUCHER_SEQ_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE','C_SEQ_VOUCHER_SEQ_NO','DUAL','sequence_name: '||I_seq_type);
            close C_SEQ_VOUCHER_SEQ_NO;
            ---
         END LOOP;
         ---
      end if;
      ---
      O_reserved_through := L_currval + (GP_increment_by_voucher_seq_no - 1);
      ---
   elsif I_seq_type = 'SA_BAL_GROUP_NO_SEQUENCE' then
      ---
      if GP_increment_by_bal_group_seq < I_values_needed then
      ---
         L_factor := ceil(I_values_needed /GP_increment_by_bal_group_seq);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_SEQ_BAL_GROUP_NO','DUAL','sequence_name: '||I_seq_type);
            open C_SEQ_BAL_GROUP_NO;
            SQL_LIB.SET_MARK('FETCH','C_SEQ_BAL_GROUP_NO','DUAL','sequence_name: '||I_seq_type);
            fetch C_SEQ_BAL_GROUP_NO into L_currval;
            ---
            if C_SEQ_BAL_GROUP_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_SEQ_BAL_GROUP_NO','DUAL','sequence_name: '||I_seq_type);
               close C_SEQ_BAL_GROUP_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---      
            SQL_LIB.SET_MARK('CLOSE','C_SEQ_BAL_GROUP_NO','DUAL','sequence_name: '||I_seq_type);
            close C_SEQ_BAL_GROUP_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      O_reserved_through := L_currval + (GP_increment_by_bal_group_seq - 1);
      ---
   elsif I_seq_type = 'SA_VALUE_REV_NO_SEQUENCE' then
      ---
      if GP_increment_by_value_rev_no < I_values_needed then
      ---
         L_factor := ceil(I_values_needed /GP_increment_by_value_rev_no);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_VALUE_REV_NO','DUAL','sequence_name: '||I_seq_type);
            open C_VALUE_REV_NO;
            SQL_LIB.SET_MARK('FETCH','C_VALUE_REV_NO','DUAL','sequence_name: '||I_seq_type);
            fetch C_VALUE_REV_NO into L_currval;
            ---
            if C_VALUE_REV_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_VALUE_REV_NO','DUAL','sequence_name: '||I_seq_type);
               close C_VALUE_REV_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---     
            SQL_LIB.SET_MARK('CLOSE','C_VALUE_REV_NO','DUAL','sequence_name: '||I_seq_type);
            close C_VALUE_REV_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      O_reserved_through := L_currval + (GP_increment_by_value_rev_no - 1);
      ---
   elsif I_seq_type = 'SA_ERROR_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_error_no < I_values_needed then
      ---
         L_factor := ceil(I_values_needed /GP_increment_by_error_no);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_SEQ_ERROR_NO' ,'DUAL','sequence_name: '||I_seq_type);
            open C_SEQ_ERROR_NO ;
            SQL_LIB.SET_MARK('FETCH','C_SEQ_ERROR_NO' ,'DUAL','sequence_name: '||I_seq_type);
            fetch C_SEQ_ERROR_NO into L_currval;
            ---
            if C_SEQ_ERROR_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_SEQ_ERROR_NO','DUAL','sequence_name: '||I_seq_type);
               close C_SEQ_ERROR_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---     
            SQL_LIB.SET_MARK('CLOSE','C_SEQ_ERROR_NO' ,'DUAL','sequence_name: '||I_seq_type);
            close C_SEQ_ERROR_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      O_reserved_through := L_currval + (GP_increment_by_error_no - 1);
      ---
   elsif I_seq_type = 'SA_EXPORT_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_export_no < I_values_needed then
      ---
         L_factor := ceil(I_values_needed /GP_increment_by_export_no);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_SEQ_EXPORT_NO','DUAL','sequence_name: '||I_seq_type);
            open C_SEQ_EXPORT_NO;
            SQL_LIB.SET_MARK('FETCH','C_SEQ_EXPORT_NO','DUAL','sequence_name: '||I_seq_type);
            fetch C_SEQ_EXPORT_NO into L_currval;
            ---
            if C_SEQ_EXPORT_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_SEQ_EXPORT_NO','DUAL','sequence_name: '||I_seq_type);
               close C_SEQ_EXPORT_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            --- 
            SQL_LIB.SET_MARK('CLOSE','C_SEQ_EXPORT_NO','DUAL','sequence_name: '||I_seq_type);
            close C_SEQ_EXPORT_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      O_reserved_through := L_currval + (GP_increment_by_export_no- 1);
      ---
   elsif I_seq_type = 'SA_TOTAL_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_total_no < I_values_needed then
      ---
         L_factor := ceil(I_values_needed /GP_increment_by_total_no);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_SEQ_TOTAL_NO','DUAL','sequence_name: '||I_seq_type);
            open C_SEQ_TOTAL_NO;
            SQL_LIB.SET_MARK('FETCH','C_SEQ_TOTAL_NO','DUAL','sequence_name: '||I_seq_type);
            fetch C_SEQ_TOTAL_NO into L_currval;
            ---
            if C_SEQ_TOTAL_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_SEQ_TOTAL_NO','DUAL','sequence_name: '||I_seq_type);
               close C_SEQ_TOTAL_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---       
            SQL_LIB.SET_MARK('CLOSE','C_SEQ_TOTAL_NO','DUAL','sequence_name: '||I_seq_type);
            close C_SEQ_TOTAL_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      O_reserved_through := L_currval + (GP_increment_by_total_no- 1);
      ---
   elsif I_seq_type = 'SA_MISS_TRAN_SEQUENCE' then
      ---
      if GP_increment_by_miss_tran_seq < I_values_needed then
      ---
         L_factor := ceil(I_values_needed / GP_increment_by_miss_tran_seq);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_MISS_TRAN_NO','DUAL','sequence_name: '||I_seq_type);
            open C_MISS_TRAN_NO;
            SQL_LIB.SET_MARK('FETCH','C_MISS_TRAN_NO','DUAL','sequence_name: '||I_seq_type);
            fetch C_MISS_TRAN_NO into L_currval;
            ---
            if C_MISS_TRAN_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_MISS_TRAN_NO','DUAL','sequence_name: '||I_seq_type);
               close C_MISS_TRAN_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---       
            SQL_LIB.SET_MARK('CLOSE','C_MISS_TRAN_NO','DUAL','sequence_name: '||I_seq_type);
            close C_MISS_TRAN_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      O_reserved_through := L_currval + (GP_increment_by_miss_tran_seq - 1);
      ---
   elsif I_seq_type = 'SA_ESCHEAT_SEQ_NO_SEQUENCE' then
      ---
      if GP_increment_by_escheat_seq < I_values_needed then
      ---
         L_factor := ceil(I_values_needed / GP_increment_by_escheat_seq);
            ---
            for i in 2 .. L_factor LOOP
            ---
            SQL_LIB.SET_MARK('OPEN','C_ESCHEAT_NO','DUAL','sequence_name: '||I_seq_type);
            open C_ESCHEAT_NO;
            SQL_LIB.SET_MARK('FETCH','C_ESCHEAT_NO','DUAL','sequence_name: '||I_seq_type);
            fetch C_ESCHEAT_NO into L_currval;
            ---
            if C_ESCHEAT_NO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_ESCHEAT_NO','DUAL','sequence_name: '||I_seq_type);
               close C_ESCHEAT_NO;
               O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---             
            SQL_LIB.SET_MARK('CLOSE','C_ESCHEAT_NO','DUAL','sequence_name: '||I_seq_type);
            close C_ESCHEAT_NO;
            ---
         END LOOP;
      ---
      end if;
      ---
      O_reserved_through := L_currval + (GP_increment_by_escheat_seq - 1);
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RESERVE_REST;
------------------------------------------------------------------
FUNCTION GET_SEQ(O_error_message      IN OUT VARCHAR2,
                 O_starting_value     IN OUT NUMBER,
                 O_reserved_through   IN OUT NUMBER,
                 I_values_needed      IN     NUMBER,
                 I_seq_type           IN     VARCHAR2)
   RETURN BOOLEAN IS
   L_program          VARCHAR2(60) := 'SA_SEQUENCE_SQL.GET_SEQ';
   L_lockhandle       NUMBER;
   L_call_status      NUMBER;
BEGIN
   if I_seq_type not in ('SA_TRAN_SEQ_NO_SEQUENCE', 'SA_STORE_DAY_SEQ_NO_SEQUENCE',
                         'SA_VOUCHER_SEQ_NO_SEQUENCE', 'SA_BAL_GROUP_NO_SEQUENCE',
                         'SA_VALUE_REV_NO_SEQUENCE','SA_ERROR_SEQ_NO_SEQUENCE',
                         'SA_EXPORT_SEQ_NO_SEQUENCE', 'SA_TOTAL_SEQ_NO_SEQUENCE',
                         'SA_MISS_TRAN_SEQUENCE', 'SA_ESCHEAT_SEQ_NO_SEQUENCE')
                          or I_seq_type is NULL or I_values_needed is NULL then
      O_error_message := sql_lib.create_msg('INV_PARAM_PROG_UNIT', L_program,
                                             null, null);
      return FALSE;
   end if;
   ---
   --Find an appropriate id to use as lock handle
   ---
   if GET_HANDLE(O_error_message,
                 I_seq_type,
                 L_lockhandle) = FALSE then
      return FALSE;
   end if;

   ---
   --This internal package function will populate variable global to the package,
   --including the system_options.table owner and increment_by and min_value for
   --the various sequences.
   ---
   if POPULATE_GLOBALS(O_error_message,
                       I_seq_type) = FALSE then
      return FALSE;
   end if;
   ---
   --This oracle built in will request a lock on the sequence.  The third parameter
   --of this function is the wait time (in seconds).  This value may need to be reassessed
   --by the system administrator depending files sizes.  This function returns
   --an integer that is evaluated by the internal function ERROR_HANDLING.
   ---
   L_call_status := DBMS_LOCK.REQUEST(L_lockhandle,
                                      DBMS_LOCK.x_mode,
                                      30,
                                      FALSE);
   ---
   --This internal function is used to evaluate the success of the call to
   --DBMS_LOCK.REQUEST.  If the function returns 0, it has suceeded.  Otherwise
   --it has failed.  If the function returns 1, it has timed out.  If the function
   --returns 2, it has encountered a deadlock.  If the function returns a 3, it has
   --been passed invalid inout parameters.  If the function returns a 4, the process
   --does not own the lock handle it is requesting a lock on.  If the function returns
   --a 5, the lock handle is illegal.
   --
   ---
   if ERROR_HANDLING (O_error_message,
                      I_seq_type,
                      L_call_status) = FALSE then
      return FALSE;
   end if;
   ---
   --This internal function is used to find the starting (nextval) of the appropriate
   --sequence.
   ---
   if GET_STARTVAL(O_error_message,
                   O_starting_value,
                   I_seq_type) = FALSE then
      return FALSE;
   end if;
   ---
   --This internal function reserves any other values needed beyond the sequences
   --increment by and calculates the value reserved through.
   ---
   if RESERVE_REST(O_error_message,
                   O_reserved_through,
                   I_seq_type,
                   O_starting_value,
                   I_values_needed) = FALSE then
      return FALSE;
   end if;
   ---
   L_call_status := DBMS_LOCK.RELEASE(L_lockhandle);
   ---
   if ERROR_HANDLING (O_error_message,
                      I_seq_type,
                      L_call_status) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SEQ;
------------------------------------------------------------------
FUNCTION AVAILABLE_ERROR_NO (O_error_message      IN OUT VARCHAR2,
                             O_starting_value        OUT NUMBER,
                             O_curr_reserved_thru    OUT NUMBER,
                             I_values_needed      IN     NUMBER)
   RETURN BOOLEAN IS

L_program           VARCHAR2(60) := 'SA_SEQUENCE_SQL.AVAILABLE_ERROR_NO';
L_seq_reserved_thru SA_ERROR.ERROR_SEQ_NO%TYPE  := NULL;

BEGIN
   /* see if we have enough left to allocate from current block */


   if (GP_seq_numleft - I_values_needed < 0) then
   /*  if not enough numbers, get another block of sequence numbers*/ 
   
      if SA_SEQUENCE_SQL.GET_SEQ( O_error_message,
                                  O_starting_value,
                                  L_seq_reserved_thru,
                                  I_values_needed,
                                  'SA_ERROR_SEQ_NO_SEQUENCE' ) = FALSE then
         return FALSE; 
      end if;
      
         /* all the variables get reset with the new values */
      GP_seq_numleft := L_seq_reserved_thru - (O_starting_value + (I_values_needed - 1));
      O_curr_reserved_thru := O_starting_value + (I_values_needed - 1);
      GP_seq_num := O_starting_value + I_values_needed;
   else
      O_starting_value := GP_seq_num;
      O_curr_reserved_thru := O_starting_value + (I_values_needed - 1);
      GP_seq_num := O_starting_value + I_values_needed;
      GP_seq_numleft := GP_seq_numleft - I_values_needed;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END AVAILABLE_ERROR_NO;
------------------------------------------------------------------
END SA_SEQUENCE_SQL;
/
