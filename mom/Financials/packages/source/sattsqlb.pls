
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY TENDER_TYPE_SQL AS
--------------------------------------------------------------------------------
FUNCTION GET_TENDER_TYPE_DESC(O_ERROR_MESSAGE     IN OUT VARCHAR2,
                              O_TENDER_TYPE_DESC  IN OUT POS_TENDER_TYPE_HEAD.TENDER_TYPE_DESC%TYPE,
                              I_TENDER_TYPE_GROUP IN     POS_TENDER_TYPE_HEAD.TENDER_TYPE_GROUP%TYPE,
                              I_TENDER_TYPE_ID    IN     POS_TENDER_TYPE_HEAD.TENDER_TYPE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'TENDER_TYPE_SQL.GET_TENDER_TYPE_DESC';

   cursor C_GET_TENDER_TYPE_DESC is
      select tender_type_desc
        from pos_tender_type_head
       where tender_type_group = I_tender_type_group
         and tender_type_id    = I_tender_type_id;

BEGIN

   if I_tender_type_group is NOT NULL and I_tender_type_id is NOT NULL then
      SQL_LIB.SET_MARK('OPEN','C_TENDER_TYPE_DESC','POS_TENDER_TYPE_HEAD',
                       'tender_type_group: '||I_tender_type_group||', tender_type_id: '||to_char(I_tender_type_id));
      open C_GET_TENDER_TYPE_DESC;
      SQL_LIB.SET_MARK('FETCH','C_TENDER_TYPE_DESC','POS_TENDER_TYPE_HEAD',
                       'tender_type_group: '||I_tender_type_group||', tender_type_id: '||to_char(I_tender_type_id));
      fetch C_GET_TENDER_TYPE_DESC into O_tender_type_desc;
      if C_GET_TENDER_TYPE_DESC%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TEND_TYPE_ID',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_TENDER_TYPE_DESC','POS_TENDER_TYPE_HEAD',
                       'tender_type_group: '||I_tender_type_group||', tender_type_id: '||to_char(I_tender_type_id));
      close C_GET_TENDER_TYPE_DESC;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_TENDER_TYPE_DESC;      
---------------------------------------------------------------------------------
END TENDER_TYPE_SQL;
/