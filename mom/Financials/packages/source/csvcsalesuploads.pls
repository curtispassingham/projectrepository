CREATE OR REPLACE PACKAGE CORESVC_SALES_UPLOAD_SQL AS
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_SALES
-- Purpose      : This is the main function of this package. Invoked by salesprocess.ksh 
--                this will perform the main processing for the item store sales
--                information uploaded from main POSU input file.
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SALES(I_sales_process_id  IN         NUMBER,
                       I_chunk_id          IN         NUMBER,
                       O_error_message     IN OUT     NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
-- Function Name: PURGE_ARCHIVE
-- Purpose      : This function will purge the records from SVC_POSUPLD_LOAD_ARCHIVE table. 
--                Input parameter is the retention days. --                
-------------------------------------------------------------------------------------------------------

FUNCTION PURGE_ARCHIVE(I_num_days          IN         NUMBER,                      
                       O_error_message     IN OUT     NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: ARCHIVE_RECORDS
-- Purpose      : This function will archive the records for the successfully processed files. --                
-------------------------------------------------------------------------------------------------------

FUNCTION ARCHIVE_RECORDS(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_FHEAD
-- Purpose      : Verifies if sales input file (POSU) has valid number of records 
--                based on FTAIL entry.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FHEAD(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result                IN OUT NUMBER,
                        I_sales_process_id      IN     NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_FTAIL
-- Purpose      : Verifies if sales input file (POSU) has valid number of records 
--                based on FTAIL entry.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FTAIL(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result                IN OUT NUMBER,
                        I_sales_process_id      IN     NUMBER)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------

-- Function Name: GENERATE_REJ_FILE
-- Purpose      : Writes rejected records in svc_posupld_rej_recs
----------------------------------------------------------------------------------------------------
FUNCTION GENERATE_REJ_FILE(I_sales_process_id      IN     NUMBER,
                           O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------

-- Function Name: CHUNK_STORE
-- Purpose      : Divides the data on svc_posupload_staging into chunks based on the location passed in.
----------------------------------------------------------------------------------------------------
FUNCTION CHUNK_STORE(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                     I_location              IN            STORE.STORE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Function Name: RANGE_ITEM_LOC
-- Purpose      : Writes missing item-locs to ITEM_LOC table
----------------------------------------------------------------------------------------------------
FUNCTION RANGE_ITEM_LOC(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
$if $$UTPLSQL=TRUE $then
   ----------------------------------------------------------------------------------------------------
   -- Function Name: SETUP_WORK_TABLE
   -- Purpose      : Populates the helper tables by parsing values loaded on the 
   --                SVC_POSUPLD_LOAD table from POSU file
   --
   --                Additional processing are as following
   --                     (target table: SVC_POSUPLD_LINE_ITEM):                 
   --                   * For pack items -  this will explode these items into its component 
   --                     items and populate corresponding columns accordingly (i.e Pack no, 
   --                     pack qty)
   --                   * For transformable sellabe items - the corresponding transformable
   --                     orderable items will be selected to populate the xform related 
   --                     columns (i.e. xform_sellable_item, xform_prod_loss_pct,
   --                     xform_yield_pct
   --                   * Default initil values that will be computed later by subsequent
   --                     function calls.
   ----------------------------------------------------------------------------------------------------
   
   FUNCTION SETUP_WORK_TABLE(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id      IN     NUMBER,
                             I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: SETUP_DISC_WORK_TABLES
   -- Purpose      : Populates the helper table for item discount information. 
   --                discount value taken from TDETL rows.
   --
   --                Additional processing are as following
   --                     (target table: SVC_POSUPLD_LINE_ITEM_DISC table ):                 
   --                   * Promotional records are validated againsts RPM tables - RPM_PROMO_COMP.
  
   ----------------------------------------------------------------------------------------------------
   FUNCTION SETUP_DISC_WORK_TABLE(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_sales_process_id      IN     NUMBER,
                                  I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: WORK_TABLE_DENORMALIZE
   -- Purpose      : Populates the columns at the SVC_POSUPLD_LINE_ITEM containing 
   --                additional information from various lookup tables
   --               (i.e. item_master, dept, class).
   ----------------------------------------------------------------------------------------------------
   FUNCTION WORK_TABLE_DENORMALIZE(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id      IN     NUMBER,
                                   I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: NON_FATAL_ERRORS_CHECK
   -- Purpose      : This function will check for errors on the POSU file with regards to format  
   --                and structure based on the expected input file format.
   ----------------------------------------------------------------------------------------------------
   FUNCTION NON_FATAL_ERRORS_CHECK(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id      IN     NUMBER,
                                   I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: IL_NON_FATAL_ERRORS_CHECK
   -- Purpose      : This function will check invalid item loc ranging for POSU line items.
   ----------------------------------------------------------------------------------------------------
   FUNCTION IL_NON_FATAL_ERRORS_CHECK(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id      IN     NUMBER,
                                      I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: CATCHWEIGHT_CONVERT
   -- Purpose      : This function will convert catchweight actual selling quantity and UOM to 
   --                be used to populate the POSU line item that is one of the staging
   --                tables for POSU processing.
   ----------------------------------------------------------------------------------------------------
   FUNCTION CATCHWEIGHT_CONVERT(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id      IN     NUMBER,
                                I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: XFORM_HELPERS
   -- Purpose      : This function is used to compute the prorated sales and unit quantity and 
   --                get the corresponding transformed orderable items for every transformable
   --                selling item on the POSU file.
   --                Important to note that there are 2 values that are being used to compute
   --                the prorated retail and quantity posted against the transformable orderable 
   --                item compared to the transformable sellable item.  
   --	              * yield_from_head_item_pct (ITEM_XFORM_DETAIL) - if a sellable only item 
   --                      is transformed from multiple orderable items, this will contain the 
   --                      percentage of the yield (retail) of the sellable item 
   --                      (ITEM_XFORM_DETAIL.DETAIL_ITEM) that is prorated and used to post the 
   --                      retail posted against the orderable item.  If this field has a NULL value,
   --                      the sellable item is only associate with a single orderable item.             
   --	             *  production_loss_pct (item_xform_head) - percentage value of wastage of the 
   --                      orderable transformable item  (item_xform_head.head_item)during the manufacture 
   --                      of the sellable items attached to it.  This is used to compute the inventory 
   --                      quantity that should be posted on the orderable item when a sellable item has 
   --                      been released from a store. This percentage is the value that should be removed 
   --                      from the total stock on hand quantity of the item.
   --                       * The units posted for the orderable items  are computed as follows:
   --                       	Yield_from_ head Pct * Sales Quantity / 1 - production_loss_pct 
   ----------------------------------------------------------------------------------------------------
   FUNCTION XFORM_HELPERS(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                          I_sales_process_id      IN     NUMBER,
                          I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: UOM_CONVERT_QTY
   -- Purpose      : Gets the standard UOM and if standard uom <> selling UOM performs
   --                UOM conversion.
   ----------------------------------------------------------------------------------------------------
   FUNCTION UOM_CONVERT_QTY(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sales_process_id      IN     NUMBER,
                            I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: RANGE_ITEM_LOC
   -- Purpose      : This function will invoke the NEW_ITEM_LOC function to associate
   --                the item to the selling store if the item/loc relationship is not
   --                setup.
   ----------------------------------------------------------------------------------------------------   
   FUNCTION RANGE_ITEM_LOC(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                           I_sales_process_id      IN     NUMBER,
                           I_chunk_id              IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   
   -- Function Name: VALIDATE_FTAIL
   -- Purpose      : Verifies if sales input file (POSU) has valid number of records 
   --                based on FTAIL entry.
   ----------------------------------------------------------------------------------------------------
   FUNCTION VALIDATE_FTAIL(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result                IN OUT NUMBER,
                           I_sales_process_id      IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   
   -- Function Name: GENERATE_REJ_FILE
   -- Purpose      : Writes rejected records in svc_posupld_rej_recs
   ----------------------------------------------------------------------------------------------------
   FUNCTION GENERATE_REJ_FILE(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sales_process_id      IN     NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
$end
----------------------------------------------------------------------------------------
END CORESVC_SALES_UPLOAD_SQL;
/
