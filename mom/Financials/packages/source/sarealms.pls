
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_REALM_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------------------
--- Function Name: GET_NEXT_ID
--- Purpose:       This function finds the next unique number value for realm ID.
--- Calls:         None
--- Created by:    Sonia Wong, 24-JUL-98
---
FUNCTION GET_NEXT_ID(O_error_message   IN OUT VARCHAR2,
                     O_realm_id        IN OUT sa_realm.realm_id%TYPE)
                     return BOOLEAN;
---
------------------------------------------------------------------------
--- Function Name: GET_MAX_REALM_ID
--- Purpose:       This function will get the updated realm_id if one exists.
---
FUNCTION GET_MAX_REALM_ID(O_error_message     IN OUT  VARCHAR2,
                          O_new_realm_id      IN OUT  SA_REALM.REALM_ID%TYPE,
                          I_original_realm_id IN      SA_REALM.REALM_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------
END SA_REALM_SQL;
/
