
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STKLEDGR_TRAN_DATA_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------
-- Function Name: GET_TRAN_DESC
-- Purpose	: This function gets the description on the tran_data_code
--		  table for a given code.
-- Called by	: 
-- Calls	: <none>
-- Input Value	: O_error_message,
--		        I_code
--		        O_desc
--
-- Created	: 31-JUL-96 by Matt Sniffen
---------------------------------------------------------------------------
   FUNCTION GET_TRAN_DESC ( O_error_message      IN OUT	VARCHAR2,
                            I_code    	       IN    	NUMBER,
                            O_desc	             IN OUT	VARCHAR2)
			return BOOLEAN;
--------------------------------------------------------------------------------
-- Procedure : GET_REF_DESC
-- Created by: Bruce Van
-- Created on: April 10, 1997
-- Purpose   : Retrieve ref_no_1, ref_no_2 and gl_ref_no descriptions from tran_data_codes_ref
--             table given the I_tran_code and I_pgm_name.
--------------------------------------------------------------------------------
FUNCTION GET_REF_DESC( O_error_message     IN OUT VARCHAR2,
                       O_ref_no_1_desc     IN OUT tran_data_codes_ref.ref_no_1_desc%TYPE,
                       O_ref_no_1_desc_tl  IN OUT tran_data_codes_ref.ref_no_1_desc%TYPE,
                       O_ref_no_2_desc     IN OUT tran_data_codes_ref.ref_no_2_desc%TYPE,
                       O_ref_no_2_desc_tl  IN OUT tran_data_codes_ref.ref_no_2_desc%TYPE,
                       O_gl_ref_no_desc    IN OUT tran_data_codes_ref.gl_ref_no_desc%TYPE,
                       O_gl_ref_no_desc_tl IN OUT tran_data_codes_ref.gl_ref_no_desc%TYPE,
                       I_tran_code         IN     tran_data_codes_ref.tran_code%TYPE,
                       I_pgm_name          IN     tran_data_codes_ref.pgm_name%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------------
END STKLEDGR_TRAN_DATA_SQL;
/


