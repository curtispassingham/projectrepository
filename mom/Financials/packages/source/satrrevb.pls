
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY TRANSACTION_REV_SQL AS
----------------------------------------------------------------------------------------
FUNCTION CHECK_DISC_REV_RECS(O_error_message   IN OUT VARCHAR2,
                             O_discount_exists IN OUT BOOLEAN,
                             I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_item_seq_no     IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                             I_rev_no          IN     SA_TRAN_ITEM_REV.REV_NO%TYPE,
                             I_store           IN     SA_TRAN_HEAD.STORE%TYPE,
                             I_day             IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS 

   L_program  VARCHAR2(60)            := 'TRANSACTION_SQL.CHECK_DISC_REV_RECS';
   L_exists   VARCHAR2(1)             := 'N';
   L_store    SA_STORE_DAY.STORE%TYPE := I_store;
   L_day      SA_STORE_DAY.DAY%TYPE   := I_day;

   cursor C_DISCOUNTS_EXIST is
      select 'Y'
        from sa_tran_disc_rev
       where tran_seq_no = I_tran_seq_no
         and store       = L_store
         and day         = L_day
         and item_seq_no = I_item_seq_no
         and rev_no      = I_rev_no;

BEGIN
   if I_tran_seq_no is not NULL and I_item_seq_no is not NULL and I_rev_no is not NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_discount_exists := FALSE;
      ---
      SQL_LIB.SET_MARK('OPEN','C_DISCOUNTS_EXIST','SA_TRAN_DISC_REV','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
      open C_DISCOUNTS_EXIST;
      SQL_LIB.SET_MARK('FETCH','C_DISCOUNTS_EXIST','SA_TRAN_DISC_REV','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
      fetch C_DISCOUNTS_EXIST into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_DISCOUNTS_EXIST','SA_TRAN_DISC_REV','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
      close C_DISCOUNTS_EXIST;
      ---
      if L_exists = 'Y' then
         O_discount_exists := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('TRAN_HEAD_ITEM_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DISC_REV_RECS;
--------------------------------------------------------------------
FUNCTION GET_TRAN_BALANCE(O_error_message IN OUT VARCHAR2,
                          O_balance       IN OUT NUMBER,
                          I_tran_seq_no   IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                          I_rev_no        IN     SA_TRAN_HEAD.REV_NO%TYPE,
                          I_store         IN     SA_TRAN_HEAD.STORE%TYPE,
                          I_day           IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN AS 

   L_program            VARCHAR2(60)                        := 'TRANSACTION_REV_SQL.GET_TRAN_BALANCE';
   L_item_amt           SA_TRAN_ITEM.UNIT_RETAIL%TYPE       := 0;
   L_tender_amt         SA_TRAN_TENDER.TENDER_AMT%TYPE      := 0;
   L_discount_amt       SA_TRAN_DISC.UNIT_DISCOUNT_AMT%TYPE := 0;
   L_tax_amt            SA_TRAN_TAX.TAX_AMT%TYPE            := 0;
   L_store              SA_STORE_DAY.STORE%TYPE             := I_store;
   L_day                SA_STORE_DAY.DAY%TYPE               := I_day;

   cursor C_GET_ITEM_AMT is
      select NVL(sum(uom_quantity * unit_retail),0)
        from SA_TRAN_ITEM_REV
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day
         and rev_no = I_rev_no;

   cursor C_GET_TENDER is
      select NVL(sum(tender_amt),0)
        from SA_TRAN_TENDER_REV
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day
         and rev_no = I_rev_no;

   cursor C_GET_DISCOUNT is
      select NVL(sum(uom_quantity * unit_discount_amt),0)
        from SA_TRAN_DISC_REV
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day
         and rev_no = I_rev_no;

   cursor C_GET_TAX is
      select NVL(sum(tax_amt),0)
        from SA_TRAN_TAX_REV
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day
         and rev_no = I_rev_no;

BEGIN
   if I_tran_seq_no is NULL or I_rev_no is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
       return FALSE;
   else
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      open C_GET_ITEM_AMT;
      fetch C_GET_ITEM_AMT into L_item_amt;
      close C_GET_ITEM_AMT;
      open C_GET_TENDER;
      fetch C_GET_TENDER into L_tender_amt;
      close C_GET_TENDER;
      open C_GET_DISCOUNT;
      fetch C_GET_DISCOUNT into L_discount_amt;
      close C_GET_DISCOUNT;
      open C_GET_TAX;
      fetch C_GET_TAX into L_tax_amt;
      close C_GET_TAX;
      O_balance := L_tender_amt - ((L_item_amt - L_discount_amt) + L_tax_amt);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TRAN_BALANCE;
---------------------------------------------------------------------------------------------
FUNCTION GET_ERROR_REV(O_error_message      IN OUT VARCHAR2,
                       O_error_desc         IN OUT SA_ERROR_CODES.ERROR_DESC%TYPE,
                       I_tran_seq_no        IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_key_value_1        IN     SA_ERROR.KEY_VALUE_1%TYPE,
                       I_key_value_2        IN     SA_ERROR.KEY_VALUE_2%TYPE,
                       I_rec_type           IN     SA_ERROR.REC_TYPE%TYPE,
                       I_rev_no             IN     SA_ERROR_REV.REV_NO%TYPE,
                       I_store              IN     SA_TRAN_HEAD.STORE%TYPE,
                       I_day                IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60)            := 'TRANSACTION_REV_SQL.GET_ERROR_REV';
   L_error_code   SA_ERROR_CODES.ERROR_CODE%TYPE;
   L_store        SA_STORE_DAY.STORE%TYPE := I_store;
   L_day          SA_STORE_DAY.DAY%TYPE   := I_day;
   
   cursor C_GET_ERROR_CODE is
      select error_code
        from sa_error_rev
       where tran_seq_no  = I_tran_seq_no
         and store        = L_store
         and day          = L_day
         and total_seq_no is NULL
         and key_value_1  = I_key_value_1
         and (key_value_2 = I_key_value_2
          or (key_value_2 is NULL
              and I_key_value_2 is NULL))
         and rec_type     = I_rec_type
         and rev_no       = I_rev_no;

BEGIN
   if I_tran_seq_no is NOT NULL and I_key_value_1 is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_ERROR_CODE','SA_ERROR_REV',NULL);
      open C_GET_ERROR_CODE;
      SQL_LIB.SET_MARK('FETCH','C_GET_ERROR_CODE','SA_ERROR_REV',NULL);
      fetch C_GET_ERROR_CODE into L_error_code;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ERROR_CODE','SA_ERROR_REV',NULL);
      close C_GET_ERROR_CODE;
      ---
      if L_error_code is not NULL then
         if SA_ERROR_SQL.GET_ERROR_DESC(O_error_message,
                                        O_error_desc,
                                        L_error_code) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ERROR_REV;
---------------------------------------------------------------------------------------

FUNCTION COUNT_REVISIONS(O_error_message    IN OUT VARCHAR2,
                         O_no_revisions     IN OUT NUMBER,
                         I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_store            IN     SA_TRAN_HEAD.STORE%TYPE,
                         I_day              IN     SA_TRAN_HEAD.DAY%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60)            := 'TRANSACTION_REV_SQL.COUNT_REVISIONS';
   L_store        SA_STORE_DAY.STORE%TYPE := I_store;
   L_day          SA_STORE_DAY.DAY%TYPE   := I_day;
   
   cursor C_GET_NO_REVS is
      select count(*)
        from sa_tran_head_rev
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day;

BEGIN
   if I_tran_seq_no is not NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---      
      SQL_LIB.SET_MARK('OPEN','C_GET_NO_REVS','SA_TRAN_HEAD_REV','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
      open C_GET_NO_REVS;
      SQL_LIB.SET_MARK('FETCH','C_GET_NO_REVS','SA_TRAN_HEAD_REV','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
      fetch C_GET_NO_REVS into O_no_revisions;
      SQL_LIB.SET_MARK('CLOSE','C_GET_NO_REVS','SA_TRAN_HEAD_REV','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
      close C_GET_NO_REVS;
      ---
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END COUNT_REVISIONS;
----------------------------------------------------------------------------------------
END TRANSACTION_REV_SQL;
/
