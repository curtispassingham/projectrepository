CREATE OR REPLACE PACKAGE BODY STKLEDGR_QUERY_SQL AS
--------------------------------------------------------------------------------
PROCEDURE INIT_VARS IS
BEGIN
   /* these variables will hold both local and primary values depending
      on the values of the currency_ind parameter.
   */

   v_purch_cost                := 0;
   v_freight_cost              := 0;
   v_rtv_cost                  := 0;
   v_net_sales_cost            := 0;
   v_cls_stk_cost              := 0;
   v_purch_retail              := 0;
   v_rtv_retail                := 0;
   v_markup_retail             := 0;
   v_markup_can_retail         := 0;
   v_clear_markdown_retail     := 0;
   v_perm_markdown_retail      := 0;
   v_prom_markdown_retail      := 0;
   v_weight_variance_retail    := 0;
   v_markdown_can_retail       := 0;
   v_net_sales_retail          := 0;
   v_net_sales_retail_ex_vat   := 0;
   v_cls_stk_retail            := 0;
   v_empl_disc_retail          := 0;
   v_stock_adj_cost            := 0;
   v_stock_adj_retail          := 0;
   v_stock_adj_cogs_cost       := 0;
   v_stock_adj_cogs_retail     := 0;
   v_up_chrg_amt_profit        := 0;
   v_up_chrg_amt_exp           := 0;
   v_tsf_in_cost               := 0;
   v_tsf_in_retail             := 0;
   v_tsf_in_book_cost          := 0;
   v_tsf_in_book_retail        := 0;
   v_tsf_out_cost              := 0;
   v_tsf_out_retail            := 0;
   v_tsf_out_book_cost         := 0;
   v_tsf_out_book_retail       := 0;
   v_reclass_in_cost           := 0;
   v_reclass_in_retail         := 0;
   v_reclass_out_cost          := 0;
   v_reclass_out_retail        := 0;
   v_returns_cost              := 0;
   v_returns_retail            := 0;
   v_shrinkage_cost            := 0;
   v_shrinkage_retail          := 0;
   v_workroom_amt              := 0;
   v_cash_disc_amt             := 0;
   v_gross_margin_amt          := 0;
   v_sales_units               := 0;
   v_cost_variance_amt         := 0;
   v_htd_gafs_cost             := 0;
   v_htd_gafs_retail           := 0;
   v_eom_date                  := NULL;
   v_eow_date                  := NULL;
   v_freight_claim_cost        := 0;
   v_freight_claim_retail      := 0;
   v_intercompany_in_retail    := 0;
   v_intercompany_in_cost      := 0;
   v_intercompany_out_retail   := 0;
   v_intercompany_out_cost     := 0;
   v_intercompany_markup       := 0;
   v_intercompany_markdown     := 0;
   v_wo_activity_upd_inv       := 0;
   v_wo_activity_post_fin      := 0;
   v_deal_income_sales         := 0;
   v_deal_income_purch         := 0;
   v_restocking_fee            := 0;
   v_margin_cost_variance      := 0;
   v_retail_cost_variance      := 0;
   v_rec_cost_adj_variance     := 0;
   v_franchise_sales_cost      := 0;
   v_franchise_returns_cost    := 0;
   v_franchise_returns_retail  := 0;
   v_franchise_markdown_retail := 0;
   v_franchise_sales_retail    := 0;
   v_franchise_markup_retail   := 0;
   v_franchise_restocking_fee  := 0;
   v_intercompany_margin       := 0;
   v_net_sales_non_inv_retail  := 0;
   v_net_sales_non_inv_cost    := 0;
   v_net_sales_non_inv_rtl_ex_vat  := 0;

END INIT_VARS;
--------------------------------------------------------------------------------
FUNCTION FETCH_MONTH_DATA_BULK(I_dept                       IN       NUMBER,
                               I_class                      IN       NUMBER,
                               I_subclass                   IN       NUMBER,
                               I_year_ind                   IN       VARCHAR2,
                               I_half                       IN       NUMBER,
                               I_loc_type                   IN       VARCHAR2,
                               I_location                   IN       NUMBER,
                               I_thousands                  IN       VARCHAR2,
                               I_currency_ind               IN       VARCHAR2,
                               I_set_of_books_id            IN       MONTH_DATA.SET_OF_BOOKS_ID%TYPE,
                               IO_month_data                IN OUT   OBJ_MONTH_DATA_TBL,
                               O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(100)   := 'STKLEDGR_QUERY_SQL.FETCH_MONTH_DATA_BULK';
   L_half                   MONTH_DATA.HALF_NO%TYPE;
   v_half                   MONTH_DATA.HALF_NO%TYPE;
   L_divisor                NUMBER(5)   := 1;
   L_ind                    system_options.calendar_454_ind%TYPE;
   L_opn_stk_cost           month_data.opn_stk_cost%TYPE       := 0;
   L_opn_stk_retail         month_data.opn_stk_retail%TYPE     := 0;
   L_cls_stk_cost           month_data.cls_stk_cost%TYPE       := 0;
   L_cls_stk_retail         month_data.cls_stk_retail%TYPE     := 0;
   L_shrinkage_cost         month_data.shrinkage_cost%TYPE     := 0;
   L_shrinkage_retail       month_data.shrinkage_retail%TYPE   := 0;
   L_gross_margin_amt       month_data.gross_margin_amt%TYPE   := 0;
   L_htd_gafs_cost          month_data.htd_gafs_cost%TYPE      := 0;
   L_htd_gafs_retail        month_data.htd_gafs_retail%TYPE    := 0;
   L_recoverable_tax        month_data.recoverable_tax%TYPE    := 0;
   L_opn_stk_cost_tot       month_data.opn_stk_cost%TYPE       := 0;
   L_opn_stk_retail_tot     month_data.opn_stk_retail%TYPE     := 0;
   L_cls_stk_cost_tot       month_data.cls_stk_cost%TYPE       := 0;
   L_cls_stk_retail_tot     month_data.cls_stk_retail%TYPE     := 0;
   L_shrinkage_cost_tot     month_data.shrinkage_cost%TYPE     := 0;
   L_shrinkage_retail_tot   month_data.shrinkage_retail%TYPE   := 0;
   L_gross_margin_amt_tot   month_data.gross_margin_amt%TYPE   := 0;
   L_htd_gafs_cost_tot      month_data.htd_gafs_cost%TYPE      := 0;
   L_htd_gafs_retail_tot    month_data.htd_gafs_retail%TYPE    := 0;
   L_recoverable_tax_tot    month_data.recoverable_tax%TYPE    := 0;
   L_loc                    month_data.location%TYPE           := NULL;
   L_loc_type               month_data.loc_type%TYPE           := NULL;
   L_dd                     NUMBER(3);
   L_mm                     NUMBER(3);
   L_yyyy                   NUMBER(5);
   v_dd                     NUMBER(3);
   v_mm                     NUMBER(3);
   v_yyyy                   NUMBER(5);
   L_return_code            VARCHAR2(5);
   L_FDOM                   DATE;
   L_eom_date               DATE;
   L_index                  BINARY_INTEGER;
   L_month_no               NUMBER   := 0;
   L_year                   NUMBER;
   L_half_no                NUMBER;
   L_count                  NUMBER   := 0;
   L_eom_start              DATE;
   L_eom_end                DATE;
   L_hdd                    NUMBER(3);
   L_hmm                    NUMBER(3);
   L_hyyyy                  NUMBER(5);
   L_fhdd                   NUMBER(3);
   L_fhmm                   NUMBER(3);
   L_fhyyyy                 NUMBER(5);

   cursor C_MONTH_LOCAL is
      select OBJ_MONTH_DATA_REC
             (dept,
             class,
             subclass,
             loc_type,
             location,
             half_no,
             month_no,
             eom_date,
             (nvl(sum(month_data.opn_stk_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.opn_stk_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.purch_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.freight_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.rtv_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.net_sales_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.cls_stk_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.purch_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.rtv_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.markup_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.markup_can_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.clear_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.perm_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.prom_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.weight_variance_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.markdown_can_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.net_sales_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.net_sales_retail_ex_vat),0)/L_divisor) ,
             (nvl(sum(month_data.cls_stk_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.empl_disc_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.stock_adj_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.stock_adj_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.stock_adj_cogs_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.stock_adj_cogs_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.up_chrg_amt_profit),0)/ L_divisor) ,
             (nvl(sum(month_data.up_chrg_amt_exp),0)/ L_divisor) ,
             (nvl(sum(month_data.tsf_in_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.tsf_in_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.tsf_in_book_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.tsf_in_book_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.tsf_out_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.tsf_out_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.tsf_out_book_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.tsf_out_book_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.reclass_in_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.reclass_in_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.reclass_out_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.reclass_out_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.returns_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.returns_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.shrinkage_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.shrinkage_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.workroom_amt),0)/ L_divisor) ,
             (nvl(sum(month_data.cash_disc_amt),0)/ L_divisor) ,
             (nvl(sum(month_data.gross_margin_amt),0)/ L_divisor) ,
             (nvl(sum(month_data.sales_units),0)/ L_divisor) ,
             (nvl(sum(month_data.cost_variance_amt),0)/ L_divisor) ,
             (nvl(sum(month_data.htd_gafs_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.htd_gafs_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.freight_claim_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.freight_claim_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.intercompany_in_retail),0)/L_divisor) ,
             (nvl(sum(month_data.intercompany_in_cost),0)/L_divisor) ,
             (nvl(sum(month_data.intercompany_out_retail),0)/L_divisor) ,
             (nvl(sum(month_data.intercompany_out_cost),0)/L_divisor) ,
             (nvl(sum(month_data.intercompany_markup),0)/L_divisor) ,
             (nvl(sum(month_data.intercompany_markdown),0)/L_divisor) ,
             (nvl(sum(month_data.wo_activity_upd_inv),0)/L_divisor) ,
             (nvl(sum(month_data.wo_activity_post_fin),0)/L_divisor) ,
             (nvl(sum(month_data.deal_income_sales),0)/L_divisor) ,
             (nvl(sum(month_data.deal_income_purch),0)/L_divisor) ,
             (nvl(sum(month_data.restocking_fee),0)/L_divisor) ,
             (nvl(sum(month_data.margin_cost_variance),0)/L_divisor) ,
             (nvl(sum(month_data.retail_cost_variance),0)/L_divisor) ,
             (nvl(sum(month_data.rec_cost_adj_variance),0)/L_divisor) ,
             (nvl(sum(month_data.franchise_sales_retail),0)/L_divisor) ,
             (nvl(sum(month_data.franchise_sales_cost),0)/L_divisor) ,
             (nvl(sum(month_data.franchise_returns_retail),0)/L_divisor) ,
             (nvl(sum(month_data.franchise_returns_cost),0)/L_divisor) ,
             (nvl(sum(month_data.franchise_markup_retail),0)/L_divisor) ,
             (nvl(sum(month_data.franchise_markdown_retail),0)/L_divisor) ,
             (nvl(sum(month_data.franchise_restocking_fee),0)/L_divisor) ,
             (nvl(sum(month_data.vat_in),0)/L_divisor) ,
             (nvl(sum(month_data.vat_out),0)/L_divisor) ,
             (nvl(sum(month_data.recoverable_tax),0)/L_divisor) ,
             (nvl(sum(month_data.intercompany_margin),0)/ L_divisor) ,
             (nvl(sum(month_data.net_sales_non_inv_retail),0)/ L_divisor) ,
             (nvl(sum(month_data.net_sales_non_inv_cost),0)/ L_divisor) ,
             (nvl(sum(month_data.net_sales_non_inv_rtl_ex_vat),0)/ L_divisor) 
             )
        from month_data
       where half_no = L_half
         and dept = nvl(I_dept,dept)
         and class = nvl(I_class,class)
         and subclass = nvl(I_subclass,subclass)
         and loc_type = nvl(I_loc_type,loc_type)
         and location = nvl(I_location,location)
         and currency_ind = I_currency_ind
         and eom_date between L_eom_start and L_eom_end
         and set_of_books_id = nvl(I_set_of_books_id,set_of_books_id)
       group by dept,class,subclass,loc_type,location,half_no, month_no, eom_date
       order by month_no;

   cursor C_MONTH_PRIMARY is
      select OBJ_MONTH_DATA_REC
             (m1.dept,
             m1.class,
             m1.subclass,
             m1.loc_type,
             m1.location,
             m1.half_no,
             m1.month_no,
             m1.eom_date,
             (nvl(sum(m2.opn_stk_cost),0)/ L_divisor) ,
             (nvl(sum(m2.opn_stk_retail),0)/ L_divisor) ,
             (nvl(sum(m1.purch_cost),0)/ L_divisor) ,
             (nvl(sum(m1.freight_cost),0)/ L_divisor) ,
             (nvl(sum(m1.rtv_cost),0)/ L_divisor) ,
             (nvl(sum(m1.net_sales_cost),0)/ L_divisor) ,
             (nvl(sum(m2.cls_stk_cost),0)/ L_divisor) ,
             (nvl(sum(m1.purch_retail),0)/ L_divisor) ,
             (nvl(sum(m1.rtv_retail),0)/ L_divisor) ,
             (nvl(sum(m1.markup_retail),0)/ L_divisor) ,
             (nvl(sum(m1.markup_can_retail),0)/ L_divisor) ,
             (nvl(sum(m1.clear_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(m1.perm_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(m1.prom_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(m1.weight_variance_retail),0)/ L_divisor) ,
             (nvl(sum(m1.markdown_can_retail),0)/ L_divisor) ,
             (nvl(sum(m1.net_sales_retail),0)/ L_divisor) ,
             (nvl(sum(m1.net_sales_retail_ex_vat), 0)/ L_divisor) ,
             (nvl(sum(m2.cls_stk_retail),0)/ L_divisor) ,
             (nvl(sum(m1.empl_disc_retail),0)/ L_divisor) ,
             (nvl(sum(m1.stock_adj_cost),0)/ L_divisor) ,
             (nvl(sum(m1.stock_adj_retail),0)/ L_divisor) ,
             (nvl(sum(m1.stock_adj_cogs_cost),0)/ L_divisor) ,
             (nvl(sum(m1.stock_adj_cogs_retail),0)/ L_divisor) ,
             (nvl(sum(m1.up_chrg_amt_profit),0)/ L_divisor) ,
             (nvl(sum(m1.up_chrg_amt_exp),0)/ L_divisor) ,
             (nvl(sum(m1.tsf_in_cost),0)/ L_divisor) ,
             (nvl(sum(m1.tsf_in_retail),0)/ L_divisor) ,
             (nvl(sum(m1.tsf_in_book_cost),0)/ L_divisor) ,
             (nvl(sum(m1.tsf_in_book_retail),0)/ L_divisor) ,
             (nvl(sum(m1.tsf_out_cost),0)/ L_divisor) ,
             (nvl(sum(m1.tsf_out_retail),0)/ L_divisor) ,
             (nvl(sum(m1.tsf_out_book_cost),0)/ L_divisor) ,
             (nvl(sum(m1.tsf_out_book_retail),0)/ L_divisor) ,
             (nvl(sum(m1.reclass_in_cost),0)/ L_divisor) ,
             (nvl(sum(m1.reclass_in_retail),0)/ L_divisor) ,
             (nvl(sum(m1.reclass_out_cost),0)/ L_divisor) ,
             (nvl(sum(m1.reclass_out_retail),0)/ L_divisor) ,
             (nvl(sum(m1.returns_cost),0)/ L_divisor) ,
             (nvl(sum(m1.returns_retail),0)/ L_divisor) ,
             (nvl(sum(m2.shrinkage_cost),0)/ L_divisor) ,
             (nvl(sum(m2.shrinkage_retail),0)/ L_divisor) ,
             (nvl(sum(m1.workroom_amt),0)/ L_divisor) ,
             (nvl(sum(m1.cash_disc_amt),0)/ L_divisor) ,
             (nvl(sum(m2.gross_margin_amt),0)/ L_divisor) ,
             (nvl(sum(m1.sales_units),0)/ L_divisor) ,
             (nvl(sum(m1.cost_variance_amt),0)/ L_divisor) ,
             (nvl(sum(m2.htd_gafs_cost),0)/ L_divisor) ,
             (nvl(sum(m2.htd_gafs_retail),0)/ L_divisor) ,
             (nvl(sum(m1.freight_claim_cost),0)/ L_divisor) ,
             (nvl(sum(m1.freight_claim_retail),0)/ L_divisor) ,
             (nvl(sum(m1.intercompany_in_retail),0)/L_divisor) ,
             (nvl(sum(m1.intercompany_in_cost),0)/L_divisor) ,
             (nvl(sum(m1.intercompany_out_retail),0)/L_divisor) ,
             (nvl(sum(m1.intercompany_out_cost),0)/L_divisor) ,
             (nvl(sum(m1.intercompany_markup),0)/L_divisor) ,
             (nvl(sum(m1.intercompany_markdown),0)/L_divisor) ,
             (nvl(sum(m1.wo_activity_upd_inv),0)/L_divisor) ,
             (nvl(sum(m1.wo_activity_post_fin),0)/L_divisor) ,
             (nvl(sum(m1.deal_income_sales),0)/L_divisor) ,
             (nvl(sum(m1.deal_income_purch),0)/L_divisor) ,
             (nvl(sum(m1.restocking_fee),0)/L_divisor) ,
             (nvl(sum(m1.margin_cost_variance),0)/L_divisor) ,
             (nvl(sum(m1.retail_cost_variance),0)/L_divisor) ,
             (nvl(sum(m1.rec_cost_adj_variance),0)/L_divisor) ,
             (nvl(sum(m1.franchise_sales_retail),0)/L_divisor) ,
             (nvl(sum(m1.franchise_sales_cost),0)/L_divisor) ,
             (nvl(sum(m1.franchise_returns_retail),0)/L_divisor) ,
             (nvl(sum(m1.franchise_returns_cost),0)/L_divisor) ,
             (nvl(sum(m1.franchise_markup_retail),0)/L_divisor) ,
             (nvl(sum(m1.franchise_markdown_retail),0)/L_divisor) ,
             (nvl(sum(m1.franchise_restocking_fee),0)/L_divisor) ,
             (nvl(sum(m1.vat_in),0)/L_divisor) ,
             (nvl(sum(m1.vat_out),0)/L_divisor) ,
             (nvl(sum(m1.recoverable_tax),0)/L_divisor) ,
             (nvl(sum(m1.intercompany_margin),0)/ L_divisor) ,
             (nvl(sum(m1.net_sales_non_inv_retail),0)/ L_divisor) ,
             (nvl(sum(m1.net_sales_non_inv_cost),0)/ L_divisor) ,
             (nvl(sum(m1.net_sales_non_inv_rtl_ex_vat),0)/ L_divisor) 
             )
        from month_data m1,
             month_data m2
       where m1.dept = m2.dept
         and m1.class = m2.class
         and m1.subclass = m2.subclass
         and m1.loc_type = m2.loc_type
         and m1.location = m2.location
         and m1.half_no = m2.half_no
         and m1.month_no = m2.month_no
         and m1.set_of_books_id = m2.set_of_books_id
         and m1.currency_ind = 'P'
         and m2.currency_ind = 'L'
         and m1.dept = nvl(I_dept,m1.dept)
         and m1.class = nvl(I_class,m1.class)
         and m1.subclass = nvl(I_subclass,m1.subclass)
         and m1.loc_type = nvl(I_loc_type,m1.loc_type)
         and m1.location = nvl(I_location,m1.location)
         and m1.half_no = L_half
         and m1.eom_date between L_eom_start and L_eom_end
         and m2.eom_date between L_eom_start and L_eom_end
         and m1.set_of_books_id = nvl(I_set_of_books_id,m1.set_of_books_id)
       group by m1.dept,m1.class,m1.subclass,m1.loc_type,m1.location,m1.half_no, m1.month_no, m1.eom_date
       order by m1.month_no;
       
   cursor C_MONTH_PRIMARY_ALL_LOCS is
      select loc_type,
             location,
             month_no,
             eom_date,
             (nvl(sum(month_data.opn_stk_cost),0)/ L_divisor) opn_stk_cost,
             (nvl(sum(month_data.opn_stk_retail),0)/ L_divisor) opn_stk_retail,
             (nvl(sum(month_data.cls_stk_cost),0)/ L_divisor) cls_stk_cost,
             (nvl(sum(month_data.cls_stk_retail),0)/ L_divisor) cls_stk_retail,
             (nvl(sum(month_data.shrinkage_cost),0)/ L_divisor) shrinkage_cost,
             (nvl(sum(month_data.shrinkage_retail),0)/ L_divisor) shrinkage_retail,
             (nvl(sum(month_data.gross_margin_amt),0)/ L_divisor) gross_margin_amt,
             (nvl(sum(month_data.htd_gafs_cost),0)/ L_divisor) htd_gafs_cost,
             (nvl(sum(month_data.htd_gafs_retail),0)/ L_divisor) htd_gafs_retail,
             (nvl(sum(month_data.recoverable_tax),0)/ L_divisor) recoverable_tax
        from month_data
       where half_no = L_half
         and dept = NVL(I_dept,dept)
         and class = NVL(I_class,class)
         and subclass = NVL(I_subclass,subclass)
         and currency_ind = 'L'
         and month_no = IO_month_data(L_index).month_no
         and eom_date = IO_month_data(L_index).eom_date
         and set_of_books_id = nvl(I_set_of_books_id,set_of_books_id)
       group by dept,class,subclass,loc_type, location, month_no, eom_date
       order by month_no;

   cursor C_454_ind is
      select calendar_454_ind
        from system_options;


BEGIN

   if I_year_ind = 'T' then
      L_half := I_half;
   else
      L_half := I_half - 10;
   end if;

   if I_thousands = 'Y' then
      L_divisor := 1000;
   end if;

   HALF_TO_454_FDOH(I_half,
                    L_fhdd,
                    L_fhmm,
                    L_fhyyyy,
                    L_return_code,
                    O_error_message);

   CAL_TO_454_LDOM(L_fhdd,
                   L_fhmm,
                   L_fhyyyy,
                   L_hdd,
                   L_hmm,
                   L_hyyyy,
                   L_return_code,
                   O_error_message);
   if L_return_code = 'TRUE' then
      L_eom_start := TO_DATE(TO_CHAR(L_hdd,'09')||TO_CHAR(L_hmm,'09')
                     ||TO_CHAR(L_hyyyy,'0999'),'DDMMYYYY');
   end if;

   HALF_TO_454_LDOH(I_half,
                    L_hdd,
                    L_hmm,
                    L_hyyyy,
                    L_return_code,
                    O_error_message);
   if L_return_code = 'TRUE' then
      L_eom_end := TO_DATE(TO_CHAR(L_hdd,'09')||TO_CHAR(L_hmm,'09')
                   ||TO_CHAR(L_hyyyy,'0999'),'DDMMYYYY');
   end if;


   if I_currency_ind = 'L' then
      open C_MONTH_LOCAL;
      fetch C_MONTH_LOCAL BULK COLLECT INTO IO_month_data;
      close C_MONTH_LOCAL;
   end if;


   if I_currency_ind = 'P' AND I_loc_type is NOT NULL AND I_location is NOT NULL then
      open C_MONTH_PRIMARY;
      fetch C_MONTH_PRIMARY BULK COLLECT INTO IO_month_data;
      close C_MONTH_PRIMARY;

      L_index := IO_month_data.FIRST;

      if L_index is NOT NULL then
         L_eom_date := v_eom_date;
         L_dd   := TO_NUMBER(TO_CHAR((L_eom_date),'DD'),'09');
         L_mm   := TO_NUMBER(TO_CHAR((L_eom_date),'MM'),'09');
         L_YYYY := TO_NUMBER(TO_CHAR((L_eom_date),'YYYY'),'0999');

         open C_454_ind;
         fetch C_454_ind into L_ind;
         close C_454_ind;

         if L_ind = 'C' then
            CAL_TO_CAL_FDOM(L_dd,
                            L_mm,
                            L_yyyy,
                            v_dd,
                            v_mm,
                            v_yyyy,
                            L_return_code,
                            O_error_message);
            if L_return_code = 'TRUE' then
               L_FDOM := TO_DATE(TO_CHAR(v_dd,'09')||TO_CHAR(v_mm,'09')
                         ||TO_CHAR(v_yyyy,'0999'),'DDMMYYYY');
            end if;
         else
            CAL_TO_454_FDOM(L_dd,
                            L_mm,
                            L_yyyy,
                            v_dd,
                            v_mm,
                            v_yyyy,
                            L_return_code,
                            O_error_message);
            if L_return_code = 'TRUE' then
               L_FDOM := TO_DATE(TO_CHAR(v_dd,'09')||TO_CHAR(v_mm,'09')||
                         TO_CHAR(v_yyyy,'0999'),'DDMMYYYY');
            end if;
         end if;
      end if;

      while L_index is NOT NULL LOOP
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).opn_stk_cost,
                                             IO_month_data(L_index).opn_stk_cost,
                                             'C',
                                             L_FDOM,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).opn_stk_retail,
                                             IO_month_data(L_index).opn_stk_retail,
                                             'R',
                                             L_FDOM,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).cls_stk_cost,
                                             IO_month_data(L_index).cls_stk_cost,
                                             'C',
                                             IO_month_data(L_index).eom_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).cls_stk_retail,
                                             IO_month_data(L_index).cls_stk_retail,
                                             'R',
                                             IO_month_data(L_index).eom_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).shrinkage_cost,
                                             IO_month_data(L_index).shrinkage_cost,
                                             'C',
                                             IO_month_data(L_index).eom_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).shrinkage_retail,
                                             IO_month_data(L_index).shrinkage_retail,
                                             'R',
                                             IO_month_data(L_index).eom_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).gross_margin_amt,
                                             IO_month_data(L_index).gross_margin_amt,
                                             'C',
                                             IO_month_data(L_index).eom_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).htd_gafs_retail,
                                             IO_month_data(L_index).htd_gafs_retail,
                                             'R',
                                             IO_month_data(L_index).eom_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).htd_gafs_cost,
                                             IO_month_data(L_index).htd_gafs_cost,
                                             'C',
                                             IO_month_data(L_index).eom_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_month_data(L_index).recoverable_tax,
                                             IO_month_data(L_index).recoverable_tax,
                                             'C',
                                             IO_month_data(L_index).eom_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         L_index := IO_month_data.NEXT(L_index);
      end LOOP;
   end if;


   if I_currency_ind = 'P' AND (I_loc_type is NULL OR I_location is NULL) then
      open C_MONTH_PRIMARY;
      fetch C_MONTH_PRIMARY BULK COLLECT INTO IO_month_data;
      close C_MONTH_PRIMARY;

      L_index := IO_month_data.FIRST;

      while L_index is NOT NULL LOOP
         open C_MONTH_PRIMARY_ALL_LOCS;
         LOOP
            fetch C_MONTH_PRIMARY_ALL_LOCS into L_loc_type,
                                                L_loc,
                                                L_month_no,
                                                v_eom_date,
                                                L_opn_stk_cost,
                                                L_opn_stk_retail,
                                                L_cls_stk_cost,
                                                L_cls_stk_retail,
                                                L_shrinkage_cost,
                                                L_shrinkage_retail,
                                                L_gross_margin_amt,
                                                L_htd_gafs_cost,
                                                L_htd_gafs_retail,
                                                L_recoverable_tax;
            if C_MONTH_PRIMARY_ALL_LOCS%NOTFOUND then
               Exit;
            else
               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_opn_stk_cost,
                                                   L_opn_stk_cost,
                                                   'C',
                                                   L_FDOM,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               L_opn_stk_cost_tot :=
                        L_opn_stk_cost_tot + nvl(L_opn_stk_cost,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_opn_stk_retail,
                                                   L_opn_stk_retail,
                                                   'R',
                                                   L_FDOM,
                                                   NULL) = FALSE then
                   return FALSE;
               end if;

               L_opn_stk_retail_tot :=
                           L_opn_stk_retail_tot + nvl(L_cls_stk_retail,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_cls_stk_cost,
                                                   L_cls_stk_cost,
                                                   'C',
                                                   v_eom_date,
                                                   NULL) = FALSE then
                   return FALSE;
               end if;

               L_cls_stk_cost_tot :=
                        L_cls_stk_cost_tot + nvl(L_cls_stk_cost,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_cls_stk_retail,
                                                   L_cls_stk_retail,
                                                   'R',
                                                   v_eom_date,
                                                   NULL) = FALSE then
                     return FALSE;
               end if;

               L_cls_stk_retail_tot :=
                           L_cls_stk_retail_tot + nvl(L_cls_stk_retail,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_shrinkage_cost,
                                                   L_shrinkage_cost,
                                                   'C',
                                                   v_eom_date,
                                                   NULL) = FALSE then
                     return FALSE;
               end if;

               L_shrinkage_cost_tot :=
                           L_shrinkage_cost_tot + nvl(L_shrinkage_cost,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_shrinkage_retail,
                                                   L_shrinkage_retail,
                                                   'R',
                                                   v_eom_date,
                                                   NULL) = FALSE then
                           return FALSE;
                     end if;

                     L_shrinkage_retail_tot :=
                             L_shrinkage_retail_tot + nvl(L_shrinkage_retail,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_gross_margin_amt,
                                                   L_gross_margin_amt,
                                                   'C',
                                                   v_eom_date,
                                                   NULL) = FALSE then
                     return FALSE;
               end if;

               L_gross_margin_amt_tot :=
                       L_gross_margin_amt_tot + nvl(L_gross_margin_amt,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_htd_gafs_retail,
                                                   L_htd_gafs_retail,
                                                   'R',
                                                   v_eom_date,
                                                   NULL) = FALSE then
                      return FALSE;
               end if;

               L_htd_gafs_retail_tot :=
                         L_htd_gafs_retail_tot + nvl(L_htd_gafs_retail,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_htd_gafs_cost,
                                                   L_htd_gafs_cost,
                                                   'C',
                                                   v_eom_date,
                                                   NULL) = FALSE then
                   return FALSE;
               end if;

               L_htd_gafs_cost_tot :=
                           L_htd_gafs_cost_tot + nvl(L_htd_gafs_cost,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_recoverable_tax,
                                                   L_recoverable_tax,
                                                   'C',
                                                   v_eom_date,
                                                   NULL) = FALSE then
                   return FALSE;
               end if;

               L_recoverable_tax_tot :=
                           L_recoverable_tax_tot + nvl(L_recoverable_tax,0);
            end if ;
         end LOOP;

         -- transfer total amounts to PL/SQL table
         IO_month_data(L_index).opn_stk_cost     := L_opn_stk_cost_tot;
         IO_month_data(L_index).opn_stk_retail   := L_opn_stk_retail_tot;
         IO_month_data(L_index).cls_stk_cost     := L_cls_stk_cost_tot;
         IO_month_data(L_index).cls_stk_retail   := L_cls_stk_retail_tot;
         IO_month_data(L_index).shrinkage_cost   := L_shrinkage_cost_tot;
         IO_month_data(L_index).shrinkage_retail := L_shrinkage_retail_tot;
         IO_month_data(L_index).gross_margin_amt := L_gross_margin_amt_tot;
         IO_month_data(L_index).htd_gafs_retail  := L_htd_gafs_retail_tot;
         IO_month_data(L_index).htd_gafs_cost    := L_htd_gafs_cost_tot;
         IO_month_data(L_index).recoverable_tax  := L_recoverable_tax_tot;

         -- reset totals
         L_opn_stk_cost_tot     := 0;
         L_opn_stk_retail_tot   := 0;
         L_cls_stk_cost_tot     := 0;
         L_cls_stk_retail_tot   := 0;
         L_shrinkage_cost_tot   := 0;
         L_shrinkage_retail_tot := 0;
         L_gross_margin_amt_tot := 0;
         L_htd_gafs_retail_tot  := 0;
         L_htd_gafs_cost_tot    := 0;
         L_recoverable_tax_tot  := 0;

         close C_MONTH_PRIMARY_ALL_LOCS;

         L_index := IO_month_data.NEXT(L_index);
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END FETCH_MONTH_DATA_BULK;
--------------------------------------------------------------------------------
FUNCTION FETCH_WEEK_DATA_BULK(I_dept                       IN       NUMBER,
                              I_class                      IN       NUMBER,
                              I_subclass                   IN       NUMBER,
                              I_year_ind                   IN       VARCHAR2,
                              I_month_date                 IN       DATE,
                              I_loc_type                   IN       VARCHAR2,
                              I_location                   IN       NUMBER,
                              I_thousands                  IN       VARCHAR2,
                              I_currency_ind               IN       VARCHAR2,
                              I_set_of_books_id            IN       WEEK_DATA.SET_OF_BOOKS_ID%TYPE,
                              IO_week_data                 IN OUT   OBJ_WEEK_DATA_TBL,
                              O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(100)   := 'STKLEDGR_QUERY_SQL.FETCH_WEEK_DATA_BULK';
   L_dd                     NUMBER(3);
   L_mm                     NUMBER(3);
   L_yyyy                   NUMBER(5);
   L_month                  WEEK_DATA.MONTH_NO%TYPE;
   L_half                   WEEK_DATA.HALF_NO%TYPE;
   return_code              VARCHAR2(5);
   v_half                   WEEK_DATA.HALF_NO%TYPE;
   L_divisor                NUMBER(5)                         := 1;
   L_opn_stk_cost           week_data.opn_stk_cost%TYPE       := 0;
   L_opn_stk_retail         week_data.opn_stk_retail%TYPE     := 0;
   L_cls_stk_cost           week_data.cls_stk_cost%TYPE       := 0;
   L_cls_stk_retail         week_data.cls_stk_retail%TYPE     := 0;
   L_shrinkage_cost         week_data.shrinkage_cost%TYPE     := 0;
   L_shrinkage_retail       week_data.shrinkage_retail%TYPE   := 0;
   L_gross_margin_amt       week_data.gross_margin_amt%TYPE   := 0;
   L_htd_gafs_cost          week_data.htd_gafs_cost%TYPE      := 0;
   L_htd_gafs_retail        week_data.htd_gafs_retail%TYPE    := 0;
   L_recoverable_tax        week_data.recoverable_tax%TYPE    := 0;
   L_opn_stk_cost_tot       week_data.opn_stk_cost%TYPE       := 0;
   L_opn_stk_retail_tot     week_data.opn_stk_retail%TYPE     := 0;
   L_cls_stk_cost_tot       week_data.cls_stk_cost%TYPE       := 0;
   L_cls_stk_retail_tot     week_data.cls_stk_retail%TYPE     := 0;
   L_shrinkage_cost_tot     week_data.shrinkage_cost%TYPE     := 0;
   L_shrinkage_retail_tot   week_data.shrinkage_retail%TYPE   := 0;
   L_gross_margin_amt_tot   week_data.gross_margin_amt%TYPE   := 0;
   L_htd_gafs_cost_tot      week_data.htd_gafs_cost%TYPE      := 0;
   L_htd_gafs_retail_tot    week_data.htd_gafs_retail%TYPE    := 0;
   L_recoverable_tax_tot    week_data.recoverable_tax%TYPE    := 0;
   L_loc                    week_data.location%TYPE           := NULL;
   L_loc_type               week_data.loc_type%TYPE           := NULL;
   L_index                  BINARY_INTEGER;
   L_eow_date               DATE;
   L_week_no                NUMBER   := 0;
   L_eow_start              DATE;
   L_eow_end                DATE;
   L_dd_454                 NUMBER   := 0;
   L_wk_454                 NUMBER   := 0;
   L_mm_454                 NUMBER   := 0;
   L_yyyy_454               NUMBER   := 0;

   cursor C_WEEK_LOCAL is
      select OBJ_WEEK_DATA_REC
             (dept,
             class,
             subclass,
             loc_type,
             location,
             half_no,
             week_no,
             eow_date,
             (nvl(sum(week_data.opn_stk_cost),0)/ L_divisor),
             (nvl(sum(week_data.opn_stk_retail),0)/ L_divisor),
             (nvl(sum(week_data.purch_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.freight_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.rtv_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.net_sales_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.cls_stk_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.purch_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.rtv_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.markup_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.markup_can_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.clear_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.perm_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.prom_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.weight_variance_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.markdown_can_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.net_sales_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.net_sales_retail_ex_vat), 0)/ L_divisor) ,
             (nvl(sum(week_data.cls_stk_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.empl_disc_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.stock_adj_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.stock_adj_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.stock_adj_cogs_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.stock_adj_cogs_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.up_chrg_amt_profit),0)/ L_divisor) ,
             (nvl(sum(week_data.up_chrg_amt_exp),0)/ L_divisor) ,
             (nvl(sum(week_data.tsf_in_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.tsf_in_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.tsf_in_book_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.tsf_in_book_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.tsf_out_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.tsf_out_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.tsf_out_book_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.tsf_out_book_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.reclass_in_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.reclass_in_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.reclass_out_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.reclass_out_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.returns_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.returns_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.shrinkage_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.shrinkage_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.workroom_amt),0)/ L_divisor) ,
             (nvl(sum(week_data.cash_disc_amt),0)/ L_divisor) ,
             (nvl(sum(week_data.gross_margin_amt),0)/ L_divisor) ,
             (nvl(sum(week_data.sales_units),0)/ L_divisor) ,
             (nvl(sum(week_data.cost_variance_amt),0)/ L_divisor) ,
             (nvl(sum(week_data.htd_gafs_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.htd_gafs_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.freight_claim_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.freight_claim_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.intercompany_in_retail),0)/L_divisor) ,
             (nvl(sum(week_data.intercompany_in_cost),0)/L_divisor) ,
             (nvl(sum(week_data.intercompany_out_retail),0)/L_divisor) ,
             (nvl(sum(week_data.intercompany_out_cost),0)/L_divisor) ,
             (nvl(sum(week_data.intercompany_markup),0)/L_divisor) ,
             (nvl(sum(week_data.intercompany_markdown),0)/L_divisor) ,
             (nvl(sum(week_data.wo_activity_upd_inv),0)/L_divisor) ,
             (nvl(sum(week_data.wo_activity_post_fin),0)/L_divisor) ,
             (nvl(sum(week_data.deal_income_sales),0)/L_divisor) ,
             (nvl(sum(week_data.deal_income_purch),0)/L_divisor) ,
             (nvl(sum(week_data.restocking_fee),0)/L_divisor) ,
             (nvl(sum(week_data.margin_cost_variance),0)/L_divisor) ,
             (nvl(sum(week_data.retail_cost_variance),0)/L_divisor) ,
             (nvl(sum(week_data.rec_cost_adj_variance),0)/L_divisor) ,
             (nvl(sum(week_data.franchise_sales_retail),0)/L_divisor) ,
             (nvl(sum(week_data.franchise_sales_cost),0)/L_divisor) ,
             (nvl(sum(week_data.franchise_returns_retail),0)/L_divisor) ,
             (nvl(sum(week_data.franchise_returns_cost),0)/L_divisor) ,
             (nvl(sum(week_data.franchise_markup_retail),0)/L_divisor) ,
             (nvl(sum(week_data.franchise_markdown_retail),0)/L_divisor) ,
             (nvl(sum(week_data.franchise_restocking_fee),0)/L_divisor) ,
             (nvl(sum(week_data.vat_in),0)/L_divisor) ,
             (nvl(sum(week_data.vat_out),0)/L_divisor) ,
             (nvl(sum(week_data.recoverable_tax),0)/L_divisor) ,
             (nvl(sum(week_data.intercompany_margin),0)/ L_divisor) ,
             (nvl(sum(week_data.net_sales_non_inv_retail),0)/ L_divisor) ,
             (nvl(sum(week_data.net_sales_non_inv_cost),0)/ L_divisor) ,
             (nvl(sum(week_data.net_sales_non_inv_rtl_ex_vat), 0)/ L_divisor))
        from week_data
       where half_no = L_half
         and month_no = L_month
         and dept = nvl(I_dept,dept)
         and class = nvl(I_class,class)
         and subclass = nvl(I_subclass,subclass)
         and loc_type = nvl(I_loc_type,loc_type)
         and location = nvl(I_location,location)
         and currency_ind = I_currency_ind
         and eow_date between L_eow_start and I_month_date
         and set_of_books_id = nvl(I_set_of_books_id,set_of_books_id)
       group by dept,class,subclass,loc_type,location,half_no, week_no, eow_date
       order by week_no;

   cursor C_WEEK_PRIMARY is
      select OBJ_WEEK_DATA_REC
             (
             wk1.dept,
             wk1.class,
             wk1.subclass,
             wk1.loc_type,
             wk1.location,
             wk1.half_no,
             wk1.week_no,
             wk1.eow_date,
             (nvl(sum(wk2.opn_stk_cost),0)/ L_divisor) ,
             (nvl(sum(wk2.opn_stk_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.purch_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.freight_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.rtv_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.net_sales_cost),0)/ L_divisor) ,
             (nvl(sum(wk2.cls_stk_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.purch_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.rtv_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.markup_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.markup_can_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.clear_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.perm_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.prom_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.weight_variance_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.markdown_can_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.net_sales_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.net_sales_retail_ex_vat), 0)/ L_divisor) ,
             (nvl(sum(wk2.cls_stk_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.empl_disc_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.stock_adj_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.stock_adj_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.stock_adj_cogs_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.stock_adj_cogs_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.up_chrg_amt_profit),0)/ L_divisor) ,
             (nvl(sum(wk1.up_chrg_amt_exp),0)/ L_divisor) ,
             (nvl(sum(wk1.tsf_in_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.tsf_in_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.tsf_in_book_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.tsf_in_book_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.tsf_out_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.tsf_out_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.tsf_out_book_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.tsf_out_book_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.reclass_in_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.reclass_in_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.reclass_out_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.reclass_out_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.returns_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.returns_retail),0)/ L_divisor) ,
             (nvl(sum(wk2.shrinkage_cost),0)/ L_divisor) ,
             (nvl(sum(wk2.shrinkage_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.workroom_amt),0)/ L_divisor) ,
             (nvl(sum(wk1.cash_disc_amt),0)/ L_divisor) ,
             (nvl(sum(wk2.gross_margin_amt),0)/ L_divisor) ,
             (nvl(sum(wk1.sales_units),0)/ L_divisor) ,
             (nvl(sum(wk1.cost_variance_amt),0)/ L_divisor) ,
             (nvl(sum(wk2.htd_gafs_cost),0)/ L_divisor) ,
             (nvl(sum(wk2.htd_gafs_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.freight_claim_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.freight_claim_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.intercompany_in_retail),0)/L_divisor) ,
             (nvl(sum(wk1.intercompany_in_cost),0)/L_divisor) ,
             (nvl(sum(wk1.intercompany_out_retail),0)/L_divisor) ,
             (nvl(sum(wk1.intercompany_out_cost),0)/L_divisor) ,
             (nvl(sum(wk1.intercompany_markup),0)/L_divisor) ,
             (nvl(sum(wk1.intercompany_markdown),0)/L_divisor) ,
             (nvl(sum(wk1.wo_activity_upd_inv),0)/L_divisor) ,
             (nvl(sum(wk1.wo_activity_post_fin),0)/L_divisor) ,
             (nvl(sum(wk1.deal_income_sales),0)/L_divisor) ,
             (nvl(sum(wk1.deal_income_purch),0)/L_divisor) ,
             (nvl(sum(wk1.restocking_fee),0)/L_divisor) ,
             (nvl(sum(wk1.margin_cost_variance),0)/L_divisor) ,
             (nvl(sum(wk1.retail_cost_variance),0)/L_divisor) ,
             (nvl(sum(wk1.rec_cost_adj_variance),0)/L_divisor) ,
             (nvl(sum(wk1.franchise_sales_retail),0)/L_divisor) ,
             (nvl(sum(wk1.franchise_sales_cost),0)/L_divisor) ,
             (nvl(sum(wk1.franchise_returns_retail),0)/L_divisor) ,
             (nvl(sum(wk1.franchise_returns_cost),0)/L_divisor) ,
             (nvl(sum(wk1.franchise_markup_retail),0)/L_divisor) ,
             (nvl(sum(wk1.franchise_markdown_retail),0)/L_divisor) ,
             (nvl(sum(wk1.franchise_restocking_fee),0)/L_divisor) ,
             (nvl(sum(wk1.vat_in),0)/L_divisor) ,
             (nvl(sum(wk1.vat_out),0)/L_divisor) ,
             (nvl(sum(wk1.recoverable_tax),0)/L_divisor) ,
             (nvl(sum(wk1.intercompany_margin),0)/ L_divisor) ,
             (nvl(sum(wk1.net_sales_non_inv_retail),0)/ L_divisor) ,
             (nvl(sum(wk1.net_sales_non_inv_cost),0)/ L_divisor) ,
             (nvl(sum(wk1.net_sales_non_inv_rtl_ex_vat), 0)/ L_divisor) )
        from week_data wk1,
             week_data wk2
       where wk1.dept = wk2.dept
         and wk1.class = wk2.class
         and wk1.subclass = wk2.subclass
         and wk1.loc_type = wk2.loc_type
         and wk1.location = wk2.location
         and wk1.half_no = wk2.half_no
         and wk1.month_no = wk2.month_no
         and wk1.week_no = wk2.week_no
         and wk1.eow_date = wk2.eow_date
         and wk1.set_of_books_id = wk2.set_of_books_id
         and wk1.currency_ind = 'P'
         and wk2.currency_ind = 'L'
         and wk1.dept = nvl(I_dept,wk1.dept)
         and wk1.class = nvl(I_class,wk1.class)
         and wk1.subclass = nvl(I_subclass,wk1.subclass)
         and wk1.loc_type = nvl(I_loc_type,wk1.loc_type)
         and wk1.location = nvl(I_location,wk1.location)
         and wk1.half_no = L_half
         and wk1.month_no = L_month
         and wk1.eow_date between L_eow_start and I_month_date
         and wk2.eow_date between L_eow_start and I_month_date
         and wk1.set_of_books_id = nvl(I_set_of_books_id,wk1.set_of_books_id)
       group by wk1.dept, wk1.class, wk1.subclass, wk1.loc_type, wk1.location,wk1.half_no, wk1.week_no, wk1.eow_date
       order by wk1.week_no;

   cursor C_WEEK_PRIMARY_ALL_LOCS is
      select loc_type,
             location,
             week_no,
             eow_date,
             (nvl(sum(week_data.opn_stk_cost),0)/ L_divisor) opn_stk_cost,
             (nvl(sum(week_data.opn_stk_retail),0)/ L_divisor) opn_stk_retail,
             (nvl(sum(week_data.cls_stk_cost),0)/ L_divisor) cls_stk_cost,
             (nvl(sum(week_data.cls_stk_retail),0)/ L_divisor) cls_stk_retail,
             (nvl(sum(week_data.shrinkage_cost),0)/ L_divisor) shrinkage_cost,
             (nvl(sum(week_data.shrinkage_retail),0)/ L_divisor) shrinkage_retail,
             (nvl(sum(week_data.gross_margin_amt),0)/ L_divisor) gross_margin_amt,
             (nvl(sum(week_data.htd_gafs_cost),0)/ L_divisor) htd_gafs_cost,
             (nvl(sum(week_data.htd_gafs_retail),0)/ L_divisor) htd_gafs_retail,
             (nvl(sum(week_data.recoverable_tax),0)/ L_divisor) recoverable_tax
        from week_data
       where half_no = L_half
         and month_no = L_month
         and dept = nvl(I_dept,dept)
         and class = nvl(I_class,class)
         and subclass = nvl(I_subclass,subclass)
         and currency_ind = 'L'
         and week_no = IO_week_data(L_index).week_no
         and eow_date = IO_week_data(L_index).eow_date
         and set_of_books_id = nvl(I_set_of_books_id,set_of_books_id)
       group by loc_type, location, week_no, eow_date
       order by week_no;


   cursor C_eow_start is
   select first_day
        from  calendar
        where year_454 = L_yyyy_454
        and   month_454 = L_mm_454;


BEGIN
   L_dd   := TO_NUMBER(TO_CHAR(I_month_date,'DD'));
   L_mm   := TO_NUMBER(TO_CHAR(I_month_date,'MM'));
   L_yyyy := TO_NUMBER(TO_CHAR(I_month_date,'YYYY'));

   CAL_TO_454_HALF(L_dd,
                   L_mm,
                   L_yyyy,
                   L_half,
                   L_month,
                   return_code,
                   O_error_message);
   if return_code = 'TRUE' then
      NULL;
   else
      return FALSE;
   end if;

   CAL_TO_454 (L_dd,
               L_mm,
               L_yyyy,
               L_dd_454,
               L_wk_454,
               L_mm_454,
               L_yyyy_454,
               return_code,
               o_error_message);

   if return_code = 'TRUE' then
      NULL;
   else
      return FALSE;
   end if;

   if I_year_ind = 'L' then
      L_half := L_half - 10;
   end if;

   if I_thousands = 'Y' then
      L_divisor := 1000;
   end if;

   open C_eow_start;
   fetch C_eow_start into L_eow_start;
   close C_eow_start;

   if I_currency_ind = 'L' then
      open C_WEEK_LOCAL;
      fetch C_WEEK_LOCAL BULK COLLECT INTO IO_week_data;
      close C_WEEK_LOCAL;
   end if;

   if I_currency_ind = 'P' AND I_loc_type is NOT NULL AND I_location is NOT NULL then
      open C_WEEK_PRIMARY;
      fetch C_WEEK_PRIMARY BULK COLLECT INTO IO_week_data;
      close C_WEEK_PRIMARY;

      L_index := IO_week_data.FIRST;

      while L_index is NOT NULL LOOP
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_week_data(L_index).opn_stk_cost,
                                             IO_week_data(L_index).opn_stk_cost,
                                             'C',
                                             (IO_week_data(L_index).eow_date - 6),
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_week_data(L_index).opn_stk_retail,
                                             IO_week_data(L_index).opn_stk_retail,
                                             'R',
                                             (IO_week_data(L_index).eow_date - 6),
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_week_data(L_index).cls_stk_cost,
                                             IO_week_data(L_index).cls_stk_cost,
                                             'C',
                                             IO_week_data(L_index).eow_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_week_data(L_index).cls_stk_retail,
                                             IO_week_data(L_index).cls_stk_retail,
                                             'R',
                                             IO_week_data(L_index).eow_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_week_data(L_index).shrinkage_cost,
                                             IO_week_data(L_index).shrinkage_cost,
                                             'C',
                                             IO_week_data(L_index).eow_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_week_data(L_index).shrinkage_retail,
                                             IO_week_data(L_index).shrinkage_retail,
                                             'R',
                                             IO_week_data(L_index).eow_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_week_data(L_index).gross_margin_amt,
                                             IO_week_data(L_index).gross_margin_amt,
                                             'C',
                                             IO_week_data(L_index).eow_date,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             IO_week_data(L_index).htd_gafs_retail,
                                             IO_week_data(L_index).htd_gafs_retail,
                                             'R',
                                             IO_week_data(L_index).eow_date,
                                             NULL) = FALSE then
             return FALSE;
          end if;

          if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                              I_location,
                                              I_loc_type,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              IO_week_data(L_index).htd_gafs_cost,
                                              IO_week_data(L_index).htd_gafs_cost,
                                              'C',
                                              IO_week_data(L_index).eow_date,
                                              NULL) = FALSE then
             return FALSE;
          end if;

          if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                              I_location,
                                              I_loc_type,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              IO_week_data(L_index).recoverable_tax,
                                              IO_week_data(L_index).recoverable_tax,
                                              'C',
                                              IO_week_data(L_index).eow_date,
                                              NULL) = FALSE then
             return FALSE;
          end if;

          L_index := IO_week_data.NEXT(L_index);
      end LOOP;
   end if;

   if I_currency_ind = 'P' AND (I_loc_type is NULL OR I_location is NULL) then
      open C_WEEK_PRIMARY;
      fetch C_WEEK_PRIMARY BULK COLLECT INTO IO_week_data;
      close C_WEEK_PRIMARY;

      L_index := IO_week_data.FIRST;

      while L_index is NOT NULL LOOP
         open C_WEEK_PRIMARY_ALL_LOCS;
         LOOP
            fetch C_WEEK_PRIMARY_ALL_LOCS into L_loc_type,
                                               L_loc,
                                               L_week_no,
                                               v_eow_date,
                                               L_opn_stk_cost,
                                               L_opn_stk_retail,
                                               L_cls_stk_cost,
                                               L_cls_stk_retail,
                                               L_shrinkage_cost,
                                               L_shrinkage_retail,
                                               L_gross_margin_amt,
                                               L_htd_gafs_cost,
                                               L_htd_gafs_retail,
                                               L_recoverable_tax;
            if C_WEEK_PRIMARY_ALL_LOCS%NOTFOUND then
               Exit;
            else
               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_opn_stk_cost,
                                                   L_opn_stk_cost,
                                                   'C',
                                                   (v_eow_date-6),
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               L_opn_stk_cost_tot :=
                        L_opn_stk_cost_tot + nvl(L_opn_stk_cost,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_opn_stk_retail,
                                                   L_opn_stk_retail,
                                                   'R',
                                                   (v_eow_date-6),
                                                   NULL) = FALSE then
                 return FALSE;
               end if;

               L_opn_stk_retail_tot :=
                           L_opn_stk_retail_tot + nvl(L_cls_stk_retail,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_cls_stk_cost,
                                                   L_cls_stk_cost,
                                                   'C',
                                                   v_eow_date,
                                                   NULL) = FALSE then
                   return FALSE;
               end if;

               L_cls_stk_cost_tot :=
                        L_cls_stk_cost_tot + nvl(L_cls_stk_cost,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_cls_stk_retail,
                                                   L_cls_stk_retail,
                                                   'R',
                                                   v_eow_date,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               L_cls_stk_retail_tot :=
                           L_cls_stk_retail_tot + nvl(L_cls_stk_retail,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_shrinkage_cost,
                                                   L_shrinkage_cost,
                                                   'C',
                                                   v_eow_date,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               L_shrinkage_cost_tot :=
                           L_shrinkage_cost_tot + nvl(L_shrinkage_cost,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_shrinkage_retail,
                                                   L_shrinkage_retail,
                                                   'R',
                                                   v_eow_date,
                                                   NULL) = FALSE then
                     return FALSE;
               end if;

               L_shrinkage_retail_tot :=
                       L_shrinkage_retail_tot + nvl(L_shrinkage_retail,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_gross_margin_amt,
                                                   L_gross_margin_amt,
                                                   'C',
                                                   v_eow_date,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               L_gross_margin_amt_tot :=
                       L_gross_margin_amt_tot + nvl(L_gross_margin_amt,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_htd_gafs_retail,
                                                   L_htd_gafs_retail,
                                                   'R',
                                                   v_eow_date,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               L_htd_gafs_retail_tot :=
                         L_htd_gafs_retail_tot + nvl(L_htd_gafs_retail,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_htd_gafs_cost,
                                                   L_htd_gafs_cost,
                                                   'C',
                                                   v_eow_date,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               L_htd_gafs_cost_tot :=
                           L_htd_gafs_cost_tot + nvl(L_htd_gafs_cost,0);

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_loc,
                                                   L_loc_type,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_recoverable_tax,
                                                   L_recoverable_tax,
                                                   'C',
                                                   v_eow_date,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               L_recoverable_tax_tot :=
                           L_recoverable_tax_tot + nvl(L_recoverable_tax,0);

            end if;
         end LOOP;

         -- transfer total amounts to PL/SQL table
         IO_week_data(L_index).opn_stk_cost     := L_opn_stk_cost_tot;
         IO_week_data(L_index).opn_stk_retail   := L_opn_stk_retail_tot;
         IO_week_data(L_index).cls_stk_cost     := L_cls_stk_cost_tot;
         IO_week_data(L_index).cls_stk_retail   := L_cls_stk_retail_tot;
         IO_week_data(L_index).shrinkage_cost   := L_shrinkage_cost_tot;
         IO_week_data(L_index).shrinkage_retail := L_shrinkage_retail_tot;
         IO_week_data(L_index).gross_margin_amt := L_gross_margin_amt_tot;
         IO_week_data(L_index).htd_gafs_retail  := L_htd_gafs_retail_tot;
         IO_week_data(L_index).htd_gafs_cost    := L_htd_gafs_cost_tot;
         IO_week_data(L_index).recoverable_tax  := L_recoverable_tax_tot;

         -- reset totals
         L_opn_stk_cost_tot     := 0;
         L_opn_stk_retail_tot   := 0;
         L_cls_stk_cost_tot     := 0;
         L_cls_stk_retail_tot   := 0;
         L_shrinkage_cost_tot   := 0;
         L_shrinkage_retail_tot := 0;
         L_gross_margin_amt_tot := 0;
         L_htd_gafs_retail_tot  := 0;
         L_htd_gafs_cost_tot    := 0;
         L_recoverable_tax_tot  := 0;

         close C_WEEK_PRIMARY_ALL_LOCS;

         L_index := IO_week_data.NEXT(L_index);
      end LOOP;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END FETCH_WEEK_DATA_BULK;
--------------------------------------------------------------------------------
FUNCTION FETCH_DAILY_DATA_CAL_BULK(I_dept                       IN       NUMBER,
                                   I_class                      IN       NUMBER,
                                   I_subclass                   IN       NUMBER,
                                   I_year_ind                   IN       VARCHAR2,
                                   I_week_date                  IN       DATE,
                                   I_loc_type                   IN       VARCHAR2,
                                   I_location                   IN       NUMBER,
                                   I_thousands                  IN       VARCHAR2,
                                   I_currency_ind               IN       VARCHAR2,
                                   I_set_of_books_id            IN       DAILY_DATA.SET_OF_BOOKS_ID%TYPE,
                                   IO_daily_data_cal            IN OUT   OBJ_DAILY_DATA_TBL,
                                   O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(100)   := 'STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_CAL_BULK';
   L_dd          NUMBER(3);
   L_mm          NUMBER(3);
   L_yyyy        NUMBER(5);
   L_week        NUMBER(3);
   L_month       DAILY_DATA.MONTH_NO%TYPE;
   L_half        DAILY_DATA.HALF_NO%TYPE;
   return_code   VARCHAR2(5);
   v_half        DAILY_DATA.HALF_NO%TYPE;
   L_divisor     NUMBER(5) := 1;
   L_ind         SYSTEM_OPTIONS.CALENDAR_454_IND%TYPE;


   cursor C_DAILY_CAL is
      select OBJ_DAILY_DATA_REC
             (dept,
             class,
             subclass,
             loc_type,
             location,
             half_no,
             data_date,
             DECODE(day_no,NULL,abs((I_week_date - daily_data.data_date) -7),day_no) ,
             (NVL(SUM(daily_data.purch_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.freight_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.rtv_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.net_sales_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.purch_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.rtv_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.markup_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.markup_can_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.clear_markdown_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.perm_markdown_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.prom_markdown_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.weight_variance_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.markdown_can_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.net_sales_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.net_sales_retail_ex_vat), 0)/ L_divisor) ,
             (NVL(SUM(daily_data.empl_disc_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.stock_adj_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.stock_adj_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.stock_adj_cogs_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.stock_adj_cogs_retail), 0)/ L_divisor) ,
             (NVL(SUM(daily_data.up_chrg_amt_profit),0)/ L_divisor) ,
             (NVL(SUM(daily_data.up_chrg_amt_exp),0)/ L_divisor) ,
             (NVL(SUM(daily_data.tsf_in_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.tsf_in_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.tsf_in_book_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.tsf_in_book_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.tsf_out_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.tsf_out_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.tsf_out_book_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.tsf_out_book_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.reclass_in_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.reclass_in_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.reclass_out_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.reclass_out_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.returns_cost),0)/ L_divisor) ,
             (NVL(SUM(daily_data.returns_retail),0)/ L_divisor) ,
             (NVL(SUM(daily_data.workroom_amt),0)/ L_divisor) ,
             (NVL(SUM(daily_data.cash_disc_amt),0)/ L_divisor) ,
             (NVL(SUM(daily_data.sales_units),0)/ L_divisor) ,
             (NVL(SUM(daily_data.cost_variance_amt),0)/L_divisor) ,
             (NVL(SUM(daily_data.freight_claim_cost),0)/L_divisor) ,
             (NVL(SUM(daily_data.freight_claim_retail),0)/L_divisor) ,
             (NVL(SUM(daily_data.intercompany_in_retail),0)/L_divisor) ,
             (NVL(SUM(daily_data.intercompany_in_cost),0)/L_divisor) ,
             (NVL(SUM(daily_data.intercompany_out_retail),0)/L_divisor) ,
             (NVL(SUM(daily_data.intercompany_out_cost),0)/L_divisor) ,
             (NVL(SUM(daily_data.intercompany_markup),0)/L_divisor) ,
             (NVL(SUM(daily_data.intercompany_markdown),0)/L_divisor) ,
             (NVL(SUM(daily_data.wo_activity_upd_inv),0)/L_divisor) ,
             (NVL(SUM(daily_data.wo_activity_post_fin),0)/L_divisor) ,
             (NVL(SUM(daily_data.deal_income_sales),0)/L_divisor) ,
             (NVL(SUM(daily_data.deal_income_purch),0)/L_divisor) ,
             (NVL(SUM(daily_data.restocking_fee),0)/L_divisor) ,
             (NVL(SUM(daily_data.margin_cost_variance),0)/L_divisor) ,
             (NVL(SUM(daily_data.retail_cost_variance),0)/L_divisor) ,
             (NVL(SUM(daily_data.rec_cost_adj_variance),0)/L_divisor) ,
             (NVL(SUM(daily_data.franchise_sales_retail),0)/L_divisor) ,
             (NVL(SUM(daily_data.franchise_sales_cost),0)/L_divisor) ,
             (NVL(SUM(daily_data.franchise_returns_retail),0)/L_divisor) ,
             (NVL(SUM(daily_data.franchise_returns_cost),0)/L_divisor) ,
             (NVL(SUM(daily_data.franchise_markup_retail),0)/L_divisor) ,
             (NVL(SUM(daily_data.franchise_markdown_retail),0)/L_divisor) ,
             (NVL(SUM(daily_data.franchise_restocking_fee),0)/L_divisor) ,
             (NVL(SUM(daily_data.vat_in),0)/L_divisor) ,
             (NVL(SUM(daily_data.vat_out),0)/L_divisor) ,
             (NVL(SUM(daily_data.recoverable_tax),0)/L_divisor) ,
             (nvl(sum(daily_data.net_sales_non_inv_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.net_sales_non_inv_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.net_sales_non_inv_rtl_ex_vat), 0)/ L_divisor))
        from daily_data
       where data_date >= (I_week_date - 6)
         and data_date <= I_week_date
         and dept = NVL(I_dept,dept)
         and class = NVL(I_class,class)
         and subclass = NVL(I_subclass,subclass)
         and loc_type = NVL(I_loc_type,loc_type)
         and location = NVL(I_location,location)
         and currency_ind = I_currency_ind
         and set_of_books_id = NVL(I_set_of_books_id,set_of_books_id)
       group by dept,class,subclass,loc_type,location,half_no, data_date, day_no 
       order by day_no;

BEGIN

   L_dd   := to_number(to_char(I_week_date,'DD'));
   L_mm   := to_number(to_char(I_week_date,'MM'));
   L_yyyy := to_number(to_char(I_week_date,'YYYY'));

   CAL_TO_CAL_HALF(L_dd,
                   L_mm,
                   L_yyyy,
                   L_half,
                   L_month,
                   return_code,
                   o_error_message);
   if return_code = 'TRUE' then
      NULL;
   else
      return FALSE;
   end if;

   if I_year_ind = 'L' then
      L_half := L_half - 10;
   end if;

   if I_thousands = 'Y' then
      L_divisor := 1000;
   end if;

   open C_DAILY_CAL;
   fetch C_DAILY_CAL BULK COLLECT INTO IO_daily_data_cal;
   close C_DAILY_CAL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,'STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_CAL_BULK',SQLCODE);
      return FALSE;
END FETCH_DAILY_DATA_CAL_BULK;
--------------------------------------------------------------------------------
FUNCTION FETCH_DAILY_DATA_454_BULK(I_dept                      IN     NUMBER,
                                   I_class                     IN     NUMBER,
                                   I_subclass                  IN     NUMBER,
                                   I_year_ind                  IN     VARCHAR2,
                                   I_week_date                 IN     DATE,
                                   I_loc_type                  IN     VARCHAR2,
                                   I_location                  IN     NUMBER,
                                   I_thousands                 IN     VARCHAR2,
                                   I_currency_ind              IN     VARCHAR2,
                                   I_set_of_books_id           IN     DAILY_DATA.SET_OF_BOOKS_ID%TYPE,
                                   IO_daily_data_454           IN OUT OBJ_DAILY_DATA_TBL,
                                   O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100)   := 'STKLEDGR_QUERY_SQL.FETCH_DAILY_DATA_454_BULK';

   L_dd        NUMBER(3);
   L_mm        NUMBER(3);
   L_yyyy      NUMBER(5);
   L_week      NUMBER(3);
   L_month     DAILY_DATA.MONTH_NO%TYPE;
   L_half      DAILY_DATA.HALF_NO%TYPE;
   return_code VARCHAR2(5);
   v_half      DAILY_DATA.HALF_NO%TYPE;
   L_divisor   NUMBER(5) := 1;
   L_ind       SYSTEM_OPTIONS.CALENDAR_454_IND%TYPE;

   cursor C_DAILY_454 is
      select OBJ_DAILY_DATA_REC
             (dept,
             class,
             subclass,
             loc_type,
             location,
             half_no,
             data_date,
             day_no,
             (nvl(sum(daily_data.purch_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.freight_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.rtv_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.net_sales_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.purch_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.rtv_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.markup_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.markup_can_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.clear_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.perm_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.prom_markdown_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.weight_variance_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.markdown_can_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.net_sales_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.net_sales_retail_ex_vat), 0)/ L_divisor) ,
             (nvl(sum(daily_data.empl_disc_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.stock_adj_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.stock_adj_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.stock_adj_cogs_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.stock_adj_cogs_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.up_chrg_amt_profit),0)/ L_divisor) ,
             (nvl(sum(daily_data.up_chrg_amt_exp),0)/ L_divisor) ,
             (nvl(sum(daily_data.tsf_in_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.tsf_in_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.tsf_in_book_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.tsf_in_book_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.tsf_out_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.tsf_out_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.tsf_out_book_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.tsf_out_book_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.reclass_in_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.reclass_in_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.reclass_out_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.reclass_out_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.returns_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.returns_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.workroom_amt),0)/ L_divisor) ,
             (nvl(sum(daily_data.cash_disc_amt),0)/ L_divisor) ,
             (nvl(sum(daily_data.sales_units),0)/ L_divisor) ,
             (nvl(sum(daily_data.cost_variance_amt),0)/L_divisor) ,
             (nvl(sum(daily_data.freight_claim_cost),0)/L_divisor) ,
             (nvl(sum(daily_data.freight_claim_retail),0)/L_divisor) ,
             (nvl(sum(daily_data.intercompany_in_retail),0)/L_divisor) ,
             (nvl(sum(daily_data.intercompany_in_cost),0)/L_divisor) ,
             (nvl(sum(daily_data.intercompany_out_retail),0)/L_divisor) ,
             (nvl(sum(daily_data.intercompany_out_cost),0)/L_divisor) ,
             (nvl(sum(daily_data.intercompany_markup),0)/L_divisor) ,
             (nvl(sum(daily_data.intercompany_markdown),0)/L_divisor) ,
             (nvl(sum(daily_data.wo_activity_upd_inv),0)/L_divisor) ,
             (nvl(sum(daily_data.wo_activity_post_fin),0)/L_divisor) ,
             (nvl(sum(daily_data.deal_income_sales),0)/L_divisor) ,
             (nvl(sum(daily_data.deal_income_purch),0)/L_divisor) ,
             (nvl(sum(daily_data.restocking_fee),0)/L_divisor) ,
             (nvl(sum(daily_data.margin_cost_variance),0)/L_divisor) ,
             (nvl(sum(daily_data.retail_cost_variance),0)/L_divisor) ,
             (nvl(sum(daily_data.rec_cost_adj_variance),0)/L_divisor) ,
             (nvl(sum(daily_data.franchise_sales_retail),0)/L_divisor) ,
             (nvl(sum(daily_data.franchise_sales_cost),0)/L_divisor) ,
             (nvl(sum(daily_data.franchise_returns_retail),0)/L_divisor) ,
             (nvl(sum(daily_data.franchise_returns_cost),0)/L_divisor) ,
             (nvl(sum(daily_data.franchise_markup_retail),0)/L_divisor) ,             
             (nvl(sum(daily_data.franchise_markdown_retail),0)/L_divisor) ,
             (nvl(sum(daily_data.franchise_restocking_fee),0)/L_divisor) ,
             (nvl(sum(daily_data.vat_in),0)/L_divisor) ,
             (nvl(sum(daily_data.vat_out),0)/L_divisor) ,
             (nvl(sum(daily_data.recoverable_tax),0)/L_divisor) ,
             (nvl(sum(daily_data.net_sales_non_inv_retail),0)/ L_divisor) ,
             (nvl(sum(daily_data.net_sales_non_inv_cost),0)/ L_divisor) ,
             (nvl(sum(daily_data.net_sales_non_inv_rtl_ex_vat), 0)/ L_divisor))
        from daily_data
       where half_no = L_half
         and month_no = L_month
         and week_no = L_week
         and dept = nvl(I_dept,dept)
         and data_date >= (I_week_date - 6)
         and data_date <= I_week_date
         and class = nvl(I_class,class)
         and subclass = nvl(I_subclass,subclass)
         and loc_type = nvl(I_loc_type,loc_type)
         and location = nvl(I_location,location)
         and currency_ind = I_currency_ind
         and set_of_books_id = nvl(I_set_of_books_id,set_of_books_id)
       group by dept,class,subclass,loc_type,location,half_no, data_date, day_no
       order by day_no;

BEGIN
   L_dd   := TO_NUMBER(TO_CHAR(I_week_date,'DD'));
   L_mm   := TO_NUMBER(TO_CHAR(I_week_date,'MM'));
   L_yyyy := TO_NUMBER(TO_CHAR(I_week_date,'YYYY'));


   CAL_TO_454_HALF(L_dd,
                   L_mm,
                   L_yyyy,
                   L_half,
                   L_month,
                   return_code,
                   O_error_message);
   if return_code = 'TRUE' then
      NULL;
   else
      return FALSE;
   end if;

   CAL_TO_454(L_dd,
              L_mm,
              L_yyyy,
              L_dd,
              L_week,
              L_mm,
              L_yyyy,
              return_code,
              O_error_message);
   if return_code = 'TRUE' then
      NULL;
   else
      return FALSE;
   end if;

   if I_year_ind = 'L' then
      L_half := L_half - 10;
   end if;

   if I_thousands = 'Y' then
      L_divisor := 1000;
   end if;

   open C_DAILY_454;
   fetch C_DAILY_454 BULK COLLECT INTO IO_daily_data_454;
   close C_DAILY_454;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END FETCH_DAILY_DATA_454_BULK;
----------------------------------------------------------------------------------
FUNCTION GET_CUM_MARKON_PCT(O_error_message     IN OUT VARCHAR2,
                            O_cum_markon_pct IN OUT week_data.cum_markon_pct%TYPE,
                            I_dept              IN     deps.dept%TYPE,
                            I_class             IN     class.class%TYPE,
                            I_subclass          IN     subclass.subclass%TYPE,
                            I_location          IN     item_loc.loc%TYPE,
                            I_loc_type          IN     item_loc.loc_type%TYPE,
                            I_tran_date         IN     DATE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'STKLEDGR_QUERY_SQL.GET_CUM_MARKON_PCT';

   L_tran_date     PERIOD.VDATE%TYPE;

   cursor C_MARKON_WK is
      select nvl(cum_markon_pct, 0)
        from week_data
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass
         and currency_ind = 'L'  -- location currency
         and cum_markon_pct IS NOT NULL
         and eow_date <= trunc(L_tran_date)
         and location = I_location
         and loc_type = I_loc_type
       order by eow_date desc;

   cursor C_MARKON_MN is
      select cum_markon_pct
        from month_data
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass
         and currency_ind = 'L'  -- location currency
         and cum_markon_pct IS NOT NULL
         and eom_date <= trunc(L_tran_date)
         and location = I_location
         and loc_type = I_loc_type
       order by eom_date desc;

BEGIN
   -- This function will return the cumulative markon pct for a dept/class/subclass/loc/date.
   -- If not found, it will return a NULL value.
   -- For the middle of the current week/month, the markon pct is not available yet.
   -- In that case, use the previous week's markon pct.

   -- default to vdate
   if I_tran_date is NULL then
      L_tran_date := GET_VDATE;
   else
      L_tran_date := I_tran_date;
   end if;

   -- check week data
   O_cum_markon_pct := NULL;

   open C_MARKON_WK;
   fetch C_MARKON_WK into O_cum_markon_pct;
   close C_MARKON_WK;

   if O_cum_markon_pct is NULL then
      -- check month data if week data is not found
      open C_MARKON_MN;
      fetch C_MARKON_MN into O_cum_markon_pct;
      close C_MARKON_MN;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_CUM_MARKON_PCT;
--------------------------------------------------------------------------------
END;
/