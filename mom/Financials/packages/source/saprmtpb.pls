
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_PARM_TYPE_SQL AS
------------------------------------------------------------------------
FUNCTION GET_PARM_TYPE_DATA(O_error_message   IN OUT VARCHAR2, 
                            O_data_type       IN OUT sa_parm_type.data_type%TYPE,
                            O_data_length     IN OUT sa_parm_type.data_length%TYPE,
                            O_data_precision  IN OUT sa_parm_type.data_precision%TYPE,
                            O_data_scale      IN OUT sa_parm_type.data_scale%TYPE,
                            I_parm_type_id    IN     sa_parm_type.parm_type_id%TYPE)
                            return BOOLEAN is

   cursor C_GET_INFO is
      select data_type, data_length, data_precision, data_scale
        from sa_parm_type
       where parm_type_id = I_parm_type_id;

BEGIN
   open C_GET_INFO;
   fetch C_GET_INFO into O_data_type,
                         O_data_length,
                         O_data_precision,
                         O_data_scale;
   if C_GET_INFO%NOTFOUND then
      close C_GET_INFO;
      O_error_message := sql_lib.create_msg('ARI_INV_PARM_TYPE',
                                             I_parm_type_id,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   close C_GET_INFO;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                            'PARM_TYPE_SQL.GET_PARM_TYPE_DATA', 
                                             to_char(SQLCODE));
   return FALSE;
END GET_PARM_TYPE_DATA; 
------------------------------------------------------------------------
END SA_PARM_TYPE_SQL;
/
