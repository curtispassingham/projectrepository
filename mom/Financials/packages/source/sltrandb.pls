
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY STKLEDGR_TRAN_DATA_SQL AS

----------------------------------------------------------------------
FUNCTION GET_TRAN_DESC (O_error_message	IN OUT	VARCHAR2,
			      I_code		IN	NUMBER,
			      O_desc		IN OUT	VARCHAR2)
   return BOOLEAN IS

   L_program	VARCHAR2(64)	:= 'STKLEDGR_TRANS_DATA.GET_TRAN_DESC';

   CURSOR c_tran_desc IS
	SELECT decode
	FROM v_tran_data_codes_tl
	WHERE code = I_code;

BEGIN
   OPEN c_tran_desc;
   FETCH c_tran_desc INTO O_desc;
   if c_tran_desc%NOTFOUND then
	CLOSE c_tran_desc;
	O_error_message := SQL_LIB.CREATE_MSG ('INV_TRAN_CODE',NULL,NULL,NULL);
	return FALSE;
   else
	CLOSE c_tran_desc;
	return TRUE;
   end if;

EXCEPTION
   when OTHERS then
	O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
						SQLERRM,
						L_program,
						NULL);
	return FALSE;

END GET_TRAN_DESC;
--------------------------------------------------------------------------------
FUNCTION GET_REF_DESC( O_error_message     IN OUT VARCHAR2,
                       O_ref_no_1_desc     IN OUT tran_data_codes_ref.ref_no_1_desc%TYPE,
                       O_ref_no_1_desc_tl  IN OUT tran_data_codes_ref.ref_no_1_desc%TYPE,
                       O_ref_no_2_desc     IN OUT tran_data_codes_ref.ref_no_2_desc%TYPE,
                       O_ref_no_2_desc_tl  IN OUT tran_data_codes_ref.ref_no_2_desc%TYPE,
                       O_gl_ref_no_desc    IN OUT tran_data_codes_ref.gl_ref_no_desc%TYPE,
                       O_gl_ref_no_desc_tl IN OUT tran_data_codes_ref.gl_ref_no_desc%TYPE,
                       I_tran_code         IN     tran_data_codes_ref.tran_code%TYPE,
                       I_pgm_name          IN     tran_data_codes_ref.pgm_name%TYPE)

   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'STKLEDGR_QUERY_SQL.GET_REF_DESC';
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_REF_DESC is
      select ref_no_1_desc,
             ref_no_2_desc,
             gl_ref_no_desc
        from v_tran_data_codes_ref_tl
       where tran_code = I_tran_code
         and pgm_name  = I_pgm_name;

BEGIN
   -- Retrieve ref_no descriptions from tran_data_codes_ref table.
   SQL_LIB.SET_MARK('OPEN', 'C_REF_DESC', 'TRAN_DATA_CODES_REF', to_char(I_tran_code));
   open C_REF_DESC;
   SQL_LIB.SET_MARK('FETCH', 'C_REF_DESC', 'TRAN_DATA_CODES_REF', to_char(I_tran_code));
   fetch C_REF_DESC into O_ref_no_1_desc,
                         O_ref_no_2_desc,
                         O_gl_ref_no_desc;
   SQL_LIB.SET_MARK('CLOSE', 'C_REF_DESC', 'TRAN_DATA_CODES_REF', to_char(I_tran_code));
   close C_REF_DESC;
   
   -- if ref desc is null, then populate default text.
   if O_ref_no_1_desc is NULL then
      ---
      if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                    'LABL',
                                    'REF1DE',
                                     O_ref_no_1_desc) = FALSE then
         return FALSE;
      end if;
      ---;
   end if;
   -- if ref desc is null, then populate default text.
   if O_ref_no_2_desc is NULL then
         ---
         if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                       'LABL',
                                       'REF2DE',
                                        O_ref_no_2_desc) = FALSE then
            return FALSE;
         end if;
         ---;
   end if;
   ---

   -- if ref desc is null, then populate default text.
   if O_gl_ref_no_desc is NULL then
      O_gl_ref_no_desc    := 'GL Reference No.';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE)); 
      return FALSE;
END GET_REF_DESC;
-----------------------------------------------------------------------------
END STKLEDGR_TRAN_DATA_SQL;
/


