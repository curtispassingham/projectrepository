
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_ROLES_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
--- Function: ROLE_EXISTS
--- Purpose:  Check if the given role already exists
---           on the sa_role_field table.
--------------------------------------------------------------------
FUNCTION ROLE_EXISTS(O_error_message IN OUT VARCHAR2,
                     O_exists        IN OUT BOOLEAN,
                     I_role          IN     SA_ROLE_FIELD.ROLE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: DBA_ROLE_EXISTS
--- Purpose:  Check if the given role already exists
---           on the dba_roles table.
--------------------------------------------------------------------
FUNCTION DBA_ROLE_EXISTS(O_error_message IN OUT VARCHAR2,
                         O_exists        IN OUT BOOLEAN,
                         I_role          IN     DBA_ROLES.ROLE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: ROLE_FIELD_EXISTS
--- Purpose:  Check if the given role/field relationship
---           already exists on the sa_role table.
--------------------------------------------------------------------
FUNCTION ROLE_FIELD_EXISTS(O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           I_role          IN     SA_ROLE_FIELD.ROLE%TYPE,
                           I_field         IN     SA_ROLE_FIELD.FIELD%TYPE )
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: CHECK_FIELD_DELETE
--- Purpose:  Check if the given field is required. 
--------------------------------------------------------------------
FUNCTION CHECK_FIELD_DELETE(O_error_message  IN OUT VARCHAR2,
                            O_delete_ind     IN OUT VARCHAR2,
                            I_field          IN     SA_ROLE_FIELD.FIELD%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: INSERT_ROLE_FIELDS
--- Purpose: Insert the default role field values for a given role.
--------------------------------------------------------------------
FUNCTION INSERT_ROLE_FIELDS(O_error_message   IN OUT VARCHAR2,
                            I_role            IN     SA_ROLE_FIELD.ROLE%TYPE,
                            I_enable_ind      IN     SA_ROLE_FIELD.ENABLE_IND%TYPE,
                            I_display_ind     IN     SA_ROLE_FIELD.DISPLAY_IND%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: DELETE_ROLE
--- Purpose: Delete all records on the sa_role_field table that
---          match the given role.
--------------------------------------------------------------------
FUNCTION DELETE_ROLE(O_error_message   IN OUT VARCHAR2,
                     I_role            IN     SA_ROLE_FIELD.ROLE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: GET_OS_FIELD_ACCESS
--- Purpose: Retrieve the enable and display indicators from the
---          sa_role_field table for each O/S field.
--------------------------------------------------------------------
FUNCTION GET_OS_FIELD_ACCESS(O_error_message        IN OUT  VARCHAR2,
                             O_act_os_display       IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                             O_trial_os_display     IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                             O_store_value_display  IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                             O_store_value_enable   IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                             O_hq_value_display     IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                             O_hq_value_enable      IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                             I_user_id              IN      VARCHAR2 )
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: GET_MISC_FIELD_ACCESS
--- Purpose: Retrieve the enable and display indicators from the
---          sa_role_field table for misc. totals field.
--------------------------------------------------------------------
FUNCTION GET_MISC_FIELD_ACCESS(O_error_message        IN OUT  VARCHAR2,
                               O_store_value_display  IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                               O_store_value_enable   IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                               O_hq_value_display     IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                               O_hq_value_enable      IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                               I_user_id              IN      VARCHAR2 )
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: GET_ERR_FIELD_ACCESS
--- Purpose: Retrieve the enable and display indicators from the
---          sa_role_field table for each error list field.
--------------------------------------------------------------------
FUNCTION GET_ERR_FIELD_ACCESS(O_error_message        IN OUT  VARCHAR2, 
                              O_store_over_display   IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                              O_store_over_enable    IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                              O_hq_over_display      IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                              O_hq_over_enable       IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                              I_user_id              IN      VARCHAR2 )
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function: GET_BAL_FIELD_ACCESS
--- Purpose: Retrieve the enable and display indicators from the 
---          sa_role_field table for the Over/Short Value.
--------------------------------------------------------------------
FUNCTION GET_BAL_FIELD_ACCESS(O_error_message   IN OUT  VARCHAR2,
                              O_os_display      IN OUT  SA_ROLE_FIELD.DISPLAY_IND%TYPE,
                              O_os_enable       IN OUT  SA_ROLE_FIELD.ENABLE_IND%TYPE,
                              I_user_id         IN      VARCHAR2 ) 
   RETURN BOOLEAN;
--------------------------------------------------------------------
END SA_ROLES_SQL;
/
