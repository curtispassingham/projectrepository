
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_TOTAL_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
FUNCTION CHECK_UNIQUE_TOTAL_ID (O_error_message    IN OUT VARCHAR2,
                                O_unique           OUT    BOOLEAN,
                                I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)                    
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION GET_MAX_TOTAL_REV_NO (O_error_message		IN OUT VARCHAR2,
                               O_max_total_rev_no       OUT    SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                               I_total_id               IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/