
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_SEQUENCE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
---  This function will return the valid unique sequence numbers to be used
---  as primary keys throught the Sales Audit Application.  Valid values for 
---  I_seq_type are 'SA_TRAN_SEQ_NO_SEQUENCE', 'SA_STORE_DAY_SEQ_NO_SEQUENCE',
---  'SA_VOUCHER_SEQ_NO_SEQUENCE', 'SA_BAL_GROUP_NO_SEQUENCE', 
---  'SA_VALUE_REV_NO_SEQUENCE', 'SA_ERROR_SEQ_NO_SEQUENCE', 'SA_TOTAL_SEQ_NO_SEQUENCE', 'SA_MISS_TRAN_SEQUENCE' and
---  'SA_EXPORT_SEQ_NO_SEQUENCE'.  
-----------------------------------------------------------------------------------
FUNCTION GET_SEQ (O_error_message      IN OUT VARCHAR2,
                  O_starting_value     IN OUT NUMBER,
                  O_reserved_through   IN OUT NUMBER,
                  I_values_needed      IN     NUMBER,
                  I_seq_type           IN     VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--- This function is used to track the available error numbers in conjunction with
--- the GET_SEQ function above.  This function is called only when the I_seq_type
--- is 'SA_ERROR_SEQ_NO_SEQUENCE'.  
-----------------------------------------------------------------------------------
FUNCTION AVAILABLE_ERROR_NO (O_error_message      IN OUT VARCHAR2,
                             O_starting_value        OUT NUMBER,
                             O_curr_reserved_thru    OUT NUMBER,
                             I_values_needed      IN     NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------

END SA_SEQUENCE_SQL;
/
