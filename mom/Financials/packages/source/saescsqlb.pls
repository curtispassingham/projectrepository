
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_ESCHEAT_SQL AS
------------------------------------------------------------------------
FUNCTION ESCHEAT_EXISTS (O_error_message      IN OUT VARCHAR2,
                         O_exists             IN OUT BOOLEAN,
                         I_partner_type	    IN     PARTNER.PARTNER_TYPE%TYPE,
                         I_partner_id         IN     PARTNER.PARTNER_ID%TYPE,
                         I_country            IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_state              IN     STATE.STATE%TYPE,
                         I_escheat_opt_seq_no IN     SA_ESCHEAT_OPTIONS.ESCHEAT_OPT_SEQ_NO%TYPE) 
   RETURN BOOLEAN IS

   L_exist           VARCHAR2(1)   := 'N';
   L_program         VARCHAR2(60)  := 'SA_ESCHEAT_SQL.ESCHEAT_EXISTS';

    cursor C_ESCHEAT is
    select 'Y' 
      from sa_escheat_options
     where partner_Type = I_partner_type
       and partner_id = I_partner_id
       and (country = I_country
        or (country is NULL 
       and I_country is NULL))
       and (state = I_state
        or (state is NULL 
       and I_state is NULL))
       and escheat_opt_seq_no != I_escheat_opt_seq_no; 

BEGIN

   if I_partner_type is NULL or I_partner_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', 
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---

   if I_partner_id is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_ESCHEAT', NULL, NULL);
      open C_ESCHEAT;
      SQL_LIB.SET_MARK('FETCH','C_ESCHEAT', NULL, NULL);
      fetch C_ESCHEAT into L_exist;
      SQL_LIB.SET_MARK('CLOSE','C_ESCHEAT', NULL, NULL);
      close C_ESCHEAT;
   end if;
   ---
   if L_exist = 'Y' then
      O_exists := TRUE;    
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program,
                                             to_char(SQLCODE));      
      return FALSE;

END ESCHEAT_EXISTS;
----------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ (O_error_message   IN OUT  VARCHAR2,
                       O_next_val        IN OUT  SA_ESCHEAT_OPTIONS.ESCHEAT_OPT_SEQ_NO%TYPE)
  RETURN BOOLEAN IS

   L_program         VARCHAR2(60)  := 'SA_ESCHEAT_SQL.GET_NEXT_SEQ';

   cursor C_NEXT_VAL is
      select SA_ESCHEAT_OPT_SEQ_NO_SEQUENCE.nextval
        from dual;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_NEXT_VAL','DUAL',NULL);
   open C_NEXT_VAL;
   SQL_LIB.SET_MARK('FETCH','C_NEXT_VAL','DUAL',NULL);
   fetch C_NEXT_VAL into O_next_val;
   if C_NEXT_VAL%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_NEXT_VAL','DUAL',NULL);
      close C_NEXT_VAL;
      O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_NEXT_VAL','DUAL',NULL);
   close C_NEXT_VAL; 
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_SEQ;
----------------------------------------------------------------------------------
FUNCTION GET_PARTNER_INFO (O_error_message  IN OUT VARCHAR2,
                           O_exists         IN OUT BOOLEAN,
                           O_partner_type   IN OUT PARTNER.PARTNER_TYPE%TYPE,
                           O_partner_id     IN OUT PARTNER.PARTNER_ID%TYPE,
                           I_country        IN     COUNTRY.COUNTRY_ID%TYPE,
                           I_state          IN     STATE.STATE%TYPE) 
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60)  := 'SA_ESCHEAT_SQL.GET_PARTNER_INFO';

    cursor C_PARTNER is
      select partner_type,
             partner_id
        from partner
       where (partner_id = I_state
              and I_state is NOT NULL
              and partner_type = 'ES')
          or (partner_id = I_country
              and I_country is NOT NULL
              and partner_type = 'EC');

BEGIN
   if I_country is NULL and I_state is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', 
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_PARTNER','PARTNER',NULL);
   open C_PARTNER;
   
   SQL_LIB.SET_MARK('FETCH','C_PARTNER','PARTNER',NULL);
   fetch C_PARTNER into O_partner_type,
                        O_partner_id;
   
   if C_PARTNER%NOTFOUND then
      O_exists := FALSE;
   end if;
   
   SQL_LIB.SET_MARK('CLOSE','C_PARTNER','PARTNER',NULL);
   close C_PARTNER;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program,
                                             to_char(SQLCODE));      
      return FALSE;
END GET_PARTNER_INFO;
----------------------------------------------------------------------------------
FUNCTION VOUCHER_EXISTS (O_error_message      IN OUT VARCHAR2,
                         O_exists             IN OUT BOOLEAN,
                         I_tender_type_id     IN     SA_VOUCHER_OPTIONS.TENDER_TYPE_ID%TYPE) 
  RETURN BOOLEAN IS

   L_exist           VARCHAR2(1)   := 'N';
   L_program         VARCHAR2(60)  := 'SA_ESCHEAT_SQL.VOUCHER_EXISTS';

    cursor C_VOUCHER is
    select 'Y' 
      from sa_voucher_options
     where tender_type_id = I_tender_type_id; 

BEGIN

   if I_tender_type_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', 
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_VOUCHER', NULL, NULL);
   open C_VOUCHER;
   SQL_LIB.SET_MARK('FETCH','C_VOUCHER', NULL, NULL);
   fetch C_VOUCHER into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_VOUCHER', NULL, NULL);
   close C_VOUCHER;
   ---
   if L_exist = 'Y' then
      O_exists := TRUE;    
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program,
                                             to_char(SQLCODE));      
      return FALSE;

END VOUCHER_EXISTS;
----------------------------------------------------------------------------------
FUNCTION DELETE_ESCHEAT (O_error_message      IN OUT VARCHAR2) 
  RETURN BOOLEAN IS

   L_program         VARCHAR2(60)  := 'SA_ESCHEAT_SQL.VOUCHER_EXISTS';
BEGIN
   delete from sa_escheat_options;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program,
                                             to_char(SQLCODE));      
      return FALSE;

END DELETE_ESCHEAT;
----------------------------------------------------------------------------------
END SA_ESCHEAT_SQL;
/