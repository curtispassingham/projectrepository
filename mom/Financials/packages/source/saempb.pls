CREATE OR REPLACE PACKAGE BODY SA_EMPLOYEE_SQL AS
------------------------------------------------------------------------
FUNCTION TRAIT_EMP_EXISTS(O_error_message IN OUT VARCHAR2, 
                          O_exists        IN OUT BOOLEAN,
                          I_emp_id        IN OUT SA_USER.USER_ID%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'SA_EMPLOYEE_SQL.TRAIT_EMP_EXISTS';
   L_exist            VARCHAR2(1)  := 'N';
   L_user_exist       BOOLEAN;
   cursor C_USER_LOC_TRAIT is
    select 'Y'
      from sa_user_loc_traits
     where UPPER(user_id) = UPPER(I_emp_id);
     
     cursor C_TRAIT is
      select user_id
        from sa_user sauser
        where not exists(select 'x' 
                           from sa_user_loc_traits loctrait 
                          where UPPER(sauser.user_id) = UPPER(loctrait.user_id))
      and rownum = 1;
      
      cursor C_USER_EXIST is
      select 'Y' 
      from sa_user
      where UPPER(user_id) = UPPER(I_emp_id);
      
BEGIN
    O_exists := FALSE;
    ---
    if I_emp_id is NOT NULL then
       if EMP_EXISTS(O_error_message, 
                     L_user_exist,
                     I_emp_id) = FALSE then
          return FALSE;
       end if;
       if L_user_exist = TRUE then
          SQL_LIB.SET_MARK('OPEN','C_USER_LOC_TRAIT','SA_USER_LOC_TRAITS','Emp: '||I_emp_id);
          open C_USER_LOC_TRAIT;
          SQL_LIB.SET_MARK('FETCH','C_USER_LOC_TRAIT','SA_USER_LOC_TRAITS','Emp: '||I_emp_id);
          fetch C_USER_LOC_TRAIT into L_exist;
          SQL_LIB.SET_MARK('CLOSE','C_USER_LOC_TRAIT','SA_USER_LOC_TRAITS','Emp: '||I_emp_id);
          close C_USER_LOC_TRAIT;
          ---
          if L_exist = 'Y' then
             O_exists := TRUE;
          end if;
       else
          O_exists := TRUE;
       end if;
    else
      SQL_LIB.SET_MARK('OPEN','C_TRAIT','SA_USER_LOC_TRAITS','Emp: '||I_emp_id);
      open C_TRAIT;
      SQL_LIB.SET_MARK('FETCH','C_TRAIT','SA_USER_LOC_TRAITS','Emp: '||I_emp_id);
      fetch C_TRAIT into I_emp_id;
      SQL_LIB.SET_MARK('CLOSE','C_TRAIT','SA_USER_LOC_TRAITS','Emp: '||I_emp_id);
      close C_TRAIT;
   end if;
      
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TRAIT_EMP_EXISTS;
--------------------------------------------------------------------------
FUNCTION DEL_EMP_DETAIL_RECS(O_error_message IN OUT VARCHAR2,
                             I_emp_id        IN     SA_USER.USER_ID%TYPE)
   RETURN BOOLEAN IS
   
   L_program         VARCHAR2(60)  := 'SA_EMPLOYEE_SQL.DEL_EMP_DETAIL_RECS';

BEGIN
   if I_emp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', 
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','SA_USER_LOC_TRAITS','User: '||I_emp_id);
   delete from sa_user_loc_traits
    where UPPER(user_id) = UPPER(I_emp_id);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','SA_USER','User: '||I_emp_id);
   delete from sa_user
    where UPPER(user_id) = UPPER(I_emp_id);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEL_EMP_DETAIL_RECS;
----------------------------------------------------------------------------
FUNCTION EMP_EXISTS(O_error_message IN OUT VARCHAR2, 
                    O_exists        IN OUT BOOLEAN,
                    I_emp_id        IN OUT SA_USER.USER_ID%TYPE)
 RETURN BOOLEAN IS
    L_user_exist       VARCHAR2(1)  := 'N';
    L_program         VARCHAR2(60)  := 'SA_EMPLOYEE_SQL.EMP_EXISTS';
    cursor C_USER_EXIST is
    select 'Y' 
      from sa_user
     where UPPER(user_id) = UPPER(I_emp_id);
 BEGIN
   O_exists :=  FALSE;
   SQL_LIB.SET_MARK('OPEN','C_USER_EXIST','SA_USER','Emp: '||I_emp_id);
   open C_USER_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_USER_EXIST','SA_USER','Emp: '||I_emp_id);
   fetch C_USER_EXIST into L_user_exist;
   SQL_LIB.SET_MARK('CLOSE','C_USER_EXIST','SA_USER','Emp: '||I_emp_id);
   close C_USER_EXIST;
   if L_user_exist = 'Y' then
      O_exists :=  TRUE;
   end if;   
 return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
 END EMP_EXISTS;
-----------------------------------------------------------------
FUNCTION GET_EMP_INFO(O_error_message   IN OUT VARCHAR2,
                      O_emp_type        IN OUT SA_EMPLOYEE.EMP_TYPE%TYPE,
                      O_cashier_ind     IN OUT SA_EMPLOYEE.CASHIER_IND%TYPE,
                      O_name            IN OUT SA_EMPLOYEE.NAME%TYPE,
                      IO_user_id        IN OUT SA_EMPLOYEE.USER_ID%TYPE,
                      IO_emp_id         IN OUT SA_EMPLOYEE.EMP_ID%TYPE) 
   RETURN BOOLEAN IS

   L_salesperson_ind  SA_EMPLOYEE.SALESPERSON_IND%TYPE;
   L_program         VARCHAR2(60)  := 'SA_EMPLOYEE_SQL.GET_EMP_INFO';

BEGIN
   if GET_EMP_INFO(O_error_message,
                   O_emp_type,
                   O_cashier_ind,
                   L_salesperson_ind,
                   O_name,
                   IO_user_id,
                   IO_emp_id) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_EMP_INFO;
--------------------------------------------------------------------------------
FUNCTION GET_EMP_INFO(O_error_message   IN OUT VARCHAR2,
                      O_emp_type        IN OUT SA_EMPLOYEE.EMP_TYPE%TYPE,
                      O_cashier_ind     IN OUT SA_EMPLOYEE.CASHIER_IND%TYPE,
                      O_salesperson_ind IN OUT SA_EMPLOYEE.SALESPERSON_IND%TYPE,
                      O_name            IN OUT SA_EMPLOYEE.NAME%TYPE,
                      IO_user_id        IN OUT SA_EMPLOYEE.USER_ID%TYPE,
                      IO_emp_id         IN OUT SA_EMPLOYEE.EMP_ID%TYPE) 
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60)  := 'SA_EMPLOYEE_SQL.GET_EMP_INFO';

   cursor C_GET_INFO is
      select emp_type,
             cashier_ind,
             salesperson_ind,
             name,
             user_id,
             emp_id
        from sa_employee
       where user_id = nvl(IO_user_id, user_id)
         and emp_id  = nvl(IO_emp_id, emp_id);

BEGIN
   if IO_emp_id is null and IO_user_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', 
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_INFO','SA_EMPLOYEE',NULL);
   open C_GET_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_INFO','SA_EMPLOYEE',NULL);
   fetch C_GET_INFO into O_emp_type,
                         O_cashier_ind,
                         O_salesperson_ind,
                         O_name,
                         IO_user_id,
                         IO_emp_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_INFO','SA_EMPLOYEE',NULL);
   close C_GET_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_EMP_INFO;
----------------------------------------------------------------------
FUNCTION GET_CASHIER_NAME(O_error_message  IN OUT  VARCHAR2,
                          O_name           IN OUT  SA_EMPLOYEE.NAME%TYPE,
                          I_pos_id         IN      SA_STORE_EMP.POS_ID%TYPE,
                          I_store          IN      STORE.STORE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program   VARCHAR(60) := 'SA_EMPLOYEE_SQL.GET_CASHIER_NAME';
   ---
   cursor C_GET_NAME is
      select e.name
        from sa_employee e, 
             sa_store_emp s
       where e.emp_id = s.emp_id
         and s.pos_id = I_pos_id
         and s.store  = I_store;
BEGIN
   if I_pos_id is NULL or I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_NAME','SA_EMPLOYEE, SA_STORE_EMP','CASHIER '||I_pos_id);
   open C_GET_NAME;
   SQL_LIB.SET_MARK('FETCH','C_GET_NAME','SA_EMPLOYEE, SA_STORE_EMP','CASHIER '||I_pos_id);
   fetch C_GET_NAME into O_name;
   SQL_LIB.SET_MARK('CLOSE','C_GET_NAME','SA_EMPLOYEE, SA_STORE_EMP','CASHIER '||I_pos_id);
   close C_GET_NAME;
   ---
   return TRUE;
EXCEPTION
      when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CASHIER_NAME;
--------------------------------------------------------------------------------
FUNCTION CHECK_LOC_TRAITS_ASSIGNED(O_error_message  IN OUT  VARCHAR2,
                                   O_exists         IN OUT  BOOLEAN,
                                   I_user_id        IN      SA_USER.USER_ID%TYPE)
  RETURN BOOLEAN AS
   cursor C_CHECK_LOC_TRAIT is
      select 'Y'
        from v_loc_traits_matrix vltm,
             v_store vs
       where vs.store = vltm.store
        and rownum =1;
  
    L_loctrait_exist       VARCHAR2(1)  := 'N';
    L_program              VARCHAR2(60)  := 'SA_EMPLOYEE_SQL.CHECK_LOC_TRAITS_ASSIGNED';

BEGIN
   O_exists :=  FALSE;
   SQL_LIB.SET_MARK('OPEN','C_CHECK_LOC_TRAIT','V_LOC_TRAITS_MATRIX','User '||I_user_id);
   open C_CHECK_LOC_TRAIT;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_LOC_TRAIT','V_LOC_TRAITS_MATRIX','User: '||I_user_id);
   fetch C_CHECK_LOC_TRAIT into L_loctrait_exist;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_LOC_TRAIT','V_LOC_TRAITS_MATRIX','User: '||I_user_id);
   close C_CHECK_LOC_TRAIT;
   if L_loctrait_exist = 'Y' then
      O_exists :=  TRUE;
   end if;   
  return true;
EXCEPTION
      when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
  
END CHECK_LOC_TRAITS_ASSIGNED;
-------------------------------------------------------------------------------  
END SA_EMPLOYEE_SQL;
/
