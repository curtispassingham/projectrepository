
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE or REPLACE PACKAGE SA_TOTAL_DEF_SQL AS
----------------------------------------------------------------
--- Function:  CHECK_TOTAL_COMPATABILITY
--- Purpose:   Ensure that similar totals are being summed 
---            together to form combined totals.
--- Called By: SA_TOTAL_DEF_SQL
---
FUNCTION CHECK_TOTAL_COMPATABILITY
      (O_error_message   IN OUT  VARCHAR2,
       O_compatible      IN OUT  BOOLEAN,
       I_base_total      IN      SA_TOTAL.TOTAL_ID%TYPE,
       I_total_to_check  IN      SA_TOTAL.TOTAL_ID%TYPE)
   return BOOLEAN;

   FUNCTION GET_PARM_TYPE_GROUP (I_parm_type_id      IN SA_PARM.PARM_TYPE_ID%TYPE)
            RETURN VARCHAR2;
   PRAGMA RESTRICT_REFERENCES (get_parm_type_group, WNDS, WNPS);

   FUNCTION ORDER_GROUP_PARMS (O_error_message       IN OUT VARCHAR2,
                               O_changed_order       IN OUT BOOLEAN,
                               O_group_seq_no1       IN OUT SA_VR_PARMS.VR_PARM_SEQ_NO%TYPE,
                               O_label1              IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                               O_group_seq_no2       IN OUT SA_VR_PARMS.VR_PARM_SEQ_NO%TYPE,
                               O_label2              IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                               O_group_seq_no3       IN OUT SA_VR_PARMS.VR_PARM_SEQ_NO%TYPE,
                               O_label3              IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                               I_vr_id               IN     SA_TOTAL_HEAD.VR_ID%TYPE,
                               I_vr_rev_no           IN     SA_TOTAL_HEAD.VR_REV_NO%TYPE)
            RETURN BOOLEAN;

   FUNCTION ORDER_GROUP_PARMS (O_error_message     IN OUT VARCHAR2,
                               O_changed_order     IN OUT BOOLEAN,
                               O_rollup1_name      IN OUT SA_PARM.PHYSICAL_NAME%TYPE,
                               O_rollup1_parm_type IN OUT SA_PARM.PARM_TYPE_ID%TYPE,
                               O_rollup2_name      IN OUT SA_PARM.PHYSICAL_NAME%TYPE,
                               O_rollup2_parm_type IN OUT SA_PARM.PARM_TYPE_ID%TYPE,
                               O_rollup3_name      IN OUT SA_PARM.PHYSICAL_NAME%TYPE,
                               O_rollup3_parm_type IN OUT SA_PARM.PARM_TYPE_ID%TYPE)
            RETURN BOOLEAN;

   FUNCTION CREATE_TOTAL_DATA (O_error_message     IN OUT VARCHAR2,
                               I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                               I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                               I_rollup1_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                               I_rollup1_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE,
                               I_rollup2_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                               I_rollup2_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE,
                               I_rollup3_name      IN     SA_PARM.PHYSICAL_NAME%TYPE,
                               I_rollup3_parm_type IN     SA_PARM.PARM_TYPE_ID%TYPE)
            RETURN BOOLEAN;

   FUNCTION CREATE_WIZ_TOTAL_DATA (O_error_message     IN OUT VARCHAR2,
                                   I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                   I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
      RETURN BOOLEAN;

   FUNCTION CREATE_COMB_TOTAL_DATA (O_error_message     IN OUT VARCHAR2,
                                    I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                    I_total_rev_no      IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
      RETURN BOOLEAN;
   FUNCTION DROP_TOTAL_VIEW (O_error_message     IN OUT VARCHAR2,
                             I_total_id          IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
      RETURN BOOLEAN;


   FUNCTION INSERT_REALM (O_error_message     IN OUT VARCHAR2,
                          I_realm_id          IN     SA_REALM.REALM_ID%TYPE,
                          I_realm_name        IN     SA_REALM.PHYSICAL_NAME%TYPE)
      RETURN BOOLEAN;
   FUNCTION INSERT_PARM (O_error_message     IN OUT VARCHAR2,
                         I_realm_id          IN     SA_PARM.REALM_ID%TYPE,
                         I_parm_type_id      IN     SA_PARM.PARM_TYPE_ID%TYPE,
                         I_parm_name         IN     SA_PARM.PHYSICAL_NAME%TYPE,
                         I_realm_key_ind     IN     SA_PARM.REALM_KEY_IND%TYPE,
                         I_sys_parm          IN     BOOLEAN)
      RETURN BOOLEAN;

   FUNCTION APPROVED_TOTAL_EXISTS (O_error_message      IN OUT VARCHAR2,
				   O_approved_exists    IN OUT  VARCHAR2,
				   I_total_id           IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
      RETURN BOOLEAN;

----------------------------------------------------------------------
-- FUNCTION VALID_MULTI_VERSIONS
-- Checks if the current total can be active with previous revisions of 
-- the same total based on start and end dates for the total.
--
   FUNCTION VALID_MULTI_VERSIONS(O_ERROR_MESSAGE	IN OUT 	VARCHAR2,
         			 O_valid_multi_rev_ind  IN OUT  VARCHAR2,			
         			 I_total_id             IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
         			 I_total_rev_no         IN      SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
         			 I_start_date           IN      SA_TOTAL_HEAD.START_BUSINESS_DATE%TYPE,
         			 I_end_date             IN      SA_TOTAL_HEAD.END_BUSINESS_DATE%TYPE)
      RETURN BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION DROP_INVALID_MULTI_VERSIONS
-- Drops all revisions of the totals that are invalid with the current
-- total revision.
--
   FUNCTION DROP_INVALID_MULTI_VERSIONS
     	(O_ERROR_MESSAGE	IN OUT 	VARCHAR2,
         I_total_id             IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
         I_total_rev_no         IN      SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
         I_start_date           IN      SA_TOTAL_HEAD.START_BUSINESS_DATE%TYPE,
         I_end_date             IN      SA_TOTAL_HEAD.END_BUSINESS_DATE%TYPE)
        RETURN BOOLEAN; 
----------------------------------------------------------------------
--- Name:    DELETE_TOTAL
--- Purpose: Completely delete the TOTAL including all revisions
---
FUNCTION DELETE_TOTAL(O_error_message     IN OUT  VARCHAR2,
                      I_total_id          IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------
--- Name:    GET_TOTAL_VIEW_NAME
--- Purpose: Get the phsyical view name for the passed-in total ID
---
FUNCTION GET_TOTAL_VIEW_NAME (I_total_id       IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE)
     RETURN VARCHAR2;
----------------------------------------------------------------------
--- Name:    BUILD_NON_SYS_TOTAL
--- Purpose: This function is called for when a total is defined to have
---          the wizard build the total without having a system calculated 
---          value. The function will verify or build the metadata and a 
---          view for this total to enable it to be used in combination totals
---          and audit rules. 
---
FUNCTION BUILD_NON_SYS_TOTAL(O_error_message     IN OUT  VARCHAR2,
                             I_total_id          IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                             I_total_rev_no      IN      SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------
END SA_TOTAL_DEF_SQL;
/
