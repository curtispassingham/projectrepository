
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_COGS AS

--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION CHANGE_CASE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message      IN OUT "RIB_CogsDesc_REC",
                     IO_message_type IN OUT VARCHAR2)
RETURN BOOLEAN;

PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                        IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause          IN     VARCHAR2,
                        I_program        IN     VARCHAR2);


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

PROCEDURE CONSUME(O_status_code   IN OUT VARCHAR2,
                  O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN     RIB_OBJECT,
                  I_message_type  IN     VARCHAR2) IS

L_program        VARCHAR2(50) := 'RMSSUB_COGS.CONSUME';

L_message_type   VARCHAR2(15) := I_message_type;
L_message        "RIB_CogsDesc_REC";
L_dsrcpt_rec     RMSSUB_COGS.COGS_REC_TYPE;

BEGIN

   O_status_code := API_CODES.SUCCESS;

   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is null then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', null, null,
      null);
      raise PROGRAM_ERROR;
   end if;

   if CHANGE_CASE(O_error_message,
                  L_message,
                  L_message_type) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_message_type = COGS_ADD then
      L_message := treat(I_message as "RIB_CogsDesc_REC");

      if RMSSUB_COGS_VALIDATE.CHECK_MESSAGE(O_error_message,
                                              L_dsrcpt_rec,
                                              L_message,
                                              L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if RMSSUB_COGS_SQL.PERSIST(O_error_message,
                                   L_message_type,
                                   L_dsrcpt_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE',
      L_message_type, null, null);
      raise PROGRAM_ERROR;
   end if;

EXCEPTION

   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);

END CONSUME;


--------------------------------------------------------------------------------
-- PRIVATE PROCEDURES
--------------------------------------------------------------------------------

FUNCTION CHANGE_CASE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message      IN OUT "RIB_CogsDesc_REC",
                     IO_message_type IN OUT VARCHAR2)
RETURN BOOLEAN IS

L_program   VARCHAR2(50) := 'RMSSUB_COGS.CHANGE_CASE';

BEGIN

   IO_message_type := lower(IO_message_type);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHANGE_CASE;
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                        IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause          IN     VARCHAR2,
                        I_program        IN     VARCHAR2) IS

L_program   VARCHAR2(50) := 'RMSSUB_COGS.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION

   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
--------------------------------------------------------------------------------

END RMSSUB_COGS;
/
