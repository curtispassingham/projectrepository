CREATE OR REPLACE PACKAGE BODY WF_SALES_UPLOAD_SQL AS
-------------------------------------------------------------------------------
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
   -------------------------------------------------------------------------------
   -- Function Name: ROLLUP_WF_SALES
   -- Purpose      : Sums up the NET_SALES_QTY and TOTAL_RETAIL_AMT values for a
   --                loc/report date/item combination.
   -------------------------------------------------------------------------------
   FUNCTION ROLLUP_WF_SALES(O_error_message          IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)
      RETURN BOOLEAN;
   -------------------------------------------------------------------------------
   -- Function Name: CHUNK_ROLLUP
   -- Purpose      : Divides the records in the WFSLSUPLD_ROLLUP table into chunks of
   --                data that will better accommodate commiting records.
   -------------------------------------------------------------------------------
   FUNCTION CHUNK_ROLLUP(O_error_message     IN OUT NOCOPY     RTK_ERRORS.RTK_TEXT%TYPE)
      RETURN BOOLEAN;
   -------------------------------------------------------------------------------
$end

-------------------------------------------------------------------------------
FUNCTION PROCESS_SALES(O_error_message          IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS

   L_program          VARCHAR2(64)  := 'SALES_UPLOAD_SQL.PROCESS_SALES';

BEGIN

   if ROLLUP_WF_SALES(O_error_message) = FALSE then
      return FALSE;
   end if;

   if CHUNK_ROLLUP(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_SALES;
-------------------------------------------------------------------------------
FUNCTION WF_PURGE_NON_STOCK_SALES_HIST(O_error_message     IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'WF_SALES_UPLOAD_SQL.WF_PURGE_NON_STOCK_SALES_HIST';
   L_retention_date       DATE := NULL;
   ---
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_REC is
      select 'x'
        from wf_non_stockholding_sales
       where report_date < L_retention_date
         for update of item, customer_loc, report_date nowait;

BEGIN


   select p.vdate - pi.wf_non_stock_sales_hist_days
     into L_retention_date
     from period p,
          purge_config_options pi;
   ---

   -- Lock the records that will be deleted on the table.
   open C_LOCK_REC;
   close C_LOCK_REC;

   -- Delete records that have past the retention days.
   delete from wf_non_stockholding_sales
      where report_date < L_retention_date;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'WF_NON_STOCKHOLDING_SALES',
                                            null,
                                            null);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_PURGE_NON_STOCK_SALES_HIST;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_DETAIL(O_error_message     IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                         I_chunk_id          IN            WFSLSUPLD_STAGING.CHUNK_ID%TYPE)

RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'WF_SALES_UPLOAD_SQL.VALIDATE_DETAIL';
   L_item             ITEM_MASTER.ITEM%TYPE;
   L_uom              UOM_CLASS.UOM%TYPE;
   L_item_val_ind     VARCHAR2(1) := NULL;
   L_uom_val_ind      VARCHAR2(1) := NULL;
   L_exist            BOOLEAN     := FALSE;
   L_key              RTK_ERRORS.RTK_KEY%TYPE := NULL;
   L_rowid            ROWID       := NULL;

   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_SALES is
      select rowid,
             item,
             net_sales_qty,
             net_sales_qty_uom,
             total_retail_amt,
             total_retail_amt_curr
        from wfslsupld_staging
       where chunk_id = I_chunk_id
         and error_msg is NULL;

   cursor C_CHECK_ITEM is
      select 'x'
        from item_master
       where item = L_item;

   cursor C_CHECK_UOM is
      select 'x'
        from uom_class
       where uom = L_uom;

   cursor C_LOCK_STAGING is
      select 'x'
        from wfslsupld_staging
       where rowid = L_rowid
         for update of error_msg nowait;

   TYPE sales_info_type is TABLE of C_GET_SALES%ROWTYPE INDEX BY BINARY_INTEGER;
   L_nonstockhold_info         SALES_INFO_TYPE;

BEGIN

   -- validate if input contains a value
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   open C_GET_SALES;
   fetch C_GET_SALES BULK COLLECT into L_nonstockhold_info;
   close C_GET_SALES;

   -- validate the information in the object
   if L_nonstockhold_info.FIRST is NULL then
      return TRUE;
   end if;

   FOR i in L_nonstockhold_info.FIRST..L_nonstockhold_info.LAST LOOP
      L_rowid        := L_nonstockhold_info(i).rowid;
      L_item         := NULL;
      L_uom_val_ind  := NULL;
      L_item_val_ind := NULL;
      ---
      if L_nonstockhold_info(i).item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SKU_NUM_REQ', NULL, NULL, NULL);

         if SQL_LIB.PARSE_MSG(O_error_message,
                              L_key) = FALSE then
            return FALSE;
         end if;
         ---
         open C_LOCK_STAGING;
         close C_LOCK_STAGING;
         ---
         update wfslsupld_staging
            set error_msg = O_error_message
          where rowid = L_nonstockhold_info(i).rowid;
      else
         L_item := L_nonstockhold_info(i).item;
         -- validate item
         open C_CHECK_ITEM;
         fetch C_CHECK_ITEM into L_item_val_ind;
         close C_CHECK_ITEM;

         if L_item_val_ind is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM', L_nonstockhold_info(i).item, NULL, NULL);

            if SQL_LIB.PARSE_MSG(O_error_message,
                                 L_key) = FALSE then
               return FALSE;
            end if;
            ---
            open C_LOCK_STAGING;
            close C_LOCK_STAGING;
            ---
            update wfslsupld_staging
               set error_msg = O_error_message
             where rowid = L_nonstockhold_info(i).rowid;
         end if;
      end if;

      if L_nonstockhold_info(i).net_sales_qty is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NULL_NET_SALES_QTY', NULL, NULL, NULL);

         if SQL_LIB.PARSE_MSG(O_error_message,
                              L_key) = FALSE then
            return FALSE;
         end if;
         ---
         open C_LOCK_STAGING;
         close C_LOCK_STAGING;
         ---
         update wfslsupld_staging
            set error_msg = O_error_message
          where rowid = L_nonstockhold_info(i).rowid;
      end if;

      L_uom := L_nonstockhold_info(i).net_sales_qty_uom;
      -- validate UOM
      open C_CHECK_UOM;
      fetch C_CHECK_UOM into L_uom_val_ind;
      close C_CHECK_UOM;

      if L_uom_val_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', L_nonstockhold_info(i).net_sales_qty_uom, NULL, NULL);

         if SQL_LIB.PARSE_MSG(O_error_message,
                              L_key) = FALSE then
            return FALSE;
         end if;
         ---
         open C_LOCK_STAGING;
         close C_LOCK_STAGING;
         ---
         update wfslsupld_staging
            set error_msg = O_error_message
          where rowid = L_nonstockhold_info(i).rowid;
      end if;

      if L_nonstockhold_info(i).total_retail_amt is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NULL_TOTAL_RET_AMT', NULL, NULL, NULL);

         if SQL_LIB.PARSE_MSG(O_error_message,
                              L_key) = FALSE then
            return FALSE;
         end if;
         ---
         open C_LOCK_STAGING;
         close C_LOCK_STAGING;
         ---
         update wfslsupld_staging
            set error_msg = O_error_message
          where rowid = L_nonstockhold_info(i).rowid;
      end if;

      -- validate currency code
      if L_nonstockhold_info(i).total_retail_amt_curr is NULL then
         L_exist := FALSE;
      else
         if CURRENCY_SQL.EXIST(O_error_message,
                               L_nonstockhold_info(i).total_retail_amt_curr,
                               L_exist) = FALSE then
            return FALSE;
         end if;
      end if;

      if L_exist = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CURR_CODE', L_nonstockhold_info(i).total_retail_amt_curr, NULL, NULL);

         if SQL_LIB.PARSE_MSG(O_error_message,
                              L_key) = FALSE then
            return FALSE;
         end if;
         ---
         open C_LOCK_STAGING;
         close C_LOCK_STAGING;
         ---
         update wfslsupld_staging
            set error_msg = O_error_message
          where rowid = L_nonstockhold_info(i).rowid;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'WFSLSUPLD_STAGING',
                                            null,
                                            null);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DETAIL;
-------------------------------------------------------------------------------
FUNCTION CHUNK_STAGING(O_error_message     IN OUT NOCOPY     RTK_ERRORS.RTK_TEXT%TYPE,
                       I_customer_loc      IN                WFSLSUPLD_STAGING.CUSTOMER_LOC%TYPE,
                       I_report_date       IN                WFSLSUPLD_STAGING.REPORT_DATE%TYPE)

RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'WF_SALES_UPLOAD_SQL.CHUNK_STAGING';
   L_detail_count       NUMBER(10) := NULL;
   L_max_chunk_size     RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE := NULL;
   L_number_of_chunks   NUMBER(10) := NULL;

   cursor C_GET_MAX_CHUNK_SIZE is
      select max_chunk_size
        from rms_plsql_batch_config
       where program_name = 'WF_SALES_UPLOAD_SQL';

BEGIN

   -- validate if input parameters contain values
   if I_customer_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_customer_loc',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_report_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_report_date',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   ---------------------------------------------------------------------------
   -- Get the maximum size of a wfslsupld transaction
   ---------------------------------------------------------------------------
   open C_GET_MAX_CHUNK_SIZE;
   fetch C_GET_MAX_CHUNK_SIZE into L_max_chunk_size;
   close C_GET_MAX_CHUNK_SIZE;

   -- Check if commit_max_ctr was retrieved
   if L_max_chunk_size is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('WFSU_NO_MAX_TRAN_SIZE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Get the number of wfslsupld detail rows
   select count(*) into L_detail_count
     from wfslsupld_staging
    where error_msg is NULL;

   -- Chunk the records on wfslsupld_staging
   merge into WFSLSUPLD_STAGING target
   using (select rowid,
                 CEIL(inner_set.rank/L_max_chunk_size) chunk_id
            from (select ws.item,
                         ROW_NUMBER()
                            OVER(ORDER by (ws.item)) rank
                    from wfslsupld_staging ws
                   where ws.error_msg is NULL) inner_set
         ) source
      on (target.rowid = source.rowid)
   when matched then
      update set target.chunk_id     = source.chunk_id,
                 target.customer_loc = case
                                        when target.customer_loc is NULL then
                                           I_customer_loc
                                        else
                                           target.customer_loc
                                       end,
                 target.report_date  = case
                                        when target.report_date is NULL then
                                           I_report_date
                                        else
                                           target.report_date
                                       end;

   -- Check for records that did not get chunked
   -- Return fatal if records are found
   if L_detail_count <> NVL(SQL%ROWCOUNT, 0) then
      O_error_message := SQL_LIB.CREATE_MSG('WFSLSUPLD_CHUNK_ERROR',
                                            L_detail_count,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHUNK_STAGING;
-------------------------------------------------------------------------------
FUNCTION ROLLUP_WF_SALES(O_error_message          IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'WF_SALES_UPLOAD_SQL.ROLLUP_WF_SALES';

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STAGING is
      select 'x'
        from wfslsupld_staging
       where error_msg is NULL
         for update nowait;

BEGIN

   -- insert the validated information into the WFSLSUPLD_ROLLUP
   merge into WFSLSUPLD_ROLLUP target
   using (select customer_loc,
                 report_date,
                 item,
                 sum(net_sales_qty) net_sales_qty,
                 net_sales_qty_uom,
                 sum(total_retail_amt) total_retail_amt,
                 total_retail_amt_curr
            from wfslsupld_staging
           where error_msg is NULL
        group by customer_loc,
                 report_date,
                 item,
                 net_sales_qty_uom,
                 total_retail_amt_curr) source
      on (target.customer_loc = source.customer_loc
          and target.report_date = source.report_date
          and target.item = source.item)
    when matched then
       update set target.net_sales_qty = target.net_sales_qty + source.net_sales_qty,
                  target.total_retail_amt = target.total_retail_amt + source.total_retail_amt
    when not matched then
   insert (customer_loc,
           report_date,
           item,
           net_sales_qty,
           net_sales_qty_uom,
           total_retail_amt,
           total_retail_amt_curr)
   values (source.customer_loc,
           source.report_date,
           source.item,
           source.net_sales_qty,
           source.net_sales_qty_uom,
           source.total_retail_amt,
           source.total_retail_amt_curr);

   -- lock the WFSLSUPLD_STAGING table and delete all the records which do not have errors.
   open C_LOCK_STAGING;
   close C_LOCK_STAGING;

   delete from WFSLSUPLD_STAGING
    where error_msg is NULL;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'WFSLSUPLD_STAGING',
                                            null,
                                            null);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ROLLUP_WF_SALES;
-------------------------------------------------------------------------------
FUNCTION CHUNK_ROLLUP(O_error_message     IN OUT NOCOPY     RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'WF_SALES_UPLOAD_SQL.CHUNK_ROLLUP';
   L_detail_count       NUMBER(10) := NULL;
   L_max_chunk_size     RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE := NULL;
   L_number_of_chunks   NUMBER(10) := NULL;

   cursor C_GET_MAX_CHUNK_SIZE is
      select max_chunk_size
        from rms_plsql_batch_config
       where program_name = 'WF_SALES_UPLOAD_SQL';

BEGIN
   ---------------------------------------------------------------------------
   -- Get the maximum size of a wfslsupld transaction
   ---------------------------------------------------------------------------
   open C_GET_MAX_CHUNK_SIZE;
   fetch C_GET_MAX_CHUNK_SIZE into L_max_chunk_size;
   close C_GET_MAX_CHUNK_SIZE;
   --
   -- Check if commit_max_ctr was retrieved
   if L_max_chunk_size is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('WFSU_NO_MAX_TRAN_SIZE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Get the number of wfslsupld detail rows
   select count(*) into L_detail_count
     from wfslsupld_rollup;

   -- Check if no records were found
   if L_detail_count is NULL or L_detail_count = 0 then
      return TRUE;
    end if;

   -- Chunk the records on wfslsupld_rollup
   merge into WFSLSUPLD_ROLLUP target
   using (select distinct
                 item,
                 customer_loc,
                 report_date,
                 CEIL(inner_set.rank/L_max_chunk_size) chunk_id
            from (select ws.item,
                         ws.customer_loc,
                         ws.report_date,
                         DENSE_RANK()
                            OVER(ORDER by (im.item)) rank
                    from wfslsupld_rollup ws,
                         item_master im
                   where ws.item = im.item) inner_set
         ) source
      on (target.customer_loc = source.customer_loc
          and target.report_date = source.report_date
          and target.item = source.item)
   when matched then
      update set target.chunk_id = source.chunk_id;

   -- Check for records that did not get chunked
   -- Return fatal if records are found
   if L_detail_count <> NVL(SQL%ROWCOUNT, 0) then
      O_error_message := SQL_LIB.CREATE_MSG('WFSLSUPLD_CHUNK_ERROR',
                                            L_detail_count,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHUNK_ROLLUP;
-------------------------------------------------------------------------------
FUNCTION LOAD_WF_SALES(O_error_message     IN OUT NOCOPY     RTK_ERRORS.RTK_TEXT%TYPE,
                       I_chunk_id          IN                WFSLSUPLD_ROLLUP.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'WF_SALES_UPLOAD_SQL.LOAD_WF_SALES';

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ROLLUP is
      select 'x'
        from wfslsupld_rollup
       where chunk_id = I_chunk_id
         for update nowait;

BEGIN

   -- check if input parameter contains a value
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   -- populate the final table, WF_NON_STOCKHOLDING_SALES
   merge into WF_NON_STOCKHOLDING_SALES target
   using(select customer_loc,
                item,
                report_date,
                net_sales_qty,
                net_sales_qty_uom,
                total_retail_amt,
                total_retail_amt_curr
           from wfslsupld_rollup
          where chunk_id = I_chunk_id
       order by item) source
      on (target.customer_loc = source.customer_loc
          and target.report_date = source.report_date
          and target.item = source.item)
    when matched then
       update set target.net_sales_qty = target.net_sales_qty + source.net_sales_qty,
                  target.total_retail_amt = target.total_retail_amt + source.total_retail_amt
    when not matched then
   insert (customer_loc,
           report_date,
           item,
           net_sales_qty,
           net_sales_qty_uom,
           total_retail_amt,
           total_retail_amt_curr)
   values (source.customer_loc,
           source.report_date,
           source.item,
           source.net_sales_qty,
           source.net_sales_qty_uom,
           source.total_retail_amt,
           source.total_retail_amt_curr);

   -- lock the WFSLSUPLD_ROLLUP table and delete the records from the table according to chunk ID.
   open C_LOCK_ROLLUP;
   close C_LOCK_ROLLUP;

   delete from WFSLSUPLD_ROLLUP
    where chunk_id = I_chunk_id;


   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'WFSLSUPLD_ROLLUP',
                                            null,
                                            null);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOAD_WF_SALES;
-------------------------------------------------------------------------------
FUNCTION PURGE_REJECTS(O_error_message    IN OUT NOCOPY      RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rej_filename     IN                 VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'WF_SALES_UPLOAD_SQL.PURGE_REJECTS';

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STAGING is
      select 'x'
        from wfslsupld_staging
       where customer_loc || '_' || to_char(report_date,'YYYYMMDDHH24MISS') = I_rej_filename
         and error_msg is NOT NULL
         for update nowait;

BEGIN

   if I_rej_filename is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_rej_filename',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   --lock the WFSLSUPLD_STAGING table.
   open C_LOCK_STAGING;
   close C_LOCK_STAGING;

   -- delete records from the WFSLSUPLD_STAGING table. Only delete those which have errors.
   delete from WFSLSUPLD_STAGING
    where customer_loc || '_' || to_char(report_date,'YYYYMMDDHH24MISS') = I_rej_filename
      and error_msg is NOT NULL;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'WFSLSUPLD_STAGING',
                                            null,
                                            null);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PURGE_REJECTS;
-------------------------------------------------------------------------------
END WF_SALES_UPLOAD_SQL;
/
