CREATE OR REPLACE PACKAGE CORESVC_SA_STORE_DATA AUTHID CURRENT_USER
is
	Type SA_STORE_DATA_rec_tab
	IS
	TABLE OF SA_STORE_DATA%rowtype;
	FUNCTION process(
	  O_error_message IN OUT rtk_errors.rtk_text%type,
	  I_process_id    IN NUMBER,
	  O_error_count OUT NUMBER) RETURN BOOLEAN;
    
END coresvc_sa_store_data;
/ 