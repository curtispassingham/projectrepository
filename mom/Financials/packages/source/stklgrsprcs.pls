
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STKLEDGR_PRICING_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
--Function Name:  BULK_INSERT_TRAN_DATA
--Purpose: This function is called from the price change and clearance API to 
--         performance bulk inserts to the tran_data table. It takes logic out of 
--         the existing STKLEDGR_SQL.BUILD_TRAN_DATA_INSERTS function to handle 
--         stock ledger writes for price change and clearance only. As a result, it
--         only handles the following tran_codes: 11 (markup), 12 (markup cancel),
--         13 (markdown), 14 (markdown cancel), 16 (clearance markdown). 
-- 
--         All items should be tran-level non-pack items, and should share the same 
--         item_attributes, including dept/class/subclass, pack_ind and pack types. 
--         They are either children of the the same parent, or a standalone items.
--         
--         This function does not contain pack specific logic. For packs (sellable), 
--         STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT should be called instead.
--
--         All locations should be of the same type (store or warehouse). 
--         I_items, I_locations, I_tran_codes, I_total_retails, I_old_unit_retails,
--         I_new_unit_retails should all be the same size, indicating the item/loc/tran_code
--         combination to be inserted.
--
--         Prior to the call, all item/locs we are dealing with are populated in
--         global temp tables api_item_temp (unique items) and api_loc_temp (unique locs).
-----------------------------------------------------------------------------------
FUNCTION BULK_INSERT_TRAN_DATA(O_error_message        IN OUT  	RTK_ERRORS.RTK_TEXT%TYPE,
                               I_items                IN      	ITEM_TBL,
                               I_item_attributes      IN      	ITEM_ATTRIB_SQL.API_ITEM_REC,
                               I_locations            IN      	LOC_TBL,
                               I_loc_type             IN      	TRAN_DATA.LOC_TYPE%TYPE,
                               I_tran_date            IN      	TRAN_DATA.TRAN_DATE%TYPE,
                               I_tran_codes           IN      	TRAN_TYPE_TBL,
                               I_units                IN      	QTY_TBL,
                               I_total_retails        IN          UNIT_RETAIL_TBL,
                               I_old_unit_retails     IN          UNIT_RETAIL_TBL,
                               I_new_unit_retails     IN          UNIT_RETAIL_TBL,
                               I_pgm_name             IN		TRAN_DATA.PGM_NAME%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END;
/
