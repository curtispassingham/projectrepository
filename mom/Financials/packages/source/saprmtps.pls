
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_PARM_TYPE_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------
---Function Name: GET_PARM_TYPE_DATA
---Purpose:       This function takes in a parameter type id and returns 
---               data type, length, precision, and scale information.
---Created by:    Sonia Wong, 23-JUL-98
------------------------------------------------------------------------
FUNCTION  GET_PARM_TYPE_DATA(O_error_message   IN OUT VARCHAR2, 
                             O_data_type       IN OUT sa_parm_type.data_type%TYPE,
                             O_data_length     IN OUT sa_parm_type.data_length%TYPE,
                             O_data_precision  IN OUT sa_parm_type.data_precision%TYPE,
                             O_data_scale      IN OUT sa_parm_type.data_scale%TYPE,
                             I_parm_type_id    IN     sa_parm_type.parm_type_id%TYPE)
                             return BOOLEAN;
------------------------------------------------------------------------
END SA_PARM_TYPE_SQL;
/
