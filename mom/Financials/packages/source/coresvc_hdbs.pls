CREATE OR REPLACE PACKAGE CORESVC_HDB AUTHID CURRENT_USER AS
   template_key          CONSTANT VARCHAR2(255) := 'HDB_DATA';
   template_category     CONSTANT VARCHAR2(255) := 'RMSFCO';
   action_new            VARCHAR2(25)           := 'NEW';
   action_mod            VARCHAR2(25)           := 'MOD';
   action_del            VARCHAR2(25)           := 'DEL';
   HDB_sheet             VARCHAR2(255)          := 'HDB';
   HDB$Action            NUMBER                 := 1;
   HDB$MARKDOWN_PCT      NUMBER                 := 9;
   HDB$SHRINKAGE_PCT     NUMBER                 := 8;
   HDB$CUM_MARKON_PCT    NUMBER                 := 7;
   HDB$LOCATION          NUMBER                 := 5;
   HDB$LOC_TYPE          NUMBER                 := 4;
   HDB$HALF_NO           NUMBER                 := 3;
   HDB$DEPT              NUMBER                 := 2;
   HDB$SET_OF_BOOKS_ID   NUMBER                 := 6;

   Type HALF_DATA_BUDGET_rec_tab IS TABLE OF HALF_DATA_BUDGET%ROWTYPE;
   sheet_name_trans   S9T_PKG.trans_map_typ;
   action_column      VARCHAR2(255) := 'ACTION';
--------------------------------------------------------------------------------  
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------                     
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------                     
END CORESVC_HDB;
/