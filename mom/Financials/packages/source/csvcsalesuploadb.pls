create or replace PACKAGE BODY CORESVC_SALES_UPLOAD_SQL AS

   LP_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
-------------------------------------------------------------------------------------------------------
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
   ----------------------------------------------------------------------------------------------------
   -- Function Name: SETUP_WORK_TABLE
   -- Purpose      : Populates the helper tables by parsing values loaded on the
   --                SVC_POSUPLD_LOAD_STAGING table from POSU file
   --
   --                Additional processing are as following
   --                     (target table: SVC_POSUPLD_LINE_ITEM_GTT):
   --                   * For pack items -  this will explode these items into its component
   --                     items and populate corresponding columns accordingly (i.e Pack no,
   --                     pack qty)
   --                   * For transformable sellabe items - the corresponding transformable
   --                     orderable items will be selected to populate the xform related
   --                     columns (i.e. xform_sellable_item, xform_prod_loss_pct,
   --                     xform_yield_pct
   --                   * Default initial values that will be computed later by subsequent
   --                     function calls.
   ----------------------------------------------------------------------------------------------------
   FUNCTION SETUP_WORK_TABLE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id   IN              NUMBER,
                             I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: SETUP_DISC_WORK_TABLE
   -- Purpose      : Populates the helper table for item discount information.
   --                discount value taken from TDETL rows.
   --
   --                Additional processing are as following
   --                     (target table: SVC_POSUPLD_LINE_ITEM_DISC table):
   --                   * Promotional records are validated against RPM view - RPM_PROMO_COMP_V.
   ----------------------------------------------------------------------------------------------------
   FUNCTION SETUP_DISC_WORK_TABLE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_sales_process_id   IN              NUMBER,
                                  I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: WORK_TABLE_DENORMALIZE
   -- Purpose      : Populates the columns at the SVC_POSUPLD_LINE_ITEM_GTT containing
   --                additional information from various lookup tables
   --               (i.e. item_master, dept, class).
   ----------------------------------------------------------------------------------------------------
   FUNCTION WORK_TABLE_DENORMALIZE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: NON_FATAL_ERRORS_CHECK
   -- Purpose      : This function will check for errors on the POSU file with regards to format
   --                and structure based on the expected input file format.
   ----------------------------------------------------------------------------------------------------
   FUNCTION NON_FATAL_ERRORS_CHECK(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: IL_NON_FATAL_ERRORS_CHECK
   -- Purpose      : This function will check invalid item loc ranging for POSU line items.
   ----------------------------------------------------------------------------------------------------
   FUNCTION IL_NON_FATAL_ERRORS_CHECK(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: CATCHWEIGHT_CONVERT
   -- Purpose      : This function will convert catchweight actual selling quantity and UOM to
   --                be used to populate the POSU line item that is one of the staging
   --                tables for POSU processing.
   ----------------------------------------------------------------------------------------------------
   FUNCTION CATCHWEIGHT_CONVERT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id   IN              NUMBER,
                                I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: XFORM_HELPERS
   -- Purpose      : This function is used to compute the prorated sales and unit quantity and
   --                get the corresponding transformed orderable items for every transformable
   --                selling item on the POSU file.
   --                Important to note that there are 2 values that are being used to compute
   --                the prorated retail and quantity posted against the transformable orderable
   --                item compared to the transformable sellable item.
   --               * yield_from_head_item_pct (ITEM_XFORM_DETAIL) - if a sellable only item
   --                      is transformed from multiple orderable items, this will contain the
   --                      percentage of the yield (retail) of the sellable item
   --                      (ITEM_XFORM_DETAIL.DETAIL_ITEM) that is prorated and used to post the
   --                      retail posted against the orderable item.  If this field has a NULL value,
   --                      the sellable item is only associated with a single orderable item.
   --               * production_loss_pct (ITEM_XFORM_HEAD) - percentage value of wastage of the
   --                      orderable transformable item  (item_xform_head.head_item)during the manufacture
   --                      of the sellable items attached to it.  This is used to compute the inventory
   --                      quantity that should be posted on the orderable item when a sellable item has
   --                      been released from a store. This percentage is the value that should be removed
   --                      from the total stock on hand quantity of the item.
   --                       * The units posted for the orderable items  are computed as follows:
   --                         yield_from_head_item_pct * Sales Quantity / 1 - production_loss_pct
   ----------------------------------------------------------------------------------------------------
   FUNCTION XFORM_HELPERS(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_sales_process_id   IN              NUMBER,
                          I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: UOM_CONVERT_QTY
   -- Purpose      : Gets the standard UOM and if standard uom <> selling UOM performs
   --                UOM conversion.
   ----------------------------------------------------------------------------------------------------
   FUNCTION UOM_CONVERT_QTY(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sales_process_id   IN              NUMBER,
                            I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------------
   -- Function Name: RANGE_ITEM_LOC
   -- Purpose      : This function will invoke the NEW_ITEM_LOC function to associate
   --                the item to the selling store if the item/loc relationship is not
   --                setup.
   ----------------------------------------------------------------------------------------------------
   FUNCTION RANGE_ITEM_LOC(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_sales_process_id   IN              NUMBER,
                           I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
$end
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_FHEAD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result             IN OUT   NUMBER,
                        I_sales_process_id   IN       NUMBER)
RETURN BOOLEAN IS

   L_store              STORE.STORE%TYPE;
   L_exist              BOOLEAN;
   L_line_seq_id        NUMBER;
   L_store_type         STORE.STORE_TYPE%TYPE;
   L_stockholding_ind   STORE.STOCKHOLDING_IND%TYPE;

   L_table              VARCHAR2(30) := 'SVC_POSUPLD_LOAD';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_STORE is
      select to_number(col_2),
             line_seq_id
        from svc_posupld_staging
       where sales_process_id = I_sales_process_id
         and file_type = 'FHEAD';

   cursor C_LOCK_FHEAD is
      select 1
        from svc_posupld_load
       where sales_process_id = I_sales_process_id
         and line_seq_id = L_line_seq_id
         for update nowait;

   cursor C_CHECK_STORE is
      select store_type,
             stockholding_ind
        from store
       where store = L_store;

BEGIN
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   O_result := 1;
   open  C_GET_STORE;
   fetch C_GET_STORE into L_store,
                          L_line_seq_id;
   close C_GET_STORE;
   -----------------------
   if L_store is NULL then
      O_result := 0;
      open C_LOCK_FHEAD;
      close C_LOCK_FHEAD;
      O_error_message := SQL_LIB.CREATE_MSG('SALEUPLD_MISSING_FHEAD',
                                            I_sales_process_id);
      update svc_posupld_load
         set error_msg = O_error_message
       where sales_process_id = I_sales_process_id
         and line_seq_id = L_line_seq_id;
      ---
      return TRUE;
   end if;
   -----------------------
   -- validate store
   open C_CHECK_STORE;
   fetch C_CHECK_STORE into L_store_type,
                            L_stockholding_ind;
   if C_CHECK_STORE%NOTFOUND then
      close C_CHECK_STORE;
      O_result := 0;
      open C_LOCK_FHEAD;
      close C_LOCK_FHEAD;
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                            I_sales_process_id);
      update svc_posupld_load
         set error_msg = O_error_message
       where sales_process_id = I_sales_process_id
         and line_seq_id = L_line_seq_id;
      ---
      return TRUE;
   end if;
   close C_CHECK_STORE;
   ---
   --non-stockholding franchise store transactions are rejected
   if L_store_type = 'F' and L_stockholding_ind = 'N' then
      O_result := 0;
      open C_LOCK_FHEAD;
      close C_LOCK_FHEAD;
      O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_COMP_STK_F_STORE',
                                            I_sales_process_id);
      update svc_posupld_load
         set error_msg = O_error_message
       where sales_process_id = I_sales_process_id
         and line_seq_id = L_line_seq_id;
      ---
      return TRUE;
   end if;
   -----------------------
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_sales_process_id,
                                            L_line_seq_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.VALIDATE_FHEAD',
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_FHEAD;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_FTAIL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result             IN OUT   NUMBER,
                        I_sales_process_id   IN       NUMBER)
RETURN BOOLEAN IS

   L_ftail_rec_num   NUMBER;
   L_total_recs      NUMBER;
   L_line_seq_id     NUMBER;

   L_table           VARCHAR2(30) := 'SVC_POSUPLD_LOAD';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_FTAIL_REC_CNT is
      select decode(CHECK_NUM_VAL(col_1), 'TRUE', to_number(col_1), 'FALSE', -1),
             line_seq_id
        from svc_posupld_staging
       where sales_process_id = I_sales_process_id
         and file_type = 'FTAIL';

   cursor C_GET_TOTAL_RECS is
      select count(*) - 2
        from svc_posupld_load
       where sales_process_id = I_sales_process_id;

   cursor C_LOCK_FTAIL is
      select 1
        from svc_posupld_load
       where sales_process_id = I_sales_process_id
         and line_seq_id = L_line_seq_id
         for update nowait;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_FTAIL_REC_CNT;
   fetch C_GET_FTAIL_REC_CNT into L_ftail_rec_num,
                                  L_line_seq_id;
   close C_GET_FTAIL_REC_CNT;
   -----------------------
   O_result := 1;
   if L_line_seq_id is null then
      open C_LOCK_FTAIL;
      close C_LOCK_FTAIL;

      O_error_message := SQL_LIB.CREATE_MSG('SALEUPLD_MISSING_FTAIL',
                                            I_sales_process_id);

      update svc_posupld_load
         set error_msg = O_error_message
       where sales_process_id = I_sales_process_id
         and line_seq_id = L_line_seq_id;

      O_result := 0;

      return TRUE;
   end if;
   if L_ftail_rec_num != -1 then

      open C_GET_TOTAL_RECS;
      fetch C_GET_TOTAL_RECS into L_total_recs;
      close C_GET_TOTAL_RECS;
   -----------------------
      if L_ftail_rec_num != L_total_recs then
         open C_LOCK_FTAIL;
         close C_LOCK_FTAIL;

         O_error_message := SQL_LIB.CREATE_MSG('INV_POSU_REC_COUNT',
                                               L_total_recs,
                                               L_ftail_rec_num);

         update svc_posupld_load
            set error_msg = O_error_message
          where sales_process_id = I_sales_process_id
            and line_seq_id = L_line_seq_id;

         O_result := 0;
      end if;
   -----------------------
   else
      open C_LOCK_FTAIL;
      close C_LOCK_FTAIL;

      O_error_message := SQL_LIB.CREATE_MSG('NON_NUMERIC',
                                            null,
                                            null);
      update svc_posupld_load
         set error_msg = O_error_message
       where sales_process_id = I_sales_process_id
         and line_seq_id = L_line_seq_id;

      O_result := 0;
   end if;
   -----------------------
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_sales_process_id,
                                            L_line_seq_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.VALIDATE_FTAIL',
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_FTAIL;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_PROM_TYPE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sales_process_id   IN       NUMBER,
                            I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into svc_posupld_tdetl_gtt target
   using(
         select tdetl.rowid gtt_row_id,
                case when cd.code is null then
                   'INV_PROM_TYPE'
				when tdetl.prom_tran_type = '1006' then
                   'INV_PROM_TYPE'
                end as prom_type_err
           from svc_posupld_tdetl_gtt tdetl,
                code_detail cd
          where tdetl.sales_process_id = I_sales_process_id
            and tdetl.chunk_id = I_chunk_id
            and cd.code_type(+) = 'PRMT'
            and tdetl.error_msg is null
            and tdetl.prom_tran_type = cd.code(+)) use_this
       on (target.rowid    = use_this.gtt_row_id)
     when matched then
     update set target.error_msg =    target.error_msg ||
      use_this.prom_type_err          ||decode(use_this.prom_type_err,null,null,';');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.VALIDATE_PROM_TYPE',
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_PROM_TYPE;
----------------------------------------------------------------------------------------
FUNCTION CHECK_RECS_INSERTED(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_recs_exist         IN OUT   BOOLEAN,
                             I_sales_process_id   IN       NUMBER,
                             I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

   L_ind   VARCHAR2(1) := NULL;

   cursor C_CHECK_ITEM_DET_REC is
      select 'x'
        from svc_posupld_line_item_gtt
       where sales_process_id = I_sales_process_id
         and chunk_id = I_chunk_id
         and rownum = 1;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_CHECK_ITEM_DET_REC;
   fetch C_CHECK_ITEM_DET_REC into L_ind;
   close C_CHECK_ITEM_DET_REC;

   if L_ind is not null then
      O_recs_exist := TRUE;
   else
      O_recs_exist := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.CHECK_RECS_INSERTED',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_RECS_INSERTED;
----------------------------------------------------------------------------------------
FUNCTION CHECK_ORDERABLE_DETL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sales_process_id   IN       NUMBER,
                              I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into svc_posupld_line_item_gtt target
   using(
         select pli.sales_thead_id,
                'STKCNT_NO_XFORM_INFO' ord_item_not_exsist
           from svc_posupld_line_item_gtt pli
          where pli.sales_process_id = I_sales_process_id
            and pli.chunk_id = I_chunk_id
            and pli.im_item_xform_ind = 'Y'
            and pli.xform_sellable_item is null
            and not exists (select 'x'
                              from item_xform_head   ixh,
                                   item_xform_detail ixd
                             where ixd.detail_item         = pli.im_item
                               and ixd.item_xform_head_id  = ixh.item_xform_head_id
                               and rownum = 1)) use_this
       on (target.sales_process_id = I_sales_process_id
       and target.chunk_id = I_chunk_id
       and target.sales_thead_id = use_this.sales_thead_id)
     when matched then
     update set target.error_msg =          target.error_msg ||
      use_this.ord_item_not_exsist          ||decode(use_this.ord_item_not_exsist,null,null,';');

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.CHECK_ORDERABLE_DETL',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ORDERABLE_DETL;
----------------------------------------------------------------------------------------
FUNCTION CHECK_BASE_PRICE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_sales_process_id   IN       NUMBER,
                          I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into svc_posupld_line_item_gtt target
   using(
         select pli.rowid gtt_row_id,
                'NO_RETAIL_ITEM' base_price_not_exist
           from svc_posupld_line_item_gtt pli
          where pli.sales_process_id = I_sales_process_id
            and pli.chunk_id = I_chunk_id
            and pli.xform_sellable_item      is null
            and pli.ph_curr_retail           is null
            and pli.ph_reg_retail            is null
            and pli.ph_prom_retail           is null
            and pli.ph_prom_ind              is null
            and pli.ph_clear_ind             is null
            and pli.old_retail_with_vat      is null
            and pli.old_retail_without_vat   is null
            and pli.wastage_amt_with_vat     is null
            and pli.wastage_amt_without_vat  is null
            and pli.error_msg                is null) use_this
       on (target.rowid = use_this.gtt_row_id)
     when matched then
     update set target.error_msg =          target.error_msg ||
      use_this.base_price_not_exist          ||decode(use_this.base_price_not_exist,null,null,';');

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.CHECK_BASE_PRICE',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_BASE_PRICE;
----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_SUPPLIER(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id   IN       NUMBER,
                             I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into svc_posupld_line_item_gtt target
   using(
      select pli.rowid gtt_row_id,
             'IS_NOT_EXIST' item_supp_not_exist
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id = I_chunk_id
         and pli.is_supplier is NULL
         and pli.pack_comp_ind = 'N'                            --this excludes the pack component records
         and ((pli.im_item_xform_ind != 'Y' and pli.IM_ORDERABLE_IND ='Y')
          or (pli.im_item_xform_ind = 'Y'
         and pli.xform_sellable_item is NOT NULL))) use_this    --this will get the record for a xform orderable item or a non-xform item
   on (target.rowid = use_this.gtt_row_id)
     when matched then
     update set target.error_msg = target.error_msg ||
      use_this.item_supp_not_exist||decode(use_this.item_supp_not_exist,NULL,NULL,';');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.CHECK_ITEM_SUPPLIER',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ITEM_SUPPLIER;
----------------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_sales_process_id   IN       NUMBER,
                       I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

   L_table         VARCHAR2(30) := 'SVC_POSUPLD_STATUS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STATUS is
      select 1
        from svc_posupld_status
       where process_id = I_sales_process_id
         and chunk_id = I_chunk_id
         for update nowait;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_LOCK_STATUS;
   close C_LOCK_STATUS;

   update svc_posupld_status sps
      set status = 'R'
    where sps.process_id = I_sales_process_id
      and sps.chunk_id = I_chunk_id
      and exists (select 1
                    from svc_posupld_line_item_gtt pli,
                         svc_posupld_staging sp
                   where pli.sales_process_id = I_sales_process_id
                     and pli.chunk_id = I_chunk_id
                     and pli.sales_process_id = sp.sales_process_id
                     and pli.chunk_id = sp.chunk_id
                     and pli.sales_thead_id = sp.line_seq_id
                     and sp.location = sps.location                     
                     and pli.error_msg is not null
                     and rownum = 1
                   union
                  select 1
                    from svc_posupld_thead_gtt phg,
                         svc_posupld_staging sp
                   where phg.sales_process_id = I_sales_process_id
                     and phg.chunk_id = I_chunk_id
                     and phg.sales_process_id = sp.sales_process_id
                     and phg.chunk_id = sp.chunk_id
                     and phg.sales_thead_id = sp.line_seq_id
                     and sp.location = sps.location
                     and phg.error_msg is not null
                     and rownum = 1
                   union
                  select 1
                    from svc_posupld_tdetl_gtt pdg,
                         svc_posupld_staging sp
                   where pdg.sales_process_id = I_sales_process_id
                     and pdg.chunk_id = I_chunk_id
                     and pdg.sales_process_id = sp.sales_process_id
                     and pdg.chunk_id = sp.chunk_id
                     and pdg.sales_tdetl_id = sp.line_seq_id
                     and sp.location = sps.location
                     and pdg.error_msg is not null
                     and rownum = 1
                   union
                  select 1
                    from svc_posupld_ttax_gtt ptg,
                         svc_posupld_staging sp
                   where ptg.sales_process_id = I_sales_process_id
                     and ptg.chunk_id = I_chunk_id
                     and ptg.sales_process_id = sp.sales_process_id
                     and ptg.chunk_id = sp.chunk_id
                     and ptg.sales_ttax_id = sp.line_seq_id
                     and sp.location = sps.location
                     and ptg.error_msg is not null
                     and rownum = 1);

   update svc_posupld_status sps
      set status = 'C'
    where sps.process_id = I_sales_process_id
      and sps.chunk_id = I_chunk_id
      and status = 'N';

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_sales_process_id,
                                            I_chunk_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.UPDATE_STATUS',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_STATUS;
----------------------------------------------------------------------------------------
FUNCTION COPY_ERROR_DETAILS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sales_process_id   IN       NUMBER,
                            I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

   L_table         VARCHAR2(30) := 'SVC_POSUPLD_LOAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_LOAD is
      select 'x'
        from svc_posupld_load load
       where sales_process_id = I_sales_process_id
         and exists (select 'x'                   --lock only those svc_posupld_load lines that are part of the transaction processed in the chunk_id.
                       from svc_posupld_thead_gtt thead
                      where load.line_seq_id = thead.sales_thead_id
                        and thead.sales_process_id = I_sales_process_id
                        and thead.chunk_id = I_chunk_id
                        and rownum = 1)
         for update nowait;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_LOCK_LOAD;
   close C_LOCK_LOAD;

   merge into svc_posupld_load target
   using(
        select pli.sales_thead_id,
               pli.error_msg
          from svc_posupld_line_item_gtt pli
         where pli.sales_process_id = I_sales_process_id
           and pli.chunk_id = I_chunk_id
           and pli.error_msg is not null
           and pli.pack_comp_ind = 'N' 
           and pli.xform_sellable_item is null) use_this
      on (target.sales_process_id = I_sales_process_id
      and target.line_seq_id = use_this.sales_thead_id)
    when matched then
    update set target.error_msg = target.error_msg || use_this.error_msg;

   merge into svc_posupld_load target
   using(
        select thead.sales_thead_id,
               thead.error_msg
          from svc_posupld_thead_gtt thead
         where thead.sales_process_id = I_sales_process_id
           and thead.chunk_id = I_chunk_id
           and thead.error_msg is not null) use_this
      on (target.sales_process_id = I_sales_process_id
      and target.line_seq_id = use_this.sales_thead_id)
    when matched then
    update set target.error_msg = target.error_msg || use_this.error_msg;

   merge into svc_posupld_load target
   using(
         select ttax.sales_ttax_id,
                ttax.error_msg
           from svc_posupld_ttax_gtt ttax
          where ttax.sales_process_id = I_sales_process_id
            and ttax.chunk_id = I_chunk_id
            and ttax.error_msg is not null) use_this
       on (target.sales_process_id = I_sales_process_id
       and target.line_seq_id = use_this.sales_ttax_id)
     when matched then
     update set target.error_msg = target.error_msg || use_this.error_msg;

   merge into svc_posupld_load target
   using(
        select tdetl.sales_tdetl_id,
               tdetl.error_msg
          from svc_posupld_tdetl_gtt tdetl
         where tdetl.sales_process_id = I_sales_process_id
           and tdetl.chunk_id = I_chunk_id
           and tdetl.error_msg is not null) use_this
      on (target.sales_process_id = I_sales_process_id
      and target.line_seq_id = use_this.sales_tdetl_id)
    when matched then
    update set target.error_msg = target.error_msg || use_this.error_msg;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_sales_process_id,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.COPY_ERROR_DETAILS',
                                            to_char(SQLCODE));
      return FALSE;
END COPY_ERROR_DETAILS;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_PROM_INFO(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sales_process_id   IN       NUMBER,
                            I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into svc_posupld_tdetl_gtt target
   using(
         select tdetl.rowid gtt_row_id,
                case when (tdetl.prom_no is not null and tdetl.prom_component is not null) and rpc.promo_comp_id is null then
                   'INV_PROMO'
                end as prom_type_err
           from svc_posupld_tdetl_gtt tdetl,
                rpm_promo_comp_v rpc
          where tdetl.sales_process_id = I_sales_process_id
            and tdetl.chunk_id = I_chunk_id
            and to_number(tdetl.prom_no)        = rpc.promo_id(+)
            and to_number(tdetl.prom_component) = rpc.promo_comp_id(+)
            and tdetl.prom_tran_type = 9999
            and tdetl.error_msg is null) use_this
      on (target.rowid = use_this.gtt_row_id)
    when matched then
    update set target.error_msg =    target.error_msg ||
     use_this.prom_type_err          ||decode(use_this.prom_type_err,null,null,';');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.VALIDATE_PROM_INFO',
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_PROM_INFO;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_ROWS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_sales_process_id   IN       NUMBER,
                       I_chunk_id           IN       NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into svc_posupld_thead_gtt target
   using (select head_detl_id,
                 total_det,
                 head_count,
                 case when total_det != head_count then
                    'INV_TTAIL_REC_COUNT' ||' total: '||head_count||' TTAIL: '||total_det
                 end ttail_err
            from (select file_type,
                         decode(file_type,'TTAIL',to_number(col_1)) total_det,
                         head_detl_id,
                         count(head_detl_id) over (partition by head_detl_id) - 1 head_count
                    from svc_posupld_staging
                   where sales_process_id = I_sales_process_id
                     and chunk_id = I_chunk_id
                     and file_type in ('TTAIL', 'TDETL', 'TTAX'))
           where file_type = 'TTAIL') use_this
    on (target.sales_process_id = I_sales_process_id
    and target.chunk_id = I_chunk_id
    and target.sales_thead_id = use_this.head_detl_id)
    when matched then
   update set target.error_msg =      target.error_msg ||
      use_this.ttail_err          ||decode(use_this.ttail_err,null,null,';');
   ---
   merge into svc_posupld_thead_gtt target
   using (
      select line_seq_id,
             case FILE_TYPE when 'THEAD' then
                case CHECK_DATE_VAL(substr(col_1, 1, 8), 'YYYYMMDD') when 'FALSE' then
                   'INV_TRAN_DATE'
                end
             end as tran_date_err,
             case FILE_TYPE when 'THEAD' then
                case CHECK_NUM_VAL(col_14) when 'FALSE' then
                   'INV_SALES_VALUE'
                end
             end as sales_val_err,
             case FILE_TYPE when 'THEAD' then
                case CHECK_NUM_VAL(col_17) when 'FALSE' then
                   'INV_SALES_QTY'
                end
             end as sales_qty_err,
             case FILE_TYPE when 'THEAD' then
                case when col_19 is not null then
                   case when col_19 not in ('N', 'Y') then
                      'INV_CW_IND'
                   end
                end
             end as cw_ind_err,
             case FILE_TYPE when 'THEAD' then
                case when col_20 is not null then
                   case CHECK_NUM_VAL(col_20) when 'FALSE' then
                      'INV_CW_ACT_WT'
                   end
                end
             end as cw_act_wt_err,
             case FILE_TYPE when 'THEAD' then
                case when LP_system_options_rec.sales_audit_ind = 'Y' then
                   case CHECK_NUM_VAL(col_4) when 'FALSE' then
                      'POS_INV_DEPT'
                   end
                end
             end as dept_err,
             case FILE_TYPE when 'THEAD' then
                case when LP_system_options_rec.sales_audit_ind = 'Y' then
                   case CHECK_NUM_VAL(col_5) when 'FALSE' then
                      'POS_INV_CLASS'
                   end
                end
             end as class_err,
             case FILE_TYPE when 'THEAD' then
                case when LP_system_options_rec.sales_audit_ind = 'Y' then
                   case CHECK_NUM_VAL(col_6) when 'FALSE' then
                      'POS_INV_SUBCLASS'
                   end
                end
             end as subclass_err,
             case FILE_TYPE when 'THEAD' then
                case when LP_system_options_rec.sales_audit_ind = 'Y' then
                   case CHECK_NUM_VAL(col_8) when 'FALSE' then
                      'INV_ITEM_LVL'
                   end
                end
             end as item_level_err,
             case FILE_TYPE when 'THEAD' then
                case when LP_system_options_rec.sales_audit_ind = 'Y' then
                   case CHECK_NUM_VAL(col_9) when 'FALSE' then
                      'INV_TRAN_LVL'
                   end
                end
             end as tran_level_err,
             case FILE_TYPE when 'THEAD' then
                case when LP_system_options_rec.sales_audit_ind = 'Y' then
                   case when col_10 is not null then
                      case when col_10 not in ('SL', 'SP') then
                         'INV_WASTAGE_TYPE'
                      end
                   end
                end
             end as waste_type_err,
             case FILE_TYPE when 'THEAD' then
                case when LP_system_options_rec.sales_audit_ind = 'Y' then
                   case when col_10 is not null then
                      case when col_10 = 'SL' then
                         case CHECK_NUM_VAL(col_11) when 'FALSE' then
                            'INV_WASTAGE_PCT'
                         end
                      end
                   end
                end
             end as waste_pct_err,
             case FILE_TYPE when 'THEAD' then
                case when col_21 is not null then
                   case when col_21 not in ('A', 'D') then
                     'INV_SUBTRANS'
                   end
                end
             end as sub_tran_err
        from svc_posupld_staging
       where sales_process_id = I_sales_process_id
         and chunk_id = I_chunk_id) use_this
   on (target.sales_process_id = I_sales_process_id
   and target.chunk_id = I_chunk_id
   and target.sales_thead_id = use_this.line_seq_id)
   when matched then
   update set target.error_msg =      target.error_msg ||
      use_this.tran_date_err          ||decode(use_this.tran_date_err,null,null,';')||
      use_this.sales_val_err          ||decode(use_this.sales_val_err,null,null,';')||
      use_this.sales_qty_err          ||decode(use_this.sales_qty_err,null,null,';')||
      use_this.cw_ind_err             ||decode(use_this.cw_ind_err,null,null,';')||
      use_this.cw_act_wt_err          ||decode(use_this.cw_act_wt_err,null,null,';')||
      use_this.dept_err               ||decode(use_this.dept_err,null,null,';')||
      --
      use_this.class_err              ||decode(use_this.class_err,null,null,';')||
      use_this.subclass_err           ||decode(use_this.subclass_err,null,null,';')||
      use_this.item_level_err         ||decode(use_this.item_level_err,null,null,';')||
      use_this.tran_level_err         ||decode(use_this.tran_level_err,null,null,';')||
      use_this.waste_type_err         ||decode(use_this.waste_type_err,null,null,';')||
      use_this.waste_pct_err          ||decode(use_this.waste_pct_err,null,null,';')||
      use_this.sub_tran_err           ||decode(use_this.sub_tran_err,null,null,';');

   merge into svc_posupld_thead_gtt target
   using (
      select line_seq_id,
             case when col_4 is not null and LP_system_options_rec.sales_audit_ind = 'Y' then
                decode(NVL(c.dept, -999), -999, 'POS_INV_MERCH_HIER')
             else
                null
             end as inv_dept_err
        from svc_posupld_staging,
             class c
       where sales_process_id = I_sales_process_id
         and chunk_id = I_chunk_id
         and file_type = 'THEAD'
         and error_msg is null
         and c.dept (+)= to_number(col_4)
         and c.class(+)= to_number(col_5)) use_this
   on (target.sales_process_id = I_sales_process_id
   and target.chunk_id = I_chunk_id
   and target.sales_thead_id = use_this.line_seq_id)
   when matched then
   update set target.error_msg =      target.error_msg ||
      use_this.inv_dept_err     ||decode(use_this.inv_dept_err,null,null,';');

   merge into svc_posupld_ttax_gtt target
   using (
      select line_seq_id,
             case when col_2 is not null then
                case CHECK_NUM_VAL(col_2) when 'FALSE' then
                   'INV_TAX_RATE'
                end
             else
                'INV_TAX_RATE'
             end as tax_rate_err,
             case when col_3 is not null then
                case CHECK_NUM_VAL(col_3) when 'FALSE' then
                   'INV_TAX_AMT'
                end
             else
                'INV_TAX_AMT'
             end as tax_amt_err
        from svc_posupld_staging
       where sales_process_id = I_sales_process_id
         and chunk_id = I_chunk_id
         and file_type = 'TTAX') use_this
   on (target.sales_process_id = I_sales_process_id
   and target.chunk_id = I_chunk_id
   and target.sales_ttax_id = use_this.line_seq_id)
   when matched then
      update set target.error_msg = target.error_msg ||
         use_this.tax_rate_err ||decode(use_this.tax_rate_err,null,null,';')||
         use_this.tax_amt_err  ||decode(use_this.tax_amt_err,null,null,';');

   merge into svc_posupld_tdetl_gtt target
   using (
      select line_seq_id,
             case FILE_TYPE when 'TDETL' then
                case when col_3 is not null then
                   case CHECK_NUM_VAL(col_3) when 'FALSE' then
                     'INV_SALES_QTY_DISC'
                   end
                end
             end as sales_qty_disc_err,
             case FILE_TYPE when 'TDETL' then
                case when col_4 is not null then
                   case CHECK_NUM_VAL(col_4) when 'FALSE' then
                     'INV_SALES_VAL_DISC'
                   end
                end
             end as sales_value_disc_err,
             case FILE_TYPE when 'TDETL' then
                case when col_5 is not null then
                   case CHECK_NUM_VAL(col_5) when 'FALSE' then
                     'INV_TOT_VAL_DISC'
                   end
                end
             end as disc_value_err,
             case FILE_TYPE when 'TDETL' then
                case when col_1 is not null then
                   case when col_1 = '9999' then
                      case CHECK_NUM_VAL(col_2) when 'FALSE' then
                         'INV_PROM_TYPE'
                      end
                   end
                     else
						'INV_PROM_TYPE'
                end
             end as prom_type_err,
             case FILE_TYPE when 'TDETL' then
                case when col_1 is not null then
                   case when col_1 = '9999' then
                      case CHECK_NUM_VAL(col_6) when 'FALSE' then
                         'INV_PROM_COMP'
                      end
                   end
                end
             end as prom_comp_err
        from svc_posupld_staging
       where sales_process_id = I_sales_process_id
         and chunk_id = I_chunk_id) use_this
   on (target.sales_process_id = I_sales_process_id
   and target.chunk_id = I_chunk_id
   and target.sales_tdetl_id = use_this.line_seq_id)
   when matched then
   update set target.error_msg =      target.error_msg ||
      use_this.sales_qty_disc_err     ||decode(use_this.sales_qty_disc_err,null,null,';')||
      use_this.sales_value_disc_err   ||decode(use_this.sales_value_disc_err,null,null,';')||
      use_this.disc_value_err         ||decode(use_this.disc_value_err,null,null,';')||
      use_this.prom_type_err          ||decode(use_this.prom_type_err,null,null,';')||
      use_this.prom_comp_err          ||decode(use_this.prom_comp_err,null,null,';');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.VALIDATE_ROWS',
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_ROWS;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_TAX(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_sales_process_id   IN      NUMBER,
                      I_chunk_id           IN      NUMBER)
RETURN BOOLEAN IS

L_vat_calc_type       vat_region.vat_calc_type%TYPE;

    Cursor C_GET_VAT_CALC_TYPE is
       select vr.vat_calc_type
         from svc_posupld_staging svc,
              vat_region vr,
              store st
        where to_number(svc.col_2) = st.store
          and st.vat_region        = vr.vat_region
          and svc.sales_process_id = I_sales_process_id
          and svc.file_type        = 'FHEAD';

BEGIN
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_VAT_CALC_TYPE;
   fetch C_GET_VAT_CALC_TYPE into L_vat_calc_type;
   close C_GET_VAT_CALC_TYPE;
   
   if LP_system_options_rec.default_tax_type in ('SVAT','GTAX') and nvl(L_vat_calc_type,'X') <> 'E' then
      merge into svc_posupld_ttax_gtt target
      using (
         select spt.rowid gtt_row_id,
                case when spt.tax_code is not null then
                   case when vc.vat_code is null then
                      'INV_VAT_CODE'
                   end
                else
                   'INV_VAT_CODE'
                end as tax_code_err
           from svc_posupld_ttax_gtt spt,
                vat_codes vc
          where spt.sales_process_id = I_sales_process_id
            and spt.chunk_id = I_chunk_id
            and vc.vat_code(+) = spt.tax_code) use_this
      on (target.rowid = use_this.gtt_row_id)
      when matched then
         update set target.error_msg = target.error_msg ||
            use_this.tax_code_err ||decode(use_this.tax_code_err,null,null,';');

   elsif (LP_system_options_rec.default_tax_type = 'SALES' or (LP_system_options_rec.default_tax_type = 'SVAT' and nvl(L_vat_calc_type,'X') = 'E')) then
      merge into svc_posupld_ttax_gtt target
      using (
         select spt.rowid gtt_row_id,
                case when spt.tax_code is not null then
                   case when cd.code is null then
                      'INV_TAXCODE'
                   end
                else
                   'INV_TAXCODE'
                end as tax_code_err
           from svc_posupld_ttax_gtt spt,
                code_detail cd
          where spt.sales_process_id = I_sales_process_id
            and spt.chunk_id = I_chunk_id
            and cd.code_type(+) = 'TAXC'
            and cd.code(+) = spt.tax_code) use_this
      on (target.rowid = use_this.gtt_row_id)
      when matched then
         update set target.error_msg = target.error_msg ||
            use_this.tax_code_err ||decode(use_this.tax_code_err,null,null,';');

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
   if C_GET_VAT_CALC_TYPE% ISOPEN then
      close C_GET_VAT_CALC_TYPE;
   end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.VALIDATE_TAX',
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_TAX;
----------------------------------------------------------------------------------------
FUNCTION SETUP_WORK_TABLE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_sales_process_id   IN              NUMBER,
                          I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   -- INSERT into svc_posupld_fhead_gtt file header of the POSU file
   merge into svc_posupld_fhead_gtt target
   using (select sales_process_id,
                 col_1,
                 col_2,
                 col_3,
                 col_4,
                 col_5,
                 col_6
            from svc_posupld_staging
           where sales_process_id = I_sales_process_id
             and file_type = 'FHEAD') use_this
      on (target.sales_process_id = use_this.sales_process_id)
      when not matched then
         insert (sales_process_id,
                 fdate,
                 loc_value,
                 vat_include,
                 vat_region,
                 currency_code,
                 currency_rtl_dec,
                 error_msg) 
         values (use_this.sales_process_id,
                 col_1,
                 col_2,
                 col_3,
                 col_4,
                 col_5,
                 col_6,
                 null);

   ---------------------------

   -- insert int svc_posupld_thead_gtt THEAD rows
   insert all
      when (file_type = 'THEAD') then
         into svc_posupld_thead_gtt (sales_process_id,
                                     sales_thead_id,
                                     chunk_id,
                                     run_seq,
                                     tran_date,
                                     item_type,
                                     item_value,
                                     dept,
                                     class,
                                     subclass,
                                     pack_ind,
                                     item_level,
                                     tran_level,
                                     waste_type,
                                     waste_pct,
                                     tran_type,
                                     dropship_ind,
                                     sales_qty,
                                     selling_uom,
                                     sales_sign,
                                     sales_value,
                                     last_modified_date,
                                     catchweight_ind,
                                     actualweight_qty,
                                     subtrans_type_ind,
                                     gtax_value,
                                     resa_sales_type,
                                     no_inv_ret_ind,
                                     return_disposition,
                                     return_wh,
                                     error_msg)
         values(sales_process_id,
                thead_id,
                chunk_id,
                run_seq,
                col_1,
                col_2,
                col_3,
                col_4,
                col_5,
                col_6,
                col_7 ,
                col_8 ,
                col_9 ,
                col_10,
                col_11,
                col_12,
                col_13,
                col_14,
                col_15,
                col_16,
                col_17,
                col_18,
                col_19,
                col_20,
                col_21,
                col_22,
                col_23,
                col_24,
                col_25,
                col_26,
                null)
      when (file_type = 'TTAX') then
         into svc_posupld_ttax_gtt (sales_process_id,
                                    sales_thead_id,
                                    sales_ttax_id,
                                    chunk_id,
                                    tax_code,
                                    tax_rate,
                                    tax_amt,
                                    error_msg)
         values(sales_process_id,
                head_detl_id,
                ttax_id,
                chunk_id,
                col_1,
                col_2,
                col_3,
                null)
      when (file_type = 'TDETL') then
         into svc_posupld_tdetl_gtt (sales_process_id,
                                     sales_thead_id,
                                     sales_tdetl_id,
                                     chunk_id,
                                     prom_tran_type,
                                     prom_no,
                                     sales_qty,
                                     sales_value,
                                     discount_value,
                                     prom_component,
                                     error_msg)
         values(sales_process_id,
                head_detl_id,
                tdetl_id,
                chunk_id,
                col_1 ,
                col_2,
                col_3,
                col_4,
                col_5,
                col_6,
                null)
   select sales_process_id,
          thead_id,
          chunk_id,
          1 run_seq,
          tdetl_id,
          file_type,
          head_detl_id,
          ttax_id,
          col_1,
          col_2 ,
          col_3 ,
          col_4 ,
          col_5 ,
          col_6 ,
          col_7 ,
          col_8 ,
          col_9 ,
          col_10,
          col_11,
          col_12,
          col_13,
          col_14,
          col_15,
          col_16,
          col_17,
          col_18,
          col_19,
          col_20,
          col_21,
          col_22,
          col_23,
          col_24,
          col_25,
          col_26
     from svc_posupld_staging
    where sales_process_id = I_sales_process_id
      and chunk_id = I_chunk_id
      and error_msg is null;

   if VALIDATE_ROWS(O_error_message,
                    I_sales_process_id,
                    I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if VALIDATE_TAX(O_error_message,
                   I_sales_process_id,
                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if VALIDATE_PROM_TYPE(O_error_message,
                         I_sales_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if LP_system_options_rec.rpm_ind = 'Y' then 
		if VALIDATE_PROM_INFO(O_error_message,
                         I_sales_process_id,
                         I_chunk_id) = FALSE then
		return FALSE;
   end if;
 end if;  

   --move data from staging tables to working table - gather extra data about svc_posupld_fhead_gtt
   --at the same time.
   insert into svc_posupld_line_item_gtt (posupld_line_item_id,
                                          sales_process_id,
                                          sales_thead_id,
                                          chunk_id,
                                          --
                                          pack_no,
                                          pack_comp_ind,
                                          pack_comp_item,
                                          pack_qty,
                                          pack_price_ratio,
                                          fdate,
                                          loc_value,
                                          tran_date,
                                          item_type,
                                          item_value,
                                          im_dept,
                                          im_class,
                                          im_subclass,
                                          im_pack_ind,
                                          im_item_level,
                                          im_tran_level,
                                          im_waste_type,
                                          im_waste_pct,
                                          tran_type_char,       --THEAD.tran_type 'S'ale or 'R'eturn
                                          tran_type_num,        --based on THEAD.tran_type: 1 for Sale, -1 for Return
                                          dropship_ind,
                                          --
                                          sales_qty,            --this accounts for THEAD.sales_sign
                                          selling_uom,
                                          sales_sign_char,      --THEAD.sales_sign 'P'ositive or 'N'egative
                                          sales_sign_num,       --based on THEAD.sales_sign: 1 for Positive, -1 for Negative
                                          sales_value,          --this accounts for THEAD.sales_sign
                                          last_modified_date,
                                          catchweight_ind,
                                          actualweight_qty,
                                          subtrans_type_ind,
                                          --
                                          st_vat_include_ind,
                                          st_currency_code,
                                          st_store_type,
                                          st_currency_rtl_dec,
                                          st_currency_cost_dec,
                                          st_vat_region,
                                          --
                                          sales_qty_std_uom,     --this accounts for THEAD.sales_sign, same as sales_qty
                                          --
                                          xform_prod_loss_pct,
                                          xform_yield_pct,
                                          uom_ratio,
                                          gtax_value,
                                          gtax_unit_value,
                                          resa_sales_type,
                                          no_inv_ret_ind,
                                          return_disposition,
                                          return_wh,
                                          --
                                          error_msg)
      select posupld_line_item_id_seq.nextval,
             fhead.sales_process_id,
             thead.sales_thead_id,
             thead.chunk_id,
             null,
             'N',    -- not a pack
             null,
             1,      -- pack_qty - put to one so can use in statements with out checking if its a pack comp
             1,      -- pack_price_ratio - put to one so can use in statements with out checking if its a pack comp
             to_date(fhead.fdate, 'YYYYMMDDHH24MISS'),
             to_number(fhead.loc_value),
             --
             trunc(to_date(thead.tran_date, 'YYYYMMDDHH24MISS')),
             thead.item_type,
             thead.item_value,
             thead.dept,
             thead.class,
             thead.subclass,
             thead.pack_ind,
             thead.item_level,
             thead.tran_level,
             thead.waste_type,
             to_number(thead.waste_pct)/1000000,
             thead.tran_type,
             decode(thead.tran_type,'R',-1,1),
             NVL(thead.dropship_ind, 'N'),
             to_number(thead.sales_qty)/10000 * decode(thead.sales_sign,'N',-1,1),
             thead.selling_uom,
             thead.sales_sign,
             decode(thead.sales_sign,'N',-1,1),
             to_number(thead.sales_value)/10000 * decode(thead.sales_sign,'N',-1,1),
             to_date(thead.last_modified_date, 'YYYYMMDDHH24MISS'),
             thead.catchweight_ind,
             to_number(thead.actualweight_qty)/10000 * decode(thead.sales_sign,'N',-1,1),
             thead.subtrans_type_ind,
             --
             decode(LP_system_options_rec.sales_audit_ind,
                    'Y', NVL(fhead.vat_include,'N'),
                    NVL(s.vat_include_ind,'N')) vat_include,
             decode(LP_system_options_rec.sales_audit_ind,
                    'Y',fhead.currency_code,
                    s.currency_code) currency_code,
             s.store_type,             
             decode(LP_system_options_rec.sales_audit_ind,
                    'Y',nvl(fhead.currency_rtl_dec,c.currency_rtl_dec),
                    c.currency_rtl_dec) currency_rtl_dec,
             c.currency_cost_dec currency_cost_dec,
             decode(LP_system_options_rec.sales_audit_ind,
                    'Y',fhead.vat_region,
                    s.vat_region) vat_region,
             --
             to_number(thead.sales_qty)/10000 * decode(thead.sales_sign,'N',-1,1),
             --
             0 xform_prod_loss_pct,
             1 xform_yield_pct,
             1 uom_ratio,
             to_number(thead.gtax_value) / 10000 * decode(thead.sales_sign,'N',-1,1),
             ((to_number(thead.gtax_value) / 10000) / (to_number(thead.sales_qty)/10000)) * decode(thead.sales_sign,'N',-1,1),
             thead.resa_sales_type,
             thead.no_inv_ret_ind,
             thead.return_disposition,
             thead.return_wh,
             --
             thead.error_msg
        from svc_posupld_fhead_gtt fhead,
             svc_posupld_thead_gtt thead,
             store s,
             currencies c
       where fhead.sales_process_id = I_sales_process_id
         and thead.chunk_id = I_chunk_id
         and fhead.error_msg is null
         and thead.error_msg is null
         and not exists (select 1
                           from svc_posupld_tdetl_gtt tdetl
                          where tdetl.sales_process_id = I_sales_process_id
                            and tdetl.chunk_id = I_chunk_id
                            and tdetl.sales_thead_id = thead.sales_thead_id
                            and tdetl.error_msg is not null
                            and rownum = 1)
         and not exists (select 1
                           from svc_posupld_ttax_gtt ttax
                          where ttax.sales_process_id = I_sales_process_id
                            and ttax.chunk_id = I_chunk_id
                            and ttax.sales_thead_id = thead.sales_thead_id
                            and ttax.error_msg is not null
                            and rownum = 1)
         and fhead.sales_process_id = thead.sales_process_id
         and fhead.loc_value        = s.store(+)
         and s.currency_code        = c.currency_code(+);

   --
   merge into svc_posupld_line_item_gtt work
   using (
      select pli.rowid gtt_row_id,
             decode(pli.item_type,'REF',im.item_parent,pli.item_value) item,
             decode(pli.item_type,'REF',pli.item_value,null) ref_item
        from svc_posupld_line_item_gtt pli,
             item_master im
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and pli.item_value       = im.item) use_this
   on (work.rowid = use_this.gtt_row_id)
   when matched then
      update set work.im_item     = use_this.item,
                 work.im_ref_item = use_this.ref_item;

   -- get orderable xform items for the sellable xform items that have been sold
   insert into svc_posupld_line_item_gtt (posupld_line_item_id,
                                          sales_process_id,
                                          sales_thead_id,
                                          chunk_id,
                                          pack_no,
                                          pack_comp_ind,
                                          pack_comp_item,
                                          pack_qty,
                                          pack_price_ratio,
                                          fdate,
                                          loc_value,
                                          tran_date,
                                          item_type,
                                          item_value,    --this will hold the xform orderable item
                                          im_item,
                                          im_dept,
                                          im_class,
                                          im_subclass,
                                          im_pack_ind,
                                          im_item_level,
                                          im_tran_level,
                                          im_waste_type,
                                          im_waste_pct,
                                          tran_type_char,
                                          tran_type_num,
                                          dropship_ind,
                                          sales_qty,
                                          selling_uom,
                                          sales_sign_char,
                                          sales_sign_num,
                                          sales_value,
                                          last_modified_date,
                                          catchweight_ind,
                                          actualweight_qty,
                                          subtrans_type_ind,
                                          --
                                          gtax_value,
                                          gtax_unit_value,
                                          --
                                          st_vat_include_ind,
                                          st_currency_code,
                                          st_store_type,
                                          st_currency_rtl_dec,
                                          st_currency_cost_dec,
                                          st_vat_region,
                                          --
                                          sales_qty_std_uom,
                                          --
                                          xform_sellable_item,   --this holds the original xform sellable item on POSU
                                          xform_prod_loss_pct,
                                          xform_yield_pct,
                                          uom_ratio,
                                          resa_sales_type,
                                          no_inv_ret_ind,
                                          return_disposition,
                                          return_wh,
                                          --
                                          error_msg)
      select posupld_line_item_id_seq.nextval,
             pli.sales_process_id,
             pli.sales_thead_id,
             pli.chunk_id,
             --
             null,   -- not a pack item
             'N',    -- pack indicator
             null,   -- no pack components
             1,      -- pack_qty - defaulted to 1 so can use in statements with out checking if its a pack comp
             1,      -- pack_price_ratio - put to one so can use in statements with out checking if its a pack comp
             pli.fdate,
             pli.loc_value,
             pli.tran_date,
             pli.item_type,
             ixh.head_item,    --xform orderable, inserted as item_value on svc_posupld_line_item_gtt for a new orderable record
             ixh.head_item im_item,
             pli.im_dept,      --xform orderable is getting the sellable's dept/class/... attributes but will be updated in work_table_denormalize
             pli.im_class,     
             pli.im_subclass,
             pli.im_pack_ind,
             pli.im_item_level,
             pli.im_tran_level,
             pli.im_waste_type,
             pli.im_waste_pct,
             pli.tran_type_char,
             pli.tran_type_num,
             pli.dropship_ind,
             pli.sales_qty,
             pli.selling_uom,
             pli.sales_sign_char,
             pli.sales_sign_num,
             pli.sales_value,
             pli.last_modified_date,
             pli.catchweight_ind,
             pli.actualweight_qty,
             pli.subtrans_type_ind,
             --
             pli.gtax_value,         --this is total gtax value copied from the xform sellable, not the orderable
             pli.gtax_unit_value,    --this is unit gtax value copied from the xform sellable, not the orderable
             --
             pli.st_vat_include_ind,
             pli.st_currency_code,
             pli.st_store_type,
             pli.st_currency_rtl_dec,
             pli.st_currency_cost_dec,
             pli.st_vat_region,
             --
             pli.sales_qty_std_uom,
             --
             pli.item_value xform_sellable_item,          --holds the original item on POSU
             NVL(ixh.production_loss_pct / 100.0,0),
             NVL(ixd.yield_from_head_item_pct / 100.0,1),
             1 uom_ratio,
             pli.resa_sales_type,
             pli.no_inv_ret_ind,
             pli.return_disposition,
             pli.return_wh,
             --
             pli.error_msg
        from svc_posupld_line_item_gtt pli,
             item_xform_head ixh,
             item_xform_detail ixd
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id = I_chunk_id
         and ixd.detail_item  = pli.im_item
         and ixd.item_xform_head_id = ixh.item_xform_head_id
         and pli.error_msg is null;

   --insert the pack component items--
   insert into svc_posupld_line_item_gtt (posupld_line_item_id,
                                          sales_process_id,
                                          sales_thead_id,
                                          chunk_id,
                                          pack_no,            --this holds the pack (item on the POSU)
                                          pack_comp_ind,
                                          pack_comp_item,     --this holds the component item
                                          pack_qty,
                                          pack_price_ratio,   --defaulted to 1 for now
                                          fdate,
                                          loc_value,
                                          tran_date,
                                          item_type,
                                          item_value,         --this holds the component item
                                          im_item,
                                          im_dept,
                                          im_class,
                                          im_subclass,
                                          im_pack_ind,
                                          im_item_level,
                                          im_tran_level,
                                          im_waste_type,
                                          im_waste_pct,
                                          tran_type_char,
                                          tran_type_num,
                                          dropship_ind,
                                          sales_qty,
                                          selling_uom,
                                          sales_sign_char,
                                          sales_sign_num,
                                          sales_value,
                                          last_modified_date,
                                          catchweight_ind,
                                          actualweight_qty,
                                          subtrans_type_ind,
                                          --
                                          gtax_value,
                                          gtax_unit_value,
                                          --
                                          st_vat_include_ind,
                                          st_currency_code,
                                          st_store_type,
                                          st_currency_rtl_dec,
                                          st_currency_cost_dec,
                                          st_vat_region,
                                          --
                                          sales_qty_std_uom,
                                          --
                                          xform_prod_loss_pct,
                                          xform_yield_pct,
                                          uom_ratio,
                                          resa_sales_type,
                                          no_inv_ret_ind,
                                          return_disposition,
                                          return_wh,
                                          --
                                          error_msg)
       select posupld_line_item_id_seq.nextval,
              pli.sales_process_id,
              pli.sales_thead_id,
              pli.chunk_id,
              pli.item_value,     --pli.pack_no
              'Y',                --pli.pack_comp_ind,
              vpq.item,           --pli.pack_comp_item,
              vpq.qty,            --pli.pack_qty,
              1,                  --pli.pack_price_ratio - this will be recomputed as needed in WORK_TABLE_DENORMALIZE
              pli.fdate,
              pli.loc_value,
              pli.tran_date,
              pli.item_type,
              vpq.item,
              vpq.item im_item,
              pli.im_dept,
              pli.im_class,
              pli.im_subclass,
              'N'  im_pack_ind,
              pli.im_item_level,
              pli.im_tran_level,
              pli.im_waste_type,
              pli.im_waste_pct,
              pli.tran_type_char,
              pli.tran_type_num,
              pli.dropship_ind,
              pli.sales_qty,           --this is sales_qty at the pack level, not at the component level
              pli.selling_uom,
              pli.sales_sign_char,
              pli.sales_sign_num,
              pli.sales_value,         --this is sales_value at the pack level, not prorated to components
              pli.last_modified_date,
              pli.catchweight_ind,
              pli.actualweight_qty,
              pli.subtrans_type_ind,
              --
              pli.gtax_value,         --this is total gtax value at the pack level, not at the component level
              pli.gtax_unit_value,    --this is unit gtax value at the pack level, not at the component level
              --
              pli.st_vat_include_ind,
              pli.st_currency_code,
              pli.st_store_type,
              pli.st_currency_rtl_dec,
              pli.st_currency_cost_dec,
              pli.st_vat_region,
              --
              pli.sales_qty_std_uom,
              --
              0 xform_prod_loss_pct,
              1 xform_yield_pct,
              1 uom_ratio,
              pli.resa_sales_type,
              pli.no_inv_ret_ind,
              pli.return_disposition,
              pli.return_wh,
              --
              pli.error_msg
         from svc_posupld_line_item_gtt pli,
              v_packsku_qty vpq
        where pli.sales_process_id = I_sales_process_id
          and pli.chunk_id         = I_chunk_id
          and pli.im_item          = vpq.pack_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.SETUP_WORK_TABLE',
                                            to_char(SQLCODE));
      return FALSE;
END SETUP_WORK_TABLE;
----------------------------------------------------------------------------------------
FUNCTION SETUP_DISC_WORK_TABLE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_sales_process_id   IN              NUMBER,
                               I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   TYPE t_rowid                IS TABLE OF ROWID INDEX BY BINARY_INTEGER;
   TYPE t_sales_qty_std_uom    IS TABLE OF SVC_POSUPLD_LINE_ITEM_GTT.SALES_QTY_STD_UOM%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_thead_id             IS TABLE OF SVC_POSUPLD_LINE_ITEM_GTT.SALES_THEAD_ID%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_item                 IS TABLE OF ITEM_MASTER.ITEM%TYPE INDEX BY BINARY_INTEGER;

   a_rowid                     T_ROWID;
   a_sales_qty_selling_uom     T_SALES_QTY_STD_UOM;
   a_sales_qty_std_uom         T_SALES_QTY_STD_UOM;
   a_total_disc_qty            T_SALES_QTY_STD_UOM;
   a_thead_id                  T_THEAD_ID;
   a_item                      T_ITEM;
 
   a_update_size               BINARY_INTEGER := 0;
   update_count                BINARY_INTEGER := 0;

   cursor C_DISC_UOM is
      select plid.rowid,
             plid.sales_qty * pli.uom_ratio sales_qty,
             sum(plid.sales_qty * pli.uom_ratio) over
                (partition by plid.sales_process_id, plid.chunk_id, plid.sales_thead_id, plid.im_item) total_disc_qty
        from svc_posupld_line_item_gtt pli,
             svc_posupld_item_disc_gtt plid
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id = I_chunk_id
         and pli.im_item = plid.im_item
         and pli.sales_process_id = plid.sales_process_id
         and pli.chunk_id = plid.chunk_id
         and pli.posupld_line_item_id = plid.posupld_line_item_id
         and pli.selling_uom    != pli.im_standard_uom
         and (case when pli.catchweight_ind = 'Y'
                   and  pli.im_standard_uom  = 'EA'
                   and pli.selling_uom_class = 'MASS' then
                      'N'
                   else
                     'Y'
                   end) = 'Y'
         and pli.error_msg is null;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   -- populate posupld_item_disc_gtt to get applicable discounts from TDETL rows of POSU file
   insert into svc_posupld_item_disc_gtt (posupld_line_item_discount_id,
                                          posupld_line_item_id,
                                          sales_process_id,
                                          chunk_id,
                                          sales_thead_id,
                                          sales_tdetl_id,
                                          im_item,
                                          prom_tran_type,
                                          prom_no,
                                          sales_qty,
                                          sales_value,
                                          discount_value,
                                          prom_component,
                                          sales_value_with_vat,
                                          sales_value_without_vat,
                                          disc_amt_with_vat,
                                          disc_amt_without_vat,
                                          prom_consignment_rate,
                                          total_disc_qty,
                                          total_disc_amt_with_vat,
                                          total_disc_amt_without_vat,
                                          emp_disc_amt_with_vat,
                                          emp_disc_amt_without_vat)
      select posupld_line_item_discount_seq.nextval,
             posupld_line_item_id,
             sales_process_id,
             chunk_id,
             sales_thead_id,
             sales_tdetl_id,
             im_item,
             prom_tran_type,
             prom_no,
             sales_qty,
             sales_value,
             discount_value,
             prom_component,
             sales_value_with_vat,
             sales_value_without_vat,
             disc_amt_with_vat,
             disc_amt_without_vat,
             prom_consignment_rate,
             total_disc_qty,
             total_disc_amt_with_vat,
             total_disc_amt_without_vat,
             emp_disc_amt_with_vat,
             emp_disc_amt_without_vat
        from (select pli.posupld_line_item_id,
                     tdetl.sales_process_id,
                     tdetl.chunk_id,
                     tdetl.sales_thead_id,
                     tdetl.sales_tdetl_id,
                     NVL(pli.xform_sellable_item, pli.im_item) im_item,
                     tdetl.prom_tran_type,
                     DECODE(tdetl.prom_no,0000000000,NULL,to_number(tdetl.prom_no)) prom_no,
                     (to_number(tdetl.sales_qty) / 10000)* pli.sales_sign_num sales_qty,
                     (to_number(tdetl.sales_value) / 10000) * pli.sales_sign_num sales_value,
                     (to_number(tdetl.discount_value) / 10000) * pli.sales_sign_num discount_value,
                     to_number(tdetl.prom_component) prom_component,
                     --
                     decode(pli.st_vat_include_ind, 'Y',
                            to_number(tdetl.sales_value) * pli.sales_sign_num,
                            (to_number(tdetl.sales_value) * pli.sales_sign_num) * (1 + (NVL(pli.vi_vat_rate,0)/100))) / 10000 sales_value_with_vat,
                     --
                     decode(pli.st_vat_include_ind, 'N',
                            to_number(tdetl.sales_value) * pli.sales_sign_num,
                            (to_number(tdetl.sales_value) * pli.sales_sign_num) / (1 + (NVL(pli.vi_vat_rate,0)/100))) / 10000 as sales_value_without_vat,
                     --
                     decode(pli.st_vat_include_ind, 'Y', 
                            to_number(tdetl.discount_value) * pli.sales_sign_num,
                            (to_number(tdetl.discount_value) * pli.sales_sign_num) * (1 + (NVL(pli.vi_vat_rate,0)/100))) / 10000 disc_amt_with_vat,
                     --
                     decode(pli.st_vat_include_ind, 'N',
                            to_number(tdetl.discount_value) * pli.sales_sign_num,
                            (to_number(tdetl.discount_value) * pli.sales_sign_num) / (1 + (NVL(pli.vi_vat_rate,0)/100))) / 10000 as disc_amt_without_vat,
                     --
                     decode(tdetl.prom_tran_type, 9999, rpc.consignment_rate, NULL) prom_consignment_rate,
                     sum(to_number(tdetl.sales_qty)/10000) over (partition by tdetl.sales_process_id, tdetl.sales_thead_id, pli.im_item) total_disc_qty,
                     --
                     decode('Y',
                            decode(tdetl.prom_tran_type,
                                  '1005', 'N',
                                  'Y'),
                            decode(pli.st_vat_include_ind, 'Y',
                                   decode(tdetl.prom_tran_type,
                                          1005, 0,
                                          (to_number(tdetl.discount_value) * pli.sales_sign_num)),
                                   decode(tdetl.prom_tran_type,
                                          1005, 0,
                                          (to_number(tdetl.discount_value) * pli.sales_sign_num)) * (1 + (NVL(pli.vi_vat_rate,0)/100))) / 10000,
                                   0) total_disc_amt_with_vat,
                     --
                     decode('Y',
                            decode(tdetl.prom_tran_type,
                                   '1005', 'N',
                                   'Y'), decode(pli.st_vat_include_ind, 'N',
                                                decode(tdetl.prom_tran_type, 1005, 0, (to_number(tdetl.discount_value) * pli.sales_sign_num)),
                                                decode(tdetl.prom_tran_type, 1005, 0, (to_number(tdetl.discount_value) * pli.sales_sign_num)) /
                                                    (1 + (NVL(pli.vi_vat_rate,0)/100))) / 10000,
                            0) total_disc_amt_without_vat,
                     --
                     decode(tdetl.prom_tran_type,
                           '1005', decode(pli.st_vat_include_ind, 'Y',
                                          decode(tdetl.prom_tran_type, 1005, (to_number(tdetl.discount_value) * pli.sales_sign_num), 0),
                                          decode(tdetl.prom_tran_type, 1005, (to_number(tdetl.discount_value) * pli.sales_sign_num), 0) *
                                              (1 + (NVL(pli.vi_vat_rate,0)/100))) / 10000,
                            0) emp_disc_amt_with_vat,
                     --
                     decode(tdetl.prom_tran_type,
                            '1005', decode(pli.st_vat_include_ind, 'N',
                                            decode(tdetl.prom_tran_type, 1005, (to_number(tdetl.discount_value) * pli.sales_sign_num), 0),
                                            decode(tdetl.prom_tran_type, 1005, (to_number(tdetl.discount_value) * pli.sales_sign_num), 0) /
                                                (1 + (NVL(pli.vi_vat_rate,0)/100))) / 10000,
                            0) emp_disc_amt_without_vat
            from svc_posupld_line_item_gtt pli,
                 svc_posupld_tdetl_gtt tdetl,
                 rpm_promo_comp_v rpc
           where pli.sales_process_id = I_sales_process_id
             and pli.chunk_id = I_chunk_id
             and tdetl.sales_process_id = pli.sales_process_id
             and tdetl.chunk_id = pli.chunk_id
             and tdetl.sales_thead_id   = pli.sales_thead_id
             and pli.xform_sellable_item is NULL
             and pli.error_msg           is NULL
             and tdetl.error_msg         is NULL
             and to_number(tdetl.prom_no)        = rpc.promo_id(+)
             and to_number(tdetl.prom_component) = rpc.promo_comp_id(+)
        order by tdetl.sales_tdetl_id);

   if LP_system_options_rec.sales_audit_ind = 'N' then
      if a_update_size > 0 then
         a_update_size := 0;
         update_count  := 0;

         FOR rec in c_disc_uom LOOP
            a_update_size                          := a_update_size + 1;
            a_rowid(a_update_size)                 := rec.rowid;
            a_sales_qty_selling_uom(a_update_size) := rec.sales_qty;
            a_total_disc_qty(a_update_size)        := rec.total_disc_qty;
         END LOOP;

         if a_update_size > 0 then
            FORALL update_count IN 1..a_update_size
               update svc_posupld_item_disc_gtt
                  set sales_qty      = a_sales_qty_selling_uom(update_count),
                      total_disc_qty = a_total_disc_qty(update_count)
                where rowid = a_rowid(update_count);
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.SETUP_DISC_WORK_TABLE',
                                            to_char(SQLCODE));
      return FALSE;
END SETUP_DISC_WORK_TABLE;
----------------------------------------------------------------------------------------
FUNCTION WORK_TABLE_DENORMALIZE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id   IN              NUMBER,
                                I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS
   ---
   L_vdate                       DATE := GET_VDATE();
   L_complex_promo_allowed_ind   RPM_SYSTEM_OPTIONS.COMPLEX_PROMO_ALLOWED_IND%TYPE;

   cursor C_RPM_SO is
      select nvl(complex_promo_allowed_ind,0)
        from rpm_system_options;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   -- get complex promo allowed indicator from RPM
   open C_RPM_SO;
   fetch C_RPM_SO into L_complex_promo_allowed_ind;
   close C_RPM_SO;
   --
   merge into svc_posupld_line_item_gtt work
   using (
      select pli.rowid gtt_row_id,
             pli.sales_process_id,
             pli.sales_thead_id,
             pli.item_value,
             pli.im_item item,
             im.status,
             im.dept,
             im.class,
             im.subclass,
             im.pack_ind,
             im.item_level,
             im.tran_level,
             im.standard_uom,
             im.waste_type,
             decode(im.waste_type,
                    'SL', NVL(im.waste_pct/100,0),
                     0) waste_pct,
             im.inventory_ind,
             im.item_xform_ind,
             im.deposit_item_type,
             im.sellable_ind,
             im.orderable_ind,
             d.purchase_type,
             --
             --Note: this sets class_vat_ind on pli to the correct value considering
             --different settings (default_tax_type/class_level_vat_ind/class_vat_ind), etc.
             --After this point, class_vat_ind reflects if unit_retail is tax inclusive or exclusive.
             case
                when LP_system_options_rec.default_tax_type = 'SVAT' then    --SVAT: depending on class_level_vat_ind
                   decode(LP_system_options_rec.class_level_vat_ind,
                          'Y', cls.class_vat_ind,
                          'Y')
                when LP_system_options_rec.default_tax_type = 'GTAX' then    --GTAX: unit_retail is always tax inclusive
                   'Y'
                when LP_system_options_rec.default_tax_type = 'SALES' then   --SALES: unit_retail is always tax exclusive
                   'N'
                end class_vat_ind,
             --
             --Note: this sets st_vat_include_ind on pli to the correct value considering
             --different settings (default_tax_type/class_level_vat_ind/class_vat_ind), etc.
                          --It also accounts for the VAT_INCLUDE_IND in FHEAD and store.vat_include_ind.
             --After this point, st_vat_include_ind correctly reflects if the sales_value 
             --on the POSU file is tax inclusive or not. The following code with the similar 
             --logic to figure out columns w/ or w/o tax does NOT need the case statements any more.
             case
                when LP_system_options_rec.default_tax_type = 'SVAT' then    --SVAT: depending on class_level_vat_ind
                   decode(LP_system_options_rec.class_level_vat_ind,
                          'Y', cls.class_vat_ind,
                          pli.st_vat_include_ind)
                when LP_system_options_rec.default_tax_type = 'GTAX' then    --GTAX: retail is always tax inclusive
                   'Y'
                when LP_system_options_rec.default_tax_type = 'SALES' then   --SALES: retail is always tax exclusive
                   'N'
                end st_vat_include_ind,
             --
             dlc.eow_date,
             dlc.eom_date,
             dlc.eow_date_454_week,
             dlc.eow_date_454_month,
             dlc.eow_date_454_year,
             dlc.eom_date_454_week,
             dlc.eom_date_454_month,
             dlc.eom_date_454_year,
             uc.uom_class as selling_uom_class,
             isupp.supplier,
             isupp.consignment_rate,
             isupp.concession_rate,
             pli.catchweight_ind
        from svc_posupld_line_item_gtt pli,
             item_master im,
             deps d,
             class cls,
             day_level_calendar dlc,
             uom_class uc,
             item_supplier isupp
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id = I_chunk_id
         and pli.im_item  = im.item
         and im.dept      = d.dept
         and d.dept       = cls.dept
         and im.class     = cls.class
         and pli.tran_date = dlc.day
         and uc.uom(+)     = pli.selling_uom
         and isupp.item(+) = im.item
         --consignment item supplier option
         and isupp.primary_supp_ind(+)  = 'Y') use_this
   on (work.rowid = use_this.gtt_row_id)
   when matched then
      update
         set work.im_status                = use_this.status,
             work.im_dept                  = use_this.dept,
             work.im_class                 = use_this.class,
             work.im_subclass              = use_this.subclass,
             work.im_pack_ind              = use_this.pack_ind,
             work.im_item_level            = use_this.item_level,
             work.im_tran_level            = use_this.tran_level,
             work.im_standard_uom          = use_this.standard_uom,
             work.im_waste_type            = use_this.waste_type,
             work.im_waste_pct             = use_this.waste_pct,
             work.im_inventory_ind         = use_this.inventory_ind,
             work.im_item_xform_ind        = use_this.item_xform_ind,
             work.im_deposit_item_type     = use_this.deposit_item_type,
             work.im_sellable_ind          = use_this.sellable_ind,
             work.im_orderable_ind         = use_this.orderable_ind,
             work.deps_purchase_type       = use_this.purchase_type,
             work.class_class_vat_ind      = use_this.class_vat_ind,
             work.st_vat_include_ind       = use_this.st_vat_include_ind,
             work.eow_date                 = use_this.eow_date,
             work.eom_date                 = use_this.eom_date,
             work.eow_date_454_week        = use_this.eow_date_454_week,
             work.eow_date_454_month       = use_this.eow_date_454_month,
             work.eow_date_454_year        = use_this.eow_date_454_year,
             work.eom_date_454_week        = use_this.eom_date_454_week,
             work.eom_date_454_month       = use_this.eom_date_454_month,
             work.eom_date_454_year        = use_this.eom_date_454_year,
             work.selling_uom_class        = use_this.selling_uom_class,
             work.is_supplier              = use_this.supplier,
             work.is_consignment_rate      = use_this.consignment_rate,
             work.is_concession_rate       = use_this.concession_rate,
             work.catchweight_ind          = case when
                                                use_this.catchweight_ind = 'Y' and use_this.standard_uom = 'EA' and
                                                use_this.selling_uom_class = 'MASS' then
                                                   use_this.catchweight_ind
                                             else
                                                'N'
                                             end;

   if CHECK_ITEM_SUPPLIER(O_error_message,
                          I_sales_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if CHECK_ORDERABLE_DETL(O_error_message,
                           I_sales_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;
   -- Catch Non-fatal errors
   if NON_FATAL_ERRORS_CHECK(O_error_message,
                             I_sales_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- Convert selling quantity based on selling UOM and standard UOM
   if UOM_CONVERT_QTY(O_error_message,
                      I_sales_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   --update VAT information based on svc_posupld_ttax_gtt
   --currently, one TTAX line per THEAD is expected since only one effective retail vat code per item/vat region on a transaction date is expected.
   merge into svc_posupld_line_item_gtt work
   using (
      select distinct ptg.sales_thead_id,
             first_value(ptg.tax_code) over (partition by ptg.sales_thead_id) tax_code,
             avg(ptg.tax_rate/10000000000) over (partition by ptg.sales_thead_id) tax_rate,
             sum(ptg.tax_amt/10000) over (partition by ptg.sales_thead_id) tax_amt
        from svc_posupld_ttax_gtt ptg
       where ptg.sales_process_id    = I_sales_process_id
         and ptg.chunk_id            = I_chunk_id
         and ptg.error_msg is null) use_this
   on (work.sales_process_id = I_sales_process_id
   and work.chunk_id = I_chunk_id
   and work.sales_thead_id = use_this.sales_thead_id)
   when matched then
      update set work.vi_vat_rate = use_this.tax_rate,
                 work.vi_vat_code = use_this.tax_code,
                 work.vi_vat_amt  = use_this.tax_amt * work.sales_sign_num;

   -- update sales information like quantity, amount based on merged information from TTAX staging table
   -- item_loc_soh and supplier information
   merge into svc_posupld_line_item_gtt work
   using (
      select pli.rowid gtt_row_id,
             il.status,
             il.primary_supp,
             il.primary_cntry,
             il.unit_retail,
             pli.class_class_vat_ind,
             pli.st_vat_include_ind,
             pli.sales_value,
             NVL(ils.unit_cost, 0) unit_cost,
             NVL(ils.av_cost, 0) av_cost,
             NVL(ils.average_weight, 0) average_weight,
             ils.last_hist_export_date,
             st.currency_code,
             st.terms,
             st.auto_appr_invc_ind,
             NVL(st.edi_sales_rpt_freq, 'N') edi_sales_rpt_freq,
             st.ret_courier,
             st.handling_pct,
             st.ret_allow_ind,
             st.duedays,
             st.percent,
             pli.sales_qty_std_uom,
             pli.im_waste_pct,
             pli.pack_qty,
             pli.is_consignment_rate / 100 as consignment_rate,
             --
             decode(pli.st_vat_include_ind, 'Y',
                    pli.sales_value,
                    pli.sales_value + NVL(pli.vi_vat_amt,0)) sales_value_with_vat,
             decode(pli.st_vat_include_ind, 'N',
                    pli.sales_value,
                    pli.sales_value - NVL(pli.vi_vat_amt,0)) sales_value_without_vat
        from svc_posupld_line_item_gtt pli,
             item_loc il,
             item_loc_soh ils,
             (select distinct s.supplier,
                     s.currency_code,
                     s.terms,
                     s.auto_appr_invc_ind,
                     s.edi_sales_rpt_freq,
                     s.ret_courier,
                     s.handling_pct,
                     s.ret_allow_ind,
                     t.duedays,
                     t.percent,
                     row_number() over (partition by s.supplier,s.terms order by start_date_active desc) ranking
                from sups s,
                     terms t
               where s.terms = t.terms
                 and t.enabled_flag = 'Y'
                 and nvl(start_date_active,L_vdate) <= L_vdate
                 and nvl(end_date_active,L_vdate) >= L_vdate) st
       where pli.sales_process_id    = I_sales_process_id
         and pli.chunk_id            = I_chunk_id
         and pli.error_msg           is null
         and pli.im_item             = il.item
         and pli.loc_value           = il.loc
         and il.item                 = ils.item
         and il.loc                  = ils.loc
         --consignment item supplier option
         and pli.is_supplier         = st.supplier(+)
         and st.ranking(+) = 1) use_this
   on (work.rowid = use_this.gtt_row_id)
   when matched then
      update set work.il_status                 = use_this.status,
                 work.il_primary_supp           = use_this.primary_supp,
                 work.il_primary_cntry          = use_this.primary_cntry,
                 work.il_unit_retail            = use_this.unit_retail,
                 work.ils_unit_cost             = use_this.unit_cost,
                 work.ils_av_cost               = use_this.av_cost,
                 work.ils_average_weight        = NVL(use_this.average_weight, 0),
                 work.ils_last_hist_export_date = use_this.last_hist_export_date,
                 --
                 work.sups_currency_code        = use_this.currency_code,
                 work.sups_terms                = use_this.terms,
                 work.sups_auto_appr_invc_ind   = use_this.auto_appr_invc_ind,
                 work.sups_edi_sales_rpt_freq   = use_this.edi_sales_rpt_freq,
                 work.sups_ret_courier          = use_this.ret_courier,
                 work.sups_handling_pct         = use_this.handling_pct,
                 work.sups_ret_allow_ind        = use_this.ret_allow_ind,
                 work.terms_duedays             = use_this.duedays,
                 work.terms_percent             = use_this.percent,
                 --
                 work.total_unit_cost_loc       = decode(use_this.consignment_rate, null,
                                                         use_this.unit_cost *
                                                            (use_this.sales_qty_std_uom / (1 - NVL(use_this.im_waste_pct,0)) * use_this.pack_qty),
                                                              use_this.sales_value_without_vat * use_this.consignment_rate),
                 --
                 work.total_av_cost_loc         = decode(use_this.consignment_rate, null,
                                                         use_this.av_cost *
                                                            (use_this.sales_qty_std_uom / (1 - NVL(use_this.im_waste_pct,0)) *
                                                              use_this.pack_qty),
                                                            use_this.sales_value_without_vat * use_this.consignment_rate),
                 --
                 work.total_wastage_qty         = use_this.sales_qty_std_uom / (1 - NVL(use_this.im_waste_pct,0)),
                 work.sales_value_with_vat      = use_this.sales_value_with_vat,
                 work.sales_value_without_vat   = use_this.sales_value_without_vat;

   -- Check for non-fatal erros on item loc records
   if IL_NON_FATAL_ERRORS_CHECK(O_error_message,
                                I_sales_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- get ph_reg_retail first so we can get the pack price ratio to be used in 
   -- calculating pro-rated tax amount
   merge into svc_posupld_line_item_gtt work
   using (
      with ph_slice as (
        select ph.tran_type,
               ph.reason,
               ph.event,
               ph.item,
               ph.loc,
               ph.loc_type,
               ph.unit_cost,
               ph.unit_retail,
               ph.selling_unit_retail,
               ph.selling_uom,
               ph.action_date,
               ph.multi_units,
               ph.multi_unit_retail,
               ph.multi_selling_uom,
               ph.post_date
          from svc_posupld_line_item_gtt pli,
               price_hist ph
         where pli.sales_process_id = I_sales_process_id
           and pli.chunk_id = I_chunk_id
           and pli.error_msg is null
           and pli.im_item = ph.item
           and pli.loc_value = ph.loc)
      select base.posupld_line_item_id,
             base.im_item,
             base.sales_thead_id,
             NVL(reg.reg_retail, NVL(base.base_retail, 0)) as reg_retail_without_vat,
             NVL(reg.reg_tran_type, base.base_tran_type) as reg_tran_type
        from (select distinct
                     pli.posupld_line_item_id,
                     pli.im_item,
                     first_value(ph.unit_retail) over (partition by ph.item, ph.loc, pli.sales_thead_id order by ph.action_date desc,DECODE(tran_type,8,'A',4,'C',11,'C',0,'D') asc) reg_retail,
                     first_value(ph.tran_type) over (partition by ph.item, ph.loc, pli.sales_thead_id order by ph.action_date desc,DECODE(tran_type,8,'A',4,'C',11,'C',0,'D') asc) reg_tran_type
                from svc_posupld_line_item_gtt pli,
                     ph_slice ph
               where pli.sales_process_id = I_sales_process_id
                 and pli.chunk_id         = I_chunk_id
                 and pli.error_msg        is null
                 and pli.xform_sellable_item is null
                 and pli.im_item          = ph.item
                 and pli.loc_value        = ph.loc
                 and ph.action_date       < pli.tran_date + 1
                 and ph.tran_type         in (0, 4, 8, 11) --base,reg,clear,multi-unit-reg
             ) reg,
             (select distinct
                     pli.posupld_line_item_id,
                     pli.im_item,
                     pli.sales_thead_id,
                     first_value(ph.unit_retail) over
                        (partition by ph.item, ph.loc, pli.sales_thead_id order by ph.action_date desc) base_retail,
                     ph.tran_type base_tran_type
                from svc_posupld_line_item_gtt pli,
                     ph_slice ph
               where pli.sales_process_id = I_sales_process_id
                 and pli.chunk_id = I_chunk_id
                 and pli.error_msg        is null
                 and pli.xform_sellable_item is null
                 and pli.im_item        = ph.item
                 and pli.loc_value      = ph.loc
                 and ph.tran_type         = 0
             ) base
       where base.posupld_line_item_id = reg.posupld_line_item_id(+)
   ) use_this
   on (work.sales_process_id = I_sales_process_id
   and work.chunk_id = I_chunk_id
   and ((work.posupld_line_item_id  = use_this.posupld_line_item_id)
        or((nvl(work.xform_sellable_item,'-999') = use_this.im_item) and (work.sales_thead_id = use_this.sales_thead_id))))
   when matched then
      update
         set work.ph_reg_retail = use_this.reg_retail_without_vat,
             work.ph_clear_ind  = decode(use_this.reg_tran_type,8,'Y','N');

   -- setup pack totals / ratios --
   merge into svc_posupld_line_item_gtt target
      using (
         select pli.rowid gtt_row_id,
                CASE WHEN  SUM(PLI.PH_REG_RETAIL * PLI.PACK_QTY) OVER (PARTITION BY SALES_THEAD_ID) <> 0 
                     THEN (pli.ph_reg_retail * pli.pack_qty) / sum(pli.ph_reg_retail * pli.pack_qty) over  
                                       (partition by sales_thead_id)  
                     ELSE  
                        pli.pack_qty / sum(pli.pack_qty) over (partition by sales_thead_id) 
                     END    pack_price_ratio
           from svc_posupld_line_item_gtt pli
          where pli.sales_process_id = I_sales_process_id
            and pli.chunk_id         = I_chunk_id
            and pli.error_msg        is null
            and pli.pack_comp_ind    = 'Y') use_this
      on (target.rowid = use_this.gtt_row_id)
   when matched then
      update set target.pack_price_ratio = use_this.pack_price_ratio;

   -- get other relevant retail values from PRICE_HIST table (ph_curr_retail, ph_prom_retail, etc)
   merge into svc_posupld_line_item_gtt work
   using (
      with ph_slice as (
        select ph.tran_type,
               ph.reason,
               ph.event,
               ph.item,
               ph.loc,
               ph.loc_type,
               ph.unit_cost,
               ph.unit_retail,
               ph.selling_unit_retail,
               ph.selling_uom,
               ph.action_date,
               ph.multi_units,
               ph.multi_unit_retail,
               ph.multi_selling_uom,
               ph.post_date
          from svc_posupld_line_item_gtt pli,
               price_hist ph
         where pli.sales_process_id = I_sales_process_id
           and pli.chunk_id = I_chunk_id
           and pli.error_msg is null
           and pli.im_item = ph.item
           and pli.loc_value = ph.loc)
      select base.posupld_line_item_id,
             base.im_item,
             base.sales_thead_id,
             NVL(curr.curr_retail, base.ph_reg_retail) as curr_retail,
             base.ph_reg_retail as reg_retail,
             promo.promo_retail,
             promo.promo_tran_type,
             decode(base.class_class_vat_ind, 
                    'Y',1,
                    1+(base.vi_vat_rate/100)) as vat_incl_ratio,
             decode(base.class_class_vat_ind, 
                    'Y',1+(base.vi_vat_rate/100),
                    1) as vat_excl_ratio,
             NVL(case when L_complex_promo_allowed_ind = 0 and base.tran_type_char = 'S' and promo.promo_tran_type = 9 
                      then promo.promo_retail end, base.ph_reg_retail) old_retail,
             base.total_wastage_qty * base.pack_qty * base.im_waste_pct  wastage_qty
        from (select distinct pli.posupld_line_item_id,
                     pli.im_item,
                     first_value(ph.unit_retail) over (partition by ph.item, ph.loc, pli.sales_thead_id order by ph.action_date desc,DECODE(tran_type,8,'A',4,'C',11,'C',0,'D') asc) curr_retail
                from svc_posupld_line_item_gtt pli,
                     ph_slice ph
               where pli.sales_process_id = I_sales_process_id
                 and pli.chunk_id         = I_chunk_id
                 and pli.error_msg        is null
                 and pli.xform_sellable_item is null
                 and pli.im_item          = ph.item
                 and pli.loc_value        = ph.loc
                 and ph.action_date       < L_vdate + 1
                 and ph.tran_type         in (0, 4, 8, 11) --base,reg,clear,multi-unit-reg
            ) curr,
            (select distinct
                    pli.posupld_line_item_id,
                    pli.im_item,
                    first_value(ph.unit_retail) over
                       (partition by ph.item, ph.loc, pli.sales_thead_id order by ph.action_date desc,DECODE(tran_type,9,'B',4,'C',11,'C') asc) promo_retail,
                    first_value(ph.tran_type) over (partition by ph.item,
                                                                 ph.loc,
                                                                 pli.sales_thead_id
                                                        order by ph.action_date desc,DECODE(tran_type,9,'B',4,'C',11,'C') asc) promo_tran_type
               from svc_posupld_line_item_gtt pli,
                    ph_slice ph
              where pli.sales_process_id = I_sales_process_id
                and pli.chunk_id         = I_chunk_id
                and pli.error_msg        is null
                and pli.xform_sellable_item is null
                and pli.im_item          = ph.item
                and pli.loc_value        = ph.loc
                and ph.action_date       < pli.tran_date + 1
                and pli.pack_comp_ind    = 'N'
                and pli.tran_type_char   = 'S'
                and ph.tran_type         in (9,4,11) --promo,reg,multi-unit-reg--
            ) promo,
            (select distinct
                    pli.posupld_line_item_id,
                    pli.im_item,
                    pli.sales_thead_id,
                    NVL(pli.vi_vat_rate,0) vi_vat_rate,
                    pli.tran_type_char,
                    pli.im_waste_pct,
                    pli.class_class_vat_ind,
                    pli.total_wastage_qty,
                    pli.pack_qty,
                    pli.ph_reg_retail
               from svc_posupld_line_item_gtt pli
              where pli.sales_process_id = I_sales_process_id
                and pli.chunk_id = I_chunk_id
                and pli.error_msg        is null
                and pli.xform_sellable_item is null
            ) base
       where base.posupld_line_item_id = curr.posupld_line_item_id(+)
         and base.posupld_line_item_id = promo.posupld_line_item_id(+)
   ) use_this
   on (work.sales_process_id = I_sales_process_id
   and work.chunk_id = I_chunk_id
   and ((work.posupld_line_item_id  = use_this.posupld_line_item_id)
        or((nvl(work.xform_sellable_item,'-999') = use_this.im_item) and (work.sales_thead_id = use_this.sales_thead_id))))
   when matched then
      update
         set work.ph_curr_retail          = use_this.curr_retail * use_this.vat_incl_ratio,
             work.ph_reg_retail           = use_this.reg_retail * use_this.vat_incl_ratio,
             work.ph_prom_retail          = use_this.promo_retail * use_this.vat_incl_ratio,
             work.ph_prom_ind             = decode(use_this.promo_tran_type,9,'Y','N'),
             work.old_retail_with_vat     = use_this.old_retail * use_this.vat_incl_ratio,
             work.old_retail_without_vat  = use_this.old_retail / use_this.vat_excl_ratio,
             work.wastage_amt_with_vat    = use_this.old_retail * use_this.wastage_qty * use_this.vat_incl_ratio,
             work.wastage_amt_without_vat = use_this.old_retail * use_this.wastage_qty / use_this.vat_excl_ratio;

   if CHECK_BASE_PRICE(O_error_message,
                       I_sales_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;
   -- Convert the catchweight item quantity and sales amount
   if CATCHWEIGHT_CONVERT(O_error_message,
                          I_sales_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   --setup sales type
   merge into svc_posupld_line_item_gtt target
      using (
         select pli.rowid gtt_row_id,
                pli.ph_clear_ind,
                pli.ph_prom_ind,
                pli.ph_reg_retail,
                pli.ph_prom_retail,
                min(tdetl.prom_tran_type),
                case when pli.ph_clear_ind = 'Y' then
                   'C'
                when pli.ph_prom_ind = 'Y' or min(tdetl.prom_tran_type) is NOT NULL then
                   'P'
                else
                   'R'
                end as sales_type
           from svc_posupld_line_item_gtt pli,
                svc_posupld_tdetl_gtt tdetl
          where pli.sales_process_id        = I_sales_process_id
            and pli.chunk_id                = I_chunk_id
            and pli.error_msg               is null
            and tdetl.error_msg             is null
            and tdetl.sales_process_id(+)   = pli.sales_process_id
            and tdetl.sales_thead_id(+)     = pli.sales_thead_id
            and tdetl.prom_tran_type(+)   not in(1004,1005)
       group by pli.rowid,
                pli.ph_clear_ind,
                pli.ph_prom_ind,
                pli.ph_reg_retail,
                pli.ph_prom_retail) use_this
      on (target.rowid = use_this.gtt_row_id)
   when matched then
      update set target.sales_type = use_this.sales_type;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.WORK_TABLE_DENORMALIZE',
                                            to_char(SQLCODE));
      return FALSE;
END WORK_TABLE_DENORMALIZE;
----------------------------------------------------------------------------------------
FUNCTION NON_FATAL_ERRORS_CHECK(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id   IN              NUMBER,
                                I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   --check constraint type checks--
   --data validation checks--
   merge into svc_posupld_line_item_gtt target
   using (
      select rowid gtt_row_id,
             --check constraint type checks--
             case item_type when 'ITM' then null
                              when 'REF' then null
                              else 'INV_SYSTEM_IND'
                              end as s_item_type_msg,
             case tran_type_char when 'S' then null
                              when 'R' then null
                              else 'INV_TRAN_TYPE'
                              end as s_tran_type_msg,
             case dropship_ind when 'Y' then null
                                 when 'N' then null
                                 else 'INV_DROP_SHIP'
                                 end as dropship_ind_msg,
             case sales_sign_char when 'P' then null
                               when 'N' then null
                               else 'INV_SALES_SIGN'
                               end as s_sales_sign_msg,
             --data validation checks--
             case NVL(im_item,'-999') when '-999' then 'INV_ITEM'
                                      else null
                                      end as im_item_msg,
             case when NVL(selling_uom_class,'-999') = '-999' and NVL(im_item,'-999') != '-999' then 'INVALID_UOM'
                                                else null
                                                end as selling_uom_class_msg,
             case when pli.im_item_level < pli.im_tran_level then
                'INV_POSU_ITEM_TYPE'
             end as item_type_msg,
             case when im_status != 'A' then
                'ITEM_STATUS'
             end item_status_msg
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id) use_this
   on (target.rowid = use_this.gtt_row_id)
   when matched then
      update set target.error_msg =        target.error_msg ||
         use_this.s_item_type_msg          ||decode(use_this.s_item_type_msg,null,null,';')||
         use_this.s_tran_type_msg          ||decode(use_this.s_tran_type_msg,null,null,';')||
         use_this.dropship_ind_msg         ||decode(use_this.dropship_ind_msg,null,null,';')||
         use_this.s_sales_sign_msg         ||decode(use_this.s_sales_sign_msg,null,null,';')||
         --
         use_this.im_item_msg              ||decode(use_this.im_item_msg,null,null,';')||
         use_this.selling_uom_class_msg    ||decode(use_this.selling_uom_class_msg,null,null,';')||
         use_this.item_type_msg            ||decode(use_this.item_type_msg,null,null,';')||
         use_this.item_status_msg          ||decode(use_this.item_status_msg,null,null,';');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.NON_FATAL_ERRORS_CHECK',
                                            to_char(SQLCODE));
      return FALSE;
END NON_FATAL_ERRORS_CHECK;
----------------------------------------------------------------------------------------
FUNCTION IL_NON_FATAL_ERRORS_CHECK(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   --item_loc data validation checks--
   merge into svc_posupld_line_item_gtt target
   using (
      select rowid gtt_row_id,
             case NVL(il_status,'-999') when '-999' then 'INV_ITEMLOC_STAT'
                                        else null
                                        end as il_status_msg
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and error_msg is null) use_this
   on (target.rowid = use_this.gtt_row_id)
   when matched then
      update set target.error_msg = target.error_msg ||
         use_this.il_status_msg || decode(use_this.il_status_msg,null,null,';');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.IL_NON_FATAL_ERRORS_CHECK',
                                            to_char(SQLCODE));
      return FALSE;
END IL_NON_FATAL_ERRORS_CHECK;
----------------------------------------------------------------------------------------
FUNCTION CATCHWEIGHT_CONVERT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id   IN              NUMBER,
                             I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   TYPE t_rowid                IS TABLE OF ROWID INDEX BY BINARY_INTEGER;
   TYPE t_cw_nominal_wt        IS TABLE OF SVC_POSUPLD_LINE_ITEM_GTT.CW_NOMINAL_WT%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_s_actualweight_qty   IS TABLE OF SVC_POSUPLD_LINE_ITEM_GTT.ACTUALWEIGHT_QTY%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_ils_average_weight   IS TABLE OF SVC_POSUPLD_LINE_ITEM_GTT.ILS_AVERAGE_WEIGHT%TYPE INDEX BY BINARY_INTEGER;

   a_rowid                     t_rowid;
   a_cw_nominal_wt             t_cw_nominal_wt;
   a_s_actualweight_qty        t_s_actualweight_qty;
   a_ils_average_weight        t_ils_average_weight;

   a_update_size               BINARY_INTEGER := 0;
   update_count                BINARY_INTEGER := 0;

   L_cuom                      ITEM_SUPP_COUNTRY.COST_UOM%TYPE;

   cursor C_CW is
      select pli.rowid,
             pli.im_item,
             pli.selling_uom,
             pli.actualweight_qty,
             pli.ils_average_weight
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id  = I_sales_process_id
         and pli.chunk_id          = I_chunk_id
         and pli.error_msg         is null
         and pli.catchweight_ind   = 'Y'
         and pli.im_standard_uom   = 'EA';

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   FOR rec in c_cw LOOP
      a_update_size                          := a_update_size + 1;
      a_rowid(a_update_size)                 := rec.rowid;
      a_cw_nominal_wt(a_update_size)         := null;
      a_s_actualweight_qty(a_update_size)    := null;
      a_ils_average_weight(a_update_size)    := rec.ils_average_weight;

      if ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                      a_cw_nominal_wt(a_update_size),
                                                      rec.im_item) = FALSE then
         return FALSE;
      end if;

      if CATCH_WEIGHT_SQL.CONVERT_WEIGHT(O_error_message,
                                         a_s_actualweight_qty(a_update_size),
                                         L_cuom,
                                         rec.im_item,
                                         rec.actualweight_qty,
                                         rec.selling_uom) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   if a_update_size > 0 then
      FORALL update_count IN 1..a_update_size
         update svc_posupld_line_item_gtt
            set actualweight_qty = a_s_actualweight_qty(update_count),
                actualwght_retail_with_vat     = (a_s_actualweight_qty(update_count) * old_retail_with_vat) /
                                                  (ils_average_weight * sales_qty),
                actualwght_retail_without_vat  = (a_s_actualweight_qty(update_count) * old_retail_without_vat) /
                                                  (ils_average_weight * sales_qty),
                ph_curr_retail     = case when a_cw_nominal_wt(update_count) != 0 and
                                               a_ils_average_weight(update_count) != 0 then
                                        ph_curr_retail * a_ils_average_weight(update_count) / a_cw_nominal_wt(update_count)

                                     else
                                        ph_curr_retail
                                     end,
                ph_reg_retail      = case when a_cw_nominal_wt(update_count) != 0 and
                                               a_ils_average_weight(update_count) != 0 then
                                        ph_reg_retail * a_ils_average_weight(update_count) / a_cw_nominal_wt(update_count)
                                     else
                                        ph_reg_retail
                                     end,
                ph_prom_retail     = case when a_cw_nominal_wt(update_count) != 0 and
                                               a_ils_average_weight(update_count) != 0 then
                                        ph_prom_retail * a_ils_average_weight(update_count) / a_cw_nominal_wt(update_count)
                                     else
                                        ph_prom_retail
                                     end,
                ils_average_weight = case when a_cw_nominal_wt(update_count) != 0 then
                                        a_cw_nominal_wt(update_count)
                                     else
                                        ils_average_weight
                                     end
          where rowid = a_rowid(update_count);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.CATCHWEIGHT_CONVERT',
                                            to_char(SQLCODE));
      return FALSE;
END CATCHWEIGHT_CONVERT;
----------------------------------------------------------------------------------------
FUNCTION XFORM_HELPERS(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_sales_process_id   IN              NUMBER,
                       I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into svc_posupld_line_item_gtt target
   using
   (with
     v1 as -- get sellable retail **
          (select pli.im_item as ord_item,
                  pli.xform_sellable_item as sell_item,
                  pli.sales_process_id,
                  pli.sales_thead_id,
                  pli.chunk_id,
                  pli.loc_value as loc,
                  NVL(pli.xform_yield_pct,1) xform_yield_pct,
                  NVL(pli.total_wastage_qty,0) total_wastage_qty,
                  NVL(il.unit_retail, ph.selling_unit_retail) as sellable_retail
             from svc_posupld_line_item_gtt pli,
                  item_loc il,
                  price_hist ph
            where pli.sales_process_id       = I_sales_process_id
              and pli.chunk_id               = I_chunk_id
              and pli.error_msg              is null
              and pli.im_item_xform_ind      = 'Y'
              and pli.xform_sellable_item    is not null
              and il.item(+) = pli.xform_sellable_item
              and il.loc(+) = pli.loc_value
              and pli.xform_sellable_item = ph.item
              and ph.tran_type = 0
              and ph.loc = 0),
     v2_1 as -- get orderable cost
          (select v1.ord_item,
                  v1.sell_item,
                  v1.loc,
                  iscl.unit_cost as orderable_cost
             from item_loc il,
                  item_supp_country_loc iscl,
                  v1
            where iscl.item = v1.ord_item
              and iscl.loc  = v1.loc
              and il.item = iscl.item
              and il.primary_supp = iscl.supplier
              and il.primary_cntry = iscl.origin_country_id
              and il.loc   = iscl.loc),
    v2_2 as -- get orderable cost if it does not exists in item_loc
          (select v1.ord_item,
                  v1.sell_item,
                  v1.loc,
                  isc.unit_cost as orderable_cost
             from v1,
                  item_supp_country isc
            where v1.ord_item = isc.item
              and isc.primary_supp_ind = 'Y'
              and isc.primary_country_ind = 'Y'
              and not exists (select 'x'
                                from v2_1
                               where v2_1.ord_item = v1.ord_item
                                 and v2_1.loc = v1.loc
                                 and rownum = 1)),
      v2 as
          (select ord_item,
                  sell_item,
                  loc,
                  orderable_cost
             from v2_1
           union all
           select ord_item,
                  sell_item,
                  loc,
                  orderable_cost
             from v2_2),
      v3 as -- get all sellable_items related to orderable item.
          (select v2.ord_item,
                  v2.loc,
                  v2.orderable_cost,
                  b.detail_item,
                  b.item_quantity_pct
             from item_xform_head a,
                  item_xform_detail b,
                  v2
            where a.head_item = v2.ord_item
              and a.item_xform_head_id = b.item_xform_head_id),
      v4 as -- get orderable retail **
          (select v3.ord_item,
                  v3.loc,
                  v3.orderable_cost,
                  sum(NVL(il.unit_retail,ph.selling_unit_retail) * (item_quantity_pct/100)) orderable_retail
             from v3,
                  item_loc il,
                  price_hist ph
            where v3.detail_item = il.item(+)
              and v3.loc = il.loc(+)
              and v3.detail_item = ph.item
              and ph.tran_type = 0
              and ph.loc = 0
         group by v3.ord_item,
                  v3.loc,
                  v3.orderable_cost),
      v5 as -- get all information
          (select v1.sales_process_id,
                  v1.sales_thead_id,
                  v1.chunk_id,
                  v1.ord_item,
                  v1.sell_item,
                  v1.loc,
                  v1.xform_yield_pct,
                  v1.total_wastage_qty,
                  v1.sellable_retail,
                  v4.orderable_cost,
                  v4.orderable_retail,
                  case when v1.xform_yield_pct in (-1,1) and v4.orderable_retail != 0 then
                      (v1.sellable_retail / v4.orderable_retail) * v4.orderable_cost
                  when v4.orderable_retail != 0 then
                     ((v1.sellable_retail / v4.orderable_retail) * v4.orderable_cost * v1.xform_yield_pct)
                  else
                     0
                  end as ord_unit_cost_loc,
                  case when v1.xform_yield_pct in (-1,1) and v4.orderable_retail != 0 then
                     (v1.sellable_retail / v4.orderable_retail) * v4.orderable_cost * total_wastage_qty
                  when v4.orderable_retail != 0 then
                     ((v1.sellable_retail / v4.orderable_retail) * v4.orderable_cost * v1.xform_yield_pct * total_wastage_qty)
                  else
                     0
                  end as total_unit_cost_loc
             from v1,
                  v4
            where v1.ord_item = v4.ord_item
              and v1.loc = v4.loc)
      select sales_process_id,
             sales_thead_id,
             chunk_id,
             sell_item,
             loc,
             sum(ord_unit_cost_loc) ord_unit_cost_loc,
             sum(total_unit_cost_loc) total_unit_cost_loc
        from v5
       group by sales_process_id,
                sales_thead_id,
                chunk_id,
                sell_item,
                loc
      ) use_this
      on (target.sales_process_id      = use_this.sales_process_id and
          target.chunk_id              = use_this.chunk_id and
          target.sales_thead_id        = use_this.sales_thead_id and
          target.loc_value             = use_this.loc and
           (target.xform_sellable_item = use_this.sell_item or target.im_item = use_this.sell_item)
          )
      when matched then
         update set target.ils_unit_cost        = use_this.ord_unit_cost_loc,
                    target.total_unit_cost_loc  = use_this.total_unit_cost_loc;
   ---
   merge into svc_posupld_line_item_gtt target
   using (select orderable.rowid gtt_row_id,
                 sellable.wastage_amt_with_vat,
                 sellable.wastage_amt_without_vat
            from svc_posupld_line_item_gtt orderable,
                 svc_posupld_line_item_gtt sellable
           where orderable.sales_process_id    = I_sales_process_id
             and orderable.chunk_id            = I_chunk_id
             and orderable.sales_process_id    = sellable.sales_process_id
             and orderable.chunk_id            = sellable.chunk_id
             and orderable.sales_thead_id      = sellable.sales_thead_id
             and orderable.xform_sellable_item = sellable.im_item
             and orderable.error_msg           is null
             and sellable.error_msg            is null
         ) use_this
      on (target.rowid    = use_this.gtt_row_id)
    when matched then
       update set wastage_amt_with_vat    = use_this.wastage_amt_with_vat,
                  wastage_amt_without_vat = use_this.wastage_amt_without_vat;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.XFORM_HELPERS',
                                            to_char(SQLCODE));
      return FALSE;
END XFORM_HELPERS;
----------------------------------------------------------------------------------------
FUNCTION UOM_CONVERT_QTY(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sales_process_id   IN              NUMBER,
                         I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   TYPE t_rowid               IS TABLE OF ROWID INDEX BY BINARY_INTEGER;
   TYPE t_sales_qty_std_uom   IS TABLE OF SVC_POSUPLD_LINE_ITEM_GTT.SALES_QTY_STD_UOM%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_thead_id            IS TABLE OF SVC_POSUPLD_LINE_ITEM_GTT.SALES_THEAD_ID%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_item                IS TABLE OF ITEM_MASTER.ITEM%TYPE INDEX BY BINARY_INTEGER;

   a_rowid                    T_ROWID;
   a_sales_qty_selling_uom    T_SALES_QTY_STD_UOM;
   a_sales_qty_std_uom        T_SALES_QTY_STD_UOM;
   a_total_disc_qty           T_SALES_QTY_STD_UOM;
   a_thead_id                 T_THEAD_ID;
   a_item                     T_ITEM;

   a_update_size              BINARY_INTEGER := 0;
   update_count               BINARY_INTEGER := 0;

   cursor C_UOM is
      select pli.rowid,
             pli.sales_qty,
             pli.selling_uom,
             pli.im_item,
             pli.sales_thead_id,
             pli.im_standard_uom,
             pli.posupld_line_item_id
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id  = I_sales_process_id
         and pli.chunk_id = I_chunk_id
         and pli.selling_uom    != pli.im_standard_uom
         and (case when pli.catchweight_ind = 'Y'
                   and  pli.im_standard_uom  = 'EA'
                   and pli.selling_uom_class = 'MASS' then
                'N'
              else
                'Y'
              end) = 'Y'
         and pli.pack_comp_ind = 'N'
         and NOT (pli.im_item_xform_ind = 'Y' and xform_sellable_item is NOT NULL) 
         and pli.error_msg is null;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   FOR rec in C_UOM LOOP
      a_update_size                          := a_update_size + 1;
      a_rowid(a_update_size)                 := rec.rowid;
      a_sales_qty_selling_uom(a_update_size) := rec.sales_qty;
      a_sales_qty_std_uom(a_update_size)     := null;
      a_thead_id(a_update_size)              := rec.sales_thead_id;
      a_item(a_update_size)                  := rec.im_item;

      if UOM_SQL.CONVERT(O_error_message,
                         a_sales_qty_std_uom(a_update_size),
                         rec.im_standard_uom,
                         rec.sales_qty,
                         rec.selling_uom,
                         rec.im_item,
                         null,
                         null) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   if a_update_size > 0 then
      FORALL update_count IN 1..a_update_size
         update svc_posupld_line_item_gtt
            set sales_qty_std_uom = a_sales_qty_std_uom(update_count),
                uom_ratio         = a_sales_qty_selling_uom(update_count) / a_sales_qty_std_uom(update_count)
          where rowid = a_rowid(update_count);

      FORALL update_count IN 1..a_update_size
         update svc_posupld_line_item_gtt
            set sales_qty_std_uom = a_sales_qty_std_uom(update_count),
                uom_ratio         = a_sales_qty_selling_uom(update_count) / a_sales_qty_std_uom(update_count)
          where pack_no = a_item(update_count)
            and sales_thead_id = a_thead_id(update_count)
            and sales_process_id = I_sales_process_id
            and chunk_id = I_chunk_id;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.UOM_CONVERT_QTY',
                                            to_char(SQLCODE));
      return FALSE;
END UOM_CONVERT_QTY;
----------------------------------------------------------------------------------------
FUNCTION RANGE_ITEM_LOC(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   cursor C_ITEMLOC is
      select distinct 
             svc.item,
             NULL,         --item_parent,
             NULL,         --item_grandparent,
             NULL,         --short_desc,
             NULL,         --dept,
             NULL,         --class,
             NULL,         --subclass,
             NULL,         --item_level,
             NULL,         --tran_level,
             NULL,         --item_status,
             NULL,         --waste_type,
             NULL,         --sellable_ind,
             NULL,         --orderable_ind,
             NULL,         --pack_ind,
             NULL,         --pack_type,
             NULL,         --item_desc,
             NULL,         --diff_1,
             NULL,         --diff_2,
             NULL,         --diff_3,
             NULL,         --diff_4,
             svc.loc,
             'S',
             NULL,
             NULL,         --unit_cost_loc,
             NULL,         --unit_retail_loc,
             NULL,         --selling_retail_loc,
             NULL,         --selling_uom,
             NULL,         --multi_units,
             NULL,         --multi_unit_retail,
             NULL,         --multi_selling_uom,
             NULL,         --item_loc_status,
             NULL,
             NULL,
             NULL,          --hi,
             NULL,          --store_ord_mult,
             NULL,         --meas_of_each,
             NULL,         --meas_of_price,
             NULL,         --uom_of_price,
             NULL,         --primary_variant,
             NULL,         --primary_supp,
             NULL,         --primary_cntry,
             NULL,         --local_item_desc,
             NULL,         --local_short_desc,
             NULL,         --primary_cost_pack,
             NULL,         --receive_as_type,
             NULL,         --store_price_ind,
             NULL,         --uin_type,
             NULL,         --uin_label,
             NULL,         --capture_time,
             NULL,         --ext_uin_ind,
             NULL,         --source_method,
             NULL,         --source_wh,
             NULL,         --inbound_handling_days,
             NULL,         --currency_code,
             NULL,         --like_store,
             'N',          --default_to_children_ind,
             NULL,         --class_vat_ind,
             NULL,         --hier_level,
             NULL,         --hier_num_value,
             NULL,         --hier_char_value,
             NULL,
             NULL,
             'N',
             NULL,
             NULL
        from (select loc,
                     item
                from svc_posupld_nil nil
               union all
              select nil.loc,
                     ixh.head_item item
                from svc_posupld_nil nil,
                     item_xform_head ixh,
                     item_xform_detail ixd
               where nil.item = ixd.detail_item
                 and ixd.item_xform_head_id = ixh.item_xform_head_id) svc;

   L_input  NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;

BEGIN

   open C_ITEMLOC;
   fetch C_ITEMLOC BULK COLLECT into L_input;
   close C_ITEMLOC;
   
   if L_input.COUNT > 0 then
      if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                       L_input) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.RANGE_ITEM_LOC',
                                            to_char(SQLCODE));
      return FALSE;
END RANGE_ITEM_LOC;
----------------------------------------------------------------------------------------
FUNCTION RANGE_ITEM_LOC(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_sales_process_id   IN              NUMBER,
                        I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   cursor C_RANGE is
      select pli.im_item,
             pli.loc_value,
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             pli.im_item_level,
             pli.im_tran_level,
             pli.im_waste_type,
             pli.im_waste_pct,
             pli.im_pack_ind,
             pli.tran_date,
             pli.rowid gtt_row_id
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and pli.error_msg        is null
         and not exists(select 'x'
                          from item_loc il
                         where il.item = pli.im_item
                           and il.loc  = pli.loc_value
                           and rownum = 1);

   TYPE t_range_recs IS TABLE OF C_RANGE%ROWTYPE INDEX BY BINARY_INTEGER;

   L_range_recs_tab   T_RANGE_RECS;

BEGIN
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_RANGE;
   fetch C_RANGE bulk collect into L_range_recs_tab;
   close C_RANGE;

   if L_range_recs_tab.FIRST is not null then
      FOR range_ind in L_range_recs_tab.FIRST..L_range_recs_tab.LAST LOOP
         if NEW_ITEM_LOC(O_error_message,
                         L_range_recs_tab(range_ind).im_item,
                         L_range_recs_tab(range_ind).LOC_VALUE,
                         null, -- ITEM_PARENT
                         null, -- ITEM_GRANDPARENT
                         'S', -- LOC_TYPE
                         null, -- SHORT_DESC
                         L_range_recs_tab(range_ind).im_dept,
                         L_range_recs_tab(range_ind).im_class,
                         L_range_recs_tab(range_ind).im_subclass,
                         L_range_recs_tab(range_ind).im_item_level,
                         L_range_recs_tab(range_ind).im_tran_level,
                         null, -- ITEM_STATUS
                         L_range_recs_tab(range_ind).im_waste_type,
                         L_range_recs_tab(range_ind).im_waste_pct,
                         null, -- SELLABLE_IND
                         null, -- ORDERABLE_IND
                         L_range_recs_tab(range_ind).im_pack_ind,
                         null, -- PACK_TYPE
                         null, -- UNIT_COST_LOC
                         null, -- UNIT_RETAIL_LOC
                         null, -- SELLING_RETAIL_LOC
                         null, -- SELLING_UOM
                         null, -- ITEM_LOC_STATUS
                         null, -- TAXABLE_IND
                         null, -- TI
                         null, -- HI
                         null, -- STORE_ORD_MULT
                         null, -- MEAS_OF_EACH
                         null, -- MEAS_OF_PRICE
                         null, -- UOM_OF_PRICE
                         null, -- PRIMARY_VARIANT
                         null, -- PRIMARY_SUPP
                         null, -- PRIMARY_CNTRY
                         null, -- LOCAL_ITEM_DESC
                         null, -- LOCAL_SHORT_DESC
                         null, -- PRIMARY_COST_PACK
                         null, -- RECEIVE_AS_TYPE
                         L_range_recs_tab(range_ind).tran_date,
                         null, -- DEFAULT_TO_CHILDREN
                         null, -- LIKE_STORE
                         null, -- ITEM_DESC
                         null, -- DIFF_1
                         null, -- DIFF_2
                         null, -- DIFF_3
                         null, -- DIFF_4
                         null, -- LANG
                         null, -- CLASS_VAT_IND
                         null, -- ELC_IND
                         null, -- STD_AV_IND
                         null, -- VAT_IND
                         null, -- RPM_IND
                         null, -- INBOUND_HANDLING_DAYS
                         null, -- GROUP_TYPE
                         null, -- STORE_PRICE_IND
                         null, -- UIN_TYPE
                         null, -- UIN_LABEL
                         null, -- CAPTURE_TIME
                         null, -- EXT_UIN_IND
                         null, -- SOURCE_METHOD
                         null, -- SOURCE_WH
                         'Y', --NEW_ITEM_LOC_IND 
                         'N', --RANGED_IND 
                         null, -- COSTING_LOC 
                         null  -- COSTING_LOC_TYPE
                         ) = FALSE then
            return FALSE;
         end if;
      END LOOP;

      FORALL upd_range IN L_range_recs_tab.FIRST..L_range_recs_tab.LAST
         update svc_posupld_line_item_gtt pli
            set new_item_loc_ind = 'Y'
          where pli.rowid = L_range_recs_tab(upd_range).gtt_row_id;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.RANGE_ITEM_LOC',
                                            to_char(SQLCODE));
      return FALSE;
END RANGE_ITEM_LOC;
----------------------------------------------------------------------------------------
FUNCTION PROCESS_SALES(I_sales_process_id    IN              NUMBER,
                       I_chunk_id            IN              NUMBER,
                       O_error_message       IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_exist   BOOLEAN;

BEGIN
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_rec) = FALSE then
      return FALSE;
   end if;

   if SETUP_WORK_TABLE(O_error_message,
                       I_sales_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if CHECK_RECS_INSERTED(O_error_message,
                          L_exist,
                          I_sales_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if L_exist = FALSE then
      if COPY_ERROR_DETAILS(O_error_message,
                            I_sales_process_id,
                            I_chunk_id) = FALSE then
         return FALSE;
      end if;

      if UPDATE_STATUS(O_error_message,
                       I_sales_process_id,
                       I_chunk_id) = FALSE then
         return FALSE;
      end if;
      return TRUE;
   end if;

   if WORK_TABLE_DENORMALIZE(O_error_message,
                             I_sales_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if XFORM_HELPERS(O_error_message,
                    I_sales_process_id,
                    I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if SETUP_DISC_WORK_TABLE(O_error_message,
                            I_sales_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST(O_error_message,
                                            I_sales_process_id,
                                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if COPY_ERROR_DETAILS(O_error_message,
                         I_sales_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if UPDATE_STATUS(O_error_message,
                    I_sales_process_id,
                    I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.PROCESS_SALES',
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_SALES;
----------------------------------------------------------------------------------------
FUNCTION PURGE_ARC_REJECTS(I_num_days        IN              NUMBER,
                           O_error_message   IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_num_days                NUMBER;
   L_min_date                VARCHAR2(12);
   L_date_arch               VARCHAR2(12);
   L_reject_records_exists   VARCHAR2(1) := 'N';

   cursor C_GET_MIN_DATE is
      select to_char(min(reject_date),'MMDDYYYY')
        from svc_posupld_rej_recs,
             period
       where trunc(reject_date) < vdate - I_num_days;

   cursor C_GET_REJECT_RECORDS is
      select 'Y'
        from svc_posupld_rej_recs
       where trunc(reject_date)  between to_date(L_min_date, 'MMDDYYYY') and to_date(L_date_arch, 'MMDDYYYY')
         and rownum = 1;

BEGIN
   ---
   if I_num_days is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_num_days',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_MIN_DATE;
   fetch C_GET_MIN_DATE into L_min_date;

   if (C_GET_MIN_DATE%NOTFOUND) then
      close C_GET_MIN_DATE;
      return TRUE;
   else
      close C_GET_MIN_DATE;
   end if;

   L_num_days := I_num_days+1;
   L_date_arch := TO_CHAR(get_vdate-L_num_days,'MMDDYYYY');

   open C_GET_REJECT_RECORDS;
   fetch C_GET_REJECT_RECORDS into L_reject_records_exists;

   if (C_GET_REJECT_RECORDS%NOTFOUND) then
      L_reject_records_exists :='N';
      close C_GET_REJECT_RECORDS;
   else
      close C_GET_REJECT_RECORDS;

      delete svc_posupld_rej_recs
       where trunc(reject_date) between to_date(L_min_date, 'MMDDYYYY') and to_date(L_date_arch, 'MMDDYYYY');
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.PURGE_ARC_REJECTS',
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_ARC_REJECTS;
----------------------------------------------------------------------------------------
FUNCTION PURGE_STATUS(O_error_message     IN OUT     NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
  L_temp_process_id         SVC_POSUPLD_STATUS.PROCESS_ID%TYPE;
  
  cursor C_GET_PROCESS_IDS is
     select process_id
       from svc_posupld_status stat
      where status != 'N'
        and not exists (select sales_process_id
                          from svc_posupld_load load
                         where load.sales_process_id = stat.process_id
                        union
                        select sales_process_id
                          from svc_posupld_load_arch arch
                         where arch.sales_process_id = stat.process_id                
                        union 
                        select sales_process_id
                          from svc_posupld_rej_recs rej
                         where rej.sales_process_id = stat.process_id);
                         
  cursor C_LOCK_DEL_RECS is
     select 1
       from svc_posupld_status
      where process_id = L_temp_process_id
        for update nowait;   
        
  TYPE t_id_recs IS TABLE OF C_GET_PROCESS_IDS%ROWTYPE INDEX BY BINARY_INTEGER;
  L_id_recs_tab    T_ID_RECS;

BEGIN

  open C_GET_PROCESS_IDS;
  fetch C_GET_PROCESS_IDS bulk collect into L_id_recs_tab;
  close C_GET_PROCESS_IDS;

  if L_id_recs_tab.first is not null then
      for i in L_id_recs_tab.first..L_id_recs_tab.last LOOP
         L_temp_process_id := L_id_recs_tab(i).process_id;
         open C_LOCK_DEL_RECS;
         close C_LOCK_DEL_RECS;
      end LOOP;
      FORALL i in L_id_recs_tab.first..L_id_recs_tab.last
          delete svc_posupld_status
           where process_id = L_id_recs_tab(i).process_id; 
  end if;
  
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.PURGE_STATUS',
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_STATUS;
----------------------------------------------------------------------------------------
FUNCTION PURGE_ARCHIVE(I_num_days        IN              NUMBER,
                       O_error_message   IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_date_arch            VARCHAR2(12);
   L_exec_string          VARCHAR2(2000);
   L_interval_clause      ALL_PART_TABLES.INTERVAL%TYPE;
   L_high_value           ALL_TAB_PARTITIONS.HIGH_VALUE%TYPE;
   L_partition_name       ALL_TAB_PARTITIONS.PARTITION_NAME%TYPE;
   L_interval             ALL_TAB_PARTITIONS.INTERVAL%TYPE;
   L_high_value_comp      VARCHAR2(9);
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_table_owner          SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_counter              NUMBER(8);

   cursor C_GET_INTERVAL_INFO is
     select interval
       from all_part_tables
      where table_name = 'SVC_POSUPLD_LOAD_ARCH'
        and owner = L_table_owner;

   cursor C_GET_PARTITION_INFO is
     select partition_name,
            high_value,
            interval
       from all_tab_partitions
      where table_name = 'SVC_POSUPLD_LOAD_ARCH'
        and table_owner = L_table_owner
      order by partition_position;
      
   cursor C_PARTITION_COUNT is
     select count(1)
       from all_tab_partitions
      where table_owner = L_table_owner
        and table_name = 'SVC_POSUPLD_LOAD_ARCH';

BEGIN
   ---
   if I_num_days is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_num_days',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   L_date_arch := TO_CHAR(get_vdate-I_num_days,'MMDDYYYY');

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_table_owner := L_system_options_row.table_owner;

   open C_GET_INTERVAL_INFO;
   fetch C_GET_INTERVAL_INFO into L_interval_clause;
   close C_GET_INTERVAL_INFO;

   FOR rec in C_GET_PARTITION_INFO LOOP
      L_partition_name := rec.partition_name;
      L_high_value     := rec.high_value;
      L_interval       := rec.interval;
      L_counter        := 0;

      /* convert the date to a format that can be compared */
      L_exec_string := 'select TO_CHAR('||L_high_value||','||''''||'MMDDYYYY'||''''||') from dual';
      execute immediate (L_exec_string) into L_high_value_comp;
      
      open C_PARTITION_COUNT;
      fetch C_PARTITION_COUNT into L_counter;
      close C_PARTITION_COUNT;

      if L_counter > 1 then
         if TO_DATE(L_high_value_comp,'MMDDYYYY') <= TO_DATE(L_date_arch,'MMDDYYYY') then
            if L_interval = 'NO' and L_interval_clause is NOT NULL then
               /* turn off interval */
               L_exec_string := 'alter table '||L_table_owner||'.'||'svc_posupld_load_arch set interval ()';
               execute immediate (L_exec_string);

               L_exec_string := NULL;
               L_exec_string := 'alter table '||L_table_owner||'.'||'svc_posupld_load_arch drop partition '||L_partition_name||' update indexes';
               execute immediate (L_exec_string);

               /* turn on interval */
               L_exec_string := NULL;
               L_exec_string := 'alter table '||L_table_owner||'.'||'svc_posupld_load_arch set interval ('||L_interval_clause||')';
               execute immediate (L_exec_string);
            else
               L_exec_string := 'alter table '||L_table_owner||'.'||'svc_posupld_load_arch drop partition '||L_partition_name||' update indexes';
               execute immediate (L_exec_string);
            end if;
         else
            exit;
         end if;
      end if;
   END LOOP;

   if PURGE_ARC_REJECTS(I_num_days,
                        O_error_message) = FALSE then
      return FALSE;
   end if;

   if PURGE_STATUS(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.PURGE_ARCHIVE',
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_ARCHIVE;
----------------------------------------------------------------------------------------
FUNCTION ARCHIVE_RECORDS(O_error_message   IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_temp_process_id   SVC_POSUPLD_LOAD.SALES_PROCESS_ID%TYPE;
   L_table             VARCHAR2(30) := 'SVC_POSUPLD_LOAD';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_PROCESS_IDS is
      select distinct arc.sales_process_id
        from svc_posupld_load arc
       where exists (select 1
                       from svc_posupld_load_arch ld
                      where arc.sales_process_id = ld.sales_process_id
                        and rownum = 1);
                        
   --Get the process ids of those files have no transactions in them. These files will have just FHEAD and FTAIL records.
   cursor C_GET_BLANK_PROCESS_IDS is
      select distinct arc.sales_process_id
        from svc_posupld_load arc
       where not exists (select 1
                       from svc_posupld_load_arch ld
                      where arc.sales_process_id = ld.sales_process_id
                        and rownum = 1)
         and file_type in ('FHEAD', 'FTAIL');  

   cursor C_LOCK_DEL_RECS is
      select 1
        from svc_posupld_load
       where sales_process_id = L_temp_process_id
         for update nowait;

   TYPE t_id_recs IS TABLE OF C_GET_PROCESS_IDS%ROWTYPE INDEX BY BINARY_INTEGER;

   L_id_recs_tab   T_ID_RECS;

BEGIN

   insert into svc_posupld_load_arch(file_type,
                                     sales_process_id,
                                     line_id,
                                     line_seq_id,
                                     line_text,
                                     thread_val,
                                     filename,
                                     process_date)
      select file_type,
             sales_process_id,
             line_id,
             line_seq_id,
             line_text,
             thread_val,
             filename,
             sysdate
        from svc_posupld_load load
       where not exists (select 1
                           from svc_posupld_rej_recs rej
                          where rej.sales_process_id = load.sales_process_id
                            and load.line_seq_id = rej.line_seq_id)
         and file_type not in ('FHEAD', 'FTAIL');

   open C_GET_PROCESS_IDS;
   fetch C_GET_PROCESS_IDS bulk collect into L_id_recs_tab;
   close C_GET_PROCESS_IDS;

   if L_id_recs_tab.FIRST is not null then
      insert into svc_posupld_load_arch(file_type,
                                        sales_process_id,
                                        line_id,
                                        line_seq_id,
                                        line_text,
                                        thread_val,
                                        filename,
                                        process_date)
         select file_type,
                sales_process_id,
                line_id,
                line_seq_id,
                line_text,
                thread_val,
                filename,
                sysdate
           from svc_posupld_load load
          where exists (select 1
                          from svc_posupld_load_arch arch
                         where arch.sales_process_id = load.sales_process_id)
            and file_type in ('FHEAD', 'FTAIL');

      FOR proc_ind in L_id_recs_tab.FIRST..L_id_recs_tab.LAST LOOP
         L_temp_process_id := L_id_recs_tab(proc_ind).sales_process_id;
         open C_LOCK_DEL_RECS;
         close C_LOCK_DEL_RECS;
      END LOOP;

      FORALL err_ind in L_id_recs_tab.FIRST..L_id_recs_tab.LAST
         delete svc_posupld_load where sales_process_id = L_id_recs_tab(err_ind).sales_process_id;
   end if;
   L_id_recs_tab.DELETE;
   
   --deleting blank file records.
   open C_GET_BLANK_PROCESS_IDS;
   fetch C_GET_BLANK_PROCESS_IDS bulk collect into L_id_recs_tab;
   close C_GET_BLANK_PROCESS_IDS;  
   
   if L_id_recs_tab.FIRST is not null then   
      FOR proc_ind in L_id_recs_tab.FIRST..L_id_recs_tab.LAST LOOP
         L_temp_process_id := L_id_recs_tab(proc_ind).sales_process_id;
         open C_LOCK_DEL_RECS;
         close C_LOCK_DEL_RECS;
      END LOOP;   
      
      FORALL err_ind in L_id_recs_tab.FIRST..L_id_recs_tab.LAST
         delete svc_posupld_load where sales_process_id = L_id_recs_tab(err_ind).sales_process_id;
   end if;  

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_temp_process_id,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.ARCHIVE_RECORDS',
                                            to_char(SQLCODE));
      return FALSE;
END ARCHIVE_RECORDS;
----------------------------------------------------------------------------------------
FUNCTION GENERATE_REJ_FILE(I_sales_process_id   IN              NUMBER,
                           O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   cursor C_COUNT_REJ_RECS is
       select count(*)
         from svc_posupld_rej_recs
        where sales_process_id = I_sales_process_id;

   cursor C_CHK_FHEAD_FTAIL_ERR is
       select 'x'
         from svc_posupld_load
        where sales_process_id = I_sales_process_id
          and error_msg is not null
          and file_type in ('FHEAD', 'FTAIL');

   cursor C_CHK_DETL_ERR is
       select 'x'
         from svc_posupld_status
        where process_id = I_sales_process_id
          and status != 'C'
          and rownum = 1;

   cursor C_CHK_REJ_RECS is
       select 'x'
         from svc_posupld_rej_recs
        where sales_process_id = I_sales_process_id
          and rownum = 1;

   cursor C_GET_REJECTS is
      --following writes the THEAD/TTAIL/TDETL/TTAIL lines with error.
      select vspl1.file_type,
             vspl1.sales_process_id,
             vspl1.line_seq_id,
             vspl1.chunk_id,
             spl.line_text,
             vspl1.filename,
             vspl1.error_msg,
             sysdate
        from svc_posupld_staging vspl1,
             svc_posupld_load    spl
       where vspl1.error_msg is not null
         and vspl1.sales_process_id = I_sales_process_id
         and spl.sales_process_id = vspl1.sales_process_id
         and spl.line_seq_id = vspl1.line_seq_id
       union
      --following writes the TTAIL/TDETL/TTAX lines if the corresponding THEAD has an error.
      select vspl1.file_type,
             vspl1.sales_process_id,
             vspl1.line_seq_id,
             vspl1.chunk_id,
             spl.line_text,
             vspl1.filename,
             vspl1.error_msg,
             sysdate
        from svc_posupld_staging  vspl1,
             svc_posupld_load    spl
       where vspl1.sales_process_id = I_sales_process_id
         and spl.sales_process_id = vspl1.sales_process_id
         and spl.line_seq_id = vspl1.line_seq_id
         and vspl1.file_type in ('TTAIL', 'TDETL', 'TTAX')
         and exists (select 1
                       from svc_posupld_staging vspl2
                      where vspl2.sales_process_id = vspl1.sales_process_id
                        and vspl1.head_detl_id = vspl2.thead_id
                        and vspl2.file_type = 'THEAD'
                        and vspl2.error_msg is not null)
       union
      --following writes the THEAD line if the corresponding TTAIL/TDETL/TTAX has an error.
      select vspl1.file_type,
             vspl1.sales_process_id,
             vspl1.line_seq_id,
             vspl1.chunk_id,
             spl.line_text,
             vspl1.filename,
             vspl1.error_msg,
             sysdate
        from svc_posupld_staging vspl1,
             svc_posupld_load    spl
       where vspl1.sales_process_id = I_sales_process_id
         and spl.sales_process_id = vspl1.sales_process_id
         and spl.line_seq_id = vspl1.line_seq_id
         and vspl1.file_type = 'THEAD'
         and exists (select 1
                       from svc_posupld_staging vspl2
                      where vspl2.sales_process_id = vspl1.sales_process_id
                        and vspl2.head_detl_id = vspl1.thead_id
                        and vspl2.file_type in ('TTAIL', 'TDETL', 'TTAX')
                        and vspl2.error_msg is not null)      
       union
       --following writes the TTAX line if the corresponding TTAX/TDETL/TTAIL has an error.
      select vspl1.file_type,
             vspl1.sales_process_id,
             vspl1.line_seq_id,
             vspl1.chunk_id,
             spl.line_text,
             vspl1.filename,
             vspl1.error_msg,
             sysdate
        from svc_posupld_staging  vspl1,
             svc_posupld_load    spl
       where vspl1.sales_process_id = I_sales_process_id
         and spl.sales_process_id = vspl1.sales_process_id
         and spl.line_seq_id = vspl1.line_seq_id
         and vspl1.file_type = 'TTAX'
         and exists (select 1
                       from svc_posupld_staging vspl3
                      where vspl3.sales_process_id = vspl1.sales_process_id
                        and vspl3.head_detl_id = vspl1.head_detl_id
                        and vspl3.file_type in ('TTAX','TDETL','TTAIL')
                        and vspl3.error_msg is not null)
       union       
       --following writes the TDETL line if the corresponding TDETL/TTAX/TTAIL has an error.
      select vspl1.file_type,
             vspl1.sales_process_id,
             vspl1.line_seq_id,
             vspl1.chunk_id,
             spl.line_text,
             vspl1.filename,
             vspl1.error_msg,
             sysdate
        from svc_posupld_staging  vspl1,
             svc_posupld_load    spl
       where vspl1.sales_process_id = I_sales_process_id
         and spl.sales_process_id = vspl1.sales_process_id
         and spl.line_seq_id = vspl1.line_seq_id
         and vspl1.file_type = 'TDETL'
         and exists (select 1
                       from svc_posupld_staging vspl3
                      where vspl3.sales_process_id = vspl1.sales_process_id
                        and vspl3.head_detl_id = vspl1.head_detl_id
                        and vspl3.file_type in ('TDETL','TTAX','TTAIL')
                        and vspl3.error_msg is not null)
       union
       --following writes the TTAIL line if the corresponding TDETL/TTAX has an error.
      select vspl1.file_type,
             vspl1.sales_process_id,
             vspl1.line_seq_id,
             vspl1.chunk_id,
             spl.line_text,
             vspl1.filename,
             vspl1.error_msg,
             sysdate
        from svc_posupld_staging  vspl1,
             svc_posupld_load    spl
       where vspl1.sales_process_id = I_sales_process_id
         and spl.sales_process_id = vspl1.sales_process_id
         and spl.line_seq_id = vspl1.line_seq_id
         and vspl1.file_type = 'TTAIL'
         and exists (select 1
                       from svc_posupld_staging vspl3
                      where vspl3.sales_process_id = vspl1.sales_process_id
                        and vspl3.head_detl_id = vspl1.head_detl_id
                        and vspl3.file_type in ('TDETL','TTAX')
                        and vspl3.error_msg is not null)
       union     
      select vspl1.file_type,
             vspl1.sales_process_id,
             vspl1.line_seq_id,
             vspl1.chunk_id,
             spl.line_text,
             vspl1.filename,
             vspl1.error_msg,
             sysdate
        from svc_posupld_staging  vspl1,
             svc_posupld_load    spl
       where vspl1.sales_process_id = I_sales_process_id
         and spl.sales_process_id = vspl1.sales_process_id
         and spl.line_seq_id = vspl1.line_seq_id
         and exists (select 1
                       from svc_posupld_status
                      where process_id = I_sales_process_id
                        and status = 'E'
                        and chunk_id = vspl1.chunk_id);

   L_num_recs   NUMBER := 0;
   L_dummy      VARCHAR2(1);

   TYPE t_reject_recs IS TABLE OF C_GET_REJECTS%ROWTYPE INDEX BY BINARY_INTEGER;

   L_reject_tab   T_REJECT_RECS;

BEGIN

   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   open C_CHK_FHEAD_FTAIL_ERR;
   fetch C_CHK_FHEAD_FTAIL_ERR into L_dummy;
   close C_CHK_FHEAD_FTAIL_ERR;

   if L_dummy is not null then
      insert into svc_posupld_staging(file_type,
                                      filename,
                                      line_seq_id,
                                      thread_val,
                                      location,
                                      sales_process_id,
                                      chunk_id,
                                      thead_id,
                                      head_detl_id,
                                      ttax_id,
                                      tdetl_id,
                                      error_msg)
                               select file_type,
                                      filename,
                                      line_seq_id,
                                      thread_val,
                                      location,
                                      sales_process_id,
                                      chunk_id,
                                      thead_id,
                                      head_detl_id,
                                      ttax_id,
                                      tdetl_id,
                                      error_msg
                                 from svc_posupld_staging_rej vspl
                                where sales_process_id = I_sales_process_id
                                  and exists (select 1
                                                from svc_posupld_status
                                               where vspl.sales_process_id = process_id
                                                 and chunk_id is not null
                                                 and status != 'C'
                                                 and rownum = 1);

      insert into svc_posupld_rej_recs(file_type,
                                       sales_process_id,
                                       line_seq_id,
                                       chunk_id,
                                       line_text,
                                       filename,
                                       error_msg,
                                       reject_date)
         select vspl1.file_type,
                vspl1.sales_process_id,
                vspl1.line_seq_id,
                vspl1.chunk_id,
                spl.line_text,
                vspl1.filename,
                vspl1.error_msg,
                sysdate
           from svc_posupld_staging vspl1,
                svc_posupld_load    spl
          where vspl1.sales_process_id = I_sales_process_id
            and spl.sales_process_id = vspl1.sales_process_id
            and spl.line_seq_id = vspl1.line_seq_id;
   else
      L_dummy := NULL;
      open C_CHK_DETL_ERR;
      fetch C_CHK_DETL_ERR into L_dummy;
      close C_CHK_DETL_ERR;

      if L_dummy is null then
         return TRUE;
      end if;

      L_dummy := NULL;

      open C_CHK_REJ_RECS;
      fetch C_CHK_REJ_RECS into L_dummy;
      close C_CHK_REJ_RECS;

      if L_dummy is null then
         insert into svc_posupld_staging(file_type,
                                         filename,
                                         line_seq_id,
                                         thread_val,
                                         location,
                                         sales_process_id,
                                         chunk_id,
                                         thead_id,
                                         head_detl_id,
                                         ttax_id,
                                         tdetl_id,
                                         error_msg)
            select file_type,
                   filename,
                   line_seq_id,
                   thread_val,
                   location,
                   sales_process_id,
                   chunk_id,
                   thead_id,
                   head_detl_id,
                   ttax_id,
                   tdetl_id,
                   error_msg
              from svc_posupld_staging_rej vspl
             where sales_process_id = I_sales_process_id
               and exists (select 1
                             from svc_posupld_status
                            where vspl.sales_process_id = process_id
                              and ((chunk_id = vspl.chunk_id
                                  and status != 'C')
                                  or chunk_id is null)
                              and rownum = 1);

         open C_GET_REJECTS;
         fetch C_GET_REJECTS bulk collect into L_reject_tab;
         close C_GET_REJECTS;

         FORALL rej_ind in L_reject_tab.FIRST..L_reject_tab.LAST
            insert into svc_posupld_rej_recs values L_reject_tab(rej_ind);
      end if;

      open C_COUNT_REJ_RECS;
      fetch C_COUNT_REJ_RECS into L_num_recs;
      close C_COUNT_REJ_RECS;
      ---
      if L_num_recs != 0 then
         insert into svc_posupld_rej_recs(file_type,
                                          sales_process_id,
                                          line_seq_id,
                                          chunk_id,
                                          line_text,
                                          filename,
                                          error_msg,
                                          reject_date)
            select vspl1.file_type,
                   vspl1.sales_process_id,
                   vspl1.line_seq_id,
                   vspl1.chunk_id,
                   case when vspl1.file_type = 'FHEAD' then
                      spl.line_text
                   else
                      substr(line_text, 1, 15)||lpad(L_num_recs, 10, 0)
                   end as line_text,
                   vspl1.filename,
                   vspl1.error_msg,
                   sysdate
              from svc_posupld_staging  vspl1,
                   svc_posupld_load    spl
             where vspl1.sales_process_id = I_sales_process_id
               and spl.sales_process_id = vspl1.sales_process_id
               and spl.line_seq_id = vspl1.line_seq_id
               and vspl1.file_type in ('FHEAD', 'FTAIL')
               and exists (select 1
                             from svc_posupld_status vspl3
                            where vspl3.process_id = vspl1.sales_process_id
                              and vspl3.status in ('E', 'R')
                              and rownum = 1);
      end if;
   end if;
   ---
   update svc_posupld_status
      set status='C'
    where process_id = I_sales_process_id
      and status in ('E','R');   
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.GENERATE_REJ_FILE',
                                            to_char(SQLCODE));
      return FALSE;
END GENERATE_REJ_FILE;
----------------------------------------------------------------------------------------
FUNCTION CHUNK_STORE(O_error_message         IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                     I_location              IN            STORE.STORE%TYPE)
RETURN BOOLEAN IS

BEGIN

   merge into svc_posupld_staging t
   using (select s.*,
                 CASE
                 WHEN file_type = 'THEAD'
                 THEN
                    CEIL (thead_chunk / max_chunk_size)
                 WHEN file_type IN ('TDETL', 'TTAIL','TTAX')
                 THEN
                    CEIL((MAX(thead_chunk)
                          OVER (PARTITION BY thread_val,
                                             sales_process_id,
                                             head_detl_id
                                    ORDER BY head_detl_id, 
                                             line_seq_id
                                        ROWS BETWEEN UNBOUNDED PRECEDING
                                         AND 1 PRECEDING))/ max_chunk_size)
                 ELSE
                 NULL
                 END AS new_chunk_id
            From (select stage.rowid rid,
                         stage.*,
                         (SELECT max_chunk_size
                            FROM RMS_PLSQL_BATCH_CONFIG
                           WHERE program_name = 'CORESVC_SALES_UPLOAD_SQL') AS max_chunk_size,
                         DECODE(file_type,
                                'THEAD',
                                DENSE_RANK () OVER(PARTITION BY file_type
                                                    ORDER BY col_3)) thead_chunk               -- dense rank on item to make sure same items fall in the same chunk
                    from svc_posupld_staging stage
                   where stage.sales_process_id in (select process_id 
                                                      from svc_posupld_status 
                                                     where location = I_location
                                                       and status = 'N')) s
         ) use_this
      on (use_this.rid=t.rowid)
   when matched then update 
   set t.chunk_id = use_this.new_chunk_id;

   merge into SVC_POSUPLD_STATUS ss
   using( SELECT DISTINCT sps.filename,
                          sps.thread_val,
                          sps.location,
                          sps.sales_process_id,
                          sps.chunk_id
                     FROM svc_posupld_staging sps,
                          svc_posupld_status s
                    WHERE sps.location = I_location
                      AND s.reference_id = sps.filename(+)
                      and s.thread_val = sps.thread_val(+)
                      and s.location = sps.location(+)
                      and s.process_id = sps.sales_process_id(+)
                      and NVL(s.status,'N') != 'C') use_this
   on (ss.reference_id = use_this.filename
       and ss.thread_val = use_this.thread_val
       and ss.location = use_this.location
       and ss.process_id = use_this.sales_process_id
       and NVL(ss.chunk_id,-999) = NVL(use_this.chunk_id,-999))
   when matched then 
   update set status = 'N',
              error_msg = null,
              last_update_datetime = sysdate              
   when not matched then
   insert (reference_id,
           thread_val,
           location,
           process_id,
           chunk_id,
           status,
           last_update_datetime)
    values (use_this.filename,
            use_this.thread_val,
            use_this.location,
            use_this.sales_process_id,
            use_this.chunk_id,
            'N',
            sysdate);
                       
   delete from svc_posupld_status 
    where location = I_location 
      and chunk_id = 0 
      and status = 'N';
      
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_SQL.CHUNK_STORE',
                                            to_char(SQLCODE));
      return FALSE;
END CHUNK_STORE;
----------------------------------------------------------------------------------------
END CORESVC_SALES_UPLOAD_SQL;
/
