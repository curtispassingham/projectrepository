
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE INVC_VALIDATE_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------------------
--Function Name:  INVC_EXIST
--Purpose      :  This function checks whether invoice number exists on invc_head table and
--                returns TRUE if invoice exists and FALSE if it is not. 
--Date Created :  13-October-98    Rebecca Dobosh
-------------------------------------------------------------------------------------------
FUNCTION INVC_EXIST (O_error_message   IN OUT  VARCHAR2,
                     O_exists          IN OUT  BOOLEAN,
                     I_invc_id         IN      INVC_HEAD.INVC_ID%TYPE) 
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------
--Function Name:  MATCH_RCPT
--Purpose      :  This function will check to see if a receipt is valid (meaning it exists
--                on the shipsku table but is not yet matched to another invoice)to be matched 
--                to an invoice line item. 
--Date Created :  13-October-98    Rebecca Dobosh
-------------------------------------------------------------------------------------------

FUNCTION MATCH_RCPT (O_error_message   IN OUT  VARCHAR2,
                     O_valid           IN OUT  BOOLEAN,
                     I_rcpt            IN      INVC_MATCH_WKSHT.SHIPMENT%TYPE,
                     I_invc_id         IN      INVC_MATCH_WKSHT.INVC_ID%TYPE,
                     I_item            IN      INVC_MATCH_WKSHT.ITEM%TYPE,
                     I_carton          IN      INVC_MATCH_WKSHT.CARTON%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name:  CHECK_MAX_DBT_PCT
--Purpose:        This function will check to see if the total costs of the debit memos and 
--                credit note requests referenced to a given invoice are a larger percentage
--                of the invoice's total cost than the system debit max percent.  If so 
--                O_over_max_dbt will be set to FALSE else it will be set to true.
--Created:     25-Feb-99
---------------------------------------------------------------------------------------------
FUNCTION CHECK_MAX_DBT_PCT(O_error_message  IN OUT  VARCHAR2,
                           O_over_max_dbt   IN OUT  BOOLEAN,
                           I_invc_id        IN      INVC_HEAD.INVC_ID%TYPE) 
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--    Name:  RECONCILE_DETAILS
-- Purpose:  This function will reconcile the details with the total cost/qty on the 
--           invoice head.  If the totals match, O_reconcile will be set to TRUE.
-- Created:  12-OCT-1999, Kevin Hearnen
---------------------------------------------------------------------------------------------
FUNCTION RECONCILE_DETAILS(O_error_message  IN OUT  VARCHAR2,
                           I_invc_id        IN      INVC_HEAD.INVC_ID%TYPE) 
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--    Name:  VAL_SERV_PERF_IND
-- Purpose:  This function will validate the service performanced indicator value to be 
--           stored in the invc_non_merch table.  The function validation will return its result in
--           the output variable O_serv_perf_fail upon completion.  It also checks on 
--           the supplier or partner attribute which associated with an invoice. The attribute
--           indicates that a service needs to be performed before an invoice of the service 
--           can be processed.  Default value of the O_service_perf_fail = False.
-- Created:  17-JAN-2000
---------------------------------------------------------------------------------------------
FUNCTION VAL_SERV_PERF_IND(O_error_message      IN OUT  VARCHAR2,
                           O_service_perf_fail  IN OUT  BOOLEAN,
                           I_invc_id            IN      INVC_HEAD.INVC_ID%TYPE,
                           I_supplier		IN      SUPS.SUPPLIER%TYPE,
                           I_partner_id         IN      PARTNER.PARTNER_ID%TYPE,
                           I_partner_type       IN      PARTNER.PARTNER_TYPE%TYPE) 
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--   Name:    LC_COMP_NON_MERCH_EXIST
--   Purpose  This function will execute a query with the input parameters in the 'where' 
--            clause to determine if the landed-cost component is associated with a non-merchandise
--            code on the NON_MERCH_CODE_COMP table. The function will output a boolean indicator 
--            O_exists with a value= FALSE if the component is not associated with the 
--            non-merchandise code or a TRUE if it is.  
--            Created 24-JAN_2000    Rachana Pandey
---------------------------------------------------------------------------------------------
FUNCTION      LC_COMP_NON_MERCH_EXIST(O_error_message   IN OUT  VARCHAR2,
                                      O_exists          IN OUT  BOOLEAN,
                                      I_non_merch_code  IN             NON_MERCH_CODE_COMP.NON_MERCH_CODE%TYPE,
                                      I_comp_id         IN      NON_MERCH_CODE_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--   Name:    NON_MERCH_LC_COMP_EXIST
--   Purpose  This function will execute a query with the input parameters in the 'where' 
--            clause to determine if landed-cost components exist for the input non-merchandise
--            code on the NON_MERCH_CODE_COMP table. The function will output a boolean indicator 
--            O_exists with a value= FALSE if no components exist for the
--            input non-merchandise code or a TRUE if cost-components do exist.  
--            Created 24-JAN_2000    Rachana Pandey
---------------------------------------------------------------------------------------------
FUNCTION      NON_MERCH_LC_COMP_EXIST(O_error_message  IN OUT  VARCHAR2,
                                      O_exists         IN OUT  BOOLEAN,
                                      I_non_merch_code IN      INVC_NON_MERCH.NON_MERCH_CODE%TYPE)
                                  
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--   Name:    NON_MERCH_CODE_EXIST
--   Purpose  This function will execute a query with the input parameters in the 'where' 
--            clause to determine if the input non-merchandise
--            code exists on the NON_MERCH_CODE_HEAD table. The function will output a boolean indicator 
--            O_exists with a value= FALSE if the input non-merchandise code does not exist on the 
--            non-merchandise code table or a TRUE if it does.  
--            Created 24-JAN_2000    Rachana Pandey
---------------------------------------------------------------------------------------------
FUNCTION     NON_MERCH_CODE_EXIST(O_error_message   IN OUT  VARCHAR2,
                                  O_exists          IN OUT  BOOLEAN,
                                  I_non_merch_code  IN      NON_MERCH_CODE_HEAD.NON_MERCH_CODE%TYPE)
                                  
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--   Name:    INVC_NON_MERCH_EXIST
--   Purpose  This function will execute a query with the input parameters in the 'where' 
--            clause to determine if the input non-merchandise
--            code exists on the INVC_NON_MERCH table. The function will output a boolean indicator 
--            O_exists with a value= FALSE if the input non-merchandise code does not exist on the 
--            invc_non_merch table or a TRUE if it does.  
--            Created 26-JAN_2000    Rachana Pandey
---------------------------------------------------------------------------------------------
FUNCTION     INVC_NON_MERCH_EXIST(O_error_message   IN OUT  VARCHAR2,
                                  O_exists          IN OUT  BOOLEAN,
                                  I_non_merch_code  IN      NON_MERCH_CODE_HEAD.NON_MERCH_CODE%TYPE)
                                  
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--   Name:    INVC_NON_MERCH_EXIST
--   Purpose  This function will execute a query with the input parameters in the 'where' 
--            clause to determine if the input non-merchandise
--            code, invc_id combination exists on the INVC_NON_MERCH table. The function will output
--	      a boolean indicator O_exists with a value= FALSE if the input non-merchandise code, 
--	      invoice_id combination does not exist on the invc_non_merch table or a TRUE if it does.  
---------------------------------------------------------------------------------------------
FUNCTION     INVC_NON_MERCH_EXIST(O_error_message   IN OUT  VARCHAR2,
                                  O_exists          IN OUT  BOOLEAN,
                                  I_non_merch_code  IN      NON_MERCH_CODE_HEAD.NON_MERCH_CODE%TYPE,
                                  I_invc_id         IN      INVC_NON_MERCH.INVC_ID%TYPE)
                                  
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--   Name:    CHECK_MATCH_TO_QTY
--   Purpose  This function will ensure that every receipt item on the invoice has been completely
--            matched to that invoice. Receipt items cannot be matched to more than
--            one invoice.
--            Created 08-Feb-2000   Randip Medhi
--------------------------------------------------------------------------------------------------
FUNCTION     CHECK_MATCH_TO_QTY(O_error_message	 IN OUT  VARCHAR2,
                                O_valid          IN OUT  BOOLEAN,
                                O_shipment       IN OUT  INVC_MATCH_WKSHT.SHIPMENT%TYPE,
                                O_carton         IN OUT  INVC_MATCH_WKSHT.CARTON%TYPE,
                                O_item           IN OUT  INVC_MATCH_WKSHT.ITEM%TYPE,
                                I_invc_id        IN      INVC_HEAD.INVC_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--   Name:    MATCH_TO_COST_QTY
--   Purpose  This function will validate a match to cost and, if passed in, match to qty for a 
--            shipment/carton item being matched to an invoice line.
--            Created 08-Feb-2000   Randip Medhi
-----------------------------------------------------------------------------------------------------
FUNCTION  MATCH_TO_COST_QTY(O_error_message  IN OUT  VARCHAR2,
                            O_valid          IN OUT  BOOLEAN,
                            I_invc_id        IN      INVC_HEAD.INVC_ID%TYPE,
                            I_shipment       IN      INVC_MATCH_WKSHT.SHIPMENT%TYPE,
                            I_carton         IN      INVC_MATCH_WKSHT.CARTON%TYPE,
                            I_item           IN      INVC_MATCH_WKSHT.ITEM%TYPE,
                            I_match_to_cost  IN      INVC_MATCH_WKSHT.MATCH_TO_COST%TYPE,
                            I_currency_invc  IN      INVC_HEAD.CURRENCY_CODE%TYPE,
                            I_exchange_rate  IN      INVC_HEAD.EXCHANGE_RATE%TYPE,
                            I_match_to_qty   IN      INVC_MATCH_WKSHT.MATCH_TO_QTY%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--   Name:     DETAILS_EXIST 
--   Purpose:  This function will determine if records exist on invc_detail for the passed in  
--             invc_id and can also search for detail records based on the item's associated 
--             ref_item, item_parent, diff_1 or diff_2.
---------------------------------------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_invc_id       IN     INVC_DETAIL.INVC_ID%TYPE,
                       I_item          IN     INVC_DETAIL.ITEM%TYPE,
                       I_ref_item      IN     INVC_DETAIL.REF_ITEM%TYPE,
                       I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_diff_1        IN     ITEM_MASTER.DIFF_1%TYPE,
                       I_diff_2        IN     ITEM_MASTER.DIFF_2%TYPE,
                       I_diff_3        IN     ITEM_MASTER.DIFF_3%TYPE,
                       I_diff_4        IN     ITEM_MASTER.DIFF_4%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END INVC_VALIDATE_SQL;
/
