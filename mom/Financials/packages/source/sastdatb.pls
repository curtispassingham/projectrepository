
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY STORE_DATA_SQL AS
----------------------------------------------------------------------
FUNCTION STORE_DATA_EXISTS(O_error_message  IN OUT  VARCHAR2,
		           O_exists         IN OUT  BOOLEAN,
                           I_store          IN      SA_STORE_DATA.STORE%TYPE,
                           I_system_code    IN      SA_STORE_DATA.SYSTEM_CODE %TYPE,
                           I_imp_exp        IN      SA_STORE_DATA.IMP_EXP%TYPE)
   RETURN BOOLEAN IS
   ---
   L_exists   VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'STORE_DATA_SQL.STORE_DATA_EXISTS';

   cursor C_EXIST is
      select 'Y'
        from sa_store_data
       where store       = I_store
         and system_code = I_system_code
         and imp_exp     = I_imp_exp;
   
BEGIN
   if I_store is NULL or I_system_code is NULL or I_imp_exp is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   else          
      SQL_LIB.SET_MARK('OPEN','C_EXIST','SA_STORE_DATA',NULL);
      open C_EXIST;
      SQL_LIB.SET_MARK('FETCH','C_EXIST','SA_STORE_DATA',NULL);
      fetch C_EXIST into L_exists; 
      SQL_LIB.SET_MARK('CLOSE','C_EXIST','SA_STORE_DATA',NULL);
      close C_EXIST;
      ---
      if L_exists = 'Y' then
	 O_exists := TRUE;         
      else
	 O_exists := FALSE;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END STORE_DATA_EXISTS;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_FUEL_STORE(O_error_message  IN OUT  VARCHAR2,
		          O_fuel_store_ind IN OUT  VARCHAR2,
                          I_store          IN      STORE.STORE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program  VARCHAR2(64) := 'STORE_DATA_SQL.CHECK_FUEL_STORE';

   cursor C_FUEL_STORE is
      select 'Y'
        from sa_store_data
       where store       = I_store
         and system_code = 'SFM'
         and imp_exp     = 'E';
   
BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      O_fuel_store_ind := 'N';
      ---
      SQL_LIB.SET_MARK('OPEN','C_FUEL_STORE','SA_STORE_DATA',NULL);
      open C_FUEL_STORE;
      SQL_LIB.SET_MARK('FETCH','C_FUEL_STORE','SA_STORE_DATA',NULL);
      fetch C_FUEL_STORE into O_fuel_store_ind; 
      SQL_LIB.SET_MARK('CLOSE','C_FUEL_STORE','SA_STORE_DATA',NULL);
      close C_FUEL_STORE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_FUEL_STORE;
-------------------------------------------------------------------------------------
FUNCTION GET_TRAN_NO_GENERATED(O_error_message     IN OUT VARCHAR2,
                               O_tran_no_generated IN OUT STORE.TRAN_NO_GENERATED%TYPE,
                               I_store             IN     STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'STORE_DATA_SQL.GET_TRAN_NO_GENERATED';
   ---
   cursor C_GET_TRAN_NO_GENERATED is
      select tran_no_generated
        from store
       where store = I_store;
 
BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_TRAN_NO_GENERATED','STORE',NULL);
      open C_GET_TRAN_NO_GENERATED;
      SQL_LIB.SET_MARK('FETCH','C_GET_TRAN_NO_GENERATED','STORE',NULL);
      fetch C_GET_TRAN_NO_GENERATED into O_tran_no_generated; 
      SQL_LIB.SET_MARK('CLOSE','C_GET_TRAN_NO_GENERATED','STORE',NULL);
      close C_GET_TRAN_NO_GENERATED;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_TRAN_NO_GENERATED;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_MLA_STORE(O_error_message  IN OUT  VARCHAR2,
		         O_mla_store_ind  IN OUT  VARCHAR2,
                         I_store          IN      STORE.STORE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program  VARCHAR2(64) := 'STORE_DATA_SQL.CHECK_MLA_STORE';
   ---
   cursor C_MLA_STORE is
      select 'Y'
        from sa_store_data
       where store       = I_store
         and system_code = 'MLA';   
BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      O_mla_store_ind := 'N';
      ---
      SQL_LIB.SET_MARK('OPEN','C_MLA_STORE','SA_STORE_DATA',NULL);
      open C_MLA_STORE;
      SQL_LIB.SET_MARK('FETCH','C_MLA_STORE','SA_STORE_DATA',NULL);
      fetch C_MLA_STORE into O_mla_store_ind; 
      SQL_LIB.SET_MARK('CLOSE','C_MLA_STORE','SA_STORE_DATA',NULL);
      close C_MLA_STORE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_MLA_STORE;
-------------------------------------------------------------------------------------
END STORE_DATA_SQL;
/
