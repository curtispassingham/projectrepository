CREATE OR REPLACE PACKAGE BODY ACC_VALIDATE_API AS
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ACC(O_error_message    OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                      O_acc_val_rec   IN OUT    ACC_VALIDATE_API.ACC_VALIDATE_TBL)
RETURN BOOLEAN IS
   L_program                VARCHAR2(60) := 'ACC_VALIDATE_API.VALIDATE_ACC';

BEGIN
   FOR i in 1..O_acc_val_rec.COUNT LOOP
      if O_acc_val_rec(i).set_of_books_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','Set_of_books_id',L_program,NULL);
         return FALSE;
      end if;
      ---web service account validation logic
      if AIA_WEBSERVICE_SQL.VALIDATE_ACCOUNT(O_error_message,
                                             O_acc_val_rec(i).requesting_system,
                                             O_acc_val_rec(i).set_of_books_id,
                                             O_acc_val_rec(i).ccid,
                                             O_acc_val_rec(i).segment_1,
                                             O_acc_val_rec(i).segment_2,
                                             O_acc_val_rec(i).segment_3,
                                             O_acc_val_rec(i).segment_4,
                                             O_acc_val_rec(i).segment_5,
                                             O_acc_val_rec(i).segment_6,
                                             O_acc_val_rec(i).segment_7,
                                             O_acc_val_rec(i).segment_8,
                                             O_acc_val_rec(i).segment_9,
                                             O_acc_val_rec(i).segment_10,
                                             O_acc_val_rec(i).segment_11,
                                             O_acc_val_rec(i).segment_12,
                                             O_acc_val_rec(i).segment_13,
                                             O_acc_val_rec(i).segment_14,
                                             O_acc_val_rec(i).segment_15,
                                             O_acc_val_rec(i).segment_16,
                                             O_acc_val_rec(i).segment_17,
                                             O_acc_val_rec(i).segment_18,
                                             O_acc_val_rec(i).segment_19,
                                             O_acc_val_rec(i).segment_20,
                                             O_acc_val_rec(i).account_status,
                                             O_acc_val_rec(i).requesting_system,
                                             O_acc_val_rec(i).set_of_books_id,
                                             O_acc_val_rec(i).ccid,
                                             O_acc_val_rec(i).segment_1,
                                             O_acc_val_rec(i).segment_2,
                                             O_acc_val_rec(i).segment_3,
                                             O_acc_val_rec(i).segment_4,
                                             O_acc_val_rec(i).segment_5,
                                             O_acc_val_rec(i).segment_6,
                                             O_acc_val_rec(i).segment_7,
                                             O_acc_val_rec(i).segment_8,
                                             O_acc_val_rec(i).segment_9,
                                             O_acc_val_rec(i).segment_10,
                                             O_acc_val_rec(i).segment_11,
                                             O_acc_val_rec(i).segment_12,
                                             O_acc_val_rec(i).segment_13,
                                             O_acc_val_rec(i).segment_14,
                                             O_acc_val_rec(i).segment_15,
                                             O_acc_val_rec(i).segment_16,
                                             O_acc_val_rec(i).segment_17,
                                             O_acc_val_rec(i).segment_18,
                                             O_acc_val_rec(i).segment_19,
                                             O_acc_val_rec(i).segment_20) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END VALIDATE_ACC;
-------------------------------------------------------------------------------------------------
END  ACC_VALIDATE_API;

/
