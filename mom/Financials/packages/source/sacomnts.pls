
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_COMMENTS_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
--- Function Name: GET_NEW_COMMENT_SEQ_NO
--- Purpose:       This function will retrieve the next sequence number to be used 
---                for the comment sequence number in the sa_comments table.
---
FUNCTION GET_NEW_COMMENT_SEQ_NO(O_error_message    IN OUT  VARCHAR2,
                                O_comment_seq_no   IN OUT  SA_COMMENTS.COMMENT_SEQ_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Function Name: KEY_DECODE
--- Purpose:       This function will decode the comment key and return a string 
---                value that describes the value in the comment key based on the
---                type of comment. Currently, the function is set to de-code the 
---                following types: Store/Day with store_day_seq_no, Transaction Number
---                with tran_seq_no, Total with total_seq_no, and Balance Group with 
---                bal_group_seq_no. If the comment type is other than one of those four
---                then the de-coded value will equal the passed in value.
---
FUNCTION KEY_DECODE(O_error_message     IN OUT  VARCHAR2,
                    O_comment_key_desc  IN OUT  VARCHAR2,
                    I_comment_type      IN      SA_COMMENTS.COMMENT_TYPE%TYPE,
                    I_comment_key       IN      SA_COMMENTS.COMMENT_KEY%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END SA_COMMENTS_SQL;
/