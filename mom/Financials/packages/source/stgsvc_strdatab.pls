create or replace PACKAGE BODY STG_SVC_SA_STORE_DATA
AS
   Type s9t_errors_tab_typ IS TABLE OF s9t_errors%rowtype;
   Lp_s9t_errors_tab s9t_errors_tab_typ;
   FUNCTION populate_lists(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_file_id       IN NUMBER)RETURN BOOLEAN;
      ------------------------------------------
   PROCEDURE write_s9t_error(
      I_file_id IN s9t_errors.file_id%type,
      I_sheet   IN VARCHAR2,
      I_row_seq IN NUMBER,
      I_col     IN VARCHAR2,
      I_sqlcode IN NUMBER,
      I_sqlerrm IN VARCHAR2)
   IS
   BEGIN
    Lp_s9t_errors_tab.extend();
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).FILE_ID              := I_file_id;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_SEQ_NO         := s9t_errors_seq.nextval;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).TEMPLATE_KEY         := STG_SVC_SA_STORE_DATA.template_key;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).WKSHT_KEY            := I_sheet;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).COLUMN_KEY           := I_col;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ROW_SEQ              := I_row_seq;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_ID            := GET_USER;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_DATETIME      := sysdate;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_ID       := GET_USER;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_DATETIME := sysdate;
   end write_s9t_error;
------------------------------------------------
   PROCEDURE populate_names
    (
      I_file_id NUMBER
    )
   IS
    l_sheets s9t_pkg.names_map_typ;
    SA_STORE_DATA_cols s9t_pkg.names_map_typ;
   BEGIN
    l_sheets               :=s9t_pkg.get_sheet_names(I_file_id);
    SA_STORE_DATA_cols              :=s9t_pkg.get_col_names(I_file_id,SA_STORE_DATA_sheet);
    SA_STORE_DATA$Action            := SA_STORE_DATA_cols('ACTION');
    SA_STORE_DATA$IMP_EXP              := SA_STORE_DATA_cols('IMP_EXP');
    SA_STORE_DATA$SYSTEM_CODE              := SA_STORE_DATA_cols('SYSTEM_CODE');
    SA_STORE_DATA$STORE              := SA_STORE_DATA_cols('STORE');
    
   end populate_names;
   -------------------------------------------------------------------------------
---  Name: populate_SA_STORE_DATA
--- Purpose: load data from sa_store_data
-------------------------------------------------------------------------------- 

   PROCEDURE populate_SA_STORE_DATA
    (
      I_file_id IN NUMBER
    )
   IS
   BEGIN
      INSERT
      INTO TABLE
      (select ss.s9t_rows
        FROM s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id  = I_file_id
        and ss.sheet_name = SA_STORE_DATA_sheet
      )
      select s9t_row(s9t_cells( NULL ,STORE,SYSTEM_CODE
                              ,IMP_EXP))
      FROM SA_STORE_DATA ;
   end populate_SA_STORE_DATA;
 PROCEDURE verify_download_data( I_file    IN OUT s9t_file)
  is
 
     cursor c_get_code_desc(p_system_desc code_detail.code_desc%type,p_imp_exp_desc code_detail.code_desc%type)
     is
      select code_desc 
        from v_code_detail_tl
       where code=(select code 
                     from v_code_detail_tl 
                    where code_desc=p_system_desc 
                      and code_type in ('SYSE','SYSI'))
         and lang=I_file.user_lang
         and code_type = ( select decode(code,'E','SYSE','SYSI') 
                             from v_code_detail_tl
                            where code_desc=p_imp_exp_desc 
                              and code_type ='IMEX');
 begin
   FOR ri IN 1..I_file.sheets(1).s9t_rows.count()
   LOOP
    
      --check if they match and get the correct value if not
      SQL_LIB.SET_MARK('OPEN','C_GET_CODE_DESC','V_CODE_DETAIL_TL',NULL);
      open c_get_code_desc (I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$SYSTEM_CODE),I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$IMP_EXP));
      SQL_LIB.SET_MARK('FETCH','C_GET_CODE_DESC','V_CODE_DETAIL_TL',NULL);
      fetch c_get_code_desc into I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$SYSTEM_CODE);
      SQL_LIB.SET_MARK('CLOSE','C_GET_CODE_DESC','V_CODE_DETAIL_TL',NULL);
      close c_get_code_desc;
      
    END LOOP;
  
  
  end verify_download_data;
  --------------------------------------------------------------------------
  ---Name : chk_i_e_system
  --Purpose: To check if the export/Import matches with system_code
  ---------------------------------------------------------------------------
  PROCEDURE chk_i_e_system( I_file    IN OUT s9t_file)
  is
  
     cursor c_get_code_desc(p_system_desc code_detail.code_desc%type,
                            p_imp_exp_desc code_detail.code_desc%type,
                            p_action code_detail.code_desc%type) is
     select code_desc ,code_type
        from v_code_detail_tl
       where code=(select code 
                     from v_code_detail_tl 
                    where code_desc=p_system_desc 
                      and code_type in ('SYSE','SYSI') 
                      and code in ('UAR','SFM'))
         and code_type = ( select decode(code,'E','SYSE','SYSI') 
                             from v_code_detail_tl
                            where code_desc = p_imp_exp_desc 
                              and code_type ='IMEX')
      and  exists ( select 1 
                      from v_code_detail_tl c,
                           s9t_list_vals s
                     where s.template_category='RSASD'
                     and s.sheet_name='SA_STORE_DATA'
                     and s.column_name='ACTION'
                     and s.code=c.code_type
                     and c.code_desc=p_action
                     and c.code='NEW');
      
       cursor C_TYPE_DESC(p_code_type code_head.code_type_desc%type) is
      select code_type_desc
        from code_head
       where code_type = p_code_type;
        L_error_msg VARCHAR2(255);
        l_code_type code_head.code_type%type;
        l_code_type_desc code_head.code_type_desc%type;
        l_code_desc code_detail.code_desc%type;
  BEGIN  
    FOR ri IN 1..I_file.sheets(1).s9t_rows.count()
    LOOP
      if I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$SYSTEM_CODE) is not null and I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$IMP_EXP)is not null   then
   
         l_code_type :=null;
         l_code_type_desc  :=null;
         l_code_desc  :=null;
         --check if they match 
         SQL_LIB.SET_MARK('OPEN','C_GET_CODE_DESC','V_CODE_DETAIL_TL',NULL);
         open c_get_code_desc (I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$SYSTEM_CODE),I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$IMP_EXP),
                                                                                              I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$ACTION));
         SQL_LIB.SET_MARK('FETCH','C_GET_CODE_DESC','V_CODE_DETAIL_TL',NULL);
         fetch c_get_code_desc into l_code_desc,l_code_type;
         SQL_LIB.SET_MARK('CLOSE','C_GET_CODE_DESC','V_CODE_DETAIL_TL',NULL);
         close c_get_code_desc;
       
        if l_code_desc is not null then
          if l_code_desc <> I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$SYSTEM_CODE) then
            --fetch the code_type desc
            sql_lib.set_mark('OPEN','C_type_desc','CODE_HEAD','CODE_TYPE : '||l_code_type);
            open C_TYPE_DESC(l_code_type);
            sql_lib.set_mark('FETCH','C_type_desc','CODE_HEAD','CODE_TYPE : '||l_code_type);
            fetch C_TYPE_DESC into L_code_type_desc;
            sql_lib.set_mark('CLOSE','C_type_desc','CODE_HEAD','CODE_TYPE : '||l_code_type);
            close C_TYPE_DESC;
            l_error_msg:=sql_lib.create_msg('MISSING_CODE', I_file.sheets(1).s9t_rows(ri).cells(SA_STORE_DATA$SYSTEM_CODE), L_code_type_desc , l_code_type);
            write_s9t_error(I_file.file_id,'SA_STORE_DATA',ri+1,'SYSTEM_CODE',null,l_error_msg);
          end if;
        end if;
      end if;
    END LOOP;
  
  end chk_i_e_system;
  
   -------------------------------------------------------------------------------
---  Name: init_s9t
-------------------------------------------------------------------------------- 

   PROCEDURE init_s9t(
      O_file_id IN OUT NUMBER)
   IS
      l_file s9t_file;
      l_file_name s9t_folder.file_name%type;
      
   BEGIN
      l_file              := NEW s9t_file();
      O_file_id           := s9t_folder_seq.nextval;
      l_file.file_id      := O_file_id;
      l_file_name         := STG_SVC_SA_STORE_DATA.template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
      l_file.file_name    := l_file_name;
      l_file.template_key := STG_SVC_SA_STORE_DATA.template_key;
      l_file.user_lang    := GET_USER_LANG;
      l_file.add_sheet(SA_STORE_DATA_sheet);
      l_file.sheets(l_file.get_sheet_index(SA_STORE_DATA_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'STORE'
                                                                                             ,'SYSTEM_CODE'
                                                                                              ,'IMP_EXP'
                                                                                              );

      s9t_pkg.save_obj(l_file);
   end init_s9t;
-------------------------------------------------------------------------------
---  Name: create_s9t
--- Purpose: called from UI to generated the excel download
-------------------------------------------------------------------------------- 

   FUNCTION create_s9t(
      O_error_message     IN OUT rtk_errors.rtk_text%type,
      O_file_id           IN OUT s9t_folder.file_id%type,
      I_template_only_ind IN CHAR DEFAULT 'N')
    RETURN BOOLEAN
   IS
      l_file s9t_file;
      L_program VARCHAR2(255):='STG_SVC_SA_STORE_DATA.create_s9t';
   BEGIN
      init_s9t(O_file_id);
      --populate the column lists
      if populate_lists(O_error_message,O_file_id)=false then
         RETURN false;
      end if;
      if I_template_only_ind = 'N' then
      ---populate the data
         populate_SA_STORE_DATA(O_file_id);
         commit;
      end if;

      s9t_pkg.translate_to_user_lang(O_file_id);
      -- Apply template
      s9t_pkg.apply_template(O_file_id,STG_SVC_SA_STORE_DATA.template_key);
      l_file:=s9t_file(O_file_id);
      if s9t_pkg.code2desc(O_error_message,
                         'RSASD',
                         l_file)=FALSE then
         return FALSE;
      end if;
    if I_template_only_ind = 'N' then
       verify_download_data(l_file);
    end if;
      s9t_pkg.save_obj(l_file);
      s9t_pkg.update_ods(l_file);
      commit;
      RETURN true;
   EXCEPTION
   WHEN OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
    RETURN FALSE;
   end create_s9t;
   -------------------------------------------------------------------------------
---  Name: process_s9t_SA_STORE_DATA
--- Purpose: Load data to excel to staging
-------------------------------------------------------------------------------- 
   PROCEDURE process_s9t_SA_STORE_DATA
    (
      I_file_id    IN s9t_folder.file_id%type,
      I_process_id IN SVC_SA_STORE_DATA.process_id%type
    )
   IS
      Type svc_SA_STORE_DATA_col_typ
      IS
       TABLE OF SVC_SA_STORE_DATA%rowtype;
       l_temp_rec SVC_SA_STORE_DATA%rowtype;
       svc_SA_STORE_DATA_col svc_SA_STORE_DATA_col_typ :=NEW svc_SA_STORE_DATA_col_typ();
       l_process_id SVC_SA_STORE_DATA.process_id%type;
       l_error BOOLEAN:=false;
       l_default_rec SVC_SA_STORE_DATA%rowtype;
       CURSOR c_mandatory_ind
       IS
         select
           IMP_EXP_mi,
           SYSTEM_CODE_mi,
           STORE_mi,
           1 as dummy
         FROM
           (select column_key,
             mandatory
           FROM s9t_tmpl_cols_def
           where template_key                              = STG_SVC_SA_STORE_DATA.template_key
           and wksht_key                                   = 'SA_STORE_DATA'
           ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('STORE' AS STORE,
           'SYSTEM_CODE' AS SYSTEM_CODE,
                             'IMP_EXP' AS IMP_EXP, null as dummy));
      l_mi_rec c_mandatory_ind%rowtype;
      dml_errors EXCEPTION;
      L_pk_columns   VARCHAR2(255) := 'store,system_code,imp_exp';
      L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
      L_error_code   NUMBER;
    L_table        VARCHAR2(30)  := 'SVC_SA_STORE_DATA';
      PRAGMA exception_init(dml_errors, -24381);
   BEGIN
    -- Get default values.
    FOR rec IN
    (select
           IMP_EXP_dv,
           SYSTEM_CODE_dv,
           STORE_dv,
      null as dummy
    FROM
      (select column_key,
        default_value
      FROM s9t_tmpl_cols_def
      where template_key                                  = STG_SVC_SA_STORE_DATA.template_key
      and wksht_key                                       = 'SA_STORE_DATA'
      ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('STORE' AS STORE,
      'SYSTEM_CODE' AS SYSTEM_CODE,
            'IMP_EXP' AS IMP_EXP,
            
            
   NULL      
   AS        
   dummy))
   )

   LOOP  


   BEGIN  

   l_default_rec.IMP_EXP := rec.IMP_EXP_dv;  


   EXCEPTION  


   WHEN OTHERS then  

   write_s9t_error(I_file_id,'SA_STORE_DATA',NULL,'IMP_EXP','INV_DEFAULT',SQLERRM);  


   end;


   BEGIN  

   l_default_rec.SYSTEM_CODE := rec.SYSTEM_CODE_dv;  


   EXCEPTION  


   WHEN OTHERS then  

   write_s9t_error(I_file_id,'SA_STORE_DATA',NULL,'SYSTEM_CODE','INV_DEFAULT',SQLERRM);  


   end;


   BEGIN  

   l_default_rec.STORE := rec.STORE_dv;  


   EXCEPTION  


   WHEN OTHERS then  

   write_s9t_error(I_file_id,'SA_STORE_DATA',NULL,'STORE','INV_DEFAULT',SQLERRM);  


   end;



   end LOOP;

   --Get mandatory indicators
   OPEN C_mandatory_ind;
   FETCH C_mandatory_ind
   INTO l_mi_rec;
   CLOSE C_mandatory_ind;
   FOR rec IN
      (select r.get_cell(SA_STORE_DATA$Action)      AS Action,
      upper(r.get_cell(SA_STORE_DATA$IMP_EXP))              AS IMP_EXP,
      upper(r.get_cell(SA_STORE_DATA$SYSTEM_CODE))              AS SYSTEM_CODE,
      r.get_cell(SA_STORE_DATA$STORE)              AS STORE,
      r.get_row_seq()                             AS row_seq
      FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) ss,
      TABLE(ss.s9t_rows) r
      where sf.file_id  = I_file_id
      and ss.sheet_name = sheet_name_trans(SA_STORE_DATA_sheet)
      )
   LOOP
      l_temp_rec.process_id        := I_process_id;
      l_temp_rec.chunk_id          := 1;
      l_temp_rec.row_seq           := rec.row_seq;
      l_temp_rec.process$status    := 'N';
      l_temp_rec.create_id         := GET_USER;
      l_temp_rec.last_upd_id       := GET_USER;
      l_temp_rec.create_datetime   := sysdate;
      l_temp_rec.last_upd_datetime := sysdate;
      l_error := false;
      BEGIN
         l_temp_rec.Action := rec.Action;
      EXCEPTION
         WHEN OTHERS then
         write_s9t_error(I_file_id,SA_STORE_DATA_sheet,rec.row_seq,'ACTION',SQLCODE,SQLERRM);
      l_error := true;
      end;
      BEGIN
         l_temp_rec.IMP_EXP := rec.IMP_EXP;
      EXCEPTION
         WHEN OTHERS then
         write_s9t_error(I_file_id,SA_STORE_DATA_sheet,rec.row_seq,'IMP_EXP',SQLCODE,SQLERRM);
         l_error := true;
      end;
      BEGIN
         l_temp_rec.SYSTEM_CODE := rec.SYSTEM_CODE;
      EXCEPTION
         WHEN OTHERS then
         write_s9t_error(I_file_id,SA_STORE_DATA_sheet,rec.row_seq,'SYSTEM_CODE',SQLCODE,SQLERRM);
         l_error := true;
      end;
      BEGIN
         l_temp_rec.STORE := rec.STORE;
      EXCEPTION
         WHEN OTHERS then
         write_s9t_error(I_file_id,SA_STORE_DATA_sheet,rec.row_seq,'STORE',SQLCODE,SQLERRM);
         l_error := true;
      end;
      if rec.action = STG_SVC_SA_STORE_DATA.action_new
      then
         l_temp_rec.IMP_EXP := NVL( l_temp_rec.IMP_EXP,l_default_rec.IMP_EXP);
         l_temp_rec.SYSTEM_CODE := NVL( l_temp_rec.SYSTEM_CODE,l_default_rec.SYSTEM_CODE);
         l_temp_rec.STORE := NVL( l_temp_rec.STORE,l_default_rec.STORE);

      end if;
      if not (
           l_temp_rec.IMP_EXP is not null and
           l_temp_rec.SYSTEM_CODE is not null and
           l_temp_rec.STORE is not null and
           1 = 1
           )
      then
         write_s9t_error(I_file_id,SA_STORE_DATA_sheet,rec.row_seq,NULL,NULL,SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns));
         l_error := true;
      end if;
      if NOT l_error then
         svc_SA_STORE_DATA_col.extend();
         svc_SA_STORE_DATA_col(svc_SA_STORE_DATA_col.count()):=l_temp_rec;
      end if;
   end LOOP;
   BEGIN
    forall i IN 1..svc_SA_STORE_DATA_col.count SAVE EXCEPTIONS Merge INTO SVC_SA_STORE_DATA st USING
    (select
      (CASE
        WHEN l_mi_rec.IMP_EXP_mi    = 'N'
        and svc_SA_STORE_DATA_col(i).action = STG_SVC_SA_STORE_DATA.action_mod
        and s1.IMP_EXP             IS NULL
        then mt.IMP_EXP
        else s1.IMP_EXP
      end) AS IMP_EXP,
      (CASE
        WHEN l_mi_rec.SYSTEM_CODE_mi    = 'N'
        and svc_SA_STORE_DATA_col(i).action = STG_SVC_SA_STORE_DATA.action_mod
        and s1.SYSTEM_CODE             IS NULL
        then mt.SYSTEM_CODE
        else s1.SYSTEM_CODE
      end) AS SYSTEM_CODE,
      (CASE
        WHEN l_mi_rec.STORE_mi    = 'N'
        and svc_SA_STORE_DATA_col(i).action = STG_SVC_SA_STORE_DATA.action_mod
        and s1.STORE             IS NULL
        then mt.STORE
        else s1.STORE
      end) AS STORE,
      null as dummy
    FROM (select
       svc_SA_STORE_DATA_col(i).IMP_EXP AS IMP_EXP,
       svc_SA_STORE_DATA_col(i).SYSTEM_CODE AS SYSTEM_CODE,
       svc_SA_STORE_DATA_col(i).STORE AS STORE,
       null as dummy
      FROM dual
      ) s1,
      SA_STORE_DATA mt
    where
    mt.IMP_EXP (+)     = s1.IMP_EXP   and
    mt.SYSTEM_CODE (+)     = s1.SYSTEM_CODE   and
    mt.STORE (+)     = s1.STORE   and
   1 = 1
    ) sq ON (
     st.IMP_EXP      = sq.IMP_EXP and
     st.SYSTEM_CODE      = sq.SYSTEM_CODE and
     st.STORE      = sq.STORE and
    svc_SA_STORE_DATA_col(i).ACTION IN ( STG_SVC_SA_STORE_DATA.action_mod,STG_SVC_SA_STORE_DATA.action_del)
    )
   WHEN matched then
    UPDATE
    SET PROCESS_ID      = svc_SA_STORE_DATA_col(i).PROCESS_ID ,
      CHUNK_ID          = svc_SA_STORE_DATA_col(i).CHUNK_ID ,
      ROW_SEQ           = svc_SA_STORE_DATA_col(i).ROW_SEQ ,
      ACTION            = svc_SA_STORE_DATA_col(i).ACTION ,
      PROCESS$STATUS    = svc_SA_STORE_DATA_col(i).PROCESS$STATUS ,
      CREATE_ID         = svc_SA_STORE_DATA_col(i).CREATE_ID ,
      CREATE_DATETIME   = svc_SA_STORE_DATA_col(i).CREATE_DATETIME ,
      LAST_UPD_ID       = svc_SA_STORE_DATA_col(i).LAST_UPD_ID ,
      LAST_UPD_DATETIME = svc_SA_STORE_DATA_col(i).LAST_UPD_DATETIME WHEN NOT matched then
    INSERT
      (
        PROCESS_ID ,
        CHUNK_ID ,
        ROW_SEQ ,
        ACTION ,
        PROCESS$STATUS ,
        IMP_EXP ,
        SYSTEM_CODE ,
        STORE ,
        CREATE_ID ,
        CREATE_DATETIME ,
        LAST_UPD_ID ,
        LAST_UPD_DATETIME
      )
      VALUES
      (
        svc_SA_STORE_DATA_col(i).PROCESS_ID ,
        svc_SA_STORE_DATA_col(i).CHUNK_ID ,
      svc_SA_STORE_DATA_col(i).ROW_SEQ ,
        svc_SA_STORE_DATA_col(i).ACTION ,
        svc_SA_STORE_DATA_col(i).PROCESS$STATUS ,
        sq.IMP_EXP ,
        sq.SYSTEM_CODE ,
        sq.STORE ,
        svc_SA_STORE_DATA_col(i).CREATE_ID ,
        svc_SA_STORE_DATA_col(i).CREATE_DATETIME ,
        svc_SA_STORE_DATA_col(i).LAST_UPD_ID ,
        svc_SA_STORE_DATA_col(i).LAST_UPD_DATETIME
      );
    EXCEPTION
    WHEN DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.count
      LOOP
         L_error_code := sql%bulk_exceptions(i).error_code;
         if L_error_code = 1 then
             L_error_code := NULL;
             L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns,l_table);
        
         end if;
       write_s9t_error
        (
          I_file_id,SA_STORE_DATA_sheet,svc_SA_STORE_DATA_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
        )
        ;
      end LOOP;
    end;
   end process_s9t_SA_STORE_DATA;
   -------------------------------------------------------------------------------
---  Name: process_s9t
--- Purpose: called from UI to upload data to staging
-------------------------------------------------------------------------------- 
   FUNCTION process_s9t
    (
      O_error_message IN OUT rtk_errors.rtk_text%type ,
      I_file_id       IN s9t_folder.file_id%type,
      I_process_id IN Number,
      O_error_count OUT NUMBER
    )
    RETURN BOOLEAN
   IS
      l_file             s9t_file;
      l_sheets           s9t_pkg.names_map_typ;
      L_program          VARCHAR2(255):='STG_SVC_SA_STORE_DATA.process_s9t';
      l_process_status   svc_process_tracker.status%type;
      l_file_name        s9t_folder.file_name%type;
                MAX_CHAR      EXCEPTION;
                PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
      
   BEGIN
      commit;--to ensure that the record in s9t_folder is commited
      s9t_pkg.ods2obj(I_file_id);
      commit;
      L_file := s9t_pkg.get_obj(I_file_id);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
    /* call chk_i_e_system to verify if the system and I/E indicator match
   This is needed as some system_codes like  UAR,SFM are associated in both SYSI and SYSE.Hence validation in core layer is not possible 
    once they have been transalted to theire original codes*/
      chk_i_e_system(l_file);
      if s9t_pkg.code2desc(O_error_message,
                         'RSASD',
                         l_file,
                         true)=FALSE then
         return FALSE;
      end if;
      s9t_pkg.save_obj(l_file);
       if s9t_pkg.validate_template(I_file_id) = false THEN
         write_s9t_error(I_file_id,NULL,NULL,null,NULL,'S9T_INVALID_TEMPLATE');
      else
      populate_names(I_file_id);
      sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key,l_file.user_lang);
      process_s9t_SA_STORE_DATA(I_file_id,I_process_id);
      end if;
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         INSERT INTO s9t_errors VALUES Lp_s9t_errors_tab
           (i
           );
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      if O_error_count    = 0 then
      l_process_status := 'PS';
      else
      l_process_status := 'PE';
      end if;
      UPDATE svc_process_tracker
      SET status       = l_process_status,
      file_id        = I_file_id
      where process_id = I_process_id;
      commit;
      RETURN true;
   EXCEPTION
           when MAX_CHAR then
              ROLLBACK;
              O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
              Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
              write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
              O_error_count := Lp_s9t_errors_tab.count();
              forall i IN 1..O_error_count
            insert into s9t_errors
                 values Lp_s9t_errors_tab(i);

              update svc_process_tracker
            set status = 'PE',
                file_id  = I_file_id
               where process_id = I_process_id;
      
              COMMIT;
      
              return FALSE;
   
      WHEN OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
         RETURN FALSE;
   end process_s9t;
   FUNCTION populate_lists(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_file_id       IN NUMBER)RETURN BOOLEAN
   IS
       L_program VARCHAR2(75) := 'STG_SVC_SA_STORE_DATA.POPULATE_LISTS';
       l_s9t_action s9t_cells := NEW s9t_cells(stg_svc_sa_store_data.action_new,stg_svc_sa_store_data.action_mod,stg_svc_sa_store_data.action_del);
   BEGIN

      if S9T_PKG.populate_lists(O_error_message,
                                I_file_id,
                                'RSASD')=FALSE then
        return FALSE;
       end if;
    
      RETURN true;
   EXCEPTION
      WHEN OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
         RETURN FALSE;
   end populate_lists;
end STG_SVC_SA_STORE_DATA;
/ 