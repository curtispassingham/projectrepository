
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_VOUCHER_SQL AS
---------------------------------------------------------------------------------------
FUNCTION ISSUE_VOUCHER(O_ERROR_MESSAGE        IN OUT VARCHAR2,
                       I_VOUCHER_NO           IN     SA_TRAN_ITEM.VOUCHER_NO%TYPE,
                       I_VOUCHER_TYPE         IN     SA_VOUCHER.TENDER_TYPE_ID%TYPE,
                       I_ISS_DATE             IN     SA_VOUCHER.ISS_DATE%TYPE,
                       I_ISS_STORE            IN     SA_VOUCHER.ISS_STORE%TYPE,
                       I_ISS_REGISTER         IN     SA_VOUCHER.ISS_REGISTER%TYPE,
                       I_ISS_CASHIER          IN     SA_VOUCHER.ISS_CASHIER%TYPE,
                       I_ISS_TRAN_SEQ_NO      IN     SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE,
                       I_ISS_TENDER_SEQ_NO    IN     SA_VOUCHER.ISS_TENDER_SEQ_NO%TYPE,
                       I_ISS_ITEM_SEQ_NO      IN     SA_VOUCHER.ISS_ITEM_SEQ_NO%TYPE,
                       I_ISS_AMT              IN     SA_VOUCHER.ISS_AMT%TYPE,
                       I_ISS_CUST_NAME        IN     SA_VOUCHER.ISS_CUST_NAME%TYPE,
                       I_ISS_CUST_ADDR1       IN     SA_VOUCHER.ISS_CUST_ADDR1%TYPE,
                       I_ISS_CUST_ADDR2       IN     SA_VOUCHER.ISS_CUST_ADDR2%TYPE,
                       I_ISS_CUST_CITY        IN     SA_VOUCHER.ISS_CUST_CITY%TYPE,
                       I_ISS_CUST_STATE       IN     SA_VOUCHER.ISS_CUST_STATE%TYPE,
                       I_ISS_CUST_POSTAL_CODE IN     SA_VOUCHER.ISS_CUST_POSTAL_CODE%TYPE,
                       I_ISS_CUST_COUNTRY     IN     SA_VOUCHER.ISS_CUST_COUNTRY%TYPE,
                       I_RECIPIENT_NAME       IN     SA_VOUCHER.RECIPIENT_NAME%TYPE,
                       I_RECIPIENT_STATE      IN     SA_VOUCHER.RECIPIENT_STATE%TYPE,
                       I_RECIPIENT_COUNTRY    IN     SA_VOUCHER.RECIPIENT_COUNTRY%TYPE)
   RETURN BOOLEAN is

   L_program           VARCHAR2(60) := 'SA_VOUCHER_SQL.ISSUE_VOUCHER';
   L_vouch_seq_no      SA_VOUCHER.VOUCHER_SEQ_NO%TYPE;

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_voucher
       where voucher_no = I_voucher_no
         for update nowait;

BEGIN

   if I_voucher_no is NOT NULL and I_voucher_type is NOT NULL and I_ISS_TRAN_SEQ_NO is NOT NULL and
         (I_ISS_TENDER_SEQ_NO is NOT NULL or I_ISS_ITEM_SEQ_NO is NOT NULL) then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      update sa_voucher
         set iss_date          = nvl(I_iss_date, iss_date),
             iss_store         = nvl(I_iss_store, iss_store),
             iss_register      = nvl(I_iss_register, iss_register),
             iss_cashier       = nvl(I_iss_cashier, iss_cashier),
             iss_tran_seq_no   = nvl(I_iss_tran_seq_no, iss_tran_seq_no),
             iss_tender_seq_no = nvl(I_iss_tender_seq_no, iss_tender_seq_no),
             iss_item_seq_no   = nvl(I_iss_item_seq_no, iss_item_seq_no),
             iss_amt           = nvl(I_iss_amt, iss_amt),
             iss_cust_name     = nvl(I_iss_cust_name, iss_cust_name),
             iss_cust_addr1    = nvl(I_iss_cust_addr1, iss_cust_addr1),
             iss_cust_addr2    = nvl(I_iss_cust_addr2, iss_cust_addr2),
             iss_cust_city     = nvl(I_iss_cust_city, iss_cust_city),
             iss_cust_state    = nvl(I_iss_cust_state, iss_cust_state),
             iss_cust_postal_code = nvl(I_iss_cust_postal_code, iss_cust_postal_code),
             iss_cust_country  = nvl(I_iss_cust_country, iss_cust_country),
             recipient_name    = nvl(I_recipient_name, recipient_name),
             recipient_state   = nvl(I_recipient_state, recipient_state),
             recipient_country = nvl(I_recipient_country, recipient_country),
             status            = 'I'
       where voucher_no        = I_voucher_no
         and iss_tran_seq_no   = nvl(I_iss_tran_seq_no, iss_tran_seq_no)
         and iss_tender_seq_no = nvl(I_iss_tender_seq_no, iss_tender_seq_no)
         and iss_item_seq_no   = nvl(I_iss_item_seq_no, iss_item_seq_no);
       ---
       if SQL%NOTFOUND then
         if SA_SEQUENCE2_SQL.GET_VOUCHER_SEQ(O_error_message,
                                             L_vouch_seq_no) = FALSE then
            return FALSE;
         end if;
         ---
         insert into sa_voucher(voucher_seq_no,
                                voucher_no,
                                tender_type_id,
                                iss_date,
                                iss_store,
                                iss_register,
                                iss_cashier,
                                iss_tran_seq_no,
                                iss_tender_seq_no,
                                iss_item_seq_no,
                                iss_amt,
                                iss_cust_name,
                                iss_cust_addr1,
                                iss_cust_addr2,
                                iss_cust_city,
                                iss_cust_state,
                                iss_cust_postal_code,
                                iss_cust_country,
                                recipient_name,
                                recipient_state,
                                recipient_country,
                                status)
                         values(L_vouch_seq_no,
                                I_voucher_no,
                                I_voucher_type,
                                I_iss_date,
                                I_iss_store,
                                I_iss_register,
                                I_iss_cashier,
                                I_iss_tran_seq_no,
                                I_iss_tender_seq_no,
                                I_iss_item_seq_no,
                                I_iss_amt,
                                I_iss_cust_name,
                                I_iss_cust_addr1,
                                I_iss_cust_addr2,
                                I_iss_cust_city,
                                I_iss_cust_state,
                                I_iss_cust_postal_code,
                                I_iss_cust_country,
                                I_recipient_name,
                                I_recipient_state,
                                I_recipient_country,
                                'I');
       end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ISSUE_VOUCHER;
---------------------------------------------------------------------------------------
FUNCTION REDEEM_VOUCHER(O_ERROR_MESSAGE     IN OUT VARCHAR2,
                        I_VOUCHER_NO        IN     SA_TRAN_ITEM.VOUCHER_NO%TYPE,
                        I_VOUCHER_TYPE      IN     SA_VOUCHER.TENDER_TYPE_ID%TYPE,
                        I_RED_DATE          IN     SA_VOUCHER.RED_DATE%TYPE,
                        I_RED_STORE         IN     SA_VOUCHER.RED_STORE%TYPE,
                        I_RED_REGISTER      IN     SA_VOUCHER.RED_REGISTER%TYPE,
                        I_RED_CASHIER       IN     SA_VOUCHER.RED_CASHIER%TYPE,
                        I_RED_TRAN_SEQ_NO   IN     SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE,
                        I_RED_TENDER_NO     IN     SA_VOUCHER.RED_TENDER_SEQ_NO%TYPE,
                        I_RED_AMT           IN     SA_VOUCHER.RED_AMT%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'SA_VOUCHER_SQL.REDEEM_VOUCHER';
   L_vouch_seq_no      SA_VOUCHER.VOUCHER_SEQ_NO%TYPE;
   L_dummy             VARCHAR2(1);
  
   cursor C_LOCK_RECORD is
      select 'x'
        from sa_voucher
       where voucher_no = I_voucher_no
         for update nowait;

   cursor C_REDEEM is
      select voucher_seq_no 
         from sa_voucher
        where voucher_no = I_voucher_no
          and status != 'R';

BEGIN

   if I_voucher_no is NOT NULL and I_red_tran_seq_no is NOT NULL then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      if SA_SEQUENCE2_SQL.GET_VOUCHER_SEQ(O_error_message,
                                          L_vouch_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
      open C_REDEEM;
      fetch C_REDEEM into L_vouch_seq_no;
      if C_REDEEM%FOUND then
         update sa_voucher
            set red_date          = nvl(I_red_date,red_date),
                red_store         = nvl(I_red_store,red_store),
                red_register      = nvl(I_red_register,red_register),
                red_cashier       = nvl(I_red_cashier,red_cashier),
                red_tran_seq_no   = nvl(I_red_tran_seq_no,red_tran_seq_no),
                red_tender_seq_no = nvl(I_red_tender_no,red_tender_seq_no),
                red_amt           = nvl(I_red_amt,red_amt),
                status            = 'R'
          where voucher_seq_no = L_vouch_seq_no;
      else
            insert into sa_voucher(voucher_seq_no,
                                   voucher_no,
                                   tender_type_id,
                                   red_date,
                                   red_store,
                                   red_register,
                                   red_cashier,
                                   red_tran_seq_no,
                                   red_tender_seq_no,
                                   red_amt,
                                   status)
                           values (L_vouch_seq_no,
                                   I_voucher_no,
                                   I_voucher_type,
                                   I_red_date,
                                   I_red_store,
                                   I_red_register,
                                   I_red_cashier,
                                   I_red_tran_seq_no,
                                   I_red_tender_no,
                                   I_red_amt,
                                   'R');           
      end if;   
      close C_REDEEM;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END REDEEM_VOUCHER;
---------------------------------------------------------------------------------------
FUNCTION DELETE_VOUCHER(O_ERROR_MESSAGE  IN OUT VARCHAR2,
                        I_VOUCHER_NO     IN     SA_TRAN_ITEM.VOUCHER_NO%TYPE,
                        I_DELETE_TYPE    IN     VARCHAR2)

   RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'SA_VOUCHER_SQL.DELETE_VOUCHER';
   L_red_tran_seq_no   SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE;
   L_iss_tran_seq_no   SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE;
   L_ass_date          SA_VOUCHER.ASS_DATE%TYPE;

   cursor C_STATUS is
      select red_tran_seq_no, iss_tran_seq_no, ass_date
        from sa_voucher
       where voucher_no = I_voucher_no;

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_voucher
       where voucher_no = I_voucher_no
         for update nowait;

BEGIN

   open C_STATUS;
   fetch C_STATUS into L_red_tran_seq_no, L_iss_tran_seq_no, L_ass_date;
   if L_ass_date is NOT NULL then
      update sa_voucher
         set status = 'A'
       where voucher_no = I_voucher_no;
   elsif L_iss_tran_seq_no is NOT NULL then
      update sa_voucher
         set status = 'I'
       where voucher_no = I_voucher_no;
   elsif L_red_tran_seq_no is NOT NULL then
      update sa_voucher
         set status = 'R'
       where voucher_no = I_voucher_no;
   end if;
   close C_STATUS;

   if I_delete_type = 'I' then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      update sa_voucher
         set iss_date             = NULL,
             iss_store            = NULL,
             iss_register         = NULL,
             iss_cashier          = NULL,
             iss_tran_seq_no      = NULL,
             iss_tender_seq_no    = NULL,
             iss_item_seq_no      = NULL,
             iss_amt              = NULL,
             iss_cust_name        = NULL,
             iss_cust_addr1       = NULL,
             iss_cust_addr2       = NULL,
             iss_cust_city        = NULL,
             iss_cust_state       = NULL,
             iss_cust_postal_code = NULL,
             iss_cust_country     = NULL,
             recipient_name       = NULL,
             recipient_state      = NULL,
             recipient_country    = NULL
       where voucher_no = I_voucher_no;
       ---
   elsif I_delete_type = 'T' then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      update sa_voucher
         set red_date         = NULL,
             red_store        = NULL,
             red_register     = NULL,
             red_cashier      = NULL,
             red_tran_seq_no  = NULL,
	     red_tender_seq_no  = NULL,
             red_amt          = NULL
       where voucher_no = I_voucher_no;
       ---
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_VOUCHER;
---------------------------------------------------------------------------------------
FUNCTION UPDATE_TRANSACTION_INFO
                      (O_ERROR_MESSAGE        IN OUT VARCHAR2,
                       I_TRAN_SEQ_NO          IN     SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE,
                       I_TRAN_DATE            IN     SA_VOUCHER.RED_DATE%TYPE,
                       I_TRAN_STORE           IN     SA_VOUCHER.RED_STORE%TYPE,
                       I_TRAN_REGISTER        IN     SA_VOUCHER.RED_REGISTER%TYPE,
                       I_TRAN_CASHIER         IN     SA_VOUCHER.RED_CASHIER%TYPE)
   RETURN BOOLEAN is

   L_program           VARCHAR2(60) := 'SA_VOUCHER_SQL.UPDATE_TRANSACTION_INFO';

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_voucher
       where red_tran_seq_no = I_tran_seq_no
         for update nowait;

BEGIN

   if I_tran_seq_no is NOT NULL then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      update sa_voucher
         set iss_date       = nvl(I_tran_date, iss_date),
             iss_store      = nvl(I_tran_store, iss_store),
             ass_date       = nvl(I_tran_date, ass_date),
             ass_store      = nvl(I_tran_store, ass_store),
             iss_register   = nvl(I_tran_register, iss_register),
             iss_cashier    = nvl(I_tran_cashier, iss_cashier)
       where iss_tran_seq_no = I_tran_seq_no;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_TRANSACTION_INFO;
---------------------------------------------------------------------------------------
FUNCTION UPDATE_ISS_ITEM_INFO
			(O_ERROR_MESSAGE    IN OUT VARCHAR2,
                        I_ISS_TRAN_SEQ_NO   IN     SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE,
                        I_ISS_ITEM_SEQ_NO   IN     SA_VOUCHER.ISS_ITEM_SEQ_NO%TYPE,
                        I_VOUCHER_NO        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_ISS_AMOUNT        IN     SA_VOUCHER.ISS_AMT%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'SA_VOUCHER_SQL.UPDATE_ISS_ITEM_INFO';

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_voucher
       where iss_tran_seq_no = I_iss_tran_seq_no
         and iss_item_seq_no = I_iss_item_seq_no
         for update nowait;

BEGIN

   if I_iss_tran_seq_no is NOT NULL and I_iss_item_seq_no is NOT NULL then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      update sa_voucher
         set voucher_no = nvl(I_voucher_no, voucher_no),
             iss_amt    = nvl(I_iss_amount, iss_amt)
       where iss_tran_seq_no = I_iss_tran_seq_no
         and iss_item_seq_no = I_iss_item_seq_no;
       ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ISS_ITEM_INFO;
---------------------------------------------------------------------------------------
FUNCTION UPDATE_ISS_TENDER_INFO
                       (O_ERROR_MESSAGE     IN OUT VARCHAR2,
                        I_ISS_TRAN_SEQ_NO   IN     SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE,
                        I_ISS_TENDER_SEQ_NO IN     SA_VOUCHER.ISS_TENDER_SEQ_NO%TYPE,
                        I_VOUCHER_NO        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_ISS_AMOUNT        IN     SA_VOUCHER.ISS_AMT%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'SA_VOUCHER_SQL.UPDATE_ISS_TENDER_INFO';

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_voucher
       where iss_tran_seq_no   = I_iss_tran_seq_no
        and  iss_tender_seq_no = I_iss_tender_seq_no
         for update nowait;

BEGIN

   if I_iss_tran_seq_no is NOT NULL and I_iss_tender_seq_no is NOT NULL then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      update sa_voucher
         set voucher_no = nvl(I_voucher_no, voucher_no),
             iss_amt    = nvl(I_iss_amount, iss_amt)
       where iss_tran_seq_no   = I_iss_tran_seq_no
         and iss_tender_seq_no = I_iss_tender_seq_no;
       ---
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ISS_TENDER_INFO;
---------------------------------------------------------------------------------------
FUNCTION UPDATE_RED_TENDER_INFO
                       (O_ERROR_MESSAGE     IN OUT VARCHAR2,
                        I_RED_TRAN_SEQ_NO   IN     SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE,
                        I_RED_TENDER_SEQ_NO IN     SA_VOUCHER.RED_TENDER_SEQ_NO%TYPE,
                        I_VOUCHER_NO        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_RED_AMOUNT        IN     SA_VOUCHER.RED_AMT%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'SA_VOUCHER_SQL.UPDATE_RED_TENDER_INFO';

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_voucher
       where red_tran_seq_no = I_red_tran_seq_no
         and red_tender_seq_no = I_red_tender_seq_no
         for update nowait;

BEGIN

   if I_red_tran_seq_no is NOT NULL and I_red_tender_seq_no is NOT NULL then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      update sa_voucher
         set voucher_no = nvl(I_voucher_no, voucher_no),
             red_amt    = nvl(I_red_amount, red_amt)
       where red_tran_seq_no   = I_red_tran_seq_no
         and red_tender_seq_no = I_red_tender_seq_no;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_RED_TENDER_INFO;
---------------------------------------------------------------------------------------
--- Function:   POST_VOID_VOUCHER
--- Purpose:    Called to undo the changes on the sa_voucher table when the
---             transaction has been post-voided.
---------------------------------------------------------------------------------------
FUNCTION POST_VOID_VOUCHER
                       (O_ERROR_MESSAGE   IN OUT VARCHAR2,
                        I_TRAN_SEQ_NO     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_VOUCHER_NO      IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_STATUS          IN     SA_VOUCHER.STATUS%TYPE DEFAULT NULL)
   RETURN BOOLEAN is

   L_program           VARCHAR2(60) := 'SA_VOUCHER_SQL.POST_VOID_VOUCHER';

   cursor C_LOCK_RECORD is
      select 'x'
        from sa_voucher
       where voucher_no = I_voucher_no
         for update nowait;

BEGIN
   if I_tran_seq_no is NOT NULL and I_voucher_no is NOT NULL then
      open C_LOCK_RECORD;
      close C_LOCK_RECORD;
      ---
      if (I_status = 'I') or (I_status is NULL) then
         update sa_voucher
            set iss_date             = null,
                iss_store            = null,
                iss_register         = null,
                iss_cashier          = null,
                iss_tran_seq_no      = null,
                iss_item_seq_no      = null,
                iss_tender_seq_no    = null,
                iss_amt              = null,
                iss_cust_name        = null,
                iss_cust_addr1       = null,
                iss_cust_addr2       = null,
                iss_cust_city        = null,
                iss_cust_state       = null,
                iss_cust_postal_code = null,
                iss_cust_country     = null,
                recipient_name       = null,
                recipient_state      = null,
                recipient_country    = null
          where iss_tran_seq_no = I_tran_seq_no
            and voucher_no      = I_voucher_no;
      end if;
      if (I_status = 'R') or (I_status is NULL) then
         update sa_voucher
            set red_date          = null,
                red_store         = null,
                red_register      = null,
                red_cashier       = null,
                red_tran_seq_no   = null,
                red_tender_seq_no = null,
                red_amt           = null
          where red_tran_seq_no = I_tran_seq_no
            and voucher_no      = I_voucher_no;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POST_VOID_VOUCHER;
---------------------------------------------------------------------------------------
FUNCTION POST_VOID_ONLINE_VOUCHER(O_error_message  IN OUT   VARCHAR2,
                                  I_tran_seq_no    IN       SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                  I_tran_type      IN       SA_TRAN_HEAD.TRAN_TYPE%TYPE,
                                  I_store          IN       SA_TRAN_HEAD.STORE%TYPE,
                                  I_day            IN       SA_TRAN_HEAD.DAY%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(60)              := 'SA_VOUCHER_SQL.POST_VOID_ONLINE_VOUCHER';
   L_store             SA_TRAN_HEAD.STORE%TYPE   := I_store;
   L_day               SA_TRAN_HEAD.DAY%TYPE     := I_day;

   cursor C_TRAN_ITEM is
      select item_type,
             voucher_no
        from sa_tran_item
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day;
         
   cursor C_TRAN_TENDER is
      select tender_type_group,
             voucher_no,
             tender_amt
        from sa_tran_tender
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day;

BEGIN

   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM', 
                                             'I_tran_seq_no', 
							   'NULL', 
                                             'NOT NULL');
      return FALSE;
   elsif I_tran_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM', 
                                             'I_tran_type', 
							   'NULL', 
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_tran_type not in ('SALE','RETURN','PAIDIN','PAIDOU') then
      return TRUE;
   else
      FOR recs in C_TRAN_ITEM LOOP
         if recs.item_type = 'GCN' then
             if SA_VOUCHER_SQL.POST_VOID_VOUCHER(O_error_message,
                                                 I_tran_seq_no,
                                                 recs.voucher_no,
                                                 'I') = FALSE then
                return FALSE;
             end if;
         end if;
      END LOOP;
      ---   
      FOR rec in C_TRAN_TENDER LOOP
         if rec.tender_type_group = 'VOUCH' then
            if rec.tender_amt > 0 then
               if SA_VOUCHER_SQL.POST_VOID_VOUCHER(O_error_message,
                                                   I_tran_seq_no,
                                                   rec.voucher_no,
                                                   'R') = FALSE then
                  return FALSE;
               end if;
            elsif rec.tender_amt < 0 then
               if SA_VOUCHER_SQL.POST_VOID_VOUCHER(O_error_message,
                                                   I_tran_seq_no,
                                                   rec.voucher_no,
                                                   'I') = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      END LOOP;
      ---
      return TRUE;
   end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POST_VOID_ONLINE_VOUCHER;
---------------------------------------------------------------------------------------
END SA_VOUCHER_SQL;
/
