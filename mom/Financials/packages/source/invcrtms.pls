
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE INVC_RTM_SQL AUTHID CURRENT_USER AS

---------------------------------------------------
-- Function    : NON_MERCH_CODE_COMP_CHECK
-- Purpose     : This function will check to see that all the components on a
--               given obligation or customs entry are associated with a 
--               non-merchandise code.
-- Created    : 14-Feb-2000
---------------------------------------------------
FUNCTION NON_MERCH_CODE_COMP_CHECK(    
        O_error_message      IN OUT   VARCHAR2,
        O_valid              IN OUT   BOOLEAN,
        I_obligation_key     IN       OBLIGATION_COMP.OBLIGATION_KEY%TYPE,
        I_ce_id              IN       CE_CHARGES.CE_ID%TYPE)
RETURN BOOLEAN;

---------------------------------------------------
-- Function    : OBL_INVC_WRITE
-- Purpose    : This function will populate the invoice tables from the 
--              obligation tables. Either I_supplier or I_partner_id and
--              I_partner_type must not be NULL. Also, I_supplier and 
--              I_partner_id should never both be NOT NULL. All other
--              parameters are required. 
-- Created    : 14-Feb-2000
---------------------------------------------------
FUNCTION OBL_INVC_WRITE(
         O_error_message      IN OUT   VARCHAR2,
         I_obligation_key       IN          OBLIGATION.OBLIGATION_KEY%TYPE,
         I_ext_invc_no        IN          OBLIGATION.EXT_INVC_NO%TYPE,
         I_ext_invc_date      IN          OBLIGATION.EXT_INVC_DATE%TYPE,
         I_currency_code      IN          OBLIGATION.CURRENCY_CODE%TYPE,
         I_exchange_rate      IN          OBLIGATION.EXCHANGE_RATE%TYPE,
         I_supplier           IN          OBLIGATION.SUPPLIER%TYPE,
         I_partner_type       IN          OBLIGATION.PARTNER_TYPE%TYPE,
         I_partner_id         IN          OBLIGATION.PARTNER_ID%TYPE)
RETURN BOOLEAN;

---------------------------------------------------
-- Function    : CE_INVC_WRITE
-- Purpose    : This function will populate the invoice tables from the
--            customs entry tables. All values are required.
-- Created    : 14-Feb-2000
----------------------------------------------------
FUNCTION CE_INVC_WRITE (
     O_error_message            IN OUT   VARCHAR2,
     I_ce_id                    IN       CE_HEAD.CE_ID%TYPE,
     I_entry_no                 IN       CE_HEAD.ENTRY_NO%TYPE,
     I_entry_date               IN       CE_HEAD.ENTRY_DATE%TYPE,
     I_payee                    IN       CE_HEAD.PAYEE%TYPE,
     I_payee_type               IN       CE_HEAD.PAYEE_TYPE%TYPE,
     I_currency_code            IN       CE_HEAD.CURRENCY_CODE%TYPE,
     I_exchange_rate            IN       CE_HEAD.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------
---------------------------------------------------
-- Function    : OBL_INVC_WRITE_PO
-- Purpose    : This function will populate the invoice tables from the 
--              obligation tables for PO and POIT obligation_level for the
--              orders which were open intially and closed after running docclose. Order number is required parameter.
--           
-- Created    : 20-MAR-2018
---------------------------------------------------
FUNCTION OBL_INVC_WRITE_PO(
         O_error_message      IN OUT   VARCHAR2,
         I_order_no           IN  APPT_DETAIL.DOC%TYPE)
RETURN BOOLEAN;

END INVC_RTM_SQL;
/

