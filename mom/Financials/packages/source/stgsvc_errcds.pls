CREATE OR REPLACE PACKAGE STG_SVC_SA_ERROR_CODES AUTHID CURRENT_USER
AS
   template_key           CONSTANT VARCHAR2(255):= 'SA_ERROR_CODES_DATA';
   action_new             VARCHAR2(25)          := 'NEW';
   action_mod             VARCHAR2(25)          := 'MOD';
   action_del             VARCHAR2(25)          := 'DEL';

   SA_ERROR_IMPACT_sheet             VARCHAR2(255)         := 'SA_ERROR_IMPACT';
   SA_ERROR_IMPACT$Action            NUMBER                :=1;
   SA_ERROR_IMPACT$REQUIRED_IND      NUMBER                :=4;
   SA_ERROR_IMPACT$SYSTEM_CODE       NUMBER                :=3;
   SA_ERROR_IMPACT$ERROR_CODE        NUMBER                :=2;
   
   Type SA_ERROR_IMPACT_REC_TAB IS  TABLE OF SA_ERROR_IMPACT%ROWTYPE;
   
   SA_ERROR_CODES_sheet              VARCHAR2(255)         := 'SA_ERROR_CODES';
   SA_ERROR_CODES$Action             NUMBER                :=1;
   SA_ERROR_CODES$REQUIRED_IND       NUMBER                :=10;
   SA_ERROR_CODES$HQ_OVERRIDE_IND    NUMBER                :=9;
   SA_ERROR_CODES$STR_OVERRIDE       NUMBER                :=8;
   SA_ERROR_CODES$REC_SOLUTION       NUMBER                :=7;
   SA_ERROR_CODES$TARGET_TAB         NUMBER                :=6;
   SA_ERROR_CODES$TARGET_FORM        NUMBER                :=5;
   SA_ERROR_CODES$SHORT_DESC         NUMBER                :=4;
   SA_ERROR_CODES$ERROR_DESC         NUMBER                :=3;
   SA_ERROR_CODES$ERROR_CODE         NUMBER                :=2;

   TYPE SA_ERROR_CODES_REC_TAB IS TABLE OF SA_ERROR_CODES%ROWTYPE;
   
   SA_ERROR_CODES_TL_sheet              VARCHAR2(255)         := 'SA_ERROR_CODES_TL';
   SA_ERROR_CODES_TL$Action             NUMBER                :=1;
   SA_ERROR_CODES_TL$LANG               NUMBER                :=2;
   SA_ERROR_CODES_TL$ERROR_CODE         NUMBER                :=3;
   SA_ERROR_CODES_TL$ERROR_DESC         NUMBER                :=4;
   SA_ERROR_CODES_TL$REC_SOLUTION       NUMBER                :=5;
   SA_ERROR_CODES_TL$SHORT_DESC         NUMBER                :=6;

   TYPE SA_ERROR_CODES_TL_REC_TAB IS TABLE OF SA_ERROR_CODES_TL%ROWTYPE;
   
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N') RETURN BOOLEAN;
   FUNCTION PROCESS_S9T(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        I_file_id          IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id       IN       NUMBER,
                        O_error_count      OUT      NUMBER) RETURN BOOLEAN;
  
   sheet_name_trans s9t_pkg.trans_map_typ;
    
END STG_SVC_SA_ERROR_CODES;
/