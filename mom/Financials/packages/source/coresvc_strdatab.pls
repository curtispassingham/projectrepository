create or replace PACKAGE body CORESVC_SA_STORE_DATA
IS
CURSOR c_svc_SA_STORE_DATA(I_process_id NUMBER,I_chunk_id NUMBER)
  IS
  select
      PK_SA_STORE_DATA.rowid  AS PK_SA_STORE_DATA_rid,
      SAS_STR_FK.rowid    AS SAS_STR_FK_rid,
      st.IMP_EXP,
      st.SYSTEM_CODE,
      st.STORE,
      st.PROCESS_ID,
      st.ROW_SEQ,
      upper(st.ACTION) AS action,
      st.PROCESS$STATUS
    from SVC_SA_STORE_DATA st,
      SA_STORE_DATA PK_SA_STORE_DATA,
      STORE SAS_STR_FK,
      dual
    where st.process_id = I_process_id
    and st.chunk_id     = I_chunk_id
    and st.IMP_EXP         = PK_SA_STORE_DATA.IMP_EXP (+)
    and st.SYSTEM_CODE         = PK_SA_STORE_DATA.SYSTEM_CODE (+)
    and st.STORE         = PK_SA_STORE_DATA.STORE (+)
    and st.STORE        = SAS_STR_FK.STORE (+)
    and st.action      IS NOT NULL;
  c_svc_SA_STORE_DATA_rec c_svc_SA_STORE_DATA%rowtype;
  Type errors_tab_typ
IS
  TABLE OF SVC_ADMIN_UPLD_ER%rowtype;
  Lp_errors_tab errors_tab_typ;
  PROCEDURE write_error(
    I_process_id  IN SVC_ADMIN_UPLD_ER.process_id%type,
    I_error_seq   IN SVC_ADMIN_UPLD_ER.error_seq%type,
    I_chunk_id    IN SVC_ADMIN_UPLD_ER.chunk_id%type,
    I_table_name  IN SVC_ADMIN_UPLD_ER.table_name%type,
    I_row_seq     IN SVC_ADMIN_UPLD_ER.row_seq%type,
    I_column_name IN SVC_ADMIN_UPLD_ER.column_name%type,
    I_error_msg   IN SVC_ADMIN_UPLD_ER.error_msg%type)
IS
BEGIN
  Lp_errors_tab.extend();
  Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
  Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
  Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
  Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
  Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
  Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
  Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
end write_error;
 -------------------------------------------------------------------------------
---  Name: exec_SA_STORE_DATA_ins
--- Purpose: For inserting data in SA_STORE_DATA
-------------------------------------------------------------------------------- 
PROCEDURE exec_SA_STORE_DATA_ins
  (
    I_ins_tab IN SA_STORE_DATA_rec_tab
  )
IS

BEGIN
 IF I_ins_tab     IS NOT NULL AND I_ins_tab.count>0 THEN
  FORALL i IN INDICES OF I_ins_tab
   
    INSERT INTO SA_STORE_DATA VALUES I_ins_tab
      (i
      );
   
 
END IF;
end exec_SA_STORE_DATA_ins;
 -------------------------------------------------------------------------------
---  Name: exec_SA_STORE_DATA_ins
--- Purpose: For deleting data in SA_STORE_DATA
-------------------------------------------------------------------------------- 
PROCEDURE exec_SA_STORE_DATA_del(
    I_del_tab IN SA_STORE_DATA_rec_tab )
IS
BEGIN
  IF I_del_tab     IS NOT NULL AND I_del_tab.count>0 THEN
  FORALL i IN INDICES OF I_del_tab
 
 
  DELETE from SA_STORE_DATA where 1 = 1
and IMP_EXP = I_del_tab(i).IMP_EXP
and SYSTEM_CODE = I_del_tab(i).SYSTEM_CODE
and STORE = I_del_tab(i).STORE;
 end if;
end exec_SA_STORE_DATA_del;
 -------------------------------------------------------------------------------
---  Name: CHECK_CORRESPONDING_IMP_EXP
--- Purpose: This procedure checks if both export and Import have been defined for  system SFM,UAR
-------------------------------------------------------------------------------- 
PROCEDURE CHECK_CORRESPONDING_IMP_EXP(
    I_tab        IN OUT SA_STORE_DATA_rec_tab,
    I_process_id IN SVC_SA_STORE_DATA.PROCESS_ID%TYPE,
    I_chunk_id   IN SVC_SA_STORE_DATA.CHUNK_ID%TYPE)
IS
  l_match_found BOOLEAN;
  l_table       VARCHAR2(255) :='SA_STORE_DATA';
  l_error_message RTK_ERRORS.rtk_key%TYPE;
  L_program VARCHAR2(255) := 'CORESVC_SA_STORE_DATA.CHECK_CORRESPONDING_IMP_EXP';
  l_row_seq SVC_SA_STORE_DATA.row_seq%type;
  CURSOR c_get_row_seq(P_store store.store%type, P_system_code sa_store_data.system_code%type, P_imp_exp sa_store_data.imp_exp%type )
  IS
    select row_seq
    from SVC_SA_STORE_DATA
    where process_id =I_process_id
      and chunk_id   =I_chunk_id
      and store      =P_store
      and system_code=P_system_code
      and imp_exp    =P_imp_exp;
      L_tab SA_STORE_DATA_rec_tab:=new SA_STORE_DATA_rec_tab();
BEGIN
L_tab:=I_tab;
  FOR i IN 1..I_tab.count
  LOOP
		if I_tab(i).system_code IN('SFM','UAR') then
			l_match_found:=false;

			FOR j IN 1..L_tab.count
			LOOP
			 
				if L_tab(j).store=I_tab(i).store and L_tab(j).system_code=I_tab(i).system_code and L_tab(j).imp_exp<>I_tab(i).imp_exp then
					l_match_found :=true;
					EXIT;
				end if;
			end LOOP;
			if NOT(l_match_found) then
				OPEN c_get_row_seq(I_tab(i).store,I_tab(i).system_code,I_tab(i).imp_exp);
				FETCH c_get_row_seq INTO l_row_seq;
				CLOSE c_get_row_seq;
				if I_tab(i).system_code   ='UAR' and I_tab(i).imp_exp='I' then
					l_error_message        :='CORR_EXP_UAR_REQ';
				elsif I_tab(i).system_code='UAR' and I_tab(i).imp_exp='E' then
					l_error_message        :='CORR_IMP_UAR_REQ';
				elsif I_tab(i).system_code='SFM' and I_tab(i).imp_exp='I' then
					l_error_message        :='CORR_EXP_SFM_REQ';
				elsif I_tab(i).system_code='SFM' and I_tab(i).imp_exp='E' then
					l_error_message        :='CORR_IMP_SFM_REQ';
				end if;
				WRITE_ERROR(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,l_row_seq,NULL,l_error_message);

				I_tab.delete(i);
			end if;
		end if;
   end LOOP;
end CHECK_CORRESPONDING_IMP_EXP;
-------------------------------------------------------------
---  Name: chk_system_code_error
---  Purpose : To verify if error was logged in staging layer for the system_code,I/E mismatch
----------------------------------------------------------------------
FUNCTION chk_system_code_error(I_row_seq svc_sa_store_data.row_seq%type,
  I_process_id IN SVC_SA_STORE_DATA.PROCESS_ID%TYPE) return boolean
  is
  cursor c_chk_system_error
  is
  select 'Y' from v_admin_errors
   where process_id=I_process_id
     and row_seq=I_row_seq
     and column_name='SYSTEM_CODE';
	  
  l_error varchar2(1):='N';
  BEGIN
    sql_lib.set_mark('OPEN','c_chk_system_error','v_admin_errors',null); 	
	 open c_chk_system_error;
	 sql_lib.set_mark('FETCH','c_chk_system_error','v_admin_errors',null); 	
    fetch c_chk_system_error into l_error;
	 sql_lib.set_mark('CLOSE','c_chk_system_error','v_admin_errors',null); 	
    close c_chk_system_error;
    return case l_error when 'Y' then TRUE else FALSE end;
  END chk_system_code_error;
 -------------------------------------------------------------------------------
---  Name: process_SA_STORE_DATA
-------------------------------------------------------------------------------- 
PROCEDURE process_SA_STORE_DATA(
    I_process_id IN SVC_SA_STORE_DATA.PROCESS_ID%TYPE,
    I_chunk_id   IN SVC_SA_STORE_DATA.CHUNK_ID%TYPE )
IS
  l_error BOOLEAN;
  SA_STORE_DATA_temp_rec SA_STORE_DATA%rowtype;
  SA_STORE_DATA_ins_rec SA_STORE_DATA_rec_tab:=NEW SA_STORE_DATA_rec_tab();
  SA_STORE_DATA_upd_rec SA_STORE_DATA_rec_tab:=NEW SA_STORE_DATA_rec_tab();
  SA_STORE_DATA_del_rec SA_STORE_DATA_rec_tab:=NEW SA_STORE_DATA_rec_tab();
  l_table VARCHAR2(255)                      :='SA_STORE_DATA';
  L_error_message VARCHAR2(255);
  l_code_desc code_detail.code_desc%type;
  
  
BEGIN
  FOR rec IN c_svc_SA_STORE_DATA(I_process_id,I_chunk_id)
  LOOP
    l_error       := False;
   
		if rec.action is NULL then
		  WRITE_ERROR(I_process_id, svc_admin_upld_er_seq.NEXTVAL, I_chunk_id, L_table, rec.row_seq, 'ACTION', 'FIELD_NOT_NULL');
		  L_error := TRUE;
		end if;
		if rec.action IS NOT NULL and rec.action NOT IN (STG_SVC_SA_STORE_DATA.action_new,STG_SVC_SA_STORE_DATA.action_del) then
			write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'ACTION','INV_ACT');
			l_error :=true;
		end if;
		if rec.action = STG_SVC_SA_STORE_DATA.action_new and rec.PK_SA_STORE_DATA_rid IS NOT NULL then
			write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,case when rec.imp_exp='I' then 'IMP_STORE_EXISTS' else 'EXP_STORE_EXISTS' end);
			l_error :=true;
		end if;
		if rec.action IN (STG_SVC_SA_STORE_DATA.action_del) and rec.PK_SA_STORE_DATA_rid IS NULL then
			write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,NULL,'PK_SA_STORE_DATA_MISSING');
			l_error :=true;
		end if;
		if rec.SAS_STR_FK_rid IS NULL  or rec.store=0 then
			write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STORE','INVALID_STORE');
			l_error :=true;
		end if;
		if NOT( rec.STORE IS NOT NULL ) then
			write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'STORE','STORE_REQ');
			l_error :=true;
		end if;
		if NOT( rec.SYSTEM_CODE IS NOT NULL ) then
			write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'SYSTEM_CODE','SYSTEM_NAME_REQ');
			l_error :=true;
		end if;
		if NOT( rec.IMP_EXP IS NOT NULL ) then
			write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'IMP_EXP','SA_IMPORT_EXPORT_REQ');
			l_error :=true;
      end if;
		if NOT( rec.IMP_EXP IN ( 'I','E' ) ) then
			write_error(I_process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,l_table,rec.row_seq,'IMP_EXP','INV_I_E_IND');
			l_error :=true;
		end if;
    if NOT(l_error) and chk_system_code_error(rec.row_seq,I_process_id) then
       l_error := true;
    end if;
    
    --- validations for NEW
		if rec.action =stg_svc_sa_store_data.action_new then

		 --check if system_code/Imp_exp combination is valid
			if rec.system_code is not null and rec.imp_exp is not null then  
				if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
				CASE rec.imp_exp
				WHEN 'I' then
				  'SYSI'
				ELSE
				  'SYSE'
				end, rec.system_code, l_code_desc) = FALSE then
					WRITE_ERROR(rec.process_id,svc_admin_upld_er_seq.nextval,I_chunk_id,L_table,rec.row_seq,null,L_error_message);
					l_error :=TRUE;
				end if;
			end if;
		end if;
		if NOT l_error then
			SA_STORE_DATA_temp_rec.STORE       := rec.STORE;
			SA_STORE_DATA_temp_rec.SYSTEM_CODE := rec.SYSTEM_CODE;
			SA_STORE_DATA_temp_rec.IMP_EXP     := rec.IMP_EXP;
			if rec.action                       = STG_SVC_SA_STORE_DATA.action_new then
			  SA_STORE_DATA_ins_rec.extend;
			  SA_STORE_DATA_ins_rec(SA_STORE_DATA_ins_rec.count()):=SA_STORE_DATA_temp_rec;
			end if;
			if rec.action = STG_SVC_SA_STORE_DATA.action_del then
			  SA_STORE_DATA_del_rec.extend;
			  
			  SA_STORE_DATA_del_rec(SA_STORE_DATA_del_rec.count()):=SA_STORE_DATA_temp_rec;
			end if;
		end if;
  end LOOP;
  --for UAR,SFM both Import and export need to be defined while insert
  CHECK_CORRESPONDING_IMP_EXP(SA_STORE_DATA_ins_rec,I_process_id,I_chunk_id);
  --for UAR,SFM both Import and export need to be deleted,cannot delete only one
  CHECK_CORRESPONDING_IMP_EXP(SA_STORE_DATA_del_rec,I_process_id,I_chunk_id);
  exec_SA_STORE_DATA_ins(SA_STORE_DATA_ins_rec);
  exec_SA_STORE_DATA_del(SA_STORE_DATA_del_rec);
end process_SA_STORE_DATA;
PROCEDURE clear_staging_table
  (
    I_process_id IN svc_process_tracker.process_id%type
  )
IS
BEGIN
  DELETE
  from svc_sa_store_data
  where process_id=I_process_id;
   
end clear_staging_table;
FUNCTION process(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_process_id    IN Number,
    O_error_count OUT NUMBER)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255):='CORESVC_SA_STORE_DATA.process';
  l_process_status svc_process_tracker.status%type:='PS';
  L_chunk_id      NUMBER :=1;
BEGIN
  Lp_errors_tab := NEW errors_tab_typ();
  process_SA_STORE_DATA(I_process_id,L_chunk_id);
  O_error_count := Lp_errors_tab.count();
  forall i IN 1..O_error_count
  INSERT INTO SVC_ADMIN_UPLD_ER VALUES Lp_errors_tab
    (i
    );
  Lp_errors_tab := NEW errors_tab_typ();
   if O_error_count    = 0 then
    l_process_status := 'PS';
  ELSE
    l_process_status := 'PE';
  end if;
  UPDATE svc_process_tracker
  SET status       = (CASE
                       when status = 'PE'
                       then 'PE'
                       else L_process_status
                    end),
  action_date =sysdate
  where process_id = I_process_id;
  clear_staging_table(I_process_id);
  COMMIT;
  RETURN true;
EXCEPTION
WHEN OTHERS then
  clear_staging_table(I_process_id);
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
end process;
end coresvc_sa_store_data;
/ 