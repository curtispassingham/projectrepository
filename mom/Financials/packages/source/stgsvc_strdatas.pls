CREATE OR REPLACE PACKAGE STG_SVC_SA_STORE_DATA AUTHID CURRENT_USER
AS
	template_key           CONSTANT VARCHAR2(255):='SA_STORE_DATA';
	action_new             VARCHAR2(25)          := 'NEW';
	action_mod             VARCHAR2(25)          := 'MOD';
	action_del             VARCHAR2(25)          := 'DEL';
	SA_STORE_DATA_sheet             VARCHAR2(255)         := 'SA_STORE_DATA';
	SA_STORE_DATA$Action           NUMBER                :=1;
	SA_STORE_DATA$IMP_EXP            NUMBER                :=4;
	SA_STORE_DATA$SYSTEM_CODE            NUMBER                :=3;
	SA_STORE_DATA$STORE            NUMBER                :=2;
	Type SA_STORE_DATA_rec_tab
	IS
	TABLE OF SA_STORE_DATA%rowtype;
	FUNCTION create_s9t(
		O_error_message     IN OUT rtk_errors.rtk_text%type,
		O_file_id           IN OUT s9t_folder.file_id%type,
		I_template_only_ind IN CHAR DEFAULT 'N')RETURN BOOLEAN;
	FUNCTION process_s9t(
		O_error_message IN OUT rtk_errors.rtk_text%type ,
		I_file_id       IN s9t_folder.file_id%type,
		I_process_id IN NUMBER,
		O_error_count OUT NUMBER)RETURN BOOLEAN;
		sheet_name_trans s9t_pkg.trans_map_typ;
END STG_SVC_SA_STORE_DATA;
/ 