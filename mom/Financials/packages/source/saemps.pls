CREATE OR REPLACE PACKAGE SA_EMPLOYEE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------
-- Function: TRAIT_EMP_EXISTS
-- Purpose:  This function will check for the existence of particular 
--           location trait/employee relationship
------------------------------------------------------------------------
FUNCTION TRAIT_EMP_EXISTS(O_error_message IN OUT VARCHAR2,  
                          O_exists        IN OUT BOOLEAN,
                          I_emp_id        IN OUT SA_USER.USER_ID%TYPE)
 RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function: DEL_EMP_DETAIL_RECS
-- Purpose:  This function will delete all child records
--           from the sa_user_loc_traits and sa_user tables for a particular
--           user_id. 
------------------------------------------------------------------------
FUNCTION DEL_EMP_DETAIL_RECS(O_error_message IN OUT VARCHAR2,
                             I_emp_id        IN     SA_USER.USER_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function: EMP_EXISTS
-- Purpose:  This function will check the existence of user_id in SA_USER 
--           table
------------------------------------------------------------------------
FUNCTION EMP_EXISTS(O_error_message IN OUT VARCHAR2, 
                    O_exists        IN OUT BOOLEAN,
                    I_emp_id        IN OUT SA_USER.USER_ID%TYPE)
 RETURN BOOLEAN;   
------------------------------------------------------------------
FUNCTION GET_EMP_INFO(O_error_message IN OUT VARCHAR2,
                      O_emp_type      IN OUT SA_EMPLOYEE.EMP_TYPE%TYPE,
                      O_cashier_ind   IN OUT SA_EMPLOYEE.CASHIER_IND%TYPE,
                      O_name          IN OUT SA_EMPLOYEE.NAME%TYPE,
                      IO_user_id      IN OUT SA_EMPLOYEE.USER_ID%TYPE,
                      IO_emp_id       IN OUT SA_EMPLOYEE.EMP_ID%TYPE) 
   RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Function: GET_EMP_INFO
-- Purpose:  This function will fetch employee information off the 
--           sa_employee table for a given user_id, or emp_id (or the combination)
-------------------------------------------------------------------------
FUNCTION GET_EMP_INFO(O_error_message   IN OUT VARCHAR2,
                      O_emp_type        IN OUT SA_EMPLOYEE.EMP_TYPE%TYPE,
                      O_cashier_ind     IN OUT SA_EMPLOYEE.CASHIER_IND%TYPE,
                      O_salesperson_ind IN OUT SA_EMPLOYEE.SALESPERSON_IND%TYPE,
                      O_name            IN OUT SA_EMPLOYEE.NAME%TYPE,
                      IO_user_id        IN OUT SA_EMPLOYEE.USER_ID%TYPE,
                      IO_emp_id         IN OUT SA_EMPLOYEE.EMP_ID%TYPE) 
   RETURN BOOLEAN;
-------------------------------------------------------------------------
--- Function: GET_CASHIER_NAME
--- Purpose:  This function will retrieve the cashier name that matches the 
---           POS ID for a given store.
----------------------------------------------------------------------------
FUNCTION GET_CASHIER_NAME(O_error_message  IN OUT  VARCHAR2,
                          O_name           IN OUT  SA_EMPLOYEE.NAME%TYPE,
                          I_pos_id         IN      SA_STORE_EMP.POS_ID%TYPE,
                          I_store          IN      STORE.STORE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_LOC_TRAITS_ASSIGNED(O_error_message  IN OUT  VARCHAR2,
                                   O_exists         IN OUT BOOLEAN,
                                   I_user_id        IN  SA_USER.USER_ID%TYPE)
  RETURN BOOLEAN;
  
END SA_EMPLOYEE_SQL;
/