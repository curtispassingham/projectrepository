
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_LOCKING_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------
-- Function:   GET_WRITE_LOCK
-- Purpose:    This function will be called from each of the Interactive Audit
--             forms to lock the store day being edited.
----------------------------------------------------------------------------------------
FUNCTION GET_WRITE_LOCK(O_error_message    IN OUT VARCHAR2,
                        I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function:  GET_READ_LOCK
-- Purpose :  This function will be used to lock Store Days for all batch
--            processing.  This will be done by calling DBMS_LOCK.ALLOCATE_UNIQUE 
--            to allocate a unique lockahandle to use for the lock.  Call 
--            DBMS_LOCK.REQUEST to get an exclusive lock using lockhandle. The 
--            mode should be exclusive, the timeout time should be 5 seconds 
--            <this number can be modified to fit within system requirements> 
--            and the release on commit should be no.  The function should call 
--            error_handling() to handle the return value from DBMS_SQL.REQUEST.  
--               The DBMS_LOCK is used in order to ensure that no one can query
--            the SA_STORE_DAY_READ_LOCK table while someone is in the process of 
--            adding a lock, but hasn't commited yet.  Without the DBMS_LOCK
--            person 1 could query the table, find no locks exist and start 
--            creating their write lock, before 1 has commited person 2 queries 
--            the table and also finds no lock and starts creating their own lock.
--            person 1 commits, then person 2 commits and there are two conflicting
--            locks existing ie. a big mess.  This function ensures that never 
--            happens by locking the table while the lock creation is taking place.  
--            this way if person 2 queries while person 1 is creating a lock
--            person 2 is unable to see the table and is queued behind person 1 and 
--            either 2 gets to query the table when person 1 is done, or 2's 
--            process times out depending on how long person 1 is taking and what the  
--            timeout parameter is set to.
----------------------------------------------------------------------------------------
FUNCTION GET_READ_LOCK(O_error_message      IN OUT VARCHAR2,
                       O_lock_success       IN OUT BOOLEAN,
                       I_store_day_seq_no   IN     NUMBER,
                       I_user_id            IN     VARCHAR2,
                       I_process            IN     VARCHAR2)
      RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function:  RELEASE_LOCK
-- Purpose :  This function will call Check_inputs, passing I_store_day_id, 
--            I_user, and I_process to ensure that all required values are 
--            not null.  The function will then delete the record on the 
--            sa_store_day_read_lock table for these key values and commit the 
--            changes.  By commiting the changes the Write lock will 
--            automatically be released because it is an oracle lock.
-----------------------------------------------------------------------------------------
FUNCTION RELEASE_LOCK(O_error_message      IN OUT VARCHAR2,
                      I_store_day_seq_no   IN     NUMBER,
                      I_user_id            IN     VARCHAR2,
                      I_process            IN     VARCHAR2)
      RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
END SA_LOCKING_SQL;
/