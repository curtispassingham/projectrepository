
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SA_ESCHEAT_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------
-- Function: ESCHEAT_EXISTS 
-- Purpose:  Check for duplicate escheatment combinations
------------------------------------------------------------------------
FUNCTION ESCHEAT_EXISTS (O_error_message      IN OUT VARCHAR2,
                         O_exists             IN OUT BOOLEAN,
                         I_partner_type	      IN     PARTNER.PARTNER_TYPE%TYPE,
                         I_partner_id         IN     PARTNER.PARTNER_ID%TYPE,
                         I_country            IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_state              IN     STATE.STATE%TYPE,
                         I_escheat_opt_seq_no IN     SA_ESCHEAT_OPTIONS.ESCHEAT_OPT_SEQ_NO%TYPE) 
  RETURN BOOLEAN;
------------------------------------------------------------------
-- Function: GET_NEXT_SEQ 
-- Purpose:  this function will retrieve the next available number in the
--           SA_ESCHEAT_OPT_SEQ_NO_SEQUENCE
------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ (O_error_message   IN OUT  VARCHAR2,
                       O_next_val        IN OUT  SA_ESCHEAT_OPTIONS.ESCHEAT_OPT_SEQ_NO%TYPE)
  RETURN BOOLEAN;
------------------------------------------------------------------
-- Function: GET_PARTNER_INFO
-- Purpose:  this function fetches the appropriate partner_id and 
--           partner_type from the partner table
------------------------------------------------------------------------
FUNCTION GET_PARTNER_INFO (O_error_message  IN OUT VARCHAR2,
                           O_exists         IN OUT BOOLEAN,
                           O_partner_type   IN OUT PARTNER.PARTNER_TYPE%TYPE,
                           O_partner_id     IN OUT PARTNER.PARTNER_ID%TYPE,
                           I_country        IN     COUNTRY.COUNTRY_ID%TYPE,
                           I_state          IN     STATE.STATE%TYPE) 
  RETURN BOOLEAN;
------------------------------------------------------------------
-- Function: VOUCHER_EXISTS
-- Purpose:  check for duplicate voucher combinations
------------------------------------------------------------------------
FUNCTION VOUCHER_EXISTS (O_error_message      IN OUT VARCHAR2,
                         O_exists             IN OUT BOOLEAN,
                         I_tender_type_id     IN     SA_VOUCHER_OPTIONS.TENDER_TYPE_ID%TYPE) 
  RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function: DELETE_ESCHEAT
-- Purpose:  deletes all records on the sa_escheat_options table
------------------------------------------------------------------------
FUNCTION DELETE_ESCHEAT(O_error_message      IN OUT VARCHAR2) 
  RETURN BOOLEAN;
------------------------------------------------------------------------
END SA_ESCHEAT_SQL;
/
