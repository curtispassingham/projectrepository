-- File Name : STG_SVC_REFERENCE_FIELD_spec.pls
CREATE OR REPLACE PACKAGE STG_SVC_REFERENCE_FIELD AUTHID CURRENT_USER
AS
  TEMPLATE_KEY           CONSTANT VARCHAR2(255):='SA_REFERENCE_FIELD_DATA';
  ACTION_NEW             VARCHAR2(25)          := 'NEW';
  ACTION_MOD             VARCHAR2(25)          := 'MOD';
  ACTION_DEL             VARCHAR2(25)          := 'DEL';
  SA_REFERENCE_sheet             VARCHAR2(255)         := 'SA_REFERENCE';
  SA_REFERENCE$Action            NUMBER                :=1;
  --SA_REFERENCE$REF_SEQ_NO       NUMBER                :=2;
  SA_REFERENCE$TRAN_TYPE         NUMBER                :=3;
  SA_REFERENCE$SUB_TRAN_TYPE     NUMBER                :=4;
  SA_REFERENCE$REASON_CODE       NUMBER                :=5;
  SA_REFERENCE$REF_NO            NUMBER                :=6;
  SA_REFERENCE$REF_LABEL_CODE    NUMBER                :=7;
 
  sheet_name_trans     S9T_PKG.TRANS_MAP_TYP;
Type SA_REFERENCE_rec_tab IS TABLE OF SA_REFERENCE%rowtype;
------------------------------------------------------------------------------------------  
-- Function Name : CREATE_S9T
--  Description  : This function will download all the data from SA_REFERENCE table.
-----------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      		    O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
      	            I_template_only_ind IN CHAR DEFAULT 'N')
 RETURN BOOLEAN;
------------------------------------------------------------------------------------------   
-- Function Name : PROCESS_S9T
--  Description  : This function will upload data from .ods file to SVC_SA_REFERNCE table.
-----------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_file_id       IN S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN NUMBER,
                     O_error_count   OUT NUMBER)
    RETURN BOOLEAN;
------------------------------------------------------------------------------------------    
END STG_SVC_REFERENCE_FIELD;
/
