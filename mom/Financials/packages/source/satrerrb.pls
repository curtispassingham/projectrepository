CREATE OR REPLACE PACKAGE BODY TRAN_ERROR_SQL AS
------------------------------------------------------------------------------------------
FUNCTION REFRESH_ITEM_ERRORS(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_ind                   IN OUT SA_TRAN_ITEM.ERROR_IND%TYPE,
                             I_store_day_seq_no            IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                             I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_item_seq_no                 IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                             I_item                        IN     SA_TRAN_ITEM.ITEM%TYPE,
                             I_selling_uom                 IN     SA_TRAN_ITEM.SELLING_UOM%TYPE,
                             I_standard_uom                IN     SA_TRAN_ITEM.STANDARD_UOM%TYPE,
                             I_item_type_changed_ind       IN     VARCHAR2,
                             I_item_changed_ind            IN     VARCHAR2,
                             I_unit_retail_changed_ind     IN     VARCHAR2,
                             I_orig_ur_changed_ind         IN     VARCHAR2,
                             I_override_reason_changed_ind IN     VARCHAR2,
                             I_qty_changed_ind             IN     VARCHAR2,
                             I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50)              := 'TRAN_ERROR_SQL.REFRESH_ITEM_ERRORS';
   L_store                 SA_STORE_DAY.STORE%TYPE   := I_store;
   L_day                   SA_STORE_DAY.DAY%TYPE     := I_day;
   L_item_error_code       SA_ERROR.ERROR_CODE%TYPE;
   L_exists                BOOLEAN                   := FALSE;
   L_uom_conversion_factor NUMBER;

   cursor C_ITEM_ERROR is
      select error_code,
             rec_type
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and key_value_1 = I_item_seq_no
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day
         and error_code in ('INVLD_ITEM_NO',
                            'SKU_NOT_FOUND',
                            'UPC_NOT_FOUND',
                            'NON_MERCH_ITEM_NOT_FOUND',
                            'NON_MERCH_ITEM_NO_REQ',
                            'INVLD_DOCUMENT',
                            'DOCUMENT_REDEEM');

BEGIN
   if I_tran_seq_no is NOT NULL and
      I_item_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_selling_uom is NOT NULL and
         I_standard_uom is NOT NULL and
         (I_selling_uom != I_standard_uom) then
         ---
         if UOM_SQL.CONVERT(O_error_message,
                            L_uom_conversion_factor,
                            I_standard_uom,
                            '1',
                            I_selling_uom,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if (L_uom_conversion_factor != 0) then
            ---
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'INVLD_SELLING_UOM',
                                          I_item_seq_no,
                                          NULL,
                                          'TITEM') = FALSE then
               return FALSE;
            end if;
            ---
         end if;
         ---
      end if;
      ---
      if I_item_type_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_SAIT',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_item_changed_ind = 'Y' then
         FOR I in C_ITEM_ERROR LOOP
            ---
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          I.error_code,
                                          I_item_seq_no,
                                          NULL,
                                          I.rec_type,
                                          L_store,
                                          L_day) = FALSE then
               return FALSE;
            end if;
         END LOOP;
         ---
      end if;
      ---
      if I_unit_retail_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'TITEM_URT_STIN',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_orig_ur_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'TITEM_OUR_STIN',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_override_reason_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_ORRC',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_qty_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'TITEM_QTY_STIN',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'TITEM_UOM_QTY_ZERO',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;                  
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'TITEM_UOM_QTY_STIN',
                                       I_item_seq_no,
                                       NULL,
                                       'TITEM',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_exists,
                                   NULL,
                                   NULL,
                                   I_tran_seq_no,
                                   I_item_seq_no,
                                   NULL,
                                   'TITEM',
                                   I_store_day_seq_no,
                                   L_store,
                                   L_day) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_ind := 'N';
      else
         O_error_ind := 'Y';
      end if;
   else
      O_error_message := sql_lib.create_msg('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END REFRESH_ITEM_ERRORS;
-------------------------------------------------------------------------------------------------------                                    
FUNCTION REFRESH_TENDER_ERRORS(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error_ind                   IN OUT SA_TRAN_TENDER.ERROR_IND%TYPE,
                               I_store_day_seq_no            IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                               I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                               I_tender_seq_no               IN     SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE,
                               I_tend_type_group_changed_ind IN     VARCHAR2,
                               I_tender_type_id_changed_ind  IN     VARCHAR2,
                               I_tender_no_changed_ind       IN     VARCHAR2,
                               I_tender_amt_changed_ind      IN     VARCHAR2,
                               I_cc_no_changed_ind           IN     VARCHAR2,
                               I_cc_exp_date_changed_ind     IN     VARCHAR2,
                               I_cc_auth_src_changed_ind     IN     VARCHAR2,
                               I_cc_card_verf_changed_ind    IN     VARCHAR2,
                               I_cc_spec_cond_changed_ind    IN     VARCHAR2,
                               I_cc_entry_mode_changed_ind   IN     VARCHAR2,
                               I_add_details_changed_ind     IN     VARCHAR2,
                               I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                               I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)             := 'TRAN_ERROR_SQL.REFRESH_TENDER_ERRORS';
   L_store   SA_STORE_DAY.STORE%TYPE  := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE    := I_day;
   L_exists  BOOLEAN                  := FALSE;

   cursor C_GET_TEND_TYPE_ID_ERROR is
      select error_code
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day
         and key_value_1 = I_tender_seq_no
         and error_code in ('TTEND_TTI_STIN','INVLD_TTEND_ID');

   cursor C_GET_TEND_CC_NO_ERROR is
      select error_code
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day
         and key_value_1 = I_tender_seq_no
         and error_code in ('INVLD_CC_CHECKSUM', 'CC_NO_REQ', 'INVLD_CC_PREFIX', 'INVLD_CC_MASK');

   cursor C_GET_TEND_CC_DATE_ERROR is
      select error_code
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day
         and key_value_1 = I_tender_seq_no
         and error_code in ('INVLD_CC_EXP_DATE');

   cursor C_GET_TEND_AD_DET_ERROR is
      select error_code
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day
         and key_value_1 = I_tender_seq_no
         and error_code in ('TTEND_CHECK_NO_REQ',
                            'INVLD_IDMH',
                            'IDNT_ID_WITHOUT_IDNT_MTHD',
                            'TTEND_OCA_STIN',
                            'ORGCUR_AMT_WITHOUT_ORGCUR',
                            'INVLD_ORIG_CURR',
                            'IDNT_MTHD_WITHOUT_IDNT_ID',
                            'ORGCUR_WITHOUT_ORGCUR_AMT',
                            'INVLD_CHECK_NO');

BEGIN

   if I_store_day_seq_no is NOT NULL and
      I_tran_seq_no is NOT NULL and
      I_tender_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---      
      if I_tend_type_group_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_TENT',
                                       I_tender_seq_no,
                                       NULL,
                                       'TTEND',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_tender_type_id_changed_ind = 'Y' then
         for A_rec in C_GET_TEND_TYPE_ID_ERROR LOOP
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          A_rec.error_code,
                                          I_tender_seq_no,
                                          NULL,
                                          'TTEND',
                                          L_store,
                                          L_day) = FALSE then
               return FALSE;
            end if;
         end loop;
      end if;
      ---
      if I_tender_amt_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'TTEND_TAM_STIN',
                                       I_tender_seq_no,
                                       NULL,
                                       'TTEND',
                                       L_store,
                                       L_day) = FALSE then
             return FALSE;
          end if;
       end if;
       ---
       if I_cc_no_changed_ind = 'Y' then
         for B_rec in C_GET_TEND_CC_NO_ERROR LOOP
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          B_rec.error_code,
                                          I_tender_seq_no,
                                          NULL,
                                          'TTEND',
                                          L_store,
                                          L_day) = FALSE then
                return FALSE;
             end if;
          end loop;
       end if;
       ---
       if I_cc_exp_date_changed_ind = 'Y' then
          for C_rec in C_GET_TEND_CC_DATE_ERROR LOOP
             if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                           I_store_day_seq_no,
                                           I_tran_seq_no,
                                           C_rec.error_code,
                                           I_tender_seq_no,
                                           NULL,
                                           'TTEND',
                                           L_store,
                                           L_day) = FALSE then
                return FALSE;
             end if;
          end loop;
       end if;
       ---
       if I_cc_auth_src_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_CCAS',
                                       I_tender_seq_no,
                                       NULL,
                                       'TTEND',
                                       L_store,
                                       L_day) = FALSE then
             return FALSE;
          end if;
       end if;
       ---
       if I_cc_card_verf_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_CCVF',
                                       I_tender_seq_no,
                                       NULL,
                                       'TTEND',
                                       L_store,
                                       L_day) = FALSE then
             return FALSE;
          end if;
       end if;
       ---
       if I_cc_spec_cond_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_CCSC',
                                       I_tender_seq_no,
                                       NULL,
                                       'TTEND',
                                       L_store,
                                       L_day) = FALSE then
             return FALSE;
          end if;
       end if;
       ---
       if I_cc_entry_mode_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_CCEM',
                                       I_tender_seq_no,
                                       NULL,
                                       'TTEND',
                                       L_store,
                                       L_day) = FALSE then
             return FALSE;
          end if;
       end if;
       ---
       if I_add_details_changed_ind = 'Y' then
          for C_rec in C_GET_TEND_AD_DET_ERROR LOOP
             if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                           I_store_day_seq_no,
                                           I_tran_seq_no,
                                           C_rec.error_code,
                                           I_tender_seq_no,
                                           NULL,
                                           'TTEND',
                                           L_store,
                                           L_day) = FALSE then
                return FALSE;
             end if;
          end loop;
       end if;
      ---
       if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                    L_exists,
                                    NULL,
                                    NULL,
                                    I_tran_seq_no,
                                    I_tender_seq_no,
                                    NULL,
                                    'TTEND',
                                    I_store_day_seq_no,
                                    L_store,
                                    L_day) = FALSE then
          return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_ind := 'N';
      else
         O_error_ind := 'Y';
      end if;
   else
      O_error_message := sql_lib.create_msg('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
       return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END REFRESH_TENDER_ERRORS;   
--------------------------------------------------------------------------------------------
FUNCTION REFRESH_TAX_ERRORS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error_ind            IN OUT SA_TRAN_TAX.ERROR_IND%TYPE,
                            I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                            I_tran_seq_no          IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                            I_tax_seq_no           IN     SA_TRAN_TAX.TAX_SEQ_NO%TYPE,
                            I_tax_code_changed_ind IN     VARCHAR2,
                            I_tax_amt_changed_ind  IN     VARCHAR2,
                            I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                            I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.REFRESH_TAX_ERRORS';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;
   L_exists  BOOLEAN;

BEGIN

   if I_store_day_seq_no is NOT NULL and
      I_tran_seq_no is NOT NULL and
      I_tax_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_tax_code_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_TAXC',
                                       I_tax_seq_no,
                                       NULL,
                                       'TTAX',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;

         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_VATC',
                                       I_tax_seq_no,
                                       NULL,
                                       'TTAX',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;

      end if;
      ---
      if I_tax_amt_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'TTAX_AMT_STIN',
                                       I_tax_seq_no,
                                       NULL,
                                       'TTAX',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_exists,
                                   NULL,
                                   NULL,
                                   I_tran_seq_no,
                                   I_tax_seq_no,
                                   NULL,
                                   'TTAX',
                                   I_store_day_seq_no,
                                   L_store,
                                   L_day) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_ind := 'N';
      else
         O_error_ind := 'Y';
      end if;
   else
      O_error_message := sql_lib.create_msg('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;  
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END REFRESH_TAX_ERRORS;
-----------------------------------------------------------------------------------------------
FUNCTION REFRESH_IGTAX_ERRORS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error_ind            IN OUT SA_TRAN_IGTAX.ERROR_IND%TYPE,
                              I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                              I_tran_seq_no          IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_item_seq_no          IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                              I_igtax_seq_no         IN     SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE,
                              I_tax_auth_changed_ind IN     VARCHAR2,
                              I_tax_code_changed_ind IN     VARCHAR2,
                              I_tax_amt_changed_ind  IN     VARCHAR2,
							  I_tax_rate_changed_ind IN		VARCHAR2,
                              I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                              I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.REFRESH_IGTAX_ERRORS';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;
   L_exists  BOOLEAN;

BEGIN
   ---
   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_item_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_igtax_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_igtax_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        I_store_day_seq_no,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_tax_auth_changed_ind = 'Y' then
      if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                    I_store_day_seq_no,
                                    I_tran_seq_no,
                                    'INVLD_TAX_AUTHORITY',
                                    I_item_seq_no,
                                    I_igtax_seq_no,
                                    'IGTAX',
                                    L_store,
                                    L_day) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_tax_code_changed_ind = 'Y' then
      if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                    I_store_day_seq_no,
                                    I_tran_seq_no,
                                    'INVLD_TAXC',
                                    I_item_seq_no,
                                    I_igtax_seq_no,
                                    'IGTAX',
                                    L_store,
                                    L_day) = FALSE then
         return FALSE;
      end if;
      if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                    I_store_day_seq_no,
                                    I_tran_seq_no,
                                    'INVLD_VATC',
                                    I_item_seq_no,
                                    I_igtax_seq_no,
                                    'IGTAX',
                                    L_store,
                                    L_day) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_tax_amt_changed_ind = 'Y' then
      if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                    I_store_day_seq_no,
                                    I_tran_seq_no,
                                    'TOTAL_IGTAX_AMT_STIN',
                                    I_item_seq_no,
                                    I_igtax_seq_no,
                                    'IGTAX',
                                    L_store,
                                    L_day) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_tax_rate_changed_ind = 'Y' then
      if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                    I_store_day_seq_no,
                                    I_tran_seq_no,
                                    'IGTAX_RAT_STIN',
                                    I_item_seq_no,
                                    I_igtax_seq_no,
                                    'IGTAX',
                                    L_store,
                                    L_day) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                    I_store_day_seq_no,
                                    I_tran_seq_no,
                                    'DUP_TAXC_COMPITEM',
                                    I_item_seq_no,
                                    I_igtax_seq_no,
                                    'IGTAX',
                                    L_store,
                                    L_day) = FALSE then
         return FALSE;
   end if;
   ---
   if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                L_exists,
                                NULL,
                                NULL,
                                I_tran_seq_no,
                                I_item_seq_no,
                                I_igtax_seq_no,
                                'IGTAX',
                                I_store_day_seq_no,
                                L_store,
                                L_day) = FALSE then
      return FALSE;
   end if;
   ---
   if L_exists = FALSE then
      O_error_ind := 'N';
   else
      O_error_ind := 'Y';
   end if;
   ---
   return TRUE;  
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END REFRESH_IGTAX_ERRORS;
-----------------------------------------------------------------------------------------------
FUNCTION REFRESH_DISC_ERRORS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_ind                  IN OUT SA_TRAN_DISC.ERROR_IND%TYPE,
                             I_store_day_seq_no           IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                             I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_item_seq_no                IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                             I_item                       IN     SA_TRAN_ITEM.ITEM%TYPE,
                             I_discount_seq_no            IN     SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE,
                             I_discount_uom               IN     SA_TRAN_ITEM.SELLING_UOM%TYPE,
                             I_standard_uom               IN     SA_TRAN_ITEM.STANDARD_UOM%TYPE,
                             I_discount_type_changed_ind  IN     VARCHAR2,
                             I_promotion_changed_ind      IN     VARCHAR2,
                             I_qty_changed_ind            IN     VARCHAR2,
                             I_unit_disc_amt_changed_ind  IN     VARCHAR2,
                             I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50)            := 'TRAN_ERROR_SQL.REFRESH_DISC_ERRORS';
   L_exists                BOOLEAN;
   L_store                 SA_STORE_DAY.STORE%TYPE := I_store;
   L_day                   SA_STORE_DAY.DAY%TYPE   := I_day;
   L_uom_conversion_factor NUMBER;

   cursor C_PROMOTION_ERRORS is
      select error_code
        from sa_error
       where tran_seq_no = I_tran_seq_no
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day
         and key_value_1 = I_item_seq_no
         and key_value_2 = I_discount_seq_no
         and error_code in ('IDISC_DRN_STIN', 'INVLD_PRMT','IDISC_PRC_STIN');

BEGIN   

   if I_store_day_seq_no is NOT NULL and
      I_tran_seq_no is NOT NULL and
      I_item_seq_no is NOT NULL and
      I_discount_seq_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_tran_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_discount_uom is NOT NULL and
         I_standard_uom is NOT NULL and 
         (I_discount_uom != I_standard_uom) then
         ---
         if UOM_SQL.CONVERT(O_error_message,
                            L_uom_conversion_factor,
                            I_standard_uom,
                            '1',
                            I_discount_uom,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if (L_uom_conversion_factor != 0) then
            ---
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          'INVLD_DISCOUNT_UOM',
                                          I_item_seq_no,
                                          I_discount_seq_no,
                                          'IDISC') = FALSE then
               return FALSE;
            end if;
            ---
         end if;
      ---
      end if;
      if I_discount_type_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'INVLD_SADT',
                                       I_item_seq_no,
                                       I_discount_seq_no,
                                       'IDISC',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_promotion_changed_ind = 'Y' then
         FOR C_rec in C_PROMOTION_ERRORS LOOP
            if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                          I_store_day_seq_no,
                                          I_tran_seq_no,
                                          C_rec.error_code,
                                          I_item_seq_no,
                                          I_discount_seq_no,
                                          'IDISC',
                                          L_store,
                                          L_day) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;
      ---
      if I_qty_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'IDISC_QTY_STIN',
                                       I_item_seq_no,
                                       I_discount_seq_no,
                                       'IDISC',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_unit_disc_amt_changed_ind = 'Y' then
         if SA_ERROR_SQL.DELETE_ERRORS(O_error_message,
                                       I_store_day_seq_no,
                                       I_tran_seq_no,
                                       'IDISC_UDA_STIN',
                                       I_item_seq_no,
                                       I_discount_seq_no,
                                       'IDISC',
                                       L_store,
                                       L_day) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if SA_ERROR_SQL.ERRORS_EXIST(O_error_message,
                                   L_exists,
                                   NULL,
                                   NULL,
                                   I_tran_seq_no,
                                   I_item_seq_no,
                                   I_discount_seq_no,
                                   'IDISC',
                                   I_store_day_seq_no,
                                   L_store,
                                   L_day) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_ind := 'N';
      else
         O_error_ind := 'Y';
      end if;
   else
      O_error_message := sql_lib.create_msg('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END REFRESH_DISC_ERRORS;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CLOSE(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_CLOSE';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'CLOSE_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'CLOSE_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'CLOSE_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'CLOSE_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'CLOSE_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);
   
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'CLOSE_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'CLOSE_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day    
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_CLOSE;
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COND(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                      I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_COND';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'COND_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'COND_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'COND_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'COND_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'COND_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'COND_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'COND_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_COND;
------------------------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DCLOSE(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_DCLOSE';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'DCLOSE_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'DCLOSE_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);                       

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'DCLOSE_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'DCLOSE_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'DCLOSE_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);                         
   
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'DCLOSE_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'DCLOSE_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day    
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_DCLOSE;
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_LOAN(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                      I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_LOAN';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'LOAN_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'LOAN_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);                       

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'LOAN_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'LOAN_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'LOAN_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'LOAN_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'LOAN_TENDER_REQ'
      and exists (select 'x' from sa_tran_tender
                   where tran_seq_no       = I_tran_seq_no
                         and store = L_store
                         and day = L_day);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_LOAN;
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_METER(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_METER';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'METER_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'METER_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);                       

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'METER_ITEM_REQ'
      and exists (select 'x' from sa_tran_item
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);                   

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'METER_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);    

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'METER_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);                            

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'METER_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_METER;
-------------------------------------------------------------------------------------------------     
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_NOSALE(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_NOSALE';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'NOSALE_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day                       
                         and attrib_seq_no = key_value_1);                         

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'NOSALE_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);                       

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'NOSALE_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'NOSALE_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'NOSALE_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'NOSALE_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);        
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'NOSALE_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_NOSALE;
------------------------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_OPEN(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                      I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_OPEN';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'OPEN_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'OPEN_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'OPEN_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'OPEN_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'OPEN_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);
   
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'OPEN_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);     

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'OPEN_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_OPEN;
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PAIDIN(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_PAIDIN';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDIN_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDIN_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDIN_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDIN_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDIN_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDIN_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2); 
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDIN_REASON_CODE_REQ'
      and exists (select 'x' from sa_tran_head
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day
                     and reason_code is NOT NULL);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDIN_TENDER_REQ'
      and exists (select 'x' from sa_tran_tender
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_PAIDIN;
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PAIDOU(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_PAIDOU';
   L_store           SA_STORE_DAY.STORE%TYPE := I_store;
   L_day             SA_STORE_DAY.DAY%TYPE   := I_day;
   L_sub_tran_type   SA_TRAN_HEAD.SUB_TRAN_TYPE%TYPE;

   cursor C_GET_SUB_TRAN_TYPE is 
      select sub_tran_type
        from sa_tran_head
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDOU_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDOU_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDOU_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDOU_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDOU_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDOU_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2); 
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDOU_REASON_CODE_REQ'
      and exists (select 'x' from sa_tran_head
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day
                     and reason_code is NOT NULL);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PAIDOU_TENDER_REQ'
      and exists (select 'x' from sa_tran_tender
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);

   open C_GET_SUB_TRAN_TYPE;
   fetch C_GET_SUB_TRAN_TYPE into L_sub_tran_type;
   close C_GET_SUB_TRAN_TYPE;

   if L_sub_tran_type = 'MV' then
      delete from sa_error
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day
         and error_code = 'INVLD_VENDOR_NO_MV'
         and exists (select 'x' from sa_tran_head
                      where vendor_no in (select to_char(supplier)
                                         from sups)
                        and tran_seq_no = I_tran_seq_no
                        and store = L_store
                        and day = L_day);
   end if;

   if L_sub_tran_type = 'EV' then
      delete from sa_error
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day
         and error_code = 'INVLD_VENDOR_NO_EV'
         and exists (select 'x' from sa_tran_head
                      where vendor_no in (select partner_id
                                          from partner
                                          where partner_type = 'EV')
                        and tran_seq_no = I_tran_seq_no
                        and store = L_store
                        and day = L_day);
   end if;



   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'VENDOR_DATA_REQ'
      and exists (select 'x' from sa_tran_head
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day
                     and ((vendor_invc_no is NOT NULL) or (payment_ref_no is NOT NULL) or (proof_of_delivery_no is NOT NULL)));

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'VENDOR_NO_REQ'
      and exists (select 'x' from sa_tran_head
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day
                     and sub_tran_type in ('MV', 'EV')
                     and vendor_no is NOT NULL);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'INVLD_NON_MERCH_CODE'
      and exists (select 'x' from sa_tran_head
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day
                     and sub_tran_type in ('MV', 'EV')
                     and reason_code in (select non_merch_code
                                           from non_merch_code_head));

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_PAIDOU;
-------------------------------------------------------------------------------------------------  
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PULL(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tran_seq_no                 IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                      I_store                       IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                      I_day                         IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_PULL';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PULL_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PULL_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PULL_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PULL_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PULL_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PULL_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PULL_TENDER_REQ'
      and exists (select 'x' from sa_tran_tender
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_PULL;
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PUMPT(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_PUMPT';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PUMPT_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PUMPT_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PUMPT_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PUMPT_ITEM_REQ'
      and exists (select 'x' from sa_tran_item
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PUMPT_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PUMPT_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PUMPT_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_PUMPT;
-------------------------------------------------------------------------------------------------  
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PVOID(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_PVOID';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PVOID_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PVOID_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PVOID_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PVOID_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PVOID_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PVOID_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'PVOID_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_PVOID;
------------------------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_REFUND(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_REFUND';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'REFUND_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'REFUND_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'REFUND_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);
   
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'REFUND_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'REFUND_TENDER_REQ'
      and exists (select 'x' from sa_tran_tender
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_REFUND;
------------------------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TANKDP(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tran_seq_no               IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                        I_store                     IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                        I_day                       IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_TANKDP';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TANKDP_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TANKDP_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TANKDP_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TANKDP_ITEM_REQ'
      and exists (select 'x' from sa_tran_item
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TANKDP_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);
   
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TANKDP_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TANKDP_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_TANKDP;
-------------------------------------------------------------------------------------------------  
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TERM(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_TERM';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TERM_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TERM_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_TERM;
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TOTAL(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_TOTAL';
   L_store   SA_STORE_DAY.STORE%TYPE := I_store;
   L_day     SA_STORE_DAY.DAY%TYPE   := I_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_NO_CATT'
      and not exists (select 'x' from sa_cust_attrib
                       where tran_seq_no   = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and attrib_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_NO_CUST'
      and not exists (select 'x' from sa_customer
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_NO_DISC'
      and not exists (select 'x' from sa_tran_disc
                       where tran_seq_no     = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no     = key_value_1
                         and discount_seq_no = key_value_2);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_NO_ITEM'
      and not exists (select 'x' from sa_tran_item
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_NO_TAX'
      and not exists (select 'x' from sa_tran_tax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tax_seq_no  = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_NO_IGTAX'
      and not exists (select 'x' from sa_tran_igtax
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and item_seq_no = key_value_1
                         and igtax_seq_no  = key_value_2);
                         
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_NO_TEND'
      and not exists (select 'x' from sa_tran_tender
                       where tran_seq_no = I_tran_seq_no
                         and store = L_store
                         and day = L_day
                         and tender_seq_no = key_value_1);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_REF_NO_1_REQ'
      and exists (select 'x' from sa_tran_head
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day
                     and ref_no1 in (select total_id
                                       from sa_total_head));

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'TOTAL_VALUE_REQ'
      and exists (select 'x' from sa_tran_head
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day
                     and value is NOT NULL);


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_TOTAL;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SALE_RETURN(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                             I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_SALE_RETURN';
   L_store       SA_STORE_DAY.STORE%TYPE := I_store;
   L_day         SA_STORE_DAY.DAY%TYPE   := I_day;
   L_cust_count  NUMBER(3)               := 0;

   cursor C_GET_CUST_COUNT is
      select count(*)
        from sa_customer
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'RETURN_ITEM_REQ'
      and exists (select 'x' from sa_tran_item
                  where tran_seq_no = I_tran_seq_no
                    and store = L_store
                    and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'SALE_ITEM_REQ'
      and exists (select 'x' from sa_tran_item
                  where tran_seq_no = I_tran_seq_no
                    and store = L_store
                    and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'RETURN_TENDER_REQ'
      and exists (select 'x' from sa_tran_tender
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'SALE_TENDER_REQ'
      and exists (select 'x' from sa_tran_tender
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day);

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   open C_GET_CUST_COUNT;
   fetch C_GET_CUST_COUNT into L_cust_count;
   close C_GET_CUST_COUNT;

   if L_cust_count < 2 then 
      delete from sa_error
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day
         and error_code = 'TRAN_XTRA_CUST';
   end if;

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'CUST_ID_REQ'
      and exists (select 'x' from sa_customer
                   where tran_seq_no = I_tran_seq_no
                     and store = L_store
                     and day = L_day
                     and cust_id is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_SALE_RETURN;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_EEXCH(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tran_seq_no                IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                       I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_EEXCH';
   L_store      SA_STORE_DAY.STORE%TYPE := I_store;
   L_day        SA_STORE_DAY.DAY%TYPE   := I_day;
   L_item_count NUMBER(3)               := 0;

   cursor C_COUNT_ITEMS is
      select count(*) from sa_tran_item
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---      
   SQL_LIB.SET_MARK('OPEN','C_COUNT_ITEMS','SA_TRAN_ITEM','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
   open C_COUNT_ITEMS;
   SQL_LIB.SET_MARK('FETCH','C_COUNT_ITEMS','SA_TRAN_ITEM','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
   fetch C_COUNT_ITEMS into L_item_count;
   SQL_LIB.SET_MARK('CLOSE','C_COUNT_ITEMS','SA_TRAN_ITEM','TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
   close C_COUNT_ITEMS;

   if L_item_count >= 2 then 
      delete from sa_error
       where tran_seq_no = I_tran_seq_no
         and store = L_store
         and day = L_day
         and error_code = 'EEXCH_ITEM_REQ';
   end if;

   delete from sa_error
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and error_code = 'THEAD_VAL_STIN';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_EEXCH;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_MISS_TRAN(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_store_day_seq_no           IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                           I_store                      IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                           I_day                        IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)            := 'TRAN_ERROR_SQL.PROCESS_MISS_TRAN';
   L_store        SA_STORE_DAY.STORE%TYPE := I_store;
   L_day          SA_STORE_DAY.DAY%TYPE   := I_day;   
   L_t_loc1        NUMBER(2);
   L_t_loc2        NUMBER(2);
   L_first_value  VARCHAR2(50);
   L_second_value VARCHAR2(50);
   L_tran_count   NUMBER(10);
   L_miss_tran    NUMBER(10);

   cursor C_GET_BIG_GAP is
      select orig_value
        from sa_error
       where error_code = 'MISSING_TRAN_BIG_GAP'
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day;

   R_GET_BIG_GAP     C_GET_BIG_GAP%ROWTYPE;

BEGIN
   if I_store_day_seq_no is NULL then
      O_error_message := sql_lib.create_msg('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        I_store_day_seq_no,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---         
   FOR R_GET_BIG_GAP IN C_GET_BIG_GAP
   LOOP
      /* Note: - If the way error message 'MISSING_TRAN_BIG_GAP' from saimptlog_tdup.c batch is changed */
      /* the below will change accordingly in finding the tran_nos and hence should be taken care into  */

      /* find the first occurance '<' in the expression so the position of the first tran_no can be known */
      select instr(R_GET_BIG_GAP.orig_value, '<', 1, 2)
        into L_t_loc1 
        from dual;

      /* find the first occcurance '>' in the expression so the position of the first tran_no can be known */
      select instr(R_GET_BIG_GAP.orig_value, '>', 1, 2)
        into L_t_loc2
        from dual;
      
      /* get the first value */
      select substr(R_GET_BIG_GAP.orig_value, L_t_loc1 + 1, L_t_loc2 - L_t_loc1 - 1)
        into L_first_value
        from dual;

      /* find the second occurance '<' in the expression so the position of the second tran_no can be known */
      select instr(R_GET_BIG_GAP.orig_value, '<', 1, 4)
        into L_t_loc1
        from dual;
        
      /* find the second occcurance '>' in the expression so the position of the second tran_no can be known */
      select instr(R_GET_BIG_GAP.orig_value, '>', 1, 4)
        into L_t_loc2
        from dual;

      /* get the second value */
      select substr(R_GET_BIG_GAP.orig_value, L_t_loc1 + 1, L_t_loc2 - L_t_loc1 - 1)
        into L_second_value
        from dual;

      /* get count of the transactions in the big gap which are not missing anymore */
      select count(*)
        into L_tran_count
        from sa_tran_head
       where tran_no between to_number(L_first_value) and to_number(L_second_value)
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day;

      L_miss_tran := ((L_second_value + 1) - L_first_value);

      /* if all the missing transactions are accounted for, delete the error message */
      if L_tran_count = L_miss_tran then
         delete from sa_error
          where orig_value = R_GET_BIG_GAP.orig_value
            and error_code = 'MISSING_TRAN_BIG_GAP'
            and store_day_seq_no = I_store_day_seq_no
            and store = L_store
            and day = L_day;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_MISS_TRAN;
-------------------------------------------------------------------------------------------------
FUNCTION UPDATE_BALANCE_GROUP(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_new_bal_group_seq_no  IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                              I_tran_seq_no           IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store                 IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                              I_day                   IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)                              
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)            := 'TRAN_ERROR_SQL.UPDATE_BALANCE_GROUP';
   L_store        SA_STORE_DAY.STORE%TYPE := I_store;
   L_day          SA_STORE_DAY.DAY%TYPE   := I_day;   

BEGIN
   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_TRAN_SEQ_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_new_bal_group_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_NEW_BAL_GROUP_SEQ_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---         
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'SA_ERROR',
                    'TRAN_SEQ_NO = '||to_char(I_tran_seq_no));
   update sa_error
      set bal_group_seq_no = I_new_bal_group_seq_no
    where tran_seq_no = I_tran_seq_no
      and store = L_store
      and day = L_day
      and (bal_group_seq_no is NOT NULL or bal_group_seq_no != I_new_bal_group_seq_no);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PROGRAM_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_BALANCE_GROUP;
-------------------------------------------------------------------------------------------------
END TRAN_ERROR_SQL;
/
