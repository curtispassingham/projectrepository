
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE INVC_ITEM_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------
-- Function: INSERT_MATCH_QUEUE
-- Purpose : Inserts the invoice into the invc_match_queue table.
-----------------------------------------------------------------------------------------
FUNCTION INSERT_MATCH_QUEUE(O_error_message IN OUT VARCHAR2,
                            I_invc_id       IN     INVC_HEAD.INVC_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function: DELETE_MATCH_WKSHT
-- Purpose : Deletes records from the invc_match_wksht table that correspond to the record
--           being deleted from the invc_detail table.
-----------------------------------------------------------------------------------------
FUNCTION DELETE_MATCH_WKSHT(O_error_message   IN OUT VARCHAR2,
                            I_invc_id         IN     INVC_HEAD.INVC_ID%TYPE,
                            I_item            IN     ITEM_MASTER.ITEM%TYPE,
                            I_invc_unit_cost  IN     INVC_DETAIL.INVC_UNIT_COST%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function: INSERT_UPDATE_INVC
-- Purpose : This function is used to keep the invc_match_wksht and invc_xref tables
--           N'sync with the invc_detail records.
-----------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_INVC(O_error_message     IN OUT VARCHAR2,
                            I_insert_update_ind IN     VARCHAR2,
                            I_invc_id           IN     INVC_HEAD.INVC_ID%TYPE,
                            I_item              IN     ITEM_MASTER.ITEM%TYPE,
                            I_invc_unit_cost    IN     INVC_DETAIL.INVC_UNIT_COST%TYPE,
                            I_shipment          IN     SHIPMENT.SHIPMENT%TYPE,
                            I_carton            IN     CARTON.CARTON%TYPE,
                            I_seq_no            IN     INVC_MATCH_WKSHT.SEQ_NO%TYPE,
                            I_match_to_cost     IN     INVC_MATCH_WKSHT.MATCH_TO_COST%TYPE,
                            I_match_to_qty      IN     INVC_MATCH_WKSHT.MATCH_TO_QTY%TYPE,
                            I_match_to_seq_no   IN     INVC_MATCH_WKSHT.MATCH_TO_SEQ_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function: CHECK_DUPLICATES
-- Purpose : Checks for the existence of the passed in invc/item/unit cost on the
--           invc_detail table.  It's used to prevent duplicate entries.
-----------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATES(O_error_message     IN OUT VARCHAR2,
                          O_exists            IN OUT BOOLEAN,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_invc_unit_cost    IN     INVC_DETAIL.INVC_UNIT_COST%TYPE,
                          I_invc_id           IN     INVC_HEAD.INVC_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function: CHECK_DUPLICATES_WKSHT
-- Purpose : Checks for the existence of the passed in invc/item/unit cost/shipment/carton
--           on the invc_match_wksht table.  It's used to prevent duplicate entries.
-----------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATES_WKSHT(O_error_message     IN OUT VARCHAR2,
                                O_exists            IN OUT BOOLEAN,
                                I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                I_invc_unit_cost    IN     INVC_DETAIL.INVC_UNIT_COST%TYPE,
                                I_invc_id           IN     INVC_HEAD.INVC_ID%TYPE,
                                I_shipment          IN     SHIPMENT.SHIPMENT%TYPE,
                                I_seq_no            IN     INVC_MATCH_WKSHT.SEQ_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function: MULTIPLE_RCPT_INFO
-- Purpose : Checks to see if multiple receipts exist for an invc/item/cost.  If they do,
--           the receipt details are returned.
-----------------------------------------------------------------------------------------
FUNCTION MULTIPLE_RCPT_INFO(O_error_message   IN OUT VARCHAR2,
                            O_count_rcpts     IN OUT NUMBER,
                            O_total_qty       IN OUT INVC_MATCH_WKSHT.MATCH_TO_QTY%TYPE,
                            O_shipment        IN OUT SHIPMENT.SHIPMENT%TYPE,
                            O_carton          IN OUT CARTON.CARTON%TYPE,
                            O_seq_no          IN OUT INVC_MATCH_WKSHT.SEQ_NO%TYPE,
                            O_match_to_qty    IN OUT INVC_MATCH_WKSHT.MATCH_TO_QTY%TYPE,
                            O_match_to_cost   IN OUT INVC_MATCH_WKSHT.MATCH_TO_COST%TYPE,
                            O_match_to_seq_no IN OUT INVC_MATCH_WKSHT.MATCH_TO_SEQ_NO%TYPE,
                            I_invc_id         IN     INVC_HEAD.INVC_ID%TYPE,
                            I_item            IN     ITEM_MASTER.ITEM%TYPE,
                            I_invc_unit_cost  IN     INVC_MATCH_WKSHT.INVC_UNIT_COST%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function: UPDATE_INVC_QTY
-- Purpose:  A record already exists on invc_detail for the invc_id/item/invc_unit_cost so
--           update the invc_deatil.invc_qty by adding the current record's invc_qty
-------------------------------------------------------------------------------------------
FUNCTION UPDATE_INVC_QTY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_invc_qty         IN OUT   INVC_DETAIL.INVC_QTY%TYPE,
                         I_item             IN       INVC_DETAIL.ITEM%TYPE,
                         I_invc_unit_cost   IN       INVC_DETAIL.INVC_UNIT_COST%TYPE,
                         I_invc_id          IN       INVC_DETAIL.INVC_ID%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------
END INVC_ITEM_SQL;
/
