CREATE OR REPLACE PACKAGE BODY TRANSACTION_VALIDATE_SQL AS
-------------------------------------------------------------------------------------
FUNCTION TRAN_DATE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_valid_date     IN OUT BOOLEAN,
                   I_business_date  IN     SA_STORE_DAY.BUSINESS_DATE%TYPE,
                   I_vdate          IN     PERIOD.VDATE%TYPE,
                   I_day_post_sale  IN     SA_SYSTEM_OPTIONS.DAY_POST_SALE%TYPE)
   RETURN BOOLEAN IS

   L_program                 VARCHAR2(60) :=  'TRANSACTION_VALIDATE_SQL.TRAN_DATE';
   L_days_before_purge       SA_SYSTEM_OPTIONS.DAYS_BEFORE_PURGE%TYPE;
   L_day_post_sale           SA_SYSTEM_OPTIONS.DAY_POST_SALE%TYPE      := I_day_post_sale;
   L_balance_level_ind       SA_SYSTEM_OPTIONS.BALANCE_LEVEL_IND%TYPE;
   L_check_miss_tran_ind     SA_SYSTEM_OPTIONS.CHECK_DUP_MISS_TRAN%TYPE;
   L_max_days_compare_dups   SA_SYSTEM_OPTIONS.MAX_DAYS_COMPARE_DUPS%TYPE;
   L_comp_base_date          SA_SYSTEM_OPTIONS.COMP_BASE_DATE%TYPE;
   L_comp_no_days            SA_SYSTEM_OPTIONS.COMP_NO_DAYS%TYPE;
   L_unit_of_work            SA_SYSTEM_OPTIONS.UNIT_OF_WORK%TYPE;
   L_audit_after_imp_ind     SA_SYSTEM_OPTIONS.AUDIT_AFTER_IMP_IND%TYPE;
   L_fuel_dept               SA_SYSTEM_OPTIONS.FUEL_DEPT%TYPE;
   L_default_chain           SA_SYSTEM_OPTIONS.DEFAULT_CHAIN%TYPE;
   L_close_in_order          SA_SYSTEM_OPTIONS.CLOSE_IN_ORDER%TYPE;
   L_vdate                   PERIOD.VDATE%TYPE   := I_vdate;

BEGIN

   if I_business_date is NOT NULL then
      if L_vdate is null then
         L_vdate := GET_VDATE;
      end if;
      ---
      if L_day_post_sale is NULL then
         if SA_SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                     L_days_before_purge,
                                                     L_day_post_sale,
                                                     L_balance_level_ind,
                                                     L_max_days_compare_dups,
                                                     L_comp_base_date,
                                                     L_comp_no_days,
                                                     L_check_miss_tran_ind,
                                                     L_unit_of_work,
                                                     L_audit_after_imp_ind,
                                                     L_fuel_dept,
                                                     L_default_chain,
                                                     L_close_in_order) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_business_date > L_vdate then
         O_valid_date := FALSE;
      elsif I_business_date < (L_vdate - L_day_post_sale) then
         O_valid_date := FALSE;
      else
         O_valid_date := TRUE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('TRAN_DATE_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TRAN_DATE;
--------------------------------------------------------------------------------------
FUNCTION VOUCHER_STATUS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_status             IN OUT SA_VOUCHER.STATUS%TYPE,
                        O_ass_store          IN OUT SA_VOUCHER.ASS_STORE%TYPE,
                        O_iss_tran_seq_no    IN OUT SA_VOUCHER.ISS_TRAN_SEQ_NO%TYPE,
                        O_iss_item_seq_no    IN OUT SA_VOUCHER.ISS_ITEM_SEQ_NO%TYPE,
                        O_iss_tender_seq_no  IN OUT SA_VOUCHER.ISS_TENDER_SEQ_NO%TYPE,
                        O_red_tran_seq_no    IN OUT SA_VOUCHER.RED_TRAN_SEQ_NO%TYPE,
                        O_red_tender_seq_no  IN OUT SA_VOUCHER.RED_TENDER_SEQ_NO%TYPE,
                        I_voucher_no         IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                        I_voucher_type       IN     SA_VOUCHER.TENDER_TYPE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'TRANSACTION_VALIDATE_SQL.VOUCHER_STATUS';
   L_status            SA_VOUCHER.STATUS%TYPE;

   cursor C_VALIDATE_VOUCHER is
      select status,
             ass_store,
             iss_tran_seq_no,
             iss_item_seq_no,
             iss_tender_seq_no,
             red_tran_seq_no,
             red_tender_seq_no
        from sa_voucher
       where voucher_no     = I_voucher_no
         and tender_type_id = I_voucher_type;

BEGIN
   if I_voucher_no is NOT NULL and I_voucher_type is NOT NULL then
      open C_VALIDATE_VOUCHER;
      fetch C_VALIDATE_VOUCHER into L_status,
                                    O_ass_store,
                                    O_iss_tran_seq_no,
                                    O_iss_item_seq_no,
                                    O_iss_tender_seq_no,
                                    O_red_tran_seq_no,
                                    O_red_tender_seq_no;
         if C_VALIDATE_VOUCHER%NOTFOUND then
            O_status := 'N';
         else
            O_status := L_status;
         end if;
      close C_VALIDATE_VOUCHER;
   else
      O_error_message := SQL_LIB.CREATE_MSG('VOUCHER_NO_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VOUCHER_STATUS;
---------------------------------------------------------------------------------------
FUNCTION VOUCHER(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_issued            IN OUT BOOLEAN,
                 O_issue_amt         IN OUT SA_VOUCHER.ISS_AMT%TYPE,
                 I_voucher_no        IN     SA_VOUCHER.VOUCHER_NO%TYPE,
                 I_voucher_type      IN     SA_VOUCHER.TENDER_TYPE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'TRANSACTION_VALIDATE_SQL.VOUCHER';

   cursor C_CHECK_VOUCHER_STATUS is
      select nvl(iss_amt,0)
        from sa_voucher
       where voucher_no = I_voucher_no
         and tender_type_id = I_voucher_type;

BEGIN

   if I_voucher_no is NOT NULL and I_voucher_type is NOT NULL then
      O_issued := FALSE;
      open C_CHECK_VOUCHER_STATUS;
      fetch C_CHECK_VOUCHER_STATUS into O_issue_amt;
      if C_CHECK_VOUCHER_STATUS%FOUND then
         O_issued := TRUE;
      end if;
      close C_CHECK_VOUCHER_STATUS;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VOUCHER;
---------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_TRANS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tran_seq_no      IN      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_tran_no          IN      SA_TRAN_HEAD.TRAN_NO%TYPE,
                         I_store_day_seq_no IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                         I_register         IN      SA_TRAN_HEAD.REGISTER%TYPE,
                         I_status           IN      SA_TRAN_HEAD.STATUS%TYPE,
                         O_exist_ind        IN OUT  BOOLEAN,
                         I_store            IN      SA_STORE_DAY.STORE%TYPE,
                         I_day              IN      SA_STORE_DAY.DAY%TYPE,
                         I_rtlog_orig_sys   IN      SA_TRAN_HEAD.RTLOG_ORIG_SYS%TYPE)
   RETURN BOOLEAN IS

   L_ind      VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(60) := 'SA_TRANSACTION_VALIDATE_SQL.CHECK_DUP_TRANS';
   L_store    SA_STORE_DAY.STORE%TYPE   := I_store;
   L_day      SA_STORE_DAY.DAY%TYPE     := I_day;

   cursor C_CHECK_EXISTS is
      select 'Y'
        from sa_tran_head h
       where h.store_day_seq_no = I_store_day_seq_no
         and h.store            = L_store
         and h.day              = L_day
         and h.tran_no          = I_tran_no
         and h.tran_seq_no      != I_tran_seq_no
         and h.status           = I_status
         and h.rtlog_orig_sys   = I_rtlog_orig_sys
         and ((h.register       = I_register
              and I_register is NOT NULL)
          or I_register is NULL);

BEGIN
   if I_store_day_seq_no is NOT NULL and I_tran_no is NOT NULL then
      ---
      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_exist_ind := FALSE;
      ---
      open C_CHECK_EXISTS;
      fetch C_CHECK_EXISTS into L_ind;
      close C_CHECK_EXISTS;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if L_ind = 'Y' then
      O_exist_ind := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_DUP_TRANS;
---------------------------------------------------------------------------------------
FUNCTION POS_DECLARED_TOTAL
         (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
          O_valid_total      IN OUT  BOOLEAN,
          O_duplicate        IN OUT  BOOLEAN,
          I_tran_seq_no      IN      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
          I_store_day_seq_no IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
          I_register         IN      SA_TRAN_HEAD.REGISTER%TYPE,
          I_cashier          IN      SA_TRAN_HEAD.REGISTER%TYPE,
          I_total_id         IN      SA_TRAN_HEAD.REF_NO1%TYPE,
          I_store            IN      SA_STORE_DAY.STORE%TYPE,
          I_day              IN      SA_STORE_DAY.DAY%TYPE)
   return BOOLEAN is

   L_program          VARCHAR2(60) := 'SA_TRANSACTION_VALIDATE_SQL.POS_DECLARED_TOTAL';
   L_dup_tran_seq_no  SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE := NULL;
   L_bal_level        SA_TOTAL_HEAD.BAL_LEVEL%TYPE  := NULL;
   L_store            SA_STORE_DAY.STORE%TYPE   := I_store;
   L_day              SA_STORE_DAY.DAY%TYPE     := I_day;

   cursor C_GET_TOTAL_INFO is
      select h1.bal_level
        from sa_total_head h1
       where h1.total_id     = I_total_id
         and h1.total_rev_no = (select max(h2.total_rev_no)
                                  from sa_total_head h2
                                 where h2.total_id = h1.total_id);

   cursor C_DUPLICATE_EXISTS is
      select h.tran_seq_no
        from sa_tran_head h
       where h.store_day_seq_no = I_store_day_seq_no
         and h.store            = L_store
         and h.day              = L_day
         and h.tran_seq_no     != I_tran_seq_no
         and h.tran_type        = 'TOTAL'
         and h.ref_no1          = I_total_id
         and h.status           = 'P'
         and (   L_bal_level = 'S'
              or (    L_bal_level = 'C'
                  and nvl(h.cashier, '-1')  = nvl(I_cashier, '-1'))
              or (    L_bal_level = 'R'
                  and nvl(h.register, '-1') = nvl(I_register, '-1')));

BEGIN
   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_TRAN_SEQ_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_store_day_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_STORE_DAY_SEQ_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_TOTAL_ID',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        I_store_day_seq_no,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TOTAL_INFO',
                    'SA_TOTAL_HEAD',
                    'TOTAL_ID: '||I_total_id);
   open C_GET_TOTAL_INFO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TOTAL_INFO',
                    'SA_TOTAL_HEAD',
                    'TOTAL_ID: '||I_total_id);
   fetch C_GET_TOTAL_INFO into L_bal_level;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TOTAL_INFO',
                    'SA_TOTAL_HEAD',
                    'TOTAL_ID: '||I_total_id);
   close C_GET_TOTAL_INFO;
   ---
   if L_bal_level is NULL then
      O_valid_total := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_TOTAL_ID',
                                             I_total_id,
                                             NULL,
                                             NULL);
      return TRUE;
   else
      O_valid_total := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_DUPLICATE_EXISTS',
                    'SA_TRAN_HEAD',
                    'TOTAL_ID: '||I_total_id);
   open C_DUPLICATE_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_DUPLICATE_EXISTS',
                    'SA_TRAN_HEAD',
                    'TOTAL_ID: '||I_total_id);
   fetch C_DUPLICATE_EXISTS into L_dup_tran_seq_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_DUPLICATE_EXISTS',
                    'SA_TRAN_HEAD',
                    'TOTAL_ID: '||I_total_id);
   close C_DUPLICATE_EXISTS;
   ---
   if L_dup_tran_seq_no is NOT NULL then
      O_duplicate := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('DUP_POS_TOTAL_DECLARATION',
                                            I_total_id,
                                            to_char(L_dup_tran_seq_no),
                                            NULL);
      return TRUE;
   else
      O_duplicate := FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END POS_DECLARED_TOTAL;
---------------------------------------------------------------------------------------
FUNCTION GET_PREV_VOID_ITEM_STATUS
         (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
          O_prev_item_status IN OUT  SA_TRAN_ITEM.ITEM_STATUS%TYPE,
          I_tran_seq_no      IN      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
          I_item_seq_no      IN      SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
          I_store            IN      SA_STORE_DAY.STORE%TYPE,
          I_day              IN      SA_STORE_DAY.DAY%TYPE)
   return BOOLEAN is

   L_program          VARCHAR2(60) := 'SA_TRANSACTION_VALIDATE_SQL.GET_PREV_VOID_ITEM_STATUS';
   L_store            SA_STORE_DAY.STORE%TYPE   := I_store;
   L_day              SA_STORE_DAY.DAY%TYPE     := I_day;

   cursor C_GET_PREV_ITEM_STATUS is
      select t1.item_status
        from sa_tran_item_rev t1
       where t1.tran_seq_no = I_tran_seq_no
         and t1.store       = L_store
         and t1.day         = L_day
         and t1.item_seq_no = I_item_seq_no
         and t1.rev_no      = (select max(t2.rev_no)
                                 from sa_tran_item_rev t2
                                where t2.tran_seq_no = t1.tran_seq_no
                                  and t2.store       = t1.store
                                  and t2.day         = t1.day
                                  and t2.item_seq_no = t1.item_seq_no
                                  and t2.item_status NOT in ('V', 'ERR'));
BEGIN
   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_TRAN_SEQ_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM_SEQ_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_tran_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PREV_ITEM_STATUS',
                    'SA_TRAN_ITEM_REV',
                    'TRAN_SEQ_NO: '||to_char(I_tran_seq_no)||', ITEM_SEQ_NO: '||to_char(I_item_seq_no));
   open C_GET_PREV_ITEM_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PREV_ITEM_STATUS',
                    'SA_TRAN_ITEM_REV',
                    'TRAN_SEQ_NO: '||to_char(I_tran_seq_no)||', ITEM_SEQ_NO: '||to_char(I_item_seq_no));
   fetch C_GET_PREV_ITEM_STATUS into O_prev_item_status;
   if SQL%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PREV_ITEM_STATUS',
                       'SA_TRAN_ITEM_REV',
                       'TRAN_SEQ_NO: '||to_char(I_tran_seq_no)||', ITEM_SEQ_NO: '||to_char(I_item_seq_no));
      close C_GET_PREV_ITEM_STATUS;
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_VOID_NO_BASE_STATUS',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PREV_ITEM_STATUS',
                    'SA_TRAN_ITEM_REV',
                    'TRAN_SEQ_NO: '||to_char(I_tran_seq_no)||', ITEM_SEQ_NO: '||to_char(I_item_seq_no));
   close C_GET_PREV_ITEM_STATUS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_PREV_VOID_ITEM_STATUS;
------------------------------------------------------------------------------
FUNCTION TENDER_ENDING_AMT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid_ending       IN OUT   BOOLEAN,
                           I_tender_amt         IN       SA_TRAN_TENDER.TENDER_AMT%TYPE,
                           I_rounding_rule_id   IN       SA_ROUNDING_RULE_HEAD.ROUNDING_RULE_ID%TYPE)
   RETURN BOOLEAN IS

   L_ending_amt         SA_TRAN_TENDER.TENDER_AMT%TYPE;

   cursor C_VALID_ENDING is
      select 'Y'
        from sa_rounding_rule_detail d
       where d.rounding_rule_id = I_rounding_rule_id
         and d.round_amt        = L_ending_amt
         and rownum             = 1;

   L_valid_ending       VARCHAR2(1);
   L_negative_flag      BOOLEAN := FALSE;
   L_tender_amt         SA_TRAN_TENDER.TENDER_AMT%TYPE;

BEGIN
   if I_tender_amt is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tender_amt',
                                            'TRANSACTION_VALIDATE_SQL.TENDER_ENDING_AMT',
                                            NULL);
      return FALSE;
   end if;

   --- I_rounding_rule_id can be null

   if I_rounding_rule_id is null then
      -- no rules defined
      O_valid_ending := TRUE;
      return TRUE;
   end if;
   ---
   if I_tender_amt < 0 then
      L_negative_flag := TRUE;
      L_tender_amt := I_tender_amt * -1;
   else
      L_negative_flag := FALSE;
      L_tender_amt := I_tender_amt;
   end if;
   ---
   if TRANSACTION_SQL.GET_ENDING_AMT(O_error_message,
                                     L_ending_amt,
                                     L_tender_amt,
                                     I_rounding_rule_id) = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_VALID_ENDING',
                    'SA_ROUNDING_RULE_DETAIL',
                    'rounding_rule_id: '||I_rounding_rule_id||' Tender Amt: '||to_char(I_tender_amt)||' Ending Amt: '|| to_char(L_ending_amt));
   open C_VALID_ENDING;

   SQL_LIB.SET_MARK('FETCH',
                    'C_VALID_ENDING',
                    'SA_ROUNDING_RULE_DETAIL',
                    'rounding_rule_id: '||I_rounding_rule_id||' Tender Amt: '||to_char(I_tender_amt)||' Ending Amt: '|| to_char(L_ending_amt));
   fetch C_VALID_ENDING into L_valid_ending;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_VALID_ENDING',
                    'SA_ROUNDING_RULE_DETAIL',
                    'rounding_rule_id: '||I_rounding_rule_id||' Tender Amt: '||to_char(L_tender_amt)||' Ending Amt: '|| to_char(L_ending_amt));
   close C_VALID_ENDING;

   if L_valid_ending is NULL then
      --- not valid ending
      O_valid_ending := FALSE;
   else
      --- valid ending
      O_valid_ending := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSACTION_VALIDATE_SQL.TENDER_ENDING_AMT',
                                            to_char(SQLCODE));
      return FALSE;

END TENDER_ENDING_AMT;
-------------------------------------------------------------------------------
FUNCTION CHECK_DUP_IGTAX(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists             IN OUT  BOOLEAN,
                         I_store              IN      SA_STORE_DAY.STORE%TYPE,
                         I_day                IN      SA_STORE_DAY.DAY%TYPE,
                         I_tran_seq_no        IN      SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_item_seq_no        IN      SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                         I_igtax_seq_no       IN      SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE,
                         I_igtax_code         IN      SA_TRAN_IGTAX.IGTAX_CODE%TYPE,
                         I_tax_authority      IN      SA_TRAN_IGTAX.TAX_AUTHORITY%TYPE,
                         I_component_item     IN      SA_TRAN_IGTAX.REF_NO21%TYPE)
return BOOLEAN IS

   L_program      VARCHAR2(64) := 'TRANSACTION_VALIDATE_SQL.CHECK_DUP_IGTAX';
   L_dup_fetch    VARCHAR2(1);
   ---
   cursor C_DUP_IGTAX is
      select 'x'
        from sa_tran_igtax sti
       where sti.store         = I_store
         and sti.day           = I_day
         and sti.tran_seq_no   = I_tran_seq_no
         and sti.item_seq_no   = I_item_seq_no
         and sti.igtax_seq_no != I_igtax_seq_no
         and sti.igtax_code    = I_igtax_code
         and sti.tax_authority = I_tax_authority
         and rownum            = 1;

  cursor C_DUP_IGTAX_COMP is
      select 'x'
        from sa_tran_igtax sti
       where sti.store         = I_store
         and sti.day           = I_day
         and sti.tran_seq_no   = I_tran_seq_no
         and sti.item_seq_no   = I_item_seq_no
         and sti.igtax_seq_no != I_igtax_seq_no
         and sti.igtax_code    = I_igtax_code
         and sti.tax_authority = I_tax_authority
         and sti.ref_no21      = I_component_item
         and rownum            = 1;

BEGIN
   O_exists := FALSE;
   ---
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_day is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_day',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tran_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_item_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_igtax_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_igtax_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_igtax_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_igtax_code',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_component_item is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_DUP_IGTAX',
                       'SA_TRAN_IGTAX',
                       'igtax_code: '||I_igtax_code||' tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)||' igtax_seq_no: '|| to_char(I_igtax_seq_no));
      open C_DUP_IGTAX;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_DUP_IGTAX',
                       'SA_TRAN_IGTAX',
                       'igtax_code: '||I_igtax_code||' tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)||' igtax_seq_no: '|| to_char(I_igtax_seq_no));
      fetch C_DUP_IGTAX into L_dup_fetch;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_DUP_IGTAX',
                       'SA_TRAN_IGTAX',
                       'igtax_code: '||I_igtax_code||' tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)||' igtax_seq_no: '|| to_char(I_igtax_seq_no));
      close C_DUP_IGTAX;
      ---
      if L_dup_fetch = 'x' then
         O_exists := TRUE;
      end if;
      ---
      return TRUE;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_DUP_IGTAX_COMP',
                       'SA_TRAN_IGTAX',
                       'igtax_code: '||I_igtax_code||' tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)||' igtax_seq_no: '|| to_char(I_igtax_seq_no));
      open C_DUP_IGTAX_COMP;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_DUP_IGTAX_COMP',
                       'SA_TRAN_IGTAX',
                       'igtax_code: '||I_igtax_code||' tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)||' igtax_seq_no: '|| to_char(I_igtax_seq_no));
      fetch C_DUP_IGTAX_COMP into L_dup_fetch;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_DUP_IGTAX',
                       'SA_TRAN_IGTAX',
                       'igtax_code: '||I_igtax_code||' tran_seq_no: '||to_char(I_tran_seq_no)||' item_seq_no: '|| to_char(I_item_seq_no)||' igtax_seq_no: '|| to_char(I_igtax_seq_no));
      close C_DUP_IGTAX_COMP;
      ---
      if L_dup_fetch = 'x' then
         O_exists := TRUE;
      end if;
      ---
      return TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DUP_IGTAX;
------------------------------------------------------------------------------
END TRANSACTION_VALIDATE_SQL;
/
