
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_FLASH_SALES_SQL AS
-------------------------------------------------------------------
FUNCTION WRITE_FLASH_SALES (O_error_message    IN OUT VARCHAR2,
                            I_store_day_seq_no IN     SA_TOTAL.STORE_DAY_SEQ_NO%TYPE)
   RETURN BOOLEAN IS
   
   L_store                     SA_STORE_DAY.STORE%TYPE;
   L_business_date             SA_STORE_DAY.BUSINESS_DATE%TYPE;
   L_net_sales                 SA_FLASH_SALES.NET_SALES%TYPE := NULL;
   L_net_sales_suspended       SA_FLASH_SALES.NET_SALES_SUSPENDED%TYPE := NULL;
   L_total_id                  SA_TOTAL.TOTAL_ID%TYPE;
   L_code                      SA_REFERENCE.REF_LABEL_CODE%TYPE;
   L_weather_ref_no            SA_REFERENCE.REF_NO%TYPE;
   L_temperature_ref_no        SA_REFERENCE.REF_NO%TYPE;
   L_latest_weather_update     VARCHAR2(20) := '00000000000000000000';
   L_latest_temperature_update VARCHAR2(20) := '00000000000000000000';
   L_update_datetime           VARCHAR2(20);
   L_ref_no1                   SA_TRAN_HEAD.REF_NO1%TYPE := NULL;
   L_ref_no2                   SA_TRAN_HEAD.REF_NO2%TYPE := NULL;
   L_ref_no3                   SA_TRAN_HEAD.REF_NO3%TYPE := NULL;
   L_ref_no4                   SA_TRAN_HEAD.REF_NO4%TYPE := NULL;
   L_weather                   SA_TRAN_HEAD.REF_NO1%TYPE;
   L_temperature               SA_TRAN_HEAD.REF_NO1%TYPE;
   L_day                       SA_STORE_DAY.DAY%TYPE;
  
   cursor C_STORE_AND_DATE is
      select store,
             business_date,
             day 
        from sa_store_day
       where store_day_seq_no = I_store_day_seq_no;

   cursor C_SALES is
      select value
        from v_sa_total_value v, 
             sa_total t
       where t.store_day_seq_no = I_store_day_seq_no
         and t.total_id         = L_total_id
         and t.store            = L_store
         and t.day              = L_day
         and t.total_seq_no     = v.total_seq_no
         and t.store            = v.store
         and t.day              = v.day;
       
   cursor C_REF_NO is
      select ref_no
        from sa_reference
       where ref_label_code = L_code
         and tran_type = 'COND';

   cursor C_WEATHER_AND_TEMPERATURE is
      select to_char(update_datetime,'YYYYMMDDHH24MISS'), 
             ref_no1, 
             ref_no2, 
             ref_no3, 
             ref_no4
        from sa_tran_head
       where store_day_seq_no = I_store_day_seq_no
         and store            = L_store
         and day              = L_day
         and tran_type        = 'COND';

BEGIN
   --- Get the store and business date for given store_day_seq_no.
   SQL_LIB.SET_MARK('OPEN','C_STORE_AND_DATE','SA_STORE_DAY', 'store_day_seq_no: '||to_char(I_store_day_seq_no));
   open C_STORE_AND_DATE;
   SQL_LIB.SET_MARK('FETCH','C_STORE_AND_DATE','SA_STORE_DAY', 'store_day_seq_no: '||to_char(I_store_day_seq_no));
   fetch C_STORE_AND_DATE into L_store, L_business_date, L_day;
   if C_STORE_AND_DATE%NOTFOUND then
      O_error_message := sql_lib.create_msg ('SATOTRUL_NOSTRDAY', I_store_day_seq_no, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE','C_STORE_AND_DATE','SA_STORE_DAY', 'store_day_seq_no: '||to_char(I_store_day_seq_no));
      close C_STORE_AND_DATE;
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE','C_STORE_AND_DATE','SA_STORE_DAY', 'store_day_seq_no: '||to_char(I_store_day_seq_no));
      close C_STORE_AND_DATE;
   end if;

   --- Get net sales.
   L_total_id := 'NETSALES';
   SQL_LIB.SET_MARK('OPEN','C_SALES', 'SA_TOTAL', 'total_id: '||L_total_id||', store_day_seq_no: '||to_char(I_store_day_seq_no));
   open C_SALES;
   SQL_LIB.SET_MARK('FETCH','C_SALES', 'SA_TOTAL', 'total_id: '||L_total_id||', store_day_seq_no: '||to_char(I_store_day_seq_no));
   fetch C_SALES into L_net_sales;
   --- If no net sales are found we can skip the rest of the logic.
   if (C_SALES%NOTFOUND OR (L_net_sales IS NULL))then
      SQL_LIB.SET_MARK('CLOSE','C_SALES', 'SA_TOTAL', 'total_id: '||L_total_id||', store_day_seq_no: '||to_char(I_store_day_seq_no));
      close C_SALES;
      return TRUE;
   else
      SQL_LIB.SET_MARK('CLOSE','C_SALES', 'SA_TOTAL', 'total_id: '||L_total_id||', store_day_seq_no: '||to_char(I_store_day_seq_no));
      close C_SALES;
   end if;

   --- Get net sales suspended.
   L_total_id := 'SNETSALES';
   SQL_LIB.SET_MARK('OPEN','C_SALES', 'SA_TOTAL', 'total_id: '||L_total_id||', store_day_seq_no: '||to_char(I_store_day_seq_no));
   open C_SALES;
   SQL_LIB.SET_MARK('FETCH','C_SALES', 'SA_TOTAL', 'total_id: '||L_total_id||', store_day_seq_no: '||to_char(I_store_day_seq_no));
   fetch C_SALES into L_net_sales_suspended;
   SQL_LIB.SET_MARK('CLOSE','C_SALES', 'SA_TOTAL', 'total_id: '||L_total_id||', store_day_seq_no: '||to_char(I_store_day_seq_no));
   close C_SALES;

   --- Get reference number for updating weather.
   L_code := 'W';
   SQL_LIB.SET_MARK('OPEN','C_REF_NO','SA_REFERENCE', 'ref_label_code: '||L_code);
   open C_REF_NO;
   SQL_LIB.SET_MARK('FETCH','C_REF_NO','SA_REFERENCE', 'ref_label_code: '||L_code);
   fetch C_REF_NO into L_weather_ref_no;
   if C_REF_NO%NOTFOUND then
      O_error_message := sql_lib.create_msg ('REF_NO_NOT_FOUND', L_code, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE','C_REF_NO','SA_REFERENCE', 'ref_label_code: '||L_code);
      close C_REF_NO;
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE','C_REF_NO','SA_REFERENCE', 'ref_label_code: '||L_code);
      close C_REF_NO;
   end if;

   --- Get reference number for updating temperature.
   L_code := 'TM';
   SQL_LIB.SET_MARK('OPEN','C_REF_NO','SA_REFERENCE', 'ref_label_code: '||L_code);
   open C_REF_NO;
   SQL_LIB.SET_MARK('FETCH','C_REF_NO','SA_REFERENCE', 'ref_label_code: '||L_code);
   fetch C_REF_NO into L_temperature_ref_no;
   if C_REF_NO%NOTFOUND then
      O_error_message := sql_lib.create_msg ('REF_NO_NOT_FOUND', L_code, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE','C_REF_NO','SA_REFERENCE', 'ref_label_code: '||L_code);
      close C_REF_NO;
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE','C_REF_NO','SA_REFERENCE', 'ref_label_code: '||L_code);
      close C_REF_NO;
   end if;

   --- Get the most recent weather and temperature updates.
   SQL_LIB.SET_MARK('OPEN','C_WEATHER_AND_TEMPERATURE','SA_TRAN_HEAD', 'store_day_seq_no: '||to_char(I_store_day_seq_no));
   open C_WEATHER_AND_TEMPERATURE;
   loop
      SQL_LIB.SET_MARK('FETCH','C_WEATHER_AND_TEMPERATURE','SA_TRAN_HEAD', 'store_day_seq_no: '||to_char(I_store_day_seq_no));
      fetch C_WEATHER_AND_TEMPERATURE into L_update_datetime, L_ref_no1, L_ref_no2, L_ref_no3, L_ref_no4;
      EXIT when C_WEATHER_AND_TEMPERATURE%NOTFOUND;
      ---
      if L_weather_ref_no = 1 then
         if L_ref_no1 is NOT NULL and L_update_datetime > L_latest_weather_update then
            L_weather := L_ref_no1;
            L_latest_weather_update := L_update_datetime;
         end if;
      elsif L_weather_ref_no = 2 then
         if L_ref_no2 is NOT NULL and L_update_datetime > L_latest_weather_update then
            L_weather := L_ref_no2;
            L_latest_weather_update := L_update_datetime;
         end if;
      elsif L_weather_ref_no = 3 then 
         if L_ref_no3 is NOT NULL and L_update_datetime > L_latest_weather_update then
            L_weather := L_ref_no3;
            L_latest_weather_update := L_update_datetime;
         end if;
      elsif L_weather_ref_no = 4 then 
         if L_ref_no4 is NOT NULL and L_update_datetime > L_latest_weather_update then
            L_weather := L_ref_no4;
            L_latest_weather_update := L_update_datetime;
         end if;
      end if;
      ---
      if L_temperature_ref_no = 1 then
         if L_ref_no1 is NOT NULL and L_update_datetime > L_latest_temperature_update then
            L_temperature := L_ref_no1;
            L_latest_temperature_update := L_update_datetime;
         end if;
      elsif L_temperature_ref_no = 2 then
         if L_ref_no2 is NOT NULL and L_update_datetime > L_latest_temperature_update then
            L_temperature := L_ref_no2;
            L_latest_temperature_update := L_update_datetime;
         end if;
      elsif L_temperature_ref_no = 3 then
         if L_ref_no3 is NOT NULL and L_update_datetime > L_latest_temperature_update then
            L_temperature := L_ref_no3;
            L_latest_temperature_update := L_update_datetime;
         end if;
      elsif L_temperature_ref_no = 4 then
         if L_ref_no4 is NOT NULL and L_update_datetime > L_latest_temperature_update then
            L_temperature := L_ref_no4;
            L_latest_temperature_update := L_update_datetime;
         end if;
      end if;
   end loop;
   SQL_LIB.SET_MARK('CLOSE','C_WEATHER_AND_TEMPERATURE','SA_TRAN_HEAD', 'store_day_seq_no: '||to_char(I_store_day_seq_no));
   close C_WEATHER_AND_TEMPERATURE;

   --- Update the flash sales table with net_sales, net_sales_suspended, and most recent 
   --- weather and temperature updates.
   SQL_LIB.SET_MARK('UPDATE', NULL, 'SA_FLASH_SALES', 'business_date: '||
                    to_char(L_business_date, 'DD-MON-YY')||', store: '||TO_CHAR(L_store));
   update sa_flash_sales
      set net_sales           = L_net_sales,
          net_sales_suspended = L_net_sales_suspended,
          weather             = L_weather,
          temperature         = L_temperature
    where store               = L_store
      and business_date       = L_business_date;
   ---   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SA_TOTAL_VALIDATE_SQL.WRITE_FLASH_SALES',
                                             to_char(SQLCODE));
      return FALSE;
END WRITE_FLASH_SALES;
--------------------------------------------------------------------------------------------
END SA_FLASH_SALES_SQL;
/
