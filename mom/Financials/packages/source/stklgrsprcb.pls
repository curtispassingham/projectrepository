



CREATE OR REPLACE PACKAGE BODY STKLEDGR_PRICING_SQL AS
-------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_items              IN       ITEM_TBL,
                          I_item_attributes    IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                          I_locations          IN       LOC_TBL,
                          I_loc_type           IN       TRAN_DATA.LOC_TYPE%TYPE,
                          I_tran_date          IN       TRAN_DATA.TRAN_DATE%TYPE,
                          I_tran_codes         IN       TRAN_TYPE_TBL,
                          I_units              IN       QTY_TBL,
                          I_total_retails      IN       UNIT_RETAIL_TBL,
                          I_old_unit_retails   IN       UNIT_RETAIL_TBL,
                          I_new_unit_retails   IN       UNIT_RETAIL_TBL,
                          I_pgm_name           IN       TRAN_DATA.PGM_NAME%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA_NO_ADJ_VAT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_items              IN       ITEM_TBL,
                                     I_item_attributes    IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                                     I_locations          IN       LOC_TBL,
                                     I_loc_type           IN       TRAN_DATA.LOC_TYPE%TYPE,
                                     I_tran_date          IN       TRAN_DATA.TRAN_DATE%TYPE,
                                     I_tran_codes         IN       TRAN_TYPE_TBL,
                                     I_units              IN       QTY_TBL,
                                     I_total_retails      IN       UNIT_RETAIL_TBL,
                                     I_old_unit_retails   IN       UNIT_RETAIL_TBL,
                                     I_new_unit_retails   IN       UNIT_RETAIL_TBL,
                                     I_pgm_name           IN       TRAN_DATA.PGM_NAME%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA_ADJ_VAT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_items              IN       ITEM_TBL,
                                  I_item_attributes    IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                                  I_locations          IN       LOC_TBL,
                                  I_loc_type           IN       TRAN_DATA.LOC_TYPE%TYPE,
                                  I_tran_date          IN       TRAN_DATA.TRAN_DATE%TYPE,
                                  I_tran_codes         IN       TRAN_TYPE_TBL,
                                  I_units              IN       QTY_TBL,
                                  I_total_retails      IN       UNIT_RETAIL_TBL,
                                  I_old_unit_retails   IN       UNIT_RETAIL_TBL,
                                  I_new_unit_retails   IN       UNIT_RETAIL_TBL,
                                  I_pgm_name           IN       TRAN_DATA.PGM_NAME%TYPE,
                                  I_incl_excl_vat      IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION INSERT_API_VAT_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_loc_type        IN       TRAN_DATA.LOC_TYPE%TYPE,
                             I_tran_date       IN       TRAN_DATA.TRAN_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-- FUNCTION BODY
-------------------------------------------------------------------------------------------------
FUNCTION BULK_INSERT_TRAN_DATA(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_items              IN       ITEM_TBL,
                               I_item_attributes    IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                               I_locations          IN       LOC_TBL,
                               I_loc_type           IN       TRAN_DATA.LOC_TYPE%TYPE,
                               I_tran_date          IN       TRAN_DATA.TRAN_DATE%TYPE,
                               I_tran_codes         IN       TRAN_TYPE_TBL,
                               I_units              IN       QTY_TBL,
                               I_total_retails      IN       UNIT_RETAIL_TBL,
                               I_old_unit_retails   IN       UNIT_RETAIL_TBL,
                               I_new_unit_retails   IN       UNIT_RETAIL_TBL,
                               I_pgm_name           IN       TRAN_DATA.PGM_NAME%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(60) := 'STKLEDGR_SQL.BULK_INSERT_TRAN_DATA';

BEGIN

   -- basic validation of data
   if I_items is NULL or I_items.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_items',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   if I_locations is NULL or I_locations.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_locations',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   if I_tran_codes is NULL or I_tran_codes.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_codes',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   if I_units is NULL or I_units.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_units',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   if I_total_retails is NULL or I_total_retails.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_total_retails',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   if I_old_unit_retails is NULL or I_old_unit_retails.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_unit_retails',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   if I_new_unit_retails is NULL or I_new_unit_retails.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_unit_retails',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   if I_items.COUNT != I_locations.COUNT or
      I_items.COUNT != I_tran_codes.COUNT or
      I_items.COUNT != I_units.COUNT or
      I_items.COUNT != I_total_retails.COUNT or
      I_items.COUNT != I_old_unit_retails.COUNT or
      I_items.COUNT != I_new_unit_retails.COUNT then
      O_error_message := SQL_LIB.CREATE_MSG('STKLEDGR_INVALID_INPUT',
                                            NULL,
                                            NULL,
                                            NULL);
      RETURN FALSE;
   end if;

   --loc type--
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   if I_loc_type NOT in ('S', 'W') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      RETURN FALSE;
   end if;

   --tran_date--
   if I_tran_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_date',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --prg_name--
   if I_pgm_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pgm_name',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   -- adjust total_retails, old_unit_retails, new_unit_retails for vat
   -- and insert into tran_data table
   if not INSERT_TRAN_DATA(O_error_message,
                           I_items,
                           I_item_attributes,
                           I_locations,
                           I_loc_type,
                           I_tran_date,
                           I_tran_codes,
                           I_units,
                           I_total_retails,
                           I_old_unit_retails,
                           I_new_unit_retails,
                           I_pgm_name) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END BULK_INSERT_TRAN_DATA;
-------------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_items              IN       ITEM_TBL,
                          I_item_attributes    IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                          I_locations          IN       LOC_TBL,
                          I_loc_type           IN       TRAN_DATA.LOC_TYPE%TYPE,
                          I_tran_date          IN       TRAN_DATA.TRAN_DATE%TYPE,
                          I_tran_codes         IN       TRAN_TYPE_TBL,
                          I_units              IN       QTY_TBL,
                          I_total_retails      IN       UNIT_RETAIL_TBL,
                          I_old_unit_retails   IN       UNIT_RETAIL_TBL,
                          I_new_unit_retails   IN       UNIT_RETAIL_TBL,
                          I_pgm_name           IN       TRAN_DATA.PGM_NAME%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'STKLEDGR_SQL.INSERT_TRAN_DATA';

   L_class_vat_ind      CLASS.CLASS_VAT_IND%TYPE;

BEGIN

   -- This function will adjust retails with regard to VAT and write stock ledger.
   -- Three parameters determine if the retails need to be adjusted:
   -- 1) SYSTEM_OPTIONS.VAT_IND indicates if VAT is used for retail in the system;
   -- 2) CLASS.CLASS_VAT_IND indicates if VAT is used for retail at the class level.
   --    It's only applicable when system_options.VAT_IND is on;
   -- 3) SYSTEM_OPTIONS.stkldgr_vat_incl_retl_ind indicates if stock ledger should
   --    include VAT or not. It's only applicable when system_options.VAT_IND is on.

   -- get system options row
   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   -- get the class_vat_ind for the dept/class of items
   if not CLASS_ATTRIB_SQL.GET_CLASS_VAT_IND(O_error_message,
                                             L_class_vat_ind,
                                             I_item_attributes.dept,
                                             I_item_attributes.class) then
      return FALSE;
   end if;

   -- Do not adjust retails when inserting tran_data for the following scenarioes:
   -- 1) If VAT is NOT on in the system;
   -- 2) If retail contains VAT and stock ledger should include vat;
   -- 3) If retail does NOT contain VAT and stock ledger should NOT include vat;

   if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type = 'SALES' or
      (L_class_vat_ind = 'Y' and
       SYSTEM_OPTIONS_SQL.GP_system_options_row.stkldgr_vat_incl_retl_ind = 'Y') or
      (L_class_vat_ind = 'N' and
       SYSTEM_OPTIONS_SQL.GP_system_options_row.stkldgr_vat_incl_retl_ind = 'N') then

      if INSERT_TRAN_DATA_NO_ADJ_VAT(O_error_message,
                                     I_items,
                                     I_item_attributes,
                                     I_locations,
                                     I_loc_type,
                                     I_tran_date,
                                     I_tran_codes,
                                     I_units,
                                     I_total_retails,
                                     I_old_unit_retails,
                                     I_new_unit_retails,
                                     I_pgm_name) = FALSE then
         return FALSE;
      end if;
      return TRUE;
   end if;

   -- The following 2 scenarioes need to adjust retails, in which case VAT_RATES are applied:
   -- 1) If retail contains VAT, but stock ledger should NOT include VAT, take VAT off retail.
   -- 2) If retail does NOT contain VAT, but stock ledger should include VAT, add VAT to retail.

   if L_class_vat_ind = 'Y' and
      SYSTEM_OPTIONS_SQL.GP_system_options_row.stkldgr_vat_incl_retl_ind = 'N' then
      ---
      -- take out vat when inserting tran_data
      if INSERT_TRAN_DATA_ADJ_VAT(O_error_message,
                                     I_items,
                                     I_item_attributes,
                                     I_locations,
                                     I_loc_type,
                                     I_tran_date,
                                     I_tran_codes,
                                     I_units,
                                     I_total_retails,
                                     I_old_unit_retails,
                                     I_new_unit_retails,
                                     I_pgm_name,
                                     'E') = FALSE then  -- exclude vat
         return FALSE;
      end if;
   elsif L_class_vat_ind = 'N' and
      SYSTEM_OPTIONS_SQL.GP_system_options_row.stkldgr_vat_incl_retl_ind = 'Y' then
      ---
      -- add vat when inserting tran_data
      if INSERT_TRAN_DATA_ADJ_VAT(O_error_message,
                                     I_items,
                                     I_item_attributes,
                                     I_locations,
                                     I_loc_type,
                                     I_tran_date,
                                     I_tran_codes,
                                     I_units,
                                     I_total_retails,
                                     I_old_unit_retails,
                                     I_new_unit_retails,
                                     I_pgm_name,
                                     'I') = FALSE then  -- include vat
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END INSERT_TRAN_DATA;
-------------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA_NO_ADJ_VAT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_items              IN       ITEM_TBL,
                                     I_item_attributes    IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                                     I_locations          IN       LOC_TBL,
                                     I_loc_type           IN       TRAN_DATA.LOC_TYPE%TYPE,
                                     I_tran_date          IN       TRAN_DATA.TRAN_DATE%TYPE,
                                     I_tran_codes         IN       TRAN_TYPE_TBL,
                                     I_units              IN       QTY_TBL,
                                     I_total_retails      IN       UNIT_RETAIL_TBL,
                                     I_old_unit_retails   IN       UNIT_RETAIL_TBL,
                                     I_new_unit_retails   IN       UNIT_RETAIL_TBL,
                                     I_pgm_name           IN       TRAN_DATA.PGM_NAME%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'STKLEDGR_PRICING_SQL.INSERT_TRAN_DATA_NO_ADJ_VAT';

   L_cur_rtl_dec        OBJ_NUMERIC_ID_TABLE;

   cursor C_CURRENCY is
      select cur.currency_rtl_dec,
             s.store loc
        from currencies cur,
             store s,
             TABLE(CAST(I_locations AS "LOC_TBL")) loc
       where value(loc)      = s.store
         and s.currency_code = cur.currency_code
      union all
      select cur.currency_rtl_dec,
             wh.wh loc
        from currencies cur,
             wh wh,
             TABLE(CAST(I_locations AS "LOC_TBL")) loc
       where value(loc)       = wh.wh
         and wh.currency_code = cur.currency_code
      union all
      select cur.currency_rtl_dec,
             TO_NUMBER(p.partner_id) loc
        from currencies cur,
             partner p,
             TABLE(CAST(I_locations AS "LOC_TBL")) loc
       where value(loc)       = p.partner_id
         and p.partner_type   = 'E'
         and p.currency_code  = cur.currency_code;

   TYPE LOC_CURR_DEC_TAB is TABLE OF C_CURRENCY%ROWTYPE INDEX BY BINARY_INTEGER;
   L_currency_loc   LOC_CURR_DEC_TAB;

BEGIN

   open C_CURRENCY;
   fetch C_CURRENCY BULK COLLECT into L_currency_loc;
   close C_CURRENCY;

   L_cur_rtl_dec := new OBJ_NUMERIC_ID_TABLE();

   /*Reorder the currency_rtl_dec values fetched in C_CURRENCY cursor,
     make sure that collection is ordered to coincide with the locations in I_locations table.*/
   for i in 1 .. I_locations.last loop
      -- For every location, find the currency_rtl_dec and save it to the L_cur_rtl_dec plsql table,
      -- and ensure it has the same index as the location it refers to in the I_locations.
      for n in 1 .. L_currency_loc.last loop
         if L_currency_loc(n).loc = I_locations(i) then
            L_cur_rtl_dec.extend();
            L_cur_rtl_dec(i) := L_currency_loc(n).currency_rtl_dec;
            exit;
         end if;
      end loop;
   end loop;

   -- This function will insert into tran_data without adjusting retails for VAT.
   forall i in I_items.first .. I_items.last
      insert into tran_data (item,
                             dept,
                             class,
                             subclass,
                             pack_ind,
                             location,
                             loc_type,
                             tran_date,
                             tran_code,
                             adj_code,
                             units,
                             total_cost,
                             total_retail,
                             ref_no_1,
                             ref_no_2,
                             gl_ref_no,
                             old_unit_retail,
                             new_unit_retail,
                             pgm_name,
                             sales_type,
                             vat_rate,
                             av_cost,
                             timestamp)
                     values (I_items(i),
                             I_item_attributes.dept,
                             I_item_attributes.class,
                             I_item_attributes.subclass,
                             I_item_attributes.pack_ind,
                             I_locations(i),
                             I_loc_type,
                             I_tran_date,
                             I_tran_codes(i),
                             NULL,       -- adj_code,
                             I_units(i),
                             NULL,       -- total_cost,
                             ROUND(I_total_retails(i), L_cur_rtl_dec(i)),
                             NULL,       -- ref_no_1,
                             NULL,       -- ref_no_2,
                             NULL,       -- gl_ref_no,
                             I_old_unit_retails(i),
                             I_new_unit_retails(i),
                             I_pgm_name,
                             NULL,       -- sales_type,
                             NULL,       -- vat_rate,
                             NULL,       -- av_cost,
                             sysdate);   -- timestamp
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END INSERT_TRAN_DATA_NO_ADJ_VAT;
-------------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA_ADJ_VAT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_items              IN       ITEM_TBL,
                                  I_item_attributes    IN       ITEM_ATTRIB_SQL.API_ITEM_REC,
                                  I_locations          IN       LOC_TBL,
                                  I_loc_type           IN       TRAN_DATA.LOC_TYPE%TYPE,
                                  I_tran_date          IN       TRAN_DATA.TRAN_DATE%TYPE,
                                  I_tran_codes         IN       TRAN_TYPE_TBL,
                                  I_units              IN       QTY_TBL,
                                  I_total_retails      IN       UNIT_RETAIL_TBL,
                                  I_old_unit_retails   IN       UNIT_RETAIL_TBL,
                                  I_new_unit_retails   IN       UNIT_RETAIL_TBL,
                                  I_pgm_name           IN       TRAN_DATA.PGM_NAME%TYPE,
                                  I_incl_excl_vat      IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'STKLEDGR_PRICING_SQL.INSERT_TRAN_DATA_ADJ_VAT';

   L_vat_type           VARCHAR2(1) := 'R';  -- retail
   L_cur_rtl_dec        OBJ_NUMERIC_ID_TABLE;

   cursor C_CURRENCY is
      select cur.currency_rtl_dec,
             s.store loc
        from currencies cur,
             store s,
             TABLE(CAST(I_locations AS "LOC_TBL")) loc
       where value(loc)      = s.store
         and s.currency_code = cur.currency_code
      union all
      select cur.currency_rtl_dec,
             wh.wh loc
        from currencies cur,
             wh wh,
             TABLE(CAST(I_locations AS "LOC_TBL")) loc
       where value(loc)       = wh.wh
         and wh.currency_code = cur.currency_code
      union all
      select cur.currency_rtl_dec,
             TO_NUMBER(p.partner_id) loc
        from currencies cur,
             partner p,
             TABLE(CAST(I_locations AS "LOC_TBL")) loc
       where value(loc)       = p.partner_id
         and p.partner_type   = 'E'
         and p.currency_code  = cur.currency_code;

   TYPE LOC_CURR_DEC_TAB is TABLE OF C_CURRENCY%ROWTYPE INDEX BY BINARY_INTEGER;
   L_currency_loc   LOC_CURR_DEC_TAB;

BEGIN

   -- This function will adjust VAT when inserting the data into TRAN_DATA.
   -- When I_incl_excl_vat = 'I', vat will be added to retails.
   -- When I_incl_excl_vat = 'E', vat will be taken out of retails.

   -- first get all relevant vat rates to a temp table
   if INSERT_API_VAT_TEMP(O_error_message,
                          I_loc_type,
                          I_tran_date) = FALSE then
      return FALSE;
   end if;

   -- Get the decimal information for the location's currency code
   open C_CURRENCY;
   fetch C_CURRENCY BULK COLLECT into L_currency_loc;
   close C_CURRENCY;

   L_cur_rtl_dec := new OBJ_NUMERIC_ID_TABLE();

   /*Reorder the currency_rtl_dec values fetched in C_CURRENCY cursor,
     make sure that collection is ordered to coincide with the locations in I_locations table.*/
   for i in 1 .. I_locations.last loop
      -- For every location, find the currency_rtl_dec and save it to the L_cur_rtl_dec plsql table,
      -- and ensure it has the same index as the location it refers to in the I_locations.
      for n in 1 .. L_currency_loc.last loop
         if L_currency_loc(n).loc = I_locations(i) then
            L_cur_rtl_dec.extend();
            L_cur_rtl_dec(i) := L_currency_loc(n).currency_rtl_dec;
            exit;
         end if;
      end loop;
   end loop;

   -- insert into tran data
   if I_incl_excl_vat = 'I' then
      forall i in I_items.first .. I_items.last
         insert into tran_data (item,
                                dept,
                                class,
                                subclass,
                                pack_ind,
                                location,
                                loc_type,
                                tran_date,
                                tran_code,
                                adj_code,
                                units,
                                total_cost,
                                total_retail,
                                ref_no_1,
                                ref_no_2,
                                gl_ref_no,
                                old_unit_retail,
                                new_unit_retail,
                                pgm_name,
                                sales_type,
                                vat_rate,
                                av_cost,
                                timestamp)
                         select I_items(i),
                                I_item_attributes.dept,
                                I_item_attributes.class,
                                I_item_attributes.subclass,
                                I_item_attributes.pack_ind,
                                I_locations(i),
                                I_loc_type,
                                I_tran_date,
                                I_tran_codes(i),
                                NULL,       -- adj_code,
                                I_units(i),
                                NULL,       -- total_cost,
                                ROUND(I_total_retails(i)*(1+vi.vat_rate/100),  L_cur_rtl_dec(i)),  -- add vat to retail
                                NULL,       -- ref_no_1,
                                NULL,       -- ref_no_2,
                                NULL,       -- gl_ref_no,
                                I_old_unit_retails(i)*(1+vi.vat_rate/100), -- add vat to retail
                                I_new_unit_retails(i)*(1+vi.vat_rate/100), -- add vat to retail
                                I_pgm_name,
                                NULL,       -- sales_type,
                                NULL,       -- vat_rate,
                                NULL,       -- av_cost,
                                sysdate     -- timestamp
                           from api_vat_temp vi
                          where vi.loc = I_locations(i)
                            and vi.item = I_items(i);
   else  -- I_incl_excl_vat = 'E'
      forall i in I_items.first .. I_items.last
         insert into tran_data (item,
                                dept,
                                class,
                                subclass,
                                pack_ind,
                                location,
                                loc_type,
                                tran_date,
                                tran_code,
                                adj_code,
                                units,
                                total_cost,
                                total_retail,
                                ref_no_1,
                                ref_no_2,
                                gl_ref_no,
                                old_unit_retail,
                                new_unit_retail,
                                pgm_name,
                                sales_type,
                                vat_rate,
                                av_cost,
                                timestamp)
                         select I_items(i),
                                I_item_attributes.dept,
                                I_item_attributes.class,
                                I_item_attributes.subclass,
                                I_item_attributes.pack_ind,
                                I_locations(i),
                                I_loc_type,
                                I_tran_date,
                                I_tran_codes(i),
                                NULL,       -- adj_code,
                                I_units(i),
                                NULL,       -- total_cost,
                                ROUND(I_total_retails(i)/(1+vi.vat_rate/100),  L_cur_rtl_dec(i)),  -- take vat out of retail
                                NULL,       -- ref_no_1,
                                NULL,       -- ref_no_2,
                                NULL,       -- gl_ref_no,
                                I_old_unit_retails(i)/(1+vi.vat_rate/100), -- take vat out of retail
                                I_new_unit_retails(i)/(1+vi.vat_rate/100), -- take vat out of retail
                                I_pgm_name,
                                NULL,       -- sales_type,
                                NULL,       -- vat_rate,
                                NULL,       -- av_cost,
                                sysdate     -- timestamp
                           from api_vat_temp vi
                          where vi.loc = I_locations(i)
                            and vi.item = I_items(i);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_TRAN_DATA_ADJ_VAT;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_API_VAT_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_loc_type        IN       TRAN_DATA.LOC_TYPE%TYPE,
                             I_tran_date       IN       TRAN_DATA.TRAN_DATE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'STKLEDGR_SQL.INSERT_API_VAT_TEMP';
   L_vat_type           VARCHAR2(1)  := 'R';  -- retail
   L_unique_items       ITEM_TBL;

   L_tax_info_tbl   OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   L_tax_info_rec   OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();

   cursor C_ITEMS is
      select item
        from api_item_temp;

BEGIN

   -- Assumption: a unique list of items and locations we are dealing with
   -- are already in API_ITEM_TEMP and API_LOC_TEMP at this point

   -- get unique items into L_unique_items for use in the subsequent queries
   open C_ITEMS;
   fetch C_ITEMS bulk collect into L_unique_items;
   close C_ITEMS;

   -- populate an API_VAT_TEMP table for all the item/locations we are dealing with
   -- should all be in L_unique_items and api_loc_temp already
   -- If vat_rate is not found, write a record with 0 vat_rate.

   -- depending on loc_type, join with store or wh table
   if I_loc_type = 'S' then
      L_tax_info_tbl.EXTEND;
      L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
      ---
      if TAX_SQL.GET_TAX_INFO(O_error_message,
                              L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
      ---
      forall i in L_unique_items.first .. L_unique_items.last
         insert into api_vat_temp (item, loc, vat_rate)
            select L_unique_items(i),
                   alt.loc,
                   vi.tax_rate
              from TABLE(CAST(L_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vi,
                   store s,
                   api_loc_temp alt
             where s.store = alt.loc
               and s.vat_region = vi.from_tax_region
               and vi.item = L_unique_items(i)
               and vi.cost_retail_ind in (L_vat_type, 'B')
               and vi.active_date = (select max(active_date)
                                       from TABLE(CAST(L_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vi2
                                      where vi2.item = L_unique_items(i)
                                        and vi2.from_tax_region = vi.from_tax_region
                                        and vi2.cost_retail_ind = vi.cost_retail_ind
                                        and vi2.active_date <= I_tran_date);

      forall i in L_unique_items.first .. L_unique_items.last
         insert into api_vat_temp (item, loc, vat_rate)
            select L_unique_items(i),
                   alt.loc,
                   0
              from api_loc_temp alt
             where not exists (select 'x'
                                 from TABLE(CAST(L_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vi,
                                      store s
                                where s.vat_region = vi.from_tax_region
                                  and s.store = alt.loc
                                  and vi.item = L_unique_items(i)
                                  and vi.cost_retail_ind in (L_vat_type, 'B')
                                  and vi.active_date <= I_tran_date
                                  and rownum=1);
   else -- loc_type = 'W'
      L_tax_info_tbl.EXTEND;
      L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
      ---
      if TAX_SQL.GET_TAX_INFO(O_error_message,
                              L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
      ---
      forall i in L_unique_items.first .. L_unique_items.last
         insert into api_vat_temp (item, loc, vat_rate)
            select L_unique_items(i),
                   alt.loc,
                   vi.tax_rate
              from TABLE(CAST(L_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vi,
                   wh,
                   api_loc_temp alt
             where wh.wh = alt.loc
               and wh.vat_region = vi.from_tax_region
               and vi.item = L_unique_items(i)
               and vi.cost_retail_ind in (L_vat_type, 'B')
               and vi.active_date = (select max(active_date)
                                       from TABLE(CAST(L_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vi2
                                      where vi2.item = L_unique_items(i)
                                        and vi2.from_tax_region = vi.from_tax_region
                                        and vi2.cost_retail_ind = vi.cost_retail_ind
                                        and vi2.active_date <= I_tran_date);

      forall i in L_unique_items.first .. L_unique_items.last
         insert into api_vat_temp (item, loc, vat_rate)
            select L_unique_items(i),
                   alt.loc,
                   0
              from api_loc_temp alt
              where not exists (select 'x'
                                 from TABLE(CAST(L_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vi,
                                      wh
                                where wh.vat_region = vi.from_tax_region
                                  and wh.wh = alt.loc
                                  and vi.item = L_unique_items(i)
                                  and vi.cost_retail_ind in (L_vat_type, 'B')
                                  and vi.active_date <= I_tran_date
                                  and rownum=1);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_API_VAT_TEMP;
-------------------------------------------------------------------------------------------------
END STKLEDGR_PRICING_SQL;
/
