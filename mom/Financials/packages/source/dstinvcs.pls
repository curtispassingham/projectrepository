
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIRECT_STORE_INVOICE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
TYPE ITEM_INVC_REC IS RECORD
   (item	  ORDSKU.ITEM%TYPE,
    qty_received  SHIPSKU.QTY_RECEIVED%TYPE,
    unit_cost     SHIPSKU.UNIT_COST%TYPE,
    status        INVC_DETAIL.STATUS%TYPE);
-----------------------------------------------------------------------------------------
TYPE ITEM_INVC_TBL is TABLE of ITEM_INVC_REC;
-------------------------------------------------------------------------------
-- Function:    CREATE_INVOICE
-- Purpose:     Create the invoice header record for the purchase order.
--              Will be called from dsdorder.fmb. If PO unit cost matches
--              with invoice unit cost, then invoice status is set as 'M'atched,
--              else is set as 'U'nmatched in the invoice header table.
-- Calls:       SQL_LIB.GET_USER_NAME,INVC_SQL.NEXT_INVC_ID, CHECK_INVC_DETAIL
-- Called From: dsdupld.pc, quordent.fmb, dsdorder.fmb
-------------------------------------------------------------------------------
FUNCTION CREATE_INVOICE(O_error_message         IN OUT VARCHAR2,
                        IO_invc_id              IN OUT INVC_HEAD.INVC_ID%TYPE,
                        I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                        I_supplier              IN     SUPS.SUPPLIER%TYPE,
                        I_vdate                 IN     PERIOD.VDATE%TYPE,
                        I_total_cost            IN     INVC_HEAD.TOTAL_MERCH_COST%TYPE,
                        I_total_qty             IN     INVC_HEAD.TOTAL_QTY%TYPE,
                        I_store                 IN     STORE.STORE%TYPE,
                        I_paid_ind              IN     INVC_HEAD.PAID_IND%TYPE,
                        I_ext_ref_no            IN     INVC_HEAD.EXT_REF_NO%TYPE,
                        I_proof_of_delivery_no  IN     INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                        I_payment_ref_no	IN     INVC_HEAD.PAYMENT_REF_NO%TYPE,
                        I_payment_date          IN     INVC_HEAD.PAYMENT_DATE%TYPE,
                        I_invc_status           IN     INVC_DETAIL.STATUS%TYPE              DEFAULT NULL,
                        I_item_invc_details     IN     ITEM_INVC_TBL                        DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function:    INVOICE_ITEM
-- Purpose:     Create the invoice item records after the invoice header record
--              has been created. It will write out a record on the invc_detail
--              table. For each line item, if PO unit cost matches with 
--              invoice unit cost, then invoice status is set as 'M'atched,
--              else is set as 'U'nmatched
-- Calls:       None
-- Called From: Internal
-------------------------------------------------------------------------------
FUNCTION INVOICE_ITEM(O_error_message     IN OUT VARCHAR2,
                      I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                      I_invc_id           IN     INVC_HEAD.INVC_ID%TYPE,
                      I_store             IN     STORE.STORE%TYPE,
                      I_item_invc_details IN     ITEM_INVC_TBL           DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function:    CHECK_INVC_DETAIL
-- Purpose:     Checks to see if the invoice has items associated with it.
--              If there are, then invoice can not be updated.
-- Called From: Internal
-------------------------------------------------------------------------------
FUNCTION CHECK_INVC_DETAIL(O_error_message  IN OUT VARCHAR2,
                           O_exists         IN OUT BOOLEAN,
                           I_invc_id        IN     INVC_DETAIL.INVC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function:    CHECK_INVC_DUPS
-- Purpose:     Checks to see if the input parameters are same as an existing invoice.
--              If there are, then the invoice already exists.
-- Called From: Internal, saexpim.pc, invoice.fmb
-------------------------------------------------------------------------------
FUNCTION CHECK_INVC_DUPS(O_error_message        IN OUT VARCHAR2,
                         O_invc_id              IN OUT INVC_HEAD.INVC_ID%TYPE,
                         I_invc_id              IN     INVC_HEAD.INVC_ID%TYPE,
                         I_supplier        	IN     INVC_HEAD.SUPPLIER%TYPE,
                         I_partner_type         IN     INVC_HEAD.PARTNER_TYPE%TYPE,
                         I_partner_id           IN     INVC_HEAD.PARTNER_ID%TYPE,
                         I_invc_date            IN     INVC_HEAD.INVC_DATE%TYPE,
                         I_ext_ref_no           IN     INVC_HEAD.EXT_REF_NO%TYPE,
                         I_proof_of_delivery_no IN     INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                         I_payment_ref_no       IN     INVC_HEAD.PAYMENT_REF_NO%TYPE,
                         I_payment_date         IN     INVC_HEAD.PAYMENT_DATE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--   Name:      INVC_NON_MERCH_TEMP_EXIST
--   Purpose    This function will execute a query with the input parameters in the 'where' 
--              clause to determine if the input non-merchandise
--              code, invc_id combination exists on the INVC_NON_MERCH_TEMP table. The function 
--              will output a boolean indicator O_exists with a value= FALSE if the input 
--              non-merchandise code, invoice_id combination does not exist on the 
--              invc_non_merch_temp table or a TRUE if it does.  
-- Called From: quordent.fmb
---------------------------------------------------------------------------------------------
FUNCTION INVC_NON_MERCH_TEMP_EXIST(O_error_message   IN OUT VARCHAR2,
                                   O_exists          IN OUT BOOLEAN,
                                   I_non_merch_code  IN     NON_MERCH_CODE_HEAD.NON_MERCH_CODE%TYPE,
                                   I_invc_id         IN     INVC_NON_MERCH.INVC_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function:    CREATE_INVOICE_WRP
-- Purpose:     This is a wrapper function for CREATE_INVOICE that passes a database 
--              type object instead of a PL/SQL type as an input parameter to be 
--              called from Java wrappers.
-------------------------------------------------------------------------------
FUNCTION CREATE_INVOICE_WRP(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_invc_id                IN OUT   INVC_HEAD.INVC_ID%TYPE,
                            I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                            I_supplier               IN       SUPS.SUPPLIER%TYPE,
                            I_vdate                  IN       PERIOD.VDATE%TYPE,
                            I_total_cost             IN       INVC_HEAD.TOTAL_MERCH_COST%TYPE,
                            I_total_qty              IN       INVC_HEAD.TOTAL_QTY%TYPE,
                            I_store                  IN       STORE.STORE%TYPE,
                            I_paid_ind               IN       INVC_HEAD.PAID_IND%TYPE,
                            I_ext_ref_no             IN       INVC_HEAD.EXT_REF_NO%TYPE,
                            I_proof_of_delivery_no   IN       INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                            I_payment_ref_no         IN       INVC_HEAD.PAYMENT_REF_NO%TYPE,
                            I_payment_date           IN       INVC_HEAD.PAYMENT_DATE%TYPE,
                            I_invc_status            IN       INVC_DETAIL.STATUS%TYPE              DEFAULT NULL,
                            I_item_invc_details      IN       WRP_ITEM_INVC_TBL                    DEFAULT NULL)
RETURN INTEGER;
END;
/
