
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_VR_SQL AS
--------------------------------------------------------------------------------

function get_signature (                                                                                 
                        v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                        n_vr_parm_seq_no   in     sa_vr_parms.vr_parm_seq_no%TYPE) 
         return varchar2 
is                                                                                     
                                                                                                    
                                                                                                    
    cursor c_get_linkage_data is                                                                    
       select svl.link_to_parm_seq_no                                                               
         from sa_vr_links svl                                                         
        where svl.vr_id          = v_vr_id                                          
          and svl.vr_rev_no      = n_vr_rev_no                                       
          and svl.vr_parm_seq_no = n_vr_parm_seq_no                                      
     order by link_to_parm_id ;                                                                    
                                                                                                    
   n_link_seq_no sa_vr_links.link_to_parm_seq_no%TYPE ;                                 
   v_signature   VARCHAR2(2000) := '' ;                                                            

begin                                                                                               
                                                                                                    
   open c_get_linkage_data ;                                                                        
   loop                                                                                             
      fetch c_get_linkage_data into n_link_seq_no ;                                                 
      exit when c_get_linkage_data%NOTFOUND or n_link_seq_no is null ;                                                       

      v_signature := v_signature || ltrim(to_char(n_link_seq_no, 'FM000')) || 'x' ;                         
   end loop ;                                                                                       
   close c_get_linkage_data ;                                                                       
   return v_signature ;                                                                             
                                                                                                    
exception

   when others then
      if c_get_linkage_data%isopen then
         close c_get_linkage_data ;
      end if ;
end ;                                                                                                
--------------------------------------------------------------------------------

function available_realm (                                                                                 
                          v_error_message   out    varchar2,
                          b_avail           out    boolean,
                          v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                          n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                          n_vr_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE,  
                          v_realm_id        in     sa_realm.realm_id%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.AVAILABLE_REALM';
   v_temp     varchar2(1) ;
   b_valid    boolean ;

   cursor c_valid_realm is
      select null
        from sa_realm r,
             sa_realm_type rt
       where r.realm_id = v_realm_id
         and rt.realm_type_id = r.realm_type_id
         and rt.retrieve_values_ind = 'Y'
         and r.active_ind = 'Y'
         and rt.special_use_ind = 'N'
         and r.logical_realm_id is NULL ;

   cursor c_unavail is  /*Are there any key parms that cannot be linked?*/
      select null
        from sa_parm p1,
             sa_parm_type pt1
       where p1.realm_id = v_realm_id
         and p1.realm_key_ind = 'Y'
         and p1.active_ind = 'Y'
         and pt1.active_ind = 'Y'
         and p1.parm_type_id = pt1.parm_type_id
         and not exists
            (select 'x'
               from sa_vr_realm svr,
                    sa_parm p2
              where svr.vr_id = v_vr_id
                and svr.vr_rev_no = n_vr_rev_no
                and (  n_vr_realm_seq_no is null or
                       svr.vr_realm_seq_no < n_vr_realm_seq_no  )
                and p2.realm_id = svr.realm_id
                and p2.parm_type_id in
                   (select pt2.parm_type_id
                      from sa_parm_type pt2
                     where pt2.active_ind = 'Y'
                start with pt2.parm_type_id = pt1.parm_type_id
                connect by pt2.parent_parm_type_id = prior pt2.parm_type_id)) ;

   cursor c_duplicate_realm is /*Is this realm already used? Duplicates are not allowed unless
                                 the function building process gets changed to reference the alias
                                 instead of the realm physical name.*/
      select null
        from sa_vr_realm svr
       where svr.vr_id             = v_vr_id
         and svr.vr_rev_no         = n_vr_rev_no
         and svr.realm_id          = v_realm_id
         and (svr.vr_realm_seq_no != n_vr_realm_seq_no or n_vr_realm_seq_no is NULL); 
  
begin
   open c_valid_realm ;
   fetch c_valid_realm into v_temp ;
   b_valid := c_valid_realm%found ;
   close c_valid_realm ;
   if not b_valid then
      v_error_message := 'Invalid sa_realm ' || v_realm_id ;
      return false ;
   end if ;

   if v_vr_id is null or n_vr_realm_seq_no = 1 then
      b_avail := true;
   else
      open c_unavail ;
      fetch c_unavail into v_temp ;
      b_avail := c_unavail%notfound ;
      close c_unavail ;
      if b_avail then
         open c_duplicate_realm ;
         fetch c_duplicate_realm into v_temp ;
         b_avail := c_duplicate_realm%notfound ;
         close c_duplicate_realm ;
      end if;
   end if;

   return true ;

exception

   when others then
      if c_valid_realm%isopen then
         close c_valid_realm ;
      end if ;
      if c_unavail%isopen then
         close c_unavail ;
      end if ;
      if c_duplicate_realm%isopen then
         close c_duplicate_realm ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
end ;
--------------------------------------------------------------------------------

function find_parm (                                                                                 
                        v_error_message   out    varchar2,
                        n_vr_realm_seq_no out    SA_VR_realm.vr_realm_seq_no%TYPE,
                        v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,
                        v_parm_name       in      sa_parm.display_name%type) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FIND_PARM' ;
   v_temp     varchar2(1) ;
   b_valid    boolean ;

   cursor c_realm is
      select svr.vr_realm_seq_no
        from sa_vr_realm svr,
             sa_parm p
       where svr.vr_id = v_vr_id                                          
         and svr.vr_rev_no = n_vr_rev_no                                       
         and p.realm_id = svr.realm_id
         and p.display_name = v_parm_name
    order by svr.vr_realm_seq_no ;

begin

   open c_realm ;
   fetch c_realm into n_vr_realm_seq_no ;
   if c_realm%notfound then
      n_vr_realm_seq_no := 0 ;
   end if ;
   close c_realm ;

   return true ;

exception

   when others then
      if c_realm%isopen then
         close c_realm ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
end ;

--------------------------------------------------------------------------------

function find_realm_id (                                                                                 
                        v_error_message   out    varchar2,
                        n_vr_realm_seq_no out    SA_VR_realm.vr_realm_seq_no%TYPE,
                        v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,
                        v_realm_id        in     sa_realm.realm_id%type) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FIND_REALM_ID';
   v_temp     varchar2(1) ;
   b_valid    boolean ;

   cursor c_realm is
      select svr.vr_realm_seq_no
        from sa_vr_realm svr
       where svr.vr_id = v_vr_id                                          
         and svr.vr_rev_no = n_vr_rev_no                                       
         and svr.realm_id = v_realm_id
    order by svr.vr_realm_seq_no ;

begin

   open c_realm ;
   fetch c_realm into n_vr_realm_seq_no ;
   if c_realm%notfound then
      n_vr_realm_seq_no := 0 ;
   end if ;
   close c_realm ;

   return true ;

exception

   when others then
      if c_realm%isopen then
         close c_realm ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
end ;
/*
--------------------------------------------------------------------------------

FUNCTION CHECK_DRIVING_REALMS (
                               v_error_message   out    varchar2,
                               n_vr_realm_seq_no out    SA_VR_realm.vr_realm_seq_no%TYPE,
                               v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                               n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE) 
         return boolean 
is
   v_program         varchar2(50)  := 'SA_VR_SQL.CHECK_DRIVING_REALMS';
   b_unique          boolean ;
   v_realm_name      sa_realm.physical_name%type ;
   v_realm_id        sa_realm.realm_id%type ;
   v_realm_alias     SA_VR_realm.realm_alias%type ;
   n_temp_seq_no     SA_VR_realm.vr_realm_seq_no%type ;
BEGIN

   v_realm_name := 'SA_TRAN_HEAD' ;
   for i in 1 .. 2 loop
      if not sa_vr_sql.get_active_realm_id (v_error_message,
                                            b_unique,
                                            v_realm_id,
                                            v_realm_name) then
         return false ;
      end if ;
      if not b_unique then
         v_error_message := 'Non-unique realm_id for ' || v_realm_name ;
         return false ;
      end if ;
      if not sa_vr_sql.find_realm_id (v_error_message,
                                      n_temp_seq_no,
                                      v_vr_id,
                                      n_vr_rev_no,
                                      v_realm_id) then
         return false ;
      end if ;
      exit when n_temp_seq_no > 0 ;
      v_realm_name := 'SA_TOTAL' ;
   end loop ;
   n_vr_realm_seq_no := n_temp_seq_no ;
   
EXCEPTION
   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
END;
*/
-----------------------------------------------------------------------

function insert_realm (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,
                       v_realm_id        in     sa_realm.realm_id%TYPE,
                       v_realm_name      in     sa_realm.display_name%TYPE,
                       v_outer_join_ind  in     SA_VR_realm.outer_join_ind%TYPE) 
         return boolean 
is

   v_program         varchar2(50)  := 'SA_VR_SQL.INSERT_REALM';
   v_realm_alias     SA_VR_realm.realm_alias%type ;
   n_vr_realm_seq_no SA_VR_realm.vr_realm_seq_no%type ;

begin

   if not sa_vr_sql.next_vr_realm_seq_no (v_error_message,
                                          n_vr_realm_seq_no,
                                          v_vr_id,
                                          n_vr_rev_no) then
      return false ;
   end if ;
   if not sa_vr_sql.make_unique_realm_alias (v_error_message,
                                             v_realm_alias,
                                             v_vr_id,
                                             n_vr_rev_no,
                                             v_realm_name) then
      return false ;
   end if ;
   insert 
     into sa_vr_realm
        ( vr_id,
          vr_rev_no,
          realm_id,
          signature,
          outer_join_ind,
          realm_alias,
          vr_realm_seq_no )
   values
        ( v_vr_id,
          n_vr_rev_no,
          v_realm_id,
          null,
          v_outer_join_ind,
          v_realm_alias,
          n_vr_realm_seq_no ) ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
end ;
/*
-----------------------------------------------------------------------

function insert_rollup_parms (                                                                                 
                              v_error_message   out    varchar2,
                              v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                              n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE) 
         return boolean 
is

   v_program         varchar2(50)  := 'SA_VR_SQL.INSERT_ROLLUP_PARMS' ;
   v_dummy           varchar2(1) ;
   b_found           boolean ;
   b_unique          boolean ;
   v_realm_name      sa_realm.physical_name%type ;
   v_realm_id        sa_realm.realm_id%type ;
   v_parm_name        sa_parm.physical_name%type ;
   v_parm_id          sa_parm.parm_id%type ;
   n_vr_realm_seq_no SA_VR_realm.vr_realm_seq_no%type ;
   n_vr_parm_seq_no  sa_vr_parms.vr_parm_seq_no%type ;

   cursor c_parm_exists (n_seq_no sa_vr_parms.vr_parm_seq_no%type) is
      select null
        from sa_vr_parms svp
       where svp.vr_id          = v_vr_id
         and svp.vr_rev_no      = n_vr_rev_no
         and svp.vr_parm_seq_no = n_seq_no ;

begin

   v_parm_name := 'STORE_DAY_SEQ_NO' ;
   v_realm_name := 'SA_STORE_DAY' ;
   for i in 1 .. 2 loop
      open c_parm_exists (i) ;
      fetch c_parm_exists into v_dummy ;
      b_found := c_parm_exists%found ;
      close c_parm_exists ;
      if not b_found then
         if not sa_vr_sql.get_active_realm_id (v_error_message,
                                               b_unique,
                                               v_realm_id,
                                               v_realm_name) then
            return false ;
         end if ;
         if not b_unique then
            v_error_message := 'Non-unique realm_id for ' || v_realm_name ;
            return false ;
         end if ;
         if not sa_vr_sql.get_active_parm_id (v_error_message,
                                              b_unique,
                                              v_parm_id,
                                              v_realm_id,
                                              v_parm_name) then
            return false ;
         end if ;
         if not b_unique then
            v_error_message := 'Non-unique parm_id for ' || v_parm_name ;
            return false ;
         end if ;
         if not sa_vr_sql.find_realm_id (v_error_message,
                                         n_vr_realm_seq_no,
                                         v_vr_id,
                                         n_vr_rev_no,
                                         v_realm_id) then
            return false ;
         end if ;
         if n_vr_realm_seq_no = 0 then
            v_error_message := 'Required sa_realm ' || v_realm_name || ' not found' ;
            return false ;
         end if ;
         n_vr_parm_seq_no := i ;
         if not sa_vr_sql.insert_link_parm (v_error_message, 
                                            n_vr_parm_seq_no,
                                            v_vr_id,
                                            n_vr_rev_no,
                                            n_vr_realm_seq_no,
                                            v_parm_id) then
            return false ;
         end if ;
         if n_vr_parm_seq_no != i then
            v_error_message := 'Error inserting sa_parm ' || v_parm_name || ': wrong sequence number' ;
            return false ;
         end if ;
      end if ;
      v_parm_name := 'BAL_GROUP_SEQ_NO' ;
      v_realm_name := 'SA_BALANCE_GROUP' ;
   end loop ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
end ;
*/
--------------------------------------------------------------------------------

function get_active_realm_id (                                                                                 
                              v_error_message   out    varchar2,
                              b_unique          out    boolean,
                              v_realm_id        out    sa_realm.realm_id%type,
                              v_display_name    in     sa_realm.display_name%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.GET_ACTIVE_REALM_ID' ;
   v_temp     sa_realm.realm_id%type ;

   cursor c_realm is
      select r.realm_id
        from sa_realm r,
             sa_realm_type rt
       where rt.realm_type_id = r.realm_type_id
         and rt.retrieve_values_ind = 'Y'
         and r.active_ind = 'Y'
         and rt.special_use_ind = 'N'
         and r.logical_realm_id is NULL 
         and r.display_name = v_display_name ;


begin

   open c_realm ;
   fetch c_realm into v_realm_id ;
   if c_realm%notfound then
      close c_realm ;
      v_error_message := 'Invalid sa_realm name ' || v_display_name ;
      return false ;
   end if ;
   fetch c_realm into v_temp ;
   b_unique := c_realm%notfound ;
   close c_realm ;

   return true ;

exception

   when others then
      if c_realm%isopen then
         close c_realm ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
end ;
--------------------------------------------------------------------------------

function get_active_parm_id (                                                                                 
                             v_error_message   out    varchar2,
                             b_unique          out    boolean,
                             v_parm_id         out     sa_parm.parm_id%type,
                             v_realm_id        in     sa_realm.realm_id%type,
                             v_display_name    in      sa_parm.display_name%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.GET_ACTIVE_PARM_ID' ;
   v_temp      sa_parm.parm_id%type ;

   cursor c_parm is
      select p.realm_id
        from sa_parm p,
             sa_parm_type pt
       where pt.parm_type_id = p.parm_type_id
         and p.realm_id = v_realm_id
         and p.active_ind = 'Y'
         and pt.active_ind = 'Y'
         and p.display_name = v_display_name ;


begin

   open c_parm ;
   fetch c_parm into v_parm_id ;
   if c_parm%notfound then
      close c_parm ;
      v_error_message := 'Invalid sa_parm name ' || v_display_name ;
      return false ;
   end if ;
   fetch c_parm into v_temp ;
   b_unique := c_parm%notfound ;
   close c_parm ;

   return true ;

exception

   when others then
      if c_parm%isopen then
         close c_parm ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
end ;
--------------------------------------------------------------------------------

function new_link_info (                                                                                 
                        v_error_message    out    varchar2,
                        b_found            out    boolean,
                        n_vr_realm_seq_no  out    SA_VR_realm.vr_realm_seq_no%TYPE,
                        v_realm_alias      out    SA_VR_realm.realm_alias%TYPE,
                        v_parm_name        out     sa_parm.display_name%TYPE,
                        v_parm_id          out     sa_parm.parm_id%TYPE,
                        v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                        n_max_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE,  
                        v_vr_parm_id       in      sa_parm.parm_id%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.NEW_LINK_INFO' ;
   n_temp_vr_realm_seq_no      SA_VR_realm.vr_realm_seq_no%TYPE ;
   v_temp_realm_alias          SA_VR_realm.realm_alias%TYPE ;
   v_temp_parm_name             sa_parm.display_name%TYPE ;
   v_temp_parm_id               sa_parm.parm_id%TYPE ;

   cursor c_link_info is
      select svr.realm_alias,
             svr.vr_realm_seq_no,
             p2.parm_id,
             p2.display_name
        from sa_vr_realm svr,
             sa_parm p1,
             sa_parm p2
       where svr.vr_id = v_vr_id
         and svr.vr_rev_no = n_vr_rev_no
         and svr.vr_realm_seq_no < n_max_realm_seq_no
         and p1.parm_id = v_vr_parm_id
         and p2.realm_id = svr.realm_id
         and upper (p1.display_name) = upper (p2.display_name)
         and p2.parm_type_id in
            (select pt.parm_type_id
               from sa_parm_type pt
              where pt.active_ind = 'Y'
         start with pt.parm_type_id = p1.parm_type_id
         connect by pt.parent_parm_type_id = prior pt.parm_type_id)
    order by svr.vr_realm_seq_no ;

begin

   open c_link_info ;
   fetch c_link_info into v_temp_realm_alias,
                          n_temp_vr_realm_seq_no,
                          v_temp_parm_id,
                          v_temp_parm_name ;
   b_found := c_link_info%found ;
   if c_link_info%found then
      v_realm_alias := v_temp_realm_alias ;
      n_vr_realm_seq_no := n_temp_vr_realm_seq_no ;
      v_parm_id := v_temp_parm_id ;
      v_parm_name := v_temp_parm_name ;
   end if;
   close c_link_info ;

   return true ;

exception

   when others then
      if c_link_info%isopen then
         close c_link_info ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function get_link_info (                                                                                 
                        v_error_message   out    varchar2,
                        n_vr_realm_seq_no out    SA_VR_realm.vr_realm_seq_no%TYPE,
                        v_realm_alias     out    SA_VR_realm.realm_alias%TYPE,
                        v_parm_name       out     sa_parm.display_name%TYPE,
                        v_parm_id         out     sa_parm.parm_id%TYPE,
                        v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                        n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.GET_LINK_INFO' ;
   b_found    boolean ;

   cursor c_link_info is
      select svr.realm_alias,
             svr.vr_realm_seq_no,
             p.parm_id,
             p.display_name
        from sa_vr_parms svp,
             sa_vr_realm svr,
             sa_parm p
       where svp.vr_id          = v_vr_id
         and svp.vr_rev_no      = n_vr_rev_no
         and svp.vr_parm_seq_no = n_vr_parm_seq_no
         and p.parm_id          = svp.parm_id
         and svr.vr_id          = svp.vr_id
         and svr.vr_rev_no      = svp.vr_rev_no
         and svr.realm_id       = p.realm_id
         and nvl(svr.signature,'Y') = 
                 nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no),'Y');

begin
   open c_link_info ;
   fetch c_link_info into v_realm_alias,
                          n_vr_realm_seq_no,
                          v_parm_id,
                          v_parm_name ;
   b_found := c_link_info%found;
   close c_link_info ;
   if not b_found then
      v_error_message := SQL_LIB.CREATE_MSG('error retrieving link',
                                            null,
                                            null,
                                            null);
      return false ;

   end if ;
   return true ;

exception

   when others then
      if c_link_info%isopen then
         close c_link_info ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function find_link_parm (                                                                                 
                         v_error_message   out    varchar2,
                         n_vr_parm_seq_no  in out sa_vr_parms.vr_parm_seq_no%TYPE,
                         v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                         n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                         n_vr_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE,
                         v_parm_id         in      sa_parm.parm_id%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FIND_LINK_PARM' ;

   cursor c_link_parm is
      select svp.vr_parm_seq_no
        from sa_vr_parms svp,
             sa_vr_realm svr
       where svr.vr_id           = v_vr_id
         and svr.vr_rev_no       = n_vr_rev_no
         and svr.vr_realm_seq_no = n_vr_realm_seq_no 
         and svp.vr_id           = svr.vr_id
         and svp.vr_rev_no       = svr.vr_rev_no
         and svp.parm_id         = v_parm_id
         and nvl(svr.signature,'Y') = 
                 nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no),'Y')
         and (  svp.vr_parm_seq_no = n_vr_parm_seq_no or
                exists
           (select null
              from sa_vr_realm svr2
             where svr2.vr_id     = svr.vr_id
               and svr2.vr_rev_no = svr.vr_rev_no
               and instr (svr2.signature, to_char(svp.vr_parm_seq_no, 'FM000') || 'x') > 0)  ) ;

begin

   open c_link_parm ;
   fetch c_link_parm into n_vr_parm_seq_no ;
   if c_link_parm%notfound then
      n_vr_parm_seq_no := 0;
   end if;
   close c_link_parm ;
   return true ;

exception

   when others then
      if c_link_parm%isopen then
         close c_link_parm ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function insert_link_parm (                                                                                 
                           v_error_message   out    varchar2,
                           n_vr_parm_seq_no  in out sa_vr_parms.vr_parm_seq_no%TYPE,
                           v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                           n_vr_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE,
                           v_parm_id         in      sa_parm.parm_id%TYPE) 
         return boolean 
is

   b_found     boolean;
   v_program   varchar2(50)  := 'SA_VR_SQL.INSERT_LINK_PARM' ;
   v_realm_id  sa_realm.realm_id%TYPE;
   v_signature SA_VR_realm.signature%TYPE;
   v_temp_alias sa_vr_parms.vr_parm_alias%TYPE;
   v_vr_parm_alias sa_vr_parms.vr_parm_alias%TYPE;
   v_outer_join_ind SA_VR_realm.outer_join_ind%TYPE;

   cursor c_realm_info is 
      select svr.realm_id,
             svr.signature,
             svr.outer_join_ind
        from sa_vr_realm svr
       where svr.vr_id           = v_vr_id
         and svr.vr_rev_no       = n_vr_rev_no
         and svr.vr_realm_seq_no = n_vr_realm_seq_no ;

   cursor c_parm_name is
      select p.display_name
        from sa_parm p
       where p.parm_id = v_parm_id ;

begin

   open c_realm_info ;
   fetch c_realm_info into v_realm_id,
                           v_signature,
                           v_outer_join_ind ;
   b_found := c_realm_info%found;
   close c_realm_info ;
   if not b_found then
      v_error_message := SQL_LIB.CREATE_MSG('error finding sa_realm info',
                                            null,
                                            null,
                                            null);
      return false ;
   end if;
   open c_parm_name ;
   fetch c_parm_name into v_temp_alias ;
   b_found := c_parm_name%found;
   close c_parm_name ;
   if not b_found then
      v_error_message := SQL_LIB.CREATE_MSG('error finding sa_parm info',
                                            null,
                                            null,
                                            null);
      return false ;
   end if;
   v_temp_alias := 'xxxx' || rtrim(substrb (v_temp_alias, 1, 116));
   if not sa_vr_sql.make_unique_parm_alias (v_error_message,
                                            v_vr_parm_alias,
                                            v_vr_id,
                                            n_vr_rev_no,
                                            v_temp_alias) then
      return false ;
   end if ;
   if n_vr_parm_seq_no is null then
      if not sa_vr_sql.next_vr_parm_seq_no (v_error_message,
                                            n_vr_parm_seq_no,
                                            v_vr_id,
                                            n_vr_rev_no) then
         return false ;
      end if ;
   end if ;
   insert into sa_vr_parms (vr_id, 
                            vr_rev_no, 
                            vr_parm_seq_no, 
                            parm_id, 
                            vr_parm_alias)
                    values (v_vr_id, 
                            n_vr_rev_no, 
                            n_vr_parm_seq_no, 
                            v_parm_id, 
                            v_vr_parm_alias);

   if v_signature is not null then
      if not sa_vr_sql.insert_links (v_error_message,
                                     v_vr_id,
                                     n_vr_rev_no,
                                     v_signature,
                                     v_realm_id,
                                     n_vr_parm_seq_no,
                                     v_outer_join_ind) then
         return false ;
      end if;
   end if;
   return true ;

exception

   when others then
      if c_realm_info%isopen then
         close c_realm_info ;
      end if ;
      if c_parm_name%isopen then
         close c_parm_name ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function dependants (                                                                                 
                     v_error_message    out    varchar2,
                     b_parm_dependants  out    boolean,
                     b_realm_dependants out    boolean,
                     v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                     n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                     n_vr_realm_seq_no  in     SA_VR_realm.vr_realm_seq_no%TYPE)
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.DEPENDANTS' ;
   v_dummy     varchar2(1) ;

   cursor c_parms_exist is
      select null
        from sa_vr_parms svp,
             sa_vr_realm svr,
             sa_parm p
       where svp.vr_id           = v_vr_id
         and svp.vr_rev_no       = n_vr_rev_no
         and svr.vr_id           = svp.vr_id
         and svr.vr_rev_no       = svp.vr_rev_no
         and svr.vr_realm_seq_no = n_vr_realm_seq_no
         and p.parm_id           = svp.parm_id
         and p.realm_id          = svr.realm_id
         and nvl(svr.signature,'Y') = 
                 nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no),'Y')
         and not exists 
        (select null
           from sa_vr_realm svr2
          where svr2.vr_id           = svp.vr_id
            and svr2.vr_rev_no       = svp.vr_rev_no
            and instr (svr2.signature, to_char(svp.vr_parm_seq_no, 'FM000') || 'x') > 0) ;

   cursor c_realms_exist is
      select null
        from sa_vr_parms svp,
             sa_vr_realm svr,
             sa_parm p
       where svp.vr_id           = v_vr_id
         and svp.vr_rev_no       = n_vr_rev_no
         and svr.vr_id           = svp.vr_id
         and svr.vr_rev_no       = svp.vr_rev_no
         and svr.vr_realm_seq_no = n_vr_realm_seq_no
         and p.parm_id           = svp.parm_id
         and p.realm_id          = svr.realm_id
         and nvl(svr.signature,'Y') = 
                 nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no),'Y')
         and exists 
        (select null
           from sa_vr_realm svr2
          where svr2.vr_id           = svp.vr_id
            and svr2.vr_rev_no       = svp.vr_rev_no
            and instr (svr2.signature, to_char(svp.vr_parm_seq_no, 'FM000') || 'x') > 0) ;
begin
   open c_parms_exist ;
   fetch c_parms_exist into v_dummy ;
   b_parm_dependants := c_parms_exist%found ;
   close c_parms_exist ;

   open c_realms_exist ;
   fetch c_realms_exist into v_dummy ;
   b_realm_dependants := c_realms_exist%found ;
   close c_realms_exist ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_realm (                                                                                 
                       v_error_message    out    varchar2,
                       v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                       n_vr_realm_seq_no  in     SA_VR_realm.vr_realm_seq_no%TYPE)
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.REMOVE_REALM' ;
   v_signature SA_VR_realm.signature%type ;

begin

   if not sa_vr_sql.fetch_signature (v_error_message,
                                     v_signature,
                                     v_vr_id,
                                     n_vr_rev_no,
                                     n_vr_realm_seq_no) then
      return false ;
   end if ;

   delete sa_vr_realm svr
    where svr.vr_id           = v_vr_id
      and svr.vr_rev_no       = n_vr_rev_no
      and svr.vr_realm_seq_no = n_vr_realm_seq_no ;

   if not sa_vr_sql.remove_parms (v_error_message,
                                  v_vr_id,
                                  n_vr_rev_no,
                                  v_signature) then
      return false ;
   end if ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_linked_realm (                                                                                 
                              v_error_message    out    varchar2,
                              v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                              n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE,  
                              n_vr_realm_seq_no  in     SA_VR_realm.vr_realm_seq_no%TYPE)
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.REMOVE_LINKED_REALM' ;
   b_avail     boolean ;
   cursor c_parms is
      select svp.vr_parm_seq_no
        from sa_vr_parms svp,
             sa_vr_realm svr,
             sa_parm p
       where svp.vr_id           = v_vr_id
         and svp.vr_rev_no       = n_vr_rev_no
         and p.parm_id           = svp.parm_id
         and svr.vr_id           = svp.vr_id
         and svr.vr_rev_no       = svp.vr_rev_no
         and svr.realm_id        = p.realm_id
         and nvl(svr.signature,'Y') = 
                 nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no),'Y')
         and svr.vr_realm_seq_no = n_vr_realm_seq_no ;

   cursor c_realms is
      select svr.vr_realm_seq_no,
             svr.realm_id
        from sa_vr_realm svr
       where svr.vr_id           = v_vr_id
         and svr.vr_rev_no       = n_vr_rev_no
         and svr.vr_realm_seq_no > n_vr_realm_seq_no
    order by svr.vr_realm_seq_no ;

begin

   for rec in c_parms loop
      if not sa_vr_sql.remove_parm (v_error_message,
                                    v_vr_id,
                                    n_vr_rev_no,
                                    rec.vr_parm_seq_no) then
         return false ;
      end if ;
   end loop;

   if not sa_vr_sql.remove_realm (v_error_message,
                                  v_vr_id,
                                  n_vr_rev_no,
                                  n_vr_realm_seq_no) then
      return false ;
   end if ;

   for rec in c_realms loop
      if not available_realm (v_error_message,
                              b_avail,
                              v_vr_id,
                              n_vr_rev_no,
                              rec.vr_realm_seq_no,
                              rec.realm_id) then
         return false ;
      end if ;
      if not b_avail then
         if not remove_realm (v_error_message,
                              v_vr_id,
                              n_vr_rev_no,
                              rec.vr_realm_seq_no) then
            return false ;
         end if ;
      end if ;
   end loop;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_all_realms (                                                                                 
                            v_error_message    out    varchar2,
                            v_vr_id            in     sa_vr_head.vr_id%TYPE,     
                            n_vr_rev_no        in     sa_vr_head.vr_rev_no%TYPE)
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.REMOVE_ALL_REALMS' ;

   cursor c_realms is
      select svr.vr_realm_seq_no,
             svr.realm_id
        from sa_vr_realm svr
       where svr.vr_id           = v_vr_id
         and svr.vr_rev_no       = n_vr_rev_no
    order by svr.vr_realm_seq_no DESC;

begin

   for rec in c_realms loop
      if not sa_vr_sql.remove_linked_realm (v_error_message,
                                            v_vr_id,
                                            n_vr_rev_no,
                                            rec.vr_realm_seq_no) then
         return false ;
      end if ;
   end loop;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function update_realm (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                       n_vr_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE,
                       v_new_signature   in     SA_VR_realm.signature%TYPE)
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.UPDATE_REALM' ;

   cursor c_parms is
      select svp.vr_parm_seq_no,
             p.realm_id,
             svr.outer_join_ind
        from sa_vr_parms svp,
             sa_vr_realm svr,
             sa_parm p
       where svp.vr_id           = v_vr_id
         and svp.vr_rev_no       = n_vr_rev_no
         and svr.vr_id           = svp.vr_id
         and svr.vr_rev_no       = svp.vr_rev_no
         and svr.vr_realm_seq_no = n_vr_realm_seq_no
         and p.parm_id           = svp.parm_id
         and p.realm_id          = svr.realm_id
         and svr.signature       = sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no);
begin
   for rec in c_parms loop
      delete sa_vr_links svl
       where svl.vr_id          = v_vr_id
         and svl.vr_rev_no      = n_vr_rev_no
         and svl.vr_parm_seq_no = rec.vr_parm_seq_no ;

      if not sa_vr_sql.insert_links (v_error_message,
                                     v_vr_id,
                                     n_vr_rev_no,
                                     v_new_signature,
                                     rec.realm_id,
                                     rec.vr_parm_seq_no,
                                     rec.outer_join_ind) then
         return false ;
      end if;
   end loop;
   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function update_signature (                                                                                 
                           v_error_message   out    varchar2,
                           v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                           n_vr_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE,
                           v_new_signature   in     SA_VR_realm.signature%TYPE)
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.UPDATE_SIGNATURE' ;

begin
   update sa_vr_realm svr
      set signature           = v_new_signature
    where svr.vr_id           = v_vr_id
      and svr.vr_rev_no       = n_vr_rev_no
      and svr.vr_realm_seq_no = n_vr_realm_seq_no;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function fetch_signature (                                                                                 
                          v_error_message   out    varchar2,
                          v_signature       out    SA_VR_realm.signature%TYPE,
                          v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                          n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                          n_vr_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE)
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.FETCH_SIGNATURE' ;

   b_found    boolean;

   cursor c_signature is
      select svr.signature
        from sa_vr_realm svr
       where svr.vr_id           = v_vr_id
         and svr.vr_rev_no       = n_vr_rev_no
         and svr.vr_realm_seq_no = n_vr_realm_seq_no ;

begin

   open c_signature ;
   fetch c_signature into v_signature ;
   b_found := c_signature%found ;
   close c_signature ;
   if not b_found then
      v_error_message := SQL_LIB.CREATE_MSG('error finding signature',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
   end if;
   return true ;

exception

   when others then
      if c_signature%isopen then
         close c_signature ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_parms (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                       v_signature       in     SA_VR_realm.signature%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.REMOVE_PARMS' ;
   n_sig_index      number := 1 ;
   n_vr_parm_seq_no sa_vr_parms.vr_parm_seq_no%TYPE ;
   v_dummy          varchar2(1) ;

   cursor c_used is
      select null
        from sa_vr_realm svr
       where svr.vr_id          = v_vr_id
         and svr.vr_rev_no      = n_vr_rev_no
         and instr (svr.signature, substr(v_signature, n_sig_index, 4)) > 0 ;


begin

   while n_sig_index <= length(v_signature) loop
      open c_used ;
      fetch c_used into v_dummy ;
      if c_used%notfound then
         n_vr_parm_seq_no := to_number (substr(v_signature, n_sig_index, 3)) ;

         if not sa_vr_sql.remove_links (v_error_message,
                                        v_vr_id,
                                        n_vr_rev_no,
                                        n_vr_parm_seq_no) then
            return false ;
         end if;

         delete sa_vr_parms svp
          where svp.vr_id          = v_vr_id
            and svp.vr_rev_no      = n_vr_rev_no
            and svp.vr_parm_seq_no = n_vr_parm_seq_no;
      end if;
      close c_used ;
      n_sig_index := n_sig_index + 4;
   end loop;
   return true ;

exception

   when others then
      if c_used%isopen then
         close c_used ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function check_signature (                                                                                 
                          v_error_message   out    varchar2,
                          b_valid           out    boolean,
                          v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                          n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                          n_vr_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE,  
                          v_signature       in     SA_VR_realm.signature%TYPE,
                          v_realm_id        in     SA_VR_realm.realm_id%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.CHECK_SIGNATURE' ;
   v_error_local    varchar2(2000) := null ;
   n_sig_index      number := 1 ;
   n_vr_parm_seq_no sa_vr_parms.vr_parm_seq_no%TYPE ;
   v_dummy          varchar2(1) ;
   b_found          boolean ;

   cursor c_exists (v_parm_type_id1 varchar2, n_realm_seq1 number, n_parm_seq2 number) is
      select null
        from sa_vr_realm svr, sa_parm p, sa_vr_parms svp
       where svp.vr_id = v_vr_id
         and svp.vr_rev_no = n_vr_rev_no
         and svp.vr_parm_seq_no = n_parm_seq2
         and p.parm_id = svp.parm_id
         and svr.vr_id = svp.vr_id
         and svr.vr_rev_no = svp.vr_rev_no
         and svr.realm_id = p.realm_id
         and nvl(svr.signature,'Y') = 
                 nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no),'Y')
         and svr.vr_realm_seq_no < n_realm_seq1
         and p.parm_type_id in
        (select pt.parm_type_id
           from sa_parm_type pt
          where pt.active_ind = 'Y'
     start with pt.parm_type_id = v_parm_type_id1
     connect by pt.parent_parm_type_id = prior pt.parm_type_id) ;

   cursor c_parm_info is
      select p.parm_type_id
        from sa_parm p,
             sa_parm_type pt
       where pt.parm_type_id = p.parm_type_id
         and p.realm_id = v_realm_id
         and p.realm_key_ind = 'Y'
         and p.active_ind = 'Y'
         and pt.active_ind = 'Y'
    order by p.parm_id ;
    
begin

   b_valid := true ;

   if n_vr_realm_seq_no = 1 then
      if v_signature is not null then
         v_error_message := 'Driving sa_realm signature not null' ;
         return false ;
      else
         return true ;
      end if ;
   end if ;
   for rec in c_parm_info loop
      if n_sig_index + 3 > nvl(length(v_signature), 0) then
         b_valid := false ;
         v_error_local := nvl (v_error_local, 'Signature too short for sa_realm #' || n_vr_realm_seq_no) ;
         v_error_message := v_error_local ;
         return true ;
      else
         if substr (v_signature, n_sig_index + 3, 1) != 'x' then
            b_valid := false ;
            v_error_local := nvl (v_error_local, 'Invalid character in signature for sa_realm #' || 
                                                 n_vr_realm_seq_no) ;
         end if;
         begin
            n_vr_parm_seq_no := to_number (substr(v_signature, n_sig_index, 3)) ;
         exception
            when others then
               v_error_message := 'Non-numeric sa_parm in signature for sa_realm #' || n_vr_realm_seq_no ;
               return false ;
         end ;
         open c_exists (rec.parm_type_id, n_vr_realm_seq_no, n_vr_parm_seq_no) ;
         fetch c_exists into v_dummy ;
         b_found := c_exists%found ;
         close c_exists ;
         if not b_found then
            v_error_message := 'Invalid sa_parm ' || n_vr_parm_seq_no || 
                               ' in signature for sa_realm #' || n_vr_realm_seq_no ;
            return false ;
         end if;
    	   n_sig_index := n_sig_index + 4 ;
      end if ;
   end loop ;

   if n_sig_index <= length(v_signature) then
      b_valid := false ;
      v_error_local := nvl (v_error_local, 'Signature too long for sa_realm #' || n_vr_realm_seq_no) ;
   end if;

   v_error_message := v_error_local ;
   return true ;

exception

   when others then
      if c_exists%isopen then
         close c_exists ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function check_realms (                                                                                 
                       v_error_message   out    varchar2,
                       b_valid           out    boolean,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE)
         return boolean 
is

   v_program         varchar2(50)  := 'SA_VR_SQL.CHECK_REALMS' ;
   b_avail           boolean ;
   v_dummy           varchar2(1) ;
   v_error_local     varchar2(2000) := null ;
   v_error_sig       varchar2(2000) := null ;
   n_vr_realm_seq_no SA_VR_realm.vr_realm_seq_no%type ;
   n_vr_parm_seq_no1 sa_vr_parms.vr_parm_seq_no%type ;
   n_vr_parm_seq_no2 sa_vr_parms.vr_parm_seq_no%type ;

   cursor c_bad_parms is
      select svp.vr_parm_seq_no
        from sa_vr_parms svp
       where svp.vr_id            = v_vr_id
         and svp.vr_rev_no        = n_vr_rev_no
         and not exists  (
         select null
           from sa_parm p,
                sa_parm_type pt
          where p.parm_id = svp.parm_id
            and p.parm_type_id = pt.parm_type_id
            and p.active_ind = 'Y'
            and pt.active_ind = 'Y'  )
    order by svp.vr_parm_seq_no ;

   cursor c_driving_realm (v_realm_id varchar2) is
      select null
        from sa_vr_head svh
       where svh.vr_id            = v_vr_id
         and svh.vr_rev_no        = n_vr_rev_no
         and svh.driving_realm_id = v_realm_id ;

   cursor c_realms is
      select svr.vr_realm_seq_no,
             svr.signature,
             svr.realm_id
        from sa_vr_realm svr
       where svr.vr_id          = v_vr_id
         and svr.vr_rev_no      = n_vr_rev_no
    order by svr.vr_realm_seq_no ;

   cursor c_parm_alias is
      select svp.vr_parm_seq_no
        from sa_vr_parms svp, sa_parm p
       where svp.vr_id = v_vr_id
         and svp.vr_rev_no = n_vr_rev_no
         and p.parm_id = svp.parm_id
         and (  (  svp.vr_parm_alias not like 'Xxxx%'
               and exists (
               select null
                 from sa_vr_realm svr
                where svr.vr_id = svp.vr_id
                  and svr.vr_rev_no = svp.vr_rev_no
                  and instr (svr.signature, to_char(svp.vr_parm_seq_no, 'FM000') || 'x') > 0  )  )
             or (  svp.vr_parm_alias like 'Xxxx%'
               and not exists (
               select null
                 from sa_vr_realm svr
                where svr.vr_id = svp.vr_id
                  and svr.vr_rev_no = svp.vr_rev_no
                  and instr (svr.signature, to_char(svp.vr_parm_seq_no, 'FM000') || 'x') > 0  )  )  ) ;

   cursor c_duplicate is
      select svp1.vr_parm_seq_no,
             svp2.vr_parm_seq_no
        from sa_vr_parms svp1, sa_vr_parms svp2
       where svp1.vr_id = v_vr_id
         and svp1.vr_rev_no = n_vr_rev_no
         and svp2.vr_id = svp1.vr_id
         and svp2.vr_rev_no = svp1.vr_rev_no
         and svp2.parm_id = svp1.parm_id
         and nvl(sa_vr_sql.get_signature (svp1.vr_id, svp1.vr_rev_no, svp1.vr_parm_seq_no),'Y') = 
                 nvl(sa_vr_sql.get_signature (svp2.vr_id, svp2.vr_rev_no, svp2.vr_parm_seq_no),'Y')
         and svp2.vr_parm_seq_no != svp1.vr_parm_seq_no
         and svp1.vr_parm_alias like 'Xxxx%'
         and svp2.vr_parm_alias like 'Xxxx%' ;

   cursor c_missing_realms is
      select svp.vr_parm_seq_no
        from sa_vr_parms svp,
             sa_parm p
       where svp.vr_id          = v_vr_id
         and svp.vr_rev_no      = n_vr_rev_no
         and p.parm_id          = svp.parm_id
         and not exists  (
         select null
           from sa_vr_realm svr
          where svr.vr_id          = svp.vr_id
            and svr.vr_rev_no      = svp.vr_rev_no
            and svr.realm_id       = p.realm_id
            and nvl(svr.signature,'Y') = 
                   nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no), 'Y')  ) ;

   cursor c_outer_join is
      select svr.vr_realm_seq_no,
             svl.vr_parm_seq_no,
             svl.link_to_parm_seq_no
        from sa_vr_realm svr, sa_parm p, sa_vr_parms svp, sa_vr_links svl
       where svl.vr_id = v_vr_id
         and svl.vr_rev_no = n_vr_rev_no
         and svp.vr_id = svl.vr_id
         and svp.vr_rev_no = svl.vr_rev_no
         and svp.vr_parm_seq_no = svl.vr_parm_seq_no
         and p.parm_id = svp.parm_id
         and svr.vr_id = svp.vr_id
         and svr.vr_rev_no = svp.vr_rev_no
         and svr.realm_id = p.realm_id
         and nvl(svr.signature,'Y') = 
                 nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no),'Y')
         and svr.outer_join_ind != svl.outer_join_ind ;

begin

   b_valid := true ;

   open c_bad_parms ;
   fetch c_bad_parms into n_vr_parm_seq_no1 ;
   b_avail := c_bad_parms%found ;
   close c_bad_parms ;
   if b_avail then
      v_error_message := 'Invalid sa_parm #' || n_vr_parm_seq_no1 ;
      return false ;
   end if ;

   open c_missing_realms ;
   fetch c_missing_realms into n_vr_parm_seq_no1 ;
   b_avail := c_missing_realms%found ;
   close c_missing_realms ;
   if b_avail then
      v_error_message := 'no sa_realm exist for sa_parm #' || n_vr_parm_seq_no1 ;
      return false ;
   end if ;

   for rec in c_realms loop
      if rec.vr_realm_seq_no = 1 then
         open c_driving_realm (rec.realm_id) ;
         fetch c_driving_realm into v_dummy ;
         b_avail := c_driving_realm%found ;
         close c_driving_realm ;
         if not b_avail then
            v_error_message := 'realm #1 not registered as driving realm' ;
            return false ;
         end if ;
      end if ;
      if not available_realm(v_error_message,
                             b_avail,
                             v_vr_id,
                             n_vr_rev_no,
                             rec.vr_realm_seq_no,
                             rec.realm_id) then
         return false ;
      end if;
      if not b_avail then
         v_error_message := 'unavailable sa_realm #' || rec.vr_realm_seq_no ;
         return false ;
      end if ;
      if not sa_vr_sql.check_signature(v_error_sig,
                                       b_avail,
                                       v_vr_id,
                                       n_vr_rev_no,
                                       rec.vr_realm_seq_no,
                                       rec.signature,
                                       rec.realm_id) then
         v_error_message := v_error_sig ;
         return false ;
      end if;
      if not b_avail then
         v_error_local := nvl (v_error_local, v_error_sig) ;
         b_valid := false ;
      end if ;
/*
      if not b_valid then
         delete sa_vr_realm svr
          where svr.vr_id           = v_vr_id
            and svr.vr_rev_no       = n_vr_rev_no
            and svr.vr_realm_seq_no = rec.vr_realm_seq_no ;
         b_repaired := true ;
      end if ;
*/
   end loop ;

   open c_parm_alias ;
   fetch c_parm_alias into n_vr_parm_seq_no1 ;
   b_avail := c_parm_alias%found ;
   close c_parm_alias ;
   if b_avail then
      v_error_local := nvl (v_error_local, 'improper use of sa_parm #' || n_vr_parm_seq_no1) ;
      b_valid := false ;
   else
      open c_duplicate ;
      fetch c_duplicate into n_vr_parm_seq_no1,
                             n_vr_parm_seq_no2 ;
      b_avail := c_duplicate%found ;
      close c_duplicate ;
      if b_avail then
         v_error_local := nvl (v_error_local, 'duplicate link parms #' || n_vr_parm_seq_no1 || 
                                              ', #' || n_vr_parm_seq_no2) ;
         b_valid := false ;
      end if ;
   end if ;

   open c_outer_join ;
   fetch c_outer_join into n_vr_realm_seq_no,
                           n_vr_parm_seq_no1,
                           n_vr_parm_seq_no2 ;
   b_avail := c_outer_join%found ;
   close c_outer_join ;
   if b_avail then
      v_error_local := nvl (v_error_local, 'outer join for sa_realm #'|| n_vr_realm_seq_no ||' does not match link '||
                                           'from sa_parm #' || n_vr_parm_seq_no1 || 'to sa_parm #' || n_vr_parm_seq_no2) ;
      b_valid := false ;
   end if ;

   v_error_message := v_error_local;
   return true ;

exception

   when others then
      if c_bad_parms%isopen then
         close c_bad_parms ;
      end if ;
      if c_missing_realms%isopen then
         close c_missing_realms ;
      end if ;
      if c_driving_realm%isopen then
         close c_driving_realm ;
      end if ;
      if c_parm_alias%isopen then
         close c_parm_alias ;
      end if ;
      if c_duplicate%isopen then
         close c_duplicate ;
      end if ;
      if c_outer_join%isopen then
         close c_outer_join ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function fetch_realm (                                                                                 
                      v_error_message   out    varchar2,
                      n_vr_realm_seq_no out    SA_VR_realm.vr_realm_seq_no%TYPE,     
                      v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                      n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,
                      n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)     
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.FETCH_REALM' ;

   cursor c_realm is
      select vr_realm_seq_no
        from sa_vr_realm svr,
             sa_vr_parms svp,
             sa_parm p
       where svp.vr_id          = v_vr_id
         and svp.vr_rev_no      = n_vr_rev_no
         and svp.vr_parm_seq_no = n_vr_parm_seq_no
         and p.parm_id          = svp.parm_id
         and svr.vr_id          = svp.vr_id
         and svr.vr_rev_no      = svp.vr_rev_no
         and svr.realm_id       = p.realm_id
         and nvl(svr.signature,'Y') = 
                 nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no),'Y');

begin

   open c_realm ;
   fetch c_realm into n_vr_realm_seq_no ;
   if c_realm%notfound then
      n_vr_realm_seq_no := 0 ;
   end if;
   close c_realm ;
   return true ;

exception

   when others then
      if c_realm%isopen then
         close c_realm ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function repair_parms (                                                                                 
                        v_error_message   out    varchar2,
                        b_repaired        out    boolean,
                        v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                        n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE)
         return boolean 
is

   v_program         varchar2(50)  := 'SA_VR_SQL.REPAIR_PARMS' ;
   n_vr_realm_seq_no SA_VR_realm.vr_realm_seq_no%TYPE;     
   v_realm_alias     SA_VR_realm.realm_alias%TYPE;     
   b_valid           boolean ;

   cursor c_missing_realms is
      select distinct
             sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no) parm_sig,
             p.realm_id,
             r.display_name
        from sa_vr_parms svp,
             sa_parm p,
             sa_realm r
       where svp.vr_id          = v_vr_id
         and svp.vr_rev_no      = n_vr_rev_no
         and p.parm_id          = svp.parm_id
         and r.realm_id         = p.realm_id
         and not exists
        (select null
           from sa_vr_realm svr
          where svr.vr_id          = svp.vr_id
            and svr.vr_rev_no      = svp.vr_rev_no
            and svr.realm_id       = p.realm_id
            and nvl(svr.signature,'Y') = 
                   nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no), 'Y')) ;

   cursor c_min_realm (v_signature SA_VR_realm.signature%TYPE, v_realm_id sa_realm.realm_id%TYPE) is
      select min (svr.vr_realm_seq_no)
        from sa_vr_parms svp,
             sa_parm p,
             sa_vr_realm svr
       where p.realm_id         = v_realm_id
         and svp.vr_id          = v_vr_id
         and svp.vr_rev_no      = n_vr_rev_no
         and p.parm_id          = svp.parm_id
         and svr.vr_id          = svp.vr_id
         and svr.vr_rev_no      = svp.vr_rev_no
         and svr.realm_id       = p.realm_id
         and v_signature        =
                nvl(sa_vr_sql.get_signature (svp.vr_id, svp.vr_rev_no, svp.vr_parm_seq_no), 'Y')
         and instr (svr.signature, to_char(svp.vr_parm_seq_no, 'FM000') || 'x') > 0 ;


begin

   b_repaired := false ;
   for rec in c_missing_realms loop
      open c_min_realm (rec.parm_sig, rec.realm_id) ;
      fetch c_min_realm into n_vr_realm_seq_no ;
      close c_min_realm ;
      if n_vr_realm_seq_no is null then
         if not sa_vr_sql.next_vr_realm_seq_no (v_error_message,
                                                n_vr_realm_seq_no,
                                                v_vr_id,
                                                n_vr_rev_no) then
            return false ;
         end if ;
      else
         update sa_vr_realm svr
            set svr.vr_realm_seq_no = svr.vr_realm_seq_no + 1
          where svr.vr_id           = v_vr_id
            and svr.vr_rev_no       = n_vr_rev_no
            and svr.vr_realm_seq_no >= n_vr_realm_seq_no ;
      end if;
      if not sa_vr_sql.make_unique_realm_alias (v_error_message,
                                                v_realm_alias,
                                                v_vr_id,
                                                n_vr_rev_no,
                                                rec.display_name) then
         return false ;
      end if ;
      insert 
        into sa_vr_realm
           ( vr_id,
             vr_rev_no,
             realm_id,
             signature,
             outer_join_ind,
             realm_alias,
             vr_realm_seq_no )
      values
           ( v_vr_id,
             n_vr_rev_no,
             rec.realm_id,
             rec.parm_sig,
             'N',
             v_realm_alias,
             n_vr_realm_seq_no ) ;

      b_repaired := true ;
   end loop ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_links (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                       n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.REMOVE_LINKS' ;

begin

   delete sa_vr_links svl
    where svl.vr_id          = v_vr_id
      and svl.vr_rev_no      = n_vr_rev_no
      and svl.vr_parm_seq_no = n_vr_parm_seq_no;
   return true;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function check_parm_dependants (                                                                                 
                                v_error_message   out    varchar2,
                                b_dependants      out    boolean,
                                v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.CHECK_PARM_DEPENDANTS' ;
   b_head           boolean ;
   b_restrictions   boolean ;

begin

   if not sa_vr_sql.check_rule_vr_dependants (v_error_message,
                                              b_head,
                                              b_restrictions,
                                              v_vr_id,
                                              n_vr_rev_no,
                                              n_vr_parm_seq_no) then
      return false ;
   end if ;
   if b_head or b_restrictions then
      b_dependants := true ;
      return true ;
   end if ;
   if not sa_vr_sql.check_total_vr_dependants (v_error_message,
                                               b_head,
                                               b_restrictions,
                                               v_vr_id,
                                               n_vr_rev_no,
                                               n_vr_parm_seq_no) then
      return false ;
   end if ;
   if b_head or b_restrictions then
      b_dependants := true ;
   else
      b_dependants := false ;
   end if ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_parm_dependants (                                                                                 
                                 v_error_message   out    varchar2,
                                 v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                 n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                 n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.REMOVE_PARM_DEPENDANTS' ;

begin

   if not sa_vr_sql.remove_rule_vr_dependants (v_error_message,
                                               v_vr_id,
                                               n_vr_rev_no,
                                               n_vr_parm_seq_no) then
      return false ;
   end if ;
   if not sa_vr_sql.remove_total_vr_dependants (v_error_message,
                                                v_vr_id,
                                                n_vr_rev_no,
                                                n_vr_parm_seq_no) then
      return false ;
   end if ;
   if not sa_vr_sql.remove_links (v_error_message,
                                  v_vr_id,
                                  n_vr_rev_no,
                                  n_vr_parm_seq_no) then
      return false ;
   end if ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function check_rule_vr_dependants (                                                                                 
                                   v_error_message   out    varchar2,
                                   b_head            out    boolean,
                                   b_restrictions    out    boolean,
                                   v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                   n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                   n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.CHECK_RULE_VR_DEPENDANTS' ;
   v_dummy          varchar2(1) ;

   cursor c_restrictions is
      select null
        from sa_rule_comp src,
             sa_rule_comp_restrictions srcr
       where srcr.rule_id          = src.rule_id
         and srcr.rule_rev_no      = src.rule_rev_no
         and srcr.rule_comp_seq_no = src.rule_comp_seq_no
         and src.vr_id             = v_vr_id
         and src.vr_rev_no         = n_vr_rev_no
         and (  srcr.parm_seq_no_1    = n_vr_parm_seq_no
             or srcr.parm_seq_no_2    = n_vr_parm_seq_no
             or srcr.parm_seq_no_3    = n_vr_parm_seq_no  ) ;
/*
   cursor c_head is
      select null
        from sa_rule_head srh
       where srh.vr_id                 = v_vr_id
         and srh.vr_rev_no             = n_vr_rev_no
         and (  srh.key_val_2_parm_seq_no = n_vr_parm_seq_no
             or srh.key_val_1_parm_seq_no = n_vr_parm_seq_no  ) ;
*/
begin
/*
   open c_head ;
   fetch c_head into v_dummy ;
   b_head := c_head%found ;
   close c_head ;
*/b_head := false ;

   open c_restrictions ;
   fetch c_restrictions into v_dummy ;
   b_restrictions := c_restrictions%found ;
   close c_restrictions ;

   return true ;

exception

   when others then
/*
      if c_head%isopen then
         close c_head ;
      end if ;
*/
      if c_restrictions%isopen then
         close c_restrictions ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function check_total_vr_dependants (                                                                                 
                                    v_error_message   out    varchar2,
                                    b_head            out    boolean,
                                    b_restrictions    out    boolean,
                                    v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                    n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                    n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.CHECK_TOTAL_VR_DEPENDANTS' ;
   v_dummy          varchar2(1) ;

   cursor c_restrictions is
      select null
        from sa_total_head sth,
             sa_total_restrictions str
       where str.total_id       = sth.total_id
         and str.total_rev_no   = sth.total_rev_no
         and sth.vr_id          = v_vr_id
         and sth.vr_rev_no      = n_vr_rev_no
         and (  str.parm_seq_no_1  = n_vr_parm_seq_no
             or str.parm_seq_no_2  = n_vr_parm_seq_no
             or str.parm_seq_no_3  = n_vr_parm_seq_no  ) ;

   cursor c_head is
      select null
        from sa_total_head sth
       where sth.vr_id         = v_vr_id
         and sth.vr_rev_no     = n_vr_rev_no
         and (  sth.total_parm_seq_no = n_vr_parm_seq_no
             or sth.group_seq_no1 = n_vr_parm_seq_no
             or sth.group_seq_no2 = n_vr_parm_seq_no
             or sth.group_seq_no3 = n_vr_parm_seq_no  ) ;

begin

   open c_head ;
   fetch c_head into v_dummy ;
   b_head := c_head%found ;
   close c_head ;

   open c_restrictions ;
   fetch c_restrictions into v_dummy ;
   b_restrictions := c_restrictions%found ;
   close c_restrictions ;

   return true;

exception

   when others then
      if c_head%isopen then
         close c_head ;
      end if ;
      if c_restrictions%isopen then
         close c_restrictions ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_rule_vr_dependants (                                                                                 
                                    v_error_message   out    varchar2,
                                    v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                    n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                    n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.REMOVE_RULE_VR_DEPENDANTS' ;

begin

   delete sa_rule_comp_restrictions srcr
    where exists
   (  select null
        from sa_rule_comp src
       where src.rule_id          = srcr.rule_id
         and src.rule_rev_no      = srcr.rule_rev_no
         and src.rule_comp_seq_no = srcr.rule_comp_seq_no
         and src.vr_id            = v_vr_id
         and src.vr_rev_no        = n_vr_rev_no  )
      and (  srcr.parm_seq_no_1   = n_vr_parm_seq_no
          or srcr.parm_seq_no_2   = n_vr_parm_seq_no
          or srcr.parm_seq_no_3   = n_vr_parm_seq_no  ) ;
/*
   update sa_rule_head srh
      set srh.key_val_2_parm_seq_no = null
    where srh.vr_id                 = v_vr_id
      and srh.vr_rev_no             = n_vr_rev_no
      and srh.key_val_2_parm_seq_no = n_vr_parm_seq_no ;

   update sa_rule_head srh
      set srh.key_val_1_parm_seq_no = srh.key_val_2_parm_seq_no,
          srh.key_val_2_parm_seq_no = null
    where srh.vr_id                 = v_vr_id
      and srh.vr_rev_no             = n_vr_rev_no
      and srh.key_val_1_parm_seq_no = n_vr_parm_seq_no ;
*/
   return true;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_total_vr_dependants (                                                                                 
                                     v_error_message   out    varchar2,
                                     v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                                     n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                                     n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program        varchar2(50)  := 'SA_VR_SQL.REMOVE_TOTAL_VR_DEPENDANTS' ;

begin

   delete sa_total_restrictions str
    where exists
   (  select null
        from sa_total_head sth
       where sth.total_id       = str.total_id
         and sth.total_rev_no   = str.total_rev_no
         and sth.vr_id          = v_vr_id
         and sth.vr_rev_no      = n_vr_rev_no  )
      and (  str.parm_seq_no_1  = n_vr_parm_seq_no
          or str.parm_seq_no_2  = n_vr_parm_seq_no
          or str.parm_seq_no_3  = n_vr_parm_seq_no  ) ;

   update sa_total_head sth
      set sth.total_parm_seq_no = 0
    where sth.vr_id             = v_vr_id
      and sth.vr_rev_no         = n_vr_rev_no
      and sth.total_parm_seq_no = n_vr_parm_seq_no ;

   update sa_total_head sth
      set sth.group_seq_no3 = null
    where sth.vr_id         = v_vr_id
      and sth.vr_rev_no     = n_vr_rev_no
      and sth.group_seq_no3 = n_vr_parm_seq_no ;

   update sa_total_head sth
      set sth.group_seq_no2 = sth.group_seq_no3,
          sth.group_seq_no3 = null
    where sth.vr_id         = v_vr_id
      and sth.vr_rev_no     = n_vr_rev_no
      and sth.group_seq_no2 = n_vr_parm_seq_no ;

   update sa_total_head sth
      set sth.group_seq_no1 = sth.group_seq_no2,
          sth.group_seq_no2 = sth.group_seq_no3,
          sth.group_seq_no3 = null
    where sth.vr_id         = v_vr_id
      and sth.vr_rev_no     = n_vr_rev_no
      and sth.group_seq_no1 = n_vr_parm_seq_no ;

   return true;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function remove_parm (                                                                                 
                      v_error_message   out    varchar2,
                      v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                      n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                      n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program   varchar2(50)  := 'SA_VR_SQL.REMOVE_PARM' ;

   cursor c_back_links is
      select svl.vr_parm_seq_no
        from sa_vr_links svl
       where svl.vr_id               = v_vr_id
         and svl.vr_rev_no           = n_vr_rev_no
         and svl.link_to_parm_seq_no = n_vr_parm_seq_no ;

   cursor c_realms is
      select svr.vr_realm_seq_no
        from sa_vr_realm svr
       where svr.vr_id = v_vr_id
         and svr.vr_rev_no = n_vr_rev_no
         and instr (svr.signature, to_char(n_vr_parm_seq_no, 'FM000') || 'x') > 0 ;

begin

   for rec in c_back_links loop
      if not sa_vr_sql.remove_parm (v_error_message,
                                    v_vr_id,
                                    n_vr_rev_no,
                                    rec.vr_parm_seq_no) then
         return false ;
      end if ;
   end loop ;

   for rec in c_realms loop
      if not sa_vr_sql.remove_realm (v_error_message,
                                     v_vr_id,
                                     n_vr_rev_no,
                                     rec.vr_realm_seq_no) then
         return false ;
      end if ;
   end loop ;

   if not sa_vr_sql.remove_parm_dependants (v_error_message,
                                            v_vr_id,
                                            n_vr_rev_no,
                                            n_vr_parm_seq_no) then
      return false ;
   end if ;

   delete sa_vr_parms svp
    where svp.vr_id = v_vr_id
      and svp.vr_rev_no = n_vr_rev_no
      and svp.vr_parm_seq_no = n_vr_parm_seq_no ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function insert_links (                                                                                 
                       v_error_message   out    varchar2,
                       v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                       v_signature       in     varchar2,
                       v_realm_id        in     sa_realm.realm_id%TYPE, 
                       n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE,
                       v_outer_join_ind  in     sa_vr_links.outer_join_ind%TYPE)
         return boolean 
is

   v_program   varchar2(50)  := 'SA_VR_SQL.INSERT_LINKS' ;
   n_sig_index  NUMBER := 1;

   cursor c_parm_info is
      select p.parm_id
        from sa_parm p,
             sa_parm_type pt
       where p.realm_id = v_realm_id
         and p.realm_key_ind = 'Y'
         and p.active_ind = 'Y'
         and pt.active_ind = 'Y'
         and p.parm_type_id = pt.parm_type_id
    order by p.parm_id;


begin

   for rec in c_parm_info loop
      if n_sig_index > nvl (length(v_signature), 0) then
         v_error_message := SQL_LIB.CREATE_MSG('signature too short',
                                               SQLERRM,
                                               v_program,
                                               to_char(SQLCODE));
         return false ;
      end if;
      insert into sa_vr_links (vr_id, 
                               vr_rev_no, 
                               vr_parm_seq_no, 
                               link_to_parm_id, 
                               link_to_parm_seq_no, 
                               outer_join_ind)
                       values (v_vr_id, 
                               n_vr_rev_no, 
                               n_vr_parm_seq_no, 
                               rec.parm_id, 
                               substr (v_signature, n_sig_index, 3), 
                               v_outer_join_ind);
      n_sig_index := n_sig_index + 4;
   end loop;
   if n_sig_index <= length(v_signature) then
         v_error_message := SQL_LIB.CREATE_MSG('signature too long',
                                               SQLERRM,
                                               v_program,
                                               to_char(SQLCODE));
         return false ;
   end if;
   return true ;

exception

   when others then
      if c_parm_info%isopen then
         close c_parm_info ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------


function find_realm_signature (                                                                                 
                               v_error_message   out    varchar2,
                               n_vr_realm_seq_no out    SA_VR_realm.vr_realm_seq_no%TYPE,
                               v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                               n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                               v_realm_id        in     SA_VR_realm.realm_id%TYPE, 
                               v_signature       in     SA_VR_realm.signature%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FIND_REALM_SIGNATURE' ;

   cursor c_signature is
      select svr.vr_realm_seq_no
        from sa_vr_realm svr
       where svr.vr_id          = v_vr_id
         and svr.vr_rev_no      = n_vr_rev_no
         and svr.realm_id       = v_realm_id
         and svr.signature      = v_signature ;

begin

   open c_signature ;
   fetch c_signature into n_vr_realm_seq_no ;
   if c_signature%notfound then
      n_vr_realm_seq_no := 0 ;
   end if;
   close c_signature ;
   return true ;

exception

   when others then
      if c_signature%isopen then
         close c_signature ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE)) ;
      return false ;

end ;
--------------------------------------------------------------------------------

function find_realm_alias (                                                                                 
                           v_error_message   out    varchar2,
                           n_vr_realm_seq_no out    SA_VR_realm.vr_realm_seq_no%TYPE,
                           v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                           v_realm_alias     in     SA_VR_realm.realm_alias%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FIND_REALM_ALIAS' ;

   cursor c_realm_alias is
      select svr.vr_realm_seq_no
        from sa_vr_realm svr
       where svr.vr_id          = v_vr_id
         and svr.vr_rev_no      = n_vr_rev_no
         and upper (svr.realm_alias)    = upper (v_realm_alias) ;

begin

   open c_realm_alias ;
   fetch c_realm_alias into n_vr_realm_seq_no ;
   if c_realm_alias%notfound then
      n_vr_realm_seq_no := 0 ;
   end if;
   close c_realm_alias ;
   return true ;

exception

   when others then
      if c_realm_alias%isopen then
         close c_realm_alias ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function find_parm_alias (                                                                                 
                           v_error_message  out    varchar2,
                           n_vr_parm_seq_no out    sa_vr_parms.vr_parm_seq_no%TYPE,
                           v_vr_id          in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no      in     sa_vr_head.vr_rev_no%TYPE,  
                           v_parm_alias     in     sa_vr_parms.vr_parm_alias%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FIND_PARM_ALIAS' ;

   cursor c_parm_alias is
      select svp.vr_parm_seq_no
        from sa_vr_parms svp
       where svp.vr_id         = v_vr_id
         and svp.vr_rev_no     = n_vr_rev_no
         and upper (svp.vr_parm_alias)    = upper (v_parm_alias) ;

begin

   open c_parm_alias ;
   fetch c_parm_alias into n_vr_parm_seq_no ;
   if c_parm_alias%notfound then
      n_vr_parm_seq_no := 0 ;
   end if;
   close c_parm_alias ;
   return true ;

exception

   when others then
      if c_parm_alias%isopen then
         close c_parm_alias ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function find_vr_name (                                                                                 
                       v_error_message  out    varchar2,
                       v_vr_id          out    sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no      out    sa_vr_head.vr_rev_no%TYPE,  
                       v_vr_name        in     sa_vr_head.vr_name%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FIND_VR_NAME'  ;

   cursor c_vr_name is
      select svh.vr_id,
             svh.vr_rev_no
        from sa_vr_head svh
       where upper (svh.vr_name)    = upper (v_vr_name) ;

begin

   open c_vr_name ;
   fetch c_vr_name into v_vr_id,
                        n_vr_rev_no ;
   if c_vr_name%notfound then
      v_vr_id := 0 ;
      n_vr_rev_no := 0 ;
   end if;
   close c_vr_name ;
   return true ;

exception

   when others then
      if c_vr_name%isopen then
         close c_vr_name ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function fetch_realm_alias (                                                                                 
                            v_error_message   out    varchar2,
                            v_realm_alias     out    SA_VR_realm.realm_alias%TYPE, 
                            v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                            n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                            n_vr_realm_seq_no in     SA_VR_realm.vr_realm_seq_no%TYPE)
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FETCH_REALM_ALIAS' ;
   b_found    boolean;

   cursor c_realm_alias is
      select svr.realm_alias
        from sa_vr_realm svr
       where svr.vr_id           = v_vr_id
         and svr.vr_rev_no       = n_vr_rev_no
         and svr.vr_realm_seq_no = n_vr_realm_seq_no ;

begin

   open c_realm_alias ;
   fetch c_realm_alias into v_realm_alias ;
   b_found := c_realm_alias%found ;
   close c_realm_alias ;
   if not b_found then
      v_error_message := SQL_LIB.CREATE_MSG('error finding sa_realm alias',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
   end if;
   return true ;

exception

   when others then
      if c_realm_alias%isopen then
         close c_realm_alias ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function fetch_parm_alias (                                                                                 
                           v_error_message   out    varchar2,
                           v_parm_alias      out    sa_vr_parms.vr_parm_alias%TYPE, 
                           v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                           n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                           n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FETCH_PARM_ALIAS' ;
   b_found    boolean;

   cursor c_parm_alias is
      select svr.vr_parm_alias
        from sa_vr_parms svr
       where svr.vr_id          = v_vr_id
         and svr.vr_rev_no      = n_vr_rev_no
         and svr.vr_parm_seq_no = n_vr_parm_seq_no ;

begin

   open c_parm_alias ;
   fetch c_parm_alias into v_parm_alias ;
   b_found := c_parm_alias%found ;
   close c_parm_alias ;
   if not b_found then
      v_error_message := SQL_LIB.CREATE_MSG('error finding sa_parm alias',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;
   end if;
   return true ;

exception

   when others then
      if c_parm_alias%isopen then
         close c_parm_alias ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function fetch_parm_type_id (                                                                                 
                             v_error_message   out    varchar2,
                             v_parm_type_id    out     sa_parm.parm_type_id%TYPE, 
                             v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                             n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE,  
                             n_vr_parm_seq_no  in     sa_vr_parms.vr_parm_seq_no%TYPE)
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.FETCH_PARM_TYPE_ID' ;
   b_found    boolean;

   cursor c_parm_type_id is
      select p.parm_type_id
        from sa_vr_parms svr,
             sa_parm p
       where p.parm_id          = svr.parm_id
         and svr.vr_id          = v_vr_id
         and svr.vr_rev_no      = n_vr_rev_no
         and svr.vr_parm_seq_no = n_vr_parm_seq_no ;

begin

   open c_parm_type_id ;
   fetch c_parm_type_id into v_parm_type_id ;
   b_found := c_parm_type_id%found ;
   close c_parm_type_id ;
   if not b_found then
      v_error_message := SQL_LIB.CREATE_MSG('error finding sa_parm type id',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE)) ;
      return false ;
   end if;
   return true ;

exception

   when others then
      if c_parm_type_id%isopen then
         close c_parm_type_id ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE)) ;
      return false ;

end;
--------------------------------------------------------------------------------

function next_vr_parm_seq_no (                                                                                 
                              v_error_message   out    varchar2,
                              n_vr_parm_seq_no  out    sa_vr_parms.vr_parm_seq_no%TYPE,     
                              v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                              n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.NEXT_VR_PARM_SEQ_NO' ;

   cursor c_max_seq is
      select max (svp.vr_parm_seq_no)
        from sa_vr_parms svp
       where svp.vr_id          = v_vr_id
         and svp.vr_rev_no      = n_vr_rev_no;
begin

   open c_max_seq ;
   fetch c_max_seq into n_vr_parm_seq_no ;
   if c_max_seq%notfound or nvl (n_vr_parm_seq_no, 0) <= 0 then
      n_vr_parm_seq_no := 0 ;
   end if;
   close c_max_seq ;
   n_vr_parm_seq_no := n_vr_parm_seq_no + 1 ;
   if n_vr_parm_seq_no > 999 then
      v_error_message := SQL_LIB.CREATE_MSG('no available parm_seq_no',
                                            null,
                                            null,
                                            null);
      return false ;
   end if;
   return true;
exception

   when others then
      if c_max_seq%isopen then
         close c_max_seq ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------
function next_vr_realm_seq_no (                                                                                 
                               v_error_message   out    varchar2,
                               n_vr_realm_seq_no out    SA_VR_realm.vr_realm_seq_no%TYPE,     
                               v_vr_id           in     sa_vr_head.vr_id%TYPE,     
                               n_vr_rev_no       in     sa_vr_head.vr_rev_no%TYPE) 
         return boolean 
is

   v_program  varchar2(50)  := 'SA_VR_SQL.NEXT_VR_REALM_SEQ_NO' ;

   cursor c_max_seq is
      select max (svr.vr_realm_seq_no)
        from sa_vr_realm svr
       where svr.vr_id          = v_vr_id
         and svr.vr_rev_no      = n_vr_rev_no;

begin

   open c_max_seq ;
   fetch c_max_seq into n_vr_realm_seq_no ;
   if c_max_seq%notfound or n_vr_realm_seq_no is null then
      n_vr_realm_seq_no := 0 ;
   end if;
   close c_max_seq ;
   n_vr_realm_seq_no := n_vr_realm_seq_no + 1 ;
   return true;

exception

   when others then
      if c_max_seq%isopen then
         close c_max_seq ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function insert_vr_head (                                                                                 
                         v_error_message   out     varchar2,
                         v_vr_id           out     sa_vr_head.vr_id%TYPE,     
                         v_vr_name         in      sa_vr_head.vr_name%TYPE, 
                         v_realm_id        in      sa_realm.realm_id%TYPE) 
         return boolean 
is

   v_program       varchar2(50)  := 'SA_VR_SQL.INSERT_VR_HEAD' ;
   v_temp_vr_id    sa_vr_head.vr_id%TYPE;
   v_temp_vr_name  sa_vr_head.vr_name%TYPE;
   v_dummy         varchar2(1);
   b_found         boolean;
   n_temp_vr_seq   number;

   cursor c_vr_name is
      select null
        from sa_vr_head svh
       where upper (svh.vr_name)    = upper (v_vr_name) ;

   cursor c_id_seq is
      select sa_vr_id_sequence.nextval
        from dual ;

begin
   if v_vr_name is not null then
      open c_vr_name ;
      fetch c_vr_name into v_dummy ;
      b_found := c_vr_name%found;
      close c_vr_name ;
      if b_found then
         v_error_message := SQL_LIB.CREATE_MSG('Please choose unique name',
                                               null,
                                               null,
                                               null);
         return false ;
      end if;
   end if;

   open c_id_seq ;
   fetch c_id_seq into n_temp_vr_seq ;
   close c_id_seq ;
   v_temp_vr_id := 'VR' || to_char (n_temp_vr_seq, 'FM00000000');
   if not sa_vr_sql.make_unique_vr_name (v_error_message,
	 	                             v_temp_vr_name,
	 	                             nvl (v_vr_name, v_temp_vr_id)) then
	return false;
   end if;
   insert into sa_vr_head (vr_id, vr_rev_no, vr_name, driving_realm_id)
                   values (v_temp_vr_id, 1, v_temp_vr_name, v_realm_id);
   v_vr_id := v_temp_vr_id;

   return true;

exception

   when others then
      if c_vr_name%isopen then
         close c_vr_name ;
      end if ;
      if c_id_seq%isopen then
         close c_id_seq ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function lock_vr_head (                                                                                 
                       v_error_message   out     varchar2,
                       v_vr_id           in      sa_vr_head.vr_id%TYPE,     
                       n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE) 
         return boolean 
is

   v_program       varchar2(50)  := 'SA_VR_SQL.LOCK_VR_HEAD' ;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT (RECORD_LOCKED, -54);
   v_dummy         varchar2(1);
   b_found         boolean;

   cursor c_lock is
      select null
        from sa_vr_head svh
       where svh.vr_id     = v_vr_id
         and svh.vr_rev_no = n_vr_rev_no 
         for update nowait;

begin
   open c_lock ;
   fetch c_lock into v_dummy ;
   b_found := c_lock%found;
   close c_lock ;
   if not b_found then
      v_error_message := SQL_LIB.CREATE_MSG('vr not found',
                                            null,
                                            null,
                                            null);
       return false ;
   end if;
   return true ;
exception

   when record_locked then
      if c_lock%isopen then
         close c_lock ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('cannot obtain lock',
                                            null,
                                            v_program,
                                            null) ;
      return false ;
   when others then
      if c_lock%isopen then
         close c_lock ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE)) ;
      return false ;

end;
--------------------------------------------------------------------------------

function update_vr_name (                                                                                 
                         v_error_message   out     varchar2,
                         v_vr_id           in out  sa_vr_head.vr_id%TYPE,
                         n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE,
                         v_vr_name         in      sa_vr_head.vr_name%TYPE) 
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.UPDATE_VR_NAME' ;

begin

   update sa_vr_head
      set vr_name = v_vr_name
    where vr_id = v_vr_id
      and vr_rev_no = n_vr_rev_no ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function update_driving_realm (                                                                                 
                               v_error_message   out     varchar2,
                               v_vr_id           in out  sa_vr_head.vr_id%TYPE,
                               n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE,
                               v_realm_id        in      sa_realm.realm_id%TYPE) 
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.UPDATE_DRIVING_REALM' ;

begin

   update sa_vr_head
      set driving_realm_id = v_realm_id
    where vr_id = v_vr_id
      and vr_rev_no = n_vr_rev_no ;

   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function delete_vr_head (                                                                                 
                         v_error_message   out     varchar2,
                         v_vr_id           in out  sa_vr_head.vr_id%TYPE) 
         return boolean 
is
   v_program   varchar2(50)  := 'SA_VR_SQL.DELETE_VR_HEAD' ;

begin

   delete sa_vr_head
         where vr_id = v_vr_id ;
   return true ;

exception

   when others then
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function make_unique_vr_name (                                                                                 
                                 v_error_message   out     varchar2,
                                 v_vr_name         out     sa_vr_head.vr_name%TYPE,     
                                 v_name            in      sa_vr_head.vr_name%TYPE)     
         return boolean 
is

   v_program   varchar2(50)  := 'SA_VR_SQL.MAKE_UNIQUE_VR_NAME' ;
   v_dummy     varchar2(1);
   n_alias_no   number := 1;
   v_temp_alias sa_vr_head.vr_name%TYPE := v_name ;

   cursor c_vr_name is
      select null
        from sa_vr_head svh
       where upper (svh.vr_name) = upper (v_temp_alias);

begin

   open c_vr_name ;
   fetch c_vr_name into v_dummy ;
   if c_vr_name%found then
      close c_vr_name ;
      loop
         v_temp_alias := rtrim(substrb (v_name, 1, 117)) || to_char (n_alias_no);
         open c_vr_name ;
         fetch c_vr_name into v_dummy ;
         exit when c_vr_name%notfound;
         close c_vr_name ;
         n_alias_no := n_alias_no + 1;
         if n_alias_no > 999 then
            v_error_message := SQL_LIB.CREATE_MSG('no available names',
                                                  null,
                                                  null,
                                                  null);
            return false;
         end if;
      end loop;
   end if;
   close c_vr_name ;
   v_vr_name := v_temp_alias ;
   return true;

exception

   when others then
      if c_vr_name%isopen then
         close c_vr_name ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;

--------------------------------------------------------------------------------

function make_unique_parm_alias (                                                                                 
                                 v_error_message   out     varchar2,
                                 v_vr_parm_alias   out     sa_vr_parms.vr_parm_alias%TYPE,     
                                 v_vr_id           in      sa_vr_head.vr_id%TYPE,     
                                 n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE,
                                 v_parm_name       in      sa_vr_parms.vr_parm_alias%TYPE)     
         return boolean 
is

   v_program   varchar2(50)  := 'SA_VR_SQL.MAKE_UNIQUE_PARM_ALIAS' ;
   v_dummy     varchar2(1);
   n_alias_no   number := 1;
   v_temp_alias sa_vr_parms.vr_parm_alias%TYPE := initcap (v_parm_name);

   cursor c_vr_parm_alias is
      select null
        from sa_vr_parms svp
       where svp.vr_id         = v_vr_id
         and svp.vr_rev_no     = n_vr_rev_no
         and upper (svp.vr_parm_alias) = upper (v_temp_alias);

begin

   open c_vr_parm_alias ;
   fetch c_vr_parm_alias into v_dummy ;
   if c_vr_parm_alias%found then
      close c_vr_parm_alias ;
      loop
         v_temp_alias := rtrim(substrb (initcap (v_parm_name), 1, 117)) || to_char (n_alias_no);
         open c_vr_parm_alias ;
         fetch c_vr_parm_alias into v_dummy ;
         exit when c_vr_parm_alias%notfound;
         close c_vr_parm_alias ;
         n_alias_no := n_alias_no + 1;
         if n_alias_no > 999 then
            v_error_message := SQL_LIB.CREATE_MSG('no available sa_parm aliases',
                                                  null,
                                                  null,
                                                  null);
            return false;
         end if;
      end loop;
   end if;
   close c_vr_parm_alias ;
   v_vr_parm_alias := v_temp_alias ;
   return true;

exception

   when others then
      if c_vr_parm_alias%isopen then
         close c_vr_parm_alias ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------

function make_unique_realm_alias (                                                                                 
                                 v_error_message   out     varchar2,
                                 v_realm_alias     out     SA_VR_realm.realm_alias%TYPE,     
                                 v_vr_id           in      sa_vr_head.vr_id%TYPE,     
                                 n_vr_rev_no       in      sa_vr_head.vr_rev_no%TYPE,     
                                 v_realm_name      in      sa_vr_parms.vr_parm_alias%TYPE)     
         return boolean 
is

   v_program   varchar2(50)  := 'SA_VR_SQL.MAKE_UNIQUE_REALM_ALIAS' ;
   v_dummy     varchar2(1);
   n_alias_no   number := 1;
   v_temp_alias SA_VR_realm.realm_alias%TYPE := initcap (v_realm_name);

   cursor c_realm_alias is
      select null
        from sa_vr_realm svr
       where svr.vr_id          = v_vr_id
         and svr.vr_rev_no      = n_vr_rev_no
         and upper (svr.realm_alias)    = upper (v_temp_alias);

begin

   open c_realm_alias ;
   fetch c_realm_alias into v_dummy ;
   if c_realm_alias%found then
      close c_realm_alias ;
      loop
         v_temp_alias := rtrim(substrb (initcap (v_realm_name), 1, 117)) || to_char (n_alias_no);
         open c_realm_alias ;
         fetch c_realm_alias into v_dummy ;
         exit when c_realm_alias%notfound;
         close c_realm_alias ;
         n_alias_no := n_alias_no + 1;
         if n_alias_no > 999 then
            v_error_message := SQL_LIB.CREATE_MSG('no available sa_realm aliases',
                                                  null,
                                                  null,
                                                  null);
            return false;
         end if;
      end loop;
   end if;
   close c_realm_alias ;
   v_realm_alias := v_temp_alias ;
   return true;

exception

   when others then
      if c_realm_alias%isopen then
         close c_realm_alias ;
      end if ;
      v_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            v_program,
                                            to_char(SQLCODE));
      return false ;

end;
--------------------------------------------------------------------------------
FUNCTION CREATE_VR_REVISION(O_error_message     IN OUT  VARCHAR2,
                            I_new_realm_id      IN      SA_REALM.REALM_ID%TYPE,
                            I_vr_id             IN      SA_VR_HEAD.VR_ID%TYPE,
                            I_old_vr_rev_no     IN      SA_VR_HEAD.VR_REV_NO%TYPE,
                            I_new_vr_rev_no     IN      SA_VR_HEAD.VR_REV_NO%TYPE)
   return BOOLEAN IS
   ---
   L_original_realm_id  SA_REALM.REALM_ID%TYPE;  
   L_original_parm_id   SA_PARM.PARM_ID%TYPE;  
   L_vr_parm_seq_no     SA_VR_LINKS.VR_PARM_SEQ_NO%TYPE;
   L_program            VARCHAR2(60) := 'SA_VR_SQL.CREATE_VR_REVISION';
   ---
   cursor C_GET_REALM_TO_UPDATE is
      select realm_id
        from sa_vr_realm
       where vr_id     = I_vr_id                  
         and vr_rev_no = I_old_vr_rev_no;
   ---
   cursor C_GET_PARM_TO_UPDATE is
      select vr.parm_id
        from sa_vr_parms vr
       where vr.vr_id     = I_vr_id                  
         and vr.vr_rev_no = I_old_vr_rev_no
         and vr.parm_id  in (select p.parm_id 
                               from sa_parm p
                              where p.realm_id = L_original_realm_id);
   ---
   cursor C_GET_LINK_TO_UPDATE is
      select vr.link_to_parm_id, 
             vr.vr_parm_seq_no
        from sa_vr_links vr
       where vr.vr_id        = I_vr_id                  
         and vr.vr_rev_no    = I_old_vr_rev_no;
   ---
BEGIN
   if I_vr_id is NULL or I_old_vr_rev_no is NULL or I_new_vr_rev_no is NULL 
         or I_new_realm_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---   
   --- Insert into VR tables
   ---
   insert into sa_vr_head
               (vr_id,
                vr_rev_no,
                vr_name,
                driving_realm_id)
        select I_vr_id,
               I_new_vr_rev_no,
               vr_name,
               I_new_realm_id
          from sa_vr_head
         where vr_id     = I_vr_id
           and vr_rev_no = I_old_vr_rev_no;
   ---
   insert into sa_vr_links
               (vr_id,
                vr_rev_no,
                vr_parm_seq_no,
                link_to_parm_id,
                link_to_parm_seq_no,
                outer_join_ind)
        select I_vr_id,
               I_new_vr_rev_no,
               vr_parm_seq_no,
               link_to_parm_id,
               link_to_parm_seq_no,
               outer_join_ind
          from sa_vr_links
         where vr_id     = I_vr_id
           and vr_rev_no = I_old_vr_rev_no;
   --- 
   insert into sa_vr_parms
               (vr_id,
                vr_rev_no,
                vr_parm_seq_no,
                parm_id,
                vr_parm_alias)
        select I_vr_id,
               I_new_vr_rev_no,
               vr_parm_seq_no,
               parm_id,
               vr_parm_alias
          from sa_vr_parms
         where vr_id     = I_vr_id
           and vr_rev_no = I_old_vr_rev_no;   
   ---   
   insert into sa_vr_realm
               (vr_id,
                vr_rev_no,
                vr_realm_seq_no,
                realm_id,
                signature,
                realm_alias,
                outer_join_ind)
        select I_vr_id,
               I_new_vr_rev_no,
               vr_realm_seq_no,
               realm_id,
               signature,
               realm_alias,
               outer_join_ind
          from sa_vr_realm
         where vr_id     = I_vr_id
           and vr_rev_no = I_old_vr_rev_no;
   ---
   for L_realm_rec in C_GET_REALM_TO_UPDATE LOOP
      L_original_realm_id := L_realm_rec.realm_id;         
      update sa_vr_realm
         set realm_id = 
                (select max(r1.realm_id)
                   from sa_realm r1
                  where r1.physical_name = 
                           (select physical_name
                              from sa_realm r2
                             where r2.realm_id = L_original_realm_id))
       where vr_id     = I_vr_id
         and vr_rev_no = I_new_vr_rev_no
         and realm_id  = L_original_realm_id;         
      ---         
      for L_parm_rec in C_GET_PARM_TO_UPDATE LOOP
         L_original_parm_id := L_parm_rec.parm_id;            
         update sa_vr_parms
            set parm_id = 
                   (select max(p1.parm_id)
                      from sa_parm p1
                     where p1.physical_name = 
                              (select p2.physical_name
                                 from sa_parm p2
                                where p2.parm_id = L_original_parm_id)
                        and p1.realm_id = 
                              (select max(r1.realm_id)
                                 from sa_realm r1
                                where r1.physical_name = 
                                         (select physical_name
                                            from sa_realm r2
                                           where r2.realm_id = L_original_realm_id)))
          where vr_id     = I_vr_id
            and vr_rev_no = I_new_vr_rev_no
            and parm_id   = L_original_parm_id;
         ---                       
      end LOOP;     --- Parm Loop
   end LOOP;        --- Realm Loop       
   ---
   for L_link_rec in C_GET_LINK_TO_UPDATE LOOP
      L_vr_parm_seq_no   := L_link_rec.vr_parm_seq_no;
      L_original_parm_id := L_link_rec.link_to_parm_id;               
      update sa_vr_links
         set link_to_parm_id = 
                (select max(p1.parm_id)
                   from sa_parm p1
                  where p1.physical_name = 
                           (select p2.physical_name
                              from sa_parm p2
                             where p2.parm_id = L_original_parm_id)
                    and p1.realm_id = 
                           (select max(r1.realm_id)
                              from sa_realm r1
                             where r1.physical_name = 
                                      (select physical_name
                                         from sa_realm r2
                                        where r2.realm_id = 
                                                 (select p3.realm_id
                                                    from sa_parm p3
                                                   where p3.parm_id = L_original_parm_id))))                                                                                           
       where vr_id           = I_vr_id
         and vr_rev_no       = I_new_vr_rev_no
         and vr_parm_seq_no  = L_vr_parm_seq_no
         and link_to_parm_id = L_original_parm_id;
      ---     
   end LOOP;  --- Link Loop 
   ---
   RETURN TRUE;
   ---
   EXCEPTION
      WHEN others THEN
         o_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                   SQLERRM,
                                                   L_program,
                                                   SQLCODE);
         RETURN FALSE;
   END CREATE_VR_REVISION;
----------------------------------------------
END SA_VR_SQL;
/
