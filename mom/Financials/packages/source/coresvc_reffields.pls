CREATE OR REPLACE PACKAGE CORESVC_REFERENCE_FIELD AUTHID CURRENT_USER 
AS
action_new             VARCHAR2(25)          := 'NEW';
action_mod             VARCHAR2(25)          := 'MOD';
action_del             VARCHAR2(25)          := 'DEL';
Type SA_REFERENCE_rec_tab IS TABLE OF SA_REFERENCE%ROWTYPE;
---------------------------------------------------------------------------
-- Function Name : PROCESS
--  Description  : This function will insert data from SVC_SA_REFERENCE
--                 to SA_REFERENCE table.
---------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_process_id     IN  NUMBER,
                 O_error_count    OUT NUMBER)
 RETURN BOOLEAN;
----------------------------------------------------------------------------
END CORESVC_REFERENCE_FIELD;
/