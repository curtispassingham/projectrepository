CREATE OR REPLACE PACKAGE ACC_VALIDATE_API AUTHID CURRENT_USER AS

TYPE  acc_validate_rec IS RECORD (requesting_system   VARCHAR2(255),
                                  segment_1           FIF_GL_CROSS_REF.CR_SEQUENCE1%TYPE,
                                  segment_2           FIF_GL_CROSS_REF.CR_SEQUENCE2%TYPE,
                                  segment_3           FIF_GL_CROSS_REF.CR_SEQUENCE3%TYPE,
                                  segment_4           FIF_GL_CROSS_REF.CR_SEQUENCE4%TYPE,
                                  segment_5           FIF_GL_CROSS_REF.CR_SEQUENCE5%TYPE,
                                  segment_6           FIF_GL_CROSS_REF.CR_SEQUENCE6%TYPE,
                                  segment_7           FIF_GL_CROSS_REF.CR_SEQUENCE7%TYPE,
                                  segment_8           FIF_GL_CROSS_REF.CR_SEQUENCE8%TYPE,
                                  segment_9           FIF_GL_CROSS_REF.CR_SEQUENCE9%TYPE,
                                  segment_10          FIF_GL_CROSS_REF.CR_SEQUENCE10%TYPE,
                                  segment_11          FIF_GL_CROSS_REF.CR_SEQUENCE11%TYPE,
                                  segment_12          FIF_GL_CROSS_REF.CR_SEQUENCE12%TYPE,
                                  segment_13          FIF_GL_CROSS_REF.CR_SEQUENCE13%TYPE,
                                  segment_14          FIF_GL_CROSS_REF.CR_SEQUENCE14%TYPE,
                                  segment_15          FIF_GL_CROSS_REF.CR_SEQUENCE15%TYPE,
                                  segment_16          FIF_GL_CROSS_REF.CR_SEQUENCE16%TYPE,
                                  segment_17          FIF_GL_CROSS_REF.CR_SEQUENCE17%TYPE,
                                  segment_18          FIF_GL_CROSS_REF.CR_SEQUENCE18%TYPE,
                                  segment_19          FIF_GL_CROSS_REF.CR_SEQUENCE19%TYPE,
                                  segment_20          FIF_GL_CROSS_REF.CR_SEQUENCE20%TYPE,
                                  set_of_books_id     FIF_GL_CROSS_REF.SET_OF_BOOKS_ID%TYPE,
                                  ccid                FIF_GL_CROSS_REF.CR_CCID%TYPE,
                                  account_status      VARCHAR2(7));

TYPE  acc_validate_tbl IS TABLE OF acc_validate_rec INDEX BY BINARY_INTEGER;
--------------------------------------------------------------------------------------------------
--- Function Name: VALIDATE_ACC
--- Purpose:       This function will validate the segments,set_of_books_id and ccid combination.

--------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ACC(O_error_message    OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                      O_acc_val_rec   IN OUT    ACC_VALIDATE_API.ACC_VALIDATE_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------                      
END  ACC_VALIDATE_API;

/
