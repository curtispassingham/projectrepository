CREATE OR REPLACE PACKAGE BODY DIRECT_STORE_INVOICE_SQL AS
-------------------------------------------------------------------------------
FUNCTION CREATE_INVOICE(O_error_message         IN OUT VARCHAR2,
                        IO_invc_id              IN OUT INVC_HEAD.INVC_ID%TYPE,
                        I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                        I_supplier              IN     SUPS.SUPPLIER%TYPE,
                        I_vdate                 IN     PERIOD.VDATE%TYPE,
                        I_total_cost            IN     INVC_HEAD.TOTAL_MERCH_COST%TYPE,
                        I_total_qty             IN     INVC_HEAD.TOTAL_QTY%TYPE,
                        I_store                 IN     STORE.STORE%TYPE,
                        I_paid_ind              IN     INVC_HEAD.PAID_IND%TYPE,
                        I_ext_ref_no            IN     INVC_HEAD.EXT_REF_NO%TYPE,
                        I_proof_of_delivery_no  IN     INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                        I_payment_ref_no        IN     INVC_HEAD.PAYMENT_REF_NO%TYPE,
                        I_payment_date          IN     INVC_HEAD.PAYMENT_DATE%TYPE,
                        I_invc_status           IN     INVC_DETAIL.STATUS%TYPE              DEFAULT NULL,
                        I_item_invc_details     IN     ITEM_INVC_TBL                        DEFAULT NULL)
RETURN BOOLEAN IS

   L_invc_id            INVC_HEAD.INVC_ID%TYPE;
   L_terms              ORDHEAD.TERMS%TYPE;
   L_freight_terms      ORDHEAD.FREIGHT_TERMS%TYPE;
   L_currency_code      ORDHEAD.CURRENCY_CODE%TYPE;
   L_payment_method     ORDHEAD.PAYMENT_METHOD%TYPE;
   L_user_name          VARCHAR2(30);
   L_percent            TERMS.PERCENT%TYPE;
   L_addr_key           ADDR.ADDR_KEY%TYPE;
   L_exists             BOOLEAN                                := FALSE;
   L_program            VARCHAR2(50)                           := 'DIRECT_STORE_INVOICE_SQL.CREATE_INVOICE';

   cursor C_ORDHEAD is
      select terms,
             freight_terms,
             currency_code,
             payment_method
        from ordhead
       where order_no = I_order_no;

   cursor C_SUPS is
      select terms,
             freight_terms,
             currency_code,
             payment_method
        from sups
       where supplier = I_supplier;

   cursor C_TERMS is
      select percent
        from terms
       where terms = L_terms
         and (NVL(start_date_active, TO_DATE('00010101', 'YYYYMMDD')) <= get_vdate
         and  NVL(end_date_active, TO_DATE('99990101', 'YYYYMMDD')) >= get_vdate)
         and enabled_flag = 'Y';

BEGIN

   if I_order_no is not NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_ORDHEAD','ordhead','order_no: '||TO_CHAR(I_order_no));
      open C_ORDHEAD;

      SQL_LIB.SET_MARK('FETCH',
                       'C_ORDHEAD','ordhead','order_no: '||TO_CHAR(I_order_no));
      fetch C_ORDHEAD into L_terms,
                           L_freight_terms,
                           L_currency_code,
                           L_payment_method;

      if C_ORDHEAD%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                       'C_ORDHEAD','ordhead','order_no: '||TO_CHAR(I_order_no));
         close C_ORDHEAD;
         O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',
                                               NULL,NULL,NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_ORDHEAD','ordhead','order_no: '||TO_CHAR(I_order_no));
      close C_ORDHEAD;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_SUPS','sups','supplier: '||TO_CHAR(I_supplier));
      open C_SUPS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_SUPS','sups','supplier: '||TO_CHAR(I_supplier));
      fetch C_SUPS into L_terms,
                        L_freight_terms,
                        L_currency_code,
                        L_payment_method;

      if C_SUPS%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                       'C_SUPS','sups','supplier: '||TO_CHAR(I_supplier));
         close C_SUPS;
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER',
                                               NULL,NULL,NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_SUPS','sups','supplier: '||TO_CHAR(I_supplier));
      close C_SUPS;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_TERMS','terms','terms: '||L_terms);
   open C_TERMS;

   SQL_LIB.SET_MARK('FETCH','C_TERMS','terms','terms: '||L_terms);
   fetch C_TERMS into L_percent;

   if C_TERMS%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_TERMS','terms','terms: '||L_terms);
      close C_TERMS;
      O_error_message := SQL_LIB.CREATE_MSG('INV_TERMS',
                                            NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_TERMS','terms','terms: '||L_terms);
   close C_TERMS;
   ---
   if SQL_LIB.GET_USER_NAME(O_error_message,
                            L_user_name) = FALSE then
      return FALSE;
   end if;
   ---
   if not SUPP_ATTRIB_SQL.GET_SUP_PRIMARY_ADDR(O_error_message,
                                               L_addr_key,
                                               I_supplier,
                                               '05') then
      return FALSE;
   end if;
   ---
   if I_order_no is not NULL then
      if not DIRECT_STORE_INVOICE_SQL.CHECK_INVC_DUPS(O_error_message,
                                                      L_invc_id,
                                                      NULL,
                                                      I_supplier,
                                                      NULL,
                                                      NULL,
                                                      I_vdate,
                                                      I_ext_ref_no,
                                                      I_proof_of_delivery_no,
                                                      I_payment_ref_no,
                                                      I_payment_date) then
         return FALSE;
      end if;
   end if;
   ---
   if L_invc_id is not NULL then
     if not DIRECT_STORE_INVOICE_SQL.CHECK_INVC_DETAIL(O_error_message,
                                                       L_exists,
                                                       L_invc_id) then
        return FALSE;
     end if;
     ---
     if L_exists = TRUE then
        O_error_message := SQL_LIB.CREATE_MSG('INVC_EXIST', L_invc_id,NULL,NULL);
        return FALSE;
     end if;
     --- invoice already exists, but is only a header record. update existing record.
     update invc_head
        set total_merch_cost     = I_total_cost,
            total_qty            = I_total_qty,
            paid_ind             = I_paid_ind,
            ext_ref_no           = nvl(I_ext_ref_no, ext_ref_no),
            proof_of_delivery_no = nvl(I_proof_of_delivery_no, proof_of_delivery_no),
            payment_ref_no       = nvl(I_payment_ref_no, payment_ref_no),
            payment_date         = nvl(I_payment_date, payment_date)
      where invc_id = L_invc_id;
   else
      if IO_invc_id is NULL then
         if INVC_SQL.NEXT_INVC_ID(O_error_message,
                                  L_invc_id) = FALSE then
            return FALSE;
         end if;
      else
         L_invc_id := IO_invc_id;
      end if;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'invc_head',NULL);
      insert into invc_head (invc_id,
                             invc_type,
                             supplier,
                             ext_ref_no,
                             status,
                             edi_invc_ind,
                             edi_sent_ind,
                             match_fail_ind,
                             ref_invc_id,
                             ref_rtv_order_no,
                             ref_rsn_code,
                             terms,
                             due_date,
                             payment_method,
                             terms_dscnt_pct,
                             terms_dscnt_appl_ind,
                             terms_dscnt_appl_non_mrch_ind,
                             freight_terms,
                             create_id,
                             create_date,
                             invc_date,
                             match_id,
                             match_date,
                             approval_id,
                             approval_date,
                             force_pay_ind,
                             force_pay_id,
                             post_date,
                             currency_code,
                             exchange_rate,
                             total_merch_cost,
                             total_qty,
                             direct_ind,
                             addr_key,
                             paid_ind,
                             payment_ref_no,
                             payment_date,
                             proof_of_delivery_no,
                             comments)
                     values (L_invc_id,
                             decode(I_order_no, NULL, 'N', 'I'),
                             I_supplier,
                             I_ext_ref_no,
                             nvl(I_invc_status,'A'),
                             'N',
                             'N',
                             'N',
                             NULL,
                             NULL,
                             NULL,
                             L_terms,
                             I_vdate,
                             L_payment_method,
                             L_percent,
                             'N',
                             'N',
                             L_freight_terms,
                             L_user_name,
                             I_vdate,
                             I_vdate,
                             L_user_name,
                             I_vdate,
                             L_user_name,
                             I_vdate,
                             'N',
                             NULL,
                             NULL,
                             L_currency_code,
                             NULL,
                             I_total_cost,
                             I_total_qty,
                             'Y',
                             L_addr_key,
                             I_paid_ind,
                             I_payment_ref_no,
                             I_payment_date,
                             I_proof_of_delivery_no,
                             NULL);
   end if; --new invoice

   if I_order_no is not NULL then
      -- The shipment tables are updated to reflect the creation/updating of
      -- the invoice.  As Quick Order Entry will only ever create one shipment
      -- per order we are able to query the shipment tables by the order number
      -- passed into the create invoice function.
      SQL_LIB.SET_MARK('UPDATE',NULL,'shipment',NULL);
      update shipment
         set invc_match_status = nvl(I_invc_status,'M'),
             invc_match_date   = I_vdate
       where order_no = I_order_no;

      SQL_LIB.SET_MARK('UPDATE',NULL,'shipsku',NULL);
      update shipsku
         set match_invc_id = L_invc_id,
             qty_matched = qty_received
       where exists (select 'x'
                       from shipment sh
                      where order_no = I_order_no
                        and sh.shipment = shipsku.shipment);
      ---
      ---For new or updated invoice, insert details. CHECK_INVC_DUPS
      ---verifies details do not exist on invc_detail
      if DIRECT_STORE_INVOICE_SQL.INVOICE_ITEM(O_error_message,
                                               I_order_no,
                                               L_invc_id,
                                               I_store,
                                               I_item_invc_details) = FALSE then
         return FALSE;
      end if;
   end if;
   --- check if incoming invoice id already exists.
   --- if it does, then non-merch details exist
   if IO_invc_id is not NULL then
      --- Insert non-merchandise details from temp table to main table
      SQL_LIB.SET_MARK('INSERT',NULL,'invc_non_merch',NULL);
      insert into invc_non_merch(invc_id,
                                 non_merch_code,
                                 non_merch_amt,
                                 vat_code,
                                 service_perf_ind,
                                 store)
                          select L_invc_id,
                                 non_merch_code,
                                 non_merch_amt,
                                 vat_code,
                                 service_perf_ind,
                                 store
                            from invc_non_merch_temp
                           where invc_id = IO_invc_id;

      SQL_LIB.SET_MARK('DELETE',NULL,'invc_non_merch_temp',NULL);
      delete from invc_non_merch_temp;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_INVOICE;
-------------------------------------------------------------------------------
FUNCTION INVOICE_ITEM(O_error_message     IN OUT VARCHAR2,
                      I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                      I_invc_id           IN     INVC_HEAD.INVC_ID%TYPE,
                      I_store             IN     STORE.STORE%TYPE,
                      I_item_invc_details IN     ITEM_INVC_TBL          DEFAULT NULL)
RETURN BOOLEAN IS

   L_shipment          SHIPMENT.SHIPMENT%TYPE;
   L_item              SHIPSKU.ITEM%TYPE;
   L_ref_item          SHIPSKU.REF_ITEM%TYPE;
   L_unit_cost         SHIPSKU.UNIT_COST%TYPE;
   L_qty_received      SHIPSKU.QTY_RECEIVED%TYPE;
   L_vat_region        VAT_REGION.VAT_REGION%TYPE  := NULL;
   L_vat_rate          VAT_ITEM.VAT_RATE%TYPE      := NULL;
   L_vat_code          VAT_DEPS.VAT_CODE%TYPE      := NULL;
   L_vdate             PERIOD.VDATE%TYPE           := GET_VDATE;
   L_exist             VARCHAR2(1)                 := 'N';
   L_default_tax_type  SYSTEM_OPTIONS.default_tax_type%TYPE;
   L_program           VARCHAR2(50)                := 'DIRECT_STORE_INVOICE_SQL.INVOICE_ITEM';
   L_status            INVC_DETAIL.STATUS%TYPE;
   L_tax_info_tbl      OBJ_TAX_INFO_TBL            := OBJ_TAX_INFO_TBL();
   L_tax_info_rec      OBJ_TAX_INFO_REC            := OBJ_TAX_INFO_REC();
   L_exists            BOOLEAN;
   --
   L_invc_date          invc_head.invc_date%TYPE       ;
   L_supplier           invc_head.supplier%TYPE;
   --
   L_tax_calc_rec          OBJ_TAX_CALC_REC    := OBJ_TAX_CALC_REC();
   L_tax_calc_tbl          OBJ_TAX_CALC_TBL    := OBJ_TAX_CALC_TBL();
   L_tax_calc_detail_tbl   OBJ_TAX_DETAIL_TBL  ;
   --
   L_shipsku_tbl    OBJ_SHIPSKU_TBL := OBJ_SHIPSKU_TBL();
   --
   L_tot_merch_invc_cost   INVC_HEAD.TOTAL_MERCH_COST%TYPE;
   --
   L_bill_to_loc        SHIPMENT.BILL_TO_LOC%TYPE        := NULL;
   L_bill_to_loc_type   SHIPMENT.BILL_TO_LOC_TYPE%TYPE   := NULL;
   L_location           INVC_XREF.LOCATION%TYPE          := NULL;
   L_loc_type           INVC_XREF.LOC_TYPE%TYPE          := NULL;
   --
   L_reverse_vat_ind    VAT_ITEM.REVERSE_VAT_IND%TYPE    := 'N';
   L_total_rev_vat_cost SHIPSKU.UNIT_COST%TYPE           := NULL;
   L_threshold_val      VAT_REGION.REVERSE_VAT_THRESHOLD%TYPE;
   L_thresh_vat_rate    VAT_ITEM.VAT_RATE%TYPE           := NULL;
   L_zero_tax_code      VAT_DEPS.VAT_CODE%TYPE           := NULL;
   L_rev_vat_item_tbl   ITEM_TBL                         := ITEM_TBL();
   --
   cursor C_SHIPMENT is
      select shipment,
             bill_to_loc,
             bill_to_loc_type
        from shipment
       where order_no = I_order_no;

   cursor C_SHIPSKU is
      select OBJ_SHIPSKU_REC(shipment,
                             NULL,
                             item,
                             NULL,
                             NULL,
                             ref_item,
                             NULL,
                             NULL,
                             NULL,
                             qty_received,
                             unit_cost,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL)
        from shipsku ss
       where shipment = L_shipment;

  cursor C_INVC_DETAIL is
     select 'Y'
       from invc_detail
      where invc_id   = I_invc_id
        and item      = L_item
        and invc_unit_cost = L_unit_cost;

  cursor C_INVC_MERCH_VAT_CHECK is
      select 'Y'
        from invc_merch_vat
       where invc_id = I_invc_id
         and vat_code = L_vat_code;

   cursor C_GET_INVC_DATE is
      select invc_date,
             supplier
        from invc_head
       where invc_id = I_invc_id;


   cursor C_GET_ORD_BILL_TO is
      select supplier
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_TOTAL_MERCH_COST is
      select SUM(invc_unit_cost * invc_qty) tot_invc_cost
        from invc_detail
       where invc_id = I_invc_id;

   cursor C_GET_ZERO_RATE_TAX_CODE is
      select vcr.vat_code
        from vat_code_rates vcr
       where vcr.vat_rate = 0
         and vcr.active_date <= L_vdate
         and not exists(select 'x'
                          from vat_code_rates vcr2
                         where vcr2.vat_code = vcr.vat_code
                           and vcr2.active_date > vcr.active_date
                           and rownum = 1);

   cursor C_GET_VAT_REGION is
      select vat_region
        from store
       where store = L_location
      union all
      select vat_region
        from wh
       where wh = L_location;

   cursor C_GET_REVERSE_VAT_IND is
      select NVL(vi.reverse_vat_ind,'N')
        from vat_item vi
       where vi.item = L_item
         and vi.vat_region = L_vat_region
         and vi.vat_type in ('C','B')
         and active_date <= L_invc_date
    order by active_date desc;

   cursor C_GET_REV_THRESH is
      select NVL(reverse_vat_threshold,0)
        from vat_region
       where vat_region = L_vat_region;


BEGIN

   SQL_LIB.SET_MARK('OPEN','C_SHIPMENT','shipment','order_no: '||TO_CHAR(I_order_no));
   open C_SHIPMENT;

   SQL_LIB.SET_MARK('FETCH','C_SHIPMENT','shipment','order_no: '||TO_CHAR(I_order_no));
   fetch C_SHIPMENT into L_shipment,
                         L_bill_to_loc,
                         L_bill_to_loc_type;

   if C_SHIPMENT%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_SHIPMENT','shipment','order_no: '||TO_CHAR(I_order_no));
      close C_SHIPMENT;
      O_error_message := SQL_LIB.CREATE_MSG('SHIP_NO_ORDER',
                                            NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_SHIPMENT','shipment','order_no: '||TO_CHAR(I_order_no));
   close C_SHIPMENT;

   if L_bill_to_loc is NOT NULL then
      L_location := L_bill_to_loc;
      L_loc_type := L_bill_to_loc_type;
   else
      L_location := I_store;
      L_loc_type := 'S';
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                              L_default_tax_type) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'invc_xref',NULL);
   insert into invc_xref (INVC_ID,
                         ORDER_NO,
                         SHIPMENT,
                         ASN,
                         LOCATION,
                         LOC_TYPE,
                         APPLY_TO_FUTURE_IND)
                 values (I_invc_id,
                         I_order_no,
                         L_shipment,
                         NULL,
                         L_location,
                         L_loc_type,
                         'N');
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_INVC_DATE',
                    'invc_head',
                    'Invoice: '||to_char(I_invc_id));
   open C_GET_INVC_DATE;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_INVC_DATE',
                    'invc_head',
                    'Invoice: '||to_char(I_invc_id));
   fetch C_GET_INVC_DATE into L_invc_date,L_supplier;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_INVC_DATE',
                    'invc_head',
                    'Invoice: '||to_char(I_invc_id));
   close C_GET_INVC_DATE;
   ---
   if L_supplier is NULL then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_ORD_BILL_TO',
                       'ordhead',
                       'Order No: '||I_order_no);
      open C_GET_ORD_BILL_TO;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_ORD_BILL_TO',
                       'ordhead',
                       'Order No: '||I_order_no);
      fetch C_GET_ORD_BILL_TO into L_supplier;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_ORD_BILL_TO',
                       'ordhead',
                       'Order No: '||I_order_no);
      close C_GET_ORD_BILL_TO;
      ---
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_VAT_REGION', 'store,wh', 'location: '||to_char(L_location));
   open C_GET_VAT_REGION;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_VAT_REGION', 'store,wh', 'location: '||to_char(L_location));
   fetch C_GET_VAT_REGION into L_vat_region;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_VAT_REGION', 'store,wh', 'location: '||to_char(L_location));
   close C_GET_VAT_REGION;

   -- Get threshold value for the VAT_REGION
   SQL_LIB.SET_MARK('OPEN', 'C_GET_REV_THRESH', 'vat_region', 'vat_region: '||to_char(L_vat_region));
   open C_GET_REV_THRESH;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_REV_THRESH', 'vat_region', 'vat_region: '||to_char(L_vat_region));
   fetch C_GET_REV_THRESH into L_threshold_val;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_REV_THRESH', 'vat_region', 'vat_region: '||to_char(L_vat_region));
   close C_GET_REV_THRESH;

   --Looping through the collection to get the total cost for items with reverse_vat_ind = 'Y'
   if I_item_invc_details is not null and I_item_invc_details.COUNT > 0 then
      L_total_rev_vat_cost  := 0;
      L_rev_vat_item_tbl.DELETE();
      for i in I_item_invc_details.FIRST..I_item_invc_details.LAST LOOP
         L_item                := I_item_invc_details(i).item;
         L_unit_cost           := I_item_invc_details(i).unit_cost;
         L_qty_received        := I_item_invc_details(i).qty_received;
         L_reverse_vat_ind     := 'N';
         SQL_LIB.SET_MARK('OPEN', 'C_GET_REVERSE_VAT_IND', 'vat_item', 'item: '||to_char(L_item) || ', vat_region: ' || to_char(L_vat_region));
         open C_GET_REVERSE_VAT_IND;
         SQL_LIB.SET_MARK('FETCH', 'C_GET_REVERSE_VAT_IND', 'vat_item', 'item: '||to_char(L_item) || ', vat_region: ' || to_char(L_vat_region));
         fetch C_GET_REVERSE_VAT_IND into L_reverse_vat_ind;
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_REVERSE_VAT_IND', 'vat_item', 'item: '||to_char(L_item) || ', vat_region: ' || to_char(L_vat_region));
         close C_GET_REVERSE_VAT_IND;
         if L_reverse_vat_ind = 'Y' then
            L_total_rev_vat_cost := L_total_rev_vat_cost + (L_unit_cost * L_qty_received);
            --Maintain the list of these items in a collection
            L_rev_vat_item_tbl.EXTEND();
            L_rev_vat_item_tbl(L_rev_vat_item_tbl.count) := L_item;
         end if;
      END LOOP;
   end if;

   -- If total vat cost > Threshold value then set Vate_rate  = 0 for items with reverse_vat_ind = 'Y'
   if L_total_rev_vat_cost > L_threshold_val then
      L_zero_tax_code := NULL;
      SQL_LIB.SET_MARK('OPEN', 'C_GET_ZERO_RATE_TAX_CODE', 'vat_code_rates', 'vat_rate : 0');
      open C_GET_ZERO_RATE_TAX_CODE;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_ZERO_RATE_TAX_CODE', 'vat_code_rates', 'vat_rate : 0');
      fetch C_GET_ZERO_RATE_TAX_CODE into L_zero_tax_code;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_ZERO_RATE_TAX_CODE', 'vat_code_rates', 'vat_rate : 0');
      close C_GET_ZERO_RATE_TAX_CODE;
      L_thresh_vat_rate := 0;
   end if;

   if I_item_invc_details is not null and I_item_invc_details.COUNT > 0 then
      for i in I_item_invc_details.FIRST..I_item_invc_details.LAST LOOP
         L_item         := I_item_invc_details(i).item;
         L_unit_cost    := I_item_invc_details(i).unit_cost;
         L_qty_received := I_item_invc_details(i).qty_received;
         L_status       := I_item_invc_details(i).status;
         ---
         if L_default_tax_type in ('SVAT','GTAX') then
            L_tax_calc_rec.I_item                := I_item_invc_details(i).item;
            L_tax_calc_rec.I_from_entity         := L_supplier;
            L_tax_calc_rec.I_from_entity_type    := 'SP';
            L_tax_calc_rec.I_to_entity           := I_store;
            L_tax_calc_rec.I_to_entity_type      := 'ST';
            L_tax_calc_rec.I_effective_from_date := L_invc_date;
            L_tax_calc_rec.I_origin_country_id   := NULL;
            ---
            L_tax_calc_rec.I_tran_type           := 'CREATEINVOICE';
            L_tax_calc_rec.I_cost_retail_ind     := 'C';
            L_tax_calc_rec.I_tran_id             := I_invc_id;
            L_tax_calc_rec.I_tran_date           := L_invc_date;
            L_tax_calc_rec.I_amount              := (L_unit_cost * L_qty_received);
            ---
            if ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY(O_error_message,
                                                        L_exists,
                                                        L_tax_calc_rec.I_origin_country_id,
                                                        I_order_no,
                                                        I_item_invc_details(i).item) = FALSE then
               return FALSE;
            end if;
            ---
            L_tax_calc_tbl.DELETE();
            L_tax_calc_tbl.EXTEND();
            L_tax_calc_tbl(L_tax_calc_tbl.COUNT) := L_tax_calc_rec;

            if TAX_SQL.CALC_COST_TAX (O_error_message,
                                      L_tax_calc_tbl) = FALSE then
               return FALSE;
            end if;

            L_vat_rate := L_tax_calc_tbl(L_tax_calc_tbl.count).O_cum_tax_pct;

            L_tax_calc_detail_tbl := L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl;
            ---
            if L_tax_calc_detail_tbl is NOT NULL and L_tax_calc_detail_tbl.count > 0 then
               L_vat_code := L_tax_calc_detail_tbl(L_tax_calc_detail_tbl.count).tax_code;
            end if;
            ---
            -- Set vat_rate to zero if L_thresh_vat_rate = 0
            if NVL(L_thresh_vat_rate,1) = 0 and(L_item MEMBER OF L_rev_vat_item_tbl) then
               L_vat_code := L_zero_tax_code;
               L_vat_rate := 0;
            end if;
            ---
            open C_INVC_MERCH_VAT_CHECK;
            fetch C_INVC_MERCH_VAT_CHECK into L_exist;
            close C_INVC_MERCH_VAT_CHECK;

            if L_exist = 'Y' then
               SQL_LIB.SET_MARK('UPDATE',NULL,'invc_merch_vat',NULL);
               update invc_merch_vat
                  set total_cost_excl_vat = total_cost_excl_vat + (L_unit_cost * L_qty_received)
                where invc_id = I_invc_id
                  and vat_code = L_vat_code;
            else
               SQL_LIB.SET_MARK('INSERT',NULL,'invc_merch_vat',NULL);
               insert into invc_merch_vat(invc_id,
                                          vat_code,
                                          total_cost_excl_vat)
                                   values(I_invc_id,
                                          L_vat_code,
                                          L_unit_cost * L_qty_received);
            end if;
         end if;
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'invc_detail',NULL);
         insert into invc_detail (invc_id,
                                  item,
                                  ref_item,
                                  invc_unit_cost,
                                  invc_qty,
                                  invc_vat_rate,
                                  status,
                                  orig_unit_cost,
                                  orig_qty,
                                  orig_vat_rate,
                                  cost_dscrpncy_ind,
                                  qty_dscrpncy_ind,
                                  vat_dscrpncy_ind,
                                  processed_ind,
                                  comments,
                                  vat_code)
                          values (I_invc_id,
                                  L_item,
                                  NULL,
                                  L_unit_cost,
                                  L_qty_received,
                                  L_vat_rate,
                                  L_status,
                                  NULL,
                                  NULL,
                                  NULL,
                                  'N',
                                  'N',
                                  'N',
                                  'N',
                                  NULL,
                                  L_vat_code);
      END LOOP; --I_item_invc_details.FIRST
   else
      L_shipsku_tbl.DELETE();
      SQL_LIB.SET_MARK('OPEN',
                       'C_SHIPSKU',
                       'shipsku',
                       'Shipment: '||L_shipment);
      open C_SHIPSKU;
      SQL_LIB.SET_MARK('FETCH',
                       'C_SHIPSKU',
                       'shipsku',
                       'Shipment: '||L_shipment);
      fetch C_SHIPSKU bulk collect into L_shipsku_tbl;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SHIPSKU',
                       'shipsku',
                       'Shipment: '||L_shipment);
      close C_SHIPSKU;

      if L_shipsku_tbl is not null and L_shipsku_tbl.COUNT > 0 then
         L_total_rev_vat_cost  := 0;
         L_rev_vat_item_tbl.DELETE();
         for i in L_shipsku_tbl.FIRST..L_shipsku_tbl.LAST LOOP
            L_item                := L_shipsku_tbl(i).item;
            L_unit_cost           := L_shipsku_tbl(i).unit_cost;
            L_qty_received        := L_shipsku_tbl(i).qty_received;
            L_reverse_vat_ind     := 'N';
            SQL_LIB.SET_MARK('OPEN', 'C_GET_REVERSE_VAT_IND', 'vat_item', 'item: '||to_char(L_item) || ', vat_region: ' || to_char(L_vat_region));
            open C_GET_REVERSE_VAT_IND;
            SQL_LIB.SET_MARK('FETCH', 'C_GET_REVERSE_VAT_IND', 'vat_item', 'item: '||to_char(L_item) || ', vat_region: ' || to_char(L_vat_region));
            fetch C_GET_REVERSE_VAT_IND into L_reverse_vat_ind;
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_REVERSE_VAT_IND', 'vat_item', 'item: '||to_char(L_item) || ', vat_region: ' || to_char(L_vat_region));
            close C_GET_REVERSE_VAT_IND;
            if L_reverse_vat_ind = 'Y' then
               L_total_rev_vat_cost := L_total_rev_vat_cost + (L_unit_cost * L_qty_received);
               --Maintain the list of these items in a collection
               L_rev_vat_item_tbl.EXTEND();
               L_rev_vat_item_tbl(L_rev_vat_item_tbl.count) := L_item;
            end if;
         END LOOP;
      end if;

      -- If total vat cost > Threshold value then set Vate_rate  = 0 for items with reverse_vat_ind = 'Y'
      if L_total_rev_vat_cost > L_threshold_val then
         L_zero_tax_code := NULL;
         SQL_LIB.SET_MARK('OPEN', 'C_GET_ZERO_RATE_TAX_CODE', 'vat_code_rates', 'vat_rate : 0');
         open C_GET_ZERO_RATE_TAX_CODE;
         SQL_LIB.SET_MARK('FETCH', 'C_GET_ZERO_RATE_TAX_CODE', 'vat_code_rates', 'vat_rate : 0');
         fetch C_GET_ZERO_RATE_TAX_CODE into L_zero_tax_code;
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_ZERO_RATE_TAX_CODE', 'vat_code_rates', 'vat_rate : 0');
         close C_GET_ZERO_RATE_TAX_CODE;
         L_thresh_vat_rate := 0;
      end if;

      if L_default_tax_type in ('SVAT','GTAX') then
         L_tax_calc_tbl.DELETE();

         for i in L_shipsku_tbl.FIRST..L_shipsku_tbl.LAST LOOP

            L_tax_calc_rec.I_item                := L_shipsku_tbl(i).item;
            L_tax_calc_rec.I_from_entity         := L_supplier;
            L_tax_calc_rec.I_from_entity_type    := 'SP';
            L_tax_calc_rec.I_to_entity           := I_store;
            L_tax_calc_rec.I_to_entity_type      := 'ST';
            L_tax_calc_rec.I_effective_from_date := L_invc_date;
            L_tax_calc_rec.I_origin_country_id   := NULL;
            ---
            L_tax_calc_rec.I_tran_type           := 'CREATEINVOICE';
            L_tax_calc_rec.I_cost_retail_ind     := 'C';
            L_tax_calc_rec.I_tran_id             := I_invc_id;
            L_tax_calc_rec.I_tran_date           := L_invc_date;
            L_tax_calc_rec.I_amount              := (L_shipsku_tbl(i).unit_cost * L_shipsku_tbl(i).qty_received);
            ---
            if ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY(O_error_message,
                                                        L_exists,
                                                        L_tax_calc_rec.I_origin_country_id,
                                                        I_order_no,
                                                        L_tax_calc_rec.I_item) = FALSE then
               return FALSE;
            end if;

            L_tax_calc_tbl.EXTEND();
            L_tax_calc_tbl(L_tax_calc_tbl.COUNT) := L_tax_calc_rec;

         END LOOP;

         if TAX_SQL.CALC_COST_TAX (O_error_message,
                                   L_tax_calc_tbl) = FALSE then
            return FALSE;
         end if;

         ---
         for i in L_tax_calc_tbl.FIRST..L_tax_calc_tbl.LAST LOOP
            -- Set vat_rate to zero if L_thresh_vat_rate = 0
            if (NVL(L_thresh_vat_rate,1) = 0 and (L_tax_calc_tbl(i).I_item MEMBER OF L_rev_vat_item_tbl))  then
               L_tax_calc_tbl(i).O_cum_tax_pct := 0;
               L_tax_calc_detail_tbl := L_tax_calc_tbl(i).O_tax_detail_tbl;
               L_tax_calc_detail_tbl(L_tax_calc_detail_tbl.count).tax_code := L_zero_tax_code;
               L_tax_calc_tbl(i).O_tax_detail_tbl := L_tax_calc_detail_tbl;
            end if;
         END LOOP;

         ---

         SQL_LIB.SET_MARK('MERGE',NULL,'invc_merch_vat',NULL);
         merge into invc_merch_vat i
          using (select tax_dtl.tax_code tax_code,
                        SUM(ss.unit_cost * ss.qty_received) total_vat
                   from TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax,
                        TABLE(O_tax_detail_tbl) tax_dtl,
                        TABLE(CAST(L_shipsku_tbl as OBJ_SHIPSKU_TBL)) ss
                  where ss.shipment = L_shipment
                    and tax.I_item = ss.item
                    group by tax_code) use_this
             on (i.invc_id = I_invc_id and
                 i.vat_code = use_this.tax_code)
          when matched then
             update
                set total_cost_excl_vat = total_cost_excl_vat + use_this.total_vat
          when not matched then
             insert (invc_id,
                     vat_code,
                          total_cost_excl_vat)
             values (I_invc_id,
                     use_this.tax_code,
                     use_this.total_vat);

      end if; -- L_default_tax_type in ('SVAT','GTAX')

      if L_default_tax_type in ('SVAT','GTAX') then
         SQL_LIB.SET_MARK('INSERT',NULL,'invc_detail',NULL);
         merge into invc_detail i
          using (select tax.I_item item,
                        tax_dtl.tax_code,
                        tax.O_cum_tax_pct tax_rate,
                        ss.ref_item,
                        ss.unit_cost,
                        ss.qty_received
                   from TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax,
                        TABLE(O_tax_detail_tbl) tax_dtl,
                        TABLE(CAST(L_shipsku_tbl as OBJ_SHIPSKU_TBL)) ss
                  where ss.shipment = L_shipment
                    and tax.I_item = ss.item) use_this
             on (i.invc_id = I_invc_id and
                 i.item = use_this.item)
           when not matched then
                insert (invc_id,
                        item,
                        ref_item,
                        invc_unit_cost,
                        invc_qty,
                        invc_vat_rate,
                        status,
                        orig_unit_cost,
                        orig_qty,
                        orig_vat_rate,
                        cost_dscrpncy_ind,
                        qty_dscrpncy_ind,
                        vat_dscrpncy_ind,
                        processed_ind,
                        comments,
                        vat_code)
                values (I_invc_id,
                        use_this.item,
                        use_this.ref_item,
                        use_this.unit_cost,
                        use_this.qty_received,
                        use_this.tax_rate,
                        'M',
                        NULL,
                        NULL,
                        NULL,
                        'N',
                        'N',
                        'N',
                        'N',
                        NULL,
                        use_this.tax_code);
      else                                                     -- For default_tax_type = SALES
         SQL_LIB.SET_MARK('INSERT',NULL,'invc_detail',NULL);
         merge into invc_detail i
          using (select ss.item item,
                        ss.ref_item,
                        ss.unit_cost,
                        ss.qty_received
                   from TABLE(CAST(L_shipsku_tbl as OBJ_SHIPSKU_TBL)) ss
                  where ss.shipment = L_shipment) use_this
             on (i.invc_id = I_invc_id and
                 i.item = use_this.item)
           when not matched then
                insert (invc_id,
                        item,
                        ref_item,
                        invc_unit_cost,
                        invc_qty,
                        invc_vat_rate,
                        status,
                        orig_unit_cost,
                        orig_qty,
                        orig_vat_rate,
                        cost_dscrpncy_ind,
                        qty_dscrpncy_ind,
                        vat_dscrpncy_ind,
                        processed_ind,
                        comments,
                        vat_code)
                values (I_invc_id,
                        use_this.item,
                        use_this.ref_item,
                        use_this.unit_cost,
                        use_this.qty_received,
                        NULL,
                        'M',
                        NULL,
                        NULL,
                        NULL,
                        'N',
                        'N',
                        'N',
                        'N',
                        NULL,
                        NULL);
      end if;
   end if; -- I_item_invc_detail

   --
   -- Update total_merch_cost of INVC_HEAD table with SUM of
   -- (invc_unit_cost * invc_qty) from INVC_DETAIL table for Invoice No.
   SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_MERCH_COST','invc_detail','invc_no: '||TO_CHAR(I_invc_id));
   open C_GET_TOTAL_MERCH_COST;

   SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_MERCH_COST','invc_detail','invc_no: '||TO_CHAR(I_invc_id));
   fetch C_GET_TOTAL_MERCH_COST into L_tot_merch_invc_cost;

   if C_GET_TOTAL_MERCH_COST%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_MERCH_COST','invc_detail','invc_no: '||TO_CHAR(I_invc_id));
      close C_GET_TOTAL_MERCH_COST;

      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_MERCH_COST','invc_detail','invc_no: '||TO_CHAR(I_invc_id));
   close C_GET_TOTAL_MERCH_COST;

   update invc_head
      set total_merch_cost = L_tot_merch_invc_cost
    where invc_id = I_invc_id;
   --

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END INVOICE_ITEM;
-------------------------------------------------------------------------------
FUNCTION CHECK_INVC_DETAIL(O_error_message  IN OUT VARCHAR2,
                           O_exists         IN OUT BOOLEAN,
                           I_invc_id        IN     INVC_DETAIL.INVC_ID%TYPE)
RETURN BOOLEAN IS

  L_exist        VARCHAR2(1)  := 'N';
  L_program      VARCHAR2(50) := 'DIRECT_STORE_INVOICE_SQL.CHECK_INVC_DETAIL';

  cursor C_INVC_DETAIL is
     select 'Y'
       from invc_detail
      where invc_id = I_invc_id;
BEGIN

  O_exists := FALSE;
  ---
  SQL_LIB.SET_MARK('OPEN','C_INVC_DETAIL','invc_detail','invc_id: '||TO_CHAR(I_invc_id));
  open C_INVC_DETAIL;

  SQL_LIB.SET_MARK('FETCH','C_INVC_DETAIL','invc_detail','invc_id: '||TO_CHAR(I_invc_id));
  fetch C_INVC_DETAIL into L_exist;

  if L_exist = 'Y' then
     O_exists := TRUE;
  end if;

  SQL_LIB.SET_MARK('CLOSE','C_INVC_DETAIL','invc_detail','invc_id: '||TO_CHAR(I_invc_id));
  close C_INVC_DETAIL;

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_INVC_DETAIL;
-------------------------------------------------------------------------------
FUNCTION CHECK_INVC_DUPS(O_error_message        IN OUT VARCHAR2,
                         O_invc_id              IN OUT INVC_HEAD.INVC_ID%TYPE,
                         I_invc_id              IN     INVC_HEAD.INVC_ID%TYPE,
                         I_supplier         IN     INVC_HEAD.SUPPLIER%TYPE,
                         I_partner_type         IN     INVC_HEAD.PARTNER_TYPE%TYPE,
                         I_partner_id           IN     INVC_HEAD.PARTNER_ID%TYPE,
                         I_invc_date            IN     INVC_HEAD.INVC_DATE%TYPE,
                         I_ext_ref_no           IN     INVC_HEAD.EXT_REF_NO%TYPE,
                         I_proof_of_delivery_no IN     INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                         I_payment_ref_no       IN     INVC_HEAD.PAYMENT_REF_NO%TYPE,
                         I_payment_date         IN     INVC_HEAD.PAYMENT_DATE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'DIRECT_STORE_INVOICE_SQL.CHECK_INVC_DUPS';

   cursor C_INVC_ID is
      select invc_id
        from invc_head
       where invc_id                      != nvl(I_invc_id, -1)
         and ((supplier                    = I_supplier
               and I_partner_type          is NULL
               and I_partner_id            is NULL)
             or (partner_type              = I_partner_type
                 and partner_id            = I_partner_id
                 and I_supplier            is NULL))
         and ((invc_date                   = I_invc_date)
             or (payment_date              = I_payment_date))
         and invc_type                     in ('I','N','O')
         and (((ext_ref_no                 = I_ext_ref_no)
             and (proof_of_delivery_no     = I_proof_of_delivery_no)
             and (payment_ref_no           = I_payment_ref_no))
          or ((ext_ref_no                  = I_ext_ref_no)
             and (proof_of_delivery_no     = I_proof_of_delivery_no)
             and (I_payment_ref_no         is NULL))
          or ((proof_of_delivery_no        = I_proof_of_delivery_no)
             and (payment_ref_no           = I_payment_ref_no)
             and (I_ext_ref_no             is NULL))
          or ((ext_ref_no                  = I_ext_ref_no)
             and (payment_ref_no           = I_payment_ref_no)
             and (I_proof_of_delivery_no   is NULL))
          or ((ext_ref_no                  = I_ext_ref_no)
             and (I_proof_of_delivery_no   is NULL)
             and (I_payment_ref_no         is NULL))
          or ((proof_of_delivery_no        = I_proof_of_delivery_no)
             and (I_ext_ref_no             is NULL)
             and (I_payment_ref_no         is NULL))
          or ((payment_ref_no              = I_payment_ref_no)
             and (I_ext_ref_no             is NULL)
             and (I_proof_of_delivery_no   is NULL))
          or ((ext_ref_no                  is NULL)
             and (proof_of_delivery_no     is NULL)
             and (payment_ref_no           = I_payment_ref_no))
          or ((proof_of_delivery_no        is NULL)
             and (payment_ref_no           is NULL)
             and (ext_ref_no               = I_ext_ref_no))
          or ((ext_ref_no                  is NULL)
             and (payment_ref_no           is NULL)
             and (proof_of_delivery_no     = I_proof_of_delivery_no))
          or ((ext_ref_no                  is NULL)
             and (proof_of_delivery_no     = I_proof_of_delivery_no)
             and (payment_ref_no           = I_payment_ref_no))
          or ((proof_of_delivery_no        is NULL)
             and (ext_ref_no               = I_ext_ref_no)
             and (payment_ref_no           = I_payment_ref_no))
          or ((payment_ref_no              is NULL)
             and (ext_ref_no               = I_ext_ref_no)
             and (proof_of_delivery_no     = I_proof_of_delivery_no)));

BEGIN

  if O_invc_id is not NULL then
     O_invc_id := NULL;
  end if;

  SQL_LIB.SET_MARK('OPEN','C_INVC_ID','invc_head','supplier: '||TO_CHAR(I_supplier));
  open C_INVC_ID;

  SQL_LIB.SET_MARK('FETCH','C_INVC_ID','invc_head','supplier: '||TO_CHAR(I_supplier));
  fetch C_INVC_ID into O_invc_id;

  SQL_LIB.SET_MARK('CLOSE','C_INVC_ID','invc_head','supplier: '||TO_CHAR(I_supplier));
  close C_INVC_ID;

  return TRUE;

EXCEPTION
  when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
     return FALSE;
END CHECK_INVC_DUPS;
-------------------------------------------------------------------------------
FUNCTION INVC_NON_MERCH_TEMP_EXIST(O_error_message   IN OUT VARCHAR2,
                                   O_exists          IN OUT BOOLEAN,
                                   I_non_merch_code  IN     NON_MERCH_CODE_HEAD.NON_MERCH_CODE%TYPE,
                                   I_invc_id         IN     INVC_NON_MERCH.INVC_ID%TYPE)

RETURN BOOLEAN IS

  L_exists          VARCHAR2(1);
  L_program         VARCHAR2(60) := 'DIRECT_STORE_INVOICE_SQL.INVC_NON_MERCH_TEMP_EXIST';

  cursor C_INVC_NON_MERCH_EXIST is
     select 'Y'
       from  invc_non_merch_temp
      where  non_merch_code  = I_non_merch_code
        and  invc_id         = I_invc_id;

BEGIN

  SQL_LIB.SET_MARK('OPEN','C_INVC_NON_MERCH_EXIST','invc_non_merch',NULL);
  open C_INVC_NON_MERCH_EXIST;

  SQL_LIB.SET_MARK('FETCH','C_INVC_NON_MERCH_EXIST','invc_non_merch',NULL);
  fetch C_INVC_NON_MERCH_EXIST into L_exists;

  if C_INVC_NON_MERCH_EXIST%NOTFOUND then
     O_exists := FALSE;
  else
     O_exists := TRUE;
  end if;

  SQL_LIB.SET_MARK('CLOSE','C_INVC_NON_MERCH_EXIST','invc_non_merch',NULL);
  close C_INVC_NON_MERCH_EXIST;

  return TRUE;

EXCEPTION
  when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
     RETURN FALSE;
END INVC_NON_MERCH_TEMP_EXIST;
----------------------------------------------------------------------------------
FUNCTION CREATE_INVOICE_WRP(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_invc_id                IN OUT   INVC_HEAD.INVC_ID%TYPE,
                            I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                            I_supplier               IN       SUPS.SUPPLIER%TYPE,
                            I_vdate                  IN       PERIOD.VDATE%TYPE,
                            I_total_cost             IN       INVC_HEAD.TOTAL_MERCH_COST%TYPE,
                            I_total_qty              IN       INVC_HEAD.TOTAL_QTY%TYPE,
                            I_store                  IN       STORE.STORE%TYPE,
                            I_paid_ind               IN       INVC_HEAD.PAID_IND%TYPE,
                            I_ext_ref_no             IN       INVC_HEAD.EXT_REF_NO%TYPE,
                            I_proof_of_delivery_no   IN       INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                            I_payment_ref_no         IN       INVC_HEAD.PAYMENT_REF_NO%TYPE,
                            I_payment_date           IN       INVC_HEAD.PAYMENT_DATE%TYPE,
                            I_invc_status            IN       INVC_DETAIL.STATUS%TYPE               DEFAULT NULL,
                            I_item_invc_details      IN       WRP_ITEM_INVC_TBL                     DEFAULT NULL)
RETURN INTEGER IS

   L_program                      VARCHAR2(64):= 'DIRECT_STORE_INVOICE_SQL.CREATE_INVOICE_WRP';
   L_item_invc_details            DIRECT_STORE_INVOICE_SQL.ITEM_INVC_TBL:= new DIRECT_STORE_INVOICE_SQL.ITEM_INVC_TBL();

BEGIN
   if I_item_invc_details.count > 0 and I_item_invc_details is not null then
      FOR i IN 1..I_item_invc_details.COUNT LOOP
         L_item_invc_details.extend();
         L_item_invc_details(i).item          := I_item_invc_details(i).item;
         L_item_invc_details(i).qty_received  := I_item_invc_details(i).qty_received;
         L_item_invc_details(i).unit_cost     := I_item_invc_details(i).unit_cost;
         L_item_invc_details(i).status        := I_item_invc_details(i).status;
      END LOOP;

      if DIRECT_STORE_INVOICE_SQL.CREATE_INVOICE (O_error_message,
                                                  O_invc_id,
                                                  I_order_no,
                                                  I_supplier,
                                                  I_vdate,
                                                  I_total_cost,
                                                  I_total_qty,
                                                  I_store,
                                                  I_paid_ind,
                                                  I_ext_ref_no,
                                                  I_proof_of_delivery_no,
                                                  I_payment_ref_no,
                                                  I_payment_date,
                                                  I_invc_status,
                                                  L_item_invc_details) = FALSE then
         return 0;
      end if;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;
END CREATE_INVOICE_WRP;
----------------------------------------------------------------------------------
END DIRECT_STORE_INVOICE_SQL;
/