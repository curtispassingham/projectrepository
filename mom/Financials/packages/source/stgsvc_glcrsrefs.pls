CREATE OR REPLACE PACKAGE STG_SVC_SA_FIF_GL_CROSS_REF AUTHID CURRENT_USER
AS
	template_key           CONSTANT VARCHAR2(255):='SA_FIF_GL_CROSS_REF_DATA';
	action_new             VARCHAR2(25)          := 'NEW';
	action_mod             VARCHAR2(25)          := 'MOD';
	action_del             VARCHAR2(25)          := 'DEL';
	SA_FIF_GL_CROSS_REF_sheet             VARCHAR2(255)         := 'SA_FIF_GL_CROSS_REF';
	SA_GL_CROSS$Action           NUMBER                :=1;
	SA_GL_CROSS$CR_CCID            NUMBER                :=19;
	SA_GL_CROSS$DR_SEQUENCE10            NUMBER                :=18;
	SA_GL_CROSS$DR_SEQUENCE9            NUMBER                :=17;
	SA_GL_CROSS$DR_SEQUENCE8            NUMBER                :=16;
	SA_GL_CROSS$DR_SEQUENCE7            NUMBER                :=15;
	SA_GL_CROSS$DR_SEQUENCE6            NUMBER                :=14;
	SA_GL_CROSS$DR_SEQUENCE5            NUMBER                :=13;
	SA_GL_CROSS$DR_SEQUENCE4            NUMBER                :=12;
	SA_GL_CROSS$DR_SEQUENCE3            NUMBER                :=11;
	SA_GL_CROSS$DR_SEQUENCE2            NUMBER                :=10;
	SA_GL_CROSS$DR_SEQUENCE1            NUMBER                :=9;
	SA_GL_CROSS$DR_CCID            NUMBER                :=8;
	SA_GL_CROSS$SET_OF_BOOKS_ID            NUMBER                :=7;
	SA_GL_CROSS$TOTAL_ID            NUMBER                :=3;
	SA_GL_CROSS$CR_SEQUENCE20            NUMBER                :=49;
	SA_GL_CROSS$ROLLUP_LEVEL_3            NUMBER                :=6;
	SA_GL_CROSS$ROLLUP_LEVEL_2            NUMBER                :=5;
	SA_GL_CROSS$ROLLUP_LEVEL_1            NUMBER                :=4;
	SA_GL_CROSS$STORE            NUMBER                :=2;
	SA_GL_CROSS$CR_SEQUENCE19            NUMBER                :=48;
	SA_GL_CROSS$CR_SEQUENCE18            NUMBER                :=47;
	SA_GL_CROSS$CR_SEQUENCE17            NUMBER                :=46;
	SA_GL_CROSS$CR_SEQUENCE16            NUMBER                :=45;
	SA_GL_CROSS$CR_SEQUENCE15            NUMBER                :=44;
	SA_GL_CROSS$CR_SEQUENCE14            NUMBER                :=43;
	SA_GL_CROSS$CR_SEQUENCE13            NUMBER                :=42;
	SA_GL_CROSS$CR_SEQUENCE12            NUMBER                :=41;
	SA_GL_CROSS$CR_SEQUENCE11            NUMBER                :=40;
	SA_GL_CROSS$DR_SEQUENCE20            NUMBER                :=39;
	SA_GL_CROSS$DR_SEQUENCE19            NUMBER                :=38;
	SA_GL_CROSS$DR_SEQUENCE18            NUMBER                :=37;
	SA_GL_CROSS$DR_SEQUENCE17            NUMBER                :=36;
	SA_GL_CROSS$DR_SEQUENCE16            NUMBER                :=35;
	SA_GL_CROSS$DR_SEQUENCE15            NUMBER                :=34;
	SA_GL_CROSS$DR_SEQUENCE14            NUMBER                :=33;
	SA_GL_CROSS$DR_SEQUENCE13            NUMBER                :=32;
	SA_GL_CROSS$DR_SEQUENCE12            NUMBER                :=31;
	SA_GL_CROSS$DR_SEQUENCE11            NUMBER                :=30;
	SA_GL_CROSS$CR_SEQUENCE10            NUMBER                :=29;
	SA_GL_CROSS$CR_SEQUENCE9            NUMBER                :=28;
	SA_GL_CROSS$CR_SEQUENCE8            NUMBER                :=27;
	SA_GL_CROSS$CR_SEQUENCE7            NUMBER                :=26;
	SA_GL_CROSS$CR_SEQUENCE6            NUMBER                :=25;
	SA_GL_CROSS$CR_SEQUENCE5            NUMBER                :=24;
	SA_GL_CROSS$CR_SEQUENCE4            NUMBER                :=23;
	SA_GL_CROSS$CR_SEQUENCE3            NUMBER                :=22;
	SA_GL_CROSS$CR_SEQUENCE2            NUMBER                :=21;
	SA_GL_CROSS$CR_SEQUENCE1            NUMBER                :=20;

	Type SA_FIF_GL_CROSS_REF_rec_tab
	IS
	TABLE OF SA_FIF_GL_CROSS_REF%rowtype;
	FUNCTION create_s9t(
		O_error_message     IN OUT rtk_errors.rtk_text%type,
		O_file_id           IN OUT s9t_folder.file_id%type,
		I_template_only_ind IN CHAR DEFAULT 'N') RETURN BOOLEAN;
	FUNCTION process_s9t(
		O_error_message IN OUT rtk_errors.rtk_text%type ,
		I_file_id       IN s9t_folder.file_id%type,
		I_process_id IN NUMBER,
		O_error_count OUT NUMBER) RETURN BOOLEAN;

	 sheet_name_trans s9t_pkg.trans_map_typ;
END STG_SVC_SA_FIF_GL_CROSS_REF;
/ 