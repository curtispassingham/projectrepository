
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_GLCACCT AUTHID CURRENT_USER AS
---------------------------------------------------------------
FUNCTION CONSUME(O_error_message        OUT     VARCHAR2,  
                 I_message              IN      CLOB)
return BOOLEAN;  
---------------------------------------------------------------------
END;
/