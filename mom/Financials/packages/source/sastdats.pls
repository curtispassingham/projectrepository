
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STORE_DATA_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
-- Function Name: STORE_DATA_EXISTS
-- Purpose      : This function will check the sa_store_data table to see if a record 
--                for the store/system_code/imp_exp already exists on the database.                
-------------------------------------------------------------------------------------
FUNCTION STORE_DATA_EXISTS(O_error_message  IN OUT  VARCHAR2,
		           O_exists         IN OUT  BOOLEAN,
                           I_store          IN      SA_STORE_DATA.STORE%TYPE,
                           I_system_code    IN      SA_STORE_DATA.SYSTEM_CODE %TYPE,
                           I_imp_exp        IN      SA_STORE_DATA.IMP_EXP%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: CHECK_FUEL_STORE
-- Purpose      : This function will check the sa_store_data table to see if a passed
--                in store sells fuel (i.e. a record exists where the system_code 
--                is 'SFM' (SFM Export) for the store with an imp_exp of 'E').                
-------------------------------------------------------------------------------------
FUNCTION CHECK_FUEL_STORE(O_error_message  IN OUT  VARCHAR2,
		          O_fuel_store_ind IN OUT  VARCHAR2,
                          I_store          IN      STORE.STORE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: GET_TRAN_NO_GENERATED
-- Purpose      : Returns the value for the tran_no_generated field on the store table
--                for the passed in store.
-------------------------------------------------------------------------------------
FUNCTION GET_TRAN_NO_GENERATED(O_error_message     IN OUT VARCHAR2,
                               O_tran_no_generated IN OUT STORE.TRAN_NO_GENERATED%TYPE,
                               I_store             IN     STORE.STORE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: CHECK_MLA_STORE
-- Purpose      : This function will check the sa_store_data table to see if a passed
--                in store uses multi-level audit (if the system code is 'MLA').          
-------------------------------------------------------------------------------------
FUNCTION CHECK_MLA_STORE(O_error_message  IN OUT  VARCHAR2,
		         O_mla_store_ind  IN OUT  VARCHAR2,
                         I_store          IN      STORE.STORE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
END STORE_DATA_SQL;
/
