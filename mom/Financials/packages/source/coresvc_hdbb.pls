CREATE OR REPLACE PACKAGE BODY CORESVC_HDB as
----------------------------------------------------------------------------------------------
   cursor C_SVC_HALF_DATA_BUDGET(I_process_id   NUMBER,
                                 I_chunk_id     NUMBER) is
      select pk_hdb.rowid            as pk_hdb_rid,
             st.rowid                as st_rid, 
             st.markdown_pct,
             pk_hdb.markdown_pct     as  h_markdown_pct,
             st.shrinkage_pct,
             pk_hdb.shrinkage_pct    as  h_shrinkage_pct,
             st.cum_markon_pct,
             pk_hdb.cum_markon_pct   as  h_cum_markon_pct,
             st.location,
             UPPER(st.loc_type)      as  loc_type,
             st.half_no,
             st.dept,
             st.set_of_books_id,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_half_data_budget st,
             half_data_budget pk_hdb,
             dual
       where st.process_id       = I_process_id
         and st.chunk_id         = I_chunk_id
         and st.set_of_books_id  = pk_hdb.set_of_books_id (+)
         and st.location         = pk_hdb.location (+)
         and UPPER(st.loc_type)  = pk_hdb.loc_type (+)
         and st.half_no          = pk_hdb.half_no (+)
         and st.dept             = pk_hdb.dept (+);
         
   Type errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   Type s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;   
   Lp_errors_tab       errors_tab_typ;
   Lp_s9t_errors_tab   s9t_errors_tab_typ;
----------------------------------------------------------------------------------------------  
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                         CASE
                                                                            WHEN I_sqlcode IS NULL THEN
                                                                               I_sqlerrm
                                                                            ELSE
                                                                               'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                            END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE) IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
  
END WRITE_ERROR;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES (I_file_id   NUMBER) IS
   L_sheets   s9t_pkg.names_map_typ;
   HDB_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets            :=s9t_pkg.get_sheet_names(I_file_id);
   HDB_cols            :=s9t_pkg.get_coL_names(I_file_id,
                                               HDB_sheet);
   HDB$Action          := HDB_cols('ACTION');
   HDB$markdown_pct    := HDB_cols('MARKDOWN_PCT');
   HDB$shrinkage_pct   := HDB_cols('SHRINKAGE_PCT');
   HDB$cum_markon_pct  := HDB_cols('CUM_MARKON_PCT');
   HDB$location        := HDB_cols('LOCATION');
   HDB$loc_type        := HDB_cols('LOC_TYPE');
   HDB$half_no         := HDB_cols('HALF_NO');
   HDB$dept            := HDB_cols('DEPT');
   HDB$set_of_books_id := HDB_cols('SET_OF_BOOKS_ID');
END POPULATE_NAMES;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HDB(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = HDB_sheet)
               select s9t_row(s9t_cells(CORESVC_HDB.action_mod,
                                        dept,
                                        half_no,
                                        loc_type,
                                        location,
                                        set_of_books_id,
                                        cum_markon_pct,
                                        shrinkage_pct,
                                        markdown_pct))
                 from HALF_DATA_BUDGET;
END POPULATE_HDB;
----------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(HDB_sheet);
   L_file.sheets(L_file.get_sheet_index(HDB_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                 'DEPT',
                                                                                 'HALF_NO',
                                                                                 'LOC_TYPE',
                                                                                 'LOCATION',
                                                                                 'SET_OF_BOOKS_ID',
                                                                                 'CUM_MARKON_PCT',
                                                                                 'SHRINKAGE_PCT',
                                                                                 'MARKDOWN_PCT');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HDB.CREATE_S9T';
   L_file      s9t_file; 
   
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_HDB(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM,  
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HDB(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_HALF_DATA_BUDGET.process_id%TYPE) IS
                                       
   Type svc_HALF_DATA_BUDGET_coL_typ IS TABLE OF SVC_HALF_DATA_BUDGET%ROWTYPE;
   L_temp_rec                 SVC_HALF_DATA_BUDGET%ROWTYPE;
   svc_HALF_DATA_BUDGET_col   svc_HALF_DATA_BUDGET_coL_typ := NEW svc_HALF_DATA_BUDGET_coL_typ();
   L_process_id               SVC_HALF_DATA_BUDGET.process_id%TYPE;
   L_error                    BOOLEAN := FALSE;
   L_default_rec              SVC_HALF_DATA_BUDGET%ROWTYPE;
   cursor c_mandatory_ind is
      select MARKDOWN_PCT_mi,
             SHRINKAGE_PCT_mi,
             CUM_MARKON_PCT_mi,
             LOCATION_mi,
             LOC_TYPE_mi,
             HALF_NO_mi,
             DEPT_mi,
             SET_OF_BOOKS_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_HDB.template_key
                 and wksht_key = 'HDB') 
               PIVOT (MAX(mandatory) as mi 
                  FOR (column_key) IN ('MARKDOWN_PCT'    as markdown_pct,
                                       'SHRINKAGE_PCT'   as shrinkage_pct,
                                       'CUM_MARKON_PCT'  as cum_markon_pct,
                                       'LOCATION'        as location,
                                       'LOC_TYPE'        as loc_type,
                                       'HALF_NO'         as half_no,
                                       'DEPT'            as dept,
                                       'SET_OF_BOOKS_ID' as set_of_books_id,
                                       NULL              as dummy));
                                       
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_HALF_DATA_BUDGET';
   L_pk_columns    VARCHAR2(255)  := 'Department,Half,Location Type,Location,Set of books ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
   
BEGIN
  -- Get default values.
   FOR rec IN(select MARKDOWN_PCT_dv,
                     SHRINKAGE_PCT_dv,
                     CUM_MARKON_PCT_dv,
                     LOCATION_dv,
                     LOC_TYPE_dv,
                     HALF_NO_dv,
                     DEPT_dv,
                     SET_OF_BOOKS_ID_dv,
                     NULL as dummy
                from (select column_key,
                             default_value
                        from s9t_tmpL_cols_def
                       where template_key  = CORESVC_HDB.template_key
                         and wksht_key = 'HDB') 
                       PIVOT (MAX(default_value) as dv 
                          FOR (column_key) IN ('MARKDOWN_PCT'    as markdown_pct,
                                               'SHRINKAGE_PCT'   as shrinkage_pct,
                                               'CUM_MARKON_PCT'  as cum_markon_pct,
                                               'LOCATION'        as location,
                                               'LOC_TYPE'        as loc_type,
                                               'HALF_NO'         as half_no,
                                               'DEPT'            as dept,
                                               'SET_OF_BOOKS_ID' as set_of_books_id,
                                               NULL              as dummy)))
 
   LOOP  
      BEGIN  
         L_default_rec.markdown_pct := rec.MARKDOWN_PCT_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'HDB',
                         NULL,
                         'MARKDOWN_PCT',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.shrinkage_pct := rec.SHRINKAGE_PCT_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'HDB',
                         NULL,
                         'SHRINKAGE_PCT',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.cum_markon_pct := rec.CUM_MARKON_PCT_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'HDB',
                         NULL,
                         'CUM_MARKON_PCT',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
      L_default_rec.location := rec.LOCATION_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'HDB',
                         NULL,
                         'LOCATION',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.loc_type := rec.LOC_TYPE_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'HDB',
                         NULL,
                         'LOC_TYPE',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.half_no := rec.HALF_NO_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'HDB',
                         NULL,
                         'HALF_NO',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.dept := rec.DEPT_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'HDB',
                         NULL,
                         'DEPT',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
      BEGIN  
         L_default_rec.set_of_books_id := rec.SET_OF_BOOKS_ID_dv;  
      EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR(I_file_id,
                         'HDB',
                         NULL,
                         'SET_OF_BOOKS_ID',
                         'INV_DEFAULT',
                         SQLERRM);  
      END;
   END LOOP;
   
   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select UPPER(r.get_cell(HDB$Action))    as   Action,
                     r.get_cell(HDB$markdown_pct)     as   markdown_pct,
                     r.get_cell(HDB$shrinkage_pct)    as   shrinkage_pct,
                     r.get_cell(HDB$cum_markon_pct)   as   cum_markon_pct,
                     r.get_cell(HDB$location)         as   location,
                     UPPER(r.get_cell(HDB$loc_type))  as   loc_type,
                     r.get_cell(HDB$half_no)          as   half_no,
                     r.get_cell(HDB$dept)             as   dept,
                     r.get_cell(HDB$set_of_books_id)  as   set_of_books_id,
                     r.get_row_seq()                  as   row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(HDB_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         action_column,
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.markdown_pct := rec.markdown_pct;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         'MARKDOWN_PCT',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.shrinkage_pct := rec.shrinkage_pct;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         'SHRINKAGE_PCT',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cum_markon_pct := rec.cum_markon_pct;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         'CUM_MARKON_PCT',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.location := rec.location;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         'LOCATION',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.loc_type := rec.loc_type;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         'LOC_TYPE',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.half_no := rec.half_no;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         'HALF_NO',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.dept := rec.dept;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         'DEPT',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.set_of_books_id := rec.set_of_books_id;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         'SET_OF_BOOKS_ID',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      if (L_temp_rec.set_of_books_id is NULL or
          L_temp_rec.location is NULL or
          L_temp_rec.loc_type is NULL or
          L_temp_rec.half_no is NULL or
          L_temp_rec.dept is NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         HDB_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_HALF_DATA_BUDGET_col.extend();
         svc_HALF_DATA_BUDGET_col(svc_HALF_DATA_BUDGET_col.count()):=L_temp_rec;
      end if;
   END LOOP;
   
   BEGIN
      forall i IN 1..svc_HALF_DATA_BUDGET_col.count SAVE EXCEPTIONS 
      merge into SVC_HALF_DATA_BUDGET st 
      using(select(case
                   when L_mi_rec.MARKDOWN_PCT_mi = 'N' 
                    and svc_HALF_DATA_BUDGET_col(i).action = coresvc_hdb.action_mod 
                    and s1.markdown_pct is NULL then
                        mt.markdown_pct
                   else s1.markdown_pct
                   end) as markdown_pct,
                  (case
                   when L_mi_rec.SHRINKAGE_PCT_mi = 'N' 
                    and svc_HALF_DATA_BUDGET_col(i).action = coresvc_hdb.action_mod 
                    and s1.shrinkage_pct is NULL then
                        mt.shrinkage_pct
                   else s1.shrinkage_pct
                   end) as shrinkage_pct,
                  (case
                   when L_mi_rec.CUM_MARKON_PCT_mi = 'N' 
                    and svc_HALF_DATA_BUDGET_col(i).action = coresvc_hdb.action_mod 
                    and s1.cum_markon_pct is NULL then
                        mt.cum_markon_pct
                   else s1.cum_markon_pct
                   end) as cum_markon_pct,
                  (case
                   when L_mi_rec.LOCATION_mi = 'N' 
                    and svc_HALF_DATA_BUDGET_col(i).action = coresvc_hdb.action_mod 
                    and s1.location is NULL then
                        mt.location
                   else s1.location
                   end) as location,
                  (case
                   when L_mi_rec.LOC_TYPE_mi = 'N' 
                    and svc_HALF_DATA_BUDGET_col(i).action = coresvc_hdb.action_mod 
                    and s1.loc_type is NULL then 
                        mt.loc_type
                   else s1.loc_type
                   end) as loc_type,
                  (case
                   when L_mi_rec.HALF_NO_mi = 'N'  
                    and svc_HALF_DATA_BUDGET_col(i).action = coresvc_hdb.action_mod 
                    and s1.half_no is NULL then
                        mt.half_no
                   else s1.half_no
                   end) as half_no,
                  (case
                   when L_mi_rec.DEPT_mi = 'N' 
                    and svc_HALF_DATA_BUDGET_col(i).action = coresvc_hdb.action_mod 
                    and s1.dept is NULL then
                        mt.dept
                   else s1.dept
                   end) as dept,
                  (case
                   when L_mi_rec.SET_OF_BOOKS_ID_mi = 'N' 
                    and svc_HALF_DATA_BUDGET_col(i).action = coresvc_hdb.action_mod 
                    and s1.set_of_books_id is NULL then
                        mt.set_of_books_id
                   else s1.set_of_books_id
                   end) as set_of_books_id,
                  NULL as dummy
             from (select svc_HALF_DATA_BUDGET_col(i).markdown_pct    as markdown_pct,
                          svc_HALF_DATA_BUDGET_col(i).shrinkage_pct   as shrinkage_pct,
                          svc_HALF_DATA_BUDGET_col(i).cum_markon_pct  as cum_markon_pct,
                          svc_HALF_DATA_BUDGET_col(i).location        as location,
                          svc_HALF_DATA_BUDGET_col(i).loc_type        as loc_type,
                          svc_HALF_DATA_BUDGET_col(i).half_no         as half_no,
                          svc_HALF_DATA_BUDGET_col(i).dept            as dept,
                          svc_HALF_DATA_BUDGET_col(i).set_of_books_id as set_of_books_id,
                          NULL as dummy
                     from dual) s1,
                  HALF_DATA_BUDGET mt
            where mt.set_of_books_id (+) = s1.set_of_books_id   
              and mt.location (+)        = s1.location   
              and mt.loc_type (+)        = s1.loc_type   
              and mt.half_no (+)         = s1.half_no   
              and mt.dept (+)            = s1.dept) sq 
              ON (st.set_of_books_id = sq.set_of_books_id and
                  st.location = sq.location and
                  st.loc_type = sq.loc_type and
                  st.half_no = sq.half_no and
                  st.dept = sq.dept and  
                  SVC_HALF_DATA_BUDGET_col(i).action in (CORESVC_HDB.action_mod,CORESVC_HDB.action_del))
      when matched then
      update 
         set process_id        = svc_HALF_DATA_BUDGET_col(i).process_id ,
             chunk_id          = svc_HALF_DATA_BUDGET_col(i).chunk_id ,
             row_seq           = svc_HALF_DATA_BUDGET_col(i).row_seq ,
             action            = SVC_HALF_DATA_BUDGET_col(i).ACTION,
             PROCESS$STATUS    = svc_HALF_DATA_BUDGET_col(i).PROCESS$STATUS ,
             cum_markon_pct    = sq.cum_markon_pct ,
             shrinkage_pct     = sq.shrinkage_pct ,
             markdown_pct      = sq.markdown_pct ,
             CREATE_ID         = svc_HALF_DATA_BUDGET_col(i).CREATE_ID ,
             CREATE_DATETIME   = svc_HALF_DATA_BUDGET_col(i).CREATE_DATETIME ,
             LAST_UPD_ID       = svc_HALF_DATA_BUDGET_col(i).LAST_UPD_ID ,
             LAST_UPD_DATETIME = svc_HALF_DATA_BUDGET_col(i).LAST_UPD_DATETIME 
      when NOT matched then
      insert (process_id ,
              chunk_id ,
              row_seq ,
              ACTION ,
              PROCESS$STATUS ,
              markdown_pct ,
              shrinkage_pct ,
              cum_markon_pct ,
              location ,
              loc_type ,
              half_no ,
              dept ,
              set_of_books_id ,
              CREATE_ID ,
              CREATE_DATETIME ,
              LAST_UPD_ID ,
              LAST_UPD_DATETIME)
      values (svc_HALF_DATA_BUDGET_col(i).process_id ,
              svc_HALF_DATA_BUDGET_col(i).chunk_id ,
              svc_HALF_DATA_BUDGET_col(i).row_seq ,
              svc_HALF_DATA_BUDGET_col(i).ACTION ,
              svc_HALF_DATA_BUDGET_col(i).PROCESS$STATUS ,
              sq.markdown_pct ,
              sq.shrinkage_pct ,
              sq.cum_markon_pct ,
              sq.location ,
              sq.loc_type ,
              sq.half_no ,
              sq.dept ,
              sq.set_of_books_id ,
              svc_HALF_DATA_BUDGET_col(i).CREATE_ID ,
              svc_HALF_DATA_BUDGET_col(i).CREATE_DATETIME ,
              svc_HALF_DATA_BUDGET_col(i).LAST_UPD_ID ,
              svc_HALF_DATA_BUDGET_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
         WRITE_S9T_ERROR( I_file_id,
                          HDB_sheet,
                          svc_half_data_budget_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);                            
         END LOOP;
   END;
END PROCESS_S9T_HDB; 
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_HDB.PROCESS_S9T';   
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	      EXCEPTION;
   PRAGMA	      EXCEPTION_INIT(MAX_CHAR, -01706);   
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_HDB(I_file_id,
                      I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   --Update process$status in svc_process_tracker
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);
      
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HDB_VAL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error              IN OUT   BOOLEAN,
                         O_GROSS_MARGIN_PCT   IN       NUMBER,
                         I_rec                IN       C_SVC_HALF_DATA_BUDGET%ROWTYPE) 
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) := 'CORESVC_HDB.PROCESS_HDB_VAL'; 
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HALF_DATA_BUDGET';
   L_length_dec   NUMBER;

BEGIN
   if I_rec.cum_markon_pct is NOT NULL then
      if I_rec.cum_markon_pct <= 99999999.99  
         and I_rec.cum_markon_pct >= 0 then
         L_length_dec := length(I_rec.cum_markon_pct - trunc(I_rec.cum_markon_pct)) - 1;
         if L_length_dec > 2 then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'CUM_MARKON_PCT',
                        'INV_HDB_FRMT');
            O_error:=TRUE;
         end if;
      else 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CUM_MARKON_PCT',
                     'VALID_RATE_RANGE');
         O_error := TRUE;
      end if;      
   end if;
 
   if I_rec.shrinkage_pct is NOT NULL then
      if I_rec.shrinkage_pct <= 99999999.99  
         and I_rec.shrinkage_pct >= 0 then
         L_length_dec := length(I_rec.shrinkage_pct - trunc(I_rec.shrinkage_pct)) - 1;
         if L_length_dec > 2 then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SHRINKAGE_PCT',
                        'INV_HDB_FRMT');
            O_error:=TRUE;
         end if;
      else 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SHRINKAGE_PCT',
                     'VALID_RATE_RANGE');
         O_error := TRUE;
      end if;      
   end if;         
   
   if I_rec.markdown_pct is NOT NULL then
      if I_rec.markdown_pct <= 99999999.99 
         and I_rec.markdown_pct >= 0 then
         L_length_dec := length(I_rec.markdown_pct - trunc(I_rec.markdown_pct)) - 1;
         if L_length_dec > 2 then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'MARKDOWN_PCT',
                        'INV_HDB_FRMT');
            O_error:=TRUE;
         end if;
      else 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MARKDOWN_PCT',
                     'VALID_RATE_RANGE');
         O_error := TRUE;
      end if;      
   end if;
   
   if O_GROSS_MARGIN_PCT >  99999999.99   
      or O_GROSS_MARGIN_PCT < -99999999.99 then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.nextval,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'Markup%,Markdown%,Shrinkage%',
                  'INV_GROSS');
      O_error :=TRUE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
									
      return FALSE;
END PROCESS_HDB_VAL;
---------------------------------------------------------------------------------------
FUNCTION EXEC_HDB_POST_UPD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_hdb_temp_rec    IN       HALF_DATA_BUDGET%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HDB.EXEC_HDB_POST_UPD';   
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HALF_DATA_BUDGET';      
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_MDB_UPD is
      select 'x'
        from MONTH_DATA_BUDGET
       where dept     = I_hdb_temp_rec.dept
         and location = I_hdb_temp_rec.location
         and half_no  = I_hdb_temp_rec.half_no       
         for update nowait;

BEGIN
   open  C_LOCK_MDB_UPD;
   close C_LOCK_MDB_UPD;   
   
   update MONTH_DATA_BUDGET
      set gross_margin = trunc(net_sales_retail * I_hdb_temp_rec.gross_margin_pct / 100)
    where dept     = I_hdb_temp_rec.dept
      and location = I_hdb_temp_rec.location
      and half_no  = I_hdb_temp_rec.half_no;   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_hdb_temp_rec.dept,
                                             I_hdb_temp_rec.half_no);
      return FALSE;
   when OTHERS then
      if C_LOCK_MDB_UPD%ISOPEN then
         close C_LOCK_MDB_UPD;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program, 
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_HDB_POST_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_HDB_UPD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_process_error   IN OUT   BOOLEAN,
                      I_hdb_temp_rec    IN       HALF_DATA_BUDGET%ROWTYPE,
                      I_rec             IN       C_SVC_HALF_DATA_BUDGET%ROWTYPE) 
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HDB.EXEC_HDB_UPD'; 
   L_svc_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HALF_DATA_BUDGET'; 
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HALF_DATA_BUDGET';    
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_HDB_UPD is
      select 'x'
        from HALF_DATA_BUDGET
       where set_of_books_id = I_hdb_temp_rec.set_of_books_id
         and location        = I_hdb_temp_rec.location
         and loc_type        = I_hdb_temp_rec.loc_type
         and half_no         = I_hdb_temp_rec.half_no
         and dept            = I_hdb_temp_rec.dept    
         for update nowait;

BEGIN
   open  C_LOCK_HDB_UPD;
   close C_LOCK_HDB_UPD;   
   
   update HALF_DATA_BUDGET 
      set row = I_hdb_temp_rec 
    where set_of_books_id = I_hdb_temp_rec.set_of_books_id
      and location        = I_hdb_temp_rec.location
      and loc_type        = I_hdb_temp_rec.loc_type
      and half_no         = I_hdb_temp_rec.half_no
      and dept            = I_hdb_temp_rec.dept;
      
   if EXEC_HDB_POST_UPD(O_error_message,
                        I_hdb_temp_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.nextval,
                  I_rec.chunk_id,
                  L_svc_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_process_error := TRUE;  
      
      ROLLBACK TO do_process; 	
   end if;           
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_hdb_temp_rec.dept,
                                             I_hdb_temp_rec.half_no);
      return FALSE;
   when OTHERS then
      if C_LOCK_HDB_UPD%ISOPEN then
         close C_LOCK_HDB_UPD;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program, 
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_HDB_UPD;
--------------------------------------------------------------------------------
FUNCTION PROCESS_HDB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id      IN       SVC_HALF_DATA_BUDGET.process_id%TYPE,
                     I_chunk_id        IN       SVC_HALF_DATA_BUDGET.chunk_id%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64) := 'CORESVC_HDB.PROCESS_HDB';  
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HALF_DATA_BUDGET';
   L_error              BOOLEAN;
   L_process_error      BOOLEAN := FALSE;
   L_HDB_temp_rec       HALF_DATA_BUDGET%ROWTYPE;
   L_GROSS_MARGIN_PCT   NUMBER(16,4);
   L_mod                BOOLEAN ;

BEGIN
   FOR rec IN C_SVC_HALF_DATA_BUDGET(I_process_id,
                                     I_chunk_id)
   LOOP
      L_mod := FALSE;   
      L_error := FALSE;
      if rec.action is NULL 
         or rec.action != action_mod then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action = action_mod 
         and rec.PK_HDB_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Department,Half,Location,Location Type,Set of books ID',
                     'NO_RECORD');
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_HDB_temp_rec.set_of_books_id  := rec.set_of_books_id;
         L_HDB_temp_rec.dept             := rec.dept;
         L_HDB_temp_rec.half_no          := rec.half_no;
         L_HDB_temp_rec.loc_type         := rec.loc_type;
         L_HDB_temp_rec.location         := rec.location;
         L_HDB_temp_rec.cum_markon_pct   := rec.cum_markon_pct;
         L_HDB_temp_rec.shrinkage_pct    := rec.shrinkage_pct;
         L_HDB_temp_rec.markdown_pct     := rec.markdown_pct;
         if rec.cum_markon_pct is NOT NULL 
            or rec.markdown_pct is NOT NULL 
            or rec.shrinkage_pct is NOT NULL then         
            L_GROSS_MARGIN_PCT := NVL(rec.cum_markon_pct,0) - NVL(rec.markdown_pct,0) - NVL(rec.shrinkage_pct,0);
         else
            L_HDB_temp_rec.GROSS_MARGIN_PCT := NULL;
         end if;
      
         if (((rec.H_cum_markon_pct <> rec.cum_markon_pct) 
            or NOT (rec.H_cum_markon_pct IS NULL and rec.cum_markon_pct IS NULL ))
            or((rec.H_shrinkage_pct <> rec.shrinkage_pct) 
            or NOT (rec.H_shrinkage_pct IS NULL and rec.shrinkage_pct IS NULL )) 
            or((rec.H_markdown_pct <> rec.markdown_pct) 
            or NOT (rec.H_markdown_pct IS NULL and rec.markdown_pct IS NULL ))) then 
            L_mod := TRUE;
         end if; 
	  
         if L_mod then 
            if PROCESS_HDB_VAL(O_error_message,
                               L_error,
                               L_GROSS_MARGIN_PCT,
                               rec) = FALSE then
               WRITE_ERROR(rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           rec.chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_error := TRUE;
            end if;
         end if;
      end if; --if NOT L_error  

      if NOT L_ERROR then
         L_HDB_temp_rec.GROSS_MARGIN_PCT := L_GROSS_MARGIN_PCT;
         SAVEPOINT do_process;
         if L_mod then 
            if EXEC_HDB_UPD(O_error_message,
                            L_process_error,
                            L_HDB_temp_rec,
                            rec) = FALSE then
               WRITE_ERROR(rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           rec.chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if; 
         end if;  --if L_mod
      end if;   --if NOT L_ERROR
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HDB;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_half_data_budget 
      where process_id = I_process_id;
END;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_HDB.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';  
   
BEGIN
   Lp_errors_tab := NEW errors_tab_typ();
   if PROCESS_HDB(O_error_message,
                  I_process_id,
                  I_chunk_id) = FALSE then
      return FALSE;
   end if; 
   O_error_count := Lp_errors_tab.count();
   forall i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER 
           values Lp_errors_tab(i);
   Lp_errors_tab := NEW errors_tab_typ();
   
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
	else
	   L_process_status := 'PE';
	end if;
		
   update svc_process_tracker
		  set status = (CASE
			              when status = 'PE'
			              then 'PE'
                    else L_process_status
                    END),
		      action_date = SYSDATE
		where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;

EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM,  
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------
END CORESVC_HDB;
/