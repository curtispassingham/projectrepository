CREATE OR REPLACE PACKAGE CORESVC_MDB AUTHID CURRENT_USER AS
   template_key                CONSTANT VARCHAR2(255) := 'MDB_DATA';
   template_category           CONSTANT VARCHAR2(255) := 'RMSBUD';
   action_new                  VARCHAR2(25) := 'NEW';
   action_mod                  VARCHAR2(25) := 'MOD';
   action_del                  VARCHAR2(25) := 'DEL';
   MDB_sheet                   VARCHAR2(255):= 'MDB';
   MDB$Action                  NUMBER       := 1;
   MDB$OPN_STK_RETAIL          NUMBER       := 8;
   MDB$LOCATION                NUMBER       := 6;
   MDB$LOC_TYPE                NUMBER       := 5;
   MDB$HALF_NO                 NUMBER       := 4;
   MDB$MONTH_NO                NUMBER       := 3;
   MDB$DEPT                    NUMBER       := 2;
   MDB$GROSS_MARGIN            NUMBER       := 26;
   MDB$CLS_STK_COST            NUMBER       := 25;
   MDB$CLS_STK_RETAIL          NUMBER       := 24;
   MDB$EMPL_DISC_RETAIL        NUMBER       := 23;
   MDB$SHRINKAGE_COST          NUMBER       := 22;
   MDB$SHRINKAGE_RETAIL        NUMBER       := 21;
   MDB$PROM_MARKDOWN_RETAIL    NUMBER       := 18;
   MDB$PERM_MARKDOWN_RETAIL    NUMBER       := 17;
   MDB$CLEAR_MARKDOWN_RETAIL   NUMBER       := 16;
   MDB$NET_SALES_COST          NUMBER       := 15;
   MDB$NET_SALES_RETAIL        NUMBER       := 14;
   MDB$RTV_COST                NUMBER       := 13;
   MDB$RTV_RETAIL              NUMBER       := 12;
   MDB$PURCH_COST              NUMBER       := 11;
   MDB$PURCH_RETAIL            NUMBER       := 10;
   MDB$OPN_STK_COST            NUMBER       := 9;
   MDB$SET_OF_BOOKS_ID         NUMBER       := 7;
                                                       
   Type MDB_rec_tab IS TABLE OF MONTH_DATA_BUDGET%ROWTYPE;
   sheet_name_trans   S9T_PKG.trans_map_typ;
   action_column      VARCHAR2(255) := 'ACTION';
----------------------------------------------------------------------------------  
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
----------------------------------------------------------------------------------
END CORESVC_MDB;
/