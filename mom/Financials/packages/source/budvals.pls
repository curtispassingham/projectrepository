
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BUDGET_VALIDATE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
-- Name:     DEPT_EXISTS
-- Purpose:  Validates that the department passed to the function is
--           valid and that the department exists on month_data_budget.
--           Returns the name of the department to the calling program.
-- Created By: Amy Selby, 13-SEP-96.
---------------------------------------------------------------------
FUNCTION DEPT_EXISTS(i_dept        IN     NUMBER,
                     o_dept_name   IN OUT VARCHAR2,
                     o_exists      IN OUT BOOLEAN,
                     error_message IN OUT VARCHAR2) RETURN BOOLEAN;
---------------------------------------------------------------------
-- Name:     HALF_EXISTS
-- Purpose:  Validates that the half number passed to the function is
--           valid and that the half exists on month_data_budget for
--           the given department.
--           Returns the name of the half to the calling program.
-- Created By: Amy Selby, 13-SEP-96.
---------------------------------------------------------------------
FUNCTION HALF_EXISTS(i_dept        IN     NUMBER,
                     i_half        IN     NUMBER,
                     i_half_name   IN OUT VARCHAR2,
                     o_exists      IN OUT BOOLEAN,
                     error_message IN OUT VARCHAR2) RETURN BOOLEAN;
---------------------------------------------------------------------
-- Name:     STORE_EXISTS
-- Purpose:  Validates that the store passed to the function is
--           valid and that the store exists on month_data_budget for
--           the given dept and half_no.
--           Returns the name of the store to the calling program.
-- Created By: Amy Selby, 13-SEP-96.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_STORE V_WH
-- which only returns data that the user has permission to access.  
---------------------------------------------------------------------
FUNCTION STORE_EXISTS(i_dept        IN     NUMBER,
                      i_half        IN     NUMBER,
                      i_location    IN     NUMBER,
                      i_loc_type    IN     VARCHAR2,
                      i_store_name  IN OUT VARCHAR2,
                      o_exists      IN OUT BOOLEAN,
                      error_message IN OUT VARCHAR2) RETURN BOOLEAN;

---------------------------------------------------------------------
-- Name:    WH_EXISTS
-- Purpose:  Validates that the warehouse passed to the function is
--           valid and that the warehouse exists on month_data_budget for
--           the given dept and half_no.
--           Returns the name of the warehouse to the calling program.
-- Created By: Amy Selby, 13-SEP-96.
---------------------------------------------------------------------
FUNCTION WH_EXISTS(i_dept        IN     NUMBER,
                   i_half        IN     NUMBER,
                   i_location    IN     NUMBER,
                   i_loc_type    In     VARCHAR2,
                   i_wh_name     IN OUT VARCHAR2,
                   o_exists      IN OUT BOOLEAN,
                   error_message IN OUT VARCHAR2) RETURN BOOLEAN;

-------------------------------------------------------------------
-- Name:     HALF_DATA_BUDGET_EXISTS
-- Purpose:  Validates that the half number passed to the function is
--           valid and that the half exists on half_data_budget for
--           the given department.
--           Returns the name of the half to the calling program.
-- Created By: Victor Savritski, 02-DEC-98.
---------------------------------------------------------------------
FUNCTION HALF_DATA_BUDGET_EXISTS(i_dept        IN     NUMBER,
                                 i_half        IN     NUMBER,
                                 i_half_name   IN OUT VARCHAR2,
                                 o_exists      IN OUT BOOLEAN,
                                 error_message IN OUT VARCHAR2) RETURN BOOLEAN;
-------------------------------------------------------------------
-- Name:     GET_SHRINKAGE_DTLS 
-- Purpose:  Fetch the shrink percentage from half data budget table for the given
--           half no,dept and store or warehouse.
---------------------------------------------------------------------
FUNCTION GET_SHRINKAGE_DTLS(i_dept          IN     HALF_DATA_BUDGET.DEPT%TYPE,
                            i_half          IN     HALF_DATA_BUDGET.HALF_NO%TYPE,
                            i_loc_type      IN     HALF_DATA_BUDGET.LOC_TYPE%TYPE,
                            i_location      IN     HALF_DATA_BUDGET.LOCATION%TYPE,
                            o_shrink_pct    IN OUT HALF_DATA_BUDGET.SHRINKAGE_PCT%TYPE,
                            o_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE) RETURN BOOLEAN;
-------------------------------------------------------------------
-- Name:     EXT_FIN_EXISTS
-- Purpose:  Validates that the external finisher passed to the function is
--           valid and that the external finisher exists on month_data_budget for
--           the given dept and half_no.
--           Returns the name of the external finisher to the calling program.
-- Created By: Gerard John B. Paras, 18-JUL-03.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER
-- which only returns data that the user has permission to access.  
---------------------------------------------------------------------
FUNCTION EXT_FIN_EXISTS(i_dept         IN     NUMBER,
                        i_half         IN     NUMBER,
                        i_location     IN     NUMBER,
                        i_loc_type     IN     VARCHAR2,
                        i_ext_fin_name IN OUT VARCHAR2,
                        o_exists       IN OUT BOOLEAN,
                        error_message  IN OUT VARCHAR2) RETURN BOOLEAN;

-------------------------------------------------------------------
-- Name:     INT_FIN_EXISTS
-- Purpose:  Validates that the internal finisher passed to the function is
--           valid and that the internal finisher exists on month_data_budget for
--           the given dept and half_no.
--           Returns the name of the internal finisher to the calling program.
-- Created By: Gerard John B. Paras, 07-AUG-03.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_INTERNAL_FINISHER
-- which only returns data that the user has permission to access.  
---------------------------------------------------------------------
FUNCTION INT_FIN_EXISTS(i_dept         IN     NUMBER,
                        i_half         IN     NUMBER,
                        i_location     IN     NUMBER,
                        i_loc_type     IN     VARCHAR2,
                        i_int_fin_name IN OUT VARCHAR2,
                        o_exists       IN OUT BOOLEAN,
                        error_message  IN OUT VARCHAR2) RETURN BOOLEAN;

END;
/
