CREATE OR REPLACE PACKAGE INVC_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
-- Function Name : LOCK_INVC
-- Purpose:        Attempts to lock the invoice header record. This function will
--                 return TRUE if the lock is successful, or FALSE if the record has
--                 been locked by a different user and therefore is unalterable.
-- Created:        10-20-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION LOCK_INVC(O_error_message   IN OUT  VARCHAR2,
                   O_lock_ind        IN OUT  BOOLEAN,
                   I_invc_id         IN      INVC_HEAD.INVC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name : UPDATE_STATUSES
-- Purpose:        Determines the status of a passed invoice or receipt by checking the
--           individual statuses of all detail records associated with the passed
--              invoice/receipt. If all detail records have a status of 'M'(Matched),
--           thefunction will set the invoice/receipt header record's status to 'M'.
--                 If only some detail records are of 'M' status, 'R'(partially matched)
--                 will be assigned.If no detail records are matched, 'U'(unmatched) will
--        be assigned.
-- Created:        10-26-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION UPDATE_STATUSES(O_error_message   IN OUT  VARCHAR2,
                         I_invc_id         IN      INVC_HEAD.INVC_ID%TYPE,
                         I_rcpt            IN      SHIPMENT.SHIPMENT%TYPE,
                         I_user_id         IN      VARCHAR2,
                         I_supplier        IN      INVC_HEAD.SUPPLIER%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: REVERT
-- Purpose:       Reverts an entire invoice or single line item back to it's orignal
--          values, which are stored in INVC_DETAIL.
-- Created:       10-26-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION REVERT(O_error_message   IN OUT   VARCHAR2,
                I_invc_id         IN       INVC_HEAD.INVC_ID%TYPE,
                I_item            IN       INVC_DETAIL.ITEM%TYPE,
                I_invc_unit_cost  IN       INVC_DETAIL.INVC_UNIT_COST%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: APPLY_XREF
-- Purpose:       Inserts records consisting of received shipments
--                associated with the passed values into the INVC_XREF table.
-- Calls:         LOCATION_ATTRIB_SQL.GET_TYPE
-- Created:       10-29-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION APPLY_XREF(O_error_message        IN OUT  VARCHAR2,
                    I_invc_id              IN      INVC_XREF.INVC_ID%TYPE,
                    I_order_no             IN      SHIPMENT.ORDER_NO%TYPE,
                    I_asn_no               IN      SHIPMENT.ASN%TYPE,
                    I_to_loc               IN      SHIPMENT.TO_LOC%TYPE,
                    I_to_loc_type          IN      SHIPMENT.TO_LOC_TYPE%TYPE,
                    I_supplier             IN      INVC_HEAD.SUPPLIER%TYPE,
                    I_apply_to_future_ind  IN      INVC_XREF.APPLY_TO_FUTURE_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: DEFAULT_ITEMS
-- Purpose :      Inserts records into INVC_DETAIL for all ITEMSs associated with receipts
--                (received shipments) that are assoicated with I_invc_id in INVC_XREF.
-- Calls:         SYSTEM_OPTIONS_SQL.GET_VAT_IND, TAX_SQL.GET_TAX_REGION_DESC
--                TAX_SQL.GET_TAX_RATE
-- Created:       11-18-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION DEFAULT_ITEMS(O_error_message  IN OUT   VARCHAR2,
                       I_invc_id        IN       INVC_HEAD.INVC_ID%TYPE,
                       I_overwrite      IN       BOOLEAN,
                       I_vat_region     IN       VAT_REGION.VAT_REGION%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: NEXT_INVC_ID
-- Purpose:       Fetches the next number in INVC_SEQUENCE, assuring each new invoice a
--                unique invc_id.
-- Created:       10-27-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION NEXT_INVC_ID(O_error_message   IN OUT   VARCHAR2,
                      O_invc_id         IN OUT   INVC_HEAD.INVC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: DELETE_INVC
-- Purpose:       Deletes all records on the various invoice tables that are associated
--                with the passed invoice id (and any invoices that reference it - credit
--                notes, debit memos etc) and optional ITEM.  An invoice that is 'M'atched
--                or 'P'artially matched  and is a merchandise (invc_type 'I') or
--                non-merchandise invoice (invc_type 'N')cannot be deleted (i.e. passed
--                into this function)
-- Calls:         INVC_SQL.DELETE_INVC_WKSHT
-- Created:       10-28-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION DELETE_INVC(O_error_message  IN OUT  VARCHAR2,
                     I_invc_id        IN      INVC_HEAD.INVC_ID%TYPE,
                     I_item           IN      INVC_DETAIL.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: WRITE_INVC_TOL_TRAN_DATA
-- Purpose:       Writes the invoice tolerance data to the TRAN_DATA table.
-- Created:       09-28-01 Scott Eibensteiner
-------------------------------------------------------------------------------------
FUNCTION WRITE_INVC_TOL_TRAN_DATA(O_error_message     IN OUT   VARCHAR2,
                                  I_invc_id           IN       INVC_HEAD.INVC_ID%TYPE,
                                  I_vdate             IN       DATE,
                                  I_default_dept      IN       DEPS.DEPT%TYPE,
                                  I_default_class     IN       CLASS.CLASS%TYPE,
                                  I_default_subclass  IN       SUBCLASS.SUBCLASS%TYPE,
                                  I_default_store     IN       STORE.STORE%TYPE,
                                  I_default_wh        IN       WH.WH%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: DELETE_INVC_WKSHT
-- Purpose:       Deletes all records on INVC_MATCH_WKSHT that are associated with
--          I_invc_id and/or I_rcpt and/or I_item.
-- Created:       10-27-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION DELETE_INVC_WKSHT(O_error_message  IN OUT   VARCHAR2,
                           I_invc_id        IN       INVC_MATCH_WKSHT.INVC_ID%TYPE,
                           I_rcpt           IN       INVC_MATCH_WKSHT.SHIPMENT%TYPE,
                           I_item           IN       INVC_MATCH_WKSHT.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: INSERT_INVC_XREF
-- Purpose:       Inserts a record into INVC_XREF consisting of the passed values
--                and associated values from the shipment table.
-- Created:       10-28-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION INSERT_INVC_XREF(O_error_message   IN OUT   VARCHAR2,
                 I_invc_id   IN       INVC_XREF.INVC_ID%TYPE,
                 I_rcpt      IN       INVC_XREF.SHIPMENT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: CHECK_MATCH
-- Purpose:       Determines if a particular line item or any line item for a given
--          receipt has been matched to an invoice.
-- Created        10-29-98 Ryan McNamara
-------------------------------------------------------------------------------------
FUNCTION CHECK_MATCH(O_error_message   IN OUT   VARCHAR2,
                     O_matched         IN OUT   BOOLEAN,
                     I_invc_id         IN       INVC_MATCH_WKSHT.INVC_ID%TYPE,
                     I_rcpt            IN       INVC_MATCH_WKSHT.SHIPMENT%TYPE,
                     I_item            IN       INVC_MATCH_WKSHT.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: CLOSE_OPEN_SHIP
-- Purpose:       This function will close open shipments with matched invoices attached if
--                the matched invoices are in the appropriate statuses.
-- Calls:         None
-- Created:       02-19-99 Sonia Wong
-------------------------------------------------------------------------------------
FUNCTION CLOSE_OPEN_SHIP(O_error_message   IN OUT   VARCHAR2,
                         O_close_ind       IN OUT   BOOLEAN,
                         I_shipment        IN       SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--Function Name: CHECK_PO_TERMS
--Purpose:       This function will check to see if all the purchase orders on a given invoice
--               have the same terms.  If so, O_matching_terms will be set to TRUE and the
--               terms will be passed out in O_po_terms otherwise it will be set to FALSE.
--Created:       24-Feb-99
------------------------------------------------------------------------------------------
FUNCTION CHECK_PO_TERMS(O_error_message   IN OUT   VARCHAR2,
                        O_matching_terms  IN OUT   BOOLEAN,
                        O_po_terms        IN OUT   ORDHEAD.TERMS%TYPE,
                        I_invc_id         IN       INVC_HEAD.INVC_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
--Function Name:   OVERWRITE_INVC_TERMS
--Purpose:         This function will overwrite the invoice terms for a given invoice to be
--                 the terms that are passed in in I_new_terms.  If I_new_terms is NULL the
--                 function will call INVC_SQL.CHECK_PO_TERMS to see if the terms can be
--                 overwritten and retrieve the appropriate terms.  If the invoice terms are
--                 overwritten O_overwritten will return TRUE else it will return FALSE.
--Created:         25-Feb-99
----------------------------------------------------------------------------------------------
FUNCTION OVERWRITE_INVC_TERMS(O_error_message  IN OUT   VARCHAR2,
                              O_overwritten    IN OUT   BOOLEAN,
                              I_invc_id        IN       INVC_HEAD.INVC_ID%TYPE,
                              I_new_terms      IN       INVC_HEAD.TERMS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name:    UPDATE_INVOICE
--Purpose:          This function will be called by the PO receiving package to correctly
--                  update the invoice tables when an order is received.
--Created:          03-DEC-01
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_INVOICE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE,
                        I_location       IN     STORE.STORE%TYPE,
                        I_shipment       IN     SHIPMENT.SHIPMENT%TYPE,
                        I_carton         IN     CARTON.CARTON%TYPE,
                        I_receipt_qty    IN     SHIPSKU.QTY_RECEIVED%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END INVC_SQL;
/
