



CREATE OR REPLACE PACKAGE SA_TRANSACTION_SEQUENCE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
--- Function:  GET_NEXT_ITEM_SEQ_NO
--- Purpose:   Generates a new item_seq_no based on the maximum on the sa_tran_item
---            table for the passed tran_head_no (required).
--------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ITEM_SEQ_NO(O_error_message   IN OUT VARCHAR2,
                              O_item_seq_no     IN OUT SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                              I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_store           IN     SA_TRAN_ITEM.STORE%TYPE DEFAULT NULL,
                              I_day             IN     SA_TRAN_ITEM.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function:  GET_NEXT_TENDER_SEQ_NO
--- Purpose:   Generates a new tender_seq_no based on the maximum on the sa_tran_tender
---            table for the passed tran_head_no (required).
----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TENDER_SEQ_NO(O_error_message   IN OUT VARCHAR2,
                                O_tender_seq_no   IN OUT SA_TRAN_TENDER.TENDER_SEQ_NO%TYPE,
                                I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                I_store           IN     SA_TRAN_TENDER.STORE%TYPE DEFAULT NULL,
                                I_day             IN     SA_TRAN_TENDER.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function:  GET_NEXT_IGTAX_SEQ_NO
--- Purpose:   Generates a new igtax_seq_no based on the maximum on the sa_tran_igtax
---            table for the passed tran_head_no and item_seq_no(required).
----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_IGTAX_SEQ_NO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_tax_seq_no      IN OUT SA_TRAN_IGTAX.IGTAX_SEQ_NO%TYPE,
                               I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                               I_item_seq_no     IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                               I_store           IN     SA_TRAN_TAX.STORE%TYPE DEFAULT NULL,
                               I_day             IN     SA_TRAN_TAX.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function:  GET_NEXT_TAX_SEQ_NO
--- Purpose:   Generates a new tax_seq_no based on the maximum on the sa_tran_tax
---            table for the passed tran_head_no (required).
----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TAX_SEQ_NO(O_error_message  IN OUT VARCHAR2,
                             O_tax_seq_no     IN OUT SA_TRAN_TAX.TAX_SEQ_NO%TYPE,
                             I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                             I_store          IN     SA_TRAN_TAX.STORE%TYPE DEFAULT NULL,
                             I_day            IN     SA_TRAN_TAX.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function name: Get_next_disc_seq_no
--- Purpose:   Generates a new disc_seq_no based on the maximum on the sa_tran_disc
---            table for the passed tran_head_no and item_seq_no (required).
----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DISC_SEQ_NO(O_error_message  IN OUT VARCHAR2,
                              O_disc_seq_no    IN OUT SA_TRAN_DISC.DISCOUNT_SEQ_NO%TYPE,
                              I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                              I_item_seq_no    IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE,
                              I_store          IN     SA_TRAN_DISC.STORE%TYPE DEFAULT NULL,
                              I_day            IN     SA_TRAN_DISC.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function name: GET_NEXT_REV_NO
--- Purpose      : returns the next sequetial revision number for a transaction
-----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_REV_NO(O_error_message  IN OUT VARCHAR2,
                         O_next_rev_no    IN OUT SA_TRAN_HEAD.REV_NO%TYPE,
                         I_tran_seq_no    IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                         I_store          IN     SA_TRAN_HEAD_REV.STORE%TYPE DEFAULT NULL,
                         I_day            IN     SA_TRAN_HEAD_REV.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function:  GET_NEXT_ATTRIB_SEQ_NO
--- Purpose:   Generates a new attrib_seq_no based on the maximum on the sa_cust_attrib
---            table for the passed tran_seq_no (required).
----------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ATTRIB_SEQ_NO(O_error_message   IN OUT VARCHAR2,
                                O_attrib_seq_no   IN OUT SA_CUST_ATTRIB.ATTRIB_SEQ_NO%TYPE,
                                I_tran_seq_no     IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                                I_store           IN     SA_CUST_ATTRIB.STORE%TYPE DEFAULT NULL,
                                I_day             IN     SA_CUST_ATTRIB.DAY%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
END SA_TRANSACTION_SEQUENCE_SQL;
/
