
CREATE OR REPLACE PACKAGE BODY RMSSUB_GLCACCT AS


-- Define a record type to store GL records
TYPE glacct_rectype IS RECORD(primary_account   FIF_GL_ACCT.PRIMARY_ACCOUNT%TYPE,
                              attribute1        FIF_GL_ACCT.ATTRIBUTE1%TYPE,
                              attribute2        FIF_GL_ACCT.ATTRIBUTE2%TYPE,
                              attribute3        FIF_GL_ACCT.ATTRIBUTE3%TYPE,
                              attribute4        FIF_GL_ACCT.ATTRIBUTE4%TYPE,
                              attribute5        FIF_GL_ACCT.ATTRIBUTE5%TYPE,
                              attribute6        FIF_GL_ACCT.ATTRIBUTE6%TYPE,
                              attribute7        FIF_GL_ACCT.ATTRIBUTE7%TYPE,
                              attribute8        FIF_GL_ACCT.ATTRIBUTE8%TYPE,
                              attribute9        FIF_GL_ACCT.ATTRIBUTE9%TYPE,
                              attribute10       FIF_GL_ACCT.ATTRIBUTE10%TYPE,
                              attribute11       FIF_GL_ACCT.ATTRIBUTE11%TYPE,
                              attribute12       FIF_GL_ACCT.ATTRIBUTE12%TYPE,
                              attribute13       FIF_GL_ACCT.ATTRIBUTE13%TYPE,
                              attribute14       FIF_GL_ACCT.ATTRIBUTE14%TYPE,
                              attribute15       FIF_GL_ACCT.ATTRIBUTE15%TYPE,
                              description1      FIF_GL_ACCT.DESCRIPTION1%TYPE,
                              description2      FIF_GL_ACCT.DESCRIPTION2%TYPE,
                              description3      FIF_GL_ACCT.DESCRIPTION3%TYPE,
                              description4      FIF_GL_ACCT.DESCRIPTION4%TYPE,
                              description5      FIF_GL_ACCT.DESCRIPTION5%TYPE,
                              description6      FIF_GL_ACCT.DESCRIPTION6%TYPE,
                              description7      FIF_GL_ACCT.DESCRIPTION7%TYPE,
                              description8      FIF_GL_ACCT.DESCRIPTION8%TYPE,
                              description9      FIF_GL_ACCT.DESCRIPTION9%TYPE,
                              description10     FIF_GL_ACCT.DESCRIPTION10%TYPE,
                              description11     FIF_GL_ACCT.DESCRIPTION11%TYPE,
                              description12     FIF_GL_ACCT.DESCRIPTION12%TYPE,
                              description13     FIF_GL_ACCT.DESCRIPTION13%TYPE,
                              description14     FIF_GL_ACCT.DESCRIPTION14%TYPE,
                              description15     FIF_GL_ACCT.DESCRIPTION15%TYPE,
                              set_of_books_id   FIF_GL_ACCT.SET_OF_BOOKS_ID%TYPE);

---------------------------------------------------------------------
FUNCTION PARSE_HEADER(O_error_message     OUT     VARCHAR2,
                      O_glacct_record     OUT     GLACCT_RECTYPE,
                      I_glacct_root       IN OUT  xmldom.DOMElement)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION PROCESS_HEADER(O_error_message IN OUT VARCHAR2,
                        I_glacct_record IN     GLACCT_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION PROCESS_GLACCT will take the input GL record
-- and place the information into a local GL record which will
-- be used in the package to manipulate the data. It will then
-- call a series of support functions to perform all business logic
-- on the record.
---------------------------------------------------------------------
FUNCTION PROCESS_GLACCT (O_text             IN OUT VARCHAR2,
                         I_glacctrec        IN     GLACCT_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION INSERT_GLACCT will insert any valid account on the GL
-- table.  It is called from PUT_GLACCT
---------------------------------------------------------------------
FUNCTION INSERT_GLACCT(O_text      IN OUT VARCHAR2,
                       I_glacctrec IN     GLACCT_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION UPDATE_GLACCT will update any valid account on the GL
-- table.  It is called from PUT_GLACCT.
---------------------------------------------------------------------
FUNCTION UPDATE_GLACCT(O_text           IN OUT VARCHAR2,
                       I_glacctrec      IN     GLACCT_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION VALIDATE_GLACCT is a wrapper function which is
-- used to call CHECK_NULLS, CHECK_ENABLED for
-- any GL record input into the package.
---------------------------------------------------------------------
FUNCTION VALIDATE_GLACCT(O_text        IN OUT VARCHAR2,
                         I_glacctrec   IN     GLACCT_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION CHECK_NULLS will check an input value if it's null.  If
--  so, an error message will be created and the int_errors error
--  handling will be called.
---------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_text            IN OUT VARCHAR2,
                      I_record_variable IN     VARCHAR2,
                      I_record_type     IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------
---FUNCTION CHECK_ATTRS will check to ensure that each description that
---is input also has an attribute that it describes.
---------------------------------------------------------------------
FUNCTION CHECK_ATTRS (O_text            IN OUT VARCHAR2,
                      I_glacctrec       IN     GLACCT_RECTYPE)
return BOOLEAN;
---------------------------------------------------------------------
/* Function and Procedure Bodies */
---------------------------------------------------------------------
FUNCTION CONSUME(O_error_message           OUT  VARCHAR2,
                 I_message                 IN   CLOB)
return BOOLEAN IS

   glacct_root        xmldom.DOMElement;
   L_glacct_record  GLACCT_RECTYPE;

   PROGRAM_ERROR    exception;

BEGIN
   -- parse the document and return the root element.
   -- Send in entire xml document, the xml file header name, and the following defaults (for now).
   glacct_root := rib_xml.readRoot(I_message, 'GLCOADesc');
   -- Process header record by retrieving the value for each associated xml tag.

   if PARSE_HEADER(O_error_message,
                   L_glacct_record,
                   glacct_root) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---

   if PROCESS_HEADER(O_error_message,
                     L_glacct_record) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   rib_xml.freeRoot( glacct_root );
   ---
   return TRUE;

 EXCEPTION
   when PROGRAM_ERROR then
      rib_xml.freeRoot( glacct_root );
      return FALSE;

   when OTHERS then
      rib_xml.freeRoot( glacct_root );
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_GLCACCT.CONSUME',
                                            to_char(SQLCODE));
         return FALSE;
END CONSUME;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_HEADER(O_error_message     OUT     VARCHAR2,
                      O_glacct_record     OUT     GLACCT_RECTYPE,
                      I_glacct_root       IN OUT  xmldom.DOMElement)
return BOOLEAN IS

BEGIN

   -- The package RIB_XML is called to retrieve values from the XML document.
   -- In order to do this, the specific tag names on the XML document will need to be known
   -- for each value we need to retrieve.
   ---
   O_glacct_record.primary_account         := rib_xml.getChildText(I_glacct_root, 'primary_account');
   O_glacct_record.attribute1              := rib_xml.getChildText(I_glacct_root, 'attribute1');
   O_glacct_record.attribute2              := rib_xml.getChildText(I_glacct_root, 'attribute2');
   O_glacct_record.attribute3              := rib_xml.getChildText(I_glacct_root, 'attribute3');
   O_glacct_record.attribute4              := rib_xml.getChildText(I_glacct_root, 'attribute4');
   O_glacct_record.attribute5              := rib_xml.getChildText(I_glacct_root, 'attribute5');
   O_glacct_record.attribute6              := rib_xml.getChildText(I_glacct_root, 'attribute6');
   O_glacct_record.attribute7              := rib_xml.getChildText(I_glacct_root, 'attribute7');
   O_glacct_record.attribute8              := rib_xml.getChildText(I_glacct_root, 'attribute8');
   O_glacct_record.attribute9              := rib_xml.getChildText(I_glacct_root, 'attribute9');
   O_glacct_record.attribute10             := rib_xml.getChildText(I_glacct_root, 'attribute10');
   O_glacct_record.attribute11             := rib_xml.getChildText(I_glacct_root, 'attribute11');
   O_glacct_record.attribute12             := rib_xml.getChildText(I_glacct_root, 'attribute12');
   O_glacct_record.attribute13             := rib_xml.getChildText(I_glacct_root, 'attribute13');
   O_glacct_record.attribute14             := rib_xml.getChildText(I_glacct_root, 'attribute14');
   O_glacct_record.attribute15             := rib_xml.getChildText(I_glacct_root, 'attribute15');
   O_glacct_record.description1            := rib_xml.getChildText(I_glacct_root, 'description1');
   O_glacct_record.description2            := rib_xml.getChildText(I_glacct_root, 'description2');
   O_glacct_record.description3            := rib_xml.getChildText(I_glacct_root, 'description3');
   O_glacct_record.description4            := rib_xml.getChildText(I_glacct_root, 'description4');
   O_glacct_record.description5            := rib_xml.getChildText(I_glacct_root, 'description5');
   O_glacct_record.description6            := rib_xml.getChildText(I_glacct_root, 'description6');
   O_glacct_record.description7            := rib_xml.getChildText(I_glacct_root, 'description7');
   O_glacct_record.description8            := rib_xml.getChildText(I_glacct_root, 'description8');
   O_glacct_record.description9            := rib_xml.getChildText(I_glacct_root, 'description9');
   O_glacct_record.description10           := rib_xml.getChildText(I_glacct_root, 'description10');
   O_glacct_record.description11           := rib_xml.getChildText(I_glacct_root, 'description11');
   O_glacct_record.description12           := rib_xml.getChildText(I_glacct_root, 'description12');
   O_glacct_record.description13           := rib_xml.getChildText(I_glacct_root, 'description13');
   O_glacct_record.description14           := rib_xml.getChildText(I_glacct_root, 'description14');
   O_glacct_record.description15           := rib_xml.getChildText(I_glacct_root, 'description15');
   O_glacct_record.set_of_books_id         := rib_xml.getChildText(I_glacct_root, 'set_of_books_id');
   ---

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_GLCACCT.PARSE_HEADER',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_HEADER;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_HEADER(O_error_message IN OUT VARCHAR2,
                        I_glacct_record IN     GLACCT_RECTYPE)
return BOOLEAN IS

   L_glacctrec           GLACCT_RECTYPE        := I_glacct_record;

BEGIN
   if PROCESS_GLACCT(O_error_message,
                     L_glacctrec) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_GLCACCT.PROCESS_HEADER',
                                             to_char(SQLCODE));
       return FALSE;

END PROCESS_HEADER;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_GLACCT(O_text             IN OUT VARCHAR2,
                        I_glacctrec        IN     GLACCT_RECTYPE)
RETURN BOOLEAN IS

   L_glacctrec        GLACCT_RECTYPE     := I_glacctrec;
   L_exists           VARCHAR2(1)        := 'N';

   CURSOR c_glacct IS
      select 'Y'
        from fif_gl_acct
       where set_of_books_id = nvl(L_glacctrec.set_of_books_id, -1)
         and primary_account = L_glacctrec.primary_account;

BEGIN
   if VALIDATE_GLACCT(O_text,
                      L_glacctrec) = FALSE then
      return FALSE;
   end if;
   ---
   open c_glacct;
   fetch c_glacct into L_exists;
   close c_glacct;
   ---
   if L_exists = 'N' then
      if INSERT_GLACCT(O_text,
                       L_glacctrec) = FALSE then
         return FALSE;
      end if;
   else
      if UPDATE_GLACCT(O_text,
                       L_glacctrec) = FALSE then
         return FALSE;
      end if;
  end if;
  ---

  return TRUE;

EXCEPTION
   when OTHERS then
       O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                     SQLERRM,
                                     'RMSSUB_GLCACCT.PROCESS_GLACCT',
                                     to_char(SQLCODE));
      return FALSE;

END PROCESS_GLACCT;
---------------------------------------------------------------------------------
FUNCTION INSERT_GLACCT(O_text      IN OUT VARCHAR2,
                       I_glacctrec IN     GLACCT_RECTYPE)
return BOOLEAN IS

BEGIN
   insert into fif_gl_acct(primary_account,
                           attribute1,
                           attribute2,
                           attribute3,
                           attribute4,
                           attribute5,
                           attribute6,
                           attribute7,
                           attribute8,
                           attribute9,
                           attribute10,
                           attribute11,
                           attribute12,
                           attribute13,
                           attribute14,
                           attribute15,
                           description1,
                           description2,
                           description3,
                           description4,
                           description5,
                           description6,
                           description7,
                           description8,
                           description9,
                           description10,
                           description11,
                           description12,
                           description13,
                           description14,
                           description15,
                           set_of_books_id)
                    values(I_glacctrec.primary_account,
                           I_glacctrec.attribute1,
                           I_glacctrec.attribute2,
                           I_glacctrec.attribute3,
                           I_glacctrec.attribute4,
                           I_glacctrec.attribute5,
                           I_glacctrec.attribute6,
                           I_glacctrec.attribute7,
                           I_glacctrec.attribute8,
                           I_glacctrec.attribute9,
                           I_glacctrec.attribute10,
                           I_glacctrec.attribute11,
                           I_glacctrec.attribute12,
                           I_glacctrec.attribute13,
                           I_glacctrec.attribute14,
                           I_glacctrec.attribute15,
                           I_glacctrec.description1,
                           I_glacctrec.description2,
                           I_glacctrec.description3,
                           I_glacctrec.description4,
                           I_glacctrec.description5,
                           I_glacctrec.description6,
                           I_glacctrec.description7,
                           I_glacctrec.description8,
                           I_glacctrec.description9,
                           I_glacctrec.description10,
                           I_glacctrec.description11,
                           I_glacctrec.description12,
                           I_glacctrec.description13,
                           I_glacctrec.description14,
                           I_glacctrec.description15,
                           NVL(I_glacctrec.set_of_books_id, -1));
   return TRUE;

EXCEPTION
   when OTHERS then
      O_text := sql_lib.create_msg('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_GLCACCT.INSERT_GLACCT',
                                   to_char(SQLCODE));
      return FALSE;
END INSERT_GLACCT;
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_GLACCT(O_text      IN OUT VARCHAR2,
                       I_glacctrec IN     GLACCT_RECTYPE)
return BOOLEAN IS

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   CURSOR c_lock_glacct IS
      select 'Y'
        from fif_gl_acct
       where set_of_books_id = nvl(I_glacctrec.set_of_books_id,-1)
         and primary_account = I_glacctrec.primary_account
         for update nowait;

BEGIN
   -- Lock the fif_gl_acct table before updating
   open c_lock_glacct;
   close c_lock_glacct;
   ---
   update fif_gl_acct
      set attribute1    = I_glacctrec.attribute1,
          attribute2    = I_glacctrec.attribute2,
          attribute3    = I_glacctrec.attribute3,
          attribute4    = I_glacctrec.attribute4,
          attribute5    = I_glacctrec.attribute5,
          attribute6    = I_glacctrec.attribute6,
          attribute7    = I_glacctrec.attribute7,
          attribute8    = I_glacctrec.attribute8,
          attribute9    = I_glacctrec.attribute9,
          attribute10   = I_glacctrec.attribute10,
          attribute11   = I_glacctrec.attribute11,
          attribute12   = I_glacctrec.attribute12,
          attribute13   = I_glacctrec.attribute13,
          attribute14   = I_glacctrec.attribute14,
          attribute15   = I_glacctrec.attribute15,
          description1  = I_glacctrec.description1,
          description2  = I_glacctrec.description2,
          description3  = I_glacctrec.description3,
          description4  = I_glacctrec.description4,
          description5  = I_glacctrec.description5,
          description6  = I_glacctrec.description6,
          description7  = I_glacctrec.description7,
          description8  = I_glacctrec.description8,
          description9  = I_glacctrec.description9,
          description10 = I_glacctrec.description10,
          description11 = I_glacctrec.description11,
          description12 = I_glacctrec.description12,
          description13 = I_glacctrec.description13,
          description14 = I_glacctrec.description14,
          description15 = I_glacctrec.description15
    where set_of_books_id = nvl(I_glacctrec.set_of_books_id ,-1)
      and primary_account = I_glacctrec.primary_account;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_text := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                   'FIF_GL_ACCT',
                                   I_glacctrec.primary_account,
                                   'RMSSUB_GLCACCT.UPDATE_GLACCT');
      return FALSE;
   when OTHERS then
      O_text := sql_lib.create_msg('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_GLCACCT.UPDATE_GLACCT',
                                   to_char(SQLCODE));
      return FALSE;
END UPDATE_GLACCT;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_GLACCT(O_text        IN OUT VARCHAR2,
                         I_glacctrec   IN     GLACCT_RECTYPE)
return BOOLEAN IS

   L_system_options    SYSTEM_OPTIONS%ROWTYPE;

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_text,
                  I_glacctrec.primary_account,
                  'primary_account') = FALSE then
      return FALSE;
   end if;
   ---
   if L_system_options.org_unit_ind = 'Y' then
      if CHECK_NULLS(O_text,
                     I_glacctrec.set_of_books_id,
                     'set_of_books_id') = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if CHECK_ATTRS(O_text,
                  I_glacctrec) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
    when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_GLCACCT.VALIDATE_GLACCT',
                                   to_char(SQLCODE));
      return FALSE;

END VALIDATE_GLACCT;
----------------------------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_text            IN OUT VARCHAR2,
                      I_record_variable IN     VARCHAR2,
                      I_record_type     IN     VARCHAR2)
return BOOLEAN IS

BEGIN
   if I_record_variable is NULL then
      O_text := sql_lib.create_msg('INVALID_PARM',
                                   I_record_type,
                                   'NULL',
                                   'NOT NULL');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_GLCACCT.CHECK_NULLS',
                                   to_char(SQLCODE));
      return FALSE;

END CHECK_NULLS;
-----------------------------------------------------------------------------------------
FUNCTION CHECK_ATTRS (O_text            IN OUT VARCHAR2,
                      I_glacctrec       IN     GLACCT_RECTYPE)
return BOOLEAN IS

BEGIN

   ---
   if (((I_glacctrec.description1 is NOT NULL) AND (I_glacctrec.attribute1 is NULL)) OR
       ((I_glacctrec.description2 is NOT NULL) AND (I_glacctrec.attribute2 is NULL)) OR
       ((I_glacctrec.description3 is NOT NULL) AND (I_glacctrec.attribute3 is NULL)) OR
       ((I_glacctrec.description4 is NOT NULL) AND (I_glacctrec.attribute4 is NULL)) OR
       ((I_glacctrec.description5 is NOT NULL) AND (I_glacctrec.attribute5 is NULL)) OR
       ((I_glacctrec.description6 is NOT NULL) AND (I_glacctrec.attribute6 is NULL)) OR
       ((I_glacctrec.description7 is NOT NULL) AND (I_glacctrec.attribute7 is NULL)) OR
       ((I_glacctrec.description8 is NOT NULL) AND (I_glacctrec.attribute8 is NULL)) OR
       ((I_glacctrec.description9 is NOT NULL) AND (I_glacctrec.attribute9 is NULL)) OR
       ((I_glacctrec.description10 is NOT NULL) AND (I_glacctrec.attribute10 is NULL)) OR
       ((I_glacctrec.description11 is NOT NULL) AND (I_glacctrec.attribute11 is NULL)) OR
       ((I_glacctrec.description12 is NOT NULL) AND (I_glacctrec.attribute12 is NULL)) OR
       ((I_glacctrec.description13 is NOT NULL) AND (I_glacctrec.attribute13 is NULL)) OR
       ((I_glacctrec.description14 is NOT NULL) AND (I_glacctrec.attribute14 is NULL)) OR
       ((I_glacctrec.description15 is NOT NULL) AND (I_glacctrec.attribute15 is NULL))) then

        O_text := sql_lib.create_msg('INVALID_PARM',
                                      ':  ATTRIBUTE',
                                      'NULL',
                                      'NOT NULL');
        return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_GLCACCT.CHECK_ATTRS',
                                   to_char(SQLCODE));
      return FALSE;

END CHECK_ATTRS;
-----------------------------------------------------------------------------------------
END RMSSUB_GLCACCT;
/
