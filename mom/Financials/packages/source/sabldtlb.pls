CREATE OR REPLACE PACKAGE BODY SA_BUILD_TOTAL_SQL AS
-------------------------------------------------------------------------------------------------
FUNCTION CREATE_REVISION (O_error_message    IN OUT VARCHAR2,
                          O_new_total_rev_no IN OUT SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                          O_total_status     IN OUT SA_TOTAL_HEAD.STATUS%TYPE,
                          I_total_id         IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                          I_total_rev_no     IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
   RETURN BOOLEAN is
   ---
   L_program            VARCHAR2(60) := 'SA_BUILD_TOTAL_SQL.CREATE_REVISION';
   L_max_rev_no         SA_TOTAL_HEAD.total_rev_no%TYPE;
   L_vr_id              SA_TOTAL_HEAD.VR_ID%TYPE;
   L_vr_rev_no          SA_TOTAL_HEAD.VR_REV_NO%TYPE;
   L_max_vr_rev_no      SA_TOTAL_HEAD.VR_REV_NO%TYPE;
   L_original_realm_id  SA_REALM.REALM_ID%TYPE;
   L_new_realm_id       SA_REALM.REALM_ID%TYPE;
   ---
   cursor C_GET_STATUS is
      select status
        from sa_total_head
       where total_id     = I_total_id
         and total_rev_no = I_total_rev_no;
   ---
   cursor C_GET_MAX_REV is
      select max(total_rev_no)
        from SA_TOTAL_HEAD
       where total_id = I_total_id;
   ---
   cursor C_GET_VR_INFO is
      select vr_id,
             vr_rev_no
        from sa_total_head
       where total_id     = I_total_id
         and total_rev_no = I_total_rev_no;
   ---
   cursor C_GET_MAX_VR_REV is
      select max(vr_rev_no)
        from sa_total_head
       where total_id     = I_total_id
         and total_rev_no = I_total_rev_no;
   ---
   cursor C_GET_ORIGINAL_REALM_ID is
      select driving_realm_id
        from sa_vr_head
       where vr_id     = L_vr_id
         and vr_rev_no = L_max_vr_rev_no;
   ---
BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STATUS',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   open  C_GET_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STATUS',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   fetch C_GET_STATUS into O_total_status;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STATUS',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   close C_GET_STATUS;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_MAX_REV',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   open C_GET_MAX_REV;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_MAX_REV',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   fetch C_GET_MAX_REV into L_max_rev_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_MAX_REV',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   close C_GET_MAX_REV;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_VR_INFO',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   open  C_GET_VR_INFO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_VR_INFO',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   fetch C_GET_VR_INFO into L_vr_id,
                            L_vr_rev_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_VR_INFO',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   close C_GET_VR_INFO;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_MAX_VR_REV',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   open C_GET_MAX_VR_REV;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_MAX_VR_REV',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   fetch C_GET_MAX_VR_REV into L_max_vr_rev_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_MAX_VR_REV',
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id);
   close C_GET_MAX_VR_REV;
   ---
   O_new_total_rev_no := L_max_rev_no + 1;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ORIGINAL_REALM_ID',
                    'SA_VR_HEAD',
                    'VR ID: '||L_vr_id||', Rev. No.: '||to_char(L_max_vr_rev_no));
   open  C_GET_ORIGINAL_REALM_ID;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ORIGINAL_REALM_ID',
                    'SA_VR_HEAD',
                    'VR ID: '||L_vr_id||', Rev. No.: '||to_char(L_max_vr_rev_no));
   fetch C_GET_ORIGINAL_REALM_ID into L_original_realm_id;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ORIGINAL_REALM_ID',
                    'SA_VR_HEAD',
                    'VR ID: '||L_vr_id||', Rev. No.: '||to_char(L_max_vr_rev_no));
   close C_GET_ORIGINAL_REALM_ID;
   ---
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_TOTAL_HEAD',
                    'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
   insert into sa_total_head
               (total_id,
                total_rev_no,
                vr_id,
                vr_rev_no,
                total_desc,
                total_type,
                os_group,
                os_operator,
                comb_total_ind,
                total_cat,
                bal_level,
                update_datetime,
                update_id,
                count_sum_ind,
                pos_ind,
                sys_calc_ind,
                store_update_ind,
                hq_update_ind,
                req_ind,
                wiz_ind,
                start_business_date,
                end_business_date,
                total_parm_seq_no,
                group_seq_no1,
                ref_label_code_1,
                group_seq_no2,
                ref_label_code_2,
                group_seq_no3,
                ref_label_code_3,
                display_order,
                status)
       select total_id,
              O_new_total_rev_no,
              L_vr_id,
              L_max_vr_rev_no + 1,
              total_desc,
              total_type,
              os_group,
              os_operator,
              comb_total_ind,
              total_cat,
              bal_level,
              sysdate,
              user,
              count_sum_ind,
              pos_ind,
              sys_calc_ind,
              store_update_ind,
              hq_update_ind,
              req_ind,
              wiz_ind,
              start_business_date,
              end_business_date,
              total_parm_seq_no,
              group_seq_no1,
              ref_label_code_1,
              group_seq_no2,
              ref_label_code_2,
              group_seq_no3,
              ref_label_code_3,
              display_order,
              status
         from sa_total_head
        where total_id     = I_total_id
          and total_rev_no = I_total_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_TOTAL_HEAD_TL',
                    'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
   insert into sa_total_head_tl
               (lang,
                total_id,
                total_rev_no,
                total_desc,
                create_datetime,
                create_id,
                last_update_datetime,
                last_update_id)
         select lang,
                total_id,
                O_new_total_rev_no,
                total_desc,
                sysdate,
                get_user(),
                sysdate,
                get_user()
           from sa_total_head_tl
          where total_id     = I_total_id
            and total_rev_no = I_total_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_TOTAL_USAGE',
                    'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
   insert into sa_total_usage
               (total_id,
                total_rev_no,
                usage_type)
       select total_id,
              O_new_total_rev_no,
              usage_type
         from sa_total_usage
        where total_id     = I_total_id
          and total_rev_no = I_total_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_COMB_TOTAL',
                    'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
   insert into sa_comb_total
               (total_id,
                total_rev_no,
                detail_seq_no,
                detail_total_id,
                detail_total_rev_no,
                total_operator)
       select distinct c.total_id,
                       O_new_total_rev_no,
                       c.detail_seq_no,
                       c.detail_total_id,
                       h.total_rev_no,
                       c.total_operator
         from sa_comb_total c,
              sa_total_head h
        where c.total_id     = I_total_id
          and c.total_rev_no = I_total_rev_no
          and h.total_rev_no = (select max(total_rev_no)
                                  from sa_total_head
                                 where total_id = c.detail_total_id);

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_TOTAL_RESTRICTIONS',
                    'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
   insert into sa_total_restrictions
               (total_id,
                total_rev_no,
                res_seq_no,
                parm_seq_no_1,
                res_operator,
                parm_seq_no_2,
                parm_seq_no_3,
                res_CONSTANT_2,
                res_CONSTANT_3)
       select total_id,
              O_new_total_rev_no,
              res_seq_no,
              parm_seq_no_1,
              res_operator,
              parm_seq_no_2,
              parm_seq_no_3,
              res_CONSTANT_2,
              res_CONSTANT_3
         from sa_total_restrictions
       where total_id      = I_total_id
          and total_rev_no = I_total_rev_no;

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SA_TOTAL_LOC_TRAIT',
                    'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
   insert into sa_total_loc_trait
               (total_id,
                total_rev_no,
                loc_trait)
       select total_id,
              O_new_total_rev_no,
              loc_trait
         from sa_total_loc_trait
        where total_id     = I_total_id
          and total_rev_no = I_total_rev_no;
   ---
   --- Insert into VR tables
   ---
   if L_original_realm_id is NOT NULL then
      if SA_REALM_SQL.GET_MAX_REALM_ID(O_error_message,
                                       L_new_realm_id,
                                       L_original_realm_id) = FALSE then
         return FALSE;
      end if;
      ---
      if SA_VR_SQL.CREATE_VR_REVISION(O_error_message,
                                      L_new_realm_id,
                                      L_vr_id,
                                      L_vr_rev_no,
                                      L_max_vr_rev_no + 1) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             SQLCODE);
      return FALSE;
END CREATE_REVISION;
-------------------------------------------------------------------------------------------------
    FUNCTION BUILD_TOTAL_FUNCTION (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_total_id           IN       SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                   I_total_rev_no       IN       SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                                   I_pos_ind            IN       SA_TOTAL_HEAD.POS_IND%TYPE,
                                   I_refresh_override   IN       BOOLEAN)
       RETURN INT
   /****************************************************************\
   |                                                                |
   |   BUILD_TOTAL_FUNCTION                                         |
   |                                                                |
   |   This function will generate a single total function as       |
   |   defined in the sa_total_head and corresponding tables.       |
   |                                                                |
   |    Arguments:          Description:                            |
   |    I_total_id          Total identifier to create.             |
   |    I_total_rev_no      Will determine which version to create. |
   |    I_pos_ind           if value passed is 'Y' the data will be |
   |                        inserted into the sa_pos_value table,   |
   |                        otherwise it will write to the          |
   |                        sa_sys_value table.                     |
   |    I_refresh_override  if value passed is 'Y' the existing     |
   |                        function (regardless of the date will   |
   |                        be deleted first.                       |
   |    O_error_message     Returned error message                  |
   |                                                                |
   |    Called from:        Sales Audit form: sa_total.fmb          |
   |                        Sales Audit nightly batch: satotals.pc  |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
    IS
        cursor_handle       INTEGER; /* The DBMS cursor handle */
        feedback            INTEGER; /* integer result capture variable */
        v_program           VARCHAR2(50) := 'SA_BUILD_TOTAL_SQL.BUILD_TOTAL_FUNCTION';
        b_string            BOOLEAN := FALSE;
        b_bal_group         BOOLEAN := FALSE;
        b_store_day         BOOLEAN := FALSE;
        b_select_done       BOOLEAN := FALSE;
        b_group_by_done     BOOLEAN := FALSE;
        b_between_op        BOOLEAN := FALSE;
        b_null_op           BOOLEAN := FALSE;
        d_end_date          VARCHAR2(8);
        d_start_date        VARCHAR2(8);
        e_total_def_error   EXCEPTION;
        l_outer_join_ind    sa_vr_links.outer_join_ind%TYPE;
        l_count_sum_ind     sa_total_head.count_sum_ind%TYPE;
        n_seq_no            sa_vr_parms.vr_parm_seq_no%TYPE;
        n_sum_parm_seq_no   sa_vr_parms.vr_parm_seq_no%TYPE;
        n_counter           INTEGER := 0;
        n_bal_group         INTEGER := 0;
        n_store_day         INTEGER := 0;
        n_error_check_count INTEGER := 0;
        n_min_joins         INTEGER := 0;
        n_count             NUMBER; /* for loop counter */
        s_sum_ind           VARCHAR2(1);
        v_data_type         sa_parm_type.data_type%TYPE;
        v_where             VARCHAR2(2000);
        v_object_name       VARCHAR2(61);
        v_realm_id          sa_realm.realm_id%TYPE;
        v_constant_2        sa_total_restrictions.res_constant_2%TYPE;
        v_constant_3        sa_total_restrictions.res_constant_3%TYPE;
        v_group_by          VARCHAR2(100);
        v_name              VARCHAR2(30); /* stores function name */
        v_bal_level         sa_total_head.bal_level%TYPE;
        v_sql               DBMS_SQL.VARCHAR2a; /* dynamic sql variable */
        v_operator          VARCHAR2(20);
        v_select            sa_total_common.v_code_array := sa_total_common.empty_v_out;
        L_parm_seq_no_2     SA_TOTAL_RESTRICTIONS.PARM_SEQ_NO_2%TYPE;
        L_parm_seq_no_3     SA_TOTAL_RESTRICTIONS.PARM_SEQ_NO_3%TYPE;
        L_parm_seq_no       SA_TOTAL_RESTRICTIONS.PARM_SEQ_NO_2%TYPE;
        v_realm2            SA_REALM.DISPLAY_NAME%TYPE;
        v_realm3            SA_REALM.DISPLAY_NAME%TYPE;
        v_parm2             SA_PARM.DISPLAY_NAME%TYPE;
        v_parm3             SA_PARM.DISPLAY_NAME%TYPE;
        v_status            all_objects.status%TYPE;
        v_db_date_format    VARCHAR2(16) := 'YYYYMMDDHH24MISS';
        L_exists            VARCHAR2(1) := 'N';

        /* retrieves the realms for the from statement */
        cursor c_get_realms is
            select DISTINCT realm_id, realm_name
             from v_sa_realm_info
            where realm_id in (select realm_id
                                 from sa_vr_realm
                                where vr_id in (select vr_id
                                                 from sa_total_head
                                                where total_id     = I_total_id
                                                  and total_rev_no = I_total_rev_no)
                                  and vr_rev_no = I_total_rev_no);
        /* retrieves the columns for the select statement */
        cursor c_get_select_parms is
            select vp.realm_name || '.' || vp.parm_name,
                   stp.vr_parm_seq_no, pti.data_type,
                   DECODE(st.total_parm_seq_no, stp.vr_parm_seq_no, 'Y', 'N') SUM_IND,
                   st.count_sum_ind, st.bal_level, to_char(st.end_business_date, 'YYYYMMDD'),
                   to_char(st.start_business_date, 'YYYYMMDD')
              from v_sa_realm_info vr, v_sa_parm_info vp, sa_vr_parms stp, sa_parm_type pti,
                   sa_total_head st
             where vr.realm_id = vp.realm_id
               and vp.parm_id = stp.parm_id
               and st.total_id = I_total_id
               and st.total_rev_no = I_total_rev_no
               and st.vr_id = stp.vr_id
               and st.vr_rev_no = stp.vr_rev_no
               and vp.parm_type_id = pti.parm_type_id
               and (stp.vr_parm_seq_no = st.total_parm_seq_no
                    or
                    stp.vr_parm_seq_no = st.group_seq_no1
                    or
                    stp.vr_parm_seq_no = st.group_seq_no2
                    or
                    stp.vr_parm_seq_no = st.group_seq_no3)
             ORDER BY SUM_IND DESC,
                      NVL(CASE
                             WHEN stp.vr_parm_seq_no=st.group_seq_no1 THEN
                                1
                             WHEN stp.vr_parm_seq_no=st.group_seq_no2 THEN
                                2
                             WHEN stp.vr_parm_seq_no=st.group_seq_no3 THEN
                                3
                          END,
                          0);
        /* Retrieves the join columns for the where clause */
        cursor c_get_where_clause is
            select distinct ri.realm_name || '.' || pi.parm_name || ' = ' ||
                   ri1.realm_name || '.' || pi1.parm_name, tl.outer_join_ind
              from sa_vr_links tl, v_sa_realm_info ri,
                   v_sa_parm_info pi, sa_vr_parms tp1, v_sa_realm_info ri1,
                   v_sa_parm_info pi1, sa_total_head st
             where ri.realm_id = pi.realm_id
               and ri1.realm_id = pi1.realm_id
               and pi.parm_id = tl.link_to_parm_id
               and pi1.parm_id = tp1.parm_id
               and tl.vr_id = st.vr_id
               and tl.vr_rev_no = st.vr_rev_no
               and tp1.vr_parm_seq_no = tl.link_to_parm_seq_no
               and tp1.vr_id = tl.vr_id
               and tp1.vr_rev_no = tl.vr_rev_no
               and st.total_id = I_total_id
               and st.total_rev_no = I_total_rev_no;
        /* retrieves the exceptions for the where clause */
        cursor c_get_exceptions is
            select ri.realm_name || '.' || pi.parm_name,
                   tr.res_operator, pti.data_type, tr.res_constant_2,
                   tr.res_constant_3, tr.parm_seq_no_1,
                   tr.parm_seq_no_2, tr.parm_seq_no_3
              from sa_total_restrictions tr, sa_total_head th,
                   sa_vr_parms tp, v_sa_parm_info pi, v_sa_realm_info ri, sa_parm_type pti
             where ri.realm_id = pi.realm_id
               and tp.vr_parm_seq_no = tr.parm_seq_no_1
               and tp.parm_id = pi.parm_id
               and pti.parm_type_id = pi.parm_type_id
               and th.total_id = I_total_id
               and th.total_rev_no = I_total_rev_no
               and th.total_id = tr.total_id
               and th.total_rev_no = tr.total_rev_no
               and th.vr_id = tp.vr_id
               and th.vr_rev_no = tp.vr_rev_no
             ORDER BY tp.vr_parm_seq_no;
        /* retrieve the info for each extra parameter */
        cursor c_get_parameter_info is
            select ri.realm_name, pi.parm_name
              from sa_vr_parms tp, v_sa_parm_info pi, v_sa_realm_info ri,
                   sa_total_head th
             where ri.realm_id = pi.realm_id
               and tp.vr_parm_seq_no = L_parm_seq_no
               and tp.parm_id = pi.parm_id
               and th.total_id = I_total_id
               and th.total_rev_no = I_total_rev_no
               and tp.vr_id = th.vr_id
               and tp.vr_rev_no = th.vr_rev_no
             ORDER BY tp.vr_parm_seq_no;
        /* retrieves the group by clause info */
        cursor c_get_group_by is
            select ri.realm_name || '.' || pi.parm_name
              from sa_total_head th, sa_vr_parms sv, v_sa_realm_info ri, v_sa_parm_info pi
             where th.total_id = I_total_id
               and th.total_rev_no = I_total_rev_no
               and th.vr_id = sv.vr_id
               and th.vr_rev_no = sv.vr_rev_no
               and sv.parm_id = pi.parm_id
               and (sv.vr_parm_seq_no = th.group_seq_no1
                  or
                  sv.vr_parm_seq_no = th.group_seq_no2
                  or
                  sv.vr_parm_seq_no = th.group_seq_no3)
              and ri.realm_id = pi.realm_id
            ORDER BY NVL(CASE
                            WHEN sv.vr_parm_seq_no=th.group_seq_no1 THEN
                               1
                            WHEN sv.vr_parm_seq_no=th.group_seq_no2 THEN
                               2
                            WHEN sv.vr_parm_seq_no=th.group_seq_no3 THEN
                               3
                         END,
                         0);
        cursor c_check_compile is
            select status
              from all_objects
             where object_name = v_name
               and object_type = 'FUNCTION';
        cursor C_get_start_end_date is
           select to_char(start_business_date,'YYMMDD'),
                  to_char(end_business_date,'YYMMDD')
             from sa_total_head
            where total_id     = I_total_id
              and total_rev_no = I_total_rev_no;
    BEGIN
        /* this function gives the user the option to maintain many
            different revisions of the same total running.  if the user
            wishes not to do this, they can pass a 'Y' in the refresh
            indicator and any existing functions for the current total
            would be deleted before another is created */
        if I_refresh_override then
            feedback := DROP_TOTAL_FUNCTION(O_error_message, I_total_id, NULL);
            if feedback <> 0 then
                RETURN(feedback);
            end if;
        end if;
         /***************************************************************\
        |                                                               |
        |   Retrieve the start and end dates for the function.          |
        |   Also grab the total status.                                 |
        |                                                               |
        \***************************************************************/
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_START_END_DATE',
                         'SA_TOTAL_HEAD',
                         'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
        open  C_get_start_end_date;
        SQL_LIB.SET_MARK('FETCH',
                         'C_GET_START_END_DATE',
                         'SA_TOTAL_HEAD',
                         'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
        fetch C_get_start_end_date into d_start_date, d_end_date;
        SQL_LIB.SET_MARK('CLOSE',
                         'C_GET_START_END_DATE',
                         'SA_TOTAL_HEAD',
                         'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
        close C_get_start_end_date;
        /***************************************************************\
        |                                                               |
        |   Create the dynamic total function                           |
        |                                                               |
        \***************************************************************/
        if d_end_date is not NULL then
           v_name := 'SA_T_' || I_total_id || '_' || d_start_date || '_' || d_end_date;
        else
           v_name := 'SA_T_' || I_total_id || '_' || d_start_date;
        end if;
        v_select(v_select.count+1) := 'CREATE or REPLACE FUNCTION ' ||
                sa_total_common.internal_schema_name || '.' || v_name;
        v_select(v_select.count+1) := '   (';
        v_select(v_select.count+1) := '    I_store_day_seq_no IN   sa_store_day.store_day_seq_no%TYPE DEFAULT NULL,';
        v_select(v_select.count+1) := '    O_error_message   OUT   VARCHAR2';
        v_select(v_select.count+1) := '   )';
        v_select(v_select.count+1) := 'return INT';
        v_select(v_select.count+1) := 'IS';
        v_select(v_select.count+1) := '   l_total_id      sa_total.total_id%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_total_rev_no  sa_sys_value.total_rev_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_sum           sa_sys_value.sys_value%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_ref_no1       sa_total.ref_no1%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_ref_no2       sa_total.ref_no1%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_ref_no3       sa_total.ref_no1%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_store_day_seq_no sa_total.store_day_seq_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_bal_group_seq_no sa_total.bal_group_seq_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_total_seq_no  sa_total.total_seq_no%TYPE;';
        v_select(v_select.count+1) := '   l_tran_date     sa_store_day.business_date%TYPE;';
        v_select(v_select.count+1) := '   l_store         sa_store_day.store%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_day           sa_store_day.day%TYPE := NULL;';
        v_select(v_select.count+1) := '   i_result        INT := 0;';
        v_select(v_select.count+1) := '   i_result2       INT := 0;';
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := 'cursor c_sum is';
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*   Parameters - select statement    */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   retrieve the parameters...select statement                  |
        |                                                               |
        \***************************************************************/
        n_error_check_count := 0;
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_SELECT_PARMS',
                         'V_SA_REALM_INFO, V_SA_PARM_INFO, SA_VR_PARMS, SA_PARM_TYPE, SA_TOTAL_HEAD',
                         'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
        open c_get_select_parms;
        loop
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_SELECT_PARMS',
                             'V_SA_REALM_INFO, V_SA_PARM_INFO, SA_VR_PARMS, SA_PARM_TYPE, SA_TOTAL_HEAD',
                             'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
            fetch c_get_select_parms INTO v_object_name, n_seq_no, v_data_type,
                s_sum_ind, l_count_sum_ind, v_bal_level, d_end_date, d_start_date;
            EXIT WHEN c_get_select_parms%NOTFOUND;
            BEGIN
                n_error_check_count := n_error_check_count + 1;
                if NOT b_select_done then
                    /* We know that the first parameter will be the sum/count
                       indicator due to the ordering of the query */
                    if  l_count_sum_ind = 'S' then
                        v_select(v_select.count+1) := 'select NVL(SUM(' || v_object_name
                            || '),0) TOTAL,';
                    ELSE
                        v_select(v_select.count+1) := 'select COUNT(' || v_object_name
                            || ') TOTAL,';
                    end if;
                    /* no need to check bal_level for 'S' since store_day
                       is always implied  */
                    /* We assume that all the Balance group tables are
                        already defined in the links and parameters
                        section, therefore we can simply add them to the
                        selection statement */
                    if v_bal_level = 'R' then
                        v_select(v_select.count+1) := '       V_SA_BALANCE_GROUP_REGISTER.BAL_GROUP_SEQ_NO BAL_GROUP, ';

                        if b_group_by_done then

                           v_group_by :=  v_group_by || 'V_SA_BALANCE_GROUP_REGISTER.BAL_GROUP_SEQ_NO, ';

                        ELSE

                           v_group_by :=  v_group_by || 'group by V_SA_BALANCE_GROUP_REGISTER.BAL_GROUP_SEQ_NO, ';
                           b_group_by_done := TRUE;

                        end if;

                        b_bal_group := true;

                    elsif v_bal_level = 'C' then

                        v_select(v_select.count+1) := '       V_SA_BALANCE_GROUP_CASHIER.BAL_GROUP_SEQ_NO BAL_GROUP, ';

                        if b_group_by_done then

                           v_group_by :=  v_group_by || 'V_SA_BALANCE_GROUP_CASHIER.BAL_GROUP_SEQ_NO, ';

                        ELSE

                           v_group_by :=  v_group_by || 'group by V_SA_BALANCE_GROUP_CASHIER.BAL_GROUP_SEQ_NO, ';
                           b_group_by_done := TRUE;

                        end if;

                        b_bal_group := true;

                    end if;

                    b_select_done := TRUE;

                ELSE
                   /* Add the roll-up columns as defined */
                   if v_data_type = 'DATE' then
                      n_counter := n_counter + 1;
                      v_select(v_select.count+1) := ' to_char(' || v_object_name || ', ''' || v_db_date_format || ''') REF_NO' ||n_counter || ', ';
                   else
                      n_counter := n_counter + 1;
                      v_select(v_select.count+1) := '       ' || v_object_name || ' REF_NO' ||n_counter || ', ';
                   end if;
                end if;
            END;
        end loop;
        close c_get_select_parms;
        /* if no columns are in the select statement
            we must error out */
        if n_error_check_count = 0 then
            O_error_message := sql_lib.create_msg('SABLDTL_NOSELECT',
                                                  v_program,
                                                  I_total_id,
                                                  NULL);
            RETURN(-1);
        end if;
        n_error_check_count := 0;
        /* remove the final comma from the select statement */
        v_select(v_select.count) := rtrim(v_select(v_select.count), ', ');
        b_select_done := FALSE;
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*      Realms - from Clause    */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   get the realms...from statement                             |
        |                                                               |
        \***************************************************************/
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_REALMS',
                         'V_SA_REALM_INFO',
                         'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
        open c_get_realms;
        loop
           SQL_LIB.SET_MARK('FETCH',
                            'C_GET_REALMS',
                            'V_SA_REALM_INFO',
                            'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
           fetch c_get_realms INTO v_realm_id, v_object_name;
            EXIT WHEN c_get_realms%NOTFOUND;
            BEGIN
                n_error_check_count := n_error_check_count + 1;
                /* check to determine if this is the first statement in the from clause */
                if NOT b_select_done then
                    v_select(v_select.count+1) := 'from ' || v_object_name || ',';
                    b_select_done := TRUE;
                ELSE
                    v_select(v_select.count+1) := '     ' || v_object_name || ', ';
                end if;
                if v_object_name = 'SA_TRAN_HEAD' then
                   L_exists := 'Y';
                end if;
            END;
        end loop;
        close c_get_realms;
        if n_error_check_count = 0 then
            O_error_message := sql_lib.create_msg('SABLDTL_NOREALMS',
                                                  v_program,
                                                  I_total_id,
                                                  NULL);
            RETURN(-1);
        end if;
        n_min_joins         := n_error_check_count - 1;
        n_error_check_count := 0;
        v_select(v_select.count) := rtrim(v_select(v_select.count), ', ');
        b_select_done := FALSE;
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*      Links - where Clause      */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   get the links...where statement                             |
        |                                                               |
        \***************************************************************/
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_WHERE_CLAUSE',
                         'SA_VR_LINKS, V_SA_REALM_INFO, V_SA_PARM_INFO, SA_VR_PARMS, SA_TOTAL_HEAD',
                         'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
        open c_get_where_clause;
        loop
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_WHERE_CLAUSE',
                             'SA_VR_LINKS, V_SA_REALM_INFO, V_SA_PARM_INFO, SA_VR_PARMS, SA_TOTAL_HEAD',
                             'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
            fetch c_get_where_clause INTO v_where, l_outer_join_ind;
            EXIT WHEN c_get_where_clause%NOTFOUND;
            BEGIN
                n_error_check_count := n_error_check_count + 1;
                /* check to determine if this is the first statement in the where clause */
                if NOT b_select_done then
                    v_select(v_select.count+1) := 'where ' || v_where;
                    b_select_done := TRUE;
                ELSE
                    v_select(v_select.count+1) := 'and ' || v_where;
                end if;
                /* check to see if there is an outer join for this link */
                if l_outer_join_ind = 'Y' then
                    v_select(v_select.count) := v_select(v_select.count) || '(+)';
                end if;
                /* check to see if the current parameter is store_day and set flag */
                if instr(upper(v_where), 'STORE_DAY_SEQ_NO') > 0 then
                    b_store_day := TRUE;
                end if;
                /* check to see if the current parameter is bal_group and set flag */
                if instr(upper(v_where), 'BAL_GROUP_SEQ_NO') >0 then
                    b_bal_group := TRUE;
                end if;
            END;
        end loop;
        close c_get_where_clause;
        if n_error_check_count < n_min_joins then
            O_error_message := sql_lib.create_msg('SABLDTL_NOJOINS',
                                                  v_program,
                                                  I_total_id,
                                                  n_min_joins);
            RETURN(-1);
        end if;
        n_error_check_count := 0;
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*      Restrictions       */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   get the restrictions...where statement                      |
        |                                                               |
        \***************************************************************/

        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_EXCEPTIONS',
                         'SA_TOTAL_RESTRICTIONS, SA_TOTAL_HEAD, SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_PARM_TYPE',
                         'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
        open c_get_exceptions;
        loop
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_EXCEPTIONS',
                             'SA_TOTAL_RESTRICTIONS, SA_TOTAL_HEAD, SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_PARM_TYPE',
                             'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
            fetch   c_get_exceptions INTO v_object_name, v_operator, v_data_type,
                v_constant_2, v_constant_3, n_seq_no, L_parm_seq_no_2, L_parm_seq_no_3;
            EXIT WHEN c_get_exceptions%NOTFOUND;
            BEGIN
                /* determine if the operator is a Between or Not Between,
                    NULL or NOT NULL */
                if v_operator = 'B' then
                    v_operator   := 'BETWEEN';
                    b_null_op    := FALSE;
                    b_between_op := TRUE;
                ELSif v_operator = 'NB' then
                    v_operator   := 'NOT BETWEEN';
                    b_null_op    := FALSE;
                    b_between_op := TRUE;
                ELSif v_operator = 'NULL' then
                    v_operator   := 'IS NULL';
                    b_null_op    := TRUE;
                    b_between_op := FALSE;
                ELSif v_operator = '!NULL' then
                    v_operator   := 'IS NOT NULL';
                    b_null_op    := TRUE;
                    b_between_op := FALSE;
                ELSIF v_operator = 'IN' THEN
                    b_null_op    := FALSE;
                    b_between_op := FALSE;
                ELSIF v_operator = 'NOT IN' THEN
                    b_null_op    := FALSE;
                    b_between_op := FALSE;
                ELSE
                    b_null_op    := FALSE;
                    b_between_op := FALSE;
                end if;
                /* Determine if we are using parmaters or constants */
                /* if parameters are defined */
                if (L_parm_seq_no_2 is NOT NULL) and (NOT b_null_op) then

                    /* get the parameter2 info */
                    L_parm_seq_no := L_parm_seq_no_2;
                    SQL_LIB.SET_MARK('OPEN',
                                     'C_GET_PARAMETER_INFO',
                                     'SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_TOTAL_HEAD',
                                     'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
                    open c_get_parameter_info;
                    SQL_LIB.SET_MARK('FETCH',
                                     'C_GET_PARAMETER_INFO',
                                     'SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_TOTAL_HEAD',
                                     'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
                    fetch c_get_parameter_info INTO v_realm2,
                                                    v_parm2;
                    SQL_LIB.SET_MARK('CLOSE',
                                     'C_GET_PARAMETER_INFO',
                                     'SA_VR_PARMS, V_SA_PARM_INFO, V_SA_REALM_INFO, SA_TOTAL_HEAD',
                                     'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
                    close c_get_parameter_info;
                    IF v_realm2 is null or v_parm2 is null THEN
                       O_error_message := sql_lib.create_msg('SABLDTL_RESTRICTIONS',
                                                             v_program,
                                                             I_total_id,
                                                             NULL);
                       RETURN(-1);
                    END IF;
                    if (L_parm_seq_no_3 is NOT NULL) then
                        /* get the parameter3 info */
                        L_parm_seq_no := L_parm_seq_no_3;
                        open c_get_parameter_info;
                        fetch c_get_parameter_info INTO v_realm3,
                                                        v_parm3;
                        close c_get_parameter_info;
                        IF v_realm3 is null or v_parm3 is null THEN
                            raise e_total_def_error;
                        END IF;
                    END if;
                    /* if there is going to be a HAVING clause */
                    if NOT b_select_done then
                        v_select(v_select.count+1) := 'where ' || v_object_name
                            || ' ' || v_operator || ' ' || v_realm2 || '.' || v_parm2;
                        b_select_done := TRUE;
                    ELSE
                        v_select(v_select.count+1) := 'and ' || v_object_name || ' '
                            || v_operator || ' ' || v_realm2 || '.' || v_parm2;
                    end if;
                    /* if parm 3 is not null, it must be a between statement */
                    if (v_realm3 is NOT NULL) and (v_parm3 is NOT NULL) then
                        if b_between_op then
                            v_select(v_select.count) := v_select(v_select.count) ||
                                ' and ' || v_realm3 || '.' || v_parm3;
                        ELSE
                            RAISE e_total_def_error;
                        end if;
                    ELSE
                        if b_between_op then
                            RAISE e_total_def_error;
                        end if;
                    end if;
                /* Constants are defined for this exception */
                ELSIF (v_constant_2 is NOT NULL) AND (NOT b_null_op) then

                   IF v_operator = 'IN' or v_operator = 'NOT IN' THEN
                       v_constant_2 := '(' || v_constant_2 || ')';
                   END if;

                    /* determine if the data type of the constant is a string or numeric */
                    if instr(v_data_type, 'VARCHAR') > 0 then
                       /* if the operator is IN or NOT IN we do not put quotes around the constants */
                       if v_operator NOT in ('IN', 'NOT IN') then
                           b_string := TRUE;
                       END IF;
                    /* If date, place the TO_CHAR function in the object name*/
                    ELSIF instr(v_data_type, 'DATE') > 0 THEN
                       v_object_name := 'To_Char (' || v_object_name || ', ' || chr(39) ||
                                        v_db_date_format || chr(39) || ')';
                       /* If the operator is IN we do not put quotes around the constants */
                       if v_operator NOT in ('IN', 'NOT IN') then
                           b_string := TRUE;
                       END IF;
                    END IF;
                    /* The constant datatype is numeric */
                    if NOT b_string then
                        IF NOT b_select_done then
                            v_select(v_select.count+1) := 'where ' ||
                                v_object_name || ' ' || v_operator || ' ' || v_constant_2;
                            b_select_done := TRUE;
                        ELSE
                            v_select(v_select.count+1) := 'and ' || v_object_name
                                || ' ' || v_operator || ' ' || v_constant_2;
                        end if;
                        /* if contstant 3 is not null, it must be a between statement */
                        if (v_constant_3 is NOT NULL) then
                            if b_between_op then
                                v_select(v_select.count) := v_select(v_select.count) ||
                                ' and ' || v_constant_3;
                            ELSE
                                RAISE e_total_def_error;
                            end if;
                        ELSE
                            if b_between_op then
                                RAISE e_total_def_error;
                            end if;
                        end if;
                    /* if the constant datatype is a string */
                    ELSE
                        IF NOT b_select_done then
                            v_select(v_select.count+1) := 'where ' || v_object_name
                                || ' ' || v_operator || ' ' || chr(39) || v_constant_2 || chr(39);
                            b_select_done := TRUE;
                        ELSE
                            v_select(v_select.count+1) := 'and ' || v_object_name
                                || ' ' || v_operator || ' ' || chr(39) || v_constant_2 || chr(39);
                        end if;
                        /* if contstant 3 is not null, it must be a between statement */
                        if (v_constant_3 is NOT NULL) then
                            if b_between_op then
                                v_select(v_select.count) := v_select(v_select.count) ||
                                ' and ' || CHR(39) || v_constant_3 || CHR(39);
                            ELSE
                                RAISE e_total_def_error;
                            end if;
                        ELSE
                            if b_between_op then
                                RAISE e_total_def_error;
                            end if;
                        end if; /* v_constant_3 is NOT NULL */
                    end if; /* Datatype is a string */
                /* Check for the is NULL or is NOT NULL operators */
                ELSif b_null_op then
                    if NOT b_select_done then
                        v_select(v_select.count+1) := 'where ' || v_object_name ||
                            ' ' || v_operator;
                        b_select_done := TRUE;
                    ELSE
                        v_select(v_select.count+1) := 'and ' || v_object_name
                            || ' ' || v_operator;
                    end if;
                ELSE
                    /* No constants or parameters are defined */
                    RAISE e_total_def_error;
                end if; /* check for parms or constants */
                b_string := FALSE;
            END;  /* Begin */
        end loop;
        close c_get_exceptions;
        /* include the exception that filters out only the passed store day */
        if NOT b_select_done then
            v_select(v_select.count+1) := 'where ' ||
                'SA_STORE_DAY.STORE_DAY_SEQ_NO = I_store_day_seq_no';
            b_select_done := TRUE;
        ELSE
            v_select(v_select.count+1) := 'and ' ||
                'SA_STORE_DAY.STORE_DAY_SEQ_NO = I_store_day_seq_no';
        end if;
        v_select(v_select.count+1) := 'and SA_STORE_DAY.STORE = l_store';
        v_select(v_select.count+1) := 'and SA_STORE_DAY.DAY = l_day';
        if L_exists ='Y' then
           v_select(v_select.count+1) := 'and SA_TRAN_HEAD.STATUS !=
           ' || chr(39) || 'D' || CHR(39);
        end if;
        /* Only process if the store days audit status is (R)ecalcuate or
           (U)n-audited, or that if the total entry for that store day does
           not exist. */
        v_select(v_select.count+1) := 'and (SA_STORE_DAY.AUDIT_STATUS =
            ' || chr(39) || 'R' || CHR(39);
        v_select(v_select.count+1) := '   or SA_STORE_DAY.STORE_DAY_SEQ_NO NOT IN';
        v_select(v_select.count+1) := '      (select SA_TOTAL.STORE_DAY_SEQ_NO';
        v_select(v_select.count+1) := '       from SA_TOTAL';
        v_select(v_select.count+1) := '       where SA_TOTAL.TOTAL_ID = '
            || chr(39) || I_total_id || chr(39);
        v_select(v_select.count+1) := '       and SA_TOTAL.STORE = SA_STORE_DAY.STORE';
        if L_exists ='Y' then
                v_select(v_select.count+1) := '       and SA_TOTAL.STATUS !=
            ' || chr(39) || 'D' || CHR(39);
        end if;
        v_select(v_select.count+1) := '       and SA_TOTAL.DAY = SA_STORE_DAY.DAY))';
        v_select(v_select.count+1) := '          ';
        v_select(v_select.count+1) := '/*      Group by Clause      */';
        v_select(v_select.count+1) := '          ';
        /***************************************************************\
        |                                                               |
        |   get the group by...group by statement                       |
        |                                                               |
        \***************************************************************/
        if b_group_by_done then
            v_select(v_select.count+1) := v_group_by;
        end if;

        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_GROUP_BY',
                         'SA_TOTAL_HEAD, SA_VR_PARMS, V_SA_REALM_INFO, V_SA_PARM_INFO',
                         'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
        open c_get_group_by;
        loop
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_GROUP_BY',
                             'SA_TOTAL_HEAD, SA_VR_PARMS, V_SA_REALM_INFO, V_SA_PARM_INFO',
                             'Total: '||I_total_id||', Rev. No.: '||to_char(I_total_rev_no));
            fetch   c_get_group_by INTO v_object_name;
            EXIT WHEN c_get_group_by%NOTFOUND;
            BEGIN
                if b_group_by_done then
                    v_select(v_select.count+1) := '         ' || v_object_name || ',';
                ELSE
                    v_select(v_select.count+1) := 'group by ' || v_object_name || ',';
                    b_group_by_done := TRUE;
                end if;
            END;
        end loop;
        close c_get_group_by;
        /* Remove the final comma on the group by statement */
        v_select(v_select.count) := rtrim(v_select(v_select.count), ', ');
        v_select(v_select.count) := v_select(v_select.count) || ';';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := 'BEGIN';
        /* include the date constraints for starting and ending the total */
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  /* Check the date contraints */';
        v_select(v_select.count+1) := '        ';

        v_select(v_select.count+1) := '   if not sa_total_common.get_tran_datetime(i_store_day_seq_no, l_tran_date, l_store, l_day, o_error_message) then';
        v_select(v_select.count+1) := '      o_error_message := sql_lib.create_msg(' || CHR(39) || 'SATOTRUL_NOSTRDAY' || CHR(39) || ',';
        v_select(v_select.count+1) := '                                            i_store_day_seq_no, NULL, NULL);';
        v_select(v_select.count+1) := '      return(-1);';
        v_select(v_select.count+1) := '   end if;';
        v_select(v_select.count+1) := '        ';

        if d_end_date is NOT NULL then
            v_select(v_select.count+1) := '  if (l_tran_date >= TO_DATE(' || CHR(39) ||
                d_start_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD'  || CHR(39) || ')) and (l_tran_date <= TO_DATE(' || CHR(39) ||
                d_end_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD' || CHR(39) || ')) then';
        else
            v_select(v_select.count+1) := '  if l_tran_date >= TO_DATE(' || CHR(39) ||
                d_start_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD' || CHR(39) || ') then';
        end if;

        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '    /* Begin cursor loop */';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '    for L_rec IN c_sum loop';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '        l_sum                := L_rec.TOTAL;';
        /* if bal_group is present, include it in the fetch */
        if b_bal_group then
            v_select(v_select.count+1) := '        l_bal_group_seq_no   := L_rec.BAL_GROUP;';
        end if;
        /* include all ref_nos defined in the fetch statement */
        for n_count IN 1..n_counter loop
            v_select(v_select.count+1) := '        l_ref_no' || n_count
                || '            := L_rec.REF_NO' || n_count || ';';
        end loop;
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '        if l_sum IS NOT NULL then';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '           /* Insert the total into the database */';
        v_select(v_select.count+1) := '           ';
        v_select(v_select.count+1) := '           i_result := SA_BUILD_TOTAL_SQL.INSERT_TOTAL ('
            || 'O_error_message,';
        v_select(v_select.count+1) := '            '
            || chr(39) || I_total_id || chr(39) || ',';
        /* We always pass the store day */
        v_select(v_select.count+1) := '               I_store_day_seq_no,';
        /* if the bal group is defined, pass variable, otherwise pass NULL */
        if b_bal_group then
            v_select(v_select.count+1) := '               l_bal_group_seq_no,';
        ELSE
            v_select(v_select.count+1) := '               NULL,';
        end if;
        /* Include the ref_no variables for ref_nos defined */
        for n_count IN 1..n_counter loop
            v_select(v_select.count+1) := '               l_ref_no' || n_count || ',';
        end loop;
        /* Include NULLS for the ref_nos not defined */
        for n_count IN 1..(3 - n_counter) loop
            v_select(v_select.count+1) := '               NULL,';
        end loop;
        v_select(v_select.count+1) := '               l_sum,';
        v_select(v_select.count+1) := '               ' || I_total_rev_no || ',';
        v_select(v_select.count+1) := '               ' || chr(39) || I_pos_ind || chr(39) || ',';
        v_select(v_select.count+1) := '               l_store,';
        v_select(v_select.count+1) := '               l_day);';
        /* check the result of the insert total function */
        v_select(v_select.count+1) := '           ';
        v_select(v_select.count+1) := '           /* Check to see if the insert succeeded */';
        v_select(v_select.count+1) := '           ';
        v_select(v_select.count+1) := '           if i_result <> 0 then';
        v_select(v_select.count+1) := '               RETURN(-1);';
        v_select(v_select.count+1) := '           end if;';
        v_select(v_select.count+1) := '           ';
        v_select(v_select.count+1) := '        end if;';
        v_select(v_select.count+1) := '           ';
        v_select(v_select.count+1) := '    end loop;';
        /* End the date check if statement */
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  end if;';
        /* Add error trapping */
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  RETURN(0);';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  EXCEPTION';
        v_select(v_select.count+1) := '      WHEN OTHERS then';
        v_select(v_select.count+1) := '          O_error_message := sql_lib.create_msg(';
        v_select(v_select.count+1) := '              ' || chr(39) || 'PACKAGE_ERROR' || chr(39) || ',';
        v_select(v_select.count+1) := '              SQLERRM,';
        v_select(v_select.count+1) := '              ' || chr(39) || v_name || chr(39) || ',';
        v_select(v_select.count+1) := '              SQLCODE);';
        v_select(v_select.count+1) := '          RETURN(-1);';
        v_select(v_select.count+1) := 'END;';
        /* load the DBMS_SQL.VARCHAR2 variable with the v_select data */
        for n_count IN 1..v_select.count
        loop
            v_sql(v_sql.count+1) := v_select(n_count);
        end loop;
        /* open the DBMS_SQL cursor */
        cursor_handle := DBMS_SQL.OPEN_CURSOR;
        /* pass in the dynamic function code for execution */
        DBMS_SQL.PARSE (cursor_handle, v_sql, v_sql.FIRST,
                        v_sql.LAST, TRUE, DBMS_SQL.NATIVE);
        /* execute the dynamic function code */
        feedback := DBMS_SQL.EXECUTE(cursor_handle);
        /* close cursor */
        DBMS_SQL.CLOSE_CURSOR(cursor_handle);
        /***************************************************************\
        |                                                               |
        |   This will determine if the code compiled successfully       |
        |                                                               |
        \***************************************************************/

         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_COMPILE',
                          'ALL_OBJECTS',
                          'Function: '||v_name);
        open c_check_compile;
         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_COMPILE',
                          'ALL_OBJECTS',
                          'Function: '||v_name);
        fetch c_check_compile INTO v_status;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_COMPILE',
                          'ALL_OBJECTS',
                          'Function: '||v_name);
        close c_check_compile;
        if v_status = 'INVALID' then
            /* Compile did not work */
            O_error_message := sql_lib.create_msg('SABLDTL_COMPILE',
                                                  I_total_id,
                                                  NULL,
                                                  NULL);
            RETURN(-1);
        end if;
        /***************************************************************\
        |                                                               |
        |   Delete any invalid definition errors for this total         |
        |                                                               |
        \***************************************************************/
        if SA_BUILD_TOTAL_SQL.DELETE_INVALID_DEF_ERROR(O_error_message,
                                                       I_total_id,
                                                       NULL /* Rule ID */) = FALSE then
           return (-1);
        end if;
        ---
        RETURN(0);
    EXCEPTION
        WHEN e_total_def_error then
            /* We found a total that was incorrectly defined */
            O_error_message := sql_lib.create_msg('SABLDTL_WRONGDEF',
                                                  I_total_id,
                                                  NULL,
                                                  NULL);
            RETURN(-1);
        WHEN others then
            /*Close the cursor c_get_select_parms if it is open*/
            if c_get_select_parms%ISOPEN then
              close c_get_select_parms;
            end if;
            O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                   SQLERRM,
                                                   'SA_BUILD_TOTAL_SQL.BUILD_TOTAL_FUNCTION',
                                                   SQLCODE);
            RETURN(-1);
    END BUILD_TOTAL_FUNCTION;
--------------------------------------------------------------------------------------------
    FUNCTION DROP_TOTAL_FUNCTION (O_error_message  IN OUT VARCHAR2,
                                  I_total_id       IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                  I_total_rev_no   IN     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE)
       RETURN INT
   /****************************************************************\
   |                                                                |
   |   DROP_TOTAL_FUNCTION                                          |
   |                                                                |
   |   This function will drop a single total function.             |
   |                                                                |
   |    Arguments:          Description:                            |
   |    I_total_id          Total identifier to create.             |
   |    O_error_message     Returned error message                  |
   |                                                                |
   |    Called from:        Sales Audit form: sa_total.fmb          |
   |                        Sales Audit dynamic total functions     |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
    IS
       result              BOOLEAN;
       cursor_handle       INTEGER;
       feedback            INTEGER;
       v_name              VARCHAR2(30);
       v_sql               VARCHAR2(2000);
       d_end_date          VARCHAR2(6);
       d_start_date        VARCHAR2(6);
       v_dates             VARCHAR2(18) := NULL;
       ---
       cursor c_get_function is
          select object_name
            from all_objects
           where object_name = 'SA_T_' || upper(I_total_id) || v_dates
             and object_type = 'FUNCTION'
             and lower(owner) = lower(sa_total_common.internal_schema_name)
             and NOT EXISTS (
                 select 'x'
                   from all_jobs
                  where instr(UPPER(what), UPPER(object_name)) <>0);
       ---
       cursor C_GET_TOTAL_INFO is
          select to_char(x.start_business_date, 'YYMMDD'),
                 to_char(x.end_business_date, 'YYMMDD')
            from sa_total_head x
           where x.total_id = I_total_id
             and x.status not in ('DI', 'DE')
             and not exists (select distinct 'N'
                               from sa_total_head z
                              where z.total_id     = x.total_id
                                and z.total_rev_no > x.total_rev_no
                                and (   (z.start_business_date <= x.start_business_date and
                                         z.end_business_date   >= x.start_business_date and
                                         z.end_business_date   is NOT NULL)
                                     or (z.start_business_date <= x.start_business_date and
                                         z.end_business_date   is NULL)
                                     or (z.start_business_date >= x.start_business_date and
                                         x.end_business_date   is NULL)
                                     or (z.start_business_date >= x.start_business_date and
                                         z.end_business_date   <= x.end_business_date and
                                         z.end_business_date   is NOT NULL)
                                     or (z.start_business_date <= x.end_business_date and
                                         z.end_business_date   >= x.end_business_date and
                                         z.end_business_date   is NOT NULL)
                                     or (z.start_business_date <= x.end_business_date and
                                         z.end_business_date   is NULL)
                                  ));
       ---
       cursor C_get_start_end_date is
          select to_char(start_business_date,'YYMMDD'),
                 to_char(end_business_date,'YYMMDD')
            from sa_total_head
           where total_id     = I_total_id
             and total_rev_no = I_total_rev_no;
       ---
    BEGIN
       if I_total_rev_no is NOT NULL then
          SQL_LIB.SET_MARK('OPEN',
                           'C_GET_START_END_DATE',
                           'SA_TOTAL_HEAD',
                           'Total ID: '||I_total_id);
          open  C_get_start_end_date;
          SQL_LIB.SET_MARK('FETCH',
                           'C_GET_START_END_DATE',
                           'SA_TOTAL_HEAD',
                           'Total ID: '||I_total_id);
          fetch C_get_start_end_date into d_start_date, d_end_date;
          SQL_LIB.SET_MARK('CLOSE',
                           'C_GET_START_END_DATE',
                           'SA_TOTAL_HEAD',
                           'Total ID: '||I_total_id);
          close C_get_start_end_date;
          if d_end_date is NULL then
             v_dates := '_' || d_start_date;
          else
             v_dates := '_' || d_start_date || '_' || d_end_date;
          end if;
          ---
          SQL_LIB.SET_MARK('OPEN',
                           'C_GET_FUNCTION',
                           'ALL_OBJECTS',
                           'Total ID: '||I_total_id);
          OPEN  c_get_function;
          SQL_LIB.SET_MARK('FETCH',
                           'C_GET_FUNCTION',
                           'ALL_OBJECTS',
                           'Total ID: '||I_total_id);
          FETCH c_get_function INTO v_name;
          SQL_LIB.SET_MARK('CLOSE',
                           'C_GET_FUNCTION',
                           'ALL_OBJECTS',
                           'Total ID: '||I_total_id);
          CLOSE c_get_function;
          if v_name is NOT NULL then
             v_sql := 'drop function ' ||
                      sa_total_common.internal_schema_name ||
                      '.' || v_name;
             cursor_handle := DBMS_SQL.OPEN_CURSOR;
             DBMS_SQL.PARSE(cursor_handle, v_sql, DBMS_SQL.NATIVE);
             feedback := DBMS_SQL.EXECUTE(cursor_handle);
             DBMS_SQL.CLOSE_CURSOR(cursor_handle);
          end if;
       else --- I_total_rev_no is NULL
          SQL_LIB.SET_MARK('OPEN',
                           'C_GET_TOTAL_INFO',
                           'SA_TOTAL_HEAD',
                           'Total ID: '||I_total_id);
          open  C_GET_TOTAL_INFO;
          LOOP
             SQL_LIB.SET_MARK('FETCH',
                              'C_GET_TOTAL_INFO',
                              'SA_TOTAL_HEAD',
                              'Total ID: '||I_total_id);
             fetch C_GET_TOTAL_INFO into d_start_date, d_end_date;
             EXIT when C_GET_TOTAL_INFO%NOTFOUND;
             BEGIN
                if d_end_date is NULL then
                   v_dates := '_' || d_start_date;
                else
                   v_dates := '_' || d_start_date || '_' || d_end_date;
                end if;
                ---
                SQL_LIB.SET_MARK('OPEN',
                                 'C_GET_FUNCTION',
                                 'ALL_OBJECTS',
                                 'Total ID: '||I_total_id);
                OPEN  c_get_function;
                SQL_LIB.SET_MARK('FETCH',
                                 'C_GET_FUNCTION',
                                 'ALL_OBJECTS',
                                 'Total ID: '||I_total_id);
                FETCH c_get_function INTO v_name;
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_GET_FUNCTION',
                                 'ALL_OBJECTS',
                                 'Total ID: '||I_total_id);
                CLOSE c_get_function;
                if v_name is NOT NULL then
                   v_sql := 'drop function ' ||
                            sa_total_common.internal_schema_name ||
                            '.' || v_name;
                   cursor_handle := DBMS_SQL.OPEN_CURSOR;
                   DBMS_SQL.PARSE(cursor_handle, v_sql, DBMS_SQL.NATIVE);
                   feedback := DBMS_SQL.EXECUTE(cursor_handle);
                   DBMS_SQL.CLOSE_CURSOR(cursor_handle);
                end if;
             END;
          end LOOP;
          SQL_LIB.SET_MARK('CLOSE',
                           'C_GET_TOTAL_INFO',
                           'SA_TOTAL_HEAD',
                           'Total ID: '||I_total_id);
          close C_GET_TOTAL_INFO;
       end if;
       ---
       RETURN(0);
       ---
    EXCEPTION
        WHEN OTHERS then
            /*Close the cursor c_get_total_info if it is open*/
            if c_get_total_info%ISOPEN then
              close c_get_total_info;
            end if;
            O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  'SA_BUILD_TOTAL_SQL.DROP_TOTAL_FUNCTION',
                                                  SQLCODE);
            RETURN(-1);
    END DROP_TOTAL_FUNCTION;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_TOTAL (O_error_message          IN OUT   VARCHAR2,
                       I_total_id               IN       SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                       I_store_day_seq_no       IN       SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                       I_balance_group_seq_no   IN       SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                       I_ref_no1                IN       SA_TOTAL.REF_NO1%TYPE,
                       I_ref_no2                IN       SA_TOTAL.REF_NO2%TYPE,
                       I_ref_no3                IN       SA_TOTAL.REF_NO3%TYPE,
                       I_total_amount           IN       SA_SYS_VALUE.SYS_VALUE%TYPE,
                       I_total_rev_no           IN       SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                       I_pos_ind                IN       SA_TOTAL_HEAD.POS_IND%TYPE,
                       I_store                  IN       SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
                       I_day                    IN       SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
RETURN INT IS
   /****************************************************************\
   |                                                                |
   |   INSERT_TOTAL                                                 |
   |                                                                |
   |   This function will generate a single total function as       |
   |   defined in the sa_total_head and corresponding tables.       |
   |                                                                |
   |    Arguments:          Description:                            |
   |    O_error_message     Returned error message                  |
   |    I_total_id          Total identifier to create.             |
   |    I_store_day_seq_no  Store day sequence number.              |
   |    I_store_day_seq_no  Balance group sequence number.          |
   |    I_ref_no1           reference number 1.                     |
   |    I_ref_no2           reference number 2.                     |
   |    I_ref_no3           reference number 3.                     |
   |    I_total_amount      Total amount.                           |
   |    I_total_rev_no      Total revision number.                  |
   |    I_pos_ind           if value passed is 'Y' the data will be |
   |                        inserted into the sa_pos_value table,   |
   |                        otherwise it will write to the          |
   |                        sa_sys_value table.                     |
   |                                                                |
   |    Called from:        dynamic total functions                 |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
   result  INT;
   ln_total_seq_no   SA_TOTAL.TOTAL_SEQ_NO%TYPE       := NULL;
   l_total_status    VARCHAR(6);
   l_precedence      V_SA_GET_TOTAL.PRECEDENCE%TYPE   := NULL;
   l_value_rev_no    V_SA_GET_TOTAL.VALUE_REV_NO%TYPE := NULL;
   l_error_seq_no    SA_ERROR_REV.ERROR_SEQ_NO%TYPE   := NULL;
   l_error_rev_no    SA_ERROR_REV.REV_NO%TYPE         := NULL;
   L_store           SA_STORE_DAY.STORE%TYPE          := I_store;
   L_day             SA_STORE_DAY.DAY%TYPE            := I_day;

   cursor C_GET_TOTAL_SEQ_NO is
      select total_seq_no
        from sa_total
       where total_id = I_total_id
         and (store_day_seq_no = I_store_day_seq_no
              or store_day_seq_no is NULL)
         and (bal_group_seq_no = I_balance_group_seq_no
              or bal_group_seq_no is NULL)
         and (store = L_store)
         and (day = L_day)
         and (ref_no1 = I_ref_no1 or ref_no1 is NULL)
         and (ref_no2 = I_ref_no2 or ref_no2 is NULL)
         and (ref_no3 = I_ref_no3 or ref_no3 is NULL);

   cursor C_GET_STATUS is
      select status
        from sa_total_head
       where total_id = I_total_id
         and total_rev_no = I_total_rev_no;

   /* Retrieve any errors that need to be moved to the revision table */
   cursor C_GET_ERROR_FOR_REVISION is
      select e1.error_seq_no
        from sa_error e1
       where e1.store_day_seq_no = I_store_day_seq_no
         and e1.total_seq_no = ln_total_seq_no
         and e1.store = L_store
         and e1.day = L_day
         and e1.error_code in ( select ec.error_code
                                  from sa_error_codes ec
                                 where ec.required_ind = 'N');

   /* Retrieve the value_rev_no that should be used in the insert to the revision table.
      This value will be the maximum value_rev_no found for the total. */
   cursor C_GET_VALUE_REV_NO is
      select NVL( max(value_rev_no), 1)
        from v_sa_total
       where total_seq_no = ln_total_seq_no
         and store = L_store
         and day = L_day;

    /* Retrieve the error rev_no that should be incremented in the insert to the revision table.
      This value will be the maximum error rev_no. */
   cursor C_GET_ERROR_REV_NO is
      select NVL(max(rev_no),0)
        from sa_error_rev
       where store_day_seq_no = I_store_day_seq_no
         and store            = L_store
         and day              = L_day
         and total_seq_no     = ln_total_seq_no
         and error_seq_no     = l_error_seq_no;

BEGIN
   if L_store is NULL or L_day is NULL then
      if STORE_DAY_SQL.GET_INTERNAL_DAY(O_error_message,
                                        L_store,
                                        L_day,
                                        I_store_day_seq_no,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL) = FALSE then
         return (-1);
      end if;
   end if;
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STATUS',
                    'SA_TOTAL_HEAD',
                    'Total ID: '||I_total_id);
   open  C_GET_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STATUS',
                    'SA_TOTAL_HEAD',
                    'Total ID: '||I_total_id);
   fetch C_GET_STATUS into l_total_status;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STATUS',
                    'SA_TOTAL_HEAD',
                    'Total ID: '||I_total_id);
   close C_GET_STATUS;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TOTAL_SEQ_NO',
                    'SA_TOTAL',
                    'Total ID: '||I_total_id);
   open C_GET_TOTAL_SEQ_NO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TOTAL_SEQ_NO',
                    'SA_TOTAL',
                    'Total ID: '||I_total_id);
   fetch C_GET_TOTAL_SEQ_NO INTO ln_total_seq_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TOTAL_SEQ_NO',
                    'SA_TOTAL',
                    'Total ID: '||I_total_id);
   close C_GET_TOTAL_SEQ_NO;
   if ln_total_seq_no is NULL then
      -- first time this total has been run.
      if NOT SA_SEQUENCE2_SQL.GET_TOTAL_SEQ(O_error_message,
                                            ln_total_seq_no) then
         RETURN(-1);
      end if;
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'SA_TOTAL',
                       'Total ID: '||I_total_id);
      INSERT INTO sa_total (total_seq_no,
                            total_id,
                            store_day_seq_no,
                            ref_no1,
                            ref_no2,
                            ref_no3,
                            bal_group_seq_no,
                            display_order,
                            error_ind,
                            status,
                            store,
                            day)
      SELECT  ln_total_seq_no,
              I_total_id,
              I_store_day_seq_no,
              I_ref_no1,
              I_ref_no2,
              I_ref_no3,
              I_balance_group_seq_no,
              display_order,
              'N',
              'P',
              L_store,
              L_day
      from    sa_total_head
      where   total_id = I_total_id
      and     total_rev_no = I_total_rev_no;

      if I_pos_ind = 'Y' then
          if l_total_status = 'A' then
             SQL_LIB.SET_MARK('INSERT',
                              NULL,
                              'SA_POS_VALUE',
                              'Total ID: '||I_total_id);
             INSERT INTO sa_pos_value (total_seq_no,
                                       value_rev_no,
                                       total_rev_no,
                                       pos_value,
                                       update_id,
                                       update_datetime,
                                       store,
                                       day)
             VALUES (ln_total_seq_no,
                     1,
                     I_total_rev_no,
                     I_total_amount,
                     USER,
                     SYSDATE,
                     L_store,
                     L_day);
         ELSE
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'SA_POS_VALUE_WKSHT',
                             'Total ID: '||I_total_id);
            INSERT INTO sa_pos_value_wksht (total_seq_no,
                                            value_rev_no,
                                            total_rev_no,
                                            pos_value,
                                            update_id,
                                            update_datetime,
                                            store,
                                            day)
             VALUES (ln_total_seq_no,
                     1,
                     I_total_rev_no,
                     I_total_amount,
                     USER,
                     SYSDATE,
                     L_store,
                     L_day);
         end if;
      ELSE
         if l_total_status = 'A' then
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'SA_SYS_VALUE',
                             'Total ID: '||I_total_id);
            INSERT INTO sa_sys_value (total_seq_no,
                                      value_rev_no,
                                      total_rev_no,
                                      sys_value,
                                      update_id,
                                      update_datetime,
                                      store,
                                      day)
            VALUES (ln_total_seq_no,
                    1,
                    I_total_rev_no,
                    I_total_amount,
                    USER,
                    SYSDATE,
                    L_store,
                    L_day);
         ELSE
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'SA_SYS_VALUE',
                             'Total ID: '||I_total_id);
            INSERT INTO sa_sys_value_wksht (total_seq_no,
                                            value_rev_no,
                                            total_rev_no,
                                            sys_value,
                                            update_id,
                                            update_datetime,
                                            store,
                                            day)
            VALUES (ln_total_seq_no,
                    1,
                    I_total_rev_no,
                    I_total_amount,
                    USER,
                    SYSDATE,
                    L_store,
                    L_day);
         end if;
      end if;
   ELSE
      --- recalculating an existing total
      ---
      --- Copy any existing errors based on the current total to the error revision table.
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_ERROR_FOR_REVISION',
                       'SA_ERROR',
                       'Total Seq. No.: '||to_char(ln_total_seq_no));
      for error_rec in C_GET_ERROR_FOR_REVISION LOOP
         l_error_seq_no := error_rec.error_seq_no;
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_VALUE_REV_NO',
                          'V_SA_TOTAL',
                          'Total Seq. No.: '||to_char(ln_total_seq_no));
         open C_GET_VALUE_REV_NO;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_VALUE_REV_NO',
                          'V_SA_TOTAL',
                          'Total Seq. No.: '||to_char(ln_total_seq_no));
         fetch C_GET_VALUE_REV_NO into l_value_rev_no;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_VALUE_REV_NO',
                          'V_SA_TOTAL',
                          'Total Seq. No.: '||to_char(ln_total_seq_no));
         close C_GET_VALUE_REV_NO;
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_ERROR_REV_NO',
                          'SA_ERROR_REV',
                          'Total Seq. No.: '||to_char(ln_total_seq_no)||', Error Seq. No.: '||to_char(l_error_seq_no));
         open C_GET_ERROR_REV_NO;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_ERROR_REV_NO',
                          'SA_ERROR_REV',
                          'Total Seq. No.: '||to_char(ln_total_seq_no)||', Error Seq. No.: '||to_char(l_error_seq_no));
         fetch C_GET_ERROR_REV_NO into l_error_rev_no;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_ERROR_REV_NO',
                          'SA_ERROR_REV',
                          'Total Seq. No.: '||to_char(ln_total_seq_no)||', Error Seq. No.: '||to_char(l_error_seq_no));
         close C_GET_ERROR_REV_NO;
         ---
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'SA_ERROR_REV',
                          'Total ID: '||I_total_id);
         INSERT INTO sa_error_rev
                     ( error_seq_no,
                       rev_no,
                       store_day_seq_no,
                       store,
                       day,
                       bal_group_seq_no,
                       total_seq_no,
                       value_rev_no,
                       tran_seq_no,
                       tran_rev_no,
                       error_code,
                       key_value_1,
                       key_value_2,
                       rec_type,
                       store_override_ind,
                       hq_override_ind,
                       update_id,
                       update_datetime,
                       orig_value )
              select  e1.error_seq_no,
                      l_error_rev_no + 1,
                      e1.store_day_seq_no,
                      e1.store,
                      e1.day,
                      e1.bal_group_seq_no,
                      e1.total_seq_no,
                      l_value_rev_no,
                      e1.tran_seq_no,
                      NULL,            --- tran_rev_no
                      e1.error_code,
                      e1.key_value_1,
                      e1.key_value_2,
                      e1.rec_type,
                      e1.store_override_ind,
                      e1.hq_override_ind,
                      e1.update_id,
                      e1.update_datetime,
                      e1.orig_value
                 from sa_error e1
                where e1.error_seq_no = l_error_seq_no
                  and e1.store = L_store
                  and e1.day = L_day
                  and not exists ( select 'x'
                                     from sa_error_rev er
                                    where er.total_seq_no = e1.total_seq_no
                                      and er.store        = e1.store
                                      and er.day          = e1.day
                                      and value_rev_no    = l_value_rev_no
                                      and er.error_code   = e1.error_code);
      end LOOP;
      if l_value_rev_no is NOT NULL then
         l_value_rev_no := NULL;
      end if;
      ---
      if SA_TOTAL_SQL.GET_PRECEDENCE(O_error_message,
                                     ln_total_seq_no,
                                     l_precedence,
                                     l_value_rev_no,
                                     L_store,
                                     L_day) = FALSE then
         return (-1);
      end if;

      update sa_total
         set status = 'P'
       where total_seq_no = ln_total_seq_no
         and store_day_seq_no = I_store_day_seq_no
         and store = L_store
         and day = L_day
         and status = 'D';
      ---
      if I_pos_ind = 'Y' then
         if l_total_status = 'A' then
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'SA_POS_VALUE',
                             'Total ID: '||I_total_id);
            INSERT INTO sa_pos_value (total_seq_no,
                                      value_rev_no,
                                      total_rev_no,
                                      pos_value,
                                      update_id,
                                      update_datetime,
                                      store,
                                      day)
            select  ln_total_seq_no,
                 (GREATEST(nvl(max(hq.value_rev_no), 0),
                           nvl(max(pos.value_rev_no), 0),
                           nvl(max(st.value_rev_no), 0),
                           nvl(max(sys.value_rev_no), 0),
                           nvl(max(posw.value_rev_no), 0),
                           nvl(max(sysw.value_rev_no), 0)) + 1),
                           I_total_rev_no,
                           I_total_amount,
                           USER,
                           SYSDATE,
                           L_store,
                           L_day
              from sa_total t,sa_hq_value hq, sa_pos_value pos,
                   sa_store_value st, sa_sys_value sys,
                   sa_pos_value_wksht posw, sa_sys_value_wksht sysw
             where t.total_seq_no = ln_total_seq_no
               and t.store        = L_store
               and t.day          = L_day
               and t.total_seq_no = pos.total_seq_no(+)
               and t.store        = pos.store(+)
               and t.day          = pos.day(+)
               and t.total_seq_no = hq.total_seq_no(+)
               and t.store        = hq.store(+)
               and t.day          = hq.day(+)
               and t.total_seq_no = st.total_seq_no(+)
               and t.store        = st.store(+)
               and t.day          = st.day(+)
               and t.total_seq_no = sys.total_seq_no(+)
               and t.store        = sys.store(+)
               and t.day          = sys.day(+)
               and t.total_seq_no = posw.total_seq_no(+)
               and t.store        = posw.store(+)
               and t.day          = posw.day(+)
               and t.total_seq_no = sysw.total_seq_no(+)
               and t.store        = sysw.store(+)
               and t.day          = sysw.day(+);
            ---
            if l_precedence >= 4 then --- Need to insert POS total into sa_exported_rev
               SQL_LIB.SET_MARK('INSERT',
                                NULL,
                                'SA_EXPORTED_REV',
                                'Total Seq. No.: '||to_char(ln_total_seq_no));
               INSERT INTO sa_exported_rev (export_seq_no,
                                            rev_no,
                                            store_day_seq_no,
                                            store,
                                            day,
                                            tran_seq_no,
                                            total_seq_no,
                                            acct_period,
                                            system_code,
                                            exp_datetime,
                                            status)
                   select  export_seq_no,
                           l_value_rev_no,
                           store_day_seq_no,
                           store,
                           day,
                           tran_seq_no,
                           total_seq_no,
                           acct_period,
                           system_code,
                           exp_datetime,
                           status
                     from  sa_exported
                    where  total_seq_no = ln_total_seq_no
                      and  store = L_store
                      and  day = L_day;
               ---
               SQL_LIB.SET_MARK('DELETE',
                                NULL,
                                'SA_EXPORTED',
                                'Total Seq. No.: '||to_char(ln_total_seq_no));
               DELETE from sa_exported
                     where total_seq_no = ln_total_seq_no
                       and store = L_store
                       and day = L_day;
            end if;

         ELSE
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'SA_POS_VALUE_WKSHT',
                             'Total Seq. No.: '||to_char(ln_total_seq_no));
            INSERT INTO sa_pos_value_wksht (total_seq_no,
                                            value_rev_no,
                                            total_rev_no,
                                            pos_value,
                                            update_id,
                                            update_datetime,
                                            store,
                                            day)
            select  ln_total_seq_no,
                 (GREATEST(nvl(max(hq.value_rev_no), 0),
                           nvl(max(pos.value_rev_no), 0),
                           nvl(max(st.value_rev_no), 0),
                           nvl(max(sys.value_rev_no), 0),
                           nvl(max(posw.value_rev_no), 0),
                           nvl(max(sysw.value_rev_no), 0)) + 1),
                           I_total_rev_no,
                           I_total_amount,
                           USER,
                           SYSDATE,
                           L_store,
                           L_day
              from sa_total t,sa_hq_value hq, sa_pos_value pos,
                   sa_store_value st, sa_sys_value sys,
                   sa_pos_value_wksht posw, sa_sys_value_wksht sysw
             where t.total_seq_no = ln_total_seq_no
               and t.store        = L_store
               and t.day          = L_day
               and t.total_seq_no = pos.total_seq_no(+)
               and t.store        = pos.store(+)
               and t.day          = pos.day(+)
               and t.total_seq_no = hq.total_seq_no(+)
               and t.store        = hq.store(+)
               and t.day          = hq.day(+)
               and t.total_seq_no = st.total_seq_no(+)
               and t.store        = st.store(+)
               and t.day          = st.day(+)
               and t.total_seq_no = sys.total_seq_no(+)
               and t.store        = sys.store(+)
               and t.day          = sys.day(+)
               and t.total_seq_no = posw.total_seq_no(+)
               and t.store        = posw.store(+)
               and t.day          = posw.day(+)
               and t.total_seq_no = sysw.total_seq_no(+)
               and t.store        = sysw.store(+)
               and t.day          = sysw.day(+);
         end if;
      ELSE
         if l_total_status = 'A' then
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'SA_SYS_VALUE',
                             'Total Seq. No.: '||to_char(ln_total_seq_no));
            INSERT INTO sa_sys_value (total_seq_no,
                                      value_rev_no,
                                      total_rev_no,
                                      sys_value,
                                      update_id,
                                      update_datetime,
                                      store,
                                      day)
            select ln_total_seq_no,
               (GREATEST(nvl(max(hq.value_rev_no), 0),
                         nvl(max(pos.value_rev_no), 0),
                         nvl(max(st.value_rev_no), 0),
                         nvl(max(sys.value_rev_no), 0),
                         nvl(max(posw.value_rev_no), 0),
                         nvl(max(sysw.value_rev_no), 0)) + 1),
                         I_total_rev_no,
                         I_total_amount,
                         USER,
                         SYSDATE,
                         L_store,
                         L_day
              from sa_total t,sa_hq_value hq, sa_pos_value pos,
                   sa_store_value st, sa_sys_value sys,
                   sa_pos_value_wksht posw, sa_sys_value_wksht sysw
             where t.total_seq_no = ln_total_seq_no
               and t.store        = L_store
               and t.day          = L_day
               and t.total_seq_no = pos.total_seq_no(+)
               and t.store        = pos.store(+)
               and t.day          = pos.day(+)
               and t.total_seq_no = hq.total_seq_no(+)
               and t.store        = hq.store(+)
               and t.day          = hq.day(+)
               and t.total_seq_no = st.total_seq_no(+)
               and t.store        = st.store(+)
               and t.day          = st.day(+)
               and t.total_seq_no = sys.total_seq_no(+)
               and t.store        = sys.store(+)
               and t.day          = sys.day(+)
               and t.total_seq_no = posw.total_seq_no(+)
               and t.store        = posw.store(+)
               and t.day          = posw.day(+)
               and t.total_seq_no = sysw.total_seq_no(+)
               and t.store        = sysw.store(+)
               and t.day          = sysw.day(+);

            if l_precedence >= 3 then --- Need to insert SYS total into sa_exported_rev
               SQL_LIB.SET_MARK('INSERT',
                                NULL,
                                'SA_EXPORTED_REV',
                                'Total Seq. No.: '||to_char(ln_total_seq_no));
               INSERT INTO sa_exported_rev (export_seq_no,
                                            rev_no,
                                            store_day_seq_no,
                                            store,
                                            day,
                                            tran_seq_no,
                                            total_seq_no,
                                            acct_period,
                                            system_code,
                                            exp_datetime,
                                            status)
                   select  export_seq_no,
                           l_value_rev_no,
                           store_day_seq_no,
                           store,
                           day,
                           tran_seq_no,
                           total_seq_no,
                           acct_period,
                           system_code,
                           exp_datetime,
                           status
                     from  sa_exported
                    where  total_seq_no = ln_total_seq_no
                      and  store = L_store
                      and  day = L_day;
               ---
               SQL_LIB.SET_MARK('DELETE',
                                NULL,
                                'SA_EXPORTED',
                                'Total Seq. No.: '||to_char(ln_total_seq_no));
               DELETE from sa_exported
                     where total_seq_no = ln_total_seq_no
                       and store = L_store
                       and day = L_day;
            end if;
         ELSE
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'SA_SYS_VALUE_WKSHT',
                             'Total Seq. No.: '||to_char(ln_total_seq_no));
            INSERT INTO sa_sys_value_wksht (total_seq_no,
                                            value_rev_no,
                                            total_rev_no,
                                            sys_value,
                                            update_id,
                                            update_datetime,
                                            store,
                                            day)
            select ln_total_seq_no,
               (GREATEST(nvl(max(hq.value_rev_no), 0),
                         nvl(max(pos.value_rev_no), 0),
                         nvl(max(st.value_rev_no), 0),
                         nvl(max(sys.value_rev_no), 0),
                         nvl(max(posw.value_rev_no), 0),
                         nvl(max(sysw.value_rev_no), 0)) + 1),
                         I_total_rev_no,
                         I_total_amount,
                         USER,
                         SYSDATE,
                         L_store,
                         L_day
              from sa_total t,sa_hq_value hq, sa_pos_value pos,
                   sa_store_value st, sa_sys_value sys,
                   sa_pos_value_wksht posw, sa_sys_value_wksht sysw
             where t.total_seq_no = ln_total_seq_no
               and t.store        = L_store
               and t.day          = L_day
               and t.total_seq_no = pos.total_seq_no(+)
               and t.store        = pos.store(+)
               and t.day          = pos.day(+)
               and t.total_seq_no = hq.total_seq_no(+)
               and t.store        = hq.store(+)
               and t.day          = hq.day(+)
               and t.total_seq_no = st.total_seq_no(+)
               and t.store        = st.store(+)
               and t.day          = st.day(+)
               and t.total_seq_no = sys.total_seq_no(+)
               and t.store        = sys.store(+)
               and t.day          = sys.day(+)
               and t.total_seq_no = posw.total_seq_no(+)
               and t.store        = posw.store(+)
               and t.day          = posw.day(+)
               and t.total_seq_no = sysw.total_seq_no(+)
               and t.store        = sysw.store(+)
               and t.day          = sysw.day(+);
         end if;
      end if;

      INSERT INTO sa_export_log (store,
                     day,
                     store_day_seq_no,
                     system_code,
                     seq_no,
                     status)
      SELECT el.store,
             el.day,
             el.store_day_seq_no,
             el.system_code,
             MAX(el.seq_no) + 1,
             'R'
        FROM sa_export_log el, sa_total_usage su
       WHERE el.store_day_seq_no = I_store_day_seq_no
         AND el.store = L_store
         AND el.day = L_day
         AND el.system_code = su.usage_type
         AND su.total_id=I_total_id
         and su.total_rev_no=I_total_rev_no
         AND NOT EXISTS (
                         SELECT 1
                           FROM sa_export_log sel
                          WHERE sel.store_day_seq_no = el.store_day_seq_no
                            AND sel.store = el.store
                            AND sel.day = el.day
                            AND sel.system_code = el.system_code
                            AND sel.status = 'R'
                          )
      GROUP BY el.store, el.day, el.store_day_seq_no, el.system_code, el.status;
   end if;

   RETURN(0);

EXCEPTION
    WHEN others then
        O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'SA_BUILD_TOTAL_SQL.INSERT_TOTAL',
                                              SQLCODE);
        RETURN(-1);
END INSERT_TOTAL;
-----------------------------------------------------------------------------------------------------
    FUNCTION BUILD_COMB_TOTAL_FUNCTION (O_error_message    IN OUT VARCHAR2,
                                        I_total_id         IN       SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                        I_total_rev_no     IN       SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                                        I_pos_ind          IN       SA_TOTAL_HEAD.POS_IND%TYPE,
                                        I_refresh_override IN       BOOLEAN)
       RETURN INT
   /****************************************************************\
   |                                                                |
   |   BUILD_COMB_TOTAL_FUNCTION                                    |
   |                                                                |
   |   This function will generate a combination total function as  |
   |   defined in the sa_total_head and corresponding tables.       |
   |                                                                |
   |    Arguments:          Description:                            |
   |    I_total_id          Total identifier to create.             |
   |    I_total_rev_no      Will determine which version to create. |
   |    I_pos_ind           if value passed is 'Y' the data will be |
   |                        inserted into the sa_pos_value table,   |
   |                        otherwise it will write to the          |
   |                        sa_sys_value table.                     |
   |    I_refresh_override  if value passed is 'Y' the existing     |
   |                        function (regardless of the date will   |
   |                        be deleted first.                       |
   |    O_error_message     Returned error message                  |
   |                                                                |
   |    Called from:        Sales Audit form: sa_total.fmb          |
   |                        Sales Audit nightly batch: satotals.pc  |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
    IS
        b_result            BOOLEAN := FALSE;
        cursor_handle       INTEGER;
        feedback            INTEGER;
        v_select            sa_total_common.v_code_array :=
                            sa_total_common.empty_v_out;
        v_total_ids         sa_total_common.v_code_array :=
                            sa_total_common.empty_v_out;
        v_total_ops         sa_total_common.v_code_array :=
                            sa_total_common.empty_v_out;
        v_name              VARCHAR2(30);
        v_sql               DBMS_SQL.VARCHAR2s;
        n_count             NUMBER;
        l_detail_total_id   sa_total_head.total_id%TYPE;
        l_total_id          sa_total_head.total_id%TYPE;
        s_bal_level         sa_total_head.bal_level%TYPE;
        s_group_seq_no_1    sa_total_head.group_seq_no1%TYPE;
        s_group_seq_no_2    sa_total_head.group_seq_no2%TYPE;
        s_group_seq_no_3    sa_total_head.group_seq_no3%TYPE;
        d_end_date          VARCHAR2(8);
        d_start_date        VARCHAR2(8);
        L_comb_operator     SA_COMB_TOTAL.TOTAL_OPERATOR%TYPE;
        v_status            all_objects.status%TYPE;
        L_comb_exists       VARCHAR2(1);
        l_base_total_id     sa_total_head.total_id%type;
        l_base_comb_total_id sa_total_head.total_id%type;
        L_continue          NUMBER := 1;

        /* Get all the dependant total_ids to this combination
            total */
        cursor c_get_total_ids is
            select  sa_comb_total.detail_total_id,
                    sa_comb_total.total_operator
            from    sa_comb_total
            where   sa_comb_total.total_id = I_total_id
            and     total_rev_no = I_total_rev_no
            ORDER BY sa_comb_total.detail_total_id;
        /* Get the characteristics of the current total_id */
        cursor c_get_total_def is
            select  bal_level,
                    to_char(end_business_date, 'YYYYMMDD'),
                    to_char(start_business_date, 'YYYYMMDD')
            from    sa_total_head
            where   total_id = I_total_id
            and     total_rev_no = I_total_rev_no
            ORDER BY total_id;

        /* Get the characteristics of the detail totals of combined total_id */
        cursor c_get_total_detail_def is
            select  group_seq_no1, group_seq_no2, group_seq_no3
            from    sa_total_head
            where   total_id = l_detail_total_id
            and     total_rev_no = (select max(total_rev_no)
                                    from sa_total_head
                                    where   total_id = l_detail_total_id)
            ORDER BY total_id;



        cursor c_check_compile is
            select status
              from all_objects
             where object_name = v_name
               and object_type = 'FUNCTION';

        cursor C_get_start_end_date is
           select to_char(start_business_date,'YYMMDD'),
                  to_char(end_business_date,'YYMMDD')
             from sa_total_head
            where total_id     = I_total_id
              and total_rev_no = I_total_rev_no;

        /* Check if the dependent totals fetched is a combined total in itself */
        cursor c_check_comb_total is
        select distinct 'x'
          from sa_comb_total
         where total_id = l_detail_total_id;

        /* Get atleast one base total for the total_id fetched which is not a comb total in itself */
         cursor c_get_base_total is
         select detail_total_id
           from sa_comb_total sct
          where sct.total_id = l_detail_total_id
            and exists (select 1
                          from sa_total_head sth
                         where sth.total_id = sct.detail_total_id
                           and sth.comb_total_ind = 'N')
            and total_rev_no = (select max(total_rev_no)
                                  from sa_comb_total sct1
                                 where sct1.total_id = sct.total_id)
            and rownum = 1;

        /* Get atleast one base total for the total_id fetched which is a comb total in itself */
         cursor c_get_base_comb_total is
         select detail_total_id
           from sa_comb_total sct
          where sct.total_id = l_detail_total_id
            and exists (select 1
                          from sa_total_head sth
                         where sth.total_id = sct.detail_total_id
                           and sth.comb_total_ind = 'Y')
            and total_rev_no = (select max(total_rev_no)
                                  from sa_comb_total sct1
                                 where sct1.total_id = sct.total_id)
            and rownum = 1;




    BEGIN
        if I_refresh_override then
            feedback := DROP_TOTAL_FUNCTION(O_error_message, I_total_id, NULL);
            if feedback <> 0 then
                RETURN(-1);
            end if;
        end if;
        /***************************************************************\
        |                                                               |
        |   Retrieve the start and end dates for the function.          |
        |   Also grab the total status.                                 |
        |                                                               |
        \***************************************************************/
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_START_END_DATE',
                         'SA_TOTAL_HEAD',
                         'Total ID: '||I_total_id);
        open  C_get_start_end_date;
        SQL_LIB.SET_MARK('FETCH',
                         'C_GET_START_END_DATE',
                         'SA_TOTAL_HEAD',
                         'Total ID: '||I_total_id);
        fetch C_get_start_end_date into d_start_date, d_end_date;
        SQL_LIB.SET_MARK('CLOSE',
                         'C_GET_START_END_DATE',
                         'SA_TOTAL_HEAD',
                         'Total ID: '||I_total_id);
        close C_get_start_end_date;
        ---
        if d_end_date is not NULL then
           v_name := 'SA_T_' || I_total_id || '_' || d_start_date || '_' || d_end_date;
        else
           v_name := 'SA_T_' || I_total_id || '_' || d_start_date;
        end if;
        v_select(v_select.count+1) := 'CREATE or REPLACE FUNCTION ' ||
            sa_total_common.internal_schema_name || '.' || v_name;
        v_select(v_select.count+1) := '   (';
        v_select(v_select.count+1) := '    I_store_day_seq_no IN   sa_store_day.store_day_seq_no%TYPE DEFAULT NULL,';
        v_select(v_select.count+1) := '    O_error_message   OUT   VARCHAR2';
        v_select(v_select.count+1) := '   )';
        v_select(v_select.count+1) := 'return INT';
        v_select(v_select.count+1) := 'IS';
        v_select(v_select.count+1) := '   l_total_id      sa_total.total_id%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_total_rev_no  sa_sys_value.total_rev_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_sum           sa_sys_value.sys_value%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_ref_no1       sa_total.ref_no1%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_ref_no2       sa_total.ref_no1%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_ref_no3       sa_total.ref_no1%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_store_day_seq_no sa_total.store_day_seq_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_bal_group_seq_no sa_total.bal_group_seq_no%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_total_seq_no  sa_total.total_seq_no%TYPE;';
        v_select(v_select.count+1) := '   L_sys_value     SA_SYS_VALUE.SYS_VALUE%TYPE;';
        v_select(v_select.count+1) := '   L_store_value   SA_STORE_VALUE.STORE_VALUE%TYPE;';
        v_select(v_select.count+1) := '   L_pos_value     SA_POS_VALUE.POS_VALUE%TYPE;';
        v_select(v_select.count+1) := '   L_hq_value      SA_HQ_VALUE.HQ_VALUE%TYPE;';
        v_select(v_select.count+1) := '   L_total_value   SA_SYS_VALUE.SYS_VALUE%TYPE;';
        v_select(v_select.count+1) := '   l_tran_date     sa_store_day.business_date%TYPE;';
        v_select(v_select.count+1) := '   l_store         sa_store_day.store%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_day           sa_store_day.day%TYPE := NULL;';
        v_select(v_select.count+1) := '   l_temp_value    sa_sys_value.sys_value%TYPE;';
        v_select(v_select.count+1) := '   i_result        INT := 0;';
        v_select(v_select.count+1) := '   b_result        BOOLEAN := FALSE;';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := 'cursor c_get_index is';
        ---
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_TOTAL_IDS',
                         'SA_COMB_TOTAL',
                         'Total ID: '||I_total_id);
        open c_get_total_ids;
        loop
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_TOTAL_IDS',
                             'SA_COMB_TOTAL',
                             'Total ID: '||I_total_id);
            fetch c_get_total_ids INTO  l_total_id,
                                        L_comb_operator;
            EXIT WHEN c_get_total_ids%NOTFOUND;
            BEGIN
                v_total_ids(v_total_ids.count+1) := l_total_id;
                v_total_ops(v_total_ops.count+1) := L_comb_operator;
            END;
        end loop;
        SQL_LIB.SET_MARK('CLOSE',
                         'C_GET_TOTAL_IDS',
                         'SA_COMB_TOTAL',
                         'Total ID: '||I_total_id);
        close c_get_total_ids;
        /* Create a query for each dependant total and UNION
            them together to get all data combinations of the
            selected totals.
            Assumption: All dependant totals must have the same
            roll-ups as each other. */
        for n_count IN 1..v_total_ids.count loop
            v_select(v_select.count+1) := '   select store_day_seq_no,';
            v_select(v_select.count+1) := '          bal_group_seq_no,';
            v_select(v_select.count+1) := '          ref_no1,';
            v_select(v_select.count+1) := '          ref_no2,';
            v_select(v_select.count+1) := '          ref_no3';
            v_select(v_select.count+1) := '   from   sa_total';
            v_select(v_select.count+1) := '   where  total_id = ' || chr(39) || v_total_ids(n_count) || chr(39);
            v_select(v_select.count+1) := '   and    store_day_seq_no = I_store_day_seq_no';
            v_select(v_select.count+1) := '   and    store = l_store';
            v_select(v_select.count+1) := '   and    day = l_day';
            v_select(v_select.count+1) := '        ';
            v_select(v_select.count+1) := '   UNION';
        end loop;
        v_select(v_select.count-2) := v_select(v_select.count-2) || ';';
        v_select(v_select.count) := '                  ';
        ---
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_TOTAL_DEF',
                         'SA_TOTAL_HEAD',
                         'Total ID: '||I_total_id);
        open c_get_total_def;
        SQL_LIB.SET_MARK('FETCH',
                         'C_GET_TOTAL_DEF',
                         'SA_TOTAL_HEAD',
                         'Total ID: '||I_total_id);
        fetch c_get_total_def INTO  s_bal_level,
                                    d_end_date,
                                    d_start_date;
        SQL_LIB.SET_MARK('CLOSE',
                         'C_GET_TOTAL_DEF',
                         'SA_TOTAL_HEAD',
                         'Total ID: '||I_total_id);
        close c_get_total_def;
        /* Now for each data combination query each dependant total
            and add the results together to make the combination
            total */
        for n_count IN 1..v_total_ids.count loop
            l_detail_total_id := v_total_ids(n_count);
            /* Check if this TOTAL_ID is a COMB TOTAL of some other TOTAL_ID's */
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_COMB_TOTAL',
                             'SA_COMB_TOTAL',
                             'Total ID: '||l_detail_total_id);
            open c_check_comb_total;
            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_COMB_TOTAL',
                             'SA_COMB_TOTAL',
                             'Total ID: '||l_detail_total_id);
            fetch c_check_comb_total into L_comb_exists;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_COMB_TOTAL',
                             'SA_COMB_TOTAL',
                             'Total ID: '||l_detail_total_id);
            close c_check_comb_total;
            /* If the combination exists, then we need to get the group_seq_no's for its base total_id.
               If the base total_id fetched is a combination total_id in itself, then drill down to the
               lowest level and get the base total_id */
            if L_comb_exists is not null then
               L_continue := 1;
               /* Check if the total_id l_detail_total_id has atleast one of its base total which is not
                  a comb total in itself */
               while L_continue = 1
               loop
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_BASE_TOTAL',
                                   'SA_COMB_TOTAL',
                                   'Total ID: '||l_detail_total_id);
                  open c_get_base_total;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_BASE_TOTAL',
                                   'SA_COMB_TOTAL',
                                   'Total ID: '||l_detail_total_id);
                  fetch c_get_base_total into l_base_total_id;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_BASE_TOTAL',
                                   'SA_COMB_TOTAL',
                                   'Total ID: '||l_detail_total_id);
                  close c_get_base_total;
                  /* If l_base_total_id exists, then we can stop the loop and exit. If not, continue till the base
                     total_id is fetched */
                  if l_base_total_id is not null then
                     l_detail_total_id := l_base_total_id;
                     L_continue := 0;
                  else
                     /* Get atleast one base total_id of l_detail_total_id which is also a comb total */
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_BASE_COMB_TOTAL',
                                      'SA_COMB_TOTAL',
                                      'Total ID: '||l_detail_total_id);
                     open c_get_base_comb_total;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_BASE_COMB_TOTAL',
                                      'SA_COMB_TOTAL',
                                      'Total ID: '||l_detail_total_id);
                     fetch c_get_base_comb_total into l_base_comb_total_id;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_BASE_COMB_TOTAL',
                                      'SA_COMB_TOTAL',
                                      'Total ID: '||l_detail_total_id);
                     close c_get_base_comb_total;
                     l_detail_total_id := l_base_comb_total_id;
                  end if;
               end loop;
            end if;

            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_TOTAL_DETAIL_DEF',
                             'SA_TOTAL_HEAD',
                             'Total ID: '||l_detail_total_id);
            open c_get_total_detail_def;
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_TOTAL_DETAIL_DEF',
                             'SA_TOTAL_HEAD',
                             'Total ID: '||l_detail_total_id);
            fetch c_get_total_detail_def INTO  s_group_seq_no_1,
                                        s_group_seq_no_2,
                                        s_group_seq_no_3;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_TOTAL_DETAIL_DEF',
                             'SA_TOTAL_HEAD',
                             'Total ID: '||l_detail_total_id);
            close c_get_total_detail_def;
            v_select(v_select.count+1) := 'cursor c_get_total_' || v_total_ids(n_count) || ' is';
            v_select(v_select.count+1) := '   select sa_pos_value.pos_value,';
            v_select(v_select.count+1) := '          sa_sys_value.sys_value,';
            v_select(v_select.count+1) := '          sa_store_value.store_value,';
            v_select(v_select.count+1) := '          sa_hq_value.hq_value';
            v_select(v_select.count+1) := '     from sa_total,';
            v_select(v_select.count+1) := '          sa_pos_value,';
            v_select(v_select.count+1) := '          sa_sys_value,';
            v_select(v_select.count+1) := '          sa_store_value,';
            v_select(v_select.count+1) := '          sa_hq_value';
            v_select(v_select.count+1) := '    where sa_total.total_seq_no = sa_pos_value.total_seq_no(+)';
            v_select(v_select.count+1) := '      and sa_total.store = sa_pos_value.store(+)';
            v_select(v_select.count+1) := '      and sa_total.day = sa_pos_value.day(+)';
            v_select(v_select.count+1) := '      and sa_total.total_seq_no = sa_sys_value.total_seq_no(+)';
            v_select(v_select.count+1) := '      and sa_total.store = sa_sys_value.store(+)';
            v_select(v_select.count+1) := '      and sa_total.day = sa_sys_value.day(+)';
            v_select(v_select.count+1) := '      and sa_total.total_seq_no = sa_store_value.total_seq_no(+)';
            v_select(v_select.count+1) := '      and sa_total.store = sa_store_value.store(+)';
            v_select(v_select.count+1) := '      and sa_total.day = sa_store_value.day(+)';
            v_select(v_select.count+1) := '      and sa_total.total_seq_no = sa_hq_value.total_seq_no(+)';
            v_select(v_select.count+1) := '      and sa_total.store = sa_hq_value.store(+)';
            v_select(v_select.count+1) := '      and sa_total.day = sa_hq_value.day(+)';
            v_select(v_select.count+1) := '      and sa_total.store_day_seq_no = I_store_day_seq_no';
            v_select(v_select.count+1) := '      and sa_total.store = l_store';
            v_select(v_select.count+1) := '      and sa_total.day = l_day';
            if s_bal_level != 'S' then
                v_select(v_select.count+1) := '      and sa_total.bal_group_seq_no = l_bal_group_seq_no'; ELSE
                v_select(v_select.count+1) := '      and sa_total.bal_group_seq_no is NULL';
            end if;
            if s_group_seq_no_1 is NOT NULL then
                v_select(v_select.count+1) := '      and sa_total.ref_no1 = l_ref_no1';
            ELSE
                v_select(v_select.count+1) := '      and sa_total.ref_no1 is NULL';
            end if;
            if s_group_seq_no_2 is NOT NULL then
                v_select(v_select.count+1) := '      and sa_total.ref_no2 = l_ref_no2';
            ELSE
                v_select(v_select.count+1) := '      and sa_total.ref_no2 is NULL';
            end if;
            if s_group_seq_no_3 is NOT NULL then
                v_select(v_select.count+1) := '      and sa_total.ref_no3 = l_ref_no3';
            ELSE
                v_select(v_select.count+1) := '      and sa_total.ref_no3 is NULL';
            end if;
            v_select(v_select.count+1) := '  and sa_total.total_id = '
                || chr(39) || v_total_ids(n_count) || chr(39);
            v_select(v_select.count+1) := '      and (not exists';
            v_select(v_select.count+1) := '             (select sv1.value_rev_no';
            v_select(v_select.count+1) := '                from sa_sys_value sv1';
            v_select(v_select.count+1) := '               where sv1.total_seq_no = sa_total.total_seq_no';
            v_select(v_select.count+1) := '               and   sv1.store = sa_total.store';
            v_select(v_select.count+1) := '               and   sv1.day = sa_total.day)';
            v_select(v_select.count+1) := '                  or sa_sys_value.value_rev_no =';
            v_select(v_select.count+1) := '             (select max(sv1.value_rev_no)';
            v_select(v_select.count+1) := '                from sa_sys_value sv1';
            v_select(v_select.count+1) := '               where sv1.total_seq_no = sa_total.total_seq_no';
            v_select(v_select.count+1) := '               and   sv1.store = sa_total.store';
            v_select(v_select.count+1) := '               and   sv1.day = sa_total.day))';
            v_select(v_select.count+1) := '      and (not exists';
            v_select(v_select.count+1) := '             (select hq1.value_rev_no';
            v_select(v_select.count+1) := '                from sa_hq_value hq1';
            v_select(v_select.count+1) := '               where hq1.total_seq_no = sa_total.total_seq_no';
            v_select(v_select.count+1) := '               and   hq1.store = sa_total.store';
            v_select(v_select.count+1) := '               and   hq1.day = sa_total.day)';
            v_select(v_select.count+1) := '                  or sa_hq_value.value_rev_no =';
            v_select(v_select.count+1) := '             (select max(hq1.value_rev_no)';
            v_select(v_select.count+1) := '                from sa_hq_value hq1';
            v_select(v_select.count+1) := '               where hq1.total_seq_no = sa_total.total_seq_no';
            v_select(v_select.count+1) := '               and   hq1.store = sa_total.store';
            v_select(v_select.count+1) := '               and   hq1.day = sa_total.day))';
            v_select(v_select.count+1) := '      and (not exists';
            v_select(v_select.count+1) := '             (select stv1.value_rev_no';
            v_select(v_select.count+1) := '                from sa_store_value stv1';
            v_select(v_select.count+1) := '               where stv1.total_seq_no = sa_total.total_seq_no';
            v_select(v_select.count+1) := '               and   stv1.store = sa_total.store';
            v_select(v_select.count+1) := '               and   stv1.day = sa_total.day)';
            v_select(v_select.count+1) := '                  or sa_store_value.value_rev_no =';
            v_select(v_select.count+1) := '             (select max(stv1.value_rev_no)';
            v_select(v_select.count+1) := '                from sa_store_value stv1';
            v_select(v_select.count+1) := '               where stv1.total_seq_no = sa_total.total_seq_no';
            v_select(v_select.count+1) := '               and   stv1.store = sa_total.store';
            v_select(v_select.count+1) := '               and   stv1.day = sa_total.day))';
            v_select(v_select.count+1) := '      and (not exists';
            v_select(v_select.count+1) := '             (select pv1.value_rev_no';
            v_select(v_select.count+1) := '                from sa_pos_value pv1';
            v_select(v_select.count+1) := '               where pv1.total_seq_no = sa_total.total_seq_no';
            v_select(v_select.count+1) := '               and   pv1.store = sa_total.store';
            v_select(v_select.count+1) := '               and   pv1.day = sa_total.day)';
            v_select(v_select.count+1) := '                  or sa_pos_value.value_rev_no =';
            v_select(v_select.count+1) := '             (select max(pv1.value_rev_no)';
            v_select(v_select.count+1) := '                from sa_pos_value pv1';
            v_select(v_select.count+1) := '               where pv1.total_seq_no = sa_total.total_seq_no';
            v_select(v_select.count+1) := '               and   pv1.store = sa_total.store';
            v_select(v_select.count+1) := '               and   pv1.day = sa_total.day));';
            v_select(v_select.count+1) := '                  ';
        end loop;
        v_select(v_select.count+1) := 'BEGIN';
        /* include the date constraints for starting and ending the total */

        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '  /* Check the date contraints */';
        v_select(v_select.count+1) := '        ';

        v_select(v_select.count+1) := '   if not sa_total_common.get_tran_datetime(I_store_day_seq_no, l_tran_date, l_store, l_day, O_error_message) then';
        v_select(v_select.count+1) := '      O_error_message := sql_lib.create_msg(' || CHR(39) || 'SATOTRUL_NOSTRDAY' || CHR(39) || ',';
        v_select(v_select.count+1) := '                                            I_store_day_seq_no, NULL, NULL);';
        v_select(v_select.count+1) := '      return(-1);';
        v_select(v_select.count+1) := '   end if;';
        v_select(v_select.count+1) := '        ';

        if d_end_date is NOT NULL then
            v_select(v_select.count+1) := '  if (l_tran_date >= TO_DATE(' || CHR(39) ||
                d_start_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD'  || CHR(39) || ')) and (l_tran_date <= TO_DATE(' || CHR(39) ||
                d_end_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD' || CHR(39) || ')) then';
        else
            v_select(v_select.count+1) := '  if l_tran_date >= TO_DATE(' || CHR(39) ||
                d_start_date || CHR(39) || ', ' || CHR(39) || 'YYYYMMDD' || CHR(39) || ') then';
        end if;

        v_select(v_select.count+1) := '                  ';
        v_select(v_select.count+1) := '   open c_get_index;';
        v_select(v_select.count+1) := '   loop';
        v_select(v_select.count+1) := '   fetch c_get_index INTO l_store_day_seq_no,';
        v_select(v_select.count+1) := '                          l_bal_group_seq_no,';
        v_select(v_select.count+1) := '                          l_ref_no1,';
        v_select(v_select.count+1) := '                          l_ref_no2,';
        v_select(v_select.count+1) := '                          l_ref_no3;';
        v_select(v_select.count+1) := '                  ';
        v_select(v_select.count+1) := '   EXIT WHEN c_get_index%NOTFOUND;';
        v_select(v_select.count+1) := '   BEGIN';
        v_select(v_select.count+1) := '                  ';
        v_select(v_select.count+1) := '      L_total_value := 0;';
        v_select(v_select.count+1) := '      l_temp_value := 0;';
        v_select(v_select.count+1) := '      L_sys_value := 0;';
        v_select(v_select.count+1) := '      L_pos_value := 0;';
        v_select(v_select.count+1) := '      L_store_value := 0;';
        v_select(v_select.count+1) := '      L_hq_value := 0;';
        v_select(v_select.count+1) := '                  ';
        for n_count IN 1..v_total_ids.count loop
            v_select(v_select.count+1) := '      open c_get_total_' || v_total_ids(n_count) || ';';
            v_select(v_select.count+1) := '      fetch c_get_total_' ||
                v_total_ids(n_count) || ' INTO L_pos_value,';
            v_select(v_select.count+1) := '                                 L_sys_value,';
            v_select(v_select.count+1) := '                                 L_store_value,';
            v_select(v_select.count+1) := '                                 L_hq_value;';
            v_select(v_select.count+1) := '                  ';
            /* determine the heiarchy for the returned total values */
            v_select(v_select.count+1) := '      b_result := SA_BUILD_TOTAL_SQL.GET_HEIRARCHY_TOTAL (';
            v_select(v_select.count+1) := '                                  O_error_message,';
            v_select(v_select.count+1) := '                                  l_temp_value,';
            v_select(v_select.count+1) := '                                  L_hq_value,';
            v_select(v_select.count+1) := '                                  L_store_value,';
            v_select(v_select.count+1) := '                                  L_sys_value,';
            v_select(v_select.count+1) := '                                  L_pos_value);';
            v_select(v_select.count+1) := '                  ';
            v_select(v_select.count+1) := '      L_total_value := (L_total_value ' ||
                v_total_ops(n_count) || ' l_temp_value);';
            v_select(v_select.count+1) := '      close c_get_total_' || v_total_ids(n_count) || ';'; v_select(v_select.count+1) := '         ';
            v_select(v_select.count+1) := '      l_temp_value := 0;';
            v_select(v_select.count+1) := '      L_sys_value := 0;';
            v_select(v_select.count+1) := '      L_pos_value := 0;';
            v_select(v_select.count+1) := '      L_store_value := 0;';
            v_select(v_select.count+1) := '      L_hq_value := 0;';
            v_select(v_select.count+1) := '         ';
            v_select(v_select.count+1) := '      if L_total_value is NULL then';
            v_select(v_select.count+1) := '         L_total_value := 0;';
            v_select(v_select.count+1) := '      end if;';
            v_select(v_select.count+1) := '         ';
        end loop;
        v_select(v_select.count+1) := '      i_result :=  SA_BUILD_TOTAL_SQL.INSERT_TOTAL ('
            || 'O_error_message,';
        v_select(v_select.count+1) := '                                                '
            || chr(39) || I_total_id || chr(39) || ',';
        v_select(v_select.count+1) := '                                                l_store_day_seq_no,';
        v_select(v_select.count+1) := '                                                l_bal_group_seq_no,';
        v_select(v_select.count+1) := '                                                l_ref_no1,';
        v_select(v_select.count+1) := '                                                l_ref_no2,';
        v_select(v_select.count+1) := '                                                l_ref_no3,';
        v_select(v_select.count+1) := '                                                L_total_value,';
        v_select(v_select.count+1) := '                                                '
            || I_total_rev_no || ',';
        v_select(v_select.count+1) := '                                                '
            || chr(39) || 'N' || chr(39) || ',';
        v_select(v_select.count+1) := '                                                l_store,';
        v_select(v_select.count+1) := '                                                l_day);';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '        if i_result <> 0 then';
        v_select(v_select.count+1) := '            RETURN(-1);';
        v_select(v_select.count+1) := '        end if;';
        v_select(v_select.count+1) := '        ';
        v_select(v_select.count+1) := '   END;';
        v_select(v_select.count+1) := '   end loop;';
        v_select(v_select.count+1) := '   close c_get_index;';
        v_select(v_select.count+1) := '                  ';
        v_select(v_select.count+1) := '  end if;';
        v_select(v_select.count+1) := '                  ';
        v_select(v_select.count+1) := '   RETURN(0);';
        v_select(v_select.count+1) := '                  ';
        v_select(v_select.count+1) := '    EXCEPTION';
        v_select(v_select.count+1) := '        WHEN OTHERS then';
        v_select(v_select.count+1) := '            O_error_message := sql_lib.create_msg(';
        v_select(v_select.count+1) := '                ' || chr(39) || 'PACKAGE_ERROR' || chr(39) || ',';
        v_select(v_select.count+1) := '                SQLERRM,';
        v_select(v_select.count+1) := '                ' || chr(39) || v_name || chr(39) || ',';
        v_select(v_select.count+1) := '                SQLCODE);';
        v_select(v_select.count+1) := '            RETURN(-1);';
        v_select(v_select.count+1) := 'END;';
        for n_count IN 1..v_select.count
        loop
            v_sql(v_sql.count+1) := v_select(n_count);
        end loop;
        cursor_handle := DBMS_SQL.OPEN_CURSOR;
        DBMS_SQL.PARSE (cursor_handle, v_sql, v_sql.FIRST,
                        v_sql.LAST, TRUE, DBMS_SQL.NATIVE);
        feedback := DBMS_SQL.EXECUTE(cursor_handle);
        DBMS_SQL.CLOSE_CURSOR(cursor_handle);
        /***************************************************************\
        |                                                               |
        |   This will determine if the code compiled successfully       |
        |                                                               |
        \***************************************************************/
        SQL_LIB.SET_MARK('OPEN',
                         'C_CHECK_COMPILE',
                         'ALL_OBJECTS',
                         'Function: '||v_name);
        open c_check_compile;
        SQL_LIB.SET_MARK('FETCH',
                         'C_CHECK_COMPILE',
                         'ALL_OBJECTS',
                         'Function: '||v_name);
        fetch c_check_compile INTO v_status;
        SQL_LIB.SET_MARK('CLOSE',
                         'C_CHECK_COMPILE',
                         'ALL_OBJECTS',
                         'Function: '||v_name);
        close c_check_compile;
        if v_status = 'INVALID' then
            /* Compile did not work */
            O_error_message := sql_lib.create_msg('SABLDTL_COMPILE',
                                                  I_total_id,
                                                  NULL,
                                                  NULL);
            RETURN(-1);
        end if;

        RETURN(0);
    EXCEPTION
        WHEN others then
            /*Close the cursor c_get_total_ids if it is open*/
            if c_get_total_ids%ISOPEN then
              close c_get_total_ids;
            end if;
            O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  'SA_BUILD_TOTAL_SQL.BUILD_COMB_TOTAL_FUNCTION',
                                                  SQLCODE);
            RETURN(-1);
    END BUILD_COMB_TOTAL_FUNCTION;
---------------------------------------------------------------------------------------------------
   FUNCTION REBUILD_TOTAL (O_error_message  IN OUT  VARCHAR2,
                           I_total_id       IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                           I_total_rev_no   IN      SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                           I_new_vr_id      IN      SA_VR_HEAD.VR_ID%TYPE,
                           I_new_vr_rev_no  IN      SA_VR_HEAD.VR_REV_NO%TYPE)
      RETURN BOOLEAN is
      ---
      L_program              VARCHAR2(60) := 'SA_BUILD_TOTAL_SQL.REBUILD_TOTAL';
      L_new_total_rev_no     SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE;
      L_total_status         SA_TOTAL_HEAD.STATUS%TYPE;
      L_valid_multi_rev_ind  VARCHAR2(1);
      L_start_date           SA_TOTAL_HEAD.START_BUSINESS_DATE%TYPE;
      L_end_date             SA_TOTAL_HEAD.END_BUSINESS_DATE%TYPE;
      L_comb_total_ind       SA_TOTAL_HEAD.COMB_TOTAL_IND%TYPE;
      L_wiz_ind              SA_TOTAL_HEAD.WIZ_IND%TYPE;
      L_sys_calc_ind         SA_TOTAL_HEAD.SYS_CALC_IND%TYPE;
      ---
      cursor C_TOTAL_INFO is
         select start_business_date,
                end_business_date,
                comb_total_ind,
                wiz_ind,
                sys_calc_ind
           from sa_total_head
          where total_id     = I_total_id
            and total_rev_no = L_new_total_rev_no;
      ---
   BEGIN
      if SA_BUILD_TOTAL_SQL.CREATE_REVISION(O_error_message,
                                            L_new_total_rev_no,
                                            L_total_status,
                                            I_total_id,
                                            I_total_rev_no) = FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_TOTAL_INFO',
                       'SA_TOTAL_HEAD',
                       'Total ID: '||I_total_id);
      open  C_TOTAL_INFO;
      SQL_LIB.SET_MARK('FETCH',
                       'C_TOTAL_INFO',
                       'SA_TOTAL_HEAD',
                       'Total ID: '||I_total_id);
      fetch C_TOTAL_INFO into L_start_date,
                              L_end_date,
                              L_comb_total_ind,
                              L_wiz_ind,
                              L_sys_calc_ind;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_TOTAL_INFO',
                       'SA_TOTAL_HEAD',
                       'Total ID: '||I_total_id);
      close C_TOTAL_INFO;
      ---
      if SA_TOTAL_DEF_SQL.DROP_INVALID_MULTI_VERSIONS(O_error_message,
                                                      I_total_id,
                                                      L_new_total_rev_no,
                                                      L_start_date,
                                                      L_end_date) = FALSE then
         return FALSE;
      end if;
      ---
      if L_wiz_ind = 'Y' and L_sys_calc_ind = 'Y' then
         if L_comb_total_ind = 'N' then
            --- Build a regular total
            if SA_BUILD_TOTAL_SQL.BUILD_TOTAL_FUNCTION(O_error_message,
                                                           I_total_id,
                                                           L_new_total_rev_no,
                                                           'N', --- Always 'N'
                                                           TRUE) != 0 then
               ---Function was not built successfully
                 if SA_BUILD_TOTAL_SQL.DROP_TOTAL_FUNCTION(O_error_message,
                                                               I_total_id,
                                                               L_new_total_rev_no) != 0 then
                  return FALSE;
               end if;
               return FALSE;
            end if;
            ---
            if SA_TOTAL_DEF_SQL.CREATE_WIZ_TOTAL_DATA (O_error_message,
                                                           I_total_id,
                                                           L_new_total_rev_no) = FALSE then
               return FALSE;
            end if;
         else
            --- Build a combination total
            if SA_BUILD_TOTAL_SQL.BUILD_COMB_TOTAL_FUNCTION(O_error_message,
                                                              I_total_id,
                                                              L_new_total_rev_no,
                                                              'N',
                                                                TRUE) != 0 then
                 --- Build function failed
                 if SA_BUILD_TOTAL_SQL.DROP_TOTAL_FUNCTION(O_error_message,
                                                           I_total_id,
                                                           L_new_total_rev_no) != 0 then
                    return FALSE;
               end if;
                 return FALSE;
              else
                 if SA_TOTAL_DEF_SQL.CREATE_COMB_TOTAL_DATA (O_error_message,
                                                               I_total_id,
                                                               L_new_total_rev_no) = FALSE then
                  return FALSE;
                 end if;
              end if;
         end if;
      end if;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               SQLCODE);
         return FALSE;
   END REBUILD_TOTAL;
-------------------------------------------------------------------------------
FUNCTION INSERT_ZERO_TOTALS
         (O_error_message            IN OUT  VARCHAR2,
          I_store_day_seq_no         IN      SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
          I_total_id                 IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
          I_process_calc_datetime    IN      SA_SYS_VALUE.UPDATE_DATETIME%TYPE,
          I_store                    IN      SA_STORE_DAY.STORE%TYPE,
          I_day                      IN      SA_STORE_DAY.DAY%TYPE)
   return BOOLEAN is

   /****************************************************************\
   |                                                                |
   |   INSERT_ZERO_TOTALS                                           |
   |                                                                |
   |   This internal function will go through all the system        |
   |   calculated totals that previously existed for the specified  |
   |   store/day, but did not have a value re-calculated during     |
   |   this instance of processing calculated totals. Then it will  |
   |   call INSERT_TOTAL to insert a value of zero.                 |
   |   value of 0 for the total.                                    |
   |                                                                |
   |   Called from: PROCESS_CALC_TOTALS                             |
   \****************************************************************/

   L_total_rev_no       SA_SYS_VALUE.TOTAL_REV_NO%TYPE;
   L_previous_total_id  SA_TOTAL.TOTAL_ID%TYPE         := '-1';
   L_store              SA_STORE_DAY.STORE%TYPE        := I_store;
   L_day                SA_STORE_DAY.DAY%TYPE          := I_day;

   cursor C_GET_ZERO_TOTALS is
      select t.total_id, t.bal_group_seq_no, t.ref_no1, t.ref_no2, t.ref_no3
        from sa_total t, sa_sys_value sys1
       where t.store_day_seq_no   = I_store_day_seq_no
         and t.total_id           = nvl(I_total_id, t.total_id)
         and t.store              = L_store
         and t.day                = L_day
         and t.total_seq_no       = sys1.total_seq_no
         and t.store              = sys1.store
         and t.day                = sys1.day
         and sys1.update_datetime < I_process_calc_datetime
         and NOT exists ( select 'x'
                            from sa_sys_value sys2
                           where sys2.total_seq_no = sys1.total_seq_no
                             and sys2.store        = sys1.store
                             and sys2.day          = sys1.day
                             and sys2.update_datetime > I_process_calc_datetime )
         and sys1.value_rev_no = (select max(sys3.value_rev_no)
                                    from sa_sys_value sys3
                                   where sys3.total_seq_no = sys1.total_seq_no
                                     and sys3.store        = sys1.store
                                     and sys3.day          = sys1.day)
         and sys1.sys_value != 0;

   cursor C_GET_TOTAL_REV_NO(C_total_id  SA_TOTAL.TOTAL_ID%TYPE) is
      select max(sys.total_rev_no)
        from sa_total t, sa_sys_value sys
       where t.total_id         = C_total_id
         and t.store_day_seq_no = I_store_day_seq_no
         and t.store            = L_store
         and t.day              = L_day
         and t.total_seq_no     = sys.total_seq_no;

BEGIN

    if L_store is NULL or L_day is NULL then
       if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                         L_store,
                                         L_day,
                                         I_store_day_seq_no,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL) = FALSE then
          return FALSE;
       end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ZERO_TOTALS',
                    'SA_TOTAL, SA_SYS_VALUE',
                    NULL);
   for L_rec in C_GET_ZERO_TOTALS LOOP
      /* Get the total revision number of the most recent version of the total. */
      if L_rec.total_id != L_previous_total_id then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_TOTAL_REV_NO',
                          'SA_TOTAL, SA_SYS_VALUE',
                          'TOTAL_ID: '||L_rec.total_id);
         open C_GET_TOTAL_REV_NO(L_rec.total_id);
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_TOTAL_REV_NO',
                          'SA_TOTAL, SA_SYS_VALUE',
                          'TOTAL_ID: '||L_rec.total_id);
         fetch C_GET_TOTAL_REV_NO into L_total_rev_no;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_TOTAL_REV_NO',
                          'SA_TOTAL, SA_SYS_VALUE',
                          'TOTAL_ID: '||L_rec.total_id);
         close C_GET_TOTAL_REV_NO;
         L_previous_total_id := L_rec.total_id;
      end if;
      ---
      /* insert zero as the total value */
      if SA_BUILD_TOTAL_SQL.INSERT_TOTAL(O_error_message,
                                         L_rec.total_id,
                                         I_store_day_seq_no,
                                         L_rec.bal_group_seq_no,
                                         L_rec.ref_no1,
                                         L_rec.ref_no2,
                                         L_rec.ref_no3,
                                         0,  /* total_amount */
                                         L_total_rev_no,
                                         'N', /* I_pos_ind */
                                         I_store,
                                         I_day) != 0 then
         return FALSE;
      end if;
   end LOOP;
   ---
   return TRUE;
   ---
EXCEPTION
   when others then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_BUILD_TOTAL_SQL.INSERT_ZERO_TOTALS',
                                            SQLCODE);
      return FALSE;
END INSERT_ZERO_TOTALS;
-------------------------------------------------------------------------------
    FUNCTION PROCESS_CALC_TOTALS
        (O_error_message        IN OUT VARCHAR2,
         I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
         I_pos_totals_ind       IN     VARCHAR2,
         I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
         I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
    RETURN BOOLEAN
   /****************************************************************\
   |                                                                |
   |   PROCESS_CALC_TOTALS                                          |
   |                                                                |
   |   This function will calculate all totals for a passed.        |
   |    store_day_seq_no.                                           |
   |                                                                |
   |    Arguments:          Description:                            |
   |    O_error_message     Returned error message                  |
   |    I_store_day_seq_no  Which store and date the totals should  |
   |                        be recalculated for.                    |
   |    I_pos_totals_ind    Determines whether the                  |
   |                        process_pos_totals function is called.  |
   |                                                                |
   |    Called from:        Sales Audit form: sa_total.fmb          |
   |                        Sales Audit nightly batch: satotals.pc  |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
    IS
        l_total_id               SA_TOTAL_HEAD.TOTAL_ID%TYPE;
        l_update_datetime        SA_TOTAL_HEAD.UPDATE_DATETIME%TYPE;
        l_start_date             VARCHAR2(6);
        l_end_date               VARCHAR2(6);
        l_status                 SA_TOTAL_HEAD.STATUS%TYPE;
        l_object_name            ALL_OBJECTS.OBJECT_NAME%TYPE;
        l_function_name          VARCHAR2(61);
        cursor_handle            INTEGER;
        feedback                 INTEGER;
        v_select                 sa_total_common.v_code_array :=  sa_total_common.empty_v_out;
        v_sql                    DBMS_SQL.VARCHAR2s;
        v_sql_blank              DBMS_SQL.VARCHAR2s;
        li_result                INT := 0;
        li_count                 INT := 0;
        l_process_calc_datetime  SA_TOTAL_HEAD.UPDATE_DATETIME%TYPE;
        L_store                  SA_STORE_DAY.STORE%TYPE := I_store;
        L_day                    SA_STORE_DAY.DAY%TYPE := I_day;

        /* retrieve all total_ids that are subscribed to by
            the passed store_day.  This query will order
            the results by update_datetime to ensure that
            any totals that are dependants are processed
            first. */
        cursor c_get_total_ids is
            select x.total_id, x.update_datetime,
                   to_char(x.start_business_date, 'YYMMDD'), to_char(x.end_business_date, 'YYMMDD'),
                   x.status
              from sa_total_head x
             where x.status not in ('DI', 'DE')
               and not exists (select distinct 'N'
                                 from sa_total_head z
                                where z.total_id     = x.total_id
                                  and z.total_rev_no > x.total_rev_no
                                  and (   (z.start_business_date <= x.start_business_date and
                                           z.end_business_date   >= x.start_business_date and
                                           z.end_business_date   is NOT NULL)
                                       or (z.start_business_date <= x.start_business_date and
                                           z.end_business_date   is NULL)
                                       or (z.start_business_date >= x.start_business_date and
                                           x.end_business_date   is NULL)
                                       or (z.start_business_date >= x.start_business_date and
                                           z.end_business_date   <= x.end_business_date and
                                           z.end_business_date   is NOT NULL)
                                       or (z.start_business_date <= x.end_business_date and
                                           z.end_business_date   >= x.end_business_date and
                                           z.end_business_date   is NOT NULL)
                                       or (z.start_business_date <= x.end_business_date and
                                           z.end_business_date   is NULL)
                                    ))
               and x.total_id in (
                   select DISTINCT h.total_id
                     from sa_total_head h
                    where h.sys_calc_ind = 'Y'
                      and h.total_id IN
                          (select total_id
                             from sa_total_loc_trait
                            where sa_total_loc_trait.total_rev_no = x.total_rev_no
                              and loc_trait IN
                                  (select loc_trait
                                     from loc_traits_matrix
                                    where STORE = L_store)))
            ORDER BY x.update_datetime;

        /* retrieve the dynamic total functions to call */
        cursor c_get_function_name is
            select owner || '.' || object_name
            from all_objects
            where object_name = L_object_name
            and object_type   = 'FUNCTION'
            and status        = 'VALID'
            and lower(owner)  = lower(sa_total_common.internal_schema_name)
            and NOT EXISTS (
               select 'x'
               from all_jobs
               where instr(UPPER(what), UPPER(object_name)) <>0);

    BEGIN
       if L_store is NULL or L_day is NULL then
          if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
             return FALSE;
          end if;
       end if;
        --- Delete previous non-approved INV_TOTAL_DEF errors.
        SQL_LIB.SET_MARK('DELETE',
                         NULL,
                         'SA_ERROR_WKSHT',
                         'Error Code: INV_TOTAL_DEF');
        delete from sa_error_wksht
         where store_day_seq_no = I_store_day_seq_no
           and store            = l_store
           and day              = l_day
           and error_code       = 'INV_TOTAL_DEF'
           and rec_type         = 'TOTERR';
        ---
        --- Delete previous approved INV_TOTAL_DEF errors.
        SQL_LIB.SET_MARK('DELETE',
                         NULL,
                         'SA_ERROR',
                         'Error Code: INV_TOTAL_DEF');
        delete from sa_error
         where store_day_seq_no = I_store_day_seq_no
           and store            = l_store
           and day              = l_day
           and error_code       = 'INV_TOTAL_DEF'
           and rec_type         = 'TOTERR';
        ---
        if I_pos_totals_ind = 'Y' THEN
            /* call the process pos totals function */
            if NOT PROCESS_POS_TOTALS(O_error_message, I_store_day_seq_no, NULL, I_store, I_day) then
                return(FALSE);
            end if;
        end if;
        ---
        /* Get the base date/time for calculating totals */
        l_process_calc_datetime := sysdate;
        ---
        cursor_handle := DBMS_SQL.OPEN_CURSOR;
        SQL_LIB.SET_MARK('OPEN',
                         'C_GET_TOTAL_IDS',
                         'SA_TOTAL_HEAD',
                         'Store/Day Seq.: '||to_char(I_store_day_seq_no));
        open c_get_total_ids;
        loop
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_TOTAL_IDS',
                             'SA_TOTAL_HEAD',
                             'Store/Day Seq.: '||to_char(I_store_day_seq_no));
            fetch c_get_total_ids INTO l_total_id, l_update_datetime,
                                       l_start_date, l_end_date,
                                       l_status;
            EXIT WHEN c_get_total_ids%NOTFOUND;
            BEGIN
                if l_end_date is NOT NULL then
                   L_object_name := 'SA_T_' || l_total_id || '_' || l_start_date || '_' || l_end_date;
                else
                   L_object_name := 'SA_T_' || l_total_id || '_' || l_start_date;
                end if;
                SQL_LIB.SET_MARK('OPEN',
                                 'C_GET_FUNCTION_NAME',
                                 'ALL_OBJECTS',
                                 'Function: '||L_object_name);
                open c_get_function_name;
                loop
                    SQL_LIB.SET_MARK('FETCH',
                                     'C_GET_FUNCTION_NAME',
                                     'ALL_OBJECTS',
                                     'Function: '||L_object_name);
                    fetch c_get_function_name INTO l_function_name;
                    EXIT WHEN c_get_function_name%NOTFOUND;
                    BEGIN
                        v_select := sa_total_common.empty_v_out;
                        v_sql := v_sql_blank;
                        feedback := 0;
                        li_count := 1;
                        v_select(v_select.count+1) := 'DECLARE';
                        v_select(v_select.count+1) := 'os_error_message VARCHAR2(256);';
                        v_select(v_select.count+1) := 'BEGIN';
                        v_select(v_select.count+1) := ':li_result := ' ||
                            l_function_name || '(:I_store_day_seq_no, os_error_message);';
                        v_select(v_select.count+1) := 'END;';
                        for n_count IN 1..v_select.count loop
                            v_sql(v_sql.count+1) := v_select(n_count);
                        end loop;
                        /* Use dynamic SQl to execute functions */
                        DBMS_SQL.PARSE (cursor_handle, v_sql, v_sql.FIRST,
                                        v_sql.LAST, TRUE, DBMS_SQL.NATIVE);
                        DBMS_SQL.BIND_VARIABLE(cursor_handle, 'li_result', li_result);
                        DBMS_SQL.BIND_VARIABLE(cursor_handle, 'I_store_day_seq_no', I_store_day_seq_no);
                        feedback := DBMS_SQL.EXECUTE(cursor_handle);
                        DBMS_SQL.VARIABLE_VALUE(cursor_handle, 'li_result', li_result);
                        if li_result <> 0 then
                           /* could not execute the function */
                           if l_status NOT in ('W', 'A') then
                              --- Status is not approved, so insert the error into the worksheet tables.
                              l_status := 'W';
                           end if;
                           --- Insert an error indicating that the total function did not execute successfully.
                           if SA_AUDIT_RULES_SQL.INSERT_ERROR(O_error_message,
                                                              I_store_day_seq_no,
                                                              NULL,            --- bal_group_seq_no
                                                              NULL,            --- total_seq_no
                                                              NULL,            --- tran_seq_no
                                                              NULL,            --- key_value_1
                                                              NULL,            --- key_value_2
                                                              'INV_TOTAL_DEF', --- error_code
                                                              'TOTERR',        --- rec_type
                                                              l_total_id,      --- orig_value
                                                              l_status,
                                                              l_store,
                                                              l_day) = FALSE then
                              --Close the Dynamic cursor before return
                              if DBMS_SQL.IS_OPEN(cursor_handle) then
                                DBMS_SQL.CLOSE_CURSOR(cursor_handle);
                              end if;
                              return FALSE;
                           end if;
                        end if;
                        if feedback <> 1 then
                            --Close the Dynamic cursor before return
                             if DBMS_SQL.IS_OPEN(cursor_handle) then
                               DBMS_SQL.CLOSE_CURSOR(cursor_handle);
                             end if;
                            RETURN(FALSE);
                        end if;
                    end;
                end loop;
                if li_count = 0 then
                    /* could not find the function */
                    O_error_message := sql_lib.create_msg('SABLDTL_NOFIND',
                                                          l_function_name,
                                                          NULL,
                                                          NULL);
                end if;
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_GET_FUNCTION_NAME',
                                 'ALL_OBJECTS',
                                 'Function: '||L_object_name);
                close c_get_function_name;
                ---
                /* zero out any values that were not recalculated by the function */
                if INSERT_ZERO_TOTALS(O_error_message,
                                      I_store_day_seq_no,
                                      l_total_id,
                                      l_process_calc_datetime,
                                      L_store,
                                      L_day) = FALSE then
                   --Close the Dynamic cursor before return
                   if DBMS_SQL.IS_OPEN(cursor_handle) then
                     DBMS_SQL.CLOSE_CURSOR(cursor_handle);
                   end if;
                   return(FALSE);
                end if;
                li_count := 0;
            END;
        end loop;
        if DBMS_SQL.IS_OPEN(cursor_handle) then
            DBMS_SQL.CLOSE_CURSOR(cursor_handle);
        end if;
        ---
        /* zero out totals that were missed by the main loop */
        if INSERT_ZERO_TOTALS(O_error_message,
                              I_store_day_seq_no,
                              null,                    -- total id
                              l_process_calc_datetime,
                              L_store,
                              L_day) = FALSE then
           return(FALSE);
        end if;
        ---
        /* Call the flash sales proc */
        if NOT SA_FLASH_SALES_SQL.WRITE_FLASH_SALES (O_error_message,
                                                     I_store_day_seq_no) then
            RETURN FALSE;
        ELSE
           /* update the audit status for the processed store_day */
           SQL_LIB.SET_MARK('UPDATE',
                            NULL,
                            'SA_STORE_DAY',
                            'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
           UPDATE sa_store_day
              SET audit_status = 'T',
                  audit_changed_datetime = SYSDATE
            WHERE store_day_seq_no = I_store_day_seq_no
              AND store = l_store
              AND day = l_day
              AND audit_status != 'T';
        END if;
        RETURN TRUE;
EXCEPTION
    WHEN others then
        /*Close the cursor cursor_handle if it is open*/
        if DBMS_SQL.IS_OPEN(cursor_handle) then
           DBMS_SQL.CLOSE_CURSOR(cursor_handle);
        end if;
        /*Close the cursor c_get_total_ids if it is open*/
        if c_get_total_ids%ISOPEN then
          close c_get_total_ids;
        end if;
        /*Close the cursor c_get_function_name if it is open*/
        if c_get_function_name%ISOPEN then
          close c_get_function_name;
        end if;
        O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'SA_BUILD_TOTAL_SQL.PROCESS_CALC_TOTALS',
                                              SQLCODE);
        RETURN(FALSE);
    END PROCESS_CALC_TOTALS;
-----------------------------------------------------------------------------------
    FUNCTION PROCESS_POS_TOTALS
        (O_error_message        IN OUT VARCHAR2,
         I_store_day_seq_no     IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
         I_total_id             IN     SA_TOTAL_HEAD.TOTAL_ID%TYPE,
         I_store                IN     SA_STORE_DAY.STORE%TYPE DEFAULT NULL,
         I_day                  IN     SA_STORE_DAY.DAY%TYPE DEFAULT NULL)
    RETURN BOOLEAN
    IS
   /****************************************************************\
   |                                                                |
   |   PROCESS_POS_TOTALS                                           |
   |                                                                |
   |   This function will calculate all POS totals for a passed     |
   |   store_day_seq_no.                                            |
   |                                                                |
   |    Arguments:          Description:                            |
   |    O_error_message     Returned error message                  |
   |    I_store_day_seq_no  Which store and date the totals should  |
   |                        be recalculated for.                    |
   |                                                                |
   |    Called from:        Sales Audit form: sa_total.fmb          |
   |                        Sales Audit nightly batch: satotals.pc  |
   |                        Transaction Detail form: satrdetl.fmb   |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
        L_total_id         SA_TOTAL_HEAD.TOTAL_ID%TYPE;
        L_total_rev_no     SA_POS_VALUE.TOTAL_REV_NO%TYPE;
        L_rollup1          SA_TOTAL.REF_NO1%TYPE;
        L_rollup2          SA_TOTAL.REF_NO2%TYPE;
        L_rollup3          SA_TOTAL.REF_NO3%TYPE;
        L_value            SA_TRAN_HEAD.VALUE%TYPE;

        L_bal_level        SA_TOTAL_HEAD.BAL_LEVEL%TYPE;
        L_cashier          SA_TRAN_HEAD.CASHIER%TYPE;
        L_register         SA_TRAN_HEAD.REGISTER%TYPE;
        L_bal_group        SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE;

        L_store            SA_STORE_DAY.STORE%TYPE := I_store;
        L_day              SA_STORE_DAY.DAY%TYPE := I_day;
        CURSOR c_get_pos_total_ids IS
        SELECT st.total_id,
               st.bal_level,
               st.total_rev_no
          FROM sa_total_head st
         WHERE st.pos_ind = 'Y'
           AND st.status in ('A', 'S', 'W')
           AND st.total_id IN
            (SELECT sa_total_loc_trait.total_id
               FROM sa_total_loc_trait
              WHERE sa_total_loc_trait.loc_trait IN
                (SELECT loc_traits_matrix.loc_trait
                   FROM loc_traits_matrix
                  WHERE loc_traits_matrix.store =
                    (SELECT sd.store
                       FROM sa_store_day sd
                      WHERE sd.store_day_seq_no = I_store_day_seq_no
                        AND sd.store = L_store
                        AND st.total_rev_no =
                            (SELECT max(st2.total_rev_no)
                               FROM sa_total_head st2
                              WHERE st2.total_id = st.total_id
                                AND sd.business_date >= st2.start_business_date
                                AND (   st.end_business_date IS NULL
                                     OR sd.business_date <= st2.end_business_date))
                        AND audit_status = 'R')))
           AND st.total_id = NVL(I_total_id, st.total_id);

        CURSOR c_get_pos_totals IS
            SELECT cashier,
                   register,
                   ref_no2,
                   ref_no3,
                   ref_no4,
                   value
              FROM sa_tran_head
             WHERE tran_type = 'TOTAL'
               AND ref_no1 = L_total_id
               AND store_day_seq_no = I_store_day_seq_no
               AND store = L_store
               AND day = L_day;

        CURSOR c_get_bal_seq_no IS
            SELECT bal_group_seq_no
              FROM sa_balance_group
             WHERE store_day_seq_no = I_store_day_seq_no
               AND (   (    cashier  = L_cashier
                        AND L_bal_level = 'C')
                    OR (    register = L_register
                        AND L_bal_level = 'R'));
    BEGIN
       if L_store is NULL or L_day is NULL then
          if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
             return FALSE;
          end if;
       end if;
       FOR r_get_pos_total_ids IN c_get_pos_total_ids LOOP
          L_total_id     := r_get_pos_total_ids.total_id;
          L_bal_level    := r_get_pos_total_ids.bal_level;
          L_total_rev_no := r_get_pos_total_ids.total_rev_no;

          FOR r_get_pos_totals IN c_get_pos_totals LOOP
             L_cashier  := r_get_pos_totals.cashier;
             L_register := r_get_pos_totals.register;
             L_rollup1  := r_get_pos_totals.ref_no2;
             L_rollup2  := r_get_pos_totals.ref_no3;
             L_rollup3  := r_get_pos_totals.ref_no4;
             L_value    := r_get_pos_totals.value;

             L_bal_group := NULL;
             -- if not at the store level, we need to find the bal_group_seq_no
             IF L_bal_level != 'S' THEN
                SQL_LIB.SET_MARK('OPEN',
                                 'C_GET_BAL_SEQ_NO',
                                 'SA_BALANCE_GROUP',
                                 'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
                OPEN  c_get_bal_seq_no;
                SQL_LIB.SET_MARK('FETCH',
                                 'C_GET_BAL_SEQ_NO',
                                 'SA_BALANCE_GROUP',
                                 'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
                FETCH c_get_bal_seq_no INTO L_bal_group;
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_GET_BAL_SEQ_NO',
                                 'SA_BALANCE_GROUP',
                                 'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
                CLOSE c_get_bal_seq_no;
                IF L_bal_group IS NULL THEN
                   --The balancing group for total, %s1, is not found at the %s2 level of %s3.
                   IF L_bal_level = 'R' THEN
                      O_error_message := SQL_LIB.CREATE_MSG('TOTAL_BAL_LEVEL_NOT_FOUND',
                                                            L_total_id,
                                                            L_bal_level,
                                                            L_register);
                   ELSE
                      O_error_message := SQL_LIB.CREATE_MSG('TOTAL_BAL_LEVEL_NOT_FOUND',
                                                            L_total_id,
                                                            L_bal_level,
                                                            L_cashier);
                   END IF;
                   RETURN FALSE;
                END IF;
             END IF;

             IF (INSERT_TOTAL (O_error_message,
                               L_total_id,
                               I_store_day_seq_no,
                               L_bal_group,
                               L_rollup1,
                               L_rollup2,
                               L_rollup3,
                               L_value,
                               L_total_rev_no,
                               'Y',
                               L_store,
                               L_day) < 0) THEN
                RETURN FALSE;
             END IF;
          END LOOP; /* c_get_pos_total_ids */
       END LOOP; /* c_get_pos_total_ids */
       RETURN (TRUE);
    EXCEPTION
        WHEN others then
            O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  'SA_BUILD_TOTAL_SQL.PROCESS_POS_TOTALS',
                                                  SQLCODE);
            RETURN(FALSE);
    END PROCESS_POS_TOTALS;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_BAL_LVL_TOTALS (O_error_message    IN OUT VARCHAR2,
                                I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE,
                                I_bal_group_seq_no IN     SA_BALANCE_GROUP.BAL_GROUP_SEQ_NO%TYPE,
                                I_store            IN     SA_STORE_DAY.STORE%TYPE,
                                I_day              IN     SA_STORE_DAY.DAY%TYPE)
   RETURN BOOLEAN
   IS
   /****************************************************************\
   |                                                                |
   |   INSERT_BAL_LVL_TOTALS                                        |
   |                                                                |
   |   This function will insert sa_total entries for all totals    |
   |   with the passed store_day_seq_no and bal_group_seq_no.       |
   |                                                                |
   |    Arguments:          Description:                            |
   |    O_error_message     Returned error message                  |
   |    I_store_day_seq_no  Which store and date the totals should  |
   |                        be recalculated for.                    |
   |    I_bal_group_seq_no  Which balance group the totals should   |
   |                        be recalculated for.                    |
   |                                                                |
   |    Called from:        Sales Audit form: sablsumm.fmb          |
   |                                                                |
   |    Created:                                                    |
   |    September 15, 1999                                          |
   |                                                                |
   \****************************************************************/
   L_total_id                SA_TOTAL_HEAD.TOTAL_ID%TYPE;
   Ln_total_seq_no           SA_TOTAL.TOTAL_SEQ_NO%TYPE := NULL;
   L_store                   SA_STORE_DAY.STORE%TYPE        := I_store;
   L_day                     SA_STORE_DAY.DAY%TYPE          := I_day;

   cursor c_get_bal_totals is
      select st.total_id
        from sa_total_head st
       where st.bal_level IN ('R', 'C')
         and st.total_rev_no =
                   (select max(th.total_rev_no)
                      from sa_total_head th
                     where th.total_id = st.total_id)
         and st.total_id in
             (select sa.total_id
                from sa_total_loc_trait sa
               where sa.loc_trait in (select l.loc_trait
                                        from loc_traits_matrix l
                                       where l.store  = I_store));

   BEGIN

      if L_store is NULL or L_day is NULL then
         if STORE_DAY_SQL.GET_INTERNAL_DAY(o_error_message,
                                           L_store,
                                           L_day,
                                           I_store_day_seq_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_BAL_TOTALS',
                       'SA_TOTAL_HEAD',
                       'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
      open c_get_bal_totals;
      loop
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_BAL_TOTALS',
                          'SA_TOTAL_HEAD',
                          'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
         fetch c_get_bal_totals into L_total_id;
         EXIT WHEN c_get_bal_totals%NOTFOUND;
         BEGIN
            /* Get total sequence number */
            if NOT SA_SEQUENCE2_SQL.GET_TOTAL_SEQ(O_error_message,
                                                  Ln_total_seq_no) then
               RETURN(FALSE);
            end if;
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'SA_TOTAL',
                             'Store/Day Seq. No.: '||to_char(I_store_day_seq_no)||', Total ID: '||L_total_id);
            INSERT INTO sa_total (total_seq_no,
                                  store,
                                  day,
                                  total_id,
                                  store_day_seq_no,
                                  ref_no1,
                                  ref_no2,
                                  ref_no3,
                                  bal_group_seq_no,
                                  error_ind,
                                  display_order,
                                  status)
                           select Ln_total_seq_no,
                                  L_store,
                                  L_day,
                                  L_total_id,
                                  I_store_day_seq_no,
                                  NULL,
                                  NULL,
                                  NULL,
                                  I_bal_group_seq_no,
                                  'N',
                                  display_order,
                                  'P'
                             from sa_total_head
                            where sa_total_head.total_id = L_total_id
                              and sa_total_head.total_rev_no =
                               (select max(total_rev_no)
                                  from sa_total_head
                                 where sa_total_head.total_id = L_total_id);
         END;
      end loop;
      SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_BAL_TOTALS',
                          'SA_TOTAL_HEAD',
                          'Store/Day Seq. No.: '||to_char(I_store_day_seq_no));
      close c_get_bal_totals;
      return (TRUE);
EXCEPTION
   WHEN others then
      /*Close the cursor c_get_bal_totals if it is open*/
      if c_get_bal_totals%ISOPEN then
        close c_get_bal_totals;
      end if;
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_BUILD_TOTAL_SQL.INSERT_BAL_LVL_TOTALS',
                                            SQLCODE);
      RETURN(FALSE);
END INSERT_BAL_LVL_TOTALS;
------------------------------------------------------------------------------------------------
FUNCTION GET_HEIRARCHY_TOTAL (O_error_message           IN OUT VARCHAR2,
                              O_total_value         IN OUT SA_SYS_VALUE.SYS_VALUE%TYPE,
                              I_hq_value            IN     SA_HQ_VALUE.HQ_VALUE%TYPE,
                              I_store_value         IN     SA_STORE_VALUE.STORE_VALUE%TYPE,
                              I_sys_value           IN     SA_SYS_VALUE.SYS_VALUE%TYPE,
                              I_pos_value           IN     SA_POS_VALUE.POS_VALUE%TYPE)
   RETURN BOOLEAN
   IS
   BEGIN
      O_total_value := 0;
      if I_hq_value is not null then
         O_total_value := I_hq_value;
      elsif I_store_value is not null then
         O_total_value := I_store_value;
      elsif I_sys_value is not null then
         O_total_value := I_sys_value;
      elsif I_pos_value is not null then
         O_total_value := I_pos_value;
      else
      O_error_message := sql_lib.create_msg('SABLDTL_NULLVALS',
                                            NULL,
                                            NULL,
                                            NULL);
         return (false);
      end if;
      return (true);
   END GET_HEIRARCHY_TOTAL;
------------------------------------------------------------------------------------------------
FUNCTION GET_SCHEMA(O_error_message        IN OUT VARCHAR2,
                    O_internal_schema_name IN OUT ALL_OBJECTS.OWNER%TYPE)
   return BOOLEAN is
BEGIN
   O_internal_schema_name := sa_total_common.internal_schema_name;
   if O_internal_schema_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SCHEMA_NAME',
                                             NULL,
                                             'SA_BUILD_TOTAL_SQL.GET_SCHEMA',
                                             NULL);
      return FALSE;
   end if;
   return TRUE;
   ---
EXCEPTION
        WHEN others then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SA_BUILD_TOTAL_SQL.GET_SCHEMA',
                                            SQLCODE);
      RETURN(FALSE);
        END GET_SCHEMA;
------------------------------------------------------------------------------------------------
FUNCTION DELETE_INVALID_DEF_ERROR(O_error_message      IN OUT  VARCHAR2,
                                  I_total_id           IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE,
                                  I_rule_id            IN      SA_RULE_HEAD.RULE_ID%TYPE)
   return BOOLEAN IS
   ---
   L_program_name  VARCHAR2(60) := 'SA_BUILD_TOTAL_SQL.DELETE_INVALID_DEF_ERROR';
   L_error_code    SA_ERROR_CODES.ERROR_CODE%TYPE := NULL;
BEGIN
   if I_total_id is NOT NULL then
      L_error_code := 'INV_TOTAL_DEF';
      ---
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'SA_ERROR',
                       'Orig. Value: '||I_total_id);
      delete from sa_error
       where error_code  = L_error_code
         and rec_type    = 'TOTERR'
         and orig_value  = I_total_id;
      ---
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'SA_ERROR_WKSHT',
                       'Orig. Value: '||I_total_id);
      delete from sa_error_wksht
       where error_code  = L_error_code
         and rec_type    = 'TOTERR'
         and orig_value  = I_total_id;
   end if;
   ---
   if I_rule_id is NOT NULL then
      L_error_code := 'INV_RULE_DEF';
      ---
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'SA_ERROR',
                       'Orig. Value: '||I_rule_id);
      delete from sa_error
       where error_code  = L_error_code
         and rec_type    = 'RULERR'
         and orig_value  = I_rule_id;
      ---
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'SA_ERROR_WKSHT',
                       'Orig. Value: '||I_rule_id);
      delete from sa_error_wksht
       where error_code  = L_error_code
         and rec_type    = 'RULERR'
         and orig_value  = I_rule_id;
   end if;
   ---
   return TRUE;
 ---
EXCEPTION
   WHEN others then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            SQLCODE);
      return FALSE;
END DELETE_INVALID_DEF_ERROR;
------------------------------------------------------------------------------------------------
FUNCTION MAX_TOTAL_REV_NO(O_error_message     IN OUT  VARCHAR2,
                          O_max_total_rev_no  IN OUT  SA_TOTAL_HEAD.TOTAL_REV_NO%TYPE,
                          I_total_id          IN      SA_TOTAL_HEAD.TOTAL_ID%TYPE)
   RETURN BOOLEAN IS
   L_program      VARCHAR2(50)   := 'SA_AUDIT_RULES_SQL.MAX_TOTAL_REV_NO';
   ---
   cursor C_GET_MAX_REV is
      select max(total_rev_no)
        from sa_total_head
       where total_id = I_total_id;
BEGIN
   if I_total_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_MAX_REV','SA_BUILD_TOTAL_SQL',NULL);
   open  C_GET_MAX_REV;
   SQL_LIB.SET_MARK('FETCH','C_GET_MAX_REV','SA_BUILD_TOTAL_SQL',NULL);
   fetch C_GET_MAX_REV into O_max_total_rev_no;
   SQL_LIB.SET_MARK('CLOSE','C_GET_MAX_REV','SA_BUILD_TOTAL_SQL',NULL);
   close C_GET_MAX_REV;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MAX_TOTAL_REV_NO;
------------------------------------------------------------------------------------------------
END SA_BUILD_TOTAL_SQL;
/
