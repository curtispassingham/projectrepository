PROMPT Creating Trigger 'RMS_TABLE_SAV_BIUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_SAV_BIUR
 BEFORE INSERT OR UPDATE
 ON SA_VOUCHER
 FOR EACH ROW

   --
BEGIN

   if INSERTING then
      -- Check for nulls in iss_date, iss_store, iss_register,
      -- iss_cashier or iss_amt
      if :new.ISS_DATE is not NULL     or
         :new.ISS_STORE is not NULL    or
         :new.ISS_REGISTER is not NULL or
         :new.ISS_CASHIER is not NULL  or
         :new.ISS_AMT is not NULL      then
         -- If any of the above is not null, place value in iss_post_date
         :new.ISS_POST_DATE := get_vdate;
      
end if;

      -- Check for nulls in red_date, red_store, red_register,
      -- red_cashier or red_amt
      if :new.RED_DATE is not NULL     or
         :new.RED_STORE is not NULL    or
         :new.RED_REGISTER is not NULL or
         :new.RED_CASHIER is not NULL  or
         :new.RED_AMT is not NULL      then
         -- If any of the above is not null, place value in red_post_date
         :new.RED_POST_DATE := get_vdate;
      
end if;

      -- Check for null in escheat_date
      if :new.ESCHEAT_DATE is not NULL     then
         -- If the above is not null, place value in esch_post_date
         :new.ESCH_POST_DATE := get_vdate;
      
end if;
   end if; -- end if INSERTING

   
if UPDATING then
      -- When checking updated rows, if :old.field is null and :new.field
      -- is not null or vice versa, the check
      --          (:old.field != :new.field)
      -- will not catch the change.  Thus the need for the checks:
      --          (:new.field is not NULL and :old.field is NULL)
      --          (:new.field is NULL and :old.field is not NULL)
      -- along with the != check.
      --
      -- Check for new values in tender_type_id, iss_date, iss_store,
      -- iss_register, iss_cashier or iss_amt.
      if :new.TENDER_TYPE_ID != :old.TENDER_TYPE_ID or
         :new.ISS_DATE       != :old.ISS_DATE       or
        (:new.ISS_DATE       is not NULL            and
         :old.ISS_DATE       is NULL)               or
        (:new.ISS_DATE       is NULL                and
         :old.ISS_DATE       is not NULL)           or
         :new.ISS_STORE      != :old.ISS_STORE      or
        (:new.ISS_STORE      is not NULL            and
         :old.ISS_STORE      is NULL)               or
        (:new.ISS_STORE      is NULL                and
         :old.ISS_STORE      is not NULL)           or
         :new.ISS_REGISTER   != :old.ISS_REGISTER   or
        (:new.ISS_REGISTER   is not NULL            and
         :old.ISS_REGISTER   is NULL)               or
        (:new.ISS_REGISTER   is NULL                and
         :old.ISS_REGISTER   is not NULL)           or
         :new.ISS_CASHIER    != :old.ISS_CASHIER    or
        (:new.ISS_CASHIER    is not NULL            and
         :old.ISS_CASHIER    is NULL)               or
        (:new.ISS_CASHIER    is NULL                and
         :old.ISS_CASHIER    is not NULL)           or
         :new.ISS_AMT        != :old.ISS_AMT        or
        (:new.ISS_AMT        is not NULL            and
         :old.ISS_AMT        is NULL)               or
        (:new.ISS_AMT        is NULL                and
         :old.ISS_AMT        is not NULL)           then
         -- If any of the above have changed, place value in iss_post_date
         :new.ISS_POST_DATE := get_vdate;
      end if;

      -- Check for new values in tender_type_id, red_date, red_store,
      -- red_register, red_cashier or red_amt
      if :new.TENDER_TYPE_ID != :old.TENDER_TYPE_ID or
         :new.RED_DATE       != :old.RED_DATE       or
        (:new.RED_DATE       is not NULL            and
         :old.RED_DATE       is NULL)               or
        (:new.RED_DATE       is NULL                and
         :old.RED_DATE       is not NULL)           or
         :new.RED_STORE      != :old.RED_STORE      or
        (:new.RED_STORE      is not NULL            and
         :old.RED_STORE      is NULL)               or
        (:new.RED_STORE      is NULL                and
         :old.RED_STORE      is not NULL)           or
         :new.RED_REGISTER   != :old.RED_REGISTER   or
        (:new.RED_REGISTER   is not NULL            and
         :old.RED_REGISTER   is NULL)               or
        (:new.RED_REGISTER   is NULL                and
         :old.RED_REGISTER   is not NULL)           or
         :new.RED_CASHIER    != :old.RED_CASHIER    or
        (:new.RED_CASHIER    is not NULL            and
         :old.RED_CASHIER    is NULL)               or
        (:new.RED_CASHIER    is NULL                and
         :old.RED_CASHIER    is not NULL)           or
         :new.RED_AMT        != :old.RED_AMT        or
        (:new.RED_AMT        is not NULL            and
         :old.RED_AMT        is NULL)               or
        (:new.RED_AMT        is NULL                and
         :old.RED_AMT        is not NULL)           then
         -- If any of the above have changed, place value in red_post_date
         :new.RED_POST_DATE := get_vdate;
      
end if;

      -- Check for new values in escheat_date
      if :new.ESCHEAT_DATE   != :old.ESCHEAT_DATE or
        (:new.ESCHEAT_DATE   is not NULL          and
         :old.ESCHEAT_DATE   is NULL)             or
        (:new.ESCHEAT_DATE   is NULL              and
         :old.ESCHEAT_DATE   is not NULL)         then
         -- If the above has changed, place value in esch_post_date
         :new.ESCH_POST_DATE := get_vdate;
      end if;
   end if; -- end if UPDATING

END;
/
