SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
------------------------------------------------------------------------------
-- Name:    CHECK_NUM_VAL
-- Purpose: This function is used to validate if I_value is a valid number value. 
--          Returns 'TRUE' if I_value is a number else returns 'FALSE'.
------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION CHECK_NUM_VAL(I_value  IN     VARCHAR2)
RETURN VARCHAR2 AUTHID CURRENT_USER IS

   L_val   NUMBER;
   
BEGIN
   L_val := TO_NUMBER(I_value);
   return 'TRUE';

EXCEPTION 
   when VALUE_ERROR then
      return 'FALSE';
   
END;
/