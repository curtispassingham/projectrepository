
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------------------
--Function Name: SA_CC_SECURITY_PREDICATE
--Purpose      : The purpose of this function is to return a predicate which would be
--               appended to the where clause of queries, when security policies are
--               implemented. It would be referred from the add_cc_sec_policy script
--               which applies the security policies to the tables containing credit
--               card information.
-----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION SA_CC_SECURITY_PREDICATE(I_object_schema IN VARCHAR2 DEFAULT NULL,
                                                    I_object_name   IN VARCHAR2 DEFAULT NULL)
   RETURN VARCHAR2 AUTHID CURRENT_USER IS
   
   L_predicate          VARCHAR2(500)           := NULL;

   
BEGIN

   L_predicate := 'exists (select ''x''' ||
                           ' from dba_role_privs' ||
                          ' where grantee = SYS_CONTEXT (''USERENV'', ''SESSION_USER'')' ||
                            ' and granted_role = ''CC_ACCESS'')';
   return L_predicate;
EXCEPTION
   when OTHERS then
      return NULL;
END SA_CC_SECURITY_PREDICATE;
/
