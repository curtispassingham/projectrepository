SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
------------------------------------------------------------------------------
-- Name:    CHECK_DATE_VAL
-- Purpose: This function will validate if a given I_value input is a valid date. 
--          Returns 'TRUE' if valid date, returs 'FALSE' otherswise. 
------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION CHECK_DATE_VAL(I_value                 IN     VARCHAR2,
                                          I_format                IN     VARCHAR2)                                          
RETURN VARCHAR2 AUTHID CURRENT_USER IS 

   L_val       DATE;   
   
BEGIN

   L_val := TO_DATE(I_value, I_format);
   
   return 'TRUE';
   
EXCEPTION
   when OTHERS then
      return 'FALSE';
END;
/