
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
------------------------------------------------------------------------------
-- Name:    SA_DATE_HASH
-- Purpose: Hash the business date into a simple number suitable for
--          a database hash partition value.
------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION SA_DATE_HASH ( I_date IN DATE)
   RETURN NUMBER AUTHID CURRENT_USER IS

BEGIN
   return to_number(to_char(I_date,'DD'));
EXCEPTION
   when OTHERS then
      return NULL;
END SA_DATE_HASH;
/
