CREATE OR REPLACE FORCE VIEW V_SA_ROUNDING_RULE_HEAD_TL (ROUNDING_RULE_ID, ROUNDING_RULE_NAME, LANG ) AS
SELECT  b.rounding_rule_id,
        case when tl.lang is not null then tl.rounding_rule_name else b.rounding_rule_name end rounding_rule_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SA_ROUNDING_RULE_HEAD b,
        SA_ROUNDING_RULE_HEAD_TL tl
 WHERE  b.rounding_rule_id = tl.rounding_rule_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SA_ROUNDING_RULE_HEAD_TL is 'This is the translation view for base table SA_ROUNDING_RULE_HEAD. This view fetches data in user langauge either from translation table SA_ROUNDING_RULE_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SA_ROUNDING_RULE_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SA_ROUNDING_RULE_HEAD_TL.ROUNDING_RULE_ID is 'This field contains the rounding rule id.'
/

COMMENT ON COLUMN V_SA_ROUNDING_RULE_HEAD_TL.ROUNDING_RULE_NAME is 'This field contains the description of the rounding rule.'
/

