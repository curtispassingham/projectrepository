CREATE OR REPLACE FORCE VIEW V_HALF_TL (HALF_NO, HALF_NAME, LANG ) AS
SELECT  b.half_no,
        case when tl.lang is not null then tl.half_name else b.half_name end half_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  HALF b,
        HALF_TL tl
 WHERE  b.half_no = tl.half_no (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_HALF_TL is 'This is the translation view for base table HALF. This view fetches data in user langauge either from translation table HALF_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_HALF_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_HALF_TL.HALF_NO is 'This field contains the year and the half number (1 or 2) for each half.  For example: 20131 or 20132.'
/

COMMENT ON COLUMN V_HALF_TL.HALF_NAME is 'This field contains the season name and year for the half.  For example:  Summer 2013.'
/

