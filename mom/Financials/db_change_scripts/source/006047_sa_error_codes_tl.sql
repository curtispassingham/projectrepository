--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SA_ERROR_CODES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SA_ERROR_CODES_TL(
LANG NUMBER(6) NOT NULL,
ERROR_CODE VARCHAR2(25) NOT NULL,
ERROR_DESC VARCHAR2(255) NOT NULL,
REC_SOLUTION VARCHAR2(255) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SA_ERROR_CODES_TL is 'This is the translation table for SA_ERROR_CODES table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.ERROR_CODE is 'This field contains a unique error code.'
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.ERROR_DESC is 'This field contains the error description.'
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.REC_SOLUTION is 'This field contains the recommended solution for the error.'
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SA_ERROR_CODES_TL ADD CONSTRAINT PK_SA_ERROR_CODES_TL PRIMARY KEY (
LANG,
ERROR_CODE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SA_ERROR_CODES_TL
 ADD CONSTRAINT SECT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SA_ERROR_CODES_TL ADD CONSTRAINT SECT_SEC_FK FOREIGN KEY (
ERROR_CODE
) REFERENCES SA_ERROR_CODES (
ERROR_CODE
)
/

