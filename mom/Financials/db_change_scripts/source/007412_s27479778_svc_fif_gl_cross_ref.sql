--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_FIF_GL_CROSS_REF'
ALTER TABLE SVC_FIF_GL_CROSS_REF ADD(
	DR_SEQUENCE11 VARCHAR2(25 ), 
	DR_SEQUENCE12 VARCHAR2(25 ), 
	DR_SEQUENCE13 VARCHAR2(25 ), 
	DR_SEQUENCE14 VARCHAR2(25 ), 
	DR_SEQUENCE15 VARCHAR2(25 ), 
	DR_SEQUENCE16 VARCHAR2(25 ), 
	DR_SEQUENCE17 VARCHAR2(25 ), 
	DR_SEQUENCE18 VARCHAR2(25 ), 
	DR_SEQUENCE19 VARCHAR2(25 ), 
	DR_SEQUENCE20 VARCHAR2(25 ),
	CR_SEQUENCE11 VARCHAR2(25 ), 
	CR_SEQUENCE12 VARCHAR2(25 ), 
	CR_SEQUENCE13 VARCHAR2(25 ), 
	CR_SEQUENCE14 VARCHAR2(25 ), 
	CR_SEQUENCE15 VARCHAR2(25 ), 
	CR_SEQUENCE16 VARCHAR2(25 ), 
	CR_SEQUENCE17 VARCHAR2(25 ), 
	CR_SEQUENCE18 VARCHAR2(25 ), 
	CR_SEQUENCE19 VARCHAR2(25 ), 
	CR_SEQUENCE20 VARCHAR2(25 ))
/	
	
COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE11" IS 'Oracle debit account mapping field11, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE12" IS 'Oracle debit account mapping field12, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE13" IS 'Oracle debit account mapping field13, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE14" IS 'Oracle debit account mapping field14, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE15" IS 'Oracle debit account mapping field15, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE16" IS 'Oracle debit account mapping field16, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE17" IS 'Oracle debit account mapping field17, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE18" IS 'Oracle debit account mapping field18, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE19" IS 'Oracle debit account mapping field19, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."DR_SEQUENCE20" IS 'Oracle debit account mapping field20, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE11" IS 'Oracle credit account mapping field11, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE12" IS 'Oracle credit account mapping field12, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE13" IS 'Oracle credit account mapping field13, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE14" IS 'Oracle credit account mapping field14, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE15" IS 'Oracle credit account mapping field15, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE16" IS 'Oracle credit account mapping field16, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE17" IS 'Oracle credit account mapping field17, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE18" IS 'Oracle credit account mapping field18, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE19" IS 'Oracle credit account mapping field19, maps to Oracle side GL_Code _combinations table'
/

COMMENT ON COLUMN "SVC_FIF_GL_CROSS_REF"."CR_SEQUENCE20" IS 'Oracle credit account mapping field20, maps to Oracle side GL_Code _combinations table'
/   
