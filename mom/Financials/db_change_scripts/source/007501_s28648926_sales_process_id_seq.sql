--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_BATCH_CONFIG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       ALTERING SEQUENCE               
--------------------------------------
PROMPT ALTERING SEQUENCE  'SALES_PROCESS_ID_SEQ'
ALTER SEQUENCE SALES_PROCESS_ID_SEQ MAXVALUE 9999999999
/
