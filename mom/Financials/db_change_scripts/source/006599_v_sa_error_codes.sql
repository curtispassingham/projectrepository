--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW ADDED:          V_SA_ERROR_CODES
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING VIEW
--------------------------------------
PROMPT Creating View 'V_SA_ERROR_CODES'
CREATE OR REPLACE FORCE VIEW V_SA_ERROR_CODES
 (ERROR_CODE
 ,ERROR_DESC
 ,TARGET_FORM
 ,TARGET_TAB
 ,REC_SOLUTION
 ,SHORT_DESC
 ,STORE_OVERRIDE_IND
 ,HQ_OVERRIDE_IND
 ,REQUIRED_IND
 ,MASS_RES_POP_UP_TYPE
 ,ERROR_FIX_TABLE
 ,ERROR_FIX_COLUMN)
 AS SELECT SEC.ERROR_CODE,
  V.ERROR_DESC AS ERROR_DESC,
  SEC.TARGET_FORM,
  SEC.TARGET_TAB,
  V.REC_SOLUTION AS REC_SOLUTION,
  V.SHORT_DESC AS SHORT_DESC,
  SEC.STORE_OVERRIDE_IND,
  SEC.HQ_OVERRIDE_IND,
  SEC.REQUIRED_IND,
  SEC.MASS_RES_POP_UP_TYPE,
  SEC.ERROR_FIX_TABLE,
  SEC.ERROR_FIX_COLUMN
FROM SA_ERROR_CODES SEC,
     V_SA_ERROR_CODES_TL V
WHERE SEC.ERROR_CODE = V.ERROR_CODE
/


COMMENT ON TABLE V_SA_ERROR_CODES IS 'This view will be used to provide translation on top of data contained in SA_ERROR_CODES table. Refer to table and column comments for SA_ERROR_CODES for more details'
/

