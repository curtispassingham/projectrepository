--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SA_TOTAL_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SA_TOTAL_HEAD_TL(
LANG NUMBER(6) NOT NULL,
TOTAL_ID VARCHAR2(10) NOT NULL,
TOTAL_REV_NO NUMBER(3) NOT NULL,
TOTAL_DESC VARCHAR2(255) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SA_TOTAL_HEAD_TL is 'This is the translation table for SA_TOTAL_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SA_TOTAL_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SA_TOTAL_HEAD_TL.TOTAL_ID is 'User entered id to uniquely identify the total being calculated.'
/

COMMENT ON COLUMN SA_TOTAL_HEAD_TL.TOTAL_REV_NO is 'Sequential number to uniquely identify the generation of the total being calculated.  This number will increment as a user edits an existing total calculation definition.'
/

COMMENT ON COLUMN SA_TOTAL_HEAD_TL.TOTAL_DESC is 'External name for the total created by the user.'
/

COMMENT ON COLUMN SA_TOTAL_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SA_TOTAL_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SA_TOTAL_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SA_TOTAL_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SA_TOTAL_HEAD_TL ADD CONSTRAINT PK_SA_TOTAL_HEAD_TL PRIMARY KEY (
LANG,
TOTAL_ID,
TOTAL_REV_NO
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SA_TOTAL_HEAD_TL
 ADD CONSTRAINT STHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SA_TOTAL_HEAD_TL ADD CONSTRAINT STHT_STH_FK FOREIGN KEY (
TOTAL_ID,
TOTAL_REV_NO
) REFERENCES SA_TOTAL_HEAD (
TOTAL_ID,
TOTAL_REV_NO
)
/

