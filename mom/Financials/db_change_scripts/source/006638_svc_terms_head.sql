--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_TERMS_HEAD'
ALTER TABLE SVC_TERMS_HEAD ADD TERMS_DESC VARCHAR2 (240 ) NULL
/

COMMENT ON COLUMN SVC_TERMS_HEAD.TERMS_DESC is 'Contains a description of the supplier terms. For example: 2.5% 30 days.'
/

ALTER TABLE SVC_TERMS_HEAD ADD TERMS_CODE VARCHAR2 (50 ) NULL
/

COMMENT ON COLUMN SVC_TERMS_HEAD.TERMS_CODE is 'Indicates the Alphanumeric representation of Term Name which acts as the Term code in Oracle Financials.'
/

