--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT DROPPING CONSTRAINT 'CHK_SA_SYS_OPT_CC_SEC_LVL'
DECLARE
  L_constraint_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraint_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_SA_SYS_OPT_CC_SEC_LVL'
     AND constraint_TYPE = 'C';

  if (L_constraint_exists != 0) then
      execute immediate 'ALTER TABLE SA_SYSTEM_OPTIONS DROP CONSTRAINT CHK_SA_SYS_OPT_CC_SEC_LVL';
  end if;
end;
/

PROMPT DROPPING CONSTRAINT 'CHK_SA_SYS_OPT_CC_VAL_REQD'
DECLARE
  L_constraint_exists_1 number := 0;
BEGIN
  SELECT count(*) INTO L_constraint_exists_1
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_SA_SYS_OPT_CC_VAL_REQD'
     AND constraint_TYPE = 'C';

  if (L_constraint_exists_1 != 0) then
      execute immediate 'ALTER TABLE SA_SYSTEM_OPTIONS DROP CONSTRAINT CHK_SA_SYS_OPT_CC_VAL_REQD';
  end if;
end;
/

PROMPT DROPPING CONSTRAINT 'CHK_SSO_CC_SEC'
DECLARE
  L_constraint_exists_2 number := 0;
BEGIN
  SELECT count(*) INTO L_constraint_exists_2
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_SSO_CC_SEC'
     AND constraint_TYPE = 'C';

  if (L_constraint_exists_2 != 0) then
      execute immediate 'ALTER TABLE SA_SYSTEM_OPTIONS DROP CONSTRAINT CHK_SSO_CC_SEC';
  end if;
end;
/

PROMPT Modifying Table 'SA_SYSTEM_OPTIONS'
ALTER TABLE SA_SYSTEM_OPTIONS DROP COLUMN CC_SEC_LVL_IND
/

ALTER TABLE SA_SYSTEM_OPTIONS DROP COLUMN CC_VAL_REQD
/
