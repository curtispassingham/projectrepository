--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 MV_SUBCLASS_LOC_HIST1
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
-- Creating Table
--------------------------------------
whenever sqlerror exit
SET SERVEROUTPUT ON

DECLARE

   L_query    VARCHAR2(10000);
   L_tab      VARCHAR2(50);         
   L_part      VARCHAR2(50);
   L_high_val  VARCHAR2(250);  
   L_first_day     DATE := NULL;
   
   cursor c_partition_min_val is
   with xml as (select dbms_xmlgen.getxmltype('select table_name, partition_name, high_value from user_tab_partitions where table_name = ''ITEM_LOC_HIST'' and partition_name = ''ITEM_LOC_HIST_MIN''' ) as x
                  from   dual)
    select extractValue(rws.object_value, '/ROW/TABLE_NAME') table_name,
           extractValue(rws.object_value, '/ROW/PARTITION_NAME') partition,
           extractValue(rws.object_value, '/ROW/HIGH_VALUE') high_value
      from xml x, 
           table(xmlsequence(extract(x.x, '/ROWSET/ROW'))) rws;
           
   CURSOR c_calendar is
      select first_day 
        from calendar 
       where rownum = 1 
    order by first_day;           

BEGIN

open c_calendar;
fetch c_calendar into L_first_day;
close c_calendar;

if L_first_day is NOT NULL then

   open c_partition_min_val;
   fetch c_partition_min_val into L_tab,L_part,L_high_val;
   close c_partition_min_val;

   L_high_val := SUBSTR(L_high_val,11,19);

   -- Create MV with interval partition with a min partition based on the partition created on ITEM_LOC_HIST
   L_query :=  'CREATE MATERIALIZED VIEW  MV_SUBCLASS_LOC_HIST ' ||
                  'PARTITION BY RANGE (EOW_DATE) INTERVAL(NUMTODSINTERVAL(7,''DAY'')) '||
                  '(PARTITION MV_SUBCLASS_LOC_HIST_MIN VALUES LESS THAN (TO_DATE('''||L_high_val||''',''SYYYY-MM-DD HH24:MI:SS'',''NLS_CALENDAR=GREGORIAN'')) INITRANS 12 TABLESPACE RETAIL_DATA)' || 
                  'BUILD IMMEDIATE REFRESH FAST ON DEMAND ENABLE QUERY REWRITE ' ||
                  'AS SELECT DEPT, CLASS, SUBCLASS, LOC, LOC_TYPE, EOW_DATE, WEEK_454, YEAR_454, SALES_TYPE, SUM(SALES_ISSUES) SALES, VALUE, GP, cast(NULL as NUMBER(12,4)) PLAN_TYPE ' ||
                  'FROM ITEM_LOC_HIST GROUP BY DEPT, CLASS, SUBCLASS, LOC,LOC_TYPE, EOW_DATE, WEEK_454, YEAR_454, SALES_TYPE, VALUE, GP';
    
   EXECUTE IMMEDIATE L_query;
   
else --during full install

   L_query :=  'CREATE MATERIALIZED VIEW  MV_SUBCLASS_LOC_HIST ' ||
                  'INITRANS 12 TABLESPACE RETAIL_DATA ' || 
                  'BUILD IMMEDIATE REFRESH FAST ON DEMAND ENABLE QUERY REWRITE ' ||
                  'AS SELECT DEPT, CLASS, SUBCLASS, LOC, LOC_TYPE, EOW_DATE, WEEK_454, YEAR_454, SALES_TYPE, SUM(SALES_ISSUES) SALES, VALUE, GP, cast(NULL as NUMBER(12,4)) PLAN_TYPE ' ||
                  'FROM ITEM_LOC_HIST GROUP BY DEPT, CLASS, SUBCLASS, LOC,LOC_TYPE, EOW_DATE, WEEK_454, YEAR_454, SALES_TYPE, VALUE, GP';
    
   EXECUTE IMMEDIATE L_query;   

end if;

EXCEPTION
      
   when OTHERS then
     dbms_output.put_line('exception: '||SQLCODE||', '||SQLERRM);
END;   
/
