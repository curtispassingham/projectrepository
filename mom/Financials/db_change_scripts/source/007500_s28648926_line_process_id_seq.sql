--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE UPDATED:				LINE_PROCESS_ID_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING SEQUENCE
--------------------------------------
PROMPT Updating Sequence 'LINE_PROCESS_ID_SEQ'
ALTER SEQUENCE LINE_PROCESS_ID_SEQ MAXVALUE 999999999999999 NOCYCLE
/



