--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 POS_TENDER_TYPE_HEAD_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'POS_TENDER_TYPE_HEAD_TL'
CREATE TABLE POS_TENDER_TYPE_HEAD_TL
 (LANG NUMBER(6) NOT NULL,
  TENDER_TYPE_ID NUMBER(6) NOT NULL,
  TENDER_TYPE_DESC VARCHAR2(120 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL,
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPDATE_DATETIME DATE NOT NULL,
  LAST_UPDATE_ID VARCHAR2(30 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE POS_TENDER_TYPE_HEAD_TL is 'This is the translation table for POS_TENDER_TYPE_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN POS_TENDER_TYPE_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN POS_TENDER_TYPE_HEAD_TL.TENDER_TYPE_ID is 'This field contains the code which uniquely identifies the tender type.'
/

COMMENT ON COLUMN POS_TENDER_TYPE_HEAD_TL.TENDER_TYPE_DESC is 'This field contains the description of the tender type.'
/

COMMENT ON COLUMN POS_TENDER_TYPE_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN POS_TENDER_TYPE_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN POS_TENDER_TYPE_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN POS_TENDER_TYPE_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

PROMPT Creating Primary Key on 'POS_TENDER_TYPE_HEAD_TL'
ALTER TABLE POS_TENDER_TYPE_HEAD_TL
 ADD CONSTRAINT PK_POS_TENDER_TYPE_HEAD_TL PRIMARY KEY
  (LANG,
   TENDER_TYPE_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

PROMPT Creating FK on 'POS_TENDER_TYPE_HEAD_TL'
 ALTER TABLE POS_TENDER_TYPE_HEAD_TL
  ADD CONSTRAINT PTTHT_LANG_FK
  FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/


PROMPT Creating FK on 'POS_TENDER_TYPE_HEAD_TL'
 ALTER TABLE POS_TENDER_TYPE_HEAD_TL
  ADD CONSTRAINT PTTHT_PTTH_FK
  FOREIGN KEY (TENDER_TYPE_ID)
 REFERENCES POS_TENDER_TYPE_HEAD (TENDER_TYPE_ID)
/

