CREATE OR REPLACE FORCE VIEW V_TRAN_DATA_CODES_TL (CODE, DECODE, LANG ) AS
SELECT  b.code,
        case when tl.lang is not null then tl.decode else b.decode end decode,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  TRAN_DATA_CODES b,
        TRAN_DATA_CODES_TL tl
 WHERE  b.code = tl.code (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_TRAN_DATA_CODES_TL is 'This is the translation view for base table TRAN_DATA_CODES. This view fetches data in user langauge either from translation table TRAN_DATA_CODES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_TL.CODE is 'This field contains the code that uniquely identifies the transaction type.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_TL.DECODE is 'This field contains the text description for the associated code.'
/

