--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_POSUPLD_STAGING_REJ
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_POSUPLD_STAGING_REJ'
CREATE TABLE SVC_POSUPLD_STAGING_REJ
 (FILE_TYPE VARCHAR2(5 ) NOT NULL,
  FILENAME VARCHAR2(250 ),
  LINE_SEQ_ID NUMBER(15) NOT NULL,
  THREAD_VAL NUMBER(10),
  LOCATION NUMBER(10),
  SALES_PROCESS_ID NUMBER(10),
  CHUNK_ID NUMBER(10),
  THEAD_ID NUMBER(10),
  HEAD_DETL_ID NUMBER(10),
  TDETL_ID NUMBER(10),
  COL_1 VARCHAR2(56 ),
  COL_2 VARCHAR2(40 ),
  COL_3 VARCHAR2(100 ),
  COL_4 VARCHAR2(80 ),
  COL_5 VARCHAR2(80 ),
  COL_6 VARCHAR2(40 ),
  COL_7 VARCHAR2(4 ),
  COL_8 VARCHAR2(4 ),
  COL_9 VARCHAR2(4 ),
  COL_10 VARCHAR2(24 ),
  COL_11 VARCHAR2(48 ),
  COL_12 VARCHAR2(4 ),
  COL_13 VARCHAR2(4 ),
  COL_14 VARCHAR2(48 ),
  COL_15 VARCHAR2(16 ),
  COL_16 VARCHAR2(4 ),
  COL_17 VARCHAR2(80 ),
  COL_18 VARCHAR2(56 ),
  COL_19 VARCHAR2(4 ),
  COL_20 VARCHAR2(48 ),
  COL_21 VARCHAR2(4 ),
  COL_22 VARCHAR2(80 ),
  COL_23 VARCHAR2(1 ),
  COL_24 VARCHAR2(1 ),
  COL_25 VARCHAR2(10 ),
  COL_26 VARCHAR2(10 ),
  ERROR_MSG VARCHAR2(255 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_POSUPLD_STAGING_REJ is 'This table holds the records coming from v_svc_posupld_load view for processing.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.FILE_TYPE is 'File type of the record associated with the POSU file uploaded. Valid values include (FHEAD,THEAD, TDETL).'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.FILENAME is 'POSU filename'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.LINE_SEQ_ID is 'Automatically generated line sequence ID to identify a line item for an imported records into the RMS table.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.THREAD_VAL is 'Thread number associated with the POSU file uploaded into the system.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.LOCATION is 'Location ID of the store to which the POSU file belongs to.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.SALES_PROCESS_ID is 'A unique key for each input file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.CHUNK_ID is 'A unique key for each chunk to be processed as one unit.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.THEAD_ID is 'A unique id for a specific THEAD.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.HEAD_DETL_ID is 'Links the thead id with the tdetl id.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.TDETL_ID is 'A unique id for a TDETL.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_1 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_2 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_3 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_4 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_5 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_6 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_7 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_8 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_9 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_10 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_11 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_12 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_13 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_14 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_15 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_16 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_17 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_18 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_19 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_20 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_21 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_22 is 'Column to hold delimited data from the file.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_23 is 'This will hold the SALES_TYPE value.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_24 is 'This will hold the NO_INV_RET_IND value.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_25 is 'This will hold the RETURN_DISPOSITION value.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.COL_26 is 'This will hold the RETURN_WH value.'
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.ERROR_MSG is 'All the errors in a given record.'
/

