--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_SA_ERROR_CODES'
ALTER TABLE SVC_SA_ERROR_CODES ADD SHORT_DESC VARCHAR2 (40 BYTE) NULL
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES.SHORT_DESC is 'This field contains the short error description.'
/

