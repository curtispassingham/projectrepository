--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_POSUPLD_NIL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_POSUPLD_NIL'
CREATE TABLE SVC_POSUPLD_NIL
 (SALES_PROCESS_ID NUMBER(10),
  ITEM VARCHAR2(25 ),
  LOC NUMBER(10)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_POSUPLD_NIL is 'This table holds the new items to be ranged from the sales upload.'
/

COMMENT ON COLUMN SVC_POSUPLD_NIL.SALES_PROCESS_ID is 'The process id for the specific sales upload file.'
/

COMMENT ON COLUMN SVC_POSUPLD_NIL.ITEM is 'The item to be ranged.'
/

COMMENT ON COLUMN SVC_POSUPLD_NIL.LOC is 'The location where the item will be ranged.'
/

