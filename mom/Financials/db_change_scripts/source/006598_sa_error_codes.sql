--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SA_ERROR_CODES'
ALTER TABLE SA_ERROR_CODES ADD MASS_RES_POP_UP_TYPE VARCHAR2 (20 ) NULL
/

COMMENT ON COLUMN SA_ERROR_CODES.MASS_RES_POP_UP_TYPE is 'Contains the Mass Resolution Popup Type. It can be POP_UP_DEL,  POP_UP_REPLACE or blank.'
/

ALTER TABLE SA_ERROR_CODES ADD ERROR_FIX_TABLE VARCHAR2 (30 ) NULL
/

COMMENT ON COLUMN SA_ERROR_CODES.ERROR_FIX_TABLE is 'Contains the table name that contains the record or field that causes the error. '
/

ALTER TABLE SA_ERROR_CODES ADD ERROR_FIX_COLUMN VARCHAR2 (30 ) NULL
/

COMMENT ON COLUMN SA_ERROR_CODES.ERROR_FIX_COLUMN is 'Contains the column name of the field that causes the error.'
/

