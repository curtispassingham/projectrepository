--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SA_CONSTANTS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SA_CONSTANTS_TL(
LANG NUMBER(6) NOT NULL,
CONSTANT_ID VARCHAR2(30) NOT NULL,
CONSTANT_NAME VARCHAR2(250) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SA_CONSTANTS_TL is 'This is the translation table for SA_CONSTANTS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SA_CONSTANTS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SA_CONSTANTS_TL.CONSTANT_ID is 'This field contains a unique identifier for the constant.'
/

COMMENT ON COLUMN SA_CONSTANTS_TL.CONSTANT_NAME is 'This field contains a name for the constant.'
/

COMMENT ON COLUMN SA_CONSTANTS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SA_CONSTANTS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SA_CONSTANTS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SA_CONSTANTS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SA_CONSTANTS_TL ADD CONSTRAINT PK_SA_CONSTANTS_TL PRIMARY KEY (
LANG,
CONSTANT_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SA_CONSTANTS_TL
 ADD CONSTRAINT SCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SA_CONSTANTS_TL ADD CONSTRAINT SCT_SC_FK FOREIGN KEY (
CONSTANT_ID
) REFERENCES SA_CONSTANTS (
CONSTANT_ID
)
/

