--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SA_ERROR_CODES_TL'
ALTER TABLE SA_ERROR_CODES_TL ADD SHORT_DESC VARCHAR2 (40 BYTE) NULL
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.SHORT_DESC is 'This field contains the short error description.'
/

