CREATE OR REPLACE FORCE VIEW V_POS_TENDER_TYPE_HEAD_TL (TENDER_TYPE_ID, TENDER_TYPE_DESC, LANG ) AS
SELECT  b.TENDER_TYPE_ID,
        case when tl.lang is not null then tl.TENDER_TYPE_DESC else b.TENDER_TYPE_DESC end TENDER_TYPE_DESC,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  POS_TENDER_TYPE_HEAD b,
        POS_TENDER_TYPE_HEAD_TL tl
 WHERE  b.tender_type_id = tl.tender_type_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_POS_TENDER_TYPE_HEAD_TL is 'This is the translation view for base table POS_TENDER_TYPE_HEAD. This view fetches data in user langauge either from translation table POS_TENDER_TYPE_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_POS_TENDER_TYPE_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_POS_TENDER_TYPE_HEAD_TL.TENDER_TYPE_ID is 'This field contains the code which uniquely identifies the tender type.'
/

COMMENT ON COLUMN V_POS_TENDER_TYPE_HEAD_TL.TENDER_TYPE_DESC is 'This field contains the description of the tender type.'
/
