--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       TRAN_DATA_CODES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE TRAN_DATA_CODES_TL(
LANG NUMBER(6) NOT NULL,
CODE NUMBER(4) NOT NULL,
DECODE VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TRAN_DATA_CODES_TL is 'This is the translation table for TRAN_DATA_CODES table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN TRAN_DATA_CODES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN TRAN_DATA_CODES_TL.CODE is 'This field contains the code that uniquely identifies the transaction type.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_TL.DECODE is 'This field contains the text description for the associated code.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE TRAN_DATA_CODES_TL ADD CONSTRAINT PK_TRAN_DATA_CODES_TL PRIMARY KEY (
LANG,
CODE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE TRAN_DATA_CODES_TL
 ADD CONSTRAINT TDCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE TRAN_DATA_CODES_TL ADD CONSTRAINT TDCT_TDC_FK FOREIGN KEY (
CODE
) REFERENCES TRAN_DATA_CODES (
CODE
)
/

