--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SA_STORE_DAY'
ALTER TABLE SA_STORE_DAY ADD OMS_FILES_LOADED NUMBER  NULL
/

COMMENT ON COLUMN SA_STORE_DAY.OMS_FILES_LOADED is 'This column indicates the number of transaction data files that have been imported from OMS system into sales audit for the store/day.'
/

