--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_TERMS_HEAD
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_TERMS_HEAD'
CREATE TABLE SVC_TERMS_HEAD
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  RANK NUMBER(10,0),
  TERMS VARCHAR2(15 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_TERMS_HEAD is 'This is  a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in TERMS_HEAD. '
/

COMMENT ON COLUMN SVC_TERMS_HEAD.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.RANK is 'Unique rank to rate invoice payment terms against purchase order terms (header table).'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.TERMS is 'Contains a number uniquely identifying the supplier terms.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_TERMS_HEAD'
ALTER TABLE SVC_TERMS_HEAD
 ADD CONSTRAINT SVC_TERMS_HEAD_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_TERMS_HEAD'
ALTER TABLE SVC_TERMS_HEAD
 ADD CONSTRAINT SVC_TERMS_HEAD_UK UNIQUE
  (TERMS
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

