CREATE OR REPLACE FORCE VIEW V_SA_CONSTANTS_TL (CONSTANT_ID, CONSTANT_NAME, LANG ) AS
SELECT  b.constant_id,
        case when tl.lang is not null then tl.constant_name else b.constant_name end constant_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SA_CONSTANTS b,
        SA_CONSTANTS_TL tl
 WHERE  b.constant_id = tl.constant_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SA_CONSTANTS_TL is 'This is the translation view for base table SA_CONSTANTS. This view fetches data in user langauge either from translation table SA_CONSTANTS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SA_CONSTANTS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SA_CONSTANTS_TL.CONSTANT_ID is 'This field contains a unique identifier for the constant.'
/

COMMENT ON COLUMN V_SA_CONSTANTS_TL.CONSTANT_NAME is 'This field contains a name for the constant.'
/

