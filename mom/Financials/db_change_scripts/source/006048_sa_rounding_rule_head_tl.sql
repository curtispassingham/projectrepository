--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SA_ROUNDING_RULE_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SA_ROUNDING_RULE_HEAD_TL(
LANG NUMBER(6) NOT NULL,
ROUNDING_RULE_ID VARCHAR2(10) NOT NULL,
ROUNDING_RULE_NAME VARCHAR2(255) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SA_ROUNDING_RULE_HEAD_TL is 'This is the translation table for SA_ROUNDING_RULE_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SA_ROUNDING_RULE_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SA_ROUNDING_RULE_HEAD_TL.ROUNDING_RULE_ID is 'This field contains the rounding rule id.'
/

COMMENT ON COLUMN SA_ROUNDING_RULE_HEAD_TL.ROUNDING_RULE_NAME is 'This field contains the description of the rounding rule.'
/

COMMENT ON COLUMN SA_ROUNDING_RULE_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SA_ROUNDING_RULE_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SA_ROUNDING_RULE_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SA_ROUNDING_RULE_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SA_ROUNDING_RULE_HEAD_TL ADD CONSTRAINT PK_SA_ROUNDING_RULE_HEAD_TL PRIMARY KEY (
LANG,
ROUNDING_RULE_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SA_ROUNDING_RULE_HEAD_TL
 ADD CONSTRAINT SRRHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SA_ROUNDING_RULE_HEAD_TL ADD CONSTRAINT SRRHT_SRRH_FK FOREIGN KEY (
ROUNDING_RULE_ID
) REFERENCES SA_ROUNDING_RULE_HEAD (
ROUNDING_RULE_ID
)
/

