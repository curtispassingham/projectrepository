--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_POSUPLD_STAGING_REJ
----------------------------------------------------------------------------


whenever sqlerror exit failure



--------------------------------------
--       Altering Table
--------------------------------------
PROMPT Altering table SVC_POSUPLD_STATUS
ALTER TABLE SVC_POSUPLD_STATUS ADD LOCATION NUMBER(10,0)
/

COMMENT ON COLUMN SVC_POSUPLD_STATUS.LOCATION IS 'This column has Location ID of the POSU file.'
/
