--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_FREIGHT_TERMS'
ALTER TABLE SVC_FREIGHT_TERMS ADD TERM_DESC VARCHAR2 (240 ) NULL
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.TERM_DESC is 'Contains a description of the freight terms used in the system. Examples include a specified percent of total cost, free, or a specified percentage added to the invoice.'
/

