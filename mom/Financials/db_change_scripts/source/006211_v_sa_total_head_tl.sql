CREATE OR REPLACE FORCE VIEW V_SA_TOTAL_HEAD_TL (TOTAL_ID, TOTAL_REV_NO, TOTAL_DESC, LANG ) AS
SELECT  b.total_id,
        b.total_rev_no,
        case when tl.lang is not null then tl.total_desc else b.total_desc end total_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SA_TOTAL_HEAD b,
        SA_TOTAL_HEAD_TL tl
 WHERE  b.total_id = tl.total_id (+)
   AND  b.total_rev_no = tl.total_rev_no (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SA_TOTAL_HEAD_TL is 'This is the translation view for base table SA_TOTAL_HEAD. This view fetches data in user langauge either from translation table SA_TOTAL_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SA_TOTAL_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SA_TOTAL_HEAD_TL.TOTAL_ID is 'User entered id to uniquely identify the total being calculated.'
/

COMMENT ON COLUMN V_SA_TOTAL_HEAD_TL.TOTAL_REV_NO is 'Sequential number to uniquely identify the generation of the total being calculated.  This number will increment as a user edits an existing total calculation definition.'
/

COMMENT ON COLUMN V_SA_TOTAL_HEAD_TL.TOTAL_DESC is 'External name for the total created by the user.'
/

