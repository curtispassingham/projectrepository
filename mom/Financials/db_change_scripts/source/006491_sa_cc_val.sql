--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Table      
-------------------------------------- 
PROMPT DROPPING CONSTRAINT 'PK_SA_CC_VAL'
DECLARE
  L_constraint_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraint_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'PK_SA_CC_VAL'
     AND constraint_TYPE = 'P';

  if (L_constraint_exists != 0) then
      execute immediate 'ALTER TABLE SA_CC_VAL DROP CONSTRAINT PK_SA_CC_VAL DROP INDEX';
  end if;
end;
/

PROMPT DROPPING CONSTRAINT 'SCV_PTT_FK'
DECLARE
  L_constraint_exists_1 number := 0;
BEGIN
  SELECT count(*) INTO L_constraint_exists_1
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'SCV_PTT_FK'
     AND constraint_TYPE = 'R';

  if (L_constraint_exists_1 != 0) then
      execute immediate 'ALTER TABLE SA_CC_VAL DROP CONSTRAINT SCV_PTT_FK';
  end if;
end;
/
        
PROMPT DROPPING Table 'SA_CC_VAL'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_TABLES
   WHERE TABLE_NAME = 'SA_CC_VAL';

  if (L_table_exists != 0) then
      execute immediate 'drop table SA_CC_VAL';
  end if;
end;
/
