--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_POSUPLD_LOAD_ARCH'
ALTER TABLE SVC_POSUPLD_LOAD_ARCH MODIFY LINE_SEQ_ID NUMBER (15)
/

COMMENT ON COLUMN SVC_POSUPLD_LOAD_ARCH.LINE_SEQ_ID is 'Automatically generated line sequence ID to identify a line item for an imported records into the RMS table.'
/

