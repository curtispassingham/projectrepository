--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_POSUPLD_STAGING_REJ'
ALTER TABLE SVC_POSUPLD_STAGING_REJ ADD TTAX_ID NUMBER (10) NULL
/

COMMENT ON COLUMN SVC_POSUPLD_STAGING_REJ.TTAX_ID is 'A unique id for a TTAX line.'
/

