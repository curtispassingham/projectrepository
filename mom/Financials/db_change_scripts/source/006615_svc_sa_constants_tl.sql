--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_SA_CONSTANTS_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_SA_CONSTANTS_TL'
CREATE TABLE SVC_SA_CONSTANTS_TL
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  CONSTANT_NAME VARCHAR2(250 ),
  CONSTANT_ID VARCHAR2(30 ),
  LANG NUMBER(6,0),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_SA_CONSTANTS_TL is 'This is  a staging table used for Admin API spreadsheet upload process.It is used to   temporarily hold data before it is uploaded/updated in SA_CONSTANTS_TL. '
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.CONSTANT_NAME is 'This field contains a name for the constant.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.CONSTANT_ID is 'This field contains a unique identifier for the constant.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS_TL.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_SA_CONSTANTS_TL'
ALTER TABLE SVC_SA_CONSTANTS_TL
 ADD CONSTRAINT SVC_SA_CONSTANTS_TL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_SA_CONSTANTS_TL'
ALTER TABLE SVC_SA_CONSTANTS_TL
 ADD CONSTRAINT SVC_SA_CONSTANTS_TL_UK UNIQUE
  (LANG,
   CONSTANT_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

