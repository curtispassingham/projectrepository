CREATE OR REPLACE FORCE VIEW V_SA_RULE_HEAD_TL (RULE_ID, RULE_REV_NO, RULE_NAME, LANG ) AS
SELECT  b.rule_id,
        b.rule_rev_no,
        case when tl.lang is not null then tl.rule_name else b.rule_name end rule_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SA_RULE_HEAD b,
        SA_RULE_HEAD_TL tl
 WHERE  b.rule_id = tl.rule_id (+)
   AND  b.rule_rev_no = tl.rule_rev_no (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SA_RULE_HEAD_TL is 'This is the translation view for base table SA_RULE_HEAD. This view fetches data in user langauge either from translation table SA_RULE_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SA_RULE_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SA_RULE_HEAD_TL.RULE_ID is 'User entered id to uniquely identify the rule being calculated.'
/

COMMENT ON COLUMN V_SA_RULE_HEAD_TL.RULE_REV_NO is 'Sequential number to uniquely identify the generation of the rule being calculated.  This number will increment as a user edits an existing rule definition.'
/

COMMENT ON COLUMN V_SA_RULE_HEAD_TL.RULE_NAME is 'User defined name for the rule.'
/

