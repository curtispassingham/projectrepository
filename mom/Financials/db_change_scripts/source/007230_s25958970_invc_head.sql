--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'INVC_HEAD'
ALTER TABLE INVC_HEAD MODIFY DUE_DATE DATE NULL
/

COMMENT ON COLUMN INVC_HEAD.DUE_DATE is 'Date the invoice is due to be paid, defaulted according to the payment terms negotiated with the supplier.'
/

