--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_POSUPLD_STATUS'

PROMPT Creating Index 'SVC_POSUPLD_STATUS_I1'
CREATE INDEX SVC_POSUPLD_STATUS_I1 on SVC_POSUPLD_STATUS
  (PROCESS_ID,
   CHUNK_ID,
   LOCATION
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

