--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_TERMS_HEAD_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_TERMS_HEAD_TL'
CREATE TABLE SVC_TERMS_HEAD_TL
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  REVIEWED_IND VARCHAR2(1 ),
  ORIG_LANG_IND VARCHAR2(1 ),
  TERMS_DESC VARCHAR2(240 ),
  TERMS_CODE VARCHAR2(50 ),
  LANG NUMBER(6,0),
  TERMS VARCHAR2(15 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_TERMS_HEAD_TL is 'This is  a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in TERMS_HEAD_TL. '
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.REVIEWED_IND is 'Indicates if the description needs to be reviewed for translation. It is set to ''N'' when the description in the original language is inserted or updated. We assume that clients will regularly run reports on all strings that are not reviewed (i.e. reviewed_ind = ''N''). When translation either provides a new string, or OKs that the existing string is correct, the reviewed_ind should be set to ''Y''.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.ORIG_LANG_IND is 'Indicates if the description is in the original language entered for the freight terms code. It is set to ''Y'' when the first record is written to the table for the freight terms code.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.TERMS_DESC is 'Contains a description of the supplier terms. For example: 2.5% 30 days.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.TERMS_CODE is 'Indicates the Alphanumeric representation of Term Name which acts as the Term code in Oracle Financials.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.LANG is 'Contains the number which uniquely identifies a language.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.TERMS is 'Contains a number uniquely identifying the supplier terms.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_TERMS_HEAD_TL.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_TERMS_HEAD_TL'
ALTER TABLE SVC_TERMS_HEAD_TL
 ADD CONSTRAINT SVC_TERMS_HEAD_TL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_TERMS_HEAD_TL'
ALTER TABLE SVC_TERMS_HEAD_TL
 ADD CONSTRAINT SVC_TERMS_HEAD_TL_UK UNIQUE
  (LANG,
   TERMS
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

