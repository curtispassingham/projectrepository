--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       TRAN_DATA_CODES_REF_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE TRAN_DATA_CODES_REF_TL(
LANG NUMBER(6) NOT NULL,
TRAN_CODE NUMBER(4) NOT NULL,
PGM_NAME VARCHAR2(100) NOT NULL,
REF_NO_1_DESC VARCHAR2(120) ,
REF_NO_2_DESC VARCHAR2(120) ,
GL_REF_NO_DESC VARCHAR2(30) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TRAN_DATA_CODES_REF_TL is 'This is the translation table for TRAN_DATA_CODES_REF table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.TRAN_CODE is 'This field contains the tran code that uniquely identifies the transaction entered in the tran_data table.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.PGM_NAME is 'This field identifies the Oracle Retail module which inserted the record into the transaction data table.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.REF_NO_1_DESC is 'This field contains a description of what is contained  in the REF_NO_1 field on TRAN_DATA for the specified TRAN_CODE/PGM_NAME combination.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.REF_NO_2_DESC is 'This field contains a description of what is contained  in the REF_NO_2  field on TRAN_DATA for the specified TRAN_CODE/PGM_NAME combination.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.GL_REF_NO_DESC is 'This field contains a description of what is contained in the GL_REF_NO field on TRAN_DATA for the specified TRAN_CODE/PGM_NAME combination.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN TRAN_DATA_CODES_REF_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE TRAN_DATA_CODES_REF_TL ADD CONSTRAINT PK_TRAN_DATA_CODES_REF_TL PRIMARY KEY (
LANG,
TRAN_CODE,
PGM_NAME
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE TRAN_DATA_CODES_REF_TL
 ADD CONSTRAINT TDCRT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE TRAN_DATA_CODES_REF_TL ADD CONSTRAINT TDCRT_TDCR_FK FOREIGN KEY (
TRAN_CODE,
PGM_NAME
) REFERENCES TRAN_DATA_CODES_REF (
TRAN_CODE,
PGM_NAME
)
/

