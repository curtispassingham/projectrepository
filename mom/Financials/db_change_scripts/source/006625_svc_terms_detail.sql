--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_TERMS_DETAIL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_TERMS_DETAIL'
CREATE TABLE SVC_TERMS_DETAIL
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  CUTOFF_DAY NUMBER(2,0),
  END_DATE_ACTIVE DATE,
  START_DATE_ACTIVE DATE,
  ENABLED_FLAG VARCHAR2(1 ),
  FIXED_DATE DATE,
  DISC_MM_FWD NUMBER(3,0),
  DISC_DOM NUMBER(2,0),
  PERCENT NUMBER(12,4),
  DISCDAYS NUMBER(3,0),
  DUE_MM_FWD NUMBER(3,0),
  DUE_DOM NUMBER(2,0),
  DUE_MAX_AMOUNT NUMBER(12,4),
  DUEDAYS NUMBER(3,0),
  TERMS_SEQ NUMBER(10,0),
  TERMS VARCHAR2(15 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_TERMS_DETAIL is 'This is  a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in TERMS_DETAIL. '
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.CUTOFF_DAY is 'Day of the month after which Oracle Payables schedules payment using the day after the current month.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.END_DATE_ACTIVE is 'Indicates the date for assigning an inactive date to the Payment Terms. This column will be simply maintained in the table for information and not to be displayed in RMS Forms for Oracle Retail 10.0.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.START_DATE_ACTIVE is 'Indicates the date for assigning an active date to the Payment Terms. This column will be simply maintained in the table for information and not to be displayed in RMS Forms for Oracle Retail 10.0.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.ENABLED_FLAG is 'Indicates whether the Payment terms are valid or invalid within the respective application. The values would be either (Y)es or (N)o. This column will be simply maintained in the table for information and not to be displayed in RMS Forms for Oracle Retail 10.0.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.FIXED_DATE is 'Fixed due date.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.DISC_MM_FWD is 'Number of months ahead to calculate discount date for invoice payment line.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.DISC_DOM is 'Day of month used to calculate discount date for invoice payment line'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.PERCENT is 'Contains the percent of discount if payment is made within the specified time frame.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.DISCDAYS is 'Contains the number of days in which payment must be made in order to receive the discount.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.DUE_MM_FWD is 'Number of months ahead used to calculate due date of invoice payment line.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.DUE_DOM is 'Day of month used to calculate due date of invoice payment line.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.DUE_MAX_AMOUNT is 'Maximum payment amound due by a certain date'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.DUEDAYS is 'Contains the number of days until payment is due.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.TERMS_SEQ is 'Order sequence in which to apply the discount percent.  Used as part of the detail line unique identifier.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.TERMS is 'Contains a number uniquely identifying the supplier terms.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_TERMS_DETAIL.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_TERMS_DETAIL'
ALTER TABLE SVC_TERMS_DETAIL
 ADD CONSTRAINT SVC_TERMS_DETAIL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_TERMS_DETAIL'
ALTER TABLE SVC_TERMS_DETAIL
 ADD CONSTRAINT SVC_TERMS_DETAIL_UK UNIQUE
  (TERMS_SEQ,
   TERMS
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

