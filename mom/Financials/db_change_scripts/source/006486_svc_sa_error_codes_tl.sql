--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_SA_ERROR_CODES_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_SA_ERROR_CODES_TL'
CREATE TABLE SVC_SA_ERROR_CODES_TL
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ),
  LANG NUMBER(6),
  ERROR_CODE VARCHAR2(25 ),
  ERROR_DESC VARCHAR2(255 ),
  REC_SOLUTION VARCHAR2(255 ),
  SHORT_DESC VARCHAR2(40 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_SA_ERROR_CODES_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated to SA_ERROR_CODES_TL'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.LANG is 'This field contains the language in which the translated text is maintained.'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.ERROR_CODE is 'This field contains a unique error code'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.ERROR_DESC is 'This field contains the error description.'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.REC_SOLUTION is 'This field contains the recommended solution for the error.'
/

COMMENT ON COLUMN SVC_SA_ERROR_CODES_TL.SHORT_DESC is 'This field contains the short description of the error code.'
/

