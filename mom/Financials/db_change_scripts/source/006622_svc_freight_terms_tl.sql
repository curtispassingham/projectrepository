--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_FREIGHT_TERMS_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_FREIGHT_TERMS_TL'
CREATE TABLE SVC_FREIGHT_TERMS_TL
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  REVIEWED_IND VARCHAR2(1 ),
  ORIG_LANG_IND VARCHAR2(1 ),
  TERM_DESC VARCHAR2(240 ),
  LANG NUMBER(6,0),
  FREIGHT_TERMS VARCHAR2(30 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_FREIGHT_TERMS_TL is 'This is  a staging table used for Admin API spreadsheet upload process.It is used to   temporarily hold data before it is uploaded/updated in FREIGHT_TERMS_TL. '
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.REVIEWED_IND is 'Indicates if the description needs to be reviewed for translation. It is set to ''N'' when the description in the original language is inserted or updated. We assume that clients will regularly run reports on all strings that are not reviewed (i.e. reviewed_ind = ''N''). When translation either provides a new string, or OKs that the existing string is correct, the reviewed_ind should be set to ''Y''.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.ORIG_LANG_IND is 'Indicates if the description is in the original language entered for the freight terms code. It is set to ''Y'' when the first record is written to the table for the freight terms code.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.TERM_DESC is 'Contains a description of the freight terms used in the system. Examples include a specified percent of total cost, free, or a specified percentage added to the invoice.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.LANG is 'Contains the number which uniquely identifies a language.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.FREIGHT_TERMS is 'Contains a number that uniquely identifies the freight terms.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS_TL.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_FREIGHT_TERMS_TL'
ALTER TABLE SVC_FREIGHT_TERMS_TL
 ADD CONSTRAINT SVC_FREIGHT_TERMS_TL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_FREIGHT_TERMS_TL'
ALTER TABLE SVC_FREIGHT_TERMS_TL
 ADD CONSTRAINT SVC_FREIGHT_TERMS_TL_UK UNIQUE
  (FREIGHT_TERMS,
   LANG
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

