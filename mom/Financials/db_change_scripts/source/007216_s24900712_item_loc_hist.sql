--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ITEM_LOC_HIST
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
-- Creating Table
--------------------------------------
SET SERVEROUTPUT ON

DECLARE

   L_in_dd         NUMBER(2);
   L_in_mm         NUMBER(2);
   L_in_yyyy       NUMBER(4);
   L_out_dd        NUMBER(2);
   L_out_mm        NUMBER(2);
   L_out_yyyy      NUMBER(4);
   L_first_day     DATE := NULL;
   L_error_msg     VARCHAR2(255);
   L_return_code   VARCHAR2(5)   := 'TRUE';
   DATE_FAILED     EXCEPTION;
   L_first_eow_date VARCHAR(17);
   L_query         VARCHAR2(10000);
   L_dynamic_call  VARCHAR2(1000);
   L_exists        VARCHAR2(1) := 'N';
   CURSOR c_calendar is
      select first_day 
        from calendar 
       where rownum = 1 
    order by first_day;
    
CURSOR c_check_bkp is
      select 'Y' 
        from user_objects 
       where object_name = upper('item_loc_hist_bkp'); 
BEGIN

open c_calendar;
fetch c_calendar into L_first_day;
close c_calendar;

open c_check_bkp;
fetch c_check_bkp into L_exists;
close c_check_bkp;
if L_first_day is NOT NULL then
  if L_exists = 'N' then
   --Rename the table
   L_query := 'RENAME item_loc_hist TO item_loc_hist_bkp';
   EXECUTE IMMEDIATE L_query;
   
   
   -- Drop all the constraints on the backed up table
   L_query := 'ALTER TABLE ITEM_LOC_HIST_BKP DROP CONSTRAINT CHK_ITEM_HIST_SALES_TYPE';
   EXECUTE IMMEDIATE L_query;   
   L_query := 'ALTER TABLE ITEM_LOC_HIST_BKP DROP CONSTRAINT CHK_ITEM_LOC_HIST_LOC_TYPE';
   EXECUTE IMMEDIATE L_query;      
   L_query := 'ALTER TABLE ITEM_LOC_HIST_BKP DROP CONSTRAINT PK_ITEM_LOC_HIST DROP INDEX';
   EXECUTE IMMEDIATE L_query;   
 end if;
 
 L_in_dd   := TO_NUMBER(TO_CHAR(L_first_day,'DD'),'09');
   L_in_mm   := TO_NUMBER(TO_CHAR(L_first_day,'MM'),'09');
   L_in_yyyy := TO_NUMBER(TO_CHAR(L_first_day,'YYYY'),'0999');
   
   L_dynamic_call := 'BEGIN CAL_TO_454_LDOW(:L_in_dd,:L_in_mm,:L_in_yyyy,:L_out_dd,:L_out_mm,:L_out_yyyy,:L_return_code,:L_error_msg); END;';
   EXECUTE IMMEDIATE L_dynamic_call using in L_in_dd, 
                                          in L_in_mm, 
                                          in L_in_yyyy, 
                                          in out L_out_dd,
                                          in out L_out_mm,
                                          in out L_out_yyyy,
                                          in out L_return_code, 
                                          in out L_error_msg;

   if L_return_code = 'N' then
      raise DATE_FAILED;
   end if;   

   L_first_eow_date := TO_CHAR(L_out_dd,'09')||TO_CHAR(L_out_mm,'09')
                                 ||TO_CHAR(L_out_yyyy,'0999')||'000000';
                                 
-- Create table with interval partition with a min partition based on the first EOW date and no constrants or indexes
   L_query :=        'CREATE TABLE  ITEM_LOC_HIST(' ||
                     'ITEM                 VARCHAR2(25 BYTE),' ||
                     'LOC                  NUMBER(10,0),' ||
                     'LOC_TYPE             VARCHAR2(1 BYTE),' ||
                     'EOW_DATE             DATE,' ||
                     'WEEK_454             NUMBER(2,0),' ||
                     'MONTH_454            NUMBER(2,0),' ||
                     'YEAR_454             NUMBER(4,0),' ||
                     'SALES_TYPE           VARCHAR2(1 BYTE),' ||
                     'SALES_ISSUES         NUMBER(12,4),' ||
                     'VALUE                NUMBER(20,4),' ||
                     'GP                   NUMBER(20,4),' ||
                     'STOCK                NUMBER(12,4),' ||
                     'RETAIL               NUMBER(20,4),' ||
                     'AV_COST              NUMBER(20,4),' ||
                     'CREATE_DATETIME      DATE,' ||
                     'LAST_UPDATE_DATETIME DATE,' ||
                     'LAST_UPDATE_ID       VARCHAR2(30 BYTE),' ||
                     'DEPT                 NUMBER(4,0),' ||
                     'CLASS                NUMBER(4,0),' ||
                     'SUBCLASS             NUMBER(4,0)) ' ||
   'INITRANS 12 TABLESPACE RETAIL_DATA NOLOGGING ' ||
   'PARTITION BY RANGE(EOW_DATE) ' ||
   'INTERVAL(NUMTODSINTERVAL(7,''DAY'')) ' ||
   'SUBPARTITION BY HASH(LOC) ' ||
   'SUBPARTITION TEMPLATE' ||
   ' (SUBPARTITION S001 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S002 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S003 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S004 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S005 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S006 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S007 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S008 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S009 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S010 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S011 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S012 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S013 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S014 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S015 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S016 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S017 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S018 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S019 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S020 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S021 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S022 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S023 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S024 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S025 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S026 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S027 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S028 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S029 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S030 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S031 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S032 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S033 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S034 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S035 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S036 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S037 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S038 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S039 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S040 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S041 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S042 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S043 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S044 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S045 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S046 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S047 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S048 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S049 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S050 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S051 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S052 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S053 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S054 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S055 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S056 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S057 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S058 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S059 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S060 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S061 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S062 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S063 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S064 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S065 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S066 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S067 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S068 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S069 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S070 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S071 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S072 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S073 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S074 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S075 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S076 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S077 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S078 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S079 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S080 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S081 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S082 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S083 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S084 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S085 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S086 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S087 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S088 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S089 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S090 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S091 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S092 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S093 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S094 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S095 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S096 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S097 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S098 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S099 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S100 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S101 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S102 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S103 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S104 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S105 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S106 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S107 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S108 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S109 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S110 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S111 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S112 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S113 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S114 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S115 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S116 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S117 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S118 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S119 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S120 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S121 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S122 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S123 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S124 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S125 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S126 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S127 TABLESPACE RETAIL_DATA ,' ||
   '  SUBPARTITION S128 TABLESPACE RETAIL_DATA)' ||
   '(PARTITION ITEM_LOC_HIST_MIN VALUES LESS THAN (TO_DATE('''||L_first_eow_date||''','' DD MM YYYYHH24MISS'',''NLS_CALENDAR=GREGORIAN'')) INITRANS 12 TABLESPACE RETAIL_DATA)'; 
   
   EXECUTE IMMEDIATE L_query;
   
   --Insert data from backup table to new table. Commit after each partition.
   for rec in (select partition_name
                from user_tab_partitions 
              where table_name = 'ITEM_LOC_HIST_BKP')
   loop
      EXECUTE IMMEDIATE 'insert /*+ append */ into ITEM_LOC_HIST(ITEM,LOC,LOC_TYPE,EOW_DATE,WEEK_454,MONTH_454,YEAR_454,SALES_TYPE,SALES_ISSUES,VALUE,GP,STOCK,RETAIL,AV_COST,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID,DEPT,CLASS,SUBCLASS)' ||
                        'select i.ITEM,i.LOC,i.LOC_TYPE,i.EOW_DATE,i.WEEK_454,i.MONTH_454,i.YEAR_454,i.SALES_TYPE,i.SALES_ISSUES,i.VALUE,i.GP,i.STOCK,i.RETAIL,i.AV_COST,i.CREATE_DATETIME,SYSDATE,i.LAST_UPDATE_ID,im.DEPT,im.CLASS,im.SUBCLASS' ||
                        ' from item_loc_hist_bkp PARTITION('||rec.partition_name||') i, item_master im where im.item = i.item';
                       
      COMMIT;                        
   end loop;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST LOGGING';
   EXECUTE IMMEDIATE L_query;
   
   --Enable and create the constraints and indexes.
   -- Create the constraints with ENABLE NOVALIDATE option. This will not validate the existing data as it is assumed that data would be clean from the backup table.
   L_query := 'ALTER TABLE  ITEM_LOC_HIST ADD (CONSTRAINT CHK_ITEM_HIST_SALES_TYPE CHECK (SALES_TYPE IN (''R'', ''P'', ''C'', ''I'')) ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE  ITEM_LOC_HIST ADD (CONSTRAINT CHK_ITEM_LOC_HIST_LOC_TYPE CHECK (LOC_TYPE IN (''S'', ''W'')) ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE  ITEM_LOC_HIST ADD (CONSTRAINT PK_ITEM_LOC_HIST PRIMARY KEY (ITEM, SALES_TYPE, LOC, EOW_DATE) USING INDEX INITRANS 24 TABLESPACE RETAIL_INDEX ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST  MODIFY (ITEM NOT NULL ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST  MODIFY (LOC NOT NULL ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST  MODIFY (LOC_TYPE NOT NULL ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST  MODIFY (EOW_DATE NOT NULL ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST  MODIFY (SALES_TYPE NOT NULL ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST  MODIFY (CREATE_DATETIME NOT NULL ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST  MODIFY (LAST_UPDATE_DATETIME NOT NULL ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'ALTER TABLE ITEM_LOC_HIST  MODIFY (LAST_UPDATE_ID NOT NULL ENABLE NOVALIDATE)';
   EXECUTE IMMEDIATE L_query;
   
                                  
   -- Create the indexes                               
   L_query := 'DROP INDEX ITEM_LOC_HIST_I1';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'DROP INDEX ITEM_LOC_HIST_I2';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'CREATE INDEX ITEM_LOC_HIST_I1 ON ITEM_LOC_HIST (ITEM,EOW_DATE) INITRANS 24 TABLESPACE RETAIL_INDEX';
   EXECUTE IMMEDIATE L_query;
   
   L_query := 'CREATE INDEX ITEM_LOC_HIST_I2 ON ITEM_LOC_HIST (LOC) INITRANS 24 TABLESPACE RETAIL_INDEX';
   EXECUTE IMMEDIATE L_query;
         
   L_query := 'DROP TABLE item_loc_hist_bkp'; 
   EXECUTE IMMEDIATE L_query;
   
else
   
   -- This is during full install when there is no data on the RMS instance. The interval partition would have been created with the partition.ksh script.
   L_query := 'ALTER TABLE ITEM_LOC_HIST ADD (DEPT NUMBER(4,0),CLASS NUMBER(4,0),SUBCLASS NUMBER(4,0))';
   EXECUTE IMMEDIATE L_query;
   
end if;
   
EXCEPTION

   when DATE_FAILED then
      dbms_output.put_line(L_error_msg);
      
   when OTHERS then
     dbms_output.put_line('exception: '||SQLCODE||', '||SQLERRM);
END;   
/



   COMMENT ON COLUMN  ITEM_LOC_HIST.ITEM
   IS
   'This field contains a unique alphanumeric value that identifies the item.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.LOC
   IS
   'This field contains the unique identifier for the location in which the transactions occurred. This may contain a store or a warehouse.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.LOC_TYPE
   IS
   'This field specifies the type of location in the location field.  Valid values are:   S = Store   W = Warehouse';
   COMMENT ON COLUMN  ITEM_LOC_HIST.EOW_DATE
   IS
   'This field contains the end of the week date of the week for the sales total and other calculations.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.WEEK_454
   IS
   'This field contain the identifer of the week in which the transactions occurred based on the 454 calendar.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.DEPT
   IS
   'This field contains the department of the item.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.CLASS
   IS
   'This field contains the class of the item.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.SUBCLASS
   IS
   'This field contains the subclass of the item.';   
   COMMENT ON COLUMN  ITEM_LOC_HIST.MONTH_454
   IS
   'This field contain the identifer of the month in which the transactions occurred based on the 454 calendar.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.YEAR_454
   IS
   'This field contain the identifer of the year in which the transactions occurred based on the 454 calendar.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.SALES_TYPE
   IS
   'This field indicates the type of sale or inventory movement.  Valid Values are: R = Regular Sales, P = Promotional Sales, C = Clearance Sales, I = Issues from Warehouses to Stores.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.SALES_ISSUES
   IS
   'This fields contains the number of units sold or transferred for item/location/week/sales type combination.  If the location type is a store, then this value represents sales.  If the location type is a warehouse, then this value represents outbound transfer/allocation transactions to stores.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.VALUE
   IS
   'This field contains the retail value of the sales for the item/location/week/sales type combination.  This field is stored in the local currency.  This field will only hold a value for stores.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.GP
   IS
   'This field contains the total gross profit calculated for the item/location/week/sales type.  This is updated for each sale that occurs during the week and is calculated as (sales retail - cost of sales). This field will only hold a value for stores.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.STOCK
   IS
   'This field contains the stock on hand for the item/location combination at the end of the week.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.RETAIL
   IS
   'This field contains the unit retail price of the item at the location at the end of the week.  This field is stored in the local currency.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.AV_COST
   IS
   'This field contains the average cost of the item at the location at the end of the week.  This field is stored in the local currency.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.CREATE_DATETIME
   IS
   'This field contains the date and time when the record was created.  This date/time will be used in export processing.  This value is populated on insert  and never be updated.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.LAST_UPDATE_DATETIME
   IS
   'This field holds the date time stamp of the most recent update by the last_update_id.';
   COMMENT ON COLUMN  ITEM_LOC_HIST.LAST_UPDATE_ID
   IS
   'This field holds the Oracle user ID of the user who most recently updated this record.';
   
COMMENT ON TABLE ITEM_LOC_HIST IS 'This table contains one row for each item/location/week/sales type combination.  Sales history, forecast and plan information about each combination may be held here.'
/   