--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

UPDATE SA_ERROR_CODES_TL SET SHORT_DESC = SUBSTRB(ERROR_DESC, 1,40) WHERE SHORT_DESC IS NULL
/

COMMIT
/

ALTER TABLE SA_ERROR_CODES_TL MODIFY SHORT_DESC VARCHAR2(40) NOT NULL
/

COMMENT ON COLUMN SA_ERROR_CODES_TL.SHORT_DESC is 'This field contains the short error description.'
/
 
