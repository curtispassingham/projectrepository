CREATE OR REPLACE FORCE VIEW V_TRAN_DATA_CODES_REF_TL (TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, LANG ) AS
SELECT  b.tran_code,
        b.pgm_name,
        case when tl.lang is not null then tl.ref_no_1_desc else b.ref_no_1_desc end ref_no_1_desc,
        case when tl.lang is not null then tl.ref_no_2_desc else b.ref_no_2_desc end ref_no_2_desc,
        case when tl.lang is not null then tl.gl_ref_no_desc else b.gl_ref_no_desc end gl_ref_no_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  TRAN_DATA_CODES_REF b,
        TRAN_DATA_CODES_REF_TL tl
 WHERE  b.tran_code = tl.tran_code (+)
   AND  b.pgm_name = tl.pgm_name (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_TRAN_DATA_CODES_REF_TL is 'This is the translation view for base table TRAN_DATA_CODES_REF. This view fetches data in user langauge either from translation table TRAN_DATA_CODES_REF_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_REF_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_REF_TL.TRAN_CODE is 'This field contains the tran code that uniquely identifies the transaction entered in the tran_data table.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_REF_TL.PGM_NAME is 'This field identifies the Oracle Retail module which inserted the record into the transaction data table.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_REF_TL.REF_NO_1_DESC is 'This field contains a description of what is contained  in the REF_NO_1 field on TRAN_DATA for the specified TRAN_CODE/PGM_NAME combination.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_REF_TL.REF_NO_2_DESC is 'This field contains a description of what is contained  in the REF_NO_2  field on TRAN_DATA for the specified TRAN_CODE/PGM_NAME combination.'
/

COMMENT ON COLUMN V_TRAN_DATA_CODES_REF_TL.GL_REF_NO_DESC is 'This field contains a description of what is contained in the GL_REF_NO field on TRAN_DATA for the specified TRAN_CODE/PGM_NAME combination.'
/

