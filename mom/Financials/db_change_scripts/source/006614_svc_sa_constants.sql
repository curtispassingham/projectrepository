--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_SA_CONSTANTS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_SA_CONSTANTS'
CREATE TABLE SVC_SA_CONSTANTS
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  DELETE_IND VARCHAR2(1 ),
  VALUE_DATA_TYPE VARCHAR2(6 ),
  CONSTANT_VALUE VARCHAR2(14 ),
  CONSTANT_NAME VARCHAR2(250 ),
  CONSTANT_ID VARCHAR2(30 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_SA_CONSTANTS is 'This is   a staging table used for Admin API spreadsheet upload process.It is used to   temporarily hold data before it is uploaded/updated in SA_CONSTANTS. '
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.DELETE_IND is 'This field contains whether or not the constant can be deleted.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.VALUE_DATA_TYPE is 'This field contains the data type of the constant value.  Valid values are on the code tables with a code type of DTTP.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.CONSTANT_VALUE is 'This field contains the value of the constant.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.CONSTANT_NAME is 'This field contains a name for the constant.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.CONSTANT_ID is 'This field contains a unique identifier for the constant.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_SA_CONSTANTS.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_SA_CONSTANTS'
ALTER TABLE SVC_SA_CONSTANTS
 ADD CONSTRAINT SVC_SA_CONSTANTS_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_SA_CONSTANTS'
ALTER TABLE SVC_SA_CONSTANTS
 ADD CONSTRAINT SVC_SA_CONSTANTS_UK UNIQUE
  (CONSTANT_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

