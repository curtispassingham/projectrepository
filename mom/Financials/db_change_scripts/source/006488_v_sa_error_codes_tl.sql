--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW Modified:          V_SA_ERROR_CODES_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       MODIFYING VIEW
--------------------------------------
CREATE OR REPLACE FORCE VIEW V_SA_ERROR_CODES_TL (ERROR_CODE, ERROR_DESC, REC_SOLUTION, SHORT_DESC, LANG ) AS
SELECT  b.error_code,
        case when tl.lang is not null then tl.error_desc else b.error_desc end error_desc,
        case when tl.lang is not null then tl.rec_solution else b.rec_solution end rec_solution,
        case when tl.lang is not null then tl.short_desc else b.short_desc end short_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SA_ERROR_CODES b,
        SA_ERROR_CODES_TL tl
 WHERE  b.error_code = tl.error_code (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SA_ERROR_CODES_TL is 'This is the translation view for base table SA_ERROR_CODES. This view fetches data in user langauge either from translation table SA_ERROR_CODES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SA_ERROR_CODES_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SA_ERROR_CODES_TL.ERROR_CODE is 'This field contains a unique error code.'
/

COMMENT ON COLUMN V_SA_ERROR_CODES_TL.ERROR_DESC is 'This field contains the error description.'
/

COMMENT ON COLUMN V_SA_ERROR_CODES_TL.REC_SOLUTION is 'This field contains the recommended solution for the error.'
/

COMMENT ON COLUMN V_SA_ERROR_CODES_TL.SHORT_DESC is 'This field contains the short error description.'
/
