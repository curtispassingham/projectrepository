--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Tablespace to ENCRYPTED_RETAIL_DATA               
--------------------------------------

PROMPT Modifying Table 'SA_CUSTOMER'
PROMPT Modifying TABLE/INDEXES SUBPARTITION and PARTITION Tablespaces to  'ENCRYPTED_RETAIL_DATA' and 'ENCRYPTED_RETAIL_INDEX'
declare
tablespace_move1 varchar(1000);
tablespace_move2 varchar(1000);
tablespace_move3 varchar(1000);
tablespace_move4 varchar(1000);
begin
	for SA1 in (select subpartition_name
					from user_tab_subpartitions
					where
						table_name = 'SA_CUSTOMER' and
						tablespace_name = 'RETAIL_DATA')
		loop
			tablespace_move1:='ALTER TABLE SA_CUSTOMER MOVE SUBPARTITION '||SA1.subpartition_name||' TABLESPACE ENCRYPTED_RETAIL_DATA update indexes';
			execute immediate tablespace_move1;
		end loop;
					
	for SA2 in (select SUBPARTITION_NAME
					from user_ind_subpartitions
					where
						index_name = 'PK_SA_CUSTOMER') 
		loop
			tablespace_move2:='ALTER INDEX PK_SA_CUSTOMER REBUILD subpartition '||SA2.subpartition_name||' TABLESPACE ENCRYPTED_RETAIL_INDEX';
			execute immediate tablespace_move2;
		end loop;
				
	for SA3 in (select partition_name
					from user_tab_partitions
					where
						table_name = 'SA_CUSTOMER' and
						tablespace_name = 'RETAIL_DATA')
		loop
			tablespace_move3:='ALTER TABLE SA_CUSTOMER MODIFY DEFAULT ATTRIBUTES FOR PARTITION '||SA3.partition_name||' TABLESPACE ENCRYPTED_RETAIL_DATA';
			execute immediate tablespace_move3;   
		end loop;
	
	for SA4 in (select PARTITION_NAME
					from user_ind_partitions
					where
						index_name = 'PK_SA_CUSTOMER') 
		loop
			tablespace_move4:='ALTER INDEX PK_SA_CUSTOMER MODIFY DEFAULT ATTRIBUTES FOR PARTITION '||SA4.partition_name||' TABLESPACE ENCRYPTED_RETAIL_INDEX';         
			execute immediate tablespace_move4;
		end loop;		
end;
/

PROMPT Modifying SUBPARTITION TEMPLATE to have Tablespace 'ENCRYPTED_RETAIL_DATA'
DECLARE
L_template_exists number := 0;
BEGIN
	SELECT count(*) INTO L_template_exists
		from USER_SUBPARTITION_TEMPLATES
					where
						TABLE_NAME = 'SA_CUSTOMER';
	
	if (L_template_exists != 0) then
		declare
		l_main_stmt varchar2(32000):='ALTER TABLE SA_CUSTOMER SET SUBPARTITION TEMPLATE(';
		begin
			for SA5 in (select SUBPARTITION_NAME from USER_SUBPARTITION_TEMPLATES where TABLE_NAME = 'SA_CUSTOMER') 
			loop
			l_main_stmt:=l_main_stmt||chr(10)||'SUBPARTITION '||SA5.subpartition_name||' TABLESPACE ENCRYPTED_RETAIL_DATA'||',';
			end loop;
				l_main_stmt:=substr(l_main_stmt,1,length(l_main_stmt)-1)||')';
				execute immediate l_main_stmt;
		end;
		end if;
end;
/

PROMPT Modifying Tablespace for Table 'SA_CUSTOMER'
ALTER TABLE SA_CUSTOMER MODIFY DEFAULT ATTRIBUTES TABLESPACE ENCRYPTED_RETAIL_DATA
/

PROMPT Modifying Tablespace for INDEX 'PK_SA_CUSTOMER'
ALTER INDEX PK_SA_CUSTOMER MODIFY DEFAULT ATTRIBUTES TABLESPACE ENCRYPTED_RETAIL_INDEX
/