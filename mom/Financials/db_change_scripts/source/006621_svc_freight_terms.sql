--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_FREIGHT_TERMS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_FREIGHT_TERMS'
CREATE TABLE SVC_FREIGHT_TERMS
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  ENABLED_FLAG VARCHAR2(1 ),
  END_DATE_ACTIVE DATE,
  START_DATE_ACTIVE DATE,
  FREIGHT_TERMS VARCHAR2(30 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_FREIGHT_TERMS is 'This is  a staging table used for Admin API spreadsheet upload process.It is used to   temporarily hold data before it is uploaded/updated in SVC_FREIGHT_TERMS. '
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.ENABLED_FLAG is 'Indicates whether the freight terms are valid or invalid within the respective application. The values would be either (Y)es or (N)o.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.END_DATE_ACTIVE is 'Indicates the date for assigning an inactive date to the Freight Terms.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.START_DATE_ACTIVE is 'Indicates the date for assigning an active date to the Freight Terms.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.FREIGHT_TERMS is 'Contains a number that uniquely identifies the freight terms.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_FREIGHT_TERMS.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_FREIGHT_TERMS'
ALTER TABLE SVC_FREIGHT_TERMS
 ADD CONSTRAINT SVC_FREIGHT_TERMS_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_FREIGHT_TERMS'
ALTER TABLE SVC_FREIGHT_TERMS
 ADD CONSTRAINT SVC_FREIGHT_TERMS_UK UNIQUE
  (FREIGHT_TERMS
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

