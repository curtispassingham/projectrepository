--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'POS_PROD_REST_HEAD'

PROMPT MODIFYING CONSTRAINT 'CHK_POS_PROD_REST_H_TNDR_TYPE'
ALTER TABLE POS_PROD_REST_HEAD DROP CONSTRAINT  CHK_POS_PROD_REST_H_TNDR_TYPE
/
ALTER TABLE POS_PROD_REST_HEAD ADD CONSTRAINT
 CHK_POS_PROD_REST_H_TNDR_TYPE CHECK (TENDER_TYPE_GROUP IN ('CASH','CHECK','CCARD', 'COUPON', 'DCARD', 'ERR','TERM','VOUCH','MORDER','DRIVEO','SOCASS', 'PAYPAL', 'FONCOT'))
/
