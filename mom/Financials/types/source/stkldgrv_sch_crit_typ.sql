CREATE OR REPLACE type STKLDGRV_SCH_CRIT_TYP
AS
  object
  ( 
    dept           NUMBER(4),
    class          NUMBER(4),
    subclass       NUMBER(4),
    currency       VARCHAR2(1),
    loc_type       VARCHAR2(1),
    location       NUMBER(10),
    mwd_view       VARCHAR2(6),
    period_no      VARCHAR2(30),
    period_date    DATE,
    template       VARCHAR2(6) );
/