--TRAN_DATA_REC

DROP type TRAN_DATA_REC FORCE
/

CREATE OR REPLACE TYPE TRAN_DATA_REC AS OBJECT
(
 ITEM                  VARCHAR2(25),
 DEPT                  NUMBER(4),
 CLASS                 NUMBER(4),
 SUBCLASS              NUMBER(4),
 PACK_IND              VARCHAR2(1),
 LOC_TYPE              VARCHAR2(1),
 LOCATION              NUMBER(10),
 TRAN_DATE             DATE,
 TRAN_CODE             NUMBER(4),
 ADJ_CODE              VARCHAR2(1),
 UNITS                 NUMBER(12,4),
 TOTAL_COST            NUMBER(20,4),
 TOTAL_RETAIL          NUMBER(20,4),
 REF_NO_1              NUMBER(12),
 REF_NO_2              NUMBER(12),
 GL_REF_NO             VARCHAR2(25),
 OLD_UNIT_RETAIL       NUMBER(20,4),
 NEW_UNIT_RETAIL       NUMBER(20,4),
 PGM_NAME              VARCHAR2(100),
 SALES_TYPE            VARCHAR2(1),
 VAT_RATE              NUMBER(12,4),
 AV_COST               NUMBER(20,4),
 TIMESTAMP             DATE,
 REF_PACK_NO           VARCHAR2(25),
 TOTAL_COST_EXCL_ELC   NUMBER(20,4),
 CONSTRUCTOR FUNCTION TRAN_DATA_REC 
 ( 
 ITEM                  VARCHAR2     DEFAULT NULL,
 DEPT                  NUMBER       DEFAULT NULL,
 CLASS                 NUMBER       DEFAULT NULL,
 SUBCLASS              NUMBER       DEFAULT NULL,
 PACK_IND              VARCHAR2     DEFAULT NULL,
 LOC_TYPE              VARCHAR2     DEFAULT NULL,
 LOCATION              NUMBER       DEFAULT NULL,
 TRAN_DATE             DATE         DEFAULT NULL,
 TRAN_CODE             NUMBER       DEFAULT NULL,
 ADJ_CODE              VARCHAR2     DEFAULT NULL,
 UNITS                 NUMBER       DEFAULT NULL,
 TOTAL_COST            NUMBER       DEFAULT NULL,
 TOTAL_RETAIL          NUMBER       DEFAULT NULL,
 REF_NO_1              NUMBER       DEFAULT NULL,
 REF_NO_2              NUMBER       DEFAULT NULL,
 GL_REF_NO             VARCHAR2     DEFAULT NULL,
 OLD_UNIT_RETAIL       NUMBER       DEFAULT NULL,
 NEW_UNIT_RETAIL       NUMBER       DEFAULT NULL,
 PGM_NAME              VARCHAR2     DEFAULT NULL,
 SALES_TYPE            VARCHAR2     DEFAULT NULL,
 VAT_RATE              NUMBER       DEFAULT NULL,
 AV_COST               NUMBER       DEFAULT NULL,
 TIMESTAMP             DATE         DEFAULT NULL,
 REF_PACK_NO           VARCHAR2     DEFAULT NULL,
 TOTAL_COST_EXCL_ELC   NUMBER       DEFAULT NULL
 ) RETURN SELF AS RESULT
)
/
 CREATE OR REPLACE TYPE BODY TRAN_DATA_REC AS
 CONSTRUCTOR FUNCTION TRAN_DATA_REC 
 ( 
 
   ITEM                  VARCHAR2     DEFAULT NULL,
   DEPT                  NUMBER       DEFAULT NULL,
   CLASS                 NUMBER       DEFAULT NULL,
   SUBCLASS              NUMBER       DEFAULT NULL,
   PACK_IND              VARCHAR2     DEFAULT NULL,
   LOC_TYPE              VARCHAR2     DEFAULT NULL,
   LOCATION              NUMBER       DEFAULT NULL,
   TRAN_DATE             DATE         DEFAULT NULL,
   TRAN_CODE             NUMBER       DEFAULT NULL,
   ADJ_CODE              VARCHAR2     DEFAULT NULL,
   UNITS                 NUMBER       DEFAULT NULL,
   TOTAL_COST            NUMBER       DEFAULT NULL,
   TOTAL_RETAIL          NUMBER       DEFAULT NULL,
   REF_NO_1              NUMBER       DEFAULT NULL,
   REF_NO_2              NUMBER       DEFAULT NULL,
   GL_REF_NO             VARCHAR2     DEFAULT NULL,
   OLD_UNIT_RETAIL       NUMBER       DEFAULT NULL,
   NEW_UNIT_RETAIL       NUMBER       DEFAULT NULL,
   PGM_NAME              VARCHAR2     DEFAULT NULL,
   SALES_TYPE            VARCHAR2     DEFAULT NULL,
   VAT_RATE              NUMBER       DEFAULT NULL,
   AV_COST               NUMBER       DEFAULT NULL,
   TIMESTAMP             DATE         DEFAULT NULL,
   REF_PACK_NO           VARCHAR2     DEFAULT NULL,
   TOTAL_COST_EXCL_ELC   NUMBER       DEFAULT NULL
  ) RETURN SELF AS RESULT IS
    BEGIN  
        SELF.ITEM                  := ITEM;
        SELF.DEPT                  := DEPT;
        SELF.CLASS                 := CLASS;
        SELF.SUBCLASS              := SUBCLASS;
        SELF.PACK_IND              := PACK_IND;
        SELF.LOC_TYPE              := LOC_TYPE;
        SELF.LOCATION              := LOCATION;
        SELF.TRAN_DATE             := TRAN_DATE;
        SELF.TRAN_CODE             := TRAN_CODE;
        SELF.ADJ_CODE              := ADJ_CODE;
        SELF.UNITS                 := UNITS;
        SELF.TOTAL_COST            := TOTAL_COST;
        SELF.TOTAL_RETAIL          := TOTAL_RETAIL;
        SELF.REF_NO_1              := REF_NO_1;
        SELF.REF_NO_2              := REF_NO_2;
        SELF.GL_REF_NO             := GL_REF_NO;
        SELF.OLD_UNIT_RETAIL       := OLD_UNIT_RETAIL;
        SELF.NEW_UNIT_RETAIL       := NEW_UNIT_RETAIL;
        SELF.PGM_NAME              := PGM_NAME;
        SELF.SALES_TYPE            := SALES_TYPE;
        SELF.VAT_RATE              := VAT_RATE;
        SELF.AV_COST               := AV_COST;
        SELF.TIMESTAMP             := TIMESTAMP;
        SELF.REF_PACK_NO           := REF_PACK_NO;
        SELF.TOTAL_COST_EXCL_ELC   := TOTAL_COST_EXCL_ELC;
        RETURN;
    END;  
END;
/
         