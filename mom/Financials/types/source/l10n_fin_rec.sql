
--L10N_FIN_REC

DROP type L10N_FIN_REC FORCE
/

CREATE OR REPLACE TYPE L10N_FIN_REC UNDER L10N_OBJ
(
 TRAN_DATA              TRAN_DATA_REC,
 GROSS_COST             NUMBER(20,4),
 QTY                    NUMBER(12,4),
 PACK_QTY               NUMBER(12,4),
 CALC_FLD_TYPE          VARCHAR2(10),
 AVERAGE_WEIGHT         NUMBER(12,4),
 FIN_ROWID              VARCHAR2(18),
 BASE_COST              NUMBER(20,4),
 TOTAL_RETAIL           NUMBER(20,4),
 CONSTRUCTOR FUNCTION L10N_FIN_REC 
 (
    PROCEDURE_KEY                 VARCHAR2        DEFAULT NULL,
    COUNTRY_ID                    VARCHAR2        DEFAULT NULL,
    DOC_TYPE                      VARCHAR2        DEFAULT NULL,
    DOC_ID                        NUMBER          DEFAULT NULL,
    SOURCE_ENTITY                 VARCHAR2        DEFAULT NULL,
    SOURCE_TYPE                   VARCHAR2        DEFAULT NULL,
    SOURCE_ID                     VARCHAR2        DEFAULT NULL,
    DEST_ENTITY                   VARCHAR2        DEFAULT NULL,
    DEST_TYPE                     VARCHAR2        DEFAULT NULL,
    DEST_ID                       VARCHAR2        DEFAULT NULL,
    DEPT                          NUMBER          DEFAULT NULL,
    CLASS                         NUMBER          DEFAULT NULL,
    SUBCLASS                      NUMBER          DEFAULT NULL,
    ITEM                          VARCHAR2        DEFAULT NULL,
    AV_COST                       NUMBER          DEFAULT NULL,
    UNIT_COST                     NUMBER          DEFAULT NULL,
    UNIT_RETAIL                   NUMBER          DEFAULT NULL,
    STOCK_ON_HAND                 NUMBER          DEFAULT NULL,
    TRAN_DATA                     TRAN_DATA_REC   DEFAULT NULL,
    GROSS_COST                    NUMBER          DEFAULT NULL,
    QTY                           NUMBER          DEFAULT NULL,
    PACK_QTY                      NUMBER          DEFAULT NULL,
    CALC_FLD_TYPE                 VARCHAR2        DEFAULT NULL,
    AVERAGE_WEIGHT                NUMBER          DEFAULT NULL,
    FIN_ROWID                     VARCHAR2        DEFAULT NULL,
    BASE_COST                     NUMBER          DEFAULT NULL,
    TOTAL_RETAIL                  NUMBER          DEFAULT NULL
 ) RETURN SELF AS RESULT
) NOT FINAL
/
CREATE OR REPLACE TYPE BODY L10N_FIN_REC AS
 CONSTRUCTOR FUNCTION L10N_FIN_REC 
 (
      PROCEDURE_KEY             VARCHAR2        DEFAULT NULL,
      COUNTRY_ID                VARCHAR2        DEFAULT NULL,
      DOC_TYPE                  VARCHAR2        DEFAULT NULL,
      DOC_ID                    NUMBER          DEFAULT NULL,
      SOURCE_ENTITY             VARCHAR2        DEFAULT NULL,
      SOURCE_TYPE               VARCHAR2        DEFAULT NULL,
      SOURCE_ID                 VARCHAR2        DEFAULT NULL,
      DEST_ENTITY               VARCHAR2        DEFAULT NULL,
      DEST_TYPE                 VARCHAR2        DEFAULT NULL,
      DEST_ID                   VARCHAR2        DEFAULT NULL,
      DEPT                      NUMBER          DEFAULT NULL,
      CLASS                     NUMBER          DEFAULT NULL,
      SUBCLASS                  NUMBER          DEFAULT NULL,
      ITEM                      VARCHAR2        DEFAULT NULL,
      AV_COST                   NUMBER          DEFAULT NULL,
      UNIT_COST                 NUMBER          DEFAULT NULL,
      UNIT_RETAIL               NUMBER          DEFAULT NULL,
      STOCK_ON_HAND             NUMBER          DEFAULT NULL,
      TRAN_DATA                 TRAN_DATA_REC   DEFAULT NULL,
      GROSS_COST                NUMBER          DEFAULT NULL,
      QTY                       NUMBER          DEFAULT NULL,
      PACK_QTY                  NUMBER          DEFAULT NULL,
      CALC_FLD_TYPE             VARCHAR2        DEFAULT NULL,
      AVERAGE_WEIGHT            NUMBER          DEFAULT NULL,
      FIN_ROWID                 VARCHAR2        DEFAULT NULL,
      BASE_COST                 NUMBER          DEFAULT NULL,
      TOTAL_RETAIL              NUMBER          DEFAULT NULL
 ) RETURN SELF AS RESULT IS
    BEGIN
        SELF.PROCEDURE_KEY := PROCEDURE_KEY;
        SELF.COUNTRY_ID := COUNTRY_ID;
        SELF.DOC_TYPE := DOC_TYPE;
        SELF.DOC_ID := DOC_ID;
        SELF.SOURCE_ENTITY := SOURCE_ENTITY;
        SELF.SOURCE_TYPE := SOURCE_TYPE;
        SELF.SOURCE_ID := SOURCE_ID;
        SELF.DEST_ENTITY := DEST_ENTITY;
        SELF.DEST_TYPE := DEST_TYPE;
        SELF.DEST_ID := DEST_ID;
        SELF.DEPT := DEPT;
        SELF.CLASS := CLASS;
        SELF.SUBCLASS := SUBCLASS;
        SELF.ITEM := ITEM;
        SELF.AV_COST := AV_COST;
        SELF.UNIT_COST := UNIT_COST;
        SELF.UNIT_RETAIL := UNIT_RETAIL;
        SELF.STOCK_ON_HAND := STOCK_ON_HAND;
        SELF.TRAN_DATA := TRAN_DATA;
        SELF.GROSS_COST := GROSS_COST;
        SELF.QTY := QTY;
        SELF.PACK_QTY := PACK_QTY;
        SELF.CALC_FLD_TYPE := CALC_FLD_TYPE;
        SELF.AVERAGE_WEIGHT := AVERAGE_WEIGHT;
        SELF.FIN_ROWID:=FIN_ROWID;
        SELF.BASE_COST:=BASE_COST;
        SELF.TOTAL_RETAIL:=TOTAL_RETAIL;
        RETURN;
    END;
END;
/
