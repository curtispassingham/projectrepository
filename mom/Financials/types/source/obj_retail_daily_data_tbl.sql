----------------------------------------------------------------------------
-- OBJECT CREATED :                    OBJ_RETAIL_DAILY_DATA_TBL 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_RETAIL_DAILY_DATA_TBL

DROP TYPE OBJ_RETAIL_DAILY_DATA_TBL FORCE
/

PROMPT Dropping Object OBJ_RETAIL_DAILY_DATA_REC

DROP TYPE OBJ_RETAIL_DAILY_DATA_REC FORCE
/

--------------------------------------
--       Creating Types
--------------------------------------

PROMPT Creating Object OBJ_RETAIL_DAILY_DATA_REC

create or replace TYPE OBJ_RETAIL_DAILY_DATA_REC AS OBJECT
(dept                               NUMBER(4),
dept_name                           VARCHAR2(120),
class                               NUMBER(4),
class_name                          VARCHAR2(120),
subclass                            NUMBER(4),
subclass_name                       VARCHAR2(120),
loc_type                            VARCHAR2(250),
location                            NUMBER(10),
loc_name                            VARCHAR2(150),
currency_code                       VARCHAR2(3),
eow_date                            DATE,
day_no                              NUMBER(2),
purch_retail_ly                     NUMBER(20,4),
purch_retail_ty                     NUMBER(20,4),
purch_cost_ly                       NUMBER(20,4),
purch_cost_ty                       NUMBER(20,4),
tsf_in_cost_ly                      NUMBER(20,4),
tsf_in_cost_ty                      NUMBER(20,4),
tsf_in_retail_ly                    NUMBER(20,4),
tsf_in_retail_ty                    NUMBER(20,4),
tsf_in_book_retail_ly               NUMBER(20,4),
tsf_in_book_retail_ty               NUMBER(20,4),
intercompany_in_retail_ly           NUMBER(20,4),
intercompany_in_retail_ty           NUMBER(20,4),
reclass_in_retail_ly                NUMBER(20,4),
reclass_in_retail_ty                NUMBER(20,4),
franchise_returns_retail_ly         NUMBER(20,4),
franchise_returns_retail_ty         NUMBER(20,4),
markup_retail_ly                    NUMBER(20,4),
markup_retail_ty                    NUMBER(20,4),
franchise_markup_retail_ly          NUMBER(20,4),
franchise_markup_retail_ty          NUMBER(20,4),
intercompany_markup_ly              NUMBER(20,4),
intercompany_markup_ty              NUMBER(20,4),
markup_can_retail_ly                NUMBER(20,4),
markup_can_retail_ty                NUMBER(20,4),
rtv_retail_ly                       NUMBER(20,4),
rtv_retail_ty                       NUMBER(20,4),
rtv_cost_ly                         NUMBER(20,4),
rtv_cost_ty                         NUMBER(20,4),
tsf_out_cost_ly                     NUMBER(20,4),
tsf_out_cost_ty                     NUMBER(20,4),
tsf_out_retail_ly                   NUMBER(20,4),
tsf_out_retail_ty                   NUMBER(20,4),
tsf_out_book_retail_ly              NUMBER(20,4),
tsf_out_book_retail_ty              NUMBER(20,4),
intercompany_out_retail_ly          NUMBER(20,4),
intercompany_out_retail_ty          NUMBER(20,4),
reclass_out_retail_ly               NUMBER(20,4),
reclass_out_retail_ty               NUMBER(20,4),
net_sales_retail_ly                 NUMBER(20,4),
net_sales_retail_ty                 NUMBER(20,4),
returns_retail_ly                   NUMBER(20,4),
returns_retail_ty                   NUMBER(20,4),
weight_variance_retail_ly           NUMBER(20,4),
weight_variance_retail_ty           NUMBER(20,4),
empl_disc_retail_ly                 NUMBER(20,4),
empl_disc_retail_ty                 NUMBER(20,4),
franchise_sales_retail_ly           NUMBER(20,4),
franchise_sales_retail_ty           NUMBER(20,4),
freight_claim_retail_ly             NUMBER(20,4),
freight_claim_retail_ty             NUMBER(20,4),
stock_adj_cogs_retail_ly            NUMBER(20,4),
stock_adj_cogs_retail_ty            NUMBER(20,4),
stock_adj_retail_ly                 NUMBER(20,4),
stock_adj_retail_ty                 NUMBER(20,4),
perm_markdown_retail_ly             NUMBER(20,4),
perm_markdown_retail_ty             NUMBER(20,4),
prom_markdown_retail_ly             NUMBER(20,4),
prom_markdown_retail_ty             NUMBER(20,4),
clear_markdown_retail_ly            NUMBER(20,4),
clear_markdown_retail_ty            NUMBER(20,4),
intercompany_markdown_ly            NUMBER(20,4),
intercompany_markdown_ty            NUMBER(20,4),
franchise_markdown_retail_ly        NUMBER(20,4),
franchise_markdown_retail_ty        NUMBER(20,4),
markdown_can_retail_ly              NUMBER(20,4),
markdown_can_retail_ty              NUMBER(20,4),
vat_in_ly                           NUMBER(20,4),
vat_in_ty                           NUMBER(20,4),
vat_out_ly                          NUMBER(20,4),
vat_out_ty                          NUMBER(20,4),
franchise_returns_cost_ly           NUMBER(20,4),
franchise_returns_cost_ty           NUMBER(20,4),
up_chrg_amt_profit_ly               NUMBER(20,4),
up_chrg_amt_profit_ty               NUMBER(20,4),
up_chrg_amt_exp_ly                  NUMBER(20,4),
up_chrg_amt_exp_ty                  NUMBER(20,4),
wo_activity_upd_inv_ly              NUMBER(20,4),
wo_activity_upd_inv_ty              NUMBER(20,4),
wo_activity_post_fin_ly             NUMBER(20,4),
wo_activity_post_fin_ty             NUMBER(20,4),
freight_cost_ly                     NUMBER(20,4),
freight_cost_ty                     NUMBER(20,4),
workroom_amt_ly                     NUMBER(20,4),
workroom_amt_ty                     NUMBER(20,4),
cash_disc_amt_ly                    NUMBER(20,4),
cash_disc_amt_ty                    NUMBER(20,4),
cost_variance_amt_ly                NUMBER(20,4),
cost_variance_amt_ty                NUMBER(20,4),
retail_cost_variance_ly             NUMBER(20,4),
retail_cost_variance_ty             NUMBER(20,4),
rec_cost_adj_variance_ly            NUMBER(20,4),
rec_cost_adj_variance_ty            NUMBER(20,4),
add_cost_ly                         NUMBER(20,4),
add_cost_ty                         NUMBER(20,4),
add_retail_ly                       NUMBER(20,4),
add_retail_ty                       NUMBER(20,4),
deal_income_purch_ly                NUMBER(20,4),
deal_income_purch_ty                NUMBER(20,4),
deal_income_sales_ly                NUMBER(20,4),
deal_income_sales_ty                NUMBER(20,4),
restocking_fee_ly                   NUMBER(20,4),
restocking_fee_ty                   NUMBER(20,4),
franchise_restocking_fee_ly         NUMBER(20,4),
franchise_restocking_fee_ty         NUMBER(20,4),
margin_cost_variance_ly             NUMBER(20,4),
margin_cost_variance_ty             NUMBER(20,4),
net_markdown_ly                     NUMBER(20,4),
net_markdown_ty                     NUMBER(20,4),
net_markup_ly                       NUMBER(20,4),
net_markup_ty                       NUMBER(20,4),
net_reclass_retail_ly               NUMBER(20,4),
net_reclass_retail_ty               NUMBER(20,4),
net_sales_retail_ex_vat_ly          NUMBER(20,4),
net_sales_retail_ex_vat_ty          NUMBER(20,4),
net_sale_noninv_r_exvat_ly          NUMBER(20,4),
net_sale_noninv_r_exvat_ty          NUMBER(20,4),
net_sales_non_inv_retail_ly         NUMBER(20,4),
net_sales_non_inv_retail_ty         NUMBER(20,4),
net_tsf_retail_ly                   NUMBER(20,4),
net_tsf_retail_ty                   NUMBER(20,4),
--net_tsf_cost_ly                     NUMBER(20,4),
--net_tsf_cost_ty                     NUMBER(20,4),
recoverable_tax_ly                  NUMBER(20,4),
recoverable_tax_ty                  NUMBER(20,4),
sales_units_ly                      NUMBER(20,4),
sales_units_ty                      NUMBER(20,4)
)
/

PROMPT Creating Object OBJ_RETAIL_DAILY_DATA_TBL

CREATE OR REPLACE TYPE OBJ_RETAIL_DAILY_DATA_TBL AS TABLE OF OBJ_RETAIL_DAILY_DATA_REC
/ 



