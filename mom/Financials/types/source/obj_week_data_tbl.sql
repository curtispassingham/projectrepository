----------------------------------------------------------------------------
-- OBJECT CREATED :                    OBJ_WEEK_DATA_TBL 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_WEEK_DATA_TBL

DROP TYPE OBJ_WEEK_DATA_TBL FORCE
/

PROMPT Dropping Object OBJ_WEEK_DATA_REC

DROP TYPE OBJ_WEEK_DATA_REC FORCE
/

--------------------------------------
--       Creating Types
--------------------------------------

PROMPT Creating Object OBJ_WEEK_DATA_REC

create or replace TYPE OBJ_WEEK_DATA_REC AS OBJECT
(dept                                 NUMBER(4),
class                                 NUMBER(4),
subclass                              NUMBER(4),
loc_type                              VARCHAR2(1),
location                              NUMBER(10),
half_no                               NUMBER(5),
week_no                               NUMBER(2),
eow_date                              DATE,
opn_stk_cost                          NUMBER(20,4),
opn_stk_retail                        NUMBER(20,4),
purch_cost                            NUMBER(20,4),
freight_cost                          NUMBER(20,4),
rtv_cost                              NUMBER(20,4),
net_sales_cost                        NUMBER(20,4),
cls_stk_cost                          NUMBER(20,4),
purch_retail                          NUMBER(20,4),
rtv_retail                            NUMBER(20,4),
markup_retail                         NUMBER(20,4),
markup_can_retail                     NUMBER(20,4),
clear_markdown_retail                 NUMBER(20,4),
perm_markdown_retail                  NUMBER(20,4),
prom_markdown_retail                  NUMBER(20,4),
weight_variance_retail                NUMBER(20,4),
markdown_can_retail                   NUMBER(20,4),
net_sales_retail                      NUMBER(20,4),
net_sales_retail_ex_vat               NUMBER(20,4),
cls_stk_retail                        NUMBER(20,4),
empl_disc_retail                      NUMBER(20,4),
stock_adj_cost                        NUMBER(20,4),
stock_adj_retail                      NUMBER(20,4),
stock_adj_cogs_cost                   NUMBER(20,4),
stock_adj_cogs_retail                 NUMBER(20,4),
up_chrg_amt_profit                    NUMBER(20,4),
up_chrg_amt_exp                       NUMBER(20,4),
tsf_in_cost                           NUMBER(20,4),
tsf_in_retail                         NUMBER(20,4),
tsf_in_book_cost                      NUMBER(20,4),
tsf_in_book_retail                    NUMBER(20,4),
tsf_out_cost                          NUMBER(20,4),
tsf_out_retail                        NUMBER(20,4),
tsf_out_book_cost                     NUMBER(20,4),
tsf_out_book_retail                   NUMBER(20,4),
reclass_in_cost                       NUMBER(20,4),
reclass_in_retail                     NUMBER(20,4),
reclass_out_cost                      NUMBER(20,4),
reclass_out_retail                    NUMBER(20,4),
returns_cost                          NUMBER(20,4),
returns_retail                        NUMBER(20,4),
shrinkage_cost                        NUMBER(20,4),
shrinkage_retail                      NUMBER(20,4),
workroom_amt                          NUMBER(20,4),
cash_disc_amt                         NUMBER(20,4),
gross_margin_amt                      NUMBER(20,4),
sales_units                           NUMBER(20,4),
cost_variance_amt                     NUMBER(20,4),
htd_gafs_cost                         NUMBER(20,4),
htd_gafs_retail                       NUMBER(20,4),
freight_claim_cost                    NUMBER(20,4),
freight_claim_retail                  NUMBER(20,4),
intercompany_in_retail                NUMBER(20,4),
intercompany_in_cost                  NUMBER(20,4),
intercompany_out_retail               NUMBER(20,4),
intercompany_out_cost                 NUMBER(20,4),
intercompany_markup                   NUMBER(20,4),
intercompany_markdown                 NUMBER(20,4),
wo_activity_upd_inv                   NUMBER(20,4),
wo_activity_post_fin                  NUMBER(20,4),
deal_income_sales                     NUMBER(20,4),
deal_income_purch                     NUMBER(20,4),
restocking_fee                        NUMBER(20,4),
retail_cost_variance                  NUMBER(20,4),
margin_cost_variance                  NUMBER(20,4),
rec_cost_adj_variance                 NUMBER(20,4),
franchise_sales_retail                NUMBER(20,4),
franchise_sales_cost                  NUMBER(20,4),
franchise_returns_retail              NUMBER(20,4),
franchise_returns_cost                NUMBER(20,4),
franchise_markup_retail               NUMBER(20,4),
franchise_markdown_retail             NUMBER(20,4),
franchise_restocking_fee              NUMBER(20,4),
vat_in                                NUMBER(20,4),
vat_out                               NUMBER(20,4),
recoverable_tax                       NUMBER(20,4),
intercompany_margin                   NUMBER(20,4),
net_sales_non_inv_retail              NUMBER(20,4),
net_sales_non_inv_cost                NUMBER(20,4),
net_sales_non_inv_rtl_ex_vat          NUMBER(20,4)
)
/

PROMPT Creating Object OBJ_WEEK_DATA_TBL

CREATE OR REPLACE TYPE OBJ_WEEK_DATA_TBL AS TABLE OF OBJ_WEEK_DATA_REC
/ 



