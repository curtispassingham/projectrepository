----------------------------------------------------------------------------
-- OBJECT CREATED :                    OBJ_ALLOC_LINKED_F_ORD_TBL 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_ALLOC_LINKED_F_ORD_TBL

DROP TYPE OBJ_ALLOC_LINKED_F_ORD_TBL FORCE
/

PROMPT Dropping Object OBJ_ALLOC_LINKED_F_ORD_REC

DROP TYPE OBJ_ALLOC_LINKED_F_ORD_REC FORCE
/

--------------------------------------
--       Creating Types
--------------------------------------

PROMPT Creating Object OBJ_ALLOC_LINKED_F_ORD_REC

CREATE OR REPLACE TYPE OBJ_ALLOC_LINKED_F_ORD_REC AS OBJECT
(
  ALLOC_NO           NUMBER(10),
  TO_LOC             NUMBER(10),
  TO_LOC_TYPE        VARCHAR2(1),
  WF_ORDER_NO        NUMBER(10)
)
/

PROMPT Creating Object OBJ_ALLOC_LINKED_F_ORD_TBL

CREATE OR REPLACE TYPE OBJ_ALLOC_LINKED_F_ORD_TBL AS TABLE OF OBJ_ALLOC_LINKED_F_ORD_REC
/ 
