----------------------------------------------------------------------------
-- OBJECT CREATED :                    ORDCUST_DETAIL_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object ORDCUST_DETAIL_REC

DROP TYPE ORDCUST_DETAIL_TBL FORCE
/

DROP TYPE ORDCUST_DETAIL_REC FORCE
/
--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object ORDCUST_DETAIL_REC 

CREATE OR REPLACE TYPE ORDCUST_DETAIL_REC AS OBJECT
( 
   ordcust_no NUMBER(15) ,
   item VARCHAR2(25) ,
   ref_item VARCHAR2(25),
   original_item VARCHAR2(25),
   qty_ordered_suom NUMBER(12,4) ,
   qty_cancelled_suom NUMBER(12,4),
   standard_uom VARCHAR2(4) ,
   transaction_uom VARCHAR2(4),
   substitute_allowed_ind VARCHAR2(1) ,
   unit_retail NUMBER(20,4),
   retail_currency_code VARCHAR2(3),
   comments VARCHAR2(2000),
   create_datetime DATE ,
   create_id VARCHAR2(30) ,
   last_update_datetime DATE ,
   last_update_id VARCHAR2(30) 
)
/

CREATE OR REPLACE TYPE ORDCUST_DETAIL_TBL AS TABLE OF ORDCUST_DETAIL_REC
/