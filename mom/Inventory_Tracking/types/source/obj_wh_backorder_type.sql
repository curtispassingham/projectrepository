----------------------------------------------------------------------------
--	OBJECT CREATED :                    WH_BACKORD_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object WH_BACKORD_TBL

DROP TYPE WH_BACKORD_TBL FORCE
/

PROMPT Dropping Object WH_BACKORD_REC

DROP TYPE WH_BACKORD_REC FORCE
/

--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object WH_BACKORD_REC

CREATE OR REPLACE TYPE WH_BACKORD_REC AS OBJECT
(
  backord_id        NUMBER(15),
  item              VARCHAR2(25),
  pack_ind          VARCHAR2(1),
  receive_as_type   VARCHAR2(1),
  location          NUMBER(10),
  vwh               NUMBER(10),
  count_vwh         NUMBER(10)
)
/

PROMPT Creating Object WH_BACKORD_TBL

CREATE OR REPLACE TYPE WH_BACKORD_TBL AS TABLE OF WH_BACKORD_REC
/