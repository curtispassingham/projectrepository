----------------------------------------------------------------------------
-- OBJECT CREATED :                    OBJ_WF_ORDER_NO_TBL 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_WF_ORDER_NO_TBL

DROP TYPE OBJ_WF_ORDER_NO_TBL FORCE
/

--------------------------------------
--       Creating Types
--------------------------------------

PROMPT Creating Object OBJ_WF_ORDER_NO_TBL

CREATE OR REPLACE TYPE OBJ_WF_ORDER_NO_TBL AS TABLE OF NUMBER(10)
/ 
