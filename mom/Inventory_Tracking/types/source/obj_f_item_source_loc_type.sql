-----------------------------------------------
--   OBJECT CREATED :   OBJ_F_ITEM_SOURCE_LOC_REC 
-----------------------------------------------

-----------------------------------------------
--       Dropping Types
-----------------------------------------------
Prompt Dropping OBJ_F_ITEM_SOURCE_LOC_TBL
DROP TYPE OBJ_F_ITEM_SOURCE_LOC_TBL FORCE
/

Prompt Dropping OBJ_F_ITEM_SOURCE_LOC_REC
DROP TYPE OBJ_F_ITEM_SOURCE_LOC_REC FORCE
/

-----------------------------------------------
--       Creating Object
-----------------------------------------------
PROMPT Creating Type OBJ_F_ITEM_SOURCE_LOC_REC
CREATE or REPLACE TYPE OBJ_F_ITEM_SOURCE_LOC_REC AS OBJECT
(
 ITEM                           VARCHAR2(25),
 CUSTOMER_LOC                   NUMBER(10),
 SOURCE_LOC_TYPE                VARCHAR2(2),
 SOURCE_LOC_ID                  NUMBER(10),
 LOC_TYPE                       VARCHAR2(2),
 LOC_ID                         NUMBER(10),
 TXN_ITEM                       VARCHAR2(25),

 CONSTRUCTOR FUNCTION OBJ_F_ITEM_SOURCE_LOC_REC
 (
 ITEM                           VARCHAR2   DEFAULT NULL,
 CUSTOMER_LOC                   NUMBER     DEFAULT NULL,
 SOURCE_LOC_TYPE                VARCHAR2   DEFAULT NULL,
 SOURCE_LOC_ID                  NUMBER     DEFAULT NULL,
 LOC_TYPE                       VARCHAR2   DEFAULT NULL,
 LOC_ID                         NUMBER     DEFAULT NULL,
 TXN_ITEM                       VARCHAR2   DEFAULT NULL
 ) RETURN SELF AS RESULT 
)
/

PROMPT Creating TYPE BODY OBJ_F_ITEM_SOURCE_LOC_REC
CREATE OR REPLACE TYPE BODY OBJ_F_ITEM_SOURCE_LOC_REC AS
 CONSTRUCTOR FUNCTION OBJ_F_ITEM_SOURCE_LOC_REC 
 (
 ITEM                           VARCHAR2   DEFAULT NULL,
 CUSTOMER_LOC                   NUMBER     DEFAULT NULL,
 SOURCE_LOC_TYPE                VARCHAR2   DEFAULT NULL,
 SOURCE_LOC_ID                  NUMBER     DEFAULT NULL,
 LOC_TYPE                       VARCHAR2   DEFAULT NULL,
 LOC_ID                         NUMBER     DEFAULT NULL,
 TXN_ITEM                       VARCHAR2   DEFAULT NULL
 ) RETURN SELF AS RESULT IS
    BEGIN
       SELF.ITEM               := ITEM;
       SELF.CUSTOMER_LOC       := CUSTOMER_LOC;
       SELF.SOURCE_LOC_TYPE    := SOURCE_LOC_TYPE;
       SELF.SOURCE_LOC_ID      := SOURCE_LOC_ID;
       SELF.LOC_TYPE           := LOC_TYPE;
       SELF.LOC_ID             := LOC_ID;
       SELF.TXN_ITEM           := TXN_ITEM;
       RETURN;
    END;
 END;
/

Prompt Creating OBJ_F_ITEM_SOURCE_LOC_TBL
CREATE OR REPLACE TYPE OBJ_F_ITEM_SOURCE_LOC_TBL AS TABLE OF OBJ_F_ITEM_SOURCE_LOC_REC
/