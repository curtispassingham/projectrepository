
----------------------------------------------------------------------------
--	OBJECT CREATED :                    CUST_ORD_SALE_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object CUST_ORD_SALE_TBL

DROP TYPE CUST_ORD_SALE_TBL FORCE
/

PROMPT Dropping Object CUST_ORD_SALE_REC

DROP TYPE CUST_ORD_SALE_REC FORCE
/
--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object CUST_ORD_SALE_REC 

CREATE OR REPLACE TYPE CUST_ORD_SALE_REC AS OBJECT
( 
  item       VARCHAR2(25),
  location   NUMBER(10),
  qty        NUMBER(12,4),
  qty_uom    VARCHAR2(4)
)
/

PROMPT Creating Object CUST_ORD_SALE_TBL

CREATE OR REPLACE TYPE CUST_ORD_SALE_TBL AS TABLE OF CUST_ORD_SALE_REC
/