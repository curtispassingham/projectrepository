----------------------------------------------------------------------------
-- OBJECT CREATED :                    ORDCUST_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object ORDCUST_REC

DROP TYPE ORDCUST_TBL FORCE
/

DROP TYPE ORDCUST_REC FORCE
/
--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object ORDCUST_REC 

CREATE OR REPLACE TYPE ORDCUST_REC AS OBJECT
( 
   ordcust_no NUMBER(15) ,
   status VARCHAR2(1) ,
   order_no NUMBER(12),
   tsf_no NUMBER(12),
   source_loc_type VARCHAR2(2),
   source_loc_id NUMBER(10),
   fulfill_loc_type VARCHAR2(1),
   fulfill_loc_id NUMBER(10),
   customer_no VARCHAR2(14),
   customer_order_no VARCHAR2(48),
   fulfill_order_no VARCHAR2(48),
   partial_delivery_ind VARCHAR2(1) ,
   delivery_type VARCHAR2(1),
   carrier_code VARCHAR2(4),
   carrier_service_code VARCHAR2(6),
   consumer_delivery_date DATE,
   consumer_delivery_time DATE,
   bill_first_name VARCHAR2(120),
   bill_phonetic_first VARCHAR2(120),
   bill_last_name VARCHAR2(120),
   bill_phonetic_last VARCHAR2(120),
   bill_preferred_name VARCHAR2(120),
   bill_company_name VARCHAR2(120),
   bill_add1 VARCHAR2(240),
   bill_add2 VARCHAR2(240),
   bill_add3 VARCHAR2(240),
   bill_county VARCHAR2(250),
   bill_city VARCHAR2(120),
   bill_state VARCHAR2(3),
   bill_country_id VARCHAR2(3),
   bill_post VARCHAR2(30),
   bill_jurisdiction VARCHAR2(10),
   bill_phone VARCHAR2(20),
   deliver_first_name VARCHAR2(120),
   deliver_phonetic_first VARCHAR2(120),
   deliver_last_name VARCHAR2(120),
   deliver_phonetic_last VARCHAR2(120),
   deliver_preferred_name VARCHAR2(120),
   deliver_company_name VARCHAR2(120),
   deliver_add1 VARCHAR2(240),
   deliver_add2 VARCHAR2(240),
   deliver_add3 VARCHAR2(240),
   deliver_city VARCHAR2(120),
   deliver_state VARCHAR2(3),
   deliver_country_id VARCHAR2(3),
   deliver_post VARCHAR2(30),
   deliver_county VARCHAR2(250),
   deliver_jurisdiction VARCHAR2(10),
   deliver_phone VARCHAR2(20),
   deliver_charges NUMBER(20,4),
   deliver_charges_curr VARCHAR2(3),
   comments VARCHAR2(2000),
   create_datetime DATE ,
   create_id VARCHAR2(30) ,
   last_update_datetime DATE ,
   last_update_id VARCHAR2(30) 
)
/

CREATE OR REPLACE TYPE ORDCUST_TBL AS TABLE OF ORDCUST_REC
/