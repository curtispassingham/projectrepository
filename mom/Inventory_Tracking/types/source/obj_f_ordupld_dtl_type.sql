-----------------------------------------------
--   OBJECT CREATED :   OBJ_F_ORDUPLD_DTL_REC 
-----------------------------------------------

-----------------------------------------------
--       Dropping Types
-----------------------------------------------
Prompt Dropping OBJ_F_ORDUPLD_DTL_TBL
DROP TYPE OBJ_F_ORDUPLD_DTL_TBL FORCE
/

Prompt Dropping OBJ_F_ORDUPLD_DTL_REC
DROP TYPE OBJ_F_ORDUPLD_DTL_REC FORCE
/

-----------------------------------------------
--       Creating Object
-----------------------------------------------
PROMPT Creating Type OBJ_F_ORDUPLD_DTL_REC
CREATE or REPLACE TYPE OBJ_F_ORDUPLD_DTL_REC AS OBJECT
(
   WF_ORDER_NO              NUMBER(10),
   WF_ORDER_LINE_NO         NUMBER(20),
   ITEM                     VARCHAR2(25),
   SOURCE_LOC_TYPE          VARCHAR2(2),
   SOURCE_LOC_ID            NUMBER(10),
   CUSTOMER_LOC             NUMBER(10),
   REQUESTED_QTY            NUMBER(12,4),
   NEED_DATE                DATE,
   NOT_AFTER_DATE           DATE,
   ACQUISITION_COST         NUMBER(20,4),
   CUSTOMER_COST            NUMBER(20,4),
   FIXED_COST               NUMBER(20,4),
   TEMPL_ID                 NUMBER(10),
   MARGIN_PCT               NUMBER(12,4),
   CALC_TYPE                VARCHAR2(1),
   TEMPL_DESC               VARCHAR2(120),
   FC_CURR_CODE             VARCHAR2(3),

   CONSTRUCTOR FUNCTION OBJ_F_ORDUPLD_DTL_REC
   (
   WF_ORDER_NO              NUMBER   DEFAULT NULL,
   WF_ORDER_LINE_NO         NUMBER   DEFAULT NULL,
   ITEM                     VARCHAR2 DEFAULT NULL,
   SOURCE_LOC_TYPE          VARCHAR2 DEFAULT NULL,
   SOURCE_LOC_ID            NUMBER   DEFAULT NULL,
   CUSTOMER_LOC             NUMBER   DEFAULT NULL,
   REQUESTED_QTY            NUMBER   DEFAULT NULL,
   NEED_DATE                DATE     DEFAULT NULL,
   NOT_AFTER_DATE           DATE     DEFAULT NULL,
   ACQUISITION_COST         NUMBER   DEFAULT NULL,
   CUSTOMER_COST            NUMBER   DEFAULT NULL,
   FIXED_COST               NUMBER   DEFAULT NULL,
   TEMPL_ID                 NUMBER   DEFAULT NULL,
   MARGIN_PCT               NUMBER   DEFAULT NULL,
   CALC_TYPE                VARCHAR2 DEFAULT NULL,
   TEMPL_DESC               VARCHAR2 DEFAULT NULL,
   FC_CURR_CODE             VARCHAR2 DEFAULT NULL
   ) RETURN SELF AS RESULT 
   )
/

PROMPT Creating TYPE BODY OBJ_F_ORDUPLD_DTL_REC
CREATE OR REPLACE TYPE BODY OBJ_F_ORDUPLD_DTL_REC AS
 CONSTRUCTOR FUNCTION OBJ_F_ORDUPLD_DTL_REC 
 (
     WF_ORDER_NO              NUMBER   DEFAULT NULL,
     WF_ORDER_LINE_NO         NUMBER   DEFAULT NULL,
     ITEM                     VARCHAR2 DEFAULT NULL,
     SOURCE_LOC_TYPE          VARCHAR2 DEFAULT NULL,
     SOURCE_LOC_ID            NUMBER   DEFAULT NULL,
     CUSTOMER_LOC             NUMBER   DEFAULT NULL,
     REQUESTED_QTY            NUMBER   DEFAULT NULL,
     NEED_DATE                DATE     DEFAULT NULL,
     NOT_AFTER_DATE           DATE     DEFAULT NULL,
     ACQUISITION_COST         NUMBER   DEFAULT NULL,
     CUSTOMER_COST            NUMBER   DEFAULT NULL,
     FIXED_COST               NUMBER   DEFAULT NULL,
     TEMPL_ID                 NUMBER   DEFAULT NULL,
     MARGIN_PCT               NUMBER   DEFAULT NULL,
     CALC_TYPE                VARCHAR2 DEFAULT NULL,
     TEMPL_DESC               VARCHAR2 DEFAULT NULL,
     FC_CURR_CODE             VARCHAR2 DEFAULT NULL
 ) RETURN SELF AS RESULT IS
    BEGIN
       SELF.WF_ORDER_NO        :=    WF_ORDER_NO;
       SELF.WF_ORDER_LINE_NO   :=    WF_ORDER_LINE_NO;
       SELF.ITEM               :=    ITEM;
       SELF.SOURCE_LOC_TYPE    :=    SOURCE_LOC_TYPE;
       SELF.SOURCE_LOC_ID      :=    SOURCE_LOC_ID;
       SELF.CUSTOMER_LOC       :=    CUSTOMER_LOC;
       SELF.REQUESTED_QTY      :=    REQUESTED_QTY;
       SELF.NEED_DATE          :=    NEED_DATE;
       SELF.NOT_AFTER_DATE     :=    NOT_AFTER_DATE;
       SELF.ACQUISITION_COST   :=    ACQUISITION_COST;
       SELF.CUSTOMER_COST      :=    CUSTOMER_COST;
       SELF.FIXED_COST         :=    FIXED_COST;
       SELF.TEMPL_ID           :=    TEMPL_ID;
       SELF.MARGIN_PCT         :=    MARGIN_PCT;
       SELF.CALC_TYPE          :=    CALC_TYPE;
       SELF.TEMPL_DESC         :=    TEMPL_DESC;
       SELF.FC_CURR_CODE       :=    FC_CURR_CODE;
       RETURN;
    END;
END;
/

Prompt Creating OBJ_F_ORDUPLD_DTL_TBL
CREATE OR REPLACE TYPE OBJ_F_ORDUPLD_DTL_TBL AS TABLE OF OBJ_F_ORDUPLD_DTL_REC
/