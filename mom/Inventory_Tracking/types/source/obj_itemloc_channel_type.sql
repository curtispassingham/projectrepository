----------------------------------------------------------------------------
-- OBJECT CREATED :                    ITEMLOC_CHANNEL_REC
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object ITEMLOC_CHANNEL_REC

DROP TYPE ITEMLOC_CHANNEL_TBL FORCE
/

DROP TYPE ITEMLOC_CHANNEL_REC FORCE
/
--------------------------------------
--       Creating Object
--------------------------------------
PROMPT Creating Object ITEMLOC_CHANNEL_REC
CREATE OR REPLACE TYPE ITEMLOC_CHANNEL_REC AS OBJECT
(
  item             VARCHAR2(25),
  loc              NUMBER(10),
  loc_type         VARCHAR2(1),
  channel_id       NUMBER(4)
)
/
CREATE OR REPLACE TYPE ITEMLOC_CHANNEL_TBL AS TABLE OF ITEMLOC_CHANNEL_REC
/