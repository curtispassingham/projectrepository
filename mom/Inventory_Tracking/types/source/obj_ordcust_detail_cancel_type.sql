
----------------------------------------------------------------------------
-- OBJECT CREATED :                    ORDCUST_DETAIL_CANCEL_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object ORDCUST_DETAIL_CANCEL_TBL

DROP TYPE ORDCUST_DETAIL_CANCEL_TBL FORCE
/

PROMPT Dropping Object ORDCUST_DETAIL_CANCEL_REC

DROP TYPE ORDCUST_DETAIL_CANCEL_REC FORCE
/

--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object ORDCUST_DETAIL_CANCEL_REC 

CREATE OR REPLACE TYPE ORDCUST_DETAIL_CANCEL_REC AS OBJECT
( 
  ordcust_no           NUMBER(15),
  item                 VARCHAR2(25),
  original_item        VARCHAR2(25),
  qty_outstanding      NUMBER(12,4), 
  qty_cancelled_suom   NUMBER(12,4),
  cancel_qty_suom      NUMBER(12,4)
)
/

PROMPT Creating Object ORDCUST_DETAIL_CANCEL_TBL

CREATE OR REPLACE TYPE ORDCUST_DETAIL_CANCEL_TBL AS TABLE OF ORDCUST_DETAIL_CANCEL_REC
/