----------------------------------------------------------------------------
--	OBJECT CREATED :                    VWH_BACKORD_RELEASE_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object VWH_BACKORD_RELEASE_TBL

DROP TYPE VWH_BACKORD_RELEASE_TBL FORCE
/

PROMPT Dropping Object VWH_BACKORD_RELEASE_REC

DROP TYPE VWH_BACKORD_RELEASE_REC FORCE
/

--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object VWH_BACKORD_RELEASE_REC

CREATE OR REPLACE TYPE VWH_BACKORD_RELEASE_REC AS OBJECT
(
  backord_id                    NUMBER(15),
  item                          VARCHAR2(25),
  pack_ind                      VARCHAR2(1),
  vwh                           NUMBER(10),
  backorder_qty                 NUMBER(12,4),
  receive_as_type               VARCHAR2(1),
  cur_backord_reserve           NUMBER(12,4),
  update_customer_backorder     NUMBER(12,4),
  update_pack_comp_backorder    NUMBER(12,4)
)
/

PROMPT Creating Object VWH_BACKORD_RELEASE_TBL

CREATE OR REPLACE TYPE VWH_BACKORD_RELEASE_TBL AS TABLE OF VWH_BACKORD_RELEASE_REC
/