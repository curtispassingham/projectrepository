DROP TYPE WRP_WF_ORDER_DETAIL_REC FORCE
/

DROP TYPE WRP_WF_ORDER_DETAIL_TAB FORCE
/

CREATE OR REPLACE 
TYPE WRP_WF_ORDER_DETAIL_REC AS OBJECT
   (
   wf_order_no               NUMBER(10),
   item                      VARCHAR2(25),
   item_desc                 VARCHAR2(250),
   source_loc_type           VARCHAR2(2),
   source_loc_id             NUMBER(10),
   cust_loc                  NUMBER(10),
   need_date                 DATE,
   not_after_date            DATE,
   uom                       VARCHAR2(4),
   requested_qty             NUMBER(12,4),
   available_qty             NUMBER(12,4),
   customer_cost_vat_incl    NUMBER(20,4),
   total_cust_cost_vat_incl  NUMBER(20,4),
   source_loc_id_name        VARCHAR2(150),
   cust_loc_name             VARCHAR2(150),
   ref_item                  VARCHAR2(25),
   item_type                 VARCHAR2(4),
   cancel_reason             VARCHAR2(6),
   acquisition_cost          NUMBER(20,4),
   customer_cost_vat_excl    NUMBER(20,4),
   total_cust_cost_vat_excl  NUMBER(20,4),
   vat_rate                  VARCHAR2(6),
   vat_amt                   NUMBER(20,4),
   margin_pct                NUMBER(12,4),
   linked_tsfpoalloc         NUMBER(12),
   linked_entity_status      VARCHAR2(1),
   fixed_cost                NUMBER(20,4),
   order_currency            VARCHAR2(3),
   ship_date                 DATE,
   ship_qty                  NUMBER(12,4),
   row_id                    VARCHAR2(25),
   return_code               VARCHAR2(10),
   error_message             VARCHAR2(255),
   pack_ind                  VARCHAR2(1),
   wf_order_line_no          NUMBER(20)
   )
/

CREATE OR REPLACE TYPE WRP_WF_ORDER_DETAIL_TAB AS TABLE OF WRP_WF_ORDER_DETAIL_REC
/
