
----------------------------------------------------------------------------
--	OBJECT CREATED :                    CUST_ORD_RESERVE_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object CUST_ORD_RESERVE_TBL

DROP TYPE CUST_ORD_RESERVE_TBL FORCE
/

PROMPT Dropping Object CUST_ORD_RESERVE_REC

DROP TYPE CUST_ORD_RESERVE_REC FORCE
/
--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object CUST_ORD_RESERVE_REC 

CREATE OR REPLACE TYPE CUST_ORD_RESERVE_REC AS OBJECT
( 
  item                VARCHAR2(25),
  loc                 NUMBER(10),
  customer_resv_qty   NUMBER(12,4)
)
/

PROMPT Creating Object CUST_ORD_RESERVE_TBL

CREATE OR REPLACE TYPE CUST_ORD_RESERVE_TBL AS TABLE OF CUST_ORD_RESERVE_REC
/