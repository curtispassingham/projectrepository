----------------------------------------------------------------------------
-- OBJECT CREATED :                    OBJ_LOC_TBL 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_LOC_TBL

DROP TYPE OBJ_LOC_TBL FORCE
/

PROMPT Dropping Object OBJ_LOC_REC

DROP TYPE OBJ_LOC_REC FORCE
/

--------------------------------------
--       Creating Types
--------------------------------------

PROMPT Creating Object OBJ_LOC_REC

CREATE OR REPLACE TYPE OBJ_LOC_REC AS OBJECT
(
  LOC           NUMBER(10),
  LOC_TYPE      VARCHAR2(1)
)
/

PROMPT Creating Object OBJ_LOC_TBL

CREATE OR REPLACE TYPE OBJ_LOC_TBL AS TABLE OF OBJ_LOC_REC
/ 
