-----------------------------------------------
--   OBJECT CREATED :   OBJ_F_ORD_APPLY_CC_TBL 
-----------------------------------------------

-----------------------------------------------
--       Dropping Types
-----------------------------------------------
Prompt Dropping OBJ_F_ORD_APPLY_CC_TBL
DROP TYPE OBJ_F_ORD_APPLY_CC_TBL FORCE
/

Prompt Dropping OBJ_F_ORD_APPLY_CC_REC
DROP TYPE OBJ_F_ORD_APPLY_CC_REC FORCE
/

-----------------------------------------------
--       Creating Object
-----------------------------------------------
PROMPT Creating Type OBJ_F_ORD_APPLY_CC_REC
CREATE or REPLACE TYPE OBJ_F_ORD_APPLY_CC_REC AS OBJECT
(
   WF_ORDER_NO              NUMBER(10),
   WF_ORDER_LINE_NO         NUMBER(20),
   ITEM                     VARCHAR2(25),
   SOURCE_LOC_TYPE          VARCHAR2(2),
   SOURCE_LOC_ID            NUMBER(10),
   CUSTOMER_LOC             NUMBER(10),
   NEED_DATE                DATE,
   APPROVAL_DATE            DATE,
   ACQUISITION_COST         NUMBER(20,4),
   PRICING_COST             NUMBER(20,4),
   ORDER_CURRENCY           VARCHAR2(3),
   FUTURE_COST_CURR         VARCHAR2(3),
   TEMPL_ID                 NUMBER(10),
   TEMPL_DESC               VARCHAR2(120),
   FIRST_APPLIED            VARCHAR2(1),
   MARGIN_PCT               NUMBER(12,4),

   CONSTRUCTOR FUNCTION OBJ_F_ORD_APPLY_CC_REC
   (
   WF_ORDER_NO              NUMBER   DEFAULT NULL,
   WF_ORDER_LINE_NO         NUMBER   DEFAULT NULL,
   ITEM                     VARCHAR2 DEFAULT NULL,
   SOURCE_LOC_TYPE          VARCHAR2 DEFAULT NULL,
   SOURCE_LOC_ID            NUMBER   DEFAULT NULL,
   CUSTOMER_LOC             NUMBER   DEFAULT NULL,
   NEED_DATE                DATE     DEFAULT NULL,
   APPROVAL_DATE            DATE     DEFAULT NULL,
   ACQUISITION_COST         NUMBER   DEFAULT NULL,
   PRICING_COST             NUMBER   DEFAULT NULL,
   ORDER_CURRENCY           VARCHAR2 DEFAULT NULL,
   FUTURE_COST_CURR         VARCHAR2 DEFAULT NULL,
   TEMPL_ID                 NUMBER   DEFAULT NULL,
   TEMPL_DESC               VARCHAR2 DEFAULT NULL,
   FIRST_APPLIED            VARCHAR2 DEFAULT NULL,
   MARGIN_PCT               NUMBER   DEFAULT NULL
   ) RETURN SELF AS RESULT 
   )
/

PROMPT Creating TYPE BODY OBJ_F_ORD_APPLY_CC_REC
CREATE OR REPLACE TYPE BODY OBJ_F_ORD_APPLY_CC_REC AS
 CONSTRUCTOR FUNCTION OBJ_F_ORD_APPLY_CC_REC 
 (
     WF_ORDER_NO              NUMBER   DEFAULT NULL,
     WF_ORDER_LINE_NO         NUMBER   DEFAULT NULL,
     ITEM                     VARCHAR2 DEFAULT NULL,
     SOURCE_LOC_TYPE          VARCHAR2 DEFAULT NULL,
     SOURCE_LOC_ID            NUMBER   DEFAULT NULL,
     CUSTOMER_LOC             NUMBER   DEFAULT NULL,
     NEED_DATE                DATE     DEFAULT NULL,
     APPROVAL_DATE            DATE     DEFAULT NULL,
     ACQUISITION_COST         NUMBER   DEFAULT NULL,
     PRICING_COST             NUMBER   DEFAULT NULL,
     ORDER_CURRENCY           VARCHAR2 DEFAULT NULL,
     FUTURE_COST_CURR         VARCHAR2 DEFAULT NULL,
     TEMPL_ID                 NUMBER   DEFAULT NULL,
     TEMPL_DESC               VARCHAR2 DEFAULT NULL,
     FIRST_APPLIED            VARCHAR2 DEFAULT NULL,
     MARGIN_PCT               NUMBER   DEFAULT NULL
 ) RETURN SELF AS RESULT IS
    BEGIN
       SELF.WF_ORDER_NO        :=    WF_ORDER_NO;
       SELF.WF_ORDER_LINE_NO   :=    WF_ORDER_LINE_NO;
       SELF.ITEM               :=    ITEM;
       SELF.SOURCE_LOC_TYPE    :=    SOURCE_LOC_TYPE;
       SELF.SOURCE_LOC_ID      :=    SOURCE_LOC_ID;
       SELF.CUSTOMER_LOC       :=    CUSTOMER_LOC;
       SELF.NEED_DATE          :=    NEED_DATE;
       SELF.APPROVAL_DATE      :=    APPROVAL_DATE;
       SELF.ACQUISITION_COST   :=    ACQUISITION_COST;
       SELF.PRICING_COST       :=    PRICING_COST;
       SELF.ORDER_CURRENCY     :=    ORDER_CURRENCY;
       SELF.FUTURE_COST_CURR   :=    FUTURE_COST_CURR;
       SELF.TEMPL_ID           :=    TEMPL_ID;
       SELF.TEMPL_DESC         :=    TEMPL_DESC;
       SELF.FIRST_APPLIED      :=    FIRST_APPLIED;
       SELF.MARGIN_PCT         :=    MARGIN_PCT;
       RETURN;
    END;
END;
/

Prompt Creating OBJ_F_ORD_APPLY_CC_TBL
CREATE OR REPLACE TYPE OBJ_F_ORD_APPLY_CC_TBL AS TABLE OF OBJ_F_ORD_APPLY_CC_REC
/