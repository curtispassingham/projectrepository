----------------------------------------------------------------------------
--	OBJECT CREATED :                    INVBACKORD_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object INVBACKORD_TBL

DROP TYPE INVBACKORD_TBL FORCE
/

PROMPT Dropping Object INVBACKORD_REC

DROP TYPE INVBACKORD_REC FORCE
/

--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object INVBACKORD_REC

CREATE OR REPLACE TYPE INVBACKORD_REC AS OBJECT
(
  backord_id        NUMBER(15),
  item              VARCHAR2(25),
  pack_ind          VARCHAR2(1),
  location          NUMBER(10),
  loc_type          VARCHAR2(1),
  backorder_qty     NUMBER(12,4),
  comp_item_ind     VARCHAR2(1)
)
/

PROMPT Creating Object INVBACKORD_TBL

CREATE OR REPLACE TYPE INVBACKORD_TBL AS TABLE OF INVBACKORD_REC
/