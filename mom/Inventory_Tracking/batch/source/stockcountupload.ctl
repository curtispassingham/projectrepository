LOAD DATA
APPEND
INTO TABLE SVC_STKUPLD_FHEAD
WHEN FILE_RECORD_DESCRIPTOR = 'FHEAD'
  (
   PROCESS_ID         "STOCK_UPLOAD_PROCESS_ID_SEQ.NEXTVAL",
   FILE_RECORD_DESCRIPTOR   position(1:5),
   FILE_LINE_ID             position(6:15),
   FILE_TYPE                position(16:19) char,
   FILE_CREATE_DATE         position(20:33) date 'YYYYMMDDHH24MISS',
   STOCK_TAKE_DATE          position(34:47) date 'YYYYMMDDHH24MISS',
   CYCLE_COUNT              position(48:55) integer external,
   LOCATION_TYPE            position(56:56) char,
   LOCATION                 position(57:66) integer external,
   STATUS                   constant "N",
   ERROR_MSG                constant ""
  )
INTO TABLE SVC_STKUPLD_FDETL
WHEN FILE_RECORD_DESCRIPTOR = 'FDETL'
  (
   PROCESS_ID         "STOCK_UPLOAD_PROCESS_ID_SEQ.CURRVAL",
   CHUNK_ID                 constant "",
   FILE_RECORD_DESCRIPTOR   position(1:5),
   FILE_LINE_ID             position(6:15),
   ITEM_TYPE                position(16:18)  char,
   ITEM_VALUE               position(19:43)  char,
   INVENTORY_QUANTITY       position(44:55)  ":INVENTORY_QUANTITY/10000",
   LOCATION_DESCRIPTION     position(56:205) char,
   STATUS                   constant "N",
   ERROR_MSG                constant ""
  )
INTO TABLE SVC_STKUPLD_FTAIL
WHEN FILE_RECORD_DESCRIPTOR = 'FTAIL'
  (
   PROCESS_ID         "STOCK_UPLOAD_PROCESS_ID_SEQ.CURRVAL",
   FILE_RECORD_DESCRIPTOR   position(1:5),
   FILE_LINE_ID             position(6:15),
   FILE_RECORD_COUNT        position(16:25) integer external,
   STATUS                   constant "N",
   ERROR_MSG                constant ""
  )
