#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  stockcountupload.ksh
#
#  Desc:  This shell script uploads stock counts from STKU files into the
#         SVC_STKUPLD_* tables.
#         These tables serves as the interface by which the RMS core stock upload
#         process updates the system with the actual inventory counts from the
#         STKU files.
#
#         The following are done in this script
#         1. Check if the input file exists and have appropriate access.
#         2. SQL Load (sqlldr) the input file into the SVC_STKUPLD_* tables.
#            A fatal error from sqlldr will halt the process. Rejected records
#            is a non-fatal error and will be continue processing.
#         3. Retrieve the process ID from the paramater tables. This will serve
#            as a key for succeeding processing. One file uploaded will have
#            one unique process ID.
#         4. Insert a record into the SVC_STKUPLD_STATUS table with a chunk ID of
#            0. This record will be used to track the status of records loaded
#            for the particular process ID.
#         5. Validate the records on the SVC_STKUPLD_* tables. A validation failure
#            is a fatal error and will halt processing.
#         6. Group or "chunk" the records on the SVC_STKUPLD_FDETL table.
#            This will group the records into chunks whose size is defined on the
#            RMS_PLSQL_BATCH_CONFIG table.
#         7. Create records on the SVC_STUPLD_STATUS tables for each chunk ID
#            generated. This will allow the succeeding batch (stockcountprocess.ksh)
#            to track execution per chunk.
#         8. Write rejected records from the validation routine a file if
#            a reject filename is specified.
#
#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='stockcountupload.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID

# File locations
CTLFILE="${MMHOME}/oracle/proc/src/stockcountupload.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"

# Initialize variables and constants
inputfile_ext=0
REJECT="R"

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <input file> <reject file>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <input file>       Input file. Can include directory path.

   <reject file>      Optional parameter. Reject records will be written if specified.
"
}

#-------------------------------------------------------------------------
# Function Name: CHECK_INPUT_FILE_EXIST
# Purpose      : Check if the input file exists
#-------------------------------------------------------------------------
function CHECK_INPUT_FILE_EXIST
{
   typeset filewithPath=$1

   if [[ -e ${filewithPath} ]]; then #Check to see if the file exists and if so then continue
      read firstLine < ${filewithPath}
      createDate=`echo $firstLine | awk '{print substr($0,20,14)}'`
      cycleCount=`echo $firstLine | awk '{print substr($0,48,8)}'`
   else
      LOG_ERROR "Cannot find input file ${filewithPath}." "CHECK_INPUT_FILE_EXIST" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Call the CHECK_FILENAME function to validate the filename passed in. 
   CHECK_FILENAME ${filewithPath} ${ERRORFILE} ${LOGFILE} ${pgmName}
   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi

   # Check if the createDate and cycleCount variables have been populated
   if [[ $createDate = "" ]] then
      LOG_ERROR "Error retrieving create date from file $filewithPath. Check file format." "CHECK_INPUT_FILE_EXIST" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   elif [[ $cycleCount = "" ]] then
      LOG_ERROR "Error retrieving cycle count from file $filewithPath. Check file format." "CHECK_INPUT_FILE_EXIST" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      :
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      : SQL Load the input file into the parameter tables
#-------------------------------------------------------------------------
function LOAD_FILE
{

   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${inputFile##*/}
   sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
   sqlldr ${connectStr} silent=feedback,header \
      control=${MMHOME}/log/$inputFileNoPath.ctl \
      log=${sqlldrLog} \
      data=${inputFile} \
      bad=${MMHOME}/log/$sqlldrFile.bad \
      rows=65534 \
      bindsize=2048000 \
      readsize=2048000 \      
      discard=${MMHOME}/log/$sqlldrFile.dsc
   sqlldr_status=$?

   # Check execution status
   if [[ $sqlldr_status != ${OK} ]]; then
      LOG_ERROR "Error while loading file ${inputFileNoPath}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      # Check log file for sql loader errors
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` != ""
         && `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` != "" ]];
      then
         LOG_MESSAGE "${inputFileNoPath} - Completed loading file to the stock upload parameter tables." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      else
         LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
         exit ${NON_FATAL}
      fi
   fi
   
   rm -f ${MMHOME}/log/$inputFileNoPath*
   
   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: INIT_STATUS
# Purpose      : Insert a record into the svc_stkupld_status table
#-------------------------------------------------------------------------
function INIT_STATUS
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_STOCK_UPLOAD_SQL.INITIALIZE_PROCESS_STATUS(:GV_script_error,
                                                                   ${stockProcessId},
                                                                   '${inputFileNoPath}') then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}
   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call INITIALIZE_PROCESS_STATUS Process Id: ${stockProcessId} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Status record successfully created. Process Id is ${stockProcessId}." "INIT_STATUS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: VALIDATE_LOAD
# Purpose      : Perform validation on loaded tables.
#              : Validation failure will result in a fatal error.
#-------------------------------------------------------------------------
function VALIDATE_LOAD
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_STOCK_UPLOAD_SQL.VALIDATE_PARAMETER_TABLES(:GV_script_error,
                                                                   ${stockProcessId}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call VALIDATE_PARAMETER_TABLES Process Id: ${stockProcessId} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Successfully validated: Process Id ${stockProcessId}." "VALIDATE_LOAD" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHECK_FOR_REJECTS
# Purpose      : Logs any non-fatal validation failures
#-------------------------------------------------------------------------
function CHECK_FOR_REJECTS
{
   invItemsInd=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select 'Y'
     from svc_stkupld_fdetl
    where process_id = ${stockProcessId}
      and status = '${REJECT}'
      and rownum = 1;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Error in checking for rejected records for file $inputFileNoPath." "CHECK_FOR_REJECTS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHUNK_STOCK_COUNT
# Purpose      :
#-------------------------------------------------------------------------
function CHUNK_STOCK_COUNT
{
   chunkCount=0

   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_STOCK_UPLOAD_SQL.CHUNK_STOCK_COUNT(:GV_script_error,
                                                           ${stockProcessId}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call CHUNK_STOCK_COUNT with Process Id: ${stockProcessId} failed" >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Successfully chunked records for Process Id ${stockProcessId}." "CHUNK_STOCK_COUNT" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: INIT_CHUNK_STATUS
# Purpose      : Perform validation on loaded tables.
#              : Validation failure will result in a fatal error.
#-------------------------------------------------------------------------
function INIT_CHUNK_STATUS
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_STOCK_UPLOAD_SQL.INITIALIZE_CHUNK_STATUS(:GV_script_error,
                                                                 ${stockProcessId}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}
   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call INITIALIZE_CHUNK_STATUS with Process Id: ${stockProcessId} failed" >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Status chunk records successfully created for Process Id ${stockProcessId}." "INIT_CHUNK_STATUS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: WRITE_REJECT_FILE
# Purpose      :
#-------------------------------------------------------------------------
function WRITE_REJECT_FILE
{
   LOG_MESSAGE "${inputFileNoPath} - Writing reject file ${rejectFile} for process ID ${stockProcessId}." "WRITE_REJECT_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

   echo `sqlplus -s $UP <<!
        set PAGESIZE 32000
        set LINESIZE 1000
        set HEADING OFF
        set FEEDBACK OFF
        set NEWPAGE NONE
        set TERMOUT OFF
        set SHOWMODE OFF
        set AUTOPRINT OFF
        set VERIFY OFF
        set TRIMOUT OFF
        set TRIMSPOOL ON
        select 'FHEAD' ||
               LPAD(1,10,0) ||
               'STKU'||
               TO_CHAR(SYSDATE, 'YYYYMMDDHHMISS')||
               TO_CHAR(stock_take_date, 'YYYYMMDD')||'000000'||
               LPAD(cycle_count, 8,0) ||
               location_type||
               LPAD(location, 10,0)
          from svc_stkupld_fhead
         where process_id = ${stockProcessId}
           and cycle_count = ${cycleCount};
        exit;
        !` >> ${rejectFile}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error writing reject file ${rejectFile}." "WRITE_REJECT_FILE" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID} 
      exit ${FATAL}
   fi

  echo "set PAGESIZE 32000
        set TAB OFF
        set LINESIZE 1000
        set HEADING OFF
        set FEEDBACK OFF
        set NEWPAGE NONE
        set TERMOUT OFF
        set SHOWMODE OFF
        set AUTOPRINT OFF
        set VERIFY OFF
        set TRIMOUT ON
        set TRIMSPOOL ON
        select 'FDETL'||
               LPAD(ROWNUM + 1,10,0)||
               item_type ||
               RPAD(item_value,25,' ')||
               DECODE(SIGN(ROUND(inventory_quantity)), 1, LPAD(ROUND(inventory_quantity), 8, 0), LPAD(0,8,0))||'0000'||
               RPAD(NVL(location_description, ' '), 150)
          from svc_stkupld_fdetl
         where process_id = ${stockProcessId}
           and status = '${REJECT}'
           and error_msg is not NULL;" | sqlplus -s $UP >> ${rejectFile}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error writing reject file ${rejectFile}." "WRITE_REJECT_FILE" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
      exit ${FATAL}
   fi

   line_count=`wc -l < ${rejectFile}`

   echo `sqlplus -s $UP <<!
        set PAGESIZE 32000
        set LINESIZE 1000
        set HEADING OFF
        set FEEDBACK OFF
        set NEWPAGE NONE
        set TERMOUT OFF
        set SHOWMODE OFF
        set AUTOPRINT OFF
        set VERIFY OFF
        set TRIMOUT OFF
        set TRIMSPOOL ON
        select 'FTAIL' ||LPAD($line_count + 1,10, 0) || LPAD($line_count - 1,10,0)
          from dual;
        !` >> ${rejectFile}

   if [ $? -ne ${OK} ]; then
      LOG_MESSAGE "Error writing reject file ${rejectFile}." "WRITE_REJECT_FILE" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GEN_CTL
# Function Name: generate control file based on file name
# Purpose      : 
#-------------------------------------------------------------------------

function GEN_CTL
{

   rm -f ${MMHOME}/log/$inputFileNoPath.ctl

   echo 'LOAD DATA' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'APPEND' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'INTO TABLE SVC_STKUPLD_FHEAD' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'WHEN FILE_RECORD_DESCRIPTOR = ' \'FHEAD\'  >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo '(' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'PROCESS_ID  "' ${stockProcessId} '",' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_RECORD_DESCRIPTOR   position(1:5),' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_LINE_ID             position(6:15),' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_TYPE                position(16:19) char,' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_CREATE_DATE         position(20:33) date' \'YYYYMMDDHH24MISS\' ',' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'STOCK_TAKE_DATE          position(34:47) date' \'YYYYMMDDHH24MISS\' ',' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'CYCLE_COUNT              position(48:55) integer external,' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'LOCATION_TYPE            position(56:56) char,' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'LOCATION                 position(57:66) integer external,' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'STATUS                   constant "N",' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'ERROR_MSG                constant ""' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo ')' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'INTO TABLE SVC_STKUPLD_FDETL' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'WHEN FILE_RECORD_DESCRIPTOR = ' \'FDETL\'  >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo '(' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'PROCESS_ID  "' ${stockProcessId} '",' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'CHUNK_ID                 constant "",' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_RECORD_DESCRIPTOR   position(1:5),' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_LINE_ID             position(6:15),' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'ITEM_TYPE                position(16:18)  char,' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'ITEM_VALUE               position(19:43)  char,' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'INVENTORY_QUANTITY       position(44:55)  ":INVENTORY_QUANTITY/10000",' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'LOCATION_DESCRIPTION     position(56:205) char,' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'STATUS                   constant "N",' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'ERROR_MSG                constant ""' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo ')' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'INTO TABLE SVC_STKUPLD_FTAIL' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'WHEN FILE_RECORD_DESCRIPTOR = ' \'FTAIL\'  >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo '(' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'PROCESS_ID  "' ${stockProcessId} '",' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_RECORD_DESCRIPTOR   position(1:5),' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_LINE_ID             position(6:15),' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'FILE_RECORD_COUNT        position(16:25) integer external,' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'STATUS                   constant "N",' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo 'ERROR_MSG                constant ""' >> ${MMHOME}/log/$inputFileNoPath.ctl
   echo ')' >> ${MMHOME}/log/$inputFileNoPath.ctl

   return ${OK}
}


#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
inputFile=$2
inputFileNoPath=${inputFile##*/}    # remove the path
rejectFile=$3
inputfile_ext=${inputFile##*.}
LOG_MESSAGE "stockcountupload.ksh started by ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

# Check if input file exists.
CHECK_INPUT_FILE_EXIST $inputFile

# Get Process Id
stockProcessId=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select stock_upload_process_id_seq.nextval
  from dual;
exit;
EOF`

GEN_CTL

# Load the stock upload parameter tables
LOAD_FILE

# Insert entry into the status table to track this upload
INIT_STATUS

# Validate the records loaded into the parameter tables
VALIDATE_LOAD

# Check if there are invalid records
CHECK_FOR_REJECTS

# Chunk the records in the parameter tables
CHUNK_STOCK_COUNT

# Create status records for the chunked records
INIT_CHUNK_STATUS

# Export invalid items to the reject file if specified
if [[ ${invItemsInd} = 'Y' ]]; then
   if [ ! -z ${rejectFile} ]; then
      WRITE_REJECT_FILE
   fi
   LOG_MESSAGE "stockcountupload.ksh finished with invalid items for ${USER} for file ${inputFileNoPath}. Check reject file ${rejectFile}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
else
   LOG_MESSAGE "stockcountupload.ksh finished for ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

exit ${OK}
