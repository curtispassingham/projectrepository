#! /bin/ksh

#---------------------------------------------------------------------------------
#  File:  refeodinventory.ksh
#
#  Desc:  This korn shell script refreshes the ITEM_LOC_SOH_EOD table during 
#         Phase 2.5. It is assumed that by the end of Phase 2, all of today�s
#         updates pertaining to stock on hand have been performed and is at the 
#         correct state to get an end-of-day inventory snapshot during Phase 2.5.
#---------------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
traceLog="${MMHOME}/log/${pgmName}_${pgmPID}_tracefiles.log"

OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <sql_trace> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <sql_trace> optional parameter. (valid values are:
               SQL_TRACE1,  -- no binds and waits
               SQL_TRACE4,  -- trace with binds
               SQL_TRACE8,  -- trace with waits
            or SQL_TRACE12).-- trace witn binds and waits
"
}

#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the  messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code   := 0;
      EXEC :GV_script_error  := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function Name: REF_SNAP
# Purpose      : Inserts records from ITEM_LOC_SOH to ITEM_LOC_SOH_EOD
#-------------------------------------------------------------------------

function REF_SNAP
{
   sqlTxt="
      DECLARE
         L_owner SYSTEM_OPTIONS.TABLE_OWNER%TYPE := NULL;
      BEGIN
         select table_owner
           into L_owner
           from system_options;

         EXECUTE IMMEDIATE 'truncate table '||L_owner||'.item_loc_soh_eod';

         EXECUTE IMMEDIATE 'alter table '||L_owner||'.item_loc_soh_eod nologging';
         EXECUTE IMMEDIATE 'alter table '||L_owner||'.item_loc_soh_eod drop primary key drop index';
         
         EXECUTE IMMEDIATE 'insert /*+ APPEND*/
                              into item_loc_soh_eod (item,
                                                     loc,
                                                     loc_type,
                                                     item_parent,
                                                     item_grandparent,
                                                     av_cost,
                                                     unit_cost,
                                                     stock_on_hand,
                                                     pack_comp_soh,
                                                     in_transit_qty,
                                                     pack_comp_intran,
                                                     tsf_reserved_qty,
                                                     pack_comp_resv,
                                                     tsf_expected_qty,
                                                     pack_comp_exp,
                                                     rtv_qty,
                                                     non_sellable_qty,
                                                     pack_comp_non_sellable,
                                                     customer_resv,
                                                     pack_comp_cust_resv,
                                                     customer_backorder,
                                                     pack_comp_cust_back,
                                                     finisher_units,
                                                     average_weight,
                                                     finisher_av_retail,
                                                     create_datetime,
                                                     create_id,
                                                     last_update_datetime,
                                                     last_update_id)
                                                     (select /*+ PARALLEL*/
                                                             item,
                                                             loc,
                                                             loc_type,
                                                             item_parent,
                                                             item_grandparent,
                                                             av_cost,
                                                             unit_cost,
                                                             stock_on_hand,
                                                             pack_comp_soh,
                                                             in_transit_qty,
                                                             pack_comp_intran,
                                                             tsf_reserved_qty,
                                                             pack_comp_resv,
                                                             tsf_expected_qty,
                                                             pack_comp_exp,
                                                             rtv_qty,
                                                             non_sellable_qty,
                                                             pack_comp_non_sellable,
                                                             customer_resv,
                                                             pack_comp_cust_resv,
                                                             customer_backorder,
                                                             pack_comp_cust_back,
                                                             finisher_units,
                                                             average_weight,
                                                             finisher_av_retail,
                                                             SYSDATE create_datetime,
                                                             USER create_id,
                                                             SYSDATE last_update_datetime,
                                                             USER last_update_id
                                                        from item_loc_soh)';
         EXECUTE IMMEDIATE 'commit';

         EXECUTE IMMEDIATE 'create unique index '||L_owner||'.PK_ITEM_LOC_SOH_EOD on '||L_owner||'.ITEM_LOC_SOH_EOD (ITEM,LOC) tablespace RETAIL_INDEX initrans 12 parallel 8 nologging';
         EXECUTE IMMEDIATE 'alter index '||L_owner||'.PK_ITEM_LOC_SOH_EOD noparallel logging';
         EXECUTE IMMEDIATE 'alter table '||L_owner||'.ITEM_LOC_SOH_EOD add (constraint PK_ITEM_LOC_SOH_EOD primary key (ITEM,LOC) using index tablespace RETAIL_INDEX initrans 12)';
         EXECUTE IMMEDIATE 'alter table '||L_owner||'.ITEM_LOC_SOH_EOD logging';

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL_T "$sqlTxt" ${CONNECT} ${SQL_TRACE} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID} "REF_SNAP"

   if [[ $? -ne ${OK} ]]; then
      echo "REF_SNAP Failed" >>${ERRORFILE}
      return ${FATAL}
   else
      if [ -e $traceLog ]
      then
         if [ `wc -l $traceLog | awk '{print $1}'` -gt 0 ]
         then
            LOG_MESSAGE "SQL TRACE Level ${SQL_TRACE##*E} files created." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
            LOG_MESSAGE "Please see ${pgmName}_${pgmPID}_tracefiles.log in the LOG directory for the listed trace files." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
         fi
      fi

      LOG_MESSAGE "Successfully Completed"
      return ${OK}
   fi
}

#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit 1
elif [ $# -lt 2 ]
then
   SQL_TRACE="SQL_TRACE0"
elif [ $# -eq 2 ]
then
   case "$2" in
      "SQL_TRACE1") SQL_TRACE=$2
      ;;
      "SQL_TRACE4") SQL_TRACE=$2
      ;;
      "SQL_TRACE8") SQL_TRACE=$2
      ;;
      "SQL_TRACE12") SQL_TRACE=$2
      ;;
      *) USAGE
         exit 1
      ;;
   esac
else
   USAGE
   exit 1
fi

CONNECT=$1
if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >>$ERRORFILE
EOF

if [ `cat $ERRORFILE | wc -l` -gt 1 ]
then
   LOG_MESSAGE "Exiting due to ORA/LOGIN Error. Check error file."
   exit 1;
fi

LOG_MESSAGE "Started by ${USER}"

REF_SNAP

if [[ ! ( -s $ERRORFILE ) ]]
then
   rm -f $ERRORFILE
fi

exit 0
