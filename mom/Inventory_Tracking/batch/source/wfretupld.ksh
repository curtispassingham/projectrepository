#! /bin/ksh
#-------------------------------------------------------------------------------------
# File:  wfretupld.ksh
#  Desc: This shell script will be used to upload Franchisee Return 
#        using SQL Loader and staging tables.
#-------------------------------------------------------------------------------------

# Common functions library and environment variables
 . ${MMHOME}/oracle/lib/src/rmsksh.lib         
pgmName=`basename $0`

pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

# File locations
CTLFILE="${MMHOME}/oracle/proc/src/wfretupld.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
FATAL=255
NON_FATAL=1
OK=0
TRUE=1
FALSE=0

USAGE="Usage: $pgmName.$pgmExt <connect> <input directory> <output directory> <number of threads>"

function CHECK_INPUT_DIR
{
   inputDir=$1
   # Check if path is valid
   if [[ -d $inputDir && -r $inputDir ]]; then
     :
   else
      exit ${FATAL}
   fi
   return ${OK}
}
function CHECK_OUTPUT_DIR
{
   outputDir=$1
   # Check if path is valid
   if [[ -d $outputDir && -w $outputDir ]]; then
     :
   else
      exit ${FATAL}
   fi
   return ${OK}
}

function VALIDATE_FILE
{
   count=${count=1}
   for inputFile in `ls ${inputDir}/wfreturn*.dat 2>/dev/null`
   do
      read firstLine < ${inputFile}
      cust_ret_ref_no="$(echo ${firstLine} | cut -f4 -d"|")"
      wf_cust_id="$(echo ${firstLine} | cut -f3 -d"|")"

      if [[ `grep FHEAD $inputFile | wc -l`  -gt ${count} || `grep FHEAD $inputFile | wc -l`  -lt ${count} ]]; then
         LOG_ERROR "The input file ${inputFile##*/} has invalid format." "VALIDATE_FILE-${inputFile##*/} " ${OK} ${ERRORFILE} ${LOGFILE} ${pgmName}
         mv $inputFile $inputFile.rej
      elif [[ ${cust_ret_ref_no} = "" ]]; then
            LOG_ERROR "The input file ${inputFile##*/} has customer return reference no as null." "VALIDATE_FILE-${inputFile##*/}" ${OK} ${ERRORFILE} ${LOGFILE} ${pgmName}
            mv $inputFile $inputFile.rej
      elif [[ $wf_cust_id = "" ]]; then
            LOG_ERROR "The input file ${inputFile##*/} has Franchisee customer id as null." "VALIDATE_FILE-${inputFile##*/}" ${OK} ${ERRORFILE} ${LOGFILE} ${pgmName}
            mv $inputFile $inputFile.rej
      else
         echo $cust_ret_ref_no"|"$wf_cust_id "|" ${inputFile##*/}  >> ${LOGDIR}/wfreturn_filelist.dat
              file_exist_ind="Y"
      fi
   done

   for inputFile in `ls ${inputDir}/wfreturn*.dat 2>/dev/null`
   do
      read firstLine < ${inputFile}
      cust_ret_ref_no="$(echo ${firstLine} | cut -f4 -d"|")"
      wf_cust_id="$(echo ${firstLine} | cut -f3 -d"|")"
      if [[ `grep  $cust_ret_ref_no"|"$wf_cust_id ${LOGDIR}/wfreturn_filelist.dat | wc -l` -gt ${count} ]]; then
         LOG_ERROR "The input file ${inputFile##*/} is duplicate." "VALIDATE_FILE-${inputFile##*/}" ${OK} ${ERRORFILE} ${LOGFILE} ${pgmName}
         mv $inputFile $inputFile.rej
      fi
   done
   if [[ $file_exist_ind == 'Y' ]]; then
      rm ${LOGDIR}/wfreturn_filelist.dat
   fi
   return ${OK}
}


function CHK_IN_FILES
{

   if [[ -n `ls -1 ${inputDir} 2>/dev/null` ]]; then
      echo ${TRUE}
   else
      echo ${FALSE}
   fi

   return ${OK}
   
}

function WRITE_REJECT
{
 echo "set feedback off;
       set heading off;
       set term off;
       set verify off;
       set serveroutput on size 1000000;
       DECLARE
          O_err_msg           VARCHAR2(255);
          L_error_tbl         OBJ_ERROR_TBL;
          L_stage_table_name  VARCHAR2(100);
          FUNCTION_ERROR      EXCEPTION;


       BEGIN
          L_stage_table_name := 'SVC_WF_RET_HEAD';   
          if NOT SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_err_msg,
                                                      L_error_tbl,
                                                      L_stage_table_name,
                                                      $ProcessId) then
             raise FUNCTION_ERROR;
          end if;

          L_stage_table_name := 'SVC_WF_RET_DETAIL';       
          if NOT SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_err_msg,
                                                      L_error_tbl,
                                                      L_stage_table_name,
                                                      $ProcessId) then
             raise FUNCTION_ERROR;
          end if;

          L_stage_table_name := 'SVC_WF_RET_TAIL'; 
          if NOT SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_err_msg,
                                                      L_error_tbl,
                                                      L_stage_table_name,
                                                      $ProcessId) then
             raise FUNCTION_ERROR;
          end if;
          FOR i in L_error_tbl.first..L_error_tbl.last loop
            dbms_output.put_line('${inputFile##*/}'||'~'||L_error_tbl(i));
          end loop;
           END;
           /    
          exit;
         EOF"  | sqlplus  -s ${connectStr} >>  ${ERRORFILE}
    return ${OK}
}

function GET_STATUS
{
status=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select process_status 
     from svc_wf_ret_head
    where process_id = ${ProcessId};
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${status}" "GET_STATUS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
  return ${OK}
}



#-------------------------------------------------------------------------
# Function Name: GET_PROCESS_ID
# Purpose      : Fetches the process_id.
#-------------------------------------------------------------------------
function GET_PROCESS_ID
{
   ProcessId=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select process_id
     from (select process_id
             from svc_wf_ret_head
            where process_status = 'N'
              and cust_ret_ref_no = '$cust_ret_ref_no'
              and wf_customer_id = '$wf_cust_id'
            order by process_id desc)
    where rownum = 1;
   exit;
   EOF`
   
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${ProcessId}" "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   
   # Check the  process id
   if [ -z ${ProcessId} ]; then
      LOG_ERROR "${inputFile##*/}; Cannot retrieve process id for file." "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
  
  return ${OK}
}

function VALIDATE_LOAD
{
 Chunkval=1
   sqlTxt="
      DECLARE
          O_key            VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT WF_RETURN_UPLOAD_SQL.VALIDATE_STG_TABLES(:GV_script_error,
                                                         ${ProcessId},
                                                         ${Chunkval}) then
            raise FUNCTION_ERROR;
         end if;
         COMMIT;
      EXCEPTION
         when FUNCTION_ERROR then
            if :GV_script_error is NULL then
               COMMIT;
            else
               ROLLBACK;
            end if;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      GET_STATUS $ProcessId
      if [[ $status == 'R' ]]; then
         WRITE_REJECT $ProcessId
         LOG_ERROR "${inputFile##*/}; Process Id: ${ProcessId}, Chunk ID ${Chunkval} failed " "VALIDATE_STG_TABLES" ${OK} ${ERRORFILE} ${LOGFILE} ${pgmName}
      else
          LOG_ERROR "${ProcessId}" "VALIDATE_LOAD" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
          exit ${FATAL} 
      fi
   else
      LOG_MESSAGE "${inputFile##*/}; Process Id: ${ProcessId}, Chunk ID ${Chunkval} successfully processed." "VALIDATE_STG_TABLES" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi
   
  return ${OK}
}
function CREATE_RETURN
{ 
   Chunkval=1
     sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT CORESVC_WF_RETURN_UPLOAD_SQL.CREATE_RMA(:GV_script_error,
                                                        ${ProcessId},
                                                        ${Chunkval}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
        when FUNCTION_ERROR then
            if :GV_script_error is NULL then
               COMMIT;
            else
               ROLLBACK;
            end if;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
 
  EXEC_SQL ${sqlTxt}
   if [[ $? -ne ${OK} ]]; then
      GET_STATUS $ProcessId
      if [[ $status == 'E' ]]; then
         WRITE_REJECT $ProcessId
         LOG_ERROR "${inputFile##*/}; Process Id: ${ProcessId}, Chunk ID ${Chunkval} failed " "CREATE_RMA" ${OK} ${ERRORFILE} ${LOGFILE} ${pgmName}
      else
         LOG_ERROR "${ProcessId}" "CREATE_RETURN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
         exit ${FATAL} 
      fi
   else
      LOG_MESSAGE "${inputFile##*/}; Process Id: ${ProcessId}, Chunk ID ${Chunkval} successfully processed." "CREATE_RMA" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      mv $inputFile $outputDir 
   fi

return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      
      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`
     
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi
   
   return ${OK}
}
#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 3 ]
then
   echo $USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
inputDir=$2
outputDir=$3
SLOTS=$4
SLOTS=${SLOTS:=1}

# If this script is killed, cleanup
trap "kill -15 0; exit 15" 1 2 3 15

LOG_MESSAGE "${pgmName}.${pgmExt} started by ${USER} for input files." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

CHECK_INPUT_DIR $inputDir
CHECK_OUTPUT_DIR $outputDir

# Check if there are input files to process
if [[ $(CHK_IN_FILES ${inputDir}) -eq ${TRUE} ]]; then 
   #pre validate the files
   VALIDATE_FILE
   for inputFile in `ls ${inputDir}/wfreturn*.dat 2>/dev/null`
   do 
      while [ `jobs | wc -l` -ge $SLOTS ]
      do
         : # Null command.
      done     
      
      read firstLine < ${inputFile}
      cust_ret_ref_no="$(echo ${firstLine} | cut -f4 -d"|")"
      wf_cust_id="$(echo ${firstLine} | cut -f3 -d"|")"
      dtStamp=`date +"%G%m%d%H%M%S"`
      sqlldrFile=${inputFile##*/}
      sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
      sqlldr ${connectStr} silent=feedback,header \
         control=${CTLFILE} \
         log=${sqlldrLog} \
         data=${inputFile} \
         bad=${MMHOME}/log/$sqlldrFile.bad \
         discard=${MMHOME}/log/$sqlldrFile.dsc 
      sqlldr_status=$?

      # Check execution status
      if [[ $sqlldr_status != ${OK} ]]; then
         LOG_MESSAGE "${inputFile##*/}; Error while loading file." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      else
         LOG_MESSAGE "${inputFile##*/}; Completed file loading." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
         GET_PROCESS_ID
         VALIDATE_LOAD $ProcessId
         GET_STATUS 
         if [[ ${status} == 'N' ]]; then
            CREATE_RETURN $ProcessId  
         fi
      fi     
  done
fi   
exit ${OK}