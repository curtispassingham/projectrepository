/* BEGIN DOC

Program Name: wfrtnprg.pc

Description: This program closes the Franchise returns after
             'wf_history_mths' number of days specified in the system_options
             table from the return create_datetime of wf_return_head table.
             All the associated transfers, shipments should be purged from its
             respective tables prior to purge the returns.

Table                    Index     Select     Insert     Update   Delete
--------------------------------------------------------------------------
PERIOD                    NO       YES         NO        NO       NO
SYSTEM_OPTIONS            NO       YES         NO        NO       NO
WF_RETURN_HEAD            NO       YES         NO        NO       YES
WF_RETURN_DETAIL          NO       NO          NO        NO       YES
WF_BILLING_RETURNS        NO       YES         NO        NO       YES
TSFHEAD                   NO       YES         NO        NO       NO
__________________________________________________________________________

END DOC */

/*-------------------------------------------------------*\
 * includes
\*-------------------------------------------------------*/
#include "retek.h"
#include "retek_2.h"
#include "intrface.h"

/*-------------------------------------------------------*\
 * defines
\*-------------------------------------------------------*/
EXEC SQL INCLUDE SQLCA.H;

#define PROGRAM "wfrtnprg"

/*-------------------------------------------------------*\
 * global variables
\*-------------------------------------------------------*/
long  SQLCODE;
char  ps_vdate[NULL_DATE];
char  ps_rma_no[NULL_RMA_NO];
long  pl_wf_history_mths ;

init_parameter parameter[] =
{
    /* NAME------------TYPE---------SUB_TYPE */
   "commit_max_ctr",  "uint",   "",
   "num_threads",     "string", "",
   "thread_val",      "string", "",
   "driver_name",     "string", "S"
};

#define NUM_INIT_PARAMETERS    (sizeof(parameter) / sizeof(init_parameter))

/* restart declarations */
uint pi_commit_max_ctr;
char ps_num_threads[NULL_THREAD];
char ps_thread_val[NULL_THREAD];
char ps_driver_name[MAX_DRIVER_NAME_LEN] = "";


struct ret_array{
   char  (*s_rma_no)[NULL_RMA_NO];
   long  l_cur_rec;
} pa_ret_recs;

struct bill_array{
   char  (*wf_order_no)[NULL_WF_ORDER_NO];
   char  (*item)[NULL_ITEM];
   char  (*rma_no)[NULL_RMA_NO];
} bill_recs;

long pl_rec_to_process = 0;

/*-------------------------------------------------------*\
 * function prototypes
\*-------------------------------------------------------*/
int init(int argc, char *argv[]);
int process(void);
int size_arrays(void);
int final(void);

int wf_delete_bill_returns(void);
int wf_delete_returns(void);

/*-------------------------------------------------------*\
 * int main()
\*-------------------------------------------------------*/

int main(int argc, char* argv[])
{
   char *function = "main";
   char ls_log_message[LEN_ERROR_MESSAGE];
   int  li_init_results;
   int  li_final_return;

   if (argc < 2)
   {
      fprintf(stderr,"Usage: %s userid/passwd\n",argv[0]);
      return(FAILED);
   }

   if (LOGON(argc, argv) < 0)
      return(FAILED);

   if ((li_init_results = init(argc, argv)) < 0)
      gi_error_flag = 2;

   if (li_init_results != NO_THREAD_AVAILABLE)
   {
      if (li_init_results == OK)
      {
         if (process() < 0)
            gi_error_flag = 1;
      }

      if (final() < 0)
      {
         if (gi_error_flag == 0)
            gi_error_flag = 3;
      }
   }

   if (gi_error_flag == 2)
   {
      LOG_MESSAGE("Aborted in init");
      li_final_return = FAILED;
   }
   else if (gi_error_flag == 1)
   {
      sprintf(ls_log_message, "Thread %s - Aborted in process", ps_thread_val);
      LOG_MESSAGE(ls_log_message);
      li_final_return = FAILED;
   }
   else if (gi_error_flag == 3)
   {
      sprintf(ls_log_message, "Thread %s - Aborted in final", ps_thread_val);
      LOG_MESSAGE(ls_log_message);
      li_final_return = FAILED;
   }
   else if (li_init_results == NO_THREAD_AVAILABLE)
   {
      LOG_MESSAGE("Terminated - no threads available");
      li_final_return = NO_THREADS;
   }
   else
   {
      sprintf(ls_log_message, "Thread %s - Terminated Successfully", ps_thread_val);
      LOG_MESSAGE(ls_log_message);
      li_final_return = SUCCEEDED;
   }

   return(li_final_return);
}


/*-------------------------------------------------------*\
 * int init()
\*-------------------------------------------------------*/

int init(int argc, char *argv[])
{
   char *function = "init";
   int  li_init_return;

   EXEC SQL DECLARE c_init CURSOR FOR
      SELECT s.wf_history_months ,
             TO_CHAR(p.vdate,'YYYYMMDD')
        FROM system_options s,
             period p;

   /* call retek_init to initialize restart process */
   li_init_return  = retek_init(NUM_INIT_PARAMETERS,
                                parameter,
                                &pi_commit_max_ctr,
                                ps_num_threads,
                                ps_thread_val,
                                ps_driver_name);

   if (li_init_return != 0)
      return (li_init_return);

   EXEC SQL OPEN c_init;

   if SQL_ERROR_FOUND
   {
      sprintf(table, "SYSTEM_OPTIONS, PERIOD");
      sprintf(err_data, "CURSOR OPEN: c_init");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL FETCH c_init INTO :pl_wf_history_mths,
                              :ps_vdate;

   if (SQL_ERROR_FOUND || NO_DATA_FOUND)
   {
      sprintf(table, "SYSTEM_OPTIONS, PERIOD");
      sprintf(err_data, "CURSOR FETCH: c_init");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL CLOSE c_init;

   if SQL_ERROR_FOUND
   {
      sprintf(table, "SYSTEM_OPTIONS, PERIOD");
      sprintf(err_data, "CURSOR CLOSE: c_init");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   /* create arrays to be size of max counter on restart_control */
   if (size_arrays() < 0) return(FATAL);

   return(OK);

}

/* Driving cursor to fetch the rma_no, transfer and shipments to purge */
EXEC SQL DECLARE c_purge_returns CURSOR FOR
   select wrh.rma_no
     from wf_return_head wrh,
          v_restart_wfreturn rwf
    where MONTHS_BETWEEN(TO_DATE(:ps_vdate,'YYYYMMDD'), wrh.create_datetime)
                                 >= :pl_wf_history_mths
      and not exists (select 1
                        from wf_billing_returns wbr
                       where wbr.rma_no = wrh.rma_no
                         and (wbr.extracted_ind = 'N'
                          or MONTHS_BETWEEN(TO_DATE(:ps_vdate,'YYYYMMDD'), wbr.extracted_date)
                                          <= :pl_wf_history_mths))
      and not exists (select 1
                        from tsfhead
                       where rma_no = wrh.rma_no)                                    
                                          
      and rwf.driver_value = wrh.rma_no
      and rwf.driver_name  = :ps_driver_name
      and rwf.num_threads  = TO_NUMBER(:ps_num_threads)
      and rwf.thread_val   = TO_NUMBER(:ps_thread_val);

/*-------------------------------------------------------*\
 * int process()
\*-------------------------------------------------------*/

int process(void)
{
   char *function = "process";
   int  li_ret_end_flag = 0;
   long ll_num_records_processed = 0;
   int i;

   EXEC SQL OPEN c_purge_returns;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"CURSOR OPEN: cursor = c_purge_returns");
      WRITE_ERROR(SQLCODE,function,"wf_return_head,tsfhead",err_data);
      return(FATAL);
   }

   while (!li_ret_end_flag)
   {
      EXEC SQL FOR :pi_commit_max_ctr
             FETCH c_purge_returns INTO :pa_ret_recs.s_rma_no;

      if (SQL_ERROR_FOUND)
      {
         sprintf(err_data,"CURSOR FETCH: cursor = c_purge_returns");
         WRITE_ERROR(SQLCODE,function,"wf_return_head,tsfhead",err_data);
         return(FATAL);
      }
      else if (NO_DATA_FOUND)
      {
            li_ret_end_flag = 1;
      }

      pl_rec_to_process = NUM_RECORDS_PROCESSED - ll_num_records_processed ;

      if (pl_rec_to_process != 0)
      {
          ll_num_records_processed = NUM_RECORDS_PROCESSED;

          for(i = 0; i < pl_rec_to_process; i++)
          {
             strcpy(ps_rma_no, pa_ret_recs.s_rma_no[i]);

             if(wf_delete_bill_returns() < 0) return(FATAL);
          }

          if(wf_delete_returns() < 0) return(FATAL);

          /* force commit for each driving cursor fetch */
          if (retek_force_commit(0) < 0)
             return(FATAL);

      } /*end if */

   } /* end while */

   return(OK);

} /* end process */




/*********************************************************************************************/
/* This function will delete returns records associated to the the rma_no idenetified  */
/* from the driving cursor                                                                   */
/*********************************************************************************************/

int wf_delete_returns(void)
{
   char *function = "wf_delete_returns";

   EXEC SQL FOR :pl_rec_to_process
      delete wf_return_detail
       where rma_no = :pa_ret_recs.s_rma_no;

   if SQL_ERROR_FOUND
   {
      snprintf(err_data,sizeof(err_data),"ARRAY DELETE: tsf_no = %s",
                       pa_ret_recs.s_rma_no);
      WRITE_ERROR(SQLCODE,function,"wf_return_detail",err_data);
      return(FATAL);
   }

   EXEC SQL FOR :pl_rec_to_process
      delete wf_return_head
       where rma_no = :pa_ret_recs.s_rma_no;

   if SQL_ERROR_FOUND
   {
      snprintf(err_data,sizeof(err_data),"ARRAY DELETE: tsf_no = %s",
                       pa_ret_recs.s_rma_no);
      WRITE_ERROR(SQLCODE,function,"wf_return_head",err_data);
      return(FATAL);
   }

   return(OK);
} /* End function wf_delete_returns */


/*********************************************************************************************/
/* This function will delete returns billing records associated to the the rma_no            */
/* idenetified from the driving cursor                                                       */
/*********************************************************************************************/

EXEC SQL DECLARE c_wf_billing_returns CURSOR FOR
   select wf_order_no,
          item,
          rma_no
     from wf_billing_returns
    where rma_no = :ps_rma_no
   order by 1;

int wf_delete_bill_returns(void)
{
   char *function = "wf_delete_bill_returns";

   long ll_wf_bill_rec_ctr = 0;
   long ll_wf_bill_del_ctr = 0;
   int  li_bill_end_flag   = 0;

   EXEC SQL OPEN c_wf_billing_returns;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"CURSOR OPEN: cursor = [%s], ps_rma_no = [%s]",
                       "c_wf_billing_returns", ps_rma_no);
      strcpy(table,"wf_billing_returns");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   while (!li_bill_end_flag)
   {
      EXEC SQL FOR :pi_commit_max_ctr
         FETCH c_wf_billing_returns INTO :bill_recs.wf_order_no,
                                         :bill_recs.item,
                                         :bill_recs.rma_no;

      if SQL_ERROR_FOUND
      {
         sprintf(err_data,"CURSOR FETCH: cursor = [%s], ps_rma_no = [%s]",
                          "c_wf_billing_returns", ps_rma_no);
         strcpy(table,"wf_billing_returns");
         WRITE_ERROR(SQLCODE,function,table,err_data);
         return(-1);
      }
      else if NO_DATA_FOUND li_bill_end_flag = 1;

      ll_wf_bill_del_ctr = NUM_RECORDS_PROCESSED - ll_wf_bill_rec_ctr;

      if (ll_wf_bill_del_ctr != 0)
      {
         ll_wf_bill_rec_ctr = NUM_RECORDS_PROCESSED;

         EXEC SQL FOR :ll_wf_bill_del_ctr
            delete wf_billing_returns
             where wf_order_no = :bill_recs.wf_order_no
               and item        = :bill_recs.item
               and rma_no      = :bill_recs.rma_no;

         if SQL_ERROR_FOUND
         {
            snprintf(err_data,sizeof(err_data),"ARRAY DELETE: wf order_no = [%s], item = [%s], rma_no = [%s]",
                             bill_recs.wf_order_no,
                             bill_recs.item,
                             ps_rma_no);
            WRITE_ERROR(SQLCODE,function,"wf_billing_returns",err_data);
            return(FATAL);
         }
      }
   }

   return(OK);
} /* End function wf_delete_bill_returns */


/*-------------------------------------------------------*\
 * int size_arrays()
\*-------------------------------------------------------*/

int size_arrays(void)
{
   char *function = "size_arrays";
   int no_mem = 0;

   /* size common update array components */
   if ((pa_ret_recs.s_rma_no = (char (*)[NULL_RMA_NO])
         calloc (pi_commit_max_ctr, NULL_RMA_NO)) == NULL)
      no_mem = 1;

    if ((bill_recs.wf_order_no = (char (*)[NULL_WF_ORDER_NO])calloc
       (pi_commit_max_ctr, NULL_WF_ORDER_NO)) == NULL)
      no_mem = 1;

   if ((bill_recs.item = (char (*)[NULL_ITEM])calloc
       (pi_commit_max_ctr, NULL_ITEM)) == NULL)
      no_mem = 1;

   if ((bill_recs.rma_no = (char (*)[NULL_RMA_NO])calloc
       (pi_commit_max_ctr, NULL_RMA_NO)) == NULL)
      no_mem = 1;

   if (no_mem)
   {
      sprintf(err_data,"CALLOC :Unable to allocate memory for ship_recs");
      WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
      return(FATAL);
   }

   return(0);
}   /* end function size_arrays() */


/*-------------------------------------------------------*\
 * int final()
\*-------------------------------------------------------*/

int final(void)
{
   char* function = "final";
   int li_final_return = 0;

   if (li_final_return && (gi_error_flag == 0))
      gi_error_flag = 3;

   if (retek_close() < 0)
      li_final_return = -1;

   return(li_final_return);

}
