/* BEGIN DOC

Program Name: wfordprg.pc

Description: This program closes the Franchise orders after
             'wf_history_mths' number of days specified in the system_options
             table from the order detail not_after_date of wf_order_detail table.
             All the associated transfers, pos,allocation and store orders 
             should be purged from its respective tables prior to 
             purge the orders.

Table                    Index     Select     Insert     Update   Delete
--------------------------------------------------------------------------
PERIOD                    NO       YES         NO        NO       NO
SYSTEM_OPTIONS            NO       YES         NO        NO       NO
WF_ORDER_HEAD             NO       YES         NO        NO       YES
WF_ORDER_DETAIL           NO       YES         NO        NO       YES
WF_BILLING_SALES          NO       YES         NO        NO       YES
WF_ORDER_AUDIT            NO       NO          NO        NO       YES
WF_ORDER_EXP              NO       NO          NO        NO       YES
TSFHEAD                   NO       YES         NO        NO       NO
ORDHEAD                   NO       YES         NO        NO       NO
ALLOC_DETAIL              NO       YES         NO        NO       NO
STORE_ORDERS              NO       YES         NO        NO       NO
__________________________________________________________________________

END DOC */

#include "retek.h"
#include "retek_2.h"

EXEC SQL INCLUDE SQLCA.H;
long SQLCODE;

init_parameter parameter[] =
{
   /*NAME ------------ TYPE --------SUB_TYPE */
   "commit_max_ctr",   "uint",      "",
   "num_threads",      "string",    "",
   "thread_val",       "string",    "",
   "wf_order_no",      "string",    "S"
};

#define NUM_INIT_PARAMETERS   (sizeof(parameter)/(sizeof(init_parameter)))

/* Restart control variables */
uint pl_commit_max_ctr;
char ps_num_threads[NULL_THREAD];
char ps_thread_val[NULL_THREAD];

/*** variables that will match those stored in the restart start array ***/
char  ps_restart_wf_order_no[NULL_WF_ORDER_NO] = "";

struct wf_order_no_array{
   char  (*wf_order_no)[NULL_WF_ORDER_NO];
} wf_order_no_recs;


struct bill_array{
   char  (*wf_order_no)[NULL_WF_ORDER_NO];
   char  (*item)[NULL_ITEM];
} bill_recs;

/***variable declarations for array processing ***/
long i = 0;                            /* for loop counter for fetched array */
long j = 0;                            /* for loop counter for fetched array */
uint pi_wf_order_no_to_process = 0;    /* number of records to process (# fetched) */
long wf_order_no_total_rec_ctr = 0;    /* total index of fetch array */

/* FUNCTION PROTOTYPES */
int main(int argc, char* argv[]);
int init(void);
int process(void);
int size_arrays(void);
int final(void);
int del_wf_orders(void);
int del_bill_sales(void);


int main(int argc, char* argv[])
{
   char* function = "main";
   int   li_init_results = 0;
   char  ls_log_message[NULL_ERROR_MESSAGE] = "";

   strcpy(PROGRAM,"wfordprg");

   if (argc < 2)
   {
      fprintf(stderr, "Usage: wfordprg userid/passwd\n");
      return(FAILED);
   }

   if (LOGON(argc, argv) < 0)
      return(FAILED);

   if ((li_init_results = init()) < 0)
      gi_error_flag = 2;

   if (li_init_results != NO_THREAD_AVAILABLE)
   {
      if (li_init_results == OK)
      {
         if (process() < 0)
            gi_error_flag = 1;
      }

      if (final() < 0)
      {
         if (gi_error_flag == 0)
            gi_error_flag = 3;
      }
   }

   if (gi_error_flag == 2)
   {
      LOG_MESSAGE("Aborted in init");
      return(FAILED);
   }
   else if (gi_error_flag == 1)
   {
      sprintf(ls_log_message,"Thread %s - Aborted in process",ps_thread_val);
      LOG_MESSAGE(ls_log_message);
      return (FAILED);
   }
   else if (gi_error_flag == 3)
   {
      sprintf(ls_log_message,"Thread %s - Aborted in final",ps_thread_val);
      LOG_MESSAGE(ls_log_message);
      return (FAILED);
   }
   else if (li_init_results == NO_THREAD_AVAILABLE)
   {
      LOG_MESSAGE("Terminated - No threads available");
      return(NO_THREADS);
   }
   else
   {
      sprintf(ls_log_message,"Thread %s - Terminated Successfully",ps_thread_val);
      LOG_MESSAGE(ls_log_message);
   }

   return (SUCCEEDED);
}  /* End of main() */


EXEC SQL DECLARE c_sys_info CURSOR FOR
   SELECT system_options.wf_history_months,
          TO_CHAR((period.vdate), 'YYYYMMDD')
     FROM system_options,
          period;

long    pl_wf_history_mths = 0;
char    ps_vdate[NULL_DATE];

int init(void)
{
    char *function = "init";

    int  li_restart_init_results;

    /* call restart_init to initialize restart process */
    li_restart_init_results = retek_init(NUM_INIT_PARAMETERS,
                                         parameter,
                                         &pl_commit_max_ctr,
                                         ps_num_threads,
                                         ps_thread_val,
                                         ps_restart_wf_order_no);

    if(li_restart_init_results != 0)
       return(li_restart_init_results);

    EXEC SQL OPEN c_sys_info;
    if SQL_ERROR_FOUND
    {
        sprintf(err_data,"CURSOR OPEN: cursor = c_sys_info");
        strcpy(table,"system_options, period");
        WRITE_ERROR(SQLCODE,function,table,err_data);
        return(-1);
    }
    EXEC SQL FETCH c_sys_info INTO :pl_wf_history_mths,
                                   :ps_vdate;

    if ((SQL_ERROR_FOUND) || (NO_DATA_FOUND))
    {
        sprintf(err_data,"CURSOR FETCH: cursor = c_sys_info");
        strcpy(table,"system_options, period");
        WRITE_ERROR(SQLCODE,function,table,err_data);
        return(-1);
    }

    /* create arrays to be size of max counter on restart_control */
    if (size_arrays() < 0) return(-1);

    return(0);

}  /* end function init */

char   os_wf_order_no[NULL_WF_ORDER_NO];

/* cursor for all histories */
EXEC SQL DECLARE c_wf_order_detail CURSOR FOR
   select woh.wf_order_no
     from wf_order_head woh,
          v_restart_wforder rv
    where not exists (select 'x'
                        from wf_order_detail wod
                       where wod.wf_order_no = woh.wf_order_no
                         and MONTHS_BETWEEN(TO_DATE(:ps_vdate,'YYYYMMDD'), wod.not_after_date)
                                            <= :pl_wf_history_mths)
      and not exists (select 'x'
                        from wf_return_detail wrd
                       where wrd.wf_order_no = woh.wf_order_no)
      and not exists (select 'x'
                        from wf_billing_sales wbs
                       where wbs.wf_order_no = woh.wf_order_no
                         and (extracted_ind  = 'N'
                          or MONTHS_BETWEEN(TO_DATE(:ps_vdate,'YYYYMMDD'), extracted_date)
                                            <= :pl_wf_history_mths))
      and not exists (select 'x'
                        from tsfhead tsf
                       where tsf.wf_order_no = woh.wf_order_no)
      and not exists (select 'x'
                        from ordhead ord
                       where ord.wf_order_no = woh.wf_order_no)
      and not exists (select 'x'
                        from alloc_detail ad
                       where ad.wf_order_no = woh.wf_order_no)   
      and not exists (select 'x'
                        from store_orders st
                       where st.wf_order_no = woh.wf_order_no)                               
      and rv.driver_value = woh.wf_order_no
      and rv.num_threads  = TO_NUMBER(:ps_num_threads)
      and rv.thread_val   = TO_NUMBER(:ps_thread_val)
   order by 1;

int process(void)
{
   char *function = "process";
   int  li_wf_order_no_end_flag = 0;

   EXEC SQL OPEN c_wf_order_detail;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"CURSOR OPEN: cursor = c_wf_order_detail");
      WRITE_ERROR(SQLCODE,function,"wf_order_detail",err_data);
      return(-1);
   }

   while (!li_wf_order_no_end_flag)
   {
      EXEC SQL FOR :pl_commit_max_ctr
         FETCH c_wf_order_detail INTO :wf_order_no_recs.wf_order_no;

      if SQL_ERROR_FOUND
      {
         sprintf(err_data,"CURSOR FETCH: cursor = c_wf_order_detail");
         WRITE_ERROR(SQLCODE,function,"wf_order_detail",err_data);
         return(-1);
      }
      else
      {
         if (NO_DATA_FOUND)
            li_wf_order_no_end_flag = 1;
      }

      pi_wf_order_no_to_process = NUM_RECORDS_PROCESSED - wf_order_no_total_rec_ctr;

      if (pi_wf_order_no_to_process != 0)
      {

         wf_order_no_total_rec_ctr = NUM_RECORDS_PROCESSED;

         for (i=0; i < pi_wf_order_no_to_process; i++)
         {
            /* set the bind variable for the order fetch */
            strcpy(os_wf_order_no,wf_order_no_recs.wf_order_no[i]);

            /* delete the records from wf_billing_sales */
            if (del_bill_sales() < 0 ) return(-1) ;

            /*wf_order_no_recs.wf_order_no[i-1][NULL_WF_ORDER_NO] = '\0';*/
            strcpy(ps_restart_wf_order_no,wf_order_no_recs.wf_order_no[i]);

         }/* end of for loop */

         if (del_wf_order() < 0 ) return(-1) ;


         /** standard commit logic **/
         if (retek_force_commit(1,
                                ps_restart_wf_order_no) < 0) return (-1);

      }/* end of if (pi_wf_order_no_to_process != 0)   */

   } /* end of while wf_order_no loop */

   return(0);

}  /*end function process() */




/*********************************************************************************************/
/* This function will delete order records idenetified from the driving cursor               */
/* idenetified from the driving cursor                                                       */
/*********************************************************************************************/
int del_wf_order(void)
{
   char *function="del_wf_order";

   EXEC SQL FOR :pl_commit_max_ctr
      delete wf_order_audit
       where wf_order_no = :wf_order_no_recs.wf_order_no;

   if(SQL_ERROR_FOUND)
   {
      sprintf(table, "wf_order_audit");
      snprintf(err_data,sizeof(err_data),"ARRAY DELETE: wf_order_no=%s", wf_order_no_recs.wf_order_no);
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   EXEC SQL FOR :pl_commit_max_ctr
      delete wf_order_exp
       where wf_order_no = :wf_order_no_recs.wf_order_no;

   if(SQL_ERROR_FOUND)
   {
      sprintf(table, "wf_order_exp");
      snprintf(err_data,sizeof(err_data),"ARRAY DELETE: wf_order_id=%s", wf_order_no_recs.wf_order_no);
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   EXEC SQL FOR :pl_commit_max_ctr
      delete wf_order_detail
       where wf_order_no = :wf_order_no_recs.wf_order_no;

   if(SQL_ERROR_FOUND)
   {
      sprintf(table, "wf_order_detail");
      snprintf(err_data,sizeof(err_data),"ARRAY DELETE: wf_order_id=%s", wf_order_no_recs.wf_order_no);
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   EXEC SQL FOR :pl_commit_max_ctr
      delete wf_order_head
       where wf_order_no = :wf_order_no_recs.wf_order_no;

   if(SQL_ERROR_FOUND)
   {
      sprintf(table, "wf_order_head");
      snprintf(err_data,sizeof(err_data),"ARRAY DELETE: wf_order_id=%s", wf_order_no_recs.wf_order_no);
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   return(0);
}/* end function del_wf_order() */

EXEC SQL DECLARE c_wf_billing_sales CURSOR FOR
   select wf_order_no,
          item
     from wf_billing_sales
    where wf_order_no = :os_wf_order_no
   order by 1;

int del_bill_sales(void)
{
   char *function = "del_bill_sales";

   long ll_wf_bill_rec_ctr = 0;
   long ll_wf_bill_del_ctr = 0;
   int  li_bill_end_flag   = 0;

   EXEC SQL OPEN c_wf_billing_sales;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"CURSOR OPEN: cursor = %s, wf_order_no = %s",
                    "c_wf_billing_sales", os_wf_order_no);
      strcpy(table,"wf_billing_sales");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   while (!li_bill_end_flag)
   {
      EXEC SQL FOR :pl_commit_max_ctr
      FETCH c_wf_billing_sales INTO :bill_recs.wf_order_no,
                                    :bill_recs.item;

      if SQL_ERROR_FOUND
      {
         sprintf(err_data,"CURSOR FETCH: cursor = %s, wf_order_id = %s",
                       "c_wf_billing_sales", os_wf_order_no);
         strcpy(table,"wf_billing_sales");
         WRITE_ERROR(SQLCODE,function,table,err_data);
         return(-1);
      }
      else if NO_DATA_FOUND li_bill_end_flag = 1;

      ll_wf_bill_del_ctr = NUM_RECORDS_PROCESSED - ll_wf_bill_rec_ctr;

      if (ll_wf_bill_del_ctr != 0)
      {
         ll_wf_bill_rec_ctr = NUM_RECORDS_PROCESSED;

         EXEC SQL FOR :ll_wf_bill_del_ctr
            delete wf_billing_sales
             where wf_order_no = :bill_recs.wf_order_no
               and item = :bill_recs.item;

         if SQL_ERROR_FOUND
         {
            snprintf(err_data,sizeof(err_data),"ARRAY DELETE: order_no = [%s], item = [%s]",
                             bill_recs.wf_order_no,
                             bill_recs.item);
            WRITE_ERROR(SQLCODE,function,"wf_billing_sales",err_data);
            return(FATAL);
         }

      }
   }

   return(OK);
} /* End function del_bill_sales */


int size_arrays(void)
{
   char *function = "size_arrays";
   int no_mem = 0;

   /* size common update array components */
   

   if ((wf_order_no_recs.wf_order_no = (char (*)[NULL_WF_ORDER_NO])calloc
       (pl_commit_max_ctr, NULL_WF_ORDER_NO)) == NULL) no_mem = 1;

   if ((bill_recs.wf_order_no = (char (*)[NULL_WF_ORDER_NO])calloc
       (pl_commit_max_ctr, NULL_WF_ORDER_NO)) == NULL) no_mem = 1;

   if ((bill_recs.item = (char (*)[NULL_ITEM])calloc
       (pl_commit_max_ctr, NULL_ITEM)) == NULL) no_mem = 1;

   if (no_mem)
   {
      sprintf(err_data,"CALLOC :Unable to allocate memory for ship_recs");
      WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
      return(-1);
   }

   return(0);
} /* end function size_arrays() */


int final(void)
{
   char *function = "final";
   int li_final_return = 0;

   /*** Restart/recovery close logic ***/

   if (li_final_return && !gi_error_flag)
      gi_error_flag = 3;

   if (retek_close() < 0)
      li_final_return = -1;

   return(li_final_return);
} /* end function final() */
