#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  stockcountprocess.ksh
#
#  Desc:  This shell script updates the RMS stock count tables with uploaded inventory
#         counts. The inventory information is read from the SVC_STKUPLD_* tables.
#         These tables were populated by a previous batch (stockcountupload.ksh).
#
#         The following are done in this script
#         1. Retrieve the maximum number of concurrent threads that can run for this
#            batch. This is retrieved from the RMS_PLSQL_BATCH_CONFIG table.
#         2. Get the process ID for the file uploaded in the previous batch.
#            This is retrieved from the SVC_STKUPLD_STATUS table.
#         3. Get the chunk IDs to be processed. This is retrieved from the
#            SVC_STKUPLD_STATUS table.
#         4. Call the core stock upload PL/SQL package for each chunk ID.
#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='stockcountprocess.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
"
}

#-------------------------------------------------------------------------
# Function Name: GET_MAX_THREADS
# Purpose      : Retrieves the maximum number of concurrent threads
#                that can be run for this batch.
#-------------------------------------------------------------------------
function GET_MAX_THREADS
{
   parallelThreads=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select max_concurrent_threads
     from rms_plsql_batch_config
    where program_name = 'CORESVC_STOCK_UPLOAD_SQL';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${parallelThreads} - Error in getting the maximum thread." "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned number of parallel threads
   if [ -z ${parallelThreads} ]; then
      LOG_ERROR "Unable to retrieve the number of threads from the RMS_PLSQL_BATCH_CONFIG table." "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "The number of concurrent threads for processing is ${parallelThreads}." "GET_MAX_THREADS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function Name: GET_CHUNKS
# Purpose      : populates thre restartArr array..
#-------------------------------------------------------------------------
function GET_CHUNKS
{
   stockProcessId=$1
   
   # Retrieve chunks to process into an indexed array
   set -A restartArr `$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select distinct chunk_id
     from svc_stkupld_status
    where process_id = ${stockProcessId}
      and status = 'N'
      and chunk_id <> 0
      order by chunk_id;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${restartArr} - Error in getting the chunk ID for the Stock Process Id ${stockProcessId}." "GET_CHUNKS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned restart array
   if [ -z ${restartArr} ]; then
      LOG_ERROR "Unable to retrieve the chunk IDs for the Stock Process Id ${stockProcessId}." "GET_CHUNKS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "Chunk IDs retrieved for the Stock Process Id ${stockProcessId}." "GET_CHUNKS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS
# Purpose      : 
#-------------------------------------------------------------------------

function PROCESS
{  
   stockProcessId=$1
   
   # Retrieve chunks to processinto an indexed array
   GET_CHUNKS  ${stockProcessId}

   # Iterate through the chunk IDs and call the core logic for each
   arr_index=0
   while [ ${arr_index} -lt  ${#restartArr[*]} ]
   do
      PROCESS_STOCK_COUNT  ${stockProcessId} ${restartArr[${arr_index}]}
      let arr_index=${arr_index}+1;
   done
   # Wait for all of the threads to complete
   wait
   
   return ${OK}

}

#-------------------------------------------------------------------------
# Function Name: PROCESS_STOCK_COUNT
# Purpose      :
#-------------------------------------------------------------------------
function PROCESS_STOCK_COUNT
{
   stockProcessId=$1
   chunkVal=$2

   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT CORESVC_STOCK_UPLOAD_SQL.PROCESS_STOCK_COUNT(:GV_script_error,
                                                             ${stockProcessId},
                                                             ${chunkVal}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "Function call PROCESS_STOCK_COUNT for Stock Count Process Id: ${stockProcessId}, Chunk ID ${chunkVal} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "Stock Count Process Id: ${stockProcessId}, Chunk ID ${chunkVal} successfully processed." "PROCESS_STOCK_COUNT" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CLEANUP
# Purpose      : clean up the SVC_STKUPLD_tables for the processed records
#-------------------------------------------------------------------------
function CLEANUP
{ 
     sqlTxt="
        BEGIN
           DELETE FROM SVC_STKUPLD_FHEAD
                 WHERE STATUS = 'P';
           
           DELETE FROM SVC_STKUPLD_FDETL
                 WHERE STATUS = 'P';
           
           DELETE FROM SVC_STKUPLD_FTAIL
                 WHERE STATUS = 'P';
                 
           DELETE FROM SVC_STKUPLD_STATUS
                 WHERE STATUS = 'P';
                 
        EXCEPTION
           when OTHERS then
              ROLLBACK;
              :GV_script_error := SQLERRM;
              :GV_return_code := ${FATAL};
        END;"
  
     EXEC_SQL ${sqlTxt}
  
     if [[ $? -ne ${OK} ]]; then
        echo "Function call CLEANUP for processed stockcount failed." >>${ERRORFILE}
        exit ${FATAL}
     else
        LOG_MESSAGE "Stock count cleanup successfully processed." "CLEANUP" ${OK} ${LOGFILE} ${pgmName}
     fi
     echo "Clean up successful"

    return ${OK}
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
LOG_MESSAGE "stockcountprocess.ksh started by ${USER}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}


# Get the maximum concurrent threads from the configuration table
GET_MAX_THREADS

#Get all process id's
set -A process_id `$ORACLE_HOME/bin/sqlplus -s $connectStr <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off

select distinct process_id
  from svc_stkupld_status
 where status = 'N';
exit;
EOF`

num_process=${#process_id[*]}

arr_index1=0;
while [ $arr_index1 -lt num_process ]
do
   LOG_MESSAGE "Started Stock Count Process id : ${process_id[arr_index1]}." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   
   if [ `jobs | wc -l` -lt ${parallelThreads} ]
   then
   (  
     PROCESS ${process_id[arr_index1]}     
   ) &
   else
      # Loop until a thread becomes available
      while [ `jobs | wc -l` -ge ${parallelThreads} ]
      do
         sleep 1
      done
      ( 
        PROCESS ${process_id[arr_index1]} 
      ) &
   fi        
   let arr_index1=$arr_index1+1;
done

# Wait for all threads to complete
wait

# clean-up the SVC_tables for all the 'P'rocessed records.
CLEANUP

LOG_MESSAGE "stockcountprocess.ksh finished for ${USER}" "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

exit ${OK}
