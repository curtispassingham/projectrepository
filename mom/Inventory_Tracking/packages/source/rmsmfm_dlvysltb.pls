CREATE OR REPLACE PACKAGE BODY RMSMFM_DLVYSLT AS
--------------------------------------------------------------------------------
LP_error_status   VARCHAR2(1) := NULL;

PROGRAM_ERROR   EXCEPTION;
---------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_DLVY_SLT(O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_message        IN OUT   nocopy RIB_OBJECT,
                                O_bus_obj_id     IN OUT   nocopy RIB_BUSOBJID_TBL,
                                O_message_type   IN OUT   DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                                I_queue_rec      IN       DELIVERY_SLOT_MFQUEUE%ROWTYPE,
                                I_rowid          IN       ROWID)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_message        IN OUT   RIB_OBJECT,
                        O_status_code    IN OUT   VARCHAR2,
                        O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message_type   IN OUT   DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                        O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                        O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                        I_seq_no         IN       DELIVERY_SLOT_MFQUEUE.SEQ_NO%TYPE,
                        I_slt_id         IN       DELIVERY_SLOT_MFQUEUE.DELIVERY_SLOT_ID%TYPE);
---------------------------------------------------------------------------------
FUNCTION ADDTOQ (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_status                OUT   VARCHAR2,
                 I_message_type       IN       DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_delivery_slot_id   IN       DELIVERY_SLOT_MFQUEUE.DELIVERY_SLOT_ID%TYPE,
                 I_delivery_slot_desc IN       DELIVERY_SLOT_MFQUEUE.DELIVERY_SLOT_DESC%TYPE,
                 I_delivery_sequence  IN       DELIVERY_SLOT_MFQUEUE.DELIVERY_SLOT_SEQUENCE%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSMFM_DLVYSLT.ADDTOQ';

BEGIN
   insert into DELIVERY_SLOT_MFQUEUE(seq_no,
                                     pub_status,
                                     message_type,
                                     delivery_slot_id,
                                     delivery_slot_desc,
                                     delivery_slot_sequence,
                                     family,
                                     thread_no,
                                     transaction_number,
                                     transaction_time_stamp)
                              values(DLVYSLT_MFSEQUENCE.NEXTVAL,
                                     'U',
                                     I_message_type,
                                     I_delivery_slot_id,
                                     I_delivery_slot_desc,
                                     I_delivery_sequence,
                                     RMSMFM_DLVYSLT.FAMILY,
                                     1,
                                     DLVYSLT_MFSEQUENCE.NEXTVAL,
                                     sysdate);
   O_status := API_CODES.SUCCESS;
   ---
   return true;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;
--------------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      IN OUT        VARCHAR2,
                 O_error_msg        IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                 O_message_type     IN OUT        DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                 O_message          IN OUT        RIB_OBJECT,
                 O_bus_obj_id       IN OUT        RIB_BUSOBJID_TBL,
                 O_routing_info     IN OUT        RIB_ROUTINGINFO_TBL,
                 I_num_threads      IN            NUMBER DEFAULT 1,
                 I_thread_val       IN            NUMBER DEFAULT 1)

IS

   L_dlvyslt_row   DELIVERY_SLOT_MFQUEUE%ROWTYPE;
   L_rowid         ROWID;
   L_hospital      VARCHAR2(1) := 'N';
   L_slt_id        VARCHAR2(15);

   CURSOR C_DLVY_SLOT_MFQUEUE is
      select ds.seq_no,
             ds.message_type,
             ds.delivery_slot_id,
             ds.delivery_slot_desc,
             ds.delivery_slot_sequence,
             ds.rowid
        from delivery_slot_mfqueue ds
       where ds.seq_no=(select min(ds2.seq_no)
                           from delivery_slot_mfqueue ds2
                          where ds2.thread_no = I_thread_val
                            and ds2.pub_status = 'U')
         and ds.thread_no = I_thread_val;

   CURSOR C_HOSP_DLVY is
      select 'Y'
        from delivery_slot_mfqueue
       where delivery_slot_id = L_slt_id
         and pub_status = API_CODES.HOSPITAL;

BEGIN
   O_status_code  := API_CODES.NO_MSG;
   O_error_msg    := NULL;
   O_message_type := NULL;
   O_message      := NULL;
   O_bus_obj_id   := NULL;
   O_routing_info := RIB_ROUTINGINFO_TBL();

   open  C_DLVY_SLOT_MFQUEUE;
   ---
   fetch C_DLVY_SLOT_MFQUEUE into L_dlvyslt_row.seq_no,
                                  O_message_type,
                                  L_dlvyslt_row.delivery_slot_id,
                                  L_dlvyslt_row.delivery_slot_desc,
                                  L_dlvyslt_row.delivery_slot_sequence,
                                  L_rowid;
   ---
   if C_DLVY_SLOT_MFQUEUE%NOTFOUND then
      close C_DLVY_SLOT_MFQUEUE;
      return;
   end if;

   close C_dlvy_slot_mfqueue;
   ---

   if L_dlvyslt_row.delivery_slot_id is not NULL then
      L_slt_id := L_dlvyslt_row.delivery_slot_id;

      open C_HOSP_DLVY;
      fetch C_HOSP_DLVY into L_hospital;
      close C_HOSP_DLVY;
   end if;

   if L_hospital = 'Y' then
      O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                        NULL,
                                        NULL,
                                        NULL);
      raise PROGRAM_ERROR;
   end if;

   if lower(O_message_type) in (RMSMFM_DLVYSLT.SLT_ADD,
                                RMSMFM_DLVYSLT.SLT_DEL,
                                RMSMFM_DLVYSLT.SLT_UPD) then

      if PROCESS_QUEUE_DLVY_SLT(O_error_msg,
                                O_message,
                                O_bus_obj_id,
                                O_message_type,
                                L_dlvyslt_row,
                                L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message IS NOT NULL then
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id  := RIB_BUSOBJID_TBL(L_dlvyslt_row.delivery_slot_id);
      end if;

   end if;

   return;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_message,
                    O_status_code,
                    O_error_msg,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_dlvyslt_row.seq_no,
                    L_slt_id);

END GETNXT;
-------------------------------------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code    IN OUT   VARCHAR2,
                    O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_message        IN OUT   RIB_OBJECT,
                    O_message_type   IN OUT   DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                    O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                    O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT     IN       RIB_OBJECT)
   IS

   L_seq_no        DELIVERY_SLOT_MFQUEUE.SEQ_NO%TYPE   := NULL;
   L_dlvyslt_row   DELIVERY_SLOT_MFQUEUE%ROWTYPE;
   L_rowid         ROWID;
   L_hospital      VARCHAR2(1) := 'N';
   L_slt_id        VARCHAR2(15);

   cursor C_RETRY_QUEUE is
      select seq_no,
             delivery_slot_id,
             delivery_slot_desc,
             delivery_slot_sequence,
             message_type,
             rowid
        from delivery_slot_mfqueue
       where seq_no = L_seq_no;

BEGIN

   LP_error_status := API_CODES.HOSPITAL;

   O_status_code  := API_CODES.NO_MSG;
   O_error_msg    := NULL;
   O_message_type := NULL;
   O_message      := NULL;
   O_bus_obj_id   := NULL;

   L_seq_no := O_routing_info(1).value;
   O_routing_info := RIB_ROUTINGINFO_TBL();

   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_dlvyslt_row.seq_no,
                            L_dlvyslt_row.delivery_slot_id,
                            L_dlvyslt_row.delivery_slot_desc,
                            L_dlvyslt_row.delivery_slot_sequence,
                            O_message_type,
                            L_rowid;

   if C_RETRY_QUEUE%NOTFOUND then
      close C_RETRY_QUEUE;
      return;
   end if;

   close C_RETRY_QUEUE;

   --Value to pass to HANDLE ERRORS in case of an error
   if L_dlvyslt_row.delivery_slot_id is not NULL then
      L_slt_id := L_dlvyslt_row.delivery_slot_id;
   end if;

   if lower(O_message_type) in (RMSMFM_DLVYSLT.SLT_ADD,
                                RMSMFM_DLVYSLT.SLT_DEL,
                                RMSMFM_DLVYSLT.SLT_UPD) then

      if PROCESS_QUEUE_DLVY_SLT(O_error_msg,
                                O_message,
                                O_bus_obj_id,
                                O_message_type,
                                L_dlvyslt_row,
                                L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message IS NOT NULL then
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id  := RIB_BUSOBJID_TBL(L_dlvyslt_row.delivery_slot_id);
      end if;

   end if;

   return;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_message,
                    O_status_code,
                    O_error_msg,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_dlvyslt_row.seq_no,
                    L_slt_id);

END PUB_RETRY;
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_DLVY_SLT(O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_message        IN OUT   nocopy RIB_OBJECT,
                                O_bus_obj_id     IN OUT   nocopy RIB_BUSOBJID_TBL,
                                O_message_type   IN OUT   DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                                I_queue_rec      IN       DELIVERY_SLOT_MFQUEUE%ROWTYPE,
                                I_rowid          IN       ROWID)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)         := 'RMSMFM_DLVYSLT.PROCESS_QUEUE_DLVY_SLT';
   L_dlvyslt_rec   "RIB_DeliverySlotDesc_REC"  := NULL;
   L_dlvysltref_rec "RIB_DeliverySlotRef_REC"  := NULL;


BEGIN

   if lower(O_message_type) in (RMSMFM_DLVYSLT.SLT_ADD,
                                RMSMFM_DLVYSLT.SLT_UPD) then

      L_dlvyslt_rec := "RIB_DeliverySlotDesc_REC"(0,
                                                  I_queue_rec.delivery_slot_id,
                                                  I_queue_rec.delivery_slot_desc,
                                                  I_queue_rec.delivery_slot_sequence);
      O_message := L_dlvyslt_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      delete from delivery_slot_mfqueue where rowid = I_rowid;

   elsif lower(O_message_type) = RMSMFM_DLVYSLT.SLT_DEL then

      L_dlvysltref_rec := "RIB_DeliverySlotRef_REC"(0,
                                                    I_queue_rec.delivery_slot_id);

      O_message := L_dlvysltref_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      delete from delivery_slot_mfqueue where rowid = I_rowid;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_DLVY_SLT;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_message        IN OUT   RIB_OBJECT,
                        O_status_code    IN OUT   VARCHAR2,
                        O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message_type   IN OUT   DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                        O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                        O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                        I_seq_no         IN       DELIVERY_SLOT_MFQUEUE.SEQ_NO%TYPE,
                        I_slt_id         IN       DELIVERY_SLOT_MFQUEUE.DELIVERY_SLOT_ID%TYPE)
   IS

   L_error_type        VARCHAR2(5)           := NULL;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then

      O_bus_obj_id   := RIB_BUSOBJID_TBL(I_slt_id);
      O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                I_seq_no,
                                                                NULL,
                                                                NULL,
                                                                NULL,
                                                                NULL));
      update delivery_slot_mfqueue
         set pub_status = LP_error_status
       where seq_no = I_seq_no;

   end if;

   SQL_LIB.API_MSG(L_error_type,
                   O_error_msg);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_DLVYSLT');
END HANDLE_ERRORS;
--------------------------------------------------------------------------------
END RMSMFM_DLVYSLT;
/
