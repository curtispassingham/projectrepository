CREATE OR REPLACE PACKAGE CORESVC_WF_RETURN_UPLOAD_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- Function Name: CREATE_RMA
-- Purpose:       This public function will create the wf returns.The function
--                will first validate the data in the staging tables .Once the data validation
--                is done, it populates the return head and detail collections.These collections are then
--                used to persist the wf_return_head and wf_return_detail table.After this a call to wf_return_sql.approve
--                is made in order to approve the newly generated RMA no's.
-- Called by:     wfretupld.ksh

FUNCTION CREATE_RMA(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id    IN  SVC_WF_RET_HEAD.PROCESS_ID%TYPE,
                    I_chunk_id      IN  SVC_WF_RET_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
END CORESVC_WF_RETURN_UPLOAD_SQL;
/