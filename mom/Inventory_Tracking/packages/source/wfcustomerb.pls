CREATE OR REPLACE PACKAGE BODY WF_CUSTOMER_SQL AS

--------------------------------------------------------------------------------------------------------------
FUNCTION WF_CUSTOMER_GROUP_ID_EXISTS (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists             IN OUT   BOOLEAN,
                                      I_wf_cust_group_id   IN       WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64)   := 'WF_CUSTOMER_SQL.WF_CUSTOMER_GROUP_ID_EXISTS';
   L_cust_grp_exist   VARCHAR2(1);

   cursor C_WF_CUST_GRP_EXISTS is
      select 'x'
        from wf_customer_group
       where wf_customer_group_id = I_wf_cust_group_id;

BEGIN
   if I_wf_cust_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_cust_group_id',
                                            L_program,
                                            NULL);
      return FALSE;
   else
      --
      O_exists := FALSE;
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_WF_CUST_GRP_EXISTS',
                       'WF_CUSTOMER_GROUP',
                       I_wf_cust_group_id);
      open C_WF_CUST_GRP_EXISTS;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_WF_CUST_GRP_EXISTS',
                       'WF_CUSTOMER_GROUP',
                       I_wf_cust_group_id);
      fetch C_WF_CUST_GRP_EXISTS into L_cust_grp_exist;
      --
      if L_cust_grp_exist = 'x' then
         O_exists := TRUE;
      end if;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_WF_CUST_GRP_EXISTS',
                       'WF_CUSTOMER_GROUP',
                       I_wf_cust_group_id);
      close C_WF_CUST_GRP_EXISTS;
      --
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_WF_CUST_GRP_EXISTS%ISOPEN then
         close C_WF_CUST_GRP_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END WF_CUSTOMER_GROUP_ID_EXISTS;
--------------------------------------------------------------------------------------------------------------
FUNCTION GET_WF_CUSTOMER_GROUP_INFO (O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_wf_customer_group_name   IN OUT   WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_NAME%TYPE,
                                     I_wf_customer_group_id     IN       WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(64)   := 'WF_CUSTOMER_SQL.GET_WF_CUSTOMER_GROUP_INFO';
   L_wf_cust_grp_id_exist   BOOLEAN;

   cursor C_GET_CUST_GROUP_INFO is
      select wf_customer_group_name
        from wf_customer_group
       where wf_customer_group_id = I_wf_customer_group_id;

BEGIN
   if I_wf_customer_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_customer_group_id',
                                            L_program,
                                            NULL);
      return FALSE;
   else
      if WF_CUSTOMER_GROUP_ID_EXISTS (O_error_message,
                                      L_wf_cust_grp_id_exist,
                                      I_wf_customer_group_id) = FALSE then
         return FALSE;
      end if;
      --
      O_wf_customer_group_name := NULL;
      --
      if L_wf_cust_grp_id_exist then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_CUST_GROUP_INFO',
                          'WF_CUSTOMER_GROUP',
                          I_wf_customer_group_id);
         open C_GET_CUST_GROUP_INFO;
         --
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_CUST_GROUP_INFO',
                          'WF_CUSTOMER_GROUP',
                          I_wf_customer_group_id);
         fetch C_GET_CUST_GROUP_INFO into O_wf_customer_group_name;
         --
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_CUST_GROUP_INFO',
                          'WF_CUSTOMER_GROUP',
                          I_wf_customer_group_id);
         close C_GET_CUST_GROUP_INFO;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_WF_CUSTOMER_GROUP_ID',
                                               I_wf_customer_group_id,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_CUST_GROUP_INFO%ISOPEN then
         close C_GET_CUST_GROUP_INFO;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END GET_WF_CUSTOMER_GROUP_INFO;
--------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_WF_CUSTOMER_GROUP (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_delete_group       IN OUT   VARCHAR2,
                                   I_wf_cust_group_id   IN       WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)   := 'WF_CUSTOMER_SQL.DELETE_WF_CUSTOMER_GROUP';
   L_wf_cust_group_id_exist   BOOLEAN;

   cursor C_WF_CUST_EXISTS is
      select 'N'
        from wf_customer
       where wf_customer_group_id = I_wf_cust_group_id;

BEGIN
   if I_wf_cust_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_cust_group_id',
                                            L_program,
                                            NULL);
      return FALSE;
   else
      --
      O_delete_group := 'Y';
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_WF_CUST_EXISTS',
                       'WF_CUSTOMER',
                       I_wf_cust_group_id);
      open C_WF_CUST_EXISTS;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_WF_CUST_EXISTS',
                       'WF_CUSTOMER',
                       I_wf_cust_group_id);
      fetch C_WF_CUST_EXISTS into O_delete_group;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_WF_CUST_EXISTS',
                       'WF_CUSTOMER',
                       I_wf_cust_group_id);
      close C_WF_CUST_EXISTS;

   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_WF_CUST_EXISTS%ISOPEN then
         close C_WF_CUST_EXISTS;
      end if;   
   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END DELETE_WF_CUSTOMER_GROUP;
--------------------------------------------------------------------------------------------------------------
FUNCTION WF_CUSTOMER_ID_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT   BOOLEAN,
                                I_wf_customer_id   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(64)   := 'WF_CUSTOMER_SQL.WF_CUSTOMER_ID_EXISTS';
   L_cust_exist   VARCHAR2(1);

   cursor C_WF_CUST_EXISTS is
      select 'x'
        from wf_customer
       where wf_customer_id = I_wf_customer_id;
BEGIN
   if I_wf_customer_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_customer_id',
                                            L_program,
                                            NULL);
      return FALSE;
   else
      --
      O_exists := FALSE;
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_WF_CUST_EXISTS',
                       'WF_CUSTOMER',
                       I_wf_customer_id);
      open C_WF_CUST_EXISTS;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_WF_CUST_EXISTS',
                       'WF_CUSTOMER',
                       I_wf_customer_id);
      fetch C_WF_CUST_EXISTS into L_cust_exist;
      --
      if L_cust_exist = 'x' then
         O_exists := TRUE;
      end if;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_WF_CUST_EXISTS',
                       'WF_CUSTOMER',
                       I_wf_customer_id);
      close C_WF_CUST_EXISTS;
      --
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_WF_CUST_EXISTS%ISOPEN then
         close C_WF_CUST_EXISTS;
      end if;   
   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
      return FALSE;
END WF_CUSTOMER_ID_EXISTS;
--------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_WF_CUSTOMER (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_delete_cust      IN OUT   VARCHAR2,
                             I_wf_customer_id   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'WF_CUSTOMER_SQL.DELETE_WF_CUSTOMER';
   L_wf_cust_id_exist   BOOLEAN;

   cursor C_WF_STORES_EXIST is
      select 'N'
        from store
       where wf_customer_id = I_wf_customer_id
         and rownum = 1;

BEGIN
   if I_wf_customer_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_customer_id',
                                            L_program,
                                            NULL);
      return FALSE;
   else
      --
      O_delete_cust := 'Y';
      --
      if WF_CUSTOMER_ID_EXISTS (O_error_message,
                                L_wf_cust_id_exist,
                                I_wf_customer_id) = FALSE then
         return FALSE;
      end if;
      --
      if L_wf_cust_id_exist then
         SQL_LIB.SET_MARK('OPEN',
                          'C_WF_STORES_EXIST',
                          'STORE',
                          I_wf_customer_id);
         open C_WF_STORES_EXIST;
         --
         SQL_LIB.SET_MARK('FETCH',
                          'C_WF_STORES_EXIST',
                          'STORE',
                          I_wf_customer_id);
         fetch C_WF_STORES_EXIST into O_delete_cust;
         --
         SQL_LIB.SET_MARK('CLOSE',
                          'C_WF_STORES_EXIST',
                          'STORE',
                          I_wf_customer_id);
         close C_WF_STORES_EXIST;
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_CUST_STORE_EXISTS',
                                               I_wf_customer_id,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_WF_STORES_EXIST%ISOPEN then
         close C_WF_STORES_EXIST;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END DELETE_WF_CUSTOMER;
--------------------------------------------------------------------------------------------------------------
FUNCTION GET_WF_CUSTOMER_INFO (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_wf_customer      IN OUT   WF_CUSTOMER%ROWTYPE,
                               I_wf_customer_id   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'WF_CUSTOMER_SQL.GET_WF_CUSTOMER_INFO';
   L_wf_cust_id_exist   BOOLEAN;

   cursor C_GET_CUST_INFO is
      select *
        from wf_customer
       where wf_customer_id = I_wf_customer_id;

BEGIN
   if I_wf_customer_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_customer_id',
                                            L_program,
                                            NULL);
      return FALSE;
   else
      if WF_CUSTOMER_ID_EXISTS (O_error_message,
                                L_wf_cust_id_exist,
                                I_wf_customer_id) = FALSE then
         return FALSE;
      end if;
      --
      O_wf_customer := NULL;
      --
      if L_wf_cust_id_exist then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_CUST_INFO',
                          'WF_CUSTOMER',
                          I_wf_customer_id);
         open C_GET_CUST_INFO;
         --
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_CUST_INFO',
                          'WF_CUSTOMER',
                          I_wf_customer_id);
         fetch C_GET_CUST_INFO into O_wf_customer;
         --
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_CUST_INFO',
                          'WF_CUSTOMER',
                          I_wf_customer_id);
         close C_GET_CUST_INFO;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_WF_CUSTOMER_ID',
                                               I_wf_customer_id,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_CUST_INFO%ISOPEN then
         close C_GET_CUST_INFO;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END GET_WF_CUSTOMER_INFO;
--------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_CUSTOMER_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cust_loc        IN OUT   VARCHAR2,
                                I_loc             IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'WF_CUSTOMER_SQL.CHECK_WF_CUSTOMER_LOC';

   cursor C_CHECK_LOC is
      select 'Y'
        from store
       where store = I_loc
         and store_type != 'C';

BEGIN
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   else
      --
      O_cust_loc := 'N';
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_LOC',
                       'STORE',
                       I_loc);
      open C_CHECK_LOC;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_LOC',
                       'STORE',
                       I_loc);
      fetch C_CHECK_LOC into O_cust_loc;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_LOC',
                       'STORE',
                       I_loc);
      close C_CHECK_LOC;
      --
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_LOC%ISOPEN then
         close C_CHECK_LOC;
      end if;       
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_WF_CUSTOMER_LOC;
--------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_CUST_CREDIT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_credit_ind      IN OUT   WF_CUSTOMER.CREDIT_IND%TYPE,
                               I_wf_customer_id  IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'WF_CUSTOMER_SQL.CHECK_WF_CUST_CREDIT';
   L_wf_cust_id_exist   BOOLEAN;

   cursor C_GET_CUST_CREDIT is
      select credit_ind
        from wf_customer
       where wf_customer_id = I_wf_customer_id;

BEGIN
   if I_wf_customer_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_customer_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if WF_CUSTOMER_ID_EXISTS (O_error_message,
                             L_wf_cust_id_exist,
                             I_wf_customer_id) = FALSE then
      return FALSE;
   end if;
 
   if NOT L_wf_cust_id_exist then
      O_error_message := SQL_LIB.CREATE_MSG('INV_WF_CUSTOMER_ID',
                                            I_wf_customer_id,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;   

   O_credit_ind := NULL;
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CUST_CREDIT',
                    'WF_CUSTOMER',
                    I_wf_customer_id);

   open C_GET_CUST_CREDIT;
   --
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CUST_CREDIT',
                    'WF_CUSTOMER',
                    I_wf_customer_id);
   fetch C_GET_CUST_CREDIT into O_credit_ind;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CUST_CREDIT',
                    'WF_CUSTOMER',
                    I_wf_customer_id);
   close C_GET_CUST_CREDIT;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_CUST_CREDIT%ISOPEN then
         close C_GET_CUST_CREDIT;
      end if;     
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_WF_CUST_CREDIT;
--------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_CUST_LOC_CREDIT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_credit_ind      IN OUT   WF_CUSTOMER.CREDIT_IND%TYPE,
                                   I_store           IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'WF_CUSTOMER_SQL.CHECK_WF_CUST_LOC_CREDIT';
   L_wf_customer_id     WF_CUSTOMER.WF_CUSTOMER_ID%TYPE;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if GET_WF_STORE_CUSTOMER_ID (O_error_message,
                                L_wf_customer_id,
                                I_store) = FALSE then
      return FALSE;
   end if;
   
   if CHECK_WF_CUST_CREDIT (O_error_message,
                            O_credit_ind,
                            L_wf_customer_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_WF_CUST_LOC_CREDIT;
--------------------------------------------------------------------------------------------------------------
FUNCTION GET_WF_STORE_CUSTOMER_ID (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_wf_customer_id      IN OUT   WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
                                   I_store               IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'WF_CUSTOMER_SQL.GET_WF_STORE_CUSTOMER_ID';
   L_store_row          STORE%ROWTYPE;
   L_exist_store        BOOLEAN;
   
BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                               L_exist_store,
                               L_store_row,
                               I_store) = FALSE then
      return FALSE;
   end if;

   if L_exist_store = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   -- Check if the input store is a valid WF store.
   elsif L_store_row.store_type NOT IN ('W', 'F') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_WF_CUST_LOC_ID',
                                            I_store,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;   

   O_wf_customer_id := L_store_row.wf_customer_id;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END GET_WF_STORE_CUSTOMER_ID;
--------------------------------------------------------------------------------------------------------------
FUNCTION WF_CUST_LOC_EXISTS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_cust_loc_exists   IN OUT   BOOLEAN,
                             I_wf_customer_id    IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
                             I_store             IN       V_STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'WF_CUSTOMER_SQL.WF_CUST_LOC_EXISTS';
   L_exist_cust         BOOLEAN;
   L_exist_store        BOOLEAN;
   L_store_row          STORE%ROWTYPE;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_wf_customer_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_customer_id',
                                            L_program,
                                            NULL);
      return FALSE;
   else
      if WF_CUSTOMER_ID_EXISTS (O_error_message,
                                L_exist_cust,
                                I_wf_customer_id) = FALSE then
         return FALSE;
      end if;

      if L_exist_cust = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_WF_CUSTOMER_ID',
                                               I_wf_customer_id,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      --
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exist_store,
                                  L_store_row,
                                  I_store) = FALSE then
         return FALSE;
      end if;
      --
      if L_exist_store = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;   
      --
      if I_wf_customer_id = L_store_row.wf_customer_id then
         O_cust_loc_exists := TRUE;
      else
         O_cust_loc_exists := FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END WF_CUST_LOC_EXISTS;
--------------------------------------------------------------------------------------------------------------
END;
/