CREATE OR REPLACE PACKAGE BODY CORESVC_STOCK_UPLOAD_SQL AS

---------------------------------------------------------------------------------------------------------------------
-- GLOBAL VARIABLES
---------------------------------------------------------------------------------------------------------------------
   TBL_error_rowid_char     ROWID_CHAR_TBL := ROWID_CHAR_TBL();
   LP_std_av_ind            SYSTEM_OPTIONS.STD_AV_IND%TYPE := NULL;
   LP_reject_recs_ind       VARCHAR2(1) := 'N';
   LP_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE := NULL;
   LP_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE := NULL;

---------------------------------------------------------------------------------------------------------------------
-- PL/SQL COLLECTIONS
---------------------------------------------------------------------------------------------------------------------
TYPE TYP_stkupld_distribute_rec IS RECORD
(
   cycle_count      STAKE_HEAD.CYCLE_COUNT%TYPE,
   item             ITEM_MASTER.ITEM%TYPE,
   pack_ind         ITEM_MASTER.PACK_IND%TYPE,
   item_xform_ind   ITEM_MASTER.ITEM_XFORM_IND%TYPE,
   physical_wh      WH.PHYSICAL_WH%TYPE,
   virtual_wh       WH.WH%TYPE,
   dist_qty         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE
);
TYPE TYP_stkupld_distribute_tbl is TABLE of TYP_stkupld_distribute_rec INDEX BY BINARY_INTEGER;

---------------------------------------------------------------------------------------------------------------------
-- PRIVATE PROCEDURES AND FUNCTIONS
---------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
-- Write errors to the the parameter tables
-- Only captured errors of a functional or known technical reason is written
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR_PARAMS(I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                             I_error_rowids    IN ROWID_CHAR_TBL DEFAULT NULL,
                             I_table_name      IN VARCHAR2 DEFAULT NULL);
----------------------------------------------------------------------------------------------
-- Write reject status to the the parameter tables
-- Only captured errors of a functional is written
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_REJECT_PARAMS(I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_error_rowids    IN ROWID_CHAR_TBL DEFAULT NULL,
                              I_table_name      IN VARCHAR2 DEFAULT NULL);
----------------------------------------------------------------------------------------------
-- Write success status to the parameter tables
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS_PARAMS(I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                               I_rowids    IN ROWID_CHAR_TBL DEFAULT NULL,
                               I_table_name      IN VARCHAR2 DEFAULT NULL);
----------------------------------------------------------------------------------------------
-- Update the process/chunk status to error
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                      I_chunk_id        IN SVC_STKUPLD_FDETL.CHUNK_ID%TYPE,
                      I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE);
----------------------------------------------------------------------------------------------
-- Update the process/chunk status to rejected
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_REJECT(I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                       I_chunk_id        IN SVC_STKUPLD_FDETL.CHUNK_ID%TYPE,
                       I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE);
----------------------------------------------------------------------------------------------
-- Update the process/chunk status to success
----------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS(I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                        I_chunk_id        IN SVC_STKUPLD_FDETL.CHUNK_ID%TYPE);
----------------------------------------------------------------------------------------------
-- Populate other tables to be used in processing
----------------------------------------------------------------------------------------------
FUNCTION POPULATE_WORK_TABLES(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Validate the stock count detail.
-- Validation failure marks a record as rejected, write and error message to the
-- error_msg column and continues processing
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_NON_FATAL(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                            I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Create new item_loc records.
-- New item records are created for items in a unit and value stock count.
----------------------------------------------------------------------------------------------
FUNCTION RANGE_NEW_ITEM_LOC(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                            I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Create stake_location records
-- New stake_location records are created for items in a unit and value stock count.
----------------------------------------------------------------------------------------------
FUNCTION CREATE_STAKE_LOCATION(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                               I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Create stake_sku_loc records
-- New stake_sku_loc records are created for items
----------------------------------------------------------------------------------------------
FUNCTION CREATE_STAKE_SKU_LOC(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Retrieve retail amounts for items with external finisher locations
----------------------------------------------------------------------------------------------
FUNCTION POPULATE_EXT_FINISHER_RETAIL(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                      I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Lock stake_sku_loc records for update
----------------------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_SKU_LOC(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                            I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Update stake_sku_loc records for update
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_SKU_LOC(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Lock stake_qty records for update
----------------------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_QTY(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                        I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Update stake_qty records for update
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_QTY(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                          I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Create stake_sku_loc records for missing orderables for unit and value stock counts.
----------------------------------------------------------------------------------------------
FUNCTION INSERT_MISSING_ORDERABLES(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                   I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Merge the reject records in the item/loc working table (work_stkupld_item_loc_gtt)
-- into the fdetl parameter table (svc_stkupld_fdetl).
----------------------------------------------------------------------------------------------
FUNCTION MERGE_BACK_TO_PARAMS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR_PARAMS(I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                             I_error_rowids    IN ROWID_CHAR_TBL DEFAULT NULL,
                             I_table_name      IN VARCHAR2 DEFAULT NULL) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program             VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.WRITE_ERROR_PARAMS';

   cursor C_LOCK_FHEAD is
      select 'x'
        from svc_stkupld_fhead sf,
             TABLE(CAST(I_error_rowids AS ROWID_CHAR_TBL)) row_id
       where sf.process_id = I_process_id
         and sf.rowid = CHARTOROWID(value(row_id))
         for update of sf.error_msg, sf.status nowait;

   cursor C_LOCK_FDETL is
      select 'x'
        from svc_stkupld_fdetl sd,
             TABLE(CAST(I_error_rowids AS ROWID_CHAR_TBL)) row_id
       where sd.process_id = I_process_id
         and sd.rowid = CHARTOROWID(value(row_id))
         for update of sd.error_msg, sd.status nowait;

   cursor C_LOCK_FTAIL is
      select 'x'
        from svc_stkupld_fhead st,
             TABLE(CAST(I_error_rowids AS ROWID_CHAR_TBL)) row_id
       where st.process_id = I_process_id
         and st.rowid = CHARTOROWID(value(row_id))
         for update of st.error_msg, st.status nowait;
BEGIN

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FHEAD and
       I_error_rowids is NOT NULL and I_error_rowids.COUNT > 0)then
      ---
      open C_LOCK_FHEAD;
      close C_LOCK_FHEAD;
      ---
      FORALL i in 1..I_error_rowids.COUNT
      update svc_stkupld_fhead sf
         set sf.error_msg = I_error_message,
             sf.status = CORESVC_STOCK_UPLOAD_SQL.ERROR
       where sf.rowid = CHARTOROWID(I_error_rowids(i))
         and sf.process_id = I_process_id;
      ---
   end if;

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FDETL and
       I_error_rowids is NOT NULL and I_error_rowids.COUNT > 0)then
      ---
      open C_LOCK_FDETL;
      close C_LOCK_FDETL;
      ---
      FORALL i in 1..I_error_rowids.COUNT
      update svc_stkupld_fdetl sd
         set sd.error_msg = I_error_message,
             sd.status = CORESVC_STOCK_UPLOAD_SQL.ERROR
       where sd.rowid = CHARTOROWID(I_error_rowids(i))
         and sd.process_id = I_process_id;
      ---
   end if;

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FTAIL and
       I_error_rowids is NOT NULL and I_error_rowids.COUNT > 0)then
      ---
      open C_LOCK_FTAIL;
      close C_LOCK_FTAIL;
      ---
      FORALL i in 1..I_error_rowids.COUNT
      update svc_stkupld_ftail st
         set st.error_msg = I_error_message,
             st.status = CORESVC_STOCK_UPLOAD_SQL.ERROR
       where st.rowid = CHARTOROWID(I_error_rowids(i))
         and st.process_id = I_process_id;
      ---
   end if;

   commit;

EXCEPTION
   when OTHERS then
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_REJECT_PARAMS(I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_error_rowids    IN ROWID_CHAR_TBL DEFAULT NULL,
                              I_table_name      IN VARCHAR2 DEFAULT NULL) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program             VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.WRITE_REJECT_PARAMS';

   cursor C_LOCK_FHEAD is
      select 'x'
        from svc_stkupld_fhead sf,
             TABLE(CAST(I_error_rowids AS ROWID_CHAR_TBL)) row_id
       where sf.process_id = I_process_id
         and sf.rowid = CHARTOROWID(value(row_id))
         for update of sf.error_msg, sf.status nowait;

   cursor C_LOCK_FDETL is
      select 'x'
        from svc_stkupld_fdetl sd,
             TABLE(CAST(I_error_rowids AS ROWID_CHAR_TBL)) row_id
       where sd.process_id = I_process_id
         and sd.rowid = CHARTOROWID(value(row_id))
         for update of sd.error_msg, sd.status nowait;

   cursor C_LOCK_FTAIL is
      select 'x'
        from svc_stkupld_fhead st,
             TABLE(CAST(I_error_rowids AS ROWID_CHAR_TBL)) row_id
       where st.process_id = I_process_id
         and st.rowid = CHARTOROWID(value(row_id))
         for update of st.error_msg, st.status nowait;
BEGIN

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FHEAD and
       I_error_rowids is NOT NULL and I_error_rowids.COUNT > 0)then
      ---
      open C_LOCK_FHEAD;
      close C_LOCK_FHEAD;
      ---
      FORALL i in 1..I_error_rowids.COUNT
      update svc_stkupld_fhead sf
         set sf.error_msg = I_error_message,
             sf.status = CORESVC_STOCK_UPLOAD_SQL.REJECTED
       where sf.rowid = CHARTOROWID(I_error_rowids(i))
         and sf.process_id = I_process_id;
      ---
   end if;

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FDETL and
       I_error_rowids is NOT NULL and I_error_rowids.COUNT > 0)then
      ---
      open C_LOCK_FDETL;
      close C_LOCK_FDETL;
      ---
      FORALL i in 1..I_error_rowids.COUNT
      update svc_stkupld_fdetl sd
         set sd.error_msg = I_error_message,
             sd.status = CORESVC_STOCK_UPLOAD_SQL.REJECTED
       where sd.rowid = CHARTOROWID(I_error_rowids(i))
         and sd.process_id = I_process_id;
      ---
   end if;

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FTAIL and
       I_error_rowids is NOT NULL and I_error_rowids.COUNT > 0)then
      ---
      open C_LOCK_FTAIL;
      close C_LOCK_FTAIL;
      ---
      FORALL i in 1..I_error_rowids.COUNT
      update svc_stkupld_ftail st
         set st.error_msg = I_error_message,
             st.status = CORESVC_STOCK_UPLOAD_SQL.REJECTED
       where st.rowid = CHARTOROWID(I_error_rowids(i))
         and st.process_id = I_process_id;
      ---
   end if;

   commit;

EXCEPTION
   when OTHERS then
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS_PARAMS(I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                               I_rowids          IN ROWID_CHAR_TBL DEFAULT NULL,
                               I_table_name      IN VARCHAR2 DEFAULT NULL) AS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program             VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.WRITE_SUCCESS_PARAMS';

   cursor C_LOCK_FHEAD is
      select 'x'
        from svc_stkupld_fhead sf,
             TABLE(CAST(I_rowids AS ROWID_CHAR_TBL)) row_id
       where sf.process_id = I_process_id
         and sf.rowid = CHARTOROWID(value(row_id))
         for update of sf.error_msg, sf.status nowait;

   cursor C_LOCK_FDETL is
      select 'x'
        from svc_stkupld_fdetl sd,
             TABLE(CAST(I_rowids AS ROWID_CHAR_TBL)) row_id
       where sd.process_id = I_process_id
         and sd.rowid = CHARTOROWID(value(row_id))
         for update of sd.error_msg, sd.status nowait;

   cursor C_LOCK_FTAIL is
      select 'x'
        from svc_stkupld_fhead st,
             TABLE(CAST(I_rowids AS ROWID_CHAR_TBL)) row_id
       where st.process_id = I_process_id
         and st.rowid = CHARTOROWID(value(row_id))
         for update of st.error_msg, st.status nowait;
BEGIN

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FHEAD and
       I_rowids is NOT NULL and I_rowids.COUNT > 0)then
      ---
      open C_LOCK_FHEAD;
      close C_LOCK_FHEAD;
      ---
      FORALL i in 1..I_rowids.COUNT
      update svc_stkupld_fhead sf
         set sf.status = CORESVC_STOCK_UPLOAD_SQL.PROCESSED
       where sf.rowid = CHARTOROWID(I_rowids(i))
         and sf.process_id = I_process_id;
      ---
   end if;

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FDETL and
       I_rowids is NOT NULL and I_rowids.COUNT > 0)then
      ---
      open C_LOCK_FDETL;
      close C_LOCK_FDETL;
      ---
      FORALL i in 1..I_rowids.COUNT
      update svc_stkupld_fdetl sd
         set sd.status = CORESVC_STOCK_UPLOAD_SQL.PROCESSED
       where sd.rowid = CHARTOROWID(I_rowids(i))
         and sd.process_id = I_process_id;
      ---
   end if;

   if (I_table_name = CORESVC_STOCK_UPLOAD_SQL.FTAIL and
       I_rowids is NOT NULL and I_rowids.COUNT > 0)then
      ---
      open C_LOCK_FTAIL;
      close C_LOCK_FTAIL;
      ---
      FORALL i in 1..I_rowids.COUNT
      update svc_stkupld_ftail st
         set st.status = CORESVC_STOCK_UPLOAD_SQL.PROCESSED
       where st.rowid = CHARTOROWID(I_rowids(i))
         and st.process_id = I_process_id;
      ---
   end if;

   commit;

EXCEPTION
   when OTHERS then
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
FUNCTION MERGE_BACK_TO_PARAMS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.MERGE_BACK_TO_PARAMS';
   L_table             VARCHAR2(30) := NULL;
   L_reject_recs_ind   VARCHAR2(1)  := NULL;

   cursor C_CHECK_REJECTS is
      select 'Y'
        from svc_stkupld_fdetl
       where status     = CORESVC_STOCK_UPLOAD_SQL.REJECTED
         and process_id = I_process_id
         and chunk_id = I_chunk_id
         and rownum     = 1;

BEGIN

      merge into svc_stkupld_fdetl target
      using (select process_id,
                    chunk_id,
                    physical_location,
                    stockcount_item,
                    status,
                    LISTAGG(error_msg, ';') WITHIN GROUP (ORDER BY error_msg) as error_msg
               from (select distinct
                            process_id,
                            chunk_id,
                            physical_location,
                            stockcount_item,
                            DECODE(status,
                                   -- records still in new status have been processed successfully
                                   CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD,
                                   CORESVC_STOCK_UPLOAD_SQL.PROCESSED,
                                   -- copy over rejects and error status
                                   status) status,
                            error_msg
                       from work_stkupld_item_loc_gtt)
              group by process_id,
                       chunk_id,
                       physical_location,
                       stockcount_item,
                       status) source
         on (    target.process_id     = source.process_id
             and target.chunk_id       = source.chunk_id
             and target.item_value     = source.stockcount_item)
         when matched then
            update set target.error_msg = source.error_msg,
                       target.status    = source.status;


   -- Set the reject records indicator if there are reject records merged
   open C_CHECK_REJECTS;
   fetch C_CHECK_REJECTS into LP_reject_recs_ind;
   close C_CHECK_REJECTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_BACK_TO_PARAMS;
---------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_MISSING_ORDERABLES(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                   I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.INSERT_MISSING_ORDERABLES';
   L_table         VARCHAR2(30) := NULL;

BEGIN

   merge into stake_sku_loc target
   using (select distinct
                 sil.cycle_count,
                 sil.location_type,
                 sil.location,
                 xordm.xform_orderable_item,
                 0 snapshot_on_hand_qty,
                 0 snapshot_in_transit_qty,
                 NVL(ils.unit_cost, 0) snapshot_unit_cost,
                 il.unit_retail snapshot_unit_retail,  --Fix this for external finisher
                 'N' processed,
                 0 physical_count_qty,
                 0 pack_comp_qty,
                 im.dept,
                 im.class,
                 im.subclass,
                 'O' xform_item_type
            from work_stkupld_item_loc_gtt sil,
                 work_stkupld_xform_ord_gtt xordm,
                 item_loc il,
                 item_loc_soh ils,
                 item_master im
           where sil.status                 = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and sil.error_msg              is NULL     -- no errors
             ---
             and sil.cycle_count            = xordm.cycle_count
             and sil.item                   = xordm.xform_sellable_item
             ---
             and xordm.xform_orderable_item = il.item
             and xordm.xform_orderable_item = ils.item
             and sil.location               = il.loc
             and il.loc                     = ils.loc
             ---
             and im.item                    = xordm.xform_orderable_item
             and im.pack_ind                = 'N') source
      on (    target.cycle_count = source.cycle_count
          and target.loc_type    = source.location_type
          and target.location    = source.location
          and target.item        = source.xform_orderable_item)
    when not matched then
       insert (cycle_count,
               loc_type,
               location,
               item,
               snapshot_on_hand_qty,
               snapshot_in_transit_qty,
               snapshot_unit_cost,
               snapshot_unit_retail,
               processed,
               physical_count_qty,
               pack_comp_qty,
               dept,
               class,
               subclass,
               xform_item_type)
       values (source.cycle_count,
               source.location_type,
               source.location,
               source.xform_orderable_item,
               source.snapshot_on_hand_qty,
               source.snapshot_in_transit_qty,
               source.snapshot_unit_cost,
               source.snapshot_unit_retail,
               source.processed,
               source.physical_count_qty,
               source.pack_comp_qty,
               source.dept,
               source.class,
               source.subclass,
               source.xform_item_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_MISSING_ORDERABLES;
---------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_QTY(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                          I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.UPDATE_STAKE_QTY';
   L_table         VARCHAR2(30) := NULL;

BEGIN


   -- Update stake_sku_loc for existing records, insert if not existing
   -- The following will not have records on stake_qty
   --    - Pack items in store locations
   --    - Pack item componenents in warehouse locations
   if GP_stkupld_fhead_row.location_type = 'E' then
      ---
      merge into stake_qty target
      using (select sil.cycle_count,
                    ---
                    sil.item,
                    sil.location,
                    sil.location_type,
                    sil.location_description,
                    ---
                    SUM(NVL(sil.stake_quantity, 0)) stake_quantity
               from work_stkupld_stake_qty_gtt sil
              where sil.status      = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg   is NULL     -- no errors
                --and sil.ssl_processed_ind is NOT NULL -- has a corresponding stake_sku_loc record
                and sil.location_description is NOT NULL
              group by sil.cycle_count,
                       sil.item,
                       sil.location,
                       sil.location_type,
                       sil.location_description) source
         on (    target.cycle_count   = source.cycle_count
             and target.item          = source.item
             and target.loc_type      = source.location_type
             and target.location      = source.location
             and target.location_desc = source.location_description)
          when matched then
             update set target.qty = source.stake_quantity
          when not matched then
             insert (cycle_count,
                     loc_type,
                     location,
                     item,
                     qty,
                     location_desc)
             values (source.cycle_count,
                     source.location_type,
                     source.location,
                     source.item,
                     source.stake_quantity,
                     source.location_description);

   elsif GP_stkupld_fhead_row.location_type = 'S' then
      ---
      merge into stake_qty target
      using (select sil.cycle_count,
                    ---
                    sil.item,
                    sil.location,
                    sil.location_type,
                    sil.location_description,
                    ---
                    SUM(NVL(sil.stake_quantity, 0)) stake_quantity
               from work_stkupld_stake_qty_gtt sil
              where sil.status      = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg   is NULL     -- no errors
                --and sil.ssl_processed_ind is NOT NULL -- has a corresponding stake_sku_loc record
                and sil.location_description is NOT NULL
                and sil.pack_ind = 'N'
              group by sil.cycle_count,
                       sil.item,
                       sil.location,
                       sil.location_type,
                       sil.location_description) source
         on (    target.cycle_count   = source.cycle_count
             and target.item          = source.item
             and target.loc_type      = source.location_type
             and target.location      = source.location
             and target.location_desc = source.location_description)
          when matched then
             update set target.qty = source.stake_quantity
          when not matched then
             insert (cycle_count,
                     loc_type,
                     location,
                     item,
                     qty,
                     location_desc)
             values (source.cycle_count,
                     source.location_type,
                     source.location,
                     source.item,
                     source.stake_quantity,
                     source.location_description);

   elsif GP_stkupld_fhead_row.location_type = 'W' then
      ---
      merge into stake_qty target
      using (select sil.cycle_count,
                    ---
                    sil.item,
                    sil.location,
                    sil.location_type,
                    sil.location_description,
                    ---
                    SUM(NVL(sil.stake_quantity, 0)) stake_quantity
               from work_stkupld_stake_qty_gtt sil,
					stake_sku_loc ssl
              where sil.status      = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.cycle_count = ssl.cycle_count
                and sil.item = ssl.item
                and ssl.location = sil.location
                and ssl.loc_type = sil.location_type
                and sil.error_msg   is NULL     -- no errors
                --and sil.ssl_processed_ind is NOT NULL -- has a corresponding stake_sku_loc record
                and sil.location_description is NOT NULL
                -- exclude the exploded component items
                and ((sil.pack_ind = 'N' and sil.pack_item is NULL) or -- non-pack item components
                     sil.pack_ind = 'Y') -- pack items
              group by sil.cycle_count,
                       sil.item,
                       sil.location,
                       sil.location_type,
                       sil.location_description) source
         on (    target.cycle_count   = source.cycle_count
             and target.item          = source.item
             and target.loc_type      = source.location_type
             and target.location      = source.location
             and target.location_desc = source.location_description)
          when matched then
             update set target.qty = source.stake_quantity
          when not matched then
             insert (cycle_count,
                     loc_type,
                     location,
                     item,
                     qty,
                     location_desc)
             values (source.cycle_count,
                     source.location_type,
                     source.location,
                     source.item,
                     source.stake_quantity,
                     source.location_description);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_STAKE_QTY;
---------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_QTY(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                        I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.LOCK_STAKE_QTY';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STAKE_QTY is
      select /*+ INDEX (sq STAKE_QTY_I1) */
             'x'
        from stake_qty sq,
             work_stkupld_stake_qty_gtt sil
       where sil.status               = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
         and sil.error_msg            is NULL
         and sil.location_description is NOT NULL
         ---
         and sil.cycle_count          = sq.cycle_count
         and sil.location_type        = sq.loc_type
         and sil.location             = sq.location
         and sil.item                 = sq.item
         and sil.location_description = sq.location_desc
         and (sil.location_type = 'E' or
              (sil.location_type = 'S' and sil.pack_ind = 'N')or
              (sil.location_type = 'W' and sil.pack_item is NULL))
         for update of sq.qty nowait;

BEGIN

   -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
   FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         L_table := 'STAKE_QTY';
         open C_LOCK_STAKE_QTY;
         close C_LOCK_STAKE_QTY;
         L_locked_acquired := 'Y';
         exit;
      EXCEPTION
         when RECORD_LOCKED then
            NULL;
      END;
      -- Sleep before the next retry
      DBMS_LOCK.SLEEP(LP_retry_wait_time);
      ---
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_STAKE_QTY;
      close C_LOCK_STAKE_QTY;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_STAKE_QTY;
---------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_SKU_LOC(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.UPDATE_STAKE_SKU_LOC';
   L_table         VARCHAR2(30) := NULL;

BEGIN

   merge into work_stkupld_item_loc_gtt target
   using (select t.cycle_count, 
                 t.item,
                 t.location, 
                 t.location_type,
                 sum(sq.qty) stake_qty
            from ( select cycle_count,
			              item,
						  location,
			              location_type,
			              location_description 
			         from (select distinct cycle_count,
					              item,
								  location,
								  location_type 
                             from WORK_STKUPLD_ITEM_LOC_GTT
                            where PROCESS_ID = I_process_id
                              and CHUNK_ID   = I_chunk_id
                              and PACK_IND = 'N'
                              ) gtt
                              ,
                          (select distinct location_description
                             from SVC_STKUPLD_FDETL
                            where PROCESS_ID = I_process_id
							  and CHUNK_ID   = I_chunk_id
							  and STATUS     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
						      ) ssf 
				         ) t,
                 stake_qty sq
           where t.cycle_count          = sq.cycle_count
             and t.item                 = sq.item
             and t.location             = sq.location
             and t.location_type        = sq.loc_type
             and t.location_description = sq.location_desc
          group by t.cycle_count, 
                   t.item,
                   t.location, 
                   t.location_type ) source
         on (    target.cycle_count      = source.cycle_count
             and target.location_type    = source.location_type
             and target.location         = source.location
             and target.item             = source.item          
             and target.status           = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and target.pack_item        is null
             and target.error_msg        is null)
          when matched then
          update set target.stake_quantity = NVL(source.stake_qty,0);

   -- Update stake_sku_loc for existing records
   -- Update for REF items and tranformed orderable items
   merge into stake_sku_loc target
   using (select sil.cycle_count,
                 ---
                 sil.item,
                 sil.location,
                 sil.location_type,
                 ---
                 SUM(NVL(sil.stockcount_quantity, 0)) stockcount_quantity
            from work_stkupld_item_loc_gtt sil
           where -- transformed orderable items or REF items
                 (NVL(sil.xform_item_type, '-999') = 'O' or NVL(sil.item_type, '-999') = 'REF')
             and sil.pack_ind    = 'N'
             and sil.status      = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and sil.error_msg   is NULL     -- no errors
             and sil.ssl_processed_ind is NOT NULL -- has a corresponding stake_sku_loc record
           group by sil.process_id,
                    sil.chunk_id,
                    sil.cycle_count,
                    sil.item,
                    sil.location,
                    sil.location_type) source
      on (    target.cycle_count = source.cycle_count
          and target.loc_type    = source.location_type
          and target.location    = source.location
          and target.item        = source.item          )
       when matched then
       -- Orderable transformed items and REF items gets updated with the total stock count quantity added to the existing count
       -- All other records gets updated with the total stock count quantity added to the existing count less the current count on the stake_qty table
       update set target.physical_count_qty = NVL(target.physical_count_qty, 0) + source.stockcount_quantity;

   if GP_stkupld_fhead_row.location_type = 'W' then  --warehouse locations

      -- Update for regular transaction-level items
      -- non-pack item components
      merge into stake_sku_loc target
      using (select sil.cycle_count,
                    ---
                    sil.item,
                    sil.location,
                    sil.location_type,
                    SUM(NVL(sil.stockcount_quantity, 0)) stockcount_quantity,
                    SUM(NVL(sil.stake_quantity, 0)) stake_quantity
               from work_stkupld_item_loc_gtt sil
              where -- not transformed orderable items or REF items
                    NVL(sil.xform_item_type, '-999') != 'O'
                and sil.item_type != 'REF'
                and sil.status      = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg   is NULL     -- no errors
                and sil.ssl_processed_ind is NOT NULL -- has a corresponding stake_sku_loc record
                and sil.pack_item is NULL
              group by sil.process_id,
                       sil.chunk_id,
                       sil.cycle_count,
                       sil.item,
                       sil.location,
                       sil.location_type) source
         on (    target.cycle_count = source.cycle_count
             and target.loc_type    = source.location_type
             and target.location    = source.location
             and target.item        = source.item)
          when matched then
          -- Non-pack items
                -- physical_count_qty = total stock count quantity + current physical_count_qty - stake_qty quantity
          update set target.physical_count_qty = NVL(target.physical_count_qty, 0) + source.stockcount_quantity - source.stake_quantity;

      -- Update for regular transaction-level items
      -- Pack item components
      merge into stake_sku_loc target
      using (select sil.cycle_count,
                    ---
                    sil.item,
                    sil.location,
                    sil.location_type,
                    SUM(NVL(sil.stockcount_quantity, 0)) stockcount_quantity,
                    SUM(NVL(sil.stake_quantity, 0)) stake_quantity
               from work_stkupld_item_loc_gtt sil
              where -- not transformed orderable items or REF items
                    NVL(sil.xform_item_type, '-999') != 'O'
                and sil.item_type != 'REF'
                and sil.status      = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg   is NULL     -- no errors
                and sil.ssl_processed_ind is NOT NULL -- has a corresponding stake_sku_loc record
                and sil.pack_ind = 'N'
                and sil.pack_item is not NULL
              group by sil.process_id,
                       sil.chunk_id,
                       sil.cycle_count,
                       sil.item,
                       sil.location,
                       sil.location_type) source
         on (    target.cycle_count = source.cycle_count
             and target.loc_type    = source.location_type
             and target.location    = source.location
             and target.item        = source.item)
          when matched then
          -- Pack item components
                -- physical_count_qty = retain current values on the db
                -- pack_comp_qty      = total stock count quantity + current physical_count_qty (component have no stake_qty records)
          update set target.pack_comp_qty = NVL(target.pack_comp_qty,0) + source.stockcount_quantity;

   else -- store and external finisher locations

      -- Update for regular transaction-level items
      -- pack items and non-pack item components
      merge into stake_sku_loc target
      using (select sil.cycle_count,
                    ---
                    sil.item,
                    sil.location,
                    sil.location_type,
                    sil.pack_ind,
                    SUM(NVL(sil.stockcount_quantity, 0)) stockcount_quantity,
                    SUM(NVL(sil.stake_quantity, 0)) stake_quantity
               from work_stkupld_item_loc_gtt sil
              where -- not transformed orderable items or REF items
                    NVL(sil.xform_item_type, '-999') != 'O'
                and sil.item_type != 'REF'
                and sil.status      = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg   is NULL     -- no errors
                and sil.ssl_processed_ind is NOT NULL -- has a corresponding stake_sku_loc record
                and sil.pack_item is NULL -- pack item or non-pack item component
              group by sil.process_id,
                       sil.chunk_id,
                       sil.cycle_count,
                       sil.item,
                       sil.location,
                       sil.location_type,
                       sil.pack_ind) source
         on (    target.cycle_count = source.cycle_count
             and target.loc_type    = source.location_type
             and target.location    = source.location
             and target.item        = source.item)
          when matched then
          -- Pack items at store locations get 0 quantities
          -- otherwise physical_count_qty = total stock count quantity + current physical_count_qty - stake_qty quantity
          update set target.physical_count_qty = DECODE(source.pack_ind, 'Y', 0,
                                                 (NVL(target.physical_count_qty, 0) + source.stockcount_quantity - source.stake_quantity));

      -- Update for regular transaction-level items
      -- Pack item components
      merge into stake_sku_loc target
      using (select sil.cycle_count,
                    ---
                    sil.item,
                    sil.location,
                    sil.location_type,
                    sil.pack_ind,
                    SUM(NVL(sil.stockcount_quantity, 0)) stockcount_quantity,
                    SUM(NVL(sil.stake_quantity, 0)) stake_quantity
               from work_stkupld_item_loc_gtt sil
              where -- not transformed orderable items or REF items
                    NVL(sil.xform_item_type, '-999') != 'O'
                and sil.item_type != 'REF'
                and sil.status      = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg   is NULL     -- no errors
                and sil.ssl_processed_ind is NOT NULL -- has a corresponding stake_sku_loc record
                and sil.pack_ind = 'N'
                and sil.pack_item is not NULL -- pack item component
              group by sil.process_id,
                       sil.chunk_id,
                       sil.cycle_count,
                       sil.item,
                       sil.location,
                       sil.location_type,
                       sil.pack_ind) source
         on (    target.cycle_count = source.cycle_count
             and target.loc_type    = source.location_type
             and target.location    = source.location
             and target.item        = source.item)
          when matched then
          -- Pack items at store locations get 0 quantities
          -- otherwise physical_count_qty = total stock count quantity + current physical_count_qty - stake_qty quantity
          update set target.physical_count_qty = DECODE(source.pack_ind, 'Y', 0,
                                                 (NVL(target.physical_count_qty, 0) + source.stockcount_quantity - source.stake_quantity));

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_STAKE_SKU_LOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_SKU_LOC(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                            I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.LOCK_STAKE_SKU_LOC';
   L_table             VARCHAR2(30) := NULL;
   L_locked_acquired   VARCHAR2(1) := 'N';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);


   cursor C_LOCK_STAKE_SKU_LOC is
      select /*+ INDEX (ssl PK_STAKE_SKU_LOC) */
             'x'
        from work_stkupld_item_loc_gtt sil,
             stake_sku_loc ssl
       where sil.status             = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
         and sil.error_msg         is NULL
         and sil.ssl_processed_ind is NOT NULL
         ---
         and sil.cycle_count   = ssl.cycle_count
         and sil.location_type = ssl.loc_type
         and sil.location      = ssl.location
         and sil.item          = ssl.item
         for update of ssl.physical_count_qty nowait;

BEGIN

   -- Retry the lock the maximum number of times defined in LP_retry_lock_attempts
   FOR i in 1..(LP_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         L_table := 'STAKE_SKU_LOC';
         open C_LOCK_STAKE_SKU_LOC;
         close C_LOCK_STAKE_SKU_LOC;
         L_locked_acquired := 'Y';
         exit;
      EXCEPTION
         when RECORD_LOCKED then
            NULL;
      END;
      -- Sleep before the next retry
      DBMS_LOCK.SLEEP(LP_retry_wait_time);
      ---
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_STAKE_SKU_LOC;
      close C_LOCK_STAKE_SKU_LOC;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_STAKE_SKU_LOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_EXT_FINISHER_RETAIL(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                      I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)   := 'CORESVC_STOCK_UPLOAD_SQL.POPULATE_EXT_FINISHER_RETAIL';
   L_unit_retail   WORK_STKUPLD_EXT_FIN_RTL_GTT.FINISHER_AV_RETAIL%TYPE := 0;

   cursor C_EXT_FIN_RECS is
      select efr.item,
             efr.external_finisher
        from work_stkupld_ext_fin_rtl_gtt efr
       where efr.finisher_av_retail is NULL
         for update of efr.finisher_av_retail nowait;

BEGIN

   FOR ext_fin_rec IN C_EXT_FIN_RECS LOOP
      ---
      if PRICING_ATTRIB_SQL.GET_EXTERNAL_FINISHER_RETAIL(O_error_message,
                                                         L_unit_retail,
                                                         ext_fin_rec.item,
                                                         ext_fin_rec.external_finisher) = FALSE then
         return FALSE;
      end if;
      ---
      update work_stkupld_ext_fin_rtl_gtt efr
         set efr.finisher_av_retail = L_unit_retail
       where efr.item              = ext_fin_rec.item
         and efr.external_finisher = ext_fin_rec.external_finisher;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_EXT_FINISHER_RETAIL;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_STAKE_SKU_LOC(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.CREATE_STAKE_SKU_LOC';
   L_count     NUMBER(10)   := 0;

   cursor C_CHECK_FINISHER_RETAIL is
      select count(*)
        from work_stkupld_item_loc_gtt sil,
             item_loc_soh ils
       where sil.process_id = I_process_id
         and sil.chunk_id   = I_chunk_id
         and sil.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
         and sil.error_msg  is NULL     -- no errors
         ---
         and sil.pack_ind   = 'N'
         ---
         and ils.item (+)   = sil.item
         and ils.loc (+)    = sil.location
         and ils.finisher_av_retail is NULL;

BEGIN

   ----------------------------------------------------------------------
   -- EXTERNAL FINISHERS
   ----------------------------------------------------------------------
   -- Handle item records for external finisher locations with no finisher_av_retail value on the item_loc_soh table.
   -- Do only for non-pack item records.
   if GP_stkupld_fhead_row.location_type = 'E' then
      ---
      insert into work_stkupld_ext_fin_rtl_gtt
                   (external_finisher,
                    item,
                    finisher_av_retail)
         select sil.location,
                sil.item,
                NULL  --finisher_av_retail
           from work_stkupld_item_loc_gtt sil,
                item_loc_soh ils
          where sil.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
            and sil.error_msg  is NULL     -- no errors
            ---
            and sil.pack_ind   = 'N'
            ---
            and ils.item (+)   = sil.item
            and ils.loc (+)    = sil.location
            and ils.finisher_av_retail is NULL;
      ---
      if NVL(SQL%ROWCOUNT, 0) > 0 then
         ---
         if POPULATE_EXT_FINISHER_RETAIL(O_error_message,
                                         I_process_id,
                                         I_chunk_id) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   end if;


   ----------------------------------------------------------------------
   -- STORES
   ----------------------------------------------------------------------
   if GP_stkupld_fhead_row.location_type in ('S', 'E') then
      -- Create stake_sku_records for for non-pack item/locs and pack item components not in stake_sku_loc
      -- This covers item/locations that are in the stock count parameter tables but not in RMS
      merge into stake_sku_loc target
      using (select distinct
                    sil.cycle_count,
                    sil.location_type,
                    sil.location,
                    sil.item,
                    0 snapshot_on_hand_qty,
                    0 snapshot_in_transit_qty,
                    DECODE(LP_std_av_ind,
                           'A',
                           NVL(ils.av_cost, 0),
                           NVL(ils.unit_cost, 0)) snapshot_unit_cost,
                    DECODE(sil.location_type,
                           'E',
                           NVL(ils.finisher_av_retail, efr.finisher_av_retail),
                           NVL(ils.finisher_av_retail, il.unit_retail)) snapshot_unit_retail,
                    'N' processed,
                    sil.stockcount_quantity,  --physical_count_qty
                    0 pack_comp_qty,
                    sil.dept,
                    sil.class,
                    sil.subclass,
                    sil.xform_item_type
               from work_stkupld_item_loc_gtt sil,
                    work_stkupld_ext_fin_rtl_gtt efr,
                    item_loc il,
                    item_loc_soh ils
              where sil.status                = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg             is NULL     -- no errors
                ---
                and sil.ssl_processed_ind is NULL -- no stake_sku_loc record
                and sil.product_level_ind != 'I'
                and sil.pack_ind              = 'N'
                ---
                and sil.item                  = il.item
                and sil.location              = il.loc
                and il.item                   = ils.item (+)
                and il.loc                    = ils.loc (+)
                ---
                and efr.item (+)              = sil.item
                and efr.external_finisher (+) = sil.location
                ) source
         on (    target.cycle_count = source.cycle_count
             and target.loc_type    = source.location_type
             and target.location    = source.location
             and target.item        = source.item)
          when NOT matched then
             insert (cycle_count,
                     loc_type,
                     location,
                     item,
                     snapshot_on_hand_qty,
                     snapshot_in_transit_qty,
                     snapshot_unit_cost,
                     snapshot_unit_retail,
                     processed,
                     physical_count_qty,
                     pack_comp_qty,
                     dept,
                     class,
                     subclass,
                     xform_item_type)
             values (source.cycle_count,
                     source.location_type,
                     source.location,
                     source.item,
                     source.snapshot_on_hand_qty,
                     source.snapshot_in_transit_qty,
                     source.snapshot_unit_cost,
                     source.snapshot_unit_retail,  --Fix this for external finisher
                     source.processed,
                     source.stockcount_quantity,  --physical_count_qty
                     source.pack_comp_qty,
                     source.dept,
                     source.class,
                     source.subclass,
                     source.xform_item_type);

      -- Create stake_location records for pack item/locs not in stake_sku_loc
      -- For pack items snapshot_unit_cost and snapshot_unit_retail are inserted as 0
      -- This covers item/locations that are in the stock count parameter tables but not in RMS
      merge into stake_sku_loc target
      using (select distinct
                    sil.cycle_count,
                    sil.location_type,
                    sil.location,
                    sil.item,
                    0 snapshot_on_hand_qty,
                    0 snapshot_in_transit_qty,
                    0 snapshot_unit_cost,
                    0 snapshot_unit_retail,
                    'N' processed,
                    0 stockcount_quantity,  --physical_count_qty
                    0 pack_comp_qty,
                    sil.dept,
                    sil.class,
                    sil.subclass,
                    sil.xform_item_type
               from work_stkupld_item_loc_gtt sil
              where sil.status            = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg         is NULL     -- no errors
                ---
                and sil.ssl_processed_ind is NULL -- no stake_sku_loc record
                and sil.product_level_ind != 'I'
                and sil.pack_ind          = 'Y'
                ) source
         on (    target.cycle_count = source.cycle_count
             and target.loc_type    = source.location_type
             and target.location    = source.location
             and target.item        = source.item)
          when NOT matched then
             insert (cycle_count,
                     loc_type,
                     location,
                     item,
                     snapshot_on_hand_qty,
                     snapshot_in_transit_qty,
                     snapshot_unit_cost,
                     snapshot_unit_retail,
                     processed,
                     physical_count_qty,
                     pack_comp_qty,
                     dept,
                     class,
                     subclass,
                     xform_item_type)
             values (source.cycle_count,
                     source.location_type,
                     source.location,
                     source.item,
                     source.snapshot_on_hand_qty,
                     source.snapshot_in_transit_qty,
                     source.snapshot_unit_cost,
                     source.snapshot_unit_retail,  --Fix this for external finisher
                     source.processed,
                     source.stockcount_quantity,  --physical_count_qty
                     source.pack_comp_qty,
                     source.dept,
                     source.class,
                     source.subclass,
                     source.xform_item_type);

   end if;


   ----------------------------------------------------------------------
   -- WAREHOUSES
   ----------------------------------------------------------------------
   if GP_stkupld_fhead_row.location_type = 'W' then
      -- Create stake_sku_records for for non-pack item/locs and pack item components not in stake_sku_loc
      -- This covers item/locations that are in the stock count parameter tables but not in RMS
      merge into stake_sku_loc target
      using (select distinct
                    sil.cycle_count,
                    sil.location_type,
                    sil.location,
                    sil.item,
                    0 snapshot_on_hand_qty,
                    0 snapshot_in_transit_qty,
                    DECODE(LP_std_av_ind,
                           'A',
                           NVL(ils.av_cost, 0),
                           NVL(ils.unit_cost, 0)) snapshot_unit_cost,
                    DECODE(sil.location_type,
                           'E',
                           NVL(ils.finisher_av_retail, efr.finisher_av_retail),
                           NVL(ils.finisher_av_retail, il.unit_retail)) snapshot_unit_retail,
                    'N' processed,
                    -- pack item component gets a NULL quantity
                    sil.stockcount_quantity stockcount_quantity,
                    -- pack item component gets the stockcount quantity
                    0 pack_comp_qty,
                    sil.dept,
                    sil.class,
                    sil.subclass,
                    sil.xform_item_type
               from work_stkupld_item_loc_gtt sil,
                    work_stkupld_ext_fin_rtl_gtt efr,
                    item_loc il,
                    item_loc_soh ils
              where sil.status                = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg             is NULL     -- no errors
                ---
                and sil.ssl_processed_ind is NULL -- no stake_sku_loc record
                and sil.product_level_ind != 'I'
                and sil.pack_ind              = 'N'
                ---
                and sil.item                  = il.item
                and sil.location              = il.loc
                and il.item                   = ils.item (+)
                and il.loc                    = ils.loc (+)
                ---
                and efr.item (+)              = sil.item
                and efr.external_finisher (+) = sil.location
                ) source
         on (    target.cycle_count = source.cycle_count
             and target.loc_type    = source.location_type
             and target.location    = source.location
             and target.item        = source.item)
          when NOT matched then
             insert (cycle_count,
                     loc_type,
                     location,
                     item,
                     snapshot_on_hand_qty,
                     snapshot_in_transit_qty,
                     snapshot_unit_cost,
                     snapshot_unit_retail,
                     processed,
                     physical_count_qty,
                     pack_comp_qty,
                     dept,
                     class,
                     subclass,
                     xform_item_type)
             values (source.cycle_count,
                     source.location_type,
                     source.location,
                     source.item,
                     source.snapshot_on_hand_qty,
                     source.snapshot_in_transit_qty,
                     source.snapshot_unit_cost,
                     source.snapshot_unit_retail,  --Fix this for external finisher
                     source.processed,
                     source.stockcount_quantity,  --physical_count_qty
                     source.pack_comp_qty,
                     source.dept,
                     source.class,
                     source.subclass,
                     source.xform_item_type);

      -- Create stake_location records for pack item/locs not in stake_sku_loc
      -- For pack items snapshot_unit_cost and snapshot_unit_retail are inserted as 0
      -- This covers item/locations that are in the stock count parameter tables but not in RMS
      merge into stake_sku_loc target
      using (select distinct
                    sil.cycle_count,
                    sil.location_type,
                    sil.location,
                    sil.item,
                    0 snapshot_on_hand_qty,
                    0 snapshot_in_transit_qty,
                    0 snapshot_unit_cost,
                    0 snapshot_unit_retail,
                    'N' processed,
                    sil.stockcount_quantity,  --physical_count_qty
                    0 pack_comp_qty,
                    sil.dept,
                    sil.class,
                    sil.subclass,
                    sil.xform_item_type
               from work_stkupld_item_loc_gtt sil,
			        item_loc il
              where sil.status            = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                and sil.error_msg         is NULL     -- no errors
                ---
                and sil.ssl_processed_ind is NULL -- no stake_sku_loc record
                and sil.product_level_ind != 'I'
                and sil.pack_ind          = 'Y'
				and sil.item              = il.item
				and sil.location          = il.loc
                ) source
         on (    target.cycle_count = source.cycle_count
             and target.loc_type    = source.location_type
             and target.location    = source.location
             and target.item        = source.item)
          when NOT matched then
             insert (cycle_count,
                     loc_type,
                     location,
                     item,
                     snapshot_on_hand_qty,
                     snapshot_in_transit_qty,
                     snapshot_unit_cost,
                     snapshot_unit_retail,
                     processed,
                     physical_count_qty,
                     pack_comp_qty,
                     dept,
                     class,
                     subclass,
                     xform_item_type)
             values (source.cycle_count,
                     source.location_type,
                     source.location,
                     source.item,
                     source.snapshot_on_hand_qty,
                     source.snapshot_in_transit_qty,
                     source.snapshot_unit_cost,
                     source.snapshot_unit_retail,  --Fix this for external finisher
                     source.processed,
                     source.stockcount_quantity,  --physical_count_qty
                     source.pack_comp_qty,
                     source.dept,
                     source.class,
                     source.subclass,
                     source.xform_item_type);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_STAKE_SKU_LOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_STAKE_LOCATION(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                               I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.CREATE_STAKE_LOCATION';

BEGIN

   -- Create stake_location records for unit and dollar stock counts
   -- This covers item/locations that are in the stock count parameter tables but not in RMS
   merge into stake_location target
   using (select distinct
                 sil.process_id,
                 sil.chunk_id,
                 sil.cycle_count,
                 ---
                 sil.location_type,
                 sil.location
            from work_stkupld_item_loc_gtt sil
           where sil.status            = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and sil.error_msg         is NULL     -- no errors
             and sil.pack_ind          = 'N'  -- non-pack items
             ---
             and sil.product_level_ind != 'I'  -- unit and amount stock count
             and sil.ssl_processed_ind is NULL -- no stake_sku_loc record
             ) source
      on (    target.cycle_count = source.cycle_count
          and target.loc_type    = source.location_type
          and target.location    = source.location)
       when NOT matched then
          insert(cycle_count,
                 loc_type,
                 location)
          values(source.cycle_count,
                 source.location_type,
                 source.location);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_STAKE_LOCATION;
---------------------------------------------------------------------------------------------------------------------
FUNCTION RANGE_NEW_ITEM_LOC(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                            I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.RANGE_NEW_ITEM_LOC';
   L_flag      BOOLEAN      := FALSE;

   cursor C_RANGE is
      select distinct
             sil.item,
             sil.location,
             sil.location_type,
             sil.item_level,
             sil.tran_level
        from work_stkupld_item_loc_gtt sil
       where NVL(sil.new_itemloc_ind, '-999') = 'Y';

BEGIN

   -- Set new_itemloc_ind to 'Y' for records that need an item/location created
   -- These are valid unit and dollar stock counts non-pack items that
   -- do not have stake_sku_loc records and do not have item_loc records
   merge into work_stkupld_item_loc_gtt target
   using (select sil.location,
                 sil.stockcount_item,
                 sil.item,
                 NVL(sil.pack_item, '-999') pack_item
            from work_stkupld_item_loc_gtt sil
           where sil.status            = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and sil.error_msg         is NULL     -- no errors
             ---
             -- These are non-pack items in a unit and amount stock count
             and (sil.product_level_ind != 'I'  -- unit and amount stock count
                  or
             -- These are pack items whose merchandise hierarchy and location
             -- is not in stake_sku_loc and stake_prod_loc and do not have item_loc records
                 (    sil.pack_ind    = 'Y'  -- pack item
                  and NOT EXISTS(select 'x'
                                   from stake_prod_loc spl
                                  where spl.dept        = sil.dept
                                    and spl.class       = sil.class
                                    and spl.subclass    = sil.subclass
                                    and spl.cycle_count = sil.cycle_count
                                    and spl.loc_type    = sil.location_type
                                    and spl.location    = sil.location)))
             ---
             and sil.ssl_processed_ind is NULL -- no stake_sku_loc record
             ---
             and NOT EXISTS(select 'x'
                              from item_loc il
                             where il.item = sil.item
                               and il.loc  = sil.location)
             ) source
      on (    target.location               = source.location
          and target.stockcount_item        = source.stockcount_item
          and target.item                   = source.item
          and NVL(target.pack_item, '-999') = source.pack_item)
       when matched then
          update set target.new_itemloc_ind = 'Y';


   -- Call NEW_ITEM_LOC for each record that requires an item/location relationship
   FOR rec in C_RANGE LOOP
      ---
      if NEW_ITEM_LOC(O_error_message,
                      rec.item,
                      rec.location,
                      NULL, -- ITEM_PARENT
                      NULL, -- ITEM_GRANDPARENT
                      rec.location_type,
                      NULL, -- SHORT_DESC
                      NULL, -- DEPT
                      NULL, -- CLASS
                      NULL, -- SUBCLASS
                      rec.item_level,
                      rec.tran_level,
                      NULL, -- ITEM_STATUS
                      NULL, -- WASTE TYPE
                      NULL, -- DAILY WASTE PCT
                      NULL, -- SELLABLE_IND
                      NULL, -- ORDERABLE_IND
                      NULL, -- PACK_IND
                      NULL, -- PACK_TYPE
                      NULL, -- UNIT_COST_LOC
                      NULL, -- UNIT_RETAIL_LOC
                      NULL, -- SELLING_RETAIL_LOC
                      NULL, -- SELLING_UOM
                      NULL, -- ITEM_LOC_STATUS
                      NULL, -- TAXABLE_IND
                      NULL, -- TI
                      NULL, -- HI
                      NULL, -- STORE_ORD_MULT
                      NULL, -- MEAS_OF_EACH
                      NULL, -- MEAS_OF_PRICE
                      NULL, -- UOM_OF_PRICE
                      NULL, -- PRIMARY_VARIANT
                      NULL, -- PRIMARY_SUPP
                      NULL, -- PRIMARY_CNTRY
                      NULL, -- LOCAL_ITEM_DESC
                      NULL, -- LOCAL_SHORT_DESC
                      NULL, -- PRIMARY_COST_PACK
                      NULL, -- RECEIVE_AS_TYPE
                      NULL, -- DATE
                      L_flag) = FALSE then
         return FALSE;
      end if;
      ---
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RANGE_NEW_ITEM_LOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_PHYSICAL_WH_COUNTS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                       I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_dist_table                DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_dist_result_table         TYP_stkupld_distribute_tbl;
   L_current_qty               NUMBER(12,4) := 0;
   L_distribute_qty            NUMBER(12,4) := 0;
   L_dist_result_table_count   NUMBER(12)   := 0;
   L_vwh_no_qty_count          NUMBER(12)   := 0;
   L_sum_snapshot_qty          NUMBER(12,4) := 0;
   L_program                   VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.DISTRIBUTE_PHYSICAL_WH_COUNTS';

   cursor C_GET_ROLLUP_PHYSICAL_WH is
      select sil.cycle_count,
             sil.physical_location,
             sil.item,
             sil.pack_item,
             sil.stockcount_item,
             sil.item_xform_ind,
             sil.xform_item_type,
             sil.stockcount_quantity,
             SUM(GREATEST(NVL(sil.ssl_snapshot_on_hand_quantity, 0),0)) sum_snapshot_qty,
             SUM(GREATEST(NVL(sil.ils_stock_on_hand, 0),0)) sum_ils_soh
        from work_stkupld_item_loc_gtt sil
       where sil.status            = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
         and sil.error_msg         is NULL
         and sil.location_type     = 'W'
         ---
         -- exclude the exploded component items when rolling up for the distribution logic
         and ((sil.pack_ind = 'N' and sil.pack_item is NULL) or -- non-pack item components
               sil.pack_ind = 'Y') -- pack items
       group by sil.cycle_count,
                sil.physical_location,
                sil.stockcount_quantity,
                sil.item,
                sil.pack_item,
                sil.stockcount_item,
                sil.item_xform_ind,
                sil.xform_item_type;

   -- Define the pl/sql collection for the summed-up physical warehouse quantities
   TYPE rollup_physical_wh_type IS TABLE OF C_GET_ROLLUP_PHYSICAL_WH%ROWTYPE INDEX BY BINARY_INTEGER;
   rollup_physical_wh_tbl rollup_physical_wh_type;

   -- Define the pl/sql collection for locations that have no quantities for distribution
   TYPE vwh_no_qty_type IS TABLE OF C_GET_ROLLUP_PHYSICAL_WH%ROWTYPE INDEX BY BINARY_INTEGER;
   vwh_no_qty_tbl vwh_no_qty_type;

BEGIN

   -- Sum up the stake_sku_loc and item_loc_soh quantities up to the physical warehouse
   -- Bulk collect the records into a collection to be passed into the DISTRIBUTE_SQL function per row
   open C_GET_ROLLUP_PHYSICAL_WH;
   fetch C_GET_ROLLUP_PHYSICAL_WH BULK COLLECT into rollup_physical_wh_tbl;
   close C_GET_ROLLUP_PHYSICAL_WH;

   -- Perform the distribution logic only if we have rows to process
   if rollup_physical_wh_tbl is NOT NULL then
      if rollup_physical_wh_tbl.COUNT > 0 then
         FOR i in rollup_physical_wh_tbl.first..rollup_physical_wh_tbl.last LOOP
            ---
            -- Reset per loop
            -- Distribution table(L_dist_table) is reset in DISTRIBUTION_SQL
            L_current_qty := 0;
            L_distribute_qty := 0;
            L_sum_snapshot_qty := rollup_physical_wh_tbl(i).sum_snapshot_qty;
            -- Transformed sellable-only items uses the stockcount quantity for distribution
            if rollup_physical_wh_tbl(i).xform_item_type = 'S' then
               L_distribute_qty := NVL(rollup_physical_wh_tbl(i).stockcount_quantity, 0);
            else
               -- Otherwise check if there are quantities to distribute by subtracting either the
               -- stake_sku_loc.snapshot_on_hand_qty or the item_loc_soh.stock_on_hand from the stockcount quantity
               if NVL(rollup_physical_wh_tbl(i).sum_snapshot_qty, 0) <> 0 then
                  L_current_qty := rollup_physical_wh_tbl(i).sum_snapshot_qty;
               -- Use the stock-on-hand quantity from item_loc_soh if the snapshot quantity is zero
               else
                  L_current_qty := rollup_physical_wh_tbl(i).sum_ils_soh;
               end if;

               -- Determine if there are any quantities to distribute
               -- Subtract the current quantity from the stock count quantity
               L_distribute_qty := NVL(rollup_physical_wh_tbl(i).stockcount_quantity, 0) - L_current_qty;

            end if;

            -- Distribute non-zero quantities
            -- Negative quantities are also distributed
            if NVL(L_distribute_qty, 0) <> 0 then
               if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                              L_dist_table,
                                              rollup_physical_wh_tbl(i).item,
                                              rollup_physical_wh_tbl(i).physical_location,
                                              L_distribute_qty,
                                              'STKREC',
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              rollup_physical_wh_tbl(i).cycle_count) = FALSE then
                  return FALSE;
               end if;

               -- Check if the distribution table is empty
               if (L_dist_table.COUNT = 0) then
                  O_error_message := SQL_LIB.CREATE_MSG('NO_DIST_WH',
                                                        SQLERRM,
                                                        'STKCOUNT_SQL.STOCK_DISTRIBUTION',
                                                        TO_CHAR(SQLCODE));
                  return FALSE;
               end if;

               -- Insert the distribution results in to a temporary working table
               -- This table will be used to update work_stkupld_item_loc_gtt at the end of the loop
               FORALL j in 1..L_dist_table.COUNT
                  insert into work_stkupld_vwh_dist_qty_gtt
                        (cycle_count,
                         physical_wh,
                         virtual_wh,
                         item,
                         pack_item,
                         stockcount_item,
                         item_xform_ind,
                         dist_qty)
                  values(rollup_physical_wh_tbl(i).cycle_count,
                         rollup_physical_wh_tbl(i).physical_location,
                         L_dist_table(j).wh,
                         rollup_physical_wh_tbl(i).item,
                         rollup_physical_wh_tbl(i).pack_item,
                         rollup_physical_wh_tbl(i).stockcount_item,
                         rollup_physical_wh_tbl(i).item_xform_ind,
                         L_dist_table(j).dist_qty);
                         
               -- Add virtual warehouses that are not part of the L_dist_table. This will update the stockcount_quantity to 0.
               insert into work_stkupld_vwh_dist_qty_gtt
                     (cycle_count,
                      physical_wh,
                      virtual_wh,
                      item,
                      pack_item,
                      stockcount_item,
                      item_xform_ind,
                      dist_qty)
                select cycle_count,
                       physical_location,
                       location,
                       item,
                       pack_item,
                       stockcount_item,
                       item_xform_ind,
                       0
                  from work_stkupld_item_loc_gtt w
                 where w.cycle_count = rollup_physical_wh_tbl(i).cycle_count 
                   and w.item = rollup_physical_wh_tbl(i).item
                   and NVL(w.pack_item,'-999') = NVL(rollup_physical_wh_tbl(i).pack_item,'-999')
                   and w.stockcount_item = rollup_physical_wh_tbl(i).stockcount_item
                   and w.physical_location = rollup_physical_wh_tbl(i).physical_location
                   and not exists (select 'x'
                                     from work_stkupld_vwh_dist_qty_gtt w1
                                    where w1.item = w.item
                                      and NVL(w1.pack_item,'-999') = NVL(w.pack_item,'-999')
                                      and w1.stockcount_item = w.stockcount_item
                                      and w1.physical_wh = w.physical_location
                                      and w1.virtual_wh = w.location);
            else
               -- No quantity to distribute
               -- Copy to a collection for updating work_stkupld_item_loc_gtt after loop
               insert into work_stkupld_pwh_no_dist_gtt
                     (cycle_count,
                      physical_wh,
                      item,
                      pack_item,
                      stockcount_item,
                      item_xform_ind)
               values(rollup_physical_wh_tbl(i).cycle_count,
                      rollup_physical_wh_tbl(i).physical_location,
                      rollup_physical_wh_tbl(i).item,
                      rollup_physical_wh_tbl(i).pack_item,
                      rollup_physical_wh_tbl(i).stockcount_item,
                      rollup_physical_wh_tbl(i).item_xform_ind);
            end if;
         end LOOP;

         -- Update work_stkupld_item_loc_gtt using the distribution quantities
         -- The transformed sellable-only attribute determines how the distributed quantities
         -- are used to update work_stkupld_item_loc_gtt.stockcount_quantity.
         merge into work_stkupld_item_loc_gtt target
         using
         (select cycle_count,
                 physical_wh,
                 virtual_wh,
                 item,
                 pack_item,
                 stockcount_item,
                 item_xform_ind,
                 dist_qty
            from work_stkupld_vwh_dist_qty_gtt) source
         on (   target.cycle_count       = source.cycle_count
            and target.physical_location = source.physical_wh
            and target.location          = source.virtual_wh
            and target.item              = source.item
            and NVL(target.pack_item, '-999') = NVL(source.pack_item, '-999')
            and target.stockcount_item   = source.stockcount_item)
         when matched then
            update set target.stockcount_quantity = case when target.xform_item_type = 'S'then
                                                       NVL(source.dist_qty, 0)
                                                    else
                                                       case when L_sum_snapshot_qty <> 0 then
                                                          GREATEST(NVL(target.ssl_snapshot_on_hand_quantity, 0),0) + NVL(source.dist_qty, 0)
                                                       else
                                                          GREATEST(NVL(target.ils_stock_on_hand, 0),0) + NVL(source.dist_qty, 0)
                                                       end
                                                    end;

         -- Update work_stkupld_item_loc_gtt for rows without any distribution
         -- Current values on stake_sku_loc.snapshot_on_hand_qty or item_loc_soh.stock_on_hand (transformed sellable)
         -- is used to update the stockcount_quantity on the work_stkupld_item_loc_gtt table
         merge into work_stkupld_item_loc_gtt target
         using
         (select sil.cycle_count,
                 sil.physical_location,
                 sil.location,
                 sil.item,
                 sil.pack_item,
                 sil.stockcount_item,
                 sil.item_xform_ind
            from work_stkupld_item_loc_gtt sil,
                 work_stkupld_pwh_no_dist_gtt sd
           where sil.status        = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and sil.error_msg     is NULL
             and sil.location_type = 'W'
             ---
             and sil.cycle_count       = sd.cycle_count
             and sil.physical_location = sd.physical_wh
             and sil.item              = sd.item
             and NVL(sil.pack_item, '-999') = NVL(sil.pack_item, '-999')
             and sil.stockcount_item   = sd.stockcount_item) source
         on (   target.cycle_count       = source.cycle_count
            and target.physical_location = source.physical_location
            and target.location          = source.location
            and target.item              = source.item
            and NVL(target.pack_item, '-999') = NVL(source.pack_item, '-999')
            and target.stockcount_item   = source.stockcount_item)
         when matched then
            update set target.stockcount_quantity = case when target.xform_item_type = 'S'then
                                                       GREATEST(NVL(target.stockcount_quantity, 0),0) -- no change in qty for transform sellable items.
                                                    else
                                                       case when L_sum_snapshot_qty <> 0 then
                                                          GREATEST(NVL(target.ssl_snapshot_on_hand_quantity, 0),0) 
                                                       else
                                                          GREATEST(NVL(target.ils_stock_on_hand, 0),0) 
                                                       end
                                                    end;

         -- distribute virtual warehouse quantity to different location_descriptions
         merge into work_stkupld_stake_qty_gtt target
            using (with
                        v1 as
                           (select sil.cycle_count,
                                   sil.physical_location,
                                   sil.location,
                                   sil.item,
                                   sil.pack_item,
                                   sil.stockcount_item,
                                   sil.item_xform_ind,
                                   sq.location_description,
                                   sq.stockcount_quantity,
                                   sq.uom_class,
                                   sil.stockcount_quantity * (sq.stockcount_quantity / sum(sq.stockcount_quantity) over (partition by sq.cycle_count,sq.item, sq.pack_item,sq.stockcount_item, sq.physical_location, sq.location)) valu           
                              from work_stkupld_item_loc_gtt sil,
                                   work_stkupld_stake_qty_gtt sq
                             where sil.cycle_count = sq.cycle_count
                               and sil.physical_location = sq.physical_location
                               and sil.location = sq.location
                               and sil.stockcount_quantity <> 0
                               and sq.stockcount_quantity <> 0
                               and sil.item = sq.item
                               and NVL(sil.pack_item,'-999') = NVL(sq.pack_item,'-999')
                               and sil.stockcount_item        = sq.stockcount_item
                               -- exclude the exploded component items
                               and ((sil.pack_ind = 'N' and sil.pack_item is NULL) or -- non-pack item components
                                   sil.pack_ind = 'Y') -- pack items                               
                             )
                             select row#,
                                    cycle_count,
                                    physical_location,
                                    location,
                                    item,
                                    pack_item,
                                    stockcount_item,
                                    item_xform_ind,
                                    location_description,
                                    valu,
                                    case when uom_class in ('QTY','PACK') then 
                                       dist_qty
                                    else
                                       valu
                                    end as disc_quantity
                               from v1
                               model
                                  partition by (cycle_count,
                                                item,
                                                pack_item,
                                                stockcount_item,
                                                physical_location,
                                                location,
                                                item_xform_ind,
                                                uom_class)
                                  dimension by (row_number() over (partition by item,pack_item,stockcount_item,physical_location,location order by location_description) AS row# )
                                  measures (location_description,
                                            valu,
                                            cast(null as number) valu_plus_remainder,
                                            cast(null as number) dist_qty) 
                                  rules update sequential order
                                  (valu_plus_remainder[row#]   = valu[cv()],
                                   valu_plus_remainder[row#>1] = valu[cv()] + (valu_plus_remainder[cv()-1] - round(valu_plus_remainder[cv()-1])),
                                   dist_qty[1]       = round(valu[cv()]),
                                   dist_qty[row#>1]  = round(valu_plus_remainder[cv()])
                                   ) 
                                order by item,pack_item,stockcount_item,location,location_description,row#
                               ) source
                 on (   target.cycle_count       = source.cycle_count
                    and target.physical_location = source.physical_location
                    and target.location          = source.location
                    and target.item              = source.item
                    and NVL(target.pack_item, '-999') = NVL(source.pack_item, '-999')
                    and NVL(target.stockcount_item,'-999')   = NVL(source.stockcount_item,'-999')
                    and target.location_description = source.location_description)
                 when matched then
             update set target.stake_quantity = source.disc_quantity;        
      end if;
   end if;

   -- Update the stockcount_quantity for pack item components after the distribution logic

   merge into work_stkupld_item_loc_gtt target
   using (select distinct
                 sil_pack.cycle_count,
                 sil_pack.item,
                 sil_pack.pack_item,
                 vpq.item comp_item,
                 sil_pack.pack_ind,
                 sil_pack.location,
                 NVL(sil_pack.stockcount_quantity, 0) * vpq.qty stockcount_qty
            from work_stkupld_item_loc_gtt sil_pack,      -- pack item set
                 v_packsku_qty vpq
           where sil_pack.status        = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and sil_pack.error_msg     is NULL
             and sil_pack.location_type = 'W'
             and sil_pack.pack_ind      = 'Y'
             -- get pack item component quantities
             and vpq.pack_no = sil_pack.item) source
   on (    target.cycle_count            = source.cycle_count
       and target.item                   = source.comp_item
       and NVL(target.pack_item, '-999') = source.item
       and target.location               = source.location
       and target.status                 = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
       and target.error_msg              is NULL
       and target.location_type          = 'W'
       and target.pack_ind               = 'N'
       and target.pack_item              is NOT NULL)
   when matched then
      update set target.stockcount_quantity = source.stockcount_qty;

   -- merge component items for work_stkupld_stake_qty_gtt after distribution
   merge into work_stkupld_stake_qty_gtt target
   using (select distinct
                 sil_pack.cycle_count,
                 sil_pack.item,
                 sil_pack.pack_item,
                 vpq.item comp_item,
                 sil_pack.pack_ind,
                 sil_pack.location,
                 sil_pack.location_description,
                 NVL(sil_pack.stake_quantity, 0) * vpq.qty stake_qty
            from work_stkupld_stake_qty_gtt sil_pack,      -- pack item set
                 v_packsku_qty vpq
           where sil_pack.status        = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and sil_pack.error_msg     is NULL
             and sil_pack.location_type = 'W'
             and sil_pack.pack_ind      = 'Y'
             -- get pack item component quantities
             and vpq.pack_no = sil_pack.item) source
   on (    target.cycle_count            = source.cycle_count
       and target.item                   = source.comp_item
       and NVL(target.pack_item, '-999') = source.item
       and target.location               = source.location
       and target.location_description   = source.location_description
       and target.status                 = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
       and target.error_msg              is NULL
       and target.location_type          = 'W'
       and target.pack_ind               = 'N'
       and target.pack_item              is NOT NULL)
   when matched then
      update set target.stake_quantity = source.stake_qty;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DISTRIBUTE_PHYSICAL_WH_COUNTS;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_NON_FATAL(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                            I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.VALIDATE_NON_FATAL';

BEGIN

   merge into work_stkupld_item_loc_gtt target
   using (
      select distinct
             sil.location,
             sil.stockcount_item,
             sil.item,
             NVL(sil.pack_item, '-999') pack_item,
             -------------------------------------------------------------
             -- Validate if the item exists on item_master
             -------------------------------------------------------------
             case when sil.item is NULL
                  then SQL_LIB.CREATE_MSG('INV_SKU_STAPLE',
                                          sil.item,
                                          NULL,
                                          NULL)
                  else NULL
             end as inv_item_err,
             -------------------------------------------------------------
             -- Validate a non-xform item if it's an inventory item
             -------------------------------------------------------------
             case when sil.inventory_ind = 'N' and
                       sil.item_xform_ind = 'N'
                  then SQL_LIB.CREATE_MSG('STKCNT_NON_INV_ITEM',
                                          NULL,
                                          NULL,
                                          NULL)
                  else NULL
             end as inv_xform_err,
             -------------------------------------------------------------
             -- Check if transformed sellable items have details
             -------------------------------------------------------------
             case when ((sil.item_xform_ind = 'Y' and sellable_ind = 'Y') and
                        ((sil.stocktake_type = 'B' and pack_ind = 'N') or
                         (sil.stocktake_type = 'U' and pack_ind = 'N' and sil.product_level_ind is NOT NULL)))
                       and (sxi.xform_sellable_item is NULL)
                  then SQL_LIB.CREATE_MSG('STKCNT_NO_XFORM_INFO',
                                          NULL,
                                          NULL,
                                          NULL)
                  else NULL
             end as no_xform_info_err,
             -------------------------------------------------------------
             -- Check that the item/loc/dept/class/subclass
             -- is in stake_prod_loc for a unit and cost stock count
             -------------------------------------------------------------
             case when sil.stocktake_type                = 'B' and -- unit and cost stock count
                       sil.pack_ind                      = 'N' and
                       NVL(sil.xform_item_type, '-999') != 'S' and
                       spl.dept is NULL -- no stake_prod_loc record
                  then SQL_LIB.CREATE_MSG('STKCNT_NO_STAKE_PROD_LOC',
                                          NULL,
                                          NULL,
                                          NULL)
                  else NULL
             end as no_stake_prod_loc_rec_err,
             -------------------------------------------------------------
             -- Check that the item's merch hierarchy
             -- is in stake_product for a unit only department level stock count,
             -------------------------------------------------------------
             case when sil.stocktake_type    = 'U' and -- unit only stock count
                       sil.pack_ind          = 'N' and
                       sil.product_level_ind = 'D' and
                       sp.dept            is NULL -- no stake_product record
                  then SQL_LIB.CREATE_MSG('STKCNT_PROD_LVL_DEPT',
                                          sil.item,
                                          NULL,
                                          NULL)
                  else NULL
             end as no_stake_prod_dept_rec_err,
             -------------------------------------------------------------
             -- Check that the item's merch hierarchy
             -- is in stake_product for a unit only class level stock count
             -------------------------------------------------------------
             case when sil.stocktake_type    = 'U' and -- unit only stock count
                       sil.pack_ind          = 'N' and
                       sil.product_level_ind = 'C' and
                       sp.class            is NULL -- no stake_product record
                  then SQL_LIB.CREATE_MSG('STKCNT_PROD_LVL_CLASS',
                                          sil.item,
                                          NULL,
                                          NULL)
                  else NULL
             end as no_stake_prod_class_rec_err,
             -------------------------------------------------------------
             -- Check that the item's merch hierarchy
             -- is in stake_product for a unit only subclass level stock count
             -------------------------------------------------------------
             case when sil.stocktake_type    = 'U' and -- unit only stock count
                       sil.pack_ind          = 'N' and
                       sil.product_level_ind = 'S' and
                       sp.subclass           is NULL -- no stake_product record
                  then SQL_LIB.CREATE_MSG('STKCNT_PROD_LVL_SUBCLASS',
                                          sil.item,
                                          NULL,
                                          NULL)
                  else NULL
             end as no_stake_prod_subclass_rec_err,
             -------------------------------------------------------------
             -- Check if the stock count has been processed for the item/loc
             -------------------------------------------------------------
             case when sil.ssl_processed_ind = 'P'
                  then SQL_LIB.CREATE_MSG('STKCNT_PROCESSED',
                                          NULL,
                                          NULL,
                                          NULL)
                  else NULL
             end as ssl_processed_err,
             -------------------------------------------------------------
             -- Check if there is an existing stake_sku_loc for a unit only stock count
             -- Only unit and dollar stock counts will generate a new item/location relationship
             -- Reject unit only stock counts if there is not stake_sku_loc record
             -------------------------------------------------------------
             case when sil.ssl_processed_ind is NULL and -- no stake_sku_loc record
                       sil.product_level_ind = 'I' -- unit only stock count
                  then SQL_LIB.CREATE_MSG('STKCNT_UNIT_NO_ITEM_LOC',
                                          NULL,
                                          NULL,
                                          NULL)
                  else NULL
             end as ssl_no_itemloc_err
        from work_stkupld_item_loc_gtt sil,
             stake_prod_loc spl,  -- for unit and unit and cost stock counts
             stake_product sp,    -- for unit only stock counts
             -- LOOKUP SET: Get the set of xform sellable items
             (select distinct  -- there can be multiple occurences of the sellable item
                     sx.cycle_count,
                     sx.location_type,
                     sx.location,
                     sx.stockcount_item xform_sellable_item
                from work_stkupld_item_loc_gtt sx
               where sx.item_xform_ind = 'Y'
                 and sx.xform_item_type = 'O') sxi
       where sil.cycle_count         = spl.cycle_count(+)
         and sil.location_type       = spl.loc_type(+)
         -- for warehouses, sil.location and spl.location are both virtual warehouses
         and sil.location            = spl.location(+)
         and sil.dept                = spl.dept(+)
         and sil.class               = spl.class(+)
         and sil.subclass            = spl.subclass(+)
         ---
         and NVL(sil.xform_item_type, '-999')     != 'O'
         and sil.cycle_count         = sp.cycle_count(+)
         and sil.dept                = sp.dept(+)
         and NVL(sil.class, -999)    = NVL(sp.class, NVL(sil.class, -999))
         and NVL(sil.subclass, -999) = NVL(sp.subclass, NVL(sil.subclass, -999))
         ---
         and sil.cycle_count         = sxi.cycle_count(+)
         -- for warehouses, sil.location and sxi.location are both virtual warehouses
         and sil.location            = sxi.location(+)
         and sil.item                = sxi.xform_sellable_item(+)
         ) source
   on (    target.location               = source.location
       and target.stockcount_item        = source.stockcount_item
       and target.item                   = source.item
       and NVL(target.pack_item, '-999') = source.pack_item)
   when matched then
   update set target.error_msg =
      source.inv_item_err || DECODE(source.inv_item_err, NULL, NULL, ';') ||
      source.inv_xform_err || DECODE(source.inv_xform_err, NULL, NULL, ';') ||
      source.no_xform_info_err || DECODE(source.no_xform_info_err, NULL, NULL, ';') ||
      source.no_stake_prod_loc_rec_err || DECODE(source.no_stake_prod_loc_rec_err, NULL, NULL, ';') ||
      source.no_stake_prod_dept_rec_err || DECODE(source.no_stake_prod_dept_rec_err, NULL, NULL, ';') ||
      source.no_stake_prod_class_rec_err || DECODE(source.no_stake_prod_class_rec_err, NULL, NULL, ';') ||
      source.no_stake_prod_subclass_rec_err || DECODE(source.no_stake_prod_subclass_rec_err, NULL, NULL, ';') ||
      source.ssl_processed_err || DECODE(source.ssl_processed_err, NULL, NULL, ';') ||
      source.ssl_no_itemloc_err || DECODE(source.ssl_no_itemloc_err, NULL, NULL, ';'),
      target.status = DECODE((source.inv_item_err ||
                              source.inv_xform_err ||
                              source.no_xform_info_err ||
                              source.no_stake_prod_loc_rec_err ||
                              source.no_stake_prod_dept_rec_err ||
                              source.no_stake_prod_class_rec_err ||
                              source.no_stake_prod_subclass_rec_err ||
                              source.ssl_processed_err ||
                              source.ssl_no_itemloc_err), NULL, target.status,
                              CORESVC_STOCK_UPLOAD_SQL.REJECTED); --rejected

   -- validation for stake_qty
   merge into work_stkupld_stake_qty_gtt target
   using (
      select distinct
             sil.location,
             sil.stockcount_item,
             sil.item,
             NVL(sil.pack_item, '-999') pack_item,
             -------------------------------------------------------------
             -- Check that the location description is NOT NULL
             -------------------------------------------------------------
             case when sil.location_description is NULL
                  then SQL_LIB.CREATE_MSG('STKCNT_NO_LOC_DESC',
                                          NULL,
                                          NULL,
                                          NULL)
                  else NULL
             end as no_loc_desc_err
        from work_stkupld_stake_qty_gtt sil
         ) source
   on (    target.location               = source.location
       and target.stockcount_item        = source.stockcount_item
       and target.item                   = source.item
       and NVL(target.pack_item, '-999') = source.pack_item)
   when matched then
   update set target.error_msg =
      source.no_loc_desc_err || DECODE(source.no_loc_desc_err, NULL, NULL, ';'),
      target.status = DECODE(source.no_loc_desc_err, NULL, target.status,
                              CORESVC_STOCK_UPLOAD_SQL.REJECTED); --rejected

   -- merge error to work_stkupld_item_loc_gtt                      
   merge into work_stkupld_item_loc_gtt target
   using (
      select distinct
             sil.location,
             sil.stockcount_item,
             sil.item,
             NVL(sil.pack_item, '-999') pack_item,
             error_msg,
             status
        from work_stkupld_stake_qty_gtt sil
         ) source
   on (    target.location               = source.location
       and target.stockcount_item        = source.stockcount_item
       and target.item                   = source.item
       and NVL(target.pack_item, '-999') = source.pack_item)
   when matched then
   update set target.error_msg = target.error_msg || source.error_msg,
              target.status = decode(target.status,CORESVC_STOCK_UPLOAD_SQL.REJECTED,target.status,source.status);

   -- merge error to work_stkupld_stake_qty_gtt                      
   merge into work_stkupld_stake_qty_gtt target
   using (
      select distinct
             sil.location,
             sil.stockcount_item,
             sil.item,
             NVL(sil.pack_item, '-999') pack_item,
             error_msg,
             status
        from work_stkupld_item_loc_gtt sil
         ) source
   on (    target.location               = source.location
       and target.stockcount_item        = source.stockcount_item
       and target.item                   = source.item
       and NVL(target.pack_item, '-999') = source.pack_item)
   when matched then
   update set target.error_msg = source.error_msg,
              target.status = decode(target.status,CORESVC_STOCK_UPLOAD_SQL.REJECTED,target.status,source.status);
  
   --For a unit only stock count, if sellable xform item is on stake_sku_loc
   --then check if the orderable is there.  If not in stake_sku_loc, flag as rejected.
   merge into work_stkupld_item_loc_gtt target
   using (
            select distinct  -- there can be multiple occurences of the sellable item
                   sx.cycle_count,
                   sx.location_type,
                   sx.location,
                   sx.stockcount_item xform_sellable_item,
                   sx.item xform_orderable_item
              from work_stkupld_item_loc_gtt sx
             where sx.item_xform_ind = 'Y'
               and sx.xform_item_type = 'O'
               and sx.item != sx.stockcount_item
               and NOT EXISTS (select 'x'
                                 from stake_sku_loc ssl
                                where ssl.cycle_count = sx.cycle_count
                                  and ssl.loc_type    = sx.location_type
                                  and ssl.location    = sx.location
                                  and ssl.item        = sx.item
                                  and rownum          = 1)) source
   on (    target.location               = source.location
       and target.item                   = source.xform_sellable_item
       and target.stocktake_type         = 'U'
       and NVL(target.xform_item_type, '-999') = 'S'
       and target.cycle_count = source.cycle_count
       AND target.pack_item IS NULL)
   when matched then
      update set target.error_msg = target.error_msg || SQL_LIB.CREATE_MSG('STKCNT_PROCESSED',
                                                                           NULL,
                                                                           NULL,
                                                                           NULL),
                 target.status = CORESVC_STOCK_UPLOAD_SQL.REJECTED;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_NON_FATAL;
---------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_WORK_TABLES(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.POPULATE_WORK_TABLES';

BEGIN

   if GP_stkupld_fhead_row.location_type = 'W' then

      ----------------------------------------------------------------------------------------
      -- WAREHOUSE PROCESSING
      ----------------------------------------------------------------------------------------
      -- Populate stkupld_item_loc for warehouse locations
         -- This table holds item-related information for items in the stock count
         -- It is assummed that there can be multiple records for the same cycle count/location/item/location description
         -- Get the item parent information of any item in the stock count that is REF item and item_level > tran_level
         -- Potential to get the same item on stkupld_item if it has multiple child REF items on the stock count
         -- Physical warehouse are exploded into their virtual warehouses
      insert into work_stkupld_item_loc_gtt(process_id,
                                        chunk_id,
                                        cycle_count,
                                        stocktake_type,
                                        product_level_ind,
                                        location_type,
                                        physical_location,  --physical wh for wh, stores for stores
                                        location,
                                        ---
                                        item,            --item parent for REF item
                                        pack_item,       --pack item of exploded components
                                        stockcount_item, --item in the stock count parameter table or REF item
                                        item_type,
                                        stockcount_quantity,
                                        dept,
                                        class,
                                        subclass,
                                        item_level,
                                        tran_level,
                                        item_status,
                                        pack_ind,
                                        simple_pack_ind,
                                        item_xform_ind,
                                        sellable_ind,
                                        orderable_ind,
                                        inventory_ind,
                                        location_description,
                                        xform_item_type,
                                        ---
                                        ils_stock_on_hand,
                                        ---
                                        ssl_snapshot_on_hand_quantity,
                                        ssl_physical_count_quantity,
                                        ssl_pack_comp_quantity,
                                        ssl_processed_ind,
                                        ---
                                        stake_quantity,
                                        ---
                                        new_itemloc_ind,
                                        status,
                                        error_msg)
      select I_process_id,
             I_chunk_id,
             header_set.cycle_count,
             header_set.stocktake_type,
             header_set.product_level_ind,
             header_set.location_type,
             header_set.location,  --physical warehouse
             wh.wh,                --virtual warehouse
             ---
             im.item,
             NULL,  --pack_item
             detail_set.stockcount_item,
             detail_set.item_type,
             detail_set.sum_inventory_quantity,
             im.dept,
             im.class,
             im.subclass,
             im.item_level,
             im.tran_level,
             im.status,
             im.pack_ind,
             im.simple_pack_ind,
             im.item_xform_ind,
             im.sellable_ind,
             im.orderable_ind,
             im.inventory_ind,
             NULL, -- Location description
             -- Determine xform_item_type based on item_master information
             case when im.inventory_ind  = 'N' and
                       im.item_xform_ind = 'Y' and
                       im.sellable_ind   = 'Y' then
                     'S' -- transformed sellable
                  when im.inventory_ind  = 'Y' and
                       im.item_xform_ind = 'Y' and
                       im.orderable_ind  = 'Y' then
                     'O' -- transformed orderable
                  else
                     NULL
             end,
             ---
             NULL, --ils_stock_on_hand
             ---
             NULL, --ssl_snapshot_on_hand_quantity
             NULL, --ssl_physical_count_qty
             NULL, --ssl_pack_comp_quantity
             NULL, --ssl_processed_ind
             ---
             NULL, --stake_quantity
             ---
             NULL, --new_itemloc_ind
             detail_set.status,
             NULL  --error_msg
        from item_master im,
             wh,  --join to explode to virtual warehouses
             ---
             (select rolled_up_details.process_id,
                     rolled_up_details.item_type,
                     rolled_up_details.stockcount_item,
                      case
                      when rolled_up_details.item_type = 'REF' AND im2.item_level > im2.tran_level THEN
                         im2.item_parent
                      else
                         im2.item
                      end item,
                     rolled_up_details.status,
                     rolled_up_details.sum_inventory_quantity
                from -- There can be multiple records for a cycle count/item/loc/location description
                     -- Do a rollup of the inventory quantities to only have 1 record
                     (select sfd.process_id,
                             sfd.item_type,
                             sfd.item_value stockcount_item,
                             sfd.status,
                             SUM(sfd.inventory_quantity) sum_inventory_quantity
                        from svc_stkupld_fdetl sfd
                       where sfd.process_id = I_process_id
                         and sfd.chunk_id   = I_chunk_id
                         and sfd.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD  --validated item record
                       group by sfd.process_id,
                                sfd.item_type,
                                sfd.item_value,
                                sfd.status) rolled_up_details,
                     item_master im2
               -- Outer join is done so that stkupld_item_loc will always have a row from stkupld_fdetl
               -- Rows without item_master details will be validated in a later process
               where rolled_up_details.stockcount_item = im2.item(+)) detail_set,
             -- header_set will do a Cartesian join
             (select sh.cycle_count,
                     sh.stocktake_type,
                     NVL(sh.product_level_ind, 'I') product_level_ind,
                     sh.loc_type location_type,
                     sf.location location
                from svc_stkupld_fhead sf,
                     stake_head sh
               where sf.process_id       = I_process_id
                 and sf.status           = CORESVC_STOCK_UPLOAD_SQL.PROCESSED --validated header record
                 and sf.cycle_count      = sh.cycle_count
                 and TO_DATE(TO_CHAR(sf.stock_take_date,'YYYYMMDD'),'YYYYMMDD')  = sh.stocktake_date) header_set,
             (select sfd.item_value item,
                     il.loc loc
                from item_loc il ,
                     svc_stkupld_fdetl sfd,
                     svc_stkupld_fhead sf, wh wh
               where il.item  = sfd.item_value
                 and il.loc = wh.wh
                 and sf.location  = wh.physical_wh
                 and wh.wh   != wh.physical_wh
                 and sfd.process_id = sf.process_id
                 and sfd.process_id = I_process_id
                 and sfd.chunk_id   = I_chunk_id
                 and sfd.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
              union 
              select sfd.item_value item,
                     wh.wh loc 
                from svc_stkupld_fdetl sfd,svc_stkupld_fhead sf, wh wh
               where sf.location    = wh.physical_wh
                 and wh.wh          != wh.physical_wh
                 and sfd.process_id = sf.process_id 
                 and sfd.process_id = I_process_id
                 and sfd.chunk_id   = I_chunk_id
                 and sfd.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                 and NOT EXISTS (select 'x'   
                                   from item_loc il2 ,
                                        wh wh2
                                  where il2.item        = sfd.item_value
                                    and il2.loc_type =  'W'
                                    and wh2.physical_wh = sf.location
                                    and wh2.wh  = il2.loc
                                    and rownum  = 1)) item_loc
         where im.item(+)           = detail_set.item
           and header_set.location  = wh.physical_wh
           and wh.wh               != wh.physical_wh
           and detail_set.item      = item_loc.item
           and item_loc.loc         = wh.wh;
           
      insert into work_stkupld_stake_qty_gtt(process_id,
                                        chunk_id,
                                        cycle_count,
                                        location_type,
                                        physical_location,  --physical wh for wh, stores for stores
                                        location,
                                        ---
                                        item,            --item parent for REF item
                                        pack_item,       --pack item of exploded components
                                        stockcount_item, --item in the stock count parameter table or REF item
                                        stockcount_quantity,
                                        location_description,
                                        pack_ind,
                                        item_xform_ind,
                                        stake_quantity,
                                        uom_class,
                                        status,
                                        error_msg)
      select I_process_id,
             I_chunk_id,
             header_set.cycle_count,
             header_set.location_type,
             header_set.location,  --physical warehouse
             wh.wh,                --virtual warehouse
             ---
             im.item,
             NULL,  --pack_item
             detail_set.stockcount_item,
             detail_set.sum_inventory_quantity,
             detail_set.location_description, 
             im.pack_ind,
             im.item_xform_ind,
             NULL, --stake_quantity,
             uc.uom_class,
             detail_set.status,
             NULL -- error_msg
        from item_master im,
             uom_class uc,
             wh,  --join to explode to virtual warehouses
             ---
             (select rolled_up_details.process_id,
                     rolled_up_details.item_type,
                     rolled_up_details.stockcount_item,
                      case
                      when rolled_up_details.item_type = 'REF' AND im2.item_level > im2.tran_level THEN
                         im2.item_parent
                      else
                         im2.item
                      end item,
                     rolled_up_details.status,
                     rolled_up_details.location_description,
                     rolled_up_details.sum_inventory_quantity
                from -- There can be multiple records for a cycle count/item/loc/location description
                     -- Do a rollup of the inventory quantities to only have 1 record
                     (select sfd.process_id,
                             sfd.item_type,
                             sfd.item_value stockcount_item,
                             sfd.status,
                             trim(sfd.location_description) location_description,
                             SUM(sfd.inventory_quantity) sum_inventory_quantity
                        from svc_stkupld_fdetl sfd
                       where sfd.process_id = I_process_id
                         and sfd.chunk_id   = I_chunk_id
                         and sfd.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD  --validated item record
                       group by sfd.process_id,
                                sfd.item_type,
                                sfd.item_value,
                                sfd.status,
                                trim(sfd.location_description)) rolled_up_details,
                     item_master im2
               -- Outer join is done so that stkupld_item_loc will always have a row from stkupld_fdetl
               -- Rows without item_master details will be validated in a later process
               where rolled_up_details.stockcount_item = im2.item(+)) detail_set,
             -- header_set will do a Cartesian join
             (select sh.cycle_count,
                     sh.stocktake_type,
                     NVL(sh.product_level_ind, 'I') product_level_ind,
                     sh.loc_type location_type,
                     sf.location location
                from svc_stkupld_fhead sf,
                     stake_head sh
               where sf.process_id       = I_process_id
                 and sf.status           = CORESVC_STOCK_UPLOAD_SQL.PROCESSED --validated header record
                 and sf.cycle_count      = sh.cycle_count
                 and TO_DATE(TO_CHAR(sf.stock_take_date,'YYYYMMDD'),'YYYYMMDD')  = sh.stocktake_date) header_set
         where im.item(+)           = detail_set.item
           and header_set.location  = wh.physical_wh
           and wh.wh               != wh.physical_wh
           and im.standard_uom = uc.uom(+);
      
      -- Populate fields on the work table related to the inventory fields from stake_sku_loc
      -- Records for a warehouse location are exploded into the virtual warehouses in the previous insert
      -- The records on stake_sku_loc are at the virtual warehouse level
      merge into work_stkupld_item_loc_gtt target
       using (select sil.location,
                     sil.stockcount_item,
                     sil.item,
                     NVL(sil.pack_item, '-999') pack_item,
                     ---
                     NVL(ssl.snapshot_on_hand_qty, 0) snapshot_on_hand_qty,
                     NVL(ssl.physical_count_qty, 0) physical_count_qty,
                     NVL(ssl.pack_comp_qty, 0) pack_comp_qty,
                     ssl.processed
                from work_stkupld_item_loc_gtt sil,
                     stake_sku_loc ssl
               where sil.cycle_count     = ssl.cycle_count
                 -- for REF items in the stockcount, this uses the parent item to join
                 and sil.item            = ssl.item
                 and sil.location_type   = ssl.loc_type
                 and sil.location        = ssl.location) source
       on (    target.location               = source.location
           and target.stockcount_item        = source.stockcount_item
           and target.item                   = source.item
           and NVL(target.pack_item, '-999') = source.pack_item)
       when matched then
       update set target.ssl_snapshot_on_hand_quantity = source.snapshot_on_hand_qty,
                  target.ssl_physical_count_quantity   = source.physical_count_qty,
                  target.ssl_pack_comp_quantity        = source.pack_comp_qty,
                  target.ssl_processed_ind             = source.processed;

      -- Populate field related to the current stock on hand on item_loc_soh
      merge into work_stkupld_item_loc_gtt target
       using (select sil.location,
                     sil.stockcount_item,
                     sil.item,
                     NVL(sil.pack_item, '-999') pack_item,
                     ---
                     NVL(ils.stock_on_hand, 0) stock_on_hand
                from work_stkupld_item_loc_gtt sil,
                     item_loc_soh ils
               where sil.item            = ils.item
                 and sil.location_type   = ils.loc_type
                 and sil.location        = ils.loc) source
       on (    target.location               = source.location
           and target.stockcount_item        = source.stockcount_item
           and target.item                   = source.item
           and NVL(target.pack_item, '-999') = source.pack_item)
       when matched then
       update set target.ils_stock_on_hand = source.stock_on_hand;

   else

   ----------------------------------------------------------------------------------------
   -- STORE PROCESSING
   ----------------------------------------------------------------------------------------

      -- Populate stkupld_item_loc for store and external finisher locations
         -- This table holds item-related information for items in the stock count
         -- It is assummed that there can be multiple records for the same cycle count/location/item/location description
         -- Get the item parent information of any item in the stock count that is REF item and item_level > tran_level
         -- Potential to get the same item on stkupld_item if it has multiple child REF items on the stock count
      insert into work_stkupld_item_loc_gtt(process_id,
                                        chunk_id,
                                        cycle_count,
                                        stocktake_type,
                                        product_level_ind,
                                        location_type,
                                        physical_location,  --physical wh for wh, stores for stores
                                        location,
                                        ---
                                        item,            --item parent for REF item
                                        pack_item,       --pack item of exploded components
                                        stockcount_item, --item in the stock count parameter table or REF item
                                        item_type,
                                        stockcount_quantity,
                                        dept,
                                        class,
                                        subclass,
                                        item_level,
                                        tran_level,
                                        item_status,
                                        pack_ind,
                                        simple_pack_ind,
                                        item_xform_ind,
                                        sellable_ind,
                                        orderable_ind,
                                        inventory_ind,
                                        location_description,
                                        xform_item_type,
                                        ---
                                        ssl_snapshot_on_hand_quantity,
                                        ssl_physical_count_quantity,
                                        ssl_pack_comp_quantity,
                                        ssl_processed_ind,
                                        ---
                                        stake_quantity,
                                        ---
                                        new_itemloc_ind,
                                        status,
                                        error_msg)
      select I_process_id,
             I_chunk_id,
             header_set.cycle_count,
             header_set.stocktake_type,
             header_set.product_level_ind,
             header_set.location_type,
             header_set.location,
             header_set.location,
             ---
             im.item,
             NULL,  --pack_item
             detail_set.stockcount_item,
             detail_set.item_type,
             detail_set.sum_inventory_quantity,
             im.dept,
             im.class,
             im.subclass,
             im.item_level,
             im.tran_level,
             im.status,
             im.pack_ind,
             im.simple_pack_ind,
             im.item_xform_ind,
             im.sellable_ind,
             im.orderable_ind,
             im.inventory_ind,
             NULL, -- location description
             -- Determine xform_item_type based on item_master information
             case when im.inventory_ind  = 'N' and
                       im.item_xform_ind = 'Y' and
                       im.sellable_ind   = 'Y' then
                     'S' -- transformed sellable
                  when im.inventory_ind  = 'Y' and
                       im.item_xform_ind = 'Y' and
                       im.orderable_ind  = 'Y' then
                     'O' -- transformed orderable
                  else
                     NULL
             end,
             ---
             NULL, --ssl_snapshot_on_hand_quantity
             NULL, --ssl_physical_count_qty
             NULL, --ssl_pack_comp_quantity
             NULL, --ssl_processed_ind
             ---
             NULL, --stake_quantity
             ---
             NULL, --new_itemloc_ind
             detail_set.status,
             NULL  --error_msg
        from item_master im,
             ---
             (select rolled_up_details.process_id,
                     rolled_up_details.item_type,
                     rolled_up_details.stockcount_item,
                     case
                     when rolled_up_details.item_type = 'REF' AND im2.item_level > im2.tran_level THEN
                        im2.item_parent
                     else
                        im2.item
                     end item,
                     rolled_up_details.status,
                     rolled_up_details.sum_inventory_quantity
                from -- There can be multiple records for a cycle count/item/loc/location description
                     -- Do a rollup of the inventory quantities to only have 1 record
                     (select sfd.process_id,
                             sfd.item_type,
                             sfd.item_value stockcount_item,
                             sfd.status,
                             SUM(sfd.inventory_quantity) sum_inventory_quantity
                        from svc_stkupld_fdetl sfd
                       where sfd.process_id = I_process_id
                         and sfd.chunk_id   = I_chunk_id
                         and sfd.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD  --validated item record
                       group by sfd.process_id,
                                sfd.item_type,
                                sfd.item_value,
                                sfd.status) rolled_up_details,
                     item_master im2
               -- Outer join is done so that stkupld_item_loc will always have a row from stkupld_fdetl
               -- Rows without item_master details will be validated in a later process
               where rolled_up_details.stockcount_item = im2.item(+)) detail_set,
             -- header_set will do a Cartesian join
             (select sh.cycle_count,
                     sh.stocktake_type,
                     NVL(sh.product_level_ind, 'I') product_level_ind,
                     sh.loc_type location_type,
                     sf.location location
                from svc_stkupld_fhead sf,
                     stake_head sh
               where sf.process_id       = I_process_id
                 and sf.status           = CORESVC_STOCK_UPLOAD_SQL.PROCESSED --validated header record
                 and sf.cycle_count      = sh.cycle_count
                 and TO_DATE(TO_CHAR(sf.stock_take_date,'YYYYMMDD'),'YYYYMMDD')  = sh.stocktake_date) header_set
         where im.item(+) = detail_set.item;

      -- Populate fields on the work table related to stake_qty

      insert into work_stkupld_stake_qty_gtt(process_id,
                                        chunk_id,
                                        cycle_count,
                                        location_type,
                                        physical_location,  --physical wh for wh, stores for stores
                                        location,
                                        ---
                                        item,            --item parent for REF item
                                        pack_item,       --pack item of exploded components
                                        stockcount_item, --item in the stock count parameter table or REF item
                                        stockcount_quantity,
                                        location_description,
                                        stake_quantity,
                                        pack_ind,
                                        item_xform_ind,
                                        uom_class,
                                        status,
                                        error_msg)
      select I_process_id,
             I_chunk_id,
             header_set.cycle_count,
             header_set.location_type,
             header_set.location,
             header_set.location,
             ---
             im.item,
             NULL,  --pack_item
             detail_set.stockcount_item,
             detail_set.sum_inventory_quantity,
             detail_set.location_description,
             detail_set.sum_inventory_quantity, --stake_quantity
             im.pack_ind,
             im.item_xform_ind,
             NULL, --uom_class
             detail_set.status,
             NULL  --error_msg
        from item_master im,
             ---
			 
             (select rolled_up_details.process_id,
                     rolled_up_details.item_type,
                     rolled_up_details.stockcount_item,
                     case
                     when rolled_up_details.item_type = 'REF' AND im2.item_level > im2.tran_level THEN
                        im2.item_parent
                     else
                        im2.item
                     end item,
                     rolled_up_details.location_description,
                     rolled_up_details.status,
                     rolled_up_details.sum_inventory_quantity
                from -- There can be multiple records for a cycle count/item/loc/location description
                     -- Do a rollup of the inventory quantities to only have 1 record
                     (select sfd.process_id,
                             sfd.item_type,
                             sfd.item_value stockcount_item,
                             trim(sfd.location_description) location_description,
                             sfd.status,
                             SUM(sfd.inventory_quantity) sum_inventory_quantity
                        from svc_stkupld_fdetl sfd
                       where sfd.process_id = I_process_id
                         and sfd.chunk_id   = I_chunk_id
                         and sfd.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD  --validated item record
                       group by sfd.process_id,
                                sfd.item_type,
                                sfd.item_value,
                                trim(sfd.location_description),
                                sfd.status) rolled_up_details,
                     item_master im2
               -- Outer join is done so that stkupld_item_loc will always have a row from stkupld_fdetl
               -- Rows without item_master details will be validated in a later process
               where rolled_up_details.stockcount_item = im2.item(+)) detail_set,
             -- header_set will do a Cartesian join
             (select sh.cycle_count,
                     sh.stocktake_type,
                     NVL(sh.product_level_ind, 'I') product_level_ind,
                     sh.loc_type location_type,
                     sf.location location
                from svc_stkupld_fhead sf,
                     stake_head sh
               where sf.process_id       = I_process_id
                 and sf.status           = CORESVC_STOCK_UPLOAD_SQL.PROCESSED --validated header record
                 and sf.cycle_count      = sh.cycle_count
                 and TO_DATE(TO_CHAR(sf.stock_take_date,'YYYYMMDD'),'YYYYMMDD')  = sh.stocktake_date) header_set
         where im.item(+) = detail_set.item;

      -- Populate fields on the work table related to the inventory fields from stake_sku_loc
      -- The records on stake_sku_loc are at the virtual warehouse level
      -- Rows not merged do not have stake_sku_loc records
      merge into work_stkupld_item_loc_gtt target
       using (select sil.location,
                     sil.stockcount_item,
                     sil.item,
                     NVL(sil.pack_item, '-999') pack_item,
                     ---
                     NVL(ssl.snapshot_on_hand_qty, 0) snapshot_on_hand_qty,
                     NVL(ssl.physical_count_qty, 0) physical_count_qty,
                     NVL(ssl.pack_comp_qty, 0) pack_comp_qty,
                     ssl.processed
                from work_stkupld_item_loc_gtt sil,
                     stake_sku_loc ssl
               where sil.cycle_count     = ssl.cycle_count
                 -- for REF items in the stockcount, this uses the parent item to join
                 and sil.item            = ssl.item
                 and sil.location_type   = ssl.loc_type
                 and sil.location        = ssl.location) source
       on (    target.location               = source.location
           and target.stockcount_item        = source.stockcount_item
           and target.item                   = source.item
           and NVL(target.pack_item, '-999') = source.pack_item)
       when matched then
       update set target.ssl_snapshot_on_hand_quantity = source.snapshot_on_hand_qty,
                  target.ssl_physical_count_quantity   = source.physical_count_qty,
                  target.ssl_pack_comp_quantity        = source.pack_comp_qty,
                  target.ssl_processed_ind             = source.processed;

   end if;

   -- Explode and insert the pack item components in work_stkupld_item_loc_gtt
   -- For pack items:
   --    - Retain the same stockcount quantity for warehouse and external finisher locations
   -- For pack item components:
   --    - Stockcount quantity is stockcount quantity * pack quantity
   --    - Inherits the stock count information of the pack item except quantities and item_master information
   insert into work_stkupld_item_loc_gtt(process_id,
                                     chunk_id,
                                     cycle_count,
                                     stocktake_type,
                                     product_level_ind,
                                     location_type,
                                     physical_location,
                                     location,
                                     ---
                                     item,
                                     pack_item,
                                     stockcount_item,
                                     item_type,
                                     stockcount_quantity,
                                     dept,                  --pack item component item_master information
                                     class,                 --pack item component item_master information
                                     subclass,              --pack item component item_master information
                                     item_level,            --pack item component item_master information
                                     tran_level,            --pack item component item_master information
                                     item_status,           --pack item component item_master information
                                     pack_ind,              --pack item component item_master information
                                     simple_pack_ind,       --pack item component item_master information
                                     item_xform_ind,        --pack item component item_master information
                                     sellable_ind,          --pack item component item_master information
                                     orderable_ind,         --pack item component item_master information
                                     inventory_ind,         --pack item component item_master information
                                     location_description,
                                     xform_item_type,
                                     ---
                                     ssl_snapshot_on_hand_quantity,  --pack item stake_sku_loc snapshot quantity
                                     ssl_physical_count_quantity,    --pack item physical count quantity
                                     ssl_pack_comp_quantity,         --pack item pack comp quantity
                                     ssl_processed_ind,
                                     ---
                                     stake_quantity,
                                     ---
                                     new_itemloc_ind,
                                     status,
                                     error_msg)
   select sil.process_id,
          sil.chunk_id,
          sil.cycle_count,
          sil.stocktake_type,
          sil.product_level_ind,
          sil.location_type,
          sil.physical_location,
          sil.location,
          ---
          im.item,   --exploded component item
          sil.item,  --pack item
          sil.stockcount_item,
          sil.item_type,
          NVL(sil.stockcount_quantity, 0) * NVL(vpq.qty, 0),  --stockcount_quantity
          im.dept,
          im.class,
          im.subclass,
          im.item_level,
          im.tran_level,
          im.status,
          im.pack_ind,
          im.simple_pack_ind,
          im.item_xform_ind,
          im.sellable_ind,
          im.orderable_ind,
          im.inventory_ind,
          NULL, -- location description
          -- Determine xform_item_type based on item_master information
          case when im.inventory_ind  = 'N' and
                    im.item_xform_ind = 'Y' and
                    im.sellable_ind   = 'Y' then
                  'S' -- transformed sellable
               when im.inventory_ind  = 'Y' and
                    im.item_xform_ind = 'Y' and
                    im.orderable_ind  = 'Y' then
                  'O' -- transformed orderable
               else
                  NULL
          end,  --xform_item_type
          ---
          --The component items inherit stock count data from the pack item
          sil.ssl_snapshot_on_hand_quantity,
          sil.ssl_physical_count_quantity,
          sil.ssl_pack_comp_quantity,
          sil.ssl_processed_ind,
          ---
          sil.stake_quantity,
          ---
          NULL, --new_itemloc_ind
          sil.status,
          NULL  --error_msg
     from work_stkupld_item_loc_gtt sil,
          item_master im,
          v_packsku_qty vpq
    where sil.pack_ind        = 'Y'  -- pack items
      and sil.item            = vpq.pack_no
      and vpq.item            = im.item
      and NOT (im.inventory_ind  = 'N' and
               im.item_xform_ind = 'N');

      -- insert work_stkupld_stake_qty_gtt for component items.
      insert into work_stkupld_stake_qty_gtt(process_id,
                                             chunk_id,
                                             cycle_count,
                                             location_type,
                                             physical_location,  --physical wh for wh, stores for stores
                                             location,
                                             ---
                                             item,            --item parent for REF item
                                             pack_item,       --pack item of exploded components
                                             stockcount_item, --item in the stock count parameter table or REF item
                                             stockcount_quantity,
                                             location_description,
                                             stake_quantity,
                                             pack_ind,
                                             item_xform_ind,
                                             uom_class,
                                             status,
                                             error_msg)
      select I_process_id,
             I_chunk_id,
             sil.cycle_count,
             sil.location_type,
             sil.physical_location,
             sil.location,
             ---
             im.item, -- exploded component item
             sil.item,  --pack_item
             sil.stockcount_item,
             NVL(sil.stockcount_quantity, 0) * NVL(vpq.qty, 0),  --stockcount_quantity             
             sil.location_description,
             NVL(sil.stockcount_quantity, 0) * NVL(vpq.qty, 0),  --stockcount_quantity
             im.pack_ind,
             im.item_xform_ind,
             uc.uom_class, --uom_class
             sil.status,
             NULL  --error_msg
        from work_stkupld_stake_qty_gtt sil,
             item_master im,
             uom_class uc,
             v_packsku_qty vpq
       where sil.pack_ind        = 'Y'  -- pack items
         and sil.item            = vpq.pack_no
         and vpq.item            = im.item
         and im.standard_uom = uc.uom
         and NOT (im.inventory_ind  = 'N' and
                  im.item_xform_ind = 'N');

               
   -- insert xform orderable items.
   insert into work_stkupld_item_loc_gtt(process_id,
                                         chunk_id,
                                         cycle_count,
                                         stocktake_type,
                                         product_level_ind,
                                         location_type,
                                         physical_location,  --physical wh for wh, stores for stores
                                         location,
                                         ---
                                         item,            --item parent for REF item
                                         pack_item,       --pack item of exploded components
                                         stockcount_item, --item in the stock count parameter table or REF item
                                         item_type,
                                         stockcount_quantity,
                                         dept,
                                         class,
                                         subclass,
                                         item_level,
                                         tran_level,
                                         item_status,
                                         pack_ind,
                                         simple_pack_ind,
                                         item_xform_ind,
                                         sellable_ind,
                                         orderable_ind,
                                         inventory_ind,
                                         location_description,
                                         xform_item_type,
                                         ---
                                         ssl_snapshot_on_hand_quantity,
                                         ssl_physical_count_quantity,
                                         ssl_pack_comp_quantity,
                                         ssl_processed_ind,
                                         ---
                                         stake_quantity,
                                         ---
                                         new_itemloc_ind,
                                         status,
                                         error_msg)
                                  select I_process_id,
                                         I_chunk_id,
                                         si.cycle_count,
                                         si.stocktake_type,
                                         si.product_level_ind,
                                         si.location_type,
                                         si.physical_location,
                                         si.location,
                                         ixh.head_item, -- orderable xform item
                                         si.pack_item,
                                         si.stockcount_item,
                                         si.item_type,
                                         si.stockcount_quantity * (NVL(ixd.yield_from_head_item_pct, 100)/100) stockcount_quantity,
                                         si.dept,
                                         si.class,
                                         si.subclass,
                                         si.item_level,
                                         si.tran_level,
                                         si.item_status,
                                         si.pack_ind,
                                         si.simple_pack_ind,
                                         si.item_xform_ind,
                                         si.sellable_ind,
                                         si.orderable_ind,
                                         si.inventory_ind,
                                         si.location_description,
                                         'O', -- xform orderable
                                         si.ssl_snapshot_on_hand_quantity,
                                         si.ssl_physical_count_quantity,
                                         si.ssl_pack_comp_quantity,
                                         si.ssl_processed_ind,
                                         si.stake_quantity,
                                         si.new_itemloc_ind,
                                         si.status,
                                         si.error_msg
                                    from work_stkupld_item_loc_gtt si,
                                         item_xform_detail ixd,
                                         item_xform_head ixh,
                                         item_master im,
                                         stake_product sp
                                   where si.item_xform_ind      = 'Y'
                                     and si.item                = ixd.detail_item
                                     and ixd.item_xform_head_id = ixh.item_xform_head_id
                                     and ixh.item_xform_type    = 'K' -- Break to sell
                                     and ixh.head_item          = im.item
                                     and si.cycle_count         = sp.cycle_count(+)
                                     and im.dept                = NVL(sp.dept, im.dept)
                                     and im.class               = NVL(sp.class, im.class)
                                     and im.subclass            = NVL(sp.subclass, im.subclass)
                                     -- Added check againts stake_sku_loc for stk rqst scenarios involving stocktake_type = 'U' and item_list.
                                     -- Since item list is not stored in any of the stake tables, the only logical choice is to validate againts stake_sku_loc to
                                     -- avoid picking up xform orderable items that are not part of the item list.
                                     and EXISTS (select 'x'
                                                   from stake_sku_loc ssl
                                                  where ssl.cycle_count = si.cycle_count
                                                    and ssl.loc_type    = si.location_type
                                                    and ssl.location    = si.location
                                                    and ssl.item        = ixh.head_item
                                                    and rownum          = 1);


      insert into work_stkupld_stake_qty_gtt(process_id,
                                        chunk_id,
                                        cycle_count,
                                        location_type,
                                        physical_location,  --physical wh for wh, stores for stores
                                        location,
                                        ---
                                        item,            --item parent for REF item
                                        pack_item,       --pack item of exploded components
                                        stockcount_item, --item in the stock count parameter table or REF item
                                        stockcount_quantity,
                                        location_description,
                                        stake_quantity,
                                        pack_ind,
                                        uom_class,
                                        status,
                                        error_msg)
                                  select I_process_id,
                                         I_chunk_id,
                                         si.cycle_count,
                                         si.location_type,
                                         si.physical_location,
                                         si.location,
                                         ixh.head_item, -- orderable xform item
                                         si.pack_item,
                                         si.stockcount_item,
                                         si.stockcount_quantity * (NVL(ixd.yield_from_head_item_pct, 100)/100) stockcount_quantity,
                                         si.location_description,
                                         si.stake_quantity * (NVL(ixd.yield_from_head_item_pct, 100)/100) stake_quantity,
                                         si.pack_ind,
                                         si.uom_class,
                                         si.status,
                                         si.error_msg
                                    from work_stkupld_stake_qty_gtt si,
                                         item_xform_detail ixd,
                                         item_xform_head ixh,
                                         item_master im,
                                         stake_product sp
                                   where si.item_xform_ind      = 'Y'
                                     and si.item                = ixd.detail_item
                                     and ixd.item_xform_head_id = ixh.item_xform_head_id
                                     and ixh.item_xform_type    = 'K' -- Break to sell
                                     and ixh.head_item          = im.item
                                     and si.cycle_count         = sp.cycle_count(+)
                                     and im.dept                = NVL(sp.dept, im.dept)
                                     and im.class               = NVL(sp.class, im.class)
                                     and im.subclass            = NVL(sp.subclass, im.subclass)
                                     -- Added check againts stake_sku_loc for stk rqst scenarios involving stocktake_type = 'U' and item_list.
                                     -- Since item list is not stored in any of the stake tables, the only logical choice is to validate againts stake_sku_loc to
                                     -- avoid picking up xform orderable items that are not part of the item list.
                                     and EXISTS (select 'x'
                                                   from stake_sku_loc ssl
                                                  where ssl.cycle_count = si.cycle_count
                                                    and ssl.loc_type    = si.location_type
                                                    and ssl.location    = si.location
                                                    and ssl.item        = ixh.head_item
                                                    and rownum          = 1);


   -- Proceed to populate xform item data if there are transformed items in this data set
   if NVL(SQL%ROWCOUNT, 0) > 0 then

      -- Populate work_stkupld_xform_ord_gtt
      -- This table contains orderables items for sellable-only items in the stock count that is not in stake_sku_loc
      insert into work_stkupld_xform_ord_gtt(
                     cycle_count,
                     ---
                     xform_sellable_item,
                     xform_orderable_item)
      select distinct s.cycle_count,
             ---
             s.stockcount_item, -- sellable item
             s.item             -- orderable item
        from work_stkupld_item_loc_gtt s,
             item_master im,
             stake_location sl,
             item_loc_soh ils
       where im.item_level                = im.tran_level
         and s.cycle_count                = sl.cycle_count
         and s.location_type              = sl.loc_type
         and s.location                   = sl.location
         and s.item_xform_ind             = 'Y'
         and s.xform_item_type            = 'O'
         and im.item                      = s.item -- xform orderable item
         and ils.item                     = s.item
         and ils.loc                      = sl.location
         and NOT EXISTS (select 'x'
                           from stake_sku_loc ssl
                          where ssl.cycle_count = s.cycle_count
                            and ssl.item        = s.item -- xform orderable_item
                            and ssl.loc_type    = s.location_type
                            and ssl.location    = s.location);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_WORK_TABLES;
---------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_STOCK_COUNT(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                             I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.PROCESS_STOCK_COUNT';

BEGIN

   -- Reset reject records indicator
   LP_reject_recs_ind := 'N';

   -- Validate required inputs
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chunk_id',
                                            L_program,
                                            NULL);
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Retrieve the header row from the parameter table
   -- There should only be one row retrieved as this was validated in the VALIDATE_PARAMETER_TABLES function
   select process_id,
          file_record_descriptor,
          file_line_id,
          file_type,
          file_create_date,
          TO_DATE(TO_CHAR(stock_take_date,'YYYYMMDD'),'YYYYMMDD') stock_take_date,
          cycle_count,
          location_type,
          location,
          status,
          error_msg
     into GP_stkupld_fhead_row
     from svc_stkupld_fhead
    where process_id = I_process_id
      and status = CORESVC_STOCK_UPLOAD_SQL.PROCESSED;

   -- Check if header row was found
   if GP_stkupld_fhead_row.process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_MISSING_FHEAD',
                                            I_process_id,
                                            NULL,
                                            NULL);
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Populate system options
   select std_av_ind
     into LP_std_av_ind
     from system_options;

   -- Check if system_option was retrieved
   if LP_std_av_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('FAILED_SYSTEM_OPTIONS',
                                            NULL,
                                            NULL,
                                            NULL);
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Populate retry parameters
   select retry_lock_attempts,
          retry_wait_time
     into LP_retry_lock_attempts,
          LP_retry_wait_time
     from rms_plsql_batch_config
    where program_name = 'CORESVC_STOCK_UPLOAD_SQL'
      and rownum = 1;

   -- Populate the work tables
   if POPULATE_WORK_TABLES(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Validate the detail records for non-fatal errors
   if VALIDATE_NON_FATAL(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Distribute physical warehouse quantities over virtual warehouse
   if GP_stkupld_fhead_row.location_type = 'W' then
      if DISTRIBUTE_PHYSICAL_WH_COUNTS(O_error_message,
                                       I_process_id,
                                       I_chunk_id) = FALSE then
         --- Update the status of the process id/chunk to error
         WRITE_ERROR(I_process_id,
                     I_chunk_id,
                     O_error_message);
         return FALSE;
      end if;
   end if;

   -- A unit and cost stock will generate new item/loc relationships
   -- Create new item/locs for records not in stake_sku_loc and item_loc
   if GP_stkupld_fhead_row.location_type = 'S' then
      if RANGE_NEW_ITEM_LOC(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
         --- Update the status of the process id/chunk to error
         WRITE_ERROR(I_process_id,
                     I_chunk_id,
                     O_error_message);
         return FALSE;
      end if;
   end if;

   -- Create stake_location records for new item/locs
   if CREATE_STAKE_LOCATION(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Create stake_sku_loc records for new item/locs for a unit and amount stock count
   if CREATE_STAKE_SKU_LOC(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Create stake_sku_loc records for mising orderables
   if INSERT_MISSING_ORDERABLES(O_error_message,
                                I_process_id,
                                I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Lock stake_sku_loc records for update
   if LOCK_STAKE_SKU_LOC(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Update stake_sku_loc records
   if UPDATE_STAKE_SKU_LOC(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Lock stake_qty records for update
   if LOCK_STAKE_QTY(O_error_message,
                     I_process_id,
                     I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Update stake_qty records
   if UPDATE_STAKE_QTY(O_error_message,
                       I_process_id,
                       I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Merge reject records back into the parameter tables
   -- Reject records are for reporting purposes
   if MERGE_BACK_TO_PARAMS(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Update the chunk status to processed
   -- If processing reaches this point then the chunk was successfully processed
   -- or had non-fatal reject records
   if LP_reject_recs_ind = 'Y' then
      WRITE_REJECT(I_process_id,
                   I_chunk_id,
                   O_error_message);
   else
      WRITE_SUCCESS(I_process_id,
                    I_chunk_id);
   end if;

   LP_reject_recs_ind := 'N';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_STOCK_UPLOAD_SQL.PROCESS_STOCK_COUNT',
                                            to_char(SQLCODE));
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  I_chunk_id,
                  O_error_message);
      return FALSE;
END PROCESS_STOCK_COUNT;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHUNK_STOCK_COUNT(O_error_message   IN OUT   NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.CHUNK_STOCK_COUNT';
   L_detail_count       NUMBER(10) := NULL;
   L_detail_chunk_count NUMBER(10) := NULL;
   L_max_chunk_size     RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE := NULL;
   L_max_chunk          NUMBER(10) := NULL;

   cursor C_GET_MAX_CHUNK_SIZE is
      select max_chunk_size
        from rms_plsql_batch_config
       where program_name = 'CORESVC_STOCK_UPLOAD_SQL';

BEGIN

   -------------------------------------------------------------------------------------
   -- VALIDATE REQUIRED INPUT
   -------------------------------------------------------------------------------------
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -------------------------------------------------------------------------------------
   -- Get the maximum size of a stkupld transaction
   -------------------------------------------------------------------------------------
   open C_GET_MAX_CHUNK_SIZE;
   fetch C_GET_MAX_CHUNK_SIZE into L_max_chunk_size;
   close C_GET_MAX_CHUNK_SIZE;
   --
   -- Check if commit_max_ctr was retrieved
   if L_max_chunk_size is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_NO_MAX_TRAN_SIZE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Get the number of stkupld detail rows
   select count(*) into L_detail_count
     from svc_stkupld_fdetl
    where process_id = I_process_id
      and status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

   -- Check if no records were found
   if L_detail_count is NULL or L_detail_count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_MISSING_FDETL',
                                            I_process_id,
                                            NULL,
                                            NULL);
      return FALSE;
    end if;

   -- Chunk the records on svc_stkupld_fdetl
   merge into svc_stkupld_fdetl target
   using (select distinct
                 process_id,
                 item_value,
                 CEIL(inner_set.rank/L_max_chunk_size) chunk_id
            from (select sfd.process_id,
                         sfd.item_value,
                         DENSE_RANK()
                            OVER(ORDER by (case
                                           -- group REF items of a transaction-level item together
                                           when sfd.item_type = 'REF' and
                                               im.item_level  > im.tran_level then
                                              im.item_parent
                                           else
                                              im.item
                                           end)) rank
                    from svc_stkupld_fdetl sfd,
                         item_master im
                   where sfd.process_id = I_process_id
                     and sfd.status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
                     and sfd.item_value = im.item) inner_set
         ) source
      on (    target.process_id = source.process_id
          and target.item_value = source.item_value)
   when matched then
      update set target.chunk_id = source.chunk_id;

   -- Retrieve highest chunk value plus 1 from the detail table
   select max(chunk_id) + 1 into L_max_chunk
     from svc_stkupld_fdetl sd
    where sd.process_id = I_process_id
      and sd.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

      
   -- Chunk packitems where the component item exists on the file
   merge into svc_stkupld_fdetl target
   using (select distinct s.process_id,
                 s.item_value,
                 L_max_chunk  chunk_id
            from svc_stkupld_fdetl s,
                 item_master im
          where s.process_id = I_process_id
            and s.item_value = im.item
            and s.status   = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
            and im.pack_ind  = 'Y'
            and exists (select 'x'
                          from v_packsku_qty v,
                               svc_stkupld_fdetl s1
                         where im.item = v.pack_no
                           and s1.process_id = s.process_id
                           and v.item = s1.item_value
                           and rownum = 1)
          ) source
      on (    target.process_id = source.process_id
          and target.item_value = source.item_value)
   when matched then
      update set target.chunk_id = source.chunk_id;

   -- Chunk component items affected by the query above
   merge into svc_stkupld_fdetl target
   using (select distinct s.process_id,
                 s.item_value,
                 L_max_chunk chunk_id
            from svc_stkupld_fdetl s
           where s.process_id = I_process_id
             and s.status   = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
             and exists (select 'x'
                          from v_packsku_qty v,
                               svc_stkupld_fdetl s1
                         where s1.process_id = s.process_id
                           and s1.chunk_id = L_max_chunk
                           and v.pack_no = s1.item_value
                           and v.item = s.item_value
                           and rownum = 1)
          ) source
      on (    target.process_id = source.process_id
          and target.item_value = source.item_value)
   when matched then
      update set target.chunk_id = source.chunk_id;
      
                                  
   -- Get the number of stkupld detail rows
   select count(*) into L_detail_chunk_count
     from svc_stkupld_fdetl
    where process_id = I_process_id
      and status     = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
      and chunk_id is not null;

   -- Check for records that did not get chunked
   -- Return fatal if records are found
   if L_detail_count <> NVL(L_detail_chunk_count, 0) then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_CHUNK_ERROR',
                                            I_process_id,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHUNK_STOCK_COUNT;
---------------------------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_PROCESS_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                   I_reference_id    IN     SVC_STKUPLD_STATUS.REFERENCE_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.INITIALIZE_PROCESS_STATUS';

BEGIN

   -- Validate required input
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Insert initial status record with chunk 0
   insert into svc_stkupld_status(process_id,
                                  chunk_id,
                                  reference_id,
                                  status,
                                  last_update_datetime)
                           values(I_process_id,
                                  0, --chunk_id
                                  I_reference_id,
                                  CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD, --new status
                                  sysdate);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INITIALIZE_PROCESS_STATUS;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_RESTART_PROCESS_ID(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                O_process_id      IN OUT SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                I_reference_id    IN     SVC_STKUPLD_STATUS.REFERENCE_ID%TYPE)
RETURN BOOLEAN IS

   L_restart_id SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE := NULL;
   L_program    VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.GET_RESTART_PROCESS_ID';

BEGIN

   -- Validate required input
   if I_reference_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_reference_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   select process_id into L_restart_id
     from svc_stkupld_status
    where reference_id = I_reference_id
      and status = 'N'
      and rownum = 1;

   O_process_id := L_restart_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_RESTART_PROCESS_ID;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                      I_chunk_id        IN SVC_STKUPLD_FDETL.CHUNK_ID%TYPE,
                      I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE) AS
   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program     VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.WRITE_ERROR';
   L_max_chunk   SVC_STKUPLD_FDETL.CHUNK_ID%TYPE := NULL;

   cursor C_LOCK_SVC_STKUPLD_STATUS is
      select 'x'
        from svc_stkupld_status s
       where process_id = I_process_id
         and chunk_id   = I_chunk_id
         for update of s.status, s.error_msg nowait;

BEGIN

   open C_LOCK_SVC_STKUPLD_STATUS;
   close C_LOCK_SVC_STKUPLD_STATUS;

   update svc_stkupld_status
      set status    = CORESVC_STOCK_UPLOAD_SQL.ERROR,
          error_msg = I_error_message
    where process_id = I_process_id
      and chunk_id   = I_chunk_id;

   commit;

EXCEPTION
   when OTHERS then
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_REJECT(I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                       I_chunk_id        IN SVC_STKUPLD_FDETL.CHUNK_ID%TYPE,
                       I_error_message   IN RTK_ERRORS.RTK_TEXT%TYPE) AS
   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program     VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.WRITE_REJECT';
   L_max_chunk   SVC_STKUPLD_FDETL.CHUNK_ID%TYPE := NULL;

   cursor C_LOCK_SVC_STKUPLD_STATUS is
      select 'x'
        from svc_stkupld_status s
       where process_id = I_process_id
         and chunk_id   = I_chunk_id
         for update of s.status, s.error_msg nowait;

BEGIN

   open C_LOCK_SVC_STKUPLD_STATUS;
   close C_LOCK_SVC_STKUPLD_STATUS;

   update svc_stkupld_status
      set status    = CORESVC_STOCK_UPLOAD_SQL.REJECTED,
          error_msg = I_error_message
    where process_id = I_process_id
      and chunk_id   = I_chunk_id;

   commit;

EXCEPTION
   when OTHERS then
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS(I_process_id      IN SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                        I_chunk_id        IN SVC_STKUPLD_FDETL.CHUNK_ID%TYPE) AS
   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program     VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.WRITE_ERROR';
   L_max_chunk   SVC_STKUPLD_FDETL.CHUNK_ID%TYPE := NULL;

   cursor C_LOCK_SVC_STKUPLD_STATUS is
      select 'x'
        from svc_stkupld_status s
       where process_id = I_process_id
         and chunk_id   = I_chunk_id
         for update of s.status nowait;

BEGIN

   open C_LOCK_SVC_STKUPLD_STATUS;
   close C_LOCK_SVC_STKUPLD_STATUS;

   update svc_stkupld_status
      set status    = CORESVC_STOCK_UPLOAD_SQL.PROCESSED
    where process_id = I_process_id
      and chunk_id   = I_chunk_id;

   commit;

EXCEPTION
   when OTHERS then
      rollback;
END;
---------------------------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_CHUNK_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.INITIALIZE_CHUNK_STATUS';
   L_max_chunk          SVC_STKUPLD_FDETL.CHUNK_ID%TYPE := NULL;

BEGIN

   -- Validate required input
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Retrieve highest chunk value from the detail table
   select max(chunk_id) into L_max_chunk
     from svc_stkupld_fdetl sd
    where sd.process_id = I_process_id
      and sd.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

   -- Generate status records for each chunk
   insert into svc_stkupld_status(process_id,
                                  chunk_id,
                                  reference_id,
                                  status,
                                  last_update_datetime)
                           select s.process_id,
                                  x.chunk_id,
                                  s.reference_id,
                                  s.status,
                                  sysdate
                             from (select process_id,
                                          reference_id,
                                          status
                                     from svc_stkupld_status
                                    where process_id = I_process_id
                                      and chunk_id   = 0) s,  --seed record
                                      --and s.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD) s,
                                  (select level chunk_id
                                     from dual
                                  connect by level < L_max_chunk + 1) x;

   -- Delete the initial status recode
   delete from svc_stkupld_status s
      where s.process_id = I_process_id
        and s.chunk_id = 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INITIALIZE_CHUNK_STATUS;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PARAMETER_TABLES(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   TBL_error_rowid_char   ROWID_CHAR_TBL := ROWID_CHAR_TBL();

   L_program              VARCHAR2(64) := 'CORESVC_STOCK_UPLOAD_SQL.VALIDATE_PARAMETER_TABLES';
   L_row_detl_count       NUMBER(10) := NULL;
   L_file_record_count    SVC_STKUPLD_FTAIL.FILE_RECORD_COUNT%TYPE := NULL;
   L_stake_head_row       STAKE_HEAD%ROWTYPE := NULL;
   L_store_type           STORE.STORE_TYPE%TYPE := NULL;
   L_stockholding_ind     STORE.STOCKHOLDING_IND%TYPE := NULL;
   L_location             ITEM_LOC.LOC%TYPE := NULL;

   cursor C_GET_FHEAD_REC is
      select sf.process_id,
             sf.file_record_descriptor,
             sf.file_line_id,
             sf.file_type,
             sf.file_create_date,
             TO_DATE(TO_CHAR(sf.stock_take_date,'YYYYMMDD'),'YYYYMMDD') stock_take_date,
             sf.cycle_count,
             sf.location_type,
             sf.location,
             sf.status,
             sf.error_msg
        from svc_stkupld_fhead sf
       where sf.process_id = I_process_id
         and sf.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

   cursor C_GET_STORE_TYPE is
      select s.store_type,
             s.stockholding_ind
        from store s
       where s.store = GP_stkupld_fhead_row.location;

   cursor C_GET_STAKE_HEAD_REC is
      select sh.cycle_count,
             sh.cycle_count_desc,
             sh.loc_type,
             sh.stocktake_date,
             sh.stocktake_type,
             sh.product_level_ind,
             sh.delete_ind
        from stake_head sh
       where sh.cycle_count = GP_stkupld_fhead_row.cycle_count
         and sh.loc_type = GP_stkupld_fhead_row.location_type
         and sh.stocktake_date = TO_DATE(TO_CHAR(GP_stkupld_fhead_row.stock_take_date,'YYYYMMDD'),'YYYYMMDD');

   cursor C_VALIDATE_LOCATION is
      select location_id
        from v_location l
       where l.location_id = GP_stkupld_fhead_row.location
         and GP_stkupld_fhead_row.location_type in ('S','W')
       union
       select finisher_id
         from v_external_finisher e
        where e.finisher_id = GP_stkupld_fhead_row.location
          and GP_stkupld_fhead_row.location_type in ('E');

BEGIN
   
   -------------------------------------------------------------------------------------
   -- VALIDATE REQUIRED INPUT
   -------------------------------------------------------------------------------------
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -------------------------------------------------------------------------------------
   -- STKUPLD_FHEAD VALIDATION
   -------------------------------------------------------------------------------------
   -- Get the fhead row for the stock count
   select ROWIDTOCHAR(sf.rowid)
     BULK COLLECT into TBL_error_rowid_char
     from svc_stkupld_fhead sf
    where sf.process_id = I_process_id
      and sf.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

   -- Check that there is only one stkupld_fhead record
   if (TBL_error_rowid_char is NULL or TBL_error_rowid_char.COUNT < 1) then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_MISSING_FHEAD',
                                            I_process_id,
                                            NULL,
                                            NULL);
      -- Update the status table to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;
   elsif (TBL_error_rowid_char is NOT NULL and TBL_error_rowid_char.COUNT > 1) then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_FHEAD_COUNT',
                                            I_process_id,
                                            NULL,
                                            NULL);
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FHEAD);
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;
   else -- Retrieve the valid header record
      open C_GET_FHEAD_REC;
      fetch C_GET_FHEAD_REC into GP_stkupld_fhead_row;
      close C_GET_FHEAD_REC;
   end if;

   -- Validate if the file type is valid
   if GP_stkupld_fhead_row.file_type != 'STKU' then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_FILE_TYPE',
                                            I_process_id,
                                            NULL,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FHEAD);
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Validate if the location type is valid
   if GP_stkupld_fhead_row.location_type not in ('W', 'S', 'E') then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_LOC_TYPE',
                                            I_process_id,
                                            NULL,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FHEAD);
      --- Update the status of the process id/chunk to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Check if the location is valid
   open C_VALIDATE_LOCATION;
   fetch C_VALIDATE_LOCATION into L_location;
   close C_VALIDATE_LOCATION;
   ---
   if L_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_LOC',
                                            GP_stkupld_fhead_row.location,
                                            I_process_id,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FHEAD);
      -- Update the status table to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;

   end if;

   -- Validate if the store is a company store or a stockholding franchise location
   if GP_stkupld_fhead_row.location_type = 'S' then
      ---
      open C_GET_STORE_TYPE;
      fetch C_GET_STORE_TYPE into L_store_type, L_stockholding_ind;
      close C_GET_STORE_TYPE;
      ---
      if not (L_store_type is NOT NULL and (L_store_type = 'C' or (L_store_type = 'F' and L_stockholding_ind = 'Y'))) then
         O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_STORE_TYPE',
                                               GP_stkupld_fhead_row.location,
                                               I_process_id,
                                               NULL);
         -- Write error to the parameter tables
         WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FHEAD);
         -- Update the status table to error
         WRITE_ERROR(I_process_id,
                     0, --I_chunk_id,
                     O_error_message);
         return FALSE;
      end if;
   end if;

   -- Validate that the stock count is valid in the stake_head table
   open C_GET_STAKE_HEAD_REC;
   fetch C_GET_STAKE_HEAD_REC into L_stake_head_row;
   close C_GET_STAKE_HEAD_REC;
   ---
   if L_stake_head_row.cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_MISSING_STAKE_HEAD',
                                            I_process_id,
                                            GP_stkupld_fhead_row.cycle_count,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FHEAD);
      -- Update the status table to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Write processed to the header parameter table
   WRITE_SUCCESS_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FHEAD);

   TBL_error_rowid_char := NULL;

   -------------------------------------------------------------------------------------
   -- STKUPLD_FDETL VALIDATION
   -------------------------------------------------------------------------------------
   select count(*) into L_row_detl_count
     from svc_stkupld_fdetl sd
    where sd.process_id = I_process_id
      and sd.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

   -- Check that there are stkupld_fdetl records
   if L_row_detl_count < 1 then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_MISSING_FDETL',
                                            I_process_id,
                                            NULL,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, NULL, CORESVC_STOCK_UPLOAD_SQL.FHEAD);
      -- Update the status table to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   -- Find records that are not ITM and REF item types
   select ROWIDTOCHAR(sd.rowid)
     BULK COLLECT into TBL_error_rowid_char
     from svc_stkupld_fdetl sd
    where sd.item_type NOT IN ('ITM', 'REF')
      and sd.process_id = I_process_id
      and sd.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

   if (TBL_error_rowid_char is NOT NULL and TBL_error_rowid_char.COUNT > 0) then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_ITEM_TYPE',
                                            GP_stkupld_fhead_row.cycle_count,
                                            I_process_id,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FDETL);
      -- Update the status table to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   TBL_error_rowid_char := NULL;

   -- Find records with invalid items
   select ROWIDTOCHAR(sd.rowid)
     BULK COLLECT into TBL_error_rowid_char
     from svc_stkupld_fdetl sd,
          item_master im
    where sd.process_id = I_process_id
      and sd.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD
      and sd.item_value = im.item (+)
      and im.item is NULL;

   if (TBL_error_rowid_char is NOT NULL and TBL_error_rowid_char.COUNT > 0) then
      -- Write error message to FDETL table
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_ITEMS',
                                            GP_stkupld_fhead_row.cycle_count,
                                            I_process_id,
                                            NULL);
      -- Write error to the parameter tables
      -- This is a non-fatal error so no return FALSE is invoked and the status record is not updated
      WRITE_REJECT_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FDETL);
      -- Update the status table to error
      WRITE_ERROR(I_process_id,
                  0, --I_chunk_id,
                  O_error_message);
      return FALSE;
   end if;

   TBL_error_rowid_char := NULL;

   --Find records with negative stock count quantity.
   select ROWIDTOCHAR(sd.rowid)
     BULK COLLECT into TBL_error_rowid_char
     from svc_stkupld_fdetl sd
    where sd.process_id = I_process_id
      and sd.inventory_quantity < 0
      and sd.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

   if (TBL_error_rowid_char is NOT NULL and TBL_error_rowid_char.COUNT > 0) then
      -- Write error message to FDETL table
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_QTY_COUNT',
                                            GP_stkupld_fhead_row.cycle_count,
                                            I_process_id,
                                            NULL);
      -- Write error to the parameter tables
      -- This is a non-fatal error so no return FALSE is invoked and the status record is not updated
      WRITE_REJECT_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FDETL);
   end if;

   TBL_error_rowid_char := NULL;
   -------------------------------------------------------------------------------------
   -- STKUPLD_FTAIL VALIDATION
   -------------------------------------------------------------------------------------
   select ROWIDTOCHAR(st.rowid)
     BULK COLLECT into TBL_error_rowid_char
     from svc_stkupld_ftail st
    where st.process_id = I_process_id
      and st.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;

   if (TBL_error_rowid_char is NULL or TBL_error_rowid_char.COUNT < 1) then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_MISSING_FTAIL',
                                            GP_stkupld_fhead_row.cycle_count,
                                            I_process_id,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, NULL, CORESVC_STOCK_UPLOAD_SQL.FTAIL);
      return FALSE;
   elsif (TBL_error_rowid_char is NOT NULL and TBL_error_rowid_char.COUNT > 1) then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_FTAIL_COUNT',
                                            GP_stkupld_fhead_row.cycle_count,
                                            I_process_id,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FTAIL);
      return FALSE;
   else -- Get file_record_count
      select NVL(st.file_record_count,0) into L_file_record_count
        from svc_stkupld_ftail st
       where st.process_id = I_process_id
         and st.status = CORESVC_STOCK_UPLOAD_SQL.NEW_RECORD;
   end if;

   -- Check if FTAIL record count matches number of detail records
   if L_file_record_count != L_row_detl_count then
      O_error_message := SQL_LIB.CREATE_MSG('STKCNT_COUNT_NOT_MATCH',
                                            GP_stkupld_fhead_row.cycle_count,
                                            I_process_id,
                                            NULL);
      -- Write error to the parameter tables
      WRITE_ERROR_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FTAIL);
      return FALSE;
   end if;

   -- Write processed to the tail parameter table
   WRITE_SUCCESS_PARAMS(O_error_message, I_process_id, TBL_error_rowid_char, CORESVC_STOCK_UPLOAD_SQL.FTAIL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_PARAMETER_TABLES;
---------------------------------------------------------------------------------------------------------------------
END CORESVC_STOCK_UPLOAD_SQL;
/
