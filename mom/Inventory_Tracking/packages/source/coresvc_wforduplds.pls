CREATE OR REPLACE PACKAGE CORESVC_WF_ORDER_UPLOAD_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: WF_CREATE_ORDER
-- Purpose: This public function will perform the basic validation on staged 
--          franchisee order requests associated with a given process_id. If there are no 
--          validation errors it will create franchisee order in RMS.
---------------------------------------------------------------------------------------------
FUNCTION WF_CREATE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END CORESVC_WF_ORDER_UPLOAD_SQL;
/