CREATE OR REPLACE PACKAGE WF_ALLOC_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
-- Constant Variables

WF_STATUS_APPROVE    CONSTANT VARCHAR2(1)  := 'A';
WF_STATUS_CANCEL     CONSTANT VARCHAR2(1)  := 'C';
WF_STATUS_INPUT      CONSTANT VARCHAR2(1)  := 'I';
WF_STATUS_REQ_CREDIT CONSTANT VARCHAR2(1)  := 'R';
WF_STATUS_INPROGRESS CONSTANT VARCHAR2(1)  := 'P';

---------------------------------------------------------------------------------------------
-- Function Name:  SYNC_F_ORDER
-- Purpose      :  This is a wrapper function for WF_CREATE_ORDER_SQL.PROCESS_F_ORDER to create or
--                 update franchise order because of allocation. This functions takes the allocation
--                 number as input and if there are franchsie locations - creates or update franchise
--                 order and returns back the created wf_order_no.
--
-- Input           Action_type       - "create" or "update"
--                 Allocation Number - The allocation for which the franchise orders have to be
--                                     created.
--                 Allocation Status - The status should be populated for create allocation or when
--                                     the status is updated to Approved.
--
-- Output          WF_ORDER_NOS      - An allocation can result in creating multiple franchise
--                                     order - one per Franchiase customer. This colleciton contains
--                                     the wf_order_no for the store.
--                 Allocation Status - The return allocation status would be same as input status
--                                     except in case of failed customer credit check when this
--                                     will be "W" - Worksheet.
---------------------------------------------------------------------------------------------
FUNCTION SYNC_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_wf_order_nos    IN OUT   OBJ_ALLOC_LINKED_F_ORD_TBL,
                       O_alloc_status    IN OUT   ALLOC_HEADER.STATUS%TYPE,
                       I_action_type     IN       VARCHAR2,
                       I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                       I_alloc_status    IN       ALLOC_HEADER.STATUS%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  BULK_SYNC_F_ORDER
-- Purpose      :  This functions is similar to SYNC_F_ORDER but has been designed to handle
--                 batch creation of allocation when a very high number of allocation will be
--                 created and the creation of franchise order should be in bulk. The current
--                 implementation will call SYNC_F_ORDER in a loop but this will change when
--                 WF_CREATE_ORDER_SQL exposes a bulk processing funciton.
--
--                 This takes the collection of allocation and intended status as input.
--                 This will return the collection of wf_order_nos created. For allocation
--                 where the linked franchsie order cannot be approved because of credit check,
--                 the allocation and status as "W" is returned back. The action type supported
--                 are 'create' and 'update'.
---------------------------------------------------------------------------------------------
FUNCTION BULK_SYNC_F_ORDER (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_wf_order_nos        IN OUT   OBJ_ALLOC_LINKED_F_ORD_TBL,
                            O_alloc_status_tbl    IN OUT   OBJ_ALLOC_STATUS_TBL,
                            I_alloc_status_tbl    IN       OBJ_ALLOC_STATUS_TBL,
                            I_action_type         IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  CANCEL_F_ORDER
-- Purpose      :  This functions cancels the linked franchise order when a allocation is
--                 cancelled.
---------------------------------------------------------------------------------------------
FUNCTION CANCEL_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  SYNC_DELETED_ALLOC_LINES
-- Purpose      :  This functions handles deletion of linked franchise order and/or deleting of
--                 franchise order detail lines when few locations are deleted from the allocation.
--                 This function deletes the franchise order if all the alloc_detail lines for the
--                 input wf_order_no have been deleted. Post delete, this function will call
--                 SYNC_F_ORDER to delete the franchise order detail lines where the location
--                 delete from the allocation results in line level deletes.
---------------------------------------------------------------------------------------------
FUNCTION SYNC_DELETED_ALLOC_LINES (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                                   I_wf_order_no_tbl IN       OBJ_WF_ORDER_NO_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  DELETE_F_ORDER
-- Purpose      :  This functions deletes the linked franchise order when a allocation is
--                 deleted.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  REPL_SYNC_F_ORDER
-- Purpose      :  This function creates WF orders for Allocations generated from replenishment.
--                 It will be called from the replenishement batch process, and the ordhead form when
--                 xdock orders are approved manually.  I_order_no will be specified if the function
--                 is called from ordhead, and NULL when called from the batch.
---------------------------------------------------------------------------------------------
FUNCTION REPL_SYNC_F_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_date          IN     ORDHEAD.WRITTEN_DATE%TYPE,
                           I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Function Name:  XDOCK_SYNC_F_ORDER
-- Purpose      :  This function creates WF orders for Approved Allocations generated from Cross Dock 
--                   PO Induction and UI.
---------------------------------------------------------------------------------------------
FUNCTION XDOCK_SYNC_F_ORDER  (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_date          IN     ORDHEAD.WRITTEN_DATE%TYPE,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  APPROVE_LINKED_ALLOC_F_ORDER
-- Purpose      :  This function checks if there are linked allocation and the credit check
--                 for other franchise linked to that allocation. If all the franchiase order
--                 can be approved, it will approve all the franchise order, including the one
--                 passed and also approve the allocation.
---------------------------------------------------------------------------------------------
FUNCTION APPROVE_LINKED_ALLOC_F_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_wf_order_no   IN     WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  CHECK_ALLOC_APPROVAL
-- Purpose      :  This function checks if the linked allocation for the franchise order
--                 can be approved if all the linked franchise order meets credit approval.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_ALLOC_APPROVAL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_alloc_approve_ind    IN OUT VARCHAR2, 
                              I_wf_order_no          IN     WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  CANCEL_FRANCHISE_ORDER
-- Purpose      :  This function closes the allocation and cancels all the linked franchise orders.
---------------------------------------------------------------------------------------------
FUNCTION CANCEL_FRANCHISE_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cancel_reason IN     WF_ORDER_HEAD.CANCEL_REASON%TYPE,
                                I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END WF_ALLOC_SQL;
/
