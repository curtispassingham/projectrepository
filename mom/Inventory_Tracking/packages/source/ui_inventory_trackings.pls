CREATE OR REPLACE PACKAGE INVENTORY_TRACKING_SQL AUTHID CURRENT_USER IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : packbrk (Form Module) 
  -- Source Object            : PROCEDURE  -> P_SET_VPN
  ---------------------------------------------------------------------------------------------
FUNCTION SET_VPN(O_error_message       IN OUT   VARCHAR2,
				 I_TI_PACK_COMPONENT   IN       VARCHAR2,
				 I_TI_VPN              IN OUT   VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : stkvarnc (Form Module) 
  -- Source Object            : PROCEDURE  -> P_STAKE_CONT_INSERT
  ---------------------------------------------------------------------------------------------
FUNCTION STAKE_CONT_INSERT(O_error_message        IN OUT   VARCHAR2,
						   I_CYCLE_COUNT          IN       NUMBER,
						   I_RUN_TYPE             IN       VARCHAR2,
						   I_TI_LOCATION          IN       NUMBER,
						   I_TI_LOC_TYPE          IN       VARCHAR2,
						   P_IntrnlVrblsGpUsrId   IN       STAKE_CONT.USER_ID%TYPE )
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : stkvwedt (Form Module) 
  -- Source Object            : PROCEDURE  -> P_GET_COSTS
  ---------------------------------------------------------------------------------------------
FUNCTION GET_COSTS(O_error_message         IN OUT   VARCHAR2,
				   I_CYCLE_COUNT           IN       NUMBER,
				   I_DI_CURRENCY_CODE1     IN OUT   VARCHAR2,
				   I_DI_CURRENCY_CODE2     IN OUT   VARCHAR2,
				   I_LOCATION              IN       NUMBER,
				   I_LOC_TYPE              IN       VARCHAR2,
				   I_TI_ITEM               IN       VARCHAR2,
				   I_TI_UNIT_COST_PRIM     IN OUT   NUMBER,
				   I_TI_UNIT_RETAIL_PRIM   IN OUT   NUMBER,
				   I_UNIT_COST             IN OUT   NUMBER,
				   I_UNIT_RETAIL           IN OUT   NUMBER,
				   P_PM_CURR_LOC           IN       VARCHAR2,
				   P_PM_CURR_PRIM          IN       VARCHAR2,
				   P_PM_LOC_TYPE           IN       VARCHAR2)
   return BOOLEAN;
END INVENTORY_TRACKING_SQL;
/