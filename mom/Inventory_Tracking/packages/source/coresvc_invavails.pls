CREATE OR REPLACE PACKAGE CORESVC_INVAVAIL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- Function Name: GET_INV_DETAIL
-- Purpose:       This function will internally call the VALIDATE_INVAVAIL to
--                validate the input request record and then GET_CUSTAVAILABLE_INV
--                to fetch and return the inventory position details.
-- Called by:     SVCPROV_INVAVAIL.GET_INV_DETAIL
--------------------------------------------------------------------------------
FUNCTION GET_INV_DETAIL(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_business_object OUT "RIB_InvAvailColDesc_REC",
                        O_error_tbl       OUT SVCPROV_UTILITY.ERROR_TBL,
                        I_business_object IN  "RIB_InvAvailCriVo_REC")
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END CORESVC_INVAVAIL;
/
