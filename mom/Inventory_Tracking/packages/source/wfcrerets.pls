CREATE OR REPLACE PACKAGE WF_CREATE_RETURN_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------------------------
-- Public Function
---------------------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_F_RETURN
-- Purpose      : This acts as main function for the package. The function validates the input return record and
--                calls other internal functions to perform Create/Update/Delete operations based on the value of 
--                I_action_type parameter.
--                I_action_type takes value of one of the variables, WF_ACTION_CREATE, WF_ACTION_UPDATE, 
--                WF_ACTION_CANCEL, WF_ACTION_DELETE in RMS_CONSTANTS.
---------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_F_RETURN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE,
                          I_return_rec      IN       OBJ_F_RETURN_HEAD_REC,
                          I_action_type     IN       VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
END WF_CREATE_RETURN_SQL;
/