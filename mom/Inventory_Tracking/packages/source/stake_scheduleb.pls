CREATE OR REPLACE PACKAGE BODY STAKE_SCHEDULE_SQL AS

TYPE sp_cycle_count_TBL  is table of STAKE_PRODUCT.CYCLE_COUNT%TYPE  INDEX BY BINARY_INTEGER;
TYPE sp_dept_TBL         is table of STAKE_PRODUCT.DEPT%TYPE         INDEX BY BINARY_INTEGER;
TYPE sp_class_TBL        is table of STAKE_PRODUCT.CLASS%TYPE        INDEX BY BINARY_INTEGER;
TYPE sp_subclass_TBL     is table of STAKE_PRODUCT.SUBCLASS%TYPE     INDEX BY BINARY_INTEGER;

TYPE sl_cycle_count_TBL  is table of STAKE_LOCATION.CYCLE_COUNT%TYPE  INDEX BY BINARY_INTEGER;
TYPE sl_location_TBL     is table of STAKE_LOCATION.LOCATION%TYPE     INDEX BY BINARY_INTEGER;
TYPE sl_loc_type_TBL     is table of STAKE_LOCATION.LOC_TYPE%TYPE     INDEX BY BINARY_INTEGER;

P_sh_ins_cycle_count         STAKE_HEAD.CYCLE_COUNT%TYPE := NULL;
P_sh_ins_cycle_count_desc    STAKE_HEAD.CYCLE_COUNT_DESC%TYPE := NULL;
P_sh_ins_loc_type            STAKE_HEAD.LOC_TYPE%TYPE := NULL;
P_sh_ins_stocktake_date      STAKE_HEAD.STOCKTAKE_DATE%TYPE := NULL;
P_sh_ins_stocktake_type      STAKE_HEAD.STOCKTAKE_TYPE%TYPE := NULL;
P_sh_ins_product_level_ind   STAKE_HEAD.PRODUCT_LEVEL_IND%TYPE := 'D';

P_sp_ins_size          NUMBER := 0;
P_sp_ins_cycle_count   sp_cycle_count_TBL;
P_sp_ins_dept          sp_dept_TBL;
P_sp_ins_class         sp_class_TBL;
P_sp_ins_subclass      sp_subclass_TBL;

P_sl_ins_size          NUMBER := 0;
P_sl_ins_cycle_count   sl_cycle_count_TBL;

P_sl_ins_location      sl_location_TBL;
P_sl_ins_loc_type      sl_loc_type_TBL;

P_message_action       VARCHAR2(15) := null;

/* Private function specs */

FUNCTION CREATE_SP_REC(O_error_message  IN OUT  VARCHAR2,
                       I_cycle_count    IN      STAKE_PRODUCT.CYCLE_COUNT%TYPE,
                       I_dept           IN      STAKE_PRODUCT.DEPT%TYPE,
                       I_class          IN      STAKE_PRODUCT.CLASS%TYPE,
                       I_subclass       IN      STAKE_PRODUCT.SUBCLASS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION CREATE_SL_REC(O_error_message  IN OUT  VARCHAR2,
                       I_cycle_count    IN      STAKE_LOCATION.CYCLE_COUNT%TYPE,
                       I_location_type  IN      STAKE_LOCATION.LOC_TYPE%TYPE,
                       I_location       IN      STAKE_LOCATION.LOCATION%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION DELETE_RECS(O_error_message  IN OUT  VARCHAR2,
                     I_cycle_count    IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION VALIDATE_HIERARCHY(O_error_message  IN OUT  VARCHAR2,
                            I_location_type  IN      STAKE_HEAD.LOC_TYPE%TYPE,
                            I_dept           IN      STAKE_PRODUCT.DEPT%TYPE,
                            I_class          IN      STAKE_PRODUCT.CLASS%TYPE,
                            I_subclass       IN      STAKE_PRODUCT.SUBCLASS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message  IN OUT  VARCHAR2,
                           I_location_type  IN      STAKE_LOCATION.LOC_TYPE%TYPE,
                           I_location       IN      STAKE_LOCATION.LOCATION%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------

/* Function Bodies */
-------------------------------------------------------------------------
FUNCTION PROCESS_PROD(O_error_message     IN OUT  VARCHAR2,
                      I_record_passed     IN      VARCHAR2,
                      I_cycle_count       IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                      I_location_type     IN      STAKE_HEAD.LOC_TYPE%TYPE,
                      I_dept              IN      STAKE_PRODUCT.DEPT%TYPE,
                      I_class             IN      STAKE_PRODUCT.CLASS%TYPE,
                      I_subclass          IN      STAKE_PRODUCT.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_module         VARCHAR2(64)   := 'STAKE_SCHEDULE_SQL.PROCESS_PROD';
   L_subclass_cnt   NUMBER(38)     := 0;
   L_class_cnt      NUMBER(38)     := 0;
   L_dept_cnt       NUMBER(38)     := 0;
   L_cc_cnt         NUMBER(38)     := 0;

   cursor C_GET_DEPS is
      select dept
        from deps;

BEGIN
   -- validate input
   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_cycle_count',
                                            'NULL', L_module);
      return false;
   elsif I_location_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_location_type',
                                            'NULL', L_module);
      return false;
   elsif I_record_passed not in ('Y', 'N') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_record_passed',
                                            I_record_passed, L_module);
      return false;
   elsif I_dept is NULL and I_record_passed = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_dept',
                                            'NULL', L_module);
      return false;
   end if;

   -- Check to make sure that we are dealing with the
   -- same cycle count as what the head level has.

   if P_sh_ins_cycle_count is NULL
      or I_cycle_count != P_sh_ins_cycle_count then

      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_cycle_count',
                                            I_cycle_count, L_module);
      return FALSE;
   end if;


   if I_record_passed = 'N' then

      FOR rec in C_GET_DEPS LOOP

         if CREATE_SP_REC(O_error_message,
                          I_cycle_count,
                          rec.dept,
                          null,
                          null) = FALSE then
            return FALSE;
         end if;

      END LOOP;
   else -- record was passed

      if VALIDATE_HIERARCHY(O_error_message,
                            I_location_type,
                            I_dept,
                            I_class,
                            I_subclass) = FALSE then
         return FALSE;
      end if;

      if CREATE_SP_REC(O_error_message,
                       I_cycle_count,
                       I_dept,
                       I_class,
                       I_subclass) = FALSE then
         return FALSE;
      end if;

      -- keep track of the hierarchy level for stake_head insert/update
      if P_sp_ins_cycle_count.COUNT = 1 then
         if I_class is NOT NULL
            and I_subclass is NULL
            and P_sh_ins_product_level_ind = 'D' then
            P_sh_ins_product_level_ind := 'C';

         elsif I_class is NOT NULL
            and I_subclass is NOT NULL
            and P_sh_ins_product_level_ind != 'S' then

            P_sh_ins_product_level_ind := 'S';
         end if;
      elsif P_sp_ins_cycle_count.COUNT > 1 then
         L_cc_cnt := P_sp_ins_cycle_count.COUNT;
         if P_sp_ins_dept.COUNT > 1 then
            for a in 1..P_sp_ins_subclass.COUNT loop
               if P_sp_ins_subclass(a) is NOT NULL then
                  L_subclass_cnt := L_subclass_cnt + 1;
               end if;
            end loop;
            for b in 1..P_sp_ins_class.COUNT loop
               if P_sp_ins_class(b) is NOT NULL then
                  L_class_cnt := L_class_cnt + 1;
               end if;
            end loop;
            for c in 1..P_sp_ins_dept.COUNT loop
               if P_sp_ins_dept(c) is NOT NULL then
                  L_dept_cnt := L_dept_cnt + 1;
               end if;
            end loop;
            ---
            if L_cc_cnt = L_subclass_cnt and L_class_cnt > 0 and L_dept_cnt > 0 then
               P_sh_ins_product_level_ind := 'S';
            elsif L_cc_cnt = L_class_cnt and L_dept_cnt > 0 and L_subclass_cnt = 0 then
               P_sh_ins_product_level_ind := 'C';
            elsif L_cc_cnt = L_dept_cnt and L_class_cnt = 0 and L_subclass_cnt = 0 then
               P_sh_ins_product_level_ind := 'D';
            else
               P_sh_ins_product_level_ind := 'N';
            end if;
         end if;
      end if;

   end if; -- I_record_passed = N

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END PROCESS_PROD;
-------------------------------------------------------------------------
FUNCTION PROCESS_LOC(O_error_message  IN OUT  VARCHAR2,
                     I_record_passed  IN      VARCHAR2,
                     I_cycle_count    IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                     I_location_type  IN      STAKE_HEAD.LOC_TYPE%TYPE,
                     I_location       IN      STAKE_LOCATION.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.PROCESS_LOC';

   cursor C_GET_STORES is
      select store
        from store;

   cursor C_GET_WHS is
      select wh
        from wh
       where stockholding_ind = 'Y';

BEGIN
   -- validate input
   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_cycle_count',
                                            'NULL', L_module);
      return false;
   elsif I_location_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_location_type',
                                            'NULL', L_module);
      return false;
   elsif I_record_passed not in ('Y', 'N') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_record_passed',
                                            I_record_passed, L_module);
      return false;
   elsif I_location is NULL and I_record_passed = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_location',
                                            'NULL', L_module);
      return false;
   end if;

   -- Check to make sure that we are dealing with the
   -- same cycle count as what the head level has.

   if P_sh_ins_cycle_count is NULL
      or I_cycle_count != P_sh_ins_cycle_count then

      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_cycle_count',
                                            I_cycle_count, L_module);
      return FALSE;
   end if;

   if I_record_passed = 'N' then

      if I_location_type = 'S' then

         FOR rec in C_GET_STORES LOOP

            if CREATE_SL_REC(O_error_message,
                             I_cycle_count,
                             'S',
                             rec.store) = FALSE then
               return FALSE;
            end if;

         END LOOP;

      else -- loc_type is W

         FOR rec in C_GET_WHS LOOP

            if CREATE_SL_REC(O_error_message,
                             I_cycle_count,
                             'W',
                             rec.wh) = FALSE then
               return FALSE;
            end if;

         END LOOP;

      end if; -- loc type is S

   else -- record was passed

      if VALIDATE_LOCATION(O_error_message,
                           I_location_type,
                           I_location) = FALSE then
         return FALSE;
      end if;

      if CREATE_SL_REC(O_error_message,
                       I_cycle_count,
                       I_location_type,
                       I_location) = FALSE then
         return FALSE;
      end if;

   end if; -- record passed = N

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END PROCESS_LOC;
-------------------------------------------------------------------------
FUNCTION INIT(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.INIT';

BEGIN

   P_sh_ins_cycle_count        := NULL;
   P_sh_ins_cycle_count_desc   := NULL;
   P_sh_ins_loc_type := NULL;
   P_sh_ins_stocktake_date     := NULL;
   P_sh_ins_stocktake_type     := NULL;
   P_sh_ins_product_level_ind  := 'D';

   P_sp_ins_size        := 0;

   P_sl_ins_size        := 0;

   P_message_action := null;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END INIT;
-------------------------------------------------------------------------
FUNCTION FLUSH(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.FLUSH';

BEGIN

   if P_message_action = STAKE_SCHEDULE_SQL.StkCountSchCre then

      SQL_LIB.SET_MARK('INSERT', NULL, 'STAKE_HEAD', 'cycle_count: '||P_sh_ins_cycle_count);
      -- create the new stake_head rec for this stock count
      insert into stake_head(cycle_count,
                             cycle_count_desc,
                             loc_type,
                             stocktake_date,
                             stocktake_type,
                             product_level_ind,
                             delete_ind)
                      values(P_sh_ins_cycle_count,
                             P_sh_ins_cycle_count_desc,
                             P_sh_ins_loc_type,
                             trunc(P_sh_ins_stocktake_date), -- store the stocktake_date without timestamp
                             P_sh_ins_stocktake_type,
                             P_sh_ins_product_level_ind,
                             'N');

   elsif P_message_action = STAKE_SCHEDULE_SQL.StkCountSchMod then

      -- mods are complete overlays, so delete all prod and loc records
      if DELETE_RECS(O_error_message,
                     P_sh_ins_cycle_count) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('UPDATE', NULL, 'STAKE_HEAD', 'cycle_count: '||P_sh_ins_cycle_count);
      -- update the stake_head rec for this stock count
      update stake_head
         set cycle_count_desc  = P_sh_ins_cycle_count_desc,
             stocktake_date    = trunc(P_sh_ins_stocktake_date),
             product_level_ind = P_sh_ins_product_level_ind
       where cycle_count = P_sh_ins_cycle_count;

   end if;

   SQL_LIB.SET_MARK('INSERT', NULL, 'STAKE_PRODUCT', 'BULK INSERT');
   -- insert into stake_product
   FORALL i in 1..P_sp_ins_size
      insert into stake_product(cycle_count,
                                dept,
                                class,
                                subclass)
                         values(P_sp_ins_cycle_count(i),
                                P_sp_ins_dept(i),
                                P_sp_ins_class(i),
                                P_sp_ins_subclass(i));

   SQL_LIB.SET_MARK('INSERT', NULL, 'STAKE_HEAD', 'BULK INSERT');
   -- insert into stake_location
   FORALL i in 1..P_sl_ins_size
      insert into stake_location(cycle_count,
                                 loc_type,
                                 location)
                          values(P_sl_ins_cycle_count(i),
                                 P_sl_ins_loc_type(i),
                                 P_sl_ins_location(i));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END FLUSH;
-------------------------------------------------------------------------
FUNCTION PROCESS_DEL(O_error_message  IN OUT  VARCHAR2,
                     I_cycle_count    IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.PROCESS_DEL';

BEGIN
   -- validate input
   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_cycle_count',
                                            'NULL', L_module);
      return false;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'STAKE_HEAD', 'cycle_count: '||I_cycle_count);
   update stake_head
      set delete_ind = 'Y'
    where cycle_count = I_cycle_count;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END PROCESS_DEL;
-------------------------------------------------------------------------
FUNCTION CREATE_SH_REC(O_error_message     IN OUT  VARCHAR2,
                       I_cycle_count       IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                       I_cycle_count_desc  IN      STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                       I_location_type     IN      STAKE_HEAD.LOC_TYPE%TYPE,
                       I_stocktake_date    IN      STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                       I_stocktake_type    IN      STAKE_HEAD.STOCKTAKE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.CREATE_SH_REC';

BEGIN

   P_sh_ins_cycle_count        := I_cycle_count;
   P_sh_ins_cycle_count_desc   := I_cycle_count_desc;
   P_sh_ins_loc_type           := I_location_type;
   P_sh_ins_stocktake_date     := I_stocktake_date;
   P_sh_ins_stocktake_type     := I_stocktake_type;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END CREATE_SH_REC;
-------------------------------------------------------------------------
FUNCTION CREATE_SP_REC(O_error_message  IN OUT  VARCHAR2,
                       I_cycle_count    IN      STAKE_PRODUCT.CYCLE_COUNT%TYPE,
                       I_dept           IN      STAKE_PRODUCT.DEPT%TYPE,
                       I_class          IN      STAKE_PRODUCT.CLASS%TYPE,
                       I_subclass       IN      STAKE_PRODUCT.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.CREATE_SP_REC';

BEGIN

   P_sp_ins_size                        := P_sp_ins_size + 1;
   P_sp_ins_cycle_count(P_sp_ins_size)  := I_cycle_count;
   P_sp_ins_dept(P_sp_ins_size)         := I_dept;
   P_sp_ins_class(P_sp_ins_size)        := I_class;
   P_sp_ins_subclass(P_sp_ins_size)     := I_subclass;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END CREATE_SP_REC;
-------------------------------------------------------------------------
FUNCTION CREATE_SL_REC(O_error_message  IN OUT  VARCHAR2,
                       I_cycle_count    IN      STAKE_LOCATION.CYCLE_COUNT%TYPE,
                       I_location_type  IN      STAKE_LOCATION.LOC_TYPE%TYPE,
                       I_location       IN      STAKE_LOCATION.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.CREATE_SL_REC';

BEGIN

   P_sl_ins_size                       := P_sl_ins_size + 1;
   P_sl_ins_cycle_count(P_sl_ins_size) := I_cycle_count;
   P_sl_ins_loc_type(P_sl_ins_size)    := I_location_type;
   P_sl_ins_location(P_sl_ins_size)    := I_location;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END CREATE_SL_REC;
-------------------------------------------------------------------------
FUNCTION DELETE_RECS(O_error_message  IN OUT  VARCHAR2,
                     I_cycle_count    IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.DELETE_RECS';

BEGIN

   SQL_LIB.SET_MARK('DELETE', NULL, 'STAKE_PRODUCT', 'cycle_count: '||I_cycle_count);
   delete from stake_product
    where cycle_count = I_cycle_count;

   SQL_LIB.SET_MARK('DELETE', NULL, 'STAKE_LOCATION', 'cycle_count: '||I_cycle_count);
   delete from stake_location
    where cycle_count = I_cycle_count;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END DELETE_RECS;
-------------------------------------------------------------------------
FUNCTION VALIDATE_VALUES(O_error_message   IN OUT  VARCHAR2,
                         I_cycle_count     IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                         I_stocktake_type  IN      STAKE_HEAD.STOCKTAKE_TYPE%TYPE,
                         I_stocktake_date  IN      STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                         I_location_type   IN      STAKE_HEAD.LOC_TYPE%TYPE,
                         I_action          IN      VARCHAR2)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.VALIDATE_VALUES';

   L_stocktake_date       STAKE_HEAD.STOCKTAKE_DATE%TYPE          := NULL;
   L_stocktake_type       STAKE_HEAD.STOCKTAKE_TYPE%TYPE          := NULL;
   L_location_type        STAKE_HEAD.LOC_TYPE%TYPE      := NULL;
   L_delete_ind           STAKE_HEAD.DELETE_IND%TYPE              := NULL;
   L_stake_lockout_days   SYSTEM_OPTIONS.STAKE_LOCKOUT_DAYS%TYPE  := NULL;
   L_vdate                PERIOD.VDATE%TYPE                       := NULL;
   L_stake_sku_loc_exist  VARCHAR2(2)                             := NULL;

   cursor C_CYCLE_COUNT is
      select stocktake_date,
             stocktake_type,
             loc_type,
             delete_ind
        from stake_head
       where cycle_count = I_cycle_count;

   cursor C_STAKE_LOCKOUT is
      select so.stake_lockout_days,
             p.vdate
        from system_options so,
             period p;

   cursor C_STAKE_SKU_LOC is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
	 and processed   = 'P';

BEGIN
   open C_STAKE_LOCKOUT;
   fetch C_STAKE_LOCKOUT into L_stake_lockout_days,
                              L_vdate;
   close C_STAKE_LOCKOUT;

   open C_CYCLE_COUNT;
   fetch C_CYCLE_COUNT into L_stocktake_date,
                            L_stocktake_type,
                            L_location_type,
                            L_delete_ind;
   close C_CYCLE_COUNT;

   open C_STAKE_SKU_LOC;
   fetch C_STAKE_SKU_LOC into L_stake_sku_loc_exist;
   close C_STAKE_SKU_LOC;

   P_message_action := lower(I_action);

   if P_message_action = STAKE_SCHEDULE_SQL.StkCountSchDel then
      if L_stocktake_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STOCK_COUNT');
         return FALSE;
       end if;

      if L_stake_sku_loc_exist = 'x' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_STKCOUNT');
         return FALSE;
      end if;

   else -- message_action != del

      if I_stocktake_type != 'B' then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                               'I_stocktake_type',
                                               I_stocktake_type,
                                               L_module);
         return FALSE;
      end if;

      if I_location_type not in ('S', 'W') then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                               'I_location_type',
                                               L_module);
         return FALSE;
      end if;

      -- check if the passed in stocktake date is valid
      if (L_vdate + L_stake_lockout_days) > I_stocktake_date then
         O_error_message := SQL_LIB.CREATE_MSG('STKCOUNT_NOT_BEFORE',
                                               L_vdate + L_stake_lockout_days);
         return FALSE;
      end if;

      if P_message_action = STAKE_SCHEDULE_SQL.StkCountSchCre then
         if L_stocktake_date is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('STOCK_COUNT_EXIST');
            return FALSE;
         end if;

      elsif P_message_action = STAKE_SCHEDULE_SQL.StkCountSchMod then
         -- if stocktake_date is null, this cycle count does not exist
         if L_stocktake_date is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STOCK_COUNT');
            return FALSE;
          end if;

         if L_delete_ind = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_UPD_STKCNT_DELETED',
                                                  I_cycle_count);
            return FALSE;
         end if;

         if L_stocktake_type != 'B' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_MOD_STKCNT_TYPE_NOT_B',
                                                  I_cycle_count);
            return FALSE;
         end if;

         if L_stake_sku_loc_exist = 'x' then
            O_error_message := SQL_LIB.CREATE_MSG('STKCOUNT_PASSED');
            return FALSE;
         end if;

         if L_location_type != I_location_type then
            if I_location_type = 'S' then
               O_error_message := SQL_LIB.CREATE_MSG('NO_WH_FOR_STKCOUNT');
            else
               O_error_message := SQL_LIB.CREATE_MSG('NO_STORE_FOR_STKCOUNT');
            end if;
            return FALSE;
         end if;

      end if; -- message_action = cre or mod

   end if; -- message_action = del

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END VALIDATE_VALUES;
-------------------------------------------------------------------------
FUNCTION VALIDATE_HIERARCHY(O_error_message  IN OUT  VARCHAR2,
                            I_location_type  IN      STAKE_HEAD.LOC_TYPE%TYPE,
                            I_dept           IN      STAKE_PRODUCT.DEPT%TYPE,
                            I_class          IN      STAKE_PRODUCT.CLASS%TYPE,
                            I_subclass       IN      STAKE_PRODUCT.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.VALIDATE_HIERARCHY';

   L_dummy            VARCHAR2(2)   := null;

   cursor C_VALID_DEPT is
      select 'x'
        from deps
       where dept = I_dept;

   cursor C_VALID_CLASS is
      select 'x'
        from class
       where class = I_class
         and dept = I_dept;

   cursor C_VALID_SUBCLASS is
      select 'x'
        from subclass
       where subclass = I_subclass
         and class = I_class
         and dept = I_dept;

BEGIN

   if I_location_type = 'W'
      and (I_class is NOT NULL
           or I_subclass is NOT NULL) then

      O_error_message := sql_lib.create_msg('WH_ONLY_DEPT_STKCNT');
      return FALSE;
   end if;

   if I_class is NULL then
      open C_VALID_DEPT;
      fetch C_VALID_DEPT into L_dummy;
      if C_VALID_DEPT%NOTFOUND then
         close C_VALID_DEPT;
         O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT_DEPT',
                                               I_dept);
         return FALSE;
      end if;

      close C_VALID_DEPT;

   elsif I_class is NOT NULL
      and I_subclass is NULL then

      open C_VALID_CLASS;
      fetch C_VALID_CLASS into L_dummy;
      if C_VALID_CLASS%NOTFOUND then
         close C_VALID_CLASS;
         O_error_message := SQL_LIB.CREATE_MSG('INV_CLASS_DEPT',
                                               I_class,
                                               I_dept);
         return FALSE;
      end if;

      close C_VALID_CLASS;

   elsif I_class is NOT NULL
      and I_subclass is NOT NULL then

      open C_VALID_SUBCLASS;
      fetch C_VALID_SUBCLASS into L_dummy;
      if C_VALID_SUBCLASS%NOTFOUND then
         close C_VALID_SUBCLASS;
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUBCLASS_CLASS_DEPT',
                                               I_subclass,
                                               I_dept,
                                               I_class);
         return FALSE;
      end if;

      close C_VALID_SUBCLASS;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END VALIDATE_HIERARCHY;
-------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message  IN OUT  VARCHAR2,
                           I_location_type  IN      STAKE_LOCATION.LOC_TYPE%TYPE,
                           I_location       IN      STAKE_LOCATION.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_module     VARCHAR2(64)  := 'STAKE_SCHEDULE_SQL.VALIDATE_LOCATION';

   L_dummy      VARCHAR2(2)   := NULL;

   cursor C_VALID_STORE is
      select 'x'
        from store
       where store = I_location;

   cursor C_VALID_WH is
      select 'x'
        from wh
       where wh = I_location
         and stockholding_ind = 'Y';

BEGIN

   if UPPER(I_location_type) = 'W' then -- check the wh value
      open C_VALID_WH;
      fetch C_VALID_WH into L_dummy;
      if C_VALID_WH%NOTFOUND then
         close C_VALID_WH;
         O_error_message := SQL_LIB.CREATE_MSG('INV_STOCKHOLDING_WH',
                                               I_location);
         return FALSE;
      end if;

      close C_VALID_WH;

   else -- check the store value
      open C_VALID_STORE;
      fetch C_VALID_STORE into L_dummy;
      if C_VALID_STORE%NOTFOUND then
         close C_VALID_STORE;
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_STORE',
                                               I_location);
         return FALSE;
      end if;

      close C_VALID_STORE;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  return FALSE;
END VALIDATE_LOCATION;
-------------------------------------------------------------------------
END STAKE_SCHEDULE_SQL;
/
