
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE UPDATE_SNAPSHOT_SQL AUTHID CURRENT_USER AS
-- Note: Overloading is used in this package   
-- Purpose:  Updates snapshot information in the system 
--           when post-dated inventory adjustments are
--           processed.  
--           Cycle Count snapshots will be updated if the
--           transaction date is between the current date
--           and the cycle count date, and the EDI daily
--           sales snapshots will be updated if they are
--           within a pre-determined number of "lag" days
--           of the current date. An exception is made for
--           the cycle count adjustments, all stock on hand
--           snapshots on the EDI sales tables will be 
--           updated regardless of transaction date. 
--------------------------------------------------------
-- Function: Execute 
-- Purpose:  Updates snapshot and cycle count information
--           for transaction
--           type 'SALADJ' 
-- Calls:
-- Created:  Kristen LeClair 4/13/97
--------------------------------------------------------
FUNCTION EXECUTE(O_error_message     IN OUT  VARCHAR2,
                 I_tran_type         IN      VARCHAR2,
                 I_item              IN      ITEM_MASTER.ITEM%TYPE,
                 I_rpt_freq          IN      VARCHAR2,
                 I_sal_store         IN      ITEM_LOC.LOC%TYPE,
                 I_tran_date         IN      DATE,
                 I_vdate             IN      DATE,
                 I_adj_qty           IN      NUMBER)
                 RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function: Execute 
-- Purpose:  Updates snapshot and cycle count information 
--           for transaction
--           types 'TFSO' and 'TFSI' 
-- Calls:
-- Created:  Kristen LeClair 4/13/97
--------------------------------------------------------
FUNCTION EXECUTE(O_error_message     IN OUT  VARCHAR2,
                 I_tran_type         IN      VARCHAR2,
                 I_item              IN      ITEM_MASTER.ITEM%TYPE,
                 I_pack_ind          IN      VARCHAR2,
                 I_rcv_store         IN      ITEM_LOC.LOC%TYPE,
                 I_rcv_wh            IN      ITEM_LOC.LOC%TYPE,
                 I_send_store        IN      ITEM_LOC.LOC%TYPE,
                 I_send_wh           IN      ITEM_LOC.LOC%TYPE,
                 I_tran_date         IN      DATE,
                 I_vdate             IN      DATE,
                 I_adj_qty           IN      NUMBER,
                 I_rcv_as_type       IN      ITEM_LOC.RECEIVE_AS_TYPE%TYPE   DEFAULT NULL)
                 RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function: Execute 
-- Purpose:  Updates snapshot and cycle count information 
--           for transaction
--           types 'TFSO' and 'TFSI' 
-- Calls:
-- Created:  Kristen LeClair 4/13/97
--------------------------------------------------------
FUNCTION EXECUTE(O_error_message     IN OUT  VARCHAR2,
                 I_tran_type         IN      VARCHAR2,
                 I_item              IN      ITEM_MASTER.ITEM%TYPE,
                 I_pack_ind          IN      VARCHAR2,
                 I_rcv_loc_type      IN      ITEM_LOC.LOC_TYPE%TYPE,
                 I_rcv_location      IN      ITEM_LOC.LOC%TYPE,
                 I_send_loc_type     IN      ITEM_LOC.LOC_TYPE%TYPE,
                 I_send_location     IN      ITEM_LOC.LOC%TYPE,
                 I_tran_date         IN      DATE,
                 I_vdate             IN      DATE,
                 I_adj_qty           IN      NUMBER,
                 I_rcv_as_type       IN      ITEM_LOC.RECEIVE_AS_TYPE%TYPE   DEFAULT NULL)
                 RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function: Execute 
-- Purpose:  Updates snapshot  and cycle count 
--           information for transaction
--           types  'RCV', 'RTV', and 'INVADJ' 
-- Calls:
-- Created:  Kristen LeClair 4/13/97
--------------------------------------------------------
FUNCTION EXECUTE(O_error_message     IN OUT  VARCHAR2,
                 I_tran_type         IN      VARCHAR2,
                 I_item              IN      ITEM_MASTER.ITEM%TYPE,
                 I_tran_store        IN      ITEM_LOC.LOC%TYPE,
                 I_tran_wh           IN     ITEM_LOC.LOC%TYPE,
                 I_tran_date         IN      DATE,
                 I_vdate             IN      DATE,
                 I_adj_qty           IN      NUMBER)
                 RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function: Execute 
-- Purpose:  Updates snapshot  and cycle count 
--           information for transaction
--           type 'STKADJ' 
-- Calls:
-- Created:  Kristen LeClair 4/13/97
--------------------------------------------------------
FUNCTION EXECUTE(O_error_message       IN OUT    VARCHAR2,
                 I_tran_type           IN        VARCHAR2,
                 I_item                IN        ITEM_MASTER.ITEM%TYPE,
                 I_tran_loc_type       IN        ITEM_LOC.LOC_TYPE%TYPE,
                 I_tran_location       IN        ITEM_LOC.LOC%TYPE,
                 I_adj_qty             IN        NUMBER)
                 RETURN BOOLEAN;

-------------------------------------------------------------------
-- Function: Execute 
-- Purpose:  Updates snapshot  and cycle count 
--           information 
--------------------------------------------------------
FUNCTION EXECUTE(O_error_message       IN OUT    VARCHAR2,
                 I_item                IN        ITEM_MASTER.ITEM%TYPE,
                 I_tran_store          IN       ITEM_LOC.LOC%TYPE,
                 I_adj_qty             IN        NUMBER)
                 RETURN BOOLEAN;
				 
------------------------------------------------------------------- 
-- Function: UPDATE_SNAPSHOT_COST  
-- Purpose:  Updates snapshot cost 
-------------------------------------------------------- 
FUNCTION UPDATE_SNAPSHOT_COST(O_error_message IN OUT VARCHAR2, 
                              I_item          IN     item_loc.item%TYPE, 
                              I_loc_type      IN     item_loc.loc_type%TYPE, 
                              I_location      IN     item_loc.loc%TYPE, 
                              I_tran_date     IN     DATE, 
                              I_adj_qty       IN     item_loc_soh.stock_on_hand%TYPE, 
                              I_unit_cost     IN     item_loc_soh.unit_cost%TYPE) 
                              RETURN BOOLEAN; 

END;
/
