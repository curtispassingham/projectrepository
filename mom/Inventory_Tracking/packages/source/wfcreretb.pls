CREATE OR REPLACE PACKAGE BODY WF_CREATE_RETURN_SQL AS

LP_user   VARCHAR2(50) := get_user;
---------------------------------------------------------------------------------------------------------------------
-- Private functions' specifications
---------------------------------------------------------------------------------------------------------------------
-- Function Name : VALIDATE_F_RETURN
-- Purpose       : This function checks for required values in the input return record, validates the franchise 
--                 return record as per the business logic before the record is applied to database tables.
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_F_RETURN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_return_rec      IN       OBJ_F_RETURN_HEAD_REC,
                           I_action_type     IN       VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function Name : CREATE_HEADER
-- Purpose       : The function generates a new RMA number from a sequence, inserts header level franchise return
--                 values into WF_RETURN_HEAD table and returns the RMA number. Returned RMA number will be stored
--                 in RMA_NO column of TSFHEAD table for the transfer correspinding to the franchise return. Currency
--                 of the return will be the franchise location's currency.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function Name : MODIFY_HEADER
-- Purpose       : The function updates header of a franchise return. Only status, and comments columns can be
--                 updated.
---------------------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN;   
---------------------------------------------------------------------------------------------------------------------
-- Function Name : CANCEL_RETURN
-- Purpose       : Sets the status of a franchise return to 'Cancelled' when the corresponding transfer is deleted.
--                 The function also sets the cancel reason for the return. Return quantity of all the items in the
--                 return is set to zero and the cancel reason is copied to all items.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CANCEL_RETURN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN;   
---------------------------------------------------------------------------------------------------------------------
-- Function Name : CREATE_DETAIL
-- Purpose       : 1. The function compares the return details in the input record with that of in the 
--                    WF_RETURN_DETAIL table, extracts the lines to be newly created from the input record and inserts
--                    them into WF_RETURN_DETAIL table.
--                 2. Return unit cost, if NULL in the input, for an item will be the average cost of the item at the
--                    franchise location.
--                 3. If a new item has to be added to a return with status 'A', the item will be added with zero return
--                    quantity.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC,
                       I_action_type     IN       VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function Name : MODIFY_DETAIL
-- Purpose       : 1. The function compares the return details in the input record with that of in the WF_RETURN_DETAIL
--                    table, extracts the lines that already exist in WF_RETURN_DETAIL table and updates them. 
--                 2. A return detail can be modified only if the return is in INPUT state. Return qty, return unit cost,
--                    and return reason fields can be updated using the function.
---------------------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function Name : DELETE_DETAIL
-- Purpose       : 1. The function compares the return details in the input record with that of in the WF_RETURN_DETAIL
--                    table, extracts the lines from WF_RETURN_DETAIL table that don't exist in the input record.
--                 2. The function deletes the extracted details from WF_RETURN_DETAIL table.                 
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function Name : RANGE_CONTAINERS
-- Purpose       : Fetches containers, for the contents having item and to-location relationship in a return, that are
--                 not ranged to return location and creates new item-loc combinations with ranged_ind same as that of
--                 the contents.    
---------------------------------------------------------------------------------------------------------------------      
FUNCTION RANGE_CONTAINERS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------         
-- FUNCTION DEFINITIONS
---------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_F_RETURN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE,
                          I_return_rec      IN       OBJ_F_RETURN_HEAD_REC,
                          I_action_type     IN       VARCHAR2)
   RETURN BOOLEAN IS
   
   L_program                VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.PROCESS_F_RETURN';
   L_rma_no                 WF_RETURN_HEAD.RMA_NO%TYPE;
   L_total_return_amt       WF_RETURN_HEAD.TOTAL_RETURN_AMT%TYPE;
   L_total_restock_amt      WF_RETURN_HEAD.TOTAL_RESTOCK_AMT%TYPE;
   L_total_net_return_amt   WF_RETURN_HEAD.TOTAL_NET_RETURN_AMT%TYPE;
   L_table                  VARCHAR2(30);
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked,-54);   
   
   cursor C_LOCK_WF_RETURN_HEAD is
      select 'X'
        from wf_return_head
       where rma_no = L_rma_no
         for update nowait;   
          
BEGIN
   
   if I_return_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_return_rec', 
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_action_type', 
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   -- If action type is not a valid value, throw an error.
   if I_action_type NOT in (RMS_CONSTANTS.WF_ACTION_CREATE, 
                            RMS_CONSTANTS.WF_ACTION_UPDATE, 
                            RMS_CONSTANTS.WF_ACTION_CANCEL, 
                            RMS_CONSTANTS.WF_ACTION_DELETE) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE', 
                                            I_action_type, 
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   
   -- Validate the input return record. Function returns FALSE if any one of the validations fails.
   if VALIDATE_F_RETURN(O_error_message,
                        I_return_rec,
                        I_action_type) = FALSE then
      return FALSE;
   end if;
   
   if I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE then    -- Create a new franchise return
      
      -- Creates franchise return header
      if CREATE_HEADER(O_error_message,
                       L_rma_no,
                       I_return_rec) = FALSE then
         return FALSE;
      end if;
      
      -- Assign the newly generated RMA number to the output variable.
      O_rma_no := L_rma_no;
      
      -- Creates return details.
      if CREATE_DETAIL(O_error_message,  
                       L_rma_no,
                       I_return_rec,
                       I_action_type) = FALSE then
         return FALSE;
      end if;
      
      -- Sync deposit contents and containers in the return.
      if WF_RETURN_SQL.SYNC_DEPOSIT_ITEMS(O_error_message,
                                          L_rma_no) = FALSE then
         return FALSE;
      end if;
      
      -- Ranging of items in return to the 'to-location' is already taken care of by transfer
      -- but for the containers in return, ranging is not done. RANGE_CONTAINERS function will fetch
      -- containers that are not ranged to return location and creates new item-loc
      -- combinations with ranged_ind same as that of the contents.
      if RANGE_CONTAINERS(O_error_message,
                          I_return_rec) = FALSE then
         return FALSE;
      end if;
      
      -- Calculate return totals
      if WF_RETURN_SQL.GET_TOTALS(O_error_message,
                                  L_total_return_amt,
                                  L_total_restock_amt,
                                  L_total_net_return_amt,
                                  L_rma_no) = FALSE then
         return FALSE;
      end if;
      
      L_table := 'WF_RETURN_HEAD';
      open C_LOCK_WF_RETURN_HEAD;
      close C_LOCK_WF_RETURN_HEAD;
      
      -- Update return totals in header table.
      update wf_return_head
         set total_return_amt       = L_total_return_amt,
             total_restock_amt      = L_total_restock_amt,
             total_net_return_amt   = L_total_net_return_amt
       where rma_no = L_rma_no;
             
   elsif I_action_type = RMS_CONSTANTS.WF_ACTION_UPDATE then    -- Modify return header, add items to return, modify items in retur, delete items from return.
   
      if I_return_rec.f_return_details is NOT NULL then
         L_rma_no := I_return_rec.rma_no;
         
         -- If action type is update, all the detail records of franchise transfer are expected in input record to this function.
         -- MODIFY_DETAIL function will extract the common items from input record and database table and update the details of the items.
         if MODIFY_DETAIL(O_error_message,
                          I_return_rec) = FALSE then
            return FALSE;
         end if;
         
         -- DELETE_DETAIL will fetch items that are present in WF_RETURN_DETAIL, for the return, but not in input record and delete the items from table.
         if DELETE_DETAIL(O_error_message,
                          I_return_rec) = FALSE then
            return FALSE;
         end if;
         
         -- CREATE_DETAIL will fetch items that are present in input record but not in WF_RETURN_DETAIL and insert the items into the table.
         if CREATE_DETAIL(O_error_message,
                          L_rma_no,
                          I_return_rec,
                          I_action_type) = FALSE then
            return FALSE;
         end if;
         
         -- Sync deposit items after create/update/delete on return detail
         if WF_RETURN_SQL.SYNC_DEPOSIT_ITEMS(O_error_message,
                                             L_rma_no) = FALSE then
            return FALSE;
         end if;
         
         -- Ranging of items in return to the 'to-location' is already taken care of by transfer
         -- but for the containers in return, ranging is not done. RANGE_CONTAINERS function will fetch
         -- containers that are not ranged to return location and creates new item-loc
         -- combinations with ranged_ind same as that of the contents.
         if RANGE_CONTAINERS(O_error_message,
                             I_return_rec) = FALSE then
            return FALSE;
         end if;
         
         -- Calculate totals after the update operation.
         if WF_RETURN_SQL.GET_TOTALS(O_error_message,
                                     L_total_return_amt,
                                     L_total_restock_amt,
                                     L_total_net_return_amt,
                                     L_rma_no) = FALSE then
            return FALSE;
         end if;

         L_table := 'WF_RETURN_HEAD';
         open C_LOCK_WF_RETURN_HEAD;
         close C_LOCK_WF_RETURN_HEAD;
         
         -- Update totals in the header.
         update wf_return_head
            set total_return_amt       = L_total_return_amt,
                total_restock_amt      = L_total_restock_amt,
                total_net_return_amt   = L_total_net_return_amt
          where rma_no = L_rma_no;

      end if;
      
      -- Modify header values of franchise return.
      if MODIFY_HEADER(O_error_message,
                       I_return_rec) = FALSE then
         return FALSE;
      end if;
      
   elsif I_action_type = RMS_CONSTANTS.WF_ACTION_DELETE then     -- Delete franchise return completely
      
      -- Deletes franchise return from WF_RETURN_HEAD and WF_RETURN_DETAIL tables.
      if WF_RETURN_SQL.DELETE(O_error_message,
                              I_return_rec.rma_no) = FALSE then
         return FALSE;
      end if;
         
   elsif I_action_type = RMS_CONSTANTS.WF_ACTION_CANCEL then     -- Cancel a franchise return completely.
      
      -- Sets the status of the return to 'Cancelled' and assigns zero to returned_qty field of all items in the return.
      if CANCEL_RETURN(O_error_message,
                       I_return_rec) = FALSE then
         return FALSE;
      end if;
   end if;
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            L_rma_no,
                                            L_program);
      return FALSE;
      
   when OTHERS then
      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_F_RETURN;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_F_RETURN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_return_rec      IN       OBJ_F_RETURN_HEAD_REC,
                           I_action_type     IN       VARCHAR2)
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.VALIDATE_F_RETURN';
   
   L_exists             BOOLEAN;
   L_loc_type           STORE.STORE_TYPE%TYPE;
   L_exist_store        BOOLEAN;
   L_store_row          STORE%ROWTYPE;
   L_ret_curr_status    WF_RETURN_HEAD.STATUS%TYPE;
   L_valid              BOOLEAN;
   L_item_master        ITEM_MASTER%ROWTYPE;
   
   -- Fetch current status of the return
   cursor C_GET_RETURN_STATUS is
      select status
        from wf_return_head
       where rma_no = NVL(I_return_rec.rma_no, -999);
   
BEGIN

-- Check for required fields in header
   
   -- RMA number is expected in the input record except in the case of new return creation.
   if I_action_type != RMS_CONSTANTS.WF_ACTION_CREATE then
      if I_return_rec.rma_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_return_rec.rma_no',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
   end if;
   
   if I_return_rec.rma_no is NOT NULL then
      open C_GET_RETURN_STATUS;
      fetch C_GET_RETURN_STATUS into L_ret_curr_status;
      close C_GET_RETURN_STATUS;
      
      -- If the given RMA number doesn't have status in WF_RETURN_HEAD table, it implies that the RMA number doesn't exist in the table.
      if L_ret_curr_status is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('FR_INV_RMA',
                                                  I_return_rec.rma_no,
                                                  NULL,
                                                  NULL);
            return FALSE;
      else  
         -- No operation can be performed on returns which are already in cancelled or closed state.
         if L_ret_curr_status in  ('C', 'D') then
            O_error_message := SQL_LIB.CREATE_MSG('FR_OP_NOT_ALLOWED',
                                                  I_return_rec.rma_no,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
          
         -- Returns in in-progress, cancelled, and closed state can not be cancelled.
         if I_action_type = RMS_CONSTANTS.WF_ACTION_CANCEL and L_ret_curr_status = 'P' then
            O_error_message := SQL_LIB.CREATE_MSG('FR_CANNOT_CANCEL',
                                                  I_return_rec.rma_no,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;
   end if;
   
   if I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE then
      
      -- Return type is mandatory to create a franchise return
      if I_return_rec.return_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_return_rec.return_type',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      
      -- Only franchise returns of type 'Externally Generated' and 'Auto' can be created using this package.
      if I_return_rec.return_type NOT in ('A', 'X') then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                               'I_return_rec.return_type',
                                               I_return_rec.return_type,
                                               NULL);
         return FALSE;
      end if;
      
      -- Return method is mandatory to create a franchise return
      if I_return_rec.return_method is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_return_rec.return_method',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      
      -- All externally generated or Auto type returns can be only 'Return to Location' type.
      if I_return_rec.return_method != 'R' then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                               'I_return_rec.return_method',
                                               I_return_rec.return_method,
                                               NULL);
         return FALSE;
      end if;
      
      -- Franchise location is mandatory to create a franchise return
      if I_return_rec.customer_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_return_rec.customer_loc',
                                               L_program,
                                               NULL);
         return FALSE;
      else
         --
         if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                     L_exist_store,
                                     L_store_row,
                                     I_return_rec.customer_loc) = FALSE then
            return FALSE;
         end if;
         --
         if L_exist_store = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         -- From location of a franchise return must be a franchise store.
         elsif L_store_row.store_type != 'F' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_WF_STORE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;
      
      if I_return_rec.return_loc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_return_rec.return_loc_type',
                                               L_program,
                                               NULL);
         return FALSE;
      else
         -- Return to location of a franchise location must be a store or warehouse
         if I_return_rec.return_loc_type NOT in ('S', 'W') then
            O_error_message := SQL_LIB.CREATE_MSG('LOC_TYPE_NOT_VALID',
                                                  I_return_rec.return_loc_type,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
         
         -- Non stock holding franchise store can return only to a warehouse.
         if L_store_row.stockholding_ind = 'N' and I_return_rec.return_loc_type = 'S' then
            O_error_message := SQL_LIB.CREATE_MSG('RETURN_NOT_ALLOWED',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;
      
      if I_return_rec.return_loc_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_return_rec.return_loc_id',
                                               L_program,
                                               NULL);
         return FALSE;
      elsif I_return_rec.return_loc_type = 'S' then
         --
         if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                     L_exist_store,
                                     L_store_row,
                                     I_return_rec.return_loc_id) = FALSE then
            return FALSE;
         end if;
         --
         if L_exist_store = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         -- Return to location of a franchise return can't be a non stockholding company store.
         elsif L_store_row.stockholding_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('FR_SH_COMP_STORE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;         
      
      if I_return_rec.currency_code is NOT NULL then
         if CURRENCY_SQL.EXIST(O_error_message,
                               I_return_rec.currency_code,
                               L_exists) = FALSE then
            return FALSE;
         end if;
         if L_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('INV_CURR_CURR',
                                                  I_return_rec.currency_code,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;
            
   end if;
   
   if I_action_type = RMS_CONSTANTS.WF_ACTION_CANCEL and I_return_rec.cancel_reason is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'Cancel Reason',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_action_type in (RMS_CONSTANTS.WF_ACTION_CREATE, RMS_CONSTANTS.WF_ACTION_UPDATE) then
      if I_return_rec.status is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'Return Status',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      
      -- Returns can be created only with INPUT or APPROVED status.
      if I_return_rec.status NOT in ('I','A','P') or 
         (I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE and I_return_rec.status NOT in ('I','A')) then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                               'Return Status',
                                               I_return_rec.status,
                                               NULL);
         return FALSE;
      end if;
      
      -- Return details must exist in the input record for create and update actions.
      if I_return_rec.f_return_details is NULL or I_return_rec.f_return_details.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('DTLS_MUST_EXIST',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;
      
-- Check for required fields in detail records

   if I_action_type in (RMS_CONSTANTS.WF_ACTION_CREATE, RMS_CONSTANTS.WF_ACTION_UPDATE) and I_return_rec.f_return_details is NOT NULL and
      I_return_rec.f_return_details.COUNT > 0 then
      FOR i in 1..I_return_rec.f_return_details.COUNT LOOP
         if I_return_rec.f_return_details(i).item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_return_rec.f_return_details('||i||').item',
                                                  L_program,
                                                  NULL);
            return FALSE;
         end if;
         
         if I_return_rec.f_return_details(i).returned_qty is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_return_rec.f_return_details('||i||').returned_qty',
                                                  L_program,
                                                  NULL);
            return FALSE;
         else
            --  Return quantity can't be less than zero
            if I_return_rec.f_return_details(i).returned_qty < 0 then
               O_error_message := SQL_LIB.CREATE_MSG('QTY_GREATER_ZERO',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;
         end if;
         
         if I_return_rec.f_return_details(i).return_reason is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_return_rec.f_return_details('||i||').return_reason',
                                                  L_program,
                                                  NULL);
            return FALSE;
         end if;      
         
      END LOOP;
   end if;
   
   return TRUE;
 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_F_RETURN;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN IS
   
   L_program         VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.CREATE_HEADER';
   L_rma_no          WF_RETURN_HEAD.RMA_NO%TYPE;
   L_currency_code   CURRENCIES.CURRENCY_CODE%TYPE := NULL;
   
   cursor C_LOC_CURR is
      select currency_code
        from store
       where store = I_return_rec.customer_loc;
   
BEGIN
   -- Generate the next valid RMA number from sequence.
   if WF_RETURN_SQL.NEXT_RMA_NUMBER(O_error_message,
                                    L_rma_no) =  FALSE then
      return FALSE;
   end if;
   
   open C_LOC_CURR;
   fetch C_LOC_CURR into L_currency_code;
   close C_LOC_CURR;
   
   insert into wf_return_head(rma_no,
                              cust_ret_ref_no,
                              status,
                              return_type,
                              customer_loc,
                              return_loc_type,
                              return_loc_id,
                              return_method,
                              currency_code,
                              cancel_reason,
                              total_return_amt,
                              total_restock_amt,
                              total_net_return_amt,
                              comments,
                              create_datetime,
                              create_id,
                              last_update_datetime,
                              last_update_id)
                       values(L_rma_no,
                              I_return_rec.cust_ret_ref_no,
                              I_return_rec.status,
                              I_return_rec.return_type,
                              I_return_rec.customer_loc,
                              I_return_rec.return_loc_type,
                              I_return_rec.return_loc_id,
                              I_return_rec.return_method,
                              NVL(I_return_rec.currency_code, L_currency_code), -- If currency is not present in input, use franchise store's currency as return currency
                              I_return_rec.cancel_reason,
                              NULL,     -- Return totals will be calculated and updated after creating return details.
                              NULL,
                              NULL,
                              I_return_rec.comments,
                              sysdate,
                              LP_user,
                              sysdate,
                              LP_user);
                              
   -- RMA number will be returned to the calling function and the RMA number will be updated in the linked transfer.                           
   O_rma_no := L_rma_no;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_LOC_CURR%ISOPEN then
         close C_LOC_CURR;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_HEADER;
---------------------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN IS
   
   L_program       VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.MODIFY_HEADER';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   L_table         VARCHAR2(30);
   L_rma_no        WF_RETURN_HEAD.RMA_NO%TYPE := I_return_rec.rma_no;

   cursor C_LOCK_WF_RETURN_HEAD is
      select 'X'
        from wf_return_head
       where rma_no = L_rma_no
         for update nowait;
         
BEGIN

   L_table := 'WF_RETURN_HEAD';
   open C_LOCK_WF_RETURN_HEAD;
   close C_LOCK_WF_RETURN_HEAD;
   
   -- Only return status and comments can be updated
   update wf_return_head
      set status               = I_return_rec.status,
          comments             = NVL(I_return_rec.comments, comments),
          last_update_datetime = sysdate,
          last_update_id       = LP_user
    where rma_no = L_rma_no;

   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            L_rma_no,
                                            L_program);
      return FALSE;

   when OTHERS then
      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MODIFY_HEADER;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CANCEL_RETURN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN IS
   
   L_program       VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.CANCEL_RETURN';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   L_table         VARCHAR2(30);
   L_rma_no        WF_RETURN_HEAD.RMA_NO%TYPE := I_return_rec.rma_no;
   
   cursor C_LOCK_WF_RETURN_HEAD is
      select 'X'
        from wf_return_head
       where rma_no = L_rma_no
         for update nowait;
            
BEGIN
   L_table := 'WF_RETURN_HEAD';
   open C_LOCK_WF_RETURN_HEAD;
   close C_LOCK_WF_RETURN_HEAD;
   
   -- If a transfer is deleted through XTSF, transfer's status will be set to 'Deleted'. If there is a linked franchise
   -- return, the return needs to be 'Cancelled'.
   update wf_return_head
      set status               = 'C',
          cancel_reason        = I_return_rec.cancel_reason,
          comments             = NVL(I_return_rec.comments, comments),
          last_update_datetime = sysdate,
          last_update_id       = LP_user
    where rma_no = L_rma_no;
    
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            L_rma_no,
                                            L_program);
      return FALSE;

   when OTHERS then
      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CANCEL_RETURN;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC,
                       I_action_type     IN       VARCHAR2)
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.CREATE_DETAIL';
   
   L_cre_f_ret_detail    OBJ_F_RETURN_DETAIL_TBL    := OBJ_F_RETURN_DETAIL_TBL();
   L_av_cost             ITEM_LOC_SOH.AV_COST%TYPE  := 0;
   L_av_cost_ret_curr    ITEM_LOC_SOH.AV_COST%TYPE  := 0;
   L_return_status       WF_RETURN_HEAD.STATUS%TYPE := 'I';
   L_customer_loc        WF_RETURN_HEAD.CUSTOMER_LOC%TYPE;
   L_cust_loc_currency   STORE.CURRENCY_CODE%TYPE;
   L_return_currency     WF_RETURN_HEAD.CURRENCY_CODE%TYPE;
   L_dept                ITEM_MASTER.DEPT%TYPE;
   L_class               ITEM_MASTER.CLASS%TYPE;
   L_subclass            ITEM_MASTER.SUBCLASS%TYPE;
   
   -- Fetches details to be added to franchise return
   cursor C_CREATE_DETAIL_RECS is
      select OBJ_F_RETURN_DETAIL_REC(fr.rma_no,
                                     fr.item,
                                     fr.wf_order_no,
                                     fr.returned_qty,
                                     fr.return_reason,
                                     fr.return_unit_cost,
                                     fr.cancel_reason,
                                     fr.restock_type,
                                     fr.unit_restock_fee)
        from table(cast(I_return_rec.f_return_details as OBJ_F_RETURN_DETAIL_TBL)) fr
       where not exists (select 1
                           from wf_return_detail wrd
                          where wrd.rma_no = I_rma_no
                            and wrd.item   = fr.item);
   
   -- Fetches the current status of franchise return.
   cursor C_RET_CURR_STATUS is
      select status
        from wf_return_head
       where rma_no = I_rma_no;

   -- Fetch currencies of franchise store and franchise return.
   cursor C_GET_CURRENCIES is
      select s.currency_code   store_currency,
             wrh.currency_code return_currency
        from store          s,
             wf_return_head wrh
       where s.store    = L_customer_loc
         and wrh.rma_no = I_rma_no;
   
   -- To fetch department, class and subclass of an item.
   cursor C_GET_ITEM_HIER(CV_item ITEM_MASTER.ITEM%TYPE) is
      select dept,
             class,
             subclass
        from item_master
       where item = CV_item;
                                  
BEGIN
   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NULL_RMA_NO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   if I_action_type = RMS_CONSTANTS.WF_ACTION_UPDATE then
      open C_RET_CURR_STATUS;
      fetch C_RET_CURR_STATUS into L_return_status;
      close C_RET_CURR_STATUS;
   end if;
   
   open C_CREATE_DETAIL_RECS;
   fetch C_CREATE_DETAIL_RECS BULK COLLECT into L_cre_f_ret_detail;
   close C_CREATE_DETAIL_RECS;

   L_customer_loc := I_return_rec.customer_loc; 
   
   open C_GET_CURRENCIES;
   fetch C_GET_CURRENCIES into L_cust_loc_currency,
                               L_return_currency;
   close C_GET_CURRENCIES;

   if L_cre_f_ret_detail is NOT NULL then
      FOR i in 1..L_cre_f_ret_detail.COUNT LOOP
         if L_cre_f_ret_detail(i).return_unit_cost is NULL then
            -- If return unit cost is not present in input record, use the WAC of the item at customer location.
            
            open C_GET_ITEM_HIER(L_cre_f_ret_detail(i).item);
            fetch C_GET_ITEM_HIER into L_dept,
                                       L_class,
                                       L_subclass;
            close C_GET_ITEM_HIER;
            
            if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                          L_av_cost,
                                          L_cre_f_ret_detail(i).item,
                                          L_dept,
                                          L_class,
                                          L_subclass,
                                          I_return_rec.customer_loc,
                                          'S',
                                          NULL,
                                          NULL,
                                          NULL) =  FALSE then
               return FALSE;
            end if;
            
            L_cre_f_ret_detail(i).return_unit_cost := L_av_cost;

            -- Convert the average cost to return currency
            if L_cust_loc_currency <> L_return_currency then
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_av_cost,                   -- Input cost
                                       L_cust_loc_currency,         -- From currency
                                       L_return_currency,           -- To currency
                                       L_av_cost_ret_curr,          -- Converted value
                                       'C',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL) = FALSE then
                  return FALSE;
               end if;
               
               L_cre_f_ret_detail(i).return_unit_cost := L_av_cost_ret_curr;
            end if;

            
         end if;
      END LOOP;
   end if;
   
   if L_cre_f_ret_detail is NOT NULL then
      FORALL i in 1..L_cre_f_ret_detail.COUNT
         insert into wf_return_detail(rma_no,
                                      item,
                                      wf_order_no,
                                      returned_qty,
                                      return_reason,
                                      return_unit_cost,
                                      cancel_reason,
                                      restock_type,
                                      unit_restock_fee,
                                      net_return_unit_cost,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
                               values(I_rma_no,
                                      L_cre_f_ret_detail(i).item,
                                      L_cre_f_ret_detail(i).wf_order_no,
                                      DECODE(L_return_status, 'I', L_cre_f_ret_detail(i).returned_qty, 0),
                                      L_cre_f_ret_detail(i).return_reason,
                                      L_cre_f_ret_detail(i).return_unit_cost,
                                      L_cre_f_ret_detail(i).cancel_reason,
                                      L_cre_f_ret_detail(i).restock_type,
                                      L_cre_f_ret_detail(i).unit_restock_fee,
                                      L_cre_f_ret_detail(i).return_unit_cost,    -- Net return unit cost is same as return unit cost because restock amount is zero for EG and Auto returns.
                                      sysdate,
                                      LP_user,
                                      sysdate,
                                      LP_user);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_RET_CURR_STATUS%ISOPEN then
         close C_RET_CURR_STATUS;
      end if;
      
      if C_CREATE_DETAIL_RECS%ISOPEN then
         close C_CREATE_DETAIL_RECS;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_DETAIL;
---------------------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.MODIFY_DETAIL';
   
   L_upd_f_ret_detail       OBJ_F_RETURN_DETAIL_TBL := OBJ_F_RETURN_DETAIL_TBL();
   L_rma_no                 WF_RETURN_HEAD.RMA_NO%TYPE := I_return_rec.rma_no;
   L_ret_curr_status        WF_RETURN_HEAD.STATUS%TYPE;
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked,-54);
   L_table                  VARCHAR2(30) := 'WF_RETURN_DETAIL';   
   
   cursor C_RETURN_CURRENT_STATUS is
      select status
        from wf_return_head
       where rma_no = L_rma_no;
   
   -- Fetches details to be modified in a franchise return
   cursor C_UPDATE_DETAIL_RECS is
      select OBJ_F_RETURN_DETAIL_REC(fr.rma_no,
                                     wrd.item,
                                     fr.wf_order_no,
                                     DECODE(L_ret_curr_status, 'I', fr.returned_qty, NULL),  -- Return Qty can be updated only if return is in INPUT state.
                                     DECODE(L_ret_curr_status, 'I', fr.return_reason, NULL),   -- Return status can be updated only if return is in INPUT state.
                                     DECODE(L_ret_curr_status, 'I', fr.return_unit_cost, NULL),  -- Return unit cost can be updated only if return is in INPUT state.
                                     NULL,
                                     fr.restock_type,
                                     fr.unit_restock_fee)
        from table(cast(I_return_rec.f_return_details as OBJ_F_RETURN_DETAIL_TBL)) fr,
             wf_return_detail wrd
       where wrd.rma_no = L_rma_no
         and wrd.item   = fr.item
         for update of wrd.item nowait;
         
BEGIN
   open C_RETURN_CURRENT_STATUS;
   fetch C_RETURN_CURRENT_STATUS into L_ret_curr_status;
   close C_RETURN_CURRENT_STATUS;
   
   open C_UPDATE_DETAIL_RECS;
   fetch C_UPDATE_DETAIL_RECS BULK COLLECT into L_upd_f_ret_detail;
   close C_UPDATE_DETAIL_RECS;
   
   if L_upd_f_ret_detail is NOT NULL then
   
      FORALL i in 1..L_upd_f_ret_detail.COUNT
         update wf_return_detail
            set returned_qty         = NVL(L_upd_f_ret_detail(i).returned_qty, returned_qty),
                return_reason        = NVL(L_upd_f_ret_detail(i).return_reason, return_reason),
                return_unit_cost     = NVL(L_upd_f_ret_detail(i).return_unit_cost, return_unit_cost),
                last_update_datetime = sysdate,
                last_update_id       = LP_user
          where rma_no = L_rma_no
            and item   = L_upd_f_ret_detail(i).item;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_RETURN_CURRENT_STATUS%ISOPEN then
         close C_RETURN_CURRENT_STATUS;
      end if;
      
      if C_UPDATE_DETAIL_RECS%ISOPEN then
         close C_UPDATE_DETAIL_RECS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            L_rma_no,
                                            L_program);
      return FALSE;

   when OTHERS then
      if C_RETURN_CURRENT_STATUS%ISOPEN then
         close C_RETURN_CURRENT_STATUS;
      end if;
      
      if C_UPDATE_DETAIL_RECS%ISOPEN then
         close C_UPDATE_DETAIL_RECS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MODIFY_DETAIL;
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.DELETE_DETAIL';
   
   L_del_f_ret_detail   OBJ_F_RETURN_DETAIL_TBL    := OBJ_F_RETURN_DETAIL_TBL();
   L_rma_no             WF_RETURN_HEAD.RMA_NO%TYPE := I_return_rec.rma_no;
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked,-54);
   L_table              VARCHAR2(30);
      
   -- Fetches details to be deleted from franchise return
   cursor C_DELETE_DETAIL_RECS is
      select OBJ_F_RETURN_DETAIL_REC(wrd.rma_no,
                                     wrd.item,
                                     wrd.wf_order_no,
                                     wrd.returned_qty,
                                     wrd.return_reason,
                                     wrd.return_unit_cost,
                                     wrd.cancel_reason,
                                     wrd.restock_type,
                                     wrd.unit_restock_fee)
        from wf_return_head wrh,
             wf_return_detail wrd,
             item_master im
       where wrh.rma_no = L_rma_no
         and wrh.rma_no = wrd.rma_no
         and im.item    = wrd.item
         and NVL(im.deposit_item_type,'0') <> 'A'    -- Deposit containers will not be selected. SYNC_DEPOSIT_ITEMS funciton will delete containers.
         and not exists (select 1
                           from table(cast(I_return_rec.f_return_details as OBJ_F_RETURN_DETAIL_TBL)) fr
                          where wrd.rma_no = L_rma_no
                            and wrd.item   = fr.item)
         for update of wrd.item nowait;
   
BEGIN
   
   L_table := 'WF_RETURN_DETAIL';
   
   open C_DELETE_DETAIL_RECS;
   fetch C_DELETE_DETAIL_RECS BULK COLLECT into L_del_f_ret_detail;
   close C_DELETE_DETAIL_RECS;
   
   if L_del_f_ret_detail is NOT NULL then   
      FORALL i in 1..L_del_f_ret_detail.COUNT
         delete
           from wf_return_detail
          where rma_no = L_rma_no
            and item   = L_del_f_ret_detail(i).item;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_DELETE_DETAIL_RECS%ISOPEN then
         close C_DELETE_DETAIL_RECS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            L_rma_no,
                                            L_program);
      return FALSE;

   when OTHERS then
      if C_DELETE_DETAIL_RECS%ISOPEN then
         close C_DELETE_DETAIL_RECS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_DETAIL;
---------------------------------------------------------------------------------------------------------------------
FUNCTION RANGE_CONTAINERS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_return_rec      IN       OBJ_F_RETURN_HEAD_REC)
   RETURN BOOLEAN IS
   
   L_program           VARCHAR2(50) := 'WF_CREATE_RETURN_SQL.RANGE_CONTAINERS';
   L_rma_no            WF_RETURN_HEAD.RMA_NO%TYPE;
   L_return_loc        WF_RETURN_HEAD.RETURN_LOC_ID%TYPE;
   L_return_loc_type   WF_RETURN_HEAD.RETURN_LOC_TYPE%TYPE;
   
   cursor C_CONTAINERS_TO_RANGE is
      select im.container_item  item,
             -- If 2 contents have same container, one content has ranged_ind='N' and the other has 'Y', then 'Y' will be used for container
             max(il.ranged_ind) ranged_ind   
        from wf_return_detail wrd,
             item_master      im,
             item_loc         il
       where wrd.rma_no  = L_rma_no
         and wrd.item    = im.item
         and im.item     = il.item
         and il.loc      = L_return_loc
         and il.loc_type = L_return_loc_type
         and NVL(im.deposit_item_type,'0') = 'E'
         and im.container_item is NOT NULL
         and not exists (select 1
                           from item_loc il2
                          where il2.loc      = L_return_loc
                            and il2.loc_type = L_return_loc_type
                            and il2.item     = im.container_item)
    group by im.container_item;
   
BEGIN

   L_rma_no          := I_return_rec.rma_no;
   L_return_loc      := I_return_rec.return_loc_id;
   L_return_loc_type := I_return_rec.return_loc_type;
   
   FOR rec in C_CONTAINERS_TO_RANGE LOOP
      -- Create new item location combinations for the identified items.
      if NEW_ITEM_LOC(O_error_message,
                      rec.item,
                      L_return_loc,
                      NULL,
                      NULL,
                      L_return_loc_type,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'N',
                      NULL,
                      NULL,
                      NULL,
                      rec.ranged_ind) = FALSE then   -- Ranged_ind
         return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RANGE_CONTAINERS;
---------------------------------------------------------------------------------------------------------------------   
END WF_CREATE_RETURN_SQL;
/