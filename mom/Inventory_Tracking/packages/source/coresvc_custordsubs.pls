CREATE OR REPLACE PACKAGE CORESVC_CUSTORDSUB AUTHID CURRENT_USER AS                
-------------------------------------------------------------------------------------------
--Function Name: CREATE_CO_SUBSTITUTE
--Purpose:       This public function will be called from the 
--               service provider for customer order substitution web service.              
--Called by:     
------------------------------------------------------------------------------------------
FUNCTION CREATE_CO_SUBSTITUTE(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id       IN        SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                              I_chunk_id         IN        SVC_CUSTORDSUB.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
END CORESVC_CUSTORDSUB;
/