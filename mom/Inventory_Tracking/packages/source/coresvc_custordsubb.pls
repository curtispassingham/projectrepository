create or replace PACKAGE BODY CORESVC_CUSTORDSUB AS

LP_custordsub_complete    SVC_CUSTORDSUB.PROCESS_STATUS%TYPE := 'C';
LP_custordsub_error       SVC_CUSTORDSUB.PROCESS_STATUS%TYPE := 'E';
LP_custordsub_validate    SVC_CUSTORDSUB.PROCESS_STATUS%TYPE := 'V';
LP_custordsub_new         SVC_CUSTORDSUB.PROCESS_STATUS%TYPE := 'N';
LP_user                   SVCPROV_CONTEXT.USER_NAME%TYPE     := GET_USER;

-----------------------------------------------------------------------------------------------
---                                   PRIVATE FUNCTION                                     ----
-----------------------------------------------------------------------------------------------
--Function Name: VALIDATE_SUBSTITUTION
--Purpose:       This private function will do the validation of the data in the staging tables 
--               for customer order substitution. For every error, it will update the error_msg 
--               in the staging table. For multiple errors in the same row, it will use the 
--               delimiter ";". If there is any error in the validation for any header or detail 
--               record for the input process_id and chunk id, it will return failure.
--Called by:     CREATE_CO_SUBSTITUTE
------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SUBSTITUTION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_CUSTORDSUB.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: CREATE_SUBSTITUTION_REC
--Purpose:       This private function will build the collection object for ordcust_detail 
--               insert/update and item_loc_soh update.
--               1)For ordcust_detail table, this function will build the collection which will 
--                  be used to update the ordcust_detail table to reflect the cancelled quantity 
--                  for the original item. It will also create the collection for the substitute 
--                  item which is based on the ordcust_detail record for the original item.
--               2)For item_loc_soh table, the collection to update the customer reserve quantity
--                  is built based on the changes to the customer reservation for the original
--                  item and the substitute item. 
--               3)For substitute item not ranged to the fulfilled location, a collection will be 
--                  populated which will be used to range them. 
--Called by:        CREATE_CO_SUBSTITUTE
-------------------------------------------------------------------------------------------
FUNCTION CREATE_SUBSTITUTION_REC(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_ordcust_detail_tbl  IN OUT  ORDCUST_DETAIL_TBL,
                                 O_cust_ord_resv_tbl   IN OUT  CUST_ORD_RESERVE_TBL,
                                 O_new_item_loc_tbl    IN OUT  OBJ_ITEMLOC_TBL,
                                 I_process_id          IN      SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                                 I_chunk_id            IN      SVC_CUSTORDSUB.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: PERSIST_SUBSTITUTION
--Purpose:       This function will persist the collections into ordcust_detail, item_loc_soh and
--               range the missing item/location.
--               1.) Call NEW_ITEM_LOC() to do missing item/location ranging for substitute item
--               2.) Update existing record in ordcust_detail, the qty cancelled (original item) or 
--                   qty ordered (substitute item for multiple calls) will be incremented. 
--                   Insert new row for new substitute request.
--               3.) Call CUSTOMER_RESERVE_SQL to update the item_loc_soh table. 
--Called by:     CREATE_CO_SUBSTITUTE
------------------------------------------------------------------------------------------
FUNCTION PERSIST_SUBSTITUTION(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_ordcust_detail_tbl IN     ORDCUST_DETAIL_TBL,
                              I_cust_ord_resv_tbl  IN     CUST_ORD_RESERVE_TBL,
                              I_new_item_loc_tbl   IN     OBJ_ITEMLOC_TBL)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
---                                   PUBLIC FUNCTION                                      ----
-----------------------------------------------------------------------------------------------
FUNCTION CREATE_CO_SUBSTITUTE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id     IN      SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                              I_chunk_id       IN      SVC_CUSTORDSUB.CHUNK_ID%TYPE)

RETURN BOOLEAN IS

   L_program               VARCHAR2(64)            := 'CORESVC_CUSTORDSUB.CREATE_CO_SUBSTITUTE';
   L_table                 VARCHAR2(50);
   L_ordcust_detail_tbl    ORDCUST_DETAIL_TBL      := ORDCUST_DETAIL_TBL();
   L_cust_ord_resv_tbl     CUST_ORD_RESERVE_TBL    := CUST_ORD_RESERVE_TBL();
   L_new_item_loc_tbl      OBJ_ITEMLOC_TBL         := OBJ_ITEMLOC_TBL();
   
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_SVC_CUSTORDSUB is
      select 'x'
        from svc_custordsub
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;

   cursor C_LOCK_SVC_CUSTORDSUBDTL is
      select 'x'
        from svc_custordsubdtl
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;
BEGIN

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chunk_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   -- Get lock for the staging tables.
   
   L_table := 'SVC_CUSTORDSUB';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SVC_CUSTORDSUB',L_table,NULL);
   open C_LOCK_SVC_CUSTORDSUB;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SVC_CUSTORDSUB',L_table,NULL);
   close C_LOCK_SVC_CUSTORDSUB;

   L_table := 'SVC_CUSTORDSUBDTL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SVC_CUSTORDSUBDTL',L_table,NULL);
   open C_LOCK_SVC_CUSTORDSUBDTL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SVC_CUSTORDSUBDTL',L_table,NULL);
   close C_LOCK_SVC_CUSTORDSUBDTL;

   if VALIDATE_SUBSTITUTION(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if CREATE_SUBSTITUTION_REC(O_error_message,
                              L_ordcust_detail_tbl,
                              L_cust_ord_resv_tbl,
                              L_new_item_loc_tbl,
                              I_process_id,
                              I_chunk_id) = FALSE  then
      return FALSE;
   end if;

   if PERSIST_SUBSTITUTION(O_error_message,
                           L_ordcust_detail_tbl,
                           L_cust_ord_resv_tbl,
                           L_new_item_loc_tbl) = FALSE then
     return FALSE;
   end if;

   -- Update the process status to Completed status for the validated and processed record.
   update svc_custordsub
      set process_status = LP_custordsub_complete,
          last_update_datetime = sysdate,
          last_update_id = LP_user
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status = LP_custordsub_validate;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SVC_CUSTORDSUB%ISOPEN then
         close C_LOCK_SVC_CUSTORDSUB;
      end if;
      if C_LOCK_SVC_CUSTORDSUBDTL%ISOPEN then
         close C_LOCK_SVC_CUSTORDSUBDTL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;
   when OTHERS then
      if C_LOCK_SVC_CUSTORDSUB%ISOPEN then
         close C_LOCK_SVC_CUSTORDSUB;
      end if;
      if C_LOCK_SVC_CUSTORDSUBDTL%ISOPEN then
         close C_LOCK_SVC_CUSTORDSUBDTL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     return FALSE;

END CREATE_CO_SUBSTITUTE;
-----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SUBSTITUTION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_CUSTORDSUB.CHUNK_ID%TYPE)

RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'CORESVC_CUSTORDSUB.VALIDATE_SUBSTITUTION';

   -- Cursor C_SVC_CUSTORDSUB to identify the row in header table which have error
      -- Customer order and Fulfillment number combination is not present in ordcust table for reservation leg  
      -- Location id is not same as the original fulfill location
      -- Location type is not a store
      -- Original item is  not present in ordcust_detail table 
      -- Original item ordered quantity is negative
      -- Original item cancelled quantity is greater than the outstanding quantity.   
      -- UOM is not the same as the uom present in ordcust_detail table.  
      -- Duplicate request 

   cursor C_SVC_CUSTORDSUB is
      select svc.custordsub_id,
             svc.customer_order_no,
             oc.customer_order_no   oc_customer_order_no,
             svc.fulfill_order_no,
             svc.loc_id,
             oc.fulfill_loc_id      oc_loc_id,
             svc.loc_type,
             svc.item,
             ocd.item               ocd_item,
             svc.item_qty,
             ocd.qty_outstanding,
             svc.qty_uom,
             ocd.standard_uom       ocd_qty_uom,
             (select count(*)
                from svc_custordsub svc2
               where svc2.process_id = I_process_id
                 and svc2.chunk_id = I_chunk_id
                 and svc.process_status = LP_custordsub_new
                 and svc2.customer_order_no = svc.customer_order_no
                 and svc2.fulfill_order_no = svc.fulfill_order_no
                 and svc2.item = svc.item
              group by svc2.customer_order_no, svc2.fulfill_order_no, svc2.item) count_rows,
             svc.error_msg
        from svc_custordsub svc,
             (select oc.customer_order_no,
                     oc.fulfill_order_no,
                     oc.fulfill_loc_id
                from ordcust oc
               where oc.source_loc_type IS NULL
                 and oc.fulfill_loc_type = 'S') oc,
             (select oc.customer_order_no,
                     oc.fulfill_order_no,
                     ocd.item,
                     (ocd.qty_ordered_suom - NVL(qty_cancelled_suom,0))    qty_outstanding,
                     ocd.standard_uom
                from ordcust oc,
                     ordcust_detail ocd
               where oc.source_loc_type IS NULL
                 and oc.fulfill_loc_type = 'S'
                 and oc.ordcust_no = ocd.ordcust_no
                 and ocd.original_item is NULL) ocd
       where svc.process_id = I_process_id
         and svc.chunk_id = I_chunk_id
         and svc.process_status = LP_custordsub_new
         and svc.customer_order_no = oc.customer_order_no(+)
         and svc.fulfill_order_no = oc.fulfill_order_no(+)
         and svc.customer_order_no = ocd.customer_order_no(+)
         and svc.fulfill_order_no = ocd.fulfill_order_no(+)
         and svc.item = ocd.item(+)
         and (    svc.item_qty < 0                                            -- Negative ordered quantity
              or (svc.qty_uom = 'EA' and ROUND(svc.item_qty) <> svc.item_qty) -- decimal quantity for UOM type as EA
              or  svc.loc_type <> 'S'                                        -- Substitute supported only for Stores.
              or  oc.customer_order_no is NULL                             -- Invalid Customer fulfillment request
              or  (oc.customer_order_no is NOT NULL and oc.fulfill_loc_id <> svc.loc_id)   -- Invalid location
              or  (oc.customer_order_no is NOT NULL and ocd.item is NULL)  -- Invalid Item
              or  (     ocd.item is NOT NULL
                   and  (    ocd.standard_uom <> svc.qty_uom               -- Invalid UOM
                         or  ocd.qty_outstanding < svc.item_qty))          -- Invalid Quantity
              or  exists (select 1 
                            from svc_custordsub svc2
                           where svc2.process_id = I_process_id
                             and svc2.chunk_id = I_chunk_id
                             and svc.process_status = LP_custordsub_new
                             and svc2.customer_order_no = svc.customer_order_no
                             and svc2.fulfill_order_no = svc.fulfill_order_no
                             and svc2.item = svc.item
                           group by svc2.customer_order_no, svc2.fulfill_order_no, svc2.item
                          having count(*) > 1));                           -- Duplicate records                                 

   -- Cursor C_SVC_CUSTORDSUBDTL to identify the row in detail table which have error
      -- Substitute item does not exists in item_master table
      -- Substitute item is not approved, sellable, inventory, transactional level item
      -- Substitute item is either catchweight or concession or consignment or transformable item
      -- Substitute item is deposit (other than deposit content)
      -- Substitute item UOM is not standard UOM from item_master
      -- Substitute item quantity is negative.
      -- Duplicate request for substitute item.
      
   cursor C_SVC_CUSTORDSUBDTL is
      select svc.custordsub_id,
             svc.customer_order_no,
             svc.fulfill_order_no,
             svcd.sub_item,
             svcd.sub_item_qty,
             svcd.sub_qty_uom,
             im.item,
             im.standard_uom,
             im.tran_level,
             im.item_level,
             im.status,
             im.sellable_ind,
             im.inventory_ind,
             im.catch_weight_ind,
             im.item_xform_ind,
             im.deposit_item_type,
             d.purchase_type,
             (select count(*) 
                from svc_custordsubdtl svcd2
               where svcd2.custordsub_id = svcd.custordsub_id
                 and svcd2.sub_item = svcd.sub_item
              group by svcd2.custordsub_id, svcd2.sub_item) count_rows,
             svcd.error_msg,
             svcd.rowid
        from svc_custordsub svc,
             svc_custordsubdtl svcd,
             item_master im,
             deps d
       where svc.process_id = I_process_id
         and svc.chunk_id = I_chunk_id
         and svc.process_status = LP_custordsub_validate    -- running this post validation of header table.
         and svc.custordsub_id = svcd.custordsub_id
         and svcd.sub_item = im.item (+)
         and im.dept = d.dept(+)
         and (    svcd.sub_item_qty < 0 
              or (svcd.sub_qty_uom = 'EA' and ROUND(svcd.sub_item_qty) <> svcd.sub_item_qty)
              or  im.item is NULL
              or  (    im.item is NOT NULL                                      
                   and (   im.status <> 'A'
                        or im.sellable_ind = 'N'
                        or im.inventory_ind = 'N'
                        or im.catch_weight_ind = 'Y'
                        or im.item_xform_ind = 'Y'
                        or im.tran_level <> im.item_level
                        or d.purchase_type <> 0
                        or im.deposit_item_type in ('A', 'Z')
                        or svcd.sub_qty_uom <> im.standard_uom)
                   )
              or   exists (select 1 
                             from svc_custordsubdtl svcd2
                            where svcd2.custordsub_id = svcd.custordsub_id
                              and svcd2.sub_item = svcd.sub_item
                           group by svcd2.custordsub_id, svcd2.sub_item
                           having count(*) > 1));

   TYPE SVC_CUSTORDSUB_TBL IS TABLE OF C_SVC_CUSTORDSUB%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_custordsub_tbl  SVC_CUSTORDSUB_TBL;

   TYPE SVC_CUSTORDSUBDTL_TBL IS TABLE OF C_SVC_CUSTORDSUBDTL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_custordsubdtl_tbl  SVC_CUSTORDSUBDTL_TBL;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_SVC_CUSTORDSUB','SVC_CUSTORDSUB, ORDCUST, ORDCUST_DETAIL',NULL);
   open C_SVC_CUSTORDSUB;

   SQL_LIB.SET_MARK('FETCH','C_SVC_CUSTORDSUB','SVC_CUSTORDSUB, ORDCUST, ORDCUST_DETAIL',NULL);
   fetch C_SVC_CUSTORDSUB BULK COLLECT INTO L_svc_custordsub_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_SVC_CUSTORDSUB','SVC_CUSTORDSUB, ORDCUST, ORDCUST_DETAIL',NULL);
   close C_SVC_CUSTORDSUB;

   if L_svc_custordsub_tbl is NOT NULL and L_svc_custordsub_tbl.COUNT > 0 then
      FOR i in 1..L_svc_custordsub_tbl.COUNT LOOP
         -- Duplicate header records.
         if L_svc_custordsub_tbl(i).count_rows > 1 then
            L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                 SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_DUP_ORG_ITM',
                                                                          L_svc_custordsub_tbl(i).customer_order_no,
                                                                          L_svc_custordsub_tbl(i).fulfill_order_no,
                                                                          L_svc_custordsub_tbl(i).item)|| ';';
         end if;
            
         -- Invalid Customer order number and Fulfillment order number combination.
         if L_svc_custordsub_tbl(i).oc_customer_order_no is NULL then
            L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                 SQL_LIB.GET_MESSAGE_TEXT('INV_CUST_ORDER',
                                                                          L_svc_custordsub_tbl(i).customer_order_no,
                                                                          L_svc_custordsub_tbl(i).fulfill_order_no,
                                                                          NULL)|| ';';
         else
            
            -- Invalid Input Location - not matching with fulfill location of original reservation request
            if L_svc_custordsub_tbl(i).oc_loc_id <> L_svc_custordsub_tbl(i).loc_id then
               L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                    SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_LOC',
                                                                             L_svc_custordsub_tbl(i).customer_order_no,
                                                                             L_svc_custordsub_tbl(i).fulfill_order_no,
                                                                             L_svc_custordsub_tbl(i).loc_id)|| ';';
            end if;

            -- Invalid location type. Should be 'S'
            if L_svc_custordsub_tbl(i).loc_type <> 'S' then
               L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                    SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_LOC_TYPE',
                                                                             L_svc_custordsub_tbl(i).customer_order_no,
                                                                             L_svc_custordsub_tbl(i).fulfill_order_no,
                                                                             L_svc_custordsub_tbl(i).loc_type)|| ';';
            end if;

            -- Invalid ordered quantity. Ordered quantity should be positive.
            if L_svc_custordsub_tbl(i).item_qty < 0 then
               L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                    SQL_LIB.GET_MESSAGE_TEXT('ORDER_QTY_POSITIVE',
                                                                             L_svc_custordsub_tbl(i).item,
                                                                             L_svc_custordsub_tbl(i).customer_order_no,
                                                                             L_svc_custordsub_tbl(i).fulfill_order_no)|| ';';
            end if;

            -- For UOM type of 'EA', the quantity cannot be decimal
            if L_svc_custordsub_tbl(i).qty_uom = 'EA' and ROUND(L_svc_custordsub_tbl(i).item_qty) <> L_svc_custordsub_tbl(i).item_qty then
               L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                    SQL_LIB.GET_MESSAGE_TEXT('ORDER_QTY_DECIMAL',
                                                                             L_svc_custordsub_tbl(i).item,
                                                                             L_svc_custordsub_tbl(i).customer_order_no,
                                                                             L_svc_custordsub_tbl(i).fulfill_order_no)|| ';';
            end if;
            
            -- Invalid original item. Item is not part of original customer order.
            if L_svc_custordsub_tbl(i).ocd_item is NULL then
               L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                    SQL_LIB.GET_MESSAGE_TEXT('ITEM_NOT_EXIST_ORDDET',
                                                                             L_svc_custordsub_tbl(i).item,
                                                                             L_svc_custordsub_tbl(i).customer_order_no,
                                                                             L_svc_custordsub_tbl(i).fulfill_order_no)|| ';';
            else
               -- Invalid ordered quantity. Trying to cancel more than outstanding ordered quantity.
               if L_svc_custordsub_tbl(i).item_qty > L_svc_custordsub_tbl(i).qty_outstanding then
                  L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                       SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_ORD_QTY',
                                                                                L_svc_custordsub_tbl(i).customer_order_no,
                                                                                L_svc_custordsub_tbl(i).fulfill_order_no,
                                                                                L_svc_custordsub_tbl(i).item)|| ';';
               end if;

               -- Invalid UOM. Not matching with ordcust_detail table.
               if L_svc_custordsub_tbl(i).qty_uom <> L_svc_custordsub_tbl(i).ocd_qty_uom then
                  L_svc_custordsub_tbl(i).error_msg := L_svc_custordsub_tbl(i).error_msg ||
                                                       SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_UOM',
                                                                                L_svc_custordsub_tbl(i).customer_order_no,
                                                                                L_svc_custordsub_tbl(i).fulfill_order_no,
                                                                                L_svc_custordsub_tbl(i).item)|| ';';
               end if;

            end if;  -- Item is invalid
         end if; -- Invalid Customer fulfillment number
      END LOOP;
   end if;
   
   --In case of addition of any new validations, we need to take the substring of error message to restrict its length to 2000 characters 
   --as error_msg field in svc_custordsub can take a maximum length of 2000 characters.
   
   if L_svc_custordsub_tbl is NOT NULL and L_svc_custordsub_tbl.COUNT > 0 then
      FORALL i in 1..L_svc_custordsub_tbl.COUNT
         update svc_custordsub
            set error_msg = L_svc_custordsub_tbl(i).error_msg,
                process_status = LP_custordsub_error,
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where custordsub_id = L_svc_custordsub_tbl(i).custordsub_id;
      return FALSE;
   else
      -- For no errors, update the status to Validated for all the records.
      update svc_custordsub
         set process_status = LP_custordsub_validate,
             last_update_datetime = SYSDATE,
             last_update_id = LP_user
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and process_status = LP_custordsub_new;
   end if;

   -- Process errors for detail table.
   SQL_LIB.SET_MARK('OPEN','C_SVC_CUSTORDSUBDTL','SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ITEM_MASTER, DEPS',NULL);
   open c_svc_custordsubdtl;

   SQL_LIB.SET_MARK('FETCH','C_SVC_CUSTORDSUBDTL','SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ITEM_MASTER, DEPS',NULL);
   fetch c_svc_custordsubdtl BULK COLLECT INTO L_svc_custordsubdtl_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_SVC_CUSTORDSUBDTL','SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ITEM_MASTER, DEPS',NULL);
   close c_svc_custordsubdtl;

   if L_svc_custordsubdtl_tbl is NOT NULL and L_svc_custordsubdtl_tbl.COUNT > 0 then
         FOR i in 1..L_svc_custordsubdtl_tbl.COUNT LOOP
            
            -- Duplicate detail records.
            if L_svc_custordsubdtl_tbl(i).count_rows > 1 then
               L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                       SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_DUP_SUBS_ITM',
                                                                             L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                             L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                                             L_svc_custordsubdtl_tbl(i).sub_item)|| ';';
            end if;
            
            -- Negative substitute quantity.
            if L_svc_custordsubdtl_tbl(i).sub_item_qty < 0 then
               L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                       SQL_LIB.GET_MESSAGE_TEXT('ORDER_QTY_POSITIVE',
                                                                                L_svc_custordsubdtl_tbl(i).sub_item,
                                                                                L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                L_svc_custordsubdtl_tbl(i).fulfill_order_no)|| ';';

            end if;
            
            -- For UOM type of 'EA', the quantity cannot be decimal
            if L_svc_custordsubdtl_tbl(i).sub_qty_uom = 'EA'   
                  and ROUND(L_svc_custordsubdtl_tbl(i).sub_item_qty) <> L_svc_custordsubdtl_tbl(i).sub_item_qty then
               L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                       SQL_LIB.GET_MESSAGE_TEXT('ORDER_QTY_DECIMAL',
                                                                                L_svc_custordsubdtl_tbl(i).sub_item,
                                                                                L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                L_svc_custordsubdtl_tbl(i).fulfill_order_no)|| ';';

            end if;
            
            -- Item not present in RMS.
            if L_svc_custordsubdtl_tbl(i).item is NULL then
               L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                       SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_SUB_ITEM',
                                                                                L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                                                L_svc_custordsubdtl_tbl(i).sub_item)|| ';';

            else -- Item present in item_master but has other errors
               -- Input UOM not same as standard UOM
               if L_svc_custordsubdtl_tbl(i).sub_qty_uom <> L_svc_custordsubdtl_tbl(i).standard_uom then
                  L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                          SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_SUB_UOM',
                                                                                   L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).sub_item)|| ';';
               end if;
               
               -- Input substitute item is not approved transaction level sellable item.
               if L_svc_custordsubdtl_tbl(i).item_level <> L_svc_custordsubdtl_tbl(i).tran_level or
                  L_svc_custordsubdtl_tbl(i).status <> 'A' or L_svc_custordsubdtl_tbl(i).sellable_ind <> 'Y' or 
                  L_svc_custordsubdtl_tbl(i).inventory_ind <> 'Y' then
                  L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                          SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_NOT_TRAN_ITEM',
                                                                                   L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).sub_item)|| ';';
               end if;

               -- Input substitute item is catch weight item.
               if L_svc_custordsubdtl_tbl(i).catch_weight_ind = 'Y' then
                  L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                          SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_SUB_CW_ITEM',
                                                                                   L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).sub_item)|| ';';
               end if;

               -- Input substitute item is Transformable item.
               if L_svc_custordsubdtl_tbl(i).item_xform_ind = 'Y' then
                  L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                          SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_SUB_XFRM_ITEM',
                                                                                   L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).sub_item)|| ';';
               end if;

               -- Input substitute item is Deposit item.
               if L_svc_custordsubdtl_tbl(i).deposit_item_type = 'A' or L_svc_custordsubdtl_tbl(i).deposit_item_type = 'Z' then
                  L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                          SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_SUB_DEP_ITEM',
                                                                                   L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).sub_item)|| ';';
               end if;

               -- Input substitute item is Concession or Consignment item.
               if L_svc_custordsubdtl_tbl(i).purchase_type <> 0 then
                  L_svc_custordsubdtl_tbl(i).error_msg := L_svc_custordsubdtl_tbl(i).error_msg ||
                                                          SQL_LIB.GET_MESSAGE_TEXT('CUSTSUB_INV_SUB_CON_ITEM',
                                                                                   L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                                                   L_svc_custordsubdtl_tbl(i).sub_item)|| ';';
               end if;

            end if; -- Invalid Item
         END LOOP;
   end if;

   --In case of addition of any new validations, we need to take the substring of error message to restrict its length to 2000 characters 
   --as error_msg field in svc_custordsubdtl can take a maximum length of 2000 characters.
   
   if L_svc_custordsubdtl_tbl is NOT NULL and L_svc_custordsubdtl_tbl.COUNT > 0 then
      -- Update the detail table with the error.
      FORALL i in 1..L_svc_custordsubdtl_tbl.COUNT
         update svc_custordsubdtl
            set error_msg = L_svc_custordsubdtl_tbl(i).error_msg,
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where rowid = L_svc_custordsubdtl_tbl(i).rowid;

      -- Update header table for errors in detail table.
      FORALL i in 1..L_svc_custordsubdtl_tbl.COUNT
         update svc_custordsub
            set error_msg = SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_DETAIL',
                                                     L_svc_custordsubdtl_tbl(i).customer_order_no,
                                                     L_svc_custordsubdtl_tbl(i).fulfill_order_no,
                                                     NULL) || ';',
                process_status = LP_custordsub_error,
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where custordsub_id = L_svc_custordsubdtl_tbl(i).custordsub_id;

      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_SVC_CUSTORDSUB%ISOPEN then
         close C_SVC_CUSTORDSUB;
      end if;
      if C_SVC_CUSTORDSUBDTL%ISOPEN then
         close C_SVC_CUSTORDSUBDTL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END VALIDATE_SUBSTITUTION;
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_SUBSTITUTION_REC(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_ordcust_detail_tbl  IN OUT  ORDCUST_DETAIL_TBL,
                                 O_cust_ord_resv_tbl   IN OUT  CUST_ORD_RESERVE_TBL,
                                 O_new_item_loc_tbl    IN OUT  OBJ_ITEMLOC_TBL,
                                 I_process_id          IN      SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                                 I_chunk_id            IN      SVC_CUSTORDSUB.CHUNK_ID%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_CUSTORDSUB.CREATE_SUBSTITUTION_REC';

   -- Cursor to get the cancelled quantity for original ordered quantity and building a new ordcust_detail row for substitute item
   cursor C_ORDCUST_DETAIL_TBL IS
      select  ORDCUST_DETAIL_REC
              (ocd.ordcust_no,
              ocd.item,
              NULL,                                                                  -- ref_item
              NULL,                                                                  -- original_item
              ocd.qty_ordered_suom,                                                  -- qty_ordered_suom
              ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0) - svc.item_qty,   -- qty_cancelled_suom
              NULL,                                                                  -- standard_uom
              NULL,                                                                  -- transaction_uom
              NULL,                                                                  -- substitute_allowed_ind
              NULL,                                                                  -- unit_retail
              NULL,                                                                  -- retail_currency_code
              NULL,                                                                  -- comments
              NULL,                                                                  -- create_datetime
              NULL,                                                                  -- create_id
              NULL,                                                                  -- last_update_datetime
              NULL)                                                                  -- last_update_id
        from  svc_custordsub svc,
              ordcust oc,
              ordcust_detail ocd
       where  svc.process_id = I_process_id
         and  svc.chunk_id = I_chunk_id
         and  svc.process_status = LP_custordsub_validate
         and  oc.customer_order_no = svc.customer_order_no
         and  oc.fulfill_order_no = svc.fulfill_order_no                 
         and  oc.source_loc_type IS NULL
         and  oc.fulfill_loc_type  = 'S'
         and  ocd.ordcust_no = oc.ordcust_no
         and  ocd.item = svc.item
         and  ocd.original_item is NULL
      UNION ALL
      select  ORDCUST_DETAIL_REC
              (oc.ordcust_no,                                                        -- ordcust_no
              svcd.sub_item,                                                         -- item
              NULL,                                                                  -- ref_item
              svc.item,                                                              -- original_item
              svcd.sub_item_qty,                                                     -- qty_ordered_suom
              NULL,                                                                  -- qty_cancelled_suom
              svcd.sub_qty_uom,                                                      -- standard_uom
              svcd.sub_qty_uom,                                                      -- transaction_uom
              'N',                                                                   -- substitute_allowed_ind
              ocd_org.unit_retail,                                                   -- unit_retail
              ocd_org.retail_currency_code,                                          -- retail_currency_code                                                     
              NULL,                                                                  -- comments
              NULL,                                                                  -- create_datetime
              NULL,                                                                  -- create_id
              NULL,                                                                  -- last_update_datetime
              NULL)                                                                  -- last_update_id
        from  svc_custordsub svc,
              svc_custordsubdtl svcd,
              ordcust oc,
              ordcust_detail ocd_org
       where  svc.process_id = I_process_id
         and  svc.chunk_id = I_chunk_id
         and  svc.process_status = LP_custordsub_validate
         and  svc.custordsub_id = svcd.custordsub_id
         and  svc.customer_order_no = oc.customer_order_no
         and  svc.fulfill_order_no = oc.fulfill_order_no   
         and  oc.source_loc_type IS NULL
         and  oc.fulfill_loc_type  = 'S'
         and  oc.ordcust_no = ocd_org.ordcust_no
         and  svc.item = ocd_org.item
         and  ocd_org.original_item is NULL;

   -- Cursor to fetch the customer reservation updates to item_loc_soh table
   cursor C_CUST_RESERVE is
      select  CUST_ORD_RESERVE_REC
              (item,
              loc_id,
              SUM(customer_resv_qty))
        from  (select  svc.item item,                                         -- Customer reserve to be reduced for original regular item
                       svc.loc_id,
                       -1 * (ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0) - svc.item_qty) customer_resv_qty
                 from  svc_custordsub svc,
                       ordcust_detail ocd,
                       ordcust oc,
                       item_master im
                where  svc.process_id = I_process_id
                  and  svc.chunk_id = I_chunk_id
                  and  svc.process_status = LP_custordsub_validate
                  and  oc.fulfill_order_no = svc.fulfill_order_no
                  and  oc.customer_order_no = svc. customer_order_no
                  and  oc.source_loc_type IS NULL
                  and  oc.fulfill_loc_type = 'S'                     
                  and  oc.ordcust_no = ocd.ordcust_no
                  and  ocd.item = svc.item
                  and  ocd.original_item IS NULL
                  and  svc.item = im.item
                  and  im.pack_ind = 'N'
               UNION ALL
               select  vpq.item item,                                         -- Customer reserve to be reduced for original pack item
                       svc.loc_id,
                       -1 * vpq.qty * (ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0) - svc.item_qty)  customer_resv_qty
                 from  svc_custordsub svc,
                       ordcust_detail ocd,
                       ordcust oc,
                       item_master im,
                       v_packsku_qty vpq
                where  svc.process_id = I_process_id
                  and  svc.chunk_id = I_chunk_id
                  and  svc.process_status = LP_custordsub_validate
                  and  oc.fulfill_order_no = svc.fulfill_order_no
                  and  oc.customer_order_no = svc.customer_order_no     
                  and  oc.source_loc_type IS NULL
                  and  oc.fulfill_loc_type = 'S'
                  and  oc.ordcust_no = ocd.ordcust_no
                  and  ocd.item = svc.item
                  and  ocd.original_item IS NULL
                  and  svc.item = im.item
                  and  im.pack_ind = 'Y'
                  and  im.item = vpq.pack_no
               UNION ALL
               select  svcd.sub_item item,                                    -- Customer reserve to be incremented for substitute item
                       svc.loc_id,
                       svcd.sub_item_qty - NVL((select  ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0)
                                                  from  ordcust oc,
                                                        ordcust_detail ocd
                                                 where  oc.ordcust_no = ocd.ordcust_no
                                                   and  ocd.item = svcd.sub_item
                                                   and  ocd.original_item = svc.item
                                                   and  oc.fulfill_order_no = svc.fulfill_order_no
                                                   and  oc.customer_order_no = svc.customer_order_no     
                                                   and  oc.source_loc_type IS NULL
                                                   and  oc.fulfill_loc_type = 'S'),0) customer_resv_qty
                 from  svc_custordsubdtl svcd,
                       svc_custordsub svc,
                       item_master im
                where  svc.process_id = I_process_id
                  and  svc.chunk_id = I_chunk_id
                  and  svc.process_status = LP_custordsub_validate
                  and  svc.custordsub_id = svcd.custordsub_id
                  and  svcd.sub_item = im.item
                  and  im.pack_ind = 'N'
               UNION ALL
               select  vpq.item item,                                         -- Customer reserve to be incremented for substitute pack item
                       svc.loc_id,
                       vpq.qty * (svcd.sub_item_qty - NVL((select  ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0)
                                                             from  ordcust oc,
                                                                   ordcust_detail ocd
                                                            where  oc.ordcust_no = ocd.ordcust_no
                                                              and  ocd.item = svcd.sub_item
                                                              and  ocd.original_item = svc.item
                                                              and  oc.fulfill_order_no = svc.fulfill_order_no
                                                              and  oc.customer_order_no = svc.customer_order_no     
                                                              and  oc.source_loc_type IS NULL
                                                              and  oc.fulfill_loc_type = 'S'),0)) customer_resv_qty
                 from  svc_custordsubdtl svcd,
                       svc_custordsub svc,
                       item_master im,
                       v_packsku_qty vpq
                where  svc.process_id = I_process_id
                  and  svc.chunk_id = I_chunk_id
                  and  svc.process_status = LP_custordsub_validate
                  and  svc.custordsub_id = svcd.custordsub_id
                  and  svcd.sub_item = im.item
                  and  im.pack_ind = 'Y'
                  and  im.item = vpq.pack_no)
      group by item, loc_id;

   -- To create item_loc relationship for all the missing item/location combinations.
   cursor C_MISSING_ITEM_LOC is
      select  OBJ_ITEMLOC_REC
              (svcd.sub_item,
              svc.loc_id)
        from  svc_custordsub svc,
              svc_custordsubdtl svcd
       where  svc.process_id = I_process_id
         and  svc.chunk_id = I_chunk_id
         and  svc.process_status = LP_custordsub_validate
         and  svc.custordsub_id = svcd.custordsub_id
         and  NOT EXISTS (select 1
                            from item_loc il
                           where il.item = svcd.sub_item
                             and il.loc = svc.loc_id)
      group by svcd.sub_item, svc.loc_id;                 -- Including group by to remove duplicates for repeating item/loc records

BEGIN

   -- Populate item_loc collection to insert into item_loc table for all missing item/location combinations.
   SQL_LIB.SET_MARK('OPEN', 'C_MISSING_ITEM_LOC', 'SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ITEM_LOC', NULL);
   open C_MISSING_ITEM_LOC;

   SQL_LIB.SET_MARK('FETCH', 'C_MISSING_ITEM_LOC', 'SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ITEM_LOC', NULL);
   fetch C_MISSING_ITEM_LOC BULK COLLECT INTO O_new_item_loc_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_MISSING_ITEM_LOC', 'SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ITEM_LOC', NULL);
   close C_MISSING_ITEM_LOC;


   -- Populate the otrdcust_detail collection for merge (update/insert) into ordcust_detail table
   -- for original item and substitute item.
   SQL_LIB.SET_MARK('OPEN','C_ORDCUST_DETAIL_TBL','SVC_CUSTORDSUB, ORDCUST_DETAIL',NULL);
   open  C_ORDCUST_DETAIL_TBL;

   SQL_LIB.SET_MARK('FETCH','C_ORDCUST_DETAIL_TBL','SVC_CUSTORDSUB, ORDCUST_DETAIL',NULL);
   fetch  C_ORDCUST_DETAIL_TBL BULK COLLECT INTO O_ordcust_detail_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_ORDCUST_DETAIL_TBL','SVC_CUSTORDSUB, ORDCUST_DETAIL',NULL);
   close C_ORDCUST_DETAIL_TBL;

   -- Populate the customer reserve collection. This will be used to update the customer reservation inventory field for original item
   -- reservation cancellation and increment customer reserved inventory for substitute item.
   SQL_LIB.SET_MARK('OPEN','C_CUST_RESERVE','SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ORDCUST, ORDCUST_DETAIL, ITEM_MASTER, V_PACKSKU_QTY',NULL);
   open  C_CUST_RESERVE;

   SQL_LIB.SET_MARK('FETCH','C_CUST_RESERVE','SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ORDCUST, ORDCUST_DETAIL, ITEM_MASTER, V_PACKSKU_QTY',NULL);
   fetch  C_CUST_RESERVE BULK COLLECT INTO O_cust_ord_resv_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_CUST_RESERVE','SVC_CUSTORDSUB, SVC_CUSTORDSUBDTL, ORDCUST, ORDCUST_DETAIL, ITEM_MASTER, V_PACKSKU_QTY',NULL);
   close  C_CUST_RESERVE;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      if C_ORDCUST_DETAIL_TBL%ISOPEN then
         close C_ORDCUST_DETAIL_TBL;
      end if;
      if C_CUST_RESERVE%ISOPEN then
         close C_CUST_RESERVE;
      end if;
      if C_MISSING_ITEM_LOC%ISOPEN then
         close C_MISSING_ITEM_LOC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CREATE_SUBSTITUTION_REC;
-----------------------------------------------------------------------------------------------------
FUNCTION PERSIST_SUBSTITUTION(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_ordcust_detail_tbl IN     ORDCUST_DETAIL_TBL,
                              I_cust_ord_resv_tbl  IN     CUST_ORD_RESERVE_TBL,
                              I_new_item_loc_tbl   IN     OBJ_ITEMLOC_TBL)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_CUSTORDSUB.PERSIST_SUBSTITUTION';
   
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_ORDCUST_DETAIL is
      select 'x'
        from ordcust_detail od,
             TABLE(CAST(I_ordcust_detail_tbl as ORDCUST_DETAIL_TBL)) odt
       where od.ordcust_no = odt.ordcust_no
         and od.item = odt.item
         and NVL(od.original_item, '-999') = NVL(odt.original_item, '-999')
         for update of od.qty_ordered_suom, od.qty_cancelled_suom nowait;
BEGIN

   -- Calling NEW_ITEM_LOC to insert into item_los_soh for all missing item/location combinations
   if I_new_item_loc_tbl is NOT NULL and I_new_item_loc_tbl.COUNT > 0 then
      FOR i in I_new_item_loc_tbl.FIRST..I_new_item_loc_tbl.LAST LOOP
         if NEW_ITEM_LOC(O_error_message,
                         I_new_item_loc_tbl(i).item,
                         I_new_item_loc_tbl(i).loc,
                         NULL,
                         NULL,
                         'S',                     -- Loc_type
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDCUST_DETAIL','ORDCUST_DETAIL',NULL);
   open C_LOCK_ORDCUST_DETAIL;
   
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDCUST_DETAIL','ORDCUST_DETAIL',NULL);
   close C_LOCK_ORDCUST_DETAIL;
   
   --Merge into ORDCUST_DETAIL table based on ORDCUST_DETAIL table collection
   if (I_ordcust_detail_tbl is NOT NULL and I_ordcust_detail_tbl.COUNT > 0) then

      merge into ordcust_detail ocd
      using (select  ordcust_no,                            
                     item,
                     original_item,
                     ref_item,
                     qty_ordered_suom,
                     qty_cancelled_suom,
                     standard_uom,
                     substitute_allowed_ind,
                     transaction_uom,
                     unit_retail,
                     retail_currency_code,
                     comments,
                     create_id,
                     create_datetime,
                     last_update_id,
                     last_update_datetime
               from  TABLE(CAST(I_ordcust_detail_tbl  AS ORDCUST_DETAIL_TBL))) ocd_tbl
         on   (    ocd.ordcust_no = ocd_tbl.ordcust_no
              and  ocd.item = ocd_tbl.item
              and  NVL(ocd.original_item,'-999') = NVL(ocd_tbl.original_item,'-999'))
      when matched then
              update set ocd.qty_ordered_suom     =  ocd_tbl.qty_ordered_suom,
                         ocd.qty_cancelled_suom   =  NVL(ocd.qty_cancelled_suom,0) + NVL(ocd_tbl.qty_cancelled_suom,0),
                         ocd.last_update_datetime =  sysdate,
                         ocd.last_update_id       =  LP_user
      when not matched then
              insert (ordcust_no,
                      item,
                      original_item,
                      ref_item,
                      qty_ordered_suom,
                      qty_cancelled_suom,
                      standard_uom,
                      substitute_allowed_ind,
                      transaction_uom,
                      unit_retail,
                      retail_currency_code,
                      comments,
                      create_id,
                      create_datetime,
                      last_update_id,
                      last_update_datetime)
              values  (ocd_tbl.ordcust_no,
                      ocd_tbl.item,
                      ocd_tbl.original_item,
                      ocd_tbl.ref_item,
                      ocd_tbl.qty_ordered_suom,
                      ocd_tbl.qty_cancelled_suom,
                      ocd_tbl.standard_uom,
                      ocd_tbl.substitute_allowed_ind,
                      ocd_tbl.transaction_uom,
                      ocd_tbl.unit_retail,
                      ocd_tbl.retail_currency_code,
                      ocd_tbl.comments,
                      LP_user,
                      sysdate,
                      LP_user,
                      sysdate);

   end if;

   -- Calling CUSTOMER_RESERVE_SQL.PROCESS_CO_RESERVE() to update ITEM_LOC_SOH customer reserve field
   if CUSTOMER_RESERVE_SQL.PROCESS_CO_RESERVE(O_error_message,
                                              I_cust_ord_resv_tbl)= FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED then
      if C_LOCK_ORDCUST_DETAIL%ISOPEN then
         close C_LOCK_ORDCUST_DETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK',
                                             'ORDCUST_DETAIL',
                                             NULL,
                                             NULL);
      return FALSE; 
   WHEN OTHERS then
      if C_LOCK_ORDCUST_DETAIL%ISOPEN then
         close C_LOCK_ORDCUST_DETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST_SUBSTITUTION;
-------------------------------------------------------------------------------------------------------------------------------------
END CORESVC_CUSTORDSUB;
/