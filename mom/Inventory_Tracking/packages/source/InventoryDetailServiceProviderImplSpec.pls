/******************************************************************************
* Service Name     : InventoryDetailService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/InventoryDetailService/v1
* Description      :
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE INVENTORYDETAILSERVICEPROVIDER AUTHID CURRENT_USER AS


/******************************************************************************
 *
 * Operation       : lookupInvAvailCriVo
 * Description     : Lookup available inventory for customer orderable items for all specified item at all specified customer orderable locations.
                        Quantities will be returned in standard unit of measure.
 *
 * Input           : "RIB_InvAvailCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvAvailCriVo/v1
 * Description     :  Inventory detail lookup criteria contains a collections of item and location for which inventory details has to be fetched.
 *
 * Output          : "RIB_InvAvailColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvAvailColDesc/v1
 * Description     :  InvAvailColDesc contains collection of item, location along with the inventory details.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
                    message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
                    "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE lookupInvAvailCriVo(
                              I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                              I_businessObject          IN     "RIB_InvAvailCriVo_REC",
                              O_serviceOperationStatus  OUT    "RIB_ServiceOpStatus_REC",
                              O_businessObject          OUT    "RIB_InvAvailColDesc_REC"
                              );
/******************************************************************************/


END InventoryDetailServiceProvider;
/

  