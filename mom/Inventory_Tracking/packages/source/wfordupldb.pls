CREATE OR REPLACE PACKAGE BODY WF_ORDER_UPLOAD_SQL AS
-----------------------------------------------------------------------------------
LP_wf_order_new            VARCHAR2(1):= 'N';
LP_wf_order_rejected       VARCHAR2(1):= 'R';
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_STG_TABLES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id    IN     SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id      IN     SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)    := 'WF_ORDER_UPLOAD_SQL.VALIDATE_STG_TABLES';
   L_table           VARCHAR2(50);
   L_rej_ind         VARCHAR2(1)     := 'N';
   L_cust_ord_ref_no SVC_WF_ORD_HEAD.CUST_ORD_REF_NO%TYPE;
   L_wf_customer_id  SVC_WF_ORD_HEAD.WF_CUSTOMER_ID%TYPE;
   L_hdr_error_msg   VARCHAR2(2000)  := NULL;
   L_tail_error_msg  VARCHAR2(2000)  := NULL;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_FHEAD is
      select swoh.process_id,
             swoh.chunk_id,
             swoh.rowid,
             swoh.line_no,
             swoh.wf_customer_id,
             swoh.cust_ord_ref_no,
             swoh.currency_code,
             swoh.default_bill_to_loc,
             swoh.comments
        from svc_wf_ord_head swoh
       where swoh.process_id = I_process_id
         and swoh.chunk_id = I_chunk_id
         and swoh.process_status = LP_wf_order_new
          for update nowait;

   cursor C_GET_DETAIL is
      select swod.rowid,
             swod.process_id,
             swod.chunk_id,
             swod.line_no,
             swod.item,
             swod.customer_loc,
             swod.source_loc_id,
             swod.source_loc_type,
             swod.requested_qty,
             swod.uop,
             swod.fixed_cost,
             swod.need_date,
             swod.not_after_date,
             swod.error_msg
        from svc_wf_ord_detail swod
       where swod.process_id = I_process_id
         and swod.chunk_id = I_chunk_id
         and(swod.item is  NULL
          or swod.customer_loc is NULL
          or ((swod.source_loc_type is NULL and swod.source_loc_id is NOT NULL)
              or (swod.source_loc_type is NOT NULL and swod.source_loc_id is NULL)) 
          or NVL(swod.source_loc_type,'WH') NOT in ('ST','SU','WH')
          or swod.requested_qty is NULL
          or swod.uop is NULL
          or swod.need_date is NULL
          or swod.not_after_date is NULL)
         for update nowait;

   cursor C_GET_FTAIL is
      select swot.rowid,
             swot.total_record_count,
             swot.error_msg,
             (select count(*)
                from svc_wf_ord_detail
               where process_id = I_process_id
                 and chunk_id = I_chunk_id) dtl_count
        from svc_wf_ord_tail swot
       where swot.process_id = I_process_id
         for update nowait;

   TYPE F_HEAD_TBL IS TABLE OF C_GET_FHEAD%ROWTYPE;
   L_f_head_tbl F_HEAD_TBL;

   TYPE F_DETAIL_TBL IS TABLE OF C_GET_DETAIL%ROWTYPE;
   L_f_detail_tbl F_DETAIL_TBL;

   TYPE F_TAIL_TBL IS TABLE OF C_GET_FTAIL%ROWTYPE;
   L_f_tail_tbl F_TAIL_TBL;

BEGIN
   -- VALIDATE REQUIRED INPUT
   if I_process_id is NULL then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                                  'I_process_id',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                                  'I_chunk_id',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;

   --Validate file header record for below errors
   -- CUSTOMER_ID should not be NULL
   -- CUST_ORD_REF_NO should not be NULL
   -- CURRENCY_CODE should not be NULL
   -- There should be one header record for a single file 

   L_table := 'SVC_WF_ORD_HEAD';
   open C_GET_FHEAD;
   fetch C_GET_FHEAD BULK COLLECT into L_f_head_tbl;
   close C_GET_FHEAD;

   if(L_f_head_tbl is NOT NULL and L_f_head_tbl.COUNT = 1) then
      L_cust_ord_ref_no := L_f_head_tbl(1).cust_ord_ref_no;
      L_wf_customer_id  := L_f_head_tbl(1).wf_customer_id;
      -- Validate if the customer id is not null
      if(L_f_head_tbl(1).wf_customer_id is NULL) then
         L_hdr_error_msg := L_hdr_error_msg || SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_REQ_CUST_ID',
                                                                        L_cust_ord_ref_no,
                                                                        L_f_head_tbl(1).line_no,
                                                                        NULL)||';';
         L_rej_ind := 'Y';
      -- Validate if the customer order ref no is not null
      elsif(L_f_head_tbl(1).cust_ord_ref_no is NULL) then
         L_hdr_error_msg := L_hdr_error_msg || SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_REQ_REF_NO',
                                                                        L_wf_customer_id,
                                                                        L_f_head_tbl(1).line_no,
                                                                        NULL)||';';
         L_rej_ind := 'Y';
      -- Validate if the currency code is not null
      elsif(L_f_head_tbl(1).currency_code is NULL) then
         L_hdr_error_msg := L_hdr_error_msg || SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_REQ_CURR',
                                                                        L_cust_ord_ref_no,
                                                                        L_wf_customer_id,
                                                                        L_f_head_tbl(1).line_no)||';';
         L_rej_ind := 'Y';
      end if;
   end if;

   --Validate file detail records for below errors
   -- ITEM should not be NULL
   -- CUSTOMER_LOC should not be NULL
   -- REQUESTED_QTY should not be NULL
   -- UOP should not be NULL
   -- NEED_DATE should not be NULL
   -- NOT_AFTER_DATE should not be NULL
   -- SOURCE_LOC_ID and SOURCE_LOC_TYPE both must be entered or both should be NULL
   -- SOURCE_LOC_TYPE if passed should be 'ST'(store)/'SU'(supplier)/'WH'(warehouse)

   L_table := 'SVC_WF_ORD_DETAIL';
   open C_GET_DETAIL;
   fetch C_GET_DETAIL BULK COLLECT INTO L_f_detail_tbl;
   close C_GET_DETAIL;

   if(L_f_detail_tbl is not NULL and L_f_detail_tbl.count > 0) then
      FOR i in L_f_detail_tbl.FIRST..L_f_detail_tbl.LAST LOOP
      -- Validate if the item is not null
         if L_f_detail_tbl(i).item is NULL then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg ||
                                           SQL_LIB.GET_MESSAGE_TEXT('F_ORDUPLD_REQ_ITEM',
                                                                    L_cust_ord_ref_no,
                                                                    L_wf_customer_id,
                                                                    L_f_detail_tbl(i).line_no)||';';
         end if;
         -- Validate if the customer_loc is not null
          if L_f_detail_tbl(i).customer_loc is NULL then
             L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg ||
                                            SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_REQ_CUST_LOC',
                                                                     L_cust_ord_ref_no,
                                                                     L_wf_customer_id,
                                                                     L_f_detail_tbl(i).line_no)||';';
         end if;
         -- Validate if source_loc_type is valid
          if L_f_detail_tbl(i).source_loc_type NOT IN ('SU','ST','WH') then
             L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg ||
                                            SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_SRC_TYPE',
                                                                     L_cust_ord_ref_no,
                                                                     L_wf_customer_id,
                                                                     L_f_detail_tbl(i).line_no)||';';
         end if;
         -- Validate if the source_loc_id and source_loc_type both are entered or both should be NULL
          if ((L_f_detail_tbl(i).source_loc_id is NULL and L_f_detail_tbl(i).source_loc_type is NOT NULL
              or L_f_detail_tbl(i).source_loc_id is NOT NULL and L_f_detail_tbl(i).source_loc_type is NULL)) then
             L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg ||
                                            SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_REQ_SRC_LOC',
                                                                     L_cust_ord_ref_no,
                                                                     L_wf_customer_id,
                                                                     L_f_detail_tbl(i).line_no)||';';
         end if;
         -- Validate if the requested_qty is not null
         if L_f_detail_tbl(i).requested_qty is NULL then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg ||
                                           SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_NO_REQ_QTY',
                                                                    L_cust_ord_ref_no,
                                                                    L_wf_customer_id,
                                                                    L_f_detail_tbl(i).line_no)||';';
         end if;
         -- Validate if the UOP is not null
         if L_f_detail_tbl(i).uop is NULL then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg ||
                                           SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_REQ_UOP',
                                                                    L_cust_ord_ref_no,
                                                                    L_wf_customer_id,
                                                                    L_f_detail_tbl(i).line_no)||';';
         end if;
         -- Validate if the need_date is valid
         if L_f_detail_tbl(i).need_date is NULL then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg ||
                                           SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_REQ_NEED_DATE',
                                                                    L_cust_ord_ref_no,
                                                                    L_wf_customer_id,
                                                                    L_f_detail_tbl(i).line_no)||';';
         end if;

         -- Validate if the not_after_date is valid
         if L_f_detail_tbl(i).not_after_date is NULL then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg ||
                                           SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_NO_NT_AFT_DT',
                                                                    L_cust_ord_ref_no,
                                                                    L_wf_customer_id,
                                                                    L_f_detail_tbl(i).line_no)||';';
         end if;
      END LOOP;
      L_rej_ind := 'Y';
   end if;

   if L_f_detail_tbl is NOT NULL and L_f_detail_tbl.COUNT > 0 then
      -- Update the detail table with the error_msg.
      FORALL i in 1..L_f_detail_tbl.LAST
         update svc_wf_ord_detail
            set error_msg = L_f_detail_tbl(i).error_msg,
                last_update_datetime = SYSDATE,
                last_update_id = GET_USER
          where rowid = L_f_detail_tbl(i).rowid;

      L_hdr_error_msg := L_hdr_error_msg || SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ERR_DTL',
                                                                     NULL,
                                                                     NULL,
                                                                     NULL)||';';
      
   end if;

   --Validate file tail record for below errors
   -- Total record count should be equal to the recods in the detail table.
   -- Multiple tail records for a single file should not be there

   L_table := 'SVC_WF_ORD_TAIL';
   open C_GET_FTAIL;
   fetch C_GET_FTAIL BULK COLLECT into L_f_tail_tbl;
   close C_GET_FTAIL;

   -- Check if there is no ftail record
   if (L_f_tail_tbl is NULL or L_f_tail_tbl.COUNT < 1) then
      L_hdr_error_msg := L_hdr_error_msg || SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_NO_FTAIL',
                                                                     NULL,
                                                                     NULL,
                                                                     NULL)||';';
   L_rej_ind := 'Y';
   -- Check if there is more than one ftail record
   elsif (L_f_tail_tbl is NOT NULL and L_f_tail_tbl.COUNT > 1) then
      L_tail_error_msg := SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_FTAIL_CNT',
                                                   NULL,
                                                   NULL,
                                                   NULL)||';';

      L_hdr_error_msg := L_hdr_error_msg || SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ERR_TAIL',
                                                                     NULL,
                                                                     NULL,
                                                                     NULL)||';';
   L_rej_ind := 'Y';
   elsif (L_f_tail_tbl is NOT NULL and L_f_tail_tbl.COUNT = 1) then
      -- Check if FTAIL record count matches number of detail records
      if (L_f_tail_tbl(1).total_record_count <> L_f_tail_tbl(1).dtl_count) then
         L_tail_error_msg := SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_CNT',
                                                      NULL,
                                                      NULL,
                                                      NULL)||';';

         L_hdr_error_msg := L_hdr_error_msg || SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ERR_TAIL',
                                                                        NULL,
                                                                        NULL,
                                                                        NULL)||';';
         L_rej_ind := 'Y';
      end if;
   end if;

   -- Update the tail table with the error_msg.
   update svc_wf_ord_tail
      set error_msg = L_tail_error_msg,
          last_update_datetime = SYSDATE,
          last_update_id = GET_USER
    where process_id = I_process_id
      and L_tail_error_msg is NOT NULL;

   -- Update the status of the process_id in the head table to reject
   update svc_wf_ord_head
      set error_msg = L_hdr_error_msg,
          process_status = LP_wf_order_rejected,
          last_update_datetime = SYSDATE,
          last_update_id = GET_USER
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and L_hdr_error_msg is NOT NULL;

   if L_rej_ind = 'Y' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_LOCKED',
                                             L_table,
                                             I_process_id,
                                             I_chunk_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;  
END VALIDATE_STG_TABLES;
-------------------------------------------------------------------------------------
END WF_ORDER_UPLOAD_SQL;
/