CREATE OR REPLACE PACKAGE CORESVC_FULFILORD_INV AUTHID CURRENT_USER AS

------------------------------------------------------------------------------
--Function Name: RESERVE_INVENTORY
--Purpose:       This public function will perform the core logic for validating
--               and processing the customer order fulfillment create requests.
--               This leads to inventory reservation process.
--Called by:     CORESVC_FULFILORD.CREATE_FULFILLMENT
------------------------------------------------------------------------------
FUNCTION RESERVE_INVENTORY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id       IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                           I_chunk_id         IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------
--Function Name: RELEASE_INVENTORY
--Purpose:       This public function will perform the core logic for validating 
--               and processing the customer order fulfillment cancellation requests.
--               This leads to the release of already reserved inventory.
--Called by:     CORESVC_FULFILORD.CANCEL_FULFILLMENT
------------------------------------------------------------------------------
FUNCTION RELEASE_INVENTORY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id       IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                           I_chunk_id         IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------

END CORESVC_FULFILORD_INV;
/