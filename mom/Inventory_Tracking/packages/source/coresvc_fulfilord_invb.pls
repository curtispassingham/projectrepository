CREATE OR REPLACE PACKAGE BODY CORESVC_FULFILORD_INV AS

-- NOTES: 
-- 1. Item types validations are not done in this package as the calling package is doing all the necessary item type checks.
-- 2. Currently, localization is not supported and hence L10N_ORDCUST_EXT_REC is created regardless of the localization attributes.
   
LP_tran_type        varchar2(1) := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_INV;
LP_status_valid     varchar2(1) := 'V';
LP_status_complete  varchar2(1) := 'C'; 
LP_status_error     varchar2(1) := 'E';
LP_user             SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;

------------------------------------------------------------------------------------------------
---                                       PRIVATE FUNCTIONS                                 ---
------------------------------------------------------------------------------------------------
---FUNCTION NAME:VALIDATE_CUST_RESERVE
---Purpose: Called by RESERVE_INVENTORY to validate the reservation records in staging tables.
------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CUST_RESERVE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:POPULATE_CREATE_REC
---Purpose: Called by RESERVE_INVENTORY to populate the collections that are used to persist 
---the database.
------------------------------------------------------------------------------------------------
FUNCTION POPULATE_CREATE_REC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ordcust_tbl          IN OUT   ORDCUST_TBL,
                             O_ordcust_detail_tbl   IN OUT   ORDCUST_DETAIL_TBL,
                             O_new_item_loc_tbl     IN OUT   OBJ_ITEMLOC_TBL,
                             O_item_loc_resv_tbl    IN OUT   CUST_ORD_RESERVE_TBL,
                             O_l10n_ordcust_ext_tbl IN OUT   L10N_ORDCUST_EXT_TBL,
                             I_process_id           IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                             I_chunk_id             IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:PERSIST_CREATE_RESERVATION
---Purpose: Called by RESERVE_INVENTORY to persist the database tables.
------------------------------------------------------------------------------------------------
FUNCTION PERSIST_CREATE_RESERVATION(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_ordcust_tbl          IN      ORDCUST_TBL,
                                    I_ordcust_detail_tbl   IN      ORDCUST_DETAIL_TBL,
                                    I_new_item_loc_tbl     IN      OBJ_ITEMLOC_TBL,
                                    I_item_loc_resv_tbl    IN      CUST_ORD_RESERVE_TBL,
                                    I_l10n_ordcust_ext_tbl IN OUT  L10N_ORDCUST_EXT_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:VALIDATE_RESV_CANCEL
---Purpose: Called by RELEASE_INVENTORY to validate the cancellation records in staging table.
------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RESV_CANCEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:POPULATE_CANCEL_REC
---Purpose: Called by RELEASE_INVENTORY to populate the collections that are used to persist 
---the database.
------------------------------------------------------------------------------------------------
FUNCTION POPULATE_CANCEL_REC(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ordcust_detail_cancel_tbl   IN OUT   ORDCUST_DETAIL_CANCEL_TBL,
                             O_item_loc_resv_canc_tbl      IN OUT   CUST_ORD_RESERVE_TBL,
                             I_process_id                  IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id                    IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:PERSIST_RESV_CANCEL
---Purpose: Called by RELEASE_INVENTORY to persist the database tables.
------------------------------------------------------------------------------------------------
FUNCTION PERSIST_RESV_CANCEL(O_error_message               IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ordcust_detail_cancel_tbl   IN      ORDCUST_DETAIL_CANCEL_TBL,
                             I_item_loc_resv_tbl           IN      CUST_ORD_RESERVE_TBL)

RETURN BOOLEAN;

------------------------------------------------------------------------------------------------
FUNCTION RESERVE_INVENTORY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id       IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                           I_chunk_id         IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64) := 'CORESVC_FULFILORD_INV.RESERVE_INVENTORY';
   L_table       VARCHAR2(50);
   L_key         VARCHAR2(100);
  
   L_ordcust_tbl             ORDCUST_TBL;          -- For insert into ordcust table
   L_ordcust_detail_tbl      ORDCUST_DETAIL_TBL;   -- For insert into ordcust_detail table
   L_new_item_loc_tbl        OBJ_ITEMLOC_TBL;      -- For ranging items to non ranged locations
   L_item_loc_resv_tbl       CUST_ORD_RESERVE_TBL; -- Will be used for updating item_loc_soh
   L_l10n_ordcust_ext_tbl    L10N_ORDCUST_EXT_TBL; -- Will be used for localization
  
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and tran_type = LP_tran_type
         for update nowait;    

   cursor C_LOCK_SVC_FULFILORDDTL is
      select 'x'
        from svc_fulfilorddtl sfd,
             svc_fulfilord sf
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.tran_type = LP_tran_type
         and sfd.fulfilord_id = sf.fulfilord_id
         for update nowait;    

BEGIN

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('REQUIRED_INPUT_IS_NULL', 'I_process_id', L_program, NULL);
      return FALSE;
   end if;
   
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('REQUIRED_INPUT_IS_NULL', 'I_chunk_id', L_program, NULL);
      return FALSE;
   end if;   

   L_table := 'SVC_FULFILORD';
   L_key := 'process_id: '||to_char(I_process_id);
   L_key := L_key || ' chunk_id: '||to_char(I_chunk_id);

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SVC_FULFILORD', L_table, L_key);
   open C_LOCK_SVC_FULFILORD;

   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SVC_FULFILORD', L_table, L_key);
   close C_LOCK_SVC_FULFILORD;
   
   L_table := 'SVC_FULFILORDDTL';

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SVC_FULFILORDDTL', L_table, L_key);
   open C_LOCK_SVC_FULFILORDDTL;

   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SVC_FULFILORDDTL', L_table, L_key);
   close C_LOCK_SVC_FULFILORDDTL;
      
   if VALIDATE_CUST_RESERVE(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   if POPULATE_CREATE_REC(O_error_message,
                          L_ordcust_tbl,
                          L_ordcust_detail_tbl,
                          L_new_item_loc_tbl,
                          L_item_loc_resv_tbl,
                          L_l10n_ordcust_ext_tbl,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CREATE_RESERVATION(O_error_message,
                                 L_ordcust_tbl,
                                 L_ordcust_detail_tbl,
                                 L_new_item_loc_tbl,
                                 L_item_loc_resv_tbl,
                                 L_l10n_ordcust_ext_tbl) = FALSE then
      return FALSE;
   end if;
   
   L_table := 'SVC_FULFILORD';
   SQL_LIB.SET_MARK('UDPATE', NULL, L_table, NULL);
   
   update svc_fulfilord
      set process_status = LP_status_complete,
          last_update_datetime = SYSDATE,
          last_update_id = LP_user
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and tran_type = LP_tran_type
      and process_status = LP_status_valid;
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SVC_FULFILORD%ISOPEN then
         close C_LOCK_SVC_FULFILORD;
      end if;
      if C_LOCK_SVC_FULFILORDDTL%ISOPEN then
         close C_LOCK_SVC_FULFILORDDTL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_LOCKED', L_table, I_process_id, I_chunk_id);
      return FALSE;
   when OTHERS then
      if C_LOCK_SVC_FULFILORD%ISOPEN then
         close C_LOCK_SVC_FULFILORD;
      end if;
      if C_LOCK_SVC_FULFILORDDTL%ISOPEN then
         close C_LOCK_SVC_FULFILORDDTL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END RESERVE_INVENTORY;
------------------------------------------------------------------------------
FUNCTION VALIDATE_CUST_RESERVE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64)     := 'CORESVC_FULFILORD_INV.VALIDATE_CUST_RESERVE';
   L_error_msg         RTK_ERRORS.RTK_TEXT%TYPE;
   L_table             VARCHAR2(50);
   L_key               VARCHAR2(100);

   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

cursor C_GET_SVC_FULFILORD is
   select sf.fulfilord_id,
          sf.customer_order_no,
          sf.fulfill_order_no
     from svc_fulfilord sf
    where sf.process_id = I_process_id
      and sf.chunk_id = I_chunk_id
      and sf.tran_type = LP_tran_type
      and sf.process_status = LP_status_valid
      and (    sf.source_loc_type is NOT NULL 
           OR  sf.fulfill_loc_type <> 'S'
           OR  not exists ( select 1 
                              from store 
                             where stockholding_ind = 'Y'
                               and store = sf.fulfill_loc_id));
       
   TYPE SVC_FULFILORD_TBL is TABLE of C_GET_SVC_FULFILORD%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_fulfilord_tbl      SVC_FULFILORD_TBL;

BEGIN

   L_table := 'SVC_FULFILORD, STORE';
   L_key := 'process_id: '||to_char(I_process_id);
   L_key := L_key || ' chunk_id: '||to_char(I_chunk_id);

   SQL_LIB.SET_MARK('OPEN', 'C_GET_SVC_FULFILORD', L_table, L_key);
   open C_GET_SVC_FULFILORD;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SVC_FULFILORD', L_table, L_key);
   fetch C_GET_SVC_FULFILORD bulk collect into L_svc_fulfilord_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SVC_FULFILORD', L_table, L_key);
   close C_GET_SVC_FULFILORD;
   
   if L_svc_fulfilord_tbl is not NULL and L_svc_fulfilord_tbl.COUNT > 0 then
      L_table := 'SVC_FULFILORD';
      SQL_LIB.SET_MARK('UDPATE', NULL, L_table, NULL);
   
      FORALL i in L_svc_fulfilord_tbl.first..L_svc_fulfilord_tbl.last
         update svc_fulfilord  
            set error_msg = error_msg || SQL_LIB.GET_MESSAGE_TEXT('INV_RESV_CANC_STG_REC',  
                                                                   L_svc_fulfilord_tbl(i).customer_order_no, 
                                                                   L_svc_fulfilord_tbl(i).fulfill_order_no, 
                                                                   NULL)||';',
                process_status = LP_status_error,
                last_update_datetime = SYSDATE,
                last_update_id = LP_user                
          where fulfilord_id = L_svc_fulfilord_tbl(i).fulfilord_id;

      -- Set O_Error_message as the first validation error
      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_msg
        from svc_fulfilord
       where fulfilord_id = L_svc_fulfilord_tbl(1).fulfilord_id
         and rownum = 1;

      O_error_message := L_error_msg;
       
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_SVC_FULFILORD%ISOPEN then
         close C_GET_SVC_FULFILORD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_LOCKED', L_table, I_process_id, I_chunk_id);
      return FALSE;
   when OTHERS then
      if C_GET_SVC_FULFILORD%ISOPEN then
         close C_GET_SVC_FULFILORD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END VALIDATE_CUST_RESERVE;
------------------------------------------------------------------------------
FUNCTION POPULATE_CREATE_REC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ordcust_tbl          IN OUT   ORDCUST_TBL,
                             O_ordcust_detail_tbl   IN OUT   ORDCUST_DETAIL_TBL,
                             O_new_item_loc_tbl     IN OUT   OBJ_ITEMLOC_TBL,
                             O_item_loc_resv_tbl    IN OUT   CUST_ORD_RESERVE_TBL,
                             O_l10n_ordcust_ext_tbl IN OUT   L10N_ORDCUST_EXT_TBL,
                             I_process_id           IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                             I_chunk_id             IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64)         := 'CORESVC_FULFILORD_INV.POPULATE_CREATE_REC';
   L_table             VARCHAR2(50);
   L_key               VARCHAR2(100);
     
cursor C_SVC_FULFILORD is
   select ORDCUST_REC
          (ordcust_seq.nextval, -- ordcust_no
          'C',                 -- status
          NULL,                -- order_no
          NULL,                -- tsf_no
          NULL,                -- source_loc_type
          NULL,                -- source_loc_id
          sf.fulfill_loc_type,
          sf.fulfill_loc_id,
          sfc.customer_no,
          sf.customer_order_no,
          sf.fulfill_order_no,
          sf.partial_delivery_ind,
          sf.delivery_type,
          sf.carrier_code,
          sf.carrier_service_code,
          sf.consumer_delivery_date,
          sf.consumer_delivery_time,
          sfc.bill_first_name,
          sfc.bill_phonetic_first,
          sfc.bill_last_name,
          sfc.bill_phonetic_last,
          sfc.bill_preferred_name,
          sfc.bill_company_name,
          sfc.bill_add1,
          sfc.bill_add2,
          sfc.bill_add3,
          sfc.bill_county,
          sfc.bill_city,
          sfc.bill_state,
          sfc.bill_country_id,
          sfc.bill_post,
          sfc.bill_jurisdiction,
          sfc.bill_phone,
          sfc.deliver_first_name,
          sfc.deliver_phonetic_first,
          sfc.deliver_last_name,
          sfc.deliver_phonetic_last,
          sfc.deliver_preferred_name,
          sfc.deliver_company_name,
          sfc.deliver_add1,
          sfc.deliver_add2,
          sfc.deliver_add3,
          sfc.deliver_city,
          sfc.deliver_state,
          sfc.deliver_country_id,
          sfc.deliver_post,
          sfc.deliver_county,
          sfc.deliver_jurisdiction,
          sfc.deliver_phone,
          sf.delivery_charges,
          sf.delivery_charges_curr,
          sf.comments,
          SYSDATE,                                    -- create_datetime
          LP_user,                                    -- create_id
          SYSDATE,                                    -- last_update_datetime
          LP_user)                                    -- last_update_id
     from svc_fulfilord sf,
          svc_fulfilordcust sfc
    where sf.process_id = I_process_id
      and sf.chunk_id = I_chunk_id
      and sf.tran_type = LP_tran_type
      and sf.process_status = LP_status_valid
      and sf.fulfilord_id = sfc.fulfilord_id;
   
   cursor C_SVC_FULFILORDDTL is
      select ORDCUST_DETAIL_REC
             (oc_tbl.ordcust_no,
             sfd.item,
             sfd.ref_item,
             NULL,               --  original_item
             sfd.order_qty_suom,
             NULL,               -- qty_cancelled_suom
             sfd.standard_uom,
             sfd.transaction_uom,
             sfd.substitute_ind, -- substitute_allowed_ind
             sfd.unit_retail,
             sfd.retail_curr,    -- retail_currency_code
             sfd.comments,
             SYSDATE,                                   -- create_datetime
             LP_user,                                   -- create_id
             SYSDATE,                                   -- last_update_datetime
             LP_user)                                   -- last_update_id
        from svc_fulfilorddtl sfd,
             svc_fulfilord sf,
             TABLE(CAST(O_ordcust_tbl AS ORDCUST_TBL)) oc_tbl
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.tran_type = LP_tran_type
         and sf.process_status = LP_status_valid
         and sf.fulfilord_id = sfd.fulfilord_id
         and sf.customer_order_no = oc_tbl.customer_order_no
         and sf.fulfill_order_no = oc_tbl.fulfill_order_no
         and NVL(sf.source_loc_id,-1) = NVL(oc_tbl.source_loc_id,-1)
         and sf.fulfill_loc_id = oc_tbl.fulfill_loc_id;
         
   -- Create L10N records
   cursor C_L10N_EXT_REC is
      select oc_tbl.ordcust_no,
             sf.fulfilord_id,
             oc_tbl.fulfill_loc_id
        from svc_fulfilord sf,
             TABLE(CAST(O_ordcust_tbl AS ORDCUST_TBL)) oc_tbl
       where sf.customer_order_no = oc_tbl.customer_order_no
         and sf.fulfill_order_no = oc_tbl.fulfill_order_no;
         
   -- To create item_loc relationship for all the missing item/location combinations.
   cursor C_MISSING_ITEM_LOC is
      select OBJ_ITEMLOC_REC
             (ocd_tbl.item,
             oc_tbl.fulfill_loc_id)
        from TABLE(CAST(O_ordcust_tbl AS ORDCUST_TBL)) oc_tbl,
             TABLE(CAST(O_ordcust_detail_tbl AS ORDCUST_DETAIL_TBL)) ocd_tbl
       where oc_tbl.ordcust_no = ocd_tbl.ordcust_no
         and NOT EXISTS (select 1
                           from item_loc il
                          where il.item = ocd_tbl.item
                            and il.loc = oc_tbl.fulfill_loc_id)
    group by ocd_tbl.item,
             oc_tbl.fulfill_loc_id;
             
   -- To populate item_loc_resv collection to be used for updating item_loc_soh table.
   -- For pack item, the components reservation quantity is being fetched because the reservation 
   -- is only for store location where pack inventory is not being maintained.
   cursor C_ITEM_LOC_RESV is
      select CUST_ORD_RESERVE_REC
             (ocd_tbl.item,
             oc_tbl.fulfill_loc_id,      -- loc
             ocd_tbl.qty_ordered_suom)   -- customer_resv
        from item_master im,
             TABLE(CAST(O_ordcust_tbl AS ORDCUST_TBL)) oc_tbl,
             TABLE(CAST(O_ordcust_detail_tbl AS ORDCUST_DETAIL_TBL)) ocd_tbl
       where oc_tbl.ordcust_no = ocd_tbl.ordcust_no
         and im.item = ocd_tbl.item
         and im.pack_ind = 'N'
      UNION ALL   
      select CUST_ORD_RESERVE_REC
             (vpq.item,                            -- Component item
             oc_tbl.fulfill_loc_id,                -- loc
             (vpq.qty * ocd_tbl.qty_ordered_suom)) -- customer_resv
        from item_master im,
             v_packsku_qty vpq,
             TABLE(CAST(O_ordcust_tbl AS ORDCUST_TBL)) oc_tbl,
             TABLE(CAST(O_ordcust_detail_tbl AS ORDCUST_DETAIL_TBL)) ocd_tbl
       where oc_tbl.ordcust_no = ocd_tbl.ordcust_no
         and vpq.pack_no = im.item
         and im.item = ocd_tbl.item
         and im.pack_ind = 'Y';

   TYPE L10N_EXT_TABLE is TABLE of C_L10N_EXT_REC%ROWTYPE INDEX BY BINARY_INTEGER;
   L_l10n_ext_tbl  L10N_EXT_TABLE;
   
  
BEGIN   
   -- Retrieve from svc_fulfilord and svc_fulfilordcust to populate ORDCUST table.
   L_table := 'SVC_FULFILORD, SVC_FULFILORDCUST';
   L_key := 'process_id: '||to_char(I_process_id);
   L_key := L_key || ' chunk_id: '||to_char(I_chunk_id);

   SQL_LIB.SET_MARK('OPEN', 'C_SVC_FULFILORD', L_table, L_key);
   open C_SVC_FULFILORD;

   SQL_LIB.SET_MARK('FETCH', 'C_SVC_FULFILORD', L_table, L_key);
   fetch C_SVC_FULFILORD bulk collect into O_ordcust_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_SVC_FULFILORD', L_table, L_key);
   close C_SVC_FULFILORD;
   
   -- Build L10N_ORDCUST_EXT_REC collection
   L_table := 'O_ordcust_tbl, SVC_FULFILORD';

   SQL_LIB.SET_MARK('OPEN', 'C_L10N_EXT_REC', L_table, L_key);
   open C_L10N_EXT_REC;

   SQL_LIB.SET_MARK('FETCH', 'C_L10N_EXT_REC', L_table, L_key);
   fetch C_L10N_EXT_REC bulk collect into L_l10n_ext_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_L10N_EXT_REC', L_table, L_key);
   close C_L10N_EXT_REC;
   
   O_l10n_ordcust_ext_tbl := L10N_ORDCUST_EXT_TBL();
   
   if L_l10n_ext_tbl is not NULL and L_l10n_ext_tbl.COUNT > 0 then
      FOR i in L_l10n_ext_tbl.first..L_l10n_ext_tbl.last LOOP
         O_l10n_ordcust_ext_tbl.EXTEND();
         O_l10n_ordcust_ext_tbl(O_l10n_ordcust_ext_tbl.LAST) := L10N_ORDCUST_EXT_REC();
         O_l10n_ordcust_ext_tbl(O_l10n_ordcust_ext_tbl.LAST).procedure_key := 'CREATE_ORDCUST_L10N_EXT';
         O_l10n_ordcust_ext_tbl(O_l10n_ordcust_ext_tbl.LAST).ordcust_no := L_l10n_ext_tbl(i).ordcust_no;
         O_l10n_ordcust_ext_tbl(O_l10n_ordcust_ext_tbl.LAST).process_id := I_process_id;
         O_l10n_ordcust_ext_tbl(O_l10n_ordcust_ext_tbl.LAST).chunk_id := I_chunk_id;
         O_l10n_ordcust_ext_tbl(O_l10n_ordcust_ext_tbl.LAST).reference_id := L_l10n_ext_tbl(i).fulfilord_id;
         O_l10n_ordcust_ext_tbl(O_l10n_ordcust_ext_tbl.LAST).source_entity := 'LOC';
         O_l10n_ordcust_ext_tbl(O_l10n_ordcust_ext_tbl.LAST).source_id := L_l10n_ext_tbl(i).fulfill_loc_id;                                                                          
      END LOOP;
   end if;
         
   -- Retrieve from svc_fulfilorddtl table and ordcust collection to populate ORDCUST_DETAIL table.
   L_table := 'SVC_FULFILORDDTL, SVC_FULFILORD';
   
   SQL_LIB.SET_MARK('OPEN', 'C_SVC_FULFILORDDTL', L_table, L_key);
   open C_SVC_FULFILORDDTL;

   SQL_LIB.SET_MARK('FETCH', 'C_SVC_FULFILORDDTL', L_table, L_key);
   fetch C_SVC_FULFILORDDTL bulk collect into O_ordcust_detail_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_SVC_FULFILORDDTL', L_table, L_key);
   close C_SVC_FULFILORDDTL;
   
   -- Populate item_loc collection to insert into item_loc table for all missing item/location combinations.
   L_table := 'ITEM_LOC';
  
   SQL_LIB.SET_MARK('OPEN', 'C_MISSING_ITEM_LOC', L_table, L_key);
   open C_MISSING_ITEM_LOC;

   SQL_LIB.SET_MARK('FETCH', 'C_MISSING_ITEM_LOC', L_table, L_key);
   fetch C_MISSING_ITEM_LOC bulk collect into O_new_item_loc_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_MISSING_ITEM_LOC', L_table, L_key);
   close C_MISSING_ITEM_LOC;


   -- Populate item_loc_resv collection to update item_loc_soh table.
   L_table := 'ITEM_MASTER, V_PACKSKU_QTY';

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM_LOC_RESV', L_table, L_key);
   open C_ITEM_LOC_RESV;

   SQL_LIB.SET_MARK('FETCH', 'C_ITEM_LOC_RESV', L_table, L_key);
   fetch C_ITEM_LOC_RESV bulk collect into O_item_loc_resv_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_LOC_RESV', L_table, L_key);
   close C_ITEM_LOC_RESV;
 
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_SVC_FULFILORD%ISOPEN then
         close C_SVC_FULFILORD;
      end if;
      if C_SVC_FULFILORDDTL%ISOPEN then
         close C_SVC_FULFILORDDTL;
      end if;
      if C_L10N_EXT_REC%ISOPEN then
         close C_L10N_EXT_REC;
      end if;
      if C_MISSING_ITEM_LOC%ISOPEN then
         close C_MISSING_ITEM_LOC;
      end if;
      if C_ITEM_LOC_RESV%ISOPEN then
         close C_ITEM_LOC_RESV;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END POPULATE_CREATE_REC;
------------------------------------------------------------------------------
FUNCTION PERSIST_CREATE_RESERVATION(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_ordcust_tbl          IN      ORDCUST_TBL,
                                    I_ordcust_detail_tbl   IN      ORDCUST_DETAIL_TBL,
                                    I_new_item_loc_tbl     IN      OBJ_ITEMLOC_TBL,
                                    I_item_loc_resv_tbl    IN      CUST_ORD_RESERVE_TBL,
                                    I_l10n_ordcust_ext_tbl IN OUT  L10N_ORDCUST_EXT_TBL)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_FULFILORD_INV.PERSIST_CREATE_RESERVATION';
   L_table             VARCHAR2(50);

BEGIN

   -- Calling NEW_ITEM_LOC to insert into item_los_soh for all missing item/location combinations
   if I_new_item_loc_tbl is not NULL and I_new_item_loc_tbl.COUNT > 0 then
      FOR i in I_new_item_loc_tbl.FIRST..I_new_item_loc_tbl.LAST LOOP
         if NEW_ITEM_LOC(O_error_message,
                         I_new_item_loc_tbl(i).item,
                         I_new_item_loc_tbl(i).loc,
                         NULL,
                         NULL,
                         'S',                     -- Loc_type 
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL) = FALSE then
                               
            return FALSE;
         end if;
      END LOOP;
   end if;
   
   -- Bulk insert into ORDCUST table.
   if I_ordcust_tbl is not NULL and I_ordcust_tbl.COUNT > 0 then
      L_table := 'ORDCUST';
      SQL_LIB.SET_MARK('INSERT', NULL, L_table, NULL);
   
      FORALL i in I_ordcust_tbl.FIRST..I_ordcust_tbl.LAST   
         insert into ordcust (ordcust_no,
                              status,
                              order_no,
                              tsf_no,
                              source_loc_type,
                              source_loc_id,
                              fulfill_loc_type,
                              fulfill_loc_id,
                              customer_no,
                              customer_order_no,
                              fulfill_order_no,
                              partial_delivery_ind,
                              delivery_type,
                              carrier_code,
                              carrier_service_code,
                              consumer_delivery_date,
                              consumer_delivery_time,
                              bill_first_name,
                              bill_phonetic_first,
                              bill_last_name,
                              bill_phonetic_last,
                              bill_preferred_name,
                              bill_company_name,
                              bill_add1,
                              bill_add2,
                              bill_add3,
                              bill_county,
                              bill_city,
                              bill_state,
                              bill_country_id,
                              bill_post,
                              bill_jurisdiction,
                              bill_phone,
                              deliver_first_name,
                              deliver_phonetic_first,
                              deliver_last_name,
                              deliver_phonetic_last,
                              deliver_preferred_name,
                              deliver_company_name,
                              deliver_add1,
                              deliver_add2,
                              deliver_add3,
                              deliver_city,
                              deliver_state,
                              deliver_country_id,
                              deliver_post,
                              deliver_county,
                              deliver_jurisdiction,
                              deliver_phone,
                              deliver_charge,
                              deliver_charge_curr,
                              comments,
                              create_datetime,
                              create_id,
                              last_update_datetime,
                              last_update_id)
                      values (I_ordcust_tbl(i).ordcust_no,
                              I_ordcust_tbl(i).status,
                              I_ordcust_tbl(i).order_no,
                              I_ordcust_tbl(i).tsf_no,
                              I_ordcust_tbl(i).source_loc_type,
                              I_ordcust_tbl(i).source_loc_id,
                              I_ordcust_tbl(i).fulfill_loc_type,
                              I_ordcust_tbl(i).fulfill_loc_id,
                              I_ordcust_tbl(i).customer_no,
                              I_ordcust_tbl(i).customer_order_no,
                              I_ordcust_tbl(i).fulfill_order_no,
                              I_ordcust_tbl(i).partial_delivery_ind,
                              I_ordcust_tbl(i).delivery_type,
                              I_ordcust_tbl(i).carrier_code,
                              I_ordcust_tbl(i).carrier_service_code,
                              I_ordcust_tbl(i).consumer_delivery_date,
                              I_ordcust_tbl(i).consumer_delivery_time,
                              I_ordcust_tbl(i).bill_first_name,
                              I_ordcust_tbl(i).bill_phonetic_first,
                              I_ordcust_tbl(i).bill_last_name,
                              I_ordcust_tbl(i).bill_phonetic_last,
                              I_ordcust_tbl(i).bill_preferred_name,
                              I_ordcust_tbl(i).bill_company_name,
                              I_ordcust_tbl(i).bill_add1,
                              I_ordcust_tbl(i).bill_add2,
                              I_ordcust_tbl(i).bill_add3,
                              I_ordcust_tbl(i).bill_county,
                              I_ordcust_tbl(i).bill_city,
                              I_ordcust_tbl(i).bill_state,
                              I_ordcust_tbl(i).bill_country_id,
                              I_ordcust_tbl(i).bill_post,
                              I_ordcust_tbl(i).bill_jurisdiction,
                              I_ordcust_tbl(i).bill_phone,
                              I_ordcust_tbl(i).deliver_first_name,
                              I_ordcust_tbl(i).deliver_phonetic_first,
                              I_ordcust_tbl(i).deliver_last_name,
                              I_ordcust_tbl(i).deliver_phonetic_last,
                              I_ordcust_tbl(i).deliver_preferred_name,
                              I_ordcust_tbl(i).deliver_company_name,
                              I_ordcust_tbl(i).deliver_add1,
                              I_ordcust_tbl(i).deliver_add2,
                              I_ordcust_tbl(i).deliver_add3,
                              I_ordcust_tbl(i).deliver_city,
                              I_ordcust_tbl(i).deliver_state,
                              I_ordcust_tbl(i).deliver_country_id,
                              I_ordcust_tbl(i).deliver_post,
                              I_ordcust_tbl(i).deliver_county,
                              I_ordcust_tbl(i).deliver_jurisdiction,
                              I_ordcust_tbl(i).deliver_phone,
                              I_ordcust_tbl(i).deliver_charges,
                              I_ordcust_tbl(i).deliver_charges_curr,
                              I_ordcust_tbl(i).comments,
                              I_ordcust_tbl(i).create_datetime,
                              I_ordcust_tbl(i).create_id,
                              I_ordcust_tbl(i).last_update_datetime,
                              I_ordcust_tbl(i).last_update_id);
   end if;
  
   -- Bulk insert into ORDCUST_DETAIL table
   if I_ordcust_detail_tbl is not NULL and I_ordcust_detail_tbl.COUNT > 0 then
      L_table := 'ORDCUST_DETAIL';
      SQL_LIB.SET_MARK('INSERT', NULL, L_table, NULL);
   
      FORALL i in  I_ordcust_detail_tbl.FIRST..I_ordcust_detail_tbl.LAST      
         insert into ordcust_detail (ordcust_no,
                                     item,
                                     ref_item,
                                     original_item,
                                     qty_ordered_suom,
                                     qty_cancelled_suom,
                                     standard_uom,
                                     transaction_uom,
                                     substitute_allowed_ind,
                                     unit_retail,
                                     retail_currency_code,
                                     comments,
                                     create_datetime,
                                     create_id,
                                     last_update_datetime,
                                     last_update_id)
                             values (I_ordcust_detail_tbl(i).ordcust_no,
                                     I_ordcust_detail_tbl(i).item,
                                     I_ordcust_detail_tbl(i).ref_item,
                                     I_ordcust_detail_tbl(i).original_item,
                                     I_ordcust_detail_tbl(i).qty_ordered_suom,
                                     I_ordcust_detail_tbl(i).qty_cancelled_suom,
                                     I_ordcust_detail_tbl(i).standard_uom,
                                     I_ordcust_detail_tbl(i).transaction_uom,
                                     I_ordcust_detail_tbl(i).substitute_allowed_ind,
                                     I_ordcust_detail_tbl(i).unit_retail,
                                     I_ordcust_detail_tbl(i).retail_currency_code,
                                     I_ordcust_detail_tbl(i).comments,
                                     I_ordcust_detail_tbl(i).create_datetime,
                                     I_ordcust_detail_tbl(i).create_id,
                                     I_ordcust_detail_tbl(i).last_update_datetime,
                                     I_ordcust_detail_tbl(i).last_update_id);

   end if;
     
   -- To insert into ORDCUST_L10N_EXT table.
   if ORDCUST_ATTRIB_SQL.PERSIST_ORDCUST_L10N_EXT (O_error_message,
                                                   I_l10n_ordcust_ext_tbl) = FALSE then
      return FALSE;
   end if;
   
   -- To do the actual update to item_loc_soh.customer_reserve
   if CUSTOMER_RESERVE_SQL.PROCESS_CO_RESERVE (O_error_message,
                                               I_item_loc_resv_tbl) = FALSE then
      return FALSE;
   end if; 
      
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));   
      return FALSE;

END PERSIST_CREATE_RESERVATION;
------------------------------------------------------------------------------
FUNCTION RELEASE_INVENTORY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id       IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                           I_chunk_id         IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64) := 'CORESVC_FULFILORD_INV.RELEASE_INVENTORY';
   L_table       VARCHAR2(50);
   L_key         VARCHAR2(100);

   L_item_loc_resv_canc_tbl       CUST_ORD_RESERVE_TBL;
   L_ordcust_detail_cancel_tbl    ORDCUST_DETAIL_CANCEL_TBL; 
   
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_SVC_FULFILORDREF is
      select 'x'
        from svc_fulfilordref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and tran_type = LP_tran_type
         and process_status = LP_status_valid
         for update nowait;   
         
   cursor C_LOCK_SVC_FULFILORDDTLREF is
      select 'x'
        from svc_fulfilorddtlref sfrd,
             svc_fulfilordref sfr
       where sfr.process_id = I_process_id  
         and sfr.chunk_id = I_chunk_id
         and sfr.tran_type = LP_tran_type
         and sfrd.fulfilordref_id = sfr.fulfilordref_id
         for update nowait;    
         
BEGIN

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_process_id', L_program, NULL);
      return FALSE;
   end if;
   
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_chunk_id', L_program, NULL);
      return FALSE;
   end if;   

   L_table := 'SVC_FULFILORDREF';
   L_key := 'process_id: '||to_char(I_process_id);
   L_key := L_key || ' chunk_id: '||to_char(I_chunk_id);

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SVC_FULFILORDREF', L_table, L_key);
   open C_LOCK_SVC_FULFILORDREF;

   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SVC_FULFILORDREF', L_table, L_key);
   close C_LOCK_SVC_FULFILORDREF;

   L_table := 'SVC_FULFILORDDTLREF';

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SVC_FULFILORDDTLREF', L_table, L_key);
   open C_LOCK_SVC_FULFILORDDTLREF;

   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SVC_FULFILORDDTLREF', L_table, L_key);
   close C_LOCK_SVC_FULFILORDDTLREF;


   if VALIDATE_RESV_CANCEL(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   if POPULATE_CANCEL_REC(O_error_message,
                          L_ordcust_detail_cancel_tbl,
                          L_item_loc_resv_canc_tbl,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_RESV_CANCEL(O_error_message,
                          L_ordcust_detail_cancel_tbl,
                          L_item_loc_resv_canc_tbl) = FALSE then
      return FALSE;
   end if;

   L_table := 'SVC_FULFILORDREF';
   SQL_LIB.SET_MARK('UDPATE', NULL, L_table, NULL);
   
   update svc_fulfilordref
      set process_status = LP_status_complete, 
          last_update_datetime = SYSDATE,
          last_update_id = LP_user
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and tran_type = LP_tran_type
      and process_status = LP_status_valid;
      
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SVC_FULFILORDREF%ISOPEN then
         close C_LOCK_SVC_FULFILORDREF;
      end if;
      if C_LOCK_SVC_FULFILORDDTLREF%ISOPEN then
         close C_LOCK_SVC_FULFILORDDTLREF;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_LOCKED', L_table, I_process_id, I_chunk_id);
      return FALSE;

   when OTHERS then
      if C_LOCK_SVC_FULFILORDREF%ISOPEN then
         close C_LOCK_SVC_FULFILORDREF;
      end if;
      if C_LOCK_SVC_FULFILORDDTLREF%ISOPEN then
         close C_LOCK_SVC_FULFILORDDTLREF;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));      
      return FALSE;

END RELEASE_INVENTORY;
------------------------------------------------------------------------------
FUNCTION VALIDATE_RESV_CANCEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)        := 'CORESVC_FULFILORD_INV.VALIDATE_RESV_CANCEL';

   L_table                VARCHAR2(50);
   L_key                  VARCHAR2(100);
   L_error_msg            RTK_ERRORS.RTK_TEXT%TYPE;
   
   L_prev_fulfilordref_id SVC_FULFILORDREF.FULFILORDREF_ID%TYPE;

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_GET_SVC_FULFILORDREF is
      select sfr.customer_order_no,
             sfr.fulfill_order_no,
             sfr.source_loc_type,
             sfr.fulfill_loc_type,
             sfr.fulfilordref_id,
             s.store,
             s.stockholding_ind,
            (select 'x' 
                from ordcust oc
               where oc.customer_order_no = sfr.customer_order_no
                 and oc.fulfill_order_no  = sfr.fulfill_order_no 
                 and oc.source_loc_type is null
                 and oc.fulfill_loc_type  = 'S') ordcust_exists
        from svc_fulfilordref sfr,
             store s
       where sfr.process_id = I_process_id
         and sfr.chunk_id = I_chunk_id
         and sfr.tran_type = LP_tran_type
         and sfr.process_status = LP_status_valid
         and sfr.fulfill_loc_id = s.store(+)
         and (    sfr.source_loc_id is NOT NULL 
              OR  sfr.fulfill_loc_type <> 'S'
              OR  s.store is NULL
              OR  s.stockholding_ind <> 'Y'
              OR  not exists (select 1
                                from ordcust oc
                               where oc.customer_order_no = sfr.customer_order_no
                                 and oc.fulfill_order_no = sfr.fulfill_order_no 
                                 and oc.source_loc_type is null
                                 and oc.fulfill_loc_type = 'S'));

  cursor C_GET_SVC_FULFILORDDTLREF is
      select sfdr.rowid,
             sfdr.fulfilordref_id,
             oc.ordcust_no,
             ocd.item ocd_item,
             sfdr.item,
             SUM(ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0)) ocd_out_qty,
             sfdr.cancel_qty_suom,
             sfr.customer_order_no,
             sfr.fulfill_order_no
        from svc_fulfilorddtlref sfdr,
             svc_fulfilordref sfr,
             ordcust_detail ocd,
             ordcust oc
       where sfr.process_id = I_process_id
         and sfr.chunk_id = I_chunk_id
         and sfr.tran_type = LP_tran_type
         and sfr.process_status = LP_status_valid
         and sfr.fulfilordref_id = sfdr.fulfilordref_id 
         and sfr.customer_order_no = oc.customer_order_no
         and sfr.fulfill_order_no = oc.fulfill_order_no
         and oc.source_loc_type is null
         and oc.fulfill_loc_type = 'S'
         and oc.ordcust_no = ocd.ordcust_no
         and sfdr.item = ocd.item
       group by oc.ordcust_no, sfdr.item, sfdr.fulfilordref_id, sfdr.rowid, ocd.item, sfdr.cancel_qty_suom, sfr.customer_order_no, sfr.fulfill_order_no
      having SUM(ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0)) < sfdr.cancel_qty_suom
      order by sfdr.fulfilordref_id;
         
   
   TYPE SVC_FULFILORDREF_TBL is TABLE of C_GET_SVC_FULFILORDREF%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_fulfilordref_tbl      SVC_FULFILORDREF_TBL;

   TYPE SVC_FULFILORDDTLREF_TBL is TABLE of C_GET_SVC_FULFILORDDTLREF%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_fulfilorddtlref_tbl   SVC_FULFILORDDTLREF_TBL;

BEGIN

   L_table := 'SVC_FULFILORDREF, ORDCUST';
   L_key := 'process_id: '||to_char(I_process_id);
   L_key := L_key || ' chunk_id: '||to_char(I_chunk_id);

   SQL_LIB.SET_MARK('OPEN', 'C_GET_SVC_FULFILORDREF', L_table, L_key);
   open C_GET_SVC_FULFILORDREF;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SVC_FULFILORDREF', L_table, L_key);
   fetch C_GET_SVC_FULFILORDREF bulk collect into L_svc_fulfilordref_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SVC_FULFILORDREF', L_table, L_key);
   close C_GET_SVC_FULFILORDREF;
   
   if L_svc_fulfilordref_tbl is not NULL and L_svc_fulfilordref_tbl.COUNT > 0 then
      L_table := 'SVC_FULFILORDREF';
      SQL_LIB.SET_MARK('UDPATE', NULL, L_table, NULL);

      FORALL i in L_svc_fulfilordref_tbl.first..L_svc_fulfilordref_tbl.last
         -- Updating the error message for any invalid header records.
         -- The error conditions are dependent on each other and only one error should be logged because subsequent errors will be cause of previous one. 
         update svc_fulfilordref  
            set error_msg = error_msg  || 
                            case 
                              when (L_svc_fulfilordref_tbl(i).fulfill_loc_type <> 'S' or L_svc_fulfilordref_tbl(i).source_loc_type is not NULL) then
                                 SQL_LIB.GET_MESSAGE_TEXT('INV_RESV_CANC_STG_REC',
                                                          L_svc_fulfilordref_tbl(i).customer_order_no, 
                                                          L_svc_fulfilordref_tbl(i).fulfill_order_no,
                                                          NULL) || ';'
                              when (L_svc_fulfilordref_tbl(i).store is NULL 
                                 or NVL(L_svc_fulfilordref_tbl(i).stockholding_ind,'N') <> 'Y') then
                                 SQL_LIB.GET_MESSAGE_TEXT('INV_CUST_ORD_STORE',             
                                                          L_svc_fulfilordref_tbl(i).customer_order_no, 
                                                          L_svc_fulfilordref_tbl(i).fulfill_order_no,
                                                          NULL) || ';'
                              when L_svc_fulfilordref_tbl(i).ordcust_exists is NULL then
                                 SQL_LIB.GET_MESSAGE_TEXT('INV_CUST_ORDER',
                                                          L_svc_fulfilordref_tbl(i).customer_order_no, 
                                                          L_svc_fulfilordref_tbl(i).fulfill_order_no,
                                                          NULL) || ';'
                             end,
                process_status = LP_status_error,
                last_update_datetime = SYSDATE,
                last_update_id = LP_user                
          where fulfilordref_id = L_svc_fulfilordref_tbl(i).fulfilordref_id;

      -- Set O_Error_message as the first validation error
      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_msg
        from svc_fulfilordref
       where fulfilordref_id = L_svc_fulfilordref_tbl(1).fulfilordref_id
         and rownum = 1;

      O_error_message := L_error_msg;

      return FALSE;
   end if;

   L_table := 'SVC_FULFILORDDTLREF, ORDCUST_DETAIL';

   SQL_LIB.SET_MARK('OPEN', 'C_GET_SVC_FULFILORDDTLREF', L_table, L_key);
   open C_GET_SVC_FULFILORDDTLREF;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SVC_FULFILORDDTLREF', L_table, L_key);
   fetch C_GET_SVC_FULFILORDDTLREF bulk collect into L_svc_fulfilorddtlref_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SVC_FULFILORDDTLREF', L_table, L_key);
   close C_GET_SVC_FULFILORDDTLREF;
   
   if L_svc_fulfilorddtlref_tbl is not NULL and L_svc_fulfilorddtlref_tbl.COUNT > 0 then
      L_table := 'SVC_FULFILORDDTLREF';
      SQL_LIB.SET_MARK('UDPATE', NULL, L_table, NULL);

      FORALL i in L_svc_fulfilorddtlref_tbl.first..L_svc_fulfilorddtlref_tbl.last
         update svc_fulfilorddtlref  
            set error_msg = error_msg || SQL_LIB.GET_MESSAGE_TEXT('INV_RESV_CANC_QTY',
                                                                  L_svc_fulfilorddtlref_tbl(i).customer_order_no, 
                                                                  L_svc_fulfilorddtlref_tbl(i).fulfill_order_no, 
                                                                  L_svc_fulfilorddtlref_tbl(i).item)||';',
                last_update_datetime = SYSDATE,
                last_update_id = LP_user                
          where rowid = L_svc_fulfilorddtlref_tbl(i).rowid;

      -- Update the header svc_fulfilordref table status and error message for any detail error. 
      L_prev_fulfilordref_id := -1;
      L_table := 'SVC_FULFILORDREF';
      SQL_LIB.SET_MARK('UDPATE', NULL, L_table, NULL);
      
      FOR i in L_svc_fulfilorddtlref_tbl.first..L_svc_fulfilorddtlref_tbl.last LOOP
         -- This update can't be inside the loop. In case there are multiple items to the same customer order, same message will be appended multiple times. 
         if L_prev_fulfilordref_id <> L_svc_fulfilorddtlref_tbl(i).fulfilordref_id then
            update svc_fulfilordref  
               set process_status = LP_status_error,
                   error_msg = error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_DETAIL', 
                                                                     L_svc_fulfilorddtlref_tbl(i).customer_order_no, 
                                                                     L_svc_fulfilorddtlref_tbl(i).fulfill_order_no, 
                                                                     NULL)||';',
                   last_update_datetime = SYSDATE,
                   last_update_id = LP_user                
             where fulfilordref_id = L_svc_fulfilorddtlref_tbl(i).fulfilordref_id;

            L_prev_fulfilordref_id := L_svc_fulfilorddtlref_tbl(i).fulfilordref_id;

         end if;

      END LOOP;
      
      -- Set O_Error_message as the first validation error
      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_msg
        from svc_fulfilorddtlref
       where rowid = L_svc_fulfilorddtlref_tbl(1).rowid
         and rownum = 1;

      O_error_message := L_error_msg;
      
      return FALSE;
   end if;     
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_SVC_FULFILORDREF%ISOPEN then
         close C_GET_SVC_FULFILORDREF;
      end if;
      if C_GET_SVC_FULFILORDDTLREF%ISOPEN then
         close C_GET_SVC_FULFILORDDTLREF;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_LOCKED', L_table, I_process_id, I_chunk_id);
      return FALSE;
   when OTHERS then
      if C_GET_SVC_FULFILORDREF%ISOPEN then
         close C_GET_SVC_FULFILORDREF;
      end if;
      if C_GET_SVC_FULFILORDDTLREF%ISOPEN then
         close C_GET_SVC_FULFILORDDTLREF;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));      
      return FALSE;

END VALIDATE_RESV_CANCEL;
------------------------------------------------------------------------------
FUNCTION POPULATE_CANCEL_REC(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ordcust_detail_cancel_tbl   IN OUT   ORDCUST_DETAIL_CANCEL_TBL,
                             O_item_loc_resv_canc_tbl      IN OUT   CUST_ORD_RESERVE_TBL,
                             I_process_id                  IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id                    IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'CORESVC_FULFILORD_INV.POPULATE_CANCEL_REC';
   
   L_table             VARCHAR2(50);
   L_key               VARCHAR2(100);
   L_prev_ordcust_no   NUMBER(12);
   L_prev_item         VARCHAR2(25);
   L_out_canc_qty      NUMBER(12,4);
   
   cursor C_GET_ORDCUST_DETAIL_CANCEL is
      select ORDCUST_DETAIL_CANCEL_REC
             (ocd.ordcust_no,
             ocd.item,
             ocd.original_item,
             ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0),   -- Outstanding order quantity
             0,                                                      -- Cancel qty - will be calculated and updated later
             sfd.cancel_qty_suom)                                    -- Total Cancelled quantity
        from svc_fulfilordref sf,
             svc_fulfilorddtlref sfd,
             ordcust oc,
             ordcust_detail ocd
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.tran_type = LP_tran_type
         and sf.process_status = LP_status_valid
         and sf.fulfilordref_id = sfd.fulfilordref_id 
         and oc.customer_order_no = sf.customer_order_no
         and oc.fulfill_order_no = sf.fulfill_order_no
         and oc.fulfill_loc_id = sf.fulfill_loc_id
         and NVL(oc.source_loc_id,-999) = NVL(sf.source_loc_id,-999)
         and oc.ordcust_no = ocd.ordcust_no
         and ocd.item = sfd.item
         and ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0) > 0
    order by ocd.ordcust_no, 
             ocd.item, 
             (ocd.qty_ordered_suom - NVL(ocd.qty_cancelled_suom,0)) desc;

   -- To populate item_loc_resv collection to be used for updating item_loc_soh table.
   -- Quantity is multiplied with -1 so the cancelled quantity be subtracted from the reserved quantity.
   -- For pack item, the components reservation quantity is being fetched because the reservation 
   -- is only for store location where pack inventory is not being maintained.
   cursor C_ITEM_LOC_RESV_CANC is
      select CUST_ORD_RESERVE_REC
             (canc_tbl.item,
             oc.fulfill_loc_id,
             -1 * canc_tbl.qty_cancelled_suom)           -- Reservation cancellation quantity
        from item_master im,
             ordcust oc,
             table(cast(O_ordcust_detail_cancel_tbl as ORDCUST_DETAIL_CANCEL_TBL)) canc_tbl
       where oc.ordcust_no = canc_tbl.ordcust_no
         and im.item = canc_tbl.item
         and im.pack_ind = 'N'
      UNION ALL   
      select CUST_ORD_RESERVE_REC
             (vpq.item,                                    -- Component item
             oc.fulfill_loc_id,               
             (-1 * vpq.qty * canc_tbl.qty_cancelled_suom)) -- Component reservation cancellation quantity
        from item_master im,
             ordcust oc,
             v_packsku_qty vpq,
             TABLE(CAST(O_ordcust_detail_cancel_tbl AS ORDCUST_DETAIL_CANCEL_TBL)) canc_tbl
       where oc.ordcust_no = canc_tbl.ordcust_no
         and vpq.pack_no = im.item
         and im.item = canc_tbl.item
         and im.pack_ind = 'Y';
      
BEGIN   

   L_table := 'ORDCUST_DETAIL';
   L_key := 'process_id: '||to_char(I_process_id);
   L_key := L_key || ' chunk_id: '||to_char(I_chunk_id);

   SQL_LIB.SET_MARK('OPEN', 'C_GET_ORDCUST_DETAIL_CANCEL', L_table, L_key);
   open C_GET_ORDCUST_DETAIL_CANCEL;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_ORDCUST_DETAIL_CANCEL', L_table, L_key);
   fetch C_GET_ORDCUST_DETAIL_CANCEL bulk collect into O_ordcust_detail_cancel_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORDCUST_DETAIL_CANCEL', L_table, L_key);
   close C_GET_ORDCUST_DETAIL_CANCEL;

   if O_ordcust_detail_cancel_tbl is not NULL and O_ordcust_detail_cancel_tbl.COUNT > 0 then
      L_prev_ordcust_no := -1;
      L_prev_item       := '-999';
      L_out_canc_qty    := 0;

      -- There can be multiple records in ordcust_detail for the same ordcust_no and item to support substitute item. 
      -- The total cancel quantity is split across these differnet rows based on individual row outstanding quantities
      for i in O_ordcust_detail_cancel_tbl.first..O_ordcust_detail_cancel_tbl.last 
      LOOP
         if O_ordcust_detail_cancel_tbl(i).ordcust_no <> L_prev_ordcust_no or O_ordcust_detail_cancel_tbl(i).item <> L_prev_item then
            L_prev_ordcust_no := O_ordcust_detail_cancel_tbl(i).ordcust_no;
            L_prev_item       := O_ordcust_detail_cancel_tbl(i).item;
            L_out_canc_qty    := O_ordcust_detail_cancel_tbl(i).cancel_qty_suom;
         end if;

         if L_out_canc_qty > 0 then
            if O_ordcust_detail_cancel_tbl(i).qty_outstanding <= L_out_canc_qty then
               O_ordcust_detail_cancel_tbl(i).qty_cancelled_suom :=  O_ordcust_detail_cancel_tbl(i).qty_outstanding;
            else
               O_ordcust_detail_cancel_tbl(i).qty_cancelled_suom :=  L_out_canc_qty;
            end if;
            L_out_canc_qty := L_out_canc_qty - O_ordcust_detail_cancel_tbl(i).qty_cancelled_suom; 
         end if;
      END LOOP;
   end if;
   
   -- Populate item_loc_resv_canc collection to update item_loc_soh table.
   L_table := 'ITEM_MASTER, V_PACKSKU_QTY';

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM_LOC_RESV_CANC', L_table, L_key);
   open C_ITEM_LOC_RESV_CANC;

   SQL_LIB.SET_MARK('FETCH', 'C_ITEM_LOC_RESV_CANC', L_table, L_key);
   fetch C_ITEM_LOC_RESV_CANC bulk collect into O_item_loc_resv_canc_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_LOC_RESV_CANC', L_table, L_key);
   close C_ITEM_LOC_RESV_CANC;
 
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_ORDCUST_DETAIL_CANCEL%ISOPEN then
         close C_GET_ORDCUST_DETAIL_CANCEL;
      end if;
      if C_ITEM_LOC_RESV_CANC%ISOPEN then
         close C_ITEM_LOC_RESV_CANC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));      
      return FALSE;

END POPULATE_CANCEL_REC;
------------------------------------------------------------------------------
FUNCTION PERSIST_RESV_CANCEL(O_error_message               IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ordcust_detail_cancel_tbl   IN      ORDCUST_DETAIL_CANCEL_TBL,
                             I_item_loc_resv_tbl           IN      CUST_ORD_RESERVE_TBL)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'CORESVC_FULFILORD_INV.PERSIST_RESV_CANCEL';
   L_table               VARCHAR2(50);   
   
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDCUST_DETAIL is
      select 'x'
        from ordcust_detail ocd,
             TABLE(CAST(I_ordcust_detail_cancel_tbl AS ORDCUST_DETAIL_CANCEL_TBL)) canc_tbl
       where ocd.ordcust_no = canc_tbl.ordcust_no
         and ocd.item = canc_tbl.item
         and NVL(ocd.original_item,'-999') = NVL(canc_tbl.original_item,'-999')
       for update of ocd.qty_cancelled_suom nowait;      
BEGIN

   if I_ordcust_detail_cancel_tbl is not NULL and I_ordcust_detail_cancel_tbl.COUNT > 0 then
      L_table := 'ORDCUST_DETAIL';

      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ORDCUST_DETAIL', L_table, NULL);
      open C_LOCK_ORDCUST_DETAIL;

      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ORDCUST_DETAIL', L_table, NULL);
      close C_LOCK_ORDCUST_DETAIL;
   
      SQL_LIB.SET_MARK('UDPATE', NULL, L_table, NULL);
      
      FORALL i in  I_ordcust_detail_cancel_tbl.FIRST..I_ordcust_detail_cancel_tbl.LAST 
         update ordcust_detail  
            set qty_cancelled_suom = NVL(qty_cancelled_suom,0) + I_ordcust_detail_cancel_tbl(i).qty_cancelled_suom,
                last_update_datetime = SYSDATE,
                last_update_id = LP_user                
          where ordcust_no = I_ordcust_detail_cancel_tbl(i).ordcust_no
            and item = I_ordcust_detail_cancel_tbl(i).item
            and NVL(original_item,'-999') = NVL(I_ordcust_detail_cancel_tbl(i).original_item,'-999');
   end if;
  
   if CUSTOMER_RESERVE_SQL.PROCESS_CO_RESERVE (O_error_message,
                                               I_item_loc_resv_tbl) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ORDCUST_DETAIL%ISOPEN then
         close C_LOCK_ORDCUST_DETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK', L_table, NULL, NULL);
      return FALSE;

   when OTHERS then
      if C_LOCK_ORDCUST_DETAIL%ISOPEN then
         close C_LOCK_ORDCUST_DETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));      
      return FALSE;

END PERSIST_RESV_CANCEL;
------------------------------------------------------------------------------

END CORESVC_FULFILORD_INV;
/