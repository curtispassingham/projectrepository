CREATE OR REPLACE PACKAGE WF_ORDER_DETAIL_SQL AUTHID CURRENT_USER AS
--****************************************************************************************************
--- This package will serve as the base table for the WFORDER.FMB form's multi-record block.         *
--- It will have procedures to get information for each of the views available in this block in      *
--- addition to procedures for inserting, updating, and deleting WF_ORDER_INFORMATION.               *
--****************************************************************************************************
---------------------------------------------------------------------------------------------------
TYPE wf_order_detail_record IS RECORD(wf_order_no               WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                                      item                      WF_ORDER_DETAIL.ITEM%TYPE,
                                      item_desc                 ITEM_MASTER.ITEM_DESC%TYPE,
                                      source_loc_type           WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                      source_loc_id             V_WH.WH%TYPE,
                                      cust_loc                  WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                                      need_date                 WF_ORDER_DETAIL.NEED_DATE%TYPE,
                                      not_after_date            WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE,
                                      uom                       UOM_CLASS.UOM%TYPE,
                                      requested_qty             WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                                      available_qty             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                      customer_cost_vat_incl    WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                      total_cust_cost_vat_incl  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                      source_loc_id_name        V_WH.WH_NAME%TYPE,
                                      cust_loc_name             STORE.STORE_NAME%TYPE,
                                      ref_item                  WF_ORDER_DETAIL.ITEM%TYPE,
                                      item_type                 CODE_DETAIL.CODE_TYPE%TYPE,
                                      cancel_reason             WF_ORDER_DETAIL.CANCEL_REASON%TYPE,
                                      acquisition_cost          WF_ORDER_DETAIL.ACQUISITION_COST%TYPE,
                                      customer_cost_vat_excl    WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                      total_cust_cost_vat_excl  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                      vat_rate                  VAT_CODE_RATES.VAT_RATE%TYPE,
                                      vat_amt                   WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                      margin_pct                WF_ORDER_DETAIL.MARGIN_PCT%TYPE,
                                      linked_tsfpoalloc         TSFHEAD.TSF_NO%TYPE,
                                      linked_entity_status      TSFHEAD.STATUS%TYPE,
                                      fixed_cost                WF_ORDER_DETAIL.FIXED_COST %TYPE,
                                      order_currency            WF_ORDER_HEAD.CURRENCY_CODE%TYPE,
                                      ship_date                 SHIPMENT.SHIP_DATE%TYPE,
                                      ship_qty                  SHIPSKU.QTY_RECEIVED%TYPE,
                                      row_id                    VARCHAR2(25),
                                      return_code               VARCHAR2(10),
                                      error_message             RTK_ERRORS.RTK_TEXT%TYPE,
                                      pack_ind                  ITEM_MASTER.PACK_IND%TYPE,
                                      wf_order_line_no          WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE);

TYPE wf_order_detail_tab IS TABLE OF wf_order_detail_record INDEX BY BINARY_INTEGER;

TYPE item_sub_rec is RECORD (sub_item        SUB_ITEMS_DETAIL.SUB_ITEM%TYPE,
                             item_desc       ITEM_MASTER.ITEM_DESC%TYPE,
                             uom             UOM_CLASS.UOM%TYPE,
                             available_qty   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             error_message   RTK_ERRORS.RTK_TEXT%TYPE);

TYPE item_sub_tbl is TABLE OF item_sub_rec INDEX BY BINARY_INTEGER;

TYPE item_dist_rec is RECORD (item_parent    WF_ORDER_DETAIL.ITEM%TYPE,
                             item            WF_ORDER_DETAIL.ITEM%TYPE,
                             item_desc       ITEM_MASTER.ITEM_DESC%TYPE,
                             diff_1          ITEM_MASTER.DIFF_1%TYPE,
                             diff_2          ITEM_MASTER.DIFF_2%TYPE,
                             diff_3          ITEM_MASTER.DIFF_3%TYPE,
                             diff_4          ITEM_MASTER.DIFF_4%TYPE,
                             error_message   RTK_ERRORS.RTK_TEXT%TYPE);

TYPE item_dist_tbl is TABLE OF item_dist_rec INDEX BY BINARY_INTEGER;

TYPE buyer_pack_rec is RECORD (pack_no         ITEM_MASTER.ITEM%TYPE,
                               item            ITEM_MASTER.ITEM%TYPE,
                               item_desc       ITEM_MASTER.ITEM_DESC%TYPE,
                               qty             WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                               error_message   RTK_ERRORS.RTK_TEXT%TYPE);

TYPE buyer_pack_tbl is TABLE OF buyer_pack_rec INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
--- Procedure Name: GET_DEFAULT_WF_ORDER_INFO
--- Purpose       : This procedure will query WF_ORDER_DETAIL and ITEM_MASTER table and populate
---                 the above declared table of records that will be used as the 'base table' in
---                 the wf order form.
---------------------------------------------------------------------------------------------------
PROCEDURE GET_DEFAULT_WF_ORDER_INFO (O_wf_order_det   IN OUT   WF_ORDER_DETAIL_TAB,
                                     I_wf_order_id    IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE);
---------------------------------------------------------------------------------------------------
--- Function Name: INSERT_ORDER_INFO
--- Purpose      : This procedure is responsible for inserting information on a franchise order.
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDER_INFO (IO_wf_order_rec   IN OUT   WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- Procedure Name: LOCK_PROCEDURE
--- Purpose       : This procedure will lock records on ITEM_LOC_SOH table.
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE (wf_order_det   IN OUT   wf_order_detail_tab);
---------------------------------------------------------------------------------------------------
--- Procedure Name: DELETE_WF_ORDER_DETAIL
--- Purpose       : This procedure is responsible for deleting information on a franchise
---                 order Details. Record deletion can only occur when an order is in 'I'nput status.
---------------------------------------------------------------------------------------------------
PROCEDURE DELETE_WF_ORDER_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_return_code     IN OUT   VARCHAR2,
                                 I_wf_order_rec    IN       WF_ORDER_DETAIL_TAB);
---------------------------------------------------------------------------------------------------
--- Procedure Name: UPDATE_PROCEDURE
--- Purpose       : This procedure will update WF_ORDER_DETAIL table by rowid for the modified values.
---------------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_return_code     IN OUT   VARCHAR2,
                            I_wf_order_rec    IN       WF_ORDER_DETAIL_TAB);
---------------------------------------------------------------------------------------------------
--- Function Name: WF_ORDER_DETAIL_CREATE
--- Purpose      : This procedure will be called to insert a new detail line. This will validate the 
---                input data and insert record in wf_order_detail and also get the lined PO and TSF
---                updated. 
---------------------------------------------------------------------------------------------------
FUNCTION WF_ORDER_DETAIL_CREATE (IO_wf_order_rec   IN OUT   WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD)
RETURN BOOLEAN ;
---------------------------------------------------------------------------------------------------
--- Function Name: WF_ORDER_DETAIL_UPDATE
--- Purpose      : This procedure is responsible for validating the Franchise order during editing
---------------------------------------------------------------------------------------------------
FUNCTION WF_ORDER_DETAIL_UPDATE (IO_wf_order_rec   IN OUT   WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD,
                                 I_status          IN       WF_ORDER_HEAD.STATUS%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- Procedure Name: GET_ITEM_SUBSTITUTE_INFO
--- Purpose       : This procedure will query SUB_ITEM_DETAILS, ITEM_LOC_SOH and V_ITEM_MASTER
---                 and populate the above declared table of records that will be used as the
---                'base table' in wforder form for Item Substitute screen.
---------------------------------------------------------------------------------------------------
PROCEDURE GET_ITEM_SUBSTITUTE_INFO (O_item_sub_tbl     IN OUT   ITEM_SUB_TBL,
                                    O_item_count_flag  IN OUT   VARCHAR2,
                                    I_item             IN       WF_ORDER_DETAIL.ITEM%TYPE,
                                    I_source_loc_type  IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                    I_source_loc_id    IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE);
---------------------------------------------------------------------------------------------------
--- Procedure Name: GET_ITEM_DISTRIBUTE_INFO
--- Purpose       : This procedure will support the distribute screen for item list and parent item. 
---------------------------------------------------------------------------------------------------
PROCEDURE GET_ITEM_DISTRIBUTE_INFO (O_item_dist_tbl    IN OUT   ITEM_DIST_TBL,
                                    O_item_count_flag  IN OUT   VARCHAR2,
                                    I_item             IN       WF_ORDER_DETAIL.ITEM%TYPE,
                                    I_item_type        IN       VARCHAR2,
                                    I_source_loc_type  IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                    I_source_loc_id    IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE);
--------------------------------------------------------------------------------------------------
PROCEDURE LOCK_DIST_PROCEDURE  (O_item_dist_tbl   IN OUT   ITEM_DIST_TBL);
---------------------------------------------------------------------------------------------------
--- Procedure Name: WF_CANCEL_HEAD
--- Purpose       : This function will be called for header level frachise order cancellation. This 
---                 will cancel the outstanding quantity from the order and also update
---                 the linked transfer, allocation and purchase order.
---------------------------------------------------------------------------------------------------
FUNCTION WF_CANCEL_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                         I_cancel_reason   IN       WF_ORDER_DETAIL.CANCEL_REASON%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- Procedure Name: WF_CANCEL_LINE
--- Purpose       : This function will cancel the outstanding quantity from the order and also update
---                 the linked transfer, allocation and purchase order.
---------------------------------------------------------------------------------------------------
FUNCTION WF_CANCEL_LINE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_cancel_allowed     IN OUT BOOLEAN,
                        O_open_cancel_qty    IN OUT WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                        I_new_qty            IN     WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                        I_old_qty            IN     WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                        I_item               IN     WF_ORDER_DETAIL.ITEM%TYPE,
                        I_source_loc_type    IN     WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                        I_source_loc_id      IN     WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                        I_customer_loc       IN     WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                        I_need_date          IN     WF_ORDER_DETAIL.NEED_DATE%TYPE,
                        I_wf_order_no        IN     WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                        I_wf_order_line_no   IN     WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE,
                        I_header_cancel_ind  IN     VARCHAR2,
                        I_cancel_reason      IN     WF_ORDER_DETAIL.CANCEL_REASON%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- FUNCTION Name: CHECK_ITEM_LIST_SUOM_CLASS
--- Purpose      : This function checks if all items in a item list has the same UOM class.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LIST_SUOM_CLASS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_itemlist        IN  SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- FUNCTION Name: CHECK_LINKED_PO
--- Purpose      : This function checks if there is a  linked purchase order for the franchise order.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_LINKED_PO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_po_exists         IN OUT   BOOLEAN,
                         I_wf_order_no       IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                         I_wf_order_line_no  IN       WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- FUNCTION Name: WF_DUP_ORDER_LINE_EXISTS
--- Purpose      : This function checks if newly added record is duplicate of an existing line.
---------------------------------------------------------------------------------------------------
FUNCTION WF_DUP_ORDER_LINE_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists          IN OUT  BOOLEAN,
                                   I_order_no        IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                   I_cust_loc        IN      WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                                   I_source_loc      IN      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                                   I_source_loc_type IN      WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                   I_item            IN      WF_ORDER_DETAIL.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- FUNCTION Name: GET_SHIPPED_QUANTITY
--- Purpose      : This function returns the shipped quantity the input order line.
---------------------------------------------------------------------------------------------------
FUNCTION GET_SHIPPED_QUANTITY (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_shipped_qty     IN OUT  TSFDETAIL.SHIP_QTY%TYPE,
                               I_wf_order_no     IN      WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                               I_source_loc_type IN      WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                               I_source_loc_id   IN      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                               I_customer_loc    IN      WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                               I_need_date       IN      WF_ORDER_DETAIL.NEED_DATE%TYPE,
                               I_item            IN      WF_ORDER_DETAIL.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- FUNCTION Name: ROUND_F_ORDER
--- Purpose      : This function rounds the quantity for supplier sourced based on rounding level setup.
---------------------------------------------------------------------------------------------------
FUNCTION ROUND_F_ORDER(IO_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_out_qty             IN OUT  WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                       O_rounded_ind         IN OUT  VARCHAR2,
                       I_item                IN      WF_ORDER_DETAIL.ITEM%TYPE,
                       I_supplier            IN      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                       I_origin_country      IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_in_qty              IN      WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                       I_uom_size            IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
 RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--- Procedure Name: GET_BUYER_PACK_INFO
--- Purpose       : This procedure will query V_PACKSKU_QTY, V_ITEM_MASTER
---                 and populate the Buyer pack table of records that will be used as the
---                'base table' in wforder form for exploding buyer pack (orderable as eaches).
---------------------------------------------------------------------------------------------------
PROCEDURE GET_BUYER_PACK_INFO(O_buyer_pack_tbl   IN OUT   BUYER_PACK_TBL,
                              O_item_count_flag  IN OUT   VARCHAR2,
                              I_item             IN       WF_ORDER_DETAIL.ITEM%TYPE,
                              I_source_loc_type  IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                              I_source_loc_id    IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                              I_requested_qty    IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE);
--------------------------------------------------------------------------------------------------
--- Procedure Name: LOCK_BUYER_PACK_PROCEDURE
--- Purpose       : This procedure is there because forms need to call a lock procudure.
--------------------------------------------------------------------------------------------------
PROCEDURE LOCK_BUYER_PACK_PROCEDURE  (O_buyer_pack_tbl IN OUT   BUYER_PACK_TBL);
---------------------------------------------------------------------------------------------------
-- Function   : CHECK_ITEM_RANGED_IND
-- Purpose    : This function returns FALSE for O_ranged parameter if there is atleast one item in 
--              a franchise order with RANGED_IND = 'N' at to location or costing location. 
--              If RANGED_IND='Y' for all item-location combinations,O_ranged will return TRUE.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_RANGED_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_ranged            IN OUT   BOOLEAN,
                               I_wf_order_no       IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function   : CHANGE_ITEM_RANGED_IND
-- Purpose    : This function changes RANGED_IND to 'Y' for all items in a franchise orders having 
--              the indicator as 'N' at to location or costing location.
---------------------------------------------------------------------------------------------------
FUNCTION CHANGE_ITEM_RANGED_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no       IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function   : WF_ORDER_DETAIL_CREATE_WRP
-- Purpose    : This function is wrapper function for calling the WF_ORDER_DETAIL_CREATE.
---------------------------------------------------------------------------------------------------
FUNCTION WF_ORDER_DETAIL_CREATE_WRP (IO_wf_order_rec    IN OUT    WRP_WF_ORDER_DETAIL_REC)
RETURN INTEGER;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--- Function Name: WF_ORDER_DETAIL_UPDATE_WRP
--- Purpose      : This function is wrapper for calling the WF_ORDER_DETAIL_UPDATE
---------------------------------------------------------------------------------------------------
FUNCTION WF_ORDER_DETAIL_UPDATE_WRP (IO_wf_order_rec   IN OUT   WRP_WF_ORDER_DETAIL_REC,
                                     I_status          IN       WF_ORDER_HEAD.STATUS%TYPE)
RETURN INTEGER;
---------------------------------------------------------------------------------------------------
--- Function Name: INSERT_ORDER_INFO_WRP
--- Purpose      : This funciton is wrapper for calling INSERT_ORDER_INFO
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDER_INFO_WRP (IO_wf_order_rec   IN OUT   WRP_WF_ORDER_DETAIL_REC)
RETURN INTEGER;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--- Procedure Name: DELETE_WF_ORDER_DETAIL_WRP
--- Purpose       : This funciton is wrapper for calling DELETE_WF_ORDER_DETAIL procedure.
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_WF_ORDER_DETAIL_WRP(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_return_code            IN OUT   VARCHAR2,
                                    I_wf_order_detail_tab    IN       WRP_WF_ORDER_DETAIL_TAB)
RETURN INTEGER;
---------------------------------------------------------------------------------------------------
--- Procedure Name: UPDATE_PROCEDURE_WRP
--- Purpose       : This function is wrapper for calling UPDATE_PROCEDURE
---------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PROCEDURE_WRP (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_return_code            IN OUT   VARCHAR2,
                               I_wf_order_detail_tab    IN       WRP_WF_ORDER_DETAIL_TAB)
RETURN INTEGER;
---------------------------------------------------------------------------------------------------
END WF_ORDER_DETAIL_SQL;
/
