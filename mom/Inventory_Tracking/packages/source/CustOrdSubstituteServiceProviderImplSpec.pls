/******************************************************************************
* Service Name     : CustOrdSubstituteService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/CustOrdSubstituteService/v1
* Description      : 
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE CUSTORDSUBSTITUTESERVICEPROVID AUTHID CURRENT_USER AS


/******************************************************************************
 *
 * Operation       : createCustOrdSubColDesc
 * Description     : Create new Customer Orders item Substitute in RMS for a customer order 
                        for requests from an external Order Management System (OMS).
             
 * 
 * Input           : "RIB_CustOrdSubColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/CustOrdSubColDesc/v1
 * Description     :  CustOrdSubColDesc object holds multiple customer order 
                    item substitute requests from OMS. 
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     :  InvocationSuccess object contains the sucess or failure status of processing the 
                              confirmation requests.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
                    message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
                    "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createCustOrdSubColDesc(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_CustOrdSubColDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_InvocationSuccess_REC"
                          );
/******************************************************************************/



END CustOrdSubstituteServiceProvid;
/
 