CREATE OR REPLACE PACKAGE BODY RMSSUB_INVADJUST AS

$if $$UTPLSQL = FALSE or $$UTPLSQL is NULL $then
/* Declare private procedure and functions.*/
----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   VARCHAR2,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
----------------------------------------------------------------------------------------
FUNCTION PROCESS_INVADJ (O_error_message   IN OUT   VARCHAR2,
                         I_message         IN       "RIB_InvAdjustDesc_REC")
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   VARCHAR2,
                             O_orderable_TBL   OUT      "RIB_InvAdjustDtl_TBL",
                             I_sellable_TBL    IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS(O_error_message   IN OUT   VARCHAR2,
                     O_sellable_TBL    OUT      "RIB_InvAdjustDtl_TBL",
                     O_detail_TBL      OUT      "RIB_InvAdjustDtl_TBL",
                     I_rib_detail_TBL  IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION CONSUME_INVADJ_CORE(O_error_message   IN OUT   VARCHAR2,
                             I_message         IN       RIB_OBJECT,
                             I_message_type    IN       VARCHAR2)
RETURN BOOLEAN;

$end
----------------------------------------------------------------------------------------
/* Function and Procedure Bodies */
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE --
----------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code       IN OUT   VARCHAR2,
                  O_error_message     IN OUT   VARCHAR2,
                  I_message           IN       RIB_OBJECT,
                  I_message_type      IN       VARCHAR2)
IS
   L_program        VARCHAR2(255) := 'RMSSUB_INVADJUST.CONSUME';

   L_check_l10n_ind VARCHAR2(1) := 'Y';

   PROGRAM_ERROR  EXCEPTION;

   L_event_run_type           COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;

   cursor C_GET_RUN_TYPE is 
        select event_run_type 
           from cost_event_run_type_config 
         where event_type='NIL';  
   
BEGIN

   FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE:=NULL;

   open C_GET_RUN_TYPE;
   fetch C_GET_RUN_TYPE into L_event_run_type;
   close C_GET_RUN_TYPE;

   if L_event_run_type = FUTURE_COST_EVENT_SQL.SYNC_COST_EVENT_RUN_TYPE then
     FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE := FUTURE_COST_EVENT_SQL.ASYNC_COST_EVENT_RUN_TYPE;
   end if;

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- Perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Check message type
   if I_message_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   CONSUME_INVADJ(O_status_code,
                  O_error_message,
                  I_message,
                  I_message_type,
                  L_check_l10n_ind);  -- 'Y'

   if O_status_code != API_CODES.SUCCESS then
      raise PROGRAM_ERROR;
   end if;

   FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE:=NULL;   
   
   return;

EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME;
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE --
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME_INVADJ(O_status_code       IN OUT  VARCHAR2,
                         O_error_message     IN OUT  VARCHAR2,
                         I_message           IN      RIB_OBJECT,
                         I_message_type      IN      VARCHAR2,
                         I_check_l10n_ind    IN      VARCHAR2)
IS
   L_program          VARCHAR2(255) := 'RMSSUB_INVADJUST.CONSUME';

   L_rib_invadjustdesc_rec    "RIB_InvAdjustDesc_REC";
   L_l10n_rib_rec             "L10N_RIB_REC" := L10N_RIB_REC();

   PROGRAM_ERROR     EXCEPTION;

BEGIN
   --
   L_l10n_rib_rec.rib_msg         :=  I_message;
   L_l10n_rib_rec.rib_msg_type    :=  I_message_type;
   L_l10n_rib_rec.procedure_key   := 'CONSUME_INVADJ';
   -- 
   if I_message_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;
   ---
   if I_check_l10n_ind = 'Y' then
      --
      --- This code below is to find out the info need to find the countries in L10N_SQL.EXEC_FUNCTION 
      ---     
      L_rib_invadjustdesc_rec := treat (I_message as "RIB_InvAdjustDesc_REC");
      --
      if L_rib_invadjustdesc_rec is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;
      --
      L_l10n_rib_rec.doc_type        := NULL;
      L_l10n_rib_rec.source_id       := L_rib_invadjustdesc_rec.dc_dest_id;
      L_l10n_rib_rec.source_type     := NULL;
      L_l10n_rib_rec.source_entity   := 'LOC';
      -- 
      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_rib_rec)= FALSE then
         raise PROGRAM_ERROR;
      end if;
      --       
   else
      -- When Localization Indicator is other than 'Y'.
      --- This is for the base functionality and also will be called for the country which is not localized.
      if RMSSUB_INVADJUST.CONSUME_INVADJ(O_error_message,
                                         L_l10n_rib_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;   
      --   
   end if;
   --
   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;  
       
EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME_INVADJ;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_INVADJ(O_error_message     IN OUT  VARCHAR2,
                        IO_L10N_RIB_REC     IN OUT  L10N_OBJ)
RETURN BOOLEAN IS

   L_program         VARCHAR2(255) := 'RMSSUB_ASNOUT.CONSUME_INVADJ';
   L_l10n_rib_rec    "L10N_RIB_REC" := L10N_RIB_REC();

BEGIN
   --
   L_l10n_rib_rec := treat(IO_L10N_RIB_REC as L10N_RIB_REC);
   if CONSUME_INVADJ_CORE(O_error_message,
                          L_l10n_rib_rec.rib_msg,
                          L_l10n_rib_rec.rib_msg_type) = FALSE then
      return FALSE;
   end if;
 
   return TRUE;    

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
      return FALSE;
   when OTHERS then
      HANDLE_ERRORS(L_l10n_rib_rec.status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
      return FALSE;
END CONSUME_INVADJ;
-------------------------------------------------------------------------
/* Private Function and Procedure Bodies */
-------------------------------------------------------------------------
FUNCTION CONSUME_INVADJ_CORE(O_error_message   IN OUT   VARCHAR2,
                             I_message         IN       RIB_OBJECT,
                             I_message_type    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(100) := 'RMSSUB_INVADJUST.CONSUME_INVADJ_CORE';

   L_rib_invadjustdesc_rec    "RIB_InvAdjustDesc_REC";

BEGIN

   L_rib_invadjustdesc_rec := treat (I_message as "RIB_InvAdjustDesc_REC");

   --empty out cache of tran_data inserts
   if INVADJ_SQL.INIT_ALL(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if PROCESS_INVADJ(O_error_message,
                     L_rib_invadjustdesc_rec) = FALSE then                     
      return FALSE;
   end if;

   --call flush
   if INVADJ_SQL.FLUSH_ALL (O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSUME_INVADJ_CORE;
-------------------------------------------------------------------------
FUNCTION PROCESS_INVADJ (O_error_message   IN OUT   VARCHAR2,
                         I_message         IN       "RIB_InvAdjustDesc_REC")
RETURN BOOLEAN IS

   L_program           VARCHAR2(62)         := 'RMSSUB_INVADJUST.PROCESS_INVADJ';

   L_invadj_detail_TBL "RIB_InvAdjustDtl_TBL" := I_message.InvAdjustDtl_TBL;

   L_detail_TBL        "RIB_InvAdjustDtl_TBL" := NULL;
   L_sellable_TBL      "RIB_InvAdjustDtl_TBL" := NULL;
   L_orderable_TBL     "RIB_InvAdjustDtl_TBL" := NULL;

BEGIN

   if CHECK_ITEMS(O_error_message,
                  L_sellable_TBL,
                  L_detail_TBL,
                  L_invadj_detail_TBL) = FALSE then
      return FALSE;
   end if;

   if L_sellable_TBL is NOT NULL and L_sellable_TBL.COUNT > 0 then

      L_orderable_TBL := "RIB_InvAdjustDtl_TBL"();

      if GET_ORDERABLE_ITEMS(O_error_message,
                             L_orderable_TBL, --- Output RIB table of orderable items
                             L_sellable_TBL) = FALSE then
         return FALSE;
      end if;

      if L_orderable_TBL is NOT NULL and L_orderable_TBL.COUNT > 0 then

         FOR i in 1..L_orderable_TBL.COUNT LOOP
            if INVADJ_SQL.BUILD_PROCESS_INVADJ(O_error_message,
                                               I_message.dc_dest_id,
                                               L_orderable_TBL(i).item_id,
                                               L_orderable_TBL(i).adjustment_reason_code,
                                               L_orderable_TBL(i).unit_qty,
                                               L_orderable_TBL(i).weight,
                                               L_orderable_TBL(i).weight_uom,
                                               L_orderable_TBL(i).from_disposition,
                                               L_orderable_TBL(i).to_disposition,
                                               L_orderable_TBL(i).user_id,
                                               L_orderable_TBL(i).create_date,
                                               L_orderable_TBL(i).po_nbr,
                                               L_orderable_TBL(i).doc_type,
                                               L_orderable_TBL(i).unit_cost, --I_wac
                                               NULL) = FALSE then            --I_unit_retail
               return FALSE;
            end if;
         END LOOP;
      end if;

   end if;

   if L_detail_TBL is NOT NULL and L_detail_TBL.COUNT > 0 then
   
      FOR i in 1..L_detail_TBL.COUNT LOOP    
         if INVADJ_SQL.BUILD_PROCESS_INVADJ(O_error_message,
                                            I_message.dc_dest_id,
                                            L_detail_TBL(i).item_id,
                                            L_detail_TBL(i).adjustment_reason_code,
                                            L_detail_TBL(i).unit_qty,
                                            L_detail_TBL(i).weight,
                                            L_detail_TBL(i).weight_uom,
                                            L_detail_TBL(i).from_disposition,
                                            L_detail_TBL(i).to_disposition,
                                            L_detail_TBL(i).user_id,
                                            L_detail_TBL(i).create_date,
                                            L_detail_TBL(i).po_nbr,
                                            L_detail_TBL(i).doc_type,
                                            L_detail_TBL(i).unit_cost, --I_wac
                                            NULL) = FALSE then         --I_unit_retail
            return FALSE;
         end if;   
      END LOOP;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
      return FALSE;

END PROCESS_INVADJ;
----------------------------------------------------------------------------------------
FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   VARCHAR2,
                             O_orderable_TBL   OUT      "RIB_InvAdjustDtl_TBL",
                             I_sellable_TBL    IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN IS

   L_program      VARCHAR2(62) := 'RMSSUB_INVADJUST.GET_ORDERABLE_ITEMS';

   L_orderable_detail_TBL   "RIB_InvAdjustDtl_TBL" := NULL;

   L_orditem_TBL            bts_orditem_qty_tbl := NULL;

   L_sell_items             ITEM_TBL;
   L_sell_qty               QTY_TBL;

   L_detail_no              NUMBER := 0;

   cursor C_SELL_ITEMS is
      select item_id,
             unit_qty
        from TABLE(cast(I_sellable_TBL AS "RIB_InvAdjustDtl_TBL"));

   cursor C_ORD_ITEM_QTY (I_item  IN ITEM_MASTER.ITEM%TYPE) is
      select orderable_item,qty
        from TABLE(cast(L_orditem_TBL AS bts_orditem_qty_tbl))
       where sellable_item = I_item;

BEGIN

   open C_SELL_ITEMS;
   fetch C_SELL_ITEMS bulk collect into L_sell_items,
                                        L_sell_qty;
   close C_SELL_ITEMS;

   if ITEM_XFORM_SQL.ORDERABLE_ITEM_INFO(O_error_message,
                                         L_orditem_TBL,  -- output table
                                         L_sell_items,
                                         L_sell_qty) = FALSE then
      return FALSE;
   end if;

   L_orderable_detail_TBL := "RIB_InvAdjustDtl_TBL"();

   FOR i in I_sellable_TBL.FIRST..I_sellable_TBL.LAST LOOP
       FOR item_rec IN C_ORD_ITEM_QTY(I_sellable_TBL(i).item_id) LOOP
           L_detail_no := L_detail_no + 1;
           L_orderable_detail_TBL.EXTEND;
           L_orderable_detail_TBL(L_detail_no) := I_sellable_TBL(i); --- Get all message values
           L_orderable_detail_TBL(L_detail_no).item_id := item_rec.orderable_item;
           L_orderable_detail_TBL(L_detail_no).unit_qty := item_rec.qty;
      END LOOP;
   END LOOP;  
                       
   O_orderable_TBL := L_orderable_detail_TBL;
 
   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
      return FALSE;

END GET_ORDERABLE_ITEMS;
----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS(O_error_message   IN OUT   VARCHAR2,
                     O_sellable_TBL    OUT      "RIB_InvAdjustDtl_TBL",
                     O_detail_TBL      OUT      "RIB_InvAdjustDtl_TBL",
                     I_rib_detail_TBL  IN       "RIB_InvAdjustDtl_TBL")
RETURN BOOLEAN IS

   L_program      VARCHAR2(62) := 'RMSSUB_INVADJUST.CHECK_ITEMS';

   L_dtl_count    NUMBER := 0;
   L_sell_count   NUMBER := 0;

   L_item_rec     ITEM_MASTER%ROWTYPE;

BEGIN
   O_sellable_TBL := "RIB_InvAdjustDtl_TBL"();
   O_detail_TBL := "RIB_InvAdjustDtl_TBL"();
   --- 
   FOR i in 1..I_rib_detail_TBL.COUNT LOOP
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item_rec,  --- output record
                                          I_rib_detail_TBL(i).item_id) = FALSE then
         return FALSE;
      end if;

      if L_item_rec.sellable_ind = 'Y'   and
         L_item_rec.orderable_ind = 'N'  and
         L_item_rec.item_xform_ind = 'Y' then
         --- add item to sellable table
         L_sell_count := L_sell_count + 1;

         O_sellable_TBL.EXTEND;
         O_sellable_TBL(L_sell_count) := I_rib_detail_TBL(i);
      else
         --- add item to a new detail table
         L_dtl_count := L_dtl_count + 1;
         O_detail_TBL.EXTEND;
         O_detail_TBL(L_dtl_count) := I_rib_detail_TBL(i);
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
      return FALSE;

END CHECK_ITEMS;
-----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   VARCHAR2,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2)
IS
   L_program  VARCHAR2(100)  := 'RMSSUB_INVADJUST.HANDLE_ERRORS';

BEGIN
   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             To_Char(SQLCODE));
      ---
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_INVADJUST;
/
