CREATE OR REPLACE PACKAGE SVCPROV_CUSTORDSUB AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- Function Name: CREATE_CO_SUBSTITUTE
-- Purpose: This procedure will be called from web service implementation package 
--          for substitute items. This will load the item substitution messsage into 
--          the header and detail staging tables and call the coresvc_custordsub package 
--          which contains the core business logic
--------------------------------------------------------------------------------
PROCEDURE CREATE_CO_SUBSTITUTE (O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                I_businessObject         IN     "RIB_CustOrdSubColDesc_REC");
--------------------------------------------------------------------------------
END SVCPROV_CUSTORDSUB;
/