CREATE OR REPLACE PACKAGE CORESVC_STOCK_UPLOAD_SQL AUTHID CURRENT_USER AS

GP_stkupld_fhead_row   SVC_STKUPLD_FHEAD%ROWTYPE;
GP_stkupld_fdetl_row   SVC_STKUPLD_FDETL%ROWTYPE;

-- Parameter tables
FHEAD   CONSTANT  VARCHAR2(30) := 'SVC_STKUPLD_FHEAD';
FDETL   CONSTANT  VARCHAR2(30) := 'SVC_STKUPLD_FDETL';
FTAIL   CONSTANT  VARCHAR2(30) := 'SVC_STKUPLD_FTAIL';

-- Process/chunk status
NEW_RECORD   CONSTANT  VARCHAR2(30) := 'N';
ERROR        CONSTANT  VARCHAR2(30) := 'E';
REJECTED     CONSTANT  VARCHAR2(30) := 'R';
PROCESSED    CONSTANT  VARCHAR2(30) := 'P';

-------------------------------------------------------------------------------------------------
-- Function: PRE_PROCESS_VALIDATION
-- Description: Validates information in the parameter tables after loading prior to main process
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PARAMETER_TABLES(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
-- Function: CHUNK_STOCK_COUNT
-- Description: Groups the data in the SVC_STKUPLD_FDETL into chunks
-------------------------------------------------------------------------------------------------
FUNCTION CHUNK_STOCK_COUNT(O_error_message    IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id       IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
-- Function: PROCESS_STOCK_COUNT
-- Description: Main process function for the stock upload process
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_STOCK_COUNT(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                             I_chunk_id        IN     SVC_STKUPLD_FDETL.CHUNK_ID%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
-- Function: INITIALIZE_PROCESS_STATUS
-- Description: Inserts the initial entry for the stock upload process.
--              This is an asynchronous function.
-------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_PROCESS_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                   I_reference_id    IN     SVC_STKUPLD_STATUS.REFERENCE_ID%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
-- Function: INITIALIZE_CHUNK_STATUS
-- Description: Inserts the status tracking entries after a successful chunking.
--              This is an asynchronous function.
-------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_CHUNK_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN     SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------
-- Function: GET_RESTART_PROCESS_ID
-- Description: Retrieves the process id from the svc_stkupld_status table for a given filename
--              with a status of "New" (N). An existing row indicates a restart.
-------------------------------------------------------------------------------------------------
FUNCTION GET_RESTART_PROCESS_ID(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                O_process_id      IN OUT SVC_STKUPLD_FHEAD.PROCESS_ID%TYPE,
                                I_reference_id    IN     SVC_STKUPLD_STATUS.REFERENCE_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END CORESVC_STOCK_UPLOAD_SQL;
/
