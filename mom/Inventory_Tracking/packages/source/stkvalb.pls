CREATE OR REPLACE PACKAGE BODY STKCNT_VALIDATE_SQL AS
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION STAKE_DATE_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exist          IN OUT  BOOLEAN,
                           I_stake_date     IN      STAKE_HEAD.STOCKTAKE_DATE%TYPE)

RETURN BOOLEAN IS

   L_exists VARCHAR2(1);

cursor C_DATE_EXISTS_V is
   select 'X'
     from stake_head sh, stake_sku_loc sl
    where stocktake_date = I_stake_date
      and sh.cycle_count = sl.cycle_count
      and exists (select 'x'
                    from v_store vs
                   where sl.loc_type = 'S'
                     and vs.store = sl.location
                   union
                  select 'x'
                    from v_wh vw
                   where sl.loc_type = 'W'
                     and vw.wh = sl.location
                   union
                  select 'x'
                    from v_internal_finisher vn
                   where sl.loc_type = 'W'
                     and vn.finisher_id = sl.location
                   union
                  select 'x'
                    from v_external_finisher ve
                   where sl.loc_type = 'E'
                     and to_number(ve.finisher_id) = sl.location);

cursor C_DATE_EXISTS_TB is
   select 'X'
     from stake_head sh, stake_sku_loc sl
    where stocktake_date = I_stake_date
      and sh.cycle_count = sl.cycle_count;

BEGIN

   open C_DATE_EXISTS_V;
   fetch C_DATE_EXISTS_V into L_exists;
   if C_DATE_EXISTS_V%NOTFOUND then
      open C_DATE_EXISTS_TB;
      fetch C_DATE_EXISTS_TB into L_exists;
      if C_DATE_EXISTS_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STAKE_DATE', NULL, NULL, NULL);
         O_exist := FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_STKCNTDT',
                                               TO_CHAR(I_stake_date, 'DD-MON-RR'),
                                               NULL,
                                               NULL);
         O_exist := FALSE;
      end if;
      close C_DATE_EXISTS_TB;
   else
      O_exist := TRUE;
   end if;
   close C_DATE_EXISTS_V;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.STAKE_DATE_EXISTS',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END STAKE_DATE_EXISTS;
------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_ITEM_MASTER V_STORE V_WH
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION ITEM_LOC_EXISTS(I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                         I_count_type     IN      CODE_DETAIL.CODE%TYPE,
                         O_exists         IN OUT  BOOLEAN,
                         O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_exists  VARCHAR2(1) := 'N';

   cursor C_EXIST_V is
      select 'Y'
        from stake_sku_loc ssl
       where ssl.cycle_count = I_stake  
         and rownum = 1
         and snapshot_on_hand_qty is not null
         and exists (select 'x'
                       from v_item_master vi
                      where ssl.item = vi.item
                        and rownum = 1)
         and exists (select 'x'
                       from v_store s
                      where ssl.loc_type = 'S'
                        and s.store = ssl.location
                      union all
                     select 'x'
                       from v_wh w
                      where ssl.loc_type = 'W'
                        and w.wh = ssl.location
                      union all
                     select 'x'
                       from v_internal_finisher n
                      where ssl.loc_type = 'W'
                        and n.finisher_id = ssl.location
                      union all
                     select 'x'
                       from v_external_finisher e
                      where ssl.loc_type = 'E'
                        and e.finisher_id = ssl.location);
            
   cursor C_EXIST_TB is
      select 'Y'
        from stake_sku_loc
       where cycle_count = I_stake
         and snapshot_on_hand_qty is not NULL
         and rownum      = 1;

   cursor C_EXIST_NULL is
      select 'Y'
        from stake_sku_loc
       where cycle_count = I_stake
         and snapshot_on_hand_qty is NULL
         and rownum      = 1;

   cursor C_EXIST_HEAD is
      select 'Y'
        from stake_head
       where cycle_count = I_stake;

BEGIN

   open  C_EXIST_V;
   fetch C_EXIST_V into L_exists;
   close C_EXIST_V;

   --- If not found in the views, check the base tables
   if L_exists = 'N' then
      ---
      O_exists := FALSE;
      ---
      open  C_EXIST_TB;
      fetch C_EXIST_TB into L_exists;
      close C_EXIST_TB;
      ---
      if L_exists = 'N' then
         open  C_EXIST_NULL;
         fetch C_EXIST_NULL into L_exists;
         close C_EXIST_NULL;
         ---
         if L_exists = 'N' then
            open  C_EXIST_HEAD;
            fetch C_EXIST_HEAD into L_exists;
            close C_EXIST_HEAD;
            ---
            if L_exists = 'N' then
               O_error_message := SQL_LIB.CREATE_MSG('INV_STKCOUNT', NULL, NULL, NULL);
            else
               O_error_message := SQL_LIB.CREATE_MSG('STAKE_SKU_LOC_MISSING', I_stake, NULL, NULL);
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('SNAPSHOT_ON_HAND_NULL', I_stake, NULL, NULL);
         end if;
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_STKCNT', I_stake, NULL, NULL);
      end if;
   else
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.ITEM_LOC_EXISTS',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END ITEM_LOC_EXISTS;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_ITEM_MASTER V_STORE V_WH
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION ITEM_PROD_EXISTS(I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                          O_exists         IN OUT  BOOLEAN,
                          O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS
    
   L_exists  VARCHAR2(1) := 'N';

   cursor C_EXISTS_V is
      select 'Y'
        from (select /*+ first_rows no_merge */ 'Y'
        		from stake_sku_loc ss,
             		 v_store vs,
             		 v_item_master vi
       		   where ss.loc_type = 'S'
         		 and ss.location = vs.store
         		 and ss.item = vi.item
         		 and cycle_count = I_stake
         		 and rownum = 1
         	 )
       union
      select 'Y'
        from (select /*+ first_rows no_merge */ 'Y'
        		from stake_sku_loc ss,
             		 v_wh vw,
             		 v_item_master vi
       		   where ss.loc_type = 'W'
         		 and ss.location = vw.wh
         		 and ss.item = vi.item
         		 and cycle_count = I_stake
         		 and rownum = 1
         	 )
       union
      select 'Y'
        from (select /*+ first_rows no_merge */ 'Y'
        		from stake_sku_loc ss,
             		 v_internal_finisher vn,
             		 v_item_master vi
       		   where ss.loc_type = 'W'
         		 and ss.location = vn.finisher_id
         		 and ss.item = vi.item
           		 and cycle_count = I_stake
         		 and rownum = 1
             )
       union
      select 'Y'
        from (select /*+ first_rows no_merge */ 'Y'
        		from stake_sku_loc ss,
             		 v_external_finisher ve,
             		 v_item_master vi
       		   where ss.loc_type = 'E'
         		 and ss.location = to_number(ve.finisher_id)
         		 and ss.item = vi.item
         		 and cycle_count = I_stake
         		 and rownum = 1
         	 )
       union
      select 'Y'
        from (select /*+ first_rows no_merge */ 'Y'
      		    from stake_location sl,
      		         stake_product sp,
      		         v_store vs
       		   where sl.loc_type = 'S'
       			 and sl.location = vs.store
       			 and sl.cycle_count = sp.cycle_count
         		 and exists (select 'X'
                      		   from v_subclass vs
                      		  where sp.dept = vs.dept 
                        		and (sp.class = vs.class or
                        		     sp.class IS NULL)
                        		and (sp.subclass = vs.subclass or
                        		     sp.subclass IS NULL)
                        		and rownum = 1)
         		 and sl.cycle_count = I_stake
         		 and rownum 		= 1
         	 )
       union
      select 'Y'
        from (select /*+ first_rows no_merge */ 'Y'
      		    from stake_location sl,
      		         stake_product sp,
      		         v_wh vw
       		   where sl.loc_type = 'W'
       			 and sl.location = vw.wh
         		 and sl.cycle_count = sp.cycle_count
       			 and exists (select 'X'
       			               from v_subclass vs
       			              where sp.dept = vs.dept 
       			                and (sp.class = vs.class or
       			                     sp.class IS NULL)
       			                and (sp.subclass = vs.subclass or
       			                     sp.subclass IS NULL)
       			                and rownum = 1)
       			 and sl.cycle_count = I_stake
       			 and rownum = 1
       		 )
       union
      select 'Y'
        from (select /*+ first_rows no_merge */ 'Y'
        		from stake_location sl,
             		 stake_product sp,
             		 v_internal_finisher vn
       		   where sl.loc_type = 'W'
         		 and sl.location = vn.finisher_id
         		 and sl.cycle_count = sp.cycle_count
         		 and exists (select 'X'
                       		   from v_subclass vs
                      		  where sp.dept = vs.dept 
                        		and (sp.class = vs.class or
                             		 sp.class IS NULL)
                        		and (sp.subclass = vs.subclass or
                             		 sp.subclass IS NULL)
                        		and rownum = 1)
         		 and sl.cycle_count = I_stake
         		 and rownum = 1
         	 )
       union
      select 'Y'
        from (select /*+ first_rows no_merge */ 'Y'
      		    from stake_location sl,
      		         stake_product sp,
      		         v_external_finisher ve
      		   where sl.loc_type = 'E'
      		     and sl.location = to_number(ve.finisher_id)
      		     and sl.cycle_count = sp.cycle_count
      			 and exists (select 'X'
      		                   from v_subclass vs
      		                  where sp.dept = vs.dept 
      		                    and (sp.class = vs.class or
      			                     sp.class IS NULL)
      		                    and (sp.subclass = vs.subclass or
      		                         sp.subclass IS NULL)
      		                    and rownum = 1)
      		     and sl.cycle_count = I_stake
      		     and rownum = 1);

   cursor C_EXISTS_TB is
      select 'Y'
        from stake_product
       where cycle_count = I_stake
         and rownum = 1
       union
      select 'Y'
        from stake_sku_loc
       where cycle_count = I_stake
         and rownum = 1;

BEGIN

   open  C_EXISTS_V;
   fetch C_EXISTS_V into L_exists;
   close C_EXISTS_V;

   --- If not found in the views, check the base tables
   if L_exists = 'N' then
      ---
      O_exists := FALSE;
      ---
      open  C_EXISTS_TB;
      fetch C_EXISTS_TB into L_exists;
      close C_EXISTS_TB;
      ---
      if L_exists = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STKCOUNT', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_STKCNT', I_stake, NULL, NULL);
      end if;
   else
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.ITEM_PROD_EXISTS',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END ITEM_PROD_EXISTS;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION PROD_LOC_EXISTS(I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                         O_exist          IN OUT  BOOLEAN,
                         O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)

   RETURN BOOLEAN IS

   L_exists VARCHAR2(1);


   cursor C_EXIST_V is
      select /*+ index(sp, stake_prod_loc_i2) */
             'x'
        from stake_prod_loc sp
       where sp.cycle_count = I_stake
         and rownum = 1
         and exists (select 'x'
                       from v_deps vd
                      where sp.dept = vd.dept)
         and exists (select 'x'
                       from v_store vs
                      where sp.loc_type = 'S'
                        and sp.location = vs.store
                      union all
                     select 'x'
                       from v_wh vw
                      where sp.loc_type = 'W'
                        and sp.location = vw.wh
                      union all
                     select 'x'
                       from v_internal_finisher vn
                      where sp.loc_type = 'W'
                        and sp.location = vn.finisher_id
                      union all
                     select 'x'
                       from v_external_finisher ve
                       where sp.loc_type = 'E'
                         and sp.location = ve.finisher_id);

   cursor C_EXIST_TB is
      select 'x'
        from stake_prod_loc
       where cycle_count = I_stake
         and rownum = 1;

BEGIN

   open C_EXIST_V;
   fetch C_EXIST_V into L_exists;
   if C_EXIST_V%NOTFOUND then
      open C_EXIST_TB;
      fetch C_EXIST_TB into L_exists;
      if C_EXIST_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STKCOUNT', NULL, NULL, NULL);
         O_exist := FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_STKCNT', I_stake, NULL, NULL);
         O_exist := FALSE;
      end if;
      close C_EXIST_TB;
   else
      O_exist := TRUE;
   end if;
   close C_EXIST_V;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.PROD_LOC_EXISTS',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END PROD_LOC_EXISTS;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_ITEM_MASTER V_STORE V_WH
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION ITEM_QTY_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_new            IN OUT  BOOLEAN,
                         O_exist          IN OUT  BOOLEAN,
                         I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE)

RETURN BOOLEAN IS

   L_exists VARCHAR2(1);

cursor C_NEW_V is
   select 'x'
     from stake_head sh,
          stake_location sl
    where sh.cycle_count = I_stake
      and sh.cycle_count = sl.cycle_count
      and rownum = 1
      and exists (select 'x'
                    from v_store vs
                   where sl.loc_type = 'S'
                     and vs.store = sl.location
                   union all
                  select 'x'
                    from v_wh vw
                   where sl.loc_type = 'W'
                     and vw.wh = sl.location
                   union all
                  select 'x'
                    from v_internal_finisher vn
                   where sl.loc_type = 'W'
                     and vn.finisher_id = sl.location
                   union all
                  select 'x'
                    from v_external_finisher ve
                   where sl.loc_type = 'E'
                     and to_number(ve.finisher_id) = sl.location)
      and exists (select 'x'
                       from stake_sku_loc ssl,
                            v_item_master vim
                      where ssl.cycle_count = sh.cycle_count
                        and ssl.loc_type = sl.loc_type
                        and ssl.location = sl.location
                        and ssl.item = vim.item
                        and not exists (select 'x'
                                          from stake_qty sq
                                         where sq.cycle_count = ssl.cycle_count
                                           and sq.loc_type = ssl.loc_type
                                           and sq.location = ssl.location
                                           and sq.item = ssl.item)
                                           and rownum = 1);

cursor C_EXIST_V is
   select 'x'
     from stake_qty sq
    where sq.cycle_count = I_stake
      and exists (select 'x'
                    from v_item_master vim
                   where vim.item = sq.item
                     and rownum = 1)
      and exists (select 'x'
                    from stake_sku_loc ssl
                   where ssl.cycle_count = sq.cycle_count 
                     and ssl.item = sq.item
                     and rownum = 1)
      and rownum = 1;

BEGIN

   open C_NEW_V;
   fetch C_NEW_V into L_exists;
   if C_NEW_V%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('STKCOUNT_NO_NEW', NULL, NULL, NULL);
      O_new := FALSE;
   else
      O_new := TRUE;
   end if;
   close C_NEW_V;

   open C_EXIST_V;
   fetch C_EXIST_V into L_exists;
   if C_EXIST_V%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('STKCOUNT_NO_EDIT', NULL, NULL, NULL);
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   close C_EXIST_V;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.ITEM_QTY_EXISTS',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END ITEM_QTY_EXISTS;
------------------------------------------------------------------
FUNCTION VALID_LOC(O_error_message  IN OUT  VARCHAR2,
                   O_valid          IN OUT  BOOLEAN,
                   I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                   I_location       IN      STAKE_LOCATION.LOCATION%TYPE,
                   I_loc_type       IN      STAKE_HEAD.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_valid     VARCHAR2(1) := 'N';
   ---
   cursor C_valid IS
      select 'Y'
        from stake_head h,
             stake_location l
       where h.cycle_count        = l.cycle_count
         and h.cycle_count        = I_stake
         and h.loc_type           = I_loc_type
         and l.loc_type           = h.loc_type
         and l.location           = I_location
         and h.stocktake_date    >= GET_VDATE;
BEGIN

   if I_loc_type not in ('S','W','E') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',NULL,NULL,NULL);
      RETURN FALSE;
   end if;
   ---
   if I_stake is NULL or I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      RETURN FALSE;
   end if;
   ---
   open C_valid;
   fetch C_valid into L_valid;
   close C_valid;
   ---
   if L_valid = 'Y' then
      O_valid := TRUE;
   else
      O_valid := FALSE;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE_SQL.VALID_LOC',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END VALID_LOC;
------------------------------------------------------------------
FUNCTION UNIT_AND_DOLLAR_ITEM
         (O_error_message  IN OUT  VARCHAR2,
          O_valid          IN OUT  BOOLEAN,
          I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
          I_item           IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_valid     VARCHAR2(1) := 'N';
   L_type      STAKE_HEAD.STOCKTAKE_TYPE%TYPE;
   L_pack      ITEM_MASTER.ITEM%TYPE := NULL;
   L_item      ITEM_MASTER.ITEM%TYPE := NULL;
   L_pack_ind  ITEM_MASTER.PACK_IND%TYPE;
   L_sellable  ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type ITEM_MASTER.PACK_TYPE%TYPE;
   ---
   cursor C_get_stake_type IS
      select stocktake_type
        from stake_head
       where cycle_count = I_stake;

   cursor C_valid IS
      select 'Y'
        from stake_product s,
             item_master i
       where s.cycle_count               = I_stake
         and i.item                      = L_item
         and s.dept                      = i.dept
         and nvl(s.class, i.class)       = i.class
         and nvl(s.subclass, i.subclass) = i.subclass;

   cursor C_ITEMS_IN_PACK is
      select item
        from packitem_breakout
       where pack_no = L_pack;

BEGIN
   if I_stake is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      RETURN FALSE;
   end if;
   ---
   open C_get_stake_type;
   fetch C_get_stake_type into L_type;
   if C_get_stake_type%NOTFOUND then
      close C_get_stake_type;
      O_error_message := SQL_LIB.CREATE_MSG('INV_STOCK_COUNT',NULL,NULL,NULL);
      RETURN FALSE;
   end if;
   close C_get_stake_type;
   ---
   if L_type != 'B' then
      O_error_message := SQL_LIB.CREATE_MSG('STAKE_NOT_BOTH_TYPE',NULL,NULL,NULL);
      RETURN FALSE;
   end if;
   ---
   O_valid := FALSE;
   L_item := I_item;
   ---
   open C_valid;
   fetch C_valid into L_valid;
   close C_valid;
   ---
   if L_valid = 'N' then
      if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                       L_pack_ind,
                                       L_sellable,
                                       L_orderable,
                                       L_pack_type,
                                       L_item) then
         return FALSE;
      end if;
      ---
      if L_pack_ind = 'Y' then
         --* loop on the pack comp sku's of the pack *--
         L_pack := L_item;
         FOR rec in C_ITEMS_IN_PACK LOOP
            L_item := rec.item;
            open C_valid;
            fetch C_valid into L_valid;
            close C_valid;
            if L_valid = 'Y' then
               O_valid := TRUE;
               EXIT;
            end if;
         END LOOP;
      end if;
   else
      O_valid := TRUE;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE_SQL.UNIT_AND_DOLLAR_ITEM',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END UNIT_AND_DOLLAR_ITEM;
--------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_WH
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION VALID_STK_COUNT_VWHDIST(O_error_message   IN OUT   VARCHAR2,
                                 O_exist           IN OUT   BOOLEAN,
                                 I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                 I_pwh             IN       WH.PHYSICAL_WH%TYPE default NULL)
   return BOOLEAN IS

   L_dummy VARCHAR2(1);


  cursor C_VALID_STK_VWD_V is
      select 'x'
        from stake_head sh,
             v_wh w
       where sh.cycle_count = I_cycle_count
         and sh.stocktake_date <= get_vdate
         and w.physical_wh = nvl(I_pwh, physical_wh)
         and exists (select 'x'
                       from stake_sku_loc ssl
                      where ssl.cycle_count = sh.cycle_count
                        and ssl.loc_type = sh.loc_type
                        and ssl.loc_type = 'W'
                        and ssl.location = w.wh
                        and physical_count_qty is not null
                        and physical_count_qty <> 0
                        and rownum = 1)
         and rownum = 1;

  cursor C_VALID_STK_VWD_TB is
      select 'x'
        from stake_head sh,
             wh w
       where sh.cycle_count = I_cycle_count
         and sh.stocktake_date <= get_vdate
         and w.physical_wh = nvl(I_pwh, physical_wh)
         and exists (select 'x'
                       from stake_sku_loc ssl
                      where ssl.cycle_count = sh.cycle_count
                        and ssl.loc_type = sh.loc_type
                        and ssl.loc_type = 'W'
                        and ssl.location = w.wh
                        and physical_count_qty is not null
                        and physical_count_qty <> 0
                        and rownum = 1)
         and rownum = 1;

BEGIN

   open C_VALID_STK_VWD_V;
   fetch C_VALID_STK_VWD_V into L_dummy;
   if C_VALID_STK_VWD_V%NOTFOUND then
      open C_VALID_STK_VWD_TB ;
      fetch C_VALID_STK_VWD_TB into L_dummy;
      if C_VALID_STK_VWD_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STKCOUNT', NULL, NULL, NULL);
         O_exist := FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_STKCNT', I_cycle_count, NULL, NULL);
         O_exist := FALSE;
      end if;
      close C_VALID_STK_VWD_TB ;
   else
      O_exist := TRUE;
   end if;
   close C_VALID_STK_VWD_V;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.VALID_STK_COUNT_VHWDIST',
                                             To_Char(SQLCODE));
      return FALSE;
END VALID_STK_COUNT_VWHDIST;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION PARTIAL_LOCATIONS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_partial        IN OUT  BOOLEAN,
                           I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE)

RETURN BOOLEAN IS

   L_location  STORE.STORE%TYPE;

   cursor C_PARTIAL_V is
      select location
        from stake_location
       where cycle_count = I_stake
       minus
      select location
        from stake_location sl
       where cycle_count = I_stake
         and exists (select 'x'
                       from v_store vs
                      where sl.loc_type = 'S'
                        and vs.store = sl.location
                      union
                     select 'x'
                       from v_wh vw
                      where sl.loc_type = 'W'
                        and vw.wh = sl.location
                      union
                     select 'x'
                       from v_internal_finisher vn
                      where sl.loc_type = 'W'
                        and vn.finisher_id = sl.location
                      union
                     select 'x'
                       from v_external_finisher ve
                      where sl.loc_type = 'E'
                        and to_number(ve.finisher_id) = sl.location);

BEGIN

   open C_PARTIAL_V;
   fetch C_PARTIAL_V into L_location;
   if C_PARTIAL_V%NOTFOUND then
      O_partial := FALSE;
   else
      O_partial := TRUE;
   end if;
   close C_PARTIAL_V;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.PARTIAL_LOCATIONS',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END PARTIAL_LOCATIONS;
-------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION PARTIAL_DEPS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_partial        IN OUT  BOOLEAN,
                      I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                      I_loc_type       IN      STAKE_LOCATION.LOC_TYPE%TYPE,
                      I_location       IN      STAKE_LOCATION.LOCATION%TYPE)

RETURN BOOLEAN IS

   L_dept  DEPS.DEPT%TYPE;

   cursor C_PARTIAL_V is
      select distinct dept
        from stake_prod_loc
       where cycle_count = I_stake
         and loc_type = I_loc_type
         and location = I_location
       minus
      select distinct sp.dept
        from stake_prod_loc sp, v_deps vd
       where cycle_count = I_stake
         and loc_type = I_loc_type
         and location = I_location
         and sp.dept = vd.dept;

BEGIN

   open C_PARTIAL_V;
   fetch C_PARTIAL_V into L_dept;
   if C_PARTIAL_V%NOTFOUND then
      O_partial := FALSE;
   else
      O_partial := TRUE;
   end if;
   close C_PARTIAL_V;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.PARTIAL_DEPS',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END PARTIAL_DEPS;
--------------------------------------------------------------------------
FUNCTION STORE_COUNT_QTY_EXISTS
            (O_error_message  IN OUT  VARCHAR2,
             O_exist          IN OUT  BOOLEAN,
             I_cycle_count    IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
             I_store          IN      STORE.STORE%TYPE default NULL)
   RETURN BOOLEAN IS

   L_dummy VARCHAR2(1);

   cursor C_STORE_QTY_UPD is
      select 'X'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and loc_type = 'S'
         and location = I_store
         and physical_count_qty is NULL
         and rownum = 1;

BEGIN
   if I_cycle_count is NULL or I_store is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
         RETURN FALSE;
   end if;
   open C_STORE_QTY_UPD;
   fetch C_STORE_QTY_UPD into L_dummy;
   ---
   -- When the cursor returns something, means not all the stock count for the location
   -- completely upload, therefor, the function should return FALSE.

   if C_STORE_QTY_UPD%FOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   close C_STORE_QTY_UPD;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE_SQL.STORE_COUNT_QTY_EXISTS',
                                             To_Char(SQLCODE));
      return FALSE;
END STORE_COUNT_QTY_EXISTS;
--------------------------------------------------------------------------
FUNCTION WH_COUNT_QTY_EXISTS
            (O_error_message  IN OUT  VARCHAR2,
             O_exist          IN OUT  BOOLEAN,
             I_cycle_count    IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
             I_wh             IN      WH.WH%TYPE default NULL)
   RETURN BOOLEAN IS

   L_dummy VARCHAR2(1);

  cursor C_WAREHOUSE_QTY_UPD is
      select 'X'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and loc_type = 'W'
         and location = I_wh
         and physical_count_qty is NULL
         and rownum = 1;

BEGIN
   if I_cycle_count is NULL or I_wh is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
         RETURN FALSE;
   end if;
   open C_WAREHOUSE_QTY_UPD;
   fetch C_WAREHOUSE_QTY_UPD into L_dummy;
   ---
   -- When the cursor returns something, means not all the stock count for the location
   -- completely upload, therefor, the function should return FALSE.

   if C_WAREHOUSE_QTY_UPD%FOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   close C_WAREHOUSE_QTY_UPD;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE_SQL.WH_COUNT_QTY_EXISTS',
                                             To_Char(SQLCODE));
      return FALSE;
END WH_COUNT_QTY_EXISTS;
--------------------------------------------------------------------------
FUNCTION EXT_FINISHER_COUNT_QTY_EXISTS
            (O_error_message     IN OUT  VARCHAR2,
             O_exist             IN OUT  BOOLEAN,
             I_cycle_count       IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
             I_external_finisher IN      STAKE_SKU_LOC.LOCATION%TYPE default NULL)
   RETURN BOOLEAN IS

   L_dummy VARCHAR2(1);

  cursor C_EXTERNAL_FINISHER_QTY_UPD is
      select 'X'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and loc_type = 'E'
         and location = I_external_finisher
         and physical_count_qty is NULL
         and rownum = 1;

BEGIN
   if I_cycle_count is NULL or I_external_finisher is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
         RETURN FALSE;
   end if;
   open C_EXTERNAL_FINISHER_QTY_UPD;
   fetch C_EXTERNAL_FINISHER_QTY_UPD into L_dummy;
   ---
   -- When the cursor returns something, means not all the stock count for the location
   -- completely upload, therefor, the function should return FALSE.

   if C_EXTERNAL_FINISHER_QTY_UPD%FOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   close C_EXTERNAL_FINISHER_QTY_UPD;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE_SQL.EXT_FINISHER_COUNT_QTY_EXISTS',
                                             To_Char(SQLCODE));
      return FALSE;
END EXT_FINISHER_COUNT_QTY_EXISTS;
--------------------------------------------------------------------------
FUNCTION VALIDATE_CYCLE_COUNT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stocktake_date   IN OUT  STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                              O_stocktake_type   IN OUT  STAKE_HEAD.STOCKTAKE_TYPE%TYPE,
                              O_cycle_count_desc IN OUT  STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                              O_cycle_recs_exist IN OUT  BOOLEAN,
                              O_exists           IN OUT  BOOLEAN,
                              I_cycle_count      IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE)
   RETURN BOOLEAN IS

   cursor C_CYCLE_EXISTS is
      select b.stocktake_date,
             b.stocktake_type,
             b.cycle_count_desc
        from stake_sku_loc a, stake_head b
       where a.cycle_count = I_cycle_count
         and a.cycle_count = b.cycle_count;
BEGIN
   if I_cycle_count is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_cycle_count','NULL','not NULL');
         RETURN FALSE;
   end if;

   O_cycle_recs_exist := TRUE;

   open C_CYCLE_EXISTS;
   fetch C_CYCLE_EXISTS into O_stocktake_date,
                             O_stocktake_type,
                             O_cycle_count_desc;
   if C_CYCLE_EXISTS%NOTFOUND then
     O_cycle_recs_exist := FALSE;
   end if;
   close C_CYCLE_EXISTS;

   --- if detail records exist, ensure the user has visibility to the records.
   if O_cycle_recs_exist = TRUE then
      if ITEM_LOC_EXISTS(I_cycle_count,
                         O_stocktake_type,
                         O_exists,
                         O_error_message) = FALSE then
            return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE_SQL.VALIDATE_CYCLE_COUNT',
                                             To_Char(SQLCODE));
      return FALSE;
END VALIDATE_CYCLE_COUNT;
-------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION PARTIAL_PRODUCT_DEPS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_partial        IN OUT  BOOLEAN,
                              I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE)

RETURN BOOLEAN IS

   L_program_name      VARCHAR2(50) := 'STKCNT_VALIDATE_SQL.PARTIAL_PRODUCT_DEPS';
   L_stake_product     NUMBER;
   L_stake_product_v   NUMBER;
   L_diff              NUMBER;

   cursor C_STAKE_PRODUCT is
      select count(*)
        from stake_product
       where cycle_count = I_stake;

   cursor C_STAKE_PRODUCT_V is
      select count(*)
        from stake_product sp
       where cycle_count = I_stake
         and exists (select 'X'
                      from v_subclass vs
                     where sp.dept = vs.dept 
                       and (sp.class = vs.class or
                            sp.class IS NULL)
                       and (sp.subclass = vs.subclass or
                            sp.subclass IS NULL)
                       and rownum = 1);

BEGIN

   if I_stake IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_stake',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_STAKE_PRODUCT_V',
                    'STAKE_PRODUCT',
                    'Cycle Count: '||to_char(I_stake));
   open C_STAKE_PRODUCT_V;

   SQL_LIB.SET_MARK('FETCH',
                    'C_STAKE_PRODUCT_V',
                    'STAKE_PRODUCT',
                    'Cycle Count: '||to_char(I_stake));
   fetch C_STAKE_PRODUCT_V into L_stake_product_v;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_STAKE_PRODUCT_V',
                    'STAKE_PRODUCT',
                    'Cycle Count: '||to_char(I_stake));
   close C_STAKE_PRODUCT_V;

   SQL_LIB.SET_MARK('OPEN',
                    'C_STAKE_PRODUCT',
                    'STAKE_PRODUCT',
                    'Cycle Count: '||to_char(I_stake));
   open C_STAKE_PRODUCT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_STAKE_PRODUCT',
                    'STAKE_PRODUCT',
                    'Cycle Count: '||to_char(I_stake));
   fetch C_STAKE_PRODUCT into L_stake_product;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_STAKE_PRODUCT',
                    'STAKE_PRODUCT',
                    'Cycle Count: '||to_char(I_stake));
   close C_STAKE_PRODUCT;

   L_diff := L_stake_product - L_stake_product_v;

   if L_diff = 0 then
      O_partial := FALSE;
   else
      O_partial := TRUE;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE.PARTIAL_PRODUCT_DEPS',
                                             To_Char(SQLCODE));
      RETURN FALSE;
END PARTIAL_PRODUCT_DEPS;
--------------------------------------------------------------------------
FUNCTION STOCK_COUNT_LOC_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid          IN OUT  BOOLEAN,
                                I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                                I_location       IN      STAKE_LOCATION.LOCATION%TYPE,
                                I_loc_type       IN      STAKE_HEAD.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_valid     VARCHAR2(1)  := 'N';
   L_program   VARCHAR2(50) := 'STKCNT_VALIDATE_SQL.STOCK_COUNT_LOC_EXISTS';
   ---
   cursor C_valid IS
      select 'Y'
        from stake_head h,
             stake_location l
       where h.cycle_count        = l.cycle_count
         and h.cycle_count        = I_stake
         and h.loc_type           = I_loc_type
         and l.loc_type           = h.loc_type
         and l.location           = I_location;
BEGIN

   if I_loc_type not in ('S','W','E') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_stake is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                            'I_stake',
                                            'NULL',
                                            L_program);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                            'I_location',
                                            'NULL',
                                            L_program);
      return FALSE;
   end if;
   ---
   open C_valid;

   fetch C_valid into L_valid;

   close C_valid;

   if L_valid = 'Y' then
      O_valid := TRUE;
   else
      O_valid := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             To_Char(SQLCODE));
      return FALSE;
END STOCK_COUNT_LOC_EXISTS;
-----------------------------------------------------------------------------------
FUNCTION STAKE_QTY_LOC_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist          IN OUT  BOOLEAN,
                              I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'STKCNT_VALIDATE_SQL.STAKE_QTY_LOC_EXISTS';
   L_exist     VARCHAR2(1) := 'N';

   cursor C_count_loc_exist is
      select 'Y'
        from stake_location sl
       where sl.cycle_count = I_stake
           and not exists (select 'x'
                             from stake_sku_loc ssl

                   where cycle_count = sl.cycle_count
                             and physical_count_qty is not null
                             and sl.location = ssl.location
                             and rownum = 1)
           and rownum = 1;
BEGIN
   ---
   if I_stake IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_stake',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_count_loc_exist;
   fetch C_count_loc_exist into L_exist;
   close C_count_loc_exist;
   ---
   if L_exist = 'Y' then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             To_Char(SQLCODE));
      return FALSE;
END STAKE_QTY_LOC_EXISTS;
------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--   V_SKULIST_HEAD V_DEPS V_CLASS V_SUBCLASS
-- which only returns data that the user has permission to access.
-------------------------------------------------------------------------------------
FUNCTION PARTIAL_STAKE_SCHEDULE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_partial           IN OUT   BOOLEAN,
                                I_location          IN       STAKE_SCHEDULE.LOCATION%TYPE,
                                I_loc_type          IN       STAKE_SCHEDULE.LOC_TYPE%TYPE,
                                I_stocktake_type    IN       STAKE_SCHEDULE.STOCKTAKE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program_name        VARCHAR2(50) := 'STKCNT_VALIDATE_SQL.PARTIAL_STAKE_SCHEDULE';
   L_skulist             stake_schedule.skulist%type;
   L_stake_sched_cnt     NUMBER;
   L_stake_sched_v_cnt   NUMBER;
   L_diff                NUMBER;

   cursor C_PARTIAL_VSKULIST is
      select skulist
        from stake_schedule
       where location = I_location
         and loc_type = I_loc_type
         and stocktake_type = I_stocktake_type
       minus
      select skulist
        from stake_schedule
       where location = I_location
         and loc_type = I_loc_type
         and stocktake_type = I_stocktake_type
         and exists (select 'X'
                      from v_skulist_head
                     where stake_schedule.skulist = v_skulist_head.skulist
                       and rownum = 1);

   cursor C_STAKE_SCHEDULE is
      select count(*)
        from stake_schedule
       where location = I_location
         and loc_type = I_loc_type
         and stocktake_type = I_stocktake_type;

   cursor C_STAKE_SCHEDULE_V is
      select count(*)
        from stake_schedule
       where location = I_location
         and loc_type = I_loc_type
         and stocktake_type = I_stocktake_type
         and exists (select 'X'
                      from v_subclass vs
                     where stake_schedule.dept = vs.dept 
                       and (stake_schedule.class = vs.class or
                            stake_schedule.class IS NULL)
                       and (stake_schedule.subclass = vs.subclass or
                            stake_schedule.subclass IS NULL)
                       and rownum = 1);

BEGIN

   if I_location IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_location,
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   if I_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_loc_type,
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   if I_stocktake_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_stocktake_type,
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_stocktake_type = 'U' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_PARTIAL_VSKULIST',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      open C_PARTIAL_VSKULIST;

      SQL_LIB.SET_MARK('FETCH',
                       'C_PARTIAL_VSKULIST',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      fetch C_PARTIAL_VSKULIST into L_skulist;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_PARTIAL_VSKULIST',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      close C_PARTIAL_VSKULIST;
      ---
      if L_skulist IS NULL then
         O_partial := FALSE;
      else
         O_partial := TRUE;
      end if;
      ---
   end if;
   ---
   if I_stocktake_type = 'B' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_STAKE_SCHEDULE_V',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      open C_STAKE_SCHEDULE_V;

      SQL_LIB.SET_MARK('FETCH',
                       'C_STAKE_SCHEDULE_V',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      fetch C_STAKE_SCHEDULE_V into L_stake_sched_v_cnt;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_STAKE_SCHEDULE_V',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      close C_STAKE_SCHEDULE_V;

      SQL_LIB.SET_MARK('OPEN',
                       'C_STAKE_SCHEDULE',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      open C_STAKE_SCHEDULE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_STAKE_SCHEDULE',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      fetch C_STAKE_SCHEDULE into L_stake_sched_cnt;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_STAKE_SCHEDULE',
                       'STAKE_SCHEDULE',
                       'Location: '||to_char(I_location)||
                       ',Loc Type: '||I_loc_type||
                       ',Stocktake Type: '||I_stocktake_type);
      close C_STAKE_SCHEDULE;
      ---
      L_diff := L_stake_sched_cnt - L_stake_sched_v_cnt;
      if L_diff = 0 then
         O_partial := FALSE;
      else
         O_partial := TRUE;
      end if;
      ---

   end if;
   --- 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCNT_VALIDATE_SQL.PARTIAL_STAKE_SCHEDULE',
                                             To_Char(SQLCODE));
      return FALSE;
END PARTIAL_STAKE_SCHEDULE;
------------------------------------------------------------------------------------
END STKCNT_VALIDATE_SQL;
/
