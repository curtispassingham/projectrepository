
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_STAKESCHEDULE AS

/* Function and Procedure Bodies */
-------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code          IN OUT  VARCHAR2,
                   O_error_message        IN OUT  VARCHAR2,
                   I_message              IN      RIB_OBJECT,
                   I_message_type         IN      VARCHAR2)
IS

   L_module VARCHAR2(64) := 'RMSSUB_STAKESCHEDULE.CONSUME';

   L_rib_stkcntscheddesc_rec    "RIB_StkCountSchDesc_REC";
   L_rib_stkcntschedref_rec     "RIB_StkCountSchRef_REC";

BEGIN

   if lower(I_message_type) = STAKE_SCHEDULE_SQL.STKCOUNTSCHDEL then
      L_rib_stkcntschedref_rec := treat (I_message as "RIB_StkCountSchRef_REC");

      --
      -- validate the input
      --
      
      if STAKE_SCHEDULE_SQL.VALIDATE_VALUES(O_error_message,
                                            L_rib_stkcntschedref_rec.cycle_count,
                                            null,   -- stocktake_type
                                            null,   -- stocktake_date
                                            null,   -- location_type
                                            I_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if STAKE_SCHEDULE_SQL.PROCESS_DEL(O_error_message,
                                        L_rib_stkcntschedref_rec.cycle_count) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   elsif lower(I_message_type) = STAKE_SCHEDULE_SQL.STKCOUNTSCHCRE or
         lower(I_message_type) = STAKE_SCHEDULE_SQL.STKCOUNTSCHMOD then
      L_rib_stkcntscheddesc_rec := treat (I_message as "RIB_StkCountSchDesc_REC");

      if STAKE_SCHEDULE_SQL.INIT(O_error_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      --
      -- validate the input
      --
      
      if STAKE_SCHEDULE_SQL.VALIDATE_VALUES(O_error_message,
                                            L_rib_stkcntscheddesc_rec.cycle_count,
                                            L_rib_stkcntscheddesc_rec.stocktake_type,
                                            L_rib_stkcntscheddesc_rec.stocktake_date,
                                            L_rib_stkcntscheddesc_rec.location_type,
                                            I_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      --
      -- create the head level record
      --
      
      if STAKE_SCHEDULE_SQL.CREATE_SH_REC(O_error_message,
                                          L_rib_stkcntscheddesc_rec.cycle_count,
                                          L_rib_stkcntscheddesc_rec.cycle_count_desc,
                                          L_rib_stkcntscheddesc_rec.location_type,
                                          L_rib_stkcntscheddesc_rec.stocktake_date,
                                          L_rib_stkcntscheddesc_rec.stocktake_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      --
      -- Product processing
      --

      if L_rib_stkcntscheddesc_rec.StkCountSchProd_tbl is NULL
         or L_rib_stkcntscheddesc_rec.StkCountSchProd_tbl.COUNT = 0 then

         if STAKE_SCHEDULE_SQL.PROCESS_PROD(O_error_message,
                                            'N',
                                            L_rib_stkcntscheddesc_rec.cycle_count,
                                            L_rib_stkcntscheddesc_rec.location_type,
                                            null,
                                            null,
                                            null)= FALSE then

            raise PROGRAM_ERROR;
         end if;

      else -- prod table count != 0

         FOR i in 1..L_rib_stkcntscheddesc_rec.StkCountSchProd_tbl.COUNT LOOP
            if L_rib_stkcntscheddesc_rec.StkCountSchProd_tbl(i).dept = -1 then
               if STAKE_SCHEDULE_SQL.PROCESS_PROD(O_error_message,
                                                  'N',
                                                  L_rib_stkcntscheddesc_rec.cycle_count,
                                                  L_rib_stkcntscheddesc_rec.location_type,
                                                  null,
                                                  null,
                                                  null) = FALSE then

                  raise PROGRAM_ERROR;
               end if;
            else
               if STAKE_SCHEDULE_SQL.PROCESS_PROD(O_error_message,
                                                  'Y',
                                                  L_rib_stkcntscheddesc_rec.cycle_count,
                                                  L_rib_stkcntscheddesc_rec.location_type,
                                                  L_rib_stkcntscheddesc_rec.StkCountSchProd_tbl(i).dept,
                                                  L_rib_stkcntscheddesc_rec.StkCountSchProd_tbl(i).class,
                                                  L_rib_stkcntscheddesc_rec.StkCountSchProd_tbl(i).subclass) = FALSE then

                  raise PROGRAM_ERROR;
               end if;
            end if;
         END LOOP;

      end if; -- if L_rib_stkcntscheddesc_rec.StkCountSchProd_tbl.COUNT = 0

      --
      -- Location processing
      --

      if L_rib_stkcntscheddesc_rec.StkCountSchLoc_tbl is NULL
         or L_rib_stkcntscheddesc_rec.StkCountSchLoc_tbl.COUNT = 0 then

         if STAKE_SCHEDULE_SQL.PROCESS_LOC(O_error_message,
                                           'N',
                                           L_rib_stkcntscheddesc_rec.cycle_count,
                                           L_rib_stkcntscheddesc_rec.location_type,
                                           null) = FALSE then
            raise PROGRAM_ERROR;
         end if;

      else -- loc table count != 0

         FOR i in 1..L_rib_stkcntscheddesc_rec.StkCountSchLoc_tbl.COUNT LOOP

            if STAKE_SCHEDULE_SQL.PROCESS_LOC(O_error_message,
                                              'Y',
                                              L_rib_stkcntscheddesc_rec.cycle_count,
                                              L_rib_stkcntscheddesc_rec.location_type,
                                              L_rib_stkcntscheddesc_rec.StkCountSchLoc_tbl(i).location) = FALSE then

               raise PROGRAM_ERROR;
            end if;
         END LOOP;

      end if; -- if L_rib_stkcntscheddesc_rec.StkCountSchLoc_tbl.COUNT = 0

      if STAKE_SCHEDULE_SQL.FLUSH(O_error_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_message_type',
                                            I_message_type, L_module);
      raise PROGRAM_ERROR;

   end if;
   
   ---   
   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;
   
 EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_module);
END CONSUME;
----------------------------------------------------------------------------------------
END RMSSUB_STAKESCHEDULE;
/
