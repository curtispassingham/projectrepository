
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY UPDATE_SNAPSHOT_SQL AS

    TYPE rowid_TBL is table of ROWID;
    TYPE adj_qty_TBL is table of edi_daily_sales.stock_on_hand%TYPE;

    LP_tran_type         VARCHAR2(6);
    LP_item              ITEM_MASTER.ITEM%TYPE;
    LP_pack_ind          VARCHAR2(1);
    LP_rpt_freq          VARCHAR2(1);
    LP_sal_store         store.store%TYPE;
    LP_rcv_store         store.store%TYPE;
    LP_rcv_wh            wh.wh%TYPE;
    LP_send_store        store.store%TYPE;
    LP_send_wh           wh.wh%TYPE;
    LP_rcv_loc_type      item_loc.loc_type%TYPE;
    LP_rcv_location      item_loc.loc%TYPE;
    LP_send_loc_type     item_loc.loc_type%TYPE;
    LP_send_location     item_loc.loc%TYPE;
    LP_tran_store        store.store%TYPE;
    LP_tran_wh           wh.wh%TYPE;
    LP_tran_loc_type     item_loc.loc_type%TYPE;
    LP_tran_location     item_loc.loc%TYPE;
    LP_tran_date         period.vdate%TYPE;
    LP_vdate             period.vdate%TYPE :=  Get_Vdate;
    LP_adj_qty           edi_daily_sales.stock_on_hand%TYPE;
    LP_receive_as_type   item_loc.receive_as_type%TYPE;

    LP_sales_qty         edi_daily_sales.stock_on_hand%TYPE;
    LP_lag_days          system_options.edi_daily_rpt_lag%TYPE;
    O_error_message      VARCHAR2(255);
    LP_table             VARCHAR2(30);

    LP_dept              ITEM_MASTER.DEPT%TYPE;
    LP_class             ITEM_MASTER.CLASS%TYPE;
    LP_subclass          ITEM_MASTER.SUBCLASS%TYPE;
    LP_std_av_ind        SYSTEM_OPTIONS.STD_AV_IND%TYPE;
    LP_cycle_count       STAKE_SKU_LOC.CYCLE_COUNT%TYPE;

    LP_lockout_days      system_options.stake_lockout_days%TYPE := stkcount_sql.lockout_days;
    LP_prev_stktake_date DATE := NULL;
    LP_prev_stktake_date_notfound BOOLEAN := FALSE;

    LP_store_type_rcv    STORE.STORE_TYPE%TYPE;
    LP_store_type_snd    STORE.STORE_TYPE%TYPE;
-------------------------------------------------------------------

--- local prototypes ---

FUNCTION UPDATE_STAKE_SKU_LOC(O_error_message IN OUT VARCHAR2,
                              I_item          IN     item_loc.item%TYPE,
                              I_loc_type      IN     item_loc.loc_type%TYPE,
                              I_location      IN     item_loc.loc%TYPE,
                              I_tran_date     IN     DATE,
                              I_adj_soh       IN     item_loc_soh.stock_on_hand%TYPE,
                              I_adj_intran    IN     item_loc_soh.stock_on_hand%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
FUNCTION CHECK_STOCK_COUNT(O_error_message       IN OUT    VARCHAR2,
                           I_store               IN        ITEM_LOC.LOC%TYPE,
                           I_wh                  IN        ITEM_LOC.LOC%TYPE,
                           I_tran_date           IN        PERIOD.VDATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_STOCK_COUNT(O_error_message       IN OUT    VARCHAR2,
                           I_loc_type            IN        ITEM_LOC.LOC_TYPE%TYPE,
                           I_location            IN        ITEM_LOC.LOC%TYPE,
                           I_tran_date           IN        PERIOD.VDATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION PROC_STK_CNT_TD_WRITE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_dept            IN       ITEM_MASTER.DEPT%TYPE,
                               I_class           IN       ITEM_MASTER.CLASS%TYPE,
                               I_subclass        IN       ITEM_MASTER.SUBCLASS%TYPE,
                               I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_location        IN       ITEM_LOC.LOC%TYPE,
                               I_snapshot_retail IN       ITEM_LOC_SOH.AV_COST%TYPE,
                               I_snapshot_cost   IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                               I_tran_date       IN       PERIOD.VDATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------

FUNCTION GET_LAG_DAYS(O_error_message   IN OUT   VARCHAR2,
                      O_lag_days     IN OUT   NUMBER)
RETURN BOOLEAN IS

   cursor C_GET_LAG_DAYS is
      select system_options.edi_daily_rpt_lag
        from system_options;

BEGIN

   if LP_tran_date = LP_vdate then
      RETURN TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_GET_LAG_DAYS', 'system_options',
                    'daily_rpt_lag_days');
   open C_GET_LAG_DAYS;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_LAG_DAYS', 'system_options',
                    'daily_rpt_lag_days');
   fetch C_GET_LAG_DAYS into O_lag_days;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_LAG_DAYS', 'system_options',
                    'daily_rpt_lag_days');
   close C_GET_LAG_DAYS;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.GET_LAG_DAYS',
                                          to_char(SQLCODE));
      RETURN FALSE;
END GET_LAG_DAYS;
------------------------------------------------------------------------
FUNCTION UPDATE_STKADJ(O_error_message IN OUT   VARCHAR2)
RETURN BOOLEAN IS

L_rowid_tbl        rowid_TBL;
L_adj_soh_tbl      adj_qty_TBL;

L_loc           EDI_DAILY_SALES.LOC%TYPE;
L_loc_type      EDI_DAILY_SALES.LOC_TYPE%TYPE;
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_EDI_DAILY_SALES is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = L_loc
      and edi_daily_sales.loc_type = L_loc_type
      for update nowait;


BEGIN
   if LP_adj_qty = 0 then
      return TRUE;
   end if;


   L_loc := LP_tran_location;
   L_loc_type := LP_tran_loc_type;

   LP_table := 'edi_daily_sales';
   SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                    'ITEM: '||(LP_item));

   open  C_LOCK_EDI_DAILY_SALES;
   fetch C_LOCK_EDI_DAILY_SALES BULK COLLECT INTO L_rowid_tbl,
                                                  L_adj_soh_tbl;
   close C_LOCK_EDI_DAILY_SALES;

   if L_rowid_tbl.COUNT > 0 then
      FORALL i in 1..L_rowid_tbl.COUNT
      UPDATE edi_daily_sales e
         SET e.stock_on_hand = e.stock_on_hand + L_adj_soh_tbl(i)
       WHERE rowid = L_rowid_tbl(i);
   end if;

RETURN TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                           LP_table,
                                           LP_item,
                                           to_char(L_loc));

      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.UPDATE_STKADJ',
                                          to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_STKADJ;
-------------------------------------------------------------------
FUNCTION UPDATE_TSFO(O_error_message   IN OUT      VARCHAR2)
RETURN BOOLEAN IS

L_rowid_tbl     rowid_TBL;
L_adj_soh_tbl   adj_qty_TBL;

L_loc           EDI_DAILY_SALES.LOC%TYPE;

RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_EDI_DAILY_SALES1 is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = LP_rcv_location
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

cursor C_LOCK_EDI_DAILY_SALES2 is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = LP_send_location
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

cursor C_LOCK_EDI_DAILY_SALES3 is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = LP_send_location
      and edi_daily_sales.loc_type = 'W'
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

cursor C_LOCK_EDI_DAILY_SALES4 is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = LP_rcv_location
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

cursor C_PACKWH is
   select nvl(receive_as_type, 'P')
     from item_loc
    where item = LP_item
      and loc  = LP_rcv_location
      and loc_type = 'W';

BEGIN

   if LP_pack_ind = 'P' then

      if ((LP_send_loc_type = 'W') AND (LP_rcv_loc_type = 'W')) then

         --- update receiving location
         if LP_tran_date >= (LP_vdate - LP_lag_days) then
            SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                             'ITEM: '||(LP_item));

            LP_table := 'edi_daily_sales';

            open  C_LOCK_EDI_DAILY_SALES1;
            fetch C_LOCK_EDI_DAILY_SALES1 BULK COLLECT INTO L_rowid_tbl,
                                                            L_adj_soh_tbl;
            close C_LOCK_EDI_DAILY_SALES1;

            if L_rowid_tbl.COUNT > 0 then
              FORALL i in 1..L_rowid_tbl.COUNT
              UPDATE edi_daily_sales e
                 SET e.in_transit_qty = e.in_transit_qty + L_adj_soh_tbl(i)
               WHERE rowid = L_rowid_tbl(i);
            end if;
            L_rowid_tbl.DELETE;
            L_adj_soh_tbl.DELETE;

         end if;

         SQL_LIB.SET_MARK('OPEN','C_PACKWH','PACKWH',
                          'pack no: '||(LP_item)||
                          ', wh: '||TO_CHAR(LP_rcv_wh));
         open C_PACKWH;
         SQL_LIB.SET_MARK('FETCH','C_PACKWH','PACKWH',
                          'pack no: '||(LP_item)||
                          ', wh: '||TO_CHAR(LP_rcv_wh));
                           fetch C_PACKWH into LP_receive_as_type;
         SQL_LIB.SET_MARK('CLOSE','C_PACKWH','PACKWH',
                          'pack no: '||(LP_item)||
                          ', wh: '||TO_CHAR(LP_rcv_wh));
         close C_PACKWH;
         ---
         -- in_transit_qty should not be updated at the pack level if the
         -- receiving wh will receive the pack at the component level.
         ---
         if LP_receive_as_type = 'P' then

            if UPDATE_STAKE_SKU_LOC(O_error_message,
                                    LP_item,
                                    LP_rcv_loc_type,
                                    LP_rcv_location,
                                    LP_tran_date,
                                    0,
                                    LP_adj_qty) = FALSE then
               return FALSE;
            end if;
         end if;

         --- update sending location stock

         if LP_tran_date >= (LP_vdate - LP_lag_days) then
            SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                             'ITEM: '||LP_item);

            LP_table := 'edi_daily_sales';

               open  C_LOCK_EDI_DAILY_SALES2;
               fetch C_LOCK_EDI_DAILY_SALES2 BULK COLLECT INTO L_rowid_tbl,
                                                               L_adj_soh_tbl;
               close C_LOCK_EDI_DAILY_SALES2;

               if L_rowid_tbl.COUNT > 0 then
                  FORALL i in 1..L_rowid_tbl.COUNT
                  UPDATE edi_daily_sales e
                     SET e.stock_on_hand = e.stock_on_hand - L_adj_soh_tbl(i)
                   WHERE rowid = L_rowid_tbl(i);
               end if;
               L_rowid_tbl.DELETE;
               L_adj_soh_tbl.DELETE;

         end if;

         if UPDATE_STAKE_SKU_LOC(O_error_message,
                                 LP_item,
                                 LP_send_loc_type,
                                 LP_send_location,
                                 LP_tran_date,
                                 LP_adj_qty * -1,
                                 0) = FALSE then
            return FALSE;
         end if;

      elsif ((LP_send_loc_type = 'W') and (LP_rcv_loc_type = 'S')) then

         --- update sending location
         if LP_tran_date >= (LP_vdate - LP_lag_days) then
            SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                             'ITEM: '||LP_item);

            LP_table := 'edi_daily_sales';

            open  C_LOCK_EDI_DAILY_SALES3;
            fetch C_LOCK_EDI_DAILY_SALES3 BULK COLLECT INTO L_rowid_tbl,
                                                            L_adj_soh_tbl;
            close C_LOCK_EDI_DAILY_SALES3;

            if L_rowid_tbl.COUNT > 0 then
               FORALL i in 1..L_rowid_tbl.COUNT
               UPDATE edi_daily_sales e
                  SET e.stock_on_hand = e.stock_on_hand - L_adj_soh_tbl(i)
                WHERE rowid = L_rowid_tbl(i);
            end if;
            L_rowid_tbl.DELETE;
            L_adj_soh_tbl.DELETE;

         end if;

         if UPDATE_STAKE_SKU_LOC(O_error_message,
                                 LP_item,
                                 LP_send_loc_type,
                                 LP_send_location,
                                 LP_tran_date,
                                 LP_adj_qty * -1,
                                 0) = FALSE then
            return FALSE;
         end if;

      end if;

   elsif LP_pack_ind = 'C' then

      if LP_send_loc_type = 'W' then

         --- update receiving location
         if LP_tran_date >= (LP_vdate - LP_lag_days) then
            SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                             'ITEM: '||LP_item);

            LP_table := 'edi_daily_sales';

            open  C_LOCK_EDI_DAILY_SALES4;
            fetch C_LOCK_EDI_DAILY_SALES4 BULK COLLECT INTO L_rowid_tbl,
                                                         L_adj_soh_tbl;
            close C_LOCK_EDI_DAILY_SALES4;

            if L_rowid_tbl.COUNT > 0 then
               FORALL i in 1..L_rowid_tbl.COUNT
               UPDATE edi_daily_sales e
                  SET e.in_transit_qty = e.in_transit_qty + L_adj_soh_tbl(i)
                WHERE rowid = L_rowid_tbl(i);
            end if;
            L_rowid_tbl.DELETE;
            L_adj_soh_tbl.DELETE;

         end if;

         if UPDATE_STAKE_SKU_LOC(O_error_message,
                                 LP_item,
                                 LP_rcv_loc_type,
                                 LP_rcv_location,
                                 LP_tran_date,
                                 0,
                                 LP_adj_qty) = FALSE then
            return FALSE;
         end if;
      end if;

   elsif LP_pack_ind = 'N' then
         if LP_rcv_loc_type = 'W' OR LP_store_type_rcv IN ('C', 'F') then
      --- update receiving location
      if LP_tran_date >= (LP_vdate - LP_lag_days) then
         SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                          'ITEM: '||LP_item);

         LP_table := 'edi_daily_sales';

         open  C_LOCK_EDI_DAILY_SALES1;
         fetch C_LOCK_EDI_DAILY_SALES1 BULK COLLECT INTO L_rowid_tbl,
                                                         L_adj_soh_tbl;
         close C_LOCK_EDI_DAILY_SALES1;

         if L_rowid_tbl.COUNT > 0 then
            FORALL i in 1..L_rowid_tbl.COUNT
            UPDATE edi_daily_sales e
               SET e.in_transit_qty = e.in_transit_qty + L_adj_soh_tbl(i)
             WHERE rowid = L_rowid_tbl(i);
         end if;
         L_rowid_tbl.DELETE;
         L_adj_soh_tbl.DELETE;

      end if;

      if UPDATE_STAKE_SKU_LOC(O_error_message,
                              LP_item,
                              LP_rcv_loc_type,
                              LP_rcv_location,
                              LP_tran_date,
                              0,
                              LP_adj_qty) = FALSE then
         return FALSE;
      end if;
      end if;

      --- update sending location stock
      if LP_rcv_loc_type = 'W' OR LP_store_type_rcv IN ('C', 'F') then
        if LP_tran_date >= (LP_vdate - LP_lag_days) then
            SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                             'ITEM: '||LP_item);

            LP_table := 'edi_daily_sales';

            open  C_LOCK_EDI_DAILY_SALES2;
            fetch C_LOCK_EDI_DAILY_SALES2 BULK COLLECT INTO L_rowid_tbl,
                                                            L_adj_soh_tbl;
            close C_LOCK_EDI_DAILY_SALES2;

            if L_rowid_tbl.COUNT > 0 then
               FORALL i in 1..L_rowid_tbl.COUNT
                  UPDATE edi_daily_sales e
                     SET e.stock_on_hand = e.stock_on_hand - L_adj_soh_tbl(i)
                   WHERE rowid = L_rowid_tbl(i);
            end if;
         end if;

         if UPDATE_STAKE_SKU_LOC(O_error_message,
                                 LP_item,
                                 LP_send_loc_type,
                                 LP_send_location,
                                 LP_tran_date,
                                 LP_adj_qty * -1,
                                 0) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

RETURN TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                          LP_table,
                                          LP_item,
                                          to_char(L_loc));

      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.UPDATE_TSFO',
                                          to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_TSFO;
-------------------------------------------------------------------
FUNCTION UPDATE_TSFI(O_error_message  IN OUT    VARCHAR2)
RETURN BOOLEAN IS

L_rowid_tbl        rowid_TBL;
L_adj_soh_tbl      adj_qty_TBL;

L_loc           EDI_DAILY_SALES.LOC%TYPE;
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);


cursor C_LOCK_EDI_DAILY_SALES1 is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = LP_rcv_location
      and edi_daily_sales.loc_type = 'W'
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

cursor C_LOCK_EDI_DAILY_SALES2 is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = LP_rcv_location
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

cursor C_LOCK_EDI_DAILY_SALES3 is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = LP_rcv_location
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

BEGIN

   if LP_pack_ind = 'P' then
      if ((LP_send_loc_type = 'W') AND (LP_rcv_loc_type = 'W')) then

         --- update receiving location
         if LP_tran_date >= (LP_vdate - LP_lag_days) then
            SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                             'ITEM: '||LP_item);

            LP_table := 'edi_daily_sales';

            open  C_LOCK_EDI_DAILY_SALES1;
            fetch C_LOCK_EDI_DAILY_SALES1 BULK COLLECT INTO L_rowid_tbl,
                                                            L_adj_soh_tbl;
            close C_LOCK_EDI_DAILY_SALES1;

            if L_rowid_tbl.COUNT > 0 then
               FORALL i in 1..L_rowid_tbl.COUNT
               UPDATE edi_daily_sales e
                  SET e.in_transit_qty = e.in_transit_qty - L_adj_soh_tbl(i),
                      e.stock_on_hand = e.stock_on_hand + L_adj_soh_tbl(i)
                WHERE rowid = L_rowid_tbl(i);
            end if;

         end if;

         if UPDATE_STAKE_SKU_LOC(O_error_message,
                                 LP_item,
                                 LP_rcv_loc_type,
                                 LP_rcv_location,
                                 LP_tran_date,
                                 LP_adj_qty,
                                 LP_adj_qty * -1) = FALSE then
            return FALSE;
         end if;
      end if;

   elsif LP_pack_ind = 'C' then

      if LP_send_loc_type = 'W' then

         --- update receiving location
         if LP_tran_date >= (LP_vdate - LP_lag_days) then
            SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                             'ITEM: '||LP_item);

            LP_table := 'edi_daily_sales';

            open  C_LOCK_EDI_DAILY_SALES2;
            fetch C_LOCK_EDI_DAILY_SALES2 BULK COLLECT INTO L_rowid_tbl,
                                                            L_adj_soh_tbl;
            close C_LOCK_EDI_DAILY_SALES2;

            if L_rowid_tbl.COUNT > 0 then
               FORALL i in 1..L_rowid_tbl.COUNT
               UPDATE edi_daily_sales e
                  SET e.in_transit_qty = e.in_transit_qty - L_adj_soh_tbl(i),
                      e.stock_on_hand = e.stock_on_hand + L_adj_soh_tbl(i)
                WHERE rowid = L_rowid_tbl(i);
            end if;

         end if;

         if UPDATE_STAKE_SKU_LOC(O_error_message,
                                 LP_item,
                                 LP_rcv_loc_type,
                                 LP_rcv_location,
                                 LP_tran_date,
                                 LP_adj_qty,
                                 LP_adj_qty * -1) = FALSE then
            return FALSE;
         end if;
      end if;

   elsif LP_pack_ind = 'N' then

         --- update receiving location
      if LP_tran_date >= (LP_vdate - LP_lag_days) then
         SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                          'ITEM: '||LP_item);

            LP_table := 'edi_daily_sales';

            open  C_LOCK_EDI_DAILY_SALES3;
            fetch C_LOCK_EDI_DAILY_SALES3 BULK COLLECT INTO L_rowid_tbl,
                                                            L_adj_soh_tbl;
            close C_LOCK_EDI_DAILY_SALES3;

            if L_rowid_tbl.COUNT > 0 then
               FORALL i in 1..L_rowid_tbl.COUNT
               UPDATE edi_daily_sales e
                  SET e.in_transit_qty = e.in_transit_qty - L_adj_soh_tbl(i),
                      e.stock_on_hand = e.stock_on_hand + L_adj_soh_tbl(i)
                WHERE rowid = L_rowid_tbl(i);
            end if;

      end if;

      if UPDATE_STAKE_SKU_LOC(O_error_message,
                              LP_item,
                              LP_rcv_loc_type,
                              LP_rcv_location,
                              LP_tran_date,
                              LP_adj_qty,
                              LP_adj_qty * -1) = FALSE then
         return FALSE;
      end if;
   end if;

RETURN TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             LP_table,
                                             LP_item,
                                             to_char(L_loc));
      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.UPDATE_TSFI',
                                          to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_TSFI;
-------------------------------------------------------------------
FUNCTION UPDATE_RCV_RTV_INVADJ(O_error_message   IN OUT     VARCHAR2)
RETURN BOOLEAN IS

L_rowid_tbl        rowid_TBL;
L_adj_soh_tbl      adj_qty_TBL;

L_loc           EDI_DAILY_SALES.LOC%TYPE;
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

L_loc_type      ITEM_LOC.LOC_TYPE%TYPE;

cursor C_LOCK_EDI_DAILY_SALES is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = L_loc
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

BEGIN


   if LP_tran_wh = -1 then
      L_loc := LP_tran_store;
      L_loc_type := 'S';
   else
      L_loc := LP_tran_wh;
      L_loc_type := 'W';
   end if;

   --- RTV subtracts adjust qty
   if LP_tran_type = 'RTV' then
      LP_adj_qty := LP_adj_qty * -1;
   end if;

   --- update receiving location
   if LP_tran_date >= (LP_vdate - LP_lag_days) then
      SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                       'ITEM: '||LP_item);

      LP_table := 'edi_daily_sales';

      open  C_LOCK_EDI_DAILY_SALES;
      fetch C_LOCK_EDI_DAILY_SALES BULK COLLECT INTO L_rowid_tbl,
                                                     L_adj_soh_tbl;
      close C_LOCK_EDI_DAILY_SALES;

      if L_rowid_tbl.COUNT > 0 then
           FORALL i in 1..L_rowid_tbl.COUNT
           UPDATE edi_daily_sales e
              SET e.stock_on_hand = e.stock_on_hand + L_adj_soh_tbl(i)
            WHERE rowid = L_rowid_tbl(i);
      end if;

   end if;

   if UPDATE_STAKE_SKU_LOC(O_error_message,
                           LP_item,
                           L_loc_type,
                           L_loc,
                           LP_tran_date,
                           LP_adj_qty,
                           0) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             LP_table,
                                             LP_item,
                                             to_char(L_loc));
      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.UPDATE_RCV_RTV_INVADJ',
                                          to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_RCV_RTV_INVADJ;
-------------------------------------------------------------------
FUNCTION UPDATE_ILSOH(O_error_message   IN OUT     VARCHAR2)
RETURN BOOLEAN IS

L_rowid_tbl        rowid_TBL;
L_adj_soh_tbl      adj_qty_TBL;

L_loc           EDI_DAILY_SALES.LOC%TYPE;
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

L_loc_type      ITEM_LOC.LOC_TYPE%TYPE;


cursor C_LOCK_EDI_DAILY_SALES is
   select rowid, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = L_loc
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

cursor C_LOCK_STAKE_SKU_LOC is
   select rowid,
          LP_adj_qty
     from stake_sku_loc
    where stake_sku_loc.item = LP_item
      and stake_sku_loc.loc_type = L_loc_type
      and stake_sku_loc.location = L_loc
      and stake_sku_loc.processed = 'N'
      and cycle_count in (SELECT cycle_count
                            FROM stake_head
                           WHERE stake_head.stocktake_date >= LP_vdate)
      for update nowait;

BEGIN

   if LP_tran_wh = -1 then
      L_loc := LP_tran_store;
      L_loc_type := 'S';
   else
      L_loc := LP_tran_wh;
      L_loc_type := 'W';
   end if;


   --- update receiving location
   SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                    'ITEM: '||LP_item);

   LP_table := 'edi_daily_sales';

   open  C_LOCK_EDI_DAILY_SALES;
   fetch C_LOCK_EDI_DAILY_SALES BULK COLLECT INTO L_rowid_tbl,
                                                  L_adj_soh_tbl;
   close C_LOCK_EDI_DAILY_SALES;

   if L_rowid_tbl.COUNT > 0 then
           FORALL i in 1..L_rowid_tbl.COUNT
           UPDATE edi_daily_sales e
              SET e.stock_on_hand = L_adj_soh_tbl(i)
            WHERE rowid = L_rowid_tbl(i);
   end if;
   L_rowid_tbl.DELETE;
   L_adj_soh_tbl.DELETE;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'stake_sku_loc',
                    'ITEM: '||LP_item);
   LP_table := 'stake_sku_loc';

   open C_LOCK_STAKE_SKU_LOC;
   fetch C_LOCK_STAKE_SKU_LOC BULK COLLECT INTO L_rowid_tbl,
                                                L_adj_soh_tbl;
   close C_LOCK_STAKE_SKU_LOC;

   if L_rowid_tbl.COUNT > 0 then
      FORALL i in 1..L_rowid_tbl.COUNT
      UPDATE stake_sku_loc s
         SET s.snapshot_on_hand_qty = L_adj_soh_tbl(i)
       WHERE rowid = L_rowid_tbl(i);
   end if;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             LP_table,
                                             LP_item,
                                             to_char(L_loc));
      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.UPDATE_ILSOH',
                                          to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_ILSOH;
-------------------------------------------------------------------
FUNCTION UPDATE_SALADJ(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

L_rowid_tbl        rowid_TBL;
L_sales_qty_tbl    adj_qty_TBL;
L_adj_soh_tbl      adj_qty_TBL;

L_loc           EDI_DAILY_SALES.LOC%TYPE;
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_EDI_DAILY_SALES is
   select rowid, LP_sales_qty, LP_adj_qty
     from edi_daily_sales
    where edi_daily_sales.item = LP_item
      and edi_daily_sales.loc = LP_sal_store
      and LP_tran_date <= edi_daily_sales.tran_date
      and edi_daily_sales.tran_date < LP_vdate
      for update nowait;

BEGIN

   if LP_adj_qty < 0 then
      LP_sales_qty := 0;
   else
      LP_sales_qty := LP_adj_qty;
   end if;

   if LP_tran_date >= (LP_vdate - LP_lag_days) then
      if LP_rpt_freq = 'D' then

         SQL_LIB.SET_MARK('UPDATE', NULL, 'edi_daily_sales',
                          'ITEM: '||LP_item);

         LP_table := 'edi_daily_sales';

         open  C_LOCK_EDI_DAILY_SALES;
         fetch C_LOCK_EDI_DAILY_SALES BULK COLLECT INTO L_rowid_tbl,
                                                        L_sales_qty_tbl,
                                                        L_adj_soh_tbl;
         close C_LOCK_EDI_DAILY_SALES;

         if L_rowid_tbl.COUNT > 0 then
            FORALL i in 1..L_rowid_tbl.COUNT
            UPDATE edi_daily_sales e
               SET e.sales_qty = e.sales_qty + L_sales_qty_tbl(i),
                   e.stock_on_hand = e.stock_on_hand - L_adj_soh_tbl(i)
             WHERE rowid = L_rowid_tbl(i);
         end if;

      end if;
   end if;

   if UPDATE_STAKE_SKU_LOC(O_error_message,
                           LP_item,
                           'S',
                           LP_sal_store,
                           LP_tran_date,
                           LP_adj_qty * -1,
                           0) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             LP_table,
                                             LP_item,
                                             to_char(L_loc));

      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.UPDATE_SALADJ',
                                          to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_SALADJ;
------------------------------------------------------------------------
FUNCTION INSERT_STAKE_SKU_LOC(O_error_message         IN OUT    VARCHAR2,
                              I_loc_type              IN        ITEM_LOC.LOC_TYPE%TYPE,
                              I_location              IN        ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS


BEGIN
   -- Determine if average cost is being used.
   if SYSTEM_OPTIONS_SQL.STD_AV_IND(O_error_message,
                                    LP_std_av_ind) = FALSE then
      RETURN FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT','NULL', ' STAKE_SKU_LOC',
                    'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
   insert into stake_sku_loc(cycle_count,
                             loc_type,
                             location,
                             item,
                             snapshot_on_hand_qty,
                             snapshot_in_transit_qty,
                             snapshot_unit_cost,
                             snapshot_unit_retail,
                             processed,
                             physical_count_qty,
                             pack_comp_qty,
                             dept,
                             class,
                             subclass)
                      select LP_cycle_count,
                             I_loc_type,
                             I_location,
                             LP_item,
                             NULL,  /* insert as NULL.  Qty will be adjusted in update statement */
                             NULL,
                             DECODE(LP_std_av_ind,
                                    'A',
                                    ils.av_cost,
                                    ils.unit_cost),
                             il.unit_retail,
                             'N',
                             0,
                             0,
                             LP_dept,
                             LP_class,
                             LP_subclass
                        from item_loc il, item_loc_soh ils
                       where il.item = LP_item
                         and il.loc_type = I_loc_type
                         and il.loc = I_location
                         and il.item = ils.item
                         and il.loc_type  = ils.loc_type
                         and il.loc  = ils.loc
                         and not exists (select 'x'
                                          from stake_sku_loc skl
                                         where skl.item        = LP_item
                                           and skl.cycle_count = LP_cycle_count
                                           and skl.loc_type    = I_loc_type
                                           and skl.location    = I_location);

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'UPDATE_SNAPSHOT_SQL.INSERT_STAKE_SKU_LOC',
                                             to_char(SQLCODE));
      RETURN FALSE;
END INSERT_STAKE_SKU_LOC;
-------------------------------------------------------------------
FUNCTION PROC_STK_CNT_TD_WRITE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_dept            IN       ITEM_MASTER.DEPT%TYPE,
                               I_class           IN       ITEM_MASTER.CLASS%TYPE,
                               I_subclass        IN       ITEM_MASTER.SUBCLASS%TYPE,
                               I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_location        IN       ITEM_LOC.LOC%TYPE,
                               I_snapshot_retail IN       ITEM_LOC_SOH.AV_COST%TYPE,
                               I_snapshot_cost   IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                               I_tran_date       IN       PERIOD.VDATE%TYPE)
   RETURN BOOLEAN IS

   cursor C_ITEM_LOC_SOH is
      select ils.av_cost,im.pack_ind
        from item_loc_soh ils,
             item_master im
       where ils.item     = I_item
         and ils.item     = im.item
         and ils.loc_type = I_loc_type
         and ils.loc      = I_location;

   cursor C_DEPT_INFO is
      select profit_calc_type
        from deps
       where dept = I_dept;

   L_pgm_name               TRAN_DATA.PGM_NAME%TYPE        := 'UPDATE_SNAPSHOT_SQL.PROC_STK_CNT_TD_WRITE';
   L_units                  TRAN_DATA.UNITS%TYPE           := LP_adj_qty * -1;
   L_total_retail           ITEM_LOC.UNIT_RETAIL%TYPE      := I_snapshot_retail * L_units;
   L_total_cost             ITEM_LOC_SOH.AV_COST%TYPE      := I_snapshot_cost   * L_units;
   L_tran_code              TRAN_DATA.TRAN_CODE%TYPE       := 22;
   L_tran_code_var          TRAN_DATA.TRAN_CODE%TYPE       := 70;
   L_av_cost                ITEM_LOC_SOH.AV_COST%TYPE;
   L_cost_variance          ITEM_LOC_SOH.AV_COST%TYPE;
   L_std_av_ind             SYSTEM_OPTIONS.STD_AV_IND%TYPE := NULL;
   L_profit_calc_type       DEPS.PROFIT_CALC_TYPE%TYPE     := 0;
   L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    'item: '||to_char(I_item)||
                    ', loc: '||to_char(I_location));
   open C_ITEM_LOC_SOH;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    'item: '||to_char(I_item)||
                    ', loc: '||to_char(I_location));
   fetch C_ITEM_LOC_SOH into L_av_cost,L_pack_ind;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    'item: '||to_char(I_item)||
                    ', loc: '||to_char(I_location));
   close C_ITEM_LOC_SOH;

   if LP_tran_type in ('SALADJ','TSFO') then
      L_units := LP_adj_qty;
      L_total_cost := I_snapshot_cost * L_units;
      L_total_retail := I_snapshot_retail * L_units;
   end if;

   if L_pack_ind = 'N' then
      L_cost_variance := L_total_cost - (L_units * L_av_cost);
   end if;

   if STKLEDGR_SQL.LIBRARY_TRAN_DATA_INSERT(O_error_message,
                                            I_item,
                                            I_dept,
                                            I_class,
                                            I_subclass,
                                            I_location,
                                            I_loc_type,
                                            I_tran_date,
                                            L_tran_code,
                                            NULL,
                                            L_units,
                                            L_total_cost,
                                            L_total_retail,
                                            I_cycle_count,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            L_pgm_name,
                                            2) = FALSE then
      return FALSE;
   end if;

   -- posting cost variances (tran_code = 70) in tran_data for cases where sales is posted after stock count
   -- processing is completed for an item in a dept/class/subclass/location
   if L_cost_variance is NOT NULL and
      L_cost_variance != 0 then
      if SYSTEM_OPTIONS_SQL.STD_AV_IND(O_error_message,
                                       L_std_av_ind) = FALSE then
         return FALSE;
      end if;

      if L_std_av_ind = 'A' then
         SQL_LIB.SET_MARK('OPEN', 'C_DEPT_INFO', 'DEPS', NULL);
         open C_DEPT_INFO;
         SQL_LIB.SET_MARK('FETCH', 'C_DEPT_INFO', 'DEPS', NULL);
         fetch C_DEPT_INFO into L_profit_calc_type;
         SQL_LIB.SET_MARK('CLOSE', 'C_DEPT_INFO', 'DEPS', NULL);
         close C_DEPT_INFO;

         if L_profit_calc_type = 1 then
            if STKLEDGR_SQL.LIBRARY_TRAN_DATA_INSERT(O_error_message,
                                                     I_item,
                                                     I_dept,
                                                     I_class,
                                                     I_subclass,
                                                     I_location,
                                                     I_loc_type,
                                                     I_tran_date,
                                                     L_tran_code_var,
                                                     NULL,
                                                     L_units,
                                                     L_cost_variance,
                                                     NULL,
                                                     I_cycle_count,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     L_pgm_name) = FALSE then
               return FALSE;
            end if;
         end if;  -- end of L_profit_calc_type = 1
      end if; -- end of std_av_ind = 'A'
   end if; --- end of L_cost_variance != 0

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            TO_CHAR(SQLCODE));
         return FALSE;

END PROC_STK_CNT_TD_WRITE;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_STOCK_COUNT(O_error_message       IN OUT    VARCHAR2,
                           I_store               IN        ITEM_LOC.LOC%TYPE,
                           I_wh                  IN        ITEM_LOC.LOC%TYPE,
                           I_tran_date           IN        PERIOD.VDATE%TYPE)
RETURN BOOLEAN IS

L_loc_type        ITEM_LOC.LOC_TYPE%TYPE;
L_location        ITEM_LOC.LOC%TYPE;

BEGIN

   if I_store = -1 then
      L_location := I_wh;
      L_loc_type := 'W';
   else
      L_location := I_store;
      L_loc_type := 'S';
   end if;

   if CHECK_STOCK_COUNT(O_error_message,
                        L_loc_type,
                        L_location,
                        I_tran_date) = FALSE then
      RETURN FALSE;
   end if;


RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'UPDATE_SNAPSHOT_SQL.CHECK_STOCK_COUNT',
                                             to_char(SQLCODE));
   RETURN FALSE;
END CHECK_STOCK_COUNT;
-------------------------------------------------------------------
FUNCTION CHECK_STOCK_COUNT(O_error_message       IN OUT    VARCHAR2,
                           I_loc_type            IN        ITEM_LOC.LOC_TYPE%TYPE,
                           I_location            IN        ITEM_LOC.LOC%TYPE,
                           I_tran_date           IN        PERIOD.VDATE%TYPE)
RETURN BOOLEAN IS

   L_processed_ind   STAKE_SKU_LOC.PROCESSED%TYPE := 'N';
   L_found           BOOLEAN                      := FALSE;
   L_tran_date       DATE                         := NVL(I_tran_date, LP_vdate);
   L_check_more      NUMBER;

   L_snapshot_cost           ITEM_LOC_SOH.AV_COST%TYPE   := 0;
   L_snapshot_retail         ITEM_LOC.UNIT_RETAIL%TYPE   := 0;
   L_stock_count_processed   BOOLEAN                     := FALSE;
   L_cycle_count             STAKE_HEAD.CYCLE_COUNT%TYPE := NULL;

cursor C_CHECK_STOCKTAKE_DATES is
   select 1
     from stake_head sh
    where sh.stocktake_date >= L_tran_date
      and sh.stocktake_date - LP_lockout_days <= L_tran_date;

cursor C_CHECK_STOCK_COUNT_PRE is
   select ssl.cycle_count,
          ssl.processed
     from stake_head sh,
          stake_sku_loc ssl
    where sh.stocktake_date >= L_tran_date
      and sh.stocktake_date - LP_lockout_days <= L_tran_date
      and sh.loc_type = I_loc_type
      and ssl.cycle_count = sh.cycle_count
      and ssl.loc_type = I_loc_type
      and ssl.location = I_location
      and ssl.item = LP_item;

cursor C_CHECK_STOCK_COUNT is
   select /*+ index(ssl,pk_stake_sku_loc) */
          ssl.cycle_count,
          ssl.processed
     from stake_head sh,
          stake_sku_loc ssl,
          stake_prod_loc spl
    where sh.stocktake_date >= L_tran_date
      and sh.stocktake_date - LP_lockout_days <= L_tran_date
      and sh.loc_type = I_loc_type
      and spl.cycle_count = sh.cycle_count
      and spl.loc_type = I_loc_type
      and spl.location = I_location
      and spl.dept = LP_dept
      and spl.class = LP_class
      and spl.subclass = LP_subclass
      and ssl.cycle_count = sh.cycle_count
      and ssl.loc_type = I_loc_type
      and ssl.location = I_location
      and ssl.dept = LP_dept
      and ssl.class = LP_class
      and ssl.subclass = LP_subclass
      and ROWNUM = 1;

cursor C_CHECK_PRODUCT_LOCATION is
   select sh.cycle_count
     from stake_head sh,
          stake_product sp,
          stake_location sl
    where sh.stocktake_date >= L_tran_date
      and sh.stocktake_date - LP_lockout_days <= L_tran_date
      and sp.cycle_count = sh.cycle_count
      and sl.cycle_count = sh.cycle_count
      and sl.loc_type = I_loc_type
      and sl.location = I_location
      and sp.dept = LP_dept
      and (sp.class is NULL or
           sp.class = LP_class)
      and (sp.subclass is NULL or
           sp.subclass = LP_subclass);

BEGIN

   if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                     LP_item,
                                     LP_dept,
                                     LP_class,
                                     LP_subclass) = FALSE then
      RETURN FALSE;
   end if;

   if I_tran_date < LP_vdate then
      if STKCNT_ATTRIB_SQL.STOCK_COUNT_PROCESSED(O_error_message,
                                                 L_stock_count_processed,
                                                 L_cycle_count,
                                                 L_snapshot_cost,
                                                 L_snapshot_retail,
                                                 L_tran_date,
                                                 LP_item,
                                                 I_loc_type,
                                                 I_location) = FALSE then
         return FALSE;
      end if;

      if L_stock_count_processed = TRUE and LP_tran_type in ('SALADJ','STKADJ','TSFO','INVADJ') then
         if PROC_STK_CNT_TD_WRITE(O_error_message,
                                  L_cycle_count,
                                  LP_item,
                                  LP_dept,
                                  LP_class,
                                  LP_subclass,
                                  I_loc_type,
                                  I_location,
                                  L_snapshot_retail,
                                  L_snapshot_cost,
                                  L_tran_date) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if LP_prev_stktake_date = L_tran_date then
      if LP_prev_stktake_date_notfound then
         return TRUE;
      end if;
   else
      LP_prev_stktake_date := L_tran_date;
      SQL_LIB.SET_MARK('OPEN','C_CHECK_STOCKTAKE_DATES', 'STAKE_HEAD, SYSTEM_OPTIONS',
                       'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
      open C_CHECK_STOCKTAKE_DATES;

      SQL_LIB.SET_MARK('FETCH','C_CHECK_STOCKTAKE_DATES', 'STAKE_HEAD, SYSTEM_OPTIONS',
                       'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
      fetch C_CHECK_STOCKTAKE_DATES into L_check_more;
      if C_CHECK_STOCKTAKE_DATES%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_STOCKTAKE_DATES', 'STAKE_HEAD, SYSTEM_OPTIONS',
                          'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
         close C_CHECK_STOCKTAKE_DATES;
         LP_prev_stktake_date_notfound := TRUE;
         return TRUE;
      else
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_STOCKTAKE_DATES', 'STAKE_HEAD, SYSTEM_OPTIONS',
                          'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
         close C_CHECK_STOCKTAKE_DATES;
         LP_prev_stktake_date_notfound := FALSE;
      end if;
   end if;

   --

   SQL_LIB.SET_MARK('OPEN','C_CHECK_STOCK_COUNT_PRE', 'STAKE_HEAD, STAKE_SKU_LOC',
                    'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
   open C_CHECK_STOCK_COUNT_PRE;

   SQL_LIB.SET_MARK('FETCH','C_CHECK_STOCK_COUNT_PRE', 'STAKE_HEAD, STAKE_SKU_LOC',
                    'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
   fetch C_CHECK_STOCK_COUNT_PRE into LP_cycle_count,
                                  L_processed_ind;
   if C_CHECK_STOCK_COUNT_PRE%FOUND then
      if L_processed_ind = 'N' then
         return TRUE;
      end if;
      L_found := TRUE;
   else
      -- Check stake product/stake location if item product group - location is on stock count
      SQL_LIB.SET_MARK('OPEN','C_CHECK_PRODUCT_LOCATION', 'STAKE_HEAD, STAKE_LOCATION, STAKE_PRODUCT',
                       'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
      open C_CHECK_PRODUCT_LOCATION;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_PRODUCT_LOCATION', 'STAKE_HEAD, STAKE_LOCATION, STAKE_PRODUCT',
                       'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
      fetch C_CHECK_PRODUCT_LOCATION into LP_cycle_count;

      -- if on stock count check if the group - location is processed
      if C_CHECK_PRODUCT_LOCATION%FOUND then
         L_found := TRUE;
      ---

         SQL_LIB.SET_MARK('OPEN','C_CHECK_STOCK_COUNT', 'STAKE_HEAD, STAKE_SKU_LOC, STAKE_PROD_LOC',
                          'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
         open C_CHECK_STOCK_COUNT;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_STOCK_COUNT', 'STAKE_HEAD, STAKE_SKU_LOC, STAKE_PROD_LOC',
                          'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
         fetch C_CHECK_STOCK_COUNT into LP_cycle_count,
                                        L_processed_ind;


         SQL_LIB.SET_MARK('CLOSE','C_CHECK_STOCK_COUNT', 'STAKE_HEAD, STAKE_SKU_LOC, STAKE_PROD_LOC',
                          'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
         close C_CHECK_STOCK_COUNT;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_CHECK_PRODUCT_LOCATION', 'STAKE_HEAD, STAKE_LOCATION, STAKE_PRODUCT',
                       'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
      close C_CHECK_PRODUCT_LOCATION;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_STOCK_COUNT_PRE', 'STAKE_HEAD, STAKE_SKU_LOC',
                    'ITEM: '||LP_item||', loc_type: '||I_loc_type||',location: '||to_char(I_location));
   close C_CHECK_STOCK_COUNT_PRE;

   ---

   if L_found and L_processed_ind = 'N' then
      if INSERT_STAKE_SKU_LOC(O_error_message,
                              I_loc_type,
                              I_location) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'UPDATE_SNAPSHOT_SQL.CHECK_STOCK_COUNT',
                                             to_char(SQLCODE));
      RETURN FALSE;
END CHECK_STOCK_COUNT;
-------------------------------------------------------------------
FUNCTION EXECUTE(O_error_message   IN OUT    VARCHAR2,
                 I_tran_type       IN        VARCHAR2,
                 I_item            IN        ITEM_MASTER.ITEM%TYPE,
                 I_rpt_freq        IN        VARCHAR2,
                 I_sal_store       IN        ITEM_LOC.LOC%TYPE,
                 I_tran_date       IN        DATE,
                 I_vdate           IN        DATE,
                 I_adj_qty         IN        NUMBER)
RETURN BOOLEAN IS

L_tran_date   PERIOD.VDATE%TYPE := I_tran_date;
L_store       ITEM_LOC.LOC%TYPE := I_sal_store;
L_wh          ITEM_LOC.LOC%TYPE := -1;

BEGIN
    LP_tran_type      := I_tran_type;
    LP_item           := I_item;
    LP_rpt_freq       := I_rpt_freq;
    LP_sal_store      := NVL(I_sal_store, -1);
    LP_tran_date      := I_tran_date;
    LP_vdate          := I_vdate;
    LP_adj_qty        := I_adj_qty;
    LP_lag_days       := NULL;

    if LP_item is NULL OR
          ((LP_rpt_freq != 'D') AND (LP_rpt_freq != 'W')
                                AND (LP_rpt_freq != 'N')) OR
          LP_sal_store = -1 OR
          LP_tran_date is NULL OR
          LP_adj_qty is NULL then
             O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                                 NULL,
                                                 NULL,
                                                 NULL);
             RETURN FALSE;
    end if;

    if LP_tran_date >= LP_vdate then
       RETURN TRUE;
    end if;
    ---
    if GET_LAG_DAYS(O_error_message, LP_lag_days) = false then
       RETURN FALSE;
    end if;
    ---
    if LP_tran_type = 'SALADJ' then
       if CHECK_STOCK_COUNT(O_error_message,
                            L_store,
                            L_wh,
                            L_tran_date) = FALSE then
          RETURN FALSE;
       end if;
       ---
       if UPDATE_SALADJ(O_error_message) = FALSE then
          RETURN FALSE;
       end if;
    else
       O_error_message := SQL_LIB.CREATE_MSG('INV_EDI_TRAN_TYPE',
                                           LP_tran_type,
                                           NULL,
                                           NULL);
       RETURN FALSE;
    end if;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.EXECUTE',
                                          to_char(SQLCODE));
      RETURN FALSE;
END EXECUTE;
------------------------------------------------------------------------
FUNCTION EXECUTE(O_error_message      IN OUT   VARCHAR2,
                 I_tran_type          IN       VARCHAR2,
                 I_item               IN       ITEM_MASTER.ITEM%TYPE,
                 I_pack_ind           IN       VARCHAR2,
                 I_rcv_store          IN       ITEM_LOC.LOC%TYPE,
                 I_rcv_wh             IN       ITEM_LOC.LOC%TYPE,
                 I_send_store         IN       ITEM_LOC.LOC%TYPE,
                 I_send_wh            IN       ITEM_LOC.LOC%TYPE,
                 I_tran_date          IN       DATE,
                 I_vdate              IN       DATE,
                 I_adj_qty            IN       NUMBER,
                 I_rcv_as_type        IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE   DEFAULT NULL)
RETURN BOOLEAN IS

L_tran_date   PERIOD.VDATE%TYPE := I_tran_date;
L_store       ITEM_LOC.LOC%TYPE;
L_wh          ITEM_LOC.LOC%TYPE;

cursor C_GET_STORE_TYPE_RCV is
   select store_type
     from store
    where store = I_rcv_store;

cursor C_GET_STORE_TYPE_SEND is
   select store_type
     from store
    where store = I_send_store;

cursor C_GET_RCV_LOCATION is
   select store,'S'
     from store
    where store=I_rcv_store
   union
   select wh,'W'
     from wh
    where wh=I_rcv_wh;

 cursor C_GET_SEND_LOCATION is
   select wh,'W'
     from wh
    where wh=I_send_wh
   union
   select store,'S'
     from store
    where store=I_send_store;

BEGIN
    LP_tran_type      := I_tran_type;
    LP_item           := I_item;
    LP_pack_ind       := I_pack_ind;
    LP_rcv_store      := NVL(I_rcv_store, -1);
    LP_rcv_wh         := NVL(I_rcv_wh, -1);
    LP_send_store     := NVL(I_send_store, -1);
    LP_send_wh        := NVL(I_send_wh, -1);
    LP_tran_date      := I_tran_date;
    LP_vdate          := I_vdate;
    LP_adj_qty        := I_adj_qty;
    LP_lag_days       := NULL;

    if (LP_rcv_store != -1) then
       open C_GET_STORE_TYPE_RCV;
       fetch C_GET_STORE_TYPE_RCV into LP_store_type_rcv;
       close C_GET_STORE_TYPE_RCV;
    else
       LP_store_type_rcv := -1;
    end if;

    if (LP_send_store != -1) then
       open C_GET_STORE_TYPE_SEND;
       fetch C_GET_STORE_TYPE_SEND into LP_store_type_snd;
       close C_GET_STORE_TYPE_SEND;
    else
       LP_store_type_snd := -1;
    end if;

    open C_GET_RCV_LOCATION;
    fetch C_GET_RCV_LOCATION into LP_rcv_location,LP_rcv_loc_type;
    close C_GET_RCV_LOCATION;

    open C_GET_SEND_LOCATION;
    fetch C_GET_SEND_LOCATION into LP_send_location,LP_send_loc_type;
    close C_GET_SEND_LOCATION;


   if LP_item is NULL OR
      ((LP_pack_ind != 'P') AND (LP_pack_ind != 'C')
        AND (LP_pack_ind != 'N')) OR
      ((LP_rcv_store = -1) AND (LP_rcv_wh = -1)) OR
      ((LP_rcv_store > 0) AND (LP_rcv_wh > 0)) OR
      ((LP_send_store = -1) AND (LP_send_wh = -1)) OR
      ((LP_send_store > 0) AND (LP_send_wh > 0)) OR
      LP_tran_date is NULL OR
      ((LP_adj_qty < 0) AND (LP_tran_type != 'TSFI')) OR
      LP_adj_qty is NULL then

      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      RETURN FALSE;
   end if;

   if LP_tran_date >= LP_vdate then
      RETURN TRUE;
   end if;
   ---
   if GET_LAG_DAYS(O_error_message, LP_lag_days) = false then
      RETURN FALSE;
   end if;
   ---
   if LP_tran_type = 'TSFO' then
      if (LP_pack_ind = 'P' and NVL(I_rcv_as_type,'P') = 'P') or (LP_pack_ind = 'C' and NVL(I_rcv_as_type,'E') = 'E') or (LP_pack_ind = 'N') then

         L_store := LP_send_store;
         L_wh    := LP_send_wh;
         ---

         if CHECK_STOCK_COUNT(O_error_message,
                              L_store,
                              L_wh,


                              L_tran_date) = FALSE then
            RETURN FALSE;
         end if;
      end if;
      ---
      if UPDATE_TSFO(O_error_message) = FALSE then
         RETURN FALSE;
      end if;
   elsif LP_tran_type = 'TSFI' then
   ---
      L_store := LP_rcv_store;
      L_wh    := LP_rcv_wh;
      ---
      if CHECK_STOCK_COUNT(O_error_message,
                           L_store,
                           L_wh,
                           L_tran_date) = FALSE then
         RETURN FALSE;
      end if;
      ---
      if UPDATE_TSFI(O_error_message) = FALSE then
         RETURN FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_EDI_TRAN_TYPE',
                                            LP_tran_type,
                                            NULL,
                                            NULL);
      RETURN FALSE;
   end if;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'UPDATE_SNAPSHOT_SQL.EXECUTE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END EXECUTE;
-------------------------------------------------------------------
------------------------------------------------------------------------
FUNCTION EXECUTE(O_error_message      IN OUT   VARCHAR2,
                 I_tran_type          IN       VARCHAR2,
                 I_item               IN       ITEM_MASTER.ITEM%TYPE,
                 I_pack_ind           IN       VARCHAR2,
                 I_rcv_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                 I_rcv_location       IN       ITEM_LOC.LOC%TYPE,
                 I_send_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                 I_send_location      IN       ITEM_LOC.LOC%TYPE,
                 I_tran_date          IN       DATE,
                 I_vdate              IN       DATE,
                 I_adj_qty            IN       NUMBER,
                 I_rcv_as_type        IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE   DEFAULT NULL)

RETURN BOOLEAN IS

L_tran_date   PERIOD.VDATE%TYPE := I_tran_date;
L_store       ITEM_LOC.LOC%TYPE;
L_wh          ITEM_LOC.LOC%TYPE;

cursor C_GET_STORE_TYPE_RCV is
   select store_type
     from store
    where store = I_rcv_location;

cursor C_GET_STORE_TYPE_SND is
   select store_type
     from store
    where store = I_send_location;

BEGIN
    LP_tran_type      := I_tran_type;
    LP_item           := I_item;
    LP_pack_ind       := I_pack_ind;
    LP_rcv_loc_type   := I_rcv_loc_type;
    LP_rcv_location   := I_rcv_location;
    LP_send_loc_type  := I_send_loc_type;
    LP_send_location  := I_send_location;
    LP_tran_date      := I_tran_date;
    LP_vdate          := I_vdate;
    LP_adj_qty        := I_adj_qty;
    LP_lag_days       := NULL;


      if I_rcv_loc_type = 'S' then
         open C_GET_STORE_TYPE_RCV;
         fetch C_GET_STORE_TYPE_RCV into LP_store_type_rcv;
         close C_GET_STORE_TYPE_RCV;
      elsif I_send_loc_type = 'S' then
         open C_GET_STORE_TYPE_SND;
         fetch C_GET_STORE_TYPE_SND into LP_store_type_snd;
         close C_GET_STORE_TYPE_SND;
      end if;

   if LP_item is NULL OR
        ((LP_pack_ind != 'P') AND (LP_pack_ind != 'C')
                              AND (LP_pack_ind != 'N')) OR
        LP_rcv_loc_type is NULL OR
        LP_rcv_location is NULL OR
        LP_send_loc_type is NULL OR
        LP_send_location is NULL OR
        LP_tran_date is NULL OR
        ((LP_adj_qty < 0) AND (LP_tran_type not in ('TSFO','TSFI'))) OR
        LP_adj_qty is NULL then

        O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      RETURN FALSE;
   end if;

   if LP_tran_date >= LP_vdate then
      RETURN TRUE;
   end if;
   ---
   if GET_LAG_DAYS(O_error_message, LP_lag_days) = false then
      RETURN FALSE;
   end if;
   ---
   if LP_tran_type = 'TSFO' then
      if (LP_pack_ind = 'P' and NVL(I_rcv_as_type,'P') = 'P') or (LP_pack_ind = 'C' and NVL(I_rcv_as_type,'E') = 'E') or (LP_pack_ind = 'N') then
         if LP_store_type_snd IN ('C', 'F') then

            if CHECK_STOCK_COUNT(O_error_message,
                                 LP_send_loc_type,
                                 LP_send_location,


                                 L_tran_date) = FALSE then
               RETURN FALSE;
            end if;
         end if;
      end if;
      ---

      if UPDATE_TSFO(O_error_message) = FALSE then
         RETURN FALSE;
      end if;
   elsif LP_tran_type = 'TSFI' then
      if CHECK_STOCK_COUNT(O_error_message,
                           LP_rcv_loc_type,
                           LP_rcv_location,
                           L_tran_date) = FALSE then
         RETURN FALSE;
      end if;
      ---
      if UPDATE_TSFI(O_error_message) = FALSE then
         RETURN FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_EDI_TRAN_TYPE',
                                             LP_tran_type,
                                             NULL,
                                             NULL);
      RETURN FALSE;
   end if;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'UPDATE_SNAPSHOT_SQL.EXECUTE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END EXECUTE;
-------------------------------------------------------------------
FUNCTION EXECUTE(O_error_message       IN OUT    VARCHAR2,
                 I_tran_type           IN        VARCHAR2,
                 I_item                IN        ITEM_MASTER.ITEM%TYPE,
                 I_tran_store          IN        ITEM_LOC.LOC%TYPE,
                 I_tran_wh             IN        ITEM_LOC.LOC%TYPE,
                 I_tran_date           IN        DATE,
                 I_vdate               IN        DATE,
                 I_adj_qty             IN        NUMBER)
RETURN BOOLEAN IS

L_tran_date   PERIOD.VDATE%TYPE := I_tran_date;
L_store       ITEM_LOC.LOC%TYPE := I_tran_store;
L_wh          ITEM_LOC.LOC%TYPE := I_tran_wh;


BEGIN

    LP_tran_type      := I_tran_type;
    LP_item           := I_item;
    LP_tran_store     := NVL(I_tran_store, -1);
    LP_tran_wh        := NVL(I_tran_wh, -1);
    LP_tran_date      := I_tran_date;
    LP_vdate          := I_vdate;
    LP_adj_qty        := I_adj_qty;
    LP_lag_days       := NULL;

    if LP_item is NULL OR
           ((LP_tran_store = -1) AND (LP_tran_wh = -1)) OR
           ((LP_tran_store > 0) AND (LP_tran_wh > 0)) OR
           LP_tran_date is NULL OR
           LP_adj_qty is NULL then
                O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                                     NULL,
                                                     NULL,
                                                     NULL);
              RETURN FALSE;
    end if;

    if LP_tran_date >= LP_vdate then
       RETURN TRUE;
    end if;
    ---
    if GET_LAG_DAYS(O_error_message, LP_lag_days) = false then
       RETURN FALSE;
    end if;
    ---
    if ((LP_tran_type = 'RCV') OR
        (LP_tran_type = 'RTV') OR
        (LP_tran_type = 'INVADJ') OR
        (LP_tran_type = 'INADJO')) then
       if CHECK_STOCK_COUNT(O_error_message,
                            L_store,
                            L_wh,
                            L_tran_date) = FALSE then
          RETURN FALSE;
       end if;
       ---
       if UPDATE_RCV_RTV_INVADJ(O_error_message) = FALSE then
          RETURN FALSE;
       end if;
    else
       O_error_message := SQL_LIB.CREATE_MSG('INV_EDI_TRAN_TYPE',
                                              LP_tran_type,
                                              NULL,
                                              NULL);
       RETURN FALSE;
    end if;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.EXECUTE',
                                          to_char(SQLCODE));
      RETURN FALSE;
END EXECUTE;


-------------------------------------------------------------------
FUNCTION EXECUTE(O_error_message       IN OUT    VARCHAR2,
                 I_tran_type           IN        VARCHAR2,
                 I_item                IN        ITEM_MASTER.ITEM%TYPE,
                 I_tran_loc_type       IN        ITEM_LOC.LOC_TYPE%TYPE,
                 I_tran_location       IN        ITEM_LOC.LOC%TYPE,
                 I_adj_qty             IN        NUMBER)
RETURN BOOLEAN IS

L_tran_date   PERIOD.VDATE%TYPE := NULL;
L_loc_type    ITEM_LOC.LOC_TYPE%TYPE := I_tran_loc_type;
L_location    ITEM_LOC.LOC%TYPE := I_tran_location;

BEGIN

    LP_tran_type      := I_tran_type;
    LP_item           := I_item;
    LP_tran_loc_type  := I_tran_loc_type;
    LP_tran_location  := I_tran_location;
    LP_adj_qty        := I_adj_qty;
    LP_lag_days       := NULL;

    if LP_item is NULL OR
       LP_tran_loc_type is NULL OR
       LP_tran_location is NULL OR
       LP_adj_qty is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
          RETURN FALSE;
    end if;

    if LP_tran_date >= LP_vdate then
       RETURN TRUE;
    end if;
    ---
    if LP_tran_type = 'STKADJ' then
       if CHECK_STOCK_COUNT(O_error_message,
                            L_loc_type,
                            L_location,
                            L_tran_date) = FALSE then
          RETURN FALSE;
       end if;
       ---
       if UPDATE_STKADJ(O_error_message) = FALSE then
          RETURN FALSE;
       end if;
    else
       O_error_message := SQL_LIB.CREATE_MSG('INV_EDI_TRAN_TYPE',
                                             LP_tran_type,
                                             NULL,
                                             NULL);
       RETURN FALSE;
    end if;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.EXECUTE',
                                          to_char(SQLCODE));
      RETURN FALSE;
END EXECUTE;

-------------------------------------------------------------------
FUNCTION EXECUTE(O_error_message       IN OUT    VARCHAR2,
                 I_item                IN        ITEM_MASTER.ITEM%TYPE,
                 I_tran_store          IN        ITEM_LOC.LOC%TYPE,
                 I_adj_qty             IN        NUMBER)
RETURN BOOLEAN IS

L_tran_date   PERIOD.VDATE%TYPE := NULL;
L_store       ITEM_LOC.LOC%TYPE := I_tran_store;
L_wh          ITEM_LOC.LOC%TYPE := -1;

BEGIN

    LP_item           := I_item;
    LP_tran_store     := NVL(I_tran_store, -1);
    LP_tran_wh        := -1;
    LP_adj_qty        := I_adj_qty;
    LP_lag_days       := NULL;


    if LP_item is NULL OR
           ((LP_tran_store = -1) AND (LP_tran_wh = -1)) OR
           ((LP_tran_store > 0) AND (LP_tran_wh > 0)) OR
           LP_adj_qty is NULL THEN

              O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                                    NULL,
                                                    NULL,
                                                    NULL);
              RETURN FALSE;
    end if;
    ---
    if CHECK_STOCK_COUNT(O_error_message,
                         L_store,
                         L_wh,
                         L_tran_date) = FALSE then
       RETURN FALSE;
    end if;
    ---
    if UPDATE_ILSOH(O_error_message) = FALSE then
       RETURN FALSE;
    end if;


RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.EXECUTE',
                                          to_char(SQLCODE));
END EXECUTE;
-------------------------------------------------------------------

FUNCTION UPDATE_STAKE_SKU_LOC(O_error_message IN OUT VARCHAR2,
                              I_item          IN     item_loc.item%TYPE,
                              I_loc_type      IN     item_loc.loc_type%TYPE,
                              I_location      IN     item_loc.loc%TYPE,
                              I_tran_date     IN     DATE,
                              I_adj_soh       IN     item_loc_soh.stock_on_hand%TYPE,
                              I_adj_intran    IN     item_loc_soh.stock_on_hand%TYPE)
RETURN BOOLEAN IS

L_rowid_tbl        rowid_TBL;
L_adj_soh_tbl      adj_qty_TBL;
L_adj_intran_tbl   adj_qty_TBL;

RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_STAKE_SKU_LOC is
   select rowid,
          I_adj_soh,
          I_adj_intran
     from stake_sku_loc
    where stake_sku_loc.item  = I_item
      and stake_sku_loc.loc_type = I_loc_type
      and stake_sku_loc.location = I_location
      and stake_sku_loc.processed = 'N'
      and stake_sku_loc.snapshot_on_hand_qty is not NULL
      and cycle_count in (SELECT cycle_count
                            FROM stake_head
                           WHERE stake_head.stocktake_date >= I_tran_date)
      for update nowait;

BEGIN

   LP_table := 'stake_sku_loc';
   SQL_LIB.SET_MARK('UPDATE', NULL, 'stake_sku_loc',
                    'ITEM: '||I_item);

   open C_LOCK_STAKE_SKU_LOC;
   fetch C_LOCK_STAKE_SKU_LOC BULK COLLECT INTO L_rowid_tbl,
                                                L_adj_soh_tbl,
                                                L_adj_intran_tbl;
   close C_LOCK_STAKE_SKU_LOC;

   if L_rowid_tbl.COUNT > 0 then
      FORALL i in 1..L_rowid_tbl.COUNT
      UPDATE stake_sku_loc s
         SET s.snapshot_on_hand_qty = NVL(s.snapshot_on_hand_qty,0) + L_adj_soh_tbl(i),
             s.snapshot_in_transit_qty = NVL(s.snapshot_in_transit_qty,0) + L_adj_intran_tbl(i)
       WHERE rowid = L_rowid_tbl(i);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             LP_table,
                                             LP_item,
                                             NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'UPDATE_SNAPSHOT_SQL.UPDATE_STAKE_SKU_LOC',
                                          to_char(SQLCODE));
END UPDATE_STAKE_SKU_LOC;

-------------------------------------------------------------------
FUNCTION UPDATE_SNAPSHOT_COST(O_error_message IN OUT VARCHAR2,
                              I_item          IN     item_loc.item%TYPE,
                              I_loc_type      IN     item_loc.loc_type%TYPE,
                              I_location      IN     item_loc.loc%TYPE,
                              I_tran_date     IN     DATE,
                              I_adj_qty       IN     item_loc_soh.stock_on_hand%TYPE,
                              I_unit_cost     IN     item_loc_soh.unit_cost%TYPE)
RETURN BOOLEAN IS

TYPE cycle_count_TBL                is table of stake_head.cycle_count%TYPE;
TYPE snapshot_unit_cost_TBL         is table of stake_sku_loc.snapshot_unit_cost%TYPE;
TYPE snapshot_on_hand_qty_TBL       is table of stake_sku_loc.snapshot_on_hand_qty%TYPE;
TYPE snapshot_in_transit_qty_TBL    is table of stake_sku_loc.snapshot_in_transit_qty%TYPE;

L_pack_qty                          stake_sku_loc.snapshot_on_hand_qty%TYPE;
L_neg_soh_wac_adj_amt               item_loc_soh.av_cost%TYPE;
L_extended_cost                     item_loc_soh.av_cost%TYPE := NULL; -- Related to Catch weight

L_rowid_tbl                         rowid_TBL                     := rowid_TBL();
L_cycle_count_tbl                   cycle_count_TBL               := cycle_count_TBL();
L_snapshot_unit_cost_tbl            snapshot_unit_cost_TBL        := snapshot_unit_cost_TBL();
L_new_snapshot_unit_cost_tbl        snapshot_unit_cost_TBL        := snapshot_unit_cost_TBL();
L_snapshot_on_hand_qty_tbl          snapshot_on_hand_qty_TBL      := snapshot_on_hand_qty_TBL();
L_snapshot_in_transit_qty_tbl       snapshot_in_transit_qty_TBL   := snapshot_in_transit_qty_TBL();

RECORD_LOCKED        EXCEPTION;
PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_STAKE_SKU_LOC is
     select l.rowid,
            nvl(l.snapshot_unit_cost,0),
            nvl(l.snapshot_on_hand_qty,0),
            nvl(l.snapshot_in_transit_qty,0),
            l.cycle_count
       from stake_head h,
            stake_sku_loc l
      where h.stocktake_date >= I_tran_date
        and h.stocktake_date <= LP_vdate
        and l.cycle_count = h.cycle_count
        and l.item = I_item
        and l.location = I_location
        and l.loc_type = I_loc_type
        and l.processed = 'N'
        and l.snapshot_on_hand_qty is not null
        for update nowait;

cursor C_GET_PACK_QTY (I_cycle_count IN stake_head.cycle_count%TYPE) is
    select nvl(sum(l.snapshot_on_hand_qty * v.qty), 0)
      from stake_sku_loc l,
           v_packsku_qty v
     where v.item = I_item
       and l.item = v.pack_no
       and l.cycle_count = I_cycle_count
       and l.location = I_location
       and l.loc_type = I_loc_type;

   BEGIN

      if I_tran_date >= LP_vdate then
         RETURN TRUE;
      end if;

      LP_table := 'stake_sku_loc';
       open C_LOCK_STAKE_SKU_LOC;
      fetch C_LOCK_STAKE_SKU_LOC bulk collect into L_rowid_tbl,
                                                   L_snapshot_unit_cost_tbl,
                                                   L_snapshot_on_hand_qty_tbl,
                                                   L_snapshot_in_transit_qty_tbl,
                                                   L_cycle_count_tbl;
      close C_LOCK_STAKE_SKU_LOC;

       L_new_snapshot_unit_cost_tbl.delete;

      if L_rowid_tbl.COUNT > 0 then
       --
         for i in 1 .. L_rowid_tbl.COUNT loop
         --
            If I_loc_type = 'W' then
               open C_GET_PACK_QTY(L_cycle_count_tbl(i));
               fetch C_GET_PACK_QTY into L_pack_qty;
               close C_GET_PACK_QTY;
            else
               L_pack_qty := 0;
            end if;
         --
         L_new_snapshot_unit_cost_tbl.extend(i);
            --
         if STKLEDGR_ACCTING_SQL.WAC_CALC_QTY_CHANGE
                                (O_error_message,
                                 L_new_snapshot_unit_cost_tbl(i),
                                 L_neg_soh_wac_adj_amt,
                                 L_snapshot_unit_cost_tbl(i),
                                ((L_snapshot_on_hand_qty_tbl(i) + L_snapshot_in_transit_qty_tbl(i) + L_pack_qty) - I_adj_qty) ,
                                  I_unit_cost,
                                  I_adj_qty,
                                  L_extended_cost) = FALSE then
             return FALSE;
         end if;
            --
         end loop;
       --
        FORALL i in 1..L_rowid_tbl.COUNT
            update stake_sku_loc
            set snapshot_unit_cost = round(L_new_snapshot_unit_cost_tbl(i) ,4)
            Where rowid = L_rowid_tbl(i);
       --
     end if;
    --
    return TRUE;

    EXCEPTION
    when RECORD_LOCKED then
           O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                 LP_table,
                                                 I_item,
                                                 NULL);
            RETURN FALSE;
        when OTHERS then
            O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  'UPDATE_SNAPSHOT_SQL.UPDATE_SNAPSHOT_COST',
                                                   to_char(SQLCODE));
         RETURN FALSE;
    END UPDATE_SNAPSHOT_COST;
-------------------------------------------------------------------
END;

/
