CREATE OR REPLACE PACKAGE RMSSUB_INVADJUST AUTHID CURRENT_USER AS

/* Function and Procedure Bodies */
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code       IN OUT  VARCHAR2,
                  O_error_message     IN OUT  VARCHAR2,
                  I_message           IN      RIB_OBJECT,
                  I_message_type      IN      VARCHAR2);
----------------------------------------------------------------------------
-- Procedure Name: CONSUME_INVADJ
-- Purpose: This public procedure is to be called from the dequeue process 
--          (notify_invadjust_message), the error retry process (ribapi_aq_reprocess_sql)
--          and from RFM's INVADJUST consume function (fm_invadj_consume_sql.consume). 
----------------------------------------------------------------------------
PROCEDURE CONSUME_INVADJ(O_status_code       IN OUT  VARCHAR2,
                         O_error_message     IN OUT  VARCHAR2,
                         I_message           IN      RIB_OBJECT,
                         I_message_type      IN      VARCHAR2,
                         I_check_l10n_ind    IN      VARCHAR2);
----------------------------------------------------------------------------
-- Function Name: CONSUME_INVADJ
-- Purpose: This public function is the RMS base function called through the
--          L10N_SQL layer. 
----------------------------------------------------------------------------
FUNCTION CONSUME_INVADJ (O_error_message     IN OUT  VARCHAR2,
                         IO_L10N_RIB_REC     IN OUT  L10N_OBJ)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL = TRUE $then
   -----------------------------------------------------------------------------
   PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                           IO_error_message   IN OUT   VARCHAR2,
                           I_cause            IN       VARCHAR2,
                           I_program          IN       VARCHAR2);
   -----------------------------------------------------------------------------
   FUNCTION PROCESS_INVADJ (O_error_message   IN OUT   VARCHAR2,
                            I_message         IN       "RIB_InvAdjustDesc_REC")
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------
   FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   VARCHAR2,
                                O_orderable_TBL   OUT      "RIB_InvAdjustDtl_TBL",
                                I_sellable_TBL    IN       "RIB_InvAdjustDtl_TBL")
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------
   FUNCTION CHECK_ITEMS(O_error_message   IN OUT   VARCHAR2,
                        O_sellable_TBL    OUT      "RIB_InvAdjustDtl_TBL",
                        O_detail_TBL      OUT      "RIB_InvAdjustDtl_TBL",
                        I_rib_detail_TBL  IN       "RIB_InvAdjustDtl_TBL")
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------
   FUNCTION CONSUME_INVADJ_CORE(O_error_message   IN OUT   VARCHAR2,
                                I_message         IN       RIB_OBJECT,
                                I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------
$end
--------------------------------------------------------------------------------
END RMSSUB_INVADJUST;
/
