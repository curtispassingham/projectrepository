CREATE OR REPLACE PACKAGE BODY CORESVC_INVAVAIL AS
--------------------------------------------------------------------------------
---                          PRIVATE FUNCTION                                ---
--------------------------------------------------------------------------------
--Function Name: VALIDATE_INVAVAIL
--Purpose:       This function will validate the input query request for inventory
--               available web service. The function will bulk validate the input
--               record and for every error, it would append the error to the
--               output error collection and return with false.
--Called by:     GET_INV_DETAIL
--------------------------------------------------------------------------------
FUNCTION VALIDATE_INVAVAIL(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error_tbl     OUT SVCPROV_UTILITY.ERROR_TBL,
                           I_itemloc_req   IN  ITEMLOC_CHANNEL_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
---                          PRIVATE FUNCTION                                ---
--------------------------------------------------------------------------------
--Function Name: GET_CUSTAVAILABLE_INV
--Purpose:       This function will fetch the inventory position details for the
--               input request record.
--Called by:     GET_INV_DETAIL
--------------------------------------------------------------------------------
FUNCTION GET_CUSTAVAILABLE_INV(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_business_object OUT "RIB_InvAvailColDesc_REC",
                               I_itemloc_req   IN  ITEMLOC_CHANNEL_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
---                          PUBLIC FUNCTION                                 ---
--------------------------------------------------------------------------------
FUNCTION GET_INV_DETAIL(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_business_object OUT "RIB_InvAvailColDesc_REC",
                        O_error_tbl       OUT SVCPROV_UTILITY.ERROR_TBL,
                        I_business_object IN  "RIB_InvAvailCriVo_REC")
RETURN BOOLEAN IS

   L_program          VARCHAR2(50)           := 'CORESVC_INVAVAIL.GET_INV_DETAIL';
   L_index            NUMBER                 := 1;
   L_itemloc_req      ITEMLOC_CHANNEL_TBL    := ITEMLOC_CHANNEL_TBL();

BEGIN

   -- Check if input business object have items and locations informations for which inventory details have to be fetched.
   if I_business_object.Items is NULL or I_business_object.Items.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_business_object.Items',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_business_object.Invlocation_TBL is NULL or I_business_object.Invlocation_TBL.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_business_object.Invlocation_TBL',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Flatten the input collections of items and collection of location into collection of item/locations
   for i in I_business_object.Items.FIRST..I_business_object.Items.LAST LOOP
      for j IN I_business_object.Invlocation_TBL.FIRST..I_business_object.Invlocation_TBL.LAST LOOP
         L_itemloc_req.EXTEND;
         L_itemloc_req(L_index) := ITEMLOC_CHANNEL_REC(I_business_object.items(i),
                                                       I_business_object.InvLocation_TBL(j).location,
                                                       I_business_object.InvLocation_TBL(j).loc_type,
                                                       I_business_object.InvLocation_TBL(j).channel_id);
         L_index := L_index + 1;
      end LOOP;
   end LOOP;

   -- Remove any duplicates from the flatten item/location detail collection
   select SET(L_itemloc_req) into L_itemloc_req from dual;

   -- Validate the input data. Populate O_error_tbl with all validation errors.
   if VALIDATE_INVAVAIL(O_error_message,
                        O_error_tbl,
                        L_itemloc_req) = FALSE then
      return FALSE;
   end if;

   -- Fetch the inventory details into the output business object.
   if GET_CUSTAVAILABLE_INV(O_error_message,
                            O_business_object,
                            L_itemloc_req) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_INV_DETAIL;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_INVAVAIL(O_error_message      OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error_tbl          OUT   SVCPROV_UTILITY.ERROR_TBL,
                           I_itemloc_req     IN       ITEMLOC_CHANNEL_TBL)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'CORESVC_INVAVAIL.VALIDATE_INVAVAIL';

   -- Bulk collect the records which have validation errors
   -- 1. Input item should be approved transaction level sellable item.
   -- 2. The item should not be of type
   --     a) Catchweight item
   --     b) Transformable item
   --     c) Concession or Consignment item
   --     d) Deposit crate/container.
   -- 3. The item should be not be a non inventory item.
   -- 4. The input location should be a valid stockholding store or a valid warehouse.
   -- 5. The loc_type should be 'S' or 'W'.
   -- 6. The channel_id should be present for physical warehouse and it should be a valid channel id and there should
   --    be a virtual warehouse with the physical WH/ channel id combination existing.
   -- 7. For store or virtual wh, the channel id is not mandatory. If channel id is present, it should be a valid channel
   --    and if the store/vwh has channel defined in store/wh table, the input store/vwh-channel id combination should match.

   cursor C_VALIDATE is
      select ilc.item                                                         item,
             im.item                                                          valid_item,
             ilc.loc,
             ilc.loc_type,
             ilc.channel_id                                                   input_channel_id,
             ch.channel_id                                                    valid_channel_id,
             im.status,
             im.sellable_ind,
             im.item_level,
             im.tran_level,
             im.inventory_ind,
             im.item_xform_ind,
             im.deposit_item_type,
             im.catch_weight_ind,
             d.purchase_type,
             s.store                                                          valid_store,
             s.channel_id                                                     store_channel_id,
             s.stockholding_ind                                               store_stockholding_ind,
             w.wh                                                             valid_warehouse,
             w.physical_wh                                                    physical_wh,
             w.channel_id                                                     wh_channel_id,
             w.stockholding_ind                                               wh_stockholding_ind,
             (select 'Y'
                from wh w2
               where w2.physical_wh = ilc.loc
                 and ilc.loc_type = 'W'
                 and w2.stockholding_ind = 'Y'
                 and w2.channel_id = ilc.channel_id
                 and rownum = 1)                                              valid_wh_ind
        from TABLE(CAST(I_itemloc_req as ITEMLOC_CHANNEL_TBL)) ilc,
             item_master im,
             deps d,
             store s,
             channels ch,
             wh w
       where ilc.item = im.item(+)
         and ilc.channel_id = ch.channel_id(+)
         and ilc.loc = s.store(+)
         and ilc.loc = w.wh(+)
         and im.dept = d.dept(+)
         and (im.item is NULL
              or im.status <> 'A'
              or im.sellable_ind <> 'Y'
              or im.item_level <> im.tran_level
              or im.inventory_ind <> 'Y'
              or im.item_xform_ind <> 'N'
              or im.catch_weight_ind <> 'N'
              or NVL(im.deposit_item_type,'E') in ('A','Z')
              or d.purchase_type <> 0
              or NVL(ilc.loc_type,'X') not in ('S','W')
              or (ilc.channel_id is not NULL and ch.channel_id is NULL)
              or (NVL(ilc.loc_type,'X') = 'S' and not exists (select 'X'
                                                                from store st
                                                               where st.store = ilc.loc
                                                                 and st.stockholding_ind = 'Y'
                                                                 and (   st.channel_id is NULL
                                                                      or ilc.channel_id is NULL
                                                                      or NVL(st.channel_id,-9999) = NVL(ilc.channel_id,-9999))))
              or (NVL(ilc.loc_type,'X') = 'W' and not exists (select 'X'
                                                                from wh wh
                                                               where wh.physical_wh = ilc.loc
                                                                 and wh.stockholding_ind = 'Y'
                                                                 and wh.channel_id = ilc.channel_id
                                                                 and wh.channel_id is not NULL
                                                                 and ilc.channel_id is not NULL
                                                                 UNION
                                                              select 'X'
                                                                from wh wh
                                                               where wh.wh = ilc.loc
                                                                 and wh.stockholding_ind = 'Y'
                                                                 and wh.channel_id = NVL(ilc.channel_id,wh.channel_id))));

   TYPE INVALID_TBL IS TABLE OF C_VALIDATE%ROWTYPE INDEX BY BINARY_INTEGER;
   L_inv_invalid_tbl INVALID_TBL;

BEGIN

   -- Initialize the output error collection.
   O_error_tbl := SVCPROV_UTILITY.ERROR_TBL();

   open  C_VALIDATE;
   fetch C_VALIDATE BULK COLLECT INTO L_inv_invalid_tbl;
   close C_VALIDATE;

   -- if the cursor returns no value, then the data has passed validation with no errors.
   if (L_inv_invalid_tbl is NULL or L_inv_invalid_tbl.COUNT = 0) then
      return TRUE;
   end if;

   for i in L_inv_invalid_tbl.FIRST..L_inv_invalid_tbl.LAST LOOP
      O_error_tbl.EXTEND;

      -- Input item exists
      if (L_inv_invalid_tbl(i).valid_item is NULL) then
         O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_ITEM_NOT_EXIST',
                                                                      L_inv_invalid_tbl(i).item,
                                                                      NULL,
                                                                      NULL)||';';
      else
         --Item is not an approved, sellable and transaction level item
         if L_inv_invalid_tbl(i).status <> 'A'
            or L_inv_invalid_tbl(i).sellable_ind <> 'Y'
            or L_inv_invalid_tbl(i).item_level <> L_inv_invalid_tbl(i).tran_level
            or L_inv_invalid_tbl(i).inventory_ind <> 'Y' then

            O_error_tbl(i) := O_error_tbl(i) ||SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_INVALID_ITEM',
                                                                        L_inv_invalid_tbl(i).item,
                                                                        NULL,
                                                                        NULL)||';';
         end if;

         --Item is a tranformable item
         if L_inv_invalid_tbl(i).item_xform_ind = 'Y' then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_TRANSFORM_ITEM',
                                                                         L_inv_invalid_tbl(i).item,
                                                                         NULL,
                                                                         NULL)||';';
         end if;

         --Item is a catchweight_item
         if L_inv_invalid_tbl(i).catch_weight_ind = 'Y' then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_CATCHWEIGHT_ITEM',
                                                                         L_inv_invalid_tbl(i).item,
                                                                         NULL,
                                                                         NULL)||';';
         end if;

         --Item is a deposit item
         if NVL(L_inv_invalid_tbl(i).deposit_item_type,'E') in ('A','Z') then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_DEPOSIT_ITEM',
                                                                         L_inv_invalid_tbl(i).item,
                                                                         NULL,
                                                                         NULL)||';';
         end if;

         --Item is a concession or consignment item
         if(L_inv_invalid_tbl(i).purchase_type <> 0) then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_CONCESSION_ITEM',
                                                                         L_inv_invalid_tbl(i).item,
                                                                         NULL,
                                                                         NULL)||';';
         end if;
      end if;  -- Item is not null

      --Channel_id is invalid
      if L_inv_invalid_tbl(i).input_channel_id is not NULL and L_inv_invalid_tbl(i).valid_channel_id is NULL then
         O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_INVALID_CHANNEL',
                                                                      TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                      TO_CHAR(L_inv_invalid_tbl(i).input_channel_id),
                                                                      NULL)||';';
      end if;

      --Loc_type is invalid
      if L_inv_invalid_tbl(i).loc_type not in ('S','W') then
         O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_INVALID_LOC_TYPE',
                                                                      L_inv_invalid_tbl(i).loc_type,
                                                                      NULL,
                                                                      NULL)||';';
      end if;

      -- Store specific validation
      if L_inv_invalid_tbl(i).loc_type = 'S' then
         --Store location is invalid
         if L_inv_invalid_tbl(i).valid_store is NULL then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_STORE_NOT_EXIST',
                                                                         TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                         NULL,
                                                                         NULL)||';';
         end if;

         -- Store should be a stock holding store
         if L_inv_invalid_tbl(i).valid_store is not NULL
            and L_inv_invalid_tbl(i).store_stockholding_ind <> 'Y' then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_STORE_TYPE',
                                                                         TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                         NULL,
                                                                         NULL)||';';
         end if;

         --Store and channel id combination should exists
         if L_inv_invalid_tbl(i).valid_store is not NULL
            and L_inv_invalid_tbl(i).input_channel_id is not NULL
            and L_inv_invalid_tbl(i).valid_channel_id is not NULL
            and NVL(L_inv_invalid_tbl(i).input_channel_id,-1) <> NVL(L_inv_invalid_tbl(i).store_channel_id,-1)then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_STORE_CHANNEL',
                                                                         TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                         L_inv_invalid_tbl(i).input_channel_id,
                                                                         NULL)||';';
         end if;
      end if; -- Location type is store

      -- Warehouse specific validation
      if L_inv_invalid_tbl(i).loc_type = 'W' then
         --Warehouse location is invalid
         if L_inv_invalid_tbl(i).valid_warehouse is NULL then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_WH_NOT_EXIST',
                                                                         TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                         NULL,
                                                                         NULL)||';';
         end if;
         if (L_inv_invalid_tbl(i).physical_wh = L_inv_invalid_tbl(i).valid_warehouse) then --physical wh
            -- Channel id should be present for physical warehouse
            if L_inv_invalid_tbl(i).input_channel_id is NULL then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_NO_CHANNEL',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            NULL,
                                                                            NULL)||';';
            end if;

            -- No stockholding virtual warehouse exists for the physical warehouse/channel combination
            if L_inv_invalid_tbl(i).valid_warehouse is NOT NULL
               and L_inv_invalid_tbl(i).input_channel_id is NOT NULL
               and L_inv_invalid_tbl(i).valid_channel_id is NOT NULL
               and NVL(L_inv_invalid_tbl(i).valid_wh_ind,'N') <> 'Y' then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_INVALID_WH',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            L_inv_invalid_tbl(i).input_channel_id,
                                                                            NULL)||';';
            end if;

         else -- virtual wh
            if L_inv_invalid_tbl(i).wh_stockholding_ind = 'N' then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INV_STOCKHOLDING_WH',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            NULL,
                                                                            NULL)||';';
            end if;
            if L_inv_invalid_tbl(i).wh_channel_id is NOT NULL
               and L_inv_invalid_tbl(i).input_channel_id is NOT NULL
               and L_inv_invalid_tbl(i).wh_channel_id <> L_inv_invalid_tbl(i).input_channel_id then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_VWH_CHANNEL',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            L_inv_invalid_tbl(i).input_channel_id,
                                                                            NULL)||';';
            end if;
         end if;
      end if; -- Location type is Warehouse

   end LOOP;

   return FALSE;

EXCEPTION
   when OTHERS then
      if C_VALIDATE%ISOPEN then
         close C_VALIDATE;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END VALIDATE_INVAVAIL;
-------------------------------------------------------------------------------------
FUNCTION GET_CUSTAVAILABLE_INV(O_error_message        OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_business_object      OUT   "RIB_InvAvailColDesc_REC",
                               I_itemloc_req       IN       ITEMLOC_CHANNEL_TBL)
RETURN BOOLEAN IS

   L_program                VARCHAR2(50)              := 'CORESVC_INVAVAIL.GET_CUSTAVAILABLE_INV';
   L_RIB_InvAvailDesc_TBL   "RIB_InvAvailDesc_TBL"    := "RIB_InvAvailDesc_TBL"();

   --Bulk collect the inventory position details
   cursor C_AVAILABLE is
      select "RIB_InvAvailDesc_REC"(0,
                                    ilc.item,
                                    ilc.loc,
                                    ilc.loc_type,
                                    ilc.channel_id,
                                    ilc.available_qty,
                                    im.standard_uom,
                                    NULL,                -- AVAILABLE_DATE
                                    ilc.pack_calculate_ind)
        from ITEM_MASTER im,
             --fetching available inventory details on warehouse
             (select inv_wh.item,
                     inv_wh.loc,
                     inv_wh.loc_type,
                     inv_wh.channel_id,
                     SUM(case when NVL(inv_wh.customer_order_loc_ind,'N') = 'Y' then
                                   NVL(GREATEST(ils.stock_on_hand - (GREATEST(ils.tsf_reserved_qty,0)+
                                                                     GREATEST(ils.customer_resv,0)+
                                                                     GREATEST(ils.rtv_qty,0)+
                                                                     GREATEST(ils.non_sellable_qty,0)),0),0)
                              else 0
                         end) available_qty,
                      case when(im.pack_ind = 'Y') then 'N' else NULL end pack_calculate_ind
                from item_loc_soh ils,
                     item_master im,
                     ((select ilc.item item,
                              w.physical_wh loc,
                              w.wh v_wh,
                              ilc.loc_type loc_type,
                              ilc.channel_id channel_id,
                              w.customer_order_loc_ind
                         from TABLE(CAST(I_itemloc_req AS ITEMLOC_CHANNEL_TBL)) ilc,
                              wh w
                        where ilc.loc = w.physical_wh
                          and w.channel_id = ilc.channel_id
                          and ilc.loc_type = 'W'
                          and w.stockholding_ind = 'Y')
                      UNION
                      (select ilc.item item,
                              w.physical_wh loc,
                              w.wh v_wh,
                              ilc.loc_type loc_type,
                              ilc.channel_id channel_id,
                              w.customer_order_loc_ind
                         from TABLE(CAST(I_itemloc_req AS ITEMLOC_CHANNEL_TBL)) ilc,
                              wh w
                        where ilc.loc = w.wh
                          and w.channel_id = NVL(ilc.channel_id,w.channel_id)
                          and ilc.loc_type = 'W'
                          and w.stockholding_ind = 'Y')) inv_wh
               where inv_wh.item = im.item
                 and inv_wh.item = ils.item(+)
                 and inv_wh.v_wh = ils.loc(+)
                 and inv_wh.loc_type = ils.loc_type(+)
              group by inv_wh.item,
                       inv_wh.loc,
                       inv_wh.loc_type,
                       inv_wh.channel_id,
                       im.pack_ind
              union all
               --fetching available inventory on store for a regular item
              select ilc.item,
                     ilc.loc,
                     ilc.loc_type,
                     ilc.channel_id,
                     case when NVL(s.customer_order_loc_ind,'N') = 'Y' then
                               NVL(GREATEST(ils.stock_on_hand - (GREATEST(ils.tsf_reserved_qty,0)+
                                                                 GREATEST(ils.customer_resv,0)+
                                                                 GREATEST(ils.rtv_qty,0)+
                                                                 GREATEST(ils.non_sellable_qty,0)),0),0)
                          else 0
                     end available_qty,
                     NULL pack_calculate_ind
                from TABLE(CAST(I_itemloc_req as ITEMLOC_CHANNEL_TBL)) ilc,
                     item_loc_soh ils,
                     item_master im,
                     store s
               where ilc.item = im.item
                 and ilc.loc = s.store
                 and ilc.loc_type = 'S'
                 and ilc.item = ils.item(+)
                 and ilc.loc = ils.loc(+)
                 and ilc.loc_type = ils.loc_type(+)
                 and im.pack_ind = 'N'
                 and s.stockholding_ind = 'Y'
              union all
              --Calculating available inventory for pack item at store based on its components available quantity
              select inv_pk.pack_item item,
                     inv_pk.loc,
                     inv_pk.loc_type,
                     inv_pk.channel_id,
                     MIN(case when NVL(s.customer_order_loc_ind,'N') = 'Y' then
                                   NVL(GREATEST(FLOOR((ils.stock_on_hand - (GREATEST(ils.tsf_reserved_qty,0)+
                                                                            GREATEST(ils.customer_resv,0)+
                                                                            GREATEST(ils.rtv_qty,0)+
                                                                            GREATEST(ils.non_sellable_qty,0))) / inv_pk.qty),0),0)
                              else 0
                         end) available_qty,
                     'Y' pack_calculate_ind
                from item_loc_soh ils,
                     store s,
                     (select /*+ result_cache */
                             ilc.item pack_item,
                             ilc.loc,
                             ilc.loc_type,
                             ilc.channel_id,
                             pib.item component,
                             SUM(pib.pack_item_qty) qty
                       from TABLE(CAST(I_itemloc_req as ITEMLOC_CHANNEL_TBL)) ilc,
                            packitem_breakout pib,
                            item_master im,
                            item_master im_comp
                      where ilc.item = pib.pack_no
                        and ilc.item = im.item
                        and pib.item = im_comp.item
                        and im_comp.inventory_ind = 'Y'
                        and im.pack_ind = 'Y'
                        and ilc.loc_type = 'S'
                   group by ilc.item,
                            ilc.loc,
                            ilc.loc_type,
                            ilc.channel_id,
                            pib.item)inv_pk
               where inv_pk.loc = s.store
                 and inv_pk.component = ils.item(+)
                 and inv_pk.loc  = ils.loc(+)
                 and inv_pk.loc_type = ils.loc_type(+)
              group by inv_pk.pack_item,
                       inv_pk.loc,
                       inv_pk.loc_type,
                       inv_pk.channel_id) ilc
       where ilc.item = im.item;

BEGIN
   --This function will fetch the available quantity details for
   --the input request record and will collect the result into
   --output business object.

   open  C_AVAILABLE;
   fetch C_AVAILABLE BULK COLLECT INTO L_RIB_InvAvailDesc_TBL;
   close C_AVAILABLE;

   O_business_object := "RIB_InvAvailColDesc_REC"(0,
                                                  L_RIB_InvAvailDesc_TBL,
                                                  L_RIB_InvAvailDesc_TBL.COUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_AVAILABLE%ISOPEN then
         close C_AVAILABLE;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_CUSTAVAILABLE_INV;
--------------------------------------------------------------------------------
END CORESVC_INVAVAIL;
/