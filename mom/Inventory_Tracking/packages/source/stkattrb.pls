CREATE OR REPLACE PACKAGE BODY STKCNT_ATTRIB_SQL AS  
---------------------------------------------------------------------
   FUNCTION GET_DESC(I_stake         IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                     O_desc          IN OUT  STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                     O_error_message IN OUT  VARCHAR2)
   RETURN BOOLEAN IS
      cursor C_stake is
         select cycle_count_desc
           from stake_head
          where cycle_count = I_stake;

   BEGIN
      open C_stake;
      fetch C_stake into O_desc;
      if C_stake%NOTFOUND then
         close C_stake;
         O_desc := NULL;
         O_error_message := sql_lib.create_msg('INV_STOCK_COUNT',NULL,NULL,NULL);
         RETURN FALSE;
      else
         close C_stake;
         RETURN TRUE;
      end if;
   EXCEPTION
      when OTHERS then
         O_error_message := sql_lib.create_msg
         ('PACKAGE_ERROR',SQLERRM,'STKCNT_ATTRIB_SQL.GET_DESC',To_Char(SQLCODE));
         RETURN FALSE;
   END GET_DESC;

---------------------------------------------------------------------
   FUNCTION GET_TYPE(I_stake         IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                     O_type          IN OUT  STAKE_HEAD.STOCKTAKE_TYPE%TYPE,
                     O_error_message IN OUT  VARCHAR2)
   RETURN BOOLEAN IS
      cursor C_type is
         select stocktake_type
           from stake_head
          where cycle_count = I_stake;

   BEGIN
      open C_type;
      fetch C_type into O_type;
      if C_type%NOTFOUND then
         close C_type;
         O_type := NULL;
         O_error_message := sql_lib.create_msg('INV_STOCK_COUNT',NULL,NULL,NULL);
         RETURN FALSE;
      else
         close C_type;
         RETURN TRUE;
      end if;
   EXCEPTION
      when OTHERS then
         O_error_message := sql_lib.create_msg
         ('PACKAGE_ERROR',SQLERRM,'STKCNT_ATTRIB_SQL.GET_TYPE',To_Char(SQLCODE));
         RETURN FALSE;
   END GET_TYPE;

---------------------------------------------------------------------
   FUNCTION PROD_LOC_PROCESSED(I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                               I_location       IN      STAKE_PROD_LOC.LOCATION%TYPE, 
                               O_proc           IN OUT  STAKE_PROD_LOC.PROCESSED%TYPE,
                               O_error_message  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS
      cursor C_proc IS
         select processed
           from stake_prod_loc
          where cycle_count = I_stake
          and processed = DECODE (I_location,location,processed,NULL,'N')
          order by processed;
      L_proc  NUMBER(8);
   BEGIN  
      open C_proc;
      fetch C_proc into O_proc;
      if C_proc%NOTFOUND then
         close C_proc;
         O_error_message := sql_lib.create_msg('INV_STOCK_COUNT',NULL,NULL,NULL);
         RETURN FALSE;
      end if;
      close C_proc;
      RETURN TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := sql_lib.create_msg
         ('PACKAGE_ERROR',SQLERRM,'STKCNT_ATTRIB_SQL.PROD_LOC_PROCESSED',to_char(SQLCODE));
         RETURN FALSE;
   END PROD_LOC_PROCESSED;

---------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message     IN OUT VARCHAR2,
                  O_stake_desc        IN OUT STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                  O_loc_type          IN OUT STAKE_HEAD.LOC_TYPE%TYPE,
                  O_stocktake_date    IN OUT STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                  O_stocktake_type    IN OUT STAKE_HEAD.STOCKTAKE_TYPE%TYPE,
                  O_product_level_ind IN OUT STAKE_HEAD.PRODUCT_LEVEL_IND%TYPE,
                  O_delete_ind        IN OUT STAKE_HEAD.DELETE_IND%TYPE,
                  I_stake             IN     STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN IS
   cursor C_STAKE is
      select cycle_count_desc,
             loc_type,
             stocktake_date,
             stocktake_type,
             product_level_ind,
             delete_ind
        from stake_head
       where cycle_count = I_stake;

BEGIN
   open C_STAKE;
   fetch C_STAKE into O_stake_desc,
                      O_loc_type,
                      O_stocktake_date,
                      O_stocktake_type,
                      O_product_level_ind,
                      O_delete_ind;
   if C_STAKE%NOTFOUND then
      close C_STAKE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_STOCK_COUNT',
                                            NULL,
                                            NULL,
                                            NULL);
         RETURN FALSE;
      else
         close C_STAKE;
      RETURN TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'STKCNT_ATTRIB_SQL.GET_INFO',
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;

END GET_INFO;
---------------------------------------------------------------------
FUNCTION STOCK_COUNT_PROCESSED(O_error_message           IN OUT VARCHAR2,
                               O_stock_count_processed   IN OUT BOOLEAN,
                               O_cycle_count             IN OUT STAKE_HEAD.CYCLE_COUNT%TYPE,
                               I_transaction_date        IN     DATE,
                               I_item                    IN     ITEM_MASTER.ITEM%TYPE,
                               I_loc_type                IN     STAKE_SKU_LOC.LOC_TYPE%TYPE,
                               I_location                IN     STAKE_SKU_LOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_stocktake_date     DATE;

   cursor C_LATEST_COUNT is
      select sh.stocktake_date,
             sh.cycle_count
        from stake_head sh,
             stake_sku_loc skl
       where skl.item        = I_item
         and skl.loc_type    = I_loc_type
         and skl.location    = I_location
         and skl.processed   = 'P'
         and skl.cycle_count = sh.cycle_count
       order by sh.stocktake_date desc;
   
BEGIN
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_LATEST_COUNT',
                    'stake_head,
                    stake_sku_loc',
                    'Item: '|| I_item);
   open C_LATEST_COUNT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LATEST_COUNT',
                    'stake_head,
                    stake_sku_loc',
                    'Item: '|| I_item);
   fetch C_LATEST_COUNT into L_stocktake_date,
                             O_cycle_count;        
   ---
   if (I_transaction_date > L_stocktake_date) or (C_LATEST_COUNT%NOTFOUND) then
      O_stock_count_processed := FALSE;
   else
      O_stock_count_processed := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LATEST_COUNT',
                    'stake_head, stake_sku_loc',
                    'Item: '|| I_item);
   close C_LATEST_COUNT;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'STKCNT_ATTRIB_SQL.STOCK_COUNT_PROCESSED',
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;

END STOCK_COUNT_PROCESSED;
-------------------------------------------------------------------------------------------
FUNCTION STOCK_COUNT_PROCESSED(O_error_message           IN OUT  VARCHAR2,
                               O_stock_count_processed   IN OUT  BOOLEAN,
                               O_cycle_count             IN OUT  STAKE_HEAD.CYCLE_COUNT%TYPE,
                               O_snapshot_unit_cost      IN OUT  STAKE_SKU_LOC.SNAPSHOT_UNIT_COST%TYPE,
                               O_snapshot_unit_retail    IN OUT  STAKE_SKU_LOC.SNAPSHOT_UNIT_RETAIL%TYPE,
                               I_transaction_date        IN      DATE,
                               I_item                    IN      ITEM_MASTER.ITEM%TYPE,
                               I_loc_type                IN      STAKE_SKU_LOC.LOC_TYPE%TYPE,
                               I_location                IN      STAKE_SKU_LOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_vdate         DATE := GET_VDATE;

   cursor C_GET_SNAPSHOT is
      select skl.snapshot_unit_cost,
             skl.snapshot_unit_retail
        from stake_sku_loc skl
       where skl.cycle_count = O_cycle_count
         and skl.item        = I_item
         and skl.location    = I_location
         and skl.loc_type    = I_loc_type;

BEGIN

   if I_transaction_date < L_vdate then
      if STKCNT_ATTRIB_SQL.STOCK_COUNT_PROCESSED(O_error_message,
                                                 O_stock_count_processed,
                                                 O_cycle_count,
                                                 I_transaction_date,
                                                 I_item,
                                                 I_loc_type,
                                                 I_location) = FALSE then
         RETURN FALSE;
      end if;
   end if;

   if O_stock_count_processed then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SNAPSHOT',
                       'STAKE_SKU_LOC',
                       'Cycle_count:'||TO_CHAR(O_cycle_count));
      open C_GET_SNAPSHOT;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SNAPSHOT',
                       'STAKE_SKU_LOC',
                       'Cycle_count:'||TO_CHAR(O_cycle_count));
      fetch C_GET_SNAPSHOT into O_snapshot_unit_cost,
                                O_snapshot_unit_retail;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SNAPSHOT',
                       'STAKE_SKU_LOC',
                       'Cycle_count:'||TO_CHAR(O_cycle_count));
      close C_GET_SNAPSHOT;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCNT_ATTRIB_SQL.STOCK_COUNT_PROCESSED',
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END STOCK_COUNT_PROCESSED;
-------------------------------------------------------------------------------------------
END STKCNT_ATTRIB_SQL;     
/
