CREATE OR REPLACE PACKAGE RMSMFM_DLVYSLT AUTHID CURRENT_USER AS

FAMILY CONSTANT  RIB_SETTINGS.FAMILY%TYPE := 'dlvyslt';

SLT_ADD   CONSTANT   VARCHAR2(15)   := 'dlvysltcre';
SLT_UPD   CONSTANT   VARCHAR2(15)   := 'dlvysltmod';
SLT_DEL   CONSTANT   VARCHAR2(15)   := 'dlvysltdel';
--------------------------------------------------------------------------------
-- Function : ADDTOQ
-- Purpose  : Add new entry to DELIVERY_SLOT_MFQUEUE
-- Calls    : None
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                O_status                    OUT   VARCHAR2,
                I_message_type           IN       DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_delivery_slot_id       IN       DELIVERY_SLOT_MFQUEUE.DELIVERY_SLOT_ID%TYPE,
                I_delivery_slot_desc     IN       DELIVERY_SLOT_MFQUEUE.DELIVERY_SLOT_DESC%TYPE,
                I_delivery_sequence      IN       DELIVERY_SLOT_MFQUEUE.DELIVERY_SLOT_SEQUENCE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function : GETNXT
-- Purpose  : Get next entry from DELIVERY_SLOT_MFQUEUE
-- Calls    : PROCESS_QUEUE_DLVY_SLT, HANDLE_ERRORS
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code    IN OUT   VARCHAR2,
                 O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_message_type   IN OUT   DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                 O_message        IN OUT   RIB_OBJECT,
                 O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                 O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                 I_num_threads    IN       NUMBER DEFAULT 1,
                 I_thread_val     IN       NUMBER DEFAULT 1);
--------------------------------------------------------------------------------
-- Function : PUB_RETRY
-- Purpose  : Retry next hospital entry from DELIVERY_SLOT_MFQUEUE
-- Calls    : PROCESS_QUEUE_DLVY_SLT, HANDLE_ERRORS
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code    IN OUT   VARCHAR2,
                    O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_message        IN OUT   RIB_OBJECT,
                    O_message_type   IN OUT   DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE,
                    O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                    O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                    I_ref_object     IN       RIB_OBJECT);
----------------------------------------------------------------------------------
END RMSMFM_DLVYSLT;
/
