CREATE OR REPLACE PACKAGE BODY RMSMFM_RCVUNITADJ AS

TYPE ROWID_TBL    IS TABLE OF ROWID INDEX BY BINARY_INTEGER;

LP_error_status   VARCHAR2(1)   :=  NULL;

--------------------------------------------------------
       /*** Private Program Declarations ***/
--------------------------------------------------------

---
PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT  VARCHAR2,
                        O_error_msg     IN OUT  VARCHAR2,
                        O_message       IN OUT  RIB_OBJECT,
                        O_message_type  IN OUT  VARCHAR2,
                        O_bus_obj_id    IN OUT  RIB_BUSOBJID_TBL,
                        O_routing_info  IN OUT  RIB_ROUTINGINFO_TBL,
                        I_location      IN      RUA_MFQUEUE.LOCATION%TYPE,
                        I_loc_type      IN      RUA_MFQUEUE.LOC_TYPE%TYPE,
                        I_seq_no        IN      RUA_MFQUEUE.SEQ_NO%TYPE);
---
FUNCTION PROCESS_QUEUE_RECORD(O_error_msg      IN OUT         VARCHAR2,
                              O_break_loop     IN OUT         BOOLEAN,
                              O_message        IN OUT nocopy  RIB_OBJECT,
                              O_routing_info   IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id     IN OUT nocopy  RIB_BUSOBJID_TBL,
                              O_message_type   IN OUT         VARCHAR2,
                              I_seq_no         IN             RUA_MFQUEUE.SEQ_NO%TYPE,
                              I_business_obj   IN             RCVUNITADJ_KEY_REC,
                              I_rowid          IN             ROWID)
RETURN BOOLEAN;
---
FUNCTION BUILD_HEADER_OBJECT(O_error_msg                   IN OUT         VARCHAR2,
                             O_rib_rcvunitadjdesc_rec          IN OUT nocopy  "RIB_RcvUnitAdjDesc_REC",
                             O_routing_info                IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                             I_business_obj             IN             RCVUNITADJ_KEY_REC)

  RETURN BOOLEAN;
---
FUNCTION BUILD_SINGLE_DETAIL(O_error_msg                   IN OUT         VARCHAR2,
                             O_message                     IN OUT nocopy  "RIB_RcvUnitAdjDtl_TBL",
                             O_rib_rcvunitadjdtl_rec           IN OUT nocopy  "RIB_RcvUnitAdjDtl_REC",
                             I_business_obj             IN             RCVUNITADJ_KEY_REC)

RETURN BOOLEAN;
---
FUNCTION DELETE_QUEUE_REC(O_error_msg  IN OUT  VARCHAR2,
                          I_rowid      IN       ROWID)
RETURN BOOLEAN;
---
FUNCTION LOCK_THE_BLOCK(O_error_msg     IN OUT  VARCHAR2,
                        O_queue_locked  IN OUT  BOOLEAN,
                        I_business_obj  IN      RCVUNITADJ_KEY_REC)
RETURN BOOLEAN;
---
FUNCTION MAKE_CREATE(O_error_msg        OUT                VARCHAR2,
                     O_message             IN OUT NOCOPY        RIB_OBJECT,
                     O_routing_info  IN OUT NOCOPY      RIB_ROUTINGINFO_TBL,
                     I_business_obj  IN                 RCVUNITADJ_KEY_REC,
                     I_rowid             IN                        ROWID)

RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg             IN OUT         VARCHAR2,
                              O_routing_info              IN OUT NOCOPY  RIB_ROUTINGINFO_TBL,
                              O_message               IN OUT NOCOPY  "RIB_RcvUnitAdjDtl_TBL",
                              I_message_type          IN             RUA_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_business_obj          IN             RCVUNITADJ_KEY_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
           /*** Public Program Bodies ***/
--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg     IN OUT  VARCHAR2,
                I_message_type  IN      VARCHAR2,
                I_business_obj  IN      RCVUNITADJ_KEY_REC)
RETURN BOOLEAN IS
--
   L_max_details        RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE   :=  NULL;
   L_initial_approval   VARCHAR2(1)                                :=  'N';
   L_num_threads        RIB_SETTINGS.NUM_THREADS%TYPE              :=  NULL;
   L_min_time_lag       RIB_SETTINGS.MINUTES_TIME_LAG%TYPE         :=  NULL;
   L_status_code        VARCHAR2(1)                                :=  NULL;

BEGIN
   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_RCVUNITADJ.FAMILY);
                                
   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   insert into rua_mfqueue(seq_no,
                           order_no,
                           asn,
                           location,
                           loc_type,
                           item,
                           carton,
                           adj_qty,
                           message_type,
                           thread_no,
                           family,
                           custom_message_type,
                           pub_status,
                           transaction_number,
                           transaction_time_stamp)
                   values (rua_mfsequence.nextval,
                           I_business_obj.order_no,
                           I_business_obj.asn,
                           I_business_obj.location,
                           I_business_obj.loc_type,
                           I_business_obj.item,
                           I_business_obj.carton,
                           I_business_obj.adj_qty,
                           I_message_type,
                           MOD(I_business_obj.location, L_num_threads) + 1,
                           RMSMFM_RCVUNITADJ.FAMILY,
                           NULL,
                           'U',
                           I_business_obj.location,
                           sysdate);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RCVUNITADJ.ADDTOQ',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;

--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1)
IS
   L_message_type    RUA_MFQUEUE.MESSAGE_TYPE%TYPE      := NULL;
   L_seq_no          RUA_MFQUEUE.SEQ_NO%TYPE            := NULL;
   L_seq_limit       RUA_MFQUEUE.SEQ_NO%TYPE            := 0;
   L_business_obj    RCVUNITADJ_KEY_REC                 := NULL;     
   L_rowid           ROWID                              := NULL;
   L_break_loop      BOOLEAN                            := FALSE;

   L_hosp            VARCHAR2(1)                        := 'N';
   L_queue_locked    BOOLEAN                            := FALSE;

   cursor C_QUEUE is
      select q.seq_no,
                q.order_no,
                q.asn,
                q.location,
                q.loc_type,
                q.item,
                q.carton,
                q.adj_qty,
             q.message_type,
                q.rowid
        from rua_mfqueue q
       where q.seq_no = (select min(q2.seq_no)
                           from rua_mfqueue q2
                          where q2.thread_no  = I_thread_val
                            and q2.pub_status = 'U'
                            and q2.seq_no     > L_seq_limit);
         
   cursor C_HOSP is
      select 'x'
        from rua_mfqueue
       where location = L_business_obj.location
         and pub_status = API_CODES.HOSPITAL;

 BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   LOOP
      O_message := NULL;

      open C_QUEUE;
      fetch C_QUEUE into L_seq_no,
                            L_business_obj.order_no,
                            L_business_obj.asn,
                            L_business_obj.location,
                            L_business_obj.loc_type,
                            L_business_obj.item,
                            L_business_obj.carton,
                            L_business_obj.adj_qty,
                            L_message_type,
                            L_rowid;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_QUEUE;

      if LOCK_THE_BLOCK(O_error_msg,
                        L_queue_locked,
                        L_business_obj) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then

         open  C_HOSP;
         fetch C_HOSP into L_hosp;
         if C_HOSP%FOUND then
            close C_HOSP;
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                              NULL, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;
         close C_HOSP;

         if PROCESS_QUEUE_RECORD(O_error_msg,
                                 L_break_loop,
                                 O_message,
                                 O_routing_info,
                                 O_bus_obj_id,
                                 L_message_type,
                                 L_seq_no,
                                 L_business_obj,
                                 L_rowid) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         if L_break_loop = TRUE then
            O_message_type := L_message_type;
            EXIT;
         end if;

      else
         L_seq_limit := L_seq_no;
      end if;

   END LOOP;

   if O_message is NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id  := NULL;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_business_obj.location,
                    L_business_obj.loc_type,
                    L_seq_no);
END GETNXT;

--------------------------------------------------------------------------------

PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_ref_object      IN           RIB_OBJECT)
IS
   L_break_loop      BOOLEAN                            := FALSE;
   L_message_type    RUA_MFQUEUE.MESSAGE_TYPE%TYPE      := NULL;
   L_seq_no          RUA_MFQUEUE.SEQ_NO%TYPE            := NULL;
   L_business_obj    RCVUNITADJ_KEY_REC                 := NULL;     
   L_rowid           ROWID                              := NULL;
   L_queue_locked    BOOLEAN                            := FALSE;

   cursor C_RETRY_QUEUE is
      select q.order_no,
                q.asn,
                q.location,
                q.loc_type,
                q.item,
                q.carton,
                q.adj_qty,
                q.rowid
        from rua_mfqueue q
       where q.seq_no = L_seq_no;



BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;


   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;

   O_message := NULL;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_business_obj.order_no,
                                    L_business_obj.asn,
                                    L_business_obj.location,
                                    L_business_obj.loc_type,
                                    L_business_obj.item,
                                    L_business_obj.carton,
                           L_business_obj.adj_qty,
                           L_rowid;
   close C_RETRY_QUEUE;

   if L_business_obj.location is NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   if LOCK_THE_BLOCK(O_error_msg,
                     L_queue_locked,
                     L_business_obj) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then

      if PROCESS_QUEUE_RECORD(O_error_msg,
                              L_break_loop,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              L_message_type,
                              L_seq_no,
                              L_business_obj,
                              L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message is NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id  := NULL;
      end if;
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_business_obj.location,
                    L_business_obj.loc_type,
                    L_seq_no);
END PUB_RETRY;

--------------------------------------------------------------------------------
           /*** Private Program Bodies ***/
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT  VARCHAR2,
                        O_error_msg     IN OUT  VARCHAR2,
                        O_message       IN OUT  RIB_OBJECT,
                        O_message_type  IN OUT  VARCHAR2,
                        O_bus_obj_id    IN OUT  RIB_BUSOBJID_TBL,
                        O_routing_info  IN OUT  RIB_ROUTINGINFO_TBL,
                        I_location      IN      RUA_MFQUEUE.LOCATION%TYPE,
                        I_loc_type      IN      RUA_MFQUEUE.LOC_TYPE%TYPE,
                        I_seq_no        IN      RUA_MFQUEUE.SEQ_NO%TYPE)

IS

   L_rib_rcvunitadjdesc_rec      "RIB_RcvUnitAdjDesc_REC";

   L_error_type                  VARCHAR2(5) := NULL;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then
      O_bus_obj_id               := NULL;
      O_routing_info             := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_seq_no,null,null,null,null));
      L_rib_rcvunitadjdesc_rec   := "RIB_RcvUnitAdjDesc_REC"(0,I_location, I_loc_type, NULL);

      O_message := L_rib_rcvunitadjdesc_rec;

      update rua_mfqueue
         set pub_status = LP_error_status
       where seq_no    = I_seq_no;
   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type, O_error_msg);

EXCEPTION
   when OTHERS then
      O_status_code := API_CODES.UNHANDLED_ERROR;
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RCVUNITADJ.HANDLE_ERRORS',
                                        TO_CHAR(SQLCODE));
END HANDLE_ERRORS;

--------------------------------------------------------------------------------

FUNCTION PROCESS_QUEUE_RECORD(O_error_msg      IN OUT         VARCHAR2,
                              O_break_loop     IN OUT         BOOLEAN,
                              O_message        IN OUT nocopy  RIB_OBJECT,
                              O_routing_info   IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id     IN OUT nocopy  RIB_BUSOBJID_TBL,
                              O_message_type   IN OUT         VARCHAR2,
                              I_seq_no         IN             RUA_MFQUEUE.SEQ_NO%TYPE,
                              I_business_obj   IN             RCVUNITADJ_KEY_REC,
                              I_rowid          IN             ROWID)
RETURN BOOLEAN IS

   L_rib_rcvunitadjdesc_rec           "RIB_RcvUnitAdjDesc_REC"                 := NULL;
   L_rib_routing_rec                  RIB_ROUTINGINFO_REC                    := NULL;

   L_rib_rcvunitadjdtl_rec      "RIB_RcvUnitAdjDtl_REC"                        := NULL;
   L_rib_rcvunitadjdtl_tbl      "RIB_RcvUnitAdjDtl_TBL"                        := NULL;

BEGIN

   O_break_loop := TRUE;

   if MAKE_CREATE(O_error_msg,        
                  O_message,             
                  O_routing_info,
                  I_business_obj,  
                  I_rowid) = FALSE then
   return FALSE;
   end if;

   if DELETE_QUEUE_REC(O_error_msg,
                       I_rowid) = FALSE then
      return FALSE;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RCVUNITADJ.PROCESS_QUEUE_RECORD',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_QUEUE_RECORD;

-------------------------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT(O_error_msg               IN OUT         VARCHAR2,
                             O_rib_rcvunitadjdesc_rec  IN OUT nocopy  "RIB_RcvUnitAdjDesc_REC",
                             O_routing_info            IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                             I_business_obj            IN             RCVUNITADJ_KEY_REC)

  RETURN BOOLEAN IS

  L_rib_routing_rec      RIB_ROUTINGINFO_REC  :=  NULL;

   PROGRAM_ERROR          EXCEPTION;


BEGIN

    O_rib_rcvunitadjdesc_rec := "RIB_RcvUnitAdjDesc_REC"(0,
                                                       I_business_obj.location,
                                                       I_business_obj.loc_type,
                                                       NULL);

   -- create routing info --
   O_routing_info := RIB_ROUTINGINFO_TBL();

      L_rib_routing_rec := RIB_ROUTINGINFO_REC('Location ', I_business_obj.location, 'loc_type', I_business_obj.loc_type, NULL, NULL);

     O_routing_info.EXTEND;
     O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RCVUNITADJ.BUILD_HEADER_OBJECT',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_HEADER_OBJECT;

--------------------------------------------------------------------------------

FUNCTION BUILD_SINGLE_DETAIL(O_error_msg                IN OUT         VARCHAR2,
                             O_message                  IN OUT nocopy  "RIB_RcvUnitAdjDtl_TBL",
                             O_rib_rcvunitadjdtl_rec    IN OUT nocopy  "RIB_RcvUnitAdjDtl_REC",
                             I_business_obj             IN             RCVUNITADJ_KEY_REC)

RETURN BOOLEAN IS

L_from_disposition         varchar2(4)      := NULL;
L_to_disposition         varchar2(4)        := NULL;

BEGIN

  if I_business_obj.adj_qty <0 then
      L_from_disposition := 'ATS';
      L_to_disposition   := NULL;
   else
      L_from_disposition := NULL;
      L_to_disposition   := 'ATS';
   end if;

   O_rib_rcvunitadjdtl_rec  := "RIB_RcvUnitAdjDtl_REC"(0,
                                                     I_business_obj.order_no,
                                                     I_business_obj.asn,
                                                     I_business_obj.item,
                                                     I_business_obj.carton,
                                                     ABS(I_business_obj.adj_qty),
                                                     L_from_disposition,
                                                     L_to_disposition);

   O_message.EXTEND;
   O_message(O_message.COUNT) := O_rib_rcvunitadjdtl_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RCVUNITADJ.BUILD_DETAIL_OBJECT',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_SINGLE_DETAIL;

--------------------------------------------------------------------------------

FUNCTION DELETE_QUEUE_REC(O_error_msg  IN OUT  VARCHAR2,
                          I_rowid      IN       ROWID)
RETURN BOOLEAN IS

BEGIN

   delete from rua_mfqueue
    where rowid = I_rowid;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RCVUNITADJ.DELETE_QUEUE_REC',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_QUEUE_REC;

--------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK(O_error_msg     IN OUT  VARCHAR2,
                        O_queue_locked  IN OUT  BOOLEAN,
                        I_business_obj  IN      RCVUNITADJ_KEY_REC)
RETURN BOOLEAN IS

   L_table         VARCHAR2(30)                         :=  'RUA_MFQUEUE';
   L_key1          VARCHAR2(100)                        :=  I_business_obj.location;
   L_key2          VARCHAR2(100)                        :=  NULL;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from rua_mfqueue
       where location = I_business_obj.location
         for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_queue_locked := TRUE;
      return TRUE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RCVUNITADJ.LOCK_THE_BLOCK',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_THE_BLOCK;

--------------------------------------------------------------------------------
FUNCTION MAKE_CREATE(O_error_msg        OUT             VARCHAR2,
                     O_message       IN OUT NOCOPY      RIB_OBJECT,
                     O_routing_info  IN OUT NOCOPY      RIB_ROUTINGINFO_TBL,
                     I_business_obj  IN                 RCVUNITADJ_KEY_REC,
                     I_rowid         IN                 ROWID)

RETURN BOOLEAN IS

   L_rib_rcvunitadjdesc_rec "RIB_RcvUnitAdjDesc_REC" := NULL;
   L_rib_rcvunitadjdtl_tbl  "RIB_RcvUnitAdjDtl_TBL"  := NULL;

   L_rib_routing_rec        RIB_ROUTINGINFO_REC    := NULL;
   L_delete_rowid_ind       VARCHAR2(1)            := 'Y';

   --L_rib_partnerref_rec   "RIB_PartnerRef_REC"     := NULL;

   PROGRAM_ERROR            EXCEPTION;

BEGIN

   if BUILD_HEADER_OBJECT(O_error_msg,
                          L_rib_rcvunitadjdesc_rec,
                          O_routing_info,
                          I_business_obj) = FALSE then
      return FALSE;
   end if;


   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           O_routing_info,
                           L_rib_rcvunitadjdesc_rec.rcvunitadjdtl_tbl,
                           NULL,
                           I_business_obj) = FALSE then
      return FALSE;
   end if;

   O_message := L_rib_rcvunitadjdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.MAKE_CREATE',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END MAKE_CREATE;

--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg             IN OUT         VARCHAR2,
                              O_routing_info          IN OUT NOCOPY  RIB_ROUTINGINFO_TBL,
                              O_message               IN OUT NOCOPY  "RIB_RcvUnitAdjDtl_TBL",
                              I_message_type          IN             RUA_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_business_obj          IN             RCVUNITADJ_KEY_REC)
RETURN BOOLEAN IS

   L_rib_rcvunitadjdtl_rec       "RIB_RcvUnitAdjDtl_REC"                       := NULL;
   L_rib_rcvunitadjdtl_tbl       "RIB_RcvUnitAdjDtl_TBL"                       := NULL;
   L_rib_routing_rec             RIB_ROUTINGINFO_REC                         := NULL;
      
BEGIN

   if O_message is NULL then
       L_rib_rcvunitadjdtl_tbl:= "RIB_RcvUnitAdjDtl_TBL"();
   else
       L_rib_rcvunitadjdtl_tbl:= O_message;
   end if;

   if BUILD_SINGLE_DETAIL(O_error_msg,
                          L_rib_rcvunitadjdtl_tbl,
                          L_rib_rcvunitadjdtl_rec,
                          I_business_obj) = FALSE then
            return FALSE;
         end if;

   if L_rib_rcvunitadjdtl_tbl.COUNT > 0 then
      O_message := L_rib_rcvunitadjdtl_tbl;
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('Location', I_business_obj.location, 'Loc_type', I_business_obj.loc_type, null, null);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;
   else
      O_message := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RCVUNITADJ.BUILD_DETAIL_OBJECTS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_OBJECTS;
--------------------------------------------------------------------------------------------
END RMSMFM_RCVUNITADJ;
/
