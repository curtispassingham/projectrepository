CREATE OR REPLACE PACKAGE BODY REF_RILU_UPDATES_SQL AS
--------------------------------------------------------------------------------
FUNCTION UPDATE_NULL_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)  := 'REF_RILU_UPDATES_SQL.UPDATE_NULL_LOC';

BEGIN

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'REPL_ITEM_LOC_UPDATES',
                    NULL);

   insert into repl_item_loc_updates(item,
                                     supplier,
                                     origin_country_id,
                                     location,
                                     loc_type,
                                     change_type)
                             (select rilu.item,
                                     rilu.supplier,
                                     rilu.origin_country_id,
                                     ril.location,
                                     ril.loc_type,
                                     rilu.change_type
                                from repl_item_loc ril,
                                     repl_item_loc_updates rilu
                               where ril.item = rilu.item
                                 and NVL(rilu.location,-1) = -1
                               group by rilu.item,
                                        rilu.supplier,
                                        rilu.origin_country_id,
                                        ril.location,
                                        ril.loc_type,
                                        rilu.change_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_NULL_LOC;
--------------------------------------------------------------------------------
FUNCTION DEL_NULL_LOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'REF_RILU_UPDATES_SQL.DEL_NULL_LOCS';

BEGIN

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'REPL_ITEM_LOC_UPDATES',
                    'NULL LOCATIONS');

   delete from repl_item_loc_updates
    where NVL(location,-1) = -1;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END DEL_NULL_LOCS;
--------------------------------------------------------------------------------
END REF_RILU_UPDATES_SQL;
/