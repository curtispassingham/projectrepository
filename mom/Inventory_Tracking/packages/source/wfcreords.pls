CREATE OR REPLACE PACKAGE WF_CREATE_ORDER_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------
---Function Name: PROCESS_F_ORDER
---Purpose:       This public function will handle the Franchise order creation through
---               replenishment requests, store orders, Item requests, AIP generated
---               transfers and allocations for stockholding Franchise stores. This
---               function will validate the input order record, and process the record
---               based on the input action type.This function will in turn call the private
---               functions to perform the necessary validations and insert/update/delete
---               the records in wf_order_head and wf_order_detail based on the input action type.
---Called By:     This function will be called by the wrapper packages WF_TRANSFER_SQL, WF_PO_SQL
---               and WF_ALLOC_SQL wheneven a tranfer/allocation/PO is created/maintained.
----------------------------------------------------------------------------------
FUNCTION PROCESS_F_ORDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_wf_order_no          IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                         I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC,
                         I_action_type          IN       VARCHAR2,
                         I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
END WF_CREATE_ORDER_SQL;
/