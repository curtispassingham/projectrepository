---------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, Retek Inc.  All rights reserved.
-- $Workfile$
-- $Revision: 1.8 $
-- $Modtime$
---------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY WF_RETURN_SQL AS

LP_user    VARCHAR2(50)        := GET_USER;
LP_vdate   PERIOD.VDATE%TYPE   := GET_VDATE;
---------------------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
---------------------------------------------------------------------------------------------------------------------
FUNCTION NEXT_RMA_NUMBER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.NEXT_RMA_NUMBER';
   L_rma_no    WF_RETURN_HEAD.RMA_NO%TYPE;
   L_exist     VARCHAR2(1) DEFAULT 'N';
   L_table     VARCHAR2(30);

   cursor C_GET_RMA_NO is
      select wf_rma_no_seq.nextval
        from dual;

   cursor C_RMA_NO_EXIST is
      select 'Y'
        from wf_return_head
       where rma_no = L_rma_no;

BEGIN
   LOOP
      SQL_LIB.SET_MARK('OPEN', 'C_GET_RMA_NO', NULL, NULL);
      open C_GET_RMA_NO;

      SQL_LIB.SET_MARK('FETCH', 'C_GET_RMA_NO', NULL, NULL);
      fetch C_GET_RMA_NO into L_rma_no;

      SQL_LIB.SET_MARK('CLOSE', 'C_GET_RMA_NO', NULL, NULL);
      close C_GET_RMA_NO;

      L_exist := 'N';
      L_table := 'WF_RETURN_HEAD';

      SQL_LIB.SET_MARK('OPEN', 'C_RMA_NO_EXIST', L_table, L_rma_no);
      open C_RMA_NO_EXIST;

      SQL_LIB.SET_MARK('FETCH', 'C_RMA_NO_EXIST', L_table, L_rma_no);
      fetch C_RMA_NO_EXIST into L_exist;

      SQL_LIB.SET_MARK('CLOSE', 'C_RMA_NO_EXIST', L_table, L_rma_no);
      close C_RMA_NO_EXIST;

      -- If generated RMA NO doesn't already exist in WF_RETURN_HEAD table, exit from the function to use the number.
      if NVL(L_exist, 'N') != 'Y' then
         EXIT;
      end if;

   END LOOP;

   O_rma_no := L_rma_no;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_RMA_NO%ISOPEN then
         close C_GET_RMA_NO;
      end if;

      if C_RMA_NO_EXIST%ISOPEN then
         close C_RMA_NO_EXIST;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_RMA_NUMBER;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RMA_NUMBER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'WF_RETURN_SQL.VALIDATE_RMA_NUMBER';
   L_exist        VARCHAR2(1) DEFAULT 'N';

   cursor C_RMA_EXIST is
      select 'Y'
        from WF_RETURN_HEAD
       where rma_no = I_rma_no;

BEGIN
   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_RMA_EXIST;
   fetch C_RMA_EXIST into L_exist;
   close C_RMA_EXIST;

   if L_exist = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_RMA_EXIST%ISOPEN then
         close C_RMA_EXIST;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_RMA_NUMBER;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       I_wf_order_id     IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                       I_item            IN       WF_ORDER_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.VALIDATE_ITEM';
   L_exist     VARCHAR2(1) DEFAULT 'N';

   -- Cusror returns 'Y' if input item or the pack, the input item is a component of, exists on the given order.
   cursor C_ITEM_EXIST is
      select 'Y'
        from wf_order_detail
       where wf_order_no = I_wf_order_id
         and item        = I_item
         and rownum      = 1

      UNION ALL

      select 'Y'
        from wf_order_detail wod,
             v_packsku_qty   vpq
       where wod.wf_order_no = I_wf_order_id
         and vpq.item        = I_item
         and wod.item        = vpq.pack_no
         and rownum          = 1;

BEGIN

   if I_wf_order_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_id',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ITEM_EXIST;
   fetch C_ITEM_EXIST into L_exist;
   close C_ITEM_EXIST;

   if L_exist = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ITEM_EXIST%ISOPEN then
         close C_ITEM_EXIST;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ITEM;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_total_return_amt         IN OUT   WF_RETURN_HEAD.TOTAL_RETURN_AMT%TYPE,
                    O_total_restock_amt        IN OUT   WF_RETURN_HEAD.TOTAL_RESTOCK_AMT%TYPE,
                    O_total_final_return_amt   IN OUT   WF_RETURN_HEAD.TOTAL_NET_RETURN_AMT%TYPE,
                    I_rma_no                   IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'WF_RETURN_SQL.GET_TOTALS';

   -- Calculates totals to populate in WF_RETURN_HEAD table. Return unit cost is exclusive of tax.
   -- So, restocking fee percent will be applied directly on return unit cost.
   cursor C_CALC_TOTALS is
      select SUM(wrd.return_unit_cost * wrd.returned_qty),
             SUM((wrd.return_unit_cost -
                        DECODE(wrd.restock_type,
                               'S', wrd.unit_restock_fee,
                               'V', wrd.return_unit_cost * (wrd.unit_restock_fee/100),
                               0)) *
                        wrd.returned_qty),
             SUM(DECODE(wrd.restock_type,
                              'S', wrd.unit_restock_fee,
                              'V', wrd.return_unit_cost * (wrd.unit_restock_fee/100),
                              0) *
                       wrd.returned_qty)
        from wf_return_detail wrd
       where wrd.rma_no = I_rma_no;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CALC_TOTALS;
   fetch C_CALC_TOTALS into O_total_return_amt,
                            O_total_final_return_amt,
                            O_total_restock_amt;
   close C_CALC_TOTALS;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CALC_TOTALS%ISOPEN then
         close C_CALC_TOTALS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_TOTALS;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE FETCH_WF_RETURN_DATA(O_wf_return_tbl   IN OUT   WF_RETURN_SQL.WF_RETURN_DTL_TBL,
                               I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE) IS

   L_program                        VARCHAR2(50) := 'WF_RETURN_SQL.FETCH_WF_RETURN_DATA';
   L_final_ret_tot_cost             WF_RETURN_DETAIL.NET_RETURN_UNIT_COST%TYPE;
   L_ret_unit_cost_incl_vat         WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE     := 0;
   L_final_ret_unit_cost_incl_vat   WF_RETURN_DETAIL.NET_RETURN_UNIT_COST%TYPE := 0;
   L_final_ret_tot_cost_incl_vat    WF_RETURN_DETAIL.NET_RETURN_UNIT_COST%TYPE := 0;
   L_primary_ref_item               ITEM_MASTER.ITEM%TYPE;
   L_exists                         BOOLEAN;
   L_error_message                  RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_tax_amount                     WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE     := 0;
   L_tax_rate                       VAT_ITEM.VAT_RATE%TYPE                     := 0;
   L_tax_code                       VAT_CODES.VAT_CODE%TYPE;
   PROGRAM_ERROR                    EXCEPTION;

   cursor C_WF_RETURN_DTL is
      select wrdt.item item,
             im.item_desc item_desc,
             wrh.customer_loc customer_loc,
             wrh.return_loc_type,
             wrh.return_loc_id,
             s.vat_region vat_region,
             wrdt.wf_order_no wf_order_id,
             SUM(wodt.requested_qty) order_qty,
             MAX(NVL(wodt.fixed_cost,NVL(wodt.customer_cost,0))) customer_cost,
             wohd.cust_ord_ref_no cust_ord_ref_no,
             wrdt.return_unit_cost return_unit_cost,
             wrdt.returned_qty returned_qty,
             wrdt.net_return_unit_cost final_return_unit_cost,
             NULL vat,
             NULL vat_rate,
             wrdt.return_reason return_reason,
             wrdt.cancel_reason cancel_reason,
             wrdt.unit_restock_fee unit_restock_fee,
             wrdt.restock_type restock_type,
             rowidtochar(wrdt.rowid) row_id,
             NULL return_code,
             NULL error_message
        from item_master      im,
             wf_order_head    wohd,
             wf_order_detail  wodt,
             wf_return_detail wrdt,
             wf_return_head   wrh,
             store            s
       where wrh.rma_no       = I_rma_no
         and wrdt.rma_no      = wrh.rma_no
         and wrdt.wf_order_no = wodt.wf_order_no(+)
         and wodt.wf_order_no = wohd.wf_order_no(+)
         and wrdt.item        = wodt.item(+)
         and wrdt.item        = im.item
         and s.store          = wrh.customer_loc
         and NVL(im.deposit_item_type,'0') != 'A'
       group by wrdt.item,
                im.item_desc,
                wrh.customer_loc,
                wrh.return_loc_type,
                wrh.return_loc_id,
                s.vat_region,
                wrdt.wf_order_no,
                wohd.cust_ord_ref_no,
                wrdt.return_unit_cost,
                wrdt.returned_qty,
                wrdt.net_return_unit_cost,
                wrdt.return_reason,
                wrdt.cancel_reason,
                wrdt.unit_restock_fee,
                wrdt.restock_type,
                rowidtochar(wrdt.rowid);

   TYPE WF_RETURN_DTL_DATA is TABLE of C_WF_RETURN_DTL%ROWTYPE INDEX BY BINARY_INTEGER;

   L_wf_return_dtl_data   WF_RETURN_DTL_DATA;

BEGIN

   if I_rma_no is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   open C_WF_RETURN_DTL;
   fetch C_WF_RETURN_DTL BULK COLLECT into L_wf_return_dtl_data;
   close C_WF_RETURN_DTL;

   if L_wf_return_dtl_data is NOT NULL and L_wf_return_dtl_data.COUNT > 0 then
      FOR i in 1..L_wf_return_dtl_data.COUNT LOOP
         L_final_ret_tot_cost := (L_wf_return_dtl_data(i).final_return_unit_cost * L_wf_return_dtl_data(i).returned_qty);

         if ITEM_ATTRIB_SQL.GET_PRIMARY_REF_ITEM(L_error_message,
                                                 L_primary_ref_item,
                                                 L_exists,
                                                 L_wf_return_dtl_data(i).item) = FALSE then
            raise PROGRAM_ERROR;
         else
            if L_exists = FALSE then
               O_wf_return_tbl(i).ref_item := NULL;
            else
               O_wf_return_tbl(i).ref_item := L_primary_ref_item;
            end if;
         end if;
         -- Get tax rate for the item at franchise location
         if GET_TAX_INFO(L_error_message,
                         L_tax_rate,
                         L_tax_code,
                         L_wf_return_dtl_data(i).item,
                         L_wf_return_dtl_data(i).customer_loc,
                         'S',
                         'R',
                         L_wf_return_dtl_data(i).return_loc_id,
                         L_wf_return_dtl_data(i).return_loc_type,
                         I_rma_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         O_wf_return_tbl(i).item                    := L_wf_return_dtl_data(i).item;
         O_wf_return_tbl(i).item_desc               := L_wf_return_dtl_data(i).item_desc;
         O_wf_return_tbl(i).wf_order_id             := L_wf_return_dtl_data(i).wf_order_id;
         O_wf_return_tbl(i).order_qty               := L_wf_return_dtl_data(i).order_qty;
         O_wf_return_tbl(i).customer_cost           := L_wf_return_dtl_data(i).customer_cost;
         O_wf_return_tbl(i).cust_ord_ref_no         := L_wf_return_dtl_data(i).cust_ord_ref_no;
         O_wf_return_tbl(i).return_unit_cost        := L_wf_return_dtl_data(i).return_unit_cost;
         O_wf_return_tbl(i).returned_qty            := L_wf_return_dtl_data(i).returned_qty;
         O_wf_return_tbl(i).final_return_unit_cost  := L_wf_return_dtl_data(i).final_return_unit_cost;
         O_wf_return_tbl(i).return_reason           := L_wf_return_dtl_data(i).return_reason;
         O_wf_return_tbl(i).cancel_reason           := L_wf_return_dtl_data(i).cancel_reason;
         O_wf_return_tbl(i).restock_type            := L_wf_return_dtl_data(i).restock_type;
         O_wf_return_tbl(i).unit_restock_fee        := L_wf_return_dtl_data(i).unit_restock_fee;
         O_wf_return_tbl(i).final_return_total_cost := L_final_ret_tot_cost;
         O_wf_return_tbl(i).row_id                  := L_wf_return_dtl_data(i).row_id;

         L_ret_unit_cost_incl_vat       := L_wf_return_dtl_data(i).return_unit_cost * (1+ nvl(L_tax_rate,0)/100);
         L_tax_amount                   := L_ret_unit_cost_incl_vat - L_wf_return_dtl_data(i).return_unit_cost;
         L_final_ret_unit_cost_incl_vat := L_wf_return_dtl_data(i).final_return_unit_cost * (1+ nvl(L_tax_rate,0)/100);
         L_final_ret_tot_cost_incl_vat  := L_final_ret_unit_cost_incl_vat * L_wf_return_dtl_data(i).returned_qty;

         O_wf_return_tbl(i).vat                        := L_tax_amount;
         O_wf_return_tbl(i).vat_rate                   := L_tax_rate;
         O_wf_return_tbl(i).return_unit_cost_incl_vat  := L_ret_unit_cost_incl_vat;
         O_wf_return_tbl(i).final_ret_unit_cost_invat  := L_final_ret_unit_cost_incl_vat;
         O_wf_return_tbl(i).final_ret_total_cost_invat := L_final_ret_tot_cost_incl_vat;
      END LOOP;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
      O_wf_return_tbl(1).error_message := L_error_message;
      O_wf_return_tbl(1).return_code   := 'FALSE';

   when OTHERS then
      O_wf_return_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                             SQLERRM,
                                                             L_program,
                                                             to_char(SQLCODE));
      O_wf_return_tbl(1).return_code := 'FALSE';

END FETCH_WF_RETURN_DATA;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE(O_wf_return_tbl   IN OUT   WF_RETURN_SQL.WF_RETURN_DTL_TBL) IS

   L_program       VARCHAR2(50) := 'WF_RETURN_SQL.LOCK_PROCEDURE';
   L_exists        VARCHAR2(1);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   L_table         VARCHAR2(30) := 'WF_RETURN_DETAIL';

BEGIN
   FOR i in 1..O_wf_return_tbl.COUNT LOOP
      select 'x'
        into L_exists
        from wf_return_detail
       where rowid = chartorowid(O_wf_return_tbl(i).row_id)
         for update nowait;
   END LOOP;

EXCEPTION
   when RECORD_LOCKED then
      O_wf_return_tbl(1).error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                                             L_table,
                                                             NULL,
                                                             L_program);
      O_wf_return_tbl(1).return_code := 'FALSE';

   when OTHERS then
      O_wf_return_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                             SQLERRM,
                                                             L_program,
                                                             to_char(SQLCODE));
      O_wf_return_tbl(1).return_code := 'FALSE';
END LOCK_PROCEDURE;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE INS_UPD_WF_RETURN_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_return_code     IN OUT   VARCHAR2,
                                 I_wf_return_tbl   IN       WF_RETURN_SQL.WF_RETURN_DTL_TBL,
                                 I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE) IS

   L_program         VARCHAR2(50)  := 'WF_RETURN_SQL.INS_UPD_WF_RETURN_DATA';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE :=  NULL;
   L_item            ITEM_MASTER.ITEM%TYPE;
   L_exists          VARCHAR2(1) DEFAULT 'N';
   L_tsf_no          TSFHEAD.TSF_NO%TYPE;
   PROGRAM_ERROR     EXCEPTION;
   L_return_status   WF_RETURN_HEAD.STATUS%TYPE;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,-54);
   L_table           VARCHAR2(30);

   cursor C_RET_DTL_EXISTS is
      select 'Y'
        from wf_return_detail
       where rma_no = I_rma_no
         and item   = L_item;

   cursor C_GET_LINKED_TSF is
      select tsf_no
        from tsfhead
       where rma_no = I_rma_no;

   cursor C_LOCK_WF_RETURN_HEAD is
      select 'X'
        from wf_return_head
       where rma_no = I_rma_no
         for update nowait;

   cursor C_LOCK_WF_RETURN_DETAIL is
      select 'X'
        from wf_return_detail
       where rma_no = I_rma_no
         and item   = L_item
         for update nowait;

BEGIN

   if I_rma_no is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   elsif I_wf_return_tbl is NULL or I_wf_return_tbl.COUNT = 0 then
      L_error_message := SQL_LIB.CREATE_MSG('NULL_RETURN_BLOCK',
                                            NULL,
                                            NULL,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   open C_GET_LINKED_TSF;
   fetch C_GET_LINKED_TSF into L_tsf_no;
   close C_GET_LINKED_TSF;

   -- Loop through the input collection and insert/update items in return.
   FOR i in 1..I_wf_return_tbl.COUNT LOOP
      L_item     := I_wf_return_tbl(i).item;
      open C_RET_DTL_EXISTS;
      fetch C_RET_DTL_EXISTS into L_exists;
      close C_RET_DTL_EXISTS;

      if L_exists = 'Y' then
         -- If item already exists in the return, update it.
         L_table := 'WF_RETURN_DETAIL';
         open C_LOCK_WF_RETURN_DETAIL;
         close C_LOCK_WF_RETURN_DETAIL;

         update wf_return_detail
            set returned_qty         = I_wf_return_tbl(i).returned_qty,
                return_reason        = I_wf_return_tbl(i).return_reason,
                cancel_reason        = I_wf_return_tbl(i).cancel_reason,
                return_unit_cost     = I_wf_return_tbl(i).return_unit_cost,
                restock_type         = I_wf_return_tbl(i).restock_type,
                unit_restock_fee     = I_wf_return_tbl(i).unit_restock_fee,
                net_return_unit_cost = I_wf_return_tbl(i).final_return_unit_cost,
                last_update_datetime = sysdate,
                last_update_id       = LP_user
          where rma_no = I_rma_no
            and item   = L_item;

      else
         -- If item doesn't exist in return, insert the item.
         insert into wf_return_detail(rma_no,
                                      item,
                                      wf_order_no,
                                      returned_qty,
                                      return_reason,
                                      cancel_reason,
                                      return_unit_cost,
                                      restock_type,
                                      unit_restock_fee,
                                      net_return_unit_cost,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
         values                      (I_rma_no,
                                      I_wf_return_tbl(i).item,
                                      I_wf_return_tbl(i).wf_order_id,
                                      I_wf_return_tbl(i).returned_qty,
                                      I_wf_return_tbl(i).return_reason,
                                      I_wf_return_tbl(i).cancel_reason,
                                      I_wf_return_tbl(i).return_unit_cost,
                                      I_wf_return_tbl(i).restock_type,
                                      I_wf_return_tbl(i).unit_restock_fee,
                                      I_wf_return_tbl(i).final_return_unit_cost,
                                      sysdate,
                                      LP_user,
                                      sysdate,
                                      LP_user);

         -- If a linked transfer exists for the return, insert item in the transfer as well.
         if L_tsf_no is NOT NULL then
            if WF_TRANSFER_SQL.WF_CREATE_TSF_DETAIL(L_error_message,
                                                    L_tsf_no,
                                                    L_item,
                                                    I_wf_return_tbl(i).returned_qty) = FALSE then
               raise PROGRAM_ERROR;
            end if;
         end if;
      end if;
   END LOOP;

   -- After insert/update/delete on WF_RETURN_DETAIL table, this function is called to keep quantities of deposit contents and
   -- deposit containers in sync.
   if SYNC_DEPOSIT_ITEMS(L_error_message,
                         I_rma_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   L_table := 'WF_RETURN_HEAD';
   open C_LOCK_WF_RETURN_HEAD;
   close C_LOCK_WF_RETURN_HEAD;

   -- If all items in the return are cancelled completely, set the status of the return to 'Cancelled'.
   update wf_return_head
      set status               = 'C',
          cancel_reason        = I_wf_return_tbl(1).cancel_reason,
          last_update_datetime = sysdate,
          last_update_id       = LP_user
    where rma_no = I_rma_no
      and status = 'A'
      and (select SUM(wrd.returned_qty)
             from wf_return_detail wrd
            where wrd.rma_no = I_rma_no) = 0
  returning status into L_return_status;

   if L_tsf_no is NOT NULL then
      if L_return_status = 'C' then
         -- If return is cancelled, cancel the transfer linked to the franchise return.
         if WF_TRANSFER_SQL.CANCEL_FRANCHISE_TSF(L_error_message,
                                                 NULL,
                                                 I_rma_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      else
         -- This function matches return quantity in WF_RETURN_DETAIL with (tsf_qty - cancelled_qty) in TSFDETAIL and
         -- updates TSFDETAIL when mismatch occurs.
         if WF_TRANSFER_SQL.MODIFY_FRANCHISE_TSF(L_error_message,
                                                 NULL,
                                                 I_rma_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;
   end if;

   O_error_message := NULL;
   O_return_code   := 'TRUE';

EXCEPTION
   when RECORD_LOCKED then
      if L_table = 'WF_RETURN_HEAD' then
         O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                               L_table,
                                               I_rma_no,
                                               L_program);
      else
         O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                               L_table,
                                               I_rma_no,
                                               L_item);
      end if;
      O_return_code   := 'FALSE';

   when PROGRAM_ERROR then
      O_error_message := L_error_message;
      O_return_code   := 'FALSE';

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      O_return_code := 'FALSE';

END INS_UPD_WF_RETURN_DATA;
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                     I_item            IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program                 VARCHAR2(50)  := 'WF_RETURN_SQL.DELETE_ITEM';
   L_tsf_no                  TSFHEAD.TSF_NO%TYPE;
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked,-54);
   L_table                   VARCHAR2(30);

   cursor C_LOCK_WF_RETURN_DETAIL is
      select 'X'
        from wf_return_detail
       where rma_no                 = I_rma_no
         and item                   = I_item
         for update nowait;

   cursor C_GET_LINKED_TSF is
      select tsf_no
        from tsfhead
       where rma_no = I_rma_no;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_table := 'WF_RETURN_DETAIL';
   open C_LOCK_WF_RETURN_DETAIL;
   close C_LOCK_WF_RETURN_DETAIL;

   -- Delete item from the return.
   delete from wf_return_detail
    where rma_no                 = I_rma_no
      and item                   = I_item;

   -- After insert/update/delete on WF_RETURN_DETAIL table, this function is called to keep quantities of deposit contents and
   -- deposit containers in sync.
   if SYNC_DEPOSIT_ITEMS(O_error_message,
                         I_rma_no) =  FALSE then
      return FALSE;
   end if;

   open C_GET_LINKED_TSF;
   fetch C_GET_LINKED_TSF into L_tsf_no;
   close C_GET_LINKED_TSF;

   -- Delete the item from linked transfer.
   if L_tsf_no is NOT NULL then
      if WF_TRANSFER_SQL.DELETE_FRANCHISE_TSF_DETAIL(O_error_message,
                                                     NULL,
                                                     I_rma_no,
                                                     I_item,
                                                     NULL,
                                                     NULL) =  FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then

      if C_LOCK_WF_RETURN_DETAIL%ISOPEN then
         close C_LOCK_WF_RETURN_DETAIL;
      end if;

      if C_GET_LINKED_TSF%ISOPEN then
         close C_GET_LINKED_TSF;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_rma_no,
                                            I_item);
      return FALSE;

   when OTHERS then

      if C_LOCK_WF_RETURN_DETAIL%ISOPEN then
         close C_LOCK_WF_RETURN_DETAIL;
      end if;

      if C_GET_LINKED_TSF%ISOPEN then
         close C_GET_LINKED_TSF;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ITEM;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_RETURN_COST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_return_cost       IN OUT   WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE,
                              I_return_currency   IN       WF_RETURN_HEAD.CURRENCY_CODE%TYPE,
                              I_return_from_loc   IN       WF_RETURN_HEAD.CUSTOMER_LOC%TYPE,
                              I_wf_order_no       IN       WF_RETURN_DETAIL.WF_ORDER_NO%TYPE,
                              I_item              IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'WF_RETURN_SQL.GET_ITEM_RETURN_COST';
   L_order_cost           WF_ORDER_DETAIL.CUSTOMER_COST%TYPE;
   L_order_currency       WF_ORDER_HEAD.CURRENCY_CODE%TYPE;
   L_pack_component_ind   VARCHAR2(1);
   L_pack_no              ITEM_MASTER.ITEM%TYPE;
   L_pct_in_pack          NUMBER;

   -- Franchise orders can have pack items but franchise returns created manually or through EDI can't contain pack items.
   -- Such returns can have pack components. If a pack component is added to a franchise return, order cost of the corresponding
   -- pack item is fetched and cost of the component item will be derived from it.

   cursor C_ORDER_COST is
   -- This query is to fetch order cost of item if input is normal item
      select NVL(wod.fixed_cost,NVL(wod.customer_cost,0)) order_cost,
             woh.currency_code  order_currency,
             'N'                pack_component_ind,
             NULL               item_pack_no
        from wf_order_detail wod,
             wf_order_head   woh
       where woh.wf_order_no = I_wf_order_no
         and woh.wf_order_no = wod.wf_order_no
         and wod.item        = I_item

      UNION ALL
   -- This query is to fetch order cost of pack item if input is component item
      select NVL(wod.fixed_cost,NVL(wod.customer_cost,0)) order_cost,
             woh.currency_code  order_currency,
             'Y'                pack_component_ind,
             vpq.pack_no        item_pack_no
        from wf_order_detail wod,
             wf_order_head   woh,
             v_packsku_qty   vpq
       where woh.wf_order_no = I_wf_order_no
         and woh.wf_order_no = wod.wf_order_no
         and vpq.item        = I_item
         and wod.item        = vpq.pack_no
         and not exists (select 1
                           from wf_order_detail wod1
                          where wod1.wf_order_no = I_wf_order_no
                            and wod1.item        = I_item);

BEGIN

   if I_return_currency is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_return_currency',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ORDER_COST;
   -- If the input item exists as a normal item and also a pack component on the franchise order, this
   -- fetch statement gets the order cost of the normal item.
   fetch C_ORDER_COST into L_order_cost,
                           L_order_currency,
                           L_pack_component_ind,
                           L_pack_no;
   close C_ORDER_COST;

   -- If input item is a pack component, find the component percent in the pack and calculate the component cost.
   if L_pack_component_ind = 'Y' then
      if I_return_from_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_return_from_loc',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;

      if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                       L_pct_in_pack,
                                       L_pack_no,
                                       I_item,
                                       I_return_from_loc) = FALSE then
         return FALSE;
      end if;

      L_order_cost := L_order_cost * L_pct_in_pack;

   end if;

   -- Convert the item cost from order currency to return currency
   if CURRENCY_SQL.CONVERT(O_error_message,
                           L_order_cost,
                           L_order_currency,
                           I_return_currency,
                           O_return_cost,                  -- Converted cost is populated in this variable.
                           'C',
                           NULL,
                           NULL,
                           NULL,
                           NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ORDER_COST%ISOPEN then
         close C_ORDER_COST;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ITEM_RETURN_COST;
---------------------------------------------------------------------------------------------------------------------
FUNCTION APPROVE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.APPROVE';

   L_return_method        WF_RETURN_HEAD.RETURN_METHOD%TYPE;
   L_tsf_no               TSFHEAD.TSF_NO%TYPE;
   L_tsf_type             TSFHEAD.TSF_TYPE%TYPE;
   L_from_loc_type        TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_from_loc             TSFHEAD.FROM_LOC%TYPE;
   L_to_loc_type          TSFHEAD.TO_LOC_TYPE%TYPE;
   L_to_loc               TSFHEAD.TO_LOC%TYPE;
   L_rma_no               WF_RETURN_HEAD.RMA_NO%TYPE;
   L_stockholding_ind     STORE.STOCKHOLDING_IND%TYPE;
   L_pack_no              ITEM_MASTER.ITEM%TYPE DEFAULT NULL;
   L_pct_in_pack          NUMBER DEFAULT NULL;
   L_item_record          ITEM_MASTER%ROWTYPE;
   L_wf_tsf_item_tbl      wf_tsf_item_tbl;
   L_rma_exists           BOOLEAN;
   L_qty_available        WF_RETURN_DETAIL.RETURNED_QTY%TYPE;
   L_unit_cost            TRAN_DATA.TOTAL_COST%TYPE;
   L_unit_retail          TRAN_DATA.TOTAL_RETAIL%TYPE;
   L_item                 WF_RETURN_DETAIL.ITEM%TYPE;
   L_loc                  WF_RETURN_HEAD.RETURN_LOC_ID%TYPE;

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked,-54);
   L_table                VARCHAR2(30);

   -- Fetches header level details of the return
   cursor C_RETURN_HEAD is
      select return_method,
             customer_loc,
             return_loc_type,
             return_loc_id
        from wf_return_head
       where rma_no = I_rma_no;

   -- Populates a collection with items and their return quantities.
   -- This collection is used to create a new transfer, if transfer doesn't exist for the return.
   cursor C_WF_ITEM_RETURN is
      select im.item,
             wf.returned_qty
        from wf_return_detail wf,
             item_master      im
       where wf.rma_no         = I_rma_no
         and wf.item           = im.item
         and NVL(im.deposit_item_type, '-999') <> 'A';

   cursor C_GET_LINKED_TSF is
      select tsf_no
        from tsfhead
       where rma_no = I_rma_no;

   cursor C_ITEM_IN_PACK is
      select v.item,
             v.qty,
             im.dept,
             im.class,
             im.subclass,
             im.sellable_ind,
             im.item_xform_ind,
             im.inventory_ind
        from item_master   im,
             v_packsku_qty v
       where v.pack_no = L_pack_no
         and im.item   = v.item;

   cursor C_LOCK_WF_RETURN_HEAD is
      select 'X'
        from wf_return_head
       where rma_no = I_rma_no
         for update nowait;

   cursor C_CHECK_IL_STATUS is
      select wrd.item,
             il.loc
        from wf_return_head wrh,
             wf_return_detail wrd,
             item_loc il,
             item_master im
       where wrh.rma_no = I_rma_no
         and wrh.return_type in ('M', 'E')
         and wrh.rma_no = wrd.rma_no
         and il.item = wrd.item
         and im.item = il.item
         and NVL(im.deposit_item_type,'-999') != 'A'
         and ((il.loc = wrh.customer_loc and
               il.loc_type = 'S')
          or (il.loc = wrh.return_loc_id and
              il.loc_type = wrh.return_loc_type))
         and il.status = 'D'
         and rownum = 1;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_RETURN_HEAD;
   fetch C_RETURN_HEAD into L_return_method,
                            L_from_loc,
                            L_to_loc_type,
                            L_to_loc;
   close C_RETURN_HEAD;

   -- NULL value for return method implies that RMA NO is invalid.
   if L_return_method is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('FR_INV_RMA',
                                            I_rma_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_WF_ITEM_RETURN;
   fetch C_WF_ITEM_RETURN BULK COLLECT into L_wf_tsf_item_tbl;
   close C_WF_ITEM_RETURN;

   L_rma_no        := I_rma_no;
   L_from_loc_type := 'S';
   L_tsf_type      := 'FR';
   --Check item loc status
   open C_CHECK_IL_STATUS;
   fetch C_CHECK_IL_STATUS into L_item,
                                L_loc;
   if C_CHECK_IL_STATUS%FOUND then
      close C_CHECK_IL_STATUS;
      O_error_message := SQL_LIB.CREATE_MSG('WF_ITEM_NOT_ACTIVE',
                                            L_item,
                                            L_loc,
                                            NULL);
      return FALSE;
   end if;
   close C_CHECK_IL_STATUS;

   -- Transfer will not be created for 'destroy on site' type returns.
   if L_return_method = 'R' then

      if STORE_ATTRIB_SQL.GET_STOCKHOLDING_IND(O_error_message,
                                               L_stockholding_ind,
                                               L_from_loc) = FALSE then
         return FALSE;
      end if;

      if L_stockholding_ind = 'Y' then
         -- Check for quantity availability of items in the return at frachise location only if the location is stock holding.
         FOR i in 1..L_wf_tsf_item_tbl.COUNT LOOP
            if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(O_error_message,
                                                          L_qty_available,
                                                          L_wf_tsf_item_tbl(i).item,
                                                          L_from_loc,
                                                          L_from_loc_type) = FALSE then
               return FALSE;
            end if;

            if L_qty_available < L_wf_tsf_item_tbl(i).returned_qty then
               O_error_message := SQL_LIB.CREATE_MSG('FR_QTY_UNAVAIL',
                                                     L_wf_tsf_item_tbl(i).item,
                                                     L_rma_no,
                                                     NULL);
               return FALSE;
            end if;
         END LOOP;
      end if;

      open C_GET_LINKED_TSF;
      fetch C_GET_LINKED_TSF into L_tsf_no;
      close C_GET_LINKED_TSF;

      if L_tsf_no is NULL then
         -- If there is no transfer for the return already, create a transfer in 'Approved' state.
         if WF_TRANSFER_SQL.WF_CREATE_TSF_HEAD(O_error_message,
                                               L_tsf_no,
                                               L_from_loc_type,
                                               L_from_loc,
                                               L_to_loc_type,
                                               L_to_loc,
                                               L_tsf_type,
                                               NULL,
                                               NULL,
                                               L_rma_no,
                                               'A') = FALSE then
            return FALSE;
         end if;

         if L_wf_tsf_item_tbl is NOT NULL and L_wf_tsf_item_tbl.COUNT > 0 then

            FOR i in 1..L_wf_tsf_item_tbl.COUNT LOOP
               if WF_TRANSFER_SQL.WF_CREATE_TSF_DETAIL(O_error_message,
                                                       L_tsf_no,
                                                       L_wf_tsf_item_tbl(i).item,
                                                       L_wf_tsf_item_tbl(i).returned_qty) = FALSE then
                  return FALSE;
               end if;
            END LOOP;
         end if;

      else
         -- If transfer already exists for the return, update its status to 'Approved'.
         -- WF_UPDATE_TSF_HEAD function takes care of inventory updates as well.
         if WF_TRANSFER_SQL.WF_UPDATE_TSF_HEAD(O_error_message,
                                               L_tsf_no,
                                               'A') = FALSE then
            return FALSE;
         end if;
      end if;

      L_table := 'WF_RETURN_HEAD';
      open C_LOCK_WF_RETURN_HEAD;
      close C_LOCK_WF_RETURN_HEAD;

      -- After approving the corresponding transfer, approve the franchise return.
      update wf_return_head
         set status               = 'A',
             last_update_datetime = sysdate,
             last_update_id       = LP_user
       where rma_no = I_rma_no;

      -- If it is a return from a non-stock holding franchise store, auto-ship the transfer.
      -- After shipping, the function changes status of the return to 'In-Progress(P)'
      if L_stockholding_ind = 'N' then
         if SHIPMENT_SQL.SHIP_TSF(O_error_message,
                                  L_tsf_no,
                                  'Y') = FALSE then
            return FALSE;
         end if;
      end if;

   elsif L_return_method = 'D' then
      -- If return is 'Destroy on site' type, it is not necessary to create a transfer. Only financials will be posted for
      -- this type of return. Since 'Destroy on site' returns can be created for only non-stock holding franchise stores,
      -- inventory updates are not appliable to the franchise store and since the transaction doesn't result in change in
      -- quantity at return location, no action is necessary for the return location as well.
      -- Return quantity will be posted as a loss at return location in TRAN_DATA.

      if L_wf_tsf_item_tbl is NOT NULL and L_wf_tsf_item_tbl.COUNT > 0 then
         FOR i in 1..L_wf_tsf_item_tbl.COUNT LOOP
            if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                               L_item_record,
                                               L_wf_tsf_item_tbl(i).item) = FALSE then
               return FALSE;
            end if;
            if L_item_record.pack_ind = 'Y' then
               L_pack_no := L_item_record.item;

               FOR rec in C_ITEM_IN_PACK LOOP
                  if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                                   L_pct_in_pack,
                                                   L_pack_no,
                                                   rec.item,
                                                   L_to_loc) = FALSE then
                     return FALSE;
                  end if;
                  if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                      L_unit_cost,
                                                      L_unit_retail,
                                                      NULL,
                                                      NULL,
                                                      LP_vdate,
                                                      rec.item,
                                                      L_pack_no,
                                                      L_pct_in_pack,
                                                      rec.dept,
                                                      rec.class,
                                                      rec.subclass,
                                                      L_wf_tsf_item_tbl(i).returned_qty * rec.qty,
                                                      L_from_loc,
                                                      L_from_loc_type,
                                                      L_to_loc,
                                                      L_to_loc_type,
                                                      NULL,
                                                      I_rma_no) = FALSE then
                     return FALSE;
                  end if;
               END LOOP;
            else
               if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                   L_unit_cost,
                                                   L_unit_retail,
                                                   NULL,
                                                   NULL,
                                                   LP_vdate,
                                                   L_item_record.item,
                                                   NULL,
                                                   NULL,
                                                   L_item_record.dept,
                                                   L_item_record.class,
                                                   L_item_record.subclass,
                                                   L_wf_tsf_item_tbl(i).returned_qty,
                                                   L_from_loc,
                                                   L_from_loc_type,
                                                   L_to_loc,
                                                   L_to_loc_type,
                                                   NULL,
                                                   I_rma_no) = FALSE then
                  return FALSE;
               end if;
            end if;
            if WF_BOL_SQL.WRITE_WF_BILLING_RETURNS(O_error_message,
                                                   I_rma_no,
                                                   NULL,
                                                   L_item_record.item,
                                                   L_to_loc,
                                                   L_to_loc_type,
                                                   L_wf_tsf_item_tbl(i).returned_qty,
                                                   LP_vdate) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;

      -- Close the franchise return.
      update wf_return_head
         set status               = 'D',
             last_update_datetime = sysdate,
             last_update_id       = LP_user
       where rma_no = I_rma_no;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_RETURN_HEAD%ISOPEN then
         close C_RETURN_HEAD;
      end if;

      if C_GET_LINKED_TSF%ISOPEN then
         close C_GET_LINKED_TSF;
      end if;

      if C_WF_ITEM_RETURN%ISOPEN then
         close C_WF_ITEM_RETURN;
      end if;

      if C_ITEM_IN_PACK%ISOPEN then
         close C_ITEM_IN_PACK;
      end if;

      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            I_rma_no,
                                            L_program);
      return FALSE;

   when OTHERS then
      if C_RETURN_HEAD%ISOPEN then
         close C_RETURN_HEAD;
      end if;

      if C_GET_LINKED_TSF%ISOPEN then
         close C_GET_LINKED_TSF;
      end if;

      if C_WF_ITEM_RETURN%ISOPEN then
         close C_WF_ITEM_RETURN;
      end if;

      if C_ITEM_IN_PACK%ISOPEN then
         close C_ITEM_IN_PACK;
      end if;

      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      if C_CHECK_IL_STATUS%ISOPEN then
         close C_CHECK_IL_STATUS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END APPROVE;
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.DELETE';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   L_table         VARCHAR2(30);

   cursor C_LOCK_WF_RETURN_DETAIL is
      select 'X'
        from wf_return_detail
       where rma_no = I_rma_no
         for update nowait;

   cursor C_LOCK_WF_RETURN_HEAD is
      select 'X'
        from wf_return_head
       where rma_no = I_rma_no
         for update nowait;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --This function will delete any transfers linked to the franchise return. If there is no transfer linked to the return,
   --function does nothing and returns TRUE.
   if WF_TRANSFER_SQL.DELETE_FRANCHISE_TSF(O_error_message,
                                           NULL,             -- WF_ORDER_NO
                                           I_rma_no) = FALSE then
      return FALSE;
   end if;

   L_table := 'WF_RETURN_DETAIL';
   open C_LOCK_WF_RETURN_DETAIL;
   close C_LOCK_WF_RETURN_DETAIL;

   --Delete the return details.
   delete from wf_return_detail
    where rma_no = I_rma_no;

   L_table := 'WF_RETURN_HEAD';
   open C_LOCK_WF_RETURN_HEAD;
   close C_LOCK_WF_RETURN_HEAD;

   -- Delete return header
   delete from wf_return_head
    where rma_no = I_rma_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_RETURN_DETAIL%ISOPEN then
         close C_LOCK_WF_RETURN_DETAIL;
      end if;

      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            I_rma_no,
                                            L_program);
      return FALSE;

   when OTHERS then
      if C_LOCK_WF_RETURN_DETAIL%ISOPEN then
         close C_LOCK_WF_RETURN_DETAIL;
      end if;

      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                              I_item            IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.CHECK_DUPLICATE_ITEM';
   L_exist     VARCHAR2(1) DEFAULT 'N';

   cursor C_ITEM_EXIST is
      select 'Y'
        from wf_return_detail
       where rma_no = I_rma_no
         and item   = I_item;
BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ITEM_EXIST;
   fetch C_ITEM_EXIST into L_exist;
   close C_ITEM_EXIST;

   --If item already exists on the given return, return TRUE else FALSE.
   if L_exist = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ITEM_EXIST%ISOPEN then
         close C_ITEM_EXIST;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DUPLICATE_ITEM;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CANCEL_HEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                     I_cancel_reason   IN       WF_RETURN_DETAIL.CANCEL_REASON%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'WF_RETURN_SQL.CANCEL_HEAD';
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked,-54);
   L_table                  VARCHAR2(30);
   L_rma_exists             BOOLEAN;
   L_status                 WF_RETURN_HEAD.STATUS%TYPE := NULL;

   cursor C_WF_RETURN_DTL is
      -- Fetches list of items with their ship quantities
      select wrd.item item,
             wrd.returned_qty returned_qty,
             td.ship_qty ship_qty,
             td.received_qty received_qty
        from wf_return_detail wrd,
             wf_return_head wrh,
             tsfdetail td,
             tsfhead th
       where wrh.rma_no = I_rma_no
         and wrh.rma_no = wrd.rma_no
         and th.rma_no  = wrh.rma_no
         and th.tsf_no  = td.tsf_no
         and wrd.item   = td.item

      UNION ALL
      -- Since transfer doesn't contain deposit containers, this query fetches the containers with their return quantities.
      Select wrd.item item,
             wrd.returned_qty returned_qty,
             0 ship_qty,
             0 received_qty
        from wf_return_detail wrd,
             item_master      im
       where wrd.rma_no = I_rma_no
         and wrd.item   = im.item
         and NVL(im.deposit_item_type, '0') = 'A';

   cursor C_LOCK_WF_RETURN_HEAD is
      select 'x'
        from WF_RETURN_HEAD
       where rma_no = I_rma_no
         for update nowait;

   cursor C_LOCK_WF_RETURN_DETAIL is
      select 'x'
        from WF_RETURN_DETAIL
       where rma_no = I_rma_no
         for update nowait;

   TYPE WF_RETURN_DTL is TABLE of C_WF_RETURN_DTL%ROWTYPE INDEX BY BINARY_INTEGER;

   L_wf_return_dtl   WF_RETURN_DTL;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if VALIDATE_RMA_NUMBER(O_error_message,
                          L_rma_exists,
                          I_rma_no) = FALSE then
      return FALSE;
   end if;

   if L_rma_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('FR_INV_RMA',
                                            I_rma_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_cancel_reason is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cancel_reason',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_WF_RETURN_DTL;
   fetch C_WF_RETURN_DTL BULK COLLECT into L_wf_return_dtl;
   close C_WF_RETURN_DTL;

   L_table := 'WF_RETURN_DETAIL';
   open C_LOCK_WF_RETURN_DETAIL;
   close C_LOCK_WF_RETURN_DETAIL;

   FORALL i in 1..L_wf_return_dtl.COUNT
      update wf_return_detail
         set returned_qty         = NVL(L_wf_return_dtl(i).ship_qty,0),    -- If an item is already shipped, set the return quantity to ship qty else set return qty to zero.
             cancel_reason        = NVL(I_cancel_reason,cancel_reason),
             last_update_datetime = sysdate,
             last_update_id       = LP_user
       where rma_no = I_rma_no
         and item   = L_wf_return_dtl(i).item;

   L_table := 'WF_RETURN_HEAD';
   open C_LOCK_WF_RETURN_HEAD;
   close C_LOCK_WF_RETURN_HEAD;

   -- If a return is completely cancelled (return in Approved state), set the status of the return to 'Closed'
   update wf_return_head
      set status               = 'C',
          last_update_datetime = sysdate,
          last_update_id       = LP_user
    where rma_no = I_rma_no
      and (select SUM(wrd.returned_qty)
             from wf_return_detail wrd
            where wrd.rma_no = I_rma_no) = 0
   returning status into L_status;

   if L_status = 'C' then
      -- Cancel the linked transfer.
      if WF_TRANSFER_SQL.CANCEL_FRANCHISE_TSF(O_error_message,
                                              NULL,
                                              I_rma_no) = FALSE then
         return FALSE;
      end if;
   else
      -- Update the cancel quantities in the corresponding transfer.
      if WF_TRANSFER_SQL.MODIFY_FRANCHISE_TSF(O_error_message,
                                              NULL,
                                              I_rma_no) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_WF_RETURN_DTL%ISOPEN then
         close C_WF_RETURN_DTL;
      end if;

      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      if C_LOCK_WF_RETURN_DETAIL%ISOPEN then
         close C_LOCK_WF_RETURN_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            I_rma_no,
                                            L_program);
      return FALSE;
   when OTHERS then
      if C_WF_RETURN_DTL%ISOPEN then
         close C_WF_RETURN_DTL;
      end if;

      if C_LOCK_WF_RETURN_HEAD%ISOPEN then
         close C_LOCK_WF_RETURN_HEAD;
      end if;

      if C_LOCK_WF_RETURN_DETAIL%ISOPEN then
         close C_LOCK_WF_RETURN_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CANCEL_HEAD;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_SHIPPED_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_shipped_qty     IN OUT   WF_RETURN_DETAIL.RETURNED_QTY%TYPE,
                         I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                         I_item            IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.GET_SHIPPED_QTY';

   -- This cursor fetches the quantity of an item that is already shipped. The result can be used to restrict a user not to cancel
   -- an item beyond the shipped quantity.
   cursor C_GET_SHIPPED_QTY is
      select GREATEST(NVL(td.ship_qty,0),
                      NVL(td.distro_qty,0),
                      NVL(td.selected_qty,0)) ship_tsf_qty
        from tsfhead th,
             tsfdetail td
       where th.rma_no   = I_rma_no
         and th.tsf_no   = td.tsf_no
         and td.item     = I_item;
BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_SHIPPED_QTY;
   fetch C_GET_SHIPPED_QTY into O_shipped_qty;
   close C_GET_SHIPPED_QTY;

   if O_shipped_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('FR_ITEM_NO_EXIST',
                                            I_item,
                                            I_rma_no,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_SHIPPED_QTY%ISOPEN then
         close C_GET_SHIPPED_QTY;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_SHIPPED_QTY;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid_itemloc   IN OUT   BOOLEAN,
                        I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'WF_RETURN_SQL.CHECK_ITEM_LOC';
   L_exists       VARCHAR2(1);
   L_rma_exists   BOOLEAN;

   -- Query fetches 'X' if there is atleast one item in the return that is not ranged to the return location
   cursor C_CHECK_ITEM_LOC is
      select 'X'
        from wf_return_detail wrd,
             wf_return_head wrh
       where wrh.rma_no = I_rma_no
         and wrh.rma_no = wrd.rma_no
         and not exists (select 1
                           from item_loc il
                          where il.loc      = wrh.return_loc_id
                            and il.loc_type = wrh.return_loc_type
                            and il.item     = wrd.item);

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if VALIDATE_RMA_NUMBER(O_error_message,
                          L_rma_exists,
                          I_rma_no) = FALSE then
      return FALSE;
   end if;

   if L_rma_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('FR_INV_RMA',
                                            I_rma_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_CHECK_ITEM_LOC;
   fetch C_CHECK_ITEM_LOC into L_exists;
   close C_CHECK_ITEM_LOC;

   if L_exists is NOT NULL then
      -- If cursor fetches 'X', there are invalid item-location combinations. So, return FALSE
      O_valid_itemloc := FALSE;
   else
      O_valid_itemloc := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_ITEM_LOC%ISOPEN then
         close C_CHECK_ITEM_LOC;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ITEM_LOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'WF_RETURN_SQL.CREATE_ITEM_LOC';

   -- Identify the items in the return that are not already ranged to the return location.
   cursor C_ITEMLOC_TO_RANGE is
      select wrd.item            item,
             wrh.return_loc_id   return_loc_id,
             wrh.return_loc_type return_loc_type
        from wf_return_detail wrd,
             wf_return_head   wrh
       where wrh.rma_no = I_rma_no
         and wrh.rma_no = wrd.rma_no
         and not exists (select 1
                           from item_loc il
                          where il.loc      = wrh.return_loc_id
                            and il.loc_type = wrh.return_loc_type
                            and il.item     = wrd.item);

   TYPE ITEMLOC_RANGE_DATA is TABLE of C_ITEMLOC_TO_RANGE%ROWTYPE INDEX BY BINARY_INTEGER;
   L_itemloc_tbl   ITEMLOC_RANGE_DATA;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ITEMLOC_TO_RANGE;
   fetch C_ITEMLOC_TO_RANGE BULK COLLECT into L_itemloc_tbl;
   close C_ITEMLOC_TO_RANGE;

   FOR i in 1..L_itemloc_tbl.COUNT LOOP
      -- Create new item location combinations for the identified items.
      if NEW_ITEM_LOC(O_error_message,
                      L_itemloc_tbl(i).item,
                      L_itemloc_tbl(i).return_loc_id,
                      NULL,
                      NULL,
                      L_itemloc_tbl(i).return_loc_type,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'N',
                      NULL,
                      NULL,
                      NULL,
                      'Y') = FALSE then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ITEMLOC_TO_RANGE%ISOPEN then
         close C_ITEMLOC_TO_RANGE;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_ITEM_LOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_INVALID_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.DELETE_INVALID_ITEMLOC';

   TYPE item_tbl is TABLE of WF_RETURN_DETAIL.ITEM%TYPE   INDEX BY BINARY_INTEGER;

   L_item_tbl      item_tbl;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   L_table         VARCHAR2(30) := 'WF_RETURN_DETAIL';

   -- Query fetches items in a return that are not ranged to the return location.
   cursor C_GET_INVALID_ITEM is
      select wrd.item
        from wf_return_detail wrd,
             wf_return_head wrh
       where wrh.rma_no = I_rma_no
         and wrh.rma_no = wrd.rma_no
         and not exists (select 1
                           from item_loc il
                          where il.loc      = wrh.return_loc_id
                            and il.loc_type = wrh.return_loc_type
                            and il.item     = wrd.item)
         for update of wrd.item nowait;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_INVALID_ITEM;
   fetch C_GET_INVALID_ITEM BULK COLLECT into L_item_tbl;
   close C_GET_INVALID_ITEM;

   if L_item_tbl is NOT NULL and L_item_tbl.COUNT > 0 then
      -- Delete items in a return that are not ranged to return location of return.
      FORALL i in 1..L_item_tbl.COUNT
      delete
        from wf_return_detail
       where rma_no = I_rma_no
         and item   = L_item_tbl(i);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_INVALID_ITEM%ISOPEN then
         close C_GET_INVALID_ITEM;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            I_rma_no,
                                            L_program);
      return FALSE;

   when OTHERS then
      if C_GET_INVALID_ITEM%ISOPEN then
         close C_GET_INVALID_ITEM;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_INVALID_ITEMLOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CUSTOMER_REF_NO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT   BOOLEAN,
                                  I_ret_ref_no      IN       WF_RETURN_HEAD.CUST_RET_REF_NO%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.VALIDATE_CUSTOMER_REF_NO';
   L_exists    VARCHAR2(1)  := 'N';

   -- Query gets 'Y' if the input customer return reference number is present for any of the returns in WF_RETURN_HEAD table.
   cursor C_CUST_REF_NO IS
      select 'Y'
        from wf_return_head
       where cust_ret_ref_no = I_ret_ref_no
         and rownum          = 1;

BEGIN
   O_exists := FALSE;

   if I_ret_ref_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ret_ref_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CUST_REF_NO;
   fetch C_CUST_REF_NO into L_exists;
   close C_CUST_REF_NO;

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CUST_REF_NO%ISOPEN then
         close C_CUST_REF_NO;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END VALIDATE_CUSTOMER_REF_NO;
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER_CUSTOMER(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid             IN OUT   BOOLEAN,
                                 I_return_customer   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
                                 I_wf_order_no       IN       WF_RETURN_DETAIL.WF_ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'WF_RETURN_SQL.VALIDATE_ORDER_CUSTOMER';
   L_order_customer   WF_CUSTOMER.WF_CUSTOMER_ID%TYPE;

   -- Query fetches the customer ID of the franchise order.
   cursor C_GET_ORDER_CUSTOMER is
      select NVL(s.wf_customer_id,-999)
        from store           s,
             wf_order_detail wod
       where wod.wf_order_no      = I_wf_order_no
         and wod.wf_order_line_no = 1
         and wod.customer_loc     = s.store;

BEGIN

   if I_return_customer is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_return_customer',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_ORDER_CUSTOMER;
   fetch C_GET_ORDER_CUSTOMER into L_order_customer;
   close C_GET_ORDER_CUSTOMER;

   -- If customer of franchise order is different from that of franchise return, return FALSE else return TRUE.
   if L_order_customer != I_return_customer then
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_ORDER_CUSTOMER%ISOPEN then
         close C_GET_ORDER_CUSTOMER;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END VALIDATE_ORDER_CUSTOMER;
---------------------------------------------------------------------------------------------------------------------
FUNCTION SYNC_DEPOSIT_ITEMS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_rma_no            IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.SYNC_DEPOSIT_ITEMS';

   TYPE return_detail_rec IS RECORD (item            WF_RETURN_DETAIL.ITEM%TYPE,
                                     qty             WF_RETURN_DETAIL.RETURNED_QTY%TYPE,
                                     wf_order_no     WF_RETURN_DETAIL.WF_ORDER_NO%TYPE,
                                     return_reason   WF_RETURN_DETAIL.RETURN_REASON%TYPE);
   TYPE return_dtl IS TABLE OF return_detail_rec INDEX BY BINARY_INTEGER;

   L_containers_to_delete   return_dtl;
   L_containers_to_update   return_dtl;
   L_container_exists       BOOLEAN;
   L_customer_loc           WF_RETURN_HEAD.CUSTOMER_LOC%TYPE;
   L_return_currency        WF_RETURN_HEAD.CURRENCY_CODE%TYPE;
   L_item_loc_exists        BOOLEAN;
   L_item_status            ITEM_LOC.STATUS%TYPE;
   L_container_cost         WF_ORDER_DETAIL.CUSTOMER_COST%TYPE;
   L_container_final_cost   WF_ORDER_DETAIL.CUSTOMER_COST%TYPE;
   L_loc_currency           STORE.CURRENCY_CODE%TYPE;
   L_return_to_loc          WF_RETURN_HEAD.RETURN_LOC_ID%TYPE;
   L_item                   WF_RETURN_DETAIL.ITEM%TYPE;
   L_ranged_ind             ITEM_LOC.RANGED_IND%TYPE;
   L_dept                   ITEM_MASTER.DEPT%TYPE;
   L_class                  ITEM_MASTER.CLASS%TYPE;
   L_subclass               ITEM_MASTER.SUBCLASS%TYPE;

   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked,-54);
   L_table                  VARCHAR2(30) := 'WF_RETURN_DETAIL';

   -- Identify containers that do not have any associated contents in the return.
   cursor C_CONTAINERS_TO_DELETE is
      select wrd.item item,
             0 qty,
             NULL wf_order_no,
             NULL return_reason
        from wf_return_detail wrd,
             item_master      im
       where wrd.rma_no = I_rma_no
         and wrd.item   = im.item
         and NVL(im.deposit_item_type, '0') = 'A'
         and not exists (select 1
                           from wf_return_detail wfrd,
                                item_master      itm
                          where wfrd.rma_no = I_rma_no
                            and wfrd.item   = itm.item
                            and NVL(itm.deposit_item_type, '0') = 'E'
                            and NVL(itm.container_item,'-999')  = im.item)
         for update of wrd.item nowait;

   -- Find contents in the return, and fetch the corresponding container items with their quantities.
   cursor C_CONTAINERS_TO_UPDATE is
      select im.container_item     item,
             SUM(wrd.returned_qty) qty,
             MAX(wrd.wf_order_no)  wf_order_no,
             MAX(wrd.return_reason) return_reason
        from wf_return_detail wrd,
             item_master      im
       where wrd.rma_no = I_rma_no
         and wrd.item   = im.item
         and NVL(im.deposit_item_type,'0') = 'E'
         and im.container_item is NOT NULL
    group by im.container_item;

   cursor C_LOCK_WF_RETURN_DETAIL is
      select 'X'
        from wf_return_detail
       where rma_no = I_rma_no
         and item   = L_item
         for update nowait;

   cursor C_GET_HEADER_INFO is
      select customer_loc,
             currency_code,
             return_loc_id
        from wf_return_head
       where rma_no = I_rma_no;

   cursor C_GET_CONTAINER_DETAILS(CV_item ITEM_MASTER.ITEM%TYPE) is
      select im.dept,
             im.class,
             im.subclass,
             s.currency_code
        from item_master im,
             store       s
       where im.item     = CV_item
         and s.store     = L_customer_loc;

   -- For the gieven container, fetch the ranged indicator of the corresponding content in the return.
   -- If there are 2 contents in return with this container, one content has ranged_ind='N' and the other has 'Y', then 'Y' will be used for container.
   cursor C_GET_RANGED_IND is
      select NVL(max(il.ranged_ind),'N')
        from item_loc il,
             item_master im,
             wf_return_detail wrd
       where wrd.rma_no        = I_rma_no
         and wrd.item          = im.item
         and im.container_item = L_item
         and im.item           = il.item
         and il.loc            = L_customer_loc;

BEGIN
   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CONTAINERS_TO_DELETE;
   fetch C_CONTAINERS_TO_DELETE BULK COLLECT into L_containers_to_delete;
   close C_CONTAINERS_TO_DELETE;

   -- Delete invalid containers from return.
   FORALL i in 1..L_containers_to_delete.COUNT
      delete
        from wf_return_detail
       where rma_no = I_rma_no
         and item   = L_containers_to_delete(i).item;

   open C_CONTAINERS_TO_UPDATE;
   fetch C_CONTAINERS_TO_UPDATE BULK COLLECT into L_containers_to_update;
   close C_CONTAINERS_TO_UPDATE;

   open C_GET_HEADER_INFO;
   fetch C_GET_HEADER_INFO into L_customer_loc,
                                L_return_currency,
                                L_return_to_loc;
   close C_GET_HEADER_INFO;

   FOR i in 1..L_containers_to_update.COUNT LOOP
      -- Check if container to be added already exists on the return.
      if CHECK_DUPLICATE_ITEM(O_error_message,
                              L_container_exists,
                              I_rma_no,
                              L_containers_to_update(i).item) =  FALSE then
         return FALSE;
      end if;

      -- If container already exists, update its return quantity
      if L_container_exists = TRUE then

         L_item := L_containers_to_update(i).item;
         open C_LOCK_WF_RETURN_DETAIL;
         close C_LOCK_WF_RETURN_DETAIL;

         update wf_return_detail
            set returned_qty         = L_containers_to_update(i).qty,
                last_update_datetime = sysdate,
                last_update_id       = LP_user
          where rma_no      = I_rma_no
            and item        = L_containers_to_update(i).item;
      else
         -- If container doesn't exist on the return, check if the container is ranged to the customer location
         if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                              L_containers_to_update(i).item,
                                              L_customer_loc,
                                              L_item_loc_exists) = FALSE then
            return FALSE;
         end if;

         -- If container is not ranged to the customer location, range it.
         if L_item_loc_exists = FALSE then
            -- Create a new item-loc combination for the item and source location taking ranged_ind from the content.
            L_item := L_containers_to_update(i).item;

            open C_GET_RANGED_IND;
            fetch C_GET_RANGED_IND into L_ranged_ind;
            close C_GET_RANGED_IND;

            if NEW_ITEM_LOC(O_error_message,
                            L_containers_to_update(i).item,
                            L_customer_loc,
                            NULL,
                            NULL,
                            'S',
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            'N',
                            NULL,
                            NULL,
                            NULL,
                            L_ranged_ind) = FALSE then
               return FALSE;
            end if;
         end if;

         -- If there is a franchise order linked to the content item, get container return unit cost from the linked order number.
         if L_containers_to_update(i).wf_order_no is NOT NULL then
            if GET_ITEM_RETURN_COST(O_error_message,
                                    L_container_final_cost,
                                    L_return_currency,
                                    L_return_to_loc,
                                    L_containers_to_update(i).wf_order_no,
                                    L_containers_to_update(i).item) = FALSE then
               return FALSE;
            end if;
         else
            -- If there is no reference order number, use WAC of container at customer location as return unit cost.
            open C_GET_CONTAINER_DETAILS(L_containers_to_update(i).item);
            fetch C_GET_CONTAINER_DETAILS into L_dept,
                                               L_class,
                                               L_subclass,
                                               L_loc_currency;
            close C_GET_CONTAINER_DETAILS;

            if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                          L_container_cost,
                                          L_containers_to_update(i).item,
                                          L_dept,
                                          L_class,
                                          L_subclass,
                                          L_customer_loc,
                                          'S',
                                          NULL,
                                          NULL,
                                          NULL) = FALSE then
               return FALSE;
            end if;

            -- Convert the cost to return currency
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_container_cost,
                                    L_loc_currency,
                                    L_return_currency,
                                    L_container_final_cost,
                                    'C',
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
         end if;

         -- Insert desposit container into the return.
         insert into wf_return_detail(rma_no,
                                      item,
                                      wf_order_no,
                                      returned_qty,
                                      return_reason,
                                      cancel_reason,
                                      return_unit_cost,
                                      restock_type,
                                      unit_restock_fee,
                                      net_return_unit_cost,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
                               values(I_rma_no,
                                      L_containers_to_update(i).item,
                                      L_containers_to_update(i).wf_order_no,
                                      L_containers_to_update(i).qty,
                                      L_containers_to_update(i).return_reason,
                                      NULL,
                                      L_container_final_cost,
                                      NULL,
                                      NULL,
                                      L_container_final_cost,
                                      sysdate,
                                      LP_user,
                                      sysdate,
                                      LP_user);
      end if;
   END LOOP;

return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_CONTAINERS_TO_DELETE%ISOPEN then
         close C_CONTAINERS_TO_DELETE;
      end if;

      if C_CONTAINERS_TO_UPDATE%ISOPEN then
         close C_CONTAINERS_TO_UPDATE;
      end if;

      if C_LOCK_WF_RETURN_DETAIL%ISOPEN then
         close C_LOCK_WF_RETURN_DETAIL;
      end if;

      if C_GET_HEADER_INFO%ISOPEN then
         close C_GET_HEADER_INFO;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_rma_no,
                                            L_item);
      return FALSE;

   when OTHERS then
      if C_CONTAINERS_TO_DELETE%ISOPEN then
         close C_CONTAINERS_TO_DELETE;
      end if;

      if C_CONTAINERS_TO_UPDATE%ISOPEN then
         close C_CONTAINERS_TO_UPDATE;
      end if;

      if C_LOCK_WF_RETURN_DETAIL%ISOPEN then
         close C_LOCK_WF_RETURN_DETAIL;
      end if;

      if C_GET_HEADER_INFO%ISOPEN then
         close C_GET_HEADER_INFO;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END SYNC_DEPOSIT_ITEMS;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_RETURN_COST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_return_cost       IN OUT   WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE,
                         O_return_currency   IN OUT   WF_RETURN_HEAD.CURRENCY_CODE%TYPE,
                         O_restocking_cost   IN OUT   WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE,
                         I_rma_no            IN       WF_RETURN_DETAIL.RMA_NO%TYPE,
                         I_tsf_no            IN       TSFHEAD.TSF_NO%TYPE,
                         I_item              IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_RETURN_SQL.GET_RETURN_COST';
   L_rma_no    WF_RETURN_DETAIL.RMA_NO%TYPE := NULL;

   -- Get the corresponding return number if transfer number is passed.
   cursor C_GET_LINKED_RMA_NO is
      select rma_no
        from tsfhead
       where tsf_no = I_tsf_no;

   -- Fetch return unit cost and restocking amount from a franchise return detail corresponding to the return and
   -- item combination given as input.
   cursor C_GET_RETURN_COST is
      select NVL(wrd.return_unit_cost, 0),
             DECODE(wrd.restock_type,
                       'S', wrd.unit_restock_fee,
                       'V', wrd.return_unit_cost * (wrd.unit_restock_fee/100),
                       0),
             wrh.currency_code
        from wf_return_detail wrd,
             wf_return_head   wrh
       where wrh.rma_no = NVL(L_rma_no, I_rma_no)
         and wrh.rma_no = wrd.rma_no
         and wrd.item   = I_item;

BEGIN

   if I_tsf_no is NULL and I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_EITHER',
                                            'I_tsf_no',
                                            'I_rma_no',
                                            L_program);
      return FALSE;
   end if;

   if I_tsf_no is NOT NULL and I_rma_no is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_COMBO',
                                            'I_tsf_no',
                                            'I_rma_no',
                                            L_program);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_tsf_no is NOT NULL then
      open C_GET_LINKED_RMA_NO;
      fetch C_GET_LINKED_RMA_NO into L_rma_no;
      close C_GET_LINKED_RMA_NO;
   end if;

   open C_GET_RETURN_COST;
   fetch C_GET_RETURN_COST into O_return_cost,
                                O_restocking_cost,
                                O_return_currency;
   close C_GET_RETURN_COST;

return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_LINKED_RMA_NO%ISOPEN then
         close C_GET_LINKED_RMA_NO;
      end if;

      if C_GET_RETURN_COST%ISOPEN then
         close C_GET_RETURN_COST;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END GET_RETURN_COST;
-----------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INFO(I_item              IN   WF_RETURN_DETAIL.ITEM%TYPE,
                      I_loc               IN   ITEM_LOC.LOC%TYPE,
                      I_loc_type          IN   ITEM_LOC.LOC_TYPE%TYPE,
                      I_cost_retail_ind   IN   VARCHAR2,
                      I_from_loc          IN   ITEM_LOC.LOC%TYPE DEFAULT NULL,
                      I_from_loc_type     IN   ITEM_LOC.LOC_TYPE%TYPE DEFAULT NULL,
                      I_rma_no            IN   WF_RETURN_HEAD.RMA_NO%TYPE DEFAULT NULL)
RETURN NUMBER IS

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_tax_code        VAT_CODES.VAT_CODE%TYPE;
   L_tax_rate        VAT_CODE_RATES.VAT_RATE%TYPE;

BEGIN

   if GET_TAX_INFO(L_error_message,
                   L_tax_rate,
                   L_tax_code,
                   I_item,
                   I_loc,
                   I_loc_type,
                   I_cost_retail_ind,
                   I_from_loc,
                   I_from_loc_type,
                   I_rma_no) = FALSE then
      raise_application_error(-20018, L_error_message);
   end if;

   return L_tax_rate;

EXCEPTION
   when OTHERS then
      raise;

END GET_TAX_INFO;
-----------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INFO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_tax_rate          IN OUT   VAT_CODE_RATES.VAT_RATE%TYPE,
                      O_tax_code          IN OUT   VAT_CODES.VAT_CODE%TYPE,
                      I_item              IN       WF_RETURN_DETAIL.ITEM%TYPE,
                      I_loc               IN       ITEM_LOC.LOC%TYPE,
                      I_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                      I_cost_retail_ind   IN       VARCHAR2,
                      I_from_loc          IN       ITEM_LOC.LOC%TYPE DEFAULT NULL,
                      I_from_loc_type     IN       ITEM_LOC.LOC_TYPE%TYPE DEFAULT NULL,
                      I_rma_no            IN       WF_RETURN_HEAD.RMA_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'WF_RETURN_SQL.GET_TAX_INFO';

   L_loc                ITEM_LOC.LOC%TYPE;
   L_loc_type           VARCHAR2(2);
   L_tax_calc_tbl        OBJ_TAX_CALC_TBL              := OBJ_TAX_CALC_TBL();
   L_tax_calc_rec        OBJ_TAX_CALC_REC              := OBJ_TAX_CALC_REC();
   L_from_loc_type      VARCHAR2(2);

   -- Get the retrun loc of customer location
   cursor C_RT_LOC is
      select store,
             'ST'
        from store
       where store      = I_loc
         and I_loc_type = 'S'
      UNION ALL
      select wh,
             'WH'
        from wh
       where wh         = I_loc
         and I_loc_type = 'W';



BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type NOT in ('S', 'W') then
      O_error_message := SQL_LIB.CREATE_MSG('LOC_TYPE_NOT_VALID',
                                            I_loc_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_cost_retail_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_retail_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_cost_retail_ind NOT in ('C', 'R') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                            'I_cost_retail_ind',
                                            I_cost_retail_ind,
                                            L_program);
      return FALSE;
   end if;

   O_tax_rate := 0;

   open C_RT_LOC;
   fetch C_RT_LOC into L_loc,
                       L_loc_type;

   close C_RT_LOC;

   if L_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_tax_calc_rec.I_item             := I_item;
   L_tax_calc_rec.I_from_entity      := L_loc;
   L_tax_calc_rec.I_from_entity_type := L_loc_type;    -- This will be used to fetch GTAX and GTAX table uses loc_type 'ST' for store, 'WH' for warehouse.
   L_tax_calc_rec.I_to_entity        := I_from_loc;
   if I_from_loc_type = 'W' then
      L_from_loc_type := 'WH';
   elsif I_from_loc_type = 'S' then
      L_from_loc_type := 'ST';
   end if;
   L_tax_calc_rec.I_to_entity_type   := L_from_loc_type;
   L_tax_calc_rec.I_tran_type        := 'RETURN';
   L_tax_calc_rec.I_tran_date        := GET_VDATE();
   L_tax_calc_rec.I_tran_id          := I_rma_no;

   L_tax_calc_tbl.extend();
   L_tax_calc_tbl(L_tax_calc_tbl.COUNT) := L_tax_calc_rec;

   if I_cost_retail_ind = 'R' then
      if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                 L_tax_calc_tbl) = FALSE then
         return FALSE;
      end if;
   elsif I_cost_retail_ind = 'C' then
      if TAX_SQL.CALC_COST_TAX(O_error_message,
                               L_tax_calc_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_tax_calc_tbl.count > 0 and L_tax_calc_tbl.count is not null then
      if L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_exempt_ind = 'Y' then
          O_tax_rate := NULL;
          O_tax_code := NULL;
      else
          O_tax_rate := L_tax_calc_tbl(L_tax_calc_tbl.count).O_cum_tax_pct;
          if L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl is not null and L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl.count > 0 then
             O_tax_code := L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl(1).tax_code;
          else
             O_tax_code := NULL;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_RT_LOC%ISOPEN then
         close C_RT_LOC;
      end if;


      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TAX_INFO;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_RANGED_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_ranged            IN OUT   BOOLEAN,
                               I_rma_no            IN       WF_RETURN_HEAD.RMA_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'WF_RETURN_SQL.CHECK_ITEM_RANGED_IND';
   L_ranged_ind   VARCHAR2(1)  := NULL;

   -- If there is atleast one item in the return with ranged_ind='N' at return location, query will return 'N'.
   cursor C_CHECK_ITEM_RANGED_IND is
      select 'N'
        from wf_return_head wrh,
             wf_return_detail wrd,
             item_loc il
       where wrh.rma_no    = I_rma_no
         and wrh.rma_no    = wrd.rma_no
         and wrd.item      = il.item
         and il.loc        = wrh.return_loc_id
         and il.loc_type   = wrh.return_loc_type
         and il.ranged_ind = 'N'
         and rownum        = 1;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CHECK_ITEM_RANGED_IND;
   fetch C_CHECK_ITEM_RANGED_IND into L_ranged_ind;
   close C_CHECK_ITEM_RANGED_IND;

   if L_ranged_ind = 'N' then
      O_ranged := FALSE;
   else
      O_ranged := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ITEM_RANGED_IND;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_ITEM_RANGED_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rma_no            IN       WF_RETURN_HEAD.RMA_NO%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'WF_RETURN_SQL.CHANGE_ITEM_RANGED_IND';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   L_table         VARCHAR2(30) := 'ITEM_LOC';

   -- Lock the rows in ITEM_LOC that need to be updated.
   cursor C_LOCK_ITEM_LOC is
      select 'X'
        from wf_return_head wrh,
             wf_return_detail wrd,
             item_loc il
       where wrh.rma_no    = I_rma_no
         and wrh.rma_no    = wrd.rma_no
         and wrd.item      = il.item
         and il.loc        = wrh.return_loc_id
         and il.loc_type   = wrh.return_loc_type
         and il.ranged_ind = 'N'
         for update of il.ranged_ind nowait;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_LOCK_ITEM_LOC;
   close C_LOCK_ITEM_LOC;

   -- For the items in the return that have RANGED_IND = 'N' in ITEM_LOC table for item and return location
   -- combinations, change RANGED_IND to 'Y'.
   merge into item_loc itl
   using (select il.item,
                 il.loc
            from wf_return_head wrh,
                 wf_return_detail wrd,
                 item_loc il
           where wrh.rma_no    = I_rma_no
             and wrh.rma_no    = wrd.rma_no
             and wrd.item      = il.item
             and il.loc        = wrh.return_loc_id
             and il.loc_type   = wrh.return_loc_type
             and il.ranged_ind = 'N') res
   on (res.item = itl.item and res.loc = itl.loc)
   when matched then
      update set ranged_ind = 'Y';

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHANGE_ITEM_RANGED_IND;
---------------------------------------------------------------------------------------------------------------------
FUNCTION INS_UPD_WF_RETURN_DATA_WRP (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_return_code     IN OUT   VARCHAR2,
                                     I_wf_return_tbl   IN       WRP_WF_RETURN_DETAIL_TBL,
                                     I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
RETURN INTEGER IS

   L_program         VARCHAR2(60) := 'WF_RETURN_SQL.INS_UPD_WF_RETURN_DATA_WRP';
   L_wf_return_tbl   WF_RETURN_SQL.WF_RETURN_DTL_TBL;

BEGIN

   if I_wf_return_tbl.COUNT > 0 and I_wf_return_tbl is NOT NULL then
      FOR i IN 1..I_wf_return_tbl.COUNT LOOP
         L_wf_return_tbl(i).item                         := I_wf_return_tbl(i).item;
         L_wf_return_tbl(i).item_desc                    := I_wf_return_tbl(i).item_desc;
         L_wf_return_tbl(i).wf_order_id                  := I_wf_return_tbl(i).wf_order_id;
         L_wf_return_tbl(i).order_qty                    := I_wf_return_tbl(i).order_qty;
         L_wf_return_tbl(i).customer_cost                := I_wf_return_tbl(i).customer_cost;
         L_wf_return_tbl(i).cust_ord_ref_no              := I_wf_return_tbl(i).cust_ord_ref_no;
         L_wf_return_tbl(i).return_unit_cost             := I_wf_return_tbl(i).return_unit_cost;
         L_wf_return_tbl(i).returned_qty                 := I_wf_return_tbl(i).returned_qty;
         L_wf_return_tbl(i).final_return_unit_cost       := I_wf_return_tbl(i).final_return_unit_cost;
         L_wf_return_tbl(i).vat                          := I_wf_return_tbl(i).vat;
         L_wf_return_tbl(i).vat_rate                     := I_wf_return_tbl(i).vat_rate;
         L_wf_return_tbl(i).return_reason                := I_wf_return_tbl(i).return_reason;
         L_wf_return_tbl(i).cancel_reason                := I_wf_return_tbl(i).cancel_reason;
         L_wf_return_tbl(i).ref_item                     := I_wf_return_tbl(i).ref_item;
         L_wf_return_tbl(i).restock_type                 := I_wf_return_tbl(i).restock_type;
         L_wf_return_tbl(i).unit_restock_fee             := I_wf_return_tbl(i).unit_restock_fee;
         L_wf_return_tbl(i).final_return_total_cost      := I_wf_return_tbl(i).final_return_total_cost;
         L_wf_return_tbl(i).return_unit_cost_incl_vat    := I_wf_return_tbl(i).return_unit_cost_incl_vat;
         L_wf_return_tbl(i).final_ret_unit_cost_invat    := I_wf_return_tbl(i).final_ret_unit_cost_invat;
         L_wf_return_tbl(i).final_ret_total_cost_invat   := I_wf_return_tbl(i).final_ret_total_cost_invat;
         L_wf_return_tbl(i).row_id                       := I_wf_return_tbl(i).row_id;
         L_wf_return_tbl(i).return_code                  := I_wf_return_tbl(i).return_code;
         L_wf_return_tbl(i).error_message                := I_wf_return_tbl(i).error_message;
      END LOOP;

      WF_RETURN_SQL.INS_UPD_WF_RETURN_DATA(O_error_message,
                                           O_return_code,
                                           L_wf_return_tbl,
                                           I_rma_no);

      if O_return_code = 'FALSE' then
         return 0;
      end if;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END INS_UPD_WF_RETURN_DATA_WRP;
---------------------------------------------------------------------------------------------------------------------
FUNCTION MASS_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                     I_restock_type    IN       WF_RETURN_DETAIL.RESTOCK_TYPE%TYPE,
                     I_restock_fee     IN       WF_RETURN_DETAIL.UNIT_RESTOCK_FEE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)             := 'WF_RETURN_SQL.MASS_UPDATE';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE :=  NULL;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,-54);
   L_table           VARCHAR2(30)             := 'WF_RETURN_DETAIL';

   cursor C_LOCK_WF_RETURN_DETAIL is
      select 'X'
        from wf_return_detail
       where rma_no = I_rma_no
         for update nowait;

BEGIN

   if I_rma_no is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rma_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_restock_type is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_restock_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_restock_fee is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_restock_fee',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_LOCK_WF_RETURN_DETAIL;
   close C_LOCK_WF_RETURN_DETAIL;

   update wf_return_detail
      set restock_type           = I_restock_type,
          unit_restock_fee       = I_restock_fee,
          net_return_unit_cost   = DECODE(I_restock_type, 'S', return_unit_cost - I_restock_fee, 'V', return_unit_cost * (1 - (I_restock_fee /100))),
          last_update_datetime   = SYSDATE,
          last_update_id         = LP_user
    where rma_no = I_rma_no;

    return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_rma_no,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END MASS_UPDATE;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_RETURN_COST(I_return_currency   IN       WF_RETURN_HEAD.CURRENCY_CODE%TYPE,
                              I_return_from_loc   IN       WF_RETURN_HEAD.CUSTOMER_LOC%TYPE,
                              I_wf_order_no       IN       WF_RETURN_DETAIL.WF_ORDER_NO%TYPE,
                              I_item              IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN NUMBER IS

   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_order_cost              WF_ORDER_DETAIL.CUSTOMER_COST%TYPE;

BEGIN

  if GET_ITEM_RETURN_COST(L_error_message,
                          L_order_cost,
                          I_return_currency,
                          I_return_from_loc,
                          I_wf_order_no,
                          I_item) = FALSE then
      raise_application_error(-20118, L_error_message);
   end if;

   return L_order_cost;

EXCEPTION
   when OTHERS then
      raise;

END GET_ITEM_RETURN_COST;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_RETURN_QTY(I_wf_order_no       IN       WF_RETURN_DETAIL.WF_ORDER_NO%TYPE,
                             I_item              IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN NUMBER IS

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_order_qty       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   
   cursor C_GET_ORDER_QTY is
      select sum(requested_qty) requested_qty
        from ( select wod.item,
                requested_qty,
                'N' pack_component_ind,
                null item_pack_no
           from wf_order_detail wod,
                wf_order_head   woh
          where woh.wf_order_no = I_wf_order_no
            and woh.wf_order_no = wod.wf_order_no
            and wod.item        = I_item
          UNION
          select vpq.item,
                 requested_qty * qty,
                 'Y' pack_component_ind,
                 vpq.pack_no        item_pack_no
            from wf_order_detail wod,
                 wf_order_head   woh,
                 v_packsku_qty   vpq
           where woh.wf_order_no = I_wf_order_no
             and woh.wf_order_no = wod.wf_order_no
             and vpq.item        = I_item
             and wod.item        = vpq.pack_no );

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ORDER_QTY', NULL, NULL);
   open C_GET_ORDER_QTY;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_ORDER_QTY', NULL, NULL);
   fetch C_GET_ORDER_QTY into L_order_qty;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORDER_QTY', NULL, NULL);
   close C_GET_ORDER_QTY;
   
   
   return L_order_qty;
EXCEPTION
   when OTHERS then
      raise;   
END GET_ITEM_RETURN_QTY;
----------------------------------------------------------------------------------------------------------
END WF_RETURN_SQL;
/