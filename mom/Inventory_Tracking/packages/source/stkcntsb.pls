CREATE OR REPLACE PACKAGE BODY STKCOUNT_SQL AS
-------------------------------------------------------------------
-- Declare record type used in NEW_STKCNT_ITEM_LOC
TYPE ITEM_REC_TYPE IS RECORD
(item       ITEM_MASTER.ITEM%TYPE,
 pack_qty   V_PACKSKU_QTY.QTY%TYPE);


TYPE ITEM_TABLE_TYPE IS TABLE OF ITEM_REC_TYPE
INDEX BY BINARY_INTEGER;

LP_item                     ITEM_TABLE_TYPE;
---------------------------------------------------------------------------------------
-- Function Name: UPDATE_STAKE_SKU_LOC
-- Purpose: This private function will handle the physical count update 
-- in stake_sku_loc table.
---------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_SKU_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                              I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                              I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                              I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_count_qty       IN       STAKE_QTY.QTY%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------
-- This procedure inserts a row into stake_product for the all
-- departments.
-------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
PROCEDURE STAKE_PRODUCT_INSERT (I_cycle_count    IN     NUMBER,
                                I_dept           IN     NUMBER,
                                O_return_code    IN OUT VARCHAR2,
                                O_error_message  IN OUT VARCHAR2) IS
   L_dept DEPS.DEPT%TYPE;
   CURSOR C_DEPS is
      select dept
        from v_deps
       where purchase_type = 0;
BEGIN
   O_return_code := 'TRUE';
   if I_dept = 9999 then
      /* All departments */
      open C_DEPS;
      LOOP
         fetch C_DEPS into L_dept;
         if C_DEPS%NOTFOUND then
            EXIT;
         else
            insert into stake_product(cycle_count,
                                      dept,
                                      class,
                                      subclass )
                               values(I_cycle_count,
                                      L_dept,
                                      NULL,
                                      NULL );
         end if;
      END LOOP;
      close C_DEPS;
   end if;
EXCEPTION
   when OTHERS then
      O_return_code   := 'FALSE';
      O_error_message := SQLERRM;
END STAKE_PRODUCT_INSERT;
------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ITEM_MASTER
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
PROCEDURE STAKE_SKULIST_INSERT (I_cycle_count    IN     STAKE_LOCATION.CYCLE_COUNT%TYPE,
                                I_skulist        IN     SKULIST_DETAIL.SKULIST%TYPE,
                                I_stocktake_date IN     DATE,
                                O_dup_unit_ind   IN OUT VARCHAR2,
                                O_no_item_ind    IN OUT VARCHAR2,
                                O_non_inv_ind    IN OUT VARCHAR2,
                                O_return_code    IN OUT VARCHAR2,
                                O_error_message  IN OUT VARCHAR2) IS

   L_item          ITEM_LOC.ITEM%TYPE;
   L_loc_type      ITEM_LOC.LOC_TYPE%TYPE;
   L_first_store   VARCHAR2(1)                 :='Y';
   L_first_item    VARCHAR2(1)                 :='Y';
   L_insert_flag   VARCHAR2(1);
   L_dept          NUMBER;
   L_class         NUMBER;
   L_subclass      NUMBER;
   L_sku_loc_cnt   NUMBER;
   L_insert_item   STAKE_SKU_LOC.ITEM%type;
   L_exist         VARCHAR2(1);
   L_pack_exist    VARCHAR2(1);
   L_location      ITEM_LOC.LOC%TYPE;
   L_pack_holder   PACKITEM_BREAKOUT.ITEM%TYPE := NULL;
   L_comp_insert   VARCHAR2(1);
   L_unit_retail   ITEM_LOC.UNIT_RETAIL%type;
   L_pack_ind      ITEM_MASTER.PACK_IND%type;
   L_sellable_ind  ITEM_MASTER.SELLABLE_IND%type;
   L_orderable_ind ITEM_MASTER.ORDERABLE_IND%type;
   L_pack_type     ITEM_MASTER.PACK_TYPE%type;
   L_item_level    ITEM_MASTER.ITEM_LEVEL%type;
   L_tran_level    ITEM_MASTER.TRAN_LEVEL%type;

   L_item_master        ITEM_MASTER%ROWTYPE;
   L_item_xform_type    VARCHAR2(1);
   L_non_inventory_ind  VARCHAR2(1);

   cursor C_GET_LOCATIONS is
       select loc_type,
              location
         from stake_location
        where stake_location.cycle_count = I_cycle_count;

   -- Need to use distinct becuase item and item parent may exist on the same skulist.
   CURSOR C_GET_ITEMS IS
       SELECT DISTINCT im.item
         FROM skulist_detail sd,
              v_item_master im,
              deps d
        WHERE sd.skulist              = I_skulist
          AND (im.item                = sd.item
               OR im.item_parent      = sd.item
               OR im.item_grandparent = sd.item)
          AND im.item_level           = im.tran_level
          AND im.dept = d.dept
          AND d.purchase_type = 0
          AND im.status               = 'A' 
          AND NOT exists (select il.item 
                            FROM item_loc il, 
                                 item_loc_soh sh
                           WHERE il.item = sd.item
                             AND sh.item = sd.item
                             AND sh.item = il.item
                             AND il.status NOT in ('I', 'D', 'A', 'C')
                             AND il.loc_type = L_loc_type
                             AND il.loc = L_location
                             AND sh.stock_on_hand = 0
                             AND sh.in_transit_qty = 0
                             AND sh.tsf_reserved_qty = 0
                             AND sh.tsf_expected_qty = 0
                             AND sh.non_sellable_qty = 0
                             AND sh.rtv_qty = 0)
          AND im.inventory_ind        = 'Y'
       UNION ALL
       SELECT DISTINCT ixd.detail_item
         FROM skulist_detail sd,
              item_xform_head ixh,
              item_xform_detail ixd,
              v_item_master im
        WHERE sd.skulist = I_skulist
          AND ixh.head_item = sd.item
          AND ixh.item_xform_head_id = ixd.item_xform_head_id
          AND ixd.detail_item = im.item
          AND im.status = 'A';

   cursor C_GET_ITEM_INFO is
       select im.dept,
              im.class,
              im.subclass,
              im.pack_ind,
              il.unit_retail
         from item_master im, item_loc il
        where il.loc                  = L_location
          and (il.item                = L_item
               or il.item_parent      = L_item
               or il.item_grandparent = L_item)
          and im.item                 = il.item
          and im.item_level           = im.tran_level;

   cursor C_FIND_EXISTS is
       select 'x'
         from stake_sku_loc ssl,
              stake_head sh
        where ssl.item                 = L_insert_item
          and ssl.loc_type             = L_loc_type
          and ssl.location             = L_location
          and ssl.cycle_count          = sh.cycle_count
          and trunc(sh.stocktake_date) = I_stocktake_date
          and ssl.cycle_count         != I_cycle_count
          and sh.delete_ind            = 'N';

   cursor C_CHECK_DUP_COMP is
       select 'x'
         from stake_sku_loc ssl
        where ssl.item        = L_insert_item
          and ssl.loc_type    = L_loc_type
          and ssl.location    = L_location
          and ssl.cycle_count = I_cycle_count;

   cursor C_ITEMS_IN_PACK is
       select distinct im.item
         from item_master im,
              packitem_breakout pb,
              item_loc il
        where im.item    = pb.item
          and im.item    = il.item
          and pb.pack_no = L_pack_holder
          and il.loc     = L_location;

   CURSOR C_NON_INVENTORY_EXISTS IS
       SELECT 'x'
         FROM skulist_detail sd,
              v_item_master im
        WHERE sd.skulist              = I_skulist
          AND (im.item                = sd.item
               OR im.item_parent      = sd.item
               OR im.item_grandparent = sd.item)
          AND im.item_level           = im.tran_level
          AND im.status               = 'A'
          AND im.inventory_ind        = 'N'
          AND ROWNUM = 1;

BEGIN
   O_return_code        := 'TRUE';
   O_error_message      := NULL;
   O_dup_unit_ind       :='N';
   O_no_item_ind        :='N';
   O_non_inv_ind        :='N';

   open C_GET_LOCATIONS;
   LOOP
      fetch C_GET_LOCATIONS into L_loc_type, L_location;
      if C_GET_LOCATIONS%NOTFOUND and L_first_store = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_LOC_FOR_CYCL_CNT',
                                               NULL,
                                               NULL,
                                               NULL);
         O_return_code := 'FALSE';
         EXIT;
      elsif C_GET_LOCATIONS%NOTFOUND and L_first_store = 'N' then
         EXIT;
      end if;

      L_first_store := 'N';

      open C_GET_ITEMS;
      LOOP
         fetch C_GET_ITEMS into L_item;

         if C_GET_ITEMS%NOTFOUND and L_first_item = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_FOR_ITEMLIST',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            O_return_code := 'FALSE';
            EXIT;
         elsif C_GET_ITEMS%NOTFOUND and L_first_item = 'N' then
            EXIT;
         end if;

         L_first_item  := 'N';
         L_insert_flag := 'N';
         ---
         open C_GET_ITEM_INFO;
         fetch C_GET_ITEM_INFO into L_dept,
                                    L_class,
                                    L_subclass,
                                    L_pack_ind,
                                    L_unit_retail;

         if C_GET_ITEM_INFO%NOTFOUND then
            O_no_item_ind := 'Y';
         else
            L_insert_flag := 'Y';
            O_no_item_ind := 'N';
         end if;
         ---
         if O_no_item_ind = 'N' then
            if L_pack_ind = 'N' then
               L_insert_item :=  L_item;

               if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                                  L_item_master,
                                                  L_item) = FALSE then
                  O_return_code := 'FALSE';
                  EXIT;
               end if;
               if L_item_master.item_xform_ind = 'Y' then
                  if L_item_master.inventory_ind = 'Y' then
                     L_item_xform_type := 'O';
                  else
                     L_item_xform_type := 'S';
                  end if;
               else
                  L_item_xform_type := NULL;
               end if;

               open C_FIND_EXISTS;
               fetch C_FIND_EXISTS into L_exist;

               if C_FIND_EXISTS%FOUND then
                  O_dup_unit_ind := 'Y';
               elsif C_FIND_EXISTS%NOTFOUND then
                  -- Only insert item if it hasn't been inserted.
                  -- Item may exist as component of a pack item
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_CHECK_DUP_COMP',
                                   'stake_head, stake_sku_loc',
                                   'SKU:'||(L_insert_item));
                  open C_CHECK_DUP_COMP;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_CHECK_DUP_COMP',
                                   'stake_head, stake_sku_loc',
                                   'SKU:'||(L_insert_item));
                  fetch C_CHECK_DUP_COMP into L_exist;
                  if C_CHECK_DUP_COMP%NOTFOUND then
                     insert into stake_sku_loc
                                              (cycle_count,
                                               loc_type,
                                               location,
                                               item,
                                               snapshot_on_hand_qty,
                                               snapshot_in_transit_qty,
                                               snapshot_unit_cost,
                                               snapshot_unit_retail,
                                               processed,
                                               physical_count_qty,
                                               pack_comp_qty,
                                               dept,
                                               class,
                                               subclass,
                                               deposit_item_type,
                                               xform_item_type)
                                        values(I_cycle_count,
                                               L_loc_type,
                                               L_location,
                                               L_insert_item,
                                               NULL,
                                               NULL,
                                               NULL,
                                               NVL(L_unit_retail,0),
                                               'N',
                                               NULL,
                                               0,
                                               L_dept,
                                               L_class,
                                               L_subclass,
                                               DECODE(L_item_master.deposit_item_type,'E','E',NULL),
                                               L_item_xform_type);
                  end if;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_CHECK_DUP_COMP',
                                   'stake_head, stake_sku_loc',
                                   'SKU:'||(L_insert_item));

                  close C_CHECK_DUP_COMP;
               end if;
               close C_FIND_EXISTS;
               L_insert_flag := 'N';

            elsif L_pack_ind = 'Y' then --  /* else for pack_ind check */
               --* loop on the pack comp items' of the pack *--
               L_pack_holder := L_item;

               FOR rec in C_ITEMS_IN_PACK LOOP
                  L_insert_item   := rec.item;

                  --* check if pack comp item exists on other stock counts *--
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_FIND_EXISTS',
                                   'stake_head, stake_sku_loc',
                                   'SKU:'||(L_insert_item));
                  open C_FIND_EXISTS;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_FIND_EXISTS',
                                   'stake_head, stake_sku_loc',
                                   'SKU:'||(L_insert_item));
                  fetch C_FIND_EXISTS into L_exist;

                  if C_FIND_EXISTS%FOUND then
                     L_comp_insert  := 'N';
                     O_dup_unit_ind := 'Y';
                  else
                     -- Only insert component if it hasn't been inserted.
                     -- Item may exist as a bulk item or as component of another pack item
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_CHECK_DUP_COMP',
                                      'stake_head, stake_sku_loc',
                                      'SKU:'||(L_insert_item));
                     open C_CHECK_DUP_COMP;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_CHECK_DUP_COMP',
                                      'stake_head, stake_sku_loc',
                                      'SKU:'||(L_insert_item));
                     fetch C_CHECK_DUP_COMP into L_exist;
                     if C_CHECK_DUP_COMP%FOUND then
                        L_comp_insert  := 'N';
                     else
                        L_comp_insert  := 'Y';
                     end if;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_CHECK_DUP_COMP',
                                      'stake_head, stake_sku_loc',
                                      'SKU:'||(L_insert_item));

                     close C_CHECK_DUP_COMP;
                  end if;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_FIND_EXISTS',
                                   'stake_head, stake_sku_loc',
                                   'SKU:'||(L_insert_item));
                  close C_FIND_EXISTS;

                  --* insert the pack comp item into stake_sku_loc *--
                  if L_comp_insert = 'Y' then
                     SQL_LIB.SET_MARK('INSERT',
                                      NULL,
                                      'stake_sku_loc',
                                      'SKU:'||(L_insert_item));
                     insert into stake_sku_loc
                                              (cycle_count,
                                               loc_type,
                                               location,
                                               item,
                                               snapshot_on_hand_qty,
                                               snapshot_in_transit_qty,
                                               snapshot_unit_cost,
                                               snapshot_unit_retail,
                                               processed,
                                               physical_count_qty,
                                               pack_comp_qty,
                                               dept,
                                               class,
                                               subclass)
                                        values(I_cycle_count,
                                               L_loc_type,
                                               L_location,
                                               L_insert_item,
                                               NULL,
                                               NULL,
                                               NULL,
                                               NVL(L_unit_retail,0),
                                               'N',
                                               NULL,
                                               0,
                                               L_dept,
                                               L_class,
                                               L_subclass);

                  end if;
               END LOOP;
               ---
               if L_insert_flag = 'Y' then
                  L_insert_item := L_pack_holder;
                     insert into stake_sku_loc
                                              (cycle_count,
                                               loc_type,
                                               location,
                                               item,
                                               snapshot_on_hand_qty,
                                               snapshot_in_transit_qty,
                                               snapshot_unit_cost,
                                               snapshot_unit_retail,
                                               processed,
                                               physical_count_qty,
                                               pack_comp_qty,
                                               dept,
                                               class,
                                               subclass)
                                       values (I_cycle_count,
                                               L_loc_type,
                                               L_location,
                                               L_insert_item,
                                               NULL,
                                               NULL,
                                               NULL,
                                               NVL(L_unit_retail,0),
                                               'N',
                                               NULL,
                                               0,
                                               L_dept,
                                               L_class,
                                               L_subclass);
               end if;
            end if; -- if L_pack_ind
         end if; -- O_no_item_ind = 'N'
         close C_GET_ITEM_INFO;
      END LOOP; -- C_GET_ITEMS
      close C_GET_ITEMS;
   END LOOP; -- C_GET_LOCATIONS;
   close C_GET_LOCATIONS;

   if O_return_code != 'FALSE' then
      select count(1)
        into L_sku_loc_cnt
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and ROWNUM = 1;

      if L_sku_loc_cnt = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('STK_RQST_NO_SKU_LOCS',
                                               NULL,
                                               NULL,
                                               NULL);
         O_return_code := 'FALSE';
      else
         open C_NON_INVENTORY_EXISTS;
         fetch C_NON_INVENTORY_EXISTS into L_non_inventory_ind;
         if L_non_inventory_ind = 'x' then
            O_non_inv_ind := 'Y';
         end if;
         close C_NON_INVENTORY_EXISTS;
      end if;
   end if;


EXCEPTION
    when OTHERS then
                O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                               'STKCOUNT_SQL.STAKE_SKULIST_INSERT',
                                               TO_CHAR(SQLCODE));
                O_return_code := 'FALSE';
END STAKE_SKULIST_INSERT;

-------------------------------------------------------------------
-------------------------------------------------------------------
-- This procedure updates stake_head.delete_ind to 'Y'.  This marks
-- the cycle count for deletion in a batch program.
-------------------------------------------------------------------
PROCEDURE DESTROY (I_cycle_count     IN     NUMBER,
                   O_return_code     IN OUT VARCHAR2,
                   O_error_message   IN OUT VARCHAR2 ) IS
   -- Record locking variables
   L_table        VARCHAR2(30) := 'STAKE_HEAD';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_LOCK_STAKE_HEAD is
      select 'x'
        from stake_head
       where cycle_count = I_cycle_count
         for update of cycle_count nowait;
   --
BEGIN
   open  C_LOCK_STAKE_HEAD;
   close C_LOCK_STAKE_HEAD ;
   ---
   update stake_head
      set delete_ind = 'Y'
    where cycle_count = I_cycle_count;
   --
   O_return_code := 'TRUE';
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_STAKE_HEAD%ISOPEN then
         close C_LOCK_STAKE_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                          L_table,
                                          TO_CHAR(I_cycle_count));
      O_return_code   := 'FALSE';
   when OTHERS then
      O_return_code   := 'FALSE';
      O_error_message := SQLERRM;
END DESTROY;
-------------------------------------------------------------------
-- This procedure updates the status of the cycle count to processed.
-------------------------------------------------------------------
PROCEDURE CREATE_ADJUSTMENTS (I_cycle_count      IN     NUMBER,
                              I_loc_type         IN     VARCHAR2,
                              I_location         IN     NUMBER,
                              O_return_code      IN OUT VARCHAR2,
                              O_error_message    IN OUT VARCHAR2 ) IS
   L_table             VARCHAR2(30) := 'STAKE_PROD_LOC';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED,
                                      -54);
   cursor C_LOCK_STAKE_PROD_LOC is
      select 'x'
        from stake_prod_loc
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location
         for update nowait;
   cursor C_LOCK_STAKE_PROD_LOC_ALL is
      select 'x'
        from stake_prod_loc
       where cycle_count = I_cycle_count
         for update nowait;
BEGIN
   if I_loc_type is NULL or I_location is NULL then
      open  C_LOCK_STAKE_PROD_LOC_ALL;
      close C_LOCK_STAKE_PROD_LOC_ALL;
      update stake_prod_loc
         set processed   = 'P'
       where cycle_count = I_cycle_count;
   else
      open  C_LOCK_STAKE_PROD_LOC;
      close C_LOCK_STAKE_PROD_LOC;
      update stake_prod_loc
         set processed   = 'P'
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location;
   end if;
   O_return_code := 'TRUE';
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_STAKE_PROD_LOC%ISOPEN then
         close C_LOCK_STAKE_PROD_LOC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                          L_table,
                                          TO_CHAR(I_cycle_count),
                                          TO_CHAR(I_location));
      O_return_code   := 'FALSE';
   when OTHERS then
      O_return_code   := 'FALSE';
      O_error_message := SQLERRM;
END CREATE_ADJUSTMENTS;
-------------------------------------------------------------------
-- This procedure updates STAKE_PROD_LOC based on cycle_count, dept
-- and location. If no row is found for update it is inserted.
-------------------------------------------------------------------
PROCEDURE STAKE_PROD_LOC_UPDATE (I_cycle_count    IN     NUMBER,
                                 I_dept           IN     NUMBER,
                                 I_class          IN     NUMBER,
                                 I_subclass       IN     NUMBER,
                                 I_loc_type       IN     VARCHAR2,
                                 I_location       IN     NUMBER,
                                 I_adjust_cost    IN     NUMBER,
                                 I_adjust_retail  IN     NUMBER,
                                 I_at_cost        IN     NUMBER,
                                 I_at_retail      IN     NUMBER,
                                 O_return_code    IN OUT VARCHAR2,
                                 O_error_message  IN OUT VARCHAR2) IS
   L_table        VARCHAR2(30) := 'STAKE_PROD_LOC';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED,
                                 -54);
   -- Record locking cursors
   cursor C_LOCK_STAKE_PROD_LOC is
      select 'x'
        from stake_prod_loc
       where cycle_count = I_cycle_count
         and dept        = I_dept
         and class       = I_class
         and subclass    = I_subclass
         and loc_type    = I_loc_type
         and location    = I_location
         for update of cycle_count nowait;
BEGIN
   open  C_LOCK_STAKE_PROD_LOC;
   close C_LOCK_STAKE_PROD_LOC;
   update stake_prod_loc
      set total_cost    = I_at_cost,
          total_retail  = I_at_retail,
          adjust_cost   = I_adjust_cost,
          adjust_retail = I_adjust_retail
    where cycle_count   = I_cycle_count
      and dept          = I_dept
      and class         = I_class
      and subclass      = I_subclass
      and loc_type      = I_loc_type
      and location      = I_location;
   if SQL%NOTFOUND then
   --
   -- If record doesn't exist, insert a new record.
   --
      insert into stake_prod_loc(cycle_count,
                                 dept,
                                 class,
                                 subclass,
                                 loc_type,
                                 location,
                                 processed,
                                 total_cost,
                                 total_retail,
                                 adjust_cost,
                                 adjust_retail)
                          values(I_cycle_count,
                                 I_dept,
                                 I_class,
                                 I_subclass,
                                 I_loc_type,
                                 I_location,
                                 'N',
                                 I_at_cost,
                                 I_at_retail,
                                 I_adjust_cost,
                                 I_adjust_retail);
   end if;
   O_return_code := 'TRUE';
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_STAKE_PROD_LOC%ISOPEN then
         close C_LOCK_STAKE_PROD_LOC;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                          L_table,
                                          TO_CHAR(I_cycle_count),
                                          TO_CHAR(I_location));
      O_return_code   := 'FALSE';
   when OTHERS then
      O_return_code   := 'FALSE';
      O_error_message := SQLERRM;
END STAKE_PROD_LOC_UPDATE;
-------------------------------------------------------------------
FUNCTION VALIDATE_CYCLE_COUNT(I_cycle_count      IN     NUMBER,
                              I_stocktake_type   IN     VARCHAR2,
                              O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN IS
   L_delete_ind  VARCHAR2(1);
   cursor C_CYCLE_COUNT_EXISTS is
      select delete_ind
        from stake_head
       where cycle_count = I_cycle_count;
BEGIN
   open C_CYCLE_COUNT_EXISTS;
   fetch C_CYCLE_COUNT_EXISTS into L_delete_ind;
   if C_CYCLE_COUNT_EXISTS%NOTFOUND then
      O_error_message := 'INV_STKCOUNT';
      close C_CYCLE_COUNT_EXISTS;
      return FALSE;
   else
      if L_delete_ind   = 'Y' then
         O_error_message := 'STKCOUNT_DELETED';
         close C_CYCLE_COUNT_EXISTS;
         return FALSE;
      end if;
   end if;
   close C_CYCLE_COUNT_EXISTS;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.VALIDATE_CYCLE_COUNT.';
      return FALSE;
END VALIDATE_CYCLE_COUNT;
-----------------------------------------------------------------------------------------
FUNCTION CYCLE_COUNT_PROCESSED(I_cycle_count      IN     NUMBER,
                               O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN IS
   L_dummy VARCHAR2(1);
   cursor C_PROCESSED is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and processed   = 'P';
BEGIN
   open C_PROCESSED;
   fetch C_PROCESSED into L_dummy;
   if C_PROCESSED%FOUND then
      O_error_message := 'NO_DEL_STKCOUNT';
      close C_PROCESSED;
      return FALSE;
   end if;
   close C_PROCESSED;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.CYCLE_COUNT_PROCESSED.';
      return FALSE;
END CYCLE_COUNT_PROCESSED;
----------------------------------------------------------------------------
FUNCTION PRE_DELETES_STKCNT (I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_group_type      IN       VARCHAR2,
                             I_group           IN       VARCHAR2,
                             O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_loc_type   STAKE_QTY.LOC_TYPE%TYPE;
   L_loc_tbl    LOC_TBL  := LOC_TBL();
   
   cursor C_ALL_LOCATIONS is
      select sl.location
        from stake_location sl
       where sl.cycle_count = I_cycle_count;

   cursor C_STORE_IN_CLASS is
      select sl.location
        from store s,
             stake_location sl
       where s.store_class  = I_group
         and sl.location    = s.store
         and sl.cycle_count = I_cycle_count
         and sl.loc_type    = L_loc_type;

   cursor C_LOC_IN_LOCLIST_ST is
      select sl.location
        from loc_list_detail lld,
             stake_location sl
       where lld.loc_list   = I_group
         and lld.loc_type   = L_loc_type
         and sl.location    = lld.location
         and sl.cycle_count = I_cycle_count
         and sl.loc_type    = lld.loc_type;

   cursor C_LOC_IN_LOCLIST_WH is
      select distinct w2.physical_wh
        from loc_list_detail lld,
             wh w,
             wh w2,
             stake_location sl
       where lld.loc_list   = I_group
         and lld.loc_type   = 'W'
         and w.wh           = lld.location
         and w2.physical_wh = w.physical_wh
         and sl.location    = w2.wh
         and sl.cycle_count = I_cycle_count
         and sl.loc_type    = lld.loc_type;

BEGIN
   -- -------------------------------------------------------------------------
   -- Retrieve all Stake Locations - A stock count can only contain one location type
   -- -------------------------------------------------------------------------
   if I_group_type in ('AW', 'AS', 'AE') then
      if ( I_group_type = 'AW' ) then
         L_loc_type := 'W';
      elsif ( I_group_type = 'AS' ) then
         L_loc_type := 'S';
      elsif ( I_group_type = 'AE' ) then
         L_loc_type := 'E';
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_ALL_LOCATIONS',
                       'STAKE_LOCATION',
                       NULL);
      open  C_ALL_LOCATIONS;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_ALL_LOCATIONS',
                       'STAKE_LOCATION',
                       NULL);
      fetch C_ALL_LOCATIONS BULK COLLECT INTO L_loc_tbl;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ALL_LOCATIONS',
                       'STAKE_LOCATION',
                       NULL);
      close C_ALL_LOCATIONS;
   -- -------------------------------------------------------------------------
   -- Retrieve all Stake Locations in the Store Class
   -- -------------------------------------------------------------------------
   elsif ( I_group_type = 'C' ) then
      L_loc_type := 'S';
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_STORE_IN_CLASS',
                       'STAKE_LOCATION',
                       NULL);
      open  C_STORE_IN_CLASS;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_STORE_IN_CLASS',
                       'STAKE_LOCATION',
                       NULL);
      fetch C_STORE_IN_CLASS BULK COLLECT INTO L_loc_tbl;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_STORE_IN_CLASS',
                       'STAKE_LOCATION',
                       NULL);
      close C_STORE_IN_CLASS;
   -- -------------------------------------------------------------------------
   --  Retrieve all Stake Locations in the Store Location List
   -- -------------------------------------------------------------------------
   elsif ( I_group_type = 'LLS' ) then
      L_loc_type := 'S';
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOC_IN_LOCLIST_ST',
                       'STAKE_LOCATION',
                       NULL);
      open  C_LOC_IN_LOCLIST_ST;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOC_IN_LOCLIST_ST',
                       'STAKE_LOCATION',
                       NULL);
      fetch C_LOC_IN_LOCLIST_ST BULK COLLECT INTO L_loc_tbl;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOC_IN_LOCLIST_ST',
                       'STAKE_LOCATION',
                       NULL);
      close C_LOC_IN_LOCLIST_ST;
   -- -------------------------------------------------------------------------
   --  Retrieve all Stake Locations in the Warehouse Location List
   -- -------------------------------------------------------------------------
   elsif ( I_group_type = 'LLW' ) then
      L_loc_type := 'P';
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOC_IN_LOCLIST_WH',
                       'STAKE_LOCATION',
                       NULL);
      open  C_LOC_IN_LOCLIST_WH;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOC_IN_LOCLIST_WH',
                       'STAKE_LOCATION',
                       NULL);
      fetch C_LOC_IN_LOCLIST_WH BULK COLLECT INTO L_loc_tbl;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOC_IN_LOCLIST_WH',
                       'STAKE_LOCATION',
                       NULL);
      close C_LOC_IN_LOCLIST_WH;
   end if;
   -- -------------------------------------------------------------------------
   -- Now delete the locations
   -- -------------------------------------------------------------------------
   FOR i IN L_loc_tbl.first..L_loc_tbl.last LOOP
      if STKCOUNT_SQL.PRE_DELETES_STKCNT_LOC(I_cycle_count,
                                             L_loc_type,
                                             L_loc_tbl(i),
                                             O_error_message) = FALSE then
         return FALSE;
      end if;
   END LOOP;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            'STKCOUNT_SQL.PRE_DELETES_STKCNT',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PRE_DELETES_STKCNT;
--------------------------------------------------------------------------------
FUNCTION PRE_DELETES_STKCNT_LOC (I_cycle_count      IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                                 I_loc_type         IN     STAKE_QTY.LOC_TYPE%TYPE,
                                 I_location         IN     STAKE_QTY.LOCATION%TYPE,
                                 O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_processed          VARCHAR2(1):= NULL;
   L_table              VARCHAR2(30);
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);

   -- -------------------------------------------------------------------------
   -- Record locking cursors for Physical Warehouses
   -- -------------------------------------------------------------------------
   cursor C_LOCK_STAKE_QTY_WH is
      select 'x'
        from stake_qty sq
       where sq.cycle_count = I_cycle_count
         and sq.loc_type    = 'W'
         and sq.location in (select wh
                               from wh
                              where physical_wh = I_location)
         for update of cycle_count nowait;

   cursor C_LOCK_STAKE_SKU_LOC_WH is
      select 'x'
        from stake_sku_loc sl
       where sl.cycle_count = I_cycle_count
         and sl.loc_type    = 'W'
         and sl.location in (select wh
                               from wh
                              where physical_wh = I_location)
         for update of cycle_count nowait;

   cursor C_LOCK_STAKE_PROD_LOC_WH is
      select 'x'
        from stake_prod_loc sl
       where sl.cycle_count = I_cycle_count
         and sl.loc_type    = 'W'
         and sl.location in (select wh
                               from wh
                              where physical_wh = I_location)
         for update of cycle_count nowait;

   cursor C_LOCK_STAKE_LOCATION_WH is
      select 'x'
        from stake_location sl
       where sl.cycle_count = I_cycle_count
         and sl.loc_type    = 'W'
         and sl.location in (select wh
                               from wh
                              where physical_wh = I_location)
         for update of cycle_count nowait;

   cursor C_PROCESSED_WH is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and loc_type    = 'W'
         and location in (select wh
                            from wh
                           where physical_wh = I_location)
         and processed   = 'P'
         and rownum = 1;
   -- -------------------------------------------------------------------------
   -- Record locking cursors for Locations: Warehouses, Stores and External Finishers
   -- -------------------------------------------------------------------------
   cursor C_LOCK_STAKE_QTY is
      select 'x'
        from stake_qty sq
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location
         for update of cycle_count nowait;

   cursor C_LOCK_STAKE_SKU_LOC is
      select 'x'
        from stake_sku_loc sl
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location
         for update of cycle_count nowait;

   cursor C_LOCK_STAKE_PROD_LOC is
      select 'x'
        from stake_prod_loc sl
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location
         for update of cycle_count nowait;

   cursor C_LOCK_STAKE_LOCATION is
      select 'x'
        from stake_location sl
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location
         for update of cycle_count nowait;

   cursor C_PROCESSED is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location
         and processed   = 'P'
         and rownum = 1;

BEGIN
   -- -------------------------------------------------------------------------
   -- Delete a single Physical Warehouse
   -- -------------------------------------------------------------------------
   if ( I_loc_type = 'P' ) then
      -- ----------------------------------------------------------------------
      -- Can't delete from a stock count that has been processed
      -- ----------------------------------------------------------------------
      SQL_LIB.SET_MARK('OPEN',
                       'C_PROCESSED_WH',
                       'STAKE_SKU_LOC',
                       NULL);
      open C_PROCESSED_WH;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_PROCESSED_WH',
                       'STAKE_SKU_LOC',
                       NULL);
      fetch C_PROCESSED_WH into L_processed;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PROCESSED_WH',
                       'STAKE_SKU_LOC',
                       NULL);
      close C_PROCESSED_WH;
      
      if L_processed is NOT NULL then
         O_error_message := 'NO_DEL_STKCOUNT_LOC';
         return FALSE;
      end if;
      -- ----------------------------------------------------------------------
      -- STAKE_QTY -  Lock and delete records
      -- ----------------------------------------------------------------------
      L_table := 'STAKE_QTY';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STAKE_QTY_WH',
                       'STAKE_QTY',
                       NULL);
      open  C_LOCK_STAKE_QTY_WH;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STAKE_QTY_WH',
                       'STAKE_QTY',
                       NULL);
      close C_LOCK_STAKE_QTY_WH;
      ---
      delete
        from stake_qty sq
       where sq.cycle_count = I_cycle_count
         and sq.loc_type    = 'W'
         and sq.location in (select wh
                               from wh
                              where physical_wh = I_location);
      -- ----------------------------------------------------------------------
      -- STAKE_SKU_LOC -  Lock and delete records
      -- ----------------------------------------------------------------------
      L_table := 'STAKE_SKU_LOC';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STAKE_SKU_LOC_WH',
                       'STAKE_SKU_LOC',
                       NULL);
      open  C_LOCK_STAKE_SKU_LOC_WH;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STAKE_SKU_LOC_WH',
                       'STAKE_SKU_LOC',
                       NULL);
      close C_LOCK_STAKE_SKU_LOC_WH;
      ---
      delete
        from stake_sku_loc sl
       where sl.cycle_count = I_cycle_count
         and sl.loc_type    = 'W'
         and sl.location in (select wh
                               from wh
                              where physical_wh = I_location);
      -- ----------------------------------------------------------------------
      -- STAKE_PROD_LOC -  Lock and delete records
      -- ----------------------------------------------------------------------
      L_table := 'STAKE_PROD_LOC';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STAKE_PROD_LOC_WH',
                       'STAKE_PROD_LOC',
                       NULL);
      open  C_LOCK_STAKE_PROD_LOC_WH;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STAKE_PROD_LOC_WH',
                       'STAKE_PROD_LOC',
                       NULL);
      close C_LOCK_STAKE_PROD_LOC_WH;
      ---
      delete
        from stake_prod_loc sl
       where sl.cycle_count = I_cycle_count
         and sl.loc_type    = 'W'
         and sl.location in (select wh
                               from wh
                              where physical_wh = I_location);
      -- ----------------------------------------------------------------------
      -- STAKE_LOCATION -  Lock and delete records
      -- ----------------------------------------------------------------------
      L_table := 'STAKE_LOCATION';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STAKE_LOCATION_WH',
                       'STAKE_LOCATION',
                       NULL);
      open  C_LOCK_STAKE_LOCATION_WH;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STAKE_LOCATION_WH',
                       'STAKE_LOCATION',
                       NULL);
      close C_LOCK_STAKE_LOCATION_WH;
      ---
      delete
        from stake_location sl
       where sl.cycle_count = I_cycle_count
         and sl.loc_type    = 'W'
         and sl.location in (select wh
                               from wh
                              where physical_wh = I_location);
   -- -------------------------------------------------------------------------
   -- Delete A single Location: Warehouse, Store, External Finisher
   -- -------------------------------------------------------------------------
   else
      -- ----------------------------------------------------------------------
      -- Can't delete from a stock count that has been processed
      -- ----------------------------------------------------------------------
      SQL_LIB.SET_MARK('OPEN',
                       'C_PROCESSED',
                       'STAKE_SKU_LOC',
                       NULL);
      open C_PROCESSED;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_PROCESSED',
                       'STAKE_SKU_LOC',
                       NULL);
      fetch C_PROCESSED into L_processed;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PROCESSED',
                       'STAKE_SKU_LOC',
                       NULL);
      close C_PROCESSED;
      
      if L_processed is NOT NULL then
         O_error_message := 'NO_DEL_STKCOUNT_LOC';
         return FALSE;
      end if;

      -- ----------------------------------------------------------------------
      -- STAKE_QTY -  Lock and delete records
      -- ----------------------------------------------------------------------
      L_table := 'STAKE_QTY';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STAKE_QTY',
                       'STAKE_QTY',
                       NULL);
      open  C_LOCK_STAKE_QTY;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STAKE_QTY',
                       'STAKE_QTY',
                       NULL);
      close C_LOCK_STAKE_QTY;
      ---
      delete
        from stake_qty sq
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location;
      -- ----------------------------------------------------------------------
      -- STAKE_SKU_LOC -  Lock and delete records
      -- ----------------------------------------------------------------------
      L_table := 'STAKE_SKU_LOC';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STAKE_SKU_LOC',
                       'STAKE_SKU_LOC',
                       NULL);
      open  C_LOCK_STAKE_SKU_LOC;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STAKE_SKU_LOC',
                       'STAKE_SKU_LOC',
                       NULL);
      close C_LOCK_STAKE_SKU_LOC;
      ---
      delete
        from stake_sku_loc sl
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location;
      -- ----------------------------------------------------------------------
      -- STAKE_PROD_LOC -  Lock and delete records
      -- ----------------------------------------------------------------------
      L_table := 'STAKE_PROD_LOC';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STAKE_PROD_LOC',
                       'STAKE_PROD_LOC',
                       NULL);
      open  C_LOCK_STAKE_PROD_LOC;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STAKE_PROD_LOC',
                       'STAKE_PROD_LOC',
                       NULL);
      close C_LOCK_STAKE_PROD_LOC;
      ---
      delete
        from stake_prod_loc sl
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location;
      -- ----------------------------------------------------------------------
      -- STAKE_LOCATION -  Lock and delete records
      -- ----------------------------------------------------------------------
      L_table := 'STAKE_LOCATION';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STAKE_LOCATION',
                       'STAKE_LOCATION',
                       NULL);
      open  C_LOCK_STAKE_LOCATION;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STAKE_LOCATION',
                       'STAKE_LOCATION',
                       NULL);
      close C_LOCK_STAKE_LOCATION;
      ---
      delete
        from stake_location sl
       where cycle_count = I_cycle_count
         and loc_type    = I_loc_type
         and location    = I_location;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      if I_loc_type = 'P' then
         if C_LOCK_STAKE_QTY_WH%ISOPEN then
            close C_LOCK_STAKE_QTY_WH;
         end if;
         if C_LOCK_STAKE_SKU_LOC_WH%ISOPEN then
            close C_LOCK_STAKE_SKU_LOC_WH;
         end if;
         if C_LOCK_STAKE_PROD_LOC_WH%ISOPEN then
            close C_LOCK_STAKE_PROD_LOC_WH;
         end if;
         if C_LOCK_STAKE_LOCATION_WH%ISOPEN then
            close C_LOCK_STAKE_LOCATION_WH;
         end if;
      else
         if C_LOCK_STAKE_QTY%ISOPEN then
            close C_LOCK_STAKE_QTY;
         end if;
         if C_LOCK_STAKE_SKU_LOC%ISOPEN then
            close C_LOCK_STAKE_SKU_LOC;
         end if;
         if C_LOCK_STAKE_PROD_LOC%ISOPEN then
            close C_LOCK_STAKE_PROD_LOC;
         end if;
         if C_LOCK_STAKE_LOCATION%ISOPEN then
            close C_LOCK_STAKE_LOCATION;
         end if;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            TO_CHAR(I_cycle_count),
                                            TO_CHAR(I_location));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            'STKCOUNT_SQL.PRE_DELETES_STKCNT_LOC',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PRE_DELETES_STKCNT_LOC;
--------------------------------------------------------------------------------
FUNCTION CHK_STAKE_PROD_RECS(I_cycle_count    IN     NUMBER,
                             O_rec_exists     IN OUT VARCHAR2,
                             O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS
   L_exists   VARCHAR2(1);
   cursor C_CHK_STK_PROD is
      select 'x'
        from stake_product
       where cycle_count = I_cycle_count;
BEGIN
   open C_CHK_STK_PROD;
   fetch C_CHK_STK_PROD into L_exists;
   if C_CHK_STK_PROD%NOTFOUND then
      O_rec_exists := 'N';
   else
      O_rec_exists := 'Y';
   end if;
   close C_CHK_STK_PROD;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.CHK_STAKE_PROD_RECS.';
      return FALSE;
END CHK_STAKE_PROD_RECS;
--------------------------------------------------------------------------------
FUNCTION CHK_STAKE_LOC_RECS(I_cycle_count    IN     NUMBER,
                            O_rec_exists     IN OUT VARCHAR2,
                            O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS
   L_exists   VARCHAR2(1);
   cursor C_CHK_STK_LOC is
      select 'x'
        from stake_location
       where cycle_count = I_cycle_count;
BEGIN
   open C_CHK_STK_LOC;
   fetch C_CHK_STK_LOC into L_exists;
   if C_CHK_STK_LOC%NOTFOUND then
      O_rec_exists := 'N';
   else
      O_rec_exists := 'Y';
   end if;
   close C_CHK_STK_LOC;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.CHK_STAKE_LOC_RECS.';
      return FALSE;
END CHK_STAKE_LOC_RECS;
--------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_PHYSICAL_WH V_STORE V_WH
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION INSERT_STAKE_LOC (I_cycle_count     IN       NUMBER,
                           I_type            IN       VARCHAR2,
                           I_location        IN       VARCHAR2,
                           O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   return BOOLEAN IS

   L_loc_type           ITEM_LOC.LOC_TYPE%TYPE;
   L_location           ITEM_LOC.LOC%TYPE;
   L_first_fetch        BOOLEAN     := TRUE;
   L_exists             VARCHAR2(1) := NULL;
   L_valid              BOOLEAN;
   L_store              V_STORE%ROWTYPE;
   ---
   cursor C_STORE is
      select 'x'
        from stake_location
       where location    = L_location
         and loc_type    = 'S'
         and cycle_count = I_cycle_count;

   cursor C_WH is
      select 'x'
        from stake_location
       where location    = L_location
         and loc_type    = 'W'
         and cycle_count = I_cycle_count;

   cursor C_EXTERNAL_FINISHER is
      select 'x'
        from stake_location
       where location    = L_location
         and loc_type    = 'E'
         and cycle_count = I_cycle_count;

   cursor C_STORE_CLASS is
      select store
        from v_store vs
       where store_class = I_location
         and stockholding_ind = 'Y'
         and not exists (select 'X'
                           from stake_location sl
                          where sl.location = vs.store
                            and cycle_count = I_cycle_count);

   cursor C_ALL_STORE is
      select store
        from v_store vs
       where stockholding_ind = 'Y'
         and not exists (select 'X'
                           from stake_location sl
                          where sl.location = vs.store
                            and cycle_count = I_cycle_count);

   cursor C_ALL_WH is
      select wh
        from v_wh vw
       where stockholding_ind = 'Y'
         and not exists (select 'x'
                           from stake_location sl
                          where sl.location = vw.wh
                            and cycle_count = I_cycle_count);

   cursor C_ALL_PHYSICAL_WH is
      select wh.wh
        from v_physical_wh vpw, wh
       where wh.physical_wh = vpw.wh
         and wh.stockholding_ind = 'Y'
         and not exists (select 'x'
                           from stake_location sl
                          where sl.location = wh.wh
                            and cycle_count = I_cycle_count);

    cursor C_ALL_EXT_FINISHERS is
      select TO_NUMBER(vef.finisher_id)
        from v_external_finisher vef
       where not exists (select 'X'
                           from stake_location sl
                          where sl.location = TO_NUMBER(vef.finisher_id)
                            and cycle_count = I_cycle_count);

   cursor C_GET_STORE_LIST is
      select ld.location
        from loc_list_detail ld,
             v_store vs
       where ld.loc_list = TO_NUMBER(I_location)
         and ld.location = vs.store
         and ld.loc_type = 'S'
         and vs.stockholding_ind = 'Y'
         and not exists (select 'X'
                           from stake_location sl
                          where sl.location = ld.location
                            and ld.loc_type = 'S'
                            and cycle_count = I_cycle_count);

   cursor C_GET_WH_LIST is
      select ld.location
        from loc_list_detail ld,
             v_wh vw
       where ld.loc_list = TO_NUMBER(I_location)
         and ld.location = wh
         and ld.loc_type = 'W'
         and exists (select 'X'
                             from wh w, wh w2
                            where w.wh = ld.location
                              and (w2.physical_wh    = w.wh
                                   or w2.physical_wh = w.physical_wh)
                              and w2.stockholding_ind = 'Y'
                              and not exists (select 'x'
                                                from stake_location sl
                                               where sl.location = w2.wh
                                                 and cycle_count = I_cycle_count));

BEGIN
   if I_type != 'C' then
      if sql_lib.check_numeric(O_error_message,
                               I_location) = FALSE then
         return FALSE;
      end if;
   end if;

   ---
   if I_type = 'S' then
      /* store */
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(O_error_message,
                                                L_valid,
                                                L_store,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_location) = FALSE then
         return FALSE;
      end if;

      ---
      if L_valid = TRUE and (L_store.store_type = 'C' or (L_store.store_type = 'F' and L_store.stockholding_ind = 'Y')) then
         L_location := TO_NUMBER(I_location);
         open C_STORE;
         fetch C_STORE into L_exists;
         if C_STORE%NOTFOUND then
            insert into stake_location(cycle_count,
                                       loc_type,
                                       location)
                                values(I_cycle_count,
                                       'S',
                                       L_location);
         end if;
         close C_STORE;
      end if;
   ---
   elsif I_type = 'W' then
      /* warehouse */
      L_location := TO_NUMBER(I_location);
      insert into stake_location(cycle_count,
                                 loc_type,
                                 location)
                          select I_cycle_count,
                                 'W',
                                 wh
                            from wh
                           where physical_wh      = L_location
                             and stockholding_ind = 'Y'
                             and not exists (select 'x'
                                               from stake_location sl
                                              where sl.location = wh.wh
                                                and cycle_count = I_cycle_count);
   ---
   elsif I_type = 'E' then
      /* external finisher */
      L_location := TO_NUMBER(I_location);
      open C_EXTERNAL_FINISHER;
      fetch C_EXTERNAL_FINISHER into L_exists;
      if C_EXTERNAL_FINISHER%NOTFOUND then
         insert into stake_location(cycle_count,
                                    loc_type,
                                    location)
                             values(I_cycle_count,
                                    'E',
                                    L_location);
      end if;
      close C_EXTERNAL_FINISHER;
   ---
   elsif I_type = 'C' then
      /* store class */
      open C_STORE_CLASS;
      LOOP
         fetch C_STORE_CLASS into L_location;
         if C_STORE_CLASS%NOTFOUND then
            if L_first_fetch then
               O_error_message := 'LL_NO_STORES';
               close C_STORE_CLASS;
               return FALSE;
            else
               EXIT;
            end if;
         end if;
         L_first_fetch := FALSE;
         insert into stake_location(cycle_count,
                                    loc_type,
                                    location)
                             values(I_cycle_count,
                                    'S',
                                    L_location);
      END LOOP;
      close C_STORE_CLASS;
   ---
   elsif I_type = 'AS' then
      /* all stores */
      ---
      open C_ALL_STORE;
      LOOP
         fetch C_ALL_STORE into L_location;
         if C_ALL_STORE%NOTFOUND then
            if L_first_fetch then
               O_error_message := 'LL_NO_STORES';
               close C_ALL_STORE;
               return FALSE;
            else
               EXIT;
            end if;
         end if;
         L_first_fetch := FALSE;
         insert into stake_location(cycle_count,
                                    loc_type,
                                    location)
                             values(I_cycle_count,
                                    'S',
                                    L_location);
      END LOOP;
      close C_ALL_STORE;
   ---
   elsif I_type = 'AW' then
      /* all warehouses */
      open C_ALL_PHYSICAL_WH;
      LOOP
         fetch C_ALL_PHYSICAL_WH into L_location;
         if C_ALL_PHYSICAL_WH%NOTFOUND then
            if L_first_fetch then
               O_error_message := 'LL_NO_WAREHOUSES';
               close C_ALL_PHYSICAL_WH;
               return FALSE;
            else
               EXIT;
            end if;
         end if;
         L_first_fetch := FALSE;
         insert into stake_location(cycle_count,
                                    loc_type,
                                    location)
                             values(I_cycle_count,
                                    'W',
                                    L_location);
      END LOOP;
      close C_ALL_PHYSICAL_WH;
   ---
   elsif I_type = 'AE' then
      /* all external finishers */
      open C_ALL_EXT_FINISHERS;
      LOOP
         fetch C_ALL_EXT_FINISHERS into L_location;
         if C_ALL_EXT_FINISHERS%NOTFOUND then
            if L_first_fetch then
               O_error_message := 'LL_NO_WAREHOUSES';
               close C_ALL_EXT_FINISHERS;
               return FALSE;
            else
               EXIT;
            end if;
         end if;
         L_first_fetch := FALSE;
         insert into stake_location(cycle_count,
                                    loc_type,
                                    location)
                             values(I_cycle_count,
                                    'E',
                                    L_location);
      END LOOP;
      close C_ALL_EXT_FINISHERS;
   ---
   elsif I_type = 'LLS' then
      /* all stores on location list */
      open C_GET_STORE_LIST;
      LOOP
         fetch C_GET_STORE_LIST into L_location;
         if C_GET_STORE_LIST%NOTFOUND then
            if L_first_fetch then
               O_error_message := 'LL_NO_STORES';
               close C_GET_STORE_LIST;
               return FALSE;
            else
               EXIT;
            end if;
         end if;
         L_first_fetch := FALSE;
         open C_STORE;
         fetch C_STORE into L_exists;
         if C_STORE%NOTFOUND then
            insert into stake_location(cycle_count,
                                       loc_type,
                                       location)
                                values(I_cycle_count,
                                       'S',
                                       L_location);
         end if;
         close C_STORE;
      END LOOP;
      close C_GET_STORE_LIST;
   ---
   elsif I_type = 'LLW' then
      open C_GET_WH_LIST;
      LOOP
         fetch C_GET_WH_LIST into L_location;
         if C_GET_WH_LIST%NOTFOUND then
            if L_first_fetch then
               O_error_message := 'LL_NO_WAREHOUSES';
               close C_GET_WH_LIST;
               return FALSE;
            else
               EXIT;
            end if;
         end if;
         L_first_fetch := FALSE;
         insert into stake_location(cycle_count,
                                    loc_type,
                                    location)
                    select distinct I_cycle_count,
                                    'W',
                                    w2.wh
                               from wh w,
                                    wh w2
                              where w.wh = L_location
                                and (w2.physical_wh    = w.wh
                                     or w2.physical_wh = w.physical_wh)
                                and w2.stockholding_ind = 'Y'
                                and not exists (select 'x'
                                                  from stake_location sl
                                                 where sl.location    = w2.wh
                                                   and cycle_count = I_cycle_count);
      END LOOP;
      close C_GET_WH_LIST;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.INSERT_STAKE_LOC.';
      return FALSE;
END INSERT_STAKE_LOC;
-----------------------------------------------------------------------------
FUNCTION MIN_DATE
RETURN DATE IS
   L_vdate PERIOD.VDATE%TYPE   := GET_VDATE;
   L_min_date DATE;
   L_stake_lockout_days SYSTEM_OPTIONS.STAKE_LOCKOUT_DAYS%TYPE;
   cursor C_STAKE_LOCKOUT_DAYS IS
      select stake_lockout_days
        from system_options;
BEGIN
   open C_STAKE_LOCKOUT_DAYS;
   fetch C_STAKE_LOCKOUT_DAYS into L_stake_lockout_days;
   close C_STAKE_LOCKOUT_DAYS;
   L_min_date := L_vdate + L_stake_lockout_days;
   return (L_min_date);
END MIN_DATE;
-----------------------------------------------------------------------------
FUNCTION LOCKOUT_DAYS
RETURN NUMBER IS
   L_days   SYSTEM_OPTIONS.STAKE_LOCKOUT_DAYS%TYPE;
   cursor C_STAKE_LOCKOUT_DAYS is
      select stake_lockout_days
        from system_options;
BEGIN
   open C_STAKE_LOCKOUT_DAYS;
   fetch C_STAKE_LOCKOUT_DAYS into L_days;
   close C_STAKE_LOCKOUT_DAYS;
   return (L_days);
END LOCKOUT_DAYS;
------------------------------------------------------------------------------
FUNCTION VALIDATE_DEPT(I_dept             IN     STAKE_PROD_LOC.DEPT%TYPE,
                       I_cycle_count      IN     STAKE_PROD_LOC.CYCLE_COUNT%TYPE,
                       I_loc              IN     STAKE_PROD_LOC.LOCATION%TYPE,
                       O_dept_desc        IN OUT DEPS.DEPT_NAME%TYPE,
                       O_profit_calc_type IN OUT DEPS.PROFIT_CALC_TYPE%TYPE,
                       O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE) RETURN BOOLEAN IS
   L_profit_calc_type  DEPS.PROFIT_CALC_TYPE%TYPE;
   cursor C_DEPT_NAME is
     select v_deps_tl.dept_name,
            deps.profit_calc_type
       from stake_product,
            deps,
            v_deps_tl
      where stake_product.cycle_count = I_cycle_count
        and deps.dept                 = stake_product.dept
        and deps.dept                 = I_dept
        and deps.dept                 = v_deps_tl.dept
        and stake_product.dept not in
                (select dept
                   from stake_prod_loc
                  where cycle_count = I_cycle_count
                    and location = I_loc
                    and processed in ('P','S'));
BEGIN
   open C_DEPT_NAME;
   fetch C_DEPT_NAME into O_dept_desc,
                          O_profit_calc_type;
     if C_DEPT_NAME%NOTFOUND then
         close C_DEPT_NAME;
         return FALSE;
     else
         close C_DEPT_NAME;
         return TRUE;
     end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.VALIDATE_DEPT';
      return FALSE;
END VALIDATE_DEPT;
------------------------------------------------------------------------------
FUNCTION VALIDATE_CLASS(I_class            IN     STAKE_PROD_LOC.CLASS%TYPE,
                        I_dept             IN     STAKE_PROD_LOC.DEPT%TYPE,
                        I_cycle_count      IN     STAKE_PROD_LOC.CYCLE_COUNT%TYPE,
                        I_prod             IN     STAKE_HEAD.PRODUCT_LEVEL_IND%TYPE,
                        O_class_name       IN OUT CLASS.CLASS_NAME%TYPE,
                        O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_class  stake_prod_loc.class%TYPE;

   cursor C_CLASS is
       select distinct c.class_name,
              c.class
         from v_class_tl c,
              stake_product sp
        where c.class        = nvl(sp.class,
                                   c.class)
          and c.class        = I_class
          and c.dept         = sp.dept
          and c.dept         = I_dept
          and sp.cycle_count = I_cycle_count
          and I_prod in ('C','S')
        union all
       select class_name,
              class
         from v_class_tl
        where dept   = I_dept
          and class  = I_class
          and I_prod = 'D'
        order by 1;
BEGIN
     open C_CLASS;
     fetch C_CLASS into O_class_name,
                        L_class;
        if C_CLASS%NOTFOUND then
           close C_CLASS;
           return FALSE;
        else
           close C_CLASS;
           return TRUE;
        end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.VALIDATE_CLASS';
      return FALSE;
END VALIDATE_CLASS;
------------------------------------------------------------------------------
FUNCTION VALIDATE_SUBCLASS(I_subclass            IN     STAKE_PROD_LOC.SUBCLASS%TYPE,
                           I_class               IN     STAKE_PROD_LOC.CLASS%TYPE,
                           I_dept                IN     STAKE_PROD_LOC.DEPT%TYPE,
                           I_cycle_count         IN     STAKE_PROD_LOC.CYCLE_COUNT%TYPE,
                           I_prod                IN     STAKE_HEAD.PRODUCT_LEVEL_IND%TYPE,
                           O_subclass_name       IN OUT CLASS.CLASS_NAME%TYPE,
                           O_error_message       IN OUT VARCHAR2)
RETURN BOOLEAN IS
   L_subclass   stake_prod_loc.subclass%TYPE;
   cursor C_SUBCLASS is
   select distinct s.sub_name,
          s.subclass
     from stake_product sp,
          v_subclass_tl s
    where s.dept         = sp.dept
      and s.subclass     = I_subclass
      and s.class        = sp.class
      and s.dept         = I_dept
      and s.class        = I_class
      and s.subclass     = nvl(sp.subclass,
                               s.subclass)
      and sp.cycle_count = I_cycle_count
      and I_prod = 'S'
    union all
   select sub_name,
          subclass
     from v_subclass_tl
    where dept     = I_dept
      and class    = I_class
      and subclass = I_subclass
      and I_prod in ('D','C')
    order by 1;
BEGIN
   open C_SUBCLASS;
   fetch C_SUBCLASS into O_subclass_name,
                         L_subclass;
   if C_SUBCLASS%FOUND then
      close C_SUBCLASS;
      return TRUE;
   elsif C_SUBCLASS%NOTFOUND then
      close C_SUBCLASS;
      return FALSE;
   end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.VALIDATE_SUBCLASS';
      return FALSE;
END VALIDATE_SUBCLASS;
------------------------------------------------------------------------------
FUNCTION RETRIEVE_VALUES(I_subclass            IN     STAKE_PROD_LOC.SUBCLASS%TYPE,
                         I_class               IN     STAKE_PROD_LOC.CLASS%TYPE,
                         I_dept                IN     STAKE_PROD_LOC.DEPT%TYPE,
                         I_loc_type            IN     STAKE_PROD_LOC.LOC_TYPE%TYPE,
                         I_location            IN     STAKE_PROD_LOC.LOCATION%TYPE,
                         I_cycle_count         IN     STAKE_PROD_LOC.CYCLE_COUNT%TYPE,
                         O_cost                IN OUT STAKE_PROD_LOC.TOTAL_COST%TYPE,
                         O_retail              IN OUT STAKE_PROD_LOC.TOTAL_RETAIL%TYPE,
                         O_adj_cost            IN OUT STAKE_PROD_LOC.ADJUST_COST%TYPE,
                         O_adj_retail          IN OUT STAKE_PROD_LOC.ADJUST_RETAIL%TYPE,
                         O_error_message       IN OUT VARCHAR2)
RETURN BOOLEAN IS
   cursor C_AMOUNT is
      select spl.total_cost,
             spl.total_retail,
             spl.adjust_cost,
             spl.adjust_retail
        from stake_prod_loc spl,
             deps d
       where spl.subclass    = I_subclass
         and spl.loc_type    = I_loc_type
         and spl.location    = I_location
         and spl.cycle_count = I_cycle_count
         and spl.dept        = d.dept
         and d.dept          = I_dept
         and spl.class       = I_class;
BEGIN
   open C_AMOUNT;
   fetch C_AMOUNT into O_cost,
                       O_retail,
                       O_adj_cost,
                       O_adj_retail;
     if C_AMOUNT%NOTFOUND then
        close C_AMOUNT;
        O_error_message := 'INVALID_LOC_DEPT';
        return FALSE;
     end if;
     close C_AMOUNT;
     return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM||' from STKCOUNT_SQL.RETRIEVE_VALUES';
      return FALSE;
END RETRIEVE_VALUES;
------------------------------------------------------------------------------
FUNCTION INSERT_HEAD(O_error_message    IN OUT VARCHAR2,
                     I_stock_count      IN     STAKE_HEAD.CYCLE_COUNT%TYPE,
                     I_stock_count_desc IN     STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                     I_loc_type         IN     STAKE_HEAD.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
   L_exists    VARCHAR2(1) := 'N';
   ---
   cursor C_EXISTS IS
      select 'Y'
        from stake_head
       where cycle_count = I_stock_count;
BEGIN
   if I_stock_count is NULL or I_stock_count_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_EXISTS;
   fetch C_EXISTS into L_exists;
   close C_EXISTS;
   ---
   if L_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('STOCK_COUNT_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc_type not in ('S','W','E') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   insert into stake_head(cycle_count,
                          cycle_count_desc,
                          loc_type,
                          stocktake_date,
                          stocktake_type,
                          product_level_ind,
                          delete_ind)
                   values(I_stock_count,
                          I_stock_count_desc,
                          I_loc_type,
                          GET_VDATE,
                          'U',
                          'D',
                          'N');
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            'STKCOUNT_SQL.INSERT_UPDATE_SKU_LOC',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_HEAD;
------------------------------------------------------------------------------
FUNCTION CALC_TOT_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_tot_qty         IN OUT   STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE,
                      I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                      I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                      I_item            IN       STAKE_SKU_LOC.ITEM%TYPE)
RETURN BOOLEAN IS

   L_pack_ind          ITEM_MASTER.PACK_IND%TYPE;
   L_dummy_ord         ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_dummy_sell        ITEM_MASTER.SELLABLE_IND%TYPE;
   L_dummy_type        ITEM_MASTER.PACK_TYPE%TYPE;

   cursor C_SUM_QTY is
      select SUM(NVL(qty, 0))
        from stake_qty sq,
             wh w
       where sq.item        = I_item
         and sq.cycle_count = I_cycle_count
         and sq.loc_type    = 'W'
         and w.wh           = sq.location
         and w.physical_wh  = I_location;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUM_QTY',
                    'STKCOUNT_SQL',
                    NULL);
   open C_SUM_QTY;
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUM_QTY',
                    'STKCOUNT_SQL',
                    NULL);
   fetch C_SUM_QTY into O_tot_qty;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUM_QTY',
                    'STKCOUNT_SQL',
                    NULL);
   close C_SUM_QTY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            'STKCOUNT_SQL.CALC_TOT_QTY',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_TOT_QTY;
-------------------------------------------------------------------------------------
FUNCTION DELETE_STAKE_QTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cycle_count   IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                          I_location      IN     STAKE_QTY.LOCATION%TYPE,
                          I_item          IN     STAKE_QTY.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_LOCK_STAKE_QTY is
      select 'x'
        from stake_qty
       where cycle_count = I_cycle_count
         and item        = I_item
         and location    = I_location
         and loc_type    = 'W'
         for UPDATE NOWAIT;

   L_table         VARCHAR2(30) := 'STAKE_QTY';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,
                                  -54);

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_STAKE_QTY',
                    'STKCOUNT_SQL',
                    NULL);
   open C_LOCK_STAKE_QTY;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_STAKE_QTY',
                    'STKCOUNT_SQL',
                    NULL);
   close C_LOCK_STAKE_QTY;
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'STAKE_QTY',
                    'Warehouse: '||I_location);
   delete
     from stake_qty
    where cycle_count = I_cycle_count
      and item        = I_item
      and location    = I_location
      and loc_type    = 'W';

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_location,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            'STKCOUNT_SQL.DELETE_STAKE_QTY',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_STAKE_QTY;
---------------------------------------------------------------------------------------
FUNCTION PWH_EXISTS_FOR_CYCLE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_pwh_name      IN OUT WH.WH_NAME%TYPE,
                              I_cycle_count   IN     STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                              I_pwh           IN     WH.WH%TYPE,
                              I_item          IN     STAKE_QTY.ITEM%TYPE)
RETURN BOOLEAN IS

   L_flag       BOOLEAN;
   L_dummy      VARCHAR2(1);
   L_program    VARCHAR2(64) := 'STKCOUNT_SQL.PWH_EXISTS_FOR_CYCLE';

   cursor C_EXISTS is
      select 'x'
        from stake_sku_loc s,
             wh w
       where s.item        = nvl(I_item,
                                 s.item)
         and s.cycle_count = I_cycle_count
         and s.location    = w.wh
         and s.loc_type    = 'W'
         and w.physical_wh = I_pwh;

BEGIN

   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CYCLE_COUNT',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_pwh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PWH',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   --if I_item is NULL then
   --   O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
   --                                         NULL,
   --                                         NULL,
   --                                         NULL);
   --   return FALSE;
   --end if;
   ---
   if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                              L_flag,
                              I_pwh) = FALSE then
      return false;
   end if;
   ---
   if L_flag = TRUE then
      SQL_LIB.SET_MARK('OPEN',
                       'C_EXISTS',
                       'STAKE_SKU_LOC',
                       'cycle_count = '||TO_CHAR(I_cycle_count));
      open C_EXISTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_EXISTS',
                       'STAKE_SKU_LOC',
                       'cycle_count = '||TO_CHAR(I_cycle_count));
      fetch C_EXISTS into L_dummy;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS',
                       'STAKE_SKU_LOC',
                       'cycle_count = '||TO_CHAR(I_cycle_count));
      close C_EXISTS;

      if L_dummy is NOT NULL then
         ---

         if WH_ATTRIB_SQL.GET_PWH_NAME(O_error_message,
                                       I_pwh,
                                       O_pwh_name) = FALSE then
            return false;
         end if;
      else
         O_pwh_name := NULL;
      end if;
      ---
      return TRUE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PWH',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PWH_EXISTS_FOR_CYCLE;
-------------------------------------------------------------------------------------
FUNCTION ITEM_ASSOC_CYCLE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item_desc     IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                          I_cycle_count   IN     STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                          I_item          IN     STAKE_QTY.ITEM%TYPE,
                          I_pwh           IN     WH.WH%TYPE)
RETURN BOOLEAN IS

   L_error_message      VARCHAR2(250);
   L_dummy              VARCHAR2(1);
   L_program            VARCHAR2(64) := 'STKCOUNT_SQL.ITEM_ASSOC_CYCLE';

   cursor C_exists is
      select 'x'
        from stake_sku_loc s,
             wh w
       where s.item        = I_item
         and s.cycle_count = I_cycle_count
         and s.location    = w.wh
         and s.loc_type    = 'W'
         and w.physical_wh = nvl(I_pwh,
                                 w.physical_wh);
BEGIN

   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CYCLE_COUNT',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'STAKE_SKU_LOC',
                    'cycle_count = '||to_char(I_cycle_count));
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'STAKE_SKU_LOC',
                    'cycle_count = '||to_char(I_cycle_count));
   fetch C_EXISTS into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    'STAKE_SKU_LOC',
                    'cycle_count = '||to_char(I_cycle_count));
   close C_EXISTS;
   ---
   if L_dummy is NOT NULL then
      if ITEM_ATTRIB_SQL.GET_DESC(L_error_message,
                                  O_item_desc,
                                  I_item) = FALSE then
         return false;
      end if;
   else
      O_item_desc := NULL;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END ITEM_ASSOC_CYCLE;
-------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_RECLASSIFICATION(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists         IN OUT  BOOLEAN,
                                     O_item           IN OUT  ITEM_MASTER.ITEM%TYPE,
                                     I_stocktake_type IN      VARCHAR2,
                                     I_item_list      IN      SKULIST_DETAIL.SKULIST%TYPE,
                                     I_stocktake_date IN      PERIOD.VDATE%TYPE,
                                     I_dept           IN      ITEM_MASTER.DEPT%TYPE,
                                     I_class          IN      ITEM_MASTER.CLASS%TYPE,
                                     I_subclass       IN      ITEM_MASTER.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_lockout_days   SYSTEM_OPTIONS.STAKE_LOCKOUT_DAYS%TYPE;

   cursor C_STK_LOCKOUT_DAYS is
      select stake_lockout_days
        from system_options;

   cursor C_RECLASS_CHK_LIST is
      select ri.item
        from reclass_item ri,
             reclass_head rh
       where rh.reclass_no    = ri.reclass_no
         and rh.reclass_date >= I_stocktake_date - L_lockout_days
         and EXISTS (select 'x'
                       from skulist_detail sd,
                            item_master im
                      where sd.skulist               = I_item_list
                        and sd.item                  = ri.item
                        and (im.item                 = sd.item
                              or im.item_parent      = sd.item
                              or im.item_grandparent = sd.item)
                             and im.item_level       = im.tran_level);

cursor C_RECLASS_CHK_ITEM is
      select ri.item
        from reclass_item ri,
             reclass_head rh
       where rh.reclass_no    = ri.reclass_no
         and rh.reclass_date >= I_stocktake_date - L_lockout_days
         and EXISTS (select 'x'
                        from item_master im
                       where im.item = ri.item
                         and im.dept = I_dept
                         and (I_class is NULL
                              or (im.class = I_class
                                  and (I_subclass is NULL
                                       or im.subclass = I_subclass))));

BEGIN
      open C_STK_LOCKOUT_DAYS;
      fetch C_STK_LOCKOUT_DAYS into L_lockout_days;
      close C_STK_LOCKOUT_DAYS;

   if I_stocktake_type = 'U' then
      open C_RECLASS_CHK_LIST;
      fetch C_RECLASS_CHK_LIST into O_item;
      if C_RECLASS_CHK_LIST%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      close C_RECLASS_CHK_LIST;
   elsif I_stocktake_type = 'B' then
      open C_RECLASS_CHK_ITEM;
      fetch C_RECLASS_CHK_ITEM into O_item;
      if C_RECLASS_CHK_ITEM%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      close C_RECLASS_CHK_ITEM;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STKCOUNT_SQL.CHECK_ITEM_RECLASSIFICATION',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_ITEM_RECLASSIFICATION;
-------------------------------------------------------------------------------------
FUNCTION STOCK_DISTRIBUTION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                            I_physical_wh     IN       WH.WH%TYPE,
                            I_item            IN       STAKE_QTY.ITEM%TYPE,
                            I_count_loc_qty   IN       STAKE_QTY.QTY%TYPE,
                            I_count_loc_desc  IN       STAKE_QTY.LOCATION_DESC%TYPE)
RETURN BOOLEAN IS

   i                           INTEGER;
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE                := 'TEST';
   L_dist_table                DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_db_qty                    STAKE_SKU_LOC.SNAPSHOT_ON_HAND_QTY%TYPE := 0;
   L_cur_qty                   STAKE_SKU_LOC.SNAPSHOT_ON_HAND_QTY%TYPE := 0;
   L_upd_qty                   STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE   := 0;
   L_qty                       NUMBER(12,4)                            := 0;
   L_failed                    NUMBER(1)                               := 0;
   L_found                     BOOLEAN;
   L_dist_qty                  STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE;
   L_ssl_dist_qty              STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE;
   L_dist_wh                   STAKE_SKU_LOC.LOCATION%TYPE;
   L_snapshot_on_hand_qty      STAKE_SKU_LOC.SNAPSHOT_ON_HAND_QTY%TYPE;
   L_sum_snapshot_on_hand_qty  STAKE_SKU_LOC.SNAPSHOT_ON_HAND_QTY%TYPE;
   L_stake_qty                 STAKE_QTY.QTY%TYPE;
   L_vwhs_exist                VARCHAR2(1)                             := NULL;
   L_exist                     VARCHAR2(1)                             := NULL;
   L_pack_ind                  VARCHAR2(1)                             := NULL;
   L_pack_comp                 VARCHAR2(1);
   L_rowid                     ROWID;

   cursor C_UPDATE_STAKE_QTY is
      select qty,
             ROWID
        from stake_qty
       where cycle_count   = I_cycle_count
         and item          = I_item
         and location      = L_dist_wh
         and location_desc = I_count_loc_desc
         for update of qty NOWAIT;

   cursor C_UPDATE_STAKE_SKU_LOC is
      select snapshot_on_hand_qty
        from stake_sku_loc
       where cycle_count   = I_cycle_count
         and item          = I_item
         and location      = L_dist_wh
         for update of physical_count_qty nowait;

   cursor C_TOTAL_SNAPSHOT_ON_HAND_QTY IS
      select NVL(SUM(NVL(snapshot_on_hand_qty,0)),0)
        from stake_sku_loc ssl,
             wh w
       where ssl.cycle_count = I_cycle_count
         and ssl.item        = I_item
         and ssl.location    = w.wh
         and w.wh           != w.physical_wh
         and w.physical_wh   = I_physical_wh;

   cursor C_CHECK_VWHS_EXIST is
      select 'x'
        from item_loc il,
             wh
       where il.item        = I_item
         and il.loc         = wh.wh
         and wh.physical_wh = I_physical_wh
         and wh.wh         != wh.physical_wh;

    cursor C_GET_PACKITEMS is
       select item,
              qty
         from v_packsku_qty
        where pack_no = I_item;

   cursor C_GET_PACK_IND is
      select pack_ind
        from item_master
       where item = I_item;

   cursor G_GET_QTY_TOTAL is
      select NVL(SUM(NVL(sq.qty, 0)), 0)
        from stake_qty sq,
             wh
       where sq.cycle_count = I_cycle_count
         and sq.loc_type = 'W'
         and sq.item = I_item
         and sq.location_desc <> I_count_loc_desc
         and wh.wh = sq.location
         and wh.physical_wh = I_physical_wh;

   cursor G_GET_LOC_TOTAL( cv_dist_wh  WH.WH%TYPE) is 
      select NVL(SUM(NVL(sq.qty, 0)), 0)
        from stake_qty sq
       where sq.cycle_count = I_cycle_count
         and sq.loc_type = 'W'
         and sq.location = cv_dist_wh
         and sq.item = I_item
         and sq.location_desc <> I_count_loc_desc;

   cursor C_SNAPSHOT_ON_HAND_QTY IS
      select ssl.location,
             ssl.snapshot_on_hand_qty
        from stake_sku_loc ssl,
             wh w
       where ssl.cycle_count = I_cycle_count
         and ssl.item        = I_item
         and ssl.location    = w.wh
         and ssl.loc_type    = 'W'
         and w.wh           != w.physical_wh
         and w.physical_wh   = I_physical_wh;

BEGIN

   open C_CHECK_VWHS_EXIST;
   fetch C_CHECK_VWHS_EXIST into L_vwhs_exist;
   close C_CHECK_VWHS_EXIST;

   if L_vwhs_exist is NULL then
      if STKCOUNT_SQL.NEW_STKCNT_ITEM_LOC(O_error_message,
                                          I_cycle_count,
                                          I_physical_wh,
                                          'W',
                                          I_item,
                                          I_count_loc_qty) = FALSE then
         return FALSE;
      end if;
      -- ----------------------------------------------------------------------
      -- return true here as the item location relationship was just built by 
      -- the function called above, for a single virtual warehouse. So 
      -- distribution logic does not need to be applied as the new location 
      -- was built with no stock on hand so the physical count quantity will 
      -- applied towards this single location.
      -- ----------------------------------------------------------------------
      return TRUE;
   end if;

   --  Is the item a Pack?
   open C_GET_PACK_IND;
   fetch C_GET_PACK_IND into L_pack_ind;
   close C_GET_PACK_IND;
   -- -------------------------------------------------------------------------
   -- Get the total for the stock count excluding the current location
   -- current location will contain values if this is an edit
   -- we want to distribute the count total to remove rounding inconsistancies
   -- Example:  3 virtual warehouses  distribute evenly
   --    Count of 10 is distributed as qty 4, 3, 3
   --    again count of 10 is distributed as qty 4, 3, 3
   --    The total stake_qty is 8, 6, 6
   --    A count of 20 is distributed as qty 7, 7, 6 which is the desired qtys
   -- ------------------------------------------------------------------------- 
   open G_GET_QTY_TOTAL;
   fetch G_GET_QTY_TOTAL into L_qty;
   close G_GET_QTY_TOTAL;

   L_qty := L_qty + I_count_loc_qty;

   -- Subtract the total snapshot value
   open C_TOTAL_SNAPSHOT_ON_HAND_QTY;
   fetch C_TOTAL_SNAPSHOT_ON_HAND_QTY into L_sum_snapshot_on_hand_qty;
   close C_TOTAL_SNAPSHOT_ON_HAND_QTY;
   
   L_qty := L_qty - L_sum_snapshot_on_hand_qty;

   L_failed := 0;
   
   if L_qty != 0 then
      if not distribution_sql.distribute(L_error_message,
                                         L_dist_table,
                                         I_item,
                                         I_physical_wh,
                                         L_qty,
                                         'STKREC',
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         I_cycle_count) then
         L_failed := 1;
         O_error_message := L_error_message;
      end if;
   end if;

   if (L_failed = 0) then

      -- tack on, to the end of the distribution table, any locations that were missed.
      FOR rec IN c_snapshot_on_hand_qty LOOP
         L_found := FALSE;
         if L_dist_table.COUNT > 0 then
            FOR j IN L_dist_table.first..L_dist_table.last LOOP
               if rec.location = L_dist_table(j).wh then
                  l_found := TRUE;
                  EXIT;
               end if;
            END LOOP;
         end if;
         if NOT L_found then
            i := L_dist_table.COUNT + 1;
            L_dist_table(i).wh := rec.location;
            L_dist_table(i).dist_qty := 0;
         end if;
      END LOOP;
      ---
      if (L_dist_table.count = 0) then
         L_failed := 1;
         O_error_message := SQL_LIB.CREATE_MSG('NO_DIST_WH',
                                               SQLERRM,
                                               'STKCOUNT_SQL.STOCK_DISTRIBUTION',
                                               TO_CHAR(SQLCODE));
         return FALSE;
      end if;

      ---
      FOR j in L_dist_table.first..L_dist_table.last LOOP

         L_dist_wh  := L_dist_table(j).wh;
         -- -------------------------------------------------------------------
         -- Get the total for the location excluding the current location qty.
         -- we want to distribute equally to each location so that the location
         -- count location matrix adds up both down and across
         -- -------------------------------------------------------------------
         open G_GET_LOC_TOTAL(L_dist_wh);
         fetch G_GET_LOC_TOTAL into L_qty;
         close G_GET_LOC_TOTAL;
         -- -------------------------------------------------------------------
         -- From Example above: distribute 10 - L_dist_table(1) = 4
         --    second time in distribute 20 (10 + 10) L_dist_table(1) =  7
         --    and then L_dist_qty = 7 - 4
         -- -------------------------------------------------------------------
         L_snapshot_on_hand_qty := NULL;
         open C_UPDATE_STAKE_SKU_LOC;
         fetch C_UPDATE_STAKE_SKU_LOC into L_snapshot_on_hand_qty;
         close C_UPDATE_STAKE_SKU_LOC;

         L_dist_qty := L_dist_table(j).dist_qty - L_qty + NVL(L_snapshot_on_hand_qty, 0);

         -- -------------------------------------------------------------------
         -- Check for a STAKE_QTY record. If it exists then this is an update. 
         -- -------------------------------------------------------------------
         L_stake_qty := NULL;
         -- -------------------------------------------------------------------
         -- locking STAKE_QTY records for update
         -- -------------------------------------------------------------------
         open C_UPDATE_STAKE_QTY;
         fetch C_UPDATE_STAKE_QTY INTO L_stake_qty, L_rowid;
         -- Close cursor after update because using current of C_UPDATE_STAKE_QTY
         close C_UPDATE_STAKE_QTY;

         L_ssl_dist_qty := L_dist_qty - NVL(L_stake_qty, 0);

         if L_snapshot_on_hand_qty is null then
            if ITEMLOC_QUANTITY_SQL.GET_STOCK_ON_HAND(O_error_message,
                                                      L_snapshot_on_hand_qty,
                                                      I_item,
                                                      L_dist_wh,
                                                      'W') = FALSE then
               return FALSE;
            end if;

            LP_item(1).item := I_item;

            if L_pack_ind = 'Y' then
               i := 1;
               FOR rec in C_GET_PACKITEMS LOOP
                  i := i + 1;
                  LP_item(i).item := rec.item;
                  LP_item(i).pack_qty := rec.qty;
               END LOOP;
            end if;

            --- This indicator allows the physical count qty to be updated for the pack but not it's components.
            --- If the item is not a pack item then the variable will not be used in the insert.
            L_pack_comp := 'N';

            FOR i in LP_item.first .. LP_item.last LOOP
               insert into stake_sku_loc(cycle_count,
                                         loc_type,
                                         location,
                                         item,
                                         snapshot_on_hand_qty,
                                         snapshot_in_transit_qty,
                                         snapshot_unit_cost,
                                         snapshot_unit_retail,
                                         processed,
                                         physical_count_qty,
                                         pack_comp_qty,
                                         dept,
                                         class,
                                         subclass)
                                 (select I_cycle_count,
                                         'W',
                                         L_dist_wh,
                                         LP_item(i).item,
                                         decode(L_pack_comp, 'N', L_snapshot_on_hand_qty, 0),
                                         0,
                                         0,
                                         il.unit_retail,
                                         'N',
                                         decode(L_pack_comp, 'N', L_ssl_dist_qty, 0),
                                         decode(L_pack_comp, 'Y', L_ssl_dist_qty * LP_item(i).pack_qty, 0),
                                         dept,
                                         class,
                                         subclass
                                    from item_master im,
                                         item_loc il
                                   where im.item = I_item
                                     and im.item = il.item
                                     and il.loc = L_dist_wh
                                     and il.loc_type = 'W');
               L_pack_comp := 'Y';
            END LOOP;
         else
            -- ---------------------------------------------------------------
            -- Update the PHYSICAL_COUNT_QTY even if the item is a pack and 
            -- the RECEIVE_AS_TYPE = 'E'
            -- ----------------------------------------------------------------
            update stake_sku_loc
               set physical_count_qty = NVL(physical_count_qty, 0) + L_ssl_dist_qty
             where cycle_count        = I_cycle_count
               and item               = I_item
               and location           = L_dist_wh
               and loc_type           = 'W';
               
            -- ----------------------------------------------------------------
            -- If the item is a Pack then update the component item quantities
            -- ----------------------------------------------------------------
            if ( L_pack_ind = 'Y' ) then
               if UPDATE_STAKE_SKU_LOC_FOR_PACK(O_error_message,
                                                I_cycle_count,
                                                L_dist_wh,
                                                I_item,
                                                L_ssl_dist_qty) = FALSE then
                  return FALSE;
               end if;
            end if;   -- if L_exist is null then
         end if;   -- if L_snapshot_on_hand_qty is null then

         if ( L_stake_qty IS NULL ) then
            -- ----------------------------------------------------------------
            -- Insert record into stake_qty
            -- ----------------------------------------------------------------
            insert into stake_qty(cycle_count,
                                  loc_type,
                                  location,
                                  item,
                                  qty,
                                  location_desc)
                           values(I_cycle_count,
                                  'W',
                                  L_dist_wh,
                                  I_item,
                                  L_dist_qty,
                                  I_count_loc_desc);
         else
            -- ----------------------------------------------------------------
            -- Update stake_qty record
            -- ----------------------------------------------------------------

            update stake_qty
               set qty = L_dist_qty
             where ROWID = L_rowid;
         end if;

      END LOOP;   -- j in L_dist_table.first..L_dist_table.last LOOP
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      L_failed := 1;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.STOCK_DISTRIBUTION',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END STOCK_DISTRIBUTION;
-------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_SKU_LOC_FOR_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                       I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                       I_pack_item       IN       STAKE_SKU_LOC.ITEM%TYPE,
                                       I_count_qty       IN       STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE)

RETURN BOOLEAN IS

   cursor C_CMPNT_SKU(P_vwh  STAKE_SKU_LOC.LOCATION%TYPE) is
      select pq.item,
             NVL(pq.qty, 0) qty
        from v_packsku_qty pq,
             stake_sku_loc sl
       where pq.pack_no     = I_pack_item
         and pq.item        = sl.item
         and sl.cycle_count = I_cycle_count
         and sl.location    = P_vwh
         for update of sl.physical_count_qty nowait;

   L_receive_as_type ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   L_table           VARCHAR2(30) := 'STAKE_SKU_LOC';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,
                                    -54);

BEGIN
   ---
   if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                             L_receive_as_type,
                                             I_pack_item,
                                             I_location) = FALSE then
      return FALSE;
   end if;
   ---
   if ( L_receive_as_type = 'E' )then
      -- ----------------------------------------------------------------
      -- Since stock for packs is counted at the component level the qty
      -- needs to be added to PHYSICAL_COUNT_QTY and not PACK_COMP_QTY.
      -- Since a pack component item may be in different packs and will  
      -- also be counted as a bulk item the new count needs to be added 
      -- instead of directly updating the qty.
      -- ----------------------------------------------------------------
      FOR c_rec IN C_CMPNT_SKU(I_location) LOOP
         update STAKE_SKU_LOC
            set physical_count_qty = NVL(physical_count_qty, 0) + (I_count_qty * c_rec.qty)
          where current of C_CMPNT_SKU;
      END LOOP;
   else
      -- ----------------------------------------------------------------
      -- Since a pack component item may be in different packs the 
      -- new count needs to be added to pack_comp_qty.
      -- ----------------------------------------------------------------
      FOR c_rec IN C_CMPNT_SKU(I_location) LOOP
         update STAKE_SKU_LOC
            set pack_comp_qty = NVL(pack_comp_qty, 0) + (I_count_qty * c_rec.qty)
          where current of C_CMPNT_SKU;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            TO_CHAR(I_location),
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            'STKCOUNT_SQL.UPDATE_STAKE_SKU_LOC_FOR_PACK',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_STAKE_SKU_LOC_FOR_PACK;
-------------------------------------------------------------------------------------
FUNCTION NEW_STKCNT_ITEM_LOC(O_error_message  IN OUT VARCHAR2,
                             I_cycle_count    IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_location       IN     ITEM_LOC.LOC%TYPE,
                             I_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_item           IN     ITEM_MASTER.ITEM%TYPE,
                             I_phys_count_qty IN     STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE)
   return BOOLEAN is

   i            INTEGER;
   L_pack_ind   ITEM_MASTER.PACK_IND%TYPE := NULL;
   L_loc        ITEM_LOC.LOC%TYPE;
   L_pack_comp  VARCHAR2(1);

   cursor C_PRIMARY_WH_N is
      select wh2.wh
        from wh wh1,
             wh wh2
       where wh1.wh = I_location
         and wh2.restricted_ind = 'N'
         and wh1.wh = wh2.physical_wh
         and wh1.primary_vwh = wh2.wh;

   cursor C_MIN_WH is
      select MIN(wh2.wh)
        from wh wh1,
             wh wh2
       where wh1.wh = I_location
         and wh2.restricted_ind = 'N'
         and wh1.wh = wh2.physical_wh
         and wh2.primary_vwh IS NULL;

   cursor C_PRIMARY_WH_Y is
      select primary_vwh
        from wh
       where wh = I_location;

   cursor C_GET_PACKITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;

   cursor C_PACK_IND is
      select pack_ind
        from item_master
       where item = I_item;

BEGIN

   if I_cycle_count is NULL or I_location is NULL or I_loc_type is NULL or I_item is NULL or I_phys_count_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'STKCOUNT_SQL.NEW_STKCNT_ITEM_LOC',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type = 'W' then
      open C_PRIMARY_WH_N;
      fetch C_PRIMARY_WH_N into L_loc;
      close C_PRIMARY_WH_N;

      if L_loc is NULL then
         open C_MIN_WH;
         fetch C_MIN_WH into L_loc;
         close C_MIN_WH;

         if L_loc is NULL then
            open C_PRIMARY_WH_Y;
            fetch C_PRIMARY_WH_Y into L_loc;
            close C_PRIMARY_WH_Y;
         end if;
      end if;
   else
      L_loc := I_location;
   end if;

   open C_PACK_IND;
   fetch C_PACK_IND into L_pack_ind;
   close C_PACK_IND;

   LP_item(1).item := I_item;

   if L_pack_ind = 'Y' then
      i := 1;
      FOR rec IN C_GET_PACKITEMS loop
         i := i + 1;
         LP_item(i).item := rec.item;
         LP_item(i).pack_qty := rec.qty;
      END LOOP;
   end if;

   --- This indicator allows the physical count qty to be updated for the pack but not it's components.
   --- If the item is not a pack item then the variable will not be used in the insert.
   L_pack_comp := 'N';

   FOR i IN LP_item.first .. LP_item.last LOOP
      if NEW_ITEM_LOC(O_error_message,
                      LP_item(i).item,
                      L_loc,
                      NULL, -- ITEM_PARENT
                      NULL, -- ITEM_GRANDPARENT
                      I_loc_type,
                      NULL, -- SHORT_DESC
                      NULL, -- DEPT
                      NULL, -- CLASS
                      NULL, -- SUBCLASS
                      NULL, -- ITEM_LEVEL,
                      NULL, -- TRAN_LEVEL,
                      NULL, -- ITEM_STATUS
                      NULL, -- WASTE TYPE
                      NULL, -- DAILY WASTE PCT
                      NULL, -- SELLABLE_IND
                      NULL, -- ORDERABLE_IND
                      L_pack_ind,
                      NULL, -- PACK_TYPE
                      NULL, -- UNIT_COST_LOC
                      NULL, -- UNIT_RETAIL_LOC
                      NULL, -- SELLING_RETAIL_LOC
                      NULL, -- SELLING_UOM
                      NULL, -- ITEM_LOC_STATUS
                      NULL, -- TAXABLE_IND
                      NULL, -- TI
                      NULL, -- HI
                      NULL, -- STORE_ORD_MULT
                      NULL, -- MEAS_OF_EACH
                      NULL, -- MEAS_OF_PRICE
                      NULL, -- UOM_OF_PRICE
                      NULL, -- PRIMARY_VARIANT
                      NULL, -- PRIMARY_SUPP
                      NULL, -- PRIMARY_CNTRY
                      NULL, -- LOCAL_ITEM_DESC
                      NULL, -- LOCAL_SHORT_DESC
                      NULL, -- PRIMARY_COST_PACK
                      NULL, -- RECEIVE_AS_TYPE
                      NULL, -- DATE,
                      NULL) -- DEFAULT_TO_CHILDREN
                      = FALSE then
         return FALSE;
      end if;

      insert into stake_sku_loc(cycle_count,
                                loc_type,
                                location,
                                item,
                                snapshot_on_hand_qty,
                                snapshot_in_transit_qty,
                                snapshot_unit_cost,
                                snapshot_unit_retail,
                                processed,
                                physical_count_qty,
                                pack_comp_qty,
                                dept,
                                class,
                                subclass)
                        (select I_cycle_count,
                                I_loc_type,
                                L_loc,
                                LP_item(i).item,
                                0,
                                0,
                                0,
                                il.unit_retail,
                                'N',
                                decode(L_pack_comp, 'N', I_phys_count_qty, 0),
                                decode(L_pack_comp, 'Y', I_phys_count_qty * LP_item(i).pack_qty, 0),
                                dept,
                                class,
                                subclass
                           from item_master im, item_loc il
                          where im.item = I_item
                            and im.item = il.item
                            and il.loc = L_loc
                            and not exists (select 'x'
                                              from stake_sku_loc ssl1
                                             where ssl1.cycle_count = I_cycle_count
                                               and ssl1.loc_type    = I_loc_type
                                               and ssl1.location    = L_loc
                                               and ssl1.item        = LP_item(i).item));

      L_pack_comp := 'Y';

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            'STKCOUNT_SQL.NEW_STKCNT_ITEM_LOC',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END NEW_STKCNT_ITEM_LOC;
-------------------------------------------------------------------------------------
FUNCTION STAKE_QTY_EXIST(O_error_message IN OUT VARCHAR2,
                         O_processed     IN OUT STAKE_SKU_LOC.PROCESSED%TYPE,
                         I_cycle_count   IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                         I_location      IN     ITEM_LOC.LOC%TYPE,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_MC_SQ_EXIST is
      select ssl.processed
        from stake_qty sq,
             stake_sku_loc ssl,
             wh
       where sq.cycle_count       = I_cycle_count
         and sq.cycle_count       = ssl.cycle_count
         and sq.item              = I_item
         and sq.loc_type          = ssl.loc_type
         and sq.location          = ssl.location
         and sq.item              = ssl.item
         and sq.location          = DECODE(sq.loc_type,
                                           'W',
                                           wh.wh,
                                           I_location)
         and wh.physical_wh       = DECODE(sq.loc_type,
                                           'W',
                                           I_location,
                                           wh.physical_wh)
         and rownum = 1;

BEGIN
   if I_cycle_count is NULL or I_location is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'STKCOUNT_SQL.STAKE_QTY_EXIST',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_MC_SQ_EXIST;
   fetch C_MC_SQ_EXIST into O_processed;
   close C_MC_SQ_EXIST;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.STAKE_QTY_EXIST',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END STAKE_QTY_EXIST;
-------------------------------------------------------------------------------------
FUNCTION STAKE_SKU_LOC_EXIST(O_error_message      IN OUT VARCHAR2,
                             I_cycle_count        IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_location           IN     ITEM_LOC.LOC%TYPE,
                             I_item               IN     ITEM_MASTER.ITEM%TYPE,
                             I_phys_count_qty     IN     STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE)
   return BOOLEAN is

   L_exist     VARCHAR2(1)               := NULL;
   L_pack_ind  ITEM_MASTER.PACK_IND%TYPE := NULL;
   cursor C_SSL_EXIST is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and item        = I_item
         and location    = I_location;

   cursor C_PACK_IND is
      select pack_ind
        from item_master
       where item = I_item;
   cursor C_GET_PACKITEMS is
      select item
        from v_packsku_qty
       where pack_no = I_item;
BEGIN

   open C_SSL_EXIST;
   fetch C_SSL_EXIST into L_exist;
   close C_SSL_EXIST;

   if L_exist is NULL then
      insert into stake_sku_loc(cycle_count,
                                loc_type,
                                location,
                                item,
                                snapshot_on_hand_qty,
                                snapshot_in_transit_qty,
                                snapshot_unit_cost,
                                snapshot_unit_retail,
                                processed,
                                physical_count_qty,
                                pack_comp_qty,
                                dept,
                                class,
                                subclass)
                        (select I_cycle_count,
                                I_loc_type,
                                I_location,
                                I_item,
                                0,
                                0,
                                0,
                                il.unit_retail,
                                'N',
                                I_phys_count_qty,
                                0,
                                dept,
                                class,
                                subclass
                           from item_master im, item_loc il
                          where im.item = I_item
                            and im.item = il.item
                            and il.loc = I_location);
   end if;

   open C_PACK_IND;
   fetch C_PACK_IND into L_pack_ind;
   close C_PACK_IND;
   if L_pack_ind = 'Y' then
      FOR rec IN C_GET_PACKITEMS LOOP
         insert into stake_sku_loc(cycle_count,
                                   loc_type,
                                   location,
                                   item,
                                   snapshot_on_hand_qty,
                                   snapshot_in_transit_qty,
                                   snapshot_unit_cost,
                                   snapshot_unit_retail,
                                   processed,
                                   physical_count_qty,
                                   pack_comp_qty,
                                   dept,
                                   class,
                                   subclass)
                           (select I_cycle_count,
                                   I_loc_type,
                                   I_location,
                                   rec.item,
                                   0,
                                   0,
                                   0,
                                   il.unit_retail,
                                   'N',
                                   I_phys_count_qty,
                                   0,
                                   dept,
                                   class,
                                   subclass
                              from item_master im, item_loc il
                             where im.item = rec.item
                               and im.item = il.item
                               and il.loc = I_location
                               and not exists (select 'x'
                                                 from stake_sku_loc ssl
                                                where ssl.item = rec.item
                                                  and ssl.cycle_count = I_cycle_count
                                                  and ssl.loc_type = I_loc_type
                                                  and ssl.location = I_location));
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.STAKE_SKU_LOC_EXIST',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END STAKE_SKU_LOC_EXIST;
-------------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_LOCATION(O_error_message      IN OUT VARCHAR2,
                             I_cycle_count        IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_location           IN     ITEM_LOC.LOC%TYPE,
                             I_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_user_id            IN     STAKE_CONT.USER_ID%TYPE)
RETURN BOOLEAN IS

   L_exception_id   NUMBER(1);
   L_location       ITEM_LOC.LOC%TYPE  := I_location;
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_locked,
                                   -54);

   cursor C_LOCK_STAKE_LOCATIONS is
      select 'x'
        from stake_location
       where cycle_count = I_cycle_count
         and location    = L_location
         and loc_type    = I_loc_type
         for update of cycle_count nowait;

   cursor C_LOCK_STAKE_LOCATIONS_ALL is
      select 'x'
        from stake_location
       where cycle_count = I_cycle_count
         and location is not NULL
         for update of cycle_count nowait;

   cursor C_GET_STAKE_CONT is
      select distinct loc_type,
             location
        from stake_cont
       where cycle_count = I_cycle_count
         and user_id     = I_user_id;

BEGIN
   L_exception_id := 1;
   if I_user_id is not NULL then
      -- Loop through each location on the stake_cont table
      -- and lock the corresponding record on stake_location
      FOR rec IN C_GET_STAKE_CONT LOOP
         L_location := rec.location;
         open C_LOCK_STAKE_LOCATIONS;
         close C_LOCK_STAKE_LOCATIONS;
      end LOOP;
   elsif I_location is not NULL then
      open C_LOCK_STAKE_LOCATIONS;
      close C_LOCK_STAKE_LOCATIONS;
   elsif I_location is NULL then
      open C_LOCK_STAKE_LOCATIONS_ALL;
      close C_LOCK_STAKE_LOCATIONS_ALL;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'STKCOUNT_SQL.LOCK_STAKE_LOCATION',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if L_exception_id = 1 then
         O_error_message := SQL_LIB.CREATE_MSG('STOCK_CONT_REC_LOCK',
                                               TO_CHAR(I_cycle_count),
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
END LOCK_STAKE_LOCATION;
-------------------------------------------------------------------------------------
FUNCTION STAKE_SKU_LOC_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   VARCHAR2,
                             I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                             I_location        IN       ITEM_LOC.LOC%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN is

   L_exists               VARCHAR2(30)           := NULL;
   L_stocktake_type       STAKE_HEAD.STOCKTAKE_TYPE%TYPE;
   L_loc_type             STAKE_HEAD.LOC_TYPE%TYPE;
   L_product_level_ind    STAKE_HEAD.PRODUCT_LEVEL_IND%TYPE;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   
   cursor C_STOCKTAKE_TYPE is
      select stocktake_type,
             loc_type,
             nvl(product_level_ind, 'I')
        from stake_head 
       where cycle_count = I_cycle_count;   
   
   cursor C_SSL_EXIST is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and item        = I_item
         and location    = I_location
         and rownum      = 1;

   cursor C_SSL_WH_EXIST is
      select 'x'
        from stake_sku_loc ssl
       where ssl.cycle_count = I_cycle_count
         and ssl.item        = I_item
         and exists (select 'x'
                       from wh
                      where wh.physical_wh = I_location
                        and wh.wh = ssl.location)
         and rownum = 1;

  -- If the item does not exist in stake_sku_loc record then check whether the item belongs to the 
  -- DEPT/CLASS/SUBCLASS for the given cycle count in stake_prod_loc. If it exists in stake_prod_loc then 
  -- create the missing stake_sku_loc record.
      cursor C_STAKE_PROD_LOC_EXIST is
         select 'x'
           from stake_prod_loc spl, 
                item_master im,
                wh wh
          where spl.dept        = im.dept
            and spl.class       = im.class
            and spl.subclass    = im.subclass
            and spl.location    = decode (spl.loc_type,'W',wh.wh, I_location)
            and wh.physical_wh  = decode(spl.loc_type,'W',I_location ,wh.physical_wh)
            and spl.loc_type    = I_loc_type
            and im.item         = I_item
            and spl.cycle_count = I_cycle_count
            and rownum=1;

        cursor C_STAKE_PRODUCT_EXIST is
           select 'x'
            from stake_product stp,
                 item_master im
           where stp.cycle_count = I_cycle_count
             and im.item         = I_item
             and im.dept         = stp.dept  
             and im.class        = nvl(stp.class,im.class)
             and im.subclass     = nvl(stp.subclass,im.subclass)
             and rownum = 1;

BEGIN

   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_STOCKTAKE_TYPE',
                    'stake_head',
                    'Cycle Count '||I_cycle_count);
   open C_STOCKTAKE_TYPE;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_STOCKTAKE_TYPE',
                    'stake_head',
                    'Cycle Count '||I_cycle_count);
   fetch C_STOCKTAKE_TYPE into L_stocktake_type,
                               L_loc_type,
                               L_product_level_ind;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_STOCKTAKE_TYPE',
                    'stake_head',
                    'Cycle Count '||I_cycle_count);
   close C_STOCKTAKE_TYPE;
   ---

   if I_loc_type = 'W' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_SSL_WH_EXIST',
                       'stake_sku_loc',
                       'Cycle Count '||I_cycle_count);
      open C_SSL_WH_EXIST;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_SSL_WH_EXIST',
                       'stake_sku_loc',
                       'Cycle Count '||I_cycle_count);
      fetch C_SSL_WH_EXIST into L_exists;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SSL_WH_EXIST',
                       'stake_sku_loc',
                       'Cycle Count '||I_cycle_count);
      close C_SSL_WH_EXIST;
      ---
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_SSL_EXIST',
                       'stake_sku_loc',
                       'Cycle Count '||I_cycle_count);
      open C_SSL_EXIST;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_SSL_EXIST',
                       'stake_sku_loc',
                       'Cycle Count '||I_cycle_count);
      fetch C_SSL_EXIST into L_exists;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SSL_EXIST',
                       'stake_sku_loc',
                       'Cycle Count '||I_cycle_count);
      close C_SSL_EXIST;
      ---
   end if;
   ---
   if L_exists is null then
       if L_stocktake_type != 'U' then
         open C_STAKE_PROD_LOC_EXIST;
         fetch C_STAKE_PROD_LOC_EXIST into L_exists;
         close C_STAKE_PROD_LOC_EXIST;
         if L_exists is not null then
            if STAKE_SKU_LOC_EXIST(O_error_message ,
                                I_cycle_count ,
                                I_loc_type ,
                                I_location ,
                                I_item   ,
                                0) = FALSE then
                return FALSE;
            end if;
         end if;
      elsif L_product_level_ind != 'I' then
         open C_STAKE_PRODUCT_EXIST;
         fetch C_STAKE_PRODUCT_EXIST into L_exists;
         close C_STAKE_PRODUCT_EXIST;
         if L_exists is not null then     
            if STAKE_SKU_LOC_EXIST(O_error_message ,
                                   I_cycle_count ,
                                   I_loc_type ,
                                   I_location ,
                                   I_item   ,
                                   0) = FALSE then
                   return FALSE;
            end if;
         end if;
      end if;
   end if;
   ---
   O_exists := L_exists;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.STAKE_SKU_LOC_CHECK',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END STAKE_SKU_LOC_CHECK;
------------------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_SCHEDULE (O_error_message      IN OUT VARCHAR2,
                              O_record_locked      IN OUT BOOLEAN,
                              I_location           IN     ITEM_LOC.LOC%TYPE,
                              I_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_stocktake_type     IN     STAKE_SCHEDULE.STOCKTAKE_TYPE%TYPE)
   RETURN BOOLEAN is
   L_exception_id   NUMBER(1);
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_STAKE_SCHEDULE_LOC is
      select 'x'
        from stake_schedule
       where location       = I_location
         and loc_type       = I_loc_type
         and stocktake_type = I_stocktake_type
         for update nowait;


BEGIN
   O_record_locked := TRUE;

   if I_location is NULL or
      I_loc_type is NULL or
      I_stocktake_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'STKCOUNT_SQL.LOCK_STAKE_SCHEDULE',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_LOCK_STAKE_SCHEDULE_LOC;
   close C_LOCK_STAKE_SCHEDULE_LOC;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_record_locked := FALSE;
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.LOCK_STAKE_SCHEDULE',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_STAKE_SCHEDULE;
------------------------------------------------------------------------------------------
FUNCTION GET_STAKE_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_loc_qty         IN OUT   STAKE_QTY.QTY%TYPE,
                        I_item            IN       STAKE_QTY.ITEM%TYPE,
                        I_location        IN       STAKE_QTY.LOCATION%TYPE,
                        I_loc_type        IN       STAKE_QTY.LOC_TYPE%TYPE,
                        I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE)
RETURN BOOLEAN is

   cursor G_GET_LOC_TOTAL is 
      select NVL(SUM(NVL(qty, 0)), 0)
        from stake_qty sq
       where sq.cycle_count = I_cycle_count
         and sq.item = I_item
         and sq.loc_type = I_loc_type
         and sq.location = I_location;

BEGIN
   open G_GET_LOC_TOTAL;
   fetch G_GET_LOC_TOTAL into O_loc_qty;
   close G_GET_LOC_TOTAL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.GET_STAKE_QTY',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_STAKE_QTY;
------------------------------------------------------------------------------------------
FUNCTION REDISTRIBUTE_PWH (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_total_qty       IN       STAKE_QTY.QTY%TYPE,
                           I_physical_wh     IN       STAKE_QTY.LOCATION%TYPE,
                           I_item            IN       STAKE_QTY.ITEM%TYPE,
                           I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE)
RETURN BOOLEAN is

   TYPE COUNT_LOC_REC_TYPE IS RECORD
   (
   location_desc     STAKE_QTY.LOCATION_DESC%TYPE,
   qty               STAKE_QTY.QTY%TYPE
   );

   TYPE COUNT_LOC_TABLE_TYPE IS TABLE OF COUNT_LOC_REC_TYPE
   INDEX BY BINARY_INTEGER;

   L_pack_ind          ITEM_MASTER.PACK_IND%TYPE;
   L_update_qty        STAKE_QTY.QTY%TYPE;
   L_count_loc_tab     COUNT_LOC_TABLE_TYPE;
   i                   NUMBER;

   cursor C_GET_PACK_IND is
      select pack_ind
        from item_master
       where item = I_item;

   cursor C_STAKE_SKU_LOC is 
      select ssl.location,
             ssl.snapshot_on_hand_qty,
             ssl.distribute_qty,
             sum(sq.qty) old_qty
        from stake_sku_loc ssl,
             stake_qty sq,
             wh
       where ssl.cycle_count = I_cycle_count
         and ssl.loc_type = 'W'
         and ssl.item = I_item
         and sq.cycle_count = ssl.cycle_count
         and sq.loc_type = ssl.loc_type
         and sq.location = ssl.location
         and sq.item = ssl.item
         and wh.wh = sq.location
         and wh.physical_wh = I_physical_wh
        group by ssl.location, ssl.snapshot_on_hand_qty, ssl.distribute_qty;

   cursor C_GET_LOC_QTY is 
      select sq.location_desc,
             sum(sq.qty) qty
        from stake_qty sq, 
             wh
       where sq.cycle_count = I_cycle_count
         and sq.loc_type = 'W'
         and sq.item = I_item
         and wh.wh = sq.location
         and wh.physical_wh = I_physical_wh
       group by sq.location_desc;

BEGIN
   -- is the item a pack?
   open C_GET_PACK_IND;
   fetch C_GET_PACK_IND into L_pack_ind;
   close C_GET_PACK_IND;
   -- -------------------------------------------------------------------------
   -- Update STAKE_SKU_LOC.PHYSICAL_COUNT_QTY for each virtual warehouse
   -- -------------------------------------------------------------------------
   FOR rec IN C_STAKE_SKU_LOC LOOP
      -- ----------------------------------------------------------------------
      -- Back out PHYSICAL_COUNT_QTY before distribution.
      -- swap snapshot_on_hand_qty and distribute_qty so STOCK_DISTRIBUTION
      -- uses the updated values
      -- ----------------------------------------------------------------------
      update stake_sku_loc ssl
         set physical_count_qty = physical_count_qty - rec.old_qty,
             distribute_qty = rec.snapshot_on_hand_qty,
             snapshot_on_hand_qty = rec.distribute_qty
       where ssl.cycle_count = I_cycle_count
         and ssl.loc_type = 'W'
         and ssl.location = rec.location
         and ssl.item = I_item;

      if ( L_pack_ind = 'Y' ) then
         if UPDATE_STAKE_SKU_LOC_FOR_PACK(O_error_message,
                                          I_cycle_count,
                                          rec.location,
                                          I_item,
                                          rec.old_qty * -1) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;

   -- -------------------------------------------------------------------------
   -- Get the stock count location totals and zero out the STAKE_QTY.QTY values 
   -- for redistribution.
   -- -------------------------------------------------------------------------
   i := 0;
   for rec in C_GET_LOC_QTY loop
      i := i + 1;
      L_count_loc_tab(i).location_desc := rec.location_desc;
      L_count_loc_tab(i).qty := rec.qty;
   end loop;
   
   update stake_qty
      set qty = 0
    where cycle_count = I_cycle_count
      and loc_type = 'W'
      and item = I_item
      and location in (select wh 
                         from wh 
                        where wh.wh = stake_qty.location
                          and wh.physical_wh = I_physical_wh);

   -- -------------------------------------------------------------------------
   -- Use the same distribution routine to redistribute the stock count 
   -- -------------------------------------------------------------------------
   if L_count_loc_tab is NOT NULL and L_count_loc_tab.count > 0 then
      for i in L_count_loc_tab.first .. L_count_loc_tab.last loop
         if STKCOUNT_SQL.STOCK_DISTRIBUTION(O_error_message,
                                            I_cycle_count,
                                            I_physical_wh,
                                            I_item,
                                            L_count_loc_tab(i).qty,
                                            L_count_loc_tab(i).location_desc)= FALSE then
            return FALSE;
         end if;
      end loop;
   end if;
   -- -------------------------------------------------------------------------
   -- Swap them back so the original SNAPSHOT_ON_HAND_QTY is displayed correctly
   -- -------------------------------------------------------------------------
   if L_count_loc_tab is NOT NULL and L_count_loc_tab.count > 0 then
      if STKCOUNT_SQL.SWAP_SNAPSHOT_DISTRIBUTE(O_error_message,
                                               I_item,
                                               I_physical_wh,
                                               I_cycle_count)= FALSE then
         return FALSE;
      end if;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.REDISTRIBUTE_PWH',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REDISTRIBUTE_PWH;
------------------------------------------------------------------------------------------
FUNCTION SWAP_SNAPSHOT_DISTRIBUTE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item            IN       STAKE_QTY.ITEM%TYPE,
                                   I_physical_wh     IN       STAKE_QTY.LOCATION%TYPE,
                                   I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE)
RETURN BOOLEAN IS

   L_exists  VARCHAR2(1) := 'N';

   cursor C_DISTRIBUTE_QTY is
      select 'Y'
        from stake_sku_loc ssl,
             wh
       where ssl.item = I_item
         and ssl.cycle_count = I_cycle_count
         and ssl.loc_type = 'W'
         and ssl.distribute_qty is not null
         and wh.wh = ssl.location
         and wh.physical_wh = I_physical_wh;

   cursor C_STAKE_SKU_LOC is
      select ssl.snapshot_on_hand_qty,
             ssl.distribute_qty
        from stake_sku_loc ssl,
             wh
       where ssl.item = I_item
         and ssl.cycle_count = I_cycle_count
         and ssl.loc_type = 'W'
         and wh.wh = ssl.location
         and wh.physical_wh = I_physical_wh
         for update of ssl.snapshot_on_hand_qty, ssl.distribute_qty nowait;

BEGIN
   open C_DISTRIBUTE_QTY;
   fetch C_DISTRIBUTE_QTY into L_exists;
   close C_DISTRIBUTE_QTY;
   ---
   if ( L_exists = 'N' ) then
      return TRUE;
   end if;
   ---
   for rec in C_STAKE_SKU_LOC loop
      update stake_sku_loc
         set snapshot_on_hand_qty = rec.distribute_qty,
             distribute_qty       = rec.snapshot_on_hand_qty
       where current of C_STAKE_SKU_LOC;
   end loop;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.SWAP_SNAPSHOT_DISTRIBUTE',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SWAP_SNAPSHOT_DISTRIBUTE;
------------------------------------------------------------------------------------------
FUNCTION UPDATE_SNAPSHOT_ON_HAND_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_new_soh_qty     IN       STAKE_SKU_LOC.SNAPSHOT_ON_HAND_QTY%TYPE,
                                      I_item            IN       STAKE_QTY.ITEM%TYPE,
                                      I_location        IN       STAKE_QTY.LOCATION%TYPE,
                                      I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE)
RETURN BOOLEAN IS

   L_dummy  VARCHAR2(1);
   
   cursor C_LOCK_STAKE_SKU_LOC is
      select 'X'
        from stake_sku_loc
       where location = I_location
         and item = I_item
         and cycle_count = I_cycle_count
         and loc_type = 'W'
         for update of snapshot_on_hand_qty;

BEGIN
   ---
   open C_LOCK_STAKE_SKU_LOC;
   fetch C_LOCK_STAKE_SKU_LOC into L_dummy;
   ---
   update stake_sku_loc
      set snapshot_on_hand_qty = I_new_soh_qty
     where current of C_LOCK_STAKE_SKU_LOC;
   ---
   close C_LOCK_STAKE_SKU_LOC;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.UPDATE_SNAPSHOT_ON_HAND_QTY',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_SNAPSHOT_ON_HAND_QTY;
------------------------------------------------------------------------------------------
FUNCTION ROLLUP_SELLABLE_ONLY_ITEM (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cycle_count    IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                    I_item           IN      ITEM_MASTER.ITEM%TYPE,
                                    I_location       IN      STAKE_SKU_LOC.LOCATION%TYPE,
                                    I_loc_type       IN      STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                    I_loc_desc       IN      STAKE_QTY.LOCATION_DESC%TYPE)
   RETURN BOOLEAN is

   L_program             VARCHAR2(40)   := 'STKCOUNT_SQL.ROLLUP_SELLABLE_ONLY_ITEM';
   L_table               VARCHAR2(20)   := NULL;
   L_sellable_qty        STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE;
   L_parent_ratio        ITEM_XFORM_DETAIL.ITEM_QUANTITY_PCT%TYPE;
   L_orderable_item      ITEM_XFORM_HEAD.HEAD_ITEM%TYPE;
   L_multi_parent_ind    BOOLEAN;
   L_dummy               NUMBER;
   L_orderable_count_qty STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE;
   L_old_qty             STAKE_QTY.QTY%TYPE;

   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_locked, -54);

   cursor C_GET_SELLABLE_QTY is
      select nvl(physical_count_qty, 0)
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and item        = I_item
         and loc_type    = I_loc_type
         and location    = I_location;

   cursor C_GET_ORDERABLE_ITEM is
      select ixh.head_item,
             NVL(ixd.yield_from_head_item_pct,100) yield_from_head_item_pct
        from item_xform_head ixh,
             item_xform_detail ixd,
             stake_sku_loc ssl
       where ixd.detail_item = I_item
         and ixh.item_xform_type = 'K'
         and ixh.item_xform_head_id = ixd.item_xform_head_id
         and ssl.item        = ixh.head_item
         and ssl.cycle_count = I_cycle_count
         and ssl.loc_type    = I_loc_type
         and ssl.location    = I_location;

   cursor C_LOCK_ORDERABLE_ITEM is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and item        = L_orderable_item
         and loc_type    = I_loc_type
         and location    = I_location
         for update nowait;

   cursor C_CHECK_STAKE_QTY is
      select 1
        from stake_qty
       where cycle_count   = I_cycle_count
         and item          = L_orderable_item
         and loc_type      = I_loc_type
         and location      = I_location
         and location_desc = I_loc_desc;

   cursor C_GET_OLD_SELLABLE_STAKE_QTY is
      select nvl(qty, 0)
        from stake_qty
       where cycle_count   = I_cycle_count
         and item          = I_item
         and loc_type      = I_loc_type
         and location      = I_location
         and location_desc = I_loc_desc;

BEGIN
   L_table := 'STAKE_SKU_LOC';
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SELLABLE_QTY',
                    'STAKE_SKU_LOC',
                    'SKU:'||(I_item));
   open C_GET_SELLABLE_QTY;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SELLABLE_QTY',
                    'STAKE_SKU_LOC',
                    'SKU:'||(I_item));
   fetch C_GET_SELLABLE_QTY into L_sellable_qty;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SELLABLE_QTY',
                    'STAKE_SKU_LOC',
                    'SKU:'||(I_item));
   close C_GET_SELLABLE_QTY;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_OLD_SELLABLE_STAKE_QTY',
                    'STAKE_QTY',
                    'SKU:'||(I_item));
   open C_GET_OLD_SELLABLE_STAKE_QTY;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_OLD_SELLABLE_STAKE_QTY',
                    'STAKE_QTY',
                    'SKU:'||(I_item));
   fetch C_GET_OLD_SELLABLE_STAKE_QTY into L_old_qty;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_OLD_SELLABLE_STAKE_QTY',
                    'STAKE_QTY',
                    'SKU:'||(I_item));
   close C_GET_OLD_SELLABLE_STAKE_QTY;

   if L_old_qty is NULL then
      L_old_qty := 0;
   end if;

   FOR rec in C_GET_ORDERABLE_ITEM LOOP

      L_orderable_item      := rec.head_item;
      L_parent_ratio        := rec.yield_from_head_item_pct;
      L_orderable_count_qty := ((L_parent_ratio/100.0)*(L_sellable_qty- L_old_qty));

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ORDERABLE_ITEM',
                       'STAKE_SKU_LOC',
                       'SKU:'||(L_orderable_item));
      open C_LOCK_ORDERABLE_ITEM;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ORDERABLE_ITEM',
                       'STAKE_SKU_LOC',
                       'SKU:'||(L_orderable_item));
      close C_LOCK_ORDERABLE_ITEM;

      update STAKE_SKU_LOC
         set physical_count_qty = NVL(physical_count_qty,0) + L_orderable_count_qty
       where cycle_count        = I_cycle_count
         and item               = L_orderable_item
         and loc_type           = I_loc_type
         and location           = I_location;

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_STAKE_QTY',
                       'STAKE_QTY',
                       'SKU:'||(L_orderable_item));
      open C_CHECK_STAKE_QTY;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_STAKE_QTY',
                       'STAKE_QTY',
                       'SKU:'||(L_orderable_item));
      fetch C_CHECK_STAKE_QTY into L_dummy;

      if C_CHECK_STAKE_QTY%NOTFOUND then
         insert into STAKE_QTY
                (cycle_count,
                 loc_type,
                 location,
                 item,
                 qty,
                 location_desc)
         values( I_cycle_count,
                 I_loc_type,
                 I_location,
                 L_orderable_item,
                 L_orderable_count_qty,
                 I_loc_desc) ;
      else
        update STAKE_QTY
           set qty = qty + L_orderable_count_qty
         where cycle_count   = I_cycle_count
           and item          = L_orderable_item
           and loc_type      = I_loc_type
           and location      = I_location
           and location_desc = I_loc_desc;
      end if;
     SQL_LIB.SET_MARK('CLOSE',
                      'C_CHECK_STAKE_QTY',
                      'STAKE_QTY',
                      'SKU:'||(L_orderable_item));
      close C_CHECK_STAKE_QTY;

   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ORDERABLE_ITEM%ISOPEN then
         close C_LOCK_ORDERABLE_ITEM;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table ,
                                            L_program,
                                            'NULL');
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROLLUP_SELLABLE_ONLY_ITEM;
------------------------------------------------------------------------------------------
FUNCTION ROLLUP_SELLABLE_ONLY_ITEM (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cycle_count    IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                    I_location       IN      STAKE_SKU_LOC.LOCATION%TYPE,
                                    I_loc_type       IN      STAKE_SKU_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(40)   := 'STKCOUNT_SQL.ROLLUP_SELLABLE_ONLY_ITEM';
   L_table               VARCHAR2(20)   := NULL;
   L_sellable_qty        STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE;
   L_old_qty             STAKE_QTY.QTY%TYPE;

   TYPE  TYP_pct         IS TABLE OF ITEM_XFORM_DETAIL.ITEM_QUANTITY_PCT%TYPE;
   TYPE  TYP_loc_desc    IS TABLE OF STAKE_QTY.LOCATION_DESC%TYPE;

   TBL_orderable         item_tbl := item_tbl();
   TBL_loc               loc_tbl := loc_tbl();
   TBL_sellable_qty      qty_tbl := qty_tbl();
   TBL_old_sellable_qty  qty_tbl := qty_tbl();
   TBL_parent_ratio      TYP_pct := TYP_pct();
   TBL_loc_desc          TYP_loc_desc := TYP_loc_desc();

   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_locked, -54);

   cursor C_GET_ORDERABLE_ITEM is
      select d.head_item,
             I_location,
             d.yield_from_head_item_pct,
             d.physical_count_qty,
             d.qty,
             d.loc_desc
       from  (select /*+ ordered index(ssl2, pk_stake_sku_loc) */ 
                     s.head_item,
                     NVL(s.yield_from_head_item_pct,100) yield_from_head_item_pct,
                     NVL(ssl2.physical_count_qty, 0) physical_count_qty,
                     sq.qty,
                     s.loc_desc
                from stk_xform_temp s,
                     stake_sku_loc ssl2,
                     stake_qty sq
               where ssl2.item = s.detail_item
                 and ssl2.cycle_count = I_cycle_count
                 and ssl2.loc_type = I_loc_type
                 and ssl2.location = I_location
                 and sq.cycle_count (+) = ssl2.cycle_count
                 and sq.loc_Type (+) = ssl2.loc_type
                 and sq.location (+) = ssl2.location
                 and sq.item (+) = ssl2.item)  d,
             stake_sku_loc ssl
       where ssl.cycle_count = I_cycle_count
         and ssl.loc_type = I_loc_type
         and ssl.location = I_location
         and ssl.item = d.head_item
         for update of ssl.item nowait;

   cursor C_GET_ORDERABLE_ITEM_WH is
      select d.head_item,
             d.location,
             d.yield_from_head_item_pct,
             d.physical_count_qty,
             d.qty,
             d.loc_desc
        from (select /*+ ordered */
                     s.head_item,
                     ssl2.location,
                     NVL(s.yield_from_head_item_pct,100) yield_from_head_item_pct,
                     NVL(ssl2.physical_count_qty, 0) physical_count_qty,
                     sq.qty,
                     s.loc_desc
                from stk_xform_temp s,
                     wh,
                     stake_sku_loc ssl2,
                     stake_qty sq
               where wh.physical_wh = I_location
                 and wh.wh != wh.physical_wh
                 and ssl2.cycle_count = I_cycle_count
                 and ssl2.loc_type = I_loc_type
                 and ssl2.location = wh.wh
                 and ssl2.item = s.detail_item
                 and sq.cycle_count (+) = ssl2.cycle_count
                 and sq.loc_Type (+) = ssl2.loc_type
                 and sq.location (+) = ssl2.location
                 and sq.item (+) = ssl2.item) d,
             stake_sku_loc ssl
       where ssl.cycle_count = I_cycle_count
         and ssl.loc_type = I_loc_type
         and ssl.location = d.location
         and ssl.item = d.head_item
         for update of ssl.item nowait;

BEGIN

   if I_loc_type = 'W' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_ORDERABLE_ITEM_WH',
                       'STK_XFORM_TEMP, WH, STK_SKU_LOC, STAKE_QTY ',
                       'CYCLE_COUNT:'||(I_cycle_count));
      open C_GET_ORDERABLE_ITEM_WH;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_ORDERABLE_ITEM_WH',
                       'STK_XFORM_TEMP, WH, STK_SKU_LOC, STAKE_QTY ',
                       'CYCLE_COUNT:'||(I_cycle_count));
      fetch C_GET_ORDERABLE_ITEM_WH BULK COLLECT into TBL_orderable,
                                                      TBL_loc,
                                                      TBL_parent_ratio,
                                                      TBL_sellable_qty,
                                                      TBL_old_sellable_qty,
                                                      TBL_loc_desc;
                                                      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_ORDERABLE_ITEM_WH',
                       'STK_XFORM_TEMP, WH, STK_SKU_LOC, STAKE_QTY ',
                       'CYCLE_COUNT:'||(I_cycle_count));
      close C_GET_ORDERABLE_ITEM_WH;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_ORDERABLE_ITEM',
                       'STK_XFORM_TEMP, STK_SKU_LOC, STAKE_QTY ',
                       'CYCLE_COUNT:'||(I_cycle_count));
      open C_GET_ORDERABLE_ITEM;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_ORDERABLE_ITEM',
                       'STK_XFORM_TEMP, STK_SKU_LOC, STAKE_QTY ',
                       'CYCLE_COUNT:'||(I_cycle_count));
      fetch C_GET_ORDERABLE_ITEM BULK COLLECT into TBL_orderable,
                                                   TBL_loc,
                                                   TBL_parent_ratio,
                                                   TBL_sellable_qty,
                                                   TBL_old_sellable_qty,
                                                   TBL_loc_desc;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_ORDERABLE_ITEM',
                       'STK_XFORM_TEMP, STK_SKU_LOC, STAKE_QTY ',
                       'CYCLE_COUNT:'||(I_cycle_count));
      close C_GET_ORDERABLE_ITEM;
   end if;

   if  TBL_orderable is NOT NULL and TBL_orderable.COUNT > 0 then
      FORALL i in  TBL_orderable.first..TBL_orderable.last
         update stake_sku_loc ssl
            set physical_count_qty = NVL(physical_count_qty,0) +
                                     ((NVL(TBL_parent_ratio(i),0)/100.0)*(NVL(TBL_sellable_qty(i),0)- NVL(TBL_old_sellable_qty(i),0)))
          where cycle_count        = I_cycle_count
            and item               = TBL_orderable(i)
            and loc_type           = I_loc_type
            and location           = TBL_loc(i);

      FORALL i in TBL_orderable.first..TBL_orderable.last
         merge into stake_qty sq
         using (select I_cycle_count cycle_count,
                       I_loc_type loc_type,
                       TBL_loc(i) location,
                       TBL_loc_desc(i) loc_desc,
                       TBL_orderable(i) orderable_item,
                       ((NVL(TBL_parent_ratio(i),0)/100.0)*(NVL(TBL_sellable_qty(i),0)- NVL(TBL_old_sellable_qty(i),0))) orderable_count_qty
                  from dual) d
         on (sq.cycle_count = d.cycle_count AND
             sq.loc_type = d.loc_type AND
             sq.location = d.location AND
             sq.item = d.orderable_item AND
             sq.location_desc = d.loc_desc)
         when matched then
            update set sq.qty = sq.qty + d.orderable_count_qty
         when NOT matched then
            insert values (d.cycle_count,
                           d.loc_type,
                           d.location,
                           d.orderable_item,
                           d.orderable_count_qty,
                           d.loc_desc);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table ,
                                            L_program,
                                            'NULL');
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROLLUP_SELLABLE_ONLY_ITEM;
------------------------------------------------------------------------------------------
FUNCTION STAKE_QTY_LOC_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist         IN OUT BOOLEAN,
                             I_cycle_count   IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_location      IN     ITEM_LOC.LOC%TYPE)
   return BOOLEAN is
   
   L_exists VARCHAR2(1) := NULL;

   cursor C_LOC_EXIST is
      select 'x'
        from stake_qty sq
       where sq.cycle_count = I_cycle_count
         and sq.location = I_location
         and rownum = 1;
         
BEGIN

   if I_cycle_count is NULL or I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', 
                                            'STKCOUNT_SQL.STAKE_QTY_LOC_EXIST',
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   ---   
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOC_EXIST',
                    'stake_qty',
                    'Location '||I_location);
   open C_LOC_EXIST;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOC_EXIST',
                    'stake_qty',
                    'Location '||I_location);
   fetch C_LOC_EXIST into L_exists;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOC_EXIST',
                    'stake_qty',
                    'Location '||I_location);
   close C_LOC_EXIST;
   ---
   if L_exists is not NULL then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
    --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.STAKE_QTY_LOC_EXIST',
                                            to_char(SQLCODE));
      return FALSE;
END STAKE_QTY_LOC_EXIST;
------------------------------------------------------------------------------------------
FUNCTION CHECK_LOCATION_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist          IN OUT BOOLEAN,
                               I_cycle_count    IN     STAKE_HEAD.CYCLE_COUNT%TYPE,
                               I_stocktake_date IN     STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                               I_location_type  IN     CODE_DETAIL.CODE%TYPE,
                               I_location       IN     VARCHAR2)
   return BOOLEAN is
   
   L_exists VARCHAR2(1) := NULL;
   
   cursor C_CHECK_WH_EXISTS is
      select 'x'
        from stake_head sh,
             stake_location sl
       where sh.cycle_count    = sl.cycle_count
         and sh.cycle_count   != I_cycle_count
         and sh.stocktake_date = I_stocktake_date
         and ((I_location_type = 'AW' 
               and sh.loc_type = 'W')
              or (I_location_type = 'W' 
                  and (sl.location = I_location
                       or exists (select 'x' 
                                    from wh 
                                   where physical_wh = I_location
                                     and wh = sl.location)))
              or (I_location_type = 'LLW'
                  and exists (select 'x'
                                from loc_list_detail lld,
                                     wh w,
                                     wh w2
                               where lld.loc_list        = I_location
                                 and lld.location        = w.wh
                                 and (sl.location        = w2.wh
                                      or sl.location     = w.wh)
                                 and (w2.physical_wh     = w.wh
                                      or w2.physical_wh  = w.physical_wh)
                                 and w2.stockholding_ind = 'Y'
                                 and lld.loc_type        = 'W')))
        and sh.delete_ind = 'N'
        and rownum = 1; 
        
   cursor C_CHECK_STORE_EXISTS is
      select 'x' 
        from(
             select sp.dept,
                    sp.class,
                    sp.subclass
               from stake_head sh,
                    stake_location sl,
                    stake_product sp
              where sh.cycle_count    = sl.cycle_count
                and sh.cycle_count   != I_cycle_count
                and sh.stocktake_date = I_stocktake_date
                and sl.cycle_count    = sp.cycle_count
                and ((I_location_type = 'AS' 
                and sh.loc_type = 'S')
                 or (I_location_type = 'S' 
                     and sl.location = I_location)
                 or (I_location_type = 'LLS' 
                     and EXISTS (select 'x'
                                   from loc_list_detail lld,
                                        store s
                                  where lld.loc_list        = I_location
                                    and lld.location        = s.store
                                    and lld.location        = sl.location
                                    and sl.loc_type         = 'S'
                                    and s.stockholding_ind  = 'Y'
                                    and lld.loc_type        = 'S'))
                 or (I_location_type = 'C'
                     and EXISTS (select 'x'
                                   from store s
                                  where sl.location   = s.store
                                    and s.store_class = I_location)))
                and sh.delete_ind = 'N') sp1,
             stake_product sp2
      where sp2.cycle_count =I_cycle_count
        and sp1.dept        = sp2.dept
        and NVL(sp1.class,NVL(sp2.class,'-999')) =NVL(sp2.class ,NVL(sp1.class,'-999'))
        and NVL(sp1.subclass,NVL(sp2.subclass,'-999')) =NVL(sp2.subclass ,NVL(sp1.subclass,'-999'))
        and rownum = 1;     
         
   cursor C_CHECK_FINISHER_EXISTS is
      select 'x' 
        from(
            select sp.dept,
                   sp.class,
                   sp.subclass
              from stake_head sh,
                   stake_location sl,
                   stake_product sp
             where sh.cycle_count    = sl.cycle_count
               and sh.cycle_count   != I_cycle_count
               and sh.stocktake_date = I_stocktake_date
               and sl.cycle_count    = sp.cycle_count
               and ((I_location_type = 'AE'
                     and sh.loc_type = 'E') 
                      or (I_location_type = 'E' 
                          and sl.location = I_location))
               and sh.delete_ind = 'N') sp1,
              stake_product sp2
       where sp2.cycle_count =I_cycle_count
         and sp1.dept        = sp2.dept
         and NVL(sp1.class,NVL(sp2.class,'-999')) =NVL(sp2.class ,NVL(sp1.class,'-999'))
         and NVL(sp1.subclass,NVL(sp2.subclass,'-999')) =NVL(sp2.subclass ,NVL(sp1.subclass,'-999'))
         and rownum = 1;

BEGIN

   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'STKCOUNT_SQL.CHECK_LOCATION_EXISTS',
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   if I_stocktake_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'STKCOUNT_SQL.CHECK_LOCATION_EXISTS',
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   if I_location_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'STKCOUNT_SQL.CHECK_LOCATION_EXISTS',
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_location_type = 'AW' or I_location_type = 'W' or I_location_type = 'LLW' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_WH_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      open C_CHECK_WH_EXISTS;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_WH_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      fetch C_CHECK_WH_EXISTS into L_exists;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_WH_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      close C_CHECK_WH_EXISTS;
   elsif I_location_type = 'AS' or I_location_type = 'S' or I_location_type = 'LLS' or I_location_type = 'C' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_STORE_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      open C_CHECK_STORE_EXISTS;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_STORE_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      fetch C_CHECK_STORE_EXISTS into L_exists;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_STORE_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      close C_CHECK_STORE_EXISTS;
   elsif I_location_type = 'AE' or I_location_type = 'E' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_FINISHER_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      open C_CHECK_FINISHER_EXISTS;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_FINISHER_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      fetch C_CHECK_FINISHER_EXISTS into L_exists;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_FINISHER_EXISTS',
                       'stake_head',
                       'Location '||I_location);
      close C_CHECK_FINISHER_EXISTS;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE', 
                                            'STKCOUNT_SQL.CHECK_LOCATION_EXISTS',
                                            NULL, 
                                            NULL);
      return FALSE;
      
   end if;
   ---
   if L_exists is not NULL then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;   
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.CHECK_LOCATION_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_LOCATION_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION GET_STAKE_OLD_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_old_qty         IN OUT   STAKE_QTY.QTY%TYPE,
                            I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE,
                            I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                            I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                            I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE)
   return BOOLEAN is
   
   L_program    VARCHAR2(40)   := 'STKCOUNT_SQL.GET_STAKE_OLD_QTY';
   
   cursor C_GET_OLD_QTY is
      select nvl(qty, 0)
        from stake_qty
       where item          = I_item
         and loc_type      = I_loc_type
         and location      = I_location
         and location_desc = I_loc_desc
         and cycle_count   = I_cycle_count;
 
BEGIN

   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', 
                                            L_program,
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   ---   
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_OLD_QTY',
                    'stake_qty',
                    'Cycle Count '||I_cycle_count);
   open C_GET_OLD_QTY;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_OLD_QTY',
                    'stake_qty',
                    'Cycle Count '||I_cycle_count);
   fetch C_GET_OLD_QTY into O_old_qty;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_OLD_QTY',
                    'stake_qty',
                    'Cycle Count '||I_cycle_count);
   close C_GET_OLD_QTY;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STAKE_OLD_QTY;
------------------------------------------------------------------------------------------
FUNCTION ROLLUP_SELLABLE_ONLY_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_old_qty         IN OUT   STAKE_QTY.QTY%TYPE,
                                    I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                    I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                    I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                    I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                    I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE)
   return BOOLEAN is

   L_program               VARCHAR2(40)         := 'STKCOUNT_SQL.ROLLUP_SELLABLE_ONLY_ITEM';
   L_table                 VARCHAR2(20)         := 'STAKE_SKU_LOC';
   L_dummy                 VARCHAR2(40)         := NULL;
   L_old_qty               STAKE_QTY.QTY%TYPE   := O_old_qty;
   L_sellable_qty          STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE;
   L_parent_ratio          ITEM_XFORM_DETAIL.ITEM_QUANTITY_PCT%TYPE;
   L_orderable_item        ITEM_XFORM_HEAD.HEAD_ITEM%TYPE;   
   L_orderable_count_qty   STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE;
   
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_locked, -54);

   cursor C_GET_SELLABLE_QTY is
      select nvl(qty, 0)
        from stake_qty
       where cycle_count   = I_cycle_count
         and item          = I_item
         and loc_type      = I_loc_type
         and location      = I_location
         and location_desc = I_loc_desc;

   cursor C_GET_ORDERABLE_ITEM is
      select ixh.head_item,
             NVL(ixd.yield_from_head_item_pct,100) yield_from_head_item_pct
        from item_xform_head ixh,
             item_xform_detail ixd,
             stake_sku_loc ssl
       where ixd.detail_item = I_item
         and ixh.item_xform_type = 'K'
         and ixh.item_xform_head_id = ixd.item_xform_head_id
         and ssl.item        = ixh.head_item
         and ssl.cycle_count = I_cycle_count
         and ssl.loc_type    = I_loc_type
         and ssl.location    = I_location;

   cursor C_LOCK_ORDERABLE_ITEM is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and item        = L_orderable_item
         and loc_type    = I_loc_type
         and location    = I_location
         for update nowait;

   cursor C_CHECK_STAKE_QTY is
      select 'x'
        from stake_qty
       where cycle_count   = I_cycle_count
         and item          = L_orderable_item
         and loc_type      = I_loc_type
         and location      = I_location
         and location_desc = I_loc_desc
         and rownum        = 1;

BEGIN     
   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', 
                                            L_program,
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   ---   
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SELLABLE_QTY',
                    'STAKE_QTY',
                    'SKU:'||(I_item));
   open C_GET_SELLABLE_QTY;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SELLABLE_QTY',
                    'STAKE_QTY',
                    'SKU:'||(I_item));
   fetch C_GET_SELLABLE_QTY into L_sellable_qty;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SELLABLE_QTY',
                    'STAKE_QTY',
                    'SKU:'||(I_item));
   close C_GET_SELLABLE_QTY;

   if L_old_qty is NULL then
      L_old_qty := 0;
   end if;

   FOR rec in C_GET_ORDERABLE_ITEM LOOP

      L_orderable_item      := rec.head_item;
      L_parent_ratio        := rec.yield_from_head_item_pct;
      L_orderable_count_qty := ((L_parent_ratio/100.0)*(L_sellable_qty - L_old_qty));

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ORDERABLE_ITEM',
                       'STAKE_SKU_LOC',
                       'SKU:'||(L_orderable_item));
      open C_LOCK_ORDERABLE_ITEM;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ORDERABLE_ITEM',
                       'STAKE_SKU_LOC',
                       'SKU:'||(L_orderable_item));
      close C_LOCK_ORDERABLE_ITEM;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'STAKE_SKU_LOC',
                       'SKU:'||(L_orderable_item));
                       
      update STAKE_SKU_LOC
         set physical_count_qty = NVL(physical_count_qty,0) + L_orderable_count_qty
       where cycle_count        = I_cycle_count
         and item               = L_orderable_item
         and loc_type           = I_loc_type
         and location           = I_location;

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_STAKE_QTY',
                       'STAKE_QTY',
                       'SKU:'||(L_orderable_item));
      open C_CHECK_STAKE_QTY;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_STAKE_QTY',
                       'STAKE_QTY',
                       'SKU:'||(L_orderable_item));
      fetch C_CHECK_STAKE_QTY into L_dummy;

      if C_CHECK_STAKE_QTY%NOTFOUND then
         insert into STAKE_QTY
                (cycle_count,
                 loc_type,
                 location,
                 item,
                 qty,
                 location_desc)
         values (I_cycle_count,
                 I_loc_type,
                 I_location,
                 L_orderable_item,
                 L_orderable_count_qty,
                 I_loc_desc) ;
      else
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'STAKE_QTY',
                          'SKU:'||(L_orderable_item));
   
         update STAKE_QTY
            set qty = qty + L_orderable_count_qty
          where cycle_count   = I_cycle_count
            and item          = L_orderable_item
            and loc_type      = I_loc_type
            and location      = I_location
            and location_desc = I_loc_desc;
      end if;
   
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_STAKE_QTY',
                       'STAKE_QTY',
                       'SKU:'||(L_orderable_item));
      close C_CHECK_STAKE_QTY;

   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ORDERABLE_ITEM%ISOPEN then
         close C_LOCK_ORDERABLE_ITEM;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table ,
                                            L_program,
                                            'NULL');
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROLLUP_SELLABLE_ONLY_ITEM;
------------------------------------------------------------------------------------------
FUNCTION GET_XFORM_ITEM_TYPE (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_xform_item_type   IN OUT   STAKE_SKU_LOC.XFORM_ITEM_TYPE%TYPE,
                              I_cycle_count       IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                              I_item              IN       ITEM_MASTER.ITEM%TYPE,
                              I_location          IN       STAKE_SKU_LOC.LOCATION%TYPE,
                              I_loc_type          IN       STAKE_SKU_LOC.LOC_TYPE%TYPE)
                              
   return BOOLEAN is

   L_program             VARCHAR2(40)   := 'STKCOUNT_SQL.GET_XFORM_ITEM_TYPE';
   
   cursor C_GET_XFORM_ITEM_TYPE is
      select xform_item_type
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and item        = I_item
         and loc_type    = I_loc_type
         and location    = I_location;

BEGIN

   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
  
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_XFORM_ITEM_TYPE',
                    'STAKE_SKU_LOC',
                    'SKU:'||(I_item));
   open C_GET_XFORM_ITEM_TYPE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_XFORM_ITEM_TYPE',
                    'STAKE_SKU_LOC',
                    'SKU:'||(I_item));
   fetch C_GET_XFORM_ITEM_TYPE into O_xform_item_type;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_XFORM_ITEM_TYPE',
                    'STAKE_SKU_LOC',
                    'SKU:'||(I_item));
   close C_GET_XFORM_ITEM_TYPE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_XFORM_ITEM_TYPE;
------------------------------------------------------------------------------------------
FUNCTION DELETE_STAKE_QTY_ITEM (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_xform_item_type    IN       STAKE_SKU_LOC.XFORM_ITEM_TYPE%TYPE,
                                I_loc_type           IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                I_cycle_count        IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                I_item               IN       ITEM_MASTER.ITEM%TYPE,
                                I_location           IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                I_loc_desc           IN       STAKE_QTY.LOCATION_DESC%TYPE)
return BOOLEAN is

   L_program          VARCHAR2(40)                     := 'STKCOUNT_SQL.DELETE_STAKE_QTY_ITEM';
   L_table            VARCHAR2(30)                     := 'STAKE_QTY / STAKE_SKU_LOC';
   L_orderable_item   ITEM_XFORM_HEAD.HEAD_ITEM%TYPE   := NULL;
   L_qty              STAKE_QTY.QTY%TYPE               := NULL;
   L_orderable_qty    STAKE_QTY.QTY%TYPE               := NULL;
   L_location         STAKE_QTY.LOCATION%TYPE          := NULL;
   
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_STAKE_QTY is
      select nvl(qty, 0)
        from stake_qty
       where item           = I_item
         and loc_type       = I_loc_type
         and location       = I_location
         and cycle_count    = I_cycle_count
         and ((location_desc is null and I_loc_desc is null) or
              (location_desc = I_loc_desc));

   --get stock count qty by virtual wh, which will be used to update stake_sku_loc by virtual wh
   cursor C_GET_STAKE_QTY_MC is
      select location,    --virtual wh on stake_qty
             nvl(qty, 0) qty
        from stake_qty,
             wh
       where item           = I_item
         and loc_type       = 'W'
         and location       = wh
         and physical_wh    = I_location
         and cycle_count    = I_cycle_count
         and ((location_desc is null and I_loc_desc is null) or
              (location_desc = I_loc_desc));

   cursor C_LOCK_STAKE_QTY is
      select 'x' 
        from stake_qty
       where item           = I_item
         and loc_type       = I_loc_type
         and location       = I_location
         and cycle_count    = I_cycle_count
         and ((location_desc is null and I_loc_desc is null) or
              (location_desc = I_loc_desc))
         for update of item nowait;

   cursor C_LOCK_STAKE_QTY_MC is
      select 'x' 
        from stake_qty sq,
             wh w
       where item           = I_item
         and loc_type       = 'W'
         and sq.location    = w.wh
         and w.physical_wh  = I_location
         and cycle_count    = I_cycle_count
         and ((location_desc is null and I_loc_desc is null) or
              (location_desc = I_loc_desc))
         for update of item nowait;

   cursor C_LOCK_STAKE_SKU_LOC(L_cur_item IN STAKE_SKU_LOC.ITEM%TYPE,
                               L_cur_loc  IN STAKE_SKU_LOC.LOCATION%TYPE) is
      select 'x' 
        from stake_sku_loc
       where item           = L_cur_item
         and loc_type       = I_loc_type
         and location       = L_cur_loc
         and cycle_count    = I_cycle_count
         for update of physical_count_qty nowait;

   cursor C_LOCK_STAKE_SKU_LOC_MC is
      select 'x' 
        from stake_sku_loc ssl,
             wh w
       where item           = I_item
         and ssl.loc_type   = 'W'
         and ssl.location   = w.wh
         and w.physical_wh  = I_location
         and cycle_count    = I_cycle_count
         for update of physical_count_qty nowait;

   --convert sellable-only transformed item's count qty to the orderable qty, accounting for yield percent 
   cursor C_GET_ORDERABLE_ITEM is
      select ixh.head_item,  --orderable
             NVL(sq.qty, 0)*(NVL(ixd.yield_from_head_item_pct, 100)/100) qty
        from item_xform_head ixh,
             item_xform_detail ixd,
             stake_qty sq
       where ixd.detail_item        = I_item  --sellable
         and ixh.item_xform_type    = 'K'
         and ixh.item_xform_head_id = ixd.item_xform_head_id
         and sq.item                = ixd.detail_item  --sellable's stake_qty
         and sq.cycle_count         = I_cycle_count
         and sq.loc_type            = I_loc_type
         and sq.location            = I_location
         and ((sq.location_desc is null and I_loc_desc is null) or
              (sq.location_desc = I_loc_desc));

   cursor C_GET_ORDERABLE_ITEM_MC is
      select sq.location,
             ixh.head_item,
             NVL(sq.qty, 0)*(NVL(ixd.yield_from_head_item_pct, 100)/100) qty
        from item_xform_head ixh,
             item_xform_detail ixd,
             stake_qty sq,
             wh w
       where ixd.detail_item        = I_item  --sellable
         and ixh.item_xform_type    = 'K'
         and ixh.item_xform_head_id = ixd.item_xform_head_id
         and sq.item                = ixd.detail_item  --sellable's stake_qty
         and sq.cycle_count         = I_cycle_count
         and sq.loc_type            = 'W'
         and sq.location            = w.wh
         and w.physical_wh          = I_location
         and ((sq.location_desc is null and I_loc_desc is null) or
              (sq.location_desc = I_loc_desc));
 
BEGIN

   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   
   if I_loc_type = 'W' then
      if I_xform_item_type = 'S' then
         ---
         -- update the sellable-only tranformed item's physical count qty to 0 for all virtual whs
         open C_LOCK_STAKE_SKU_LOC_MC;
         close C_LOCK_STAKE_SKU_LOC_MC;
         ---
         update stake_sku_loc
            set physical_count_qty = 0
          where item          = I_item
            and loc_type      = I_loc_type
            and cycle_count   = I_cycle_count
            and exists (select wh 
                          from wh 
                         where wh = location
                           and loc_type = 'W'
                           and physical_wh = I_location
                           and rownum = 1);

         -- decrement the orderable's physical count qty at virtual wh level      
         FOR rec in C_GET_ORDERABLE_ITEM_MC LOOP
            L_location       := rec.location;     
            L_orderable_item := rec.head_item;
            L_orderable_qty  := rec.qty;
            ---
            open C_LOCK_STAKE_SKU_LOC(L_orderable_item, L_location);
            close C_LOCK_STAKE_SKU_LOC;
            ---
            update STAKE_SKU_LOC
               set physical_count_qty = NVL(physical_count_qty,0) - NVL(L_orderable_qty,0)
             where cycle_count        = I_cycle_count
               and item               = L_orderable_item
               and loc_type           = I_loc_type
               and location           = L_location;
         END LOOP;
         ---
         open C_LOCK_STAKE_QTY_MC;
         close C_LOCK_STAKE_QTY_MC;
         ---
         delete from stake_qty sq 
          where item          = I_item
            and loc_type      = I_loc_type
            and ((location_desc is null and I_loc_desc is null) or
                 (location_desc = I_loc_desc))
            and cycle_count   = I_cycle_count
            and exists (select wh 
                          from wh 
                         where wh.wh       = sq.location
                           and sq.loc_type = 'W'
                           and physical_wh = I_location
                           and rownum = 1);
         
      else --- I_xform_item_type = 'O'
         ---
         --lock all records associated with the physical wh at once
         open C_LOCK_STAKE_SKU_LOC_MC;
         close C_LOCK_STAKE_SKU_LOC_MC;

         -- decrement the physical count qty at virtual wh level      
         for rec in C_GET_STAKE_QTY_MC loop
            L_location := rec.location;
            L_qty := rec.qty;
            ---
            update stake_sku_loc ssl
               set physical_count_qty = NVL(physical_count_qty,0) - NVL(L_qty,0)
             where item          = I_item
               and loc_type      = I_loc_type
               and location      = L_location  --update by virual wh
               and cycle_count   = I_cycle_count;
         end loop;
         ---
         open C_LOCK_STAKE_QTY_MC;
         close C_LOCK_STAKE_QTY_MC;
         ---
         delete from stake_qty sq 
          where item          = I_item
            and loc_type      = I_loc_type
            and cycle_count   = I_cycle_count
            and ((location_desc is null and I_loc_desc is null) or
                 (location_desc = I_loc_desc))
            and exists (select wh 
                          from wh 
                         where wh.wh = sq.location
                           and sq.loc_type = 'W'
                           and physical_wh = I_location
                           and rownum = 1);                           
      end if;
   else --- I_loc_type = 'S'
      if I_xform_item_type = 'S' then
         ---
         -- update the sellable's physical count qty to 0
         open C_LOCK_STAKE_SKU_LOC(I_item, I_location);
         close C_LOCK_STAKE_SKU_LOC;
         ---
         update stake_sku_loc
            set physical_count_qty = 0
          where item          = I_item
            and loc_type      = I_loc_type
            and location      = I_location
            and cycle_count   = I_cycle_count;

         -- update the orderable's physical count qty
         FOR rec in C_GET_ORDERABLE_ITEM LOOP
            L_orderable_item := rec.head_item;
            L_orderable_qty  := rec.qty;
            ---         
            open C_LOCK_STAKE_SKU_LOC(L_orderable_item, I_location);
            close C_LOCK_STAKE_SKU_LOC;
            ---
            update STAKE_SKU_LOC
               set physical_count_qty = NVL(physical_count_qty,0) - NVL(L_orderable_qty,0)
             where item               = L_orderable_item
               and loc_type           = I_loc_type
               and location           = I_location
               and cycle_count        = I_cycle_count;
         END LOOP;
        
         --delete stake_qty for the sellable
         open C_LOCK_STAKE_QTY;
         close C_LOCK_STAKE_QTY;
         ---
         delete from stake_qty 
          where item = I_item
            and loc_type = I_loc_type
            and location = I_location
            and cycle_count = I_cycle_count
            and ((location_desc is null and I_loc_desc is null) or
                 (location_desc = I_loc_desc));

      else --- I_xform_item_type = 'O'
         ---
         open C_GET_STAKE_QTY;
         fetch C_GET_STAKE_QTY into L_qty;
         close C_GET_STAKE_QTY;
         ---
         open C_LOCK_STAKE_SKU_LOC(I_item, I_location);
         close C_LOCK_STAKE_SKU_LOC;
         ---
         update stake_sku_loc
            set physical_count_qty = NVL(physical_count_qty,0) - NVL(L_qty,0)
          where item          = I_item
            and loc_type      = I_loc_type
            and location      = I_location
            and cycle_count   = I_cycle_count;
         ---
         open C_LOCK_STAKE_QTY;
         close C_LOCK_STAKE_QTY;
         ---
         delete from stake_qty 
          where item          = I_item
            and loc_type      = I_loc_type
            and location      = I_location
            and cycle_count   = I_cycle_count
            and ((location_desc is null and I_loc_desc is null) or
                 (location_desc = I_loc_desc));
      end if;
   end if;
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table ,
                                            L_program,
                                            'NULL');
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_STAKE_QTY_ITEM;
------------------------------------------------------------------------------------------
FUNCTION CHECK_STK_COUNT_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exist          IN OUT BOOLEAN,
                                O_cycle_count    IN OUT STAKE_HEAD.CYCLE_COUNT%TYPE,
                                I_cycle_count    IN     STAKE_HEAD.CYCLE_COUNT%TYPE,
                                I_stocktake_date IN     STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                                I_location_type  IN     CODE_DETAIL.CODE%TYPE,
                                I_location       IN     VARCHAR2)
   return BOOLEAN is
   
   L_exists VARCHAR2(1) := NULL;
   
   cursor C_CHECK_WH_EXISTS is
      select sh.cycle_count
        from stake_head sh,
             stake_location sl
       where sh.cycle_count    = sl.cycle_count
         and sh.cycle_count   != I_cycle_count
         and sh.stocktake_date = I_stocktake_date
         and sl.location       = I_location
         and sl.loc_type       = I_location_type
         and sh.delete_ind     = 'N';

BEGIN

   if I_cycle_count is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'STKCOUNT_SQL.CHECK_STK_COUNT_EXISTS',
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   if I_stocktake_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'STKCOUNT_SQL.CHECK_STK_COUNT_EXISTS',
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   if I_location_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'STKCOUNT_SQL.CHECK_STK_COUNT_EXISTS',
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'STKCOUNT_SQL.CHECK_STK_COUNT_EXISTS',
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_WH_EXISTS',
                    'stake_head',
                    'Location '||I_location);
   open C_CHECK_WH_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_WH_EXISTS',
                    'stake_head',
                    'Location '||I_location);
   fetch C_CHECK_WH_EXISTS into O_cycle_count;
   ---
   if C_CHECK_WH_EXISTS%FOUND then
      O_exist := TRUE;   
   else
      O_exist := FALSE; 
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_WH_EXISTS',
                    'stake_head',
                    'Location '||I_location);
   close C_CHECK_WH_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKCOUNT_SQL.CHECK_STK_COUNT_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_STK_COUNT_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_STAKE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                                 I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                 I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                 I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_count_qty       IN       STAKE_QTY.QTY%TYPE,
                                 I_old_count_qty   IN       STAKE_QTY.QTY%TYPE,
                                 I_total_qty       IN       STAKE_QTY.QTY%TYPE)
   return BOOLEAN IS
   
   L_program           VARCHAR2(64) := 'STKCOUNT_SQL.INSERT_UPDATE_STAKE_QTY';
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_dummy             VARCHAR2(1) := 'N';
   L_stockholding_ind  WH.STOCKHOLDING_IND%TYPE;
   L_table             VARCHAR(30) := 'STAKE_QTY';
   Record_Locked       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_exist             BOOLEAN;
   L_old_qty           STAKE_QTY.QTY%TYPE;
   
   cursor C_CHECK_STOCK is
      select 'x'
       from stake_qty
      where item           = I_item
        and loc_type       = I_loc_type
        and location       = I_location
        and (location_desc = I_loc_desc
         or (location_desc is NULL
        and I_loc_desc is NULL))
        and cycle_count    = I_cycle_count
        for update of item nowait;
BEGIN

   if I_count_qty is NOT NULL then
      ---  
      if I_loc_type = 'W' then
         ---------------------------------------------------------------------------------
         -- If the stock count distribution has been modified using Virtual WH Distribution screen. 
         -- Swap the DISTRIBUTE_QTY values with the SNAPSHOT_ON_HAND_QTY values so the routine 
         -- distributes the count qty according to the updated values.
         -- ---------------------------------------------------------------------------------
         if STKCOUNT_SQL.SWAP_SNAPSHOT_DISTRIBUTE(L_error_message,
                                                  I_item,
                                                  I_location,
                                                  I_cycle_count)= FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if I_old_count_qty <> I_count_qty then
            if I_loc_desc is not NULL then
               if STKCOUNT_SQL.STOCK_DISTRIBUTION(L_error_message,
                                                  I_cycle_count,
                                                  I_location,
                                                  I_item,
                                                  I_count_qty,
                                                  I_loc_desc)= FALSE then
                   O_error_message := L_error_message;
                   return false;
               end if;
            end if;
         end if;      
         -- ---------------------------------------------------------------------------------
         -- Now swap them back so the original SNAPSHOT_ON_HAND_QTY will be displayed
         -- ---------------------------------------------------------------------------------
         if STKCOUNT_SQL.SWAP_SNAPSHOT_DISTRIBUTE(L_error_message,
                                                  I_item,
                                                  I_location,
                                                  I_cycle_count)= FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
      else -- Single Channel or store
         open  C_CHECK_STOCK;
         fetch C_CHECK_STOCK into L_dummy;
         close C_CHECK_STOCK;
         if (I_loc_desc is not NULL) then
            if L_dummy = 'x' then
               ---
               if I_count_qty is NOT NULL then                 
                  if STKCOUNT_SQL.GET_STAKE_OLD_QTY(L_error_message,
                                                    L_old_qty,
                                                    I_cycle_count,
                                                    I_item,
                                                    I_location,
                                                    I_loc_type,
                                                    I_loc_desc)= FALSE then
                     O_error_message := L_error_message;
                     return false;
                  end if;
               end if;
               ---
               update stake_qty
                  set qty           = I_count_qty
                where item          = I_item
                  and loc_type      = I_loc_type
                  and location      = I_location
                  and location_desc = I_loc_desc
                  and cycle_count   = I_cycle_count;
               ---
               if I_count_qty is NOT NULL then
                  if STKCOUNT_SQL.ROLLUP_SELLABLE_ONLY_ITEM(L_error_message,
                                                            L_old_qty,
                                                            I_cycle_count,
                                                            I_item,
                                                            I_location,
                                                            I_loc_type,
                                                            I_loc_desc) = FALSE then
                     O_error_message := L_error_message;
                     return false;
                  end if;
               end if;
               ---
               L_old_qty := NULL;
               ---
            else

               if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(L_error_message,
                                                    I_item,
                                                    I_location,
                                                    L_exist) = FALSE THEN
                  O_error_message := L_error_message;
                  return false;
               end if;
               ---
               if L_exist = FALSE then
                  if STKCOUNT_SQL.NEW_STKCNT_ITEM_LOC(L_error_message,
                                                      I_cycle_count,
                                                      I_location,
                                                      I_loc_type,
                                                      I_item,
                                                      I_total_qty) = FALSE then
                     O_error_message := L_error_message;
                     return false;
                  end if;
               elsif L_exist = TRUE then
                  if STKCOUNT_SQL.STAKE_SKU_LOC_EXIST(L_error_message,
                                                      I_cycle_count,
                                                      I_loc_type,
                                                      I_location,
                                                      I_item,
                                                      I_total_qty) = FALSE then
                     O_error_message := L_error_message;
                     return false;
                  end if;
               end if;

               insert into stake_qty(cycle_count,
                                     loc_type,
                                     location,
                                     item,
                                     qty,
                                     location_desc)
                               values(I_cycle_count,
                                      I_loc_type,
                                      I_location,
                                      I_item,
                                      I_count_qty,
                                      I_loc_desc);
            end if;
         elsif (I_count_qty is not NULL) then
            ---
            ---- Update to the Null location desc.
            ---- The only way to have a Null description is
            ---- through batch and only updates are allowed.
            ---
            if I_count_qty is NOT NULL then                 
               if STKCOUNT_SQL.GET_STAKE_OLD_QTY(L_error_message,
                                                 L_old_qty,
                                                 I_cycle_count,
                                                 I_item,
                                                 I_location,
                                                 I_loc_type,
                                                 I_loc_desc)= FALSE then
                  O_error_message := L_error_message;
                  return false;
               end if;
            end if;
            ---
            update stake_qty
               set qty           = I_count_qty
             where item          = I_item
               and loc_type      = I_loc_type
               and location      = I_location
               and location_desc is NULL
               and cycle_count   = I_cycle_count;
            ---
            if I_count_qty is NOT NULL then
               if STKCOUNT_SQL.ROLLUP_SELLABLE_ONLY_ITEM(L_error_message,
                                                         L_old_qty,
                                                         I_cycle_count,
                                                         I_item,
                                                         I_location,
                                                         I_loc_type,
                                                         I_loc_desc) = FALSE then
                  O_error_message := L_error_message;
                  return false;
               end if;
            end if;
            ---
            L_old_qty := NULL;
            ---
         end if;

         ---
         if STKCOUNT_SQL.UPDATE_STAKE_SKU_LOC(L_error_message,
                                              I_cycle_count,
                                              I_loc_type,
                                              I_location,
                                              I_loc_desc,
                                              I_item,
                                              I_count_qty) = FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
         ---
      end if;
   end if;
   
   return true;
   
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_cycle_count),
                                             to_char(SQLCODE));
      return FALSE;

   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_UPDATE_STAKE_QTY;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_SKU_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                              I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                              I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                              I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_count_qty       IN       STAKE_QTY.QTY%TYPE)
return BOOLEAN IS
   L_program         VARCHAR2(64) := 'STKCOUNT_SQL.UPDATE_STAKE_SKU_LOC';
   L_sum             STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE := 0;
   L_physical_count  STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE := 0;
   L_item            STAKE_SKU_LOC.ITEM%TYPE               := I_item;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_pack_ind        ITEM_MASTER.PACK_IND%TYPE;
   L_dummy1          ITEM_MASTER.SELLABLE_IND%TYPE;
   L_dummy2          ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_dummy3          ITEM_MASTER.PACK_TYPE%TYPE;

   Record_Locked     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   L_table           VARCHAR2(15)      := 'STAKE_SKU_LOC';

   cursor C_QTY is
      select nvl(sum(nvl(qty,0)),0)
        from stake_qty
       where cycle_count   = I_cycle_count
         and item          = L_item
         and loc_type      = I_loc_type
         and location      = I_location;

   cursor C_CMPNT_SKU is
      select pq.item,
             pq.qty
        from v_packsku_qty pq,
             stake_sku_loc sl
       where pq.pack_no     = L_item
         and pq.item        = sl.item
         and sl.loc_type    = I_loc_type
         and sl.location    = I_location
         and sl.cycle_count = I_cycle_count
         for update of pack_comp_qty nowait;

   cursor C_PHYSICAL_COUNT is
      select nvl(physical_count_qty,0)
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and item        = L_item
         and loc_type    = I_loc_type
         and location    = I_location;

   cursor C_LOCK_STAKE_SKU_LOC is
      select 'x'
        from stake_sku_loc
       where cycle_count = I_cycle_count
         and item        = L_item
         and loc_type    = I_loc_type
         and location    = I_location
         for update of item nowait;
BEGIN
   if I_count_qty is NOT NULL then
      if NOT (I_loc_type = 'W' ) then

         open  C_QTY;
         fetch C_QTY into L_sum;
         close C_QTY;

         open C_PHYSICAL_COUNT;
         fetch C_PHYSICAL_COUNT into L_physical_count;
         close C_PHYSICAL_COUNT;

         --- Locking record before updating
         open C_LOCK_STAKE_SKU_LOC;
         close C_LOCK_STAKE_SKU_LOC;

         update stake_sku_loc
            set physical_count_qty = L_sum
          where cycle_count        = I_cycle_count
            and item               = L_item
            and loc_type           = I_loc_type
            and location           = I_location;
           ---
           
         if ITEM_ATTRIB_SQL.GET_PACK_INDS(L_error_message,
                                          L_pack_ind,
                                          L_dummy1,
                                          L_dummy2,
                                          L_dummy3,
                                          L_item) = FALSE then
            O_error_message := L_error_message;
         end if;

         --  If ITEM is a pack item, update stake_sku_loc.pack_comp_qty
         if L_pack_ind = 'Y' then
            FOR c_rec in C_CMPNT_SKU LOOP
               L_item := c_rec.item;
               --- Locking record prior to updating
               open C_LOCK_STAKE_SKU_LOC;
               close C_LOCK_STAKE_SKU_LOC;

               update STAKE_SKU_LOC
                  set stake_sku_loc.physical_count_qty = nvl(stake_sku_loc.physical_count_qty,0) +
                        ((L_sum - L_physical_count)*c_rec.qty)
                where current of C_CMPNT_SKU;

               ---
               if SQL%NOTFOUND then
                  NULL;
               end if;
              ----
            END LOOP;
         end if;
      end if;
      ---
      if I_count_qty is NOT NULL then
         if STKCOUNT_SQL.ROLLUP_SELLABLE_ONLY_ITEM(L_error_message,
                                                   I_cycle_count,
                                                   L_item,
                                                   I_location,
                                                   I_loc_type,
                                                   I_loc_desc) = FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
      end if;
      ---
   end if;

   return true;
 
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_cycle_count),
                                             to_char(SQLCODE));
      return FALSE;

   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_STAKE_SKU_LOC;
-------------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_UPDATE_STAKE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                                 I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                 I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                 I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_count_qty       IN       STAKE_QTY.QTY%TYPE)
return BOOLEAN IS

   L_program           VARCHAR2(64)                := 'STKCOUNT_SQL.DELETE_UPDATE_STAKE_QTY';
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_item              STAKE_QTY.ITEM%TYPE         := I_item;
   L_loc_type          STAKE_QTY.LOC_TYPE%TYPE     := I_loc_type;
   L_location          STAKE_QTY.LOCATION%TYPE     := I_location;
   L_cycle_count       STAKE_QTY.CYCLE_COUNT%TYPE  := I_cycle_count;
   L_total_qty         STAKE_QTY.QTY%TYPE          := 0;
   
   L_xform_item_type   STAKE_SKU_LOC.XFORM_ITEM_TYPE%TYPE := NULL;
   L_loc_desc          STAKE_QTY.LOCATION_DESC%TYPE       := I_loc_desc;
   
   L_table         VARCHAR2(30)               := 'STAKE_QTY';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   
   cursor C_LOCK_STAKE_QTY is
      select 'x' 
        from stake_qty
       where item           = L_item
         and loc_type       = L_loc_type
         and location       = L_location
         and cycle_count    = L_cycle_count
         and ((location_desc is null and I_loc_desc is null) or
              (location_desc = I_loc_desc))
         for update of item nowait;

   cursor C_LOCK_STAKE_QTY_MC is
      select 'x' 
        from stake_qty sq,
             wh w
       where item           = L_item
         and loc_type       = 'W'
         and sq.location    = w.wh
         and w.physical_wh  = L_location
         and cycle_count    = L_cycle_count
         and ((location_desc is null and I_loc_desc is null) or
              (location_desc = I_loc_desc))
         for update of item nowait;

   cursor C_LOCK_STAKE_SKU_LOC is
      select 'x' 
        from stake_sku_loc
       where item           = L_item
         and loc_type       = L_loc_type
         and location       = L_location
         and cycle_count    = L_cycle_count
         for update of physical_count_qty nowait;

   cursor C_LOCK_STAKE_SKU_LOC_MC is
      select 'x' 
        from stake_sku_loc ssl,
             wh w
       where item           = L_item
         and ssl.loc_type   = 'W'
         and ssl.location   = w.wh
         and w.physical_wh  = L_location
         and cycle_count    = L_cycle_count
         for update of physical_count_qty nowait;

BEGIN

   if L_loc_type = 'I' then
      L_loc_type := 'W';
   end if;
   ---
   if I_loc_type = 'W' then 
      ---
      if I_count_qty is NOT NULL then
         if STKCOUNT_SQL.GET_XFORM_ITEM_TYPE (L_error_message,
                                              L_xform_item_type,
                                              L_cycle_count,
                                              L_item,
                                              L_location,
                                              L_loc_type)= FALSE then
            O_error_message := L_error_message;
            return false;
         end if;

         if STKCOUNT_SQL.DELETE_STAKE_QTY_ITEM (L_error_message,
                                                L_xform_item_type,
                                                L_loc_type,
                                                L_cycle_count,
                                                L_item,
                                                L_location,
                                                L_loc_desc)= FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
      else
         open  C_LOCK_STAKE_QTY_MC;
         close C_LOCK_STAKE_QTY_MC;
         ---
         delete from stake_qty sq 
          where item          = L_item
            and loc_type      = L_loc_type
            and location_desc = L_loc_desc
            and cycle_count   = L_cycle_count
            and exists( select wh 
                          from wh 
                         where wh.wh       = sq.location
                           and sq.loc_type = 'W'
                           and physical_wh = L_location
                           and rownum = 1);
         
         open  C_LOCK_STAKE_SKU_LOC_MC;
         close C_LOCK_STAKE_SKU_LOC_MC;
         ---
         update stake_sku_loc ssl
            set physical_count_qty = 0
          where item          = L_item
            and loc_type      = L_loc_type
            and cycle_count   = L_cycle_count
            and exists( select wh 
                          from wh 
                         where wh.wh        = ssl.location
                           and ssl.loc_type = 'W'
                           and physical_wh  = L_location
                           and rownum = 1);
      end if;             
   else --Single Channel or Store
      if I_count_qty is NOT NULL then
         if STKCOUNT_SQL.GET_XFORM_ITEM_TYPE (L_error_message,
                                              L_xform_item_type,
                                              L_cycle_count,
                                              L_item,
                                              L_location,
                                              L_loc_type)= FALSE then
            O_error_message := L_error_message;
            return false;
         end if;

         if STKCOUNT_SQL.DELETE_STAKE_QTY_ITEM (L_error_message,
                                                L_xform_item_type,
                                                L_loc_type,
                                                L_cycle_count,
                                                L_item,
                                                L_location,
                                                L_loc_desc) = FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
      else
         open  C_LOCK_STAKE_QTY;
         close C_LOCK_STAKE_QTY;
         ---
         delete from stake_qty 
          where item          = L_item
            and loc_type      = L_loc_type
            and location      = L_location
            and ((location_desc is null and I_loc_desc is null) or
                 (location_desc = I_loc_desc))
            and cycle_count     = L_cycle_count;

         open  C_LOCK_STAKE_SKU_LOC;
         close C_LOCK_STAKE_SKU_LOC;
         ---
         update stake_sku_loc
            set physical_count_qty = 0
          where item          = L_item
            and loc_type      = L_loc_type
            and location      = L_location
            and cycle_count   = L_cycle_count;
      end if;
   end if;

   return true;

EXCEPTION

   when RECORD_LOCKED then
      ---
      if C_LOCK_STAKE_QTY%ISOPEN then
         close C_LOCK_STAKE_QTY;
      end if;
      ---
      if C_LOCK_STAKE_QTY_MC%ISOPEN then
         close C_LOCK_STAKE_QTY;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_cycle_count),
                                             to_char(SQLCODE));
      return false;

   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return false;
END DELETE_UPDATE_STAKE_QTY;
---------------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_STAKE_CONT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_location        IN       ITEM_LOC.LOC%TYPE,
                           I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                           I_run_type        IN       STAKE_CONT.RUN_TYPE%TYPE,
                           I_user_id         IN       STAKE_CONT.USER_ID%TYPE)
return BOOLEAN IS

   L_program    TRAN_DATA.PGM_NAME%TYPE  := 'STKCOUNT_SQL.INSERT_STAKE_CONT';

BEGIN

      insert into stake_cont(item,
                             loc_type,
                             location,
                             cycle_count,
                             run_type,
                             user_id)
                      select ssl.item,
                             I_loc_type,
                             I_location,
                             I_cycle_count,
                             I_run_type,
                             I_user_id
                        from stake_sku_loc ssl
                       where ssl.cycle_count = I_cycle_count
                         and ssl.loc_type = I_loc_type
                         and ssl.location = I_location
                         and ssl.processed = 'N'
                         and NOT EXISTS (select 'x'
                                           from stake_cont sc
                                          where sc.cycle_count = ssl.cycle_count
                                            and sc.loc_type = ssl.loc_type
                                            and sc.location = ssl.location
                                            and sc.item = ssl.item
                                            and sc.user_id = I_user_id
                                            and sc.run_type = I_run_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_STAKE_CONT;
---------------------------------------------------------------------------------------------------------------------------
END STKCOUNT_SQL;
/
