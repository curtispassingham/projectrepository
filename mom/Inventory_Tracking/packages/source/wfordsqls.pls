CREATE OR REPLACE PACKAGE WF_ORDER_SQL AUTHID CURRENT_USER AS
TYPE item_tab         is TABLE OF SHIPSKU.ITEM%TYPE INDEX BY BINARY_INTEGER;
--------------------------------------------------------------------------------
-- Name:    GET_CREDIT
-- Purpose: This function is used for checking the customer credit
--          is good or Bad
--------------------------------------------------------------------------------
FUNCTION GET_CREDIT ( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_credit_ind     IN OUT  WF_CUSTOMER.CREDIT_IND%TYPE,
                      I_customer_id    IN      WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name   : GET_LEAD_TIME
-- Purpose: This function is used to get the lead time.
--------------------------------------------------------------------------------
FUNCTION GET_LEAD_TIME (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_lead_time         IN OUT   REPL_ITEM_LOC.WH_LEAD_TIME%TYPE,
                        I_item              IN       WF_ORDER_DETAIL.ITEM%TYPE,
                        I_cust_loc          IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                        I_source_loc_id     IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                        I_source_loc_type   IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    VERIFY_REPL_INFO
-- Purpose: This function will be used to check if the item is in store order
--          replenishment.
--------------------------------------------------------------------------------
FUNCTION VERIFY_REPL_INFO (O_error_message  IN OUT  VARCHAR2,
                           I_store          IN      STORE_ORDERS.STORE%TYPE,
                           I_item           IN      STORE_ORDERS.ITEM%TYPE,
                           I_not_after_date IN      WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE,
                           I_source_wh      IN      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_FREIGHT_OTHER_CHRG
-- Purpose: This function is used for getting the freight and other charges
--          for the wholsale franchise orders
--------------------------------------------------------------------------------
FUNCTION GET_FREIGHT_OTHER_CHRG ( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_freight_chrg   IN OUT  WF_ORDER_HEAD.FREIGHT%TYPE,
                                  O_other_chrg     IN OUT  WF_ORDER_HEAD.OTHER_CHARGES%TYPE,
                                  I_order_no       IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    POP_FREIGHT_OTHER_CHRG
-- Purpose: This function is used for updating the freight and other charges
--          for the wholsale franchise orders
--------------------------------------------------------------------------------
FUNCTION POP_FREIGHT_OTHER_CHRG ( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_freight_chrg   IN      WF_ORDER_HEAD.FREIGHT%TYPE,
                                  I_other_chrg     IN      WF_ORDER_HEAD.OTHER_CHARGES%TYPE,
                                  I_order_no       IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    NEXT_WF_ORDER_NO
-- Purpose: This function is used for generating new Wholsale Franchise order
--          sequence
--------------------------------------------------------------------------------
FUNCTION NEXT_WF_ORDER_NO ( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_wf_order_no    IN OUT  WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_CUSTOMER_REF_NO
-- Purpose: This function is used for validating the customer refrence number
--          WF_ORDER_EXISTS
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CUSTOMER_REF_NO ( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists         IN OUT  BOOLEAN,
                                    I_ord_ref_no     IN      WF_ORDER_HEAD.CUST_ORD_REF_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    WF_ORDER_EXISTS
-- Purpose: This function checks for the existance of Wholsale Franchise order
--          in the system for the customer loc,wh and need date combination(order
--          other than the one being created).
--------------------------------------------------------------------------------
FUNCTION WF_ORDER_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT   BOOLEAN,
                          O_exist_order_no   IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                          I_order_no         IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                          I_cust_loc         IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                          I_source_loc_type  IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                          I_source_loc_id    IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                          I_need_date        IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
                          I_item             IN       WF_ORDER_DETAIL.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    WF_ORDER_EXISTS
-- Purpose: This function checks for the existance of Wholsale Franchise order
--          in the system for the input parameter as customer loc,wh and need date.
--------------------------------------------------------------------------------
FUNCTION WF_ORDER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_cust_loc        IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                         I_source_loc_type IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                         I_source_loc_id   IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                         I_need_date       IN       WF_ORDER_DETAIL.NEED_DATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    WF_ORDER_EXISTS
-- Purpose: This overloading function checks for the existance of Wholsale Franchise order
--          in the system.for the order number.
--------------------------------------------------------------------------------
FUNCTION WF_ORDER_EXISTS ( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT  BOOLEAN,
                           I_order_no       IN      WF_ORDER_DETAIL.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    APPROVE_WF_ORDERS
-- Purpose: This function will vaidate the Wholsale Franchise order before
--          approving the order.This function is called from WF order form.
--------------------------------------------------------------------------------
FUNCTION APPROVE_WF_ORDERS ( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no       IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_COST_INFORMATION
-- Purpose: This function will function will populate the cost information
--           to the WF_ORDER_DETAIL table
--------------------------------------------------------------------------------
FUNCTION GET_COST_INFORMATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_templ_id        IN OUT   WF_COST_RELATIONSHIP.TEMPL_ID%TYPE,
                              O_templ_desc      IN OUT   WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE,
                              O_margin_pct      IN OUT   WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE,
                              O_calc_type       IN OUT   WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE,
                              O_cust_cost       IN OUT   FUTURE_COST.PRICING_COST%TYPE,
                              O_acqui_cost      IN OUT   FUTURE_COST.ACQUISITION_COST%TYPE,
                              O_currency_code   IN OUT   FUTURE_COST.CURRENCY_CODE%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_wf_store        IN       STORE.STORE%TYPE,
                              I_source_loc_type IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE DEFAULT NULL,
                              I_source_loc_id   IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE DEFAULT NULL,
                              I_recalc_order    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_EXISTING_ORDER_COST_INFO
-- Purpose: This function will function will be used to show the cost information
--          from the WF_ORDER_DETAIL table
--------------------------------------------------------------------------------
FUNCTION GET_EXISTING_ORDER_COST_INFO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_margin_pct      IN OUT   WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE,
                                       O_cust_cost       IN OUT   FUTURE_COST.PRICING_COST%TYPE,
                                       O_acqui_cost      IN OUT   FUTURE_COST.ACQUISITION_COST%TYPE,
                                       I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                       I_item            IN       STORE_ORDERS.ITEM%TYPE,
                                       I_store           IN       STORE_ORDERS.STORE%TYPE,
                                       I_source_loc_type IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                       I_source_loc_id   IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                                       I_need_date       IN       WF_ORDER_DETAIL.NEED_DATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_UPCHARGE_INFORMATION
-- Purpose: This function will populate the upcharge information to the
--          WF_ORDER_EXP table
--------------------------------------------------------------------------------
FUNCTION GET_UPCHARGE_INFORMATION(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_order_no           IN      WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                                  I_order_cur          IN      CURRENCY_RATES.CURRENCY_CODE%TYPE,
                                  I_item               IN      WF_ORDER_DETAIL.ITEM%TYPE,
                                  I_source_loc_type    IN      WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                  I_source_loc_id      IN      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                                  I_customer_loc       IN      WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_TOTAL_UPCHARGE
-- Purpose: This function will populate the upcharge information to the
-- WF_ORDER_EXP table
--------------------------------------------------------------------------------
FUNCTION GET_TOTAL_UPCHARGE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_total_upcharge    IN OUT   WF_ORDER_EXP.EST_UPCHARGE_AMT%TYPE,
                            I_order_no          IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                            I_source_loc_type   IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                            I_source_loc_id     IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                            I_item              IN       WF_ORDER_DETAIL.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

-- Name:    TOTAL_ORDER_CUSTOMER_COST
-- Purpose: This function will be used to calculate the
--            Total acquisition cost for order and primary currency
--            Total customer cost excluding VAT for order and primary currency
--            Total customer cost including VAT for order and primary currency
--            Total margin $ for order and primary currency. The calculation will
--             be based on the cost template settings and summed up for a total
--             dollar amount
--------------------------------------------------------------------------------
FUNCTION TOTAL_ORDER_CUSTOMER_COST ( O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_tot_acq_cost            IN OUT  WF_ORDER_DETAIL.ACQUISITION_COST%TYPE,
                                     O_tot_acq_cost_pri        IN OUT  WF_ORDER_DETAIL.ACQUISITION_COST%TYPE,
                                     O_cust_cost_excl_vat      IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                     O_cust_cost_excl_vat_pri  IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                     O_cust_cost_incl_vat      IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                     O_cust_cost_incl_vat_pri  IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                     O_total_margin            IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                     O_total_margin_pri        IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                     I_wf_order_no             IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                     I_wf_order_cur            IN      WF_ORDER_HEAD.CURRENCY_CODE%TYPE,
                                     I_wf_order_exg_rate       IN      WF_ORDER_HEAD.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    ORDER_AUDIT
-- Purpose: This function will be used to capture the quantity modification to
--          the WF_ORDER_AUDIT table
--
--------------------------------------------------------------------------------
FUNCTION ORDER_AUDIT ( O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_wf_order_no      IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       I_order_line_no    IN      WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE,
                       I_cancel_item_ind  IN      WF_ORDER_AUDIT.ITEM_CANCELLED_IND%TYPE,
                       I_qty              IN      TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name   : DELETE_WF_ORDER
-- Purpose: This function will be used to delete the WF order from WF_ORDER_HEAD
--          WF_ORDER_DETAIL and WF_ORDER_EXP table
--------------------------------------------------------------------------------
FUNCTION DELETE_WF_ORDER (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_wf_order_no      IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_ORDER_DETAILS
-- Purpose: This function is used for fetching the details related to order like
-- Customer group name, Customer name etc
--------------------------------------------------------------------------------
FUNCTION GET_ORDER_DETAILS(O_error_message         IN OUT  rtk_errors.rtk_text%TYPE,
                           O_customer_group_name   IN OUT  wf_customer_group.wf_customer_group_name%TYPE,
                           O_customer_name         IN OUT  wf_customer.wf_customer_name%TYPE,
                           O_status                IN OUT  wf_order_head.status%TYPE,
                           O_cust_ord_ref_no       IN OUT  wf_order_head.cust_ord_ref_no%TYPE ,
                           O_order_type            IN OUT  wf_order_head.order_type%TYPE,
                           O_customer_group_id     IN OUT  wf_customer.wf_customer_group_id%TYPE,
                           O_customer_id           IN OUT  store.wf_customer_id%TYPE ,
                           I_order_no              IN      wf_order_head.wf_order_no%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:     GET_RETURN_DETAILS
-- Purpose: This function is used for fetching the details related to return item
--------------------------------------------------------------------------------
FUNCTION GET_RETURN_DETAILS( O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE ,
                             O_cust_ret_ref_no       IN OUT  WF_RETURN_HEAD.CUST_RET_REF_NO%TYPE,
                             O_customer_group_name   IN OUT  WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_NAME%TYPE,
                             O_customer_name         IN OUT  WF_CUSTOMER.WF_CUSTOMER_NAME%TYPE,
                             O_status                IN OUT  WF_RETURN_HEAD.STATUS%TYPE,
                             O_return_type           IN OUT  WF_RETURN_HEAD.RETURN_TYPE%TYPE,
                             O_return_method         IN OUT  WF_RETURN_HEAD.RETURN_METHOD%TYPE,
                             O_cust_loc              IN OUT  WF_RETURN_HEAD.CUSTOMER_LOC%TYPE ,
                             O_customer_group_id     IN OUT  WF_CUSTOMER.WF_CUSTOMER_GROUP_ID%TYPE ,
                             O_customer_id           IN OUT  STORE.WF_CUSTOMER_ID%TYPE ,
                             O_return_loc_type       IN OUT  WF_RETURN_HEAD.RETURN_LOC_TYPE%TYPE,
                             O_return_loc_id         IN OUT  WF_RETURN_HEAD.RETURN_LOC_ID%TYPE,
                             I_rma_no                IN      WF_RETURN_HEAD.RMA_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_WF_ORDER_HEAD_DETAILS
-- Purpose: This function is used for fetching the header level details related
--          to wf_order_no
--------------------------------------------------------------------------------
FUNCTION GET_WF_ORDER_HEAD_DETAILS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_wf_order_head_details  IN OUT   WF_ORDER_HEAD%ROWTYPE,
                                   I_wf_order_no            IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN ;
--------------------------------------------------------------------------------
-- Name:    GET_WF_ORDER_LINE_INFO
-- Purpose: This function is used get the WF detail information for the passed in
--          WF order no, item and location
--------------------------------------------------------------------------------
FUNCTION GET_WF_ORDER_LINE_INFO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_wf_order_detail   IN OUT   WF_ORDER_DETAIL%ROWTYPE,
                                I_wf_order_no       IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                I_item              IN       WF_ORDER_DETAIL.ITEM%TYPE,
                                I_cust_loc          IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                                I_source_loc_type   IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                I_source_loc_id     IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_ORDER_ITEM_COST
-- Purpose: This function is used get the order cost for the passed in linked
--          transfer/allocation detail line.
--------------------------------------------------------------------------------
FUNCTION GET_ORDER_ITEM_COST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_order_cost       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                             O_currency_code    IN OUT   CURRENCY_RATES.CURRENCY_CODE%TYPE,
                             I_distro_no        IN       SHIPSKU.DISTRO_NO%TYPE,
                             I_distro_type      IN       SHIPSKU.DISTRO_TYPE%TYPE,
                             I_to_loc           IN       ALLOC_DETAIL.TO_LOC%TYPE,
                             I_to_loc_type      IN       ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
                             I_item             IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_TAX_INFO
-- Purpose: This function is used get the vat code and vat rate for a order detail line
--------------------------------------------------------------------------------
FUNCTION GET_TAX_INFO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tax_rate        IN OUT   VAT_CODE_RATES.VAT_RATE%TYPE,
                       O_tax_code        IN OUT   VAT_CODES.VAT_CODE%TYPE,
                       I_item            IN       WF_ORDER_DETAIL.ITEM%TYPE,
                       I_source_loc_type IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                       I_source_loc_id   IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                       I_customer_loc    IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                       I_cost_retail_ind IN       VARCHAR2,
                       I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    REAPPLY_CUSTOMER_COST
-- Purpose: This function applies the latest future cost to the existing franchise
--          order before approval.
--------------------------------------------------------------------------------
FUNCTION REAPPLY_CUSTOMER_COST (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no          IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
---FUNCTION NAME:APPLY_SUPPLIER_COST_CHANGE
---Purpose: This function will be called from wf_apply_supp_cc.ksh. This function will fetch
---         all the Franchise orders in approved status having item/location for which the
---         supplier cost is changed. The supplier cost change records that have been extracted,
---         and have the recalc_ord_ind = 'Y' are used to update the Franchise order cost.
---         This will also sync up the cost and template information on wf_order_detail based
---         on the latest cost and template in future_cost. This will also recalculate the expenses
---         in wf_order_exp.
--------------------------------------------------------------------------------
FUNCTION APPLY_SUPPLIER_COST_CHANGE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
---FUNCTION NAME:CHECK_DEP_ITEM_COSTING_LOC
---Purpose: This function will check if all the supplier sourced deposit content item has the
---         same costing location as its container item at the franchise store.
--------------------------------------------------------------------------------
FUNCTION CHECK_DEP_ITEM_COSTING_LOC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_wf_order_no          IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function   : CHECK_ITEM_LOC
-- Purpose    : This functions checks whether all items in a franchise order are ranged to customer location and
--              returns FALSE for O_valid_itemloc if itemloc ranging doesn't exist for atleast one item.
--------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid_itemloc   IN OUT   BOOLEAN,
                        I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function   : CREATE_ITEM_LOC
-- Purpose    : This function finds the items, in a franchise order, that are not ranged to the customer location and
--              creates new item-loc combinations for them.
--------------------------------------------------------------------------------
FUNCTION CREATE_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function   : DELETE_INVALID_ITEMLOC
-- Purpose    : This function fetches the items in a franchise order that do not have valid item-customer location
--              ranging and deletes the items from the order.
--------------------------------------------------------------------------------
FUNCTION DELETE_INVALID_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
END WF_ORDER_SQL;
/