CREATE OR REPLACE PACKAGE BODY CORESVC_INVBACKORD AS
-------------------------------------------------------------------------------------------------------
LP_backorder_complete    SVC_INVBACKORD.PROCESS_STATUS%TYPE := 'C';
LP_backorder_error       SVC_INVBACKORD.PROCESS_STATUS%TYPE := 'E';
LP_backorder_validate    SVC_INVBACKORD.PROCESS_STATUS%TYPE := 'V';
LP_backorder_new         SVC_INVBACKORD.PROCESS_STATUS%TYPE := 'N';
LP_user                  SVCPROV_CONTEXT.USER_NAME%TYPE     := get_user;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: VALIDATE_BACKORDER
-- Purpose: This private function will perform basic validation of backorder records on SVC_INVBACKORD table.
--          For all the errors, it will update the error_msg column with the error details.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_BACKORDER(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id    IN      SVC_INVBACKORD.PROCESS_ID%TYPE,
                            I_chunk_id      IN      SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: POPULATE_BACKORDER
-- Purpose: This private function will prepare the collections with the back order updates to be made to
--          item_loc_soh table for bulk processing.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_BACKORDER(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_backord_tbl  IN OUT  INVBACKORD_TBL,
                            IO_process_id   IN OUT  SVC_INVBACKORD.PROCESS_ID%TYPE,
                            IO_chunk_id     IN OUT  SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: POPULATE_STORE_BACKORDER_QTY
-- Purpose: This private function will process the back order request for stores
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_STORE_BACKORDER_QTY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                      IO_backord_tbl  IN OUT  INVBACKORD_TBL,
                                      IO_process_id   IN OUT  SVC_INVBACKORD.PROCESS_ID%TYPE,
                                      IO_chunk_id     IN OUT  SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: POPULATE_WH_BACKORDER_QTY
-- Purpose: This private function will process the back order request for warehouse
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_WH_BACKORDER_QTY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_backord_tbl  IN OUT  INVBACKORD_TBL,
                                   IO_process_id   IN OUT  SVC_INVBACKORD.PROCESS_ID%TYPE,
                                   IO_chunk_id     IN OUT  SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: PERSISTS_BACKORDER
-- Purpose: This private function will take the back order collection and bulk update item_loc_soh table.
-------------------------------------------------------------------------------------------------------
FUNCTION PERSISTS_BACKORDER(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_backord_tbl   IN      INVBACKORD_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: POPULATE_SINGLE_VWH_QTY
-- Purpose: This private function called from POPULATE_WH_BACKORDER_QTY will process the back order
--          request for Warehouse where the input physcial WH and channel_id combination results in only
--          one virtual warehouse. The entire back order quantity has to be applied to this single
--          virtual warehouse without any distribution logic.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SINGLE_VWH_QTY(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_backord_tbl        IN OUT   INVBACKORD_TBL,
                                 I_WH_BACKORD_TBL      IN       WH_BACKORD_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: GET_VWH_RESERVE
-- Purpose: This private function called from POPULATE_WH_BACKORDER_QTY will process back order request
--          for warehouse where the back order quantity is positive and the physical warehouse/channel
--          id combination results in multiple valid virtual warehouse. This function chooses a virtual
--          warehouse based on certain rules and assign entire back order quantities to this choosen WH.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_VWH_RESERVE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_backord_tbl        IN OUT   INVBACKORD_TBL,
                         I_WH_BACKORD_TBL      IN       WH_BACKORD_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: GET_VWH_REG_RELEASE
-- Purpose: This private function called from POPULATE_WH_BACKORDER_QTY will process back order request
--          for regular item and physical warehouse where the back order quantity is negative. This
--          function will fetch all potential virtual warehouse from where the back order quantity has
--          to be released along with the current back order reserved quantity from item_loc_soh. This
--          function will also adjust backorder reserved quantity fetched from item_loc_soh for
--          identified (but not yet persisted to database) backorder updates for the same item/virtual
--          warehouse earlier in this program.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_VWH_REG_RELEASE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_backord_tbl        IN OUT   INVBACKORD_TBL,
                             I_WH_BACKORD_TBL      IN       WH_BACKORD_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: GET_VWH_PACK_RELEASE
-- Purpose: This private function called from POPULATE_WH_BACKORDER_QTY will process back order request
--          for pack item and physical warehouse where the back order quantity is negative. This
--          function will fetch all potential virtual warehouse from where the back order quantity has
--          to be released along with the current back order reserved quantity from item_loc_soh. This
--          function will also adjust backorder reserved quantity fetched from item_loc_soh for
--          identified (but not yet persisted to database) backorder updates for the same item(or pack
--          component)/virtual warehouse earlier in this program.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_VWH_PACK_RELEASE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_backord_tbl        IN OUT   INVBACKORD_TBL,
                              I_WH_BACKORD_TBL      IN       WH_BACKORD_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: DISTRIBUTE_VWH_RELEASE
-- Purpose: This private function called from GET_VWH_REG_RELEASE and GET_VWH_PACK_RELEASE will
--          distribute the back order release quantity from the multiple identified warehouse. This
--          function will try to first release the back order quantity based on the current back order
--          reserved quantity at the virtual warehouse. In case the back order release quantity is
--          greater than the total current back order reserved quantity, it will distribute the leftover
--          quantities to the last virtual warehouse.
-------------------------------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_VWH_RELEASE (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_backord_tbl      IN OUT   INVBACKORD_TBL,
                                 I_vwh_process_TBL   IN       VWH_BACKORD_RELEASE_TBL)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
--- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_BACKORDER(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_process_id   IN OUT  SVC_INVBACKORD.PROCESS_ID%TYPE,
                          IO_chunk_id     IN OUT  SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(64)         := 'CORESVC_INVBACKORD.CREATE_BACKORDER';
   L_inv_backorder_tbl     INVBACKORD_TBL       := INVBACKORD_TBL();
   L_table                 VARCHAR2(255);

   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_INVBACKORD is
      select 'x'
        from svc_invbackord
       where process_id = IO_process_id
         and chunk_id = IO_chunk_id
         for update nowait;
BEGIN

   L_table := 'SVC_INVBACKORD';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SVC_INVBACKORD',L_table,'Process id: ' || IO_process_id || ' Chunk id: ' || IO_chunk_id);
   open C_LOCK_SVC_INVBACKORD;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SVC_INVBACKORD',L_table,'Process id: ' || IO_process_id || ' Chunk id: ' || IO_chunk_id);
   close C_LOCK_SVC_INVBACKORD;

   if VALIDATE_BACKORDER(O_error_message,
                         IO_process_id,
                         IO_chunk_id) = FALSE then
      return FALSE;
   end if;

   if POPULATE_BACKORDER(O_error_message,
                         L_inv_backorder_tbl,   -- Output. Will be populated with back order quantity to be applied in bulk to item_loc_soh
                         IO_process_id,
                         IO_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSISTS_BACKORDER(O_error_message,
                         L_inv_backorder_tbl) = FALSE then
      return FALSE;
   end if;

   -- Update the process status to Completed status.
   if L_inv_backorder_tbl is NOT NULL and L_inv_backorder_tbl.COUNT > 0 then
      FORALL i IN 1..L_inv_backorder_tbl.COUNT
         update svc_invbackord
            set process_status = LP_backorder_complete,
                last_update_datetime = SYSDATE,
                last_update_id = LP_user
          where backord_id = L_inv_backorder_tbl(i).backord_id
            and process_status = LP_backorder_validate;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SVC_INVBACKORD%ISOPEN then
         close C_LOCK_SVC_INVBACKORD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_LOCKED',
                                             L_table,
                                             IO_process_id,
                                             IO_chunk_id);
      return FALSE;

   when OTHERS then
      if C_LOCK_SVC_INVBACKORD%ISOPEN then
         close C_LOCK_SVC_INVBACKORD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_BACKORDER;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_BACKORDER(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id    IN      SVC_INVBACKORD.PROCESS_ID%TYPE,
                            I_chunk_id      IN      SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)                  := 'CORESVC_INVBACKORD.VALIDATE_BACKORDER';
   L_error_msg        SVC_INVBACKORD.ERROR_MSG%TYPE := NULL;
   L_table            VARCHAR2(255);

  -- Bulk collect the records which have some validation error and populate the svc_invbackord.error_msg
  -- column with validation errors and return back the error message
   cursor C_VALIDATE_SVC_INVBACKORD is
      select si.channel_id                     stg_channel_id,
             si.backord_id                     backord_id,
             ch.channel_id                     channels_channel_id,
             si.backorder_qty                  backorder_qty,
             si.backorder_uom                  stg_uom,
             uc.uom                            uom_class_uom,
             si.item                           stg_item,
             im.item                           im_item,
             im.tran_level                     tran_level,
             im.item_level                     item_level,
             im.sellable_ind                   sellable_ind,
             im.inventory_ind                  inventory_ind,
             im.status                         status,
             im.standard_uom                   standard_uom,
             im.catch_weight_ind               catch_weight_ind,
             im.item_xform_ind                 item_xform_ind,
             im.deposit_item_type              deposit_item_type,
             NVL(d.purchase_type,-1)           purchase_type,
             si.location                       stg_location,
             si.loc_type                       stg_loc_type,
             all_locs.loc                      loc,
             valid_locs.loc                    valid_location,
             (select MAX(DECODE(status,'D',-1,1))           -- If all rows are delete, it will return -1 which will be rejected.
                from item_loc il,
                     (select s.store loc,
                             NULL physical_wh,
                             s.channel_id
                        from store s
                       where s.customer_order_loc_ind = 'Y'
                         and s.stockholding_ind = 'Y'
                      UNION
                      select wh.wh loc,
                             wh.physical_wh,
                             wh.channel_id
                        from wh
                       where wh.customer_order_loc_ind = 'Y'
                         and wh.stockholding_ind = 'Y'
                      ) loc
               where ((si.loc_type = 'S'
                     and si.location = loc.loc)
                     or (si.loc_type = 'W'
                      and si.location = loc.physical_wh))
                 and il.item = si.item
                 and il.loc = loc.loc
                 and (NVL(si.channel_id,NVL(loc.channel_id,-1)) = NVL(loc.channel_id,-1)
                      or (si.loc_type = 'S' and loc.channel_id is NULL))
             ) item_loc_ind,
             NULL err_msg
        from svc_invbackord si,
             channels ch,
             uom_class uc,
             item_master im,
             deps d,
            (select store loc,
                    'S'   loc_type
               from store
              where stockholding_ind = 'Y'
             UNION
             select wh loc,
                    'W' loc_type
               from wh
            ) all_locs,
            (select store loc,
                    NULL channel_id,
                    'S' loc_type
               from store
              where customer_order_loc_ind = 'Y'
                and stockholding_ind = 'Y'
             UNION
             select store loc,
                    channel_id,
                    'S' loc_type
               from store
              where customer_order_loc_ind = 'Y'
                and stockholding_ind = 'Y'
             UNION
             select physical_wh loc,
                    channel_id,
                    'W' loc_type
               from wh
              where physical_wh <> wh
                and customer_order_loc_ind = 'Y'
                and stockholding_ind = 'Y'
             group by physical_wh,channel_id
             UNION
             select physical_wh loc,
                    null channel_id,
                    'W' loc_type
              from wh
             where physical_wh <> wh
               and customer_order_loc_ind = 'Y'
               and stockholding_ind = 'Y'
             group by physical_wh
             UNION
             select wh loc,
                    channel_id,
                    'W' loc_type
               from wh
              where physical_wh <> wh
                and customer_order_loc_ind = 'Y'
                and stockholding_ind = 'Y'
             UNION
             select wh loc,
                    NULL channel_id,
                    'W' loc_type
               from wh
              where physical_wh <> wh
                and customer_order_loc_ind = 'Y'
                and stockholding_ind = 'Y'
            ) valid_locs
       where si.process_id = I_process_id
         and si.chunk_id = I_chunk_id
         and si.process_status = LP_backorder_new
         and si.channel_id = ch.channel_id(+)
         and si.backorder_uom = uc.uom(+)
         and si.item = im.item(+)
         and im.dept = d.dept(+)
         and (si.location = all_locs.loc(+)
             and si.loc_type = all_locs.loc_type(+))
         and si.location = valid_locs.loc(+)
         and si.loc_type = valid_locs.loc_type(+)
         and NVL(si.channel_id,-1) = NVL(valid_locs.channel_id(+),-1)
         and ((ch.channel_id is NULL and si.channel_id is not null)
           or uc.uom is NULL
           or (si.backorder_uom = 'EA' and ROUND(si.backorder_qty) <> si.backorder_qty)
           or all_locs.loc is NULL
           or (all_locs.loc is not NULL and valid_locs.loc is NULL)
           or im.item is NULL
           or (     im.item is not NULL
                and (im.status <> 'A'
                   or im.standard_uom <> si.backorder_uom
                   or im.sellable_ind <> 'Y'
                   or im.catch_weight_ind = 'Y'
                   or im.item_xform_ind = 'Y'
                   or im.tran_level <> im.item_level
                   or im.inventory_ind <> 'Y'
                   or d.purchase_type <> 0
                   or NVL(im.deposit_item_type,'E') IN ('A','Z')
                    )
              )
           or NOT exists (select 1
                            from item_loc il,
                                 (select s.store loc,
                                         NULL physical_wh,
                                         s.channel_id
                                    from store s
                                   where s.customer_order_loc_ind = 'Y'
                                     and s.stockholding_ind = 'Y'
                                  UNION
                                  select wh.wh loc,
                                         wh.physical_wh,
                                         wh.channel_id
                                    from wh
                                   where wh.customer_order_loc_ind = 'Y'
                                     and wh.stockholding_ind = 'Y'
                                  ) loc
                           where (  (si.location = loc.loc)
                                 or (si.location = loc.physical_wh))
                             and si.item = il.item
                             and loc.loc = il.loc
                             and il.status <> 'D'
                             and (NVL(si.channel_id,NVL(loc.channel_id,-1)) = NVL(loc.channel_id,-1)
                                  or (si.loc_type = 'S' and loc.channel_id is NULL))
                           )
                );

   TYPE val_svc_invbackord_TBL is TABLE of C_VALIDATE_SVC_INVBACKORD%ROWTYPE;
   L_val_svc_invbackord_TBL val_svc_invbackord_TBL;

BEGIN

   /* Validate the record in staging table and populate the error message column in staging table if some
      validation error occurs and return FALSE.The validations that are covered under this section are -
       * Only Approved, Sellable, inventory and Transactional level items are allowed.
       * Item shouldn't be Catchweight,Transformable, Deposit and Consignment/Concession item.
       * UOM passed should be valid UOM and it should be match to the standard UOM for the item.
       * If UOM is Eaches, the back order quantity should not be a decimal number.
       * CHANNEL_ID if passed should be valid CHANNEL_ID.
       * Item should be ranged to the requested location.In case of warehouse item should be ranged to atleast
         one of the virtual warehouse under passed physical warehouse/channel_id combination.
       * Incase of passed location is 'S'tore validate that it should be customer order location and if channel id
         is passed in message store/channel_id combination should exist in store table. Only stockholding 'C'ompany
         stores are allowed.
       * Incase of location passed is 'W'arehouse, validate if it's a valid warehouse. If channel_id is
         present, validate atleast one customer orderable virtual warehouse is present for that warehouse/channel_id
         combination. If channel_id is not present, validate there is atleast one customer orderable virtual
         warehouse for that physical warehouse or the passed in virtual wh is stockholding and customer orderable.
   */

   L_table  := 'SVC_INVBACKORD, ITEM_MASTER, ITEM_LOC, STORE, WH, DEPS, CHANNELS, UOM_CLASS';
   SQL_LIB.SET_MARK('OPEN','C_VALIDATE_SVC_INVBACKORD',L_table,NULL);
   open C_VALIDATE_SVC_INVBACKORD;

   SQL_LIB.SET_MARK('FETCH','C_VALIDATE_SVC_INVBACKORD',L_table,NULL);
   fetch C_VALIDATE_SVC_INVBACKORD BULK COLLECT into L_val_svc_invbackord_TBL;

   SQL_LIB.SET_MARK('CLOSE','C_VALIDATE_SVC_INVBACKORD',L_table,NULL);
   close C_VALIDATE_SVC_INVBACKORD;

   if L_val_svc_invbackord_TBL is NOT NULL and L_val_svc_invbackord_TBL.COUNT > 0 then
      FOR i in 1..L_val_svc_invbackord_TBL.COUNT LOOP
        --Check if item passed is a valid item
         if L_val_svc_invbackord_TBL(i).im_item is NULL then
            L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                   SQL_LIB.GET_MESSAGE_TEXT('BACKORD_ITEM_NOT_EXIST',
                                                                            L_val_svc_invbackord_TBL(i).stg_item,
                                                                            L_val_svc_invbackord_TBL(i).stg_location,
                                                                            NULL)|| ';';
         else
            --Check if item passed is not a valid approved transaction level sellable item.
            if L_val_svc_invbackord_TBL(i).item_level <> L_val_svc_invbackord_TBL(i).tran_level
               or L_val_svc_invbackord_TBL(i).sellable_ind <> 'Y'
               or L_val_svc_invbackord_TBL(i).inventory_ind <> 'Y'
               or L_val_svc_invbackord_TBL(i).status <> 'A'then

               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_INVALID_ITEM',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            end if;

            --Check if item passed is a catch weight item
            if L_val_svc_invbackord_TBL(i).catch_weight_ind = 'Y' then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_CATCHWEIGHT_ITM',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            end if;
            --Check if item passed is a transformable item
            if L_val_svc_invbackord_TBL(i).item_xform_ind = 'Y' then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_TRANSFORM_ITEM',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            end if;
            --Check if quantity of item passed is in standard uom
            if L_val_svc_invbackord_TBL(i).standard_uom <> L_val_svc_invbackord_TBL(i).stg_uom
               and L_val_svc_invbackord_TBL(i).uom_class_uom is NOT NULL then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_NOT_STANDARD_UOM',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               L_val_svc_invbackord_TBL(i).stg_uom)|| ';';
            end if;
            --Check if item passed is deposit item. Only a null value or item type Content 'E' is allowed
            if NVL(L_val_svc_invbackord_TBL(i).deposit_item_type,'E') in ('A','Z') then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_DEPOSIT_ITEM',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            end if;
            --Check if item passed is Concession/Consignment item
            if L_val_svc_invbackord_TBL(i).purchase_type <> 0 then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_CONCESSION_ITEM',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            end if;
         end if;--Item Validations

         --Check if uom passed is valid uom
         if L_val_svc_invbackord_TBL(i).uom_class_uom is NULL then
            L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                   SQL_LIB.GET_MESSAGE_TEXT('BACKORD_INVALID_UOM',
                                                                            L_val_svc_invbackord_TBL(i).stg_item,
                                                                            L_val_svc_invbackord_TBL(i).stg_location,
                                                                            L_val_svc_invbackord_TBL(i).stg_uom)|| ';';
         end if;

         --Check if UOM is EA, the back order quantity should not be a decimal number
         if L_val_svc_invbackord_TBL(i).stg_uom ='EA' and
               (ROUND(L_val_svc_invbackord_TBL(i).backorder_qty) <> L_val_svc_invbackord_TBL(i).backorder_qty) then
            L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                   SQL_LIB.GET_MESSAGE_TEXT('BACKORD_INVALID_EA_QTY',
                                                                            L_val_svc_invbackord_TBL(i).stg_item,
                                                                            L_val_svc_invbackord_TBL(i).stg_location,
                                                                            L_val_svc_invbackord_TBL(i).backorder_qty)|| ';';
         end if;

         --Check if channel_id passed is valid channel_id
         if L_val_svc_invbackord_TBL(i).channels_channel_id is NULL
            and L_val_svc_invbackord_TBL(i).stg_channel_id is NOT NULL then
            L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                   SQL_LIB.GET_MESSAGE_TEXT('BACKORD_INVALID_CHANNEL',
                                                                            L_val_svc_invbackord_TBL(i).stg_item,
                                                                            L_val_svc_invbackord_TBL(i).stg_location,
                                                                            L_val_svc_invbackord_TBL(i).stg_channel_id)|| ';';
         end if;

         --Check if location passed is valid location
         if L_val_svc_invbackord_TBL(i).loc is NULL then
            if L_val_svc_invbackord_TBL(i).stg_loc_type = 'S' then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_STORE_NOT_EXIST',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            elsif L_val_svc_invbackord_TBL(i).stg_loc_type = 'W' then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_WH_NOT_EXIST',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            end if;
         elsif L_val_svc_invbackord_TBL(i).channels_channel_id is NOT NULL
            or L_val_svc_invbackord_TBL(i).stg_channel_id is NULL then
            --Check if 'S'tore/Channel id passed is valid customer orderable store
            if L_val_svc_invbackord_TBL(i).valid_location is NULL and L_val_svc_invbackord_TBL(i).stg_loc_type = 'S' then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_INVALID_STORE',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            --Check if 'W'arehouse/Channel id passed is valid customer orderable warehouse
            elsif L_val_svc_invbackord_TBL(i).valid_location is NULL and L_val_svc_invbackord_TBL(i).stg_loc_type = 'W' then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_INVALID_WH',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            --Check if item/location is backorder location
            elsif (L_val_svc_invbackord_TBL(i).item_loc_ind is NULL OR  L_val_svc_invbackord_TBL(i).item_loc_ind = -1)
               and L_val_svc_invbackord_TBL(i).im_item is not NULL then
               L_val_svc_invbackord_TBL(i).err_msg := L_val_svc_invbackord_TBL(i).err_msg ||
                                                      SQL_LIB.GET_MESSAGE_TEXT('BACKORD_LOC_NOT_RANGED',
                                                                               L_val_svc_invbackord_TBL(i).stg_item,
                                                                               L_val_svc_invbackord_TBL(i).stg_location,
                                                                               NULL)|| ';';
            end if;
         end if;---Location validations
      END LOOP;
   end if;

   -- Set the process_status to 'E' and update the error message for the rows having validation error.
   if L_val_svc_invbackord_TBL is NOT NULL and L_val_svc_invbackord_TBL.COUNT > 0 then
      FORALL i in 1..L_val_svc_invbackord_TBL.COUNT
         update svc_invbackord
            set error_msg = L_val_svc_invbackord_TBL(i).err_msg,
                process_status = LP_backorder_error,
                last_update_datetime = SYSDATE,
                last_update_id = LP_user
          where backord_id = L_val_svc_invbackord_TBL(i).backord_id;

      return FALSE;

   else
      -- If all the records have passed validation, update the status to Validated for all the records.
      update svc_invbackord
         set process_status = LP_backorder_validate,
             last_update_datetime = SYSDATE,
             last_update_id = LP_user
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and process_status = LP_backorder_new;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then

      if C_VALIDATE_SVC_INVBACKORD%ISOPEN then
         close C_VALIDATE_SVC_INVBACKORD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_BACKORDER;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_BACKORDER (O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_backord_tbl  IN OUT  INVBACKORD_TBL,
                             IO_process_id   IN OUT  SVC_INVBACKORD.PROCESS_ID%TYPE,
                             IO_chunk_id     IN OUT  SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)  := 'CORESVC_INVBACKORD.POPULATE_BACKORDER';

BEGIN

   -- The back order processing is split for stores and warehouse. The collection IO_backord_tbl will be first
   -- populated for stores and then appended for back order request at warehouse.
   if POPULATE_STORE_BACKORDER_QTY(O_error_message,
                                   IO_backord_tbl,
                                   IO_process_id,
                                   IO_chunk_id) = FALSE then
      return FALSE;
   end if;

   if POPULATE_WH_BACKORDER_QTY(O_error_message,
                                IO_backord_tbl,
                                IO_process_id,
                                IO_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_BACKORDER;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_STORE_BACKORDER_QTY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                      IO_backord_tbl  IN OUT  INVBACKORD_TBL,
                                      IO_process_id   IN OUT  SVC_INVBACKORD.PROCESS_ID%TYPE,
                                      IO_chunk_id     IN OUT  SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
L_program               VARCHAR2(64)      := 'CORESVC_INVBACKORD.POPULATE_STORE_BACKORDER_QTY';
L_store_backorder_tbl   INVBACKORD_TBL    := INVBACKORD_TBL();
L_table                 VARCHAR2(255);

   -- Get the back order quantity for the store.
   -- For pack item, fetch the component items
   cursor C_POPULATE_STORE_REC is
     select INVBACKORD_REC
            (si.backord_id,
            si.item,
            im.pack_ind,
            si.location,
            si.loc_type,
            si.backorder_qty,
            'N')                     -- Pack's component SOH indicator
       from svc_invbackord si,
            item_master im
      where si.process_id = IO_process_id
        and si.chunk_id = IO_chunk_id
        and si.item = im.item
        and im.pack_ind = 'N'
        and si.loc_type = 'S'
        and si.process_status = LP_backorder_validate
     union all
     select INVBACKORD_REC
            (si.backord_id,
            vpq.item,
            im.pack_ind,
            si.location,
            si.loc_type,
            (vpq.qty * si.backorder_qty),
            'N')
       from svc_invbackord si,
            item_master im,
            v_packsku_qty vpq,
            item_master im2
      where si.process_id = IO_process_id
        and si.chunk_id = IO_chunk_id
        and si.item = im.item
        and im.pack_ind = 'Y'
        and si.item = vpq.pack_no
        and si.loc_type = 'S'
        and vpq.item = im2.item
        and im2.inventory_ind = 'Y'                  -- Ignoring non-inventory pack component
        and si.process_status = LP_backorder_validate;

BEGIN

   L_table  := 'SVC_INVBACKORD, ITEM_MASTER, V_PACKSKU_QTY';
   SQL_LIB.SET_MARK('OPEN','C_POPULATE_STORE_REC',L_table,NULL);
   open C_POPULATE_STORE_REC;

   SQL_LIB.SET_MARK('FETCH','C_POPULATE_STORE_REC',L_table,NULL);
   fetch C_POPULATE_STORE_REC BULK COLLECT into L_store_backorder_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_POPULATE_STORE_REC',L_table,NULL);
   close C_POPULATE_STORE_REC;

   IO_backord_tbl := IO_backord_tbl MULTISET UNION L_store_backorder_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_POPULATE_STORE_REC%ISOPEN then
         close C_POPULATE_STORE_REC;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_STORE_BACKORDER_QTY;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_WH_BACKORDER_QTY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_backord_tbl  IN OUT  INVBACKORD_TBL,
                                   IO_process_id   IN OUT  SVC_INVBACKORD.PROCESS_ID%TYPE,
                                   IO_chunk_id     IN OUT  SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
L_program               VARCHAR2(64)            := 'CORESVC_INVBACKORD.POPULATE_WH_BACKORDER_QTY';
L_wh_backord_tbl        WH_BACKORD_TBL          := WH_BACKORD_TBL();
L_table                 VARCHAR2(255);

   cursor C_GET_BACKORD_VWH_COUNT is
      select WH_BACKORD_REC(si.backord_id,
                            si.item,
                            im.pack_ind,
                            il.receive_as_type,
                            si.location,
                            wh.wh,
                            count(*) over (partition by backord_id))          -- count_vwh
        from svc_invbackord si,
             item_master im,
             wh wh,
             item_loc il
       where si.process_id = IO_process_id
         and si.chunk_id = IO_chunk_id
         and si.process_status = LP_backorder_validate
         and si.loc_type = 'W'
         and si.item = im.item
         and ( wh.physical_wh = si.location
             or wh.wh = si.location)
         and NVL(wh.channel_id,-1) = NVL(si.channel_id,NVL(wh.channel_id,-1))
         and NVL(wh.customer_order_loc_ind,'N') ='Y'
         and wh.stockholding_ind = 'Y'
         and il.item = si.item
         and il.loc = wh.wh
         and il.status <> 'D';

BEGIN

   L_table := 'SVC_INVBACKORD, ITEM_MASTER, WH, ITEM_LOC';
   SQL_LIB.SET_MARK('OPEN','C_GET_BACKORD_VWH_COUNT',L_table,NULL);
   open C_GET_BACKORD_VWH_COUNT;

   SQL_LIB.SET_MARK('FETCH','C_GET_BACKORD_VWH_COUNT',L_table,NULL);
   fetch C_GET_BACKORD_VWH_COUNT BULK COLLECT into L_wh_backord_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_GET_BACKORD_VWH_COUNT',L_table,NULL);
   close C_GET_BACKORD_VWH_COUNT;

   -- Splitting the processing of WH for input Physcial/Channel having a single Virtual Warehouse and having multiple virtual warehouse.
   -- Physcial/Channel will generally have only one virtual warehouse setup while RMS has no restiction on number of VWH for Physical warehouse
   -- and channel. The processing for multiple warehouse requires distribution logic which is performance intensive. Hence separating the logic
   -- to avoid heavy performance calls for single virtual warehouse scenarios which is the most common.
   -- The count of virtaul wh as '1' will be process by POPULATE_SINGLE_VWH_QTY whereas the other functions will process records for item/physical
   -- warehouse resulting in multiple virtual warehouse.

   -- for no warehouse in the backorder request, return.
   if L_wh_backord_tbl is NULL or L_wh_backord_tbl.count = 0 then
      return TRUE;
   end if;

   -- For single virtual warehouse
   if POPULATE_SINGLE_VWH_QTY(O_error_message,
                              IO_backord_tbl,
                              L_wh_backord_tbl) = FALSE then
      return FALSE;
   end if;

   -- For multiple virtual warehouse
   if GET_VWH_RESERVE(O_error_message,
                      IO_backord_tbl,
                      L_wh_backord_tbl) = FALSE then
      return FALSE;
   end if;

   if GET_VWH_REG_RELEASE(O_error_message,
                          IO_backord_tbl,
                          L_wh_backord_tbl) = FALSE then
      return FALSE;
   end if;

   if GET_VWH_PACK_RELEASE(O_error_message,
                          IO_backord_tbl,
                          L_wh_backord_tbl) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_WH_BACKORDER_QTY;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SINGLE_VWH_QTY(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_backord_tbl          IN OUT  INVBACKORD_TBL,
                                 I_WH_BACKORD_TBL        IN      WH_BACKORD_TBL)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)               := 'CORESVC_INVBACKORD.POPULATE_SINGLE_VWH_QTY';
   L_wh_backorder_tbl         INVBACKORD_TBL             := INVBACKORD_TBL();
   L_table                    VARCHAR2(255);

   -- Fetch both increase and decrease back order reserve quantity for single valid VWH records, fetch
   -- regular item and pack item records along with pack item component.
   cursor C_GET_VWH_BACK_ORDER_QTY is
      select INVBACKORD_REC(
             si.backord_id,
             si.item,
             wb.pack_ind,
             wb.vwh,
             si.loc_type,
             si.backorder_qty,
             'N')                                  --comp_item_ind
        from svc_invbackord si,
             TABLE(CAST(I_WH_BACKORD_TBL AS WH_BACKORD_TBL)) wb
       where wb.backord_id = si.backord_id
         and wb.pack_ind = 'N'
         and wb.count_vwh = 1
         and wb.location = si.location
      UNION ALL
      select INVBACKORD_REC(
             si.backord_id,
             si.item,
             wb.pack_ind,
             wb.vwh,
             si.loc_type,
             si.backorder_qty,
             'N')
        from svc_invbackord si,
             TABLE(CAST(I_WH_BACKORD_TBL AS WH_BACKORD_TBL)) wb
       where wb.backord_id = si.backord_id
         and wb.pack_ind = 'Y'
         and wb.count_vwh = 1
         and wb.location = si.location
         and NVL(wb.receive_as_type,'E') = 'P'
      UNION ALL
      select INVBACKORD_REC(
             si.backord_id,
             vpq.item,
             wb.pack_ind,
             wb.vwh,
             si.loc_type,
             (vpq.qty * si.backorder_qty),
             DECODE(wb.receive_as_type,'P','Y','N'))                 -- For receive_as_type 'E', inv is maintained only at component level.
        from svc_invbackord si,
             TABLE(CAST(I_WH_BACKORD_TBL AS WH_BACKORD_TBL)) wb,
             v_packsku_qty vpq,
             item_master im
       where wb.backord_id = si.backord_id
         and wb.pack_ind = 'Y'
         and wb.count_vwh = 1
         and wb.location = si.location
         and wb.item = vpq.pack_no
         and vpq.item = im.item
         and im.inventory_ind = 'Y';

BEGIN

   L_table := 'SVC_INVBACKORD, V_PACKSKU_QTY, ITEM_MASTER';
   SQL_LIB.SET_MARK('OPEN','C_GET_VWH_BACK_ORDER_QTY',L_table,NULL);
   open C_GET_VWH_BACK_ORDER_QTY;

   SQL_LIB.SET_MARK('FETCH','C_GET_VWH_BACK_ORDER_QTY',L_table,NULL);
   fetch C_GET_VWH_BACK_ORDER_QTY BULK COLLECT into L_wh_backorder_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_GET_VWH_BACK_ORDER_QTY',L_table,NULL);
   close C_GET_VWH_BACK_ORDER_QTY;

   -- Append to the collection for item_loc_soh bulk update, the records for backorder reservation/release request for single virtual warehouse
   IO_backord_tbl := IO_backord_tbl MULTISET UNION L_wh_backorder_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_VWH_BACK_ORDER_QTY%ISOPEN then
         close C_GET_VWH_BACK_ORDER_QTY;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_SINGLE_VWH_QTY;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_VWH_RESERVE (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_backord_tbl      IN OUT   INVBACKORD_TBL,
                          I_WH_BACKORD_TBL    IN       WH_BACKORD_TBL)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)               := 'CORESVC_INVBACKORD.GET_VWH_RESERVE';
   L_ranked_vwh_tbl           INVBACKORD_TBL             := INVBACKORD_TBL();
   L_wh_reserve_tbl           INVBACKORD_TBL             := INVBACKORD_TBL();
   L_table                    VARCHAR2(255);

   -- Cursor to identify the virtual warehouse to be used for reserving the back order quantity.
   cursor C_GET_BACKORD_VWH is
      select INVBACKORD_REC
                (backord_id,
                item,
                pack_ind,
                vwh,
                loc_type,
                backorder_qty,
                NULL)
        from (
             select si.backord_id,
                    si.item,
                    wb.pack_ind,
                    wh.wh vwh,
                    si.loc_type,
                    si.backorder_qty,
                    RANK() OVER (PARTITION BY si.item,
                                              si.location,
                                              si.channel_id
                                     ORDER BY decode(il.status,'A',1,'I',2,'C',3) asc,
                                              wh.restricted_ind asc,                         -- 'N' has higher priority
                                              decode(phy_wh.primary_vwh,wh.wh,1,0) desc,     -- Primary wh has higher priority
                                              wh.wh asc) rank
               from svc_invbackord si,
                    TABLE(CAST(I_WH_BACKORD_TBL AS WH_BACKORD_TBL)) wb,
                    item_loc il,
                    wh wh,
                    wh phy_wh
              where si.backord_id = wb.backord_id
                and wb.count_vwh > 1
                and si.backorder_qty >= 0
                and wb.location = si.location
                and wb.vwh = wh.wh
                and wh.physical_wh = phy_wh.wh
                and il.item = si.item
                and il.loc = wh.wh
                and il.status <> 'D')
       where rank = 1;

   -- For the identified virtual warehouse, populate the record for item_loc_soh update.
   -- Explode pack to component for warehouse where receive_as_type = 'E' as the inventory will be held at component level only.
   -- For pack item and virtual warehouse where receive_as_type = 'P', get rows for both Pack and component item. The component
   -- pack_comp_cust_back field will be updated for the change in backorder quantity for the pack item.
   cursor C_VWH_RESERVE is
      select INVBACKORD_REC                                       -- Regular items
             (bo.backord_id,
             bo.item,
             bo.pack_ind,
             bo.location,
             'W',                                                 -- Loc_type
             bo.backorder_qty,
             'N')                   -- comp_item_ind
        from TABLE(CAST(L_ranked_vwh_tbl AS INVBACKORD_TBL)) bo
       where bo.pack_ind = 'N'
      UNION ALL
      select INVBACKORD_REC                                       -- Fetch Pack item row
             (bo.backord_id,
             bo.item,
             bo.pack_ind,
             bo.location,
             'W',                                                 -- Loc_type
             bo.backorder_qty,
             'N')
        from TABLE(CAST(L_ranked_vwh_tbl AS INVBACKORD_TBL)) bo,
             item_loc il
       where bo.pack_ind = 'Y'
         and bo.item = il.item
         and bo.location = il.loc
         and NVL(il.receive_as_type,'E') = 'P'
      UNION ALL                                                   -- Fetch pack component rows
      select INVBACKORD_REC
             (bo.backord_id,
             vpq.item,
             bo.pack_ind,
             bo.location,
             'W',                                                 -- Loc_type
             (vpq.qty * bo.backorder_qty),
             DECODE(il.receive_as_type,'P','Y','N'))
        from TABLE(CAST(L_ranked_vwh_tbl AS INVBACKORD_TBL)) bo,
             item_loc il,
             v_packsku_qty vpq,
             item_master im
       where bo.pack_ind = 'Y'
         and bo.item = il.item
         and bo.location = il.loc
         and bo.item = vpq.pack_no
         and vpq.item = im.item
         and im.inventory_ind = 'Y';                             -- Ignoring non-inventory pack component

BEGIN
   /* Identify the virtual warehouse for the passed physical warehouse.using below functional rules
      * Virtual WH to which the item is ranged.
      * Item/location status (First active, then inactive followed by discontinued)
      * Restricted indicator (A virtual warehouse that is not restricted will get preference)
      * Primary virtual warehouse for the physical warehouse will have higher preference
      * Lowest numbered virtual warehouse will have higher preference
   */

   L_table := 'SVC_INVBACKORD, ITEM_LOC, WH';
   SQL_LIB.SET_MARK('OPEN','C_GET_BACKORD_VWH',L_table,NULL);
   open C_GET_BACKORD_VWH;

   SQL_LIB.SET_MARK('FETCH','C_GET_BACKORD_VWH',L_table,NULL);
   fetch C_GET_BACKORD_VWH BULK COLLECT into L_ranked_vwh_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_GET_BACKORD_VWH',L_table,NULL);
   close C_GET_BACKORD_VWH;

   L_table := 'SVC_INVBACKORD, V_PACKSKU_QTY, ITEM_MASTER, ITEM_LOC';
   SQL_LIB.SET_MARK('OPEN','C_VWH_RESERVE',L_table,NULL);
   open C_VWH_RESERVE;

   SQL_LIB.SET_MARK('FETCH','C_VWH_RESERVE',L_table,NULL);
   fetch C_VWH_RESERVE BULK COLLECT into L_wh_reserve_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_VWH_RESERVE',L_table,NULL);
   close C_VWH_RESERVE;

   -- Append to the collection for item_loc_soh bulk update the backorder quantities processed in this function
   IO_backord_tbl := IO_backord_tbl MULTISET UNION L_wh_reserve_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then

      if C_GET_BACKORD_VWH%ISOPEN then
         close C_GET_BACKORD_VWH;
      end if;
      if C_VWH_RESERVE%ISOPEN then
         close C_VWH_RESERVE;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_VWH_RESERVE;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_VWH_REG_RELEASE (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_backord_tbl      IN OUT   INVBACKORD_TBL,
                              I_WH_BACKORD_TBL    IN       WH_BACKORD_TBL)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)            := 'CORESVC_INVBACKORD.GET_VWH_REG_RELEASE';
   L_vwh_process_TBL VWH_BACKORD_RELEASE_TBL := VWH_BACKORD_RELEASE_TBL();
   L_table           VARCHAR2(255);

   -- Retrive the virtual warehouse with their existing (and adjust) back order quantity for regular item
   -- where backorder quantity has to be released (negative).
   cursor C_VWH_RELEASE is
      select VWH_BACKORD_RELEASE_REC
                (si.backord_id,
                 si.item,
                 'N',                                                   -- Pack ind
                 wb.vwh,
                 si.backorder_qty,
                 'E',                                                   -- Receive_as_type,
                 ils.customer_backorder + NVL(res_bo.backorder_qty,0),
                 0 ,                                                    -- Update_customer_backorder: Delta customer back order. Will be updated later.
                 0)                                                     -- Update_pack_comp_backorder: Delta pack customer backorder. Will be updated later.
        from svc_invbackord si,
             item_loc_soh ils,
             TABLE(CAST(I_WH_BACKORD_TBL AS WH_BACKORD_TBL)) wb,
             (select item,
                     location,
                     SUM(backorder_qty) backorder_qty
                from TABLE(CAST(IO_backord_tbl as INVBACKORD_TBL))
               where comp_item_ind = 'N'
             group by item,location) res_bo                             -- To adjust item_loc_soh backorder reserve quantity for non-committed records in the collection
       where si.backord_id = wb.backord_id
         and wb.count_vwh > 1
         and wb.pack_ind = 'N'
         and si.backorder_qty < 0
         and wb.location = si.location
         and ils.item = wb.item
         and ils.loc = wb.vwh
         and ils.item = res_bo.item(+)                                  -- Using outer join because no records may exists in IO_backord_tbl to adjust.
         and ils.loc = res_bo.location(+)
      order by si.backord_id, ils.customer_backorder + NVL(res_bo.backorder_qty,0) desc;

BEGIN

   L_table := 'SVC_INVBACKORD, ITEM_LOC_SOH';
   SQL_LIB.SET_MARK('OPEN','C_VWH_RELEASE',L_table,NULL);
   open C_VWH_RELEASE;

   SQL_LIB.SET_MARK('FETCH','C_VWH_RELEASE',L_table,NULL);
   fetch C_VWH_RELEASE BULK COLLECT into L_vwh_process_TBL;

   SQL_LIB.SET_MARK('CLOSE','C_VWH_RELEASE',L_table,NULL);
   close C_VWH_RELEASE;

   if DISTRIBUTE_VWH_RELEASE(O_error_message,
                             IO_backord_tbl,
                             L_vwh_process_TBL) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then

      if C_VWH_RELEASE%ISOPEN then
         close C_VWH_RELEASE;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_VWH_REG_RELEASE;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_VWH_PACK_RELEASE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_backord_tbl      IN OUT   INVBACKORD_TBL,
                              I_WH_BACKORD_TBL    IN       WH_BACKORD_TBL)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)            := 'CORESVC_INVBACKORD.GET_VWH_PACK_RELEASE';
   L_vwh_process_TBL VWH_BACKORD_RELEASE_TBL := VWH_BACKORD_RELEASE_TBL();
   L_table           VARCHAR2(255);

   cursor C_VWH_RELEASE is
      select VWH_BACKORD_RELEASE_REC
                (backord_id,
                 item,
                 pack_ind,
                 vwh,
                 backorder_qty,
                 receive_as_type,
                 SUM(customer_backorder) + SUM(est_pack_backorder),
                 0,                                                     -- Update_customer_backorder: Delta customer back order. Will be updated later.
                 0)                                                     -- Update_pack_comp_backorder: Delta pack customer backorder. Will be updated later.
        from (
             -- Get the details for pack item row where inventory is maintained at pack level in the virtual warehouse.
             select si.backord_id,
                    si.item,
                    wb.pack_ind,
                    si.location,
                    wb.vwh vwh,
                    si.backorder_qty,
                    wb.receive_as_type,
                    (ils.customer_backorder + NVL(res_bo.backorder_qty,0)) customer_backorder,
                    0 est_pack_backorder
               from svc_invbackord si,
                    item_loc_soh ils,
                    TABLE(CAST(I_WH_BACKORD_TBL AS WH_BACKORD_TBL)) wb,
                    (select item,
                            location,
                            SUM(backorder_qty) backorder_qty
                       from TABLE(CAST(IO_backord_tbl as INVBACKORD_TBL))
                      where comp_item_ind = 'N'
                    group by item,location) res_bo                      -- To adjust item_loc_soh backorder reserve quantity for non-committed records in the collection
              where si.backord_id = wb.backord_id
                and wb.count_vwh > 1
                and wb.pack_ind = 'Y'
                and si.backorder_qty < 0
                and wb.location = si.location
                and ils.item = wb.item
                and ils.loc = wb.vwh
                and ils.item = res_bo.item(+)                           -- Using outer join because no records may exists in IO_backord_tbl to adjust.
                and ils.loc = res_bo.location(+)
             UNION ALL
             -- Get the details for pack item components.
             select si.backord_id,
                    si.item,
                    wb.pack_ind,
                    si.location,
                    wb.vwh vwh,
                    si.backorder_qty,
                    wb.receive_as_type,
                    0 customer_backorder,
                    FLOOR(MIN((ils.customer_backorder + NVL(res_bo.backorder_qty,0))/vpq.qty)) est_pack_backorder
               from svc_invbackord si,
                    item_loc_soh ils,                                   -- Item_loc_soh is being joined for pack component item (v_packsku_qty).
                    v_packsku_qty vpq,
                    item_master im,
                    TABLE(CAST(I_WH_BACKORD_TBL AS WH_BACKORD_TBL)) wb,
                    (select item,
                            location,
                            SUM(backorder_qty) backorder_qty
                       from TABLE(CAST(IO_backord_tbl as INVBACKORD_TBL))
                      where comp_item_ind = 'N'
                    group by item,location) res_bo                      -- To adjust item_loc_soh backorder reserve quantity for non-committed records in the collection
              where si.backord_id = wb.backord_id
                and wb.count_vwh > 1
                and wb.pack_ind = 'Y'
                and si.backorder_qty < 0
                and wb.pack_ind = 'Y'
                and si.item = vpq.pack_no
                and si.location = wb.location
                and NVL(wb.receive_as_type,'E') = 'E'
                and vpq.item = ils.item
                and vpq.item = im.item
                and im.inventory_ind = 'Y'                              -- Ignoring non-inventory pack component
                and wb.vwh = ils.loc
                and ils.item = res_bo.item(+)                           -- Using outer join because no records may exists in IO_backord_tbl to adjust. Also IO_backord_tbl will have pack component.
                and ils.loc = res_bo.location(+)
              group by si.backord_id, si.item, si.location, wb.pack_ind, wb.vwh, si.backorder_qty, wb.receive_as_type
              )
       group by backord_id, item, pack_ind, vwh, backorder_qty, receive_as_type
       order by backord_id, SUM(customer_backorder) + SUM(est_pack_backorder) desc;

BEGIN
   L_table := 'SVC_INVBACKORD, ITEM_LOC_SOH, V_PACKSKU_QTY, ITEM_MASTER';
   SQL_LIB.SET_MARK('OPEN','C_VWH_RELEASE',L_table,NULL);
   open C_VWH_RELEASE;

   SQL_LIB.SET_MARK('FETCH','C_VWH_RELEASE',L_table,NULL);
   fetch C_VWH_RELEASE BULK COLLECT into L_vwh_process_TBL;

   SQL_LIB.SET_MARK('CLOSE','C_VWH_RELEASE',L_table,NULL);
   close C_VWH_RELEASE;

   if DISTRIBUTE_VWH_RELEASE(O_error_message,
                             IO_backord_tbl,
                             L_vwh_process_TBL) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then

      if C_VWH_RELEASE%ISOPEN then
         close C_VWH_RELEASE;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_VWH_PACK_RELEASE;
-------------------------------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_VWH_RELEASE (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_backord_tbl      IN OUT   INVBACKORD_TBL,
                                 I_vwh_process_TBL   IN       VWH_BACKORD_RELEASE_TBL)
RETURN BOOLEAN IS

   L_vwh             WH.WH%TYPE;
   L_item            ITEM_MASTER.ITEM%TYPE;
   L_backord_id      SVC_INVBACKORD.BACKORD_ID%TYPE            := -1;
   L_backorder_qty   ITEM_LOC_SOH.CUSTOMER_BACKORDER%TYPE      := 0;
   L_distributed_qty ITEM_LOC_SOH.CUSTOMER_BACKORDER%TYPE      := 0;
   L_program         VARCHAR2(64)                              := 'CORESVC_INVBACKORD.DISTRIBUTE_VWH_RELEASE';
   L_vwh_process_TBL VWH_BACKORD_RELEASE_TBL                   := I_vwh_process_TBL;
   L_vwh_release_TBL INVBACKORD_TBL                            := INVBACKORD_TBL();
   L_table           VARCHAR2(255);

   -- Cursor for populating the backorder collection for update to item_loc_soh.
   CURSOR C_POPULATE_RESERVE_REC is
      select INVBACKORD_REC
             (bo.backord_id,
             bo.item,
             bo.pack_ind,
             bo.vwh,
             'W',                                                    -- Loc_type
             bo.update_customer_backorder * -1,
             'N')                                                    --  Pack Comp SOH indicator
        from TABLE(CAST(L_vwh_process_TBL AS VWH_BACKORD_RELEASE_TBL)) bo
       where bo.pack_ind = 'N'
          OR (     bo.pack_ind = 'Y'
               and NVL(bo.receive_as_type,'E') = 'P')                -- Fetch regular and item and record for Pack
      UNION ALL
      -- Fetch pack item component where receive_as_type is 'P' - Component inv will go to item_loc_soh.pack_comp_cust_back.
      select INVBACKORD_REC
             (bo.backord_id,
             vpq.item,
             bo.pack_ind,
             bo.vwh,
             'W',                                                     -- Loc_type
             vpq.qty * bo.update_customer_backorder * -1,
             'Y')
        from TABLE(CAST(L_vwh_process_TBL AS VWH_BACKORD_RELEASE_TBL)) bo,
             v_packsku_qty vpq,
             item_master im
       where bo.pack_ind = 'Y'
         and bo.item = vpq.pack_no
         and vpq.item = im.item
         and im.inventory_ind = 'Y'           -- Ignoring non-inventory pack component
         and NVL(bo.receive_as_type,'E') = 'P'
      UNION ALL
      -- Fetch pack item component where receive_as_type is 'E' - Only component inv is maintained for such pack/location.
      select INVBACKORD_REC
             (bo.backord_id,
             vpq.item,
             bo.pack_ind,
             bo.vwh,
             'W',                                                     -- Loc_type
             vpq.qty * bo.update_pack_comp_backorder * -1,
             'N')
        from TABLE(CAST(L_vwh_process_TBL AS VWH_BACKORD_RELEASE_TBL)) bo,
             v_packsku_qty vpq,
             item_master im
       where bo.pack_ind = 'Y'
         and bo.item = vpq.pack_no
         and vpq.item = im.item
         and im.inventory_ind = 'Y'            -- Ignoring non-inventory pack component
         and NVL(bo.receive_as_type,'E') = 'E';

BEGIN

   /* Loop through all the identified virtual warehouse for a backorder request. Distribute the back order request to the virtual warehouse
      based on the existing back order quantity in item_loc_soh. If after processing all the virtual warehouse, there are still some
      backorder quantity left, distribute the leftover quantities to the last virtual warehouse for that backorder request.
      For any back order quantity released from a item/virtual warehouse, loop through the remaining records in the input collection and
      adjust (decrease) the existing backorder quantity with the released quantity for the current row.
   */

   if L_vwh_process_TBL is NULL or L_vwh_process_TBL.COUNT = 0 then
      return TRUE;
   end if;

   FOR i in 1..L_vwh_process_TBL.COUNT LOOP
      if L_vwh_process_TBL(i).backord_id <> L_backord_id then

          -- Handling the scenario where the previous backorder id processing was not able to distribute the complete back order quantity.
          -- The left over back order quantity will be assigned to the last processed virtual warehouse of the previous backorder id.
          if L_backorder_qty > 0 then
             if NVL(L_vwh_process_TBL(i-1).receive_as_type,'E') = 'E' and L_vwh_process_TBL(i-1).pack_ind = 'Y' then
                L_vwh_process_TBL(i-1).update_pack_comp_backorder := L_vwh_process_TBL(i-1).update_pack_comp_backorder + L_backorder_qty;
             else
                L_vwh_process_TBL(i-1).update_customer_backorder := L_vwh_process_TBL(i-1).update_customer_backorder + L_backorder_qty;
             end if;

             -- Adjust the back order reserved quantity of the subsequent records in the collections for the same item Virtual warehouse
             -- for the decrease in reserve quantity by this record.
             if i <= L_vwh_process_TBL.COUNT then
                FOR j in i..L_vwh_process_TBL.COUNT LOOP
                   if L_vwh = L_vwh_process_TBL(j).vwh and L_item = L_vwh_process_TBL(j).item then
                      L_vwh_process_TBL(j).cur_backord_reserve := L_vwh_process_TBL(j).cur_backord_reserve - L_backorder_qty;
                   end if;
                END LOOP;
             end if;
          end if;

          L_backord_id     := L_vwh_process_TBL(i).backord_id;
          L_backorder_qty  := L_vwh_process_TBL(i).backorder_qty * -1;  -- Making backorder quantity positive for calculation purpose.
      end if;

      L_vwh            := L_vwh_process_TBL(i).vwh;
      L_item           := L_vwh_process_TBL(i).item;
      L_distributed_qty := 0;

      -- Distribute the outstanding back order quantity based on the current backorder reserve quantity.
      if L_backorder_qty > L_vwh_process_TBL(i).cur_backord_reserve and L_vwh_process_TBL(i).cur_backord_reserve > 0 then
         L_distributed_qty := L_vwh_process_TBL(i).cur_backord_reserve;
      else
         L_distributed_qty := L_backorder_qty;
      end if;

      -- If the receive as type for a pack item is Each, the inventory for the pack is maintained only at the pack component.
      if NVL(L_vwh_process_TBL(i).receive_as_type,'E') = 'E' and L_vwh_process_TBL(i).pack_ind = 'Y' then
         L_vwh_process_TBL(i).update_pack_comp_backorder := L_distributed_qty;
      else
         L_vwh_process_TBL(i).update_customer_backorder := L_distributed_qty;
      end if;

      L_backorder_qty := L_backorder_qty - L_distributed_qty;

      -- Adjust the back order reserved quantity of the subsequent records in the collections for the same item Virtual warehouse
      -- for the decrease in reserve quantity by this record.
      if i < L_vwh_process_TBL.COUNT then
         FOR j in i+1..L_vwh_process_TBL.COUNT LOOP
            if L_vwh = L_vwh_process_TBL(j).vwh and L_item = L_vwh_process_TBL(j).item then
               L_vwh_process_TBL(j).cur_backord_reserve := L_vwh_process_TBL(j).cur_backord_reserve - L_distributed_qty;
            end if;
         END LOOP;
      end if;

   END LOOP;

   -- Handling the scenario where the loop ended but the last processed backorder id had leftover backorder quantity.
   if L_backorder_qty > 0 then
      if NVL(L_vwh_process_TBL(L_vwh_process_TBL.LAST).receive_as_type,'E') = 'E' and L_vwh_process_TBL(L_vwh_process_TBL.LAST).pack_ind = 'Y' then
         L_vwh_process_TBL(L_vwh_process_TBL.LAST).update_pack_comp_backorder := L_vwh_process_TBL(L_vwh_process_TBL.LAST).update_pack_comp_backorder + L_backorder_qty;
      else
         L_vwh_process_TBL(L_vwh_process_TBL.LAST).update_customer_backorder := L_vwh_process_TBL(L_vwh_process_TBL.LAST).update_customer_backorder + L_backorder_qty;
      end if;
   end if;

   L_table := 'V_PACKSKU_QTY, ITEM_MASTER';
   SQL_LIB.SET_MARK('OPEN','C_POPULATE_RESERVE_REC',L_table,NULL);
   open C_POPULATE_RESERVE_REC;

   SQL_LIB.SET_MARK('FETCH','C_POPULATE_RESERVE_REC',L_table,NULL);
   fetch C_POPULATE_RESERVE_REC BULK COLLECT into L_vwh_release_TBL;

   SQL_LIB.SET_MARK('CLOSE','C_POPULATE_RESERVE_REC',L_table,NULL);
   close C_POPULATE_RESERVE_REC;

   IO_backord_tbl := IO_backord_tbl MULTISET UNION L_vwh_release_TBL;

   return TRUE;

EXCEPTION
   when OTHERS then

      if C_POPULATE_RESERVE_REC%ISOPEN then
         close C_POPULATE_RESERVE_REC;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DISTRIBUTE_VWH_RELEASE;
-------------------------------------------------------------------------------------------------------
FUNCTION PERSISTS_BACKORDER (O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_backord_tbl   IN      INVBACKORD_TBL)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'CORESVC_INVBACKORD.PERSISTS_BACKORDER';
   L_table        VARCHAR2(255);

   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_LOC_SOH is
     select 'x'
       from item_loc_soh ils,
            TABLE(CAST(I_backord_tbl AS INVBACKORD_TBL)) inv_tbl
      where ils.item = inv_tbl.item
        and ils.loc  = inv_tbl.location
        for update of ils.customer_backorder,ils.pack_comp_cust_back nowait;

BEGIN

   L_table := 'ITEM_LOC_SOH';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_LOC_SOH',L_table,NULL);
   open C_LOCK_ITEM_LOC_SOH;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_LOC_SOH',L_table,NULL);
   close C_LOCK_ITEM_LOC_SOH;

   /* Bulk update item_loc_soh table using I_backord_tbl. For recods where comp_item_ind is 'N', update the
      customer_backorder column. For recods with comp_item_ind as 'Y', update the pack_comp_cust_back column.
   */
   if I_backord_tbl is NOT NULL and I_backord_tbl.COUNT > 0 then
      FORALL i IN 1..I_backord_tbl.COUNT
         update item_loc_soh ils
            set customer_backorder = decode (I_backord_tbl(i).comp_item_ind,'N',
                                             (ils.customer_backorder + I_backord_tbl(i).backorder_qty),
                                             (ils.customer_backorder + 0)),
                pack_comp_cust_back = decode (I_backord_tbl(i).comp_item_ind,'Y',
                                              (ils.pack_comp_cust_back + I_backord_tbl(i).backorder_qty),
                                              (ils.pack_comp_cust_back + 0)),
                last_update_datetime = SYSDATE,
                last_update_id       = LP_user
          where ils.item = I_backord_tbl(i).item
            and ils.loc  = I_backord_tbl(i).location;

      --Insert location,loc_type into inv_resv_update_temp table which will be used by extract to AIP to send the backorder quantity
      --details of location against which backorder was received
      insert into inv_resv_update_temp (location,
                                        loc_type,
                                        process_status,
                                        create_datetime,
                                        create_id,
                                        last_update_datetime,
                                        last_update_id)
                        select distinct inv_tbl.location,
                                        inv_tbl.loc_type,
                                        LP_backorder_new,
                                        SYSDATE,
                                        LP_user,
                                        SYSDATE,
                                        LP_user
                                   from TABLE(CAST(I_backord_tbl AS INVBACKORD_TBL)) inv_tbl
                                  where not exists (select 'x'
                                                      from inv_resv_update_temp bt
                                                     where bt.location = inv_tbl.location
                                                       and bt.process_status = LP_backorder_new);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ITEM_LOC_SOH%ISOPEN then
         close C_LOCK_ITEM_LOC_SOH;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;

   when OTHERS then
      if C_LOCK_ITEM_LOC_SOH%ISOPEN then
         close C_LOCK_ITEM_LOC_SOH;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSISTS_BACKORDER;
-------------------------------------------------------------------------------------------------------
END CORESVC_INVBACKORD;
/