CREATE OR REPLACE PACKAGE BODY SVCPROV_INVAVAIL AS
--------------------------------------------------------------------------------
---                          PUBLIC PROCEDURE                                ---
--------------------------------------------------------------------------------
PROCEDURE GET_INV_DETAIL(O_ServiceOperationStatus    IN OUT "RIB_ServiceOpStatus_REC",
                         O_business_object           OUT    "RIB_InvAvailColDesc_REC",
                         I_business_object           IN     "RIB_InvAvailCriVo_REC")
IS
   L_Program                 VARCHAR2(50)                   := 'SVCPROV_INVAVAIL.GET_INV_DETAIL';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE; 
   L_error_tbl               SVCPROV_UTILITY.ERROR_TBL;
   L_status_code             VARCHAR2(1);
   PROGRAM_ERROR             EXCEPTION;

BEGIN
-- The procedure calls the core layer which processes the input lookup message.
-- The core layer will return true for successful processing.
-- In case of unhandled error, L_error_message will be populated with the unhandled error. 
-- In case of validation error from the above call, the procedure calls SVCPROV_UTILITY.PARSE_ERR_MSG()
-- to build FailStatus_TBL object in the output RIB_ServiceOpStatus_REC based on ERROR_TBL returned.

   
   if CORESVC_INVAVAIL.GET_INV_DETAIL(L_error_message,
                                      O_business_object,
                                      L_error_tbl,
                                      I_business_object) = FALSE then
      
      if L_error_tbl is NOT NULL and L_error_tbl.COUNT > 0 then
         -- Update the output service operation status object FailStatus_TBL with the validation errors.
         if SVCPROV_UTILITY.PARSE_ERR_MSG(L_error_message,
                                          O_ServiceOperationStatus.FailStatus_TBL,
                                          L_error_tbl) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if; 
         
      raise PROGRAM_ERROR;
   end if;
   
EXCEPTION
   when PROGRAM_ERROR then
      L_status_code := API_CODES.UNHANDLED_ERROR;

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_ServiceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_PROGRAM);
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_ServiceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_PROGRAM);
   
END GET_INV_DETAIL;
-------------------------------------------------------------------------------------------------------
END SVCPROV_INVAVAIL;
/
