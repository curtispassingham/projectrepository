CREATE OR REPLACE PACKAGE WF_RETURN_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------------------------------
   -- PACKAGE RECORD TYPES
---------------------------------------------------------------------------------------------------------------------
TYPE wf_tsf_item_rec IS RECORD(item           ITEM_MASTER.ITEM%TYPE,
                               returned_qty   WF_RETURN_DETAIL.RETURNED_QTY%TYPE);

TYPE wf_tsf_item_tbl IS TABLE of wf_tsf_item_rec INDEX BY BINARY_INTEGER;

TYPE wf_return_dtl_rec IS RECORD(item                         ITEM_MASTER.ITEM%TYPE,
                                 item_desc                    ITEM_MASTER.ITEM_DESC%TYPE,
                                 wf_order_id                  WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                                 order_qty                    WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                                 customer_cost                WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                 cust_ord_ref_no              WF_ORDER_HEAD.CUST_ORD_REF_NO%TYPE,
                                 return_unit_cost             WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE,
                                 returned_qty                 WF_RETURN_DETAIL.RETURNED_QTY%TYPE,
                                 final_return_unit_cost       WF_RETURN_DETAIL.NET_RETURN_UNIT_COST%TYPE,
                                 vat                          WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
                                 vat_rate                     VAT_CODE_RATES.VAT_RATE%TYPE,
                                 return_reason                WF_RETURN_DETAIL.RETURN_REASON%TYPE,
                                 cancel_reason                WF_RETURN_DETAIL.CANCEL_REASON%TYPE,
                                 ref_item                     ITEM_MASTER.ITEM%TYPE,
                                 restock_type                 WF_RETURN_DETAIL.RESTOCK_TYPE%TYPE,
                                 unit_restock_fee             WF_RETURN_DETAIL.UNIT_RESTOCK_FEE%TYPE,
                                 final_return_total_cost      WF_RETURN_DETAIL.NET_RETURN_UNIT_COST%TYPE,
                                 return_unit_cost_incl_vat    WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE,
                                 final_ret_unit_cost_invat    WF_RETURN_DETAIL.NET_RETURN_UNIT_COST%TYPE,
                                 final_ret_total_cost_invat   WF_RETURN_DETAIL.NET_RETURN_UNIT_COST%TYPE,
                                 row_id                       VARCHAR2(25),
                                 return_code                  VARCHAR2(10),
                                 error_message                RTK_ERRORS.RTK_TEXT%TYPE);

TYPE wf_return_dtl_tbl IS TABLE of wf_return_dtl_rec INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
---------------------------------------------------------------------------------------------------------------------
-- Function   : NEXT_RMA_NUMBER
-- Purpose    : This public function generates a new RMA number from a sequence and returns the number..
---------------------------------------------------------------------------------------------------------------------
FUNCTION NEXT_RMA_NUMBER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : VALIDATE_RMA_NUMBER
-- Purpose    : This public function validates the passed RMA number and returns TRUE for O_exists parameter if the
--              RMA number exists and returns FALSE if RMA doesn't exist..
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RMA_NUMBER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : VALIDATE_ITEM
-- Purpose    : This function checks whether input item or a pack item, the input item is a component of, exists
--              on the franchise order and returns TRUE for O_exists if exists on order else returns FALSE.
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       I_wf_order_id     IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                       I_item            IN       WF_ORDER_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : GET_TOTALS
-- Purpose    : This public function calculates the total amounts for a franchise return and returns the values..
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_total_return_amt         IN OUT   WF_RETURN_HEAD.TOTAL_RETURN_AMT%TYPE,
                    O_total_restock_amt        IN OUT   WF_RETURN_HEAD.TOTAL_RESTOCK_AMT%TYPE,
                    O_total_final_return_amt   IN OUT   WF_RETURN_HEAD.TOTAL_NET_RETURN_AMT%TYPE,
                    I_rma_no                   IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Procedure   : FETCH_WF_RETURN_DATA
-- Purpose     : This public procedure fetches data to be displayed in multi-view block of wfreturn.fmb form.
---------------------------------------------------------------------------------------------------------------------
PROCEDURE FETCH_WF_RETURN_DATA(O_wf_return_tbl   IN OUT   WF_RETURN_SQL.WF_RETURN_DTL_TBL,
                               I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE);
---------------------------------------------------------------------------------------------------------------------
--- Procedure   : LOCK_PROCEDURE
--- Purpose     : This procedure will lock records on WF_RETURN_DETAIL table.
---------------------------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE(O_wf_return_tbl   IN OUT   WF_RETURN_SQL.WF_RETURN_DTL_TBL);
---------------------------------------------------------------------------------------------------------------------
-- Procedure   : INS_UPD_WF_RETURN_DATA
-- Purpose     : This public procedure inserts data from multi-view block of wfreturn form to WF_RETURN_DETAIL table.
---------------------------------------------------------------------------------------------------------------------
PROCEDURE INS_UPD_WF_RETURN_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_return_code     IN OUT   VARCHAR2,
                                 I_wf_return_tbl   IN       WF_RETURN_SQL.WF_RETURN_DTL_TBL,
                                 I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE);
---------------------------------------------------------------------------------------------------------------------
-- Function   : DELETE_ITEM
-- Purpose    : This public function deletes an item from wf_return_detail table for a franchise return.
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                     I_item            IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : GET_ITEM_RETURN_COST
-- Purpose    : This public function will fetch item cost from the given franchise order and convert the value to the
--              currency of franchise return and return the final value. If input item is a component of a pack item
--              on the franchise order, pack item cost will be fetched and the component item cost will be derived
--              from it.
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_RETURN_COST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_return_cost       IN OUT   WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE,
                              I_return_currency   IN       WF_RETURN_HEAD.CURRENCY_CODE%TYPE,
                              I_return_from_loc   IN       WF_RETURN_HEAD.CUSTOMER_LOC%TYPE,
                              I_wf_order_no       IN       WF_RETURN_DETAIL.WF_ORDER_NO%TYPE,
                              I_item              IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : APPROVE
-- Purpose    : This public function creates a transfer for a franchise return. If the return method is 'R' and
--              franchise store is non-stock holding, shipment for the transfer is automatically created. If the
--              return type is 'D', shipment, receiving is done automatically for the transer, financial posting is
--              done and franchise return is closed. The function also publishes a message to WMS about the item
--              returns, and thus inventory changes that may follow soon.
---------------------------------------------------------------------------------------------------------------------
FUNCTION APPROVE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : DELETE
-- Purpose    : This public function deletes a franchise return from WF_RETURN_HEAD and WF_RETURN_DETAIL tables if
--              the return is in 'Input' status.
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : CHECK_DUPLICATE_ITEM
-- Purpose    : This public function checks whether the item being added to a return already exists in the return.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                              I_item            IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : CANCEL_HEAD
-- Purpose    : This public function cancels a franchise return in 'Approved' or 'In Progress' state. If return is in
--              approved state, return is completely cancelled. If return is in 'In progress' state, only the quantity
--              that is not yet shipped will be cancelled.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CANCEL_HEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                     I_cancel_reason   IN       WF_RETURN_DETAIL.CANCEL_REASON%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : GET_SHIPPED_QTY
-- Purpose    : This public function fetches the shipped quantity of an item in a franchise return. For a return in
--              approved or in progress state, quantity of an item can't be cancelled beyond the shipped qty.
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_SHIPPED_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_shipped_qty     IN OUT   WF_RETURN_DETAIL.RETURNED_QTY%TYPE,
                         I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                         I_item            IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : CHECK_ITEM_LOC
-- Purpose    : This functions checks whether all items in a franchise return are ranged to return location and
--              returns FALSE for O_valid_itemloc if itemloc ranging doesn't exist for any items.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid_itemloc   IN OUT   BOOLEAN,
                        I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : CREATE_ITEM_LOC
-- Purpose    : This function finds the items, in a franchise return, that are not ranged to the return location and
--              creates new item-loc combinations for them.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : DELETE_INVALID_ITEMLOC
-- Purpose    : This function fetches the items in a franchise return that do not have valid item-return location
--              ranging and deletes the items from the return.
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_INVALID_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : VALIDATE_CUSTOMER_REF_NO
-- Purpose    : This function is used to validate customer return reference number. The function returns TRUE for
--              O_exists if the input cust ref no exists.
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CUSTOMER_REF_NO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT   BOOLEAN,
                                  I_ret_ref_no      IN       WF_return_HEAD.CUST_RET_REF_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : VALIDATE_ORDER_CUSTOMER
-- Purpose    : This function checks whether the franchise order used in a return belongs to the same customer as in
--              return and returns FALSE for O_valid if the customer is different.
---------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER_CUSTOMER(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid             IN OUT   BOOLEAN,
                                 I_return_customer   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
                                 I_wf_order_no       IN       WF_RETURN_DETAIL.WF_ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : SYNC_DEPOSIT_ITEMS
-- Purpose    : This function is called after every create, update, delete operation for a return on wf_return_detail
--              table. The function will create new deposit containers or update the quantity of existing containers
--              to keep in sync with the deposit contents in a return. If there are any containers in a return without
--              a corresponding deposit content, the containers will be deleted.
---------------------------------------------------------------------------------------------------------------------
FUNCTION SYNC_DEPOSIT_ITEMS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_rma_no            IN       WF_RETURN_HEAD.RMA_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : GET_RETURN_COST
-- Purpose    : The function expects only one of transfer number and rma number. If transfer number is passed, the
--              corresponding RMA number is fetched.
--              The function retrieves return unit cost and restocking cost of the input item in the franchise return
--              and returns the values. The function also returns the currency of return cost as O_return_currency.
--              If no records are found for the franchise return, and item combination, the function will return NULL
--              values for output variables.
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_RETURN_COST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_return_cost       IN OUT   WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE,
                         O_return_currency   IN OUT   WF_RETURN_HEAD.CURRENCY_CODE%TYPE,
                         O_restocking_cost   IN OUT   WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE,
                         I_rma_no            IN       WF_RETURN_DETAIL.RMA_NO%TYPE,
                         I_tsf_no            IN       TSFHEAD.TSF_NO%TYPE,
                         I_item              IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : GET_TAX_INFO
-- Purpose    : This is overloaded function which returns tax rate for an item and location combination.
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INFO(I_item              IN   WF_RETURN_DETAIL.ITEM%TYPE,
                      I_loc               IN   ITEM_LOC.LOC%TYPE,
                      I_loc_type          IN   ITEM_LOC.LOC_TYPE%TYPE,
                      I_cost_retail_ind   IN   VARCHAR2,
                      I_from_loc          IN   ITEM_LOC.LOC%TYPE DEFAULT NULL,
                      I_from_loc_type     IN   ITEM_LOC.LOC_TYPE%TYPE DEFAULT NULL,
                      I_rma_no            IN   WF_RETURN_HEAD.RMA_NO%TYPE DEFAULT NULL)
RETURN NUMBER;
---------------------------------------------------------------------------------------------------------------------
-- Function   : GET_TAX_INFO
-- Purpose    : This function fetches tax rate and tax code effective on the VDATE for an item and location combination.
--              Either 'C' or 'R' should be passed for cost_retail_ind.
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INFO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_tax_rate          IN OUT   VAT_CODE_RATES.VAT_RATE%TYPE,
                      O_tax_code          IN OUT   VAT_CODES.VAT_CODE%TYPE,
                      I_item              IN       WF_RETURN_DETAIL.ITEM%TYPE,
                      I_loc               IN       ITEM_LOC.LOC%TYPE,
                      I_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                      I_cost_retail_ind   IN       VARCHAR2,
                      I_from_loc          IN       ITEM_LOC.LOC%TYPE DEFAULT NULL,
                      I_from_loc_type     IN       ITEM_LOC.LOC_TYPE%TYPE DEFAULT NULL,
                      I_rma_no            IN       WF_RETURN_HEAD.RMA_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : CHECK_ITEM_RANGED_IND
-- Purpose    : This function returns FALSE for O_ranged parameter if there is atleast one item in a franchise return
--              with RANGED_IND = 'N' at return location. If RANGED_IND='Y' for all item-return location combinations,
--              O_ranged will be TRUE.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_RANGED_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_ranged            IN OUT   BOOLEAN,
                               I_rma_no            IN       WF_RETURN_HEAD.RMA_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : CHANGE_ITEM_RANGED_IND
-- Purpose    : This function changes RANGED_IND to 'Y' for all items in a franchise return having the indicator as 'N'
--              at return location.
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_ITEM_RANGED_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rma_no            IN       WF_RETURN_HEAD.RMA_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : INS_UPD_WF_RETURN_DATA_WRP
-- Purpose    : This is wrapper function for INS_UPD_WF_RETURN_DATA that passes a database type object instead PL/SQL type
--              as an input parameter to be called from Java wrappers.
---------------------------------------------------------------------------------------------------------------------
FUNCTION INS_UPD_WF_RETURN_DATA_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_return_code     IN OUT   VARCHAR2,
                                    I_wf_return_tbl   IN       WRP_WF_RETURN_DETAIL_TBL,
                                    I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE)
RETURN INTEGER;
---------------------------------------------------------------------------------------------------------------------
-- Function   : MASS_UPDATE
-- Purpose    : This function is does a mass update of restock type and fee for a given RMA No.
---------------------------------------------------------------------------------------------------------------------
FUNCTION MASS_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                     I_restock_type    IN       WF_RETURN_DETAIL.RESTOCK_TYPE%TYPE,
                     I_restock_fee     IN       WF_RETURN_DETAIL.UNIT_RESTOCK_FEE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function   : GET_ITEM_RETURN_COST
-- Purpose    : This is overloaded function which returns order cost for an item and order combination.
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_RETURN_COST(I_return_currency   IN       WF_RETURN_HEAD.CURRENCY_CODE%TYPE,
                              I_return_from_loc   IN       WF_RETURN_HEAD.CUSTOMER_LOC%TYPE,
                              I_wf_order_no       IN       WF_RETURN_DETAIL.WF_ORDER_NO%TYPE,
                              I_item              IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN NUMBER;
---------------------------------------------------------------------------------------------------------------------
-- Function   : GET_ITEM_RETURN_QTY
-- Purpose    : This function returns order qty for an item and order combination.
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_RETURN_QTY(I_wf_order_no       IN       WF_RETURN_DETAIL.WF_ORDER_NO%TYPE,
                             I_item              IN       WF_RETURN_DETAIL.ITEM%TYPE)
   RETURN NUMBER;
--------------------------------------------------------------------------------------------------------------------- 
END WF_RETURN_SQL;
/