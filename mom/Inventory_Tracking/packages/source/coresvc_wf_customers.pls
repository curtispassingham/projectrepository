CREATE OR REPLACE PACKAGE CORESVC_WF_CUSTOMER AUTHID CURRENT_USER AS
   template_key          CONSTANT VARCHAR2(255)   := 'WF_CUSTOMER_DATA';
   action_new                     VARCHAR2(25)    := 'NEW';
   action_mod                     VARCHAR2(25)    := 'MOD';
   action_del                     VARCHAR2(25)    := 'DEL';

   WF_CUST_sheet                  VARCHAR2(255)   := 'WF_CUSTOMER';
   WF_CUST$Action                 NUMBER          :=1;
   WF_CUST$WF_CUSTOMER_ID         NUMBER          :=2;
   WF_CUST$WF_CUSTOMER_NAME       NUMBER          :=3;
   WF_CUST$CREDIT_IND             NUMBER          :=4;
   WF_CUST$WF_CUSTOMER_GROUP_ID   NUMBER          :=5;
   WF_CUST$AUTO_APPROVE_IND       NUMBER          :=6;

   WF_CUST_TL_sheet               VARCHAR2(255)   := 'WF_CUSTOMER_TL';
   WF_CUST_TL$Action              NUMBER          :=1;
   WF_CUST_TL$LANG                NUMBER          :=2;
   WF_CUST_TL$WF_CUSTOMER_ID      NUMBER          :=3;
   WF_CUST_TL$WF_CUSTOMER_NAME    NUMBER          :=4;
   
   WFCUSTG_sheet                   VARCHAR2(255)  :='WF_CUST_GRP';
   WFCUSTG$Action                  NUMBER         :=1;
   WFCUSTG$WF_CUSTOMER_GROUP_ID    NUMBER         :=2;
   WFCUSTG$WF_CUSTOMER_GROUP_NAME  NUMBER         :=3;

   WFCUSTG_TL_sheet                VARCHAR2(255)  :='WF_CUST_GRP_TL';
   WFCUSTG_TL$Action               NUMBER         :=1;
   WFCUSTG_TL$LANG                 NUMBER         :=2;
   WFCUSTG_TL$WF_CUSTG_ID          NUMBER         :=3;
   WFCUSTG_TL$WF_CUSTG_NAME        NUMBER         :=4;

   sheet_name_trans               S9T_PKG.trans_map_typ;
   action_column                  VARCHAR2(255)         := 'ACTION';
   template_category              CODE_DETAIL.CODE%TYPE := 'RMSFRN';
   TYPE WF_CUST_rec_tab IS TABLE OF WF_CUSTOMER%ROWTYPE;
   TYPE WFCUSTG_rec_tab IS TABLE OF WF_CUSTOMER_GROUP%ROWTYPE;
------------------------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
------------------------------------------------------------------------------------------------------
END CORESVC_WF_CUSTOMER;
/