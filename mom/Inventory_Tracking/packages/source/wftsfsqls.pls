CREATE OR REPLACE PACKAGE WF_TRANSFER_SQL AUTHID CURRENT_USER AS

-- Constant Variables
ACTION_CREATE_WF_ORDER      CONSTANT VARCHAR2(1)  := 'O';
ACTION_CREATE_WF_RETURN     CONSTANT VARCHAR2(1)  := 'R';
ACTION_CREATE_WF_NONE       CONSTANT VARCHAR2(1)  := 'N';

WF_STATUS_APPROVE           CONSTANT VARCHAR2(1)  := 'A';
WF_STATUS_CANCEL            CONSTANT VARCHAR2(1)  := 'C';
WF_STATUS_INPUT             CONSTANT VARCHAR2(1)  := 'I';
WF_STATUS_INPROGRESS        CONSTANT VARCHAR2(1)  := 'P';
WF_STATUS_REQ_CREDIT        CONSTANT VARCHAR2(1)  := 'R';

-----------------------------------------------------------------------------------
-- WF_CREATE_TSF_HEAD
-- This function will create transfer header records for the franchise orders and returns.
-----------------------------------------------------------------------------------
FUNCTION WF_CREATE_TSF_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tsf_no          IN OUT   TSFHEAD.TSF_NO%TYPE,
                             I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                             I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                             I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                             I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                             I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                             I_need_date       IN       TSFHEAD.DELIVERY_DATE%TYPE,
                             I_wf_order_no     IN       TSFHEAD.WF_ORDER_NO%TYPE,
                             I_rma_no          IN       TSFHEAD.RMA_NO%TYPE,
                             I_status          IN       TSFHEAD.STATUS%TYPE)
RETURN BOOLEAN ;

-----------------------------------------------------------------------------------
-- WF_CREATE_TSF_DETAIL
-- This function will create transfer detail records for the franchise orders and returns.
-----------------------------------------------------------------------------------
FUNCTION WF_CREATE_TSF_DETAIL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_qty             IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------
-- WF_UPDATE_TSF_HEAD
-- This function will update the status of the transfer head record.
-----------------------------------------------------------------------------------
FUNCTION WF_UPDATE_TSF_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                             I_status          IN       TSFHEAD.STATUS%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------

-- WF_UPDATE_TSF_DETAIL
-- This function will update the tsfdetails records for the franchise orders and returns.
-----------------------------------------------------------------------------------
FUNCTION WF_UPDATE_TSF_DETAIL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------
-- WF_DELETE_TSF_HEAD
-- This function will delete all the tsfdetail records and then the tsfheader records.
-----------------------------------------------------------------------------------
FUNCTION WF_DELETE_TSF_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------
-- WF_DELETE_TSF_DETAIL
-- This  function will delete the tsfdetails records for the passed item.
-- It will also delete the transfer if there are no records in the tsdfdetails for 
-- the franchise orders.
-----------------------------------------------------------------------------------
FUNCTION WF_DELETE_TSF_DETAIL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------
-- WF_UPD_ITEM_RESV
-- The new function will reserve the quantities for the WF orders.
--
-----------------------------------------------------------------------------------
FUNCTION WF_UPD_ITEM_RESV (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE,
                           I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                           I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- WF_UPD_ITEM_EXP
-- The new function will update expected quantities for the WF returns.
--
-----------------------------------------------------------------------------------
FUNCTION WF_UPD_ITEM_EXP ( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE,
                           I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                           I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE )
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- STORE_ORDER_EXISTS
-- This function checks for the existance of store order in the system based on
-- input item/store/need date
----------------------------------------------------------------------------
FUNCTION STORE_ORDER_EXISTS ( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists         IN OUT  BOOLEAN,
                              O_processed      IN OUT  BOOLEAN,
                              I_item           IN      STORE_ORDERS.ITEM%TYPE,
                              I_store          IN      STORE_ORDERS.STORE%TYPE,
                              I_need_date      IN      WF_ORDER_DETAIL.NEED_DATE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- STORE_ORDER_EXISTS
-- This function checks for the existance of store order in the system based on 
-- wf_order_no/wf_order_line_no
----------------------------------------------------------------------------
FUNCTION STORE_ORDER_EXISTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists           IN OUT  BOOLEAN,
                            O_processed        IN OUT  BOOLEAN,
                            I_wf_order_no      IN      STORE_ORDERS.WF_ORDER_NO%TYPE,
                            I_wf_order_line_no IN      STORE_ORDERS.WF_ORDER_LINE_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- DELETE_STORE_ORDER
-- This function will delete the store_orders records for the item/store/need date.
----------------------------------------------------------------------------
FUNCTION DELETE_STORE_ORDER ( O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_wf_order_no       IN      WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                              I_wf_order_line_no  IN      WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- CREATE_STORE_ORDER
-- This function will create a store_orders records for the wf_order_line_no.
----------------------------------------------------------------------------
FUNCTION CREATE_STORE_ORDER ( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item             IN       STORE_ORDERS.ITEM%TYPE,
                              I_store            IN       STORE_ORDERS.STORE%TYPE,
                              I_need_date        IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
                              I_qty              IN       TSFDETAIL.TSF_QTY%TYPE,
                              I_wf_order_no      IN       STORE_ORDERS.WF_ORDER_NO%TYPE,
                              I_wf_order_line_no IN       STORE_ORDERS.WF_ORDER_LINE_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- UPDATE_STORE_ORDER
-- This function will update a store_orders records for the wf_order_line_no.
----------------------------------------------------------------------------
FUNCTION UPDATE_STORE_ORDER(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wf_order_no      IN      STORE_ORDERS.WF_ORDER_NO%TYPE,
                            I_wf_order_line_no IN      STORE_ORDERS.WF_ORDER_LINE_NO%TYPE,
                            I_qty              IN      TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- BUILD_TSF_STORE_ORDER
-- This function will create, delete all transfer and store orders that were staged in the APPROVE_WF_ORDER.
----------------------------------------------------------------------------
FUNCTION BUILD_TSF_STORE_ORDER(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_wf_order_no          IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                               I_status               IN      WF_ORDER_HEAD.STATUS%TYPE,                               
                               I_transfer_temp_tbl    IN      OBJ_F_TRANSFER_TBL,
                               I_store_order_temp_tbl IN      OBJ_F_STORE_ORDER_TBL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- DELETE_FRANCHISE_TSF
-- This function will delete the transfer records for the given franchise order or franchise return.
-----------------------------------------------------------------------------------
FUNCTION DELETE_FRANCHISE_TSF ( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no     IN       TSFHEAD.WF_ORDER_NO%TYPE,
                                I_rma_no          IN       TSFHEAD.RMA_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- DELETE_FRANCHISE_TSF_DETAIL
-- This function will delete the transfer records for the given franchise order.
-----------------------------------------------------------------------------------
FUNCTION DELETE_FRANCHISE_TSF_DETAIL ( O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_wf_order_no        IN       TSFHEAD.WF_ORDER_NO%TYPE,
                                       I_rma_no             IN       TSFHEAD.RMA_NO%TYPE,
                                       I_item               IN       TSFDETAIL.ITEM%TYPE,
                                       I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                                       I_to_loc             IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION CANCEL_FRANCHISE_TSF ( O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no        IN       TSFHEAD.WF_ORDER_NO%TYPE,
                                I_rma_no             IN       TSFHEAD.RMA_NO%TYPE)                              
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION MODIFY_FRANCHISE_TSF ( O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no        IN       TSFHEAD.WF_ORDER_NO%TYPE,
                                I_rma_no             IN       TSFHEAD.RMA_NO%TYPE)                              
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : DETERMINE_F_ORDRET_STATUS
--       Purpose : This function will determine the status of the franchise order
--                 and the linked trasnfer to be created.
-----------------------------------------------------------------------------------
FUNCTION DETERMINE_F_ORDRET_STATUS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_tsf_status      IN OUT   TSFHEAD.STATUS%TYPE,
                                    O_f_order_status  IN OUT   WF_ORDER_HEAD.STATUS%TYPE,
                                    O_f_return_status IN OUT   WF_RETURN_HEAD.STATUS%TYPE,
                                    I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                                    I_from_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                                    I_to_loc          IN       ITEM_LOC.LOC%TYPE,
                                    I_to_loc_type     IN       ITEM_LOC.LOC_TYPE%TYPE,
                                    I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                                    I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : IS_FRANCHISE_ORDER_RETURN
--       Purpose : This function will check if the input from loc and to loc
--                 combination result in a company to/from franchise location
--                 movement
-----------------------------------------------------------------------------------
FUNCTION IS_FRANCHISE_ORDER_RETURN (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_action_type     IN OUT   VARCHAR2,
                                    I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                                    I_from_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                                    I_to_loc          IN       ITEM_LOC.LOC%TYPE,
                                    I_to_loc_type     IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : SYNC_F_ORDER
--       Purpose : This function will sync the linked Franchise order
-----------------------------------------------------------------------------------
FUNCTION SYNC_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_wf_order_no     IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       O_tsf_status      IN OUT   TSFHEAD.STATUS%TYPE,
                       I_action_type     IN       VARCHAR2,
                       I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                       I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                       I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : SYNC_F_RETURN
--       Purpose : This function will sync the linked Franchise return
-----------------------------------------------------------------------------------
FUNCTION SYNC_F_RETURN (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE,
                        I_action_type     IN       VARCHAR2,
                        I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                        I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                        I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : CANCEL_F_ORDER
--       Purpose : This function will cancel the linked Franchise order
-----------------------------------------------------------------------------------
FUNCTION CANCEL_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : CANCEL_F_RETURN
--       Purpose : This function will cancel the linked Franchise return
-----------------------------------------------------------------------------------
FUNCTION CANCEL_F_RETURN (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : GET_WF_ORDER_RMA_NO
--       Purpose : This function returns wf_order or RMA number associated with the 
--                 input distro number.
-----------------------------------------------------------------------------------
FUNCTION GET_WF_ORDER_RMA_NO(O_error_message        IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                             O_wf_order_no          IN OUT      WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                             O_rma_no               IN OUT      WF_RETURN_HEAD.RMA_NO%TYPE,
                             I_distro_type          IN          SHIPSKU.DISTRO_TYPE%TYPE,
                             I_distro_no            IN          SHIPSKU.DISTRO_NO%TYPE,
                             I_to_loc               IN          TSFHEAD.TO_LOC%TYPE,
                             I_to_loc_type          IN          TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : BULK_SYNC_F_ORDER
--       Purpose : This functions is similar to SYNC_F_ORDER but has been designed 
--                 to handle bulk creation of franchise order for transfers. The current
--                 implementation will call SYNC_F_ORDER in a loop.
-----------------------------------------------------------------------------------
FUNCTION BULK_SYNC_F_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_action_type     IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END WF_TRANSFER_SQL;
/