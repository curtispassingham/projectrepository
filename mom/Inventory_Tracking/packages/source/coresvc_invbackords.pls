CREATE OR REPLACE PACKAGE CORESVC_INVBACKORD AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- Function Name: CREATE_BACKORDER
-- Purpose: This public function will perform the basic validation on staged 
--          backorder requests associated with a given process_id.
--------------------------------------------------------------------------------
FUNCTION CREATE_BACKORDER(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_process_id   IN OUT  SVC_INVBACKORD.PROCESS_ID%TYPE,
                          IO_chunk_id     IN OUT  SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END CORESVC_INVBACKORD;
/