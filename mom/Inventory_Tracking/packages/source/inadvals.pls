
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE INVADJ_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Function Name: REASON_EXIST
-- Purpose      : Check to see if the REASON is valid
-------------------------------------------------------------------------------
FUNCTION REASON_EXIST(I_reason         IN      INV_ADJ_REASON.REASON%TYPE,
                      O_reason_desc    IN OUT  INV_ADJ_REASON_TL.REASON_DESC%TYPE,
                      O_error_message  IN OUT  VARCHAR2,
                      O_found          IN OUT  BOOLEAN)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: LOCATION_EXIST
-- Purpose      : Check to see if the LOCATION is valid
-------------------------------------------------------------------------------
FUNCTION LOCATION_EXIST(I_location       IN      ITEM_LOC_SOH.LOC%TYPE,
                        I_item           IN      ITEM_MASTER.ITEM%TYPE,
                        I_loc_type       IN      INV_ADJ.LOC_TYPE%TYPE,
                        I_inv_status     IN      INV_STATUS_QTY.INV_STATUS%TYPE,
                        O_location_desc  IN OUT  PARTNER.PARTNER_DESC%TYPE,
                        O_stock_on_hand  IN OUT  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        O_error_message  IN OUT  VARCHAR2,
                        O_found          IN OUT  BOOLEAN)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: ITEM_LOC_EXIST
-- Purpose      : Check to see if the item is a valid
--                Staple/Fashion/Non-sellable Pack at a given location.
-------------------------------------------------------------------------------
FUNCTION ITEM_LOC_EXIST(I_item           IN      ITEM_MASTER.ITEM%TYPE,
                        I_location       IN      ITEM_LOC_SOH.LOC%TYPE,
                        I_loc_type       IN      ITEM_LOC_SOH.LOC_TYPE%TYPE,
                        I_inv_status     IN      INV_ADJ.INV_STATUS%TYPE,
                        O_stock_on_hand  IN OUT  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        O_error_message  IN OUT  VARCHAR2,
                        O_found          IN OUT  BOOLEAN)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--- Function:  CHECK_INV_STATUS_EXIST
--- Purpose:   Check the inv_status_type for duplicate INV_STATUS entries.
-------------------------------------------------------------------------------
FUNCTION CHECK_INV_STATUS_EXIST(I_inv_status     IN      INV_STATUS_TYPES.INV_STATUS%TYPE,
                                O_error_message  IN OUT  VARCHAR2,
                                O_found          IN OUT  BOOLEAN)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--- Function:  INV_STATUS_CONSTRAINTS_EXIST
--- Purpose:   Check the inv_status_type for foreign constraint violations.
-------------------------------------------------------------------------------
FUNCTION INV_STATUS_CONSTRAINTS_EXIST(I_foreign_constraints  IN      INV_STATUS_TYPES.INV_STATUS%TYPE,
                                      O_error_message        IN OUT  VARCHAR2,
                                      O_found                IN OUT  BOOLEAN)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--- Function:  CHECK_INV_REASON_EXIST
--- Purpose:   Check the Reason for duplicate entries.
-------------------------------------------------------------------------------
FUNCTION CHECK_INV_REASON_EXIST(I_reason         IN      INV_ADJ_REASON.REASON%TYPE,
                                O_error_message  IN OUT  VARCHAR2,
                                O_found          IN OUT  BOOLEAN)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--- Function:  INV_REASON_CONSTRAINTS_EXIST
--- Purpose:   Check the REASON for foreign constraint violations.
-------------------------------------------------------------------------------
FUNCTION INV_REASON_CONSTRAINTS_EXIST(I_foreign_constraints  IN      INV_ADJ_REASON.REASON%TYPE,
                                      O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_found                IN OUT  BOOLEAN)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END;
/
