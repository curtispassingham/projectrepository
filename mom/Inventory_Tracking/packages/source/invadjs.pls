CREATE OR REPLACE PACKAGE INVADJ_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------
--------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Function Name: ADJ_STOCK_ON_HAND
-- Purpose      : Updates the appropriate item/location tables where updates
--                will occur.  If the item is is a display pack, then for
--                each SKU that belongs in the pack, the component stock
--                on hand field on hte win_wh will also be updated.
---------------------------------------------------------------------------------------------
FUNCTION ADJ_STOCK_ON_HAND(I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                           I_location        IN       INV_ADJ.LOCATION%TYPE,
                           I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                           O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_found           IN OUT   BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: BUILD_ADJ_STOCK_ON_HAND
-- Purpose      : Bulk version of ADJ_STOCK_ON_HAND
--                INIT_SOH_LOCK and INIT_SOH_UPDATE should be called
--                before the first call to BUILD_ADJ_STOCK_ON_HAND
--                FLUSH_SOH_LOCK and FLUSH_SOH_UPDATE should be called
--                after the last call to BUILD_ADJ_STOCK_ON_HAND
---------------------------------------------------------------------------------------------
FUNCTION BUILD_ADJ_STOCK_ON_HAND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_found           IN OUT   BOOLEAN,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                                 I_location        IN       INV_ADJ.LOCATION%TYPE,
                                 I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                                 I_adj_weight      IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                                 I_adj_weight_uom  IN       UOM_CLASS.UOM%TYPE,
                                 I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ADJ_UNAVAILABLE
-- Purpose      : This function will update or insert into inv_status_qty
---------------------------------------------------------------------------------------------
FUNCTION ADJ_UNAVAILABLE(I_item            IN       ITEM_MASTER.ITEM%TYPE,
                         I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE,
                         I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                         I_location        IN       INV_ADJ.LOCATION%TYPE,
                         I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_found           IN OUT   BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: BUILD_ADJ_UNAVAILABLE
-- Purpose      : Bulk version of ADJ_UNAVAILABLE
--                INIT_SOH_LOCK, INIT_SOH_UPDATE and INIT_INV_STAT_QTY
--                should be called before the first call to BUILD_ADJ_UNAVAILABLE.
--                FLUSH_SOH_LOCK, FLUSH_SOH_UPDATE, FLUSH_INV_STAT_QTY_INSUPD,
--                and FLUSH_INV_STAT_QTY_DEL should be called
--                after the last call to BUILD_ADJ_UNAVAILABLE
---------------------------------------------------------------------------------------------
FUNCTION BUILD_ADJ_UNAVAILABLE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_found           IN OUT   BOOLEAN,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE,
                               I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                               I_location        IN       INV_ADJ.LOCATION%TYPE,
                               I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ADJ_TRAN_DATA
-- Purpose      : This function will update or insert into TRAN_DATA.
--                I_wac, I_unit_retail are for item transformation only, populated with the
--                corresponding value of from-item; otherwise, populate NULL.
---------------------------------------------------------------------------------------------
FUNCTION ADJ_TRAN_DATA(I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                       I_location        IN       INV_ADJ.LOCATION%TYPE,
                       I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                       I_program         IN       TRAN_DATA.PGM_NAME%TYPE,
                       I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE,
                       I_tran_code       IN       TRAN_DATA.TRAN_CODE%TYPE,
                       I_reason          IN       INV_ADJ.REASON%TYPE,
                       I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE,
                       I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                       I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                       O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_found           IN OUT   BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ADJ_TRAN_DATA
-- Purpose      : This overloaded function will update or insert into TRAN_DATA,
--                including order number.
---------------------------------------------------------------------------------------------l retail and order number.
FUNCTION ADJ_TRAN_DATA(I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                       I_location        IN       INV_ADJ.LOCATION%TYPE,
                       I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                       I_total_cost      IN       ORDLOC.UNIT_COST%TYPE,
                       I_total_retail    IN       ORDLOC.UNIT_RETAIL%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_program         IN       TRAN_DATA.PGM_NAME%TYPE,
                       I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE,
                       I_tran_code       IN       TRAN_DATA.TRAN_CODE%TYPE,
                       I_reason          IN       INV_ADJ.REASON%TYPE,
                       I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE,
                       I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                       I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                       O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_found           IN OUT   BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: BUILD_ADJ_TRAN_DATA
-- Purpose      : Bulk version of ADJ_TRAN_DATA.
--                STKLEDGR_SQL.INIT_TRAN_DATA_INSERT should
--                be called before the first call to BUILD_ADJ_TRAN_DATA.
--                STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT should
--                be called after the last call to BUILD_ADJ_TRAN_DATA.
--                I_wac, I_unit_retail are for item transformation only, populated with the
--                corresponding value of from-item; otherwise, populate NULL.
---------------------------------------------------------------------------------------------
FUNCTION BUILD_ADJ_TRAN_DATA(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_found            IN OUT   BOOLEAN,
                             I_item             IN       ITEM_MASTER.ITEM%TYPE,
                             I_loc_type         IN       INV_ADJ.LOC_TYPE%TYPE,
                             I_location         IN       INV_ADJ.LOCATION%TYPE,
                             I_adj_qty          IN       INV_ADJ.ADJ_QTY%TYPE,
                             I_adj_weight       IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE DEFAULT NULL,
                             I_adj_weight_uom   IN       UOM_CLASS.UOM%TYPE DEFAULT NULL,
                             I_ref_no_1         IN       TRAN_DATA.REF_NO_1%TYPE   DEFAULT NULL,
                             I_program          IN       TRAN_DATA.PGM_NAME%TYPE,
                             I_adj_date         IN       INV_ADJ.ADJ_DATE%TYPE,
                             I_tran_code        IN       TRAN_DATA.TRAN_CODE%TYPE,
                             I_reason           IN       INV_ADJ.REASON%TYPE,
                             I_inv_status       IN       INV_ADJ.INV_STATUS%TYPE,
                             I_wac              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                             I_unit_retail      IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                             I_pack_ind         IN       ITEM_MASTER.PACK_IND%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_UNAVAILABLE
-- Purpose      : Retrieves the quantity of unavailable stock.
---------------------------------------------------------------------------------------------
FUNCTION GET_UNAVAILABLE(I_item            IN       ITEM_MASTER.ITEM%TYPE,
                         I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                         I_location        IN       INV_ADJ.LOCATION%TYPE,
                         O_unavl_qty       IN OUT   INV_STATUS_QTY.QTY%TYPE,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_found           IN OUT   BOOLEAN)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Name:    GET_AVAIL
-- Purpose: This function will return the available inventory (total
--          stock on hand - total unavailable stock) for a SKU at a
--          location (store or warehouse).
--------------------------------------------------------------------
FUNCTION GET_AVAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_avail_qty       IN OUT   INV_STATUS_QTY.QTY%TYPE,
                   I_item            IN       INV_STATUS_QTY.ITEM%TYPE,
                   I_location        IN       INV_STATUS_QTY.LOCATION%TYPE,
                   I_loc_type        IN       INV_STATUS_QTY.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Name:    INSERT_INV_ADJ
-- Purpose: This function will insert a new record into the inv_adj
--          audit table.
-------------------------------------------------------------------
FUNCTION INSERT_INV_ADJ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       ITEM_MASTER.ITEM%TYPE,
                        I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE,
                        I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                        I_location        IN       INV_ADJ.LOCATION%TYPE,
                        I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                        I_reason          IN       INV_ADJ.REASON%TYPE,
                        I_user_id         IN       INV_ADJ.USER_ID%TYPE,
                        I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------
-- Name:    ADJ_STOCK
-- Purpose: This function will perform all necessary logic to update
--          the available stock on hand at a location by performing
--          an inventory adjustment.
--          I_wac, I_unit_retail are for item transformation only, populated with the
--          corresponding value of from-item; otherwise, populate NULL.
-------------------------------------------------------------------
FUNCTION ADJ_STOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item            IN       ITEM_MASTER.ITEM%TYPE,
                   I_location        IN       INV_ADJ.LOCATION%TYPE,
                   I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                   I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                   I_reason          IN       INV_ADJ.REASON%TYPE,
                   I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                   I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                   I_user_id         IN       INV_ADJ.USER_ID%TYPE,
                   I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------
-- Name:    STOCK_OUT
-- Purpose: This function will perform all logic necessary for a
--          stock-out transaction.
--          I_wac, I_unit_retail are for item transformation only, populated with the
--          corresponding value of from-item; otherwise, populate NULL.
-------------------------------------------------------------------
FUNCTION STOCK_OUT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item            IN       ITEM_MASTER.ITEM%TYPE,
                   I_location        IN       INV_ADJ.LOCATION%TYPE,
                   I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                   I_reason          IN       INV_ADJ.REASON%TYPE,
                   I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                   I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                   I_user_id         IN       INV_ADJ.USER_ID%TYPE,
                   I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
-- Name:    CHANGE_STATUS
-- Purpose: This function will move stock between the available and
--          unavailable inventory buckets.
--          I_wac, I_unit_retail are for item transformation only, populated with the
--          corresponding value of from-item; otherwise, populate NULL.
------------------------------------------------------------------
FUNCTION CHANGE_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE,
                       I_location        IN       INV_ADJ.LOCATION%TYPE,
                       I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                       I_move_to         IN       VARCHAR2,
                       I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                       I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                       I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                       I_user_id         IN       INV_ADJ.USER_ID%TYPE,
                       I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
-- Name:    REMOVE_STOCK
-- Purpose: This function will perform logic necessary to remove stock
--          from either available or unavailable inventory.
--          I_wac, I_unit_retail are for item transformation only, populated with the
--          corresponding value of from-item; otherwise, populate NULL.
------------------------------------------------------------------
FUNCTION REMOVE_STOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item            IN       ITEM_MASTER.ITEM%TYPE,
                      I_location        IN       INV_ADJ.LOCATION%TYPE,
                      I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                      I_remove_from     IN       VARCHAR2,
                      I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE,
                      I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                      I_reason          IN       INV_ADJ.REASON%TYPE,
                      I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                      I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                      I_user_id         IN       INV_ADJ.USER_ID%TYPE,
                      I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: BUILD_PROCESS_INVADJ
-- Purpose      : Processes inventory adjustment messages.
--                INIT_ALL should be called
--                before the first call to BUILD_PROCESS_INVADJ
--                FLUSH_ALL should be called
--                after the last call to BUILD_PROCESS_INVADJ
--
-- The following variables are not currently used and can be removed
-- at a later date:
--    transshipment_nbr,
--    from_trouble_code,
--    to_trouble_code,
--    from_wip_code,
--    to_wip_code,
--    transaction_code,
--    aux_reason_code
--                I_wac, I_unit_retail are for item transformation only, populated with the
--                corresponding value of from-item; otherwise, populate NULL.
---------------------------------------------------------------------------------------------
FUNCTION BUILD_PROCESS_INVADJ(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_location            IN       INV_ADJ.LOCATION%TYPE,
                              I_item                IN       INV_ADJ.ITEM%TYPE,
                              I_reason              IN       INV_ADJ.REASON%TYPE,
                              I_adj_qty             IN       INV_ADJ.ADJ_QTY%TYPE,
                              I_adj_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_adj_weight_uom      IN       UOM_CLASS.UOM%TYPE,
                              I_from_disposition    IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                              I_to_disposition      IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                              I_user_id             IN       INV_ADJ.USER_ID%TYPE,
                              I_adj_date            IN       INV_ADJ.ADJ_DATE%TYPE,
                              I_doc_no              IN       NUMBER,
                              I_doc_type            IN       VARCHAR2,
                              I_wac                 IN       ITEM_LOC_SOH.AV_COST%TYPE,
                              I_unit_retail         IN       ITEM_LOC.UNIT_RETAIL%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:      GET_INV_STATUS
-- Purpose:   This function returns inv_status for inv_status_code from the
--            inv_status_codes table.  The database table is cached since it is
--            a small table that could potentially be queried many times.
--------------------------------------------------------------------------------
FUNCTION GET_INV_STATUS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_inv_status        IN OUT   INV_STATUS_CODES.INV_STATUS%TYPE,
                        I_inv_status_code   IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:      GET_REASON_INFO
-- Purpose:   This function retrieves the reason_desc and cogs_ind from the
--            inv_adj_reason table.
--------------------------------------------------------------------------------
FUNCTION GET_REASON_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_reason_desc     IN OUT   INV_ADJ_REASON_TL.REASON_DESC%TYPE,
                         O_cogs_ind        IN OUT   INV_ADJ_REASON.COGS_IND%TYPE,
                         I_reason          IN       INV_ADJ_REASON.REASON%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:      GET_UNAVL_INV_QTY
-- Purpose:   Called from unavlinv.fmb to retrieve the total unavailable
--            inventory for the selected criteria.
--------------------------------------------------------------------------------
FUNCTION GET_UNAVL_INV_QTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_total_unavail IN OUT INV_STATUS_QTY.QTY%TYPE,
                           I_item          IN     INV_STATUS_QTY.ITEM%TYPE,
                           I_inv_status    IN     INV_STATUS_QTY.INV_STATUS%TYPE,
                           I_loc_type      IN     INV_STATUS_QTY.LOC_TYPE%TYPE,
                           I_location      IN     INV_STATUS_QTY.LOCATION%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:      UNAVL_INV_EXIST
-- Purpose:   Called from unavlinv.fmb to validate if unavailable inventory 
--            exists for the selected criteria.
--------------------------------------------------------------------------------
FUNCTION UNAVL_INV_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exist         IN OUT BOOLEAN,
                         I_item          IN     INV_STATUS_QTY.ITEM%TYPE,
                         I_inv_status    IN     INV_STATUS_QTY.INV_STATUS%TYPE,
                         I_loc_type      IN     INV_STATUS_QTY.LOC_TYPE%TYPE,
                         I_location      IN     INV_STATUS_QTY.LOCATION%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- The following functions are used to bulk DML statements together
--------------------------------------------------------------------------------
FUNCTION INIT_INV_ADJ_INSERT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION FLUSH_INV_ADJ_INSERT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION INIT_SOH_UPDATE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION FLUSH_SOH_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION INIT_INV_STAT_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION FLUSH_INV_STAT_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION INIT_ALL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION FLUSH_ALL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: MERGE_INV_ADJ_REASON_TL
-- Purpose      : Inserts Inventory Adjustment Reason description record into INV_ADJ_REASON_TL table.
-------------------------------------------------------------------------------
FUNCTION MERGE_INV_ADJ_REASON_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_reason          IN       INV_ADJ_REASON.REASON%TYPE,
                                 I_reason_desc     IN       INV_ADJ_REASON_TL.REASON_DESC%TYPE,
                                 I_lang            IN       LANG.LANG%TYPE) 
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: DEL_INV_ADJ_REASON_TL
-- Purpose      : Deletes Inventory Adjustment Reason description record from INV_ADJ_REASON_TL table.
-------------------------------------------------------------------------------
FUNCTION DEL_INV_ADJ_REASON_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_reason          IN       INV_ADJ_REASON.REASON%TYPE) 
RETURN BOOLEAN;
-------------------------------------------------------------------------------

-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=TRUE $then
   ---------------------------------------------------------------------------------------------
   -- Function Name: UPD_NON_SELLABLE_QTY
   -- Purpose      : Updates the non-sellable quantity on ITEM_LOC_SOH for the specified
   --                item/loc.  This is called when the inventory is unavailable.
   ---------------------------------------------------------------------------------------------
   FUNCTION UPD_NON_SELLABLE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                                 I_location        IN       INV_ADJ.LOCATION%TYPE,
                                 I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: PROCESS_AVAILABLE
   -- Purpose      : This is called when the inv_status specifies that the inventory is
   --                available.  The appropriate tables are updated to reflect the available
   --                inventory.
   ---------------------------------------------------------------------------------------------
   FUNCTION PROCESS_AVAILABLE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_inv_adj         IN       INV_ADJ%ROWTYPE,
                              I_adj_weight      IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_adj_weight_uom  IN       UOM_CLASS.UOM%TYPE,
                              I_vdate           IN       DATE,
                              I_pgm_name        IN       TRAN_DATA.PGM_NAME%TYPE,
                              I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                              I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                              I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: PROCESS_UNAVAILABLE
   -- Purpose      : This is called when the inv_status specifies that the inventory is
   --                unavailable.  The appropriate tables are updated to reflect the unavailable
   --                inventory.
   ---------------------------------------------------------------------------------------------
   FUNCTION PROCESS_UNAVAILABLE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_inv_adj         IN       INV_ADJ%ROWTYPE,
                                I_adj_weight      IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                                I_adj_weight_uom  IN       UOM_CLASS.UOM%TYPE,
                                I_pgm_name        IN       TRAN_DATA.PGM_NAME%TYPE,
                                I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: CREATE_ITEM_LOC_REL
   -- Purpose      : Creates the item/location relationship by calling NEW_ITEM_LOC.
   ---------------------------------------------------------------------------------------------
   FUNCTION CREATE_ITEM_LOC_REL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item            IN       INV_ADJ.ITEM%TYPE,
                                I_location        IN       INV_ADJ.LOCATION%TYPE,
                                I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                                I_item_level      IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                                I_tran_level      IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                                I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                                I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: VALIDATE_INVADJ
   -- Purpose      : Validates the item, location, inv_status, and reason.
   ---------------------------------------------------------------------------------------------
   FUNCTION VALIDATE_INVADJ(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_pack_ind         IN OUT   ITEM_MASTER.PACK_IND%TYPE,
                            O_simple_pack_ind  IN OUT   ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                            O_catch_weight_ind IN OUT   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                            IO_inv_adj         IN OUT   INV_ADJ%ROWTYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: ITEM_LOC_EXIST
   -- Purpose      : Checks to see if the item/location exists on item_loc_soh.  Also
   --                gets the current qty on inv_status_qty by calling GET_INV_STATUS_QTY.
   ---------------------------------------------------------------------------------------------
   FUNCTION ITEM_LOC_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_stock_on_hand   IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            O_found           IN OUT   BOOLEAN,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE,
                            I_location        IN       ITEM_LOC_SOH.LOC%TYPE,
                            I_loc_type        IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                            I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: STOCKHOLDING_INVADJ
   -- Purpose      : Adjusts the inventory for the item and location specified.
   ---------------------------------------------------------------------------------------------
   FUNCTION STOCKHOLDING_INVADJ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_location        IN       INV_ADJ.LOCATION%TYPE,
                                I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                                I_item            IN       INV_ADJ.ITEM%TYPE,
                                I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE,
                                I_reason          IN       INV_ADJ.REASON%TYPE,
                                I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                                I_adj_weight      IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                                I_adj_weight_uom  IN       UOM_CLASS.UOM%TYPE,
                                I_adj_date        IN       INV_ADJ.ADJ_DATE%TYPE,
                                I_wac             IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                I_unit_retail     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                I_user_id         IN       INV_ADJ.USER_ID%TYPE,
                                I_vdate           IN       DATE,
                                I_pgm_name        IN       TRAN_DATA.PGM_NAME%TYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: GET_INV_STATUS_QTY
   -- Purpose      : Gets the qty from the table INV_STATUS_QTY.  Because bulk binding is being
   --                used for all of the inserts/updates/deletes to the inv_status_qty table,
   --                we need to check the PL/SQL table used for bulk binding as well as
   --                the database table.
   ---------------------------------------------------------------------------------------------
   FUNCTION GET_INV_STATUS_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_qty             IN OUT   INV_STATUS_QTY.QTY%TYPE,
                                O_tbl_index       IN OUT   BINARY_INTEGER,
                                I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                I_location        IN       ITEM_LOC_SOH.LOC%TYPE,
                                I_loc_type        IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                                I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: ADD_ILSOH_UPDATE_REC
   -- Purpose      : Adds a record to the PL/SQL table used for bulk updates to item_loc_soh.
   ---------------------------------------------------------------------------------------------
   FUNCTION ADD_ILSOH_UPDATE_REC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item             IN       ITEM_MASTER.ITEM%TYPE,
                                 I_location         IN       ITEM_LOC_SOH.LOC%TYPE,
                                 I_soh_adj_qty      IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_pcsoh_adj_qty    IN       ITEM_LOC_SOH.PACK_COMP_SOH%TYPE,
                                 I_non_sell_qty     IN       ITEM_LOC_SOH.NON_SELLABLE_QTY%TYPE,
                                 I_average_weight   IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE)
      RETURN BOOLEAN;
   ---------------------------------------------------------------------------------------------
   -- Function Name: GET_INV_STATUS_TYPES
   -- Purpose      : Checks for the existence of the input inv_status
   --                in the table inv_status_types.  The database table is cached in the global
   --                variable LP_inv_status_types, since it is a small table that could
   --                potentially be queried many times.
   ---------------------------------------------------------------------------------------------
   FUNCTION GET_INV_STATUS_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
      RETURN BOOLEAN;
$end
---------------------------------------------------------------------------------------------
-- Function Name: GET_INV_STATUS_TYPES_DESC
-- Purpose      : Gets the description for the input inv_status from the view
--                v_inv_status_types_tl.
---------------------------------------------------------------------------------------------
FUNCTION GET_INV_STATUS_TYPES_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_description     IN OUT   INV_STATUS_TYPES_TL.INV_STATUS_DESC%TYPE,
                                   I_inv_status      IN       INV_STATUS_TYPES.INV_STATUS%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: MERGE_INV_STATUS_TYPES_TL
-- Purpose      : Inserts or updates the inv_status_types_tl table given the input inv_status,
--                inv_status_desc, and language.
---------------------------------------------------------------------------------------------
FUNCTION MERGE_INV_STATUS_TYPES_TL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_inv_status        IN       INV_STATUS_TYPES.INV_STATUS%TYPE,
                                   I_inv_status_desc   IN       INV_STATUS_TYPES_TL.INV_STATUS_DESC%TYPE,
                                   I_lang              IN       LANG.LANG%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DEL_INV_STATUS_TYPES_TL
-- Purpose      : Deletes the inv_status from the inv_status_types_tl table.
---------------------------------------------------------------------------------------------
FUNCTION DEL_INV_STATUS_TYPES_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_inv_status      IN       INV_STATUS_TYPES.INV_STATUS%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item              IN OUT   INV_ADJ.ITEM%TYPE,
                    I_location          IN OUT   INV_ADJ.LOCATION%TYPE,
                    I_function_key      IN OUT   VARCHAR2,
                    I_seq_no            IN OUT   NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END;
/
