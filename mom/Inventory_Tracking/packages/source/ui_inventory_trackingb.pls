CREATE OR REPLACE PACKAGE Body INVENTORY_TRACKING_SQL IS
  ---------------------------------------------------------------------------------------------
  -- Purpose                   : packbrk (Form Module) 
  -- Source Object            : PROCEDURE  -> P_SET_VPN
  -- NEW ARGUMENTS (2)
  -- ITEM          :B_DETAIL.TI_PACK_COMPONENT............. -> I_TI_PACK_COMPONENT
  -- ITEM          :B_DETAIL.TI_VPN........................ -> I_TI_VPN
  ---------------------------------------------------------------------------------------------
FUNCTION SET_VPN(O_error_message       IN OUT   VARCHAR2,
                 I_TI_PACK_COMPONENT   IN       VARCHAR2,
                 I_TI_VPN              IN OUT   VARCHAR2)
   return BOOLEAN
is
  L_vpn             ITEM_SUPPLIER.VPN%TYPE;
  L_sku_tl          CODE_DETAIL.CODE_DESC%TYPE;
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_val1            code_detail.code_desc%TYPE;
  L_program         VARCHAR2(64) := 'INVENTORY_TRACKING_SQL.SET_VPN';
  cursor C_GET_VPN is
    select vpn
      from item_supplier
     where item = I_TI_PACK_COMPONENT
       and vpn is NOT NULL;
BEGIN
  if NOT LANGUAGE_SQL.GET_CODE_DESC(L_error_message, 
                                    'LABL', 
									'SKU', 
									L_sku_tl) then
    O_error_message := SQL_LIB.CREATE_MSG(L_error_message, 
	                                      NULL, 
										  NULL, 
										  NULL);
    return FALSE;
  end if;
  ---
  open C_GET_VPN;
  fetch C_GET_VPN into L_vpn;
  if C_GET_VPN%NOTFOUND then
    close C_GET_VPN;
    I_TI_VPN := L_sku_tl || ': ' || I_TI_PACK_COMPONENT;
  else
    I_TI_VPN := L_vpn;
    ---
    fetch C_GET_VPN into L_vpn;
    if C_GET_VPN%FOUND then
      I_TI_VPN := L_sku_tl || ': ' || I_TI_PACK_COMPONENT;
    end if;
    close C_GET_VPN;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
	                                        SQLERRM, 
											L_program, 
											to_char(SQLCODE));
   return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Purpose                   : stkvarnc (Form Module) 
-- Source Object            : PROCEDURE  -> P_STAKE_CONT_INSERT
-- NEW ARGUMENTS (5)
-- ITEM          :B_CYCLE_COUNT.CYCLE_COUNT.............. -> I_CYCLE_COUNT
-- ITEM          :B_LOCATION_LIST.RUN_TYPE............... -> I_RUN_TYPE
-- ITEM          :B_LOCATION_LIST.TI_LOCATION............ -> I_TI_LOCATION
-- ITEM          :B_LOCATION_LIST.TI_LOC_TYPE............ -> I_TI_LOC_TYPE
-- Pack.Spec     INTERNAL_VARIABLES.GP_USER_ID........... -> P_IntrnlVrblsGpUsrId
---------------------------------------------------------------------------------------------
FUNCTION STAKE_CONT_INSERT(O_error_message        IN OUT   VARCHAR2,
						   I_CYCLE_COUNT          IN       NUMBER,
						   I_RUN_TYPE             IN       VARCHAR2,
						   I_TI_LOCATION          IN       NUMBER,
						   I_TI_LOC_TYPE          IN       VARCHAR2,
						   P_IntrnlVrblsGpUsrId   IN       STAKE_CONT.USER_ID%TYPE )
   return BOOLEAN
is
  L_loc_type      STAKE_CONT.LOC_TYPE%TYPE := I_TI_LOC_TYPE;
  L_location      STAKE_CONT.LOCATION%TYPE := I_TI_LOCATION;
  L_cycle_count   STAKE_CONT.CYCLE_COUNT%TYPE := I_CYCLE_COUNT;
  L_run_type      STAKE_CONT.RUN_TYPE%TYPE := I_RUN_TYPE;
  L_program       VARCHAR2(64) := 'INVENTORY_TRACKING_SQL.STAKE_CONT_INSERT';
BEGIN
  if L_loc_type = 'I' then
    L_loc_type := 'W';
  end if;
  ---
  insert into stake_cont(cycle_count,
                         item,
                         loc_type,
                         location,
                         run_type,
                         user_id)
     select /*+ index (stake_sku_loc pk_stake_sku_loc) */ 
            L_cycle_count,
		    item,
		    L_loc_type,
		    L_location,
		    L_run_type,
		    P_IntrnlVrblsGpUsrId
       from stake_sku_loc
      where cycle_count = L_cycle_count
        and loc_type = L_loc_type
        and location = L_location
        and processed = 'N'
        and NOT EXISTS
          (select 'x'
             from stake_cont
            where cycle_count = L_cycle_count
              and loc_type = L_loc_type
              and location = L_location
              and user_id = P_IntrnlVrblsGpUsrId
              and run_type = L_run_type
              and item = stake_sku_loc.item);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
	                                        SQLERRM, 
											L_program, 
											to_char(SQLCODE));
   return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Purpose                   : stkvwedt (Form Module) 
-- Source Object            : PROCEDURE  -> P_GET_COSTS
-- NEW ARGUMENTS (13)
-- ITEM          :B_STAKE_DATE.CYCLE_COUNT............... -> I_CYCLE_COUNT
-- ITEM          :B_STAKE_DATE.DI_CURRENCY_CODE1......... -> I_DI_CURRENCY_CODE1
-- ITEM          :B_STAKE_DATE.DI_CURRENCY_CODE2......... -> I_DI_CURRENCY_CODE2
-- ITEM          :B_STAKE_DATE.LOCATION.................. -> I_LOCATION
-- ITEM          :B_STAKE_DATE.LOC_TYPE.................. -> I_LOC_TYPE
-- ITEM          :B_STAKE_DATE.TI_ITEM................... -> I_TI_ITEM
-- ITEM          :B_STAKE_DATE.TI_UNIT_COST_PRIM......... -> I_TI_UNIT_COST_PRIM
-- ITEM          :B_STAKE_DATE.TI_UNIT_RETAIL_PRIM....... -> I_TI_UNIT_RETAIL_PRIM
-- ITEM          :B_STAKE_DATE.UNIT_COST................. -> I_UNIT_COST
-- ITEM          :B_STAKE_DATE.UNIT_RETAIL............... -> I_UNIT_RETAIL
-- PARAMETER     :PARAMETER.PM_CURR_LOC.................. -> P_PM_CURR_LOC
-- PARAMETER     :PARAMETER.PM_CURR_PRIM................. -> P_PM_CURR_PRIM
-- PARAMETER     :PARAMETER.PM_LOC_TYPE.................. -> P_PM_LOC_TYPE
---------------------------------------------------------------------------------------------
FUNCTION GET_COSTS(O_error_message         IN OUT   VARCHAR2,
				   I_CYCLE_COUNT           IN       NUMBER,
				   I_DI_CURRENCY_CODE1     IN OUT   VARCHAR2,
				   I_DI_CURRENCY_CODE2     IN OUT   VARCHAR2,
				   I_LOCATION              IN       NUMBER,
				   I_LOC_TYPE              IN       VARCHAR2,
				   I_TI_ITEM               IN       VARCHAR2,
				   I_TI_UNIT_COST_PRIM     IN OUT   NUMBER,
				   I_TI_UNIT_RETAIL_PRIM   IN OUT   NUMBER,
				   I_UNIT_COST             IN OUT   NUMBER,
				   I_UNIT_RETAIL           IN OUT   NUMBER,
				   P_PM_CURR_LOC           IN       VARCHAR2,
				   P_PM_CURR_PRIM          IN       VARCHAR2,
				   P_PM_LOC_TYPE           IN       VARCHAR2)
   return BOOLEAN
is
  L_unit_retail     stake_sku_loc.snapshot_unit_retail%TYPE := NULL;
  L_unit_cost       stake_sku_loc.snapshot_unit_cost%TYPE := NULL;
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
  L_program         VARCHAR2(64) := 'INVENTORY_TRACKING_SQL.GET_COSTS';
  cursor C_GET_COST_LOC is
    select snapshot_unit_cost,
           snapshot_unit_retail
      from stake_sku_loc
     where item = I_TI_ITEM
       and loc_type = P_PM_LOC_TYPE
       and location = I_LOCATION
       and cycle_count = I_CYCLE_COUNT;
  cursor C_GET_COST_WH_MC is
    select DISTINCT snapshot_unit_cost,
                    snapshot_unit_retail
      from stake_sku_loc sl,
           wh w
     where item = I_TI_ITEM
       and sl.loc_type = 'W'
       and sl.location  = w.wh
       and w.physical_wh = I_LOCATION
       and cycle_count = I_CYCLE_COUNT;
BEGIN
  if I_LOC_TYPE = 'W' then
    open C_GET_COST_WH_MC;
    fetch C_GET_COST_WH_MC into l_unit_cost, l_unit_retail;
    if C_GET_COST_WH_MC%FOUND then
       I_UNIT_COST   := l_unit_cost;
       I_UNIT_RETAIL := l_unit_retail;
    end if;
    close C_GET_COST_WH_MC;
  else
    open C_GET_COST_LOC;
    fetch C_GET_COST_LOC into l_unit_cost, l_unit_retail;
    if C_GET_COST_LOC%FOUND then
       I_UNIT_COST   := l_unit_cost;
       I_UNIT_RETAIL := l_unit_retail;
    end if;
    close C_GET_COST_LOC;
  end if;
  ---
  if I_UNIT_COST is NOT NULL then
    --convert the local values to primary
    if CURRENCY_SQL.CONVERT(L_error_message, 
	                        I_UNIT_COST, 
							P_PM_CURR_LOC, 
							P_PM_CURR_PRIM, 
							I_TI_UNIT_COST_PRIM, 
							'C', 
							NULL, 
							NULL) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message, 
	                                         NULL, 
											 NULL, 
											 NULL);
      return FALSE;
    end if;
  end if;
  if I_UNIT_RETAIL is NOT NULL then
    if CURRENCY_SQL.CONVERT(L_error_message, 
	                        I_UNIT_RETAIL, 
							P_PM_CURR_LOC, 
							P_PM_CURR_PRIM, 
							I_TI_UNIT_RETAIL_PRIM, 
							'R', 
							NULL, 
							NULL) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message, 
	                                         NULL, 
										     NULL, 
										     NULL);
      return FALSE;
    end if;
  end if;
  --populate the currency code fields
  I_DI_CURRENCY_CODE1 := '('||P_PM_CURR_LOC||')';
  I_DI_CURRENCY_CODE2 := '('||P_PM_CURR_LOC||')';
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
	                                        SQLERRM, 
											L_program, 
											to_char(SQLCODE));
   return FALSE;
end;
end INVENTORY_TRACKING_SQL;
/
