CREATE OR REPLACE PACKAGE WF_ORDER_UPLOAD_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------
-- Function: VALIDATE_STG_TABLES
-- Description: Validates information in the staging tables for franchisee order upload.
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STG_TABLES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id    IN     SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id      IN     SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END WF_ORDER_UPLOAD_SQL;
/