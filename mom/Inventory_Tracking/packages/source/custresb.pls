CREATE OR REPLACE PACKAGE BODY CUSTOMER_RESERVE_SQL AS
------------------------------------------------------
LP_user   VARCHAR2(30) := get_user;
------------------------------------------------------------------------------------------------
---                                       PUBLIC FUNCTIONS:                                  ---
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CO_RESERVE(O_error_message         IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cust_ord_reserve_tbl  IN        CUST_ORD_RESERVE_TBL) 
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) := 'CUSTOMER_RESERVE_SQL.PROCESS_CO_RESERVE';
   L_table                   VARCHAR2(50);
   
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(RECORD_LOCKED, -54);

   L_item                    ITEM_MASTER.ITEM%TYPE;
   L_loc                     STORE.STORE%TYPE;
   L_invalid_loc_ind         VARCHAR2(1) := 'N';
   L_invalid_item_ind        VARCHAR2(1) := 'N';
   L_invalid_item_loc_ind    VARCHAR2(1) := 'N';
   L_cust_ord_reserve_tbl    CUST_ORD_RESERVE_TBL;
   L_process_status          INV_RESV_UPDATE_TEMP.PROCESS_STATUS%TYPE := 'N';
   L_cust_resv_loc_type      INV_RESV_UPDATE_TEMP.LOC_TYPE%TYPE := 'S'; --Customer reservation is supported for physical stores only

   cursor C_GET_INVALID_RESV_RECORDS is
      select reserve_tbl.item,
             reserve_tbl.loc,
             case when st.store is NULL 
                       or st.stockholding_ind = 'N' 
                  then 'Y' else 'N' 
             end invalid_loc_ind,
             case when im.item is NULL 
                       or im.status != 'A' 
                       or im.pack_ind = 'Y'
                  then 'Y' else 'N' 
             end invalid_item_ind,
             case when il.item is NULL
                       or il.status = 'D'
                  then 'Y' else 'N'
             end invalid_item_loc_ind
        from store st,
             item_master im,
             item_loc il,
             TABLE(CAST (I_cust_ord_reserve_tbl AS CUST_ORD_RESERVE_TBL)) reserve_tbl
       where reserve_tbl.item = im.item(+)
         and reserve_tbl.loc = st.store(+)
         and reserve_tbl.item = il.item(+)
         and reserve_tbl.loc = il.loc(+)
         and (im.item is NULL
              or im.status != 'A'
              or im.pack_ind = 'Y'
              or st.store is NULL
              or st.stockholding_ind = 'N' 
              or il.item is NULL
              or il.status = 'D')
         and rownum = 1;
           
   cursor C_LOCK_ITEM_LOC_SOH is
      select CUST_ORD_RESERVE_REC(reserve_tbl.item,
                                  reserve_tbl.loc,
                                  NVL(reserve_tbl.customer_resv_qty, 0))
        from item_loc_soh ils,
             TABLE(CAST (I_cust_ord_reserve_tbl AS CUST_ORD_RESERVE_TBL)) reserve_tbl,
             item_master im
       where ils.item = reserve_tbl.item
         and ils.loc  = reserve_tbl.loc
         and ils.item = im.item
         and im.inventory_ind = 'Y'               --Ignoring the non-inventory items - handling deposit container.
         for update of ils.customer_resv nowait;

BEGIN

   if I_cust_ord_reserve_tbl is NULL or I_cust_ord_reserve_tbl.COUNT <= 0 then
      return TRUE;
   end if;
   
   if I_cust_ord_reserve_tbl is NOT NULL and I_cust_ord_reserve_tbl.COUNT > 0 then
      
      L_table := 'ITEM_MASTER , ITEM_LOC, STORE';
      
      SQL_LIB.SET_MARK('OPEN', 'C_GET_INVALID_RESV_RECORDS', L_table, NULL);
      open C_GET_INVALID_RESV_RECORDS;
      
      SQL_LIB.SET_MARK('FETCH', 'C_GET_INVALID_RESV_RECORDS', L_table, NULL);
      fetch C_GET_INVALID_RESV_RECORDS INTO L_item, L_loc, L_invalid_loc_ind, L_invalid_item_ind, L_invalid_item_loc_ind;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_INVALID_RESV_RECORDS', L_table, NULL);
      close C_GET_INVALID_RESV_RECORDS;
      
      if L_invalid_item_ind = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_IN_RESV_REQUEST', 
                                                L_item,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      
      if L_invalid_loc_ind = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_IN_RESV_REQUEST',
                                                L_loc,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      
      if L_invalid_item_loc_ind = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM_LOC',
                                                L_item,
                                                L_loc,
                                                NULL);
         return FALSE;
      end if;
      
      L_table := 'ITEM_LOC_SOH';
      
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_LOC_SOH', L_table, NULL);
      open C_LOCK_ITEM_LOC_SOH;
      
      SQL_LIB.SET_MARK('FETCH', 'C_LOCK_ITEM_LOC_SOH', L_table, NULL);
      fetch C_LOCK_ITEM_LOC_SOH BULK COLLECT INTO L_cust_ord_reserve_tbl;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_LOC_SOH', L_table, NULL);
      close C_LOCK_ITEM_LOC_SOH;
   end if;
   ---
   if L_cust_ord_reserve_tbl is NOT NULL and L_cust_ord_reserve_tbl.COUNT > 0 then
      SQL_LIB.SET_MARK('UPDATE', NULL, L_table, NULL);
   
      FORALL i in L_cust_ord_reserve_tbl.FIRST..L_cust_ord_reserve_tbl.LAST
         update item_loc_soh
            set customer_resv = customer_resv + L_cust_ord_reserve_tbl(i).customer_resv_qty,
                last_update_datetime = SYSDATE,
                last_update_id = LP_user
          where item = L_cust_ord_reserve_tbl(i).item
            and loc  = L_cust_ord_reserve_tbl(i).loc;
            
      insert into inv_resv_update_temp (location,
                                        loc_type,
                                        process_status,
                                        create_datetime,
                                        create_id,
                                        last_update_datetime,
                                        last_update_id)
                        select distinct reserve_tbl.loc,
                                        L_cust_resv_loc_type,
                                        L_process_status,
                                        SYSDATE,
                                        LP_user,
                                        SYSDATE,
                                        LP_user
                                   from TABLE(CAST(L_cust_ord_reserve_tbl AS CUST_ORD_RESERVE_TBL)) reserve_tbl
                                  where not exists (select 'x'
                                                      from inv_resv_update_temp ir
                                                     where ir.location = reserve_tbl.loc
                                                       and ir.loc_type = L_cust_resv_loc_type
                                                       and ir.process_status = L_process_status);    
   end if;
   ---   
   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      if C_GET_INVALID_RESV_RECORDS%ISOPEN then
         close C_GET_INVALID_RESV_RECORDS;
      end if;
      if C_LOCK_ITEM_LOC_SOH%ISOPEN then
         close C_LOCK_ITEM_LOC_SOH;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_GET_INVALID_RESV_RECORDS%ISOPEN then
         close C_GET_INVALID_RESV_RECORDS;
      end if;
      if C_LOCK_ITEM_LOC_SOH%ISOPEN then
         close C_LOCK_ITEM_LOC_SOH;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_CO_RESERVE;
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CO_SALES(O_error_message         IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cust_ord_sale_tbl     IN        CUST_ORD_SALE_TBL)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(64) := 'CUSTOMER_RESERVE_SQL.PROCESS_CO_SALES';
   L_table                     VARCHAR2(50);
   
   L_cust_ord_reserve_tbl      CUST_ORD_RESERVE_TBL := CUST_ORD_RESERVE_TBL();
   L_xform_orderable_tbl       BTS_ORDITEM_QTY_TBL := NULL;

   -- As most of the items will have standard UOM same as the selling UOM, splitting the cursor 
   -- based on the UOM for better performance.
   -- This cursor does not process the transformable sellable and catch-weight simple pack items.
   -- Both these item types are processed later in different cursors.
   cursor C_GET_SALE_QTY_STANDARD_UOM is
      select CUST_ORD_RESERVE_REC
             (sale_tbl.item,
              sale_tbl.location,
              -1 * NVL(sale_tbl.qty,0))        -- Multiply the quantity with -1 since it is a release reservation request.
        from item_master im,
             TABLE(CAST(I_cust_ord_sale_tbl AS CUST_ORD_SALE_TBL)) sale_tbl
       where im.item = sale_tbl.item
         and im.pack_ind = 'N'
         and NOT(im.item_xform_ind = 'Y'
                 and im.sellable_ind = 'Y'
                 and im.orderable_ind = 'N')
         and im.standard_uom = sale_tbl.qty_uom
      UNION ALL
      select CUST_ORD_RESERVE_REC
             (vpq.item,
              sale_tbl.location,
              -1 * vpq.qty * NVL(sale_tbl.qty,0))
        from v_packsku_qty vpq,
             item_master im,
             TABLE(CAST(I_cust_ord_sale_tbl AS CUST_ORD_SALE_TBL)) sale_tbl
       where im.item = sale_tbl.item
         and im.pack_ind = 'Y'
         and vpq.pack_no = im.item
         and im.catch_weight_ind = 'N'
         and im.standard_uom = sale_tbl.qty_uom;
       
   -- This cursor fetches the rows where the sale UOM is not same as standard UOM. 
   -- This cursor does not process the transformable sellable and catch-weight simple pack items.
   -- Both these item types are processed later in different cursors.         
   cursor C_GET_SALE_QTY_SELLING_UOM is
      select sale_tbl.item,
             NULL pack_no,
             sale_tbl.location,
             NULL pack_item_qty, 
             NVL(sale_tbl.qty,0) qty,
             im.standard_uom,
             sale_tbl.qty_uom,
             im.pack_ind
        from item_master im,
             TABLE(CAST(I_cust_ord_sale_tbl AS CUST_ORD_SALE_TBL)) sale_tbl
       where im.item = sale_tbl.item
         and im.pack_ind = 'N'
         and NOT(im.item_xform_ind = 'Y'
                 and im.sellable_ind = 'Y'
                 and im.orderable_ind = 'N')
         and im.standard_uom != sale_tbl.qty_uom
      UNION ALL
      select vpq.item,
             vpq.pack_no,
             sale_tbl.location,
             vpq.qty pack_item_qty,
             NVL(sale_tbl.qty,0) qty,
             im.standard_uom,
             sale_tbl.qty_uom,
             im.pack_ind
        from v_packsku_qty vpq,
             item_master im,
             TABLE(CAST(I_cust_ord_sale_tbl AS CUST_ORD_SALE_TBL)) sale_tbl
       where im.item = sale_tbl.item
         and im.pack_ind = 'Y'
         and vpq.pack_no = im.item
         and im.catch_weight_ind = 'N'
         and im.standard_uom != sale_tbl.qty_uom;
        
   -- This cursor retrieves the catch-weight simple pack items.        
   cursor C_GET_CATCH_WEIGHT_ITEMS is
      select sale_tbl.item pack_no,
             sale_tbl.location,
             NVL(sale_tbl.qty, 0) pack_qty,
             sale_tbl.qty_uom,
             vpq.item,
             vpq.qty comp_qty
        from item_master im,
             v_packsku_qty vpq,
             TABLE(CAST(I_cust_ord_sale_tbl AS CUST_ORD_SALE_TBL)) sale_tbl
       where im.item = sale_tbl.item
         and im.pack_ind = 'Y'
         and vpq.pack_no = im.item
         and im.catch_weight_ind = 'Y'
         and im.simple_pack_ind = 'Y';
     
   -- This cursor retrieves the transformable sellable items.         
   cursor C_GET_TRANSFORMABLE_ITEMS is
      select sale_tbl.item,
             sale_tbl.location,
             NVL(sale_tbl.qty, 0) qty,
             sale_tbl.qty_uom,
             im.standard_uom
        from item_master im,
             TABLE(CAST(I_cust_ord_sale_tbl AS CUST_ORD_SALE_TBL)) sale_tbl
       where im.item = sale_tbl.item
         and im.item_xform_ind = 'Y'
         and im.sellable_ind = 'Y'
         and im.orderable_ind = 'N';     
 
   TYPE ITEM_QTY_UOM_TBL is TABLE of C_GET_SALE_QTY_SELLING_UOM%ROWTYPE INDEX BY BINARY_INTEGER;
   L_item_qty_uom_TBL ITEM_QTY_UOM_TBL;
   
   TYPE CATCH_WEIGHT_ITEM_TBL is TABLE of C_GET_CATCH_WEIGHT_ITEMS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_catch_weight_item_TBL CATCH_WEIGHT_ITEM_TBL;
   
   TYPE XFORM_SELLABLE_ITEM_SALE_TBL is TABLE of C_GET_TRANSFORMABLE_ITEMS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_xform_sellable_item_sale_TBL XFORM_SELLABLE_ITEM_SALE_TBL;
         
BEGIN

   /* This function is called from the sales upload process to release the  customer reserve inventory *
    * from item_loc_soh table. However since we are dealing with the reservation/release  requests for *
    * regular items, pack items are broken into the component items before doing the reservation/      *
    * release, resulting in the reservation/release of component items.                                *
    * This function also does the UOM conversion of the release quantity if the selling uom contained  *
    * in the sales record is not the same as standard_uom, since customer_resv holds the quantity in   *
    * standard uom.                                                                                    */
                    
   if I_cust_ord_sale_tbl is NULL or I_cust_ord_sale_tbl.COUNT <= 0 then
      return TRUE;
   end if;
   
   L_table := 'ITEM_MASTER, V_PACKSKU_QTY';
      
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SALE_QTY_STANDARD_UOM', L_table, NULL);
   open C_GET_SALE_QTY_STANDARD_UOM;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SALE_QTY_STANDARD_UOM', L_table, NULL);
   fetch C_GET_SALE_QTY_STANDARD_UOM BULK COLLECT into L_cust_ord_reserve_TBL;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SALE_QTY_STANDARD_UOM', L_table, NULL);
   close C_GET_SALE_QTY_STANDARD_UOM;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SALE_QTY_SELLING_UOM', L_table, NULL);
   open C_GET_SALE_QTY_SELLING_UOM;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SALE_QTY_SELLING_UOM', L_table, NULL);
   fetch C_GET_SALE_QTY_SELLING_UOM BULK COLLECT into L_item_qty_uom_TBL;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SALE_QTY_SELLING_UOM', L_table, NULL);
   close C_GET_SALE_QTY_SELLING_UOM;
   ---
   
   -- The collection L_cust_ord_reserve_TBL, is populated with items whose standard_uom is same as the transaction UOM,
   -- requiring no UOM conversion. 
   -- For the records requiring UOM conversion, L_cust_ord_reserve_TBL will be extended in the subsequent loop with the
   -- data fetched from the second cursor.

   if L_item_qty_uom_TBL is NOT NULL and L_item_qty_uom_TBL.COUNT > 0 then
      FOR i in L_item_qty_uom_TBL.FIRST..L_item_qty_uom_TBL.LAST LOOP
      
         -- If the standard uom != transaction uom, call UOM_SQL.CONVERT to convert the
         -- input quantity from transaction uom to standard uom.
         if L_item_qty_uom_TBL(i).pack_ind = 'Y' then
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty_uom_TBL(i).qty,
                               L_item_qty_uom_TBL(i).standard_uom,
                               L_item_qty_uom_TBL(i).qty,
                               L_item_qty_uom_TBL(i).qty_uom,
                               L_item_qty_uom_TBL(i).pack_no,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
            -- If the item is a pack, get the component item quantity.
            L_item_qty_uom_TBL(i).qty := L_item_qty_uom_TBL(i).qty * L_item_qty_uom_TBL(i).pack_item_qty;
         else
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty_uom_TBL(i).qty,
                               L_item_qty_uom_TBL(i).standard_uom,
                               L_item_qty_uom_TBL(i).qty,
                               L_item_qty_uom_TBL(i).qty_uom,
                               L_item_qty_uom_TBL(i).item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --
         L_cust_ord_reserve_TBL.EXTEND;
         
         -- Multiply the quantity with -1 since it has to release the customer reserve quantity.
         L_cust_ord_reserve_TBL(L_cust_ord_reserve_TBL.LAST) := CUST_ORD_RESERVE_REC(L_item_qty_uom_TBL(i).item,
                                                                                     L_item_qty_uom_TBL(i).location,
                                                                                     -1 * L_item_qty_uom_TBL(i).qty);                                             
      END LOOP;
   end if;
   ---
   
   --Process Catch-weight items
   L_table := 'ITEM_MASTER, V_PACKSKU_QTY';
      
   SQL_LIB.SET_MARK('OPEN', 'C_GET_CATCH_WEIGHT_ITEMS', L_table, NULL);
   open C_GET_CATCH_WEIGHT_ITEMS;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_CATCH_WEIGHT_ITEMS', L_table, NULL);
   fetch C_GET_CATCH_WEIGHT_ITEMS BULK COLLECT INTO L_catch_weight_item_TBL;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_CATCH_WEIGHT_ITEMS', L_table, NULL);
   close C_GET_CATCH_WEIGHT_ITEMS;
   
   if L_catch_weight_item_TBL is NOT NULL and L_catch_weight_item_TBL.COUNT > 0 then
      FOR i in L_catch_weight_item_TBL.FIRST..L_catch_weight_item_TBL.LAST LOOP
         if CATCH_WEIGHT_SQL.CONVERT_WEIGHT(O_error_message,
                                            L_catch_weight_item_TBL(i).pack_qty,
                                            L_catch_weight_item_TBL(i).qty_uom,
                                            L_catch_weight_item_TBL(i).pack_no,
                                            L_catch_weight_item_TBL(i).pack_qty,
                                            L_catch_weight_item_TBL(i).qty_uom) = FALSE then
            return FALSE;
         end if;
         
         if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                                  L_catch_weight_item_TBL(i).comp_qty,
                                                  L_catch_weight_item_TBL(i).item,           -- component item
                                                  L_catch_weight_item_TBL(i).pack_qty * L_catch_weight_item_TBL(i).comp_qty,
                                                  NULL,                                      -- weight
                                                  NULL,                                      -- weight uom
                                                  L_catch_weight_item_TBL(i).pack_no,        -- pack
                                                  L_catch_weight_item_TBL(i).location,
                                                  'S',                                       -- Loc_type as Store. 
                                                  L_catch_weight_item_TBL(i).pack_qty) = FALSE then
            return FALSE;
         end if;
         
         L_cust_ord_reserve_TBL.EXTEND;
         
         -- Multiply the quantity with -1 since it has to release the customer reserve quantity.
         L_cust_ord_reserve_TBL(L_cust_ord_reserve_TBL.LAST) := CUST_ORD_RESERVE_REC(L_catch_weight_item_TBL(i).item,
                                                                                     L_catch_weight_item_TBL(i).location,
                                                                                     -1 * L_catch_weight_item_TBL(i).comp_qty);
      END LOOP;
   end if;
            
   --Process transformable items
   L_table := 'ITEM_MASTER';
   
   SQL_LIB.SET_MARK('OPEN', 'C_GET_TRANSFORMABLE_ITEMS', L_table, NULL);
   open C_GET_TRANSFORMABLE_ITEMS;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TRANSFORMABLE_ITEMS', L_table, NULL);
   fetch C_GET_TRANSFORMABLE_ITEMS BULK COLLECT INTO L_xform_sellable_item_sale_TBL;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TRANSFORMABLE_ITEMS', L_table, NULL);
   close C_GET_TRANSFORMABLE_ITEMS;
   
   if L_xform_sellable_item_sale_TBL is NOT NULL and L_xform_sellable_item_sale_TBL.COUNT > 0 then
      -- Convert the quantity of transformable sellable item to standard UOM, get the transformable orderable item with their
      -- quantities and then add the orderable items to L_cust_ord_reserve_TBL.
      FOR i in L_xform_sellable_item_sale_TBL.FIRST..L_xform_sellable_item_sale_TBL.LAST LOOP
         if UOM_SQL.CONVERT(O_error_message,
                            L_xform_sellable_item_sale_TBL(i).qty,            -- Output quantity in std uom
                            L_xform_sellable_item_sale_TBL(i).standard_uom,   
                            L_xform_sellable_item_sale_TBL(i).qty,            -- Input quantity
                            L_xform_sellable_item_sale_TBL(i).qty_uom,
                            L_xform_sellable_item_sale_TBL(i).item,
                            NULL,
                            NULL) = FALSE then
            return FALSE;
         end if;
         
         if ITEM_XFORM_SQL.ORDERABLE_ITEM_INFO(O_error_message,
                                               L_xform_orderable_TBL,                           -- output table
                                               ITEM_TBL(L_xform_sellable_item_sale_TBL(i).item),
                                               QTY_TBL(L_xform_sellable_item_sale_TBL(i).qty)) = FALSE then
            return FALSE;
         end if;
           
         if L_xform_orderable_TBL is NOT NULL and L_xform_orderable_TBL.COUNT > 0 then
            FOR j in L_xform_orderable_TBL.FIRST..L_xform_orderable_TBL.LAST LOOP
               L_cust_ord_reserve_TBL.EXTEND;
               -- Multiply the quantity with -1 since it has to release the customer reserve quantity.
               L_cust_ord_reserve_TBL(L_cust_ord_reserve_TBL.LAST) := CUST_ORD_RESERVE_REC(L_xform_orderable_TBL(j).orderable_item,
                                                                                           L_xform_sellable_item_sale_TBL(i).location,
                                                                                           -1 * L_xform_orderable_TBL(j).qty);
            END LOOP;
         end if;
      END LOOP;
   end if;
   
   -- L_cust_ord_reserve_TBL contains the updates to the customer reservation quantity in standard uom.
   -- Passing this collection to PROCESS_CO_RESERVE to update item_loc_soh.
   if PROCESS_CO_RESERVE(O_error_message,
                         L_cust_ord_reserve_TBL) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_GET_SALE_QTY_STANDARD_UOM%ISOPEN then
         close C_GET_SALE_QTY_STANDARD_UOM;
      end if;
      if C_GET_SALE_QTY_SELLING_UOM%ISOPEN then
         close C_GET_SALE_QTY_SELLING_UOM;
      end if;
      if C_GET_CATCH_WEIGHT_ITEMS%ISOPEN then
         close C_GET_CATCH_WEIGHT_ITEMS;
      end if;
      if C_GET_TRANSFORMABLE_ITEMS%ISOPEN then
         close C_GET_TRANSFORMABLE_ITEMS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_CO_SALES;          
----------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_CUSTOMER_ORDER (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) := 'CUSTOMER_RESERVE_SQL.PURGE_CUSTOMER_ORDER';
   L_table                   VARCHAR2(60);
   
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   -- Fetch the historical records for customer order fulfillment record or obsolete records with status 'X'.
   -- Old customer order purchase or transfer leg records will be purged by respective ordprg.pc or tsfprg.pc jobs.
   cursor C_GET_CUSTOMER_ORDERS is
      select oc.ordcust_no
        from ordcust oc,
             purge_config_options pc,
             period p
       where (   oc.status = 'X' 
             or  (   oc.status = 'C'
                 and oc.order_no is NULL
                 and oc.tsf_no is NULL))
         and NVL(MONTHS_BETWEEN(p.vdate, oc.create_datetime),0) > pc.cust_order_history_months
      for update of oc.ordcust_no nowait;
   
   cursor C_LOCK_ORDCUST_DETAIL is
      select ocd.ordcust_no
        from ordcust oc,
             ordcust_detail ocd,
             purge_config_options pc,
             period p
       where (   oc.status = 'X' 
             or  (   oc.status = 'C'
                 and oc.order_no is NULL
                 and oc.tsf_no is NULL))
         and NVL(MONTHS_BETWEEN(p.vdate, oc.create_datetime),0) > pc.cust_order_history_months
         and oc.ordcust_no = ocd.ordcust_no
      for update of ocd.ordcust_no nowait;
      
   TYPE CUST_ORDER_NO_TBL is TABLE of C_GET_CUSTOMER_ORDERS%ROWTYPE;
   L_ordcust_no_TBL CUST_ORDER_NO_TBL;
   
BEGIN

   L_table := 'ORDCUST';
   
   SQL_LIB.SET_MARK('OPEN', 'C_GET_CUSTOMER_ORDERS', L_table, NULL);
   open C_GET_CUSTOMER_ORDERS;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_CUSTOMER_ORDERS', L_table, NULL);
   fetch C_GET_CUSTOMER_ORDERS BULK COLLECT into L_ordcust_no_TBL;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_CUSTOMER_ORDERS', L_table, NULL);
   close C_GET_CUSTOMER_ORDERS;
   ---
   
   if L_ordcust_no_TBL is not NULL and L_ordcust_no_TBL.COUNT > 0 then

      L_table := 'ORDCUST_DETAIL';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ORDCUST_DETAIL', L_table, NULL);
      open C_LOCK_ORDCUST_DETAIL;

      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ORDCUST_DETAIL', L_table, NULL);
      close C_LOCK_ORDCUST_DETAIL;

      forall i in L_ordcust_no_TBL.first..L_ordcust_no_TBL.last
         delete from ordcust_detail
          where ordcust_no = L_ordcust_no_TBL(i).ordcust_no;
      
      L_table := 'ORDCUST';
      forall i in L_ordcust_no_TBL.first..L_ordcust_no_TBL.last
         delete from ordcust
          where ordcust_no = L_ordcust_no_TBL(i).ordcust_no;
   end if;
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_CUSTOMER_ORDERS%ISOPEN then
         close C_GET_CUSTOMER_ORDERS;
      end if;
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('BULK_TABLE_LOCK', L_table, NULL, NULL);
      return FALSE;
   when OTHERS then
      if C_GET_CUSTOMER_ORDERS%ISOPEN then
         close C_GET_CUSTOMER_ORDERS;
      end if;
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;  
END PURGE_CUSTOMER_ORDER;
----------------------------------------------------------------------------------------------------------------
END CUSTOMER_RESERVE_SQL;
/