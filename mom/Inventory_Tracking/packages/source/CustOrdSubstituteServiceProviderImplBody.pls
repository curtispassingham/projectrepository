/******************************************************************************
* Service Name     : CustOrdSubstituteService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/CustOrdSubstituteService/v1
* Description      : 
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE BODY CustOrdSubstituteServiceProvid AS


/******************************************************************************
 *
 * Operation       : createCustOrdSubColDesc
 * Description     : Create new Customer Orders item Substitute in RMS for a customer order 
                        for requests from an external Order Management System (OMS).
                       
 * 
 * Input           : "RIB_CustOrdSubColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/CustOrdSubColDesc/v1
 * Description     :  CustOrdSubColDesc object holds multiple customer order 
                    item substitute requests from OMS. 
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     :  InvocationSuccess object contains the sucess or failure status of processing the 
                              confirmation requests.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
                    message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
                    "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createCustOrdSubColDesc(I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                                  I_businessObject          IN     "RIB_CustOrdSubColDesc_REC",
                                  O_serviceOperationStatus  OUT    "RIB_ServiceOpStatus_REC",
                                  O_businessObject          OUT    "RIB_InvocationSuccess_REC")
IS
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_status              "RIB_SuccessStatus_REC" := NULL;
   L_status_code         VARCHAR2(1);
   L_successStatus_TBL   "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL      "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
   L_program             VARCHAR2(60)            := 'CustOrdSubstituteServiceProvid.createCustOrdSubColDesc';

BEGIN
   -- Validate the input service operation context and initialize the output service operation object.
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;

   SVCPROV_CUSTORDSUB.CREATE_CO_SUBSTITUTE(O_serviceOperationStatus,
                                           I_businessObject);
   
   -- For any error in the call to SVCPROV_CUSTORDSUB, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is NULL, populate output service object with success message.
   
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.COUNT = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, 'createCustOrdSubColDesc service call was successful.');
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
      O_businessObject := "RIB_InvocationSuccess_REC"(0,'createCustOrdSubColDesc service call was successful.');
   end if;
   
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END createCustOrdSubColDesc;
/******************************************************************************/
END CustOrdSubstituteServiceProvid;
/