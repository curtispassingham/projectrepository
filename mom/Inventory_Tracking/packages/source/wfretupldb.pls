CREATE OR REPLACE PACKAGE BODY WF_RETURN_UPLOAD_SQL AS

LP_new            VARCHAR2(1):= 'N';
LP_rejected       VARCHAR2(1):= 'R';
  

FUNCTION VALIDATE_STG_TABLES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_WF_RET_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id        IN     SVC_WF_RET_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS


   L_table             VARCHAR2(50);
   L_key               VARCHAR2(50);
   L_rej_ind           VARCHAR2(1)  := 'N';
   L_program           VARCHAR2(64) := 'WF_RETURN_UPLOAD_SQL.VALIDATE_STG_TABLES';
   L_head_count        NUMBER(10);
   L_detail_count      NUMBER(10);
   L_cust_ret_ref_no   SVC_WF_RET_HEAD.CUST_RET_REF_NO%TYPE;
   L_hdr_error_msg     VARCHAR2(2000) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);


   --Cursor C_GET_FHEAD is to identify the row in the head table which have error
   --wf_customer_id is NULL
   --Currency code is NULL

   cursor C_GET_FHEAD is
      select svh.wf_return_id,
             svh.wf_customer_id,
             svh.cust_ret_ref_no,
             svh.currency_code,
             svh.error_msg
        from svc_wf_ret_head svh
       where svh.process_id = I_process_id
         and svh.process_status = LP_new
         for update nowait;
         
   --Cursor C_GET_FDETL is to identify the row in the detail table which have error
   --Item is NULL
   --wf_order_no is NULL
   --customer_loc is NULL
   --return_loc_id is NULL
   --Unit_of_measure is NULL
   --returned_qty is less than or equal to zero
   --return_unit_cost is less than zero
   --restock_fee is less than or equal to zero
   --Return_method is invalid
   --Return reason is invalid
   --Restock_type is  invalid
   --Return_loc_type is invalid
   
         
   cursor C_GET_FDETL is
      select svd.wf_return_id,
             svd.line_no,
             svd.item,
             svd.wf_order_no,
             svd.customer_loc,
             svd.return_loc_id,
             svd.return_loc_type,
             svd.return_method,
             svd.unit_of_measure,
             svd.returned_qty,
             svd.return_reason,
             svd.return_unit_cost,
             svd.restock_type,
             svd.unit_restock_fee,
             svd.rowid,
             svd.error_msg
        from svc_wf_ret_detail svd
       where svd.process_id = I_process_id
         and (   svd.item is  NULL
              or (svd.wf_order_no is null and svd.return_unit_cost is null)
              or svd.customer_loc is NULL
              or svd.return_loc_id is NULL
              or svd.unit_of_measure is NULL
              or NVL(svd.return_method,'X') not in ('R','D')
              or NVL(svd.return_loc_type,'X') not in ('S','W')
              or NVL(svd.return_reason,'X') not in ('O','U','W')
              or NVL(svd.restock_type,'S') not in ('S','V')
              or svd.returned_qty is NULL
              or svd.return_unit_cost < 0
              or (svd.returned_qty is not NULL and
                  svd.returned_qty <= 0)
              or (svd.restock_type is NULL
                  and svd.unit_restock_fee is not NULL) 
              or (svd.restock_type is not NULL
                  and svd.unit_restock_fee is NULL)
              or (svd.unit_restock_fee is not NULL
                  and svd.unit_restock_fee <= 0))
         for update nowait;
              

   --Cursor C_GET_FTAIL is to identify the row in the tail table which have error
   --Total record count is not equal to the recods in the detail table.
   --Multiple tail records for a single file
   
   cursor C_GET_FTAIL is
      select svf.wf_return_id,
             svf.total_record_count,
             (select count(*)
               FROM SVC_WF_RET_DETAIL
              where process_id = I_process_id) detail_count,              
             svf.error_msg
        from svc_wf_ret_tail svf
       where svf.process_id = I_process_id
         for update nowait;
   
   TYPE F_HEAD_TBL IS TABLE OF C_GET_FHEAD%ROWTYPE;
   L_f_head_tbl F_HEAD_TBL;
   
   TYPE F_DETAIL_TBL IS TABLE OF C_GET_FDETL%ROWTYPE;
   L_f_detail_tbl F_DETAIL_TBL;
   
   TYPE F_TAIL_TBL IS TABLE OF C_GET_FTAIL%ROWTYPE;
   L_f_tail_tbl F_TAIL_TBL;    

BEGIN
   -- VALIDATE REQUIRED INPUT
   if I_process_id is NULL then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                                  I_process_id,
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;

   -- WFRETUPLD FHEAD VALIDATION
   
   -- Retrieve the header record
   L_table := 'SVC_WF_RET_HEAD';
   L_key   := 'I_process_id: '|| I_process_id || 'I_chunk_id: '|| I_chunk_id;
   
   open C_GET_FHEAD;
   fetch C_GET_FHEAD BULK COLLECT INTO L_f_head_tbl;
   close C_GET_FHEAD;
   
   if(L_f_head_tbl is NOT NULL and L_f_head_tbl.COUNT = 1) then
      L_cust_ret_ref_no := L_f_head_tbl(1).cust_ret_ref_no;
      -- Validate if the currency code is not NULL
      if(L_f_head_tbl(1).currency_code is NULL)then
         L_f_head_tbl(1).error_msg := SQL_LIB.GET_MESSAGE_TEXT('FR_CURRENCY_NULL',
                                                               L_cust_ret_ref_no,
                                                               NULL,
                                                               NULL)||';';
         L_rej_ind := 'Y';                                                      
         L_hdr_error_msg := L_f_head_tbl(1).error_msg;
      end if;
      -- Validate if the customer_id is not NULL
      if(L_f_head_tbl(1).wf_customer_id IS NULL)THEN
         L_hdr_error_msg := L_hdr_error_msg||SQL_LIB.GET_MESSAGE_TEXT('FR_WF_CUSTID_NULL',
                                                                      L_cust_ret_ref_no,
                                                                      NULL,
                                                                      NULL)||';';
         L_rej_ind := 'Y';     
      end if;
   end if;
    
   -- WFRETUPLD FDETL VALIDATION
   -- Retrieve the detail record
   
   L_table := 'SVC_WF_RET_DETAIL';
   L_key   := 'I_process_id: '|| I_process_id || 'I_chunk_id: '|| I_chunk_id;
   
   open C_GET_FDETL;
   fetch C_GET_FDETL BULK COLLECT INTO L_f_detail_tbl;
   close C_GET_FDETL;  
   
   if(L_f_detail_tbl is not NULL and L_f_detail_tbl.count > 0) then
      FOR i in L_f_detail_tbl.first..L_f_detail_tbl.last loop
      -- Validate if the item is not NULL
         if(L_f_detail_tbl(i).item is NULL) then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_ITEM_NULL',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
     -- Validate if the return unit cost is not NULL  
         if(L_f_detail_tbl(i).wf_order_no is NULL) then
             if(L_f_detail_tbl(i).return_unit_cost is NULL) then
                L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                               SQL_LIB.GET_MESSAGE_TEXT('FR_RETURN_UNIT_COST_NULL',
                                                                         L_cust_ret_ref_no,
                                                                         L_f_detail_tbl(i).line_no,
                                                                         NULL)||';';
              end if;                                                           
         end if;
         -- Validate if the customer_loc is NULL
         if(L_f_detail_tbl(i).customer_loc is NULL) then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_WF_CUST_LOC_NULL',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
         -- Validate if the return_loc_id is NULL
         if(L_f_detail_tbl(i).return_loc_id is NULL) then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_RET_LOC_NULL',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
         
         -- Validate if the return_loc_type is valid
          if(NVL(L_f_detail_tbl(i).return_loc_type,'X') not in ('S','W')) then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RETLOC_TYPE',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
         -- Validate if the UOM is not NULL
         if(L_f_detail_tbl(i).unit_of_measure is NULL) then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_UOM_NULL',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
         -- Validate if the return_method is valid
         if(NVL(L_f_detail_tbl(i).return_method,'X') not in ('R','D')) then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RET_METHOD',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
         
         -- Validate if the return_reason is valid
         if(NVL(L_f_detail_tbl(i).return_reason,'X') not in ('O','U','W'))then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RET_REASON',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
         -- Validate if the restock_type is valid
         if(NVL(L_f_detail_tbl(i).restock_type,'S') not in ('S','V')
            or L_f_detail_tbl(i).restock_type is not NULL and L_f_detail_tbl(i).unit_restock_fee is NULL
            or L_f_detail_tbl(i).restock_type is NULL and L_f_detail_tbl(i).unit_restock_fee is not NULL)then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RESTOCK_TYPE',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
         
         -- Validate if the returned_qty is not NULL  
         if(L_f_detail_tbl(i).returned_qty is NULL) then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_RET_QTY_NULL',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;    
         
         -- Validate if the returned_qty is less than or equal to zero
         if(L_f_detail_tbl(i).returned_qty <= 0) then
             L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                            SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RETURNED_QTY',
                                                                     L_cust_ret_ref_no,
                                                                     L_f_detail_tbl(i).line_no,
                                                                     NULL)||';';
         end if;
         -- Validate if the return_unit_cost is less than zero
         if(L_f_detail_tbl(i).return_unit_cost < 0) then
            L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                           SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RETURN_UNIT_COST',
                                                                    L_cust_ret_ref_no,
                                                                    L_f_detail_tbl(i).line_no,
                                                                    NULL)||';';
         end if;
         -- Validate if the restock is less than or equal to zero
         if(L_f_detail_tbl(i).unit_restock_fee <= 0) then
             L_f_detail_tbl(i).error_msg := L_f_detail_tbl(i).error_msg||
                                            SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RESTOCK_FEE',
                                                                     L_cust_ret_ref_no,
                                                                     L_f_detail_tbl(i).line_no,
                                                                     NULL)||';';
         end if;
      END loop;
      L_rej_ind := 'Y';
   end if;
   if L_f_detail_tbl is NOT NULL and L_f_detail_tbl.COUNT > 0 then
   -- Update the detail table with the error_msg.
      FORALL i in 1..L_f_detail_tbl.last         
         update svc_wf_ret_detail
            set error_msg = L_f_detail_tbl(i).error_msg,
                last_update_datetime = sysdate,
                last_update_id = get_user
          where rowid = L_f_detail_tbl(i).rowid;
       

         L_hdr_error_msg := L_hdr_error_msg ||
                            SQL_LIB.GET_MESSAGE_TEXT('FR_ERROR_SVC_DETAIL',L_cust_ret_ref_no,NULL,NULL)||';';           

   end if;
   
   -- WFRETUPLD FTAIL VALIDATION
   -- Retrieve the tail record
   
   L_table := 'SVC_WF_RET_TAIL';
   L_key   := 'I_process_id: '|| I_process_id || 'I_chunk_id: '|| I_chunk_id;
   
   open  C_GET_FTAIL;
   fetch C_GET_FTAIL BULK COLLECT INTO L_f_tail_tbl;
   close C_GET_FTAIL;
   
   if(L_f_tail_tbl is NULL or L_f_tail_tbl.COUNT < 1) then      
      L_hdr_error_msg := L_hdr_error_msg ||
                         SQL_LIB.GET_MESSAGE_TEXT('FR_MISSING_TAIL',L_cust_ret_ref_no,NULL,NULL)||';'; 
      
      L_rej_ind := 'Y';       
         
   elsif (L_f_tail_tbl is NOT NULL and L_f_tail_tbl.COUNT > 1) then
          L_f_tail_tbl(1).error_msg := SQL_LIB.GET_MESSAGE_TEXT('FR_INV_FTAIL_COUNT',
                                                                L_cust_ret_ref_no,
                                                                NULL,
                                                                NULL);
         update svc_wf_ret_tail
            set error_msg = SQL_LIB.GET_MESSAGE_TEXT('FR_INV_FTAIL_COUNT',L_cust_ret_ref_no,NULL,NULL)||';',
                last_update_datetime = sysdate,
                last_update_id = get_user
          where process_id = I_process_id;  
         
         L_hdr_error_msg := L_hdr_error_msg ||
                            SQL_LIB.GET_MESSAGE_TEXT('FR_ERROR_SVC_TAIL',L_cust_ret_ref_no,NULL,NULL)||';'; 
         L_rej_ind := 'Y';                         
      -- Check if FTAIL record count matches number of detail records    
   elsif (L_f_tail_tbl(1).total_record_count <> L_f_tail_tbl(1).detail_count)then
          L_f_tail_tbl(1).error_msg := SQL_LIB.GET_MESSAGE_TEXT('FR_COUNT_NOT_MATCH',
                                                                L_cust_ret_ref_no,
                                                                NULL,
                                                                NULL);
        -- Update the tail table with the error_msg.  
         update svc_wf_ret_tail
            set error_msg = SQL_LIB.GET_MESSAGE_TEXT('FR_COUNT_NOT_MATCH',L_cust_ret_ref_no,NULL,NULL)||';',
                last_update_datetime = sysdate,
                last_update_id = get_user
          where process_id = I_process_id;
          -- Update the status of the process_id in the head table to reject    
         
         L_hdr_error_msg := L_hdr_error_msg ||
                            SQL_LIB.GET_MESSAGE_TEXT('FR_ERROR_SVC_TAIL',L_cust_ret_ref_no,NULL,NULL)||';';
         L_rej_ind := 'Y';                   
   end if;
     
   update SVC_WF_RET_HEAD
      set error_msg = L_hdr_error_msg,
          process_status = LP_rejected,
          last_update_datetime = sysdate,
          last_update_Id = get_user
    where process_Id = I_Process_Id
      and chunk_id = I_chunk_id
      and L_hdr_error_msg is not NULL; 
   
   if ( L_rej_ind = 'Y') then     
      return FALSE;
   end if;
   
   return TRUE;   

   
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key,
                                             NULL);

      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  L_program,
                                                  to_char(SQLCODE));
      return FALSE;  
END VALIDATE_STG_TABLES;
-------------------------------------------------------------------------------
END WF_RETURN_UPLOAD_SQL;
/