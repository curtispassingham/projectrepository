CREATE OR REPLACE PACKAGE CUSTOMER_RESERVE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------------
/* Function Name  :  PROCESS_CO_RESERVE
 * Purpose        :  This function will update the customer_resv field in item_loc_soh based on the  *
 *                   reservation or release request in the input collection. The quantity in the     *
 *                   input collection should be the delta amount which will be added to the existing *
 *                   cutomer_resv field in item_loc_soh table. If the request is to reserve          *
 *                   inventory, the quantity field should be positive. If the request is to release  *
 *                   the inventory, the quantity field should be negative. This function is valid    *
 *                   only for the reservation requests made for the regular items at customer        *
 *                   orderable physical stores. Transformable items are ignored with the assumption  *
 *                   that they have infinite inventory.                                              */
---------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CO_RESERVE(O_error_message         IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cust_ord_reserve_tbl  IN        CUST_ORD_RESERVE_TBL) 
return BOOLEAN;
---------------------------------------------------------------------------------------------------------
/* Function Name  :  PROCESS_CO_SALES
 * Purpose        :  This function be called from the sales upload process to release the customer   *
 *                   reserve inventory from item_loc_soh table. This function should be called only  *
 *                   for Rolled up sales transaction for OMS customer order sales only. For non OMS  *
 *                   Customer order sales (Regular store sales or In-Store Customer order sales) or  *
 *                   for a return of online item in the store, this function should not be called.   *
 *                   The sign of the sale quantity should be the same as posted in POSU file. In case*
 *                   of regular sale posting, it will be +ve. However, we can also have a sale       *
 *                   transaction edited in ReSA after it was posted to RMS. For this, POSU will get  *
 *                   two records: -ve sales record to negate the earlier posted sales and a +ve sales*
 *                   record with the corrected value. The SUM of total sales quantity will be taken  *
 *                   up to release the customer reserve column.                                      */
---------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CO_SALES(O_error_message         IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cust_ord_sale_tbl     IN        CUST_ORD_SALE_TBL)
return BOOLEAN; 
---------------------------------------------------------------------------------------------------------
/* Function Name  :  PURGE_CUSTOMER_ORDER
 * Purpose        :  This function will purge the customer records from ORDCUST and ORDCUST_DETAIL   *
 *                   tables based on the CUST_ORDER_HISTORY_MONTHS from PURGE_CONFIG_OPTIONS table.  *
 *                   This will also purge the obsolete records created through PO and TSF with       *
 *                   status 'X'                                                                      */
---------------------------------------------------------------------------------------------------------
FUNCTION PURGE_CUSTOMER_ORDER(O_error_message         IN OUT    RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------------------

END;
/