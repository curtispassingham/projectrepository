CREATE OR REPLACE PACKAGE BODY WF_CREATE_ORDER_SQL AS

LP_vdate            PERIOD.VDATE%TYPE    := get_vdate;
LP_create_type      VARCHAR2(6)          := RMS_CONSTANTS.WF_ACTION_CREATE;
LP_update_type      VARCHAR2(6)          := RMS_CONSTANTS.WF_ACTION_UPDATE;
LP_delete_type      VARCHAR2(6)          := RMS_CONSTANTS.WF_ACTION_DELETE;
LP_cancel_type      VARCHAR2(6)          := RMS_CONSTANTS.WF_ACTION_CANCEL;

TYPE ROWID_TBL is TABLE of ROWID INDEX BY BINARY_INTEGER;
------------------------------------------------------------------------------------------------
---                                       PRIVATE FUNCTIONS                                 ---
------------------------------------------------------------------------------------------------
---FUNCTION NAME:VALIDATE_F_ORDER
---Purpose: This function validates the input header and detail records based on the input
---         action type.
------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_F_ORDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC,
                          I_action_type          IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:CREATE_HEADER
---Purpose: This function will build the Franchise order based on the input collection of header 
---         and detail records, when a transfer/PO/allocation is created to a Franchise store.
---         This function will essentailly insert the records into wf_order_head, and will return the 
---         newly generated wf_order_no, which will be used to update the wf_order_no field on the 
---         tsf/PO/alloc tables. This will essentially serve as a link between the Franchise order, 
---         and its associated transfer/PO/allocation. 
------------------------------------------------------------------------------------------------
FUNCTION CREATE_HEADER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_wf_order_no          IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:MODIFY_HEADER
---Purpose: This function will update the existing Franchise order based on the input header record, 
---         when the linked transfer/allocation/PO is modified. The header level modifications to 
---         the existing Franchise order essentially includes updates to the status and the
---         cancel_reason on wf_order_head.
------------------------------------------------------------------------------------------------
FUNCTION MODIFY_HEADER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:CANCEL_F_ORDER
---Purpose: This function will update the status and cancelled_reason of existing Franchise order 
---         based on the input collection of header record, when the linked PO is cancelled.
------------------------------------------------------------------------------------------------
FUNCTION CANCEL_F_ORDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:CREATE_DETAIL
---Purpose: This function will build the Franchise order detail records based on the input collection 
---         of header and detail records, when a transfer/PO/allocation is created to a Franchise store
---         or the line items are added to an existing tsf/PO/allocation. Post approval, the detail
---         line items will be created with 0 requested_qty. This function will also insert 
---         expenses into wf_order_exp.
------------------------------------------------------------------------------------------------
FUNCTION CREATE_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC,
                       I_action_type          IN       VARCHAR2,
                       I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:MODIFY_DETAIL
---Purpose: This function will update the existing Franchise order detail record based on the input 
---         header record, when the linked transfer/allocation/PO is modified to change the qty, 
---         need date, etc for the line items. Post approval, the detail line items will not be
---         modified. This function will also update the expenses in wf_order_exp to handle the
---         updates to requested_qty. 
------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:DELETE_DETAIL
---Purpose: This function will delete the Franchise order detail record when a line item is deleted 
---         from the linked tsf/allocation/PO. This will also insert the deleted item into 
---         wf_order_audit.
------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:SYNC_DEPOSIT_ITEMS
---Purpose: This function will sync the deposit containers with the deposit contents on the order
---         resulting in insert/update/delete of container items in the order.
------------------------------------------------------------------------------------------------
FUNCTION SYNC_DEPOSIT_ITEMS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wf_order_no          IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                            I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---FUNCTION NAME:DELETE_F_ORDER
---Purpose: This function will delete the records from all the wf order tables for the passed 
---         wf order number.
------------------------------------------------------------------------------------------------
FUNCTION DELETE_F_ORDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_wf_order_no          IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
---                                       PUBLIC FUNCTION                                 ---
------------------------------------------------------------------------------------------------
---FUNCTION NAME:PROCESS_F_ORDER
---Purpose: This public function will handle the Franchise order creation through
---         replenishment requests, store orders, Item requests, AIP generated
---         transfers and allocations for stockholding Franchise stores. This
---         function will validate the input order record, and process the record
---         based on the input action type.This function will in turn call the private
---         functions to perform the necessary validations and insert/update/delete
---         the records in wf_order_head and wf_order_detail based on the input action type.
----------------------------------------------------------------------------------
FUNCTION PROCESS_F_ORDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_wf_order_no          IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                         I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC,
                         I_action_type          IN       VARCHAR2,
                         I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.PROCESS_F_ORDER';
   L_table                VARCHAR2(50);
     
   L_f_order_head_rec     OBJ_F_ORDER_HEAD_REC;
   L_wf_order_no          WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_order_type           WF_ORDER_HEAD.ORDER_TYPE%TYPE;
   L_existing_status      WF_ORDER_HEAD.STATUS%TYPE;                                                   
        
   --Fetch the existing status. MODIFY_DETAIL will be called only
   --if the status is 'I' or 'R'
   cursor C_GET_EXISTING_STATUS is
      select status,
             order_type
        from wf_order_head
       where wf_order_no = L_wf_order_no;    
               
BEGIN
   
   if I_f_order_head_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec', L_program, NULL);
      return FALSE;
   end if;
   
   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_action_type', L_program, NULL);
      return FALSE;
   end if;
   
   if I_action_type not in (LP_create_type, LP_update_type, LP_cancel_type, LP_delete_type) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE', 
                                            I_action_type, 
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- Validate that the input collection contains the required fields based on the input action type. 
   if VALIDATE_F_ORDER(O_error_message,
                       I_f_order_head_rec,
                       I_action_type) = FALSE then
      return FALSE;
   end if;
   
   L_wf_order_no := I_f_order_head_rec.wf_order_no;         --L_wf_order_no will be NULL for create request
   
   if I_action_type = LP_create_type then
      L_f_order_head_rec := I_f_order_head_rec;
      
      --Create Franchise order header
      if CREATE_HEADER(O_error_message,
                       L_wf_order_no,
                       L_f_order_head_rec) = FALSE then
         return FALSE;
      end if;
      
      L_f_order_head_rec.wf_order_no := L_wf_order_no;
      
      if L_f_order_head_rec.f_order_details is NOT NULL and L_f_order_head_rec.f_order_details.COUNT > 0 then
         
         --Set the wf_order_no to newly generated wf_order_no returned by CREATE_HEADER
         FOR i in L_f_order_head_rec.f_order_details.FIRST..L_f_order_head_rec.f_order_details.LAST LOOP
            L_f_order_head_rec.f_order_details(i).wf_order_no := L_wf_order_no;
         END LOOP;
      
         --Create Franchise order details
         if CREATE_DETAIL(O_error_message,
                          L_f_order_head_rec,
                          I_action_type,
                          NULL) = FALSE then
            return FALSE;
         end if;
         
         --Sync the deposit container with the deposit content on the order
         if SYNC_DEPOSIT_ITEMS(O_error_message,
                               L_wf_order_no,
                               NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      
      O_wf_order_no := L_wf_order_no;
      
   elsif I_action_type = LP_update_type then
      --Action_type 'update' can include the following updates:
      --a) Modify the header for the status and the comments.
      --b) Add new records into wf_order_detail.
      --c) Modify the quantity/dates for the existing records in wf_order_detail.
      --d) Delete the records from wf_order_detail
      
      if I_f_order_head_rec.f_order_details is NOT NULL and I_f_order_head_rec.f_order_details.COUNT > 0 then
         
         L_table := 'WF_ORDER_HEAD';
         
         SQL_LIB.SET_MARK('OPEN', 'C_GET_EXISTING_STATUS', L_table, to_char(L_wf_order_no));
         open C_GET_EXISTING_STATUS;
         
         SQL_LIB.SET_MARK('FETCH', 'C_GET_EXISTING_STATUS', L_table, to_char(L_wf_order_no));
         fetch C_GET_EXISTING_STATUS into L_existing_status, L_order_type;
         
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EXISTING_STATUS', L_table, to_char(L_wf_order_no));
         close C_GET_EXISTING_STATUS;

         -- The quantity edits should be applied to franchise order (Auto and externally generated) if the franchise order is not approved. 
         -- The line item delete should result in deleting the franchise order detail line for Auto and externally generated franchise order. 
         -- New line item create should result in creating a new franchise detail line. Post approval, requested_qty should be set to '0'. 
         if L_order_type in ('X','A') then
            if L_existing_status in ('I', 'R') then
               if MODIFY_DETAIL(O_error_message,
                                I_f_order_head_rec) = FALSE then
                  return FALSE;
               end if;
            end if;

            if DELETE_DETAIL(O_error_message,
                             I_f_order_head_rec) = FALSE then
               return FALSE;
            end if;
         end if;
          
         --Call create_detail after modify_detail to ensure that modify_detail
         --does not update the newly inserted records
         if CREATE_DETAIL(O_error_message,
                          I_f_order_head_rec,
                          I_action_type,
                          I_tsf_no) = FALSE then
            return FALSE;
         end if;
         
         --Sync the deposit container with the deposit content on the order
         if SYNC_DEPOSIT_ITEMS(O_error_message,
                               L_wf_order_no,
                               I_tsf_no) = FALSE then
            return FALSE;
         end if;
      
      end if; --end input detail collection is not null
      
      --Modify the Franchise order status at the end
      if MODIFY_HEADER(O_error_message,
                       I_f_order_head_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_action_type = LP_delete_type then
      if DELETE_F_ORDER(O_error_message,
                        L_wf_order_no) = FALSE then
         return FALSE;
      end if; 
   elsif I_action_type = LP_cancel_type then
      if CANCEL_F_ORDER(O_error_message,
                        I_f_order_head_rec) = FALSE then
         return FALSE;
      end if;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_GET_EXISTING_STATUS%ISOPEN then
         close C_GET_EXISTING_STATUS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_F_ORDER;
----------------------------------------------------------------------------------
FUNCTION VALIDATE_F_ORDER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_f_order_head_rec    IN       OBJ_F_ORDER_HEAD_REC,
                          I_action_type         IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.VALIDATE_F_ORDER';
   
   L_wf_order_no        WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_item               WF_ORDER_DETAIL.ITEM%TYPE;
   L_source_loc_type    WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_source_loc_id      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_customer_loc       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_existing_status    WF_ORDER_HEAD.STATUS%TYPE;
   L_order_type         WF_ORDER_HEAD.ORDER_TYPE%TYPE;
   L_count              NUMBER  := NULL;
   L_exist_store        BOOLEAN;
   L_store_row          STORE%ROWTYPE;
   L_container_item     ITEM_MASTER.CONTAINER_ITEM%TYPE;
      
   cursor C_GET_STATUS_ORDER_TYPE is
      select status, 
             order_type
        from wf_order_head
       where wf_order_no = L_wf_order_no; 
       
   --Check that there are no duplicate records in the input detail collection
   cursor C_CHECK_DUPLICATE_DTL is
      select item, 
             source_loc_type, 
             source_loc_id, 
             customer_loc
        from (select item, 
                     source_loc_type, 
                     source_loc_id, 
                     customer_loc
                from TABLE(CAST(I_f_order_head_rec.f_order_details as OBJ_F_ORDER_DETAIL_TBL)) wodt
               group by wf_order_no, item, source_loc_type, source_loc_id, customer_loc        
              having count(*) > 1)                                                
       where rownum = 1;
       
   --Check that the quantity is a whole number when the item's SUOM is 'Eaches'       
   cursor C_CHECK_ITEM_QTY is
      select wodt.item
        from TABLE(CAST(I_f_order_head_rec.f_order_details as OBJ_F_ORDER_DETAIL_TBL)) wodt,
             item_master im
       where wodt.item = im.item
         and im.standard_uom = 'EA'
         and ROUND(wodt.requested_qty) <> wodt.requested_qty
         and rownum = 1;    
         
   --Check that all the Franchise locations belong to the same customer
   cursor C_CHECK_ALL_LOC_CUSTOMER is
      select count(*)
        from store s,
             TABLE(CAST(I_f_order_head_rec.f_order_details as OBJ_F_ORDER_DETAIL_TBL)) wodt
       where wodt.customer_loc = s.store
         and s.store_type      = 'F'     --This will also ensure that all the locations are Franchise stores
       group by wf_customer_id;

   --Check that the deposit container item-supplier relationship exists if source_loc_type = 'SU'
   --Ranging to store/warehouse will be done later if not already ranged.         
   cursor C_CHK_DEP_CONT_SUP_RNGNG is
      select im.container_item,
             wodt.item,
             wodt.source_loc_id
        from TABLE(CAST(I_f_order_head_rec.f_order_details as OBJ_F_ORDER_DETAIL_TBL)) wodt,
             item_master im
       where wodt.item = im.item
         and NVL(im.deposit_item_type, '0') = 'E'
         and wodt.source_loc_type = 'SU' 
         and not exists(select 'x'
                          from item_supplier is1
                         where is1.item     =  im.container_item
                           and is1.supplier =  wodt.source_loc_id)
         and rownum = 1;
                           
BEGIN

   --Header level validations
   --Validate the following:
   --1. If action_type = 'Create', then order_type must be populated. Order_type must be either 'A'uto or E'x'ternal.
   --2. If action_type is 'create' or 'update', then the status must be populated. Valid status values are 'I', 'A','P','R'.
   --3. For all the action types other than create, wf_order_no must be populated, and must be valid.
   --4. For 'create' and 'update' actions, if the status is 'A'pproved, then the detail records must be populated.
   --5. Franchise orders in "In progress" status cannot be deleted.
   --6. Only auto/external type Franchise orders can be deleted/cancelled.
   --7. If action_type is 'cancel', then the cancel reason in the header record must be populated.
   
   L_wf_order_no := I_f_order_head_rec.wf_order_no;
   
   if I_action_type = LP_create_type then
      if I_f_order_head_rec.order_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.order_type', L_program, NULL);
         return FALSE;
      end if;
      
      if I_f_order_head_rec.order_type not in ('X','A') then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_f_order_head_rec.order_type', I_f_order_head_rec.order_type, 'X or A');
         return FALSE;
      end if;
   else  -- action type is update, delete or cancel
      if I_f_order_head_rec.wf_order_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.wf_order_no', L_program, NULL);
         return FALSE;
      end if;
      
      SQL_LIB.SET_MARK('OPEN', 'C_GET_STATUS_ORDER_TYPE', 'WF_ORDER_HEAD', to_char(L_wf_order_no));
      open C_GET_STATUS_ORDER_TYPE;
      
      SQL_LIB.SET_MARK('FETCH', 'C_GET_STATUS_ORDER_TYPE', 'WF_ORDER_HEAD', to_char(L_wf_order_no));
      fetch C_GET_STATUS_ORDER_TYPE into L_existing_status, L_order_type;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_STATUS_ORDER_TYPE', 'WF_ORDER_HEAD', to_char(L_wf_order_no));
      close C_GET_STATUS_ORDER_TYPE;
      
      --Check that the Franchise order exists for an existing franchise order maintenance
      if L_existing_status is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('WF_ORD_NOT_EXISTS', L_wf_order_no, NULL, NULL);
         return FALSE;
      end if;
      
      --If the order is already cancelled/closed, then it cannot be updated/cancelled
      if L_existing_status in ('C', 'D') and I_action_type in (LP_update_type, LP_cancel_type) then
         O_error_message := SQL_LIB.CREATE_MSG('WF_UPDATE_NOT_ALLOWED', L_wf_order_no, NULL, NULL);
         return FALSE;
      end if;
      
      --Only external/auto type orders can be deleted/cancelled
      if L_order_type not in ('X', 'A') and I_action_type in (LP_delete_type, LP_cancel_type) then
         O_error_message := SQL_LIB.CREATE_MSG('WF_ONLY_AUTO_EXT_ALLOWED', L_wf_order_no, NULL, NULL);
         return FALSE;
      end if;
      
      -- Cancel reason is mandatory for header level cancellation.
      if I_action_type = LP_cancel_type then
         if I_f_order_head_rec.cancel_reason is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.cancel_reason', L_program, NULL);
            return FALSE;
         end if;
      end if;
      
      --Orders in "In Progress" status cannot be deleted
      if I_action_type = LP_delete_type and L_existing_status = 'P' then
         O_error_message := SQL_LIB.CREATE_MSG('WF_DELETE_NOT_ALLOWED', L_wf_order_no, NULL, NULL);
         return FALSE;
      end if;
   end if;   --end action_type = create
   
   if I_action_type in (LP_create_type, LP_update_type) then
      if I_f_order_head_rec.status is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.status', L_program, NULL);
         return FALSE;
      end if;

      --Franchise order can be created only in 'I'nput or 'A'pproved or 'R'ejected status
      if I_f_order_head_rec.status not in ('I','A','P','R') or
         (I_action_type = LP_create_type and I_f_order_head_rec.status = 'P') then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE', 'Order Status', I_f_order_head_rec.status, NULL);
         return FALSE;
      end if;

      ---If there are no detail records, error out
      if I_f_order_head_rec.f_order_details is NULL or I_f_order_head_rec.f_order_details.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('DTLS_MUST_EXIST', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   ---     
   --Detail level validations
   --Validate the following:
   --1. Following fields must be populated:
   --       item
   --       source_loc_type
   --       source_loc_id
   --       customer_loc
   --       need_date
   --       not_after_date
   --       requested_qty
   --2. Source_loc_type must be 'SU'/'ST'/'WH'.
   --3. Need date must be greater than or equal to the vdate.
   --4. Not after date must be greater than the not after date.
   --5. Check that duplicate detail records are not present in the input collection.
   --6. Validate that the quantity is a whole number when the item's SUOM is 'E'aches.
   --7. Validate that all the locations belong to the same customer.
   --8. Customer location is a valid Franchise store.
   --9. Source location if store is a valid stock-holding company store.
   --10. If the Franchise store is non-stockholding, then the source location is not a store
   
   if I_f_order_head_rec.f_order_details is NOT NULL and I_f_order_head_rec.f_order_details.COUNT > 0 then
      --Check for duplicate detail records
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_DUPLICATE_DTL', NULL, NULL);
      open C_CHECK_DUPLICATE_DTL;
      
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_DUPLICATE_DTL', NULL, NULL);
      fetch C_CHECK_DUPLICATE_DTL into L_item, L_source_loc_type, L_source_loc_id, L_customer_loc;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_DUPLICATE_DTL', NULL, NULL);
      close C_CHECK_DUPLICATE_DTL;
      
      if L_item is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('WF_DUP_DTL_RECS', L_item, L_source_loc_type || ':'|| L_source_loc_id, L_customer_loc);
         return FALSE;
      end if;
      
      --Check that the quantity is a whole number when the standard UOM of the item is
      --'Eaches'.
      
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ITEM_QTY', 'V_ITEM_MASTER', NULL);
      open C_CHECK_ITEM_QTY;
      
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ITEM_QTY', 'V_ITEM_MASTER', NULL);
      fetch C_CHECK_ITEM_QTY into L_item;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM_QTY', 'V_ITEM_MASTER', NULL);
      close C_CHECK_ITEM_QTY;
      
      if L_item is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('WF_NO_DECIMAL_QTY', L_wf_order_no, L_item, NULL);
         return FALSE;
      end if;
      
      --Validate that all the Franchise locations belong to the same customer
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ALL_LOC_CUSTOMER', 'V_STORE', NULL);
      open C_CHECK_ALL_LOC_CUSTOMER;
      
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ALL_LOC_CUSTOMER', 'V_STORE', NULL);
      fetch C_CHECK_ALL_LOC_CUSTOMER into L_count;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ALL_LOC_CUSTOMER', 'V_STORE', NULL);
      close C_CHECK_ALL_LOC_CUSTOMER;
      
      if NVL(L_count, 0) <> I_f_order_head_rec.f_order_details.COUNT then
         O_error_message := SQL_LIB.CREATE_MSG('WF_DIFF_CUST', NULL, NULL, NULL);
         return FALSE;
      end if;
         
      --Detail level validation
      FOR i in I_f_order_head_rec.f_order_details.FIRST..I_f_order_head_rec.f_order_details.LAST LOOP
         L_item := I_f_order_head_rec.f_order_details(i).item;
         L_source_loc_type := I_f_order_head_rec.f_order_details(i).source_loc_type;
         L_source_loc_id := I_f_order_head_rec.f_order_details(i).source_loc_id;
         L_customer_loc := I_f_order_head_rec.f_order_details(i).customer_loc;
         
         if L_item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.f_order_details('||i||').item', L_program, NULL);
            return FALSE;
         end if;
         
         if L_source_loc_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.f_order_details('||i||').source_loc_type', L_program, NULL);
            return FALSE;
         end if;
         
         if L_source_loc_type not in ('SU','ST','WH') then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                                  'I_f_order_head_rec.f_order_details('||i||').source_loc_type', 
                                                   L_source_loc_type, 
                                                  'SU or ST or WH');
            return FALSE;
         end if;
         
         if L_source_loc_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.f_order_details('||i||').source_loc_id', L_program, NULL);
            return FALSE;
         end if;
         
         --Source location if store must be a company stock-holding store
         if L_source_loc_type = 'ST' then
            if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                        L_exist_store,
                                        L_store_row,
                                        L_source_loc_id) = FALSE then
               return FALSE;
            end if;
         
            if L_exist_store = FALSE then
               O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            -- Check if the input store is a stockholding store.
            elsif L_store_row.store_type != 'C' or L_store_row.stockholding_ind = 'N' then
               O_error_message := SQL_LIB.CREATE_MSG('WF_ONLY_COMP_STCKHLDNG',
                                                     L_source_loc_id,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;   
         end if;
         
         if L_customer_loc is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.f_order_details('||i||').customer_loc', L_program, NULL);
            return FALSE;
         end if;

         if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                     L_exist_store,
                                     L_store_row,
                                     L_customer_loc) = FALSE then
            return FALSE;
         end if;
      
         if L_exist_store = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         -- Check if the input store is a stockholding store.
         elsif L_source_loc_type = 'ST' and L_store_row.stockholding_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('WF_NO_ST_TO_NON_STKHLDNG',
                                                  L_wf_order_no,
                                                  L_customer_loc,
                                                  NULL);
            return FALSE;
         end if;
         
         if I_f_order_head_rec.f_order_details(i).need_date is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.f_order_details('||i||').need_date', L_program, NULL);
            return FALSE;
         end if;
         
         if I_f_order_head_rec.f_order_details(i).not_after_date is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_f_order_head_rec.f_order_details('||i||').not_after_date', L_program, NULL);
            return FALSE;
         end if;
         
         --Check the need date and not after date
         if I_f_order_head_rec.f_order_details(i).need_date < LP_vdate then
            O_error_message := SQL_LIB.CREATE_MSG('WF_NEED_DATE',NULL,NULL,NULL);
            return FALSE;
         end if;
         
         if I_f_order_head_rec.f_order_details(i).not_after_date < I_f_order_head_rec.f_order_details(i).need_date then
            O_error_message := SQL_LIB.CREATE_MSG('WF_NOT_AFTER_DATE',NULL,NULL,NULL);
            return FALSE;
         end if;
         
         if I_f_order_head_rec.f_order_details(i).requested_qty is NULL or I_f_order_head_rec.f_order_details(i).requested_qty < 0 then
            O_error_message := SQL_LIB.CREATE_MSG('WF_NO_NULL_NEG_ZERO_QTY', L_item, L_source_loc_type || ':'|| L_source_loc_id, L_customer_loc);
            return FALSE;
         end if;
      END LOOP;
      
      SQL_LIB.SET_MARK('OPEN', 'C_CHK_DEP_CONT_SUP_RNGNG', 'ITEM_MASTER, ITEM_SUPPLIER', NULL);
      open C_CHK_DEP_CONT_SUP_RNGNG;
      
      SQL_LIB.SET_MARK('FETCH', 'C_CHK_DEP_CONT_SUP_RNGNG', 'ITEM_MASTER, ITEM_SUPPLIER', NULL);
      fetch C_CHK_DEP_CONT_SUP_RNGNG into L_container_item, L_item, L_source_loc_id;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_CHK_DEP_CONT_SUP_RNGNG', 'ITEM_MASTER, ITEM_SUPPLIER', NULL);
      close C_CHK_DEP_CONT_SUP_RNGNG;
      
      if L_container_item is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('WF_CONT_ITEM_NOT_RANGED', L_item, L_container_item, L_source_loc_id);
         return FALSE;
      end if;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CHECK_DUPLICATE_DTL%ISOPEN then
         close C_CHECK_DUPLICATE_DTL;
      end if;
      if C_CHECK_ITEM_QTY%ISOPEN then
         close C_CHECK_ITEM_QTY;
      end if;
      if C_CHECK_ALL_LOC_CUSTOMER%ISOPEN then
         close C_CHECK_ALL_LOC_CUSTOMER;
      end if;
      if C_GET_STATUS_ORDER_TYPE%ISOPEN then
         close C_GET_STATUS_ORDER_TYPE;
      end if;
      if C_CHK_DEP_CONT_SUP_RNGNG%ISOPEN then
         close C_CHK_DEP_CONT_SUP_RNGNG;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_F_ORDER;
-----------------------------------------------------------------------------------       
FUNCTION CREATE_HEADER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_wf_order_no          IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN IS   
   L_program             VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.CREATE_HEADER';
   
   L_wf_order_no         WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_currency_code       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   L_exchange_rate       CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_det_count           NUMBER;
   
   --Get the Franchise location currency code if all the
   --locations have the same currency code
   cursor C_GET_ALL_LOC_SAME_CURR is
      select s.currency_code
        from store s,
             TABLE(CAST(I_f_order_head_rec.f_order_details as OBJ_F_ORDER_DETAIL_TBL)) wodt
       where wodt.customer_loc = s.store
       group by currency_code
       having count(*) = L_det_count;
       
BEGIN

   --Call wf_order_sql which returns the next sequence number to be used
   --as the Franchise order number of the new record
   if WF_ORDER_SQL.NEXT_WF_ORDER_NO(O_error_message,
                                    L_wf_order_no) = FALSE then
      return FALSE;
   end if;
   
   --If the currency code in the input is not populated, check if all the locations have the 
   --same currency code. If yes, use the fetched currency code, else get the currency code 
   --from system options.
   
   if I_f_order_head_rec.currency_code is NULL then
      if I_f_order_head_rec.f_order_details is NOT NULL and I_f_order_head_rec.f_order_details.COUNT > 0 then
         L_det_count := I_f_order_head_rec.f_order_details.COUNT;
         
         SQL_LIB.SET_MARK('OPEN', 'C_GET_ALL_LOC_SAME_CURR', 'STORE', NULL);
         open C_GET_ALL_LOC_SAME_CURR;
         
         SQL_LIB.SET_MARK('FETCH', 'C_GET_ALL_LOC_SAME_CURR', 'STORE', NULL);
         fetch C_GET_ALL_LOC_SAME_CURR into L_currency_code;
         
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_ALL_LOC_SAME_CURR', 'STORE', NULL);
         close C_GET_ALL_LOC_SAME_CURR;
      end if;
      
      if L_currency_code is NULL then
         if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                             L_currency_code) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      L_currency_code := I_f_order_head_rec.currency_code;
   end if;
         
   --Get the exchange rate         
   if CURRENCY_SQL.GET_RATE(O_error_message,
                            L_exchange_rate,
                            L_currency_code,
                            NULL,
                            NULL) = FALSE then
      return FALSE;
   end if;
                              
   insert into wf_order_head (wf_order_no,
                              cust_ord_ref_no,
                              status,
                              order_type,
                              currency_code,
                              exchange_rate,
                              freight,
                              other_charges,
                              cancel_reason,
                              all_location_billing,
                              default_bill_to_loc,
                              bill_to_addr_type,
                              comments,
                              approval_date,
                              create_datetime,
                              create_id,
                              last_update_datetime,
                              last_update_id)
                      values  (L_wf_order_no,
                               NULL,               --cust_ord_ref_no
                               I_f_order_head_rec.status,
                               I_f_order_head_rec.order_type,
                               L_currency_code,
                               L_exchange_rate,
                               NULL,               --freight
                               NULL,               --other charges
                               NULL,               --cancel reason
                               'Y',                --all_location_billing
                               NULL,               --default_bill_to_loc
                               NULL,               --bill_to_addr_type
                               I_f_order_head_rec.comments,
                               decode(I_f_order_head_rec.status, 'A', sysdate, NULL),
                               sysdate,
                               get_user,
                               sysdate,
                               get_user);
                                                             
   O_wf_order_no := L_wf_order_no;
   
   return TRUE;        
                       
EXCEPTION
   when OTHERS then
      if C_GET_ALL_LOC_SAME_CURR%ISOPEN then
         close C_GET_ALL_LOC_SAME_CURR;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_HEADER;
----------------------------------------------------------------------------------
FUNCTION MODIFY_HEADER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN IS   
   L_program          VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.MODIFY_HEADER';
   L_table            VARCHAR2(50) := 'WF_ORDER_HEAD';
   L_key              VARCHAR2(64);
   
   L_existing_status  WF_ORDER_HEAD.STATUS%TYPE;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_WF_ORDER_HEAD is
      select status
        from wf_order_head
       where wf_order_no = I_f_order_head_rec.wf_order_no
         for update nowait;
   
BEGIN

   L_key := 'wf_order_no: '||to_char(I_f_order_head_rec.wf_order_no);
   
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_HEAD', L_table, L_key);
   open C_LOCK_WF_ORDER_HEAD;
   
   SQL_LIB.SET_MARK('FETCH', 'C_LOCK_WF_ORDER_HEAD', L_table, L_key);
   fetch C_LOCK_WF_ORDER_HEAD into L_existing_status;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_HEAD', L_table, L_key);
   close C_LOCK_WF_ORDER_HEAD;
   
   --At the time of approval, reapply the cost to account for any change in the
   --cost due to cost template change or due to the change in costing location
   if L_existing_status in ('I', 'R') and I_f_order_head_rec.status = 'A' then
      if WF_ORDER_SQL.REAPPLY_CUSTOMER_COST(O_error_message,
                                            I_f_order_head_rec.wf_order_no) = FALSE then
         return FALSE;
      end if;
   end if;
   
   --Do not update the status if the existing status is 'In Progress'. Franchise order will
   --be closed by wfordcls batch.
   update wf_order_head
      set status                = decode(status, 'P', status, I_f_order_head_rec.status),      
          comments              = I_f_order_head_rec.comments,
          approval_date         = case when L_existing_status in ('I', 'R')
                                            and I_f_order_head_rec.status = 'A' then sysdate
                                       else approval_date
                                  end,                               
          last_update_datetime  = sysdate,
          last_update_id        = get_user
    where wf_order_no = I_f_order_head_rec.wf_order_no;
    
   return TRUE;
    
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_ORDER_HEAD%ISOPEN then
         close C_LOCK_WF_ORDER_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_WF_ORDER_HEAD%ISOPEN then
         close C_LOCK_WF_ORDER_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MODIFY_HEADER;
----------------------------------------------------------------------------------
FUNCTION CANCEL_F_ORDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.CANCEL_F_ORDER';
   L_table            VARCHAR2(50) := 'WF_ORDER_HEAD';
   L_key              VARCHAR2(64);
   
   L_cancel_status    VARCHAR2(1)  := 'C';
   
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_WF_ORDER_HEAD is
      select 'x'
        from wf_order_head
       where wf_order_no = I_f_order_head_rec.wf_order_no
         for update nowait;
BEGIN

   L_key := 'wf_order_no: '||to_char(I_f_order_head_rec.wf_order_no);
   
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_HEAD', L_table, L_key);
   open C_LOCK_WF_ORDER_HEAD;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_HEAD', L_table, L_key);
   close C_LOCK_WF_ORDER_HEAD;
   
   update wf_order_head
      set status               = L_cancel_status,
          cancel_reason        = I_f_order_head_rec.cancel_reason,
          last_update_datetime = sysdate,
          last_update_id       = get_user
    where wf_order_no = I_f_order_head_rec.wf_order_no;
   
   return TRUE;
    
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_ORDER_HEAD%ISOPEN then
         close C_LOCK_WF_ORDER_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_WF_ORDER_HEAD%ISOPEN then
         close C_LOCK_WF_ORDER_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CANCEL_F_ORDER;
----------------------------------------------------------------------------------
FUNCTION CREATE_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC,
                       I_action_type          IN       VARCHAR2,
                       I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.CREATE_DETAIL';
   L_table                      VARCHAR2(50);
   L_key                        VARCHAR2(100);
   
   L_wf_order_no                WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_item                       WF_ORDER_DETAIL.ITEM%TYPE;
   L_source_loc_type            WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_source_loc_id              WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_customer_loc               WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_requested_qty              WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_need_date                  WF_ORDER_DETAIL.NEED_DATE%TYPE;
   L_not_after_date             WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE;
   L_wf_order_line_no           WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_templ_id                   WF_COST_RELATIONSHIP.TEMPL_ID%TYPE;
   L_templ_desc                 WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE;
   L_margin_pct                 WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE;
   L_cust_cost                  FUTURE_COST.PRICING_COST%TYPE;
   L_acqui_cost                 FUTURE_COST.ACQUISITION_COST%TYPE;
   L_calc_type                  WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE;
   L_future_cost_curr           FUTURE_COST.CURRENCY_CODE%TYPE;
   L_wf_order_curr              WF_ORDER_HEAD.CURRENCY_CODE%TYPE;
   L_approval_date              DATE;
   L_cust_cost_order_curr       FUTURE_COST.PRICING_COST%TYPE;
   L_acqui_cost_order_curr      FUTURE_COST.ACQUISITION_COST%TYPE;
   L_cre_f_ord_detail           OBJ_F_ORDER_DETAIL_TBL;
   L_fixed_cost                 WF_ORDER_DETAIL.FIXED_COST%TYPE;
         
   --Fetch the records to be inserted into wf_order_detail
   cursor C_CREATE_DETAIL_RECS is
      select OBJ_F_ORDER_DETAIL_REC( wodt.wf_order_no,
                                     wodt.item,
                                     wodt.source_loc_type,
                                     wodt.source_loc_id,
                                     wodt.customer_loc,
                                     wodt.requested_qty,
                                     wodt.cancel_reason,
                                     wodt.need_date,
                                     wodt.not_after_date)
        from TABLE(CAST(I_f_order_head_rec.f_order_details as OBJ_F_ORDER_DETAIL_TBL)) wodt,
             item_master im
       where wodt.item = im.item
         and NVL(im.deposit_item_type, '0') <> 'A' 
         and not exists(select '1'
                          from wf_order_detail wod
                         where wf_order_no      = wodt.wf_order_no
                           and item             = wodt.item
                           and source_loc_type  = wodt.source_loc_type
                           and source_loc_id    = wodt.source_loc_id
                           and customer_loc     = wodt.customer_loc
                           and wf_order_no      = L_wf_order_no);
                                  
   cursor C_CURR_WF_ORDER_LINE_NO is
      select NVL(MAX(wf_order_line_no), 0)
        from wf_order_detail
       where wf_order_no = L_wf_order_no;
          
   --Fetch the Franchise order currency for the currency conversion of order cost                               
   cursor C_WF_ORDER_CURR is
      select currency_code,
             approval_date
        from wf_order_head
       where wf_order_no = L_wf_order_no;   
   
   -- Special handling for transfer inititated update request. 
   -- For manual/edi franchise order resulting in store orders, the store order processing will 
   -- create a new transfer which may have a different source warehouse (repl_item_loc.source_wh) 
   -- than the original order source location. The wf_order_detail record will be written for this
   -- location with requested qty = 0 but the pricing details will be copied from the original record
   -- to post stockledger cost/retail based on original order cost.
   cursor C_GET_ORIG_ORD_LINE_COST is
      select wod.templ_id,
             wod.templ_desc,
             wod.margin_pct,
             wod.calc_type,
             wod.customer_cost,
             wod.acquisition_cost,
             wod.fixed_cost
        from wf_order_detail wod,
             tsfhead th,
             store_orders so
       where I_tsf_no is NOT NULL
         and th.tsf_no             =   I_tsf_no
         and th.wf_order_no is NOT NULL
         and th.create_id          =   'BATCH'
         and so.wf_order_no        =   L_wf_order_no
         and so.item               =   L_item
         and so.store              =   L_customer_loc
         and so.need_date          =   L_need_date
         and so.processed_date is NOT NULL
         and th.wf_order_no        =   so.wf_order_no
         and th.wf_need_date       =   so.need_date
         and wod.wf_order_no       =   so.wf_order_no
         and wod.wf_order_line_no  =   so.wf_order_line_no;                                                            
BEGIN
   
   L_wf_order_no := I_f_order_head_rec.wf_order_no;
   L_key := 'wf_order_no: '||to_char(L_wf_order_no);
   L_table := 'WF_ORDER_DETAIL';
   
   SQL_LIB.SET_MARK('OPEN', 'C_CREATE_DETAIL_RECS', L_table, L_key);
   open C_CREATE_DETAIL_RECS;
   
   SQL_LIB.SET_MARK('FETCH', 'C_CREATE_DETAIL_RECS', L_table, L_key);
   fetch C_CREATE_DETAIL_RECS BULK COLLECT into L_cre_f_ord_detail;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_CREATE_DETAIL_RECS', L_table, L_key);
   close C_CREATE_DETAIL_RECS;
   
   if L_cre_f_ord_detail is NOT NULL and L_cre_f_ord_detail.COUNT > 0 then
      L_table := 'WF_ORDER_HEAD';
      
      --Fetch the order currency code
      SQL_LIB.SET_MARK('OPEN', 'C_WF_ORDER_CURR', L_table, L_key);
      open C_WF_ORDER_CURR;
      
      SQL_LIB.SET_MARK('FETCH', 'C_WF_ORDER_CURR', L_table, L_key);
      fetch C_WF_ORDER_CURR into L_wf_order_curr, L_approval_date;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_WF_ORDER_CURR', L_table, L_key);
      close C_WF_ORDER_CURR;
      
      L_table := 'WF_ORDER_DETAIL';
      
      --Fetch the maximum wf_order_line_no
      SQL_LIB.SET_MARK('OPEN', 'C_CURR_WF_ORDER_LINE_NO', L_table, L_key);
      open C_CURR_WF_ORDER_LINE_NO;
               
      SQL_LIB.SET_MARK('FETCH', 'C_CURR_WF_ORDER_LINE_NO', L_table, L_key);
      fetch C_CURR_WF_ORDER_LINE_NO into L_wf_order_line_no;
              
      SQL_LIB.SET_MARK('CLOSE', 'C_CURR_WF_ORDER_LINE_NO', L_table, L_key);
      close C_CURR_WF_ORDER_LINE_NO;
      
      FOR i in L_cre_f_ord_detail.FIRST..L_cre_f_ord_detail.LAST LOOP
         L_item                  := L_cre_f_ord_detail(i).item;
         L_source_loc_type       := L_cre_f_ord_detail(i).source_loc_type;
         L_source_loc_id         := L_cre_f_ord_detail(i).source_loc_id;
         L_customer_loc          := L_cre_f_ord_detail(i).customer_loc;
         L_requested_qty         := L_cre_f_ord_detail(i).requested_qty;
         L_need_date             := L_cre_f_ord_detail(i).need_date;
         L_not_after_date        := L_cre_f_ord_detail(i).not_after_date;
         -- Initialize the values to null to avoid retaining the values in the loop
         L_fixed_cost            := NULL;
         L_templ_id              := NULL;
         L_templ_desc            := NULL;
         L_margin_pct            := NULL;
         L_calc_type             := NULL;
         L_cust_cost_order_curr  := NULL;
         L_acqui_cost_order_curr := NULL;
         L_fixed_cost            := NULL;
         L_cust_cost             := NULL;
         L_acqui_cost            := NULL;  
         L_future_cost_curr      := NULL;  
         
         --Call wf_order_sql to fetch the cost information from future cost and 
         --also get the cost template information from wf_cost_buildup_tmpl_head
         --and wf_cost_relationship
         if WF_ORDER_SQL.GET_COST_INFORMATION(O_error_message,
                                              L_templ_id,
                                              L_templ_desc,
                                              L_margin_pct,
                                              L_calc_type,
                                              L_cust_cost,
                                              L_acqui_cost,
                                              L_future_cost_curr,
                                              L_item,
                                              L_customer_loc,
                                              L_source_loc_type,
                                              L_source_loc_id)= FALSE then
            return FALSE;
         end if;
            
         if L_future_cost_curr != L_wf_order_curr then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_cust_cost,
                                    L_future_cost_curr,
                                    L_wf_order_curr,
                                    L_cust_cost_order_curr,
                                    'C',    --cost_retail_ind
                                    L_approval_date,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_acqui_cost,
                                    L_future_cost_curr,
                                    L_wf_order_curr,
                                    L_acqui_cost_order_curr,
                                    'C',   --cost_retail_ind
                                    L_approval_date,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
         else
            L_cust_cost_order_curr := L_cust_cost;
            L_acqui_cost_order_curr := L_acqui_cost;
         end if;
         
         L_table := 'WF_ORDER_DETAIL, TSFHEAD, TSFDETAIL';
         
         SQL_LIB.SET_MARK('OPEN', 'C_GET_ORIG_ORD_LINE_COST', L_table, L_key);
         open C_GET_ORIG_ORD_LINE_COST;
         
         SQL_LIB.SET_MARK('FETCH', 'C_GET_ORIG_ORD_LINE_COST', L_table, L_key);
         fetch C_GET_ORIG_ORD_LINE_COST into L_templ_id,
                                             L_templ_desc,
                                             L_margin_pct,
                                             L_calc_type,
                                             L_cust_cost_order_curr,
                                             L_acqui_cost_order_curr,
                                             L_fixed_cost;
                                             
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORIG_ORD_LINE_COST', L_table, L_key);                                             
         close C_GET_ORIG_ORD_LINE_COST;                                             
            
         L_table := 'WF_ORDER_DETAIL';
         
         SQL_LIB.SET_MARK('INSERT', NULL, L_table, L_key);
         --Post approval insert the record with requested_qty = 0
         insert into wf_order_detail (wf_order_no,
                                      wf_order_line_no,
                                      item,
                                      source_loc_type,
                                      source_loc_id,
                                      customer_loc,
                                      requested_qty,
                                      cancel_reason,
                                      need_date,
                                      not_after_date,
                                      acquisition_cost,
                                      customer_cost,
                                      fixed_cost,
                                      templ_id,
                                      margin_pct,
                                      calc_type,
                                      templ_desc,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
                              (select L_wf_order_no,
                                      NVL(L_wf_order_line_no, 0) + i,
                                      L_item,
                                      L_source_loc_type,
                                      L_source_loc_id,
                                      L_customer_loc,
                                      case when woh.status in ('I', 'R') 
                                                or (woh.status = 'A' 
                                                    and I_action_type = LP_create_type) then L_requested_qty
                                           else 0
                                      end,                    --requested_qty
                                      NULL,                   --cancel_reason
                                      L_need_date,
                                      L_not_after_date,
                                      L_acqui_cost_order_curr,
                                      L_cust_cost_order_curr,
                                      L_fixed_cost,
                                      L_templ_id,
                                      L_margin_pct,
                                      L_calc_type,
                                      L_templ_desc,
                                      sysdate,
                                      get_user,
                                      sysdate,
                                      get_user
                                 from wf_order_head woh
                                where wf_order_no = L_wf_order_no); 
                                    
         --Call wf_order_sql to build the expenses for the Franchise order
         --This will insert the records into wf_order_exp                                    
         if WF_ORDER_SQL.GET_UPCHARGE_INFORMATION(O_error_message,
                                                  L_wf_order_no,
                                                  L_wf_order_curr,
                                                  L_item,
                                                  L_source_loc_type,
                                                  L_source_loc_id,
                                                  L_customer_loc) = FALSE then
            return FALSE;
         end if;                                                           
      END LOOP;
   end if; --if L_cre_f_order_detail is NOT NULL
   
   return TRUE;
       
EXCEPTION
   when OTHERS then
      if C_CREATE_DETAIL_RECS%ISOPEN then
         close C_CREATE_DETAIL_RECS;
      end if;
      if C_CURR_WF_ORDER_LINE_NO%ISOPEN then
         close C_CURR_WF_ORDER_LINE_NO;
      end if;
      if C_WF_ORDER_CURR%ISOPEN then
         close C_WF_ORDER_CURR;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_DETAIL;
----------------------------------------------------------------------------------
FUNCTION MODIFY_DETAIL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_f_order_head_rec    IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN IS
   L_program               VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.MODIFY_DETAIL';
   L_table                 VARCHAR2(50) := 'WF_ORDER_DETAIL';
   
   L_wf_order_no           WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_upd_f_ord_detail      OBJ_F_ORDER_DETAIL_TBL;
   
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   --Fetch the records for modifying wf_order_detail         
   cursor C_UPDATE_DETAIL_RECS is
      select OBJ_F_ORDER_DETAIL_REC( wodt.wf_order_no,
                                     wodt.item,
                                     wodt.source_loc_type,
                                     wodt.source_loc_id,
                                     wodt.customer_loc,
                                     wodt.requested_qty,
                                     wodt.cancel_reason,
                                     wodt.need_date,
                                     wodt.not_after_date)
        from TABLE(CAST(I_f_order_head_rec.f_order_details as OBJ_F_ORDER_DETAIL_TBL)) wodt,
             wf_order_detail wod,
             item_master im
       where wod.wf_order_no      = wodt.wf_order_no
         and wod.item             = wodt.item
         and wod.source_loc_type  = wodt.source_loc_type
         and wod.source_loc_id    = wodt.source_loc_id
         and wod.customer_loc     = wodt.customer_loc
         and wod.wf_order_no      = L_wf_order_no
         and wod.item             = im.item
         and NVL(im.deposit_item_type, '0') <> 'A'
         for update of wod.requested_qty nowait;       
BEGIN
   
   L_wf_order_no := I_f_order_head_rec.wf_order_no;
   
   SQL_LIB.SET_MARK('OPEN', 'C_UPDATE_DETAIL_RECS', L_table, to_char(L_wf_order_no));
   open C_UPDATE_DETAIL_RECS;
   
   SQL_LIB.SET_MARK('FETCH', 'C_UPDATE_DETAIL_RECS', L_table, to_char(L_wf_order_no));
   fetch C_UPDATE_DETAIL_RECS BULK COLLECT INTO L_upd_f_ord_detail;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_UPDATE_DETAIL_RECS', L_table, to_char(L_wf_order_no));
   close C_UPDATE_DETAIL_RECS;
   
   if L_upd_f_ord_detail is NOT NULL and L_upd_f_ord_detail.COUNT > 0 then
   
      SQL_LIB.SET_MARK('UPDATE', NULL, L_table, NULL);
      FORALL i in L_upd_f_ord_detail.FIRST..L_upd_f_ord_detail.LAST
         update wf_order_detail
            set requested_qty         = L_upd_f_ord_detail(i).requested_qty,
                need_date             = L_upd_f_ord_detail(i).need_date,
                not_after_date        = L_upd_f_ord_detail(i).not_after_date,
                last_update_datetime  = sysdate,
                last_update_id        = get_user
          where wf_order_no      = L_wf_order_no
            and item             = L_upd_f_ord_detail(i).item
            and source_loc_type  = L_upd_f_ord_detail(i).source_loc_type
            and source_loc_id    = L_upd_f_ord_detail(i).source_loc_id
            and customer_loc     = L_upd_f_ord_detail(i).customer_loc;
   end if;   
   
   return TRUE;          
   
EXCEPTION
   when RECORD_LOCKED then
      if C_UPDATE_DETAIL_RECS%ISOPEN then
         close C_UPDATE_DETAIL_RECS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_UPDATE_DETAIL_RECS%ISOPEN then
         close C_UPDATE_DETAIL_RECS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MODIFY_DETAIL;
----------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_f_order_head_rec     IN       OBJ_F_ORDER_HEAD_REC)
RETURN BOOLEAN IS   
   L_program                VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.DELETE_DETAIL';
   L_table                  VARCHAR2(50);
   
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   L_wf_order_no            WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_exp_rowid_tbl          ROWID_TBL;
   L_audit_rowid_tbl        ROWID_TBL;
   L_del_f_ord_detail       OBJ_F_ORDER_DETAIL_TBL;
   L_existing_status        WF_ORDER_HEAD.STATUS%TYPE;
                     
   --Fetch the records to be deleted from wf_order_detail         
   cursor C_DELETE_DETAIL_RECS is
      select OBJ_F_ORDER_DETAIL_REC( wod.wf_order_no,
                                     wod.item,
                                     wod.source_loc_type,
                                     wod.source_loc_id,
                                     wod.customer_loc,
                                     wod.requested_qty,
                                     wod.cancel_reason,
                                     wod.need_date,
                                     wod.not_after_date)
        from wf_order_detail wod,
             item_master im
       where wf_order_no = L_wf_order_no
         and wod.item = im.item
         and NVL(im.deposit_item_type, '0') <> 'A'            --Filter the deposit container items since these will be processed by SYNC_DEPOSIT_ITEMS
         and not exists(select '1'
                          from TABLE(CAST(I_f_order_head_rec.f_order_details as OBJ_F_ORDER_DETAIL_TBL)) wodt
                         where wf_order_no      = wod.wf_order_no
                           and item             = wod.item
                           and source_loc_type  = wod.source_loc_type
                           and source_loc_id    = wod.source_loc_id
                           and customer_loc     = wod.customer_loc)
         for update of wod.wf_order_no nowait;
   
   cursor C_LOCK_WF_ORDER_AUDIT is
      select woa.rowid
        from wf_order_audit woa,
             wf_order_detail wod,
             TABLE(CAST (L_del_f_ord_detail as OBJ_F_ORDER_DETAIL_TBL)) wodt
       where wod.wf_order_no      = wodt.wf_order_no
         and wod.wf_order_no      = L_wf_order_no
         and wod.item             = wodt.item
         and wod.source_loc_type  = wodt.source_loc_type
         and wod.source_loc_id    = wodt.source_loc_id
         and wod.customer_loc     = wodt.customer_loc
         and wod.wf_order_no      = woa.wf_order_no
         and wod.wf_order_line_no = woa.wf_order_line_no
         for update of woa.wf_order_no nowait;  
                                              
   cursor C_LOCK_WF_ORDER_EXP is
      select woe.rowid
        from wf_order_exp woe,
             wf_order_detail wod,
             TABLE(CAST (L_del_f_ord_detail as OBJ_F_ORDER_DETAIL_TBL)) wodt
       where wod.wf_order_no      = wodt.wf_order_no
         and wod.wf_order_no      = L_wf_order_no
         and wod.item             = wodt.item
         and wod.source_loc_type  = wodt.source_loc_type
         and wod.source_loc_id    = wodt.source_loc_id
         and wod.customer_loc     = wodt.customer_loc
         and wod.wf_order_no      = woe.wf_order_no
         and wod.wf_order_line_no = woe.wf_order_line_no
         for update of woe.wf_order_no nowait;                  
                
BEGIN
   
   L_wf_order_no := I_f_order_head_rec.wf_order_no; 
   L_table := 'WF_ORDER_DETAIL ' || ', ITEM_MASTER';
   
   SQL_LIB.SET_MARK('OPEN', 'C_DELETE_DETAIL_RECS', L_table, to_char(L_wf_order_no));
   open C_DELETE_DETAIL_RECS;
   
   SQL_LIB.SET_MARK('FETCH', 'C_DELETE_DETAIL_RECS', L_table, to_char(L_wf_order_no));
   fetch C_DELETE_DETAIL_RECS BULK COLLECT into L_del_f_ord_detail;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_DELETE_DETAIL_RECS', L_table, to_char(L_wf_order_no));
   close C_DELETE_DETAIL_RECS;
   
   if L_del_f_ord_detail is NOT NULL and L_del_f_ord_detail.COUNT > 0 then
      
      L_table := 'WF_ORDER_EXP';
      
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_EXP', L_table, to_char(L_wf_order_no));
      open C_LOCK_WF_ORDER_EXP;
   
      SQL_LIB.SET_MARK('FETCH', 'C_LOCK_WF_ORDER_EXP', L_table, to_char(L_wf_order_no));
      fetch C_LOCK_WF_ORDER_EXP BULK COLLECT INTO L_exp_rowid_tbl;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_EXP', L_table, to_char(L_wf_order_no));
      close C_LOCK_WF_ORDER_EXP;
      
      if L_exp_rowid_tbl is NOT NULL and L_exp_rowid_tbl.COUNT > 0 then
         SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
         FORALL i in L_exp_rowid_tbl.FIRST..L_exp_rowid_tbl.LAST
            delete from wf_order_exp
             where rowid = L_exp_rowid_tbl(i); 
      end if;
      
      L_table := 'WF_ORDER_AUDIT';
      
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_AUDIT', L_table, to_char(L_wf_order_no));
      open C_LOCK_WF_ORDER_AUDIT;
   
      SQL_LIB.SET_MARK('FETCH', 'C_LOCK_WF_ORDER_AUDIT', L_table, to_char(L_wf_order_no));
      fetch C_LOCK_WF_ORDER_AUDIT BULK COLLECT INTO L_audit_rowid_tbl;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_AUDIT', L_table, to_char(L_wf_order_no));
      close C_LOCK_WF_ORDER_AUDIT;
      
      if L_audit_rowid_tbl is NOT NULL and L_audit_rowid_tbl.COUNT > 0 then
         SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
         FORALL i in L_audit_rowid_tbl.FIRST..L_audit_rowid_tbl.LAST
            delete from wf_order_audit
             where rowid = L_audit_rowid_tbl(i); 
      end if;
      
      L_table := 'WF_ORDER_DETAIL';
      
      SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
      FORALL i in L_del_f_ord_detail.FIRST..L_del_f_ord_detail.LAST
         delete from wf_order_detail
          where wf_order_no       = L_wf_order_no
            and item              = L_del_f_ord_detail(i).item
            and source_loc_type   = L_del_f_ord_detail(i).source_loc_type
            and source_loc_id     = L_del_f_ord_detail(i).source_loc_id
            and customer_loc      = L_del_f_ord_detail(i).customer_loc;      
   end if;
   
   return TRUE;
      
EXCEPTION
   when RECORD_LOCKED then
      if C_DELETE_DETAIL_RECS%ISOPEN then
         close C_DELETE_DETAIL_RECS;
      end if;
      if C_LOCK_WF_ORDER_EXP%ISOPEN then
         close C_LOCK_WF_ORDER_EXP;
      end if;
      if C_LOCK_WF_ORDER_AUDIT%ISOPEN then
         close C_LOCK_WF_ORDER_AUDIT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_DELETE_DETAIL_RECS%ISOPEN then
         close C_DELETE_DETAIL_RECS;
      end if;
      if C_LOCK_WF_ORDER_EXP%ISOPEN then
         close C_LOCK_WF_ORDER_EXP;
      end if;
      if C_LOCK_WF_ORDER_AUDIT%ISOPEN then
         close C_LOCK_WF_ORDER_AUDIT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_DETAIL;
----------------------------------------------------------------------------------
FUNCTION DELETE_F_ORDER(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_wf_order_no          IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS   
   L_program             VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.DELETE_F_ORDER';
   L_table               VARCHAR2(50);
   
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_WF_ORDER_AUDIT is         
      select 'x'
        from wf_order_audit
       where wf_order_no = I_wf_order_no
         for update nowait;
         
   cursor C_LOCK_WF_ORDER_EXP is         
      select 'x'
        from wf_order_exp
       where wf_order_no = I_wf_order_no
         for update nowait;
         
   cursor C_LOCK_WF_ORDER_DETAIL is         
      select 'x'
        from wf_order_detail
       where wf_order_no = I_wf_order_no
         for update nowait;
         
   cursor C_LOCK_WF_ORDER_HEAD is         
      select 'x'
        from wf_order_head
       where wf_order_no = I_wf_order_no
         for update nowait;         
                
BEGIN
   
   L_table := 'WF_ORDER_AUDIT';
   
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_AUDIT', L_table, I_wf_order_no);
   open C_LOCK_WF_ORDER_AUDIT;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_AUDIT', L_table, I_wf_order_no);
   close C_LOCK_WF_ORDER_AUDIT;
   
   delete from wf_order_audit
         where wf_order_no = I_wf_order_no;
         
   L_table := 'WF_ORDER_EXP';
   
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_EXP', L_table, I_wf_order_no);
   open C_LOCK_WF_ORDER_EXP;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_EXP', L_table, I_wf_order_no);
   close C_LOCK_WF_ORDER_EXP;
   
   delete from wf_order_exp
         where wf_order_no = I_wf_order_no;
   
   L_table := 'WF_ORDER_DETAIL';  
   
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_DETAIL', L_table, I_wf_order_no);       
   open C_LOCK_WF_ORDER_DETAIL;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_DETAIL', L_table, I_wf_order_no);
   close C_LOCK_WF_ORDER_DETAIL;
   
   delete from wf_order_detail
         where wf_order_no = I_wf_order_no;
         
   L_table := 'WF_ORDER_HEAD';
   
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_HEAD', L_table, I_wf_order_no);         
   open C_LOCK_WF_ORDER_HEAD;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_HEAD', L_table, I_wf_order_no);
   close C_LOCK_WF_ORDER_HEAD;
   
   delete from wf_order_head
         where wf_order_no = I_wf_order_no;               
   
   return TRUE;
      
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_ORDER_AUDIT%ISOPEN then
         close C_LOCK_WF_ORDER_AUDIT;
      end if;
      if C_LOCK_WF_ORDER_EXP%ISOPEN then
         close C_LOCK_WF_ORDER_EXP;
      end if;
      if C_LOCK_WF_ORDER_DETAIL%ISOPEN then
         close C_LOCK_WF_ORDER_DETAIL;
      end if;
      if C_LOCK_WF_ORDER_HEAD%ISOPEN then
         close C_LOCK_WF_ORDER_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_wf_order_no,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_WF_ORDER_AUDIT%ISOPEN then
         close C_LOCK_WF_ORDER_AUDIT;
      end if;
      if C_LOCK_WF_ORDER_EXP%ISOPEN then
         close C_LOCK_WF_ORDER_EXP;
      end if;
      if C_LOCK_WF_ORDER_DETAIL%ISOPEN then
         close C_LOCK_WF_ORDER_DETAIL;
      end if;
      if C_LOCK_WF_ORDER_HEAD%ISOPEN then
         close C_LOCK_WF_ORDER_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_F_ORDER;
----------------------------------------------------------------------------------
FUNCTION SYNC_DEPOSIT_ITEMS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wf_order_no          IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                            I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS   
   L_program                VARCHAR2(64) := 'WF_CREATE_ORDER_SQL.SYNC_DEPOSIT_ITEMS';
   L_table                  VARCHAR2(50);
   
   L_exp_rowid_tbl          ROWID_TBL;
   L_audit_rowid_tbl        ROWID_TBL;
   L_containers_to_del      OBJ_F_ORDER_DETAIL_TBL;
   L_containers_to_upd      OBJ_F_ORDER_DETAIL_TBL;
   L_item_loc_exists        BOOLEAN;
   L_templ_id               WF_COST_RELATIONSHIP.TEMPL_ID%TYPE;
   L_templ_desc             WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE;
   L_margin_pct             WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE;
   L_cust_cost              FUTURE_COST.PRICING_COST%TYPE;
   L_acqui_cost             FUTURE_COST.ACQUISITION_COST%TYPE;
   L_calc_type              WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE;
   L_currency_code          FUTURE_COST.CURRENCY_CODE%TYPE;
   L_wf_order_curr          WF_ORDER_HEAD.CURRENCY_CODE%TYPE;
   L_cust_cost_order_curr   FUTURE_COST.PRICING_COST%TYPE;
   L_acqui_cost_order_curr  FUTURE_COST.ACQUISITION_COST%TYPE;
   L_wf_order_line_no       WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_existing_status        WF_ORDER_HEAD.STATUS%TYPE;
   L_approval_date          DATE;
   L_container_item         WF_ORDER_DETAIL.ITEM%TYPE;
   L_customer_loc           WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);
         
   cursor C_LOCK_WF_ORDER_DETAIL is 
      select 'x'
        from wf_order_detail wod,
             item_master im
       where wod.wf_order_no = I_wf_order_no
         and wod.item = im.item
         and NVL(im.deposit_item_type, '0') = 'A'
         for update of wod.requested_qty nowait;
         
   cursor C_LOCK_WF_ORDER_EXP is 
      select woe.rowid
        from wf_order_exp woe,
             wf_order_detail wod,
             TABLE(CAST(L_containers_to_del as OBJ_F_ORDER_DETAIL_TBL)) wodt
       where wod.wf_order_no      =  I_wf_order_no
         and wod.wf_order_no      =  wodt.wf_order_no
         and wod.item             =  wodt.item
         and wod.source_loc_type  =  wodt.source_loc_type
         and wod.source_loc_id    =  wodt.source_loc_id
         and wod.customer_loc     =  wodt.customer_loc
         and woe.wf_order_no      =  wod.wf_order_no
         and woe.wf_order_line_no =  wod.wf_order_line_no
         for update of woe.wf_order_no nowait;
         
   cursor C_LOCK_WF_ORDER_AUDIT is 
      select woa.rowid
        from wf_order_audit woa,
             wf_order_detail wod,
             TABLE(CAST(L_containers_to_del as OBJ_F_ORDER_DETAIL_TBL)) wodt
       where wod.wf_order_no      =  I_wf_order_no
         and wod.wf_order_no      =  wodt.wf_order_no
         and wod.item             =  wodt.item
         and wod.source_loc_type  =  wodt.source_loc_type
         and wod.source_loc_id    =  wodt.source_loc_id
         and wod.customer_loc     =  wodt.customer_loc
         and woa.wf_order_no      =  wod.wf_order_no
         and woa.wf_order_line_no =  wod.wf_order_line_no
         for update of woa.wf_order_no nowait;
                  
   --Fetch the deposit container items to be deleted from the Franchise order
   --if the linked deposit content items are removed       
   cursor C_GET_CONTAINERS_TO_DEL is
      select OBJ_F_ORDER_DETAIL_REC(wf_order_no,
                                    wod.item,
                                    source_loc_type,
                                    source_loc_id,
                                    customer_loc,
                                    requested_qty,
                                    cancel_reason,
                                    need_date,
                                    not_after_date)
        from wf_order_detail wod,
             item_master im
       where wod.wf_order_no = I_wf_order_no
         and wod.item        = im.item
         and NVL(im.deposit_item_type, '0') = 'A'
         and not exists (select '1'
                           from wf_order_detail wod2,
                                item_master im2
                          where wod2.wf_order_no     = I_wf_order_no
                            and wod2.item            = im2.item
                            and wod2.source_loc_type = wod.source_loc_type
                            and wod2.source_loc_id   = wod.source_loc_id
                            and wod2.customer_loc    = wod.customer_loc
                            and NVL(im2.deposit_item_type, '0') = 'E'
                            and NVL(im2.container_item,'-999')  = im.item);
   
   --Fetch the deposit container items to be updated in the Franchise order
   --if the linked deposit content items are updated  
   cursor C_GET_CONTAINERS_TO_UPD is
      select OBJ_F_ORDER_DETAIL_REC(wf_order_no,
                                    item,
                                    source_loc_type,
                                    source_loc_id,
                                    customer_loc,
                                    requested_qty,
                                    cancel_reason,
                                    need_date,
                                    not_after_date)
        from (select wf_order_no,
                     im.container_item item,
                     source_loc_type,
                     source_loc_id,
                     customer_loc,
                     SUM(requested_qty) requested_qty,
                     MAX(cancel_reason) cancel_reason,
                     MAX(need_date) need_date,
                     MAX(not_after_date) not_after_date
                from wf_order_detail wod,
                     item_master im
               where wod.wf_order_no = I_wf_order_no
                 and wod.item        = im.item
                 and NVL(im.deposit_item_type, '0') = 'E'
            group by wf_order_no, source_loc_type, source_loc_id, customer_loc, im.container_item);
              
   --Fetch the deposit container items to be inserted into the Franchise order
   --if the linked deposit content items are added to the order                                                          
   cursor C_GET_CONTAINERS_TO_INS is
      select wod.wf_order_no,
             im.container_item item,
             wod.source_loc_type,
             wod.source_loc_id,
             wod.customer_loc,
             SUM(wod.requested_qty) requested_qty,
             MAX(wod.cancel_reason) cancel_reason,
             MAX(wod.need_date) need_date,
             MAX(wod.not_after_date) not_after_date,
             MAX(il1.ranged_ind) to_ranged_ind,
             MAX(il2.ranged_ind) from_ranged_ind
       from wf_order_detail wod,
            item_master im,
            item_loc il1,
            item_loc il2
      where wod.wf_order_no = I_wf_order_no
        and wod.item        = im.item
        and NVL(im.deposit_item_type,'0') = 'E'
        and il1.item         = wod.item
        and il1.loc          = wod.customer_loc
        and il2.item(+)      = wod.item
        and il2.loc(+)       = wod.source_loc_id    --outer join to source location since source_loc_type can be 'SU'
        and il2.loc_type(+)  = decode(wod.source_loc_type, 'ST', 'S', 'WH', 'W')
        and not exists(select '1'
                         from wf_order_detail wod2
                        where wod2.wf_order_no     = wod.wf_order_no
                          and wod2.item            = im.container_item
                          and wod2.source_loc_type = wod.source_loc_type
                          and wod2.source_loc_id   = wod.source_loc_id
                          and wod2.customer_loc    = wod.customer_loc)
   group by wf_order_no, source_loc_type, source_loc_id, customer_loc, im.container_item;  
   
   --Get the Franchise order currency code which will be used
   --to perform currency conversion of order cost      
   cursor C_WF_ORDER_CURR is
      select currency_code,
             approval_date
        from wf_order_head
       where wf_order_no = I_wf_order_no;    
       
   cursor C_CURR_WF_ORDER_LINE_NO is
      select NVL(MAX(wf_order_line_no), 0)
        from wf_order_detail
       where wf_order_no = I_wf_order_no;

   -- Copy an existing container cost for content row created becuase of transfer created because of franchise order initiated
   -- store order. The container item cost detail is from the future_cost and hence copy any container item cost for store
   -- or warehouse sourced.
   cursor C_GET_ORIG_ORD_LINE_COST is
      select wod.templ_id,
             wod.templ_desc,
             wod.margin_pct,
             wod.calc_type,
             wod.customer_cost,
             wod.acquisition_cost
        from wf_order_detail wod,
             tsfhead th
       where wod.item = L_container_item
         and wod.customer_loc = L_customer_loc
         and wod.source_loc_type in ('WH','ST')
         and wod.wf_order_no = I_wf_order_no
         and th.tsf_no = I_tsf_no
         and th.create_id = 'BATCH'
         and th.wf_order_no = wod.wf_order_no
         and rownum = 1;
   
   TYPE TBL_CONTAINERS_TO_INS is TABLE of C_GET_CONTAINERS_TO_INS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_containers_to_ins TBL_CONTAINERS_TO_INS;
                                                
BEGIN
   
   L_table := 'WF_ORDER_DETAIL, '||'ITEM_MASTER';
   
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_DETAIL', L_table, to_char(I_wf_order_no));
   open C_LOCK_WF_ORDER_DETAIL;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_DETAIL', L_table, to_char(I_wf_order_no));  
   close C_LOCK_WF_ORDER_DETAIL;
   
   --Fetch the container items to be deleted
   SQL_LIB.SET_MARK('OPEN', 'C_GET_CONTAINERS_TO_DEL', L_table, to_char(I_wf_order_no));
   open C_GET_CONTAINERS_TO_DEL;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_CONTAINERS_TO_DEL', L_table, to_char(I_wf_order_no));
   fetch C_GET_CONTAINERS_TO_DEL BULK COLLECT INTO L_containers_to_del;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_CONTAINERS_TO_DEL', L_table, to_char(I_wf_order_no));
   close C_GET_CONTAINERS_TO_DEL;
   
   L_table := 'WF_ORDER_DETAIL, '||'ITEM_MASTER, ' || 'WF_ORDER_EXP';
   
   --Fetch the records in wf_order_exp to be deleted 
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_EXP', L_table, to_char(I_wf_order_no));
   open C_LOCK_WF_ORDER_EXP;
   
   SQL_LIB.SET_MARK('FETCH', 'C_LOCK_WF_ORDER_EXP', L_table, to_char(I_wf_order_no));
   fetch C_LOCK_WF_ORDER_EXP BULK COLLECT INTO L_exp_rowid_tbl;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_EXP', L_table, to_char(I_wf_order_no));  
   close C_LOCK_WF_ORDER_EXP;
   
   L_table := 'WF_ORDER_EXP';
   
   --Delete the expenses for deposit containers with no linked deposit contents
   if L_exp_rowid_tbl is NOT NULL and L_exp_rowid_tbl.COUNT > 0 then
      SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
      FORALL i in L_exp_rowid_tbl.FIRST..L_exp_rowid_tbl.LAST
         delete from wf_order_exp
          where rowid = L_exp_rowid_tbl(i);
   end if;
   
   L_table := 'WF_ORDER_DETAIL, '||'ITEM_MASTER, ' || 'WF_ORDER_AUDIT';
   
   --Fetch the records in wf_order_exp to be deleted 
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_AUDIT', L_table, to_char(I_wf_order_no));
   open C_LOCK_WF_ORDER_AUDIT;
   
   SQL_LIB.SET_MARK('FETCH', 'C_LOCK_WF_ORDER_AUDIT', L_table, to_char(I_wf_order_no));
   fetch C_LOCK_WF_ORDER_AUDIT BULK COLLECT INTO L_audit_rowid_tbl;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_AUDIT', L_table, to_char(I_wf_order_no));  
   close C_LOCK_WF_ORDER_AUDIT;
   
   L_table := 'WF_ORDER_AUDIT';
   
   --Delete the audit entries for deposit containers with no linked deposit contents
   if L_audit_rowid_tbl is NOT NULL and L_audit_rowid_tbl.COUNT > 0 then
      SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
      FORALL i in L_audit_rowid_tbl.FIRST..L_audit_rowid_tbl.LAST
         delete from wf_order_audit
          where rowid = L_audit_rowid_tbl(i);
   end if;
   
   L_table := 'WF_ORDER_DETAIL';
   
   if L_containers_to_del is NOT NULL and L_containers_to_del.COUNT > 0 then
      SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
      FORALL i in L_containers_to_del.FIRST..L_containers_to_del.LAST
         delete from wf_order_detail
          where wf_order_no        =  I_wf_order_no
            and item               =  L_containers_to_del(i).item
            and source_loc_type    =  L_containers_to_del(i).source_loc_type
            and source_loc_id      =  L_containers_to_del(i).source_loc_id
            and customer_loc       =  L_containers_to_del(i).customer_loc;
   end if;
   
   L_table := 'WF_ORDER_DETAIL, '||'ITEM_MASTER';
      
   SQL_LIB.SET_MARK('OPEN', 'C_GET_CONTAINERS_TO_UPD', L_table, to_char(I_wf_order_no));
   open C_GET_CONTAINERS_TO_UPD;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_CONTAINERS_TO_UPD', L_table, to_char(I_wf_order_no));
   fetch C_GET_CONTAINERS_TO_UPD BULK COLLECT INTO L_containers_to_upd;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_CONTAINERS_TO_UPD', L_table, to_char(I_wf_order_no));
   close C_GET_CONTAINERS_TO_UPD;
   
   L_table := 'WF_ORDER_DETAIL';
   --Update the deposit container items based on the details of the linked deposit
   --contents
   if L_containers_to_upd is NOT NULL and L_containers_to_upd.COUNT > 0 then
      SQL_LIB.SET_MARK('UPDATE', NULL, L_table, NULL);
      FORALL i in L_containers_to_upd.FIRST..L_containers_to_upd.LAST
         update wf_order_detail
            set requested_qty        = L_containers_to_upd(i).requested_qty,
                need_date            = L_containers_to_upd(i).need_date,
                not_after_date       = L_containers_to_upd(i).not_after_date,
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where wf_order_no     = I_wf_order_no
            and item            = L_containers_to_upd(i).item
            and source_loc_type = L_containers_to_upd(i).source_loc_type
            and source_loc_id   = L_containers_to_upd(i).source_loc_id
            and customer_loc    = L_containers_to_upd(i).customer_loc;
   end if;
   
   L_table := 'WF_ORDER_DETAIL, '||'ITEM_MASTER';
   
   --Retrieve the deposit container items to be added to the Franchise order
   SQL_LIB.SET_MARK('OPEN', 'C_GET_CONTAINERS_TO_INS', L_table, to_char(I_wf_order_no));                               
   open C_GET_CONTAINERS_TO_INS;
   
   SQL_LIB.SET_MARK('FETCH', 'C_GET_CONTAINERS_TO_INS', L_table, to_char(I_wf_order_no));
   fetch C_GET_CONTAINERS_TO_INS BULK COLLECT INTO L_containers_to_ins;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_CONTAINERS_TO_INS', L_table, to_char(I_wf_order_no));
   close C_GET_CONTAINERS_TO_INS;
   
   if L_containers_to_ins is NOT NULL and L_containers_to_ins.COUNT > 0 then
      
      L_table := 'WF_ORDER_HEAD';
      
      SQL_LIB.SET_MARK('OPEN', 'C_WF_ORDER_CURR', L_table, to_char(I_wf_order_no));
      open C_WF_ORDER_CURR;
      
      SQL_LIB.SET_MARK('FETCH', 'C_WF_ORDER_CURR', L_table, to_char(I_wf_order_no));
      fetch C_WF_ORDER_CURR into L_wf_order_curr, L_approval_date;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_WF_ORDER_CURR', L_table, to_char(I_wf_order_no));
      close C_WF_ORDER_CURR;
      
      L_table := 'WF_ORDER_DETAIL';
      
      --Fetch the maximum wf_order_line_no in wf_order_detail
      SQL_LIB.SET_MARK('OPEN', 'C_CURR_WF_ORDER_LINE_NO', L_table, to_char(I_wf_order_no));      
      open C_CURR_WF_ORDER_LINE_NO;
      
      SQL_LIB.SET_MARK('FETCH', 'C_CURR_WF_ORDER_LINE_NO', L_table, to_char(I_wf_order_no));
      fetch C_CURR_WF_ORDER_LINE_NO into L_wf_order_line_no;
      
      SQL_LIB.SET_MARK('CLOSE', 'C_CURR_WF_ORDER_LINE_NO', L_table, to_char(I_wf_order_no));
      close C_CURR_WF_ORDER_LINE_NO;
      
      FOR i in L_containers_to_ins.FIRST..L_containers_to_ins.LAST LOOP
         --Range the deposit container items if not already ranged to the source location. Ranging of deposit
         --content items will be done by the calling function.
         L_container_item           :=  L_containers_to_ins(i).item;
         L_customer_loc             :=  L_containers_to_ins(i).customer_loc;
         L_templ_id                 :=  NULL;
         L_templ_desc               :=  NULL;
         L_margin_pct               :=  NULL;
         L_calc_type                :=  NULL;
         L_cust_cost_order_curr     :=  NULL;
         L_acqui_cost_order_curr    :=  NULL;
         L_cust_cost                :=  NULL;
         L_acqui_cost               :=  NULL;
         
         if L_containers_to_ins(i).from_ranged_ind is NOT NULL then   --from_ranged_ind will be NULL for physical warehouse and supplier
            if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                                 L_containers_to_ins(i).item,
                                                 L_containers_to_ins(i).source_loc_id,
                                                 L_item_loc_exists) = FALSE then
               return FALSE;
            end if;
               
            if L_item_loc_exists = FALSE then
               if NEW_ITEM_LOC(O_error_message,
                               L_containers_to_ins(i).item,
                               L_containers_to_ins(i).source_loc_id,
                               NULL,
                               NULL,
                               case when L_containers_to_ins(i).source_loc_type = 'ST' then 'S'
                                    else 'W'
                               end,    -- Loc_type
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               L_containers_to_ins(i).from_ranged_ind) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         ---   
         --Range the deposit container items if not already ranged to the Franchise location
         if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                              L_containers_to_ins(i).item,
                                              L_containers_to_ins(i).customer_loc,
                                              L_item_loc_exists) = FALSE then
            return FALSE;
         end if;
            
         if L_item_loc_exists = FALSE then
            if NEW_ITEM_LOC(O_error_message,
                            L_containers_to_ins(i).item,
                            L_containers_to_ins(i).customer_loc,
                            NULL,
                            NULL,
                            'S',                     -- Loc_type
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            L_containers_to_ins(i).to_ranged_ind) = FALSE then
               return FALSE;
            end if;
         end if;                                           


         --Call wf_order_sql to retrieve tha cost information for Franchise order
         --from future cost                                 
         if WF_ORDER_SQL.GET_COST_INFORMATION(O_error_message,
                                              L_templ_id,
                                              L_templ_desc,
                                              L_margin_pct,
                                              L_calc_type,
                                              L_cust_cost,
                                              L_acqui_cost,
                                              L_currency_code,
                                              L_containers_to_ins(i).item,
                                              L_containers_to_ins(i).customer_loc,
                                              L_containers_to_ins(i).source_loc_type,
                                              L_containers_to_ins(i).source_loc_id)= FALSE then
            return FALSE;
         end if;
         
         if L_currency_code != L_wf_order_curr then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_cust_cost,
                                    L_currency_code,
                                    L_wf_order_curr,
                                    L_cust_cost_order_curr,
                                    'C',    --cost_retail_ind
                                    L_approval_date,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_acqui_cost,
                                    L_currency_code,
                                    L_wf_order_curr,
                                    L_acqui_cost_order_curr,
                                    'C',    --cost_retail_ind
                                    L_approval_date,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
         else
            L_cust_cost_order_curr := L_cust_cost;
            L_acqui_cost_order_curr := L_acqui_cost;
         end if;
         
         L_table := 'WF_ORDER_DETAIL, TSFHEAD, TSFDETAIL';
         
         SQL_LIB.SET_MARK('OPEN', 'C_GET_ORIG_ORD_LINE_COST', L_table, to_char(I_wf_order_no));
         open C_GET_ORIG_ORD_LINE_COST;
         
         SQL_LIB.SET_MARK('FETCH', 'C_GET_ORIG_ORD_LINE_COST', L_table, to_char(I_wf_order_no));
         fetch C_GET_ORIG_ORD_LINE_COST into L_templ_id,
                                             L_templ_desc,
                                             L_margin_pct,
                                             L_calc_type,
                                             L_cust_cost_order_curr,
                                             L_acqui_cost_order_curr;
                                                      
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORIG_ORD_LINE_COST', L_table, to_char(I_wf_order_no));                                             
         close C_GET_ORIG_ORD_LINE_COST;
         
         L_table := 'WF_ORDER_DETAIL';
         
         SQL_LIB.SET_MARK('INSERT', 'NULL', L_table, NULL);
         insert into wf_order_detail (wf_order_no,
                                      wf_order_line_no,
                                      item,
                                      source_loc_type,
                                      source_loc_id,
                                      customer_loc,
                                      requested_qty,
                                      cancel_reason,
                                      need_date,
                                      not_after_date,
                                      acquisition_cost,
                                      customer_cost,
                                      fixed_cost,
                                      templ_id,
                                      margin_pct,
                                      calc_type,
                                      templ_desc,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
                              values (I_wf_order_no,
                                      L_wf_order_line_no + i,
                                      L_containers_to_ins(i).item,
                                      L_containers_to_ins(i).source_loc_type,
                                      L_containers_to_ins(i).source_loc_id,
                                      L_containers_to_ins(i).customer_loc,
                                      L_containers_to_ins(i).requested_qty,
                                      NULL,                            --cancel_reason
                                      L_containers_to_ins(i).need_date,
                                      L_containers_to_ins(i).not_after_date,
                                      L_acqui_cost_order_curr,
                                      L_cust_cost_order_curr,
                                      NULL,                            --fixed_cost
                                      L_templ_id,
                                      L_margin_pct,
                                      L_calc_type,
                                      L_templ_desc,
                                      sysdate,
                                      get_user,
                                      sysdate,
                                      get_user);
         
         --Call wf_order_sql to create expenses for the newly added deposit container
         --items. This will insert records into wf_order_exp
         if WF_ORDER_SQL.GET_UPCHARGE_INFORMATION(O_error_message,
                                                  I_wf_order_no,
                                                  L_wf_order_curr,
                                                  L_containers_to_ins(i).item,
                                                  L_containers_to_ins(i).source_loc_type,
                                                  L_containers_to_ins(i).source_loc_id,
                                                  L_containers_to_ins(i).customer_loc) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
           
   return TRUE;
      
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_ORDER_DETAIL%ISOPEN then
         close C_LOCK_WF_ORDER_DETAIL;
      end if;
      if C_GET_CONTAINERS_TO_DEL%ISOPEN then
         close C_GET_CONTAINERS_TO_DEL;
      end if;
      if C_GET_CONTAINERS_TO_INS%ISOPEN then
         close C_GET_CONTAINERS_TO_INS;
      end if;
      if C_GET_CONTAINERS_TO_UPD%ISOPEN then
         close C_GET_CONTAINERS_TO_UPD;
      end if;
      if C_WF_ORDER_CURR%ISOPEN then
         close C_WF_ORDER_CURR;
      end if;
      if C_CURR_WF_ORDER_LINE_NO%ISOPEN then
         close C_CURR_WF_ORDER_LINE_NO;
      end if;
      if C_LOCK_WF_ORDER_EXP%ISOPEN then
         close C_LOCK_WF_ORDER_EXP;
      end if;
      if C_LOCK_WF_ORDER_AUDIT%ISOPEN then
         close C_LOCK_WF_ORDER_AUDIT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             'wf_order_no: '||to_char(I_wf_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_WF_ORDER_DETAIL%ISOPEN then
         close C_LOCK_WF_ORDER_DETAIL;
      end if;
      if C_GET_CONTAINERS_TO_DEL%ISOPEN then
         close C_GET_CONTAINERS_TO_DEL;
      end if;
      if C_GET_CONTAINERS_TO_INS%ISOPEN then
         close C_GET_CONTAINERS_TO_INS;
      end if;
      if C_GET_CONTAINERS_TO_UPD%ISOPEN then
         close C_GET_CONTAINERS_TO_UPD;
      end if;
      if C_WF_ORDER_CURR%ISOPEN then
         close C_WF_ORDER_CURR;
      end if;
      if C_CURR_WF_ORDER_LINE_NO%ISOPEN then
         close C_CURR_WF_ORDER_LINE_NO;
      end if;
      if C_LOCK_WF_ORDER_EXP%ISOPEN then
         close C_LOCK_WF_ORDER_EXP;
      end if;
      if C_LOCK_WF_ORDER_AUDIT%ISOPEN then
         close C_LOCK_WF_ORDER_AUDIT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SYNC_DEPOSIT_ITEMS;
----------------------------------------------------------------------------------
END WF_CREATE_ORDER_SQL;
/