CREATE OR REPLACE PACKAGE BODY WF_BOL_SQL AS

FUNCTION INSERT_WF_BILLING_SALES(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_wf_order_no        IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                 I_item_rec           IN       ITEM_MASTER%ROWTYPE,
                                 I_from_loc           IN       ITEM_LOC.LOC%TYPE,
                                 I_from_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                                 I_to_loc             IN       ITEM_LOC.LOC%TYPE,
                                 I_costing_loc        IN       ITEM_LOC.COSTING_LOC%TYPE,
                                 I_costing_loc_type   IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE,
                                 I_qty                IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_tran_date          IN       DATE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION INSERT_WF_BILLING_RETURNS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                                   I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                                   I_item_rec        IN       ITEM_MASTER%ROWTYPE,
                                   I_to_loc          IN       SHIPMENT.TO_LOC%TYPE,
                                   I_to_loc_type     IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                                   I_qty             IN       WF_RETURN_DETAIL.RETURNED_QTY%TYPE,
                                   I_tran_date       IN       DATE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION UPDATE_SHIPPED_WF_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                                 I_distro_type     IN       SHIPSKU.DISTRO_TYPE%TYPE,
                                 I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE,
                                 I_to_loc_type     IN       ALLOC_DETAIL.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_function      VARCHAR2(61) := 'WF_BOL_SQL.UPDATE_SHIPPED_WF_ORDER';
   L_req_field     VARCHAR2(30);
   L_wf_order_no   WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rowid         ROWID;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_WF_ORDER_HEAD is
      select woh.rowid,
             woh.wf_order_no
        from (select wf_order_no
                from tsfhead
               where I_distro_type = 'T'
                 and tsf_no = I_distro_no
                 and status in ('P','S','A','L')
           union all
              select wf_order_no
                from alloc_detail
               where I_distro_type = 'A'
                 and alloc_no = I_distro_no
                 and to_loc = I_to_loc
                 and to_loc_type = I_to_loc_type
                 and qty_transferred > 0) distro,
             wf_order_head woh
       where distro.wf_order_no = woh.wf_order_no
         and woh.status NOT in ('P','C','D')
         for update of woh.wf_order_no nowait;

BEGIN

   if I_distro_no is NULL then
      L_req_field := 'I_distro_no';
   elsif I_distro_type is NULL then
      L_req_field := 'I_distro_type';
   elsif I_distro_type = 'A' then
      if I_to_loc is NULL then
         L_req_field := 'I_to_loc';
      elsif I_to_loc_type is NULL then
         L_req_field := 'I_to_loc_type';
      end if;
   end if;

   if L_req_field is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_req_field,
                                            L_function);
      return FALSE;
   end if;

   if I_distro_type = 'A' and I_to_loc_type <> 'S' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TO_LOC',
	                                        L_function);
      return FALSE;
   end if;

   open C_LOCK_WF_ORDER_HEAD;
   fetch C_LOCK_WF_ORDER_HEAD into L_rowid,
                                   L_wf_order_no;
   close C_LOCK_WF_ORDER_HEAD;

   update wf_order_head
      set status = 'P',
          last_update_datetime = sysdate,
          last_update_id = get_user
    where rowid = L_rowid;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'WF_ORDER_HEAD',
                                             'WF_ORDER_NO:'||to_char(L_wf_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_SHIPPED_WF_ORDER;
-------------------------------------------------------------------------------
FUNCTION UPDATE_SHIPPED_WF_RETURN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_function      VARCHAR2(61) := 'WF_BOL_SQL.UPDATE_SHIPPED_WF_RETURN';
   L_rma_no        WF_RETURN_HEAD.RMA_NO%TYPE;
   L_rowid         ROWID;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_WF_RETURN_HEAD is
      select rowid,
             rma_no
        from wf_return_head wrh
       where wrh.status NOT in ('P','C','D')
         and exists (select 'x'
                      from tsfhead tsf
                     where tsf.tsf_no = I_tsf_no
                       and tsf.rma_no = wrh.rma_no
                       and tsf.status in ('P','S','A','L'))
         for update nowait;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_function);
      return FALSE;
   end if;

   open C_LOCK_WF_RETURN_HEAD;
   fetch C_LOCK_WF_RETURN_HEAD into L_rowid,
                                    L_rma_no;
   close C_LOCK_WF_RETURN_HEAD;

   update wf_return_head
      set status = 'P',
          last_update_datetime = sysdate,
          last_update_id = get_user
    where rowid = L_rowid;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'WF_RETURN_HEAD',
                                             'RMA_NO:'||to_char(L_rma_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_SHIPPED_WF_RETURN;
-------------------------------------------------------------------------------
FUNCTION WRITE_WF_BILLING_SALES(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no        IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                I_item               IN       ITEM_MASTER.ITEM%TYPE,
                                I_from_loc           IN       ITEM_LOC.LOC%TYPE,
                                I_from_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                                I_to_loc             IN       ITEM_LOC.LOC%TYPE,
                                I_costing_loc        IN       ITEM_LOC.COSTING_LOC%TYPE,
                                I_costing_loc_type   IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE,
                                I_qty                IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                I_tran_date          IN       DATE)

RETURN BOOLEAN IS

   L_function              VARCHAR2(61) := 'WF_BOL_SQL.WRITE_WF_BILLING_SALES';
   L_item_master_rec       ITEM_MASTER%ROWTYPE;
   L_container_item_rec    ITEM_MASTER%ROWTYPE;
   L_req_field             VARCHAR2(30);

BEGIN
   if I_wf_order_no is NULL then
      L_req_field := 'I_wf_order_no';
   elsif I_item is NULL then
      L_req_field := 'I_item';
   elsif I_from_loc is NULL then
      L_req_field := 'I_from_loc';
   elsif I_from_loc_type is NULL then
      L_req_field := 'I_from_loc_type';
   elsif I_to_loc is NULL then
      L_req_field := 'I_to_loc';
   elsif I_qty is NULL then
      L_req_field := 'I_qty';
   elsif I_tran_date is NULL then
      L_req_field := 'I_tran_date';
   elsif I_from_loc_type = 'U' then
      if I_costing_loc is NULL then
         L_req_field := 'I_costing_loc';
      elsif I_costing_loc_type is NULL then
         L_req_field := 'I_costing_loc_type';
      end if;
   end if;

   if L_req_field is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_req_field,
                                            L_function);
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_master_rec,
                                      I_item) = FALSE then
      return FALSE;
   end if;

   if INSERT_WF_BILLING_SALES(O_error_message,
                              I_wf_order_no,
                              L_item_master_rec,
                              I_from_loc,
                              I_from_loc_type,
                              I_to_loc,
                              I_costing_loc,
                              I_costing_loc_type,
                              I_qty,
                              I_tran_date) = FALSE then
      return FALSE;
   end if;

   --write billing sales for deposit container
   if L_item_master_rec.container_item is NOT NULL then
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_container_item_rec,
                                         L_item_master_rec.container_item) = FALSE then
         return FALSE;
      end if;

      if INSERT_WF_BILLING_SALES(O_error_message,
                                 I_wf_order_no,
                                 L_container_item_rec,
                                 I_from_loc,
                                 I_from_loc_type,
                                 I_to_loc,
                                 I_costing_loc,
                                 I_costing_loc_type,
                                 I_qty,
                                 I_tran_date) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_WF_BILLING_SALES;
-------------------------------------------------------------------------------------------
FUNCTION WRITE_WF_BILLING_RETURNS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                                  I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                                  I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                  I_to_loc          IN       SHIPMENT.TO_LOC%TYPE,
                                  I_to_loc_type     IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                                  I_qty             IN       WF_RETURN_DETAIL.RETURNED_QTY%TYPE,
                                  I_tran_date       IN       DATE)
RETURN BOOLEAN IS

   L_function              VARCHAR2(61) := 'WF_BOL_SQL.WRITE_WF_BILLING_RETURNS';
   L_req_field             VARCHAR2(30);
   L_item_master_rec       ITEM_MASTER%ROWTYPE;
   L_container_item_rec    ITEM_MASTER%ROWTYPE;

BEGIN

   if I_tsf_no is NULL AND I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELDS_NULL',
                                            'I_tsf_no',
                                            'I_rma_no');
      return FALSE;
   end if;

   if I_item is NULL then
      L_req_field := 'I_item';
   elsif I_to_loc is NULL then
      L_req_field := 'I_to_loc';
   elsif I_to_loc_type is NULL then
      L_req_field := 'I_to_loc_type';
   elsif I_tran_date is NULL then
      L_req_field := 'I_tran_date';
   elsif I_qty is NULL then
      L_req_field := 'I_qty';
   end if;

   if L_req_field is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_req_field,
                                            L_function);
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_master_rec,
                                      I_item) = FALSE then
      return FALSE;
   end if;

   if INSERT_WF_BILLING_RETURNS(O_error_message,
                                I_rma_no,
                                I_tsf_no,
                                L_item_master_rec,
                                I_to_loc,
                                I_to_loc_type,
                                I_qty,
                                I_tran_date) = FALSE then
      return FALSE;
   end if;

   --write billing returns for deposit container
   if L_item_master_rec.container_item is NOT NULL then
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_container_item_rec,
                                         L_item_master_rec.container_item) = FALSE then
         return FALSE;
      end if;

      if INSERT_WF_BILLING_RETURNS(O_error_message,
                                   I_rma_no,
                                   I_tsf_no,
                                   L_container_item_rec,
                                   I_to_loc,
                                   I_to_loc_type,
                                   I_qty,
                                   I_tran_date) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END WRITE_WF_BILLING_RETURNS;
--------------------------------------------------------------------------------
FUNCTION WRITE_WF_BILLING_SALES_RETURNS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                        I_distro_no             IN     SHIPSKU.DISTRO_NO%TYPE,
                                        I_distro_type           IN     SHIPSKU.DISTRO_TYPE%TYPE,
                                        I_qty                   IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                        I_from_loc              IN     ITEM_LOC.LOC%TYPE,
                                        I_from_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                        I_to_loc                IN     ITEM_LOC.LOC%TYPE,
                                        I_to_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                                        I_tran_date             IN     PERIOD.VDATE%TYPE,
                                        I_franchise_ordret_ind  IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(61)          := 'WF_BOL_SQL.WRITE_WF_BILLING_SALES_RETURNS';
   L_inv_parm                   VARCHAR2(30)          := NULL;
   L_wf_order_no                WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rma_no                     WF_RETURN_HEAD.RMA_NO%TYPE;
   L_action_type                VARCHAR2(1);

BEGIN

   if I_item is NULL then
      L_inv_parm := 'I_item';
   elsif I_distro_no is NULL then
      L_inv_parm := 'I_distro_no';
   elsif I_distro_type is NULL then
      L_inv_parm := 'I_distro_type';
   elsif I_qty is NULL then
      L_inv_parm := 'I_qty';
    elsif I_from_loc is NULL then
      L_inv_parm := 'I_from_loc';
   elsif I_from_loc_type is NULL then
      L_inv_parm := 'I_from_loc_type';
   elsif I_to_loc is NULL then
      L_inv_parm := 'I_to_loc';
   elsif I_to_loc_type is NULL then
      L_inv_parm := 'I_to_loc_type';
   elsif I_tran_date is NULL then
      L_inv_parm := 'I_tran_date';
   end if;
   ---
   if L_inv_parm is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_franchise_ordret_ind is NULL then
      if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                   L_action_type,
                                                   I_from_loc,
                                                   I_from_loc_type,
                                                   I_to_loc,
                                                   I_to_loc_type) = FALSE then
         return FALSE;
      end if;
   else
      L_action_type := I_franchise_ordret_ind;
   end if;
   ---
   if L_action_type IN (WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER,WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN) then
      if WF_TRANSFER_SQL.GET_WF_ORDER_RMA_NO(O_error_message,
                                             L_wf_order_no,
                                             L_rma_no,
                                             I_distro_type,
                                             I_distro_no,
                                             I_to_loc,
                                             I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if L_action_type = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
         if WF_BOL_SQL.WRITE_WF_BILLING_RETURNS(O_error_message,
                                                L_rma_no,
                                                I_distro_no,
                                                I_item,
                                                I_to_loc,
                                                I_to_loc_type,
                                                I_qty,
                                                I_tran_date) = FALSE then
            return FALSE;
         end if;
      else
         if WF_BOL_SQL.WRITE_WF_BILLING_SALES(O_error_message,
                                              L_wf_order_no,
                                              I_item,
                                              I_from_loc,
                                              I_from_loc_type,
                                              I_to_loc,
                                              NULL,
                                              NULL,
                                              I_qty,
                                              I_tran_date) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END WRITE_WF_BILLING_SALES_RETURNS;
--------------------------------------------------------------------------------
FUNCTION INSERT_WF_BILLING_SALES(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_wf_order_no        IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                 I_item_rec           IN       ITEM_MASTER%ROWTYPE,
                                 I_from_loc           IN       ITEM_LOC.LOC%TYPE,
                                 I_from_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                                 I_to_loc             IN       ITEM_LOC.LOC%TYPE,
                                 I_costing_loc        IN       ITEM_LOC.COSTING_LOC%TYPE,
                                 I_costing_loc_type   IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE,
                                 I_qty                IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_tran_date          IN       DATE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(61)                          := 'WF_BOL_SQL.INSERT_WF_BILLING_SALES';
   L_obj_tax_calc_rec    OBJ_TAX_CALC_REC                      := OBJ_TAX_CALC_REC();
   L_obj_tax_calc_tbl    OBJ_TAX_CALC_TBL                      := OBJ_TAX_CALC_TBL();
   L_tax_code            WF_BILLING_SALES.VAT_CODE%TYPE        := NULL;
   L_tax_rate            WF_BILLING_SALES.VAT_RATE%TYPE        := NULL;
   L_source_loc_type     WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_customer_cost       WF_ORDER_DETAIL.CUSTOMER_COST%TYPE;
   L_fixed_cost          WF_ORDER_DETAIL.FIXED_COST%TYPE;
   L_to_loc_type         VARCHAR2(2);

   cursor C_GET_COST is
      select customer_cost,
             fixed_cost
        from wf_order_detail
       where wf_order_no = I_wf_order_no
         and item = I_item_rec.item
         and customer_loc = I_to_loc;

   cursor C_GET_TO_LOC_TYPE is
      select 'ST'
        from store
       where store = I_to_loc
       union
      select 'WH'
        from wh
       where wh = I_to_loc;

BEGIN

   open C_GET_COST;
   fetch C_GET_COST into L_customer_cost,
                         L_fixed_cost;
   close C_GET_COST;

   open C_GET_TO_LOC_TYPE;
   fetch C_GET_TO_LOC_TYPE into L_to_loc_type;
   close C_GET_TO_LOC_TYPE;

   L_obj_tax_calc_rec.I_item                := I_item_rec.item;
   L_obj_tax_calc_rec.I_pack_ind            := I_item_rec.pack_ind;
   L_obj_tax_calc_rec.I_from_entity         := I_from_loc;
   L_obj_tax_calc_rec.I_effective_from_date := I_tran_date;
   L_obj_tax_calc_rec.I_tran_type           := 'WFSALES';
   L_obj_tax_calc_rec.I_to_entity           := I_to_loc;
   L_obj_tax_calc_rec.I_to_entity_type      := L_to_loc_type;

   if I_from_loc_type = 'U' then
      L_obj_tax_calc_rec.I_source_entity_type := 'SU';
      L_obj_tax_calc_rec.I_source_entity      := I_from_loc;
   end if;

   L_obj_tax_calc_rec.I_tran_date           := I_tran_date;
   L_obj_tax_calc_rec.I_tran_id             := I_wf_order_no;
   L_obj_tax_calc_rec.I_cost_retail_ind     := 'R';
   L_obj_tax_calc_rec.I_amount              := NVL(L_fixed_cost,L_customer_cost);

   if I_from_loc_type = 'W' then
      L_obj_tax_calc_rec.I_from_entity_type := 'WH';
      L_source_loc_type := L_obj_tax_calc_rec.I_from_entity_type;
   elsif I_from_loc_type = 'S' then
      L_obj_tax_calc_rec.I_from_entity_type := 'ST';
      L_source_loc_type := L_obj_tax_calc_rec.I_from_entity_type;
   -- if from_loc is a supplier, use the costing_loc and costing_loc_type to retrieve Retail tax code/tax rate.
   elsif I_from_loc_type = 'U' then
      if I_costing_loc_type = 'W' then
         L_obj_tax_calc_rec.I_from_entity_type := 'WH';
      elsif I_costing_loc_type = 'S' then
         L_obj_tax_calc_rec.I_from_entity_type := 'ST';
      else
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_costing_loc_type',
                                               I_costing_loc_type,
                                               'W or S');
         return FALSE;
      end if;
      L_obj_tax_calc_rec.I_from_entity := I_costing_loc;
      L_source_loc_type := 'SU';
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_from_loc_type',
                                            I_from_loc_type,
                                            'W, S or U');
      return FALSE;
   end if;

   L_obj_tax_calc_tbl.EXTEND;
   L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT) := L_obj_tax_calc_rec;

   if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                              L_obj_tax_calc_tbl) = FALSE then
      return FALSE;
   end if;

   -- Franchise is only supported for a SVAT configuration. In case of SVAT,
   -- there is only 1 vat_code for a given item/vat_region/vat_code_type.
   -- The returned O_tax_detail_tbl would only contain 1 record.

   if L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_tax_detail_tbl is NOT NULL and
      L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_tax_detail_tbl.COUNT > 0 then
      L_tax_code := L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_tax_detail_tbl(1).tax_code;
      L_tax_rate := L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_cum_tax_pct;
   end if;

   insert into wf_billing_sales(customer_loc,
                                cust_ord_ref_no,
                                wf_order_no,
                                order_date,
                                shipment_date,
                                item,
                                dept,
                                class,
                                subclass,
                                order_qty,
                                customer_cost,
                                freight_cost,
                                other_charges,
                                vat_code,
                                vat_rate,
                                extracted_ind)
                               (select wod.customer_loc,
                                       woh.cust_ord_ref_no,
                                       woh.wf_order_no,
                                       NVL(woh.approval_date, I_tran_date),
                                       I_tran_date,
                                       I_item_rec.item,
                                       I_item_rec.dept,
                                       I_item_rec.class,
                                       I_item_rec.subclass,
                                       I_qty,
                                       NVL(wod.fixed_cost, wod.customer_cost),
                                       decode(qty.total_qty, 0, 0, (NVL(woh.freight, 0)/qty.total_qty)),
                                       decode(qty.total_qty, 0, 0, (NVL(woh.other_charges, 0)/qty.total_qty)),
                                       L_tax_code,
                                       L_tax_rate,
                                       'N'
                                  from wf_order_head woh,
                                       wf_order_detail wod,
                                       (select sum(NVL(requested_qty, 0)) total_qty
                                          from wf_order_detail
                                         where wf_order_no = I_wf_order_no) qty
                                 where woh.wf_order_no = I_wf_order_no
                                   and woh.wf_order_no = wod.wf_order_no
                                   and wod.customer_loc = I_to_loc
                                   and wod.item = I_item_rec.item
                                   and (wod.source_loc_type = L_source_loc_type -- (SU/ST/WH)
                                        and (wod.source_loc_id = I_from_loc
                                             or wod.source_loc_type = 'WH'
                                             and wod.source_loc_id in (select physical_wh
                                                                         from wh
                                                                        where wh = I_from_loc))) );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_WF_BILLING_SALES;
--------------------------------------------------------------------------------
FUNCTION INSERT_WF_BILLING_RETURNS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                                   I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                                   I_item_rec        IN       ITEM_MASTER%ROWTYPE,
                                   I_to_loc          IN       SHIPMENT.TO_LOC%TYPE,
                                   I_to_loc_type     IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                                   I_qty             IN       WF_RETURN_DETAIL.RETURNED_QTY%TYPE,
                                   I_tran_date       IN       DATE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(61)                    := 'WF_BOL_SQL.INSERT_WF_BILLING_RETURNS';
   L_obj_tax_calc_rec    OBJ_TAX_CALC_REC                := OBJ_TAX_CALC_REC();
   L_obj_tax_calc_tbl    OBJ_TAX_CALC_TBL                := OBJ_TAX_CALC_TBL();
   L_tax_code            WF_BILLING_SALES.VAT_CODE%TYPE  := NULL;
   L_tax_rate            WF_BILLING_SALES.VAT_RATE%TYPE  := NULL;
   L_customer_loc        WF_RETURN_HEAD.CUSTOMER_LOC%TYPE;
   L_cust_ord_ref_no     WF_ORDER_HEAD.CUST_ORD_REF_NO%TYPE;
   L_wf_order_no         WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rma_no              WF_RETURN_HEAD.RMA_NO%TYPE;
   L_return_unit_cost    WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE;
   L_unit_restock_fee    WF_RETURN_DETAIL.UNIT_RESTOCK_FEE%TYPE;
   L_customer_loc_type   VARCHAR2(2);

   cursor C_GET_RETURN_DETAILS_DISTRO is
      select wfrh.customer_loc,
             wfoh.cust_ord_ref_no,
             wfrd.wf_order_no,
             wfrh.rma_no,
             wfrd.return_unit_cost,
             DECODE(wfrd.restock_type,
                         'S', wfrd.unit_restock_fee,
                         'V', wfrd.return_unit_cost * (wfrd.unit_restock_fee/100),
                         0) unit_restock_fee
        from tsfhead th,
             wf_return_head wfrh,
             wf_return_detail wfrd,
             wf_order_head wfoh
       where th.tsf_no = I_tsf_no
         and wfrh.rma_no = th.rma_no
         and wfrd.rma_no = wfrh.rma_no
         and wfrd.item = I_item_rec.item
         and wfoh.wf_order_no(+) = wfrd.wf_order_no;

   cursor C_GET_RETURN_DETAILS_RMA is
      select wfrh.customer_loc,
             wfoh.cust_ord_ref_no,
             wfrd.wf_order_no,
             wfrh.rma_no,
             wfrd.return_unit_cost,
             DECODE(wfrd.restock_type,
                         'S', wfrd.unit_restock_fee,
                         'V', wfrd.return_unit_cost * (wfrd.unit_restock_fee/100),
                         0) unit_restock_fee
        from wf_return_head wfrh,
             wf_return_detail wfrd,
             wf_order_head wfoh
       where wfrh.rma_no = I_rma_no
         and wfrd.rma_no = wfrh.rma_no
         and wfrd.item = I_item_rec.item
         and wfoh.wf_order_no(+) = wfrd.wf_order_no;

   cursor C_GET_CUSTOMER_LOC_TYPE is
      select 'ST'
        from store
       where store = L_customer_loc
       union
      select 'WH'
        from wh
       where wh = L_customer_loc;

BEGIN

   if I_tsf_no is NOT NULL then
      open C_GET_RETURN_DETAILS_DISTRO;
      fetch C_GET_RETURN_DETAILS_DISTRO into L_customer_loc,
                                             L_cust_ord_ref_no,
                                             L_wf_order_no,
                                             L_rma_no,
                                             L_return_unit_cost,
                                             L_unit_restock_fee;
      close C_GET_RETURN_DETAILS_DISTRO;
   elsif I_rma_no is NOT NULL then
      open C_GET_RETURN_DETAILS_RMA;
      fetch C_GET_RETURN_DETAILS_RMA into L_customer_loc,
                                          L_cust_ord_ref_no,
                                          L_wf_order_no,
                                          L_rma_no,
                                          L_return_unit_cost,
                                          L_unit_restock_fee;
      close C_GET_RETURN_DETAILS_RMA;
   end if;

   if L_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC');
      return FALSE;
   end if;

   L_obj_tax_calc_rec.I_item                := I_item_rec.item;
   L_obj_tax_calc_rec.I_pack_ind            := I_item_rec.pack_ind;
   --use tax_code/tax_rate of the return location (company loc)'s vat_region
   L_obj_tax_calc_rec.I_from_entity         := I_to_loc;
   L_obj_tax_calc_rec.I_effective_from_date := I_tran_date;
   L_obj_tax_calc_rec.I_to_entity           := L_customer_loc;

   open C_GET_CUSTOMER_LOC_TYPE;
   fetch C_GET_CUSTOMER_LOC_TYPE into L_customer_loc_type;
   close C_GET_CUSTOMER_LOC_TYPE;

   L_obj_tax_calc_rec.I_to_entity_type      := L_customer_loc_type;
   L_obj_tax_calc_rec.I_tran_type           := 'WFRETURNS';
   L_obj_tax_calc_rec.I_cost_retail_ind     := 'R';
   L_obj_tax_calc_rec.I_tran_date           := I_tran_date;
   L_obj_tax_calc_rec.I_tran_id             := L_rma_no;
   L_obj_tax_calc_rec.I_amount              := L_return_unit_cost;

   if I_to_loc_type = 'W' then
      L_obj_tax_calc_rec.I_from_entity_type := 'WH';
   elsif I_to_loc_type = 'S' then
      L_obj_tax_calc_rec.I_from_entity_type := 'ST';
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_to_loc_type',
                                            I_to_loc_type,
                                            'W or S');
      return FALSE;
   end if;

   L_obj_tax_calc_tbl.EXTEND;
   L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT) := L_obj_tax_calc_rec;

   if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                              L_obj_tax_calc_tbl) = FALSE then
      return FALSE;
   end if;

   -- Franchise is only supported for a SVAT configuration. In case of SVAT,
   -- there is only 1 vat_code for a given item/vat_region/vat_code_type.
   -- The returned O_tax_detail_tbl would only contain 1 record.

   if L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_tax_detail_tbl is NOT NULL and
      L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_tax_detail_tbl.COUNT > 0 then
      L_tax_code := L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_tax_detail_tbl(1).tax_code;
      L_tax_rate := L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_cum_tax_pct;
   end if;

   insert into wf_billing_returns (customer_loc,
                                   cust_ord_ref_no,
                                   wf_order_no,
                                   rma_no,
                                   return_date,
                                   item,
                                   dept,
                                   class,
                                   subclass,
                                   returned_qty,
                                   return_unit_cost,
                                   restocking_fee,
                                   vat_code,
                                   vat_rate,
                                   extracted_ind,
                                   extracted_date)
                           values (L_customer_loc,
                                   L_cust_ord_ref_no,
                                   L_wf_order_no,
                                   L_rma_no,
                                   I_tran_date,
                                   I_item_rec.item,
                                   I_item_rec.dept,
                                   I_item_rec.class,
                                   I_item_rec.subclass,
                                   I_qty,
                                   L_return_unit_cost,
                                   L_unit_restock_fee,
                                   L_tax_code,
                                   L_tax_rate,
                                   'N',
                                   NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_WF_BILLING_RETURNS;
--------------------------------------------------------------------------------
END WF_BOL_SQL;
/