----------------------------------------------------------------------------------------
--Name     : WF_PO_SQL
--Package  : This package will contain the functions that will process Franchise PO
--           creation, modification, cancellation or deletion.  These functions will
--           process transactions that result from creating, modifying or cancelling
--           a franchise order.
----------------------------------------------------------------------------------------
CREATE OR REPLACE PACKAGE WF_PO_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------
-- Constant Variables
WF_STATUS_APPROVE    CONSTANT VARCHAR2(1)  := 'A';
WF_STATUS_INPUT      CONSTANT VARCHAR2(1)  := 'I';
WF_STATUS_INPROGRESS CONSTANT VARCHAR2(1)  := 'P';
WF_STATUS_REQ_CREDIT CONSTANT VARCHAR2(1)  := 'R';
-----------------------------------------------------------------------------------
-- SYNC_F_ORDER
-- This function will merge the purchase order details to create/update franchise 
-- order. The supported action types are 'create', 'update', 'shipped'. 
-- For action type as create, the newly create wf_order_no will be returned back
-----------------------------------------------------------------------------------
FUNCTION SYNC_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_wf_order_no     IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       O_order_status    IN OUT   ORDHEAD.STATUS%TYPE,
                       I_action_type     IN       VARCHAR2,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_order_status    IN       ORDHEAD.STATUS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- DELETE_F_ORDER
-- The function will delete the linked franchise order for a purchase order.
-----------------------------------------------------------------------------------
FUNCTION DELETE_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : CREATE_FRANCHISE_PO
--Function : This function will create purchase orders associated to a franchise order
--           with source_type of Supplier.
----------------------------------------------------------------------------------------
FUNCTION CREATE_FRANCHISE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                             I_scaling         IN       VARCHAR2,
                             I_undo_scaling    IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : MODIFY_FRANCHISE_PO
--Function : This function will modify purchase orders associated to a franchise order
--           with source_type of 'S'upplier.
----------------------------------------------------------------------------------------
FUNCTION MODIFY_FRANCHISE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       ORDHEAD.WF_ORDER_NO%TYPE,
                             I_scaling         IN       VARCHAR2,
                             I_undo_scaling    IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : DELETE_FRANCHISE_PO
--Function : This function will delete all purchase orders associated to a given
--           franchise order.
----------------------------------------------------------------------------------------
FUNCTION DELETE_FRANCHISE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : CANCEL_FRANCHISE_PO
--Function : This function will cancel all purchase orders associated to a given
--           franchise order.
----------------------------------------------------------------------------------------
FUNCTION CANCEL_FRANCHISE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       ORDHEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
END WF_PO_SQL;
/