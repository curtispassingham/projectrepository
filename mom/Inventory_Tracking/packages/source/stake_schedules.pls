-------------------------------------------------------------------------
--
-- This package will be used for the actual processing of stock
-- count subscription.  Prior to any calls to this package, the
-- INIT function has to be called and subsequent to any calls to
-- this package, the FLUSH function has to be called.
--
-- It is assumed that the VALIDATE_VALUES function and CREATE_SH_REC
-- fucntion (in that order) will be called prior to calling
-- the PROCESS_PROD and/or PROCESS_LOC functions.  This will ensure
-- all necessary validation happens prior to a lot of other
-- processing.
--
-------------------------------------------------------------------------

CREATE OR REPLACE PACKAGE STAKE_SCHEDULE_SQL AUTHID CURRENT_USER AS

STKCOUNTSCHCRE VARCHAR2(15) := 'stkcountschcre';
STKCOUNTSCHMOD VARCHAR2(15) := 'stkcountschmod';
STKCOUNTSCHDEL VARCHAR2(15) := 'stkcountschdel';

/* Function and Procedure Bodies */
-------------------------------------------------------------------------
-- This function will validate the passed in values to ensure that
-- they are valid and applicable for the given stock count.
-------------------------------------------------------------------------
FUNCTION VALIDATE_VALUES(O_error_message   IN OUT  VARCHAR2,
                         I_cycle_count     IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                         I_stocktake_type  IN      STAKE_HEAD.STOCKTAKE_TYPE%TYPE,
                         I_stocktake_date  IN      STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                         I_location_type   IN      STAKE_HEAD.LOC_TYPE%TYPE,
                         I_action          IN      VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------
-- This function will populate the variables for inserting into the
-- STAKE_HEAD table.
-------------------------------------------------------------------------
FUNCTION CREATE_SH_REC(O_error_message     IN OUT  VARCHAR2,
                       I_cycle_count       IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                       I_cycle_count_desc  IN      STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                       I_location_type     IN      STAKE_HEAD.LOC_TYPE%TYPE,
                       I_stocktake_date    IN      STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                       I_stocktake_type    IN      STAKE_HEAD.STOCKTAKE_TYPE%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------
-- This function will create records to be inserted into the STAKE_PRODUCT
-- table.  This function will handle either a single hierarchy
-- instance at a time (with assumed looping happening at the level
-- above this) or if no hierarchy information is passed in, then it
-- will do the looping at a dept level for the system.
-------------------------------------------------------------------------
FUNCTION PROCESS_PROD(O_error_message     IN OUT  VARCHAR2,
                      I_record_passed     IN      VARCHAR2,
                      I_cycle_count       IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                      I_location_type     IN      STAKE_HEAD.LOC_TYPE%TYPE,
                      I_dept              IN      STAKE_PRODUCT.DEPT%TYPE,
                      I_class             IN      STAKE_PRODUCT.CLASS%TYPE,
                      I_subclass          IN      STAKE_PRODUCT.SUBCLASS%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------
-- This function is similar to PROCESS_PROD in that it will validate
-- all necessary information against the system and then create records
-- to be inserted into the STAKE_LOCATION table.  This function will
-- also process either one record at a time or loop through the locations
-- in the system according to the location type passed in.
-------------------------------------------------------------------------
FUNCTION PROCESS_LOC(O_error_message  IN OUT  VARCHAR2,
                     I_record_passed  IN      VARCHAR2,
                     I_cycle_count    IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                     I_location_type  IN      STAKE_HEAD.LOC_TYPE%TYPE,
                     I_location       IN      STAKE_LOCATION.LOCATION%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------
-- This function will serve to reset all package variables prior to
-- any processing
-------------------------------------------------------------------------
FUNCTION INIT(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------
-- This function will write records created in CREATE_SH_REC,
-- PROCESS_PROD and PROCESS_LOC to the appropriate tables.
-------------------------------------------------------------------------
FUNCTION FLUSH(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------
-- This function will handle the deletion messages that are received
-- by validating necessary information and then updating the
-- STAKE_HEAD table to signify a deletion for the passed in stock count.
-------------------------------------------------------------------------
FUNCTION PROCESS_DEL(O_error_message  IN OUT  VARCHAR2,
                     I_cycle_count    IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
END STAKE_SCHEDULE_SQL;
/
