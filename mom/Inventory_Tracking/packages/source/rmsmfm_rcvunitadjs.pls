CREATE OR REPLACE PACKAGE RMSMFM_RCVUNITADJ AUTHID CURRENT_USER AS

TYPE rcvunitadj_key_rec IS RECORD(ORDER_NO SHIPMENT.ORDER_NO%TYPE,      
                                  ASN      SHIPMENT.ASN%TYPE,      
                                  LOCATION SHIPMENT.TO_LOC%TYPE,      
                                  LOC_TYPE SHIPMENT.TO_LOC_TYPE%TYPE,      
                                  ITEM     SHIPSKU.ITEM%TYPE,      
                                  CARTON   SHIPSKU.CARTON%TYPE,
                                  ADJ_QTY  RUA_MFQUEUE.ADJ_QTY%TYPE);      
 
/*--- message type parameters ---*/

FAMILY                CONSTANT        RIB_SETTINGS.FAMILY%TYPE := 'rcvunitadj';

RCVUNITADJ_ADD        CONSTANT        VARCHAR2(15)             := 'rcvunitadjcre';

---------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg     IN OUT  VARCHAR2,
                I_message_type  IN      VARCHAR2,
                I_business_obj  IN      RCVUNITADJ_KEY_REC)
RETURN BOOLEAN;
---------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1);
---------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_ref_object      IN           RIB_OBJECT);
---------------------------------------------------------------------------
END RMSMFM_RCVUNITADJ;
/
