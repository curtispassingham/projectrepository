CREATE OR REPLACE PACKAGE BODY WF_ALLOC_SQL AS
-----------------------------------------------------------------------------------------
FUNCTION SYNC_F_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_wf_order_nos    IN OUT   OBJ_ALLOC_LINKED_F_ORD_TBL,
                      O_alloc_status    IN OUT   ALLOC_HEADER.STATUS%TYPE,
                      I_action_type     IN       VARCHAR2,
                      I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                      I_alloc_status    IN       ALLOC_HEADER.STATUS%TYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(50)                  := 'WF_ALLOC_SQL.SYNC_F_ORDER';
   L_inv_parm                   VARCHAR2(30)                  := NULL;
   L_alloc_status               ALLOC_HEADER.STATUS%TYPE;
   L_f_order_head_rec           OBJ_F_ORDER_HEAD_REC          := OBJ_F_ORDER_HEAD_REC();
   L_f_order_detail_tbl         OBJ_F_ORDER_DETAIL_TBL        := OBJ_F_ORDER_DETAIL_TBL();
   L_wf_customer_id             WF_CUSTOMER.WF_CUSTOMER_ID%TYPE;
   L_wf_order_no                WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_new_wf_order_no            WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_detail_action_type         VARCHAR2(10);
   L_alloc_credit_ind           WF_CUSTOMER.CREDIT_IND%TYPE;
   L_detail_credit_ind          WF_CUSTOMER.CREDIT_IND%TYPE;
   L_alloc_linked_f_ord_tbl     OBJ_ALLOC_LINKED_F_ORD_TBL    := OBJ_ALLOC_LINKED_F_ORD_TBL();
   L_f_order_status             WF_ORDER_HEAD.STATUS%TYPE     := NULL;
   L_table                      VARCHAR2(50)                  := NULL;
   -- variable defined for post approval franchise order creation to handle ordered quantity as 0 and status update to 'P'. 
   L_new_f_order_post_approval  VARCHAR2(1);

   cursor C_CHECK_ALLOC is
      select status
        from alloc_header
       where alloc_no = I_alloc_no;

   cursor C_GET_DISTINCT_CUSTOMER is
      select wc.wf_customer_id,
             wc.credit_ind
        from store s,
             alloc_detail ad,
             wf_customer wc
       where ad.alloc_no      = I_alloc_no
         and ad.to_loc        = s.store
         and ad.to_loc_type   = 'S'
         and s.store_type     = 'F'
         and s.wf_customer_id = wc.wf_customer_id
    group by wc.wf_customer_id, wc.credit_ind;

   TYPE distinct_customer_TBL is TABLE of C_GET_DISTINCT_CUSTOMER%ROWTYPE;
   L_distinct_customer_TBL distinct_customer_TBL;

   cursor C_GET_ALLOC_HEAD is
      select OBJ_F_ORDER_HEAD_REC(L_wf_order_no,                                                           -- WF_ORDER_NO
                                  'A',                                                                     -- ORDER_TYPE 'AUTO'
                                  NULL,                                                                    -- CURRENCY CODE
                                  L_f_order_status,                                                        -- STATUS
                                  NULL,                                                                    -- CANCEL REASON
                                  ah.comment_desc,                                                         -- COMMENTS
                                  L_f_order_detail_tbl)                                                    -- F_ORDER_DETAILS
        from alloc_header ah
       where ah.alloc_no = I_alloc_no;

   cursor C_GET_ALLOC_DETAIL is
      select OBJ_F_ORDER_DETAIL_REC( ad.wf_order_no,                                                       -- WF_ORDER_NO
                                     ah.item,                                                              -- ITEM
                                     'WH',                                                                 -- SOURCE_LOC_TYPE
                                     ah.wh,                                                                -- SOURCE_LOC_ID
                                     ad.to_loc,                                                            -- CUSTOMER_LOC
                                     DECODE(L_new_f_order_post_approval, 'Y', 0, ad.qty_allocated),        -- REQUESTED_QTY
                                     NULL,                                                                 -- CANCEL_REASON
                                     GREATEST(NVL(ad.in_store_date,get_vdate),get_vdate),                  -- NEED DATE
                                     GREATEST(NVL(ad.in_store_date,get_vdate+1), get_vdate + 1))           -- NOT_AFTER_DATE
      from  alloc_header ah,
            alloc_detail ad,
            store s
      where ah.alloc_no      = ad.alloc_no
        and ah.alloc_no      = I_alloc_no
        and ad.to_loc        = s.store
        and ad.to_loc_type   = 'S'
        and s.store_type     = 'F'
        and s.wf_customer_id = L_wf_customer_id;

   cursor C_GET_ALLOC_LINKED_F_ORDER is
      select OBJ_ALLOC_LINKED_F_ORD_REC(I_alloc_no,
                                        customer_loc,
                                        'S',
                                        L_wf_order_no)
        from TABLE(CAST(L_f_order_detail_tbl AS OBJ_F_ORDER_DETAIL_TBL));

BEGIN

   if I_alloc_no is NULL then
      L_inv_parm := 'I_alloc_no';
   elsif I_action_type is NULL then
      L_inv_parm := 'I_action_type';
   elsif I_alloc_status is NULL and I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE then
      L_inv_parm := 'I_alloc_status';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_action_type not in (RMS_CONSTANTS.WF_ACTION_CREATE, RMS_CONSTANTS.WF_ACTION_UPDATE, RMS_CONSTANTS.WF_ACTION_SHIPPED) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE',
                                            I_action_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Initialise O_wf_order_nos and O_alloc_status
   O_wf_order_nos := OBJ_ALLOC_LINKED_F_ORD_TBL();
   -- O_alloc_status be be same as Input alloc_status except for failed credit check.
   O_alloc_status := I_alloc_status;

   -- Validate that the Allocation number exists.
   L_table := 'ALLOC_HEADER';
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   open C_CHECK_ALLOC;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   fetch C_CHECK_ALLOC into L_alloc_status;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   close C_CHECK_ALLOC;

   if L_alloc_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ALLOC_NUM',
                                            I_alloc_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- The franchise order will only be created on Approval of allocation and updates are taken
   -- if the allocation status is Approved.
   -- If the allocation is in worksheet/Reserved/Closed status, ignore the request.
   if (I_alloc_status is NOT NULL and I_alloc_status <> 'A') or
      (I_alloc_status is NULL and L_alloc_status <> 'A' and I_action_type in (RMS_CONSTANTS.WF_ACTION_UPDATE, RMS_CONSTANTS.WF_ACTION_SHIPPED)) then
      return TRUE;
   end if;

   -- Identify the number of franchise order mapped to the allocation based on WF customer for the locations.
   L_table := 'ALLOC_DETAIL, STORE, WF_CUSTOMER';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_DISTINCT_CUSTOMER', L_table, I_alloc_no);
   open C_GET_DISTINCT_CUSTOMER;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_DISTINCT_CUSTOMER', L_table, I_alloc_no);
   fetch C_GET_DISTINCT_CUSTOMER BULK COLLECT into L_distinct_customer_tbl;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_DISTINCT_CUSTOMER', L_table, I_alloc_no);
   close C_GET_DISTINCT_CUSTOMER;

   if L_distinct_customer_tbl is NOT NULL and L_distinct_customer_tbl.COUNT > 0 then

      -- Check if any of the customer fails credit check.
      for i in L_distinct_customer_tbl.FIRST..L_distinct_customer_tbl.LAST LOOP
         if L_distinct_customer_tbl(i).credit_ind = 'N' then
            L_alloc_credit_ind := 'N';
         end if;
      end LOOP;
      -- end if;

      -- Define the Franchise order status and also output allocation status.
      if I_alloc_status = 'A' and L_alloc_credit_ind = 'N' and
         (I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE or (I_action_type = RMS_CONSTANTS.WF_ACTION_UPDATE and L_alloc_status in ('W','R')))then
         O_alloc_status   := L_alloc_status;      -- Retain existing status as in alloc_header.
         L_f_order_status := WF_STATUS_REQ_CREDIT;
      elsif I_action_type = RMS_CONSTANTS.WF_ACTION_SHIPPED then
         L_f_order_status := WF_STATUS_APPROVE;    -- Treat same as update. WF_BOL_SQL will update status to 'P'
      elsif I_alloc_status = 'A' then
         L_f_order_status := WF_STATUS_APPROVE;
      elsif I_alloc_status is NULL and L_alloc_status = 'A' then
         L_f_order_status := WF_STATUS_APPROVE;
      end if;

      FOR i in L_distinct_customer_tbl.FIRST..L_distinct_customer_tbl.LAST LOOP
         L_wf_customer_id            := L_distinct_customer_tbl(i).wf_customer_id;
         L_new_f_order_post_approval := 'N';

         -- Get the wf_order_no for the alloc_no/franchise customer.
         -- For allocation for which wf_order_no has not be created yet, wf_order_no will be fetched as NULL.
         -- For update and shipment request post franchise order create, wf_order_no will be populated except for special handling for
         -- For update and shipment for a allocation where new allocation detail line has been added post initial create
         --   if the new location is part of an existing franchise customer in the allocation, pick the wf_order_no for that customer
         --   if the new location is part of a franchise customer which is not in the allocation, wf_order_no will be null and it require a new franchise order to be created. 
         select MIN(wf_order_no) into L_wf_order_no
           from alloc_detail ad,
                store s
          where ad.alloc_no      = I_alloc_no
            and ad.to_loc        = s.store
            and ad.to_loc_type   = 'S'
            and s.store_type     = 'F'
            and s.wf_customer_id = L_wf_customer_id;

         if I_action_type in (RMS_CONSTANTS.WF_ACTION_UPDATE, RMS_CONSTANTS.WF_ACTION_SHIPPED) and L_wf_order_no is NULL then
            -- Update/Shipped request has a new allocation detail - franchise location belonging to a customer
            -- for which franchise order is not present. For action type as Update, check customer credit 
            -- but for shipped action type, do not check for credit approval as the inventory has already 
            -- been shipped
            
            if L_alloc_status not in  ('W','R') and I_action_type = RMS_CONSTANTS.WF_ACTION_UPDATE then
               
               if WF_CUSTOMER_SQL.CHECK_WF_CUST_CREDIT (O_error_message,
                                                        L_detail_credit_ind,
                                                        L_wf_customer_id) = false then
                  return false;
               end if;

               -- Post approval, franchise order can't be created in "R" Require credit approval status and hence reject.
               if L_detail_credit_ind = 'N' then
                  O_error_message := SQL_LIB.CREATE_MSG('F_CREDIT_CHECK_FAILED',
                                                        I_alloc_no,
                                                        L_wf_customer_id,
                                                        NULL);
                  return FALSE;
               end if;
            end if;
            
            -- Changing the action type to create for the call to WF_CREATE_ORDER_SQL.PROCESS_F_ORDER. 
            L_detail_action_type := RMS_CONSTANTS.WF_ACTION_CREATE;
            
            -- Post approval, the franchise order should be created with 0 order quantity. 
            if L_alloc_status not in  ('W','R') then
               L_new_f_order_post_approval := 'Y';
            end if;
         elsif I_action_type = RMS_CONSTANTS.WF_ACTION_SHIPPED then
            -- Action type Shipped will sync franchise order for any detail level unexpected item or location shipment. Hence 
            -- changing the action type to Update for syncing.
            L_detail_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
         else
            L_detail_action_type := I_action_type;
         end if;

         L_table := 'ALLOC_HEADER, ALLOC_DETAIL, STORE';
         SQL_LIB.SET_MARK('OPEN', 'C_GET_ALLOC_DETAIL', L_table, I_alloc_no);
         open C_GET_ALLOC_DETAIL;
         SQL_LIB.SET_MARK('FETCH', 'C_GET_ALLOC_DETAIL', L_table, I_alloc_no);
         fetch C_GET_ALLOC_DETAIL BULK COLLECT into L_f_order_detail_tbl;
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_ALLOC_DETAIL', L_table, I_alloc_no);
         close C_GET_ALLOC_DETAIL;

         L_table := 'ALLOC_HEADER';
         SQL_LIB.SET_MARK('OPEN', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
         open C_GET_ALLOC_HEAD;
         SQL_LIB.SET_MARK('FETCH', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
         fetch C_GET_ALLOC_HEAD into L_f_order_head_rec;
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
         close C_GET_ALLOC_HEAD;
         
         -- Actual franchise order create/update is done by WF_CREATE_ORDER_SQL.
         if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                                L_new_wf_order_no,    -- returned for a newly created
                                                L_f_order_head_rec,
                                                L_detail_action_type) = FALSE then
            return FALSE;
         end if;

         -- Return the collection of Alloc_no/To Loc/WF Order number
         -- For new franchise order get the wf_order_no into local variable.
         -- Append to the output collection the wf_order_no for every wf_customer_id
         if L_wf_order_no is NULL then
            L_wf_order_no := L_new_wf_order_no;
         end if;

         L_table := 'OBJ_F_ORDER_DETAIL_TBL';
         SQL_LIB.SET_MARK('OPEN', 'C_GET_ALLOC_LINKED_F_ORDER', L_table, NULL);
         open C_GET_ALLOC_LINKED_F_ORDER;
         SQL_LIB.SET_MARK('FETCH', 'C_GET_ALLOC_LINKED_F_ORDER', L_table, NULL);
         fetch C_GET_ALLOC_LINKED_F_ORDER BULK COLLECT into L_alloc_linked_f_ord_tbl;
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_ALLOC_LINKED_F_ORDER', L_table, NULL);
         close C_GET_ALLOC_LINKED_F_ORDER;

         O_wf_order_nos := O_wf_order_nos MULTISET UNION L_alloc_linked_f_ord_tbl;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_ALLOC%ISOPEN then
         close C_CHECK_ALLOC;
      end if;

      if C_GET_ALLOC_DETAIL%ISOPEN then
         close C_GET_ALLOC_DETAIL;
      end if;

      if C_GET_ALLOC_HEAD%ISOPEN then
         close C_GET_ALLOC_HEAD;
      end if;

      if C_GET_DISTINCT_CUSTOMER%ISOPEN then
         close C_GET_DISTINCT_CUSTOMER;
      end if;

      if C_GET_ALLOC_LINKED_F_ORDER%ISOPEN then
         close C_GET_ALLOC_LINKED_F_ORDER;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SYNC_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION CANCEL_F_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(50)                  := 'WF_ALLOC_SQL.CANCEL_F_ORDER';
   L_dummy                    NUMBER(1);
   L_f_order_head_tbl         OBJ_F_ORDER_HEAD_TBL          := OBJ_F_ORDER_HEAD_TBL();
   L_dummy_order_no           WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_table                    VARCHAR2(50)                  := NULL;

   cursor C_CHECK_ALLOC is
      select 1
        from alloc_header
       where alloc_no = I_alloc_no;

   cursor C_GET_ALLOC_HEAD is
      select OBJ_F_ORDER_HEAD_REC(wf_order_no,             -- wf_order_no
                                  NULL,                    -- Order_type
                                  NULL,                    -- Currency Code
                                  NULL,                    -- Status
                                  'ED',                    -- Cancel Reason
                                  NULL,                    -- Comments
                                  NULL)                    -- F_ORDER_DETAILS
        from alloc_detail ad
       where ad.alloc_no = I_alloc_no
         and wf_order_no is not NULL
      group by wf_order_no;

BEGIN

   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_alloc_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Validate that the Allocation number exists.
   L_table := 'ALLOC_HEADER';
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   open C_CHECK_ALLOC;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   fetch C_CHECK_ALLOC into L_dummy;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   close C_CHECK_ALLOC;

   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ALLOC_NUM',
                                            I_alloc_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_table := 'ALLOC_DETAIL';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
   open C_GET_ALLOC_HEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
   fetch C_GET_ALLOC_HEAD BULK COLLECT into L_f_order_head_tbl;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
   close C_GET_ALLOC_HEAD;

   if L_f_order_head_tbl is NOT NULL and L_f_order_head_tbl.COUNT > 0 then
      FOR i in L_f_order_head_tbl.FIRST..L_f_order_head_tbl.LAST LOOP
         if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                                L_dummy_order_no,
                                                L_f_order_head_tbl(i),
                                                RMS_CONSTANTS.WF_ACTION_CANCEL) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_ALLOC_HEAD%ISOPEN then
         close C_GET_ALLOC_HEAD;
      end if;

      if C_CHECK_ALLOC%ISOPEN then
         close C_CHECK_ALLOC;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CANCEL_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION SYNC_DELETED_ALLOC_LINES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                                  I_wf_order_no_tbl IN       OBJ_WF_ORDER_NO_TBL)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(50)                  := 'WF_ALLOC_SQL.SYNC_DELETED_ALLOC_LINES';

   TYPE WF_ORDER_NO_TBL is TABLE of WF_ORDER_HEAD.WF_ORDER_NO%TYPE  INDEX BY BINARY_INTEGER;
   L_wf_order_no_tbl          WF_ORDER_NO_TBL;

   -- Dummy variables
   L_dummy                    WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_alloc_status             ALLOC_HEADER.STATUS%TYPE      := NULL;
   L_wf_order_nos             OBJ_ALLOC_LINKED_F_ORD_TBL    := OBJ_ALLOC_LINKED_F_ORD_TBL();

   L_table                    VARCHAR2(50)                  := NULL;

   cursor C_GET_DETETED_WF_ORDER is
      select distinct value(wfo)
       from TABLE(CAST(I_wf_order_no_tbl AS OBJ_WF_ORDER_NO_TBL)) wfo
      where not exists (select 1
                          from alloc_detail ad
                         where ad.alloc_no = I_alloc_no
                           and ad.wf_order_no = value(wfo));

BEGIN

   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_alloc_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_table := 'OBJ_WF_ORDER_NO_TBL';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_DETETED_WF_ORDER', L_table, I_alloc_no);
   open C_GET_DETETED_WF_ORDER;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_DETETED_WF_ORDER', L_table, I_alloc_no);
   fetch C_GET_DETETED_WF_ORDER BULK COLLECT into L_wf_order_no_tbl;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_DETETED_WF_ORDER', L_table, I_alloc_no);
   close C_GET_DETETED_WF_ORDER;

   if L_wf_order_no_tbl is NOT NULL and L_wf_order_no_tbl.COUNT > 0 then
      for i in L_wf_order_no_tbl.FIRST..L_wf_order_no_tbl.LAST LOOP
         if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                                L_dummy,
                                                OBJ_F_ORDER_HEAD_REC( L_wf_order_no_tbl(i),    -- wf_order_no
                                                                      NULL,                    -- Order_type
                                                                      NULL,                    -- Currency Code
                                                                      NULL,                    -- Status
                                                                      NULL,                    -- Cancel Reason
                                                                      NULL,                    -- Comments
                                                                      NULL),                   -- F_ORDER_DETAILS,
                                                RMS_CONSTANTS.WF_ACTION_DELETE) = FALSE then
            return FALSE;
         end if;
      end LOOP;
   end if;

   -- Update the franchise order where the allocation line item delete results in partial franchaise order delete.
   -- As this function will be called for delete location scenarios, no new wf order will be created or change in
   -- status. Hence the returned L_alloc_status and L_wf_order_nos will be ignored.
   if WF_ALLOC_SQL.SYNC_F_ORDER(O_error_message,
                                L_wf_order_nos,
                                L_alloc_status,
                                RMS_CONSTANTS.WF_ACTION_UPDATE,
                                I_alloc_no,
                                NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_DETETED_WF_ORDER%ISOPEN then
         close C_GET_DETETED_WF_ORDER;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SYNC_DELETED_ALLOC_LINES;
---------------------------------------------------------------------------------------------
FUNCTION BULK_SYNC_F_ORDER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_wf_order_nos        IN OUT   OBJ_ALLOC_LINKED_F_ORD_TBL,
                           O_alloc_status_tbl    IN OUT   OBJ_ALLOC_STATUS_TBL,
                           I_alloc_status_tbl    IN       OBJ_ALLOC_STATUS_TBL,
                           I_action_type         IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(50)                  := 'WF_ALLOC_SQL.BULK_SYNC_F_ORDER';
   L_alloc_linked_f_ord_tbl   OBJ_ALLOC_LINKED_F_ORD_TBL    := OBJ_ALLOC_LINKED_F_ORD_TBL();
   L_alloc_status_tbl         OBJ_ALLOC_STATUS_TBL          := OBJ_ALLOC_STATUS_TBL();
   L_alloc_status             ALLOC_HEADER.STATUS%TYPE;

BEGIN

   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_action_type not in (RMS_CONSTANTS.WF_ACTION_CREATE, RMS_CONSTANTS.WF_ACTION_UPDATE, RMS_CONSTANTS.WF_ACTION_SHIPPED)then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE',
                                            I_action_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- initialise
   O_alloc_status_tbl := OBJ_ALLOC_STATUS_TBL();
   O_wf_order_nos := OBJ_ALLOC_LINKED_F_ORD_TBL();

   -- Loop through the input collection and call SYNC_F_ORDER.
   -- This loop should be removed when WF_CREATE_ORDER_SQL supports bulk processing.
   if I_alloc_status_tbl is NOT NULL and I_alloc_status_tbl.COUNT > 0 then
      for i in I_alloc_status_tbl.FIRST..I_alloc_status_tbl.LAST LOOP
         if WF_ALLOC_SQL.SYNC_F_ORDER (O_error_message,
                                       L_alloc_linked_f_ord_tbl,
                                       L_alloc_status,
                                       I_action_type,
                                       I_alloc_status_tbl(i).alloc_no,
                                       I_alloc_status_tbl(i).status) = FALSE then
            return FALSE;
         end if;

         -- Copy the allocation number and output status into the output collection.
         L_alloc_status_tbl.extend;

         L_alloc_status_tbl(L_alloc_status_tbl.LAST) := OBJ_ALLOC_STATUS_REC(I_alloc_status_tbl(i).alloc_no,
                                                                             L_alloc_status);

         -- Append to the output alloc_no to wf_order_no collection
         O_wf_order_nos := O_wf_order_nos MULTISET UNION L_alloc_linked_f_ord_tbl;

      end LOOP;
      O_alloc_status_tbl := L_alloc_status_tbl;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BULK_SYNC_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION DELETE_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(50)                  := 'WF_ALLOC_SQL.DELETE_F_ORDER';
   L_dummy            NUMBER(1);
   L_f_order_head_tbl OBJ_F_ORDER_HEAD_TBL          := OBJ_F_ORDER_HEAD_TBL();
   L_dummy_order_no   WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_table            VARCHAR2(50)                  := NULL;
   RECORD_LOCKED              EXCEPTION;
   PRAGMA EXCEPTION_INIT(Record_Locked, -54);   
   
   cursor C_CHECK_ALLOC is
      select 1
        from alloc_header
       where alloc_no = I_alloc_no;

   cursor C_GET_ALLOC_HEAD is
      select OBJ_F_ORDER_HEAD_REC(wf_order_no,             -- wf_order_no
                                  NULL,                    -- Order_type
                                  NULL,                    -- Currency Code
                                  NULL,                    -- Status
                                  NULL,                    -- Cancel Reason
                                  NULL,                    -- Comments
                                  NULL)                    -- F_ORDER_DETAILS
        from alloc_detail ad
       where ad.alloc_no = I_alloc_no
         and wf_order_no is not NULL
      group by wf_order_no;

   cursor C_LOCK_ALLOC_DETAIL is
      select 'x'
        from alloc_detail
       where alloc_no = I_alloc_no
         and wf_order_no is not NULL
         for update nowait;

BEGIN

   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_alloc_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Validate that the Allocation number exists.
   L_table := 'ALLOC_HEADER';
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   open C_CHECK_ALLOC;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   fetch C_CHECK_ALLOC into L_dummy;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ALLOC', L_table, I_alloc_no);
   close C_CHECK_ALLOC;

   if L_dummy is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ALLOC_NUM',
                                            I_alloc_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_table := 'ALLOC_DETAIL';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
   open C_GET_ALLOC_HEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
   fetch C_GET_ALLOC_HEAD BULK COLLECT into L_f_order_head_tbl;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ALLOC_HEAD', L_table, I_alloc_no);
   close C_GET_ALLOC_HEAD;

   if L_f_order_head_tbl is NOT NULL and L_f_order_head_tbl.COUNT > 0 then
      
      L_table := 'ALLOC_DETAIL';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ALLOC_DETAIL', L_table, I_alloc_no);
      open C_GET_ALLOC_HEAD;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ALLOC_DETAIL', L_table, I_alloc_no);
      close C_GET_ALLOC_HEAD;      

      -- Update alloc_detail to remove the refernce to wf_order_no
      update alloc_detail
         set wf_order_no = NULL
       where alloc_no = I_alloc_no
         and wf_order_no is not NULL;
      
      FOR i in L_f_order_head_tbl.FIRST..L_f_order_head_tbl.LAST LOOP
         if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                                L_dummy_order_no,
                                                L_f_order_head_tbl(i),
                                                RMS_CONSTANTS.WF_ACTION_DELETE) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ALLOC_DETAIL%ISOPEN then
         close C_LOCK_ALLOC_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_alloc_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_GET_ALLOC_HEAD%ISOPEN then
         close C_GET_ALLOC_HEAD;
      end if;

      if C_CHECK_ALLOC%ISOPEN then
         close C_CHECK_ALLOC;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION REPL_SYNC_F_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_date          IN     ORDHEAD.WRITTEN_DATE%TYPE,
                           I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)               := 'WF_ALLOC_SQL.REPL_SYNC_F_ORDER';

   L_alloc_list       OBJ_ALLOC_STATUS_TBL       := OBJ_ALLOC_STATUS_TBL();
   L_wf_order_nos     OBJ_ALLOC_LINKED_F_ORD_TBL := OBJ_ALLOC_LINKED_F_ORD_TBL();
   L_alloc_status_tbl OBJ_ALLOC_STATUS_TBL       := OBJ_ALLOC_STATUS_TBL();

   cursor C_ALLOC_LIST is
      select OBJ_ALLOC_STATUS_REC(ah.alloc_no,
                                  ah.status)
        from ordhead oh,
             alloc_header ah
       where oh.written_date = NVL(I_date, oh.written_date)
         and oh.order_no     = NVL(I_order_no, ah.order_no)
         and oh.orig_ind     = 0           /*Replenishment Orders*/
         and oh.order_no     = ah.order_no
         and ah.status       = 'A'         /*Only Approved Allocations will be syncrhonized.*/
         and NOT EXISTS (select 'X'        /*In case the Alloc already has a wf_order_no (in case the replenishment wf sync batch is rerun or orhead.fmb approves the order more than once).*/
                           from alloc_detail ad
                          where ad.alloc_no    = ah.alloc_no
                            and ad.wf_order_no is NOT NULL);

BEGIN

   open C_ALLOC_LIST;
   fetch C_ALLOC_LIST BULK COLLECT into L_alloc_list;
   close C_ALLOC_LIST;

   /*Call function to WF orders for the allocations if applicable.*/
   if WF_ALLOC_SQL.BULK_SYNC_F_ORDER(O_error_message,
                                     L_wf_order_nos,
                                     L_alloc_status_tbl,
                                     L_alloc_list,
                                     RMS_CONSTANTS.WF_ACTION_CREATE) = FALSE then
      return FALSE;
   end if;

   /*Update Alloc_detail with the wf_order_no created in BULK_SYNC_F_ORDER.*/
   merge into alloc_detail target
   using(select wfo.alloc_no,
                wfo.to_loc,
                wfo.to_loc_type,
                wfo.wf_order_no
           from table(cast(L_wf_order_nos AS OBJ_ALLOC_LINKED_F_ORD_TBL)) wfo) source
   on (    target.alloc_no    = source.alloc_no
       and target.to_loc_type = source.to_loc_type
       and target.to_loc      = source.to_loc)
   when MATCHED then
      update
         set target.wf_order_no = source.wf_order_no;

   /*Update alloc header with the new status returned by WF_ALLOC_SQL.BULK_SYNC_F_ORDER.*/
   merge into alloc_header target
   using(select als.alloc_no,
                als.status
           from table(cast(L_alloc_status_tbl AS OBJ_ALLOC_STATUS_TBL)) als) source
   on (    target.alloc_no = source.alloc_no)
   when MATCHED then
      update
         set target.status = source.status;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END REPL_SYNC_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION APPROVE_LINKED_ALLOC_F_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_wf_order_no   IN     WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)               := 'WF_ALLOC_SQL.APPROVE_LINKED_ALLOC_F_ORDER';
   L_alloc_no           ALLOC_HEADER.ALLOC_NO%TYPE;
   L_wf_order_no        WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_alloc_approve_ind  VARCHAR2(1);   
   L_table              VARCHAR2(50);
   L_key                VARCHAR2(64);
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   CURSOR C_GET_LINKED_ALLOC is 
      select alloc_no
        from alloc_detail
       where wf_order_no = I_wf_order_no
         and rownum = 1;

   CURSOR C_GET_LINKED_WF_ORDER is 
      select wf_order_no
        from alloc_detail
       where alloc_no = L_alloc_no
         and wf_order_no is NOT NULL
       group by wf_order_no;

   CURSOR C_LOCK_WF_ORDER_HEAD is
      select 'x'
        from wf_order_head
       where wf_order_no = L_wf_order_no
         for update nowait;

   CURSOR C_LOCK_ALLOC_HEADER is
      select 'x'
        from alloc_header
       where alloc_no = L_alloc_no
         for update nowait;       
BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if WF_ALLOC_SQL.CHECK_ALLOC_APPROVAL(O_error_message,
                                        L_alloc_approve_ind,
                                        I_wf_order_no) = FALSE then
      return FALSE;
   end if;      
   
   open C_GET_LINKED_ALLOC;
   fetch C_GET_LINKED_ALLOC into L_alloc_no;
   close C_GET_LINKED_ALLOC;

   if L_alloc_no is not NULL and L_alloc_approve_ind = 'Y' then

      -- Update the reserved and expected quantities for the entire allocation
      if ALLOC_ATTRIB_SQL.UPD_ALLOC_RESV_EXP(O_error_message,
                                             L_alloc_no,
                                             'A') = FALSE then
         return FALSE;
      end if;

      -- Approve all the linked Franchise order and then approve the allocation. 
      for rec in C_GET_LINKED_WF_ORDER LOOP
         L_wf_order_no := rec.wf_order_no;
         
         -- Input franchsie order updates will be handled by the calling form.
         if I_wf_order_no <> L_wf_order_no then
            L_table := 'WF_ORDER_HEAD';
            L_key := 'wf_order_no: ' || TO_CHAR(L_wf_order_no);
            open C_LOCK_WF_ORDER_HEAD;
            close C_LOCK_WF_ORDER_HEAD;

            -- There could be changes to the pricing cost at the franchise location from the date of initial
            -- order creation and now. Hence update the pricing cost of the all the franchise order before approval.  
            if WF_ORDER_SQL.REAPPLY_CUSTOMER_COST(O_error_message,
                                                  L_wf_order_no) = FALSE then
               return FALSE;
            end if;
            
            update wf_order_head
               set status = WF_STATUS_APPROVE,
                   approval_date = sysdate,
                   last_update_datetime = sysdate,
                   last_update_id = get_user
             where wf_order_no = L_wf_order_no;
         end if;
         
      end LOOP;      
      
      L_table := 'ALLOC_HEADER';
      L_key := 'alloc_no: ' || TO_CHAR(L_alloc_no);
      open C_LOCK_ALLOC_HEADER;
      close C_LOCK_ALLOC_HEADER;
      
      update alloc_header
         set status = 'A'
       where alloc_no = L_alloc_no;
   end if;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END APPROVE_LINKED_ALLOC_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION CHECK_ALLOC_APPROVAL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_alloc_approve_ind    IN OUT VARCHAR2, 
                              I_wf_order_no          IN     WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                       := 'WF_ALLOC_SQL.CHECK_ALLOC_APPROVAL';
   L_alloc_no         ALLOC_HEADER.ALLOC_NO%TYPE;
   L_alloc_status     ALLOC_HEADER.STATUS%TYPE;
   L_customer_id      WF_CUSTOMER.WF_CUSTOMER_ID%TYPE;
   L_wf_order_no      WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   
   CURSOR C_GET_LINKED_ALLOC is 
      select ah.alloc_no, 
             ah.status
        from alloc_detail ad,
             alloc_header ah
       where ad.wf_order_no = I_wf_order_no
         and ah.alloc_no = ad.alloc_no
         and rownum = 1;

   CURSOR C_CHECK_CREDIT is
      select wc.wf_customer_id, 
             ad.wf_order_no
        from alloc_detail ad,
             store s,
             wf_customer wc
       where ad.alloc_no      = L_alloc_no
         and ad.wf_order_no is NOT NULL
         and ad.to_loc        = s.store
         and s.wf_customer_id = wc.wf_customer_id
         and wc.credit_ind    = 'N'
         and rownum = 1;
        
BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_LINKED_ALLOC;
   fetch C_GET_LINKED_ALLOC into L_alloc_no, L_alloc_status;
   close C_GET_LINKED_ALLOC;
   
   if L_alloc_no is not NULL then
      if L_alloc_status <> 'W' then
         O_error_message := SQL_LIB.CREATE_MSG('WF_LINK_ALLOC_STATUS',
                                               I_wf_order_no,
                                               L_alloc_no,
                                               NULL);
         return FALSE;
      end if;
      
      open C_CHECK_CREDIT;
      fetch C_CHECK_CREDIT into L_customer_id, L_wf_order_no;
      close C_CHECK_CREDIT;
      
      -- If the allocation cannot be approved, throw an error to be displayed to the user.
      if L_customer_id is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('WF_LINK_ALLOC_APPRV_FAIL',
                                               I_wf_order_no,
                                               L_wf_order_no,
                                               L_customer_id);
         return FALSE;
      end if;
      O_alloc_approve_ind := 'Y';
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ALLOC_APPROVAL;
-----------------------------------------------------------------------------------------
FUNCTION XDOCK_SYNC_F_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_date          IN     ORDHEAD.WRITTEN_DATE%TYPE,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)               := 'WF_ALLOC_SQL.XDOCK_SYNC_F_ORDER';

   L_alloc_list       OBJ_ALLOC_STATUS_TBL       := OBJ_ALLOC_STATUS_TBL();
   L_wf_order_nos     OBJ_ALLOC_LINKED_F_ORD_TBL := OBJ_ALLOC_LINKED_F_ORD_TBL();
   L_alloc_status_tbl OBJ_ALLOC_STATUS_TBL       := OBJ_ALLOC_STATUS_TBL();

   cursor C_ALLOC_LIST is
      select OBJ_ALLOC_STATUS_REC(ah.alloc_no,
                                  'A')
        from ordhead oh,
             alloc_header ah
       where oh.written_date = NVL(I_date, oh.written_date)
         and oh.order_no     = NVL(I_order_no, ah.order_no)
         and oh.orig_ind     != 0          
         and oh.order_no     = ah.order_no
         and NOT EXISTS (select 'X'        
                           from alloc_detail ad
                          where ad.alloc_no    = ah.alloc_no
                            and ad.wf_order_no is NOT NULL);

BEGIN

   open C_ALLOC_LIST;
   fetch C_ALLOC_LIST BULK COLLECT into L_alloc_list;
   close C_ALLOC_LIST;

   /*Call function to WF orders for the allocations if applicable.*/
   if WF_ALLOC_SQL.BULK_SYNC_F_ORDER(O_error_message,
                                     L_wf_order_nos,
                                     L_alloc_status_tbl,
                                     L_alloc_list,
                                     RMS_CONSTANTS.WF_ACTION_CREATE) = FALSE then
      return FALSE;
   end if;

   /*Update Alloc_detail with the wf_order_no created in BULK_SYNC_F_ORDER.*/
   merge into alloc_detail target
   using(select wfo.alloc_no,
                wfo.to_loc,
                wfo.to_loc_type,
                wfo.wf_order_no
           from table(cast(L_wf_order_nos AS OBJ_ALLOC_LINKED_F_ORD_TBL)) wfo) source
   on (    target.alloc_no    = source.alloc_no
       and target.to_loc_type = source.to_loc_type
       and target.to_loc      = source.to_loc)
   when MATCHED then
      update
         set target.wf_order_no = source.wf_order_no;

   /*Update alloc header with the new status returned by WF_ALLOC_SQL.BULK_SYNC_F_ORDER.*/
   merge into alloc_header target
   using(select als.alloc_no,
                als.status
           from table(cast(L_alloc_status_tbl AS OBJ_ALLOC_STATUS_TBL)) als) source
   on (    target.alloc_no = source.alloc_no)
   when MATCHED then
      update
         set target.status = source.status;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END XDOCK_SYNC_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION CANCEL_FRANCHISE_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cancel_reason IN     WF_ORDER_HEAD.CANCEL_REASON%TYPE,
                                I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)               := 'WF_ALLOC_SQL.CANCEL_FRANCHISE_ORDER';
   L_table              VARCHAR2(50);
   L_key                VARCHAR2(64);
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   CURSOR C_LOCK_WF_ORDER_DETAIL is 
      select 'x'
        from alloc_detail ad,
             wf_order_detail wod
       where ad.alloc_no = I_alloc_no
         and ad.wf_order_no is NOT NULL
         and ad.wf_order_no = wod.wf_order_no
         for update of wod.requested_qty;

   CURSOR C_LOCK_WF_ORDER_HEAD is
      select 'x'
        from alloc_detail ad,
             wf_order_head woh
       where ad.alloc_no = I_alloc_no
         and ad.wf_order_no is NOT NULL
         and ad.wf_order_no = woh.wf_order_no
         for update of woh.status;

   CURSOR C_LOCK_ALLOC_HEADER is
      select 'x'
        from alloc_header
       where alloc_no = I_alloc_no
         for update nowait;       
BEGIN

   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_alloc_no',L_program,NULL);
      return FALSE;
   end if;

   if I_cancel_reason is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_cancel_reason',L_program,NULL);
      return FALSE;
   end if;
   
   L_table := 'WF_ORDER_DETAIL';
   L_key   := NULL;
   open C_LOCK_WF_ORDER_DETAIL;
   close C_LOCK_WF_ORDER_DETAIL;

   L_table := 'WF_ORDER_HEAD';
   L_key   := NULL;
   open C_LOCK_WF_ORDER_HEAD;
   close C_LOCK_WF_ORDER_HEAD;

   L_table := 'ALLOC_HEADER';
   L_key   := 'alloc_no : ' || I_alloc_no;
   open C_LOCK_ALLOC_HEADER;
   close C_LOCK_ALLOC_HEADER;

   -- Update the requested quantity to 0 for all linked franchise order and update status to 'C'. 
   update wf_order_detail
      set requested_qty = 0,
          cancel_reason = I_cancel_reason,
          last_update_datetime = sysdate,
          last_update_id = get_user
    where wf_order_no in (select distinct wf_order_no 
                            from alloc_detail
                           where alloc_no = I_alloc_no
                             and wf_order_no is not NULL);

   update wf_order_head
      set status = 'C',
          cancel_reason = I_cancel_reason,
          last_update_datetime = sysdate,
          last_update_id = get_user
    where wf_order_no in (select distinct wf_order_no 
                            from alloc_detail
                           where alloc_no = I_alloc_no
                             and wf_order_no is not NULL);
      
   -- Close the allocation.                              
   update alloc_header
      set status = 'C'
    where alloc_no = I_alloc_no;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CANCEL_FRANCHISE_ORDER;
-----------------------------------------------------------------------------------------
END WF_ALLOC_SQL;
/
