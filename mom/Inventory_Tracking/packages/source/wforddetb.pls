CREATE OR REPLACE PACKAGE BODY WF_ORDER_DETAIL_SQL AS

LP_vdate   period.vdate%TYPE   := get_vdate;
LP_user    varchar2(50)        := get_user;

-------------------------------------------------------------------------------------------------
FUNCTION SYNC_CONTAINER_ITEM(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no         IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                             I_status              IN       WF_ORDER_HEAD.STATUS%TYPE,
                             I_item                IN       WF_ORDER_DETAIL.ITEM%TYPE,
                             I_source_loc_type     IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                             I_source_loc_id       IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                             I_customer_loc        IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                             I_old_qty             IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                             I_new_qty             IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                             I_need_date           IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
                             I_not_after_date      IN       WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE,
                             I_cancel_reason       IN       WF_ORDER_DETAIL.CANCEL_REASON%TYPE,
                             I_cancel_ind          IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION CANCEL_TSF_STORE_ORDER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                IN       WF_ORDER_DETAIL.ITEM%TYPE,
                                I_source_loc_type     IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                I_source_loc_id       IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                                I_customer_loc        IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                                I_need_date           IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
                                I_wf_order_no         IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                                I_wf_order_line_no    IN       WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE,
                                I_cancel_reason       IN       WF_ORDER_DETAIL.CANCEL_REASON%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--- Procedure Name: UPDATE_ORDER_INFO
--- Purpose       : This procedure will be used for updating the detail records for source type as
---                 warehouse. This function is called during editing and this will handle creating/
---                 updating/deleting linked warehouse sourced linked transfer or store orders.
---------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_INFO (IO_wf_order_rec           IN OUT   WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD,
                            I_status                  IN       WF_ORDER_HEAD.STATUS%TYPE,
                            I_old_order_qty           IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                            I_cancel_ind              IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
PROCEDURE GET_DEFAULT_WF_ORDER_INFO (O_wf_order_det   IN OUT   WF_ORDER_DETAIL_TAB,
                                     I_wf_order_id    IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
IS

   L_program   VARCHAR2(64)   := 'WF_ORDER_DETAIL_SQL.GET_DEFAULT_WF_ORDER_INFO';
   L_vat_rate  VAT_CODE_RATES.VAT_RATE%TYPE;
   L_vat_code  VAT_CODES.VAT_CODE%TYPE;

   cursor C_WF_ORD_DET is
      select wd.wf_order_no,
             wd.item,
             wd.item_desc,
             wd.source_loc_type,
             wd.source_loc_id,
             wd.customer_loc cust_loc,
             wd.need_date,
             wd.not_after_date,
             wd.uom,
             wd.requested_qty,
             wd.available_qty,
             wd.customer_cost_vat_excl customer_cost_vat_incl,
             wd.customer_cost_vat_excl * wd.requested_qty total_cust_cost_vat_incl,
             wd.source_loc_id_name,
             wd.cust_loc_name,
             wd.ref_item,
             wd.item_type,
             wd.cancel_reason,
             wd.acquisition_cost,
             wd.customer_cost_vat_excl,
             wd.customer_cost_vat_excl * wd.requested_qty total_cust_cost_vat_excl,
             0 vat_rate,
             0 vat_amt,
             wd.margin_pct,
             wd.linked_tsfpoalloc,
             wd.linked_entity_status,
             wd.fixed_cost,
             wd.order_currency,
             wd.ship_date,
             wd.ship_qty,
             wd.row_id,
             wd.return_code,
             wd.error_message,
             wd.pack_ind,
             wd.wf_order_line_no
        from (select wod.*,
                     rowidtochar(wod.rowid) row_id,
                     woh.currency_code order_currency,
                     0 vat_rate_pct,
                     im.item_desc item_desc,
                     im.standard_uom uom,
                     im.pack_ind pack_ind,
                     decode(wod.source_loc_type,'SU',NULL,GREATEST(ils.stock_on_hand - ( GREATEST(ils.tsf_reserved_qty,0)
                                                                                       + GREATEST(ils.customer_resv,0)
                                                                                       + GREATEST(ils.rtv_qty,0)
                                                                                       + GREATEST(ils.non_sellable_qty,0)),0))available_qty,
                     NVL(wod.fixed_cost,wod.customer_cost) customer_cost_vat_excl,
                     comp_loc.from_loc_name source_loc_id_name,
                     cust_loc.store_name cust_loc_name,
                     NULL ref_item,
                     NULL item_type,
                     tsf_det.linked_tsfpoalloc,
                     tsf_det.linked_entity_status,
                     tsf_det.ship_date,
                     tsf_det.ship_qty,
                     NULL return_code,
                     NULL error_message
                from wf_order_head woh,
                     wf_order_detail wod,
                     v_store cust_loc,
                     v_item_master im,
                     (select wod1.item,
                             loc,
                             'WS' typ1,
                             stock_on_hand,
                             rtv_qty,
                             non_sellable_qty,
                             tsf_reserved_qty,
                             customer_resv
                        from item_loc_soh ils1,
                             wf_order_detail wod1
                       where ils1.item = wod1.item
                         and ils1.loc = wod1.source_loc_id
                         and wod1.source_loc_type in ('WH','ST')
                         and wod1.wf_order_no =  I_wf_order_id
                       UNION
                      select wod1.item,
                             supplier,
                             'SU' typ1,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL
                       from item_supplier its,
                            wf_order_detail wod1
                      where its.item = wod1.item
                        and its.supplier = wod1.source_loc_id
                        and wod1.source_loc_type = 'SU'
                        and wod1.wf_order_no =  I_wf_order_id
                       UNION
                      select wod1.item,            /* For EG Transfer, WF_ORDER_DETAIL will have Physical warehouse*/
                             wh.physical_wh loc,
                             'WS' typ1,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL
                        from wf_order_detail wod1 ,
                             v_wh wh
                       where wod1.wf_order_no = I_wf_order_id
                         and wod1.source_loc_type = 'WH'
                         and wod1.source_loc_id = wh.wh
                         and wh.wh = wh.physical_wh ) ils,
                      (select wh from_loc,
                             'WH' typ,
                             wh_name  from_loc_name
                        from v_wh
                     UNION ALL
                      select supplier from_loc,
                             'SU' typ,
                             sup_name from_loc_name
                        from v_sups
                     UNION ALL
                      select store from_loc,
                             'ST' typ,
                             store_name from_loc_name
                        from v_store) comp_loc,
                     (select DECODE(MAX(tsf_no),MIN(tsf_no),MAX(tsf_no),NULL) linked_tsfpoalloc,
                             DECODE(MAX(status),MIN(status),MAX(status),NULL) linked_entity_status,
                             MAX(ship_date) ship_date,
                             SUM(ship_qty) ship_qty,
                             wf_order_no,
                             item,
                             to_loc,
                             from_loc,
                             from_loc_type
                        from ( -- Fetch all rec for which xFer and shipment both exist.
                             (select th.tsf_no,
                                     th.status,
                                     MAX(sh.ship_date) ship_date,
                                     SUM(sk.qty_expected) ship_qty,
                                     th.wf_order_no wf_order_no,
                                     sk.item item,
                                     th.to_loc to_loc,
                                     th.from_loc from_loc,
                                     decode(th.from_loc_type,'S','ST','W','WH') from_loc_type
                               from  tsfhead th,
                                     shipment sh,
                                     shipsku sk
                               where th.wf_order_no = I_wf_order_id
                                 and sk.distro_no = th.tsf_no
                                 and sk.shipment = sh.shipment
                                 and sk.distro_type = 'T'
                            group by th.tsf_no,
                                     th.status,
                                     th.wf_order_no,
                                     sk.item,
                                     th.to_loc,
                                     th.from_loc,
                                     th.from_loc_type)
                            UNION ALL
                            -- Fetch all rec for which alloc and shipment both exist.
                             (select ah.alloc_no tsf_no,
                                     ah.status,
                                     MAX(sh.ship_date) ship_date,
                                     SUM(sk.qty_expected) ship_qty,
                                     ad.wf_order_no wf_order_no,
                                     sk.item item,
                                     ad.to_loc to_loc,
                                     ah.wh from_loc,
                                     decode(sh.from_loc_type,'S','ST','W','WH') from_loc_type
                               from  alloc_header ah,
                                     alloc_detail ad,
                                     shipment sh,
                                     shipsku sk
                              where ad.wf_order_no = I_wf_order_id
                                and ad.alloc_no = ah.alloc_no
                                and sk.distro_no = ah.alloc_no
                                and sk.item = ah.item
                                and sk.shipment = sh.shipment
                                and sk.distro_type = 'A'
                           group by ah.alloc_no,
                                    ah.status,
                                    ad.wf_order_no,
                                    sk.item,
                                    ad.to_loc,
                                    ah.wh,
                                    sh.from_loc_type)
                            UNION ALL
                            -- Fetch all rec for which PO and shipment both exist.
                           (select oh.order_no tsf_no,
                                   oh.status,
                                   MAX(sh.ship_date) ship_date,
                                   SUM(sk.qty_expected) ship_qty,
                                   oh.wf_order_no wf_order_no,
                                   sk.item item,
                                   sh.to_loc to_loc,
                                   oh.supplier from_loc,
                                   'SU' from_loc_type
                             from  ordhead oh,
                                   ordloc ol,
                                   shipment sh,
                                   shipsku sk
                            where oh.wf_order_no = I_wf_order_id
                              and ol.order_no = oh.order_no
                              and sh.order_no = oh.order_no
                              and sk.item = ol.item
                              and sk.shipment = sh.shipment
                         group by oh.order_no,
                                  oh.status,
                                  oh.wf_order_no,
                                  sk.item,
                                  sh.to_loc,
                                  oh.supplier)
                           UNION ALL
                           -- Fetch all rec for which only xFer exists and shipment does not.
                           (select th.tsf_no tsf_no,
                                   th.status status,
                                   NULL ship_date,
                                   NULL ship_qty,
                                   th.wf_order_no wf_order_no,
                                   td.item item,
                                   th.to_loc to_loc,
                                   th.from_loc from_loc,
                                   decode(th.from_loc_type,'S','ST','W','WH') from_loc_type
                              from tsfhead th,
                                   tsfdetail td
                             where th.wf_order_no  = I_wf_order_id
                               and td.tsf_no= th.tsf_no
                               and not exists (select 1
                                                 from shipsku sk
                                                where sk.distro_no = th.tsf_no
                                                  and sk.item = td.item
                                                  and sk.distro_type = 'T'))
                           UNION ALL
                           -- Fetch all rec for which only alloc exists and shipment does not.
                           (select ah.alloc_no tsf_no,
                                   ah.status status,
                                   NULL ship_date,
                                   NULL ship_qty,
                                   ad.wf_order_no wf_order_no,
                                   ah.item item,
                                   ad.to_loc to_loc,
                                   ah.wh from_loc,
                                   'WH' from_loc_type
                              from alloc_header ah,
                                   alloc_detail ad
                             where ad.wf_order_no  = I_wf_order_id
                               and ad.alloc_no= ah.alloc_no
                               and not exists (select 1
                                                 from shipsku sk
                                                where sk.distro_no = ah.alloc_no
                                                  and sk.item = ah.item
                                                  and sk.distro_type = 'A'))
                           UNION ALL
                           -- Fetch all rec for which only PO exists and shipment does not.
                             (select oh.order_no tsf_no,
                                     oh.status,
                                     NULL ship_date,
                                     NULL ship_qty,
                                     oh.wf_order_no wf_order_no,
                                     ol.item item,
                                     ol.location to_loc,
                                     oh.supplier from_loc,
                                     'SU' from_loc_type
                               from  ordhead oh,
                                     ordloc ol
                              where oh.wf_order_no = I_wf_order_id
                                and ol.order_no = oh.order_no
                                and not exists (select 1
                                                  from shipment sh,
                                                       shipsku sk
                                                 where sk.shipment = sh.shipment
                                                   and sh.order_no = oh.order_no
                                                   and sk.item = ol.item))
                           UNION ALL
                           -- Fetch all rec which are neither xFered,alloc,po not Shipped.
                           (select NULL tsf_no,
                                   NULL status,
                                   NULL ship_date,
                                   NULL ship_qty,
                                   NULL wf_order_no,
                                   NULL item,
                                   NULL to_loc,
                                   NULL from_loc,
                                   NULL from_loc_type
                              from wf_order_head woh
                             where woh.wf_order_no  = I_wf_order_id
                               and (not exists (select 1
                                                 from tsfhead th
                                                where th.wf_order_no = I_wf_order_id
                                                UNION
                                                select 1
                                                 from ordhead oh
                                                where oh.wf_order_no = I_wf_order_id
                                                UNION
                                                select 1
                                                 from alloc_detail ad
                                                where ad.wf_order_no = I_wf_order_id)))
                        )
                  group by
                        wf_order_no,
                        item,
                        to_loc,
                        from_loc,
                        from_loc_type
                    ) tsf_det
               where wod.wf_order_no = I_wf_order_id
                 and wod.wf_order_no = woh.wf_order_no
                 and comp_loc.from_loc = wod.source_loc_id
                 and comp_loc.typ = wod.source_loc_type
                 and cust_loc.store = wod.customer_loc
                 and im.item = wod.item
                 and ils.item = wod.item
                 and NVL(im.deposit_item_type,'x') != 'A'         -- Container items are not displayed on the screen.
                 and ils.loc = wod.source_loc_id
                 and Decode(source_loc_type,'SU','SU','WS') = ils.typ1
                 and NVL(tsf_det.wf_order_no,wod.wf_order_no) = wod.wf_order_no
                 and NVL(tsf_det.item(+),wod.item) = wod.item
                 and NVL(tsf_det.to_loc(+),wod.customer_loc) = wod.customer_loc
                 and NVL(tsf_det.from_loc(+),wod.source_loc_id)= wod.source_loc_id
                 and NVL(tsf_det.from_loc_type,wod.source_loc_type) = wod.source_loc_type ) wd
       order by wd.wf_order_line_no;


BEGIN

   open C_WF_ORD_DET;
   fetch C_WF_ORD_DET BULK COLLECT into O_wf_order_det;
   close C_WF_ORD_DET;

   if O_wf_order_det is NOT NULL and O_wf_order_det.COUNT > 0 then
      FOR i in O_wf_order_det.FIRST..O_wf_order_det.LAST LOOP

         if WF_ORDER_SQL.GET_TAX_INFO(O_wf_order_det(i).error_message,
                                      L_vat_rate,
                                      L_vat_code,
                                      O_wf_order_det(i).item,
                                      O_wf_order_det(i).source_loc_type,
                                      O_wf_order_det(i).source_loc_id,
                                      O_wf_order_det(i).cust_loc,
                                      'R',
                                      I_wf_order_id) = FALSE then
            O_wf_order_det(1).error_message :=  O_wf_order_det(i).error_message;
         end if;

         O_wf_order_det(i).vat_rate := L_vat_rate;
         O_wf_order_det(i).customer_cost_vat_incl :=  O_wf_order_det(i).customer_cost_vat_excl * (1+(nvl(L_vat_rate,0)/100));
         O_wf_order_det(i).total_cust_cost_vat_incl :=  O_wf_order_det(i).customer_cost_vat_incl *  O_wf_order_det(i).requested_qty;
         O_wf_order_det(i).vat_amt := O_wf_order_det(i).total_cust_cost_vat_incl - O_wf_order_det(i).total_cust_cost_vat_excl;
      END LOOP;
   end if;

   return;

EXCEPTION
   when OTHERS then
      O_wf_order_det(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                             SQLERRM,
                                                             L_program,
                                                             TO_CHAR(SQLCODE));
      O_wf_order_det(1).return_code := 'FALSE';
      return;
END GET_DEFAULT_WF_ORDER_INFO;
---------------------------------------------------------------------------------------------------
FUNCTION WF_ORDER_DETAIL_CREATE (IO_wf_order_rec    IN OUT    WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD)
RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_CREATE';
   L_item                  WF_ORDER_DETAIL.ITEM%TYPE;
   L_cust_loc              WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_source_loc_id         WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_source_loc_type       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_need_date             WF_ORDER_DETAIL.NEED_DATE%TYPE;
   L_not_after_date        WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE;
   L_available_qty         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_wf_order_no           WF_ORDER_DETAIL.WF_ORDER_NO%TYPE;
   L_lead_time             REPL_ITEM_LOC.WH_LEAD_TIME%TYPE;
   L_so_exists             BOOLEAN;
   L_so_processed          BOOLEAN;
   L_dupl_order_exists     BOOLEAN;
   L_order_detail_exists   VARCHAR2(1) :='N';
   L_system_options_row    SYSTEM_OPTIONS%ROWTYPE;
   L_exist_order_no        WF_ORDER_HEAD.WF_ORDER_NO%TYPE := NULL;
   L_message_text          RTK_ERRORS.RTK_TEXT%TYPE;
   L_valid                 BOOLEAN;
   L_requested_qty         WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_po_exist              BOOLEAN;
   L_order_type            WF_ORDER_HEAD.ORDER_TYPE%TYPE  := NULL;
   L_tsf_no                TSFHEAD.TSF_NO%TYPE;

   cursor C_WF_ORDER_DETAIL_EXISTS IS
      select 'Y'
        from wf_order_detail
       where wf_order_no = L_wf_order_no
         and item = L_item
         and customer_loc = L_cust_loc
         and source_loc_id = L_source_loc_id
         and source_loc_type = L_source_loc_type
         and rownum = 1;

   cursor C_WF_ORDER_TYPE IS
      select order_type
        from wf_order_head
       where wf_order_no = L_wf_order_no;

   --get the linked transfers
   cursor C_GET_LINK_TSF IS
      select th.tsf_no tsf_no
        from tsfhead th
       where th.wf_order_no      = L_wf_order_no
         and th.from_loc         = L_source_loc_id
         and th.from_loc_type    = decode(L_source_loc_type, 'ST', 'S', 'W')
         and th.to_loc           = L_cust_loc
         and th.status not in ('C','D');

BEGIN
   L_wf_order_no     := IO_wf_order_rec.wf_order_no;
   L_item            := IO_wf_order_rec.item;
   L_cust_loc        := IO_wf_order_rec.cust_loc;
   L_need_date       := IO_wf_order_rec.need_date;
   L_source_loc_id   := IO_wf_order_rec.source_loc_id;
   L_source_loc_type := IO_wf_order_rec.source_loc_type;
   L_not_after_date  := IO_wf_order_rec.not_after_date;
   L_requested_qty   := IO_wf_order_rec.requested_qty;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(IO_wf_order_rec.error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if IO_wf_order_rec.need_date < LP_vdate then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_NEED_DATE',NULL,NULL,NULL);
      return FALSE;
   end if;

   if IO_wf_order_rec.need_date > IO_wf_order_rec.not_after_date then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_NOT_AFTER_DATE',NULL,NULL,NULL);
      return FALSE;
   end if;

   if IO_wf_order_rec.requested_qty <= 0 then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_REQUESTED_QTY',NULL,NULL,NULL);
      return FALSE;
   end if;


   open C_WF_ORDER_TYPE;
   fetch C_WF_ORDER_TYPE into L_order_type;
   close C_WF_ORDER_TYPE;

   -- Line item addition is not allowed for externally generated ('X') franchise orders.
   if L_order_type = 'X' then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_EG_NO_ADD_DETAIL',L_wf_order_no,NULL,NULL);
      return FALSE;
   end if;

   -- For line item addition for store/warehouse sourced auto order type, a transfer should exists
   -- for the same from loc and to loc combination. Need date is not used to get the transfer number.
   -- Allocation driven franchise order is created in approved status and hence line item addition is
   -- not allowed. Hence the check is only for linked transfer.
   if L_order_type = 'A' and L_source_loc_type <> 'SU' then
      open C_GET_LINK_TSF;
      fetch C_GET_LINK_TSF into L_tsf_no;
      close C_GET_LINK_TSF;

      if L_tsf_no is NULL then
         IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_TRANSFER_NOT_FOUND',L_wf_order_no,L_source_loc_id,L_cust_loc);
         return FALSE;
      end if;
   end if;

   if WF_ORDER_SQL.GET_LEAD_TIME (IO_wf_order_rec.error_message,
                                  L_lead_time,
                                  L_item,
                                  L_cust_loc,
                                  L_source_loc_id,
                                  L_source_loc_type) =FALSE then
      return FALSE;
   end if;

   if L_source_loc_type = 'SU' then
      if L_not_after_date - L_lead_time < LP_vdate then
         IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
         return FALSE;
      end if;
   elsif L_source_loc_type = 'ST' then
      if L_not_after_date - L_lead_time < LP_vdate then
         IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
         return FALSE;
      end if;
      if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(IO_wf_order_rec.error_message,
                                                    L_available_qty,
                                                    L_item,
                                                    L_source_loc_id,
                                                    'S')= FALSE then
         return FALSE;
      end if;
      if L_available_qty < IO_wf_order_rec.requested_qty then
         IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_INSUFF_INV',L_item,L_source_loc_id,IO_wf_order_rec.requested_qty);
         return FALSE;
      end if;
   else     -- Warehouse
      if (L_not_after_date - L_lead_time) < LP_vdate then
         IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
         return FALSE;
      end if;

      if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(IO_wf_order_rec.error_message,
                                                    L_available_qty,
                                                    L_item,
                                                    L_source_loc_id,
                                                    'W') = FALSE then
         return FALSE;
      end if;

      if IO_wf_order_rec.requested_qty > L_available_qty then
         -- Store order will not be created for Auto order type.
         if L_order_type not in ('M','E') then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_INSUFF_INV',L_item,L_source_loc_id,IO_wf_order_rec.requested_qty);
            return FALSE;
         end if;
         ---
         if IO_wf_order_rec.pack_ind ='Y' then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_PACK_ITEM',NULL,NULL,NULL);
            return FALSE;
         end if;
         ---
         if WF_ORDER_SQL.VERIFY_REPL_INFO(IO_wf_order_rec.error_message,
                                          L_cust_loc,
                                          L_item,
                                          L_not_after_date,
                                          L_source_loc_id) = FALSE then
            return FALSE;
         end if;
         ---
         if WF_TRANSFER_SQL.STORE_ORDER_EXISTS(IO_wf_order_rec.error_message,
                                               L_so_exists,
                                               L_so_processed,
                                               L_item,
                                               L_cust_loc,
                                               L_need_date)= FALSE then
            return FALSE;
         end if;
         ---
         if L_so_exists = TRUE then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('STORE_ORDER_EXISTS',NULL,NULL,NULL);
            return FALSE;
         end if;
      end if;
   end if;     -- source_loc_type = WH

   -- Validate the item is not on order for the same need date for the same from location and to location as part
   -- of a different manual or edi order.
   if WF_ORDER_SQL.WF_ORDER_EXISTS(IO_wf_order_rec.error_message,
                                   L_dupl_order_exists,
                                   L_exist_order_no,
                                   L_wf_order_no,
                                   L_cust_loc,
                                   L_source_loc_type,
                                   L_source_loc_id,
                                   L_need_date,
                                   L_item) = FALSE then
      return FALSE;
   end if;

   if L_dupl_order_exists = TRUE then
      L_message_text  := L_cust_loc||', '||L_source_loc_id||', '||L_need_date||', '||L_item;
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_DUP_ORDER_EXISTS',L_exist_order_no,L_message_text,NULL);
      return FALSE;
   end if;

   ---
   open C_WF_ORDER_DETAIL_EXISTS;
   fetch C_WF_ORDER_DETAIL_EXISTS into L_order_detail_exists;
   close C_WF_ORDER_DETAIL_EXISTS;
   ---
   if L_order_detail_exists ='Y' then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_ORDER_EXISTS',L_item,L_cust_loc,L_source_loc_id);
      return FALSE;
   end if;
   ---
   if WF_ORDER_DETAIL_SQL.INSERT_ORDER_INFO(IO_wf_order_rec) = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_LINKED_PO(IO_wf_order_rec.error_message,
                      L_po_exist,
                      L_wf_order_no,
                      NULL)= FALSE then
      return FALSE;
   end if;
   ---
   if L_po_exist = TRUE then
      if WF_PO_SQL.MODIFY_FRANCHISE_PO (IO_wf_order_rec.error_message,
                                        L_wf_order_no,
                                        'N',
                                        'N') = FALSE THEN

         RETURN FALSE;
      end if;
   end if;

   -- for non manual/edi orders, the line item addition should update the linked transfer.
   if L_order_type = 'A' and L_source_loc_type <> 'SU' then
      if WF_TRANSFER_SQL.WF_CREATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                               L_tsf_no,
                                               L_item,
                                               IO_wf_order_rec.requested_qty) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           to_char(SQLCODE));
      return FALSE;
END WF_ORDER_DETAIL_CREATE;
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDER_INFO (IO_wf_order_rec   IN OUT   WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) := 'WF_ORDER_DETAIL_SQL.INSERT_ORDER_INFO';
   L_templ_id                WF_COST_RELATIONSHIP.TEMPL_ID%TYPE;
   L_templ_desc              WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE;
   L_margin_pct              WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE;
   L_vat_rate                VAT_CODE_RATES.VAT_RATE%TYPE;
   L_cust_cost               FUTURE_COST.PRICING_COST%TYPE;
   L_acqui_cost              FUTURE_COST.ACQUISITION_COST%TYPE;
   L_calc_type               WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE;
   L_order_no                WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_need_date               WF_ORDER_DETAIL.NEED_DATE%TYPE;
   L_seq                     WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_source_loc_id           WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_cust_loc                WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_standard_uom            UOM_CLASS.UOM%TYPE;
   L_standard_class          UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor             ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_std_uom_qty             WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_cust_cost_order_curr    FUTURE_COST.PRICING_COST%TYPE;
   L_acqui_cost_order_curr   FUTURE_COST.ACQUISITION_COST%TYPE;
   L_base_cost               FUTURE_COST.BASE_COST%TYPE;
   L_net_cost                FUTURE_COST.NET_COST%TYPE;
   L_pricing_cost            FUTURE_COST.PRICING_COST%TYPE;
   L_item_record             ITEM_MASTER%ROWTYPE;
   L_source_loc_type         WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_fixed_cost              WF_ORDER_DETAIL.FIXED_COST%TYPE;
   L_container_item          ITEM_MASTER.ITEM%TYPE;
   L_currency_code           FUTURE_COST.CURRENCY_CODE%TYPE;
   L_container_exists        VARCHAR2(1) := 'N';
   L_item_loc_exists         BOOLEAN;
   L_ranged_ind              ITEM_LOC.RANGED_IND%TYPE;
   L_loc_type                ITEM_LOC.LOC_TYPE%TYPE;

   TYPE l_item_tbl IS TABLE OF ITEM_MASTER.ITEM%TYPE INDEX BY BINARY_INTEGER;
   L_item                    l_item_tbl;
   L_item_table_index        NUMBER(1)     := 1;

   L_table                   VARCHAR2(30);
   L_key1                    VARCHAR2(100);
   L_key2                    VARCHAR2(100);

   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked, -54);

   cursor C_NEXT_SEQ is
      select NVL(MAX(wf_order_line_no + 1), 1)
        from wf_order_detail
       where wf_order_no = L_order_no;

   cursor C_CONTAINER_EXISTS is
      select 'Y'
        from wf_order_detail
       where wf_order_no = L_order_no
         and item = L_container_item
         and source_loc_id = L_source_loc_id
         and source_loc_type = L_source_loc_type
         and customer_loc = L_cust_loc
         for update nowait;

   cursor C_GET_CONTENT_RANGE_IND is
      select il.ranged_ind,
             il.loc_type
        from item_loc il
       where item        = IO_wf_order_rec.item
         and il.loc      = L_source_loc_id
         and il.loc_type = decode(L_source_loc_type, 'ST', 'S', 'WH', 'W');
BEGIN

   L_item(L_item_table_index)  := IO_wf_order_rec.item;
   L_source_loc_id             := IO_wf_order_rec.source_loc_id;
   L_cust_loc                  := IO_wf_order_rec.cust_loc;
   L_need_date                 := IO_wf_order_rec.need_date;
   L_order_no                  := IO_wf_order_rec.wf_order_no;
   L_source_loc_type           := IO_wf_order_rec.source_loc_type;

   --get the deposit container item if the item is deposit content item
   if ITEM_ATTRIB_SQL.GET_CONTAINER_ITEM(IO_wf_order_rec.error_message,
                                         L_container_item,
                                         IO_wf_order_rec.item) = FALSE then
      return FALSE;
   end if;

   if L_container_item is NOT NULL then
      L_item(L_item_table_index + 1) := L_container_item;
      if L_source_loc_type <> 'SU' then
         open C_GET_CONTENT_RANGE_IND;
         fetch C_GET_CONTENT_RANGE_IND into L_ranged_ind,L_loc_type;
         close C_GET_CONTENT_RANGE_IND;
         if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(IO_wf_order_rec.error_message,
                                              L_container_item,
                                              L_source_loc_id,
                                              L_item_loc_exists) = FALSE then
            return FALSE;
         end if;

         if L_item_loc_exists = FALSE then
            if NEW_ITEM_LOC(IO_wf_order_rec.error_message,
                            L_container_item,
                            L_source_loc_id,
                            NULL,
                            NULL,
                            L_loc_type,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            'N',
                            NULL,
                            NULL,
                            NULL,
                            L_ranged_ind) = FALSE then   -- Ranged_ind
               return FALSE;
            end if;
         end if;
      end if;
   end if;

   FOR I in 1..L_item.count LOOP
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(IO_wf_order_rec.error_message,
                                         L_item_record,
                                         L_item(i)) = FALSE then
         return FALSE;
      end if;
      ---
      if L_item(i) = L_container_item then
         L_fixed_cost := NULL;
      else
         L_fixed_cost := IO_wf_order_rec.fixed_cost;
      end if;
      ---
      if WF_ORDER_SQL.GET_COST_INFORMATION(IO_wf_order_rec.error_message,
                                           L_templ_id,
                                           L_templ_desc,
                                           L_margin_pct,
                                           L_calc_type,
                                           L_cust_cost,
                                           L_acqui_cost,
                                           L_currency_code,
                                           L_item(i),
                                           L_cust_loc,
                                           L_source_loc_type,
                                           L_source_loc_id)= FALSE then
         return FALSE;
      end if;

      -- Convert to order currency, the customer cost and acquisition cost
      if CURRENCY_SQL.CONVERT(IO_wf_order_rec.error_message,
                              L_cust_cost,
                              L_currency_code,
                              IO_wf_order_rec.order_currency,
                              L_cust_cost_order_curr,
                              'N',
                              NULL,
                              NULL,
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;

      if CURRENCY_SQL.CONVERT(IO_wf_order_rec.error_message,
                              L_acqui_cost,
                              L_currency_code,
                              IO_wf_order_rec.order_currency,
                              L_acqui_cost_order_curr,
                              'N',
                              NULL,
                              NULL,
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
      ---

      if L_item_record.standard_uom != IO_wf_order_rec.uom then
         if UOM_SQL.CONVERT(IO_wf_order_rec.error_message,
                            L_std_uom_qty,
                            L_standard_uom,
                            IO_wf_order_rec.requested_qty,
                            IO_wf_order_rec.uom,
                            L_item(i),
                            NULL,
                            NULL,
                            NULL,
                            NULL) = FALSE then
            return FALSE;
         end if;
      else
         L_std_uom_qty := IO_wf_order_rec.requested_qty;
      end if;

      -- Check if the container item record already exists becuase of  different content.
      if L_item(i) = L_container_item then
         L_table := 'WF_ORDER_DETAIL';
         L_key1  := L_item(i);
         L_key2  := TO_CHAR(L_source_loc_id);

         open C_CONTAINER_EXISTS;
         fetch C_CONTAINER_EXISTS into L_container_exists;
         close C_CONTAINER_EXISTS;
      end if;

      if L_container_exists = 'N' then -- Insert new record for regular item and container item..
         --no container item exists
         open C_NEXT_SEQ;
         fetch C_NEXT_SEQ into L_seq;
         close C_NEXT_SEQ;

         insert into wf_order_detail (wf_order_no,
                                      wf_order_line_no,
                                      item,
                                      source_loc_id,
                                      source_loc_type,
                                      customer_loc,
                                      requested_qty,
                                      cancel_reason,
                                      need_date,
                                      not_after_date,
                                      acquisition_cost,
                                      customer_cost,
                                      fixed_cost,
                                      templ_id,
                                      margin_pct,
                                      calc_type,
                                      templ_desc,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
                              values (L_order_no,
                                      L_seq,
                                      L_item(i),
                                      L_source_loc_id,
                                      IO_wf_order_rec.source_loc_type,
                                      L_cust_loc,
                                      L_std_uom_qty,
                                      IO_wf_order_rec.cancel_reason,
                                      L_need_date,
                                      IO_wf_order_rec.not_after_date,
                                      L_acqui_cost_order_curr,
                                      L_cust_cost_order_curr,
                                      L_fixed_cost,
                                      L_templ_id,
                                      L_margin_pct,
                                      L_calc_type,
                                      L_templ_desc,
                                      sysdate,
                                      get_user,
                                      sysdate,
                                      get_user);
      else  -- Update quantity for an existing container record.

         update wf_order_detail
            set requested_qty        = requested_qty + L_std_uom_qty,
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where wf_order_no          = L_order_no
            and item                 = L_item(i)
            and source_loc_id        = L_source_loc_id
            and source_loc_type      = IO_wf_order_rec.source_loc_type
            and customer_loc         = L_cust_loc;
      end if;

      if WF_ORDER_SQL.GET_UPCHARGE_INFORMATION(IO_wf_order_rec.error_message,
                                               L_order_no,
                                               IO_wf_order_rec.order_currency,
                                               L_item(i),
                                               IO_wf_order_rec.source_loc_type,
                                               L_source_loc_id,
                                               L_cust_loc) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                          L_table,
                                                          L_key1,
                                                          L_key2);
      return FALSE;
   when OTHERS then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          L_program,
                                                          to_char(SQLCODE));
      return FALSE;
END INSERT_ORDER_INFO;
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE(wf_order_det   IN OUT   WF_ORDER_DETAIL_TAB)
IS
   L_program              VARCHAR2(64)          := 'WF_ORDER_DETAIL_SQL.LOCK_PROCEDURE';
   L_exists               VARCHAR2(1);
   L_rowid                VARCHAR2(25);
   L_table                VARCHAR2(30);
   L_key1                 VARCHAR2(100);

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
BEGIN
   FOR i in 1..wf_order_det.COUNT LOOP
      L_rowid := chartorowid(wf_order_det(i).row_id);
      ---
      L_table := 'WF_ORDER_DETAIL';
      L_key1  := L_rowid;
      ---
      select 'x'
        into L_exists
        from wf_order_detail
       where rowid = chartorowid(wf_order_det(i).row_id)
         for update nowait;
   END LOOP;

EXCEPTION
   when RECORD_LOCKED then
      wf_order_det(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                          L_table,
                                                          L_key1,
                                                          NULL);
      return;
   when OTHERS then
      wf_order_det(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          L_program,
                                                          TO_CHAR(SQLCODE));
      return;
END LOCK_PROCEDURE;
---------------------------------------------------------------------------------------------------
PROCEDURE DELETE_WF_ORDER_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_return_code     IN OUT   VARCHAR2,
                                 I_wf_order_rec    IN       WF_ORDER_DETAIL_TAB)
IS

   L_program                  VARCHAR2(64)               := 'WF_ORDER_DETAIL_SQL.DELETE_WF_ORDER_DETAIL';
   L_status                   WF_ORDER_HEAD.STATUS%TYPE  := NULL;
   L_wf_order_detail_index    NUMBER                     := 1;
   L_container_item           ITEM_MASTER.ITEM%TYPE;
   L_container_qty            WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_order_type               WF_ORDER_HEAD.ORDER_TYPE%TYPE;
   L_po_exist                 BOOLEAN;
   L_wf_order_no              WF_ORDER_DETAIL.WF_ORDER_NO%TYPE;
   L_source_loc_type          WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_source_loc_id            WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_cust_loc                 WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_item                     WF_ORDER_DETAIL.ITEM%TYPE;
   L_wf_order_line_no         WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_cont_wf_order_line_no    WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE :=  NULL;
   L_table                    VARCHAR2(30);
   L_key1                     VARCHAR2(100);
   L_key2                     VARCHAR2(100);

   PROGRAM_ERROR              EXCEPTION;
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);

   cursor C_WF_ORDER_STATUS IS
      select status,
             order_type
        from wf_order_head
       where wf_order_no = I_wf_order_rec(L_wf_order_detail_index).wf_order_no;

   cursor C_LOCK_WF_ORDER_DETAIL(cv_wf_order_line_no  WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE) IS
      select 'x'
        from wf_order_detail
       where wf_order_no = L_wf_order_no
         and wf_order_line_no = cv_wf_order_line_no
         for update nowait;


   cursor C_LOCK_WF_ORDER_EXP(cv_wf_order_line_no  WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE) IS
      select 'x'
        from wf_order_exp
       where wf_order_no = L_wf_order_no
         and wf_order_line_no = cv_wf_order_line_no
         for update nowait;

   --Get the requested quantity for container item
   cursor C_CONTAINER_QTY IS
      select requested_qty,
             wf_order_line_no
        from wf_order_detail
       where wf_order_no = L_wf_order_no
         and item = L_container_item
         and source_loc_type = L_source_loc_type
         and source_loc_id = L_source_loc_id
         and customer_loc = L_cust_loc
         for update nowait;

BEGIN

   open C_WF_ORDER_STATUS;
   fetch C_WF_ORDER_STATUS into L_status, L_order_type;
   close C_WF_ORDER_STATUS;

   if L_status <> 'I' then
      L_error_message := SQL_LIB.CREATE_MSG('DEL_INPUT_ORD','I_wf_order_no',L_program,NULL);
      raise PROGRAM_ERROR;
   end if;

   FOR i in 1..I_wf_order_rec.COUNT LOOP
      L_wf_order_no        := I_wf_order_rec(i).wf_order_no;
      L_source_loc_type    := I_wf_order_rec(i).source_loc_type;
      L_source_loc_id      := I_wf_order_rec(i).source_loc_id;
      L_cust_loc           := I_wf_order_rec(i).cust_loc;
      L_item               := I_wf_order_rec(i).item;
      L_wf_order_line_no   := I_wf_order_rec(i).wf_order_line_no;

      L_table := 'WF_ORDER_DETAIL';
      L_key1  := L_item;
      L_key2  := TO_CHAR(L_wf_order_line_no);

      open C_LOCK_WF_ORDER_DETAIL(L_wf_order_line_no);
      close C_LOCK_WF_ORDER_DETAIL;

      L_table := 'WF_ORDER_EXP';
      L_key1  := L_item;
      L_key2  := TO_CHAR(L_wf_order_line_no);

      open C_LOCK_WF_ORDER_EXP(L_wf_order_line_no);
      close C_LOCK_WF_ORDER_EXP;

      --Retrieve the deposit container item if the item is deposit content item
      if ITEM_ATTRIB_SQL.GET_CONTAINER_ITEM(L_error_message,
                                            L_container_item,
                                            L_item) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_container_item is not null then
         open C_CONTAINER_QTY;
         fetch C_CONTAINER_QTY into L_container_qty, L_cont_wf_order_line_no;
         close C_CONTAINER_QTY;
      end if;

      delete from wf_order_exp
       where wf_order_no      = L_wf_order_no
         and wf_order_line_no = L_wf_order_line_no;

      delete from wf_order_detail
       where wf_order_no      = L_wf_order_no
         and wf_order_line_no = L_wf_order_line_no;

      -- handle deposit container item
      if L_container_item is not null then
         ---
         L_table := 'WF_ORDER_DETAIL';
         L_key1  := L_container_item;
         L_key2  := TO_CHAR(L_cont_wf_order_line_no);

         open C_LOCK_WF_ORDER_DETAIL(L_cont_wf_order_line_no);
         close C_LOCK_WF_ORDER_DETAIL;

         if L_container_qty = I_wf_order_rec(i).requested_qty then               -- Delete the container record.
            ---
            L_table := 'WF_ORDER_EXP';
            L_key1  := L_container_item;
            L_key2  := TO_CHAR(L_cont_wf_order_line_no);
            open C_LOCK_WF_ORDER_EXP(L_cont_wf_order_line_no);
            close C_LOCK_WF_ORDER_EXP;
            ---
            delete from wf_order_exp
             where wf_order_no      = L_wf_order_no
               and wf_order_line_no = L_cont_wf_order_line_no;

            delete from wf_order_detail
             where wf_order_no      = L_wf_order_no
               and wf_order_line_no = L_cont_wf_order_line_no;
         else                                                                    -- Reduce the container record as the same container may be used by other content.
            update wf_order_detail
               set requested_qty = requested_qty - I_wf_order_rec(i).requested_qty,
                   last_update_datetime = sysdate,
                   last_update_id = get_user
             where wf_order_no      = L_wf_order_no
               and wf_order_line_no = L_cont_wf_order_line_no;
         end if;
      end if;

      if L_order_type in ('X','A') and L_source_loc_type <> 'SU' then
         if WF_TRANSFER_SQL.DELETE_FRANCHISE_TSF_DETAIL (L_error_message,
                                                         L_wf_order_no,
                                                         NULL,
                                                         L_item,
                                                         L_source_loc_id,
                                                         L_cust_loc) = FALSE THEN

            raise PROGRAM_ERROR;
         end if;
      end if;
   END LOOP;

   if CHECK_LINKED_PO(L_error_message,
                      L_po_exist,
                      I_wf_order_rec(L_wf_order_detail_index).wf_order_no,
                      NULL)= FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_po_exist = TRUE then
      if WF_PO_SQL.MODIFY_FRANCHISE_PO (L_error_message,
                                        I_wf_order_rec(L_wf_order_detail_index).wf_order_no,
                                        'N',
                                        'N') = FALSE THEN

         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                L_table,
                                                L_key1,
                                                L_key2);
         O_return_code := 'FALSE';

      when PROGRAM_ERROR then
         O_error_message := L_error_message;
         O_return_code   := 'FALSE';

      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         O_return_code := 'FALSE';

END DELETE_WF_ORDER_DETAIL;
---------------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_return_code     IN OUT   VARCHAR2,
                            I_wf_order_rec    IN       WF_ORDER_DETAIL_TAB)
IS

   L_program                VARCHAR2(64) :='WF_ORDER_DETAIL_SQL.UPDATE_PROCEDURE';
   L_wf_order_no            WF_ORDER_DETAIL.WF_ORDER_NO%TYPE;
   L_item                   WF_ORDER_DETAIL.ITEM%TYPE;
   L_cust_loc               WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_need_date              WF_ORDER_DETAIL.NEED_DATE%TYPE;
   L_source_loc_id          WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_source_loc_type        WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_wf_order_line_no       WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_new_requested_qty      WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_old_requested_qty      WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_status                 WF_ORDER_HEAD.STATUS%TYPE  := NULL;
   L_order_type             WF_ORDER_HEAD.ORDER_TYPE%TYPE;
   L_tsf_no                 TSFHEAD.TSF_NO%TYPE;
   L_tot_qty                TSFDETAIL.SELECTED_QTY%TYPE;
   L_wf_order_rec           WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD;
   L_error_message          RTK_ERRORS.RTK_TEXT%TYPE :=  NULL;
   L_po_exist               BOOLEAN;
   L_rowid                  VARCHAR2(25);
   L_table                  VARCHAR2(30);
   L_key1                   VARCHAR2(100);

   PROGRAM_ERROR          EXCEPTION;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_WF_ORDER_DETAIL IS
      select 'x'
        from wf_order_detail
       where rowid = L_rowid
         for update nowait;

   cursor C_OLD_REQUESTED_QTY IS
      select NVL(requested_qty,0)
        from wf_order_detail
       where wf_order_no = L_wf_order_no
         and item = L_item
         and customer_loc = L_cust_loc
         and source_loc_id = L_source_loc_id
         and source_loc_type = L_source_loc_type;

   cursor C_GET_TSF_DETAIL IS
      select th.tsf_no
        from tsfhead th,
             tsfdetail td
       where th.tsf_no      = td.tsf_no
         and td.item        = L_item
         and th.wf_order_no = L_wf_order_no
         and th.from_loc    = L_source_loc_id
         and th.to_loc      = L_cust_loc;

   cursor C_WF_ORDER_STATUS IS
      select status,
             order_type
        from wf_order_head
       where wf_order_no = L_wf_order_no;

BEGIN
   FOR i in 1..I_wf_order_rec.COUNT LOOP
      L_rowid                 := chartorowid(I_wf_order_rec(i).row_id);
      L_wf_order_no           := I_wf_order_rec(i).wf_order_no;
      L_wf_order_line_no      := I_wf_order_rec(i).wf_order_line_no;
      L_item                  := I_wf_order_rec(i).item;
      L_cust_loc              := I_wf_order_rec(i).cust_loc;
      L_need_date             := I_wf_order_rec(i).need_date;
      L_source_loc_id         := I_wf_order_rec(i).source_loc_id;
      L_source_loc_type       := I_wf_order_rec(i).source_loc_type;
      L_new_requested_qty     := I_wf_order_rec(i).requested_qty;

      open C_WF_ORDER_STATUS;
      fetch C_WF_ORDER_STATUS into L_status, L_order_type;
      close C_WF_ORDER_STATUS;

      open C_OLD_REQUESTED_QTY;
      fetch C_OLD_REQUESTED_QTY into L_old_requested_qty;
      close C_OLD_REQUESTED_QTY;

      ---
      L_table := 'WF_ORDER_DETAIL';
      L_key1  := L_rowid;

      open C_LOCK_WF_ORDER_DETAIL;
      close C_LOCK_WF_ORDER_DETAIL;
      ---

      update wf_order_detail
         set requested_qty        = I_wf_order_rec(i).requested_qty,
             fixed_cost           = I_wf_order_rec(i).fixed_cost,
             need_date            = I_wf_order_rec(i).need_date,
             not_after_date       = I_wf_order_rec(i).not_after_date,
             last_update_datetime = sysdate,
             last_update_id       = get_user
       where rowid                = chartorowid(I_wf_order_rec(i).row_id);

      if L_status = 'A' and I_wf_order_rec(i).source_loc_type in ('SU', 'ST') then    -- UPDATE_ORDER_INFO will handle audit entry for WH sourced.
         if WF_ORDER_SQL.ORDER_AUDIT(L_error_message,
                                     L_wf_order_no,
                                     L_wf_order_line_no,
                                     'N',
                                     L_old_requested_qty) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;

      --if this is a Deposit content item, repeat the processing for container item
      if SYNC_CONTAINER_ITEM(L_error_message,
                             L_wf_order_no,
                             L_status,
                             L_item,
                             L_source_loc_type,
                             L_source_loc_id,
                             L_cust_loc,
                             L_old_requested_qty,
                             L_new_requested_qty,
                             I_wf_order_rec(i).need_date,
                             I_wf_order_rec(i).not_after_date,
                             NULL,  --I_cancel_reason
                             'N') = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Update linked transfer and store orders if one exists.
      if (L_status = 'I' and L_order_type in ('X', 'A')) or
         (L_status = 'A' and L_order_type in ('M','E')) then
         ---
         if L_source_loc_type = 'ST' or (L_source_loc_type = 'WH' and L_order_type in ('X', 'A')) then
            ---
            open C_GET_TSF_DETAIL;
            fetch C_GET_TSF_DETAIL into L_tsf_no;
            close C_GET_TSF_DETAIL;
            ---
            if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL(L_error_message,
                                                    L_tsf_no,
                                                    L_item,
                                                    L_new_requested_qty) = FALSE THEN
               raise PROGRAM_ERROR;
            end if;
         elsif L_source_loc_type = 'WH' then    -- Manual/EDI orders
            L_wf_order_rec.wf_order_no      := L_wf_order_no;
            L_wf_order_rec.wf_order_line_no := L_wf_order_line_no;
            L_wf_order_rec.item             := L_item;
            L_wf_order_rec.source_loc_type  := 'WH';
            L_wf_order_rec.source_loc_id    := L_source_loc_id;
            L_wf_order_rec.cust_loc         := L_cust_loc;
            L_wf_order_rec.need_date        := L_need_date;
            L_wf_order_rec.requested_qty    := L_new_requested_qty;

            if WF_ORDER_DETAIL_SQL.UPDATE_ORDER_INFO(L_wf_order_rec,
                                                     L_status,
                                                     L_old_requested_qty,
                                                     'N') = FALSE then   --cancel_ind
               L_error_message := L_wf_order_rec.error_message;
               raise PROGRAM_ERROR;
            end if;
         end if;
      end if; -- Status is Input for Auto/EG or A for Manual/EDI

      -- Update linked purchse order if one exists.
      if CHECK_LINKED_PO(L_error_message,
                         L_po_exist,
                         L_wf_order_no,
                         NULL)= FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_po_exist = TRUE then
         if WF_PO_SQL.MODIFY_FRANCHISE_PO (L_error_message,
                                           L_wf_order_no,
                                           'N',
                                           'N') = FALSE THEN
            raise PROGRAM_ERROR;
         end if;
      end if;

   END LOOP;

EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                L_table,
                                                L_key1,
                                                NULL);
         O_return_code   := 'FALSE';

      when PROGRAM_ERROR then
         O_error_message := L_error_message;
         O_return_code   := 'FALSE';

      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         O_return_code   := 'FALSE';

END UPDATE_PROCEDURE;
---------------------------------------------------------------------------------------------------
FUNCTION WF_ORDER_DETAIL_UPDATE (IO_wf_order_rec   IN OUT   WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD,
                                 I_status          IN       WF_ORDER_HEAD.STATUS%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) :='WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_UPDATE';
   L_item                    WF_ORDER_DETAIL.ITEM%TYPE;
   L_cust_loc                WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_source_loc_id           WF_ORDER_DETAIL.source_loc_id%TYPE;
   L_need_date               WF_ORDER_DETAIL.NEED_DATE%TYPE;
   L_not_after_date          WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE;
   L_available_qty           ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_wf_order_no             WF_ORDER_DETAIL.WF_ORDER_NO%TYPE;
   L_wf_order_line_no        WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_tsf_ship_qty            TSFDETAIL.SHIP_QTY%TYPE;
   L_tsf_qty                 TSFDETAIL.TSF_QTY%TYPE;
   L_tsf_status              TSFHEAD.STATUS%TYPE;
   L_tsf_exists              VARCHAR2(1)                          := 'Y';
   L_lead_time               REPL_ITEM_LOC.WH_LEAD_TIME%TYPE;
   L_order_type              WF_ORDER_HEAD.ORDER_TYPE%TYPE         := NULL;
   L_store_order_exist_ind   VARCHAR2(1)                           :='N';
   L_old_requested_qty       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_system_options_row      SYSTEM_OPTIONS%ROWTYPE;
   L_item_row                ITEM_MASTER%ROWTYPE;
   L_source_loc_type         WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE  := NULL;
   L_so_exists               BOOLEAN                               := FALSE;
   L_so_processed            BOOLEAN                               := FALSE;
   L_so_exists_diff_line     BOOLEAN                               := FALSE;
   L_so_processed_diff_line  BOOLEAN                               := FALSE;
   L_open_tsf_exists         VARCHAR2(1)                           := NULL;

   cursor C_OLD_REQUESTED_QTY IS
      select NVL(requested_qty,0)
        from wf_order_detail
       where wf_order_no = L_wf_order_no
         and item = L_item
         and customer_loc = L_cust_loc
         and need_date = L_need_date
         and source_loc_id = L_source_loc_id
         and source_loc_type = L_source_loc_type
         for update nowait;

   cursor C_STORE_ORD_PROCESSED is
      select 'Y'
        from store_orders
       where item = L_item
         and store = L_cust_loc
         and need_date = L_need_date
         and wf_order_no = L_wf_order_no
         and processed_date is not NULL;

   cursor C_WF_ORDER_TYPE IS
      select order_type
        from wf_order_head
       where wf_order_no = IO_wf_order_rec.wf_order_no;


   cursor C_GET_TSF_QTY IS
      select td.tsf_qty tsf_qty,
             GREATEST(NVL(td.ship_qty,0),
                      NVL(td.distro_qty,0),
                      NVL(td.selected_qty,0)) ship_qty,
             th.status status
        from tsfhead th,
             tsfdetail td
       where th.tsf_no      = td.tsf_no
         and td.item        = L_item
         and th.wf_order_no = L_wf_order_no
         and th.from_loc    = L_source_loc_id
         and th.to_loc      = L_cust_loc
         and th.create_id  != 'BATCH';

   cursor C_CHECK_OPEN_TSF_EXISTS IS
      select 'x'
        from tsfhead th,
             tsfdetail td
       where th.tsf_no      = td.tsf_no
         and td.item        = L_item
         and th.wf_order_no = L_wf_order_no
         and th.from_loc    = L_source_loc_id
         and th.to_loc      = L_cust_loc
         and th.status not in ('C','D')
         and td.tsf_qty     > GREATEST(NVL(td.ship_qty,0),
                                       NVL(td.distro_qty,0),
                                       NVL(td.selected_qty,0))
         and rownum = 1;

BEGIN

   L_wf_order_no      := IO_wf_order_rec.wf_order_no;
   L_wf_order_line_no := IO_wf_order_rec.wf_order_line_no;
   L_item             := IO_wf_order_rec.item;
   L_cust_loc         := IO_wf_order_rec.cust_loc;
   L_need_date        := IO_wf_order_rec.need_date;
   L_source_loc_id    := IO_wf_order_rec.source_loc_id;
   L_not_after_date   := IO_wf_order_rec.not_after_date;
   L_source_loc_type  := IO_wf_order_rec.source_loc_type;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(IO_wf_order_rec.error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(IO_wf_order_rec.error_message,
                                      L_item_row,
                                      L_item) = FALSE then
       return FALSE;
   end if;

   if WF_ORDER_SQL.GET_LEAD_TIME (IO_wf_order_rec.error_message,
                                  L_lead_time,
                                  L_item,
                                  L_cust_loc,
                                  L_source_loc_id,
                                  L_source_loc_type) =FALSE then
      return FALSE;
   end if;

   open C_WF_ORDER_TYPE;
   fetch C_WF_ORDER_TYPE into L_order_type;
   close C_WF_ORDER_TYPE;

   if I_status <> 'A' then
      if L_order_type not in ('X','A') then
         if IO_wf_order_rec.need_date < LP_vdate then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_NEED_DATE',NULL,NULL,NULL);
            return FALSE;
         end if;
      end if;
   end if;

   if IO_wf_order_rec.need_date > IO_wf_order_rec.not_after_date then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_NOT_AFTER_DATE',NULL,NULL,NULL);
      return FALSE;
   end if;

   if IO_wf_order_rec.requested_qty <= 0 then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_REQUESTED_QTY',NULL,NULL,NULL);
      return FALSE;
   end if;

   if L_order_type not in ('M','E') then
      L_lead_time := 0;    -- Lead time should not be checked for Auto transfers.
   end if;

   open C_OLD_REQUESTED_QTY;
   fetch C_OLD_REQUESTED_QTY into L_old_requested_qty;
   close C_OLD_REQUESTED_QTY;

   if I_status ='I' then
      if L_source_loc_type = 'SU' then
         if L_not_after_date - L_lead_time < LP_vdate then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
            return FALSE;
         end if;
      elsif L_source_loc_type = 'ST' then
         if L_not_after_date - L_lead_time < LP_vdate then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
            return FALSE;
         end if;

         if L_need_date > LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0) and L_order_type in ('M','E') then
             IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_ST_LEAD_DAYS',L_item, TO_CHAR(L_need_date,'DD-Mon-YYYY'), TO_CHAR(LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0),'DD-Mon-YYYY'));
             return FALSE;
         end if;
          ---
         if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(IO_wf_order_rec.error_message,
                                                       L_available_qty,
                                                       L_item,
                                                       L_source_loc_id,
                                                      'S')= FALSE then
           return FALSE;
         end if;
         if L_available_qty < IO_wf_order_rec.requested_qty then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_INSUFF_INV',L_item,L_source_loc_id,IO_wf_order_rec.requested_qty);
            return FALSE;
         end if;
      else  -- Warehouse
         if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(IO_wf_order_rec.error_message,
                                                       L_available_qty,
                                                       L_item,
                                                       L_source_loc_id,
                                                       'W') = FALSE then
            return FALSE;
         end if;
         ---
         if L_available_qty <0 then
            L_available_qty := 0;
         end if;
         ---
         if IO_wf_order_rec.requested_qty > L_available_qty then
            -- Store order is not created for Auto order type. Hence error out if inventory is not available.
            if L_order_type not in ('M','E') then
               IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_INSUFF_INV',L_item,L_source_loc_id,IO_wf_order_rec.requested_qty);
               return FALSE;
            end if;
            ---
            -- Warn user if pack is being put on store order
            if L_need_date > LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0)
               and (IO_wf_order_rec.pack_ind ='Y') then
               IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_ND_PACK_ITEM',L_item, TO_CHAR(L_need_date,'DD-Mon-YYYY'), TO_CHAR(LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0),'DD-Mon-YYYY'));
               return FALSE;
            end if;
            ---
            if IO_wf_order_rec.pack_ind ='Y' then
               IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_PACK_ITEM',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---
            if WF_ORDER_SQL.VERIFY_REPL_INFO(IO_wf_order_rec.error_message,
                                             L_cust_loc,
                                             L_item,
                                             L_not_after_date,
                                             L_source_loc_id) = FALSE then
               return FALSE;
            end if;
            ---
            if (L_not_after_date - L_lead_time) < LP_vdate then
               IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
               return FALSE;
            end if;
            ---
            if WF_TRANSFER_SQL.STORE_ORDER_EXISTS(IO_wf_order_rec.error_message,
                                                  L_so_exists,
                                                  L_so_processed,
                                                  L_item,
                                                  L_cust_loc,
                                                  L_need_date)= FALSE then
               return FALSE;
            end if;
            ---
            if L_so_exists = TRUE then
               IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('STORE_ORDER_EXISTS',NULL,NULL,NULL);
               return FALSE;
            end if;
            ---
         end if;
         ---
         if not IO_wf_order_rec.requested_qty > 0 then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_REQUESTED_QTY',NULL,NULL,NULL);
            return FALSE;
         end if;
         ---
         if IO_wf_order_rec.need_date < LP_vdate then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_NEED_DATE',NULL,NULL,NULL);
            return FALSE;
         end if;
         ---
      end if;  -- source_loc_type = WH
   elsif I_status in ('A','P')then
      ---
      open C_GET_TSF_QTY;
      fetch C_GET_TSF_QTY into L_tsf_qty, L_tsf_ship_qty, L_tsf_status;
      if C_GET_TSF_QTY%NOTFOUND then
         L_tsf_exists :='N';
      end if;
      close C_GET_TSF_QTY;
      ---
      if L_source_loc_type = 'ST' then
         if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(IO_wf_order_rec.error_message,
                                                       L_available_qty,
                                                       L_item,
                                                       L_source_loc_id,
                                                       'S')= FALSE then
            return FALSE;
         end if;
         ---
         if IO_wf_order_rec.requested_qty > L_old_requested_qty then
            if L_available_qty < (IO_wf_order_rec.requested_qty - L_old_requested_qty) then
               IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_INSUFF_INV',L_item,L_source_loc_id,IO_wf_order_rec.requested_qty);
               return FALSE;
            end if;
         end if;
         ---
         if L_tsf_exists ='Y' and (L_tsf_status in('C','D') or L_tsf_ship_qty >= L_tsf_qty) then
            IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_TSF_STATUS',NULL,NULL,NULL);
            return FALSE;
         end if;
      elsif L_source_loc_type = 'WH' then
         ---
         if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(IO_wf_order_rec.error_message,
                                                       L_available_qty,
                                                       L_item,
                                                       L_source_loc_id,
                                                       'W') = FALSE then
            return FALSE;
         end if;
         ---
         if L_available_qty < 0 then
            L_available_qty := 0;
         end if;
         ---
         if WF_TRANSFER_SQL.STORE_ORDER_EXISTS(IO_wf_order_rec.error_message,
                                               L_so_exists,
                                               L_so_processed,
                                               L_wf_order_no,
                                               L_wf_order_line_no)= FALSE then
            return FALSE;
         end if;
         ---
         if WF_TRANSFER_SQL.STORE_ORDER_EXISTS(IO_wf_order_rec.error_message,
                                               L_so_exists_diff_line,
                                               L_so_processed_diff_line,
                                               L_item,
                                               L_cust_loc,
                                               L_need_date)= FALSE then
            return FALSE;
         end if;
         ---
         if IO_wf_order_rec.requested_qty != L_old_requested_qty then
            if (IO_wf_order_rec.requested_qty - L_old_requested_qty) > L_available_qty then
               if IO_wf_order_rec.pack_ind ='Y' then
                  IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_PACK_ITEM',NULL,NULL,NULL);
                  return FALSE;
               end if;

               if WF_ORDER_SQL.VERIFY_REPL_INFO(IO_wf_order_rec.error_message,
                                                L_cust_loc,
                                                L_item,
                                                L_not_after_date,
                                                L_source_loc_id) = FALSE then
                  return FALSE;
               end if;
               ---
               ---Validate that if a store order doesn't already exist for this line item, then no other store
               ---order is available for the same item/customer loc/need date combination.
               if L_so_exists = FALSE then
                  if L_so_exists_diff_line = TRUE then
                     IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('STORE_ORDER_EXISTS',NULL,NULL,NULL);
                     return FALSE;
                  end if;
               end if;
               ---
               if L_so_processed = TRUE then
                  IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_BACKORDER_PROCESSED',NULL,NULL,NULL);
                  return FALSE;
               end if;
               ---
               if (L_not_after_date - L_lead_time) < LP_vdate then
                  IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
                  return FALSE;
               end if;
               ---
               if L_need_date < LP_vdate then
                  IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_BACKORDER_NEED_DATE',NULL,NULL,NULL);
                  return FALSE;
               end if;
               ---
            else
               -- If quantity is available, atleast one of the following should be valid
               --   Unprocessed store order exists
               --   Store order does not exists and a new store order can be created
               --   Existing transfer line is not closed/cancelled or fully shipped
               --   Transfer line item does not exists

               open C_CHECK_OPEN_TSF_EXISTS;
               fetch C_CHECK_OPEN_TSF_EXISTS into L_open_tsf_exists;
               close C_CHECK_OPEN_TSF_EXISTS;
               ---
               if L_so_exists = FALSE and L_tsf_exists = 'Y' and L_open_tsf_exists is NULL then
                  if IO_wf_order_rec.pack_ind ='Y' then
                     IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_PACK_ITEM',NULL,NULL,NULL);
                     return FALSE;
                  end if;
                  ---
                  if WF_ORDER_SQL.VERIFY_REPL_INFO(IO_wf_order_rec.error_message,
                                                   L_cust_loc,
                                                   L_item,
                                                   L_not_after_date,
                                                   L_source_loc_id) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  ---Validate that if a store order doesn't already exist for this line item, then no other store
                  ---order is available for the same item/customer loc/need date combination.
                  if L_so_exists_diff_line = TRUE then
                     IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('STORE_ORDER_EXISTS',NULL,NULL,NULL);
                     return FALSE;
                  end if;
                  ---
                  if (L_not_after_date - L_lead_time) < LP_vdate then
                     IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
                     return FALSE;
                  end if;
                  ---
                  if L_need_date < LP_vdate then
                     IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_BACKORDER_NEED_DATE',NULL,NULL,NULL);
                     return FALSE;
                  end if;
                  ---
               end if;
               ---
               if L_so_processed = TRUE then
                  if L_open_tsf_exists is NULL then
                     IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_TSF_STATUS',NULL,NULL,NULL);
                     return FALSE;
                  end if;
               end if;
            end if; -- (IO_wf_order_rec.requested_qty - L_old_requested_qty) > L_available_qty
         end if;  -- IO_wf_order_rec.requested_qty != L_old_requested_qty
      end if;  -- source_loc_type = WH

      if IO_wf_order_rec.requested_qty < 0 then
         IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('WF_REQUESTED_QTY',NULL,NULL,NULL);
         return FALSE;
      end if;
   end if;  -- Status

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           to_char(SQLCODE));
      return FALSE;
END WF_ORDER_DETAIL_UPDATE;
---------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_INFO(IO_wf_order_rec          IN OUT   WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD,
                           I_status                 IN       WF_ORDER_HEAD.STATUS%TYPE,
                           I_old_order_qty          IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                           I_cancel_ind             IN       VARCHAR2)

RETURN BOOLEAN IS

   L_program                   VARCHAR2(64) :='WF_ORDER_DETAIL_SQL.UPDATE_ORDER_INFO';
   L_system_options_row        SYSTEM_OPTIONS%ROWTYPE;
   L_requested_qty             WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_remaining_requested_qty   WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_need_date                 WF_ORDER_DETAIL.NEED_DATE%TYPE;
   L_available_qty             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_update_tsfqty_amt         WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_create_tsfqty_amt         WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_update_soqty_amt          WF_ORDER_DETAIL.REQUESTED_QTY%TYPE       := 0;
   L_create_soqty_amt          WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_tsf_no                    TSFHEAD.TSF_NO%TYPE;
   L_tsf_qty                   TSFDETAIL.TSF_QTY%TYPE;
   L_tsf_type                  TSFHEAD.TSF_TYPE%TYPE;
   L_tsf_ship_qty              TSFDETAIL.SHIP_QTY%TYPE;
   L_item                      WF_ORDER_DETAIL.ITEM%TYPE;
   L_store                     WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_source_loc_id             WF_ORDER_DETAIL.source_loc_id%TYPE;
   L_source_loc_type           WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_wf_order_no               WF_ORDER_DETAIL.WF_ORDER_NO%TYPE;
   L_wf_order_line_no          WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_delete_store_order        VARCHAR2(1)                              :='N';
   L_exisitng_tsf_no           TSFHEAD.TSF_NO%TYPE                      := NULL;
   L_tsf_status                TSFHEAD.STATUS%TYPE;
   L_tsf_line_open             BOOLEAN                                  := FALSE;
   L_batch_tsf_exists          BOOLEAN                                  := TRUE;
   L_so_exists                 BOOLEAN                                  := FALSE;
   L_so_processed              BOOLEAN                                  := FALSE;
   L_tsf_exists                BOOLEAN                                  := FALSE;
   L_total_batch_shipped_qty   TSFDETAIL.SHIP_QTY%TYPE                  := 0;

   ---
   --This cursor will fetch the online transfer created only if it is not fully shipped
   --and is not closed/deleted.
   cursor C_GET_TSF_NO is
      select th.tsf_no,
             td.tsf_qty,
             GREATEST(NVL(td.ship_qty,0),
                      NVL(td.distro_qty,0),
                      NVL(td.selected_qty,0)) ship_qty,
             th.status
        from tsfhead th,
             tsfdetail td
       where th.tsf_no        = td.tsf_no
         and td.item          = L_item
         and th.wf_order_no   = L_wf_order_no
         and th.from_loc      = L_source_loc_id
         and th.to_loc        = L_store
         and th.wf_need_date  = L_need_date
         and th.create_id    != 'BATCH';

   --This cursor will fetch the transfer created by the store order
   cursor C_GET_BATCH_TSF_NO is
      select th.tsf_no,
             td.tsf_qty,
             GREATEST(NVL(td.ship_qty,0),
                      NVL(td.distro_qty,0),
                      NVL(td.selected_qty,0)) ship_qty,
             SUM(GREATEST(NVL(td.ship_qty,0),
                          NVL(td.distro_qty,0),
                          NVL(td.selected_qty,0))) over() total_ship_qty
        from tsfhead th,
             tsfdetail td
       where th.tsf_no        = td.tsf_no
         and td.item          = L_item
         and th.wf_order_no   = L_wf_order_no
         and th.to_loc        = L_store
         and th.wf_need_date  = L_need_date
         and th.status not in ('C','D')
         and th.create_id     = 'BATCH'
       order by th.delivery_date asc;

   TYPE TYP_batch_tsf_no           is TABLE of C_GET_BATCH_TSF_NO%ROWTYPE;
   TBL_batch_tsf_no                TYP_batch_tsf_no     := NULL;

   -- If a tsf already exists with the same source location, customer_loc and need date, then all new items
   -- should be added to that tsf instead of creating new tsf
   -- This cursor will fetch the transfer no created when enough quantity is available
   cursor C_GET_EXISTING_TSF is
      select th.tsf_no tsf_no
        from tsfhead th
       where th.wf_order_no  = L_wf_order_no
         and th.from_loc     = L_source_loc_id
         and th.to_loc       = L_store
         and th.wf_need_date = L_need_date
         and th.status not in ('C','D')
         and th.create_id   != 'BATCH';

BEGIN

   L_requested_qty    := IO_wf_order_rec.requested_qty;
   L_need_date        := IO_wf_order_rec.need_date;
   L_item             := IO_wf_order_rec.item;
   L_store            := IO_wf_order_rec.cust_loc;
   L_source_loc_id    := IO_wf_order_rec.source_loc_id;
   L_source_loc_type  := IO_wf_order_rec.source_loc_type;
   L_wf_order_no      := IO_wf_order_rec.wf_order_no;
   L_wf_order_line_no := IO_wf_order_rec.wf_order_line_no;

   -- This following logic is implemented in this function
   --  1. This is called only for edit of Warehouse sourced franchise order in Approved and In-Progress status
   --     to adjust the linked transfer, linked unprocessed store orders and replenishment batch created transfers
   --     because of franchise order initiated store orders.
   --  2. The data validation is done before calling this function by WF_ORDER_DETAIL_UPDATE for increase in
   --     quantity and WF_CANCEL_LINE for quantity cancellation.
   --  3. The function prioritize transfers over store orders if there are available inventory during update
   --     Decrease unprocessed store order if there are available qty and increase (update/create) transfer
   --     quantity.
   --  4. Create or increase store order quantity if partial or full quantity is available but the original
   --     transfer is closed, cancelled or line item is fully shipped.
   --  5. If store order has been processed, the edits are made to the original transfer and to the
   --     transfers created by the replenishment batch by processing the store orders.
   --  6. For line level cancellation, the linked transfer (online created and batch created) and unprocessed
   --     store orders are cancelled.
   --  7. Update wf audit table.

   -- This function only handles post approval adjustment to transfer qty and store orders.
   if I_status not in ('A','P') then
      return TRUE;
   end if;
   ---
   --If only fixed cost is changed and there are no quantity updates.
   if L_requested_qty = I_old_order_qty then
      return TRUE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(IO_wf_order_rec.error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   if WF_TRANSFER_SQL.STORE_ORDER_EXISTS(IO_wf_order_rec.error_message,
                                         L_so_exists,
                                         L_so_processed,
                                         L_wf_order_no,
                                         L_wf_order_line_no)= FALSE then
      return FALSE;
   end if;
   ---
   -- Get the transfer details for the transfer created for available inventory at the time of
   -- online approval or edi upload approval.
   open C_GET_TSF_NO;
   fetch C_GET_TSF_NO into L_tsf_no, L_tsf_qty, L_tsf_ship_qty, L_tsf_status;
   close C_GET_TSF_NO;
   if L_tsf_no is NOT NULL then
      L_tsf_exists := TRUE;
      if L_tsf_status not in ('C', 'D') and L_tsf_ship_qty < L_tsf_qty then
         L_tsf_line_open := TRUE;
      end if;
   end if;
   ---
   -- The processing of store orders written at the time of franchise order approval will result in
   -- one of two transfer to be created with create_id as 'BATCH'. Fetch the transfer details for
   -- batch created transfers.
   open C_GET_BATCH_TSF_NO;
   fetch C_GET_BATCH_TSF_NO bulk collect into TBL_batch_tsf_no;
   close C_GET_BATCH_TSF_NO;
   ---
   if TBL_batch_tsf_no is NULL or TBL_batch_tsf_no.count = 0 then
      L_batch_tsf_exists := FALSE;
   else
      L_total_batch_shipped_qty := TBL_batch_tsf_no(TBL_batch_tsf_no.FIRST).total_ship_qty;
   end if;
   ---
   if L_requested_qty > 0 then
      if (L_need_date <= (LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0))) then
         ---
         if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(IO_wf_order_rec.error_message,
                                                       L_available_qty,
                                                       L_item,
                                                       L_source_loc_id,
                                                       'W') = FALSE then
            return FALSE;
         end if;
         ---
         if L_available_qty <0 then
            L_available_qty := 0;
         end if;
         ---
         if L_so_processed = FALSE then
            if (L_requested_qty - I_old_order_qty)  <= L_available_qty then
               ---
               if L_tsf_line_open = TRUE  and L_so_exists = TRUE then
                  if (L_requested_qty - L_tsf_qty ) <= L_available_qty then
                     L_update_tsfqty_amt  := L_requested_qty;
                     L_delete_store_order :='Y';
                  end if;
                  ---
                  if (L_requested_qty - L_tsf_qty)>= L_available_qty then
                     L_update_soqty_amt   := L_requested_qty - (L_tsf_qty  + L_available_qty );
                     L_update_tsfqty_amt  := L_tsf_qty + L_available_qty;
                  end if;
                  ---
                  if L_available_qty =0 then
                     L_update_soqty_amt   := L_requested_qty - L_tsf_qty;
                  end if;
               else
                  if L_tsf_line_open = TRUE then
                     L_update_tsfqty_amt  := L_requested_qty;
                  elsif L_so_exists = TRUE then
                     ---
                     if L_tsf_exists = FALSE then
                        if L_available_qty > 0 then
                           if L_requested_qty > L_available_qty then
                              L_create_tsfqty_amt  := L_available_qty;
                              L_update_soqty_amt   := L_requested_qty - L_available_qty;
                           else
                              L_create_tsfqty_amt  := L_requested_qty;
                              L_delete_store_order :='Y';
                           end if;
                        else     -- L_available_qty = 0
                           L_update_soqty_amt := L_requested_qty;
                        end if;
                     else
                        L_update_soqty_amt := L_requested_qty - L_tsf_qty;
                     end if;
                  else -- Orignal transfer closed/cancelled or fully shipped and no store order exists
                     L_create_soqty_amt := L_requested_qty - I_old_order_qty;
                  end if;
                  ---
                  if L_so_exists = TRUE and L_update_soqty_amt = 0 then
                     L_delete_store_order :='Y';
                  end if;
                  ---
               end if; -- L_tsf_line_open = TRUE  and L_so_exists = TRUE
               ---
               if L_delete_store_order = 'Y' then
                  if WF_TRANSFER_SQL.DELETE_STORE_ORDER(IO_wf_order_rec.error_message,
                                                        L_wf_order_no,
                                                        L_wf_order_line_no) = FALSE then
                     return FALSE;
                  end if;
               end if;
               ---
            else -- (L_requested_qty - I_old_order_qty) > L_available_qty
               if L_tsf_line_open = TRUE then
                  L_update_tsfqty_amt := (L_tsf_qty + L_available_qty);
                  if L_so_exists = TRUE then
                     L_update_soqty_amt := L_requested_qty - (L_tsf_qty + L_available_qty );
                  else
                     L_create_soqty_amt := L_requested_qty - (L_tsf_qty + L_available_qty );
                  end if;
               elsif L_so_exists = TRUE then
                  if L_tsf_exists = FALSE then
                     L_create_tsfqty_amt := L_available_qty;
                     L_update_soqty_amt := L_requested_qty - L_available_qty;
                  else
                     L_update_soqty_amt := L_requested_qty - L_tsf_qty;
                  end if;
               else  -- Orignal transfer closed/cancelled or fully shipped and no store orde exists
                  L_create_soqty_amt := L_requested_qty - I_old_order_qty;
               end if;
               ---
            end if;
            ---
         else -- L_so_processed = true
            -- For processed store order, the pre-validation before calling update_order_info checks
            -- that quantity is either cancelled or increased if there is available inventory and an
            -- open transfer exists.
            if L_requested_qty > I_old_order_qty then
               if L_tsf_line_open = TRUE then
                  if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                            L_tsf_no,
                                                            L_item,
                                                            L_tsf_qty + (L_requested_qty - I_old_order_qty)) =FALSE then
                     return FALSE;
                  end if;
               else -- update the first batch created transfer
                  if L_batch_tsf_exists = TRUE and TBL_batch_tsf_no is not NULL and TBL_batch_tsf_no.count > 0 then
                     if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                              TBL_batch_tsf_no(TBL_batch_tsf_no.FIRST).tsf_no,
                                                              L_item,
                                                              TBL_batch_tsf_no(TBL_batch_tsf_no.FIRST).tsf_qty + (L_requested_qty - I_old_order_qty)) = FALSE then
                        return FALSE;
                     end if;
                  end if;
               end if;
               ---
            elsif L_requested_qty < I_old_order_qty then
               ---
               -- Reduce the transfer quantity for the reduction in requested quantity.
               -- L_requested_qty will be greater than total ship qty and hence there is not check again to validate new tsf qty > ship qty.
               if L_tsf_line_open = TRUE then
                  if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                            L_tsf_no,
                                                            L_item,
                                                            LEAST(L_tsf_qty, L_requested_qty - L_total_batch_shipped_qty)) =FALSE then
                     return FALSE;
                  end if;
                  ---
                  L_remaining_requested_qty := L_requested_qty - LEAST(L_tsf_qty, L_requested_qty - L_total_batch_shipped_qty);
               end if;
               ---
               -- Update batch created transfer for reduction in requested qty
               if L_batch_tsf_exists = TRUE and TBL_batch_tsf_no is not NULL and TBL_batch_tsf_no.count > 0 then
                  for i in 1..TBL_batch_tsf_no.count LOOP
                     if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                              TBL_batch_tsf_no(i).tsf_no,
                                                              L_item,
                                                              LEAST(TBL_batch_tsf_no(i).tsf_qty, GREATEST(L_remaining_requested_qty,TBL_batch_tsf_no(i).ship_qty))) = FALSE then
                        return FALSE;
                     end if;
                     ---
                     L_remaining_requested_qty := L_remaining_requested_qty - LEAST(TBL_batch_tsf_no(i).tsf_qty, GREATEST(L_remaining_requested_qty,TBL_batch_tsf_no(i).ship_qty));
                  end LOOP;
               end if;
               ---
            end if;
         end if; -- L_so_processed = true
      elsif L_need_date > LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0) then
         ---
         if L_tsf_line_open = TRUE then
            -- possible only when wf_order_lead_days was lowered after initial order approval.
            if WF_TRANSFER_SQL.WF_DELETE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                     L_tsf_no,
                                                     L_item) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_so_processed = FALSE then
            if L_so_exists = TRUE then
               L_update_soqty_amt := L_requested_qty;
            else
               L_create_soqty_amt := L_requested_qty;
            end if;
         end if;
      end if;
      ---
      if L_update_tsfqty_amt > 0 then
        if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                 L_tsf_no,
                                                 L_item,
                                                 L_update_tsfqty_amt) =FALSE then
           return FALSE;
        end if;
      end if;
      ---
      if L_create_tsfqty_amt > 0 then
         if L_tsf_line_open = TRUE then
           if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                    L_tsf_no,
                                                    L_item,
                                                    L_create_tsfqty_amt) = FALSE then
              return FALSE;
           end if;
         else
            L_tsf_type := 'FO';

            open C_GET_EXISTING_TSF;
            fetch C_GET_EXISTING_TSF into L_exisitng_tsf_no;
            close C_GET_EXISTING_TSF;
            ---
            if L_exisitng_tsf_no is not NULL then
               if WF_TRANSFER_SQL.WF_CREATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                        L_exisitng_tsf_no,
                                                        L_item,
                                                        L_create_tsfqty_amt) = FALSE then
                  return FALSE;
               end if;
            else  -- create a new transfer
               if WF_TRANSFER_SQL.WF_CREATE_TSF_HEAD (IO_wf_order_rec.error_message,
                                                      L_tsf_no,
                                                      'W',
                                                      L_source_loc_id,
                                                      'S',
                                                      L_store,
                                                      L_tsf_type,
                                                      L_need_date,
                                                      L_wf_order_no,
                                                      NULL,
                                                      'A') =FALSE then
                  return FALSE;
               end if;
               ---
               if WF_TRANSFER_SQL.WF_CREATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                        L_tsf_no,
                                                        L_item,
                                                        L_create_tsfqty_amt) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      end if; --end of L_create_tsfqty_amt > 0
      ---
      if L_update_soqty_amt > 0 then
         if WF_TRANSFER_SQL.UPDATE_STORE_ORDER(IO_wf_order_rec.error_message,
                                               L_wf_order_no,
                                               L_wf_order_line_no,
                                               L_update_soqty_amt) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_create_soqty_amt > 0 then
         if WF_TRANSFER_SQL.CREATE_STORE_ORDER(IO_wf_order_rec.error_message,
                                               L_item,
                                               L_store,
                                               L_need_date,
                                               L_create_soqty_amt,
                                               L_wf_order_no,
                                               L_wf_order_line_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
   elsif L_requested_qty = 0 then
      ---
      if L_tsf_line_open = TRUE then
         if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                  L_tsf_no,
                                                  L_item,
                                                  L_requested_qty) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_batch_tsf_exists = TRUE and TBL_batch_tsf_no is not NULL and TBL_batch_tsf_no.count > 0 then
         for i in 1..TBL_batch_tsf_no.count LOOP
            if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL (IO_wf_order_rec.error_message,
                                                     TBL_batch_tsf_no(i).tsf_no,
                                                     L_item,
                                                     L_requested_qty) = FALSE then
               return FALSE;
            end if;
         end LOOP;
      end if;
      ---
      if L_so_exists = TRUE and L_so_processed = FALSE then
         if WF_TRANSFER_SQL.DELETE_STORE_ORDER(IO_wf_order_rec.error_message,
                                               L_wf_order_no,
                                               L_wf_order_line_no) = FALSE then
            return FALSE;
         end if;
      end if;
   end if; --- check for L_requested_qty > 0 ends
   ---
   if L_requested_qty != I_old_order_qty then
      if WF_ORDER_SQL.ORDER_AUDIT(IO_wf_order_rec.error_message,
                                  L_wf_order_no,
                                  L_wf_order_line_no,
                                  I_cancel_ind,
                                  I_old_order_qty)= FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          L_program,
                                                          to_char(SQLCODE));
      return FALSE;
END UPDATE_ORDER_INFO;
------------------------------------------------------------------------------------------------
PROCEDURE GET_ITEM_SUBSTITUTE_INFO (O_item_sub_tbl     IN OUT   ITEM_SUB_TBL,
                                    O_item_count_flag  IN OUT   VARCHAR2,
                                    I_item             IN       WF_ORDER_DETAIL.ITEM%TYPE,
                                    I_source_loc_type  IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                    I_source_loc_id    IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE)
IS
   L_program             VARCHAR2(64)                     := 'WF_ORDER_DETAIL_SQL.GET_ITEM_SUBSTITUTE_INFO';
   L_item_loc_rec        ITEM_LOC%ROWTYPE;
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_cnt                 NUMBER(4)                        := 1;
   L_item_cnt            NUMBER(4)                        := 0;
   L_to_value            ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_item_sub_tot_count  NUMBER(4)                        :=0;
   L_item_count_flag     VARCHAR2(1)                      := 'N';

   cursor C_ITEM_SUB_TOT_COUNT is
      select count(distinct sub_item)
        from sub_items_detail sid,
             v_item_master vim
       where sid.item     = I_item
         and vim.item     = sid.sub_item
         and LP_vdate between sid.start_date and sid.end_date;

   cursor C_ITEM_SUB_WH_ST_INFO is
      select sid.sub_item sub_item,
             vim.item_desc item_desc,
             vim.standard_uom uom,
             ils.stock_on_hand available_qty
        from sub_items_detail sid,
             v_item_master vim,
             item_loc_soh ils,
             item_loc il
       where sid.item     = I_item
         and vim.item     = sid.sub_item
         and ils.item     = sid.sub_item
         and ils.loc      = sid.location
         and il.item      = sid.item
         and il.loc       = sid.location
         and il.status    = 'A'
         and sid.location = I_source_loc_id
         and LP_vdate between sid.start_date and sid.end_date;

BEGIN

   open C_ITEM_SUB_TOT_COUNT;
   fetch C_ITEM_SUB_TOT_COUNT into L_item_sub_tot_count;
   close C_ITEM_SUB_TOT_COUNT;

   if I_source_loc_type in ('WH','ST') then
      FOR rec in C_ITEM_SUB_WH_ST_INFO LOOP
         if rec.uom != 'EA' then
            if UOM_SQL.CONVERT(L_error_message,
                               L_to_value,
                               'EA',
                               rec.available_qty,
                               rec.uom,
                               rec.sub_item,
                               NULL,
                               NULL,
                               NULL,
                               NULL) = FALSE then
               O_item_sub_tbl(1).error_message := L_error_message;
               return;
            end if;
         else
            L_to_value := rec.available_qty;
         end if;
      ---
         O_item_sub_tbl(L_cnt).sub_item      := rec.sub_item;
         O_item_sub_tbl(L_cnt).item_desc     := rec.item_desc;
         O_item_sub_tbl(L_cnt).uom           := 'EA';
         O_item_sub_tbl(L_cnt).available_qty := L_to_value;
         L_item_cnt := L_cnt;
         L_cnt := L_cnt + 1;
      END LOOP;

      if L_item_sub_tot_count > L_item_cnt then
         L_item_count_flag:= 'Y';
      end if;

      O_item_count_flag := L_item_count_flag;
   end if;

   return;

EXCEPTION
   when OTHERS then
      O_item_sub_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                             SQLERRM,
                                                             L_program,
                                                             to_char(SQLCODE));
   return ;
END GET_ITEM_SUBSTITUTE_INFO;
-----------------------------------------------------------------------------------------------
PROCEDURE GET_ITEM_DISTRIBUTE_INFO (O_item_dist_tbl    IN OUT   ITEM_DIST_TBL,
                                    O_item_count_flag  IN OUT   VARCHAR2,
                                    I_item             IN       WF_ORDER_DETAIL.ITEM%TYPE,
                                    I_item_type        IN       VARCHAR2,
                                    I_source_loc_type  IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                    I_source_loc_id    IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE)
IS

   L_item                 WF_ORDER_DETAIL.ITEM%TYPE;
   L_exists               VARCHAR2(1)   := 'N';
   L_cnt                  NUMBER(4)     := 1;
   L_item_cnt             NUMBER(4)     := 0;
   L_program              VARCHAR2(64)  := 'WF_ORDER_DETAIL_SQL.GET_ITEM_DISTRIBUTE_INFO';
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_ip_tot_count         NUMBER(4) :=0;
   L_ip_loc_wh_st_count   NUMBER(4) :=0;
   L_ip_loc_su_count      NUMBER(4) :=0;
   L_il_tot_count         NUMBER(4) :=0;
   L_il_loc_wh_st_count   NUMBER(4) :=0;
   L_il_loc_su_count      NUMBER(4) :=0;
   L_item_count_flag      VARCHAR2(1) := 'N';



   cursor C_IP_TOT_COUNT is
      select count(1)
        from v_item_master
       where item in (select to_char(im.item) item
                        from item_master im
                       where im.item_parent = I_item
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item_level=tran_level);

   cursor C_IP_LOC_WH_ST_COUNT is
      select count(1)
        from v_item_master
       where item in (select im.item item
                        from item_master im
                       where im.item_parent = I_item
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item_level=tran_level
                         and EXISTS (select 1
                                       from item_loc il
                                      where il.item     = im.item
                                        and il.loc      = I_source_loc_id
                                        and il.status   = 'A'
                                        and il.loc_type in ('W','S')));


   cursor C_IP_WH_ST_DIST_INFO is
      select item_parent,
             item,
             item_desc,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             NULL error_message
        from v_item_master
       where item in (select to_char(im.item) item
                        from item_master im
                       where im.item_parent = I_item
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item_level=tran_level
                         and EXISTS (select 1
                                       from item_loc il
                                      where il.item     = im.item
                                        and il.loc      = I_source_loc_id
                                        and il.status   = 'A'
                                        and il.loc_type in ('W','S') ));
   cursor C_IP_LOC_SU_COUNT is
      select count(1)
        from v_item_master
       where item in (select to_char(im.item) item
                        from item_master im,
                             item_supplier isu
                       where im.item_parent = I_item
                         and im.item_parent = isu.item
                         and isu.supplier = I_source_loc_id
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item_level=tran_level);

   cursor C_IP_SU_DIST_INFO is
      select item_parent,
             item,
             item_desc,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             NULL error_message
        from v_item_master
       where item in (select to_char(im.item) item
                        from item_master im,
                             item_supplier isu
                       where im.item_parent = I_item
                         and im.item_parent = isu.item
                         and isu.supplier = I_source_loc_id
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item_level=tran_level);
---
   cursor C_IL_TOT_COUNT is
      select count(1)
        from v_item_master
       where item in (select to_char(sd.item) item
                        from skulist_detail sd,
                             item_master im
                       where skulist = I_item
                         and sd.item_level= sd.tran_level
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item=sd.item
                       UNION ALL
                      select to_char(im.item) item
                        from skulist_detail sd,
                             item_master im
                       where skulist = I_item
                         and sd.item_level != sd.tran_level
                         and im.tran_level = sd.tran_level
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item_parent=sd.item
                         and (im.pack_ind='N' or im.simple_pack_ind='Y'));

   cursor C_IL_WH_ST_DIST_INFO is
      select item_parent,
             item,
             item_desc,
             diff_1,
             diff_2,
             diff_3,
             diff_4
        from v_item_master
       where item in (select to_char(sd.item) item
                        from skulist_detail sd,
                             item_master im
                       where skulist = I_item
                         and sd.item_level= sd.tran_level
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item=sd.item
                       UNION ALL
                      select to_char(im.item) item
                        from skulist_detail sd,
                             item_master im
                       where skulist = I_item
                         and sd.item_level != sd.tran_level
                         and im.tran_level = sd.tran_level
                         and im.inventory_ind = 'Y'
                         and im.status='A'
                         and im.item_parent=sd.item
                         and (im.pack_ind='N' or im.simple_pack_ind='Y'));

   cursor C_IL_CHECK_SOURCE_WH_ST is
      select 'Y'
        from item_loc
       where item = L_item
         and loc = I_source_loc_id
         and loc_type in ('W','S')
         and status   = 'A';

   cursor C_IL_CHECK_SOURCE_SU is
      select 'Y'
        from item_supplier
       where item = L_item
         and supplier = I_source_loc_id;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_row) = FALSE then
      return ;
   end if;

   if I_item_type = 'IP' then

      open C_IP_TOT_COUNT;
      fetch C_IP_TOT_COUNT into L_ip_tot_count;
      close C_IP_TOT_COUNT;

      if I_source_loc_type in ('WH','ST') then

         open C_IP_WH_ST_DIST_INFO ;
         fetch C_IP_WH_ST_DIST_INFO BULK COLLECT into O_item_dist_tbl;
         close C_IP_WH_ST_DIST_INFO;

         open C_IP_LOC_WH_ST_COUNT;
         fetch C_IP_LOC_WH_ST_COUNT into L_ip_loc_wh_st_count;
         close C_IP_LOC_WH_ST_COUNT;

         if L_ip_tot_count > L_ip_loc_wh_st_count then
            L_item_count_flag := 'Y';
         end if;

         O_item_count_flag := L_item_count_flag;
      else --I_source_loc_type ='SU'

         open C_IP_SU_DIST_INFO;
         fetch C_IP_SU_DIST_INFO BULK COLLECT into O_item_dist_tbl;
         close C_IP_SU_DIST_INFO;

         open C_IP_LOC_SU_COUNT;
         fetch C_IP_LOC_SU_COUNT into L_ip_loc_su_count;
         close C_IP_LOC_SU_COUNT;

         if L_ip_tot_count > L_ip_loc_su_count then
            L_item_count_flag := 'Y';
         end if;

         O_item_count_flag := L_item_count_flag;
      end if;
   else --I_item_type = 'IL'

      open C_IL_TOT_COUNT;
      fetch C_IL_TOT_COUNT into L_il_tot_count;
      close C_IL_TOT_COUNT;

      FOR rec in C_IL_WH_ST_DIST_INFO LOOP
         L_item := rec.item;
         if I_source_loc_type in ('WH','ST') then

            open C_IL_CHECK_SOURCE_WH_ST;
            fetch C_IL_CHECK_SOURCE_WH_ST into L_exists;
            close C_IL_CHECK_SOURCE_WH_ST;
         else --I_source_loc_type ='SU'

            open C_IL_CHECK_SOURCE_SU;
            fetch C_IL_CHECK_SOURCE_SU into L_exists;
            close C_IL_CHECK_SOURCE_SU;
         end if;

         if L_exists = 'Y' then
            O_item_dist_tbl(L_cnt).item_parent := rec.item_parent;
            O_item_dist_tbl(L_cnt).item        := rec.item;
            O_item_dist_tbl(L_cnt).item_desc   := rec.item_desc;
            O_item_dist_tbl(L_cnt).diff_1 := rec.diff_1;
            O_item_dist_tbl(L_cnt).diff_2 := rec.diff_2;
            O_item_dist_tbl(L_cnt).diff_3 := rec.diff_3;
            O_item_dist_tbl(L_cnt).diff_4 := rec.diff_4;
            L_exists := 'N';
            L_item_cnt := L_cnt;
            L_cnt := L_cnt + 1;
         end if;
      END LOOP;

      if L_il_tot_count > L_item_cnt then
         L_item_count_flag := 'Y';
      end if;

      O_item_count_flag := L_item_count_flag;

   end if;

   return;

EXCEPTION
   when OTHERS then
      O_item_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                             SQLERRM,
                                                             L_program,
                                                             to_char(SQLCODE));
   return ;
END GET_ITEM_DISTRIBUTE_INFO;
-----------------------------------------------------------------------------------------------
PROCEDURE LOCK_DIST_PROCEDURE  (O_item_dist_tbl IN OUT ITEM_DIST_TBL)
IS
   L_program              VARCHAR2(64)          := 'WF_ORDER_DETAIL_SQL.LOCK_DIST_PROCEDURE';
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_exists               VARCHAR2(1);
   L_rowid                VARCHAR2(25);
   L_table                VARCHAR2(30);
   L_key1                 VARCHAR2(100);

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
BEGIN
   FOR i in 1..O_item_dist_tbl.COUNT LOOP
      L_item := O_item_dist_tbl(i).item;
      ---
      L_table := 'V_ITEM_MASTER';
      L_key1  := L_item;
      ---
      select 'x'
        into L_exists
        from v_item_master
       where item = O_item_dist_tbl(i).item
         for update nowait;
   END LOOP;

EXCEPTION
   when RECORD_LOCKED then
      O_item_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                             L_table,
                                                             L_key1,
                                                             NULL);
      return;
   when OTHERS then
      O_item_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                             SQLERRM,
                                                             L_program,
                                                             TO_CHAR(SQLCODE));
      return;
END LOCK_DIST_PROCEDURE;
-----------------------------------------------------------------------------------------------
FUNCTION WF_CANCEL_HEAD (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_wf_order_no       IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                         I_cancel_reason     IN       WF_ORDER_DETAIL.CANCEL_REASON%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)                        := 'WF_ORDER_DETAIL_SQL.WF_CANCEL_HEAD';
   L_cancel_allowed       BOOLEAN                             := TRUE;
   L_open_cancel_qty      WF_ORDER_DETAIL.REQUESTED_QTY%TYPE  := 0;
   L_header_cancel_ind    VARCHAR2(1)                         := 'Y';
   L_status               WF_ORDER_HEAD.STATUS%TYPE           := NULL;
   L_order_type           WF_ORDER_HEAD.ORDER_TYPE%TYPE       := NULL;
   L_alloc_no             ALLOC_HEADER.ALLOC_NO%TYPE          := NULL;
   L_tsf_no               TSFHEAD.TSF_NO%TYPE                 := NULL;
   L_po_exist             BOOLEAN;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
   L_table                VARCHAR2(30);
   L_key                  VARCHAR2(100);


   cursor C_WF_ORDER_DET is
      select wd.item,
             source_loc_id,
             source_loc_type,
             customer_loc,
             need_date,
             requested_qty,
             wf_order_line_no
        from wf_order_detail wd,
             item_master im
       where wf_order_no = I_wf_order_no
         and wd.item     = im.item
         and NVL(im.deposit_item_type, '0') <> 'A';  --Process containers later

   cursor C_WF_ORDER_TYPE is
      select status,
             order_type
        from wf_order_head
       where wf_order_no = I_wf_order_no
         for update of status nowait;

   cursor C_LOCK_WF_ORDER_DETAIL is
      select 'x'
        from wf_order_detail
       where wf_order_no = I_wf_order_no
         for update nowait;

   cursor C_WF_TSF_DETAIL is
      select td.tsf_no,
             td.item
        from tsfhead th,
             tsfdetail td
       where th.wf_order_no = I_wf_order_no
         and th.tsf_no      = td.tsf_no;

   cursor C_GET_ALLOC is
      select alloc_no
        from alloc_detail
       where wf_order_no = I_wf_order_no
         and rownum = 1;
BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   if I_cancel_reason is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_cancel_reason',L_program,NULL);
      return FALSE;
   end if;

   L_table := 'WF_ORDER_HEAD';
   L_key   := 'WF_ORDER_NO : ' || I_wf_order_no;
   open C_WF_ORDER_TYPE;
   fetch C_WF_ORDER_TYPE into L_status, L_order_type;
   close C_WF_ORDER_TYPE;

   if CHECK_LINKED_PO(O_error_message,
                      L_po_exist,
                      I_wf_order_no,
                      NULL)= FALSE then
      return FALSE;
   end if;

   if L_status = 'R' then  -- For external orders only with order_type = 'A'.
      open C_GET_ALLOC;
      fetch C_GET_ALLOC into L_alloc_no;
      close C_GET_ALLOC;

      if L_alloc_no is not NULL then
         -- Allocation can have multiple linked franchise orders. The entire Handling is in WF_ALLOC_SQL.CANCEL_FRANCHISE_ORDER
         if WF_ALLOC_SQL.CANCEL_FRANCHISE_ORDER(O_error_message,
                                                I_cancel_reason,
                                                L_alloc_no) = FALSE then
            return FALSE;
         end if;
      else -- Linked Transfer or PO
         -- Update franchise order requested quantity to 0.
         -- Update status to 'C' for franchise order
         -- Cancel linked transfer and update tsf_qty = 0
         -- Cancel linked PO.
         L_table := 'WF_ORDER_DETAIL';
         L_key   := 'WF_ORDER_NO : ' || I_wf_order_no;
         open C_LOCK_WF_ORDER_DETAIL;
         close C_LOCK_WF_ORDER_DETAIL;

         update wf_order_detail
            set requested_qty = 0,
                cancel_reason = I_cancel_reason,
                last_update_datetime = sysdate,
                last_update_id = get_user
          where wf_order_no = I_wf_order_no;

         update wf_order_head
            set status = 'C',
                cancel_reason = I_cancel_reason,
                last_update_datetime = sysdate,
                last_update_id = get_user
          where wf_order_no = I_wf_order_no;

         if L_po_exist = TRUE then
            -- Cancel the linked purchase order.
            if WF_PO_SQL.CANCEL_FRANCHISE_PO (O_error_message,
                                              I_wf_order_no) = FALSE THEN
               return FALSE;
            end if;
         else
            -- Update TSF quantity to 0.
            for rec in C_WF_TSF_DETAIL LOOP
               L_tsf_no := rec.tsf_no;    --
               if WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL(O_error_message,
                                                       rec.tsf_no,
                                                       rec.item,
                                                       0) = FALSE THEN        -- Set tsf quantity to 0.
                  return FALSE;
               end if;
            end LOOP;
            -- Update status of Transfer to 'D'.
            if WF_TRANSFER_SQL.WF_UPDATE_TSF_HEAD(O_error_message,
                                                  L_tsf_no,
                                                  'D') = FALSE THEN           -- Set status to 'D'
               return FALSE;
            end if;
         end if;
      end if; -- Linked PO or Transfer.
   else -- status in 'A' or 'P'
      FOR rec IN C_WF_ORDER_DET LOOP
         if rec.source_loc_type = 'WH' then
            --Call CANCEL_TSF_STORE_ORDER which will do the following:
            --1. Update the linked tsf/store order.
            --2. Update wf_order_detail, wf_order_exp.
            --3. Update the item_loc_soh reserved buckets.
            --4. Insert the record into wf_order_audit
            --5. This function handles update for both 'A' and 'P' status

            if WF_ORDER_DETAIL_SQL.CANCEL_TSF_STORE_ORDER(O_error_message,
                                                          rec.item,
                                                          rec.source_loc_type,
                                                          rec.source_loc_id,
                                                          rec.customer_loc,
                                                          rec.need_date,
                                                          I_wf_order_no,
                                                          rec.wf_order_line_no,
                                                          I_cancel_reason) = FALSE then
               return FALSE;
            end if;
         else
            ---Update wf_order_detail
            ---Update expenses wf_order_exp
            ---Add to wf_order_audit
            if WF_ORDER_DETAIL_SQL.WF_CANCEL_LINE(O_error_message,
                                                  L_cancel_allowed,
                                                  L_open_cancel_qty,
                                                  0,          ---new qty
                                                  rec.requested_qty,
                                                  rec.item,
                                                  rec.source_loc_type,
                                                  rec.source_loc_id,
                                                  rec.customer_loc,
                                                  rec.need_date,
                                                  I_wf_order_no,
                                                  rec.wf_order_line_no,
                                                  L_header_cancel_ind,
                                                  I_cancel_reason) = FALSE then
               return FALSE;
            end if;
         end if;
      END LOOP;

      if L_status = 'A' and L_order_type in ('M','E') then
         update wf_order_head
            set status = 'C',
                cancel_reason = I_cancel_reason,
                last_update_datetime = sysdate,
                last_update_id = get_user
          where wf_order_no = I_wf_order_no;

         if L_po_exist = TRUE then
            if WF_PO_SQL.CANCEL_FRANCHISE_PO (O_error_message,
                                              I_wf_order_no) = FALSE THEN
               return FALSE;
            end if;
         end if;

         --This function should process records with source_loc_type = 'ST' and not 'WH'
         if WF_TRANSFER_SQL.CANCEL_FRANCHISE_TSF (O_error_message,
                                                  I_wf_order_no,
                                                  NULL) = FALSE THEN

            return FALSE;
         end if;

      elsif L_status = 'P' then

         -- Update the linked PO and Transfer once the line level cancellation is done.
         if L_po_exist = TRUE then
            if WF_PO_SQL.MODIFY_FRANCHISE_PO (O_error_message,
                                              I_wf_order_no,
                                              'N',
                                              'N') = FALSE THEN

               RETURN FALSE;
            end if;
         end if;

         --This function should process records with source_loc_type = 'ST' and not 'WH'
         if WF_TRANSFER_SQL.MODIFY_FRANCHISE_TSF (O_error_message,
                                                  I_wf_order_no,
                                                  NULL) = FALSE THEN

            return FALSE;
         end if;
      end if;
   end if; -- Status is not 'R'.
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key,
                                             NULL);
      return FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
   RETURN FALSE;
END WF_CANCEL_HEAD;
--------------------------------------------------------------------------------------------
FUNCTION WF_CANCEL_LINE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_cancel_allowed      IN OUT   BOOLEAN,
                        O_open_cancel_qty     IN OUT   WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                        I_new_qty             IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                        I_old_qty             IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                        I_item                IN       WF_ORDER_DETAIL.ITEM%TYPE,
                        I_source_loc_type     IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                        I_source_loc_id       IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                        I_customer_loc        IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                        I_need_date           IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
                        I_wf_order_no         IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                        I_wf_order_line_no    IN       WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE,
                        I_header_cancel_ind   IN       VARCHAR2,
                        I_cancel_reason       IN       WF_ORDER_DETAIL.CANCEL_REASON%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)                        := 'WF_ORDER_DETAIL_SQL.WF_CANCEL_LINE';
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);
   L_table                    VARCHAR2(30);
   L_key1                     VARCHAR2(100);
   L_key2                     VARCHAR2(100);
   L_status                   WF_ORDER_HEAD.STATUS%TYPE      := NULL;
   L_shpqty_total             TSFDETAIL.SHIP_QTY%TYPE        := NULL;
   L_new_requested_qty        WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_container_item           ITEM_MASTER.ITEM%TYPE;
   L_cont_requested_qty       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_cont_wf_order_line_no    WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_wf_order_rec             WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD;
   L_old_tsf_qty              TSFDETAIL.TSF_QTY%TYPE  := 0;
   L_cancel_head              VARCHAR2(1):='N';

   cursor C_WF_ORDER_STATUS IS
      select status
        from wf_order_head
       where wf_order_no = I_wf_order_no
         for update of status nowait;

   cursor C_LOCK_WF_ORDER_DETAIL IS
      select 'x'
        from wf_order_detail
       where wf_order_no = I_wf_order_no
         and wf_order_line_no = I_wf_order_line_no
         for update nowait;

   cursor C_CHECK_ALL_LINE_CANCEL is
      select 'Y'
        from wf_order_head
       where wf_order_no = I_wf_order_no
         and not exists (select 1
                           from wf_order_detail
                          where wf_order_no = I_wf_order_no
                            and requested_qty > 0);
BEGIN

   if I_new_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_new_qty','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_old_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_old_qty','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_source_loc_type','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_source_loc_id','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_customer_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_customer_loc','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_need_date','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_wf_order_no','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_wf_order_line_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_wf_order_line_no','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_header_cancel_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_header_cancel_ind','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_cancel_reason is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_cancel_reason','NULL','NOT NULL');
      return FALSE;
   end if;

   O_cancel_allowed := TRUE;

   L_table := 'WF_ORDER_HEAD';
   L_key1  := 'wf_order_no: '||I_wf_order_no;
   L_key2 := NULL;

   open C_WF_ORDER_STATUS;
   fetch C_WF_ORDER_STATUS into L_status;
   close C_WF_ORDER_STATUS;

   if L_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_WF_ORDER_NO',I_wf_order_no,NULL,NULL);
      return FALSE;
   elsif L_status not in  ('A','P') then
      O_error_message := SQL_LIB.CREATE_MSG('WF_INV_STATUS_FOR_CANCEL',I_wf_order_no,L_status,NULL);
      return FALSE;
   end if;

   L_table := 'WF_ORDER_DETAIL';
   L_key1  := TO_CHAR(I_wf_order_no);
   L_key2  := TO_CHAR(I_wf_order_line_no);

   open C_LOCK_WF_ORDER_DETAIL;
   close C_LOCK_WF_ORDER_DETAIL;

   if L_status = 'P' then
      if GET_SHIPPED_QUANTITY (O_error_message,
                               L_shpqty_total,
                               I_wf_order_no,
                               I_source_loc_type,
                               I_source_loc_id,
                               I_customer_loc,
                               I_need_date,
                               I_item) = FALSE THEN

         return FALSE;
      end if;
   end if;

   -- If the detail cancellation is called from Cancel_head
   if I_header_cancel_ind = 'Y' then

      if L_status = 'P' then
         update wf_order_detail
            set requested_qty        = NVL(L_shpqty_total, 0),
                cancel_reason        = I_cancel_reason,
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where wf_order_no          = I_wf_order_no
            and wf_order_line_no     = I_wf_order_line_no
          returning requested_qty into L_new_requested_qty;
      elsif L_status = 'A' then
         update wf_order_detail
            set requested_qty        = 0,
                cancel_reason        = I_cancel_reason,
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where wf_order_no          = I_wf_order_no
            and wf_order_line_no     = I_wf_order_line_no
            returning requested_qty into L_new_requested_qty;
      end if;
   else -- Called from the form for reduction in qty
      if L_status = 'P' and I_new_qty < NVL(L_shpqty_total, 0) then
         O_cancel_allowed := FALSE;
         O_open_cancel_qty   := I_old_qty - NVL(L_shpqty_total, 0);
         O_error_message := SQL_LIB.CREATE_MSG('QTY_CANCEL_ORD_LESS_OUT',O_open_cancel_qty,NULL,NULL);
         return FALSE;
      end if;

      update wf_order_detail
         set requested_qty        = I_new_qty,
             cancel_reason        = I_cancel_reason,
             last_update_datetime = sysdate,
             last_update_id       = get_user
       where wf_order_no          = I_wf_order_no
         and wf_order_line_no     = I_wf_order_line_no
         returning requested_qty into L_new_requested_qty;
   end if;

   if WF_ORDER_SQL.ORDER_AUDIT(O_error_message,
                               I_wf_order_no,
                               I_wf_order_line_no,
                               'Y',     --cancel_ind
                               I_old_qty) = FALSE then
      return FALSE;
   end if;

   --If this is a Deposit content item, repeat the processing for container item
   if SYNC_CONTAINER_ITEM(O_error_message,
                          I_wf_order_no,
                          L_status,
                          I_item,
                          I_source_loc_type,
                          I_source_loc_id,
                          I_customer_loc,
                          I_old_qty,
                          L_new_requested_qty,
                          I_need_date,
                          NULL,  --I_not_after_date
                          I_cancel_reason,
                          'Y') = FALSE then
      return FALSE;
   end if;

   -- If the cancellation is because of header cancel, the transfer and PO updates will be handled in the calling function.
   -- For detail level cancellation called from form, update the linked entities.
   if I_header_cancel_ind = 'N' then
      -- For approved FO, if this line item is the last non fully cancelled franchise detail line, cancel the header.

      if I_source_loc_type = 'SU' then
         if WF_PO_SQL.MODIFY_FRANCHISE_PO(O_error_message,
                                          I_wf_order_no,
                                          'N',
                                          'N') = FALSE THEN

            return FALSE;
         end if;
      elsif I_source_loc_type = 'ST' then
         --This function will handle updates for transfers with source_loc_type = 'ST'
         if WF_TRANSFER_SQL.MODIFY_FRANCHISE_TSF(O_error_message,
                                                 I_wf_order_no,
                                                 NULL) = FALSE THEN

            return FALSE;
         end if;
      else
         ---
         L_wf_order_rec.wf_order_no      := I_wf_order_no;
         L_wf_order_rec.wf_order_line_no := I_wf_order_line_no;
         L_wf_order_rec.item             := I_item;
         L_wf_order_rec.source_loc_type  := 'WH';
         L_wf_order_rec.source_loc_id    := I_source_loc_id;
         L_wf_order_rec.cust_loc         := I_customer_loc;
         L_wf_order_rec.need_date        := I_need_date;
         L_wf_order_rec.requested_qty    := I_new_qty;

         --Call update_order_info which will update the linked transfer/store order or both
         if WF_ORDER_DETAIL_SQL.UPDATE_ORDER_INFO(L_wf_order_rec,
                                                  L_status,
                                                  I_old_qty,         --old order qty
                                                  'Y') = FALSE then  --cancel_ind
            ---
            O_error_message := L_wf_order_rec.error_message;
            return FALSE;
         end if;
      end if;

      if L_status = 'A' then
         open C_CHECK_ALL_LINE_CANCEL;
         fetch C_CHECK_ALL_LINE_CANCEL into L_cancel_head;
         close C_CHECK_ALL_LINE_CANCEL;
      end if;

      if L_cancel_head = 'Y' then
         -- If the line cancellation is cancalling the last record, cancel the franchise order
         -- and also the linked PO, TSF and store order.
         if WF_CANCEL_HEAD (O_error_message,
                            I_wf_order_no,
                            I_cancel_reason) = FALSE then
            return FALSE;
         end if;
      end if; -- Cancel_head = 'N'
   end if; -- I_header_cancel_ind
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END WF_CANCEL_LINE;
--------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LIST_SUOM_CLASS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'WF_ORDER_DETAIL_SQL.CHECK_ITEM_LIST_SUOM_CLASS';
   L_cnt       NUMBER;

   cursor C_SKULIST_ITEMS_UOMCLASS is
      select count(1)
        from (select uc.uom_class, count(1)
                from skulist_detail sd,
                     item_master im,
                     uom_class uc
               where sd.skulist = I_itemlist
                 and sd.item    = im.item
                 and uc.uom     = im.standard_uom
            group by uc.uom_class);

BEGIN

   if I_itemlist is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_itemlist',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_SKULIST_ITEMS_UOMCLASS;
   fetch C_SKULIST_ITEMS_UOMCLASS into L_cnt;
   close C_SKULIST_ITEMS_UOMCLASS;

   if L_cnt = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SKU_LIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   elsif L_cnt > 1 then
      O_error_message := SQL_LIB.CREATE_MSG('SKULIST_UOMCLASS_NOTSAME',
                                            I_itemlist,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ITEM_LIST_SUOM_CLASS;
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_LINKED_PO(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_po_exists         IN OUT BOOLEAN,
                         I_wf_order_no       IN     WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                         I_wf_order_line_no  IN     WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)  := 'WF_ORDER_DETAIL_SQL.CHECK_LINKED_PO';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_FO_PO_EXISTS is
      select 'Y'
        from ordhead
       where wf_order_no = I_wf_order_no
         and status not in ('C')
         and rownum = 1;

   cursor C_FO_PO_LINE_EXISTS is
      select 'Y'
        from ordhead oh,
             ordloc ol,
             wf_order_detail wod
       where wod.wf_order_no = I_wf_order_no
         and wod.wf_order_line_no = I_wf_order_line_no
         and wod.wf_order_no = oh.wf_order_no
         and oh.order_no = ol.order_no
         and oh.status not in ('C')
         and wod.item = ol.item
         and wod.source_loc_id = oh.supplier
         and wod.customer_loc = ol.location
         and rownum = 1;

BEGIN

   O_po_exists := FALSE;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   if I_wf_order_line_no is NOT NULL then

      open C_FO_PO_LINE_EXISTS;
      fetch C_FO_PO_LINE_EXISTS into L_exists;
      close C_FO_PO_LINE_EXISTS;

      if L_exists = 'Y' then
         O_po_exists := TRUE;
      end if;
   else

      open C_FO_PO_EXISTS;
      fetch C_FO_PO_EXISTS into L_exists;
      close C_FO_PO_EXISTS;

      if L_exists = 'Y' then
         O_po_exists := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
   RETURN FALSE;
END CHECK_LINKED_PO;
--------------------------------------------------------------------------------------------
FUNCTION WF_DUP_ORDER_LINE_EXISTS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT  BOOLEAN,
                                  I_order_no        IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                  I_cust_loc        IN      WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                                  I_source_loc      IN      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                                  I_source_loc_type IN      WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                  I_item            IN      WF_ORDER_DETAIL.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)  := 'WF_ORDER_DETAIL_SQL.WF_DUP_ORDER_LINE_EXISTS';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_ORDER_LINE_EXISTS IS
      select 'Y'
        from wf_order_detail wod,
             wf_order_head woh
       where wod.wf_order_no     = woh.wf_order_no
         and wod.wf_order_no     = I_order_no
         and wod.customer_loc    = I_cust_loc
         and wod.source_loc_id   = I_source_loc
         and wod.source_loc_type = I_source_loc_type
         and wod.item            = I_item
         and rownum = 1;
BEGIN

   O_exists := FALSE;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;
   if I_cust_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_cust_loc',L_program,NULL);
      return FALSE;
   end if;
   if I_source_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc',L_program,NULL);
      return FALSE;
   end if;
   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_type',L_program,NULL);
      return FALSE;
   end if;

   open C_ORDER_LINE_EXISTS;
   fetch C_ORDER_LINE_EXISTS into L_exists;
   close C_ORDER_LINE_EXISTS;

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
   RETURN FALSE;
END WF_DUP_ORDER_LINE_EXISTS;
----------------------------------------------------------------------------------------------------
FUNCTION GET_SHIPPED_QUANTITY (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_shipped_qty     IN OUT  TSFDETAIL.SHIP_QTY%TYPE,
                               I_wf_order_no     IN      WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                               I_source_loc_type IN      WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                               I_source_loc_id   IN      WF_ORDER_DETAIL.source_loc_id%TYPE,
                               I_customer_loc    IN      WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                               I_need_date       IN      WF_ORDER_DETAIL.NEED_DATE%TYPE,
                               I_item            IN      WF_ORDER_DETAIL.ITEM%TYPE)
RETURN BOOLEAN IS

  L_program       VARCHAR2(64)                  := 'WF_ORDER_DETAIL_SQL.GET_SHIPPED_QUANTITY';

   cursor C_GET_SHIP_QTY IS
      select sum(ship_qty_tbl.ship_tsf_qty)
        from (select td.tsf_no,
                     th.status,
                     NVL(td.tsf_qty,0) tsf_qty,
                     GREATEST(NVL(td.ship_qty,0),
                              NVL(td.distro_qty,0),
                              NVL(td.selected_qty,0)) ship_tsf_qty,
                     td.item item
                from tsfhead th,
                     tsfdetail td
               where th.wf_order_no   = I_wf_order_no
                 and th.from_loc      = I_source_loc_id
                 and th.from_loc_type = decode(I_source_loc_type, 'ST', 'S', 'WH', 'W')
                 and th.to_loc        = I_customer_loc
                 and th.tsf_no        = td.tsf_no
                 and td.item          = I_item) ship_qty_tbl
            group by ship_qty_tbl.item
      union
      select sum(sku.qty_expected)
        from ordhead ord,
             shipment s,
             shipsku sku
       where ord.wf_order_no   = I_wf_order_no
         and I_source_loc_type = 'SU'
         and ord.supplier      = I_source_loc_id
         and ord.order_no      = s.order_no
         and s.to_loc          = I_customer_loc
         and s.shipment        = sku.shipment
         and sku.item          = I_item
    group by sku.item;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   open C_GET_SHIP_QTY;
   fetch C_GET_SHIP_QTY into O_shipped_qty;
   close C_GET_SHIP_QTY;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      RETURN FALSE;
END GET_SHIPPED_QUANTITY;
----------------------------------------------------------------------------------------------------
FUNCTION ROUND_F_ORDER(IO_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_out_qty             IN OUT  WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                       O_rounded_ind         IN OUT  VARCHAR2,
                       I_item                IN      WF_ORDER_DETAIL.ITEM%TYPE,
                       I_supplier            IN      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                       I_origin_country      IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_in_qty              IN      WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                       I_uom_size            IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'WF_ORDER_DETAIL_SQL.ROUND_F_ORDER';
   L_round_lvl           ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE;
   L_to_case_pct         ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE ;
   L_to_layer_pct        ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE;
   L_to_pallet_pct       ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE ;
   L_must_roundup_ind    VARCHAR2(1)  := 'N';
   L_round_pct           SYSTEM_OPTIONS.ROUND_TO_INNER_PCT%TYPE;
   L_remainder           NUMBER;
   L_floor               NUMBER;
   L_threshold           NUMBER;

   CURSOR C_ISC_ROUND_INFO is
      select round_lvl,
             round_to_case_pct,
             round_to_inner_pct,
             round_to_pallet_pct
        from item_supp_country
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country;

BEGIN

   /* Check Input Values And Set Defaults   */
   if I_item is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_in_qty is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_in_qty',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   O_out_qty := I_in_qty;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_ISC_ROUND_INFO', 'item_supp_country', NULL);
   open C_ISC_ROUND_INFO;
   SQL_LIB.SET_MARK('FETCH', 'C_ISC_ROUND_INFO', 'item_supp_country', NULL);
   fetch C_ISC_ROUND_INFO into L_round_lvl, L_to_case_pct, L_to_layer_pct, L_to_pallet_pct;
   ---
   if C_isc_round_info%NOTFOUND then
      IO_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ISC',null,null,null);
      SQL_LIB.SET_MARK('CLOSE', 'C_ISC_ROUND_INFO', 'item_supp_country', NULL);
      close C_ISC_ROUND_INFO;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_ISC_ROUND_INFO', 'item_supp_country', NULL);
   close C_ISC_ROUND_INFO;
   ---
   if L_round_lvl in ('P', 'LP', 'CLP') then
      L_round_pct := L_to_pallet_pct;
   end if;
   ---
   if L_round_lvl in ('L', 'LP', 'CL', 'CLP') then
      L_round_pct := L_to_layer_pct;
   end if;
   ---
   if L_round_lvl in ('C', 'CL', 'CLP') then
      L_round_pct := L_to_case_pct;
   end if;

  /* Evaluate Requisite  Terms For Rounding */
   L_remainder := MOD(I_in_qty, I_uom_size);
   L_floor     := I_in_qty - L_remainder;
   L_threshold := (L_round_pct/100) * I_uom_size;
   ---
   /* Perform       Rounding */
   O_out_qty := I_in_qty; -- default if rounding doesn't occur
   O_rounded_ind := 'N';  -- default if rounding doesn't occur
   ---
   if I_in_qty < I_uom_size  then
      -- cannot round to zero, so go up to 1 unit
      O_out_qty := I_uom_size;
      O_rounded_ind := 'Y';
   else
      if ((L_remainder >= L_threshold)
          or (L_must_roundup_ind = 'Y' and L_remainder >0)) then
         -- threshold exceeded; round up to next unit OR forced upward rounding
         O_out_qty := L_floor + I_uom_size;
         O_rounded_ind := 'Y';
      else
         -- threshold not met AND final round; round down to next unit
         O_out_qty := L_floor;
         O_rounded_ind := 'Y';
      end if;
   end if;
   ---
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      IO_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      RETURN FALSE;
END ROUND_F_ORDER;
----------------------------------------------------------------------------------------------------
FUNCTION CANCEL_TSF_STORE_ORDER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                IN       WF_ORDER_DETAIL.ITEM%TYPE,
                                I_source_loc_type     IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                                I_source_loc_id       IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                                I_customer_loc        IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                                I_need_date           IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
                                I_wf_order_no         IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                                I_wf_order_line_no    IN       WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE,
                                I_cancel_reason       IN       WF_ORDER_DETAIL.CANCEL_REASON%TYPE)
RETURN BOOLEAN IS

   L_store_ord_qty                STORE_ORDERS.NEED_QTY%TYPE   := 0;
   L_tsf_qty                      TSFDETAIL.TSF_QTY%TYPE       := 0;
   L_open_to_cancel               TSFDETAIL.TSF_QTY%TYPE       := 0;
   L_cancel_qty                   TSFDETAIL.TSF_QTY%TYPE       := 0;
   L_tsf_no                       TSFDETAIL.TSF_NO%TYPE;
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(Record_Locked, -54);
   L_table                        VARCHAR2(30);
   L_key1                         VARCHAR2(100);
   L_key2                         VARCHAR2(100);
   L_store_ord_exists             BOOLEAN  := TRUE;
   L_ship_tsf_qty                 TSFDETAIL.TSF_QTY%TYPE       := 0;
   L_tsf_cancel_qty               TSFDETAIL.TSF_QTY%TYPE       := 0;
   L_rowid                        ROWID;
   L_tsf_seq_no                   TSFDETAIL.TSF_SEQ_NO%TYPE    := 0;
   L_tot_open_to_cancel           TSFDETAIL.TSF_QTY%TYPE       := 0;
   L_tot_cancel_qty               TSFDETAIL.TSF_QTY%TYPE       := 0;
   L_tsf_exists                   VARCHAR2(1);
   L_prev_req_qty                 WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_new_req_qty                  WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_order_status                 WF_ORDER_HEAD.STATUS%TYPE;

   --cursor to get unprocessed store_order need qty
   cursor C_GET_STORE_ORDER is
   select need_qty
     from store_orders
    where wf_order_no = I_wf_order_no
      and wf_order_line_no = I_wf_order_line_no
      and processed_date is NULL
      for update of need_qty nowait;

   --get the tsf qty and processed qty on the tsf
   cursor C_GET_TSF_QTY is
   select td.tsf_no,
          NVL(td.tsf_qty,0) tsf_qty,
          GREATEST(NVL(td.ship_qty,0),
                   NVL(td.distro_qty,0),
                   NVL(td.selected_qty,0)) ship_tsf_qty,
          td.rowid,
          td.tsf_seq_no
     from tsfhead th,
          tsfdetail td
    where th.wf_order_no = I_wf_order_no
      and th.from_loc = I_source_loc_id
      and th.to_loc = I_customer_loc
      and th.wf_need_date = I_need_date
      and th.tsf_no = td.tsf_no
      and td.item = I_item
    order by th.delivery_date desc
      for update nowait;

   -- check if transfer exists for a wf order
   cursor C_TSF_EXISTS is
      select 'x'
        from tsfhead th
       where th.wf_order_no = I_wf_order_no
         and th.from_loc = I_source_loc_id
         and th.to_loc = I_customer_loc
         and th.wf_need_date = I_need_date;

   cursor C_GET_ORDER_STATUS is
      select status
        from wf_order_head
       where wf_order_no = I_wf_order_no;

   cursor C_LOCK_WF_ORDER_DETAIL is
      select requested_qty
        from wf_order_detail
       where wf_order_no      = I_wf_order_no
         and wf_order_line_no = I_wf_order_line_no
         for update nowait;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_source_loc_type','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_source_loc_id','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_customer_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_customer_loc','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_need_date','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_wf_order_no','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_wf_order_line_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_wf_order_line_no','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_cancel_reason is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_cancel_reason','NULL','NOT NULL');
      return FALSE;
   end if;

   L_table := 'STORE_ORDERS';
   L_key1  := 'wf_order_no: '     || I_wf_order_no;
   L_key2  := 'wf_order_line_no: '||I_wf_order_line_no;
   open C_GET_STORE_ORDER;
   fetch C_GET_STORE_ORDER into L_store_ord_qty;
   if C_GET_STORE_ORDER%NOTFOUND then
      L_store_ord_exists := FALSE;
   end if;
   close C_GET_STORE_ORDER;

   open C_TSF_EXISTS;
   fetch C_TSF_EXISTS into L_tsf_exists;
   close C_TSF_EXISTS;

   if L_store_ord_exists = TRUE then
      -- If unprocessed store order exists, delete the store order.
      L_tot_open_to_cancel := L_store_ord_qty;

      delete store_orders
       where wf_order_no      = I_wf_order_no
         and wf_order_line_no = I_wf_order_line_no;

   end if;

   if L_tsf_exists is not NULL then
      FOR rec in C_GET_TSF_QTY LOOP
         L_tsf_no        := rec.tsf_no;
         L_tsf_qty       := rec.tsf_qty;
         L_ship_tsf_qty  := rec.ship_tsf_qty;
         L_rowid         := rec.rowid;
         L_tsf_seq_no    := rec.tsf_seq_no;

         -- qty that can be cancelled on the tsf
         L_tsf_cancel_qty     := GREATEST(L_tsf_qty - L_ship_tsf_qty,0);
         L_tot_open_to_cancel := L_tot_open_to_cancel + L_tsf_cancel_qty;

         L_table := 'TSFDETAIL';
         L_key1  := 'tsf_no: '    ||L_tsf_no;
         L_key2  := 'tsf_seq_no: '||L_tsf_seq_no;

         update tsfdetail
            set tsf_qty = tsf_qty - NVL(L_tsf_cancel_qty,0),
                updated_by_rms_ind = 'Y'
          where rowid = L_rowid;

         L_table := 'TSFHEAD';
         L_key1  := 'tsf_no: ' || L_tsf_no;
         L_key2  := NULL;

         update tsfhead th
            set status = 'D'
          where th.tsf_no = L_tsf_no
            and not exists ( select 1
                               from tsfdetail td
                              where td.tsf_no = th.tsf_no
                                and td.tsf_qty > 0);

         update tsfhead tsh
            set status = 'C',
                close_date = LP_vdate
          where tsh.tsf_no = L_tsf_no
            and tsh.status != 'D'
            and not exists ( select 1
                               from tsfdetail tsd
                              where tsd.tsf_no = tsh.tsf_no
                                and tsd.tsf_qty > NVL(tsd.received_qty,0));

         if WF_TRANSFER_SQL.WF_UPD_ITEM_RESV(O_error_message,
                                             I_item,
                                             -1 * GREATEST(L_tsf_cancel_qty, 0),
                                             I_source_loc_id,
                                             'W') = FALSE then      -- Hardcoding as this function is called only for source type 'WH'
            return FALSE;
         end if;

         if WF_TRANSFER_SQL.WF_UPD_ITEM_EXP(O_error_message,
                                            I_item,
                                            -1 * GREATEST(L_tsf_cancel_qty, 0),
                                            I_customer_loc,
                                            'S') = FALSE then
            return FALSE;
         end if;

      end LOOP;
   end if;

   L_table := 'WF_ORDER_DETAIL';
   L_key1  := 'wf_order_no: '     ||I_wf_order_no;
   L_key2  := 'wf_order_line_no: '||I_wf_order_line_no;

   open C_LOCK_WF_ORDER_DETAIL;
   fetch C_LOCK_WF_ORDER_DETAIL into L_prev_req_qty;
   close C_LOCK_WF_ORDER_DETAIL;

   L_new_req_qty := GREATEST(L_prev_req_qty - L_tot_open_to_cancel,0);

   update wf_order_detail
      set requested_qty        = L_new_req_qty,
          cancel_reason        = I_cancel_reason,
          last_update_datetime = sysdate,
          last_update_id       = get_user
    where wf_order_no          = I_wf_order_no
      and wf_order_line_no     = I_wf_order_line_no;

   if not WF_ORDER_SQL.ORDER_AUDIT(O_error_message,
                                   I_wf_order_no,
                                   I_wf_order_line_no,
                                   'Y',
                                   L_prev_req_qty) then
      return FALSE;
   end if;

   open C_GET_ORDER_STATUS;
   fetch C_GET_ORDER_STATUS into L_order_status;
   close C_GET_ORDER_STATUS;

   if SYNC_CONTAINER_ITEM(O_error_message,
                          I_wf_order_no,
                          L_order_status,
                          I_item,
                          I_source_loc_type,
                          I_source_loc_id,
                          I_customer_loc,
                          L_prev_req_qty,
                          L_new_req_qty,
                          I_need_date,
                          NULL,  --I_not_after_date
                          I_cancel_reason,
                          'Y') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'WF_ORDER_DETAIL_SQL.CANCEL_TSF_STORE_ORDER',
                                            TO_CHAR(SQLCODE));
   return FALSE;

END CANCEL_TSF_STORE_ORDER;
----------------------------------------------------------------------------------------------------
--This function will only handle updates to the Deposit container when a linked
--deposit content is modified.
----------------------------------------------------------------------------------------------------
FUNCTION SYNC_CONTAINER_ITEM(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no         IN       WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                             I_status              IN       WF_ORDER_HEAD.STATUS%TYPE,
                             I_item                IN       WF_ORDER_DETAIL.ITEM%TYPE,
                             I_source_loc_type     IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                             I_source_loc_id       IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                             I_customer_loc        IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
                             I_old_qty             IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                             I_new_qty             IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE,
                             I_need_date           IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
                             I_not_after_date      IN       WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE,
                             I_cancel_reason       IN       WF_ORDER_DETAIL.CANCEL_REASON%TYPE,
                             I_cancel_ind          IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64) := 'WF_ORDER_DETAIL_SQL.SYNC_CONTAINER_ITEM';
   L_table                    VARCHAR2(50) := 'WF_ORDER_DETAIL';

   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(RECORD_LOCKED, -54);

   L_container_item           WF_ORDER_DETAIL.ITEM%TYPE;
   L_cont_wf_order_line_no    WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;

   cursor C_GET_CONTAINER_LINE_NO is
      select wf_order_line_no
        from wf_order_detail
       where wf_order_no = I_wf_order_no
         and item = L_container_item
         and source_loc_type = I_source_loc_type
         and source_loc_id = I_source_loc_id
         and customer_loc = I_customer_loc
         for update nowait;
BEGIN
   if ITEM_ATTRIB_SQL.GET_CONTAINER_ITEM(O_error_message,
                                         L_container_item,
                                         I_item) = FALSE then
      return FALSE;
   end if;

   if L_container_item is not NULL then
     open C_GET_CONTAINER_LINE_NO;
     fetch C_GET_CONTAINER_LINE_NO into L_cont_wf_order_line_no;
     close C_GET_CONTAINER_LINE_NO;

     update wf_order_detail
        set requested_qty        = requested_qty + NVL(I_new_qty,0) - NVL(I_old_qty,0),
            need_date            = NVL(I_need_date,need_date),
            not_after_date       = NVL(I_not_after_date,not_after_date),
            cancel_reason        = decode(I_cancel_ind, 'Y', I_cancel_reason, cancel_reason),
            last_update_datetime = sysdate,
            last_update_id       = get_user
      where wf_order_no          = I_wf_order_no
        and wf_order_line_no     = L_cont_wf_order_line_no;

     if I_status in ('A', 'P') then
        if WF_ORDER_SQL.ORDER_AUDIT(O_error_message,
                                    I_wf_order_no,
                                    L_cont_wf_order_line_no,
                                    I_cancel_ind,
                                    I_old_qty) = FALSE then
           return FALSE;
        end if;
     end if;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'wf_order_no '||I_wf_order_no,
                                            'wf_order_line_no '||L_cont_wf_order_line_no);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END SYNC_CONTAINER_ITEM;
----------------------------------------------------------------------------------------------------
PROCEDURE LOCK_BUYER_PACK_PROCEDURE  (O_buyer_pack_tbl IN OUT BUYER_PACK_TBL)
IS
   L_program      VARCHAR2(64)      := 'WF_ORDER_DETAILS.LOCK_BUYER_PACK_PROCEDURE';
BEGIN
   return; -- Dummy call.
EXCEPTION
   when OTHERS then
      O_buyer_pack_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                              SQLERRM,
                                                              L_program,
                                                              TO_CHAR(SQLCODE));
     return;
END LOCK_BUYER_PACK_PROCEDURE;
----------------------------------------------------------------------------------------------------
PROCEDURE GET_BUYER_PACK_INFO(O_buyer_pack_tbl   IN OUT   BUYER_PACK_TBL,
                              O_item_count_flag  IN OUT   VARCHAR2,
                              I_item             IN       WF_ORDER_DETAIL.ITEM%TYPE,
                              I_source_loc_type  IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
                              I_source_loc_id    IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
                              I_requested_qty    IN       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE)
IS

cursor C_EXPLODE_BUYER_PACK is
   select v.pack_no,
          v.item,
          im.item_desc,
          v.qty * I_requested_qty,
          NULL error_message
     from v_packsku_qty v,
          item_master im,
          item_master im1
    where v.pack_no         = I_item
      and v.item            = im.item
      and im1.item          = v.pack_no
      and im1.status        = 'A'
      and im1.pack_ind      = 'Y'
      and im1.pack_type     = 'B'
      and im1.order_as_type = 'E'
      and im.inventory_ind  = 'Y'
      and I_source_loc_type in ('WH','ST')
      and EXISTS (select 1
                   from item_loc il
                  where il.item          = v.item
                    and il.loc           = I_source_loc_id
                    and il.status        = 'A')
   UNION ALL
   select v.pack_no,
          v.item,
          im.item_desc,
          v.qty * I_requested_qty,
          NULL error_message
     from v_packsku_qty v,
          item_master im,
          item_master im1
    where v.pack_no         = I_item
      and im1.item          = v.pack_no
      and im1.status        = 'A'
      and im1.pack_ind      = 'Y'
      and im1.pack_type     = 'B'
      and im1.order_as_type = 'E'
      and im.item           = v.item
      and im.inventory_ind  =  'Y'
      and I_source_loc_type = 'SU'
      and EXISTS (select 1
                    from item_supplier isu,
                         sups s
                   where isu.item         = v.item
                     and isu.supplier     = s.supplier
                     and sup_status       = 'A'
                     and isu.supplier     = I_source_loc_id);

cursor C_TOTAL_SKU is
   select count(1)
     from v_packsku_qty v,
          item_master im,
          item_master im1
    where v.pack_no        = I_item
      and im1.item          = v.pack_no
      and im1.status        = 'A'
      and im1.pack_ind      = 'Y'
      and im1.pack_type     = 'B'
      and im1.order_as_type = 'E'
      and im.item           = v.item
      and im.inventory_ind = 'Y';

   L_total_cnt    NUMBER := 0;
   L_actual_cnt   NUMBER := 0;
   L_program      VARCHAR2(64)  := 'WF_ORDER_DETAILS.GET_BUYER_PACK_INFO';

BEGIN

   open C_TOTAL_SKU;
   fetch C_TOTAL_SKU into L_total_cnt;
   close C_TOTAL_SKU;

   open C_EXPLODE_BUYER_PACK ;
   fetch C_EXPLODE_BUYER_PACK BULK COLLECT into O_buyer_pack_tbl;
   L_actual_cnt := C_EXPLODE_BUYER_PACK%ROWCOUNT;
   close C_EXPLODE_BUYER_PACK;

   -- If any component item was filtered out in the explode query, set the flag to notify users
   if L_total_cnt > L_actual_cnt then
      O_item_count_flag := 'Y';
   end if;

   return;
EXCEPTION
   when OTHERS then
      O_buyer_pack_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                              SQLERRM,
                                                              L_program,
                                                              to_char(SQLCODE));
   return ;
END GET_BUYER_PACK_INFO;
----------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_RANGED_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_ranged            IN OUT   BOOLEAN,
                               I_wf_order_no       IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'WF_ORDER_DETAIL_SQL.CHECK_ITEM_RANGED_IND';
   L_ranged_ind   VARCHAR2(1)  := NULL;

   -- If there is atleast one item for franchise order with ranged_ind = 'N' at to location or costing location, query will return 'N'.
   cursor C_CHECK_ITEM_RANGED_IND is
      select 'N'
        from item_loc il,
             (select il1.item,
                     il1.loc,
                     il1.costing_loc
                from wf_order_head woh,
                     wf_order_detail wod,
                     item_loc il1
               where woh.wf_order_no = I_wf_order_no
                 and woh.wf_order_no = wod.wf_order_no
                 and wod.item = il1.item
                 and wod.customer_loc = il1.loc
              group by il1.item, il1.loc, il1.costing_loc) itl
       where il.item = itl.item
         and il.loc in (itl.loc, itl.costing_loc)
         and il.ranged_ind = 'N'
         and rownum = 1;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CHECK_ITEM_RANGED_IND;
   fetch C_CHECK_ITEM_RANGED_IND into L_ranged_ind;
   close C_CHECK_ITEM_RANGED_IND;

   if L_ranged_ind = 'N' then
      O_ranged := FALSE;
   else
      O_ranged := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ITEM_RANGED_IND;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_ITEM_RANGED_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no       IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'WF_ORDER_DETAIL_SQL.CHANGE_ITEM_RANGED_IND';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   L_table         VARCHAR2(30) := 'ITEM_LOC';

   -- Lock the rows in ITEM_LOC that needs to be updated.
   cursor C_LOCK_ITEM_LOC is
      select il.rowid
        from item_loc il,
             (select il1.item,
                     il1.loc,
                     il1.costing_loc
                from wf_order_head woh,
                     wf_order_detail wod,
                     item_loc il1
               where woh.wf_order_no  = I_wf_order_no
                 and woh.wf_order_no  = wod.wf_order_no
                 and wod.item         = il1.item
                 and wod.customer_loc = il1.loc
              group by il1.item, il1.loc, il1.costing_loc) itl
       where il.item       = itl.item
         and il.loc        in (itl.loc, itl.costing_loc)
         and il.ranged_ind = 'N'
         for update of il.ranged_ind nowait;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Lock the records
   open C_LOCK_ITEM_LOC;
   close C_LOCK_ITEM_LOC;

   -- For the items in franchise order that have RANGED_IND = 'N' in ITEM_LOC table for to location or costing location,
   -- change RANGED_IND to 'Y'.
   merge into item_loc il
   using (select distinct il.item,
                          il.loc
            from item_loc il,
                 (select il1.item,
                         il1.loc,
                         il1.costing_loc
                    from wf_order_head woh,
                         wf_order_detail wod,
                         item_loc il1
                   where woh.wf_order_no  = I_wf_order_no
                     and woh.wf_order_no  = wod.wf_order_no
                     and wod.item         = il1.item
                     and wod.customer_loc = il1.loc
                  group by il1.item, il1.loc, il1.costing_loc) itl
           where il.item       =  itl.item
             and il.loc        in (itl.loc, itl.costing_loc)
             and il.ranged_ind =  'N') res
      on (res.item = il.item and res.loc = il.loc)
    when matched then
       update set ranged_ind = 'Y';

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHANGE_ITEM_RANGED_IND;
---------------------------------------------------------------------------------------------------------------------
FUNCTION WF_ORDER_DETAIL_CREATE_WRP (IO_wf_order_rec    IN OUT    WRP_WF_ORDER_DETAIL_REC)
RETURN INTEGER IS
   L_program VARCHAR2(50) := 'WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_CREATE_WRP';
   L_wf_order_rec WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD;
BEGIN
   --copy the data in database type IO_wf_order_rec to PLSQL type L_wf_order_rec
   L_wf_order_rec.wf_order_no               := IO_wf_order_rec.wf_order_no;
   L_wf_order_rec.item                      := IO_wf_order_rec.item;
   L_wf_order_rec. item_desc                := IO_wf_order_rec.item_desc;
   L_wf_order_rec.source_loc_type           := IO_wf_order_rec.source_loc_type;
   L_wf_order_rec.source_loc_id             := IO_wf_order_rec.source_loc_id;
   L_wf_order_rec.cust_loc                  := IO_wf_order_rec.cust_loc;
   L_wf_order_rec.need_date                 := IO_wf_order_rec.need_date;
   L_wf_order_rec.not_after_date            := IO_wf_order_rec.not_after_date;
   L_wf_order_rec.uom                       := IO_wf_order_rec.uom;
   L_wf_order_rec.requested_qty             := IO_wf_order_rec.requested_qty;
   L_wf_order_rec.available_qty             := IO_wf_order_rec.available_qty;
   L_wf_order_rec.customer_cost_vat_incl    := IO_wf_order_rec.customer_cost_vat_incl;
   L_wf_order_rec.total_cust_cost_vat_incl  := IO_wf_order_rec.total_cust_cost_vat_incl;
   L_wf_order_rec.source_loc_id_name        := IO_wf_order_rec.source_loc_id_name;
   L_wf_order_rec.cust_loc_name             := IO_wf_order_rec.cust_loc_name;
   L_wf_order_rec.ref_item                  := IO_wf_order_rec.ref_item;
   L_wf_order_rec.item_type                 := IO_wf_order_rec.item_type;
   L_wf_order_rec.cancel_reason             := IO_wf_order_rec.cancel_reason;
   L_wf_order_rec.acquisition_cost          := IO_wf_order_rec.acquisition_cost;
   L_wf_order_rec.customer_cost_vat_excl    := IO_wf_order_rec.customer_cost_vat_excl;
   L_wf_order_rec.total_cust_cost_vat_excl  := IO_wf_order_rec.total_cust_cost_vat_excl;
   L_wf_order_rec.vat_rate                  := IO_wf_order_rec.vat_rate;
   L_wf_order_rec.vat_amt                   := IO_wf_order_rec.vat_amt;
   L_wf_order_rec.margin_pct                := IO_wf_order_rec.margin_pct;
   L_wf_order_rec.linked_tsfpoalloc         := IO_wf_order_rec.linked_tsfpoalloc;
   L_wf_order_rec.linked_entity_status      := IO_wf_order_rec.linked_entity_status;
   L_wf_order_rec.fixed_cost                := IO_wf_order_rec.fixed_cost;
   L_wf_order_rec.order_currency            := IO_wf_order_rec.order_currency;
   L_wf_order_rec.ship_date                 := IO_wf_order_rec.ship_date;
   L_wf_order_rec.ship_qty                  := IO_wf_order_rec.ship_qty;
   L_wf_order_rec.row_id                    := IO_wf_order_rec.row_id;
   L_wf_order_rec.return_code               := IO_wf_order_rec.return_code;
   L_wf_order_rec.error_message             := IO_wf_order_rec.error_message;
   L_wf_order_rec.pack_ind                  := IO_wf_order_rec.pack_ind;
   L_wf_order_rec.wf_order_line_no          := IO_wf_order_rec.wf_order_line_no;

   if (WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_CREATE(L_wf_order_rec)) = FALSE then
      IO_wf_order_rec.error_message:=L_wf_order_rec.error_message;
      return 0;
   end if;
   return 1;
EXCEPTION
   WHEN OTHERS THEN
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          L_program,
                                                          TO_CHAR(SQLCODE));
      return 0;
END WF_ORDER_DETAIL_CREATE_WRP;
---------------------------------------------------------------------------------------------------------------------
FUNCTION WF_ORDER_DETAIL_UPDATE_WRP (IO_wf_order_rec   IN OUT   WRP_WF_ORDER_DETAIL_REC,
                                     I_status          IN       WF_ORDER_HEAD.STATUS%TYPE)
RETURN INTEGER IS
   L_program VARCHAR2(50) := 'WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_UPDATE_WRP';
   L_wf_order_rec WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD;
BEGIN
    --copy the data in database type IO_wf_order_rec to PLSQL type L_wf_order_rec
   L_wf_order_rec.wf_order_no               := IO_wf_order_rec.wf_order_no;
   L_wf_order_rec.item                      := IO_wf_order_rec.item;
   L_wf_order_rec. item_desc                := IO_wf_order_rec.item_desc;
   L_wf_order_rec.source_loc_type           := IO_wf_order_rec.source_loc_type;
   L_wf_order_rec.source_loc_id             := IO_wf_order_rec.source_loc_id;
   L_wf_order_rec.cust_loc                  := IO_wf_order_rec.cust_loc;
   L_wf_order_rec.need_date                 := IO_wf_order_rec.need_date;
   L_wf_order_rec.not_after_date            := IO_wf_order_rec.not_after_date;
   L_wf_order_rec.uom                       := IO_wf_order_rec.uom;
   L_wf_order_rec.requested_qty             := IO_wf_order_rec.requested_qty;
   L_wf_order_rec.available_qty             := IO_wf_order_rec.available_qty;
   L_wf_order_rec.customer_cost_vat_incl    := IO_wf_order_rec.customer_cost_vat_incl;
   L_wf_order_rec.total_cust_cost_vat_incl  := IO_wf_order_rec.total_cust_cost_vat_incl;
   L_wf_order_rec.source_loc_id_name        := IO_wf_order_rec.source_loc_id_name;
   L_wf_order_rec.cust_loc_name             := IO_wf_order_rec.cust_loc_name;
   L_wf_order_rec.ref_item                  := IO_wf_order_rec.ref_item;
   L_wf_order_rec.item_type                 := IO_wf_order_rec.item_type;
   L_wf_order_rec.cancel_reason             := IO_wf_order_rec.cancel_reason;
   L_wf_order_rec.acquisition_cost          := IO_wf_order_rec.acquisition_cost;
   L_wf_order_rec.customer_cost_vat_excl    := IO_wf_order_rec.customer_cost_vat_excl;
   L_wf_order_rec.total_cust_cost_vat_excl  := IO_wf_order_rec.total_cust_cost_vat_excl;
   L_wf_order_rec.vat_rate                  := IO_wf_order_rec.vat_rate;
   L_wf_order_rec.vat_amt                   := IO_wf_order_rec.vat_amt;
   L_wf_order_rec.margin_pct                := IO_wf_order_rec.margin_pct;
   L_wf_order_rec.linked_tsfpoalloc         := IO_wf_order_rec.linked_tsfpoalloc;
   L_wf_order_rec.linked_entity_status      := IO_wf_order_rec.linked_entity_status;
   L_wf_order_rec.fixed_cost                := IO_wf_order_rec.fixed_cost;
   L_wf_order_rec.order_currency            := IO_wf_order_rec.order_currency;
   L_wf_order_rec.ship_date                 := IO_wf_order_rec.ship_date;
   L_wf_order_rec.ship_qty                  := IO_wf_order_rec.ship_qty;
   L_wf_order_rec.row_id                    := IO_wf_order_rec.row_id;
   L_wf_order_rec.return_code               := IO_wf_order_rec.return_code;
   L_wf_order_rec.error_message             := IO_wf_order_rec.error_message;
   L_wf_order_rec.pack_ind                  := IO_wf_order_rec.pack_ind;
   L_wf_order_rec.wf_order_line_no          := IO_wf_order_rec.wf_order_line_no;
   
   if (WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_UPDATE(L_wf_order_rec, 
                                                  I_status)) = FALSE then
	  IO_wf_order_rec.error_message:=L_wf_order_rec.error_message;
      return 0;
   end if;
   return 1;
EXCEPTION
   WHEN OTHERS THEN
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          L_program,
                                                          TO_CHAR(SQLCODE));
      return 0;
END WF_ORDER_DETAIL_UPDATE_WRP;
----------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDER_INFO_WRP (IO_wf_order_rec   IN OUT   WRP_WF_ORDER_DETAIL_REC)
RETURN INTEGER IS
   L_program VARCHAR2(64) := 'WF_ORDER_DETAIL_SQL.INSERT_ORDER_INFO_WRP';
   L_wf_order_rec        WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD;
BEGIN
    --copy the data in database type IO_wf_order_rec to PLSQL type L_wf_order_rec
   L_wf_order_rec.wf_order_no               := IO_wf_order_rec.wf_order_no;
   L_wf_order_rec.item                      := IO_wf_order_rec.item;
   L_wf_order_rec. item_desc                := IO_wf_order_rec.item_desc;
   L_wf_order_rec.source_loc_type           := IO_wf_order_rec.source_loc_type;
   L_wf_order_rec.source_loc_id             := IO_wf_order_rec.source_loc_id;
   L_wf_order_rec.cust_loc                  := IO_wf_order_rec.cust_loc;
   L_wf_order_rec.need_date                 := IO_wf_order_rec.need_date;
   L_wf_order_rec.not_after_date            := IO_wf_order_rec.not_after_date;
   L_wf_order_rec.uom                       := IO_wf_order_rec.uom;
   L_wf_order_rec.requested_qty             := IO_wf_order_rec.requested_qty;
   L_wf_order_rec.available_qty             := IO_wf_order_rec.available_qty;
   L_wf_order_rec.customer_cost_vat_incl    := IO_wf_order_rec.customer_cost_vat_incl;
   L_wf_order_rec.total_cust_cost_vat_incl  := IO_wf_order_rec.total_cust_cost_vat_incl;
   L_wf_order_rec.source_loc_id_name        := IO_wf_order_rec.source_loc_id_name;
   L_wf_order_rec.cust_loc_name             := IO_wf_order_rec.cust_loc_name;
   L_wf_order_rec.ref_item                  := IO_wf_order_rec.ref_item;
   L_wf_order_rec.item_type                 := IO_wf_order_rec.item_type;
   L_wf_order_rec.cancel_reason             := IO_wf_order_rec.cancel_reason;
   L_wf_order_rec.acquisition_cost          := IO_wf_order_rec.acquisition_cost;
   L_wf_order_rec.customer_cost_vat_excl    := IO_wf_order_rec.customer_cost_vat_excl;
   L_wf_order_rec.total_cust_cost_vat_excl  := IO_wf_order_rec.total_cust_cost_vat_excl;
   L_wf_order_rec.vat_rate                  := IO_wf_order_rec.vat_rate;
   L_wf_order_rec.vat_amt                   := IO_wf_order_rec.vat_amt;
   L_wf_order_rec.margin_pct                := IO_wf_order_rec.margin_pct;
   L_wf_order_rec.linked_tsfpoalloc         := IO_wf_order_rec.linked_tsfpoalloc;
   L_wf_order_rec.linked_entity_status      := IO_wf_order_rec.linked_entity_status;
   L_wf_order_rec.fixed_cost                := IO_wf_order_rec.fixed_cost;
   L_wf_order_rec.order_currency            := IO_wf_order_rec.order_currency;
   L_wf_order_rec.ship_date                 := IO_wf_order_rec.ship_date;
   L_wf_order_rec.ship_qty                  := IO_wf_order_rec.ship_qty;
   L_wf_order_rec.row_id                    := IO_wf_order_rec.row_id;
   L_wf_order_rec.return_code               := IO_wf_order_rec.return_code;
   L_wf_order_rec.error_message             := IO_wf_order_rec.error_message;
   L_wf_order_rec.pack_ind                  := IO_wf_order_rec.pack_ind;
   L_wf_order_rec.wf_order_line_no          := IO_wf_order_rec.wf_order_line_no;
   
   if (WF_ORDER_DETAIL_SQL.INSERT_ORDER_INFO(L_wf_order_rec)) = FALSE then
      IO_wf_order_rec.error_message:= L_wf_order_rec.error_message;
      return 0;
   end if;
   return 1;
EXCEPTION
   WHEN OTHERS THEN
      IO_wf_order_rec.error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           TO_CHAR(SQLCODE));
      return 0;
END INSERT_ORDER_INFO_WRP;
----------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_WF_ORDER_DETAIL_WRP(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_return_code          IN OUT   VARCHAR2,
                                    I_wf_order_detail_tab  IN       WRP_WF_ORDER_DETAIL_TAB)
RETURN INTEGER IS
   L_program              VARCHAR2(64) := 'WF_ORDER_DETAIL_SQL.DELETE_WF_ORDER_DETAIL';

   L_wf_order_rec         WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD;
   L_wf_order_detail_tbl  WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_TAB;
BEGIN
   for i in I_wf_order_detail_tab.FIRST .. I_wf_order_detail_tab.LAST loop
        --copy the data in database type WRP_WF_ORDER_DETAIL_TAB to PLSQL type L_wf_order_detail_tbl
      L_wf_order_rec.wf_order_no               := I_wf_order_detail_tab(i).wf_order_no;
      L_wf_order_rec.item                      := I_wf_order_detail_tab(i).item;
      L_wf_order_rec. item_desc                := I_wf_order_detail_tab(i).item_desc;
      L_wf_order_rec.source_loc_type           := I_wf_order_detail_tab(i).source_loc_type;
      L_wf_order_rec.source_loc_id             := I_wf_order_detail_tab(i).source_loc_id;
      L_wf_order_rec.cust_loc                  := I_wf_order_detail_tab(i).cust_loc;
      L_wf_order_rec.need_date                 := I_wf_order_detail_tab(i).need_date;
      L_wf_order_rec.not_after_date            := I_wf_order_detail_tab(i).not_after_date;
      L_wf_order_rec.uom                       := I_wf_order_detail_tab(i).uom;
      L_wf_order_rec.requested_qty             := I_wf_order_detail_tab(i).requested_qty;
      L_wf_order_rec.available_qty             := I_wf_order_detail_tab(i).available_qty;
      L_wf_order_rec.customer_cost_vat_incl    := I_wf_order_detail_tab(i).customer_cost_vat_incl;
      L_wf_order_rec.total_cust_cost_vat_incl  := I_wf_order_detail_tab(i).total_cust_cost_vat_incl;
      L_wf_order_rec.source_loc_id_name        := I_wf_order_detail_tab(i).source_loc_id_name;
      L_wf_order_rec.cust_loc_name             := I_wf_order_detail_tab(i).cust_loc_name;
      L_wf_order_rec.ref_item                  := I_wf_order_detail_tab(i).ref_item;
      L_wf_order_rec.item_type                 := I_wf_order_detail_tab(i).item_type;
      L_wf_order_rec.cancel_reason             := I_wf_order_detail_tab(i).cancel_reason;
      L_wf_order_rec.acquisition_cost          := I_wf_order_detail_tab(i).acquisition_cost;
      L_wf_order_rec.customer_cost_vat_excl    := I_wf_order_detail_tab(i).customer_cost_vat_excl;
      L_wf_order_rec.total_cust_cost_vat_excl  := I_wf_order_detail_tab(i).total_cust_cost_vat_excl;
      L_wf_order_rec.vat_rate                  := I_wf_order_detail_tab(i).vat_rate;
      L_wf_order_rec.vat_amt                   := I_wf_order_detail_tab(i).vat_amt;
      L_wf_order_rec.margin_pct                := I_wf_order_detail_tab(i).margin_pct;
      L_wf_order_rec.linked_tsfpoalloc         := I_wf_order_detail_tab(i).linked_tsfpoalloc;
      L_wf_order_rec.linked_entity_status      := I_wf_order_detail_tab(i).linked_entity_status;
      L_wf_order_rec.fixed_cost                := I_wf_order_detail_tab(i).fixed_cost;
      L_wf_order_rec.order_currency            := I_wf_order_detail_tab(i).order_currency;
      L_wf_order_rec.ship_date                 := I_wf_order_detail_tab(i).ship_date;
      L_wf_order_rec.ship_qty                  := I_wf_order_detail_tab(i).ship_qty;
      L_wf_order_rec.row_id                    := I_wf_order_detail_tab(i).row_id;
      L_wf_order_rec.return_code               := I_wf_order_detail_tab(i).return_code;
      L_wf_order_rec.error_message             := I_wf_order_detail_tab(i).error_message;
      L_wf_order_rec.pack_ind                  := I_wf_order_detail_tab(i).pack_ind;
      L_wf_order_rec.wf_order_line_no          := I_wf_order_detail_tab(i).wf_order_line_no;
      L_wf_order_detail_tbl(i) := L_wf_order_rec;
   end loop;
   
   WF_ORDER_DETAIL_SQL.DELETE_WF_ORDER_DETAIL(O_error_message,
                                              O_return_code,
                                              L_wf_order_detail_tbl);
   if (O_return_code = 'FALSE') then
      return 0;
   else 
     return 1;
   end if;
            
                      
   return 1;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END DELETE_WF_ORDER_DETAIL_WRP;
----------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PROCEDURE_WRP (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_return_code            IN OUT   VARCHAR2,
                               I_wf_order_detail_tab    IN       WRP_WF_ORDER_DETAIL_TAB)
RETURN INTEGER IS
   L_program VARCHAR2(64) := 'WF_ORDER_DETAIL_SQL.UPDATE_PROCEDURE_WRP';

   L_wf_order_rec         WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_RECORD;
   L_wf_order_detail_tbl  WF_ORDER_DETAIL_SQL.WF_ORDER_DETAIL_TAB;
BEGIN
   for i in I_wf_order_detail_tab.FIRST .. I_wf_order_detail_tab.LAST loop
        --copy the data in database type WF_ORDER_DETAIL_TAB_WRP to PLSQL type L_wf_order_detail_tbl
      L_wf_order_rec.wf_order_no               := I_wf_order_detail_tab(i).wf_order_no;
      L_wf_order_rec.item                      := I_wf_order_detail_tab(i).item;
      L_wf_order_rec. item_desc                := I_wf_order_detail_tab(i).item_desc;
      L_wf_order_rec.source_loc_type           := I_wf_order_detail_tab(i).source_loc_type;
      L_wf_order_rec.source_loc_id             := I_wf_order_detail_tab(i).source_loc_id;
      L_wf_order_rec.cust_loc                  := I_wf_order_detail_tab(i).cust_loc;
      L_wf_order_rec.need_date                 := I_wf_order_detail_tab(i).need_date;
      L_wf_order_rec.not_after_date            := I_wf_order_detail_tab(i).not_after_date;
      L_wf_order_rec.uom                       := I_wf_order_detail_tab(i).uom;
      L_wf_order_rec.requested_qty             := I_wf_order_detail_tab(i).requested_qty;
      L_wf_order_rec.available_qty             := I_wf_order_detail_tab(i).available_qty;
      L_wf_order_rec.customer_cost_vat_incl    := I_wf_order_detail_tab(i).customer_cost_vat_incl;
      L_wf_order_rec.total_cust_cost_vat_incl  := I_wf_order_detail_tab(i).total_cust_cost_vat_incl;
      L_wf_order_rec.source_loc_id_name        := I_wf_order_detail_tab(i).source_loc_id_name;
      L_wf_order_rec.cust_loc_name             := I_wf_order_detail_tab(i).cust_loc_name;
      L_wf_order_rec.ref_item                  := I_wf_order_detail_tab(i).ref_item;
      L_wf_order_rec.item_type                 := I_wf_order_detail_tab(i).item_type;
      L_wf_order_rec.cancel_reason             := I_wf_order_detail_tab(i).cancel_reason;
      L_wf_order_rec.acquisition_cost          := I_wf_order_detail_tab(i).acquisition_cost;
      L_wf_order_rec.customer_cost_vat_excl    := I_wf_order_detail_tab(i).customer_cost_vat_excl;
      L_wf_order_rec.total_cust_cost_vat_excl  := I_wf_order_detail_tab(i).total_cust_cost_vat_excl;
      L_wf_order_rec.vat_rate                  := I_wf_order_detail_tab(i).vat_rate;
      L_wf_order_rec.vat_amt                   := I_wf_order_detail_tab(i).vat_amt;
      L_wf_order_rec.margin_pct                := I_wf_order_detail_tab(i).margin_pct;
      L_wf_order_rec.linked_tsfpoalloc         := I_wf_order_detail_tab(i).linked_tsfpoalloc;
      L_wf_order_rec.linked_entity_status      := I_wf_order_detail_tab(i).linked_entity_status;
      L_wf_order_rec.fixed_cost                := I_wf_order_detail_tab(i).fixed_cost;
      L_wf_order_rec.order_currency            := I_wf_order_detail_tab(i).order_currency;
      L_wf_order_rec.ship_date                 := I_wf_order_detail_tab(i).ship_date;
      L_wf_order_rec.ship_qty                  := I_wf_order_detail_tab(i).ship_qty;
      L_wf_order_rec.row_id                    := I_wf_order_detail_tab(i).row_id;
      L_wf_order_rec.return_code               := I_wf_order_detail_tab(i).return_code;
      L_wf_order_rec.error_message             := I_wf_order_detail_tab(i).error_message;
      L_wf_order_rec.pack_ind                  := I_wf_order_detail_tab(i).pack_ind;
      L_wf_order_rec.wf_order_line_no          := I_wf_order_detail_tab(i).wf_order_line_no;
      L_wf_order_detail_tbl(i) := L_wf_order_rec;
   end loop;
   
   WF_ORDER_DETAIL_SQL.UPDATE_PROCEDURE(O_error_message,
                                        O_return_code,
                                        L_wf_order_detail_tbl);
                              
   if (O_return_code = 'FALSE') then
      return 0;
   else 
     return 1;
   end if;
                      
   return 1;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END UPDATE_PROCEDURE_WRP;
---------------------------------------------------------------------------------------------------------------------
END WF_ORDER_DETAIL_SQL;
/