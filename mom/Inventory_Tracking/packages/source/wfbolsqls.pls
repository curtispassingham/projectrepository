CREATE OR REPLACE PACKAGE WF_BOL_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------
-- Name:    UPDATE_SHIPPED_WF_ORDER
-- Purpose: This function is used to update the wf_order_head status to 'P'
--          when a franchise order to either stockholding or non-stockholding
--          store is shipped.
----------------------------------------------------------------------------
FUNCTION UPDATE_SHIPPED_WF_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                                 I_distro_type     IN       SHIPSKU.DISTRO_TYPE%TYPE,
                                 I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE,
                                 I_to_loc_type     IN       ALLOC_DETAIL.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Name:    UPDATE_SHIPPED_WF_RETURN
-- Purpose: This function is used to update the wf_return_head status to 'P'
--          when a franchise return is shipped.
----------------------------------------------------------------------------
FUNCTION UPDATE_SHIPPED_WF_RETURN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Name:    WRITE_WF_BILLING_SALES
-- Purpose: This function is used to insert a record into the
--          wf_billing_sales table.
----------------------------------------------------------------------------
FUNCTION WRITE_WF_BILLING_SALES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no         IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                                I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                I_from_loc            IN       ITEM_LOC.LOC%TYPE,
                                I_from_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                                I_to_loc              IN       ITEM_LOC.LOC%TYPE,
                                I_costing_loc         IN       ITEM_LOC.COSTING_LOC%TYPE,
                                I_costing_loc_type    IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE,
                                I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                I_tran_date           IN       DATE)
RETURN BOOLEAN;

---------------------------------------------------------------------------
-- Name:    WRITE_WF_BILLING_RETURNS
-- Purpose: This function is used to insert a record into the
--          wf_billing_returns table.
----------------------------------------------------------------------------
FUNCTION WRITE_WF_BILLING_RETURNS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rma_no          IN       WF_RETURN_HEAD.RMA_NO%TYPE,
                                  I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                                  I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                  I_to_loc          IN       SHIPMENT.TO_LOC%TYPE,
                                  I_to_loc_type     IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                                  I_qty             IN       WF_RETURN_DETAIL.RETURNED_QTY%TYPE,
                                  I_tran_date       IN       DATE)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function Name: WRITE_WF_BILLING_SALES_RETURNS
-- Purpose:       This function checks if a WF_ORDER/WF_RETURN
--                is associated with the Transfer/Allocation and then calls
--                WF_BOL_SQL.WRITE_WF_BILLING_SALES or
--                WF_BOL_SQL.WRITE_WF_BILLING_RETURNS accordingly.
----------------------------------------------------------------------------
FUNCTION WRITE_WF_BILLING_SALES_RETURNS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                        I_distro_no             IN     SHIPSKU.DISTRO_NO%TYPE,
                                        I_distro_type           IN     SHIPSKU.DISTRO_TYPE%TYPE,
                                        I_qty                   IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                        I_from_loc              IN     ITEM_LOC.LOC%TYPE,
                                        I_from_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                        I_to_loc                IN     ITEM_LOC.LOC%TYPE,
                                        I_to_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                                        I_tran_date             IN     PERIOD.VDATE%TYPE,
                                        I_franchise_ordret_ind  IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------
END;
/
