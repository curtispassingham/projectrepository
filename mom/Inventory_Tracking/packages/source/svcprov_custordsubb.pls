CREATE OR REPLACE PACKAGE BODY SVCPROV_CUSTORDSUB AS
------------------------------------------------------------------------------------------------
LP_cre_type              VARCHAR2(15) := 'create';
LP_process_status_new    SVC_CUSTORDSUB.PROCESS_STATUS%TYPE := 'N';
LP_process_status_comp   SVC_CUSTORDSUB.PROCESS_STATUS%TYPE := 'C'; 
LP_user                  SVCPROV_CONTEXT.USER_NAME%TYPE     := get_user;
------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
---                                       PRIVATE FUNCTION PROTOTYPE:                        ---
------------------------------------------------------------------------------------------------
--Function Name: POP_TABLES
--Purpose:       This function will populate the SVC_CUSTORDSUB and SVC_CUSTORDSUBDTL
--               staging tables based on the values of the input object.
------------------------------------------------------------------------------
FUNCTION POP_TABLES(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id         IN      SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                    I_business_object    IN      "RIB_CustOrdSubColDesc_REC",
                    I_chunk_id           IN      SVC_CUSTORDSUB.CHUNK_ID%TYPE,
                    I_action_type        IN      SVC_CUSTORDSUB.ACTION_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------
--Function Name: CLEANUP_TABLES
--Purpose:       This function will delete the records from the staging tables for
--               the current process ID where process status is 'C'ompleted.
------------------------------------------------------------------------------
FUNCTION CLEANUP_TABLES(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id    IN      SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                        I_chunk_id      IN      SVC_CUSTORDSUB.CHUNK_ID%TYPE,
                        I_action_type   IN      SVC_CUSTORDSUB.ACTION_TYPE%TYPE)
RETURN BOOLEAN;


------------------------------------------------------------------------------------------------
---                                       PUBLIC PROCEDURE:                                  ---
------------------------------------------------------------------------------------------------
PROCEDURE CREATE_CO_SUBSTITUTE (O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                I_businessObject         IN     "RIB_CustOrdSubColDesc_REC")
IS

   L_program                       VARCHAR2(255) := 'SVCPROV_CUSTORDSUB.CREATE_CO_SUBSTITUTE';
   L_chunk_id                      SVC_CUSTORDSUB.CHUNK_ID%TYPE   := 1;
   L_process_id                    SVC_CUSTORDSUB.PROCESS_ID%TYPE;
   L_table_name                    VARCHAR2(50);
   L_status_code                   VARCHAR2(1) := API_CODES.SUCCESS;
   L_error_message                 RTK_ERRORS.RTK_TEXT%TYPE;
   
   PROGRAM_ERROR                   EXCEPTION;

   cursor C_GET_NEXT_PROCESS_ID is
      select svc_custordsub_proc_seq.NEXTVAL
        from dual;

BEGIN
   
   if I_businessObject is NULL or I_businessObject.CustOrdSubDesc_TBL is NULL or I_businessObject.CustOrdSubDesc_TBL.COUNT = 0 then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                                  'I_businessObject',
                                                   L_program,
                                                   NULL);
      raise PROGRAM_ERROR;
   end if;
   
   if I_businessObject.CustOrdSubDesc_TBL.COUNT <> I_businessObject.collection_size then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_CUSTORDSUB_COL_COUNT', 
                                                   NULL,
                                                   NULL,
                                                   NULL);                                        
      raise PROGRAM_ERROR;
   end if;
   
   --get a unique process id for this item substitution request
   open C_GET_NEXT_PROCESS_ID;
   fetch C_GET_NEXT_PROCESS_ID into L_process_id;
   close C_GET_NEXT_PROCESS_ID;
   
   -- Stage the item substitution requests to interface staging tables
   if POP_TABLES (L_error_message,
                  L_process_id,
                  I_businessObject,
                  L_chunk_id,
                  LP_cre_type) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Call the function containing the core business logic to process the customer substitute and update the 
   -- ordcust_detail table, along with releasing and reserving inventory.
   if CORESVC_CUSTORDSUB.CREATE_CO_SUBSTITUTE (L_error_message,
                                               L_process_id,
                                               L_chunk_id) = FALSE then
      
      --Call PARSE_STAGED_ERR_MSG() to parse the ';' delimited error messages on the SVC_CUSTORDSUB and
      --SVC_CUSTORDSUBDTL tables, and return them as individual error strings in "RIB_FailStatus_TBL".                                               
      if L_error_message is NULL then
         L_table_name := 'SVC_CUSTORDSUB';
         if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(L_error_message,
                                                 O_serviceOperationStatus.FailStatus_TBL,
                                                 L_table_name,
                                                 L_process_id) = FALSE then
            raise PROGRAM_ERROR;
         end if;
        
         L_table_name := 'SVC_CUSTORDSUBDTL';
         if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(L_error_message,
                                                 O_serviceOperationStatus.FailStatus_TBL,
                                                 L_table_name,
                                                 L_process_id) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;
      raise PROGRAM_ERROR;
   end if;

   -- Delete the staged item substitution requests from interface staging table where process_status = 'C'ompleted
   if CLEANUP_TABLES (L_error_message,
                      L_process_id,
                      L_chunk_id,
                      LP_cre_type) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   return;

EXCEPTION
   when PROGRAM_ERROR then
      if C_GET_NEXT_PROCESS_ID%ISOPEN then
         close C_GET_NEXT_PROCESS_ID;
      end if;

      L_status_code := API_CODES.UNHANDLED_ERROR;
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      if C_GET_NEXT_PROCESS_ID%ISOPEN then
         close C_GET_NEXT_PROCESS_ID;
      end if;

      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END CREATE_CO_SUBSTITUTE;
-------------------------------------------------------------------------------------------------------
FUNCTION POP_TABLES(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id         IN      SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                    I_business_object    IN      "RIB_CustOrdSubColDesc_REC",
                    I_chunk_id           IN      SVC_CUSTORDSUB.CHUNK_ID%TYPE,
                    I_action_type        IN      SVC_CUSTORDSUB.ACTION_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(100) := 'SVCPROV_CUSTORDSUB.POP_TABLES';
   L_custordsub_id       SVC_CUSTORDSUB.CUSTORDSUB_ID%TYPE;
   
   L_dtl_index           NUMBER(10) := 0;
   
   TYPE T_svc_custordsub_TBL is TABLE OF SVC_CUSTORDSUB%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_custordsub_TBL T_svc_custordsub_TBL;
   
   TYPE T_svc_custordsubdtl_TBL is TABLE OF SVC_CUSTORDSUBDTL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_custordsubdtl_TBL T_svc_custordsubdtl_TBL;
   --
BEGIN
   
   --Populate the data from the Input business object into the local collections which will be used further to
   --do bulk insert into the staging tables.
   if I_business_object.CustOrdSubDesc_TBL is NOT NULL and I_business_object.CustOrdSubDesc_TBL.COUNT > 0 then
      FOR i in I_business_object.CustOrdSubDesc_TBL.FIRST..I_business_object.CustOrdSubDesc_TBL.LAST LOOP
         L_custordsub_id := svc_custordsub_id_seq.NEXTVAL;
         
         L_svc_custordsub_TBL(i).process_id            := I_process_id;
         L_svc_custordsub_TBL(i).chunk_id              := I_chunk_id;
         L_svc_custordsub_TBL(i).process_status        := LP_process_status_new;
         L_svc_custordsub_TBL(i).action_type           := I_action_type;
         L_svc_custordsub_TBL(i).custordsub_id         := L_custordsub_id;
         L_svc_custordsub_TBL(i).customer_order_no     := I_business_object.CustOrdSubDesc_TBL(i).customer_order_no;
         L_svc_custordsub_TBL(i).fulfill_order_no      := I_business_object.CustOrdSubDesc_TBL(i).fulfill_order_no;
         L_svc_custordsub_TBL(i).item                  := I_business_object.CustOrdSubDesc_TBL(i).item;
         L_svc_custordsub_TBL(i).item_qty              := I_business_object.CustOrdSubDesc_TBL(i).item_qty;
         L_svc_custordsub_TBL(i).qty_uom               := I_business_object.CustOrdSubDesc_TBL(i).qty_uom;
         L_svc_custordsub_TBL(i).loc_id                := I_business_object.CustOrdSubDesc_TBL(i).loc_id;
         L_svc_custordsub_TBL(i).loc_type              := I_business_object.CustOrdSubDesc_TBL(i).loc_type;
         L_svc_custordsub_TBL(i).error_msg             := NULL;
         L_svc_custordsub_TBL(i).create_datetime       := SYSDATE;
         L_svc_custordsub_TBL(i).create_id             := LP_user;
         L_svc_custordsub_TBL(i).last_update_datetime  := SYSDATE;
         L_svc_custordsub_TBL(i).last_update_id        := LP_user;
         
         if I_business_object.CustOrdSubDesc_TBL(i).SubItemDetails_TBL is NOT NULL and I_business_object.CustOrdSubDesc_TBL(i).SubItemDetails_TBL.COUNT > 0 then
            FOR j in I_business_object.CustOrdSubDesc_TBL(i).SubItemDetails_TBL.FIRST..I_business_object.CustOrdSubDesc_TBL(i).SubItemDetails_TBL.LAST LOOP
               L_dtl_index := L_dtl_index + 1;
               
               L_svc_custordsubdtl_TBL(L_dtl_index).process_id            := I_process_id;
               L_svc_custordsubdtl_TBL(L_dtl_index).chunk_id              := I_chunk_id;
               L_svc_custordsubdtl_TBL(L_dtl_index).custordsub_id         := L_custordsub_id;
               L_svc_custordsubdtl_TBL(L_dtl_index).sub_item              := I_business_object.CustOrdSubDesc_TBL(i).SubItemDetails_TBL(j).sub_item;
               L_svc_custordsubdtl_TBL(L_dtl_index).sub_item_qty          := I_business_object.CustOrdSubDesc_TBL(i).SubItemDetails_TBL(j).sub_item_qty;
               L_svc_custordsubdtl_TBL(L_dtl_index).sub_qty_uom           := I_business_object.CustOrdSubDesc_TBL(i).SubItemDetails_TBL(j).sub_qty_uom;
               L_svc_custordsubdtl_TBL(L_dtl_index).error_msg             := NULL;
               L_svc_custordsubdtl_TBL(L_dtl_index).create_datetime       := SYSDATE;
               L_svc_custordsubdtl_TBL(L_dtl_index).create_id             := LP_user;
               L_svc_custordsubdtl_TBL(L_dtl_index).last_update_datetime  := SYSDATE;
               L_svc_custordsubdtl_TBL(L_dtl_index).last_update_id        := LP_user;
            END LOOP;
         end if;
      END LOOP;
   end if;
   --Bulk insert the Input Business object from the local collections into the staging tables.
   if L_svc_custordsub_TBL is NOT NULL and L_svc_custordsub_TBL.COUNT > 0 then
      FORALL i in L_svc_custordsub_TBL.FIRST..L_svc_custordsub_TBL.LAST   
         insert into svc_custordsub (process_id,
                                     chunk_id,
                                     process_status,
                                     action_type,
                                     custordsub_id,
                                     customer_order_no,
                                     fulfill_order_no,
                                     item,
                                     item_qty,
                                     qty_uom,
                                     loc_id,
                                     loc_type,
                                     error_msg,
                                     create_datetime,
                                     create_id,
                                     last_update_datetime,
                                     last_update_id)
                              values (L_svc_custordsub_TBL(i).process_id,
                                      L_svc_custordsub_TBL(i).chunk_id,
                                      L_svc_custordsub_TBL(i).process_status,
                                      L_svc_custordsub_TBL(i).action_type,
                                      L_svc_custordsub_TBL(i).custordsub_id,
                                      L_svc_custordsub_TBL(i).customer_order_no,
                                      L_svc_custordsub_TBL(i).fulfill_order_no,
                                      L_svc_custordsub_TBL(i).item,
                                      L_svc_custordsub_TBL(i).item_qty,
                                      L_svc_custordsub_TBL(i).qty_uom,
                                      L_svc_custordsub_TBL(i).loc_id,
                                      L_svc_custordsub_TBL(i).loc_type,
                                      L_svc_custordsub_TBL(i).error_msg,
                                      L_svc_custordsub_TBL(i).create_datetime,
                                      L_svc_custordsub_TBL(i).create_id,
                                      L_svc_custordsub_TBL(i).last_update_datetime,
                                      L_svc_custordsub_TBL(i).last_update_id);
   end if;
   
   if L_svc_custordsubdtl_TBL is NOT NULL and L_svc_custordsubdtl_TBL.COUNT > 0 then
      FORALL i in L_svc_custordsubdtl_TBL.FIRST..L_svc_custordsubdtl_TBL.LAST   
         insert into svc_custordsubdtl (process_id,
                                        chunk_id,
                                        custordsub_id,
                                        sub_item,
                                        sub_item_qty,
                                        sub_qty_uom,
                                        error_msg,
                                        create_datetime,
                                        create_id,
                                        last_update_datetime,
                                        last_update_id)
                                 values (L_svc_custordsubdtl_TBL(i).process_id,
                                         L_svc_custordsubdtl_TBL(i).chunk_id,
                                         L_svc_custordsubdtl_TBL(i).custordsub_id,
                                         L_svc_custordsubdtl_TBL(i).sub_item,
                                         L_svc_custordsubdtl_TBL(i).sub_item_qty,
                                         L_svc_custordsubdtl_TBL(i).sub_qty_uom,
                                         L_svc_custordsubdtl_TBL(i).error_msg,
                                         L_svc_custordsubdtl_TBL(i).create_datetime,
                                         L_svc_custordsubdtl_TBL(i).create_id,
                                         L_svc_custordsubdtl_TBL(i).last_update_datetime,
                                         L_svc_custordsubdtl_TBL(i).last_update_id);
   end if;
   return TRUE;
            
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_TABLES;
-------------------------------------------------------------------------------------------------------
FUNCTION CLEANUP_TABLES(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id    IN      SVC_CUSTORDSUB.PROCESS_ID%TYPE,
                        I_chunk_id      IN      SVC_CUSTORDSUB.CHUNK_ID%TYPE,
                        I_action_type   IN      SVC_CUSTORDSUB.ACTION_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(100) := 'SVCPROV_CUSTORDSUB.CLEANUP_TABLES';

BEGIN

   delete from svc_custordsubdtl scd
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and exists (select 'x'
                    from svc_custordsub sc
                   where sc.process_id = I_process_id
                     and sc.chunk_id = I_chunk_id
                     and sc.custordsub_id = scd.custordsub_id
                     and sc.process_status = LP_process_status_comp);

   delete from svc_custordsub
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status = LP_process_status_comp;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CLEANUP_TABLES;
-------------------------------------------------------------------------------------------------------
END SVCPROV_CUSTORDSUB;
/