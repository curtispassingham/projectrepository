CREATE OR REPLACE PACKAGE SVCPROV_INVAVAIL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- Procedure Name : GET_INV_DETAIL
-- Purpose:         The procedure calls the CORESVC_INVAVAIL.GET_INV_DETAIL to 
--                  process the inventory lookup message. In case of error from 
--                  the above call, the procedure calls PARSE_ERR_MSG to build 
--                  FailStatus_TBL object in the output RIB_ServiceOpStatus_REC 
--                  based on ERROR_TBL returned.
-- Called by:       Inventory Available web service
--------------------------------------------------------------------------------
PROCEDURE GET_INV_DETAIL(O_ServiceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                         O_business_object        OUT    "RIB_InvAvailColDesc_REC",
                         I_business_object        IN     "RIB_InvAvailCriVo_REC");
END SVCPROV_INVAVAIL;
/
