CREATE OR REPLACE PACKAGE STKCOUNT_SQL AUTHID CURRENT_USER AS

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.  
------------------------------------------------------------------------------
PROCEDURE STAKE_PRODUCT_INSERT (I_cycle_count    IN     NUMBER,
                                I_dept           IN     NUMBER,
                                O_return_code    IN OUT VARCHAR2,
                                O_error_message  IN OUT VARCHAR2);

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ITEM_MASTER
-- which only returns data that the user has permission to access.  
------------------------------------------------------------------------------
PROCEDURE STAKE_SKULIST_INSERT (I_cycle_count    IN     STAKE_LOCATION.CYCLE_COUNT%TYPE,
                                I_skulist        IN     SKULIST_DETAIL.SKULIST%TYPE,
                                I_stocktake_date IN     DATE,
                                O_dup_unit_ind   IN OUT VARCHAR2,
                                O_no_item_ind    IN OUT VARCHAR2,
                                O_non_inv_ind    IN OUT VARCHAR2,
                                O_return_code    IN OUT VARCHAR2,
                                O_error_message  IN OUT VARCHAR2);

-------------------------------------------------------------------------------
PROCEDURE DESTROY (I_cycle_count    IN     NUMBER,
                   O_return_code    IN OUT VARCHAR2,
                   O_error_message  IN OUT VARCHAR2 );
-------------------------------------------------------------------------------
PROCEDURE CREATE_ADJUSTMENTS (I_cycle_count    IN     NUMBER,
                              I_loc_type       IN     VARCHAR2,
                              I_location       IN     NUMBER,
                              O_return_code    IN OUT VARCHAR2,
                              O_error_message  IN OUT VARCHAR2 );
-------------------------------------------------------------------------------
PROCEDURE STAKE_PROD_LOC_UPDATE (I_cycle_count    IN     NUMBER,
                                 I_dept           IN     NUMBER,
                                 I_class          IN     NUMBER,
                                 I_subclass       IN     NUMBER,
                                 I_loc_type       IN     VARCHAR2,
                                 I_location       IN     NUMBER,
                                 I_adjust_cost    IN     NUMBER,
                                 I_adjust_retail  IN     NUMBER,
                                 I_at_cost        IN     NUMBER,
                                 I_at_retail      IN     NUMBER,
                                 O_return_code    IN OUT VARCHAR2,
                                 O_error_message  IN OUT VARCHAR2 );
-------------------------------------------------------------------------------------
-- Name: VALIDATE_CYCLE_COUNT
-- Called by : stkrqst.fmb
-- Purpose : To validate that the cycle count entered is a valid cycle count number.
-- Created on : 22-Jul-96
-- Created by : Amy Selby
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_CYCLE_COUNT(I_cycle_count      IN     NUMBER,
                              I_stocktake_type   IN     VARCHAR2,
                              O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: CYCLE_COUNT_PROCESSED
-- Called by : stkrqst.fmb
-- Purpose : This function will check if the cycle count has been processed and
--           return an indicator.
-- Created on : 22-Jul-96
-- Created by : Amy Selby
-------------------------------------------------------------------------------------
FUNCTION CYCLE_COUNT_PROCESSED(I_cycle_count      IN     NUMBER,
                               O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name      : PRE_DELETE_STKCNT
-- Called by : stkrqst.fmb
-- Purpose   : Deletes locations from a stock count for all  
--             locations, a location list or a Store class. 
-------------------------------------------------------------------------------------
FUNCTION PRE_DELETES_STKCNT(I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE,
                            I_group_type      IN       VARCHAR2,
                            I_group           IN       VARCHAR2,
                            O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: PRE_DELETE_STKCNT_LOC
-- Called by  : stkrqst.fmb, PRE_DELETE_STKCNT
-- Purpose    : This function will delete from the stake_prod_loc, stake_sku_loc and
--              stake_qty before attempting to delete the record from stake_location.
-------------------------------------------------------------------------------------
FUNCTION PRE_DELETES_STKCNT_LOC(I_cycle_count      IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                                I_loc_type         IN     STAKE_QTY.LOC_TYPE%TYPE,
                                I_location         IN     STAKE_QTY.LOCATION%TYPE,
                                O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: CHK_STAKE_PROD_RECS
-- Called by : stkrqst.fmb
-- Purpose : This function checks for stake_product records
-- Created on : 22-Jul-96
-- Created by : Amy Selby
-------------------------------------------------------------------------------------
FUNCTION CHK_STAKE_PROD_RECS(I_cycle_count    IN        NUMBER,
                             O_rec_exists     IN OUT    VARCHAR2,
                             O_error_message  IN OUT    VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: CHK_STAKE_LOC_RECS
-- Called by : stkrqst.fmb
-- Purpose : This function checks for stake_location records
-- Created on : 22-Jul-96
-- Created by : Amy Selby
-------------------------------------------------------------------------------------
FUNCTION CHK_STAKE_LOC_RECS(I_cycle_count    IN     NUMBER,
                            O_rec_exists     IN OUT VARCHAR2,
                            O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: INSERT_STAKE_LOC
-- Called by : stkrqst.fmb
-- Purpose : This function inserts stake_location records based on the type that is
--           passed to it.  Valid types are: Store, Store Class, All Stores, Warehouse,
--           and All Warehouses.
-- Created on : 22-Jul-96
-- Created by : Amy Selby

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_PHYSICAL_WH V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------------
FUNCTION INSERT_STAKE_LOC(I_cycle_count   IN     NUMBER,
                          I_type          IN     VARCHAR2,
                          I_location      IN     VARCHAR2,
                          O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: MIN_DATE
-- Called by : stkrqst.fmb
-- Purpose : This function determines the earliest date on which a stock
--           count can occur.
-- Created on : 22-Jul-96
-- Created by : Amy Selby
-------------------------------------------------------------------------------------
FUNCTION MIN_DATE RETURN DATE;
-------------------------------------------------------------------------------------
-- Name: LOCKOUT_DATE
-- Called by : stkrqst.fmb
-- Purpose : This function fetches the number of days before the stock count occurs
--           in which the stock count request will be "locked out", un-editable.
-- Created on : 22-Jul-96
-- Created by : Amy Selby
-------------------------------------------------------------------------------------
FUNCTION LOCKOUT_DAYS RETURN NUMBER;
-------------------------------------------------------------------------------------
-- Name: Validate_Dept
-- Called by : stkdeptd.fmb
-- Purpose : This function validates a department if it exists and its processed_id
--           is not 'P'
-- Input Values: I_dept, I_cycle_count, I_loc
-- Return Values :Returns true if the validation is successful and false otherwise.
-- Created on : 01-Aug-96
-- Created by : Matt Augustson
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_DEPT(I_dept             IN     STAKE_PROD_LOC.DEPT%TYPE,
                       I_cycle_count      IN     STAKE_PROD_LOC.CYCLE_COUNT%TYPE,
                       I_loc              IN     STAKE_PROD_LOC.LOCATION%TYPE,
                       O_dept_desc        IN OUT DEPS.DEPT_NAME%TYPE,
                       O_profit_calc_type IN OUT DEPS.PROFIT_CALC_TYPE%TYPE,
                       O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: Validate_Class
-- Called by : stkdeptd.fmb
-- Purpose : This function validates a class if it exists within the current stocktake/department
--           and its processed_id is not 'P'
-- Input Values: I_class, I_dept, I_cycle_count, I_prod
-- Return Values :Returns true if the validation is successful and false otherwise.
-- Created on : 01-Aug-96
-- Created by : Matt Augustson
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_CLASS(I_class         IN     STAKE_PROD_LOC.CLASS%TYPE,
                        I_dept          IN     STAKE_PROD_LOC.DEPT%TYPE,
                        I_cycle_count   IN     STAKE_PROD_LOC.CYCLE_COUNT%TYPE,
                        I_prod          IN     STAKE_HEAD.PRODUCT_LEVEL_IND%TYPE,
                        O_class_name    IN OUT CLASS.CLASS_NAME%TYPE,
                        O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: Validate_Subclass
-- Called by : stkdeptd.fmb
-- Purpose : This function validates a subclass if it exists within the current stocktake/department/class
--           and its processed_id is not 'P'
-- Input Values: I_dept
-- Return Values :Returns true if the validation is successful and false otherwise.
-- Created on : 01-Aug-96
-- Created by : Matt Augustson
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_SUBCLASS(I_subclass            IN     STAKE_PROD_LOC.SUBCLASS%TYPE,
                           I_class               IN     STAKE_PROD_LOC.CLASS%TYPE,
                           I_dept                IN     STAKE_PROD_LOC.DEPT%TYPE,
                           I_cycle_count         IN     STAKE_PROD_LOC.CYCLE_COUNT%TYPE,
                           I_prod                IN     STAKE_HEAD.PRODUCT_LEVEL_IND%TYPE,
                           O_subclass_name       IN OUT CLASS.CLASS_NAME%TYPE,
                           O_error_message       IN OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: Retrieve_Values
-- Called by : stkdeptd.fmb
-- Purpose : This function returns at_cost, at_retail, adj_cost, adj_retail and profit_calc_type
--           current stocktake/location/department/class/subclass.
-- Input Values: I_subclass, I_store, I_wh, I_cycle_count
-- Return Values :Returns true if the validation is successful and false otherwise.
-- Created on : 01-Aug-96
-- Created by : Matt Augustson
-------------------------------------------------------------------------------------
FUNCTION RETRIEVE_VALUES(I_subclass        IN     STAKE_PROD_LOC.SUBCLASS%TYPE,
                         I_class           IN     STAKE_PROD_LOC.CLASS%TYPE,
                         I_dept            IN     STAKE_PROD_LOC.DEPT%TYPE,
                         I_loc_type        IN     STAKE_PROD_LOC.LOC_TYPE%TYPE,
                         I_location        IN     STAKE_PROD_LOC.LOCATION%TYPE,
                         I_cycle_count     IN     STAKE_PROD_LOC.CYCLE_COUNT%TYPE,
                         O_cost            IN OUT STAKE_PROD_LOC.TOTAL_COST%TYPE,
                         O_retail          IN OUT STAKE_PROD_LOC.TOTAL_RETAIL%TYPE,
                         O_adj_cost        IN OUT STAKE_PROD_LOC.ADJUST_COST%TYPE,
                         O_adj_retail      IN OUT STAKE_PROD_LOC.ADJUST_RETAIL%TYPE,
                         O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name: INSERT_HEAD
-- Name:          Insert_Head
-- Called by:     (RSS)STOCK COUNT module
-- Purpose:       Inser a Unit type stock count on table stake_head, date defult to vdate
-- Input Values:  I_stock_count, I_stock_count_desc, I_loc_type
-- Return Values: TRUE if success, FALSE otherwise
-- Created on:    02-Mar-99
-- Created by:    Jin Zhu
-------------------------------------------------------------------------------------
FUNCTION INSERT_HEAD(O_error_message    IN OUT VARCHAR2,
                     I_stock_count      IN     STAKE_HEAD.CYCLE_COUNT%TYPE,
                     I_stock_count_desc IN     STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                     I_loc_type         IN     STAKE_HEAD.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:          CALC_TOT_QTY
-- Called by:
-- Purpose:       This function will return the total quantity of stock count
--                for a specific item/ physical warehouse
-- Input Values:  I_cycle_count, I_pwh, I_item
-- Output Values: O_tot_qty
-- Return Values: TRUE if success, FALSE otherwise
-- Created on:    22-Mar-2001
-- Created by:    Shaam A. Khan
-------------------------------------------------------------------------------------
FUNCTION CALC_TOT_QTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_tot_qty       IN OUT STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE,
                      I_cycle_count   IN     STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                      I_location      IN     STAKE_SKU_LOC.LOCATION%TYPE,
                      I_item          IN     STAKE_SKU_LOC.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:          DELETE_STAKE_QTY
-- Called by:
-- Purpose:       This function will lock and then delete records from stake_qty table
-- Input Values:  I_cycle_count, I_wh, I_item
-- Return Values: TRUE if success, FALSE otherwise
-- Created on:    28-Mar-2001
-- Created by:    Shaam A. Khan
-------------------------------------------------------------------------------------
FUNCTION DELETE_STAKE_QTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cycle_count   IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                          I_location      IN     STAKE_QTY.LOCATION%TYPE,
                          I_item          IN     STAKE_QTY.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:          PWH_EXISTS_FOR_CYCLE
-- Purpose:       This function will verify if the passed in physical warehouse is
--                associated to the passed in cycle count
-- Created on:    07-June-2001
-- Created by:    Jodi Schaubschlager
-------------------------------------------------------------------------------------
FUNCTION PWH_EXISTS_FOR_CYCLE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_pwh_name      IN OUT WH.WH_NAME%TYPE,
                              I_cycle_count   IN     STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                              I_pwh           IN     WH.WH%TYPE,
                              I_item          IN     STAKE_QTY.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:          ITEM_ASSOC_CYCLE
-- Purpose:       This function will verify if the passed in item is associated to
--                the passed in cycle count and pwh.
-- Created on:    08-June-2001
-- Created by:    Jodi Schaubschlager
-------------------------------------------------------------------------------------
FUNCTION ITEM_ASSOC_CYCLE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item_desc     IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                          I_cycle_count   IN     STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                          I_item          IN     STAKE_QTY.ITEM%TYPE,
                          I_pwh           IN     WH.WH%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:          CHECK_RECLASS_ITEM
-- Purpose:       This function checks the re-classification of passed in item
-- Called in:     STKRQST.FMB
-- Created on:    08-Aug-2001
-- Created by:    Shaam A. Khan
-------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_RECLASSIFICATION(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists         IN OUT  BOOLEAN,
                                     O_item           IN OUT  ITEM_MASTER.ITEM%TYPE,
                                     I_stocktake_type IN      VARCHAR2,
                                     I_item_list      IN      SKULIST_DETAIL.SKULIST%TYPE,
                                     I_stocktake_date IN      PERIOD.VDATE%TYPE,
                                     I_dept           IN      ITEM_MASTER.DEPT%TYPE,
                                     I_class          IN      ITEM_MASTER.CLASS%TYPE,
                                     I_subclass       IN      ITEM_MASTER.SUBCLASS%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Name:          STOCK_DISTRIBUTION
-- Purpose:       This function uses the distribution module to insert/update
--                distributed stocks in the STAKE_SKU_LOC table
-- Input Values:  I_cycle_count, I_pwh, I_item, I_count_loc_qty, I_count_loc_desc
-- Called in:     STKVWEDT.FMB
-- Created on:    06-Sep-2001
-- Created by:    Shaam A. Khan
-------------------------------------------------------------------------------------
FUNCTION STOCK_DISTRIBUTION(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cycle_count      IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                            I_physical_wh      IN       WH.WH%TYPE,
                            I_item             IN       STAKE_QTY.ITEM%TYPE,
                            I_count_loc_qty    IN       STAKE_QTY.QTY%TYPE,
                            I_count_loc_desc   IN       STAKE_QTY.LOCATION_DESC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:          UPDATE_STAKE_SKU_LOC_FOR_PACK
-- Purpose:       This function updates STAKE_SKU_LOC table with distributed quantities
--                for pack items
-- Input Values:  I_cycle_count, I_wh, I_store, I_item, I_count_qty
-- Called in:     STKVWEDT.FMB
-- Created on:    06-Sep-2001
-- Created by:    Shaam A. Khan
-------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_SKU_LOC_FOR_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                       I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                       I_pack_item       IN       STAKE_SKU_LOC.ITEM%TYPE,
                                       I_count_qty       IN       STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Name:         NEW_STKCNT_ITEM_LOC
-- Purpose:      This function will add a record to the stake_sku_loc table when
--               a new stock count (count quantity) is created for a location that
--               is not part of an item/location relationship.
-- Created On:   20-Feb-2002
-------------------------------------------------------------------------------------
FUNCTION NEW_STKCNT_ITEM_LOC(O_error_message  IN OUT VARCHAR2,
                             I_cycle_count    IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_location       IN     ITEM_LOC.LOC%TYPE,
                             I_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_item           IN     ITEM_MASTER.ITEM%TYPE,
                             I_phys_count_qty IN     STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Purpose: Verifies stake_qty records exist.
-- Created On: 03-21-2002
-------------------------------------------------------------------------------------
FUNCTION STAKE_QTY_EXIST(O_error_message IN OUT VARCHAR2,
                         O_processed     IN OUT STAKE_SKU_LOC.PROCESSED%TYPE,
                         I_cycle_count   IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                         I_location      IN     ITEM_LOC.LOC%TYPE,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Purpose: Verifies stake_sku_loc records exist.
-- Created On: 03-21-2002
-------------------------------------------------------------------------------------
FUNCTION STAKE_SKU_LOC_EXIST(O_error_message      IN OUT VARCHAR2,
                             I_cycle_count        IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_location           IN     ITEM_LOC.LOC%TYPE,
                             I_item               IN     ITEM_MASTER.ITEM%TYPE,
                             I_phys_count_qty     IN     STAKE_SKU_LOC.PHYSICAL_COUNT_QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Purpose: Locks the stake_location table based on cycle count and either a specific
--          location, all of a type, or any locations on the stake_cont table.
-------------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_LOCATION(O_error_message      IN OUT VARCHAR2,
                             I_cycle_count        IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_location           IN     ITEM_LOC.LOC%TYPE,
                             I_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_user_id            IN     STAKE_CONT.USER_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Purpose: To check whether the entered item is in the stake_sku_location table
-- or not and to return the stock take type for the given cycle count.
-------------------------------------------------------------------------------------
FUNCTION STAKE_SKU_LOC_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   VARCHAR2,
                             I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                             I_location        IN       ITEM_LOC.LOC%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Purpose: This function will lock the specified record on STAKE_SCHEDULE.  If the
--          record is already locked, O_record_locked will be FALSE.
-------------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_SCHEDULE (O_error_message      IN OUT VARCHAR2,
                              O_record_locked      IN OUT BOOLEAN,
                              I_location           IN     ITEM_LOC.LOC%TYPE,
                              I_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_stocktake_type     IN     STAKE_SCHEDULE.STOCKTAKE_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Purpose: Returns the SUM(STAKE_QTY.QTY) for a cycle_count/location/item
---------------------------------------------------------------------------------------
FUNCTION GET_STAKE_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_loc_qty         IN OUT   STAKE_QTY.QTY%TYPE,  
                        I_item            IN       STAKE_QTY.ITEM%TYPE,
                        I_location        IN       STAKE_QTY.LOCATION%TYPE,
                        I_loc_type        IN       STAKE_QTY.LOC_TYPE%TYPE,
                        I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Purpose: Redistributes STAKE_QTY.QTY for each count location after the user has 
--          modified the totals for each virtual warehouse using the Stock Count 
--          Virtual Warehouse Distribution window.  This is necessary because we need
--          to retain the original SNAPSHOT_ON_HAND_QTY while distributing STAKE_QTY
--          using the percentages entered by the user.
---------------------------------------------------------------------------------------
FUNCTION REDISTRIBUTE_PWH (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_total_qty       IN       STAKE_QTY.QTY%TYPE,
                           I_physical_wh     IN       STAKE_QTY.LOCATION%TYPE,
                           I_item            IN       STAKE_QTY.ITEM%TYPE,
                           I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Purpose: This routine swaps the SNAPSHOT_ON_HAND_QTY with the DISTRIBUTE_QTY.  This 
--          is necessary so that the same routine can be called (STOCK_DISTRIBUTE) for
--          adjusted Stock counts as well as non-adjusted.
---------------------------------------------------------------------------------------
FUNCTION SWAP_SNAPSHOT_DISTRIBUTE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item            IN       STAKE_QTY.ITEM%TYPE,
                                   I_physical_wh     IN       STAKE_QTY.LOCATION%TYPE,
                                   I_cycle_count     IN       STAKE_QTY.CYCLE_COUNT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: ROLLUP_SELLABLE_ONLY_ITEM 
-- Purpose      : This new function will roll-up the count quantity of sellable-only 
--                items to its associated orderable-only transformed item(s).
-------------------------------------------------------------------------------
FUNCTION ROLLUP_SELLABLE_ONLY_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                    I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                    I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                    I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                    I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: ROLLUP_SELLABLE_ONLY_ITEM
-- Purpose      : This new function will roll-up the count quantity of sellable-only
--                items to its associated orderable-only transformed item(s).
--                Uses a temp table of items.
-- Used by      : stkupld batch
-------------------------------------------------------------------------------
FUNCTION ROLLUP_SELLABLE_ONLY_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                    I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                    I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: STAKE_QTY_LOC_EXIST
-- Purpose: This new function will check whether the record is in stake_sku_location table or not.
---------------------------------------------------------------------------------------
FUNCTION STAKE_QTY_LOC_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist         IN OUT BOOLEAN,
                             I_cycle_count   IN     STAKE_QTY.CYCLE_COUNT%TYPE,
                             I_location      IN     ITEM_LOC.LOC%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: CHECK_LOCATION_EXISTS
-- Purpose: This new function will check whether a stock request for this location already exists.
---------------------------------------------------------------------------------------
FUNCTION CHECK_LOCATION_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist          IN OUT BOOLEAN,
                               I_cycle_count    IN     STAKE_HEAD.CYCLE_COUNT%TYPE,
                               I_stocktake_date IN     STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                               I_location_type  IN     CODE_DETAIL.CODE%TYPE,                         
                               I_location       IN     VARCHAR2)
   return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: GET_STAKE_OLD_QTY 
-- Purpose      : This new function will get the old count quantity in the stake_qty 
--                table. Called by the stkvwedt.fmb form
-------------------------------------------------------------------------------
FUNCTION GET_STAKE_OLD_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_old_qty         IN OUT   STAKE_QTY.QTY%TYPE,
                            I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE,
                            I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                            I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                            I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: ROLLUP_SELLABLE_ONLY_ITEM 
-- Purpose      : This new function will roll-up the count quantity of sellable-only 
--                items to its associated orderable-only transformed item(s).
--                Called by the stkvwedt.fmb form.
-------------------------------------------------------------------------------
FUNCTION ROLLUP_SELLABLE_ONLY_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_old_qty         IN OUT   STAKE_QTY.QTY%TYPE,
                                    I_cycle_count     IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                    I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                    I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                    I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                    I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_XFORM_ITEM_TYPE 
-- Purpose      : This new function will get the value of xform_item_type in the 
--                stake_sku_loc table. 
--                Called by the stkvwedt.fmb form.
-------------------------------------------------------------------------------
FUNCTION GET_XFORM_ITEM_TYPE (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_xform_item_type   IN OUT   STAKE_SKU_LOC.XFORM_ITEM_TYPE%TYPE,
                              I_cycle_count       IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                              I_item              IN       ITEM_MASTER.ITEM%TYPE,
                              I_location          IN       STAKE_SKU_LOC.LOCATION%TYPE,
                              I_loc_type          IN       STAKE_SKU_LOC.LOC_TYPE%TYPE)
                              
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: DELETE_STAKE_QTY_ITEM 
-- Purpose      : This new function will delete all the items that is related 
--                to the sellable item in the stake_qty table and update the 
--                stake_sku_loc and stake_qty stock counts. 
--                Called by the form stkvwedt.fmb.
-------------------------------------------------------------------------------
FUNCTION DELETE_STAKE_QTY_ITEM (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_xform_item_type    IN       STAKE_SKU_LOC.XFORM_ITEM_TYPE%TYPE,
                                I_loc_type           IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                I_cycle_count        IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                I_item               IN       ITEM_MASTER.ITEM%TYPE,
                                I_location           IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                I_loc_desc           IN       STAKE_QTY.LOCATION_DESC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------


-- Function Name: CHECK_STK_COUNT_EXISTS
-- Purpose: This new function will check whether a stock request for this location already exists
--          and returns the cycle count id in which the location exists.
---------------------------------------------------------------------------------------
FUNCTION CHECK_STK_COUNT_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exist          IN OUT BOOLEAN,
                                O_cycle_count    IN OUT STAKE_HEAD.CYCLE_COUNT%TYPE,
                                I_cycle_count    IN     STAKE_HEAD.CYCLE_COUNT%TYPE,
                                I_stocktake_date IN     STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                                I_location_type  IN     CODE_DETAIL.CODE%TYPE,                         
                                I_location       IN     VARCHAR2)
   return BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name: INSERT_UPDATE_STAKE_QTY
-- Purpose: This function will be called from ADF Stock Count Result screen to insert or update the 
--          STAKE_QTY table for the specified cycle count id, location type,location,location description
--          and an Item.
----------------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_STAKE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                                 I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                 I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                 I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_count_qty       IN       STAKE_QTY.QTY%TYPE,
                                 I_old_count_qty   IN       STAKE_QTY.QTY%TYPE,
                                 I_total_qty       IN       STAKE_QTY.QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name: UPDATE_DELETE_STAKE_QTY
-- Purpose: This function will be called from ADF Stock Count Result screen to update and delete the 
--          stock count for the specified cycle count id, location type,location,location description
--          and an Item.
------------------------------------------------------------------------------------------------------
FUNCTION DELETE_UPDATE_STAKE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                                 I_loc_type        IN       STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                 I_location        IN       STAKE_SKU_LOC.LOCATION%TYPE,
                                 I_loc_desc        IN       STAKE_QTY.LOCATION_DESC%TYPE,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_count_qty       IN       STAKE_QTY.QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name: INSERT_STAKE_CONT
-- Purpose: This function will be called from ADF Stock Count Variance screen to insert item-locations records 
--          into stake_cont table for the specified cycle count id, location type,location when adjustments 
--          are created.
----------------------------------------------------------------------------------------------------
FUNCTION INSERT_STAKE_CONT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_location        IN       ITEM_LOC.LOC%TYPE,
                           I_cycle_count     IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                           I_run_type        IN       STAKE_CONT.RUN_TYPE%TYPE,
                           I_user_id         IN       STAKE_CONT.USER_ID%TYPE)
RETURN BOOLEAN; 
----------------------------------------------------------------------------------------------------
END STKCOUNT_SQL;
/
