CREATE OR REPLACE PACKAGE BODY SVCPROV_INVBACKORD AS
-------------------------------------------------------------------------------------------------------
LP_create_type        VARCHAR2(15) := 'create'; 
LP_backorder_new      SVC_INVBACKORD.PROCESS_STATUS%TYPE := 'N';
LP_backorder_complete SVC_INVBACKORD.PROCESS_STATUS%TYPE := 'C';--To cleanup completed backorders

-------------------------------------------------------------------------------------------------------
---                          PRIVATE FUNCTION                                                       ---
-------------------------------------------------------------------------------------------------------
-- Function Name: POP_TABLES
-- Purpose: This public function will stage backorder requests in the input business
--          object to the interface staging tables. 
--------------------------------------------------------------------------------
FUNCTION POP_TABLES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id     IN      SVC_INVBACKORD.PROCESS_ID%TYPE,
                    I_businessObject IN      "RIB_InvBackOrdDesc_TBL",
                    I_chunk_id       IN      SVC_INVBACKORD.CHUNK_ID%TYPE,
                    I_action_type    IN      SVC_INVBACKORD.ACTION_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
---                          PRIVATE FUNCTION                                                       ---
-------------------------------------------------------------------------------------------------------
-- Function Name: CLEANUP_TABLES
-- Purpose: This public function will delete staged backorder requests in the 
--          interface staging tables. 
--------------------------------------------------------------------------------
FUNCTION CLEANUP_TABLES(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id    IN      SVC_INVBACKORD.PROCESS_ID%TYPE,
                        I_chunk_id      IN      SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
PROCEDURE CREATE_BACKORDER(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                           I_businessObject         IN     "RIB_InvBackOrdColDesc_REC")
IS

   L_program         VARCHAR2(64) := 'SVCPROV_INVBACKORD.CREATE_BACKORDER';
   L_chunk_id        SVC_INVBACKORD.CHUNK_ID%TYPE   := 1;   -- For webservice, the entire message will be processed in single chunk. 
   L_process_id      SVC_INVBACKORD.PROCESS_ID%TYPE;
   L_table_name      VARCHAR2(50) := 'SVC_INVBACKORD';
   L_status_code     VARCHAR2(1) := API_CODES.SUCCESS;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;

   cursor C_GET_NEXT_PROCESS_ID is
      select svc_invbackord_proc_seq.NEXTVAL
        from dual;

BEGIN

   if I_businessObject.InvBackOrdDesc_TBL is NULL or I_businessObject.InvBackOrdDesc_TBL.count = 0 then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQUIRED_INPUT_IS_NULL',
                                                  'I_businessObject',
                                                  L_program,
                                                  NULL);
      raise PROGRAM_ERROR;
   end if;

   if I_businessObject.InvBackOrdDesc_tbl.COUNT <> I_businessObject.collection_size then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_BACKORD_COL_COUNT', 
                                                  NULL,
                                                  NULL,
                                                  NULL);
      raise PROGRAM_ERROR;
   end if;

   -- Get an unique process id for this backorder service request
   open C_GET_NEXT_PROCESS_ID;
   fetch C_GET_NEXT_PROCESS_ID into L_process_id;
   close C_GET_NEXT_PROCESS_ID;

   -- Stage backorder requests to interface staging tables
   if POP_TABLES(L_error_message,
                 L_process_id,
                 I_businessObject.InvBackOrdDesc_TBL,
                 L_chunk_id,
                 LP_create_type) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Calling the core layer which processes the input message. The core layer will return true for successful processing. 
   -- In case of unhandled error, L_error_message will be populated with the unhandled error. 
   -- In case of validation error, the called function will return false and will have L_error_message as null. The validation 
   -- errors will be updated in the staging table.
   if CORESVC_INVBACKORD.CREATE_BACKORDER(L_error_message,
                                          L_process_id,
                                          L_chunk_id) = FALSE then
      if L_error_message is NULL then
         -- Update the output business object failstatus_TBL with the validation errors.
         if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(L_error_message,
                                                 O_serviceOperationStatus.FailStatus_TBL,
                                                 L_table_name,
                                                 L_process_id) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;

      raise PROGRAM_ERROR;
   end if;

   -- Delete staged backorder requests  with status as 'Completed' from interface staging table
   if CLEANUP_TABLES(L_error_message,
                     L_process_id,
                     L_chunk_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then

      if C_GET_NEXT_PROCESS_ID%ISOPEN then
         close C_GET_NEXT_PROCESS_ID;
      end if;

      L_status_code := API_CODES.UNHANDLED_ERROR;

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);

   when OTHERS then

      if C_GET_NEXT_PROCESS_ID%ISOPEN then
         close C_GET_NEXT_PROCESS_ID;
      end if;

      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END CREATE_BACKORDER;
-------------------------------------------------------------------------------------------------------
FUNCTION POP_TABLES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id     IN      SVC_INVBACKORD.PROCESS_ID%TYPE,
                    I_businessObject IN      "RIB_InvBackOrdDesc_TBL",
                    I_chunk_id       IN      SVC_INVBACKORD.CHUNK_ID%TYPE,
                    I_action_type    IN      SVC_INVBACKORD.ACTION_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'SVCPROV_INVBACKORD.POP_TABLES';

BEGIN

   -- Bulk insert BackOrder request into interface staging table
   forall i in 1..I_businessObject.COUNT
      insert into svc_invbackord (process_id,
                                  chunk_id,
                                  backord_id,
                                  process_status,
                                  action_type,
                                  item,
                                  location,
                                  loc_type,
                                  channel_id,
                                  backorder_qty,
                                  backorder_uom,
                                  create_datetime,
                                  create_id,
                                  last_update_datetime,
                                  last_update_id)
                           values (I_process_id,
                                   I_chunk_id,
                                   svc_invbackord_id_seq.nextval,
                                   LP_backorder_new,
                                   I_action_type,
                                   I_businessObject(i).item,
                                   I_businessObject(i).location,
                                   I_businessObject(i).loc_type,
                                   I_businessObject(i).channel_id,
                                   I_businessObject(i).backorder_qty,
                                   I_businessObject(i).unit_of_measure,
                                   SYSDATE,
                                   USER,
                                   SYSDATE,
                                   USER);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_TABLES;
-------------------------------------------------------------------------------------------------------
FUNCTION CLEANUP_TABLES(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id    IN      SVC_INVBACKORD.PROCESS_ID%TYPE,
                        I_chunk_id      IN      SVC_INVBACKORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'SVC_INVBACKORD.CLEANUP_TABLES';

BEGIN  

   delete from svc_invbackord
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status = LP_backorder_complete;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CLEANUP_TABLES;
-------------------------------------------------------------------------------------------------------
END SVCPROV_INVBACKORD;
/