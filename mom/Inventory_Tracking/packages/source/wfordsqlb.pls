create or replace PACKAGE BODY WF_ORDER_SQL AS

LP_vdate		    period.vdate%TYPE := get_vdate;

--------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_RATE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
			    O_currency_code   IN OUT   CURRENCIES.CURRENCY_CODE%TYPE,
			    O_exchange_rate   IN OUT   CURRENCY_RATES.EXCHANGE_RATE%TYPE,
			    I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'WF_ORDER_SQL.GET_CURRENCY_RATE';

   cursor C_CURRENCY_RATE is
      select currency_code,
	     exchange_rate
	from wf_order_head
       where wf_order_no = I_wf_order_no;

BEGIN

   open C_CURRENCY_RATE;
   fetch C_CURRENCY_RATE into O_currency_code,
			      O_exchange_rate;

   if C_CURRENCY_RATE%NOTFOUND then
      close C_CURRENCY_RATE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_WF_ORDER_NO',I_wf_order_no,L_program,NULL);
      return FALSE;
   end if;

   close C_CURRENCY_RATE;

   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      if C_CURRENCY_RATE%ISOPEN then
	 close C_CURRENCY_RATE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    NULL);
      RETURN FALSE;
END GET_CURRENCY_RATE;
-------------------------------------------------------------------------
FUNCTION GET_CREDIT (O_error_message   IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
		     O_credit_ind      IN OUT	WF_CUSTOMER.CREDIT_IND%TYPE,
		     I_customer_id     IN	WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
RETURN BOOLEAN IS
   L_program	  VARCHAR2(50) := 'WF_ORDER_SQL.GET_CREDIT';

BEGIN
   if I_customer_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_customer_id',L_program,NULL);
      return FALSE;
   end if;

   if WF_CUSTOMER_SQL.CHECK_WF_CUST_CREDIT (O_error_message,
					    O_credit_ind,
					    I_customer_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END GET_CREDIT;
------------------------------------------------------------------------
FUNCTION GET_LEAD_TIME (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
			O_lead_time	    IN OUT   REPL_ITEM_LOC.WH_LEAD_TIME%TYPE,
			I_item		    IN	     WF_ORDER_DETAIL.ITEM%TYPE,
			I_cust_loc	    IN	     WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
			I_source_loc_id     IN	     WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
			I_source_loc_type   IN	     WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program	    VARCHAR2(50)		      := 'WF_ORDER_SQL.GET_LEAD_TIME';
   L_lead_time	    REPL_ITEM_LOC.WH_LEAD_TIME%TYPE   := 0 ;

   cursor C_LEAD_TIME_WH IS
      select NVL(ril.wh_lead_time,0) +
	     NVL(ril.pickup_lead_time,0)+
	     NVL(isc.lead_time,0)+
	     NVL(il.inbound_handling_days,0)
	from repl_item_loc ril,
	     item_supp_country isc,
	     (select inbound_handling_days,
		     item
		from item_loc
	       where item = I_item
		 and loc  = I_source_loc_id) il
       where ril.item		       = il.item
	 and ril.item		       = isc.item
	 and ril.primary_repl_supplier = isc.supplier
	 and ril.origin_country_id     = isc.origin_country_id
	 and ril.item		       = I_item
	 and ril.location	       = I_cust_loc
	 and ril.repl_method	       = 'SO'	  -- Store Orders
	 and stock_cat		       = 'L';	  -- WH/Cross linked

   cursor C_LEAD_TIME_SU IS
      select NVL(isc.lead_time, 0)
	from item_supp_country isc
       where isc.item = I_item
	 and isc.supplier = I_source_loc_id
	 and isc.primary_country_ind = 'Y';

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;

   if I_cust_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_cust_loc',L_program,NULL);
      return FALSE;
   end if;

   if I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_id',L_program,NULL);
      return FALSE;
   end if;

   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_type',L_program,NULL);
      return FALSE;
   end if;

   if I_source_loc_type ='WH' then
      open C_LEAD_TIME_WH;
      fetch C_LEAD_TIME_WH into L_lead_time;
      close C_LEAD_TIME_WH;
   elsif I_source_loc_type ='SU' then
      open C_LEAD_TIME_SU;
      fetch C_LEAD_TIME_SU into L_lead_time;
      close C_LEAD_TIME_SU;
   else /*Store*/
      L_lead_time := 0;
   end if;

   O_lead_time := NVL(L_lead_time,0);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END GET_LEAD_TIME;
------------------------------------------------------------------------
FUNCTION VERIFY_REPL_INFO (O_error_message    IN OUT  VARCHAR2,
			   I_store	      IN      STORE_ORDERS.STORE%TYPE,
			   I_item	      IN      STORE_ORDERS.ITEM%TYPE,
			   I_not_after_date   IN      WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE,
			   I_source_wh	      IN      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program		    VARCHAR2(64)			       := 'WF_ORDER_SQL.VERIFY_REPL_INFO';
   L_reject_store_ord_ind   REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE    := NULL;
   L_next_delivery_date     REPL_ITEM_LOC.NEXT_DELIVERY_DATE%TYPE      := NULL;
   L_source_wh		    REPL_ITEM_LOC.SOURCE_WH%TYPE	       := NULL;
   L_system_options_row     SYSTEM_OPTIONS%ROWTYPE;

   cursor C_CHECK_REPL_EXISTS is
      select reject_store_ord_ind,
	     next_delivery_date,
	     source_wh
	from repl_item_loc
       where item	 = I_item
	 and location	 = I_store
	 and repl_method = 'SO'     -- Store Orders
	 and stock_cat	 = 'L';     -- WH/Cross linked

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
					    L_system_options_row) = FALSE then
      return FALSE;
   end if;

   open C_CHECK_REPL_EXISTS;
   fetch C_CHECK_REPL_EXISTS into L_reject_store_ord_ind,
				  L_next_delivery_date,
				  L_source_wh;

   if C_CHECK_REPL_EXISTS%NOTFOUND then
      close C_CHECK_REPL_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('WF_REPL_SO_ITEM_LOC',
					     I_item,
					     I_source_wh || ' / ' || I_store,
					     TO_CHAR(LP_vdate + 1 + NVL(L_system_options_row.wf_order_lead_days,0),'DD-Mon-YYYY'));
      return FALSE;
   end if;

   close C_CHECK_REPL_EXISTS;

   -- If the franchise order source warehouse is not same as replenishment warehouse, error out
   if I_source_wh <> L_source_wh then
      O_error_message := SQL_LIB.CREATE_MSG('WF_DIFF_WF_REPL_WH',I_item || ' / ' || I_store, I_source_wh, L_source_wh);
      return FALSE;
   end if;

   -- If the reject_store_ord_ind is 'Y', and the not after date is before the next delivery date,
   -- the not after date is invalid.
   if (L_reject_store_ord_ind = 'Y') and (I_not_after_date < L_next_delivery_date) then
      O_error_message := SQL_LIB.CREATE_MSG('SO_REJECT_DATE',I_store,I_item, NULL);
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    NULL);
      return FALSE;
END VERIFY_REPL_INFO;
------------------------------------------------------------------------------
FUNCTION GET_FREIGHT_OTHER_CHRG(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
				O_freight_chrg	 IN OUT  WF_ORDER_HEAD.FREIGHT%TYPE,
				O_other_chrg	 IN OUT  WF_ORDER_HEAD.OTHER_CHARGES%TYPE,
				I_order_no	 IN	 WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program	  VARCHAR2(50) := 'WF_ORDER_SQL.GET_FREIGHT_OTHER_CHRG';

   cursor C_FREIGHT_OTHER_CHRG IS
      select NVL(freight,0),
	     NVL(other_charges,0)
	from wf_order_head
       where wf_order_no = I_order_no;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   open C_FREIGHT_OTHER_CHRG;
   fetch C_FREIGHT_OTHER_CHRG into O_freight_chrg, O_other_chrg;
   close C_FREIGHT_OTHER_CHRG;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END GET_FREIGHT_OTHER_CHRG;
------------------------------------------------------------------------
FUNCTION POP_FREIGHT_OTHER_CHRG(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
				I_freight_chrg	 IN	 WF_ORDER_HEAD.FREIGHT%TYPE,
				I_other_chrg	 IN	 WF_ORDER_HEAD.OTHER_CHARGES%TYPE,
				I_order_no	 IN	 WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program	  VARCHAR2(50) := 'WF_ORDER_SQL.POP_FREIGHT_OTHER_CHRG';
   L_table	  VARCHAR2(50) := 'WF_ORDER_HEAD';
   L_key	  VARCHAR2(64);

   RECORD_LOCKED  EXCEPTION;
   PRAGMA	  EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_WF_ORD_HEAD IS
      select 'x'
	from wf_order_head
       where wf_order_no = I_order_no
	 for update nowait;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   L_key := 'wf_order_no: '||to_char(I_order_no);
   open C_LOCK_WF_ORD_HEAD;
   close C_LOCK_WF_ORD_HEAD;

   update wf_order_head
      set freight	= NVL(I_freight_chrg,0),
	  other_charges = NVL(I_other_chrg,0)
    where wf_order_no = I_order_no;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      if C_LOCK_WF_ORD_HEAD%ISOPEN then
	 close C_LOCK_WF_ORD_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
					     L_table,
					     L_key,
					     NULL);
      return FALSE;
   WHEN OTHERS THEN
      if C_LOCK_WF_ORD_HEAD%ISOPEN then
	 close C_LOCK_WF_ORD_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   return FALSE;
END POP_FREIGHT_OTHER_CHRG;
------------------------------------------------------------------------
FUNCTION NEXT_WF_ORDER_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
			  O_wf_order_no    IN OUT  WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program	  VARCHAR2(50) := 'WF_ORDER_SQL.NEXT_WF_ORDER_NO';
   L_exists	  VARCHAR2(1)  := 'N';
   L_wf_order_no  WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_wrap_seq_no  WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   SEQ_MAXVAL	  EXCEPTION;
   PRAGMA	  EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select wf_rma_no_seq.NEXTVAL
	from dual;

   cursor C_SEQ_EXISTS is
      select 'Y'
	from wf_order_head
       where wf_order_no = L_wf_order_no;

BEGIN
   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_wf_order_no;
   close C_NEXTVAL;

   -- Save the first sequence value fetched.
   L_wrap_seq_no := L_wf_order_no;

   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      --
      if L_exists = 'N'  then
	 O_wf_order_no := L_wf_order_no;
	 return TRUE;
      end if;

      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_wf_order_no;
      close C_NEXTVAL;

      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_wf_order_no = L_wrap_seq_no then
	 O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
					       L_wf_order_no,
					       NULL,
					       NULL);
	 return FALSE;
      end if;

   END LOOP;

EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
	 close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
	 close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_EXIST',
					    L_wf_order_no,
					    NULL,
					    NULL);
      return FALSE;
   when OTHERS then
      if C_NEXTVAL%ISOPEN then
	 close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
	 close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    to_char(SQLCODE));
      return FALSE;
END NEXT_WF_ORDER_NO;
------------------------------------------------------------------------
FUNCTION VALIDATE_CUSTOMER_REF_NO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
				  O_exists	    IN OUT   BOOLEAN,
				  I_ord_ref_no	    IN	     WF_ORDER_HEAD.CUST_ORD_REF_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'WF_ORDER_SQL.VALIDATE_CUSTOMER_REF_NO';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_CUST_REF_NO IS
      select 'Y'
	from wf_order_head
       where cust_ord_ref_no = I_ord_ref_no;

BEGIN
   O_exists := FALSE;

   if I_ord_ref_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_ord_ref_no',L_program,NULL);
      return FALSE;
   end if;

   open C_CUST_REF_NO;
   fetch C_CUST_REF_NO into L_exists;
   close C_CUST_REF_NO;

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END VALIDATE_CUSTOMER_REF_NO;
------------------------------------------------------------------------
FUNCTION WF_ORDER_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
			  O_exists	     IN OUT   BOOLEAN,
			  O_exist_order_no   IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
			  I_order_no	     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
			  I_cust_loc	     IN       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
			  I_source_loc_type  IN       WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
			  I_source_loc_id    IN       WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
			  I_need_date	     IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
			  I_item	     IN       WF_ORDER_DETAIL.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'WF_ORDER_SQL.WF_ORDER_EXISTS';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_ORDER_EXISTS IS
      select 'Y',
	     woh.wf_order_no
	from wf_order_detail wod,
	     wf_order_head woh,
	     wf_order_head woh1
       where wod.wf_order_no	 = woh.wf_order_no
	 and wod.wf_order_no	!= I_order_no
	 and woh1.wf_order_no	 = I_order_no
	 and woh1.order_type	 in ('M','E')		     -- Check only for new Manual and Edi orders
	 and wod.customer_loc	 = I_cust_loc
	 and wod.source_loc_id	 = I_source_loc_id
	 and wod.source_loc_type = I_source_loc_type
	 and wod.need_date	 = I_need_date
	 and wod.item		 = I_item
	 and woh.status 	 = 'A'
	 and woh.order_type	 in ('M','E')		      -- Duplicate check is only for existing Manual and EDI orders.
	 and rownum = 1;
BEGIN
   O_exists := FALSE;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;
   if I_cust_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_cust_loc',L_program,NULL);
      return FALSE;
   end if;
   if I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_id',L_program,NULL);
      return FALSE;
   end if;
   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_type',L_program,NULL);
      return FALSE;
   end if;
   if I_need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_need_date',L_program,NULL);
      return FALSE;
   end if;

   open C_ORDER_EXISTS;
   fetch C_ORDER_EXISTS into L_exists,
			     O_exist_order_no;
   close C_ORDER_EXISTS;

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END WF_ORDER_EXISTS;
------------------------------------------------------------------------
FUNCTION WF_ORDER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
			 O_exists	   IN OUT   BOOLEAN,
			 I_cust_loc	   IN	    WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
			 I_source_loc_type IN	    WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
			 I_source_loc_id   IN	    WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
			 I_need_date	   IN	    WF_ORDER_DETAIL.NEED_DATE%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'WF_ORDER_SQL.WF_ORDER_EXISTS';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_ORDER_EXISTS IS
      select 'Y'
	from wf_order_detail
       where customer_loc    = I_cust_loc
	 and source_loc_id   = I_source_loc_id
	 and source_loc_type = I_source_loc_type
	 and need_date	     = I_need_date
	 and rownum = 1;
BEGIN
   O_exists := FALSE;

   if I_cust_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_cust_loc',L_program,NULL);
      return FALSE;
   end if;
   if I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_id',L_program,NULL);
      return FALSE;
   end if;
   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_type',L_program,NULL);
      return FALSE;
   end if;
   if I_need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_need_date',L_program,NULL);
      return FALSE;
   end if;

   open C_ORDER_EXISTS;
   fetch C_ORDER_EXISTS into L_exists;
   close C_ORDER_EXISTS;

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END WF_ORDER_EXISTS;
-------------------------------------------------------------------------
FUNCTION WF_ORDER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
			 O_exists	   IN OUT   BOOLEAN,
			 I_order_no	   IN	    WF_ORDER_DETAIL.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'WF_ORDER_SQL.WF_ORDER_EXISTS';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_ORDER_EXISTS IS
      select 'Y'
	from wf_order_head
       where wf_order_no = I_order_no
	 and rownum = 1;
BEGIN
   O_exists := FALSE;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   open C_ORDER_EXISTS;
   fetch C_ORDER_EXISTS into L_exists;
   close C_ORDER_EXISTS;

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END WF_ORDER_EXISTS;
------------------------------------------------------------------------
FUNCTION APPROVE_WF_ORDERS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
			    I_order_no	      IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program		  VARCHAR2(50)				 := 'WF_ORDER_SQL.APPROVE_WF_ORDERS';
   L_wf_order_line_no	  WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE;
   L_item		  ITEM_MASTER.ITEM%TYPE;
   L_source_loc_id	  WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_source_loc_type	  WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_cust_loc		  WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_need_date		  WF_ORDER_DETAIL.NEED_DATE%TYPE;
   L_ord_exists 	  VARCHAR2(1)				 := 'N';
   L_exists		  BOOLEAN;
   L_so_exists		  BOOLEAN;
   L_so_processed	  BOOLEAN;
   L_credit_ind 	  WF_CUSTOMER.CREDIT_IND%TYPE;
   L_available_qty	  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_tsf_type		  TSFHEAD.TSF_TYPE%TYPE 		 := 'FO';
   L_lead_time		  REPL_ITEM_LOC.WH_LEAD_TIME%TYPE;
   L_requested_qty	  WF_ORDER_DETAIL.REQUESTED_QTY%TYPE;
   L_not_after_date	  WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE;
   L_fixed_cost 	  WF_ORDER_DETAIL.FIXED_COST%TYPE;
   L_customer_cost	  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE;
   L_exist_order_no	  WF_ORDER_HEAD.WF_ORDER_NO%TYPE := NULL;
   L_message_text	  RTK_ERRORS.RTK_TEXT%TYPE;
   L_item_master_tbl	  ITEM_MASTER%ROWTYPE;
   L_transfer_rec	  OBJ_F_TRANSFER_REC			 := OBJ_F_TRANSFER_REC();
   L_transfer_tbl	  OBJ_F_TRANSFER_TBL			 := OBJ_F_TRANSFER_TBL();
   L_store_order_rec	  OBJ_F_STORE_ORDER_REC 		 := OBJ_F_STORE_ORDER_REC();
   L_store_order_tbl	  OBJ_F_STORE_ORDER_TBL 		 := OBJ_F_STORE_ORDER_TBL();
   L_linked_tsf_no	  TSFHEAD.TSF_NO%TYPE;
   L_linked_po		  BOOLEAN				 := FALSE;
   L_linked_alloc	  VARCHAR2(1)				 := 'N';
   L_alloc_approve_ind	  VARCHAR2(1);
   L_sup_source_ind	  VARCHAR2(1);
   L_order_type 	  WF_ORDER_HEAD.ORDER_TYPE%TYPE;
   L_table		  VARCHAR2(50);
   L_key		  VARCHAR2(64);
   L_prev_item		  ITEM_MASTER.ITEM%TYPE;
   L_prev_source_id	  WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_prev_source_type	  WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_prev_tot_tsf_qty	  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

   RECORD_LOCKED	  EXCEPTION;
   PRAGMA		  EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_CHECK_WF_ORD_DETAIL IS
      select 'Y'
	from wf_order_detail
       where wf_order_no = I_order_no
	 and rownum = 1;

   cursor C_GET_ORD_HEAD IS
      select order_type
	from wf_order_head
       where wf_order_no = I_order_no;

   cursor C_GET_ORD_DETAILS IS
      select wod.wf_order_line_no,
	     wod.item,
	     wod.source_loc_id,
	     wod.source_loc_type,
	     wod.customer_loc,
	     wod.requested_qty,
	     wod.need_date,
	     wod.not_after_date,
	     wod.fixed_cost,
	     wod.customer_cost
	from wf_order_detail wod,
	     item_master im,
	     repl_item_loc ril			-- ril is joined only for sorting. Distribute WH available inv to non replenishment item/store first
       where wf_order_no	= I_order_no
	 and im.item		= wod.item
	 and wod.item		= ril.item(+)
	 and wod.customer_loc	= ril.location(+)
	 and ril.repl_method(+) = 'SO'
	 and ril.stock_cat(+)	= 'L'
	 and NVL(im.deposit_item_type,'x') != 'A'      --Ignore deposit container for validation and for TSF/Store Order creation.
    order by wod.item, wod.source_loc_type, wod.source_loc_id, NVL2(ril.item,2,1) asc;

   cursor C_GET_CREDIT_IND IS
      select wc.credit_ind
	from store s,
	     wf_customer wc
       where s.store_type = 'F'
	 and s.wf_customer_id = wc.wf_customer_id
	 and s.store =(select customer_loc
			 from wf_order_detail
			where wf_order_no = I_order_no
			  and rownum = 1);

   cursor C_SUP_SOURCE_LOC IS
      select 'Y'
	from wf_order_detail
       where wf_order_no = I_order_no
	 and source_loc_type = 'SU'
	 and rownum = 1;

   cursor C_GET_LINKED_TSF IS
      select tsf_no
	from tsfhead
       where wf_order_no = I_order_no;

   cursor C_LINKED_PO_ORD_DETAILS IS
      select item,
	     source_loc_id,
	     source_loc_type,
	     customer_loc,
	     wf_order_line_no
	from wf_order_detail
       where wf_order_no = I_order_no;

   cursor C_CHECK_IL_STATUS IS
      select wod.item,
	     il.loc
	from wf_order_head woh,
	     wf_order_detail wod,
	     item_loc il,
	     item_master im
       where woh.wf_order_no = I_order_no
	 and woh.order_type in ('M', 'E')
	 and woh.wf_order_no = wod.wf_order_no
	 and wod.source_loc_type in ('ST','WH')
	 and il.item = wod.item
	 and im.item = il.item
	 and NVL(im.deposit_item_type,'0') != 'A'
	 and ((     il.loc = wod.customer_loc
		and il.loc_type = 'S'
		and il.status in ('I', 'D'))
	      or  ( il.loc = wod.source_loc_id
		and il.loc_type = decode(wod.source_loc_type,'WH','W','S')
		and il.status = 'D'))
	 and rownum = 1
       union all
      select wod.item,
	     il.loc
	from wf_order_head woh,
	     wf_order_detail wod,
	     item_loc il,
	     item_master im
       where woh.wf_order_no = I_order_no
	 and woh.order_type in ('M', 'E')
	 and woh.wf_order_no = wod.wf_order_no
	 and wod.source_loc_type = 'SU'
	 and il.item = wod.item
	 and im.item = il.item
	 and NVL(im.deposit_item_type,'0') != 'A'
	 and il.loc = wod.customer_loc
	 and il.loc_type = 'S'
	 and il.status != 'A'
	 and rownum = 1;

   TYPE ils_wod_TBL is TABLE of C_LINKED_PO_ORD_DETAILS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_ils_wod_tbl      ils_wod_TBL;
   L_inv_item	      WF_ORDER_DETAIL.ITEM%TYPE;
   L_inv_loc	      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   open C_GET_ORD_HEAD;
   fetch C_GET_ORD_HEAD into L_order_type;
   close C_GET_ORD_HEAD;

   if L_order_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_WF_ORDER_NO',NULL,NULL,NULL);
      return FALSE;
   end if;

   open C_CHECK_WF_ORD_DETAIL;
   fetch C_CHECK_WF_ORD_DETAIL into L_ord_exists;
   close C_CHECK_WF_ORD_DETAIL;

   if L_ord_exists ='N' then
      O_error_message := SQL_LIB.CREATE_MSG('WF_DETAIL_NOT_EXISTS',NULL,NULL,NULL);
      return FALSE;
   end if;

   open C_GET_CREDIT_IND;
   fetch C_GET_CREDIT_IND into L_credit_ind;
   close C_GET_CREDIT_IND;

   if L_credit_ind ='N' then
      O_error_message := SQL_LIB.CREATE_MSG('BAD_CUSTOMER_CREDIT',NULL,NULL,NULL);
      return FALSE;
   end if;

   -- Check the deposit item content and container has the same costing location for supplier sourced rows.
   if WF_ORDER_SQL.CHECK_DEP_ITEM_COSTING_LOC(O_error_message,
					      I_order_no) = FALSE then
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
					    L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if L_order_type in ('M','E') then		      -- Do detail level processing for Manual and EDI orders only.
      --Check the item loc status is not in 'Delete' status at the source store or warehouse
      --Item loc status at customer location should be Active for supplier sourced or Active/Discontinued for warehouse/store sourced.
      open C_CHECK_IL_STATUS;
      fetch C_CHECK_IL_STATUS into L_inv_item, L_inv_loc;
      if C_CHECK_IL_STATUS%FOUND then
	 close C_CHECK_IL_STATUS;
	 O_error_message := SQL_LIB.CREATE_MSG('WF_ORD_INV_ITEMLOC',
					       L_inv_item,
					       L_inv_loc,
					       NULL);
	 return FALSE;
      end if;
      close C_CHECK_IL_STATUS;
      ---
      FOR wf_det_rec in C_GET_ORD_DETAILS LOOP
	 L_wf_order_line_no := wf_det_rec.wf_order_line_no;
	 L_item 	    := wf_det_rec.item;
	 L_source_loc_id    := wf_det_rec.source_loc_id;
	 L_source_loc_type  := wf_det_rec.source_loc_type;
	 L_cust_loc	    := wf_det_rec.customer_loc;
	 L_need_date	    := wf_det_rec.need_date;
	 L_requested_qty    := wf_det_rec.requested_qty;
	 L_not_after_date   := wf_det_rec.not_after_date;
	 L_fixed_cost	    := wf_det_rec.fixed_cost;
	 L_customer_cost    := wf_det_rec.customer_cost;

	 if WF_ORDER_SQL.WF_ORDER_EXISTS(O_error_message,
					 L_exists,
					 L_exist_order_no,
					 I_order_no,
					 L_cust_loc,
					 L_source_loc_type,
					 L_source_loc_id,
					 L_need_date,
					 L_item)= FALSE then
	    return FALSE;
	 end if;

	 if L_exists = TRUE then
	    L_message_text  := L_cust_loc||', '||L_source_loc_id||', '||L_need_date||', '||L_item;
	    O_error_message := SQL_LIB.CREATE_MSG('WF_DUP_ORDER_EXISTS',L_exist_order_no,L_message_text,NULL);
	    return FALSE;
	 end if;

	 if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
					    L_item_master_tbl,
					    L_item) = FALSE then
	    return FALSE;
	 end if;

	 -- One of customer cost or fixed cost should be available.
	 if L_fixed_cost is NULL and L_customer_cost is NULL then
	    O_error_message := SQL_LIB.CREATE_MSG('WF_FIXED_CUST_COST',L_item,L_source_loc_id,L_cust_loc);
	    return FALSE;
	 end if;

	 if L_need_date < LP_vdate then
	    O_error_message := SQL_LIB.CREATE_MSG('WF_NEED_DATE',NULL,NULL,NULL);
	    return FALSE;
	 end if;

	 if WF_ORDER_SQL.GET_LEAD_TIME (O_error_message,
					L_lead_time,
					L_item,
					L_cust_loc,
					L_source_loc_id,
					L_source_loc_type) =FALSE then
	    return FALSE;
	 end if;

	 if L_source_loc_type = 'SU' then
	    -- The not after date should be greater than vdate + lead time.
	    if L_not_after_date - L_lead_time < LP_vdate then
	       O_error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
	       return FALSE;
	    end if;
	 elsif L_source_loc_type = 'ST' then
	    if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(O_error_message,
							  L_available_qty,
							  L_item,
							  L_source_loc_id,
							  'S')= FALSE then
	       return FALSE;
	    end if;

	    -- Adjust available quantity if the same item/source store is used for more franchise store.
	    if L_item = NVL(L_prev_item, '-999')
	       and L_source_loc_id = NVL(L_prev_source_id,-999)
	       and L_source_loc_type = NVL(L_prev_source_type,'xx') then
	       ---
	       L_available_qty := GREATEST(L_available_qty - L_prev_tot_tsf_qty,0);
	    else
	       L_prev_tot_tsf_qty := 0;
	    end if;

	    if L_available_qty < L_requested_qty then
	       O_error_message := SQL_LIB.CREATE_MSG('WF_INSUFF_INV',L_item,L_source_loc_id,L_requested_qty);
	       return FALSE;
	    end if;

	    if L_not_after_date - L_lead_time < LP_vdate then
	       O_error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
	       return FALSE;
	    end if;

	    if L_need_date > LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0)	then
	       O_error_message := SQL_LIB.CREATE_MSG('WF_ST_LEAD_DAYS',L_item, TO_CHAR(L_need_date,'DD-Mon-YYYY'), TO_CHAR(LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0),'DD-Mon-YYYY'));
	       return FALSE;
	    end if;

	    L_transfer_rec := OBJ_F_TRANSFER_REC(L_item,
						 L_source_loc_type,
						 L_source_loc_id,
						 'S',			   -- Customer Loc type
						 L_cust_loc,
						 L_tsf_type,
						 L_need_date,
						 I_order_no,
						 L_requested_qty);
	    L_transfer_tbl.EXTEND;
	    L_transfer_tbl(L_transfer_tbl.count) := L_transfer_rec;

	    L_prev_tot_tsf_qty := L_prev_tot_tsf_qty + L_requested_qty;

	 else -- WH
	    if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(O_error_message,
							  L_available_qty,
							  L_item,
							  L_source_loc_id,
							  'W')= FALSE then
	       return FALSE;
	    end if;

	    -- Adjust available quantity if the same item/source store is used for more franchise store.
	    if L_item = NVL(L_prev_item, '-999')
	       and L_source_loc_id = NVL(L_prev_source_id,-999)
	       and L_source_loc_type = NVL(L_prev_source_type,'xx') then
	       L_available_qty := GREATEST(L_available_qty - L_prev_tot_tsf_qty,0);
	    else
	       L_prev_tot_tsf_qty := 0;
	    end if;

	    --
	    if L_need_date <= LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0) then
	       if L_available_qty >= L_requested_qty then
		  if L_requested_qty > 0 then
		     L_transfer_rec := OBJ_F_TRANSFER_REC(L_item,
							  L_source_loc_type,
							  L_source_loc_id,
							  'S',
							  L_cust_loc,
							  L_tsf_type,
							  L_need_date,
							  I_order_no,
							  L_requested_qty);
		     L_transfer_tbl.EXTEND;
		     L_transfer_tbl(L_transfer_tbl.count) := L_transfer_rec;
		     L_prev_tot_tsf_qty := L_prev_tot_tsf_qty + L_requested_qty;
		  end if;
	       else
	       -- Available qty less than requested qty.
		  if WF_TRANSFER_SQL.STORE_ORDER_EXISTS(O_error_message,
							L_so_exists,
							L_so_processed,
							L_item,
							L_cust_loc,
							L_need_date)= FALSE then
		     return FALSE;
		  end if;
		  if L_so_exists =TRUE then
		     O_error_message := SQL_LIB.CREATE_MSG('STORE_ORDER_EXISTS', L_item || ', ' || L_cust_loc || ', ' || TO_CHAR(L_need_date,'DD-Mon-YYYY'),NULL,NULL);
		     return FALSE;
		  end if;
		  if L_item_master_tbl.pack_ind ='Y' then
		     O_error_message := SQL_LIB.CREATE_MSG('WF_PACK_ITEM',NULL,NULL,NULL);
		     return FALSE;
		  end if;

		  if L_available_qty >0 then
		     if WF_ORDER_SQL.VERIFY_REPL_INFO(O_error_message,
						      L_cust_loc,
						      L_item,
						      L_not_after_date,
						      L_source_loc_id)= FALSE then
			return FALSE;
		     end if;
		     if L_not_after_date - L_lead_time >= LP_vdate then
			L_transfer_rec := OBJ_F_TRANSFER_REC(L_item,
							     L_source_loc_type,
							     L_source_loc_id,
							     'S',		       -- Customer Loc type
							     L_cust_loc,
							     L_tsf_type,
							     L_need_date,
							     I_order_no,
							     L_available_qty);
			L_transfer_tbl.EXTEND;
			L_transfer_tbl(L_transfer_tbl.count) := L_transfer_rec;

			L_prev_tot_tsf_qty := L_prev_tot_tsf_qty + L_available_qty;

			L_store_order_rec := OBJ_F_STORE_ORDER_REC(I_order_no,
								   L_wf_order_line_no,
								   L_item,
								   L_cust_loc,
								   L_need_date,
								   L_requested_qty - L_available_qty);

			L_store_order_tbl.EXTEND;
			L_store_order_tbl(L_store_order_tbl.count) := L_store_order_rec;
		     else
			O_error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
			return FALSE;
		     end if;
		  elsif L_available_qty = 0 then
		     if L_item_master_tbl.pack_ind ='Y' then
			O_error_message := SQL_LIB.CREATE_MSG('WF_PACK_ITEM',NULL,NULL,NULL);
			return FALSE;
		     end if;
		     if WF_ORDER_SQL.VERIFY_REPL_INFO(O_error_message,
						      L_cust_loc,
						      L_item,
						      L_not_after_date,
						      L_source_loc_id)= FALSE then
			return FALSE;
		     end if;
		     if L_not_after_date - L_lead_time >= LP_vdate then
			L_store_order_rec := OBJ_F_STORE_ORDER_REC(I_order_no,
								   L_wf_order_line_no,
								   L_item,
								   L_cust_loc,
								   L_need_date,
								   L_requested_qty);
			L_store_order_tbl.EXTEND;
			L_store_order_tbl(L_store_order_tbl.count) := L_store_order_rec;
		     else
			O_error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
			return FALSE;
		     end if;
		  end if; -- End of L_available_qty >0
	       end if; -- End of check L_available_qty >= L_requested_qty
	    else
	       --L_need_date > LP_vdate + wf_order_lead_days
	       if WF_TRANSFER_SQL.STORE_ORDER_EXISTS(O_error_message,
						     L_so_exists,
						     L_so_processed,
						     L_item,
						     L_cust_loc,
						     L_need_date)= FALSE then
		  return FALSE;
	       end if;

	       if L_so_exists =TRUE then
		  O_error_message := SQL_LIB.CREATE_MSG('STORE_ORDER_EXISTS',L_item || ', ' ||	L_cust_loc || ', ' || TO_CHAR(L_need_date,'DD-Mon-YYYY'),NULL,NULL);
		  return FALSE;
	       end if;

	       if L_item_master_tbl.pack_ind ='Y' then
		  O_error_message := SQL_LIB.CREATE_MSG('WF_ND_PACK_ITEM',L_item,To_CHAR(L_need_date,'DD-Mon-YYYY'), TO_CHAR(LP_vdate + NVL(L_system_options_row.wf_order_lead_days,0),'DD-Mon-YYYY'));
		  return FALSE;
	       end if;

	       if L_requested_qty >0 then
		  if WF_ORDER_SQL.VERIFY_REPL_INFO(O_error_message,
						   L_cust_loc,
						   L_item,
						   L_not_after_date,
						   L_source_loc_id)= FALSE then
		     return FALSE;
		  end if;
		  if L_not_after_date - L_lead_time >= LP_vdate then

		     L_store_order_rec := OBJ_F_STORE_ORDER_REC(I_order_no,
								L_wf_order_line_no,
								L_item,
								L_cust_loc,
								L_need_date,
								L_requested_qty);
		     L_store_order_tbl.EXTEND;
		     L_store_order_tbl(L_store_order_tbl.count) := L_store_order_rec;
		  else
		     O_error_message := SQL_LIB.CREATE_MSG('WF_LEAD_FAILED',L_item,L_source_loc_id,L_lead_time);
		     return FALSE;
		  end if;
	       end if;
	    end if; -- L_need_date <= LP_vdate + L_system_options_row.wf_order_lead_days
	 end if; -- Source loc type = WH

	 L_prev_item	     := wf_det_rec.item;
	 L_prev_source_id    := wf_det_rec.source_loc_id;
	 L_prev_source_type  := wf_det_rec.source_loc_type;
      END LOOP;
   end if;     -- order type in ('M','E'). Order Type A or X will be directly approved.

   open C_SUP_SOURCE_LOC;
   fetch C_SUP_SOURCE_LOC into L_sup_source_ind;
   close C_SUP_SOURCE_LOC;
   -- If there are any line item sourced from supplier, create/update the purchase orders first before approving the FO.
   if L_sup_source_ind = 'Y' then
      if WF_ORDER_DETAIL_SQL.CHECK_LINKED_PO(O_error_message,
					     L_linked_po,
					     I_order_no,
					     NULL) = FALSE then
	 return FALSE;
      end if;

      if L_linked_po = TRUE then
	 if WF_PO_SQL.MODIFY_FRANCHISE_PO(O_error_message,
					  I_order_no,
					  'N',				-- Scaling
					  'N') = FALSE then		-- Undo Scaling
	    return FALSE;
	 end if;
      else
	 if WF_PO_SQL.CREATE_FRANCHISE_PO(O_error_message,
					  I_order_no,
					  'N',				-- Scaling
					  'N') = FALSE then		-- Undo Scaling
	    return FALSE;
	 end if;
      end if;
   end if;

   -- The approval for Allocation require approving other franchise order linked to
   -- the same allocation. Check if any of those franchise order will fail approval because of
   -- credit check. If yes error out. Else approve the linked allocation and all the franchise
   -- order for that allocation by calling WF_ALLOC_SQL.APPROVE_LINKED_ALLOC_F_ORDER
   if WF_ALLOC_SQL.CHECK_ALLOC_APPROVAL(O_error_message,
					L_alloc_approve_ind,
					I_order_no) = FALSE then
      return FALSE;
   end if;

   -- Approve the linked Purchase orders.
   if L_sup_source_ind = 'Y' then
      if WF_PO_SQL.MODIFY_FRANCHISE_PO(O_error_message,
				       I_order_no,
				       'N',
				       'N') = FALSE then
	 return FALSE;
      end if;
   end if;

   -- Create the linked Transfers for Manual and EDI.
   -- For EG or Auto approve the linked allocation or transfer.
   if L_order_type in ('X', 'A') and NVL(L_sup_source_ind,'N') = 'N' then
      open C_GET_LINKED_TSF;
      fetch C_GET_LINKED_TSF into L_linked_tsf_no;
      close C_GET_LINKED_TSF;
      if L_linked_tsf_no is NOT NULL then
	 if WF_TRANSFER_SQL.WF_UPDATE_TSF_HEAD(O_error_message,
					       L_linked_tsf_no,
					       'A') = FALSE then
	    return FALSE;
	 end if;
      else
	 if WF_ALLOC_SQL.APPROVE_LINKED_ALLOC_F_ORDER(O_error_message,
						      I_order_no) = FALSE then
	    return FALSE;
	 end if;
      end if;
   else  -- Order type M or E
      if (L_store_order_tbl is NOT NULL and L_store_order_tbl.count > 0) or (L_transfer_tbl is NOT NULL and L_transfer_tbl.count > 0 ) then
	 if WF_TRANSFER_SQL.BUILD_TSF_STORE_ORDER(O_error_message,
						  I_order_no,
						  'A',
						  L_transfer_tbl,
						  L_store_order_tbl) = FALSE then
	    return FALSE;
	 end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
					     L_table,
					     L_key,
					     NULL);
      return FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
      return FALSE;
END APPROVE_WF_ORDERS;
------------------------------------------------------------------------
FUNCTION GET_COST_INFORMATION(O_error_message	IN OUT	 RTK_ERRORS.RTK_TEXT%TYPE,
			      O_templ_id	IN OUT	 WF_COST_RELATIONSHIP.TEMPL_ID%TYPE,
			      O_templ_desc	IN OUT	 WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE,
			      O_margin_pct	IN OUT	 WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE,
			      O_calc_type	IN OUT	 WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE,
			      O_cust_cost	IN OUT	 FUTURE_COST.PRICING_COST%TYPE,
			      O_acqui_cost	IN OUT	 FUTURE_COST.ACQUISITION_COST%TYPE,
			      O_currency_code	IN OUT	 FUTURE_COST.CURRENCY_CODE%TYPE,
			      I_item		IN	 ITEM_MASTER.ITEM%TYPE,
			      I_wf_store	IN	 STORE.STORE%TYPE,
			      I_source_loc_type IN	 WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE DEFAULT NULL,
			      I_source_loc_id	IN	 WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE DEFAULT NULL,
			      I_recalc_order	IN	 VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS
   L_program		    VARCHAR2(50)			   := 'WF_ORDER_SQL.GET_COST_INFORMATION';
   L_item_master_tbl	    ITEM_MASTER%ROWTYPE;
   L_item		    ITEM_MASTER.ITEM%TYPE;
   L_acquisition_cost	    FUTURE_COST.ACQUISITION_COST%TYPE := NULL;
   L_pricing_cost	    FUTURE_COST.PRICING_COST%TYPE     := NULL;
   L_future_cost_exists     BOOLEAN			      := TRUE;

   cursor C_GET_COST_INFO is
      select inner1.templ_id templ_id,
	     inner1.templ_desc templ_desc,
	     inner1.first_applied first_applied,
	     inner1.margin_pct margin_pct
	from (select wcr.templ_id,
		     wct.templ_desc,
		     wct.first_applied,
		     wct.margin_pct,
		     case
			when (wcr.class  = '-1' and wcr.subclass  = '-1' and wcr.item = '-1') then 4
			when (wcr.class != '-1' and wcr.subclass  = '-1' and wcr.item = '-1') then 3
			when (wcr.class != '-1' and wcr.subclass != '-1' and wcr.item = '-1') then 2
			else 1
		     end rnk_item
		from wf_cost_buildup_tmpl_head wct,
		     wf_cost_relationship wcr,
		     item_master im
	       where im.item	    = I_item
		 and wcr.location   = I_wf_store
		 and DECODE(wcr.item,'-1',im.item,wcr.item) = im.item
		 and im.dept	    = wcr.dept
		 and im.class	    = DECODE(wcr.class, '-1',im.class, wcr.class)
		 and im.subclass    = DECODE(wcr.subclass, '-1', im.subclass, wcr.subclass)
		 and wct.templ_id   = wcr.templ_id
		 and wcr.start_date <= LP_vdate
		 and wcr.end_date   >= LP_vdate
	     ) inner1
	order by inner1.rnk_item; -- If there is a row with the item and also a null (-1) row, fetch the one with item

   cursor C_GET_ITEMS is
      select vpq.item,
	     vpq.qty
	from v_packsku_qty vpq
       where vpq.pack_no = I_item;

   cursor C_GET_FUTURE_COST is
      select acquisition_cost,
	     pricing_cost,
	     currency_code
	from (select fc.acquisition_cost  acquisition_cost,
		     fc.pricing_cost pricing_cost,
		     fc.currency_code,
		     rank () over (partition by fc.item, fc.location order by active_date desc) rnk
		from future_cost fc
	       where fc.item	    = L_item
		 and fc.location    = I_wf_store
		 and NVL(I_source_loc_type,'x') <> 'SU'
		 and primary_supp_country_ind = 'Y'
		 and fc.active_date <= LP_vdate
	      UNION ALL
	      select fc.acquisition_cost acquisition_cost,
		     fc.pricing_cost pricing_cost,
		     fc.currency_code,
		     rank () over (partition by fc.item, fc.location order by active_date desc) rnk
		from future_cost fc,
		     item_supp_country isc
	       where NVL(I_source_loc_type,'x') = 'SU'
		 and fc.item	    = L_item
		 and fc.location    = I_wf_store
		 and fc.item = isc.item
		 and fc.supplier = isc.supplier
		 and fc.origin_country_id = isc.origin_country_id
		 and isc.supplier = I_source_loc_id
		 and isc.primary_country_ind = 'Y'
		 and fc.active_date <= LP_vdate)
	where rnk = 1;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;

   if I_wf_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_store',L_program,NULL);
      return FALSE;
   end if;

   if I_source_loc_type is not NULL and I_source_loc_type not in ('SU','ST','WH') then
      O_error_message := SQL_LIB.CREATE_MSG('WF_INV_SOURCE_LOC_TYPE','I_wf_store',NULL,NULL);
      return FALSE;
   end if;

   if I_source_loc_type is not NULL and I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_id',L_program,NULL);
      return FALSE;
   end if;

   open C_GET_COST_INFO;
   fetch C_GET_COST_INFO into O_templ_id,
			      O_templ_desc,
			      O_calc_type,
			      O_margin_pct;
   close C_GET_COST_INFO;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
				      L_item_master_tbl,
				      I_item) = FALSE then
      return FALSE;
   end if;

   if L_item_master_tbl.pack_type is NOT NULL and L_item_master_tbl.pack_type = 'B' then
      O_acqui_cost := 0;
      O_cust_cost  := 0;

      FOR c_rec in C_GET_ITEMS LOOP
	 L_item 	      := c_rec.item;
	 L_acquisition_cost   := NULL;
	 L_pricing_cost       := NULL;
	 L_future_cost_exists := TRUE;

	 open C_GET_FUTURE_COST;
	 fetch C_GET_FUTURE_COST into L_acquisition_cost,
				      L_pricing_cost,
				      O_currency_code;
	 if C_GET_FUTURE_COST%NOTFOUND then
	    L_future_cost_exists := FALSE;
	 end if;
	 close C_GET_FUTURE_COST;

	 -- If there are no records in future cost, error out. Except for the call from WF_ORDER_SQL.REAPPLY_CUSTOMER_COST
	 -- Reapply customer cost is called post initial creation and for EDI, the ranging is done by the form after this call.
	 if not L_future_cost_exists and NVL(I_recalc_order,'N') = 'N' then
	    if I_source_loc_type = 'SU' then
	       O_error_message := SQL_LIB.CREATE_MSG('WF_NO_CUST_COST_SU',L_item,I_wf_store,I_source_loc_id);
	    else
	       O_error_message := SQL_LIB.CREATE_MSG('WF_NO_CUST_COST',L_item,I_wf_store,NULL);
	    end if;
	    return FALSE;
	 end if;

	 O_acqui_cost := O_acqui_cost + (L_acquisition_cost * c_rec.qty);
	 O_cust_cost  := O_cust_cost  + (L_pricing_cost     * c_rec.qty );
      END LOOP;
   else
      L_item := I_item;

      open C_GET_FUTURE_COST;
      fetch C_GET_FUTURE_COST into L_acquisition_cost,
				   L_pricing_cost,
				   O_currency_code;
      if C_GET_FUTURE_COST%NOTFOUND then
	 L_future_cost_exists := FALSE;
      end if;
      close C_GET_FUTURE_COST;

      -- If there are no records in future cost, error out. Except for the call from WF_ORDER_SQL.REAPPLY_CUSTOMER_COST
      -- Reapply customer cost is called post initial creation and for EDI, the ranging is done by the form after this call.
      if not L_future_cost_exists and NVL(I_recalc_order,'N') = 'N' then
	 if I_source_loc_type = 'SU' then
	    O_error_message := SQL_LIB.CREATE_MSG('WF_NO_CUST_COST_SU',L_item,I_wf_store,I_source_loc_id);
	 else
	    O_error_message := SQL_LIB.CREATE_MSG('WF_NO_CUST_COST',L_item,I_wf_store,NULL);
	 end if;
	 return FALSE;
      end if;

      O_acqui_cost := L_acquisition_cost;
      O_cust_cost  := L_pricing_cost;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END GET_COST_INFORMATION;
-------------------------------------------------------------------------------
FUNCTION GET_EXISTING_ORDER_COST_INFO (O_error_message	 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
				       O_margin_pct	 IN OUT   WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE,
				       O_cust_cost	 IN OUT   FUTURE_COST.PRICING_COST%TYPE,
				       O_acqui_cost	 IN OUT   FUTURE_COST.ACQUISITION_COST%TYPE,
				       I_wf_order_no	 IN	  WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
				       I_item		 IN	  STORE_ORDERS.ITEM%TYPE,
				       I_store		 IN	  STORE_ORDERS.STORE%TYPE,
				       I_source_loc_type IN	  WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
				       I_source_loc_id	 IN	  WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
				       I_need_date	 IN	  WF_ORDER_DETAIL.NEED_DATE%TYPE)
RETURN BOOLEAN IS
   L_program	  VARCHAR2(50)	:= 'WF_ORDER_SQL.GET_EXISTING_ORDER_COST_INFO';

   cursor C_GET_COST_INFO IS
      select margin_pct,
	     acquisition_cost,
	     NVL(fixed_cost,customer_cost) customer_cost
	from wf_order_detail
       where wf_order_no     = I_wf_order_no
	 and item	     = I_item
	 and customer_loc    = I_store
	 and source_loc_id   = I_source_loc_id
	 and source_loc_type = I_source_loc_type
	 and need_date	     = I_need_date;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;

   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_store',L_program,NULL);
      return FALSE;
   end if;

   if I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_id',L_program,NULL);
      return FALSE;
   end if;

   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_type',L_program,NULL);
      return FALSE;
   end if;

   if I_need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_need_date',L_program,NULL);
      return FALSE;
   end if;

   open C_GET_COST_INFO;
   fetch C_GET_COST_INFO into O_margin_pct,
			      O_acqui_cost,
			      O_cust_cost;
   close C_GET_COST_INFO;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END GET_EXISTING_ORDER_COST_INFO;
-------------------------------------------------------------------------------
FUNCTION GET_UPCHARGE_INFORMATION (O_error_message     IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
				   I_order_no	       IN	WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
				   I_order_cur	       IN	CURRENCY_RATES.CURRENCY_CODE%TYPE,
				   I_item	       IN	WF_ORDER_DETAIL.ITEM%TYPE,
				   I_source_loc_type   IN	WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
				   I_source_loc_id     IN	WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
				   I_customer_loc      IN	WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE)
RETURN BOOLEAN IS

   L_program		 VARCHAR2(50)				       := 'WF_ORDER_SQL.GET_UPCHARGE_INFORMATION';
   L_dept		 ITEM_MASTER.DEPT%TYPE;
   L_class		 ITEM_MASTER.CLASS%TYPE;
   L_subclass		 ITEM_MASTER.SUBCLASS%TYPE;
   L_cust_loc		 WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_seq_no		 WF_ORDER_EXP.SEQ_NO%TYPE;
   L_upcharge_amt	 WF_ORDER_EXP.EST_UPCHARGE_AMT%TYPE	       := 0;
   L_exchange_rate	 WF_ORDER_EXP.EXCHANGE_RATE%TYPE;
   L_exchange_rate_comp  WF_ORDER_EXP.EXCHANGE_RATE%TYPE;
   L_exchange_rate_order WF_ORDER_EXP.EXCHANGE_RATE%TYPE;
   L_comp_currency	 WF_ORDER_EXP.COMP_CURRENCY%TYPE	       := NULL;
   L_per_count_uom	 WF_ORDER_EXP.PER_COUNT_UOM%TYPE	       := NULL;
   L_per_count		 WF_ORDER_EXP.PER_COUNT%TYPE		       := NULL;
   L_source_loc_id	 WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_supplier		 ITEM_LOC.PRIMARY_SUPP%TYPE;
   L_origin_country_id	 ITEM_LOC.PRIMARY_CNTRY%TYPE;
   L_templ_id		 WF_ORDER_DETAIL.TEMPL_ID%TYPE;
   L_approval_date	 WF_ORDER_HEAD.APPROVAL_DATE%TYPE	       := NULL;
   L_dimension_ind	 VARCHAR2(1)				       := NULL;
   L_uom_class		 UOM_CLASS.UOM_CLASS%TYPE;
   L_standard_uom	 UOM_CLASS.UOM%TYPE;
   L_standard_class	 UOM_CLASS.UOM_CLASS%TYPE;
   L_per_unit_value	 NUMBER 				       := 0;
   L_value		 NUMBER 				       := 0;
   L_est_upchrg_exp	 NUMBER;
   L_uom		 UOM_CLASS.UOM%TYPE;
   L_comp_rate		 WF_COST_BUILDUP_TMPL_DETAIL.COMP_RATE%TYPE;


   L_supp_pack_size	 ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_ship_carton_wt	 ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_weight_uom 	 ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_ship_carton_len	 ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_ship_carton_hgt	 ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_ship_carton_wid	 ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_dimension_uom	 ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_liquid_volume	 ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_liquid_volume_uom	 ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;

   cursor C_GET_ORDER_DETAIL IS
      select im.dept		    dept,
	     im.class		    class,
	     im.subclass	    subclass,
	     wod.customer_loc	    customer_loc,
	     wod.source_loc_id	    source_loc_id,
	     wod.templ_id	    templ_id,
	     wod.acquisition_cost   acquisition_cost,
	     wod.calc_type	    calc_type,
	     wod.margin_pct	    margin_pct,
	     wod.wf_order_line_no   wf_order_line_no,
	     woh.approval_date	    approval_date
	from wf_order_detail wod,
	     wf_order_head woh,
	     item_master im
       where woh.wf_order_no	 = I_order_no
	 and woh.wf_order_no	 = wod.wf_order_no
	 and wod.item		 = I_item
	 and wod.source_loc_type = I_source_loc_type
	 and wod.source_loc_id	 = I_source_loc_id
	 and wod.customer_loc	 = I_customer_loc
	 and im.item		 = wod.item;

   cursor C_GET_DIMENSION is
      select i.supp_pack_size,
	     id.weight,
	     id.weight_uom,
	     id.length,
	     id.height,
	     id.width,
	     id.lwh_uom,
	     id.liquid_volume,
	     id.liquid_volume_uom
	from item_supp_country i,
	     item_supp_country_dim id
       where i.item = id.item
	 and i.supplier = id.supplier
	 and i.origin_country_id = id.origin_country
	 and id.dim_object = 'CA'
	 and i.item	  = I_item
	 and ((i.supplier = L_supplier
	       and L_supplier is not NULL)
		or (i.primary_supp_ind = 'Y'
		     and L_supplier is NULL))
	 and ((i.origin_country_id = L_origin_country_id
	       and L_origin_country_id is not NULL)
		or (i.primary_country_ind = 'Y'
		     and L_origin_country_id is NULL));

   cursor C_NEXT_SEQ is
      select nvl(max(seq_no + 1), 1)
	from wf_order_exp
       where wf_order_no = I_order_no;

   cursor C_CHK_CALC_BASIS is
      select 'x'
	from wf_cost_buildup_tmpl_detail
       where templ_id	= L_templ_id
	 and calc_basis = 'S'
	 and rownum	= 1;

   cursor C_GET_MISC_VALUE is
      select value
	from item_supp_uom
       where item     = I_item
	 and supplier = L_supplier
	 and uom      = L_per_count_uom;

   cursor C_GET_SUPPLIER_COUNTRY is
      select supplier,
	     origin_country_id
	from item_supp_country
       where item		 = I_item
	 and I_source_loc_type	 = 'SU'
	 and supplier		 = I_source_loc_id
	 and primary_country_ind = 'Y'
      UNION
      select supplier,
	     origin_country_id
	from item_supp_country
       where item		 = I_item
	 and I_source_loc_type	 <> 'SU'
	 and primary_country_ind = 'Y'
	 and primary_supp_ind	 = 'Y';

   -- The expense will be written only for calc_type as 'M'argin or 'U'pcharge first.
   -- For Retail and Fixed cost based template, there will be no record in wf_cost_buildup_tmpl_detail table.
   cursor C_GET_COST_DETAIL IS
      select wd.cost_comp_id cost_comp_id,
	     wd.calc_basis calc_basis,
	     wd.comp_rate comp_rate,
	     wd.per_count per_count,
	     wd.per_count_uom per_count_uom,
	     wd.comp_currency comp_currency
	from wf_cost_buildup_tmpl_detail wd,
	     wf_cost_relationship wr
   where wr.templ_id    = wd.templ_id
	 and wr.dept	    = L_dept
	 and DECODE(wr.item,'-1',I_item,wr.item) = I_item
	 and wr.class = L_class
	 and wr.subclass = L_subclass
	 and wr.location    = L_cust_loc
	 and wr.templ_id    = L_templ_id
	 and wr.start_date <= LP_vdate
	 and wr.end_date   >= LP_vdate
   union
  select wd.cost_comp_id cost_comp_id,
	     wd.calc_basis calc_basis,
	     wd.comp_rate comp_rate,
	     wd.per_count per_count,
	     wd.per_count_uom per_count_uom,
	     wd.comp_currency comp_currency
	from wf_cost_buildup_tmpl_detail wd,
	     wf_cost_relationship wr
   where wr.templ_id    = wd.templ_id
	 and wr.dept	    = L_dept
	 and DECODE(wr.item,'-1',I_item,wr.item) = I_item
	 and wr.class = L_class
	 and wr.subclass = -1
	 and wr.location    = L_cust_loc
	 and wr.templ_id    = L_templ_id
	 and wr.start_date <= LP_vdate
	 and wr.end_date   >= LP_vdate
   union
  select wd.cost_comp_id cost_comp_id,
	     wd.calc_basis calc_basis,
	     wd.comp_rate comp_rate,
	     wd.per_count per_count,
	     wd.per_count_uom per_count_uom,
	     wd.comp_currency comp_currency
	from wf_cost_buildup_tmpl_detail wd,
	     wf_cost_relationship wr
   where wr.templ_id    = wd.templ_id
	 and wr.dept	    = L_dept
	 and DECODE(wr.item,'-1',I_item,wr.item) = I_item
	 and wr.class = -1
	 and wr.subclass = -1
	 and wr.location    = L_cust_loc
	 and wr.templ_id    = L_templ_id
	 and wr.start_date <= get_vdate
	 and wr.end_date   >= get_vdate;
   
BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   FOR rec_ord in C_GET_ORDER_DETAIL LOOP
      L_dept		:= rec_ord.dept;
      L_class		:= rec_ord.class;
      L_subclass	:= rec_ord.subclass;
      L_cust_loc	:= rec_ord.customer_loc;
      L_source_loc_id	:= rec_ord.source_loc_id;
      L_templ_id	:= rec_ord.templ_id;
      L_approval_date	:= rec_ord.approval_date;

      FOR rec_cost in C_GET_COST_DETAIL LOOP
	 SQL_LIB.SET_MARK('OPEN','C_NEXT_SEQ', 'WF_ORDER_EXP', 'wf_order_no: '||to_char(I_order_no));
	 open C_NEXT_SEQ;
	 SQL_LIB.SET_MARK('FETCH','C_NEXT_SEQ', 'WF_ORDER_EXP', 'wf_order_no: '||to_char(I_order_no));
	 fetch C_NEXT_SEQ into L_seq_no;
	 SQL_LIB.SET_MARK('CLOSE','C_NEXT_SEQ', 'WF_ORDER_EXP', 'wf order no: '||to_char(I_order_no));
	 close C_NEXT_SEQ;
	 L_exchange_rate := 1;

	 ----Getting primay suplier and country id for the item
	 open C_GET_SUPPLIER_COUNTRY;
	 fetch C_GET_SUPPLIER_COUNTRY into L_supplier, L_origin_country_id;
	 close C_GET_SUPPLIER_COUNTRY;

	 SQL_LIB.SET_MARK('OPEN','C_CHK_CALC_BASIS', 'WF_COST_BUILDUP_TMPL_DETAIL', 'wf_order_no: '||to_char(I_order_no));
	 open C_CHK_CALC_BASIS;
	 SQL_LIB.SET_MARK('FETCH','C_CHK_CALC_BASIS', 'WF_COST_BUILDUP_TMPL_DETAIL', 'wf_order_no: '||to_char(I_order_no));
	 fetch C_CHK_CALC_BASIS into L_dimension_ind;
	 SQL_LIB.SET_MARK('CLOSE','C_CHK_CALC_BASIS', 'WF_COST_BUILDUP_TMPL_DETAIL', 'wf order no: '||to_char(I_order_no));
	 close C_CHK_CALC_BASIS;

	 if L_dimension_ind is NOT NULL then
	    SQL_LIB.SET_MARK('OPEN','C_GET_DIMENSION', 'ITEM_SUPP_COUNTRY_DIM', 'wf_order_no: '||to_char(I_order_no));
	    open C_GET_DIMENSION;
	    SQL_LIB.SET_MARK('FETCH','C_GET_DIMENSION', 'ITEM_SUPP_COUNTRY_DIM', 'wf_order_no: '||to_char(I_order_no));
	    fetch C_GET_DIMENSION into L_supp_pack_size,
				       L_ship_carton_wt,
				       L_weight_uom,
				       L_ship_carton_len,
				       L_ship_carton_hgt,
				       L_ship_carton_wid,
				       L_dimension_uom,
				       L_liquid_volume,
				       L_liquid_volume_uom;
	    SQL_LIB.SET_MARK('CLOSE','C_GET_DIMENSION', 'ITEM_SUPP_COUNTRY_DIM', 'wf order no: '||to_char(I_order_no));
	    close C_GET_DIMENSION;
	 end if;

	 if rec_cost.calc_basis ='V' then
        L_upcharge_amt  := rec_ord.acquisition_cost*(rec_cost.comp_rate/100);
	    L_comp_currency := NULL;
	    L_per_count     := NULL;
	    L_per_count_uom := NULL;
	    L_comp_rate     := NULL;
	    L_exchange_rate := NULL;
	 else
	    L_comp_currency := rec_cost.comp_currency;
	    L_per_count     := rec_cost.per_count;
	    L_per_count_uom := rec_cost.per_count_uom;
	    L_comp_rate     := rec_cost.comp_rate;

	    if UOM_SQL.GET_CLASS(O_error_message,
				 L_uom_class,
				 L_per_count_uom) = FALSE then
	      return FALSE;
	    end if;

	    if L_uom_class = 'QTY' then
	       if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
						   L_standard_uom,
						   L_standard_class,
						   L_per_unit_value, -- standard UOM conversion factor
						   I_item,
						   'N') = FALSE then
		  return FALSE;
	       end if;
	       if L_per_unit_value is NULL then
		  if L_standard_uom <> 'EA' then
		     L_per_unit_value := 0;
		  else
		     L_per_unit_value := 1;
		  end if;
	       end if;
	       ---
	       L_uom := 'EA';
	    elsif L_uom_class = 'PACK' then
	       L_value := 1/L_supp_pack_size;
	    elsif L_uom_class = 'MISC' then
	       open C_GET_MISC_VALUE;
	       fetch C_GET_MISC_VALUE into L_value;
	       close C_GET_MISC_VALUE;
	       ---
	       if L_value is NULL then
		  L_value := 0;
	       end if;
	    elsif L_uom_class = 'MASS' then
	       if L_ship_carton_wt is NULL then
		  L_per_unit_value := 0;
	       else
		  L_per_unit_value := L_ship_carton_wt/L_supp_pack_size;
		  L_uom := L_weight_uom;
	       end if;
	    elsif L_uom_class = 'LVOL' then
	       if L_liquid_volume_uom is NULL then
		  L_per_unit_value := 0;
	       else
		  L_per_unit_value := L_liquid_volume/L_supp_pack_size;
		  L_uom := L_liquid_volume_uom;
	       end if;
	    elsif L_uom_class = 'VOL' then
	       if L_ship_carton_len is NULL or
		  L_ship_carton_wid is NULL or
		  L_ship_carton_hgt is NULL then
		  L_per_unit_value := 0;
	       else
		  ---
		  -- Get the per unit ship carton volume.
		  ---
		  L_per_unit_value := (L_ship_carton_len * L_ship_carton_hgt * L_ship_carton_wid)
				       /L_supp_pack_size;
		  ---
		  L_uom := L_dimension_uom||'3';
	       end if;
	    elsif L_uom_class = 'AREA' then
	       if L_ship_carton_len is NULL or
		  L_ship_carton_wid is NULL then
		  L_per_unit_value := 0;
	       else
		  ---
		  -- Get the per unit ship carton area.
		  ---
		  L_per_unit_value := (L_ship_carton_len * L_ship_carton_wid)/L_supp_pack_size;
		  ---
		  L_uom := L_dimension_uom||'2';
	       end if;
	    elsif L_uom_class = 'DIMEN' then
	       if L_ship_carton_len is NULL then
		  L_per_unit_value := 0;
	       else
		  ---
		  -- Get the per unit ship carton length.
		  ---
		  L_per_unit_value := L_ship_carton_len/L_supp_pack_size;
		  ---
		  L_uom := L_dimension_uom;
	       end if;
	    end if;
	    ---
	    if L_uom_class in ('VOL','AREA','DIMEN','QTY','MASS','LVOL') then
	       if L_per_unit_value != 0 then
		  if UOM_SQL.WITHIN_CLASS(O_error_message,
					  L_value,
					  L_per_count_uom,
					  L_per_unit_value,
					  L_uom,
					  L_uom_class) = FALSE then
		     return FALSE;
		  end if;
	       else
		  L_value := 0;
	       end if;
	    end if;

	    L_upcharge_amt:= (L_value * (L_comp_rate/L_per_count));

	    if I_order_cur != L_comp_currency then
	       -- Getting the exchange rate of the  the order currency in primary currency
	       if CURRENCY_SQL.GET_RATE(O_error_message,
					L_exchange_rate_order,
					I_order_cur,
					NULL,
					L_approval_date) = FALSE then
		  return FALSE;
	       end if;
	       -- Getting the exchange rate of the  the component currency in primary currency
	       if CURRENCY_SQL.GET_RATE(O_error_message,
					L_exchange_rate_comp,
					L_comp_currency,
					NULL,
					L_approval_date) = FALSE then
		  return FALSE;
	       end if;
	       L_exchange_rate := L_exchange_rate_comp / L_exchange_rate_order;
	    end if;
	 end if;

	 SQL_LIB.SET_MARK('INSERT',NULL,'WF_ORDER_EXP', 'wf_order_no: '||to_char(I_order_no));

	 insert into wf_order_exp(wf_order_no,
				  seq_no,
				  wf_order_line_no,
				  comp_id,
				  calc_basis,
				  comp_rate,
				  per_count,
				  per_count_uom,
				  comp_currency,
				  exchange_rate,
				  est_upcharge_amt)
			   values(I_order_no,
				  L_seq_no,
				  rec_ord.wf_order_line_no,
				  rec_cost.cost_comp_id,
				  rec_cost.calc_basis,
				  rec_cost.comp_rate,
				  L_per_count,
				  L_per_count_uom,
				  L_comp_currency,
				  L_exchange_rate,
				  L_upcharge_amt);
      END LOOP;
   END LOOP;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END GET_UPCHARGE_INFORMATION;
--------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_UPCHARGE(O_error_message	IN OUT	 RTK_ERRORS.RTK_TEXT%TYPE,
			    O_total_upcharge	IN OUT	 WF_ORDER_EXP.EST_UPCHARGE_AMT%TYPE,
			    I_order_no		IN	 WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
			    I_source_loc_type	IN	 WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
			    I_source_loc_id	IN	 WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
			    I_item		IN	 WF_ORDER_DETAIL.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'WF_ORDER_SQL.GET_TOTAL_UPCHARGE';

   cursor C_TOT_EST_UPCHG_AMT IS
      select SUM(NVL((wod.requested_qty * woe.est_upcharge_amt),0) / NVL(woe.exchange_rate,1))
	from wf_order_exp woe,
	     wf_order_detail wod
       where wod.wf_order_no	  = I_order_no
	 and wod.item		  = NVL(I_item,wod.item)
	 and wod.source_loc_type  = NVL(I_source_loc_type,wod.source_loc_type)
	 and wod.source_loc_id	  = NVL(I_source_loc_id,wod.source_loc_id)
	 and wod.wf_order_no	  = woe.wf_order_no
	 and wod.wf_order_line_no = woe.wf_order_line_no;
BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   open C_TOT_EST_UPCHG_AMT;
   fetch C_TOT_EST_UPCHG_AMT into O_total_upcharge;
   close C_TOT_EST_UPCHG_AMT;

   O_total_upcharge := NVL(O_total_upcharge,0);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END GET_TOTAL_UPCHARGE;
--------------------------------------------------------------------------------------
FUNCTION TOTAL_ORDER_CUSTOMER_COST(O_error_message	    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
				   O_tot_acq_cost	    IN OUT  WF_ORDER_DETAIL.ACQUISITION_COST%TYPE,
				   O_tot_acq_cost_pri	    IN OUT  WF_ORDER_DETAIL.ACQUISITION_COST%TYPE,
				   O_cust_cost_excl_vat     IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
				   O_cust_cost_excl_vat_pri IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
				   O_cust_cost_incl_vat     IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
				   O_cust_cost_incl_vat_pri IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
				   O_total_margin	    IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
				   O_total_margin_pri	    IN OUT  WF_ORDER_DETAIL.CUSTOMER_COST%TYPE,
				   I_wf_order_no	    IN	    WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
				   I_wf_order_cur	    IN	    WF_ORDER_HEAD.CURRENCY_CODE%TYPE,
				   I_wf_order_exg_rate	    IN	    WF_ORDER_HEAD.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN IS

   L_program		      VARCHAR2(60)			 := 'WF_ORDER_SQL.TOTAL_ORDER_CUSTOMER_COST';
   L_system_options_row       SYSTEM_OPTIONS%ROWTYPE;
   L_item		      WF_ORDER_DETAIL.ITEM%TYPE;
   L_cust_loc		      WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_source_loc_id	      WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_source_loc_type	      WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_vat_rate		      VAT_CODE_RATES.VAT_RATE%TYPE	 := 0;
   L_vat_code		      VAT_CODE_RATES.VAT_CODE%TYPE	 := NULL;
   L_order_cur		      WF_ORDER_HEAD.CURRENCY_CODE%TYPE;
   L_exchange_rate	      WF_ORDER_HEAD.EXCHANGE_RATE%TYPE;
   L_cust_cost_excl_vat       WF_ORDER_DETAIL.CUSTOMER_COST%TYPE :=0;
   L_tot_cust_cost_incl_vat   WF_ORDER_DETAIL.CUSTOMER_COST%TYPE :=0;
   L_tot_cust_cost_excl_vat   WF_ORDER_DETAIL.CUSTOMER_COST%TYPE :=0;

   cursor C_WF_ORDER_DETAIL IS
      select item,
	     source_loc_id,
	     source_loc_type,
	     customer_loc,
	     NVL(requested_qty,0) * NVL(fixed_cost, NVL(customer_cost,0)) customer_cost
	from wf_order_detail
       where wf_order_no = I_wf_order_no;

   TYPE wf_ord_det_TBL IS TABLE OF C_WF_ORDER_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_ord_dtl_tbl  wf_ord_det_TBL;

   cursor C_MARGIN_ACQUISITION_COST IS
      select SUM(NVL(requested_qty,0) * NVL(wod.acquisition_cost,0))			  acquisition_cost,
	     SUM(NVL(requested_qty,0) * (NVL(wod.fixed_cost,NVL(wod.customer_cost,0))
				       - NVL(wod.acquisition_cost,0)
				       - NVL(exp.upcharge_amt,0)))			  margin
	from wf_order_detail wod,
	     (select wod.wf_order_no,
		     wod.wf_order_line_no,
		     SUM(NVL(woe.est_upcharge_amt,0) / NVL(woe.exchange_rate,1))	  upcharge_amt
		from wf_order_exp woe,
		     wf_order_detail wod
	       where wod.wf_order_no = I_wf_order_no
		 and wod.wf_order_no = woe.wf_order_no(+)
		 and wod.wf_order_line_no = woe.wf_order_line_no(+)
	       group by wod.wf_order_no,wod.wf_order_line_no) exp
       where wod.wf_order_no = exp.wf_order_no
	 and wod.wf_order_line_no = exp.wf_order_line_no
      group by wod.wf_order_no;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   L_order_cur	   := I_wf_order_cur;
   L_exchange_rate := I_wf_order_exg_rate;

   if ((L_order_cur is NULL) OR (L_exchange_rate is NULL)) then
      if GET_CURRENCY_RATE(O_error_message,
			   L_order_cur,
			   L_exchange_rate,
			   I_wf_order_no) = FALSE then
	 return FALSE;
      end if;
   end if;

   open C_MARGIN_ACQUISITION_COST;
   fetch C_MARGIN_ACQUISITION_COST into O_tot_acq_cost, O_total_margin;
   close C_MARGIN_ACQUISITION_COST;

   FOR rec IN C_WF_ORDER_DETAIL LOOP
      L_item		   := rec.item;
      L_cust_loc	   := rec.customer_loc;
      L_source_loc_id	   := rec.source_loc_id;
      L_source_loc_type    := rec.source_loc_type;
      L_cust_cost_excl_vat := rec.customer_cost;

      if NOT WF_ORDER_SQL.GET_TAX_INFO(O_error_message,
				       L_vat_rate,
				       L_vat_code,
				       L_item,
				       L_source_loc_type,
				       L_source_loc_id,
				       L_cust_loc,
				       'R',
				       I_wf_order_no) then
	 return FALSE;
      end if;

      L_tot_cust_cost_excl_vat := L_tot_cust_cost_excl_vat + L_cust_cost_excl_vat;
      L_tot_cust_cost_incl_vat := L_tot_cust_cost_incl_vat + (L_cust_cost_excl_vat * (1 + nvl(L_vat_rate,0)/100));

   END LOOP;

   O_cust_cost_excl_vat := L_tot_cust_cost_excl_vat;
   O_cust_cost_incl_vat := L_tot_cust_cost_incl_vat;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
					    L_system_options_row) = FALSE then
      return FALSE;
   end if;

   -- convert order currency values to primary currency values
   if L_order_cur = L_system_options_row.currency_code then
      O_tot_acq_cost_pri       := O_tot_acq_cost;
      O_cust_cost_excl_vat_pri := O_cust_cost_excl_vat;
      O_cust_cost_incl_vat_pri := O_cust_cost_incl_vat;
      O_total_margin_pri       := O_total_margin;
   else  -- Convert
      O_tot_acq_cost_pri       := O_tot_acq_cost       / NVL(L_exchange_rate,1);
      O_cust_cost_excl_vat_pri := O_cust_cost_excl_vat / NVL(L_exchange_rate,1);
      O_cust_cost_incl_vat_pri := O_cust_cost_incl_vat / NVL(L_exchange_rate,1);
      O_total_margin_pri       := O_total_margin       / NVL(L_exchange_rate,1);
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     NULL);
   RETURN FALSE;
END TOTAL_ORDER_CUSTOMER_COST;
------------------------------------------------------------------------
FUNCTION ORDER_AUDIT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
		     I_wf_order_no     IN     WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
		     I_order_line_no   IN     WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE,
		     I_cancel_item_ind IN     WF_ORDER_AUDIT.ITEM_CANCELLED_IND%TYPE,
		     I_qty	       IN     TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_program		     VARCHAR2(30)  := 'WF_ORDER_SQL.ORDER_AUDIT';
   L_seq		     WF_ORDER_AUDIT.SEQ_NO%TYPE;
   cursor C_NEXT_SEQ is
      select nvl(max(seq_no + 1), 1)
	from wf_order_audit
       where wf_order_no = I_wf_order_no;
BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   if I_order_line_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_line_no',L_program,NULL);
      return FALSE;
   end if;

   open C_NEXT_SEQ;
   fetch C_NEXT_SEQ into L_seq;
   close C_NEXT_SEQ;

   insert into wf_order_audit(wf_order_no,
			      seq_no,
			      wf_order_line_no,
			      item_cancelled_ind,
			      modified_qty)
		       values(I_wf_order_no,
			      L_seq,
			      I_order_line_no,
			      I_cancel_item_ind,
			      I_qty);

   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    NULL);
      RETURN FALSE;
END ORDER_AUDIT;
------------------------------------------------------------------------
FUNCTION DELETE_WF_ORDER ( O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
			   I_wf_order_no      IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program	  VARCHAR2(50)		     := 'WF_ORDER_SQL.DELETE_WF_ORDER';
   L_status	  WF_ORDER_HEAD.STATUS%TYPE  := NULL;
   L_table	  VARCHAR2(50);
   L_key	  VARCHAR2(64)		     := I_wf_order_no;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA	  EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_WF_ORDER_HEAD IS
      select 'x'
	from wf_order_head
       where wf_order_no = I_wf_order_no
	 for update nowait;

   cursor C_LOCK_WF_ORDER_DETAIL IS
      select 'x'
	from wf_order_detail
       where wf_order_no = I_wf_order_no
	 for update nowait;

   cursor C_LOCK_WF_ORDER_EXP IS
      select 'x'
	from wf_order_exp
       where wf_order_no = I_wf_order_no
	 for update nowait;

   cursor C_WF_ORDER_STATUS IS
      select status
	from wf_order_head
       where wf_order_no = I_wf_order_no;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   open C_WF_ORDER_STATUS;
   fetch C_WF_ORDER_STATUS into L_status;
   close C_WF_ORDER_STATUS;

   if L_status <> 'I' then
      O_error_message := SQL_LIB.CREATE_MSG('DEL_INPUT_ORD','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   L_table := 'WF_ORDER_EXP';
   open C_LOCK_WF_ORDER_EXP;
   close C_LOCK_WF_ORDER_EXP;

   L_table := 'WF_ORDER_DETAIL';
   open C_LOCK_WF_ORDER_DETAIL;
   close C_LOCK_WF_ORDER_DETAIL;

   L_table := 'WF_ORDER_HEAD';
   open C_LOCK_WF_ORDER_HEAD;
   close C_LOCK_WF_ORDER_HEAD;

   delete from wf_order_exp
    where wf_order_no = I_wf_order_no;

   delete from wf_order_detail
    where wf_order_no = I_wf_order_no;

   --to delete the PO's linked with the wf order
   if WF_PO_SQL.DELETE_FRANCHISE_PO (O_error_message,
				     I_wf_order_no) = FALSE THEN

      RETURN FALSE;
   end if;

   --to delete Transfers linked with the wf order
   if WF_TRANSFER_SQL.DELETE_FRANCHISE_TSF (O_error_message,
					    I_wf_order_no,
					    NULL) = FALSE THEN

      RETURN FALSE;
   end if;

   delete from wf_order_head
    where wf_order_no = I_wf_order_no;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      if C_LOCK_WF_ORDER_EXP%ISOPEN then
	 close C_LOCK_WF_ORDER_EXP;
      end if;
      if C_LOCK_WF_ORDER_DETAIL%ISOPEN then
	 close C_LOCK_WF_ORDER_DETAIL;
      end if;
      if C_LOCK_WF_ORDER_HEAD%ISOPEN then
	 close C_LOCK_WF_ORDER_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
					     L_table,
					     L_key,
					     NULL);
      return FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    NULL);
      RETURN FALSE;
END DELETE_WF_ORDER;
------------------------------------------------------------------------
FUNCTION GET_ORDER_DETAILS( O_error_message	    IN OUT  rtk_errors.rtk_text%TYPE,
			    O_customer_group_name   IN OUT  wf_customer_group.wf_customer_group_name%TYPE,
			    O_customer_name	    IN OUT  wf_customer.wf_customer_name%TYPE,
			    O_status		    IN OUT  wf_order_head.status%TYPE,
			    O_cust_ord_ref_no	    IN OUT  wf_order_head.cust_ord_ref_no%TYPE ,
			    O_order_type	    IN OUT  wf_order_head.order_type%TYPE,
			    O_customer_group_id     IN OUT  wf_customer.wf_customer_group_id%TYPE,
			    O_customer_id	    IN OUT  store.wf_customer_id%TYPE ,
			    I_order_no		    IN	    wf_order_head.wf_order_no%TYPE)
RETURN BOOLEAN IS

   L_program	  VARCHAR2(50) := 'WF_ORDER_SQL.GET_ORDER_DETAILS';

   cursor C_order_details is
      select woh.status,
	     woh.cust_ord_ref_no,
	     woh.order_type,
	     wc.wf_customer_name,
	     wcg.wf_customer_group_name,
	     wc.wf_customer_group_id,
	     st.wf_customer_id
	from wf_order_head woh,
	     wf_order_detail wod,
	     wf_customer wc,
	     wf_customer_group wcg,
	     store st
       where woh.wf_order_no	       =  I_order_no
	 and woh.wf_order_no	       =  wod.wf_order_no
	 and wc.wf_customer_id	       =  st.wf_customer_id
	 and wod.customer_loc	       =  st.store
	 and wcg.wf_customer_group_id  =  wc.wf_customer_group_id
	 and rownum = 1;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   open c_order_details;
   fetch c_order_details into O_status, O_cust_ord_ref_no, O_order_type,O_customer_name, O_customer_group_name, O_customer_group_id, O_customer_id;
   close c_order_details;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    NULL);
      RETURN FALSE;
END GET_ORDER_DETAILS;
------------------------------------------------------------------------
FUNCTION GET_RETURN_DETAILS( O_error_message	     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE ,
			     O_cust_ret_ref_no	     IN OUT  WF_RETURN_HEAD.CUST_RET_REF_NO%TYPE,
			     O_customer_group_name   IN OUT  WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_NAME%TYPE,
			     O_customer_name	     IN OUT  WF_CUSTOMER.WF_CUSTOMER_NAME%TYPE,
			     O_status		     IN OUT  WF_RETURN_HEAD.STATUS%TYPE,
			     O_return_type	     IN OUT  WF_RETURN_HEAD.RETURN_TYPE%TYPE,
			     O_return_method	     IN OUT  WF_RETURN_HEAD.RETURN_METHOD%TYPE,
			     O_cust_loc 	     IN OUT  WF_RETURN_HEAD.CUSTOMER_LOC%TYPE ,
			     O_customer_group_id     IN OUT  WF_CUSTOMER.WF_CUSTOMER_GROUP_ID%TYPE ,
			     O_customer_id	     IN OUT  STORE.WF_CUSTOMER_ID%TYPE ,
			     O_return_loc_type	     IN OUT  WF_RETURN_HEAD.RETURN_LOC_TYPE%TYPE,
			     O_return_loc_id	     IN OUT  WF_RETURN_HEAD.RETURN_LOC_ID%TYPE,
			     I_rma_no		     IN      WF_RETURN_HEAD.RMA_NO%TYPE)
RETURN BOOLEAN IS

   L_program	  VARCHAR2(50) := 'WF_ORDER_SQL.GET_RETURN_DETAILS';

   cursor C_rma_details is
      select wrh.status,
	     wrh.customer_loc,
	     wrh.cust_ret_ref_no,
	     wrh.return_loc_type,
	     wrh.return_loc_id,
	     wrh.return_type,
	     wrh.return_method,
	     wc.wf_customer_id,
	     wc.wf_customer_name,
	     wc.wf_customer_group_id,
	     wcg.wf_customer_group_name,
	     st.store
	from wf_return_head wrh,
	     wf_customer wc,
	     store st,
	     wf_customer_group wcg
       where wrh.rma_no 	       =  I_rma_no
	 and wrh.customer_loc	       =  st.store
	 and wc.wf_customer_id	       =  st.wf_customer_id
	 and wcg.wf_customer_group_id  =  wc.wf_customer_group_id
	 and rownum = 1;

BEGIN

   if I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_rma_no',L_program,NULL);
      return FALSE;
   end if;

   open c_rma_details;
   fetch c_rma_details into  O_status,
			     O_cust_loc,
			     O_cust_ret_ref_no,
			     O_return_loc_type,
			     O_return_loc_id,
			     O_return_type,
			     O_return_method,
			     O_customer_id,
			     O_customer_name,
			     O_customer_group_id,
			     O_customer_group_name,
			     O_cust_loc;
   close c_rma_details;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    NULL);
      RETURN FALSE;
END GET_RETURN_DETAILS;
------------------------------------------------------------------------
FUNCTION GET_WF_ORDER_HEAD_DETAILS(O_error_message	    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
				   O_wf_order_head_details  IN OUT   WF_ORDER_HEAD%ROWTYPE,
				   I_wf_order_no	    IN	     WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'WF_ORDER_SQL.GET_WF_ORDER_HEAD_DETAILS';

   cursor C_WF_ORDER_HEAD is
      select *
	from wf_order_head
       where wf_order_no = I_wf_order_no;

BEGIN
   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   open  C_WF_ORDER_HEAD;
   fetch C_WF_ORDER_HEAD into O_wf_order_head_details;
   close C_WF_ORDER_HEAD;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    to_char(SQLCODE));
      return FALSE;
END GET_WF_ORDER_HEAD_DETAILS;
------------------------------------------------------------------------
FUNCTION GET_WF_ORDER_LINE_INFO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
				O_wf_order_detail   IN OUT   WF_ORDER_DETAIL%ROWTYPE,
				I_wf_order_no	    IN	     WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
				I_item		    IN	     WF_ORDER_DETAIL.ITEM%TYPE,
				I_cust_loc	    IN	     WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
				I_source_loc_type   IN	     WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
				I_source_loc_id     IN	     WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE)

RETURN BOOLEAN IS

   L_program	VARCHAR2(64) := 'WF_ORDER_SQL.GET_WF_ORDER_LINE_INFO';

   cursor C_GET_DETAIL_INFO is
      select *
	from wf_order_detail
       where wf_order_no = I_wf_order_no
	 and item = I_item
	 and customer_loc = I_cust_loc
	 and source_loc_type = I_source_loc_type
	 and source_loc_id = I_source_loc_id;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_cust_loc',L_program,NULL);
      return FALSE;
   end if;

   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_type',L_program,NULL);
      return FALSE;
   end if;

   if I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_id',L_program,NULL);
      return FALSE;
   end if;

   open C_GET_DETAIL_INFO;
   fetch C_GET_DETAIL_INFO into O_wf_order_detail;
   close C_GET_DETAIL_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    to_char(SQLCODE));
      return FALSE;
END GET_WF_ORDER_LINE_INFO;
-----------------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_ITEM_COST(O_error_message	IN OUT	 RTK_ERRORS.RTK_TEXT%TYPE,
			     O_order_cost	IN OUT	 ITEM_LOC.UNIT_RETAIL%TYPE,
			     O_currency_code	IN OUT	 CURRENCY_RATES.CURRENCY_CODE%TYPE,
			     I_distro_no	IN	 SHIPSKU.DISTRO_NO%TYPE,
			     I_distro_type	IN	 SHIPSKU.DISTRO_TYPE%TYPE,
			     I_to_loc		IN	 ALLOC_DETAIL.TO_LOC%TYPE,
			     I_to_loc_type	IN	 ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
			     I_item		IN	 ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

    L_program	VARCHAR2(50) := 'GET_ORDER_SQL.GET_ORDER_ITEM_COST';

    cursor C_GET_ORDER_RETAIL is
      select NVL(wod.fixed_cost,wod.customer_cost),
	     woh.currency_code
	from (select wf_order_no,
		     DECODE(from_loc_type,'S','ST','W','WH',from_loc_type) from_loc_type,
		     from_loc,
		     to_loc
		from tsfhead
	       where tsf_no	   = I_distro_no
		 and I_distro_type = 'T'
	      UNION ALL
	      select ad.wf_order_no,
		     'WH'  from_loc_type,
		     ah.wh from_loc,
		     ad.to_loc
		from alloc_detail ad,
		     alloc_header ah
	       where ah.alloc_no   = I_distro_no
		 and ah.alloc_no   = ad.alloc_no
		 and I_distro_type = 'A'
		 and to_loc	   = I_to_loc
		 and to_loc_type   = I_to_loc_type) tsfalloc,
	     wf_order_head woh,
	     wf_order_detail wod
       where tsfalloc.wf_order_no  = wod.wf_order_no
	 and wod.item		   = I_item
	 and woh.wf_order_no	   = wod.wf_order_no
	 and wod.source_loc_type = from_loc_type
	 and wod.source_loc_id = from_loc
	 and wod.customer_loc = to_loc;
 BEGIN

    if I_distro_no is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_distro_no',L_program,NULL);
       return FALSE;
    end if;

    if I_distro_type is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_distro_type',L_program,NULL);
       return FALSE;
    end if;

    if I_item is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
       return FALSE;
    end if;

    if I_distro_type = 'A' and I_to_loc is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_to_loc',L_program,NULL);
       return FALSE;
    end if;

    if I_distro_type = 'A' and I_to_loc_type is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_to_loc_type',L_program,NULL);
       return FALSE;
    end if;

    open C_GET_ORDER_RETAIL;
    fetch C_GET_ORDER_RETAIL into O_order_cost, O_currency_code;
    close C_GET_ORDER_RETAIL;

 return TRUE;

 EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					      SQLERRM,
					      L_program,
					      to_char(SQLCODE));
       return FALSE;

END GET_ORDER_ITEM_COST;
-----------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INFO (O_error_message	   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
		       O_tax_rate	   IN OUT   VAT_CODE_RATES.VAT_RATE%TYPE,
		       O_tax_code	   IN OUT   VAT_CODES.VAT_CODE%TYPE,
		       I_item		   IN	    WF_ORDER_DETAIL.ITEM%TYPE,
		       I_source_loc_type   IN	    WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE,
		       I_source_loc_id	   IN	    WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE,
		       I_customer_loc	   IN	    WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE,
		       I_cost_retail_ind   IN	    VARCHAR2,
		       I_wf_order_no	   IN	    WF_ORDER_HEAD.WF_ORDER_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program		VARCHAR2(64)   := 'WF_ORDER_SQL.GET_TAX_INFO';

   L_supplier_id	ORDHEAD.SUPPLIER%TYPE;
   L_tax_calc_tbl	 OBJ_TAX_CALC_TBL	       := OBJ_TAX_CALC_TBL();
   L_tax_calc_rec	 OBJ_TAX_CALC_REC	       := OBJ_TAX_CALC_REC();

   L_source_loc_id	WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_source_loc_type	WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;


   -- For supplier sourced, get the vat region for the costing location.
   cursor C_SRC_LOC is
      select 'WH' source_loc_type,
	     wh source_loc_id
	from wh
       where wh = I_source_loc_id
	 and I_source_loc_type ='WH'
      UNION
      select 'ST' source_loc_type,
	     store source_loc_id
	from store
       where store = I_source_loc_id
	 and I_source_loc_type ='ST'
      UNION
      select 'WH' source_loc_type,
	     il.costing_loc  source_loc_id
	from wh wh,
	     item_loc il
       where wh.wh = il.costing_loc
	 and il.item = I_item
	 and il.loc = I_customer_loc
	 and I_source_loc_type ='SU'
      UNION
      select 'ST' source_loc_type ,
	     il.costing_loc  source_loc_id
	from store st,
	     item_loc il
       where st.store = il.costing_loc
	 and il.item = I_item
	 and il.loc = I_customer_loc
	 and I_source_loc_type ='SU';


   cursor C_GET_ORDER_SUPS is
      select supplier
	from ordhead
       where wf_order_no = I_wf_order_no;


BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;
   if I_source_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_type',L_program,NULL);
      return FALSE;
   end if;
   if I_source_loc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_source_loc_id',L_program,NULL);
      return FALSE;
   end if;
   if I_customer_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_customer_loc',L_program,NULL);
      return FALSE;
   end if;
   if I_cost_retail_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_cost_retail_ind',L_program,NULL);
      return FALSE;
   end if;
   if I_cost_retail_ind NOT in ('C', 'R') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_cost_retail_ind',I_cost_retail_ind,L_program);
      return FALSE;
   end if;

   O_tax_rate := 0;

   open C_SRC_LOC;
   fetch C_SRC_LOC into L_source_loc_type,
			L_source_loc_id;
   close C_SRC_LOC;

   L_tax_calc_rec.I_item	       := I_item;
   L_tax_calc_rec.I_from_entity        := L_source_loc_id;
   L_tax_calc_rec.I_from_entity_type   := L_source_loc_type;
   L_tax_calc_rec.I_to_entity	       := I_customer_loc;
   L_tax_calc_rec.I_to_entity_type     := 'ST';
   L_tax_calc_rec.I_tran_type	       := 'SALE';
   L_tax_calc_rec.I_tran_date	       := GET_VDATE();
   L_tax_calc_rec.I_tran_id	       := I_wf_order_no;

   if I_source_loc_type = 'SU' then
      L_tax_calc_rec.I_source_entity_type := I_source_loc_type;
      L_tax_calc_rec.I_source_entity	  := I_source_loc_id;
   else
      open C_GET_ORDER_SUPS;
      fetch C_GET_ORDER_SUPS into L_supplier_id;
      if C_GET_ORDER_SUPS%found then
	 L_tax_calc_rec.I_source_entity_type := 'SU';
	 L_tax_calc_rec.I_source_entity      := L_supplier_id;
      end if;
      close C_GET_ORDER_SUPS;
   end if;

   L_tax_calc_tbl.extend;
   L_tax_calc_tbl(L_tax_calc_tbl.count) := L_tax_calc_rec;

   if I_cost_retail_ind = 'R' then
       if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
				  L_tax_calc_tbl) = FALSE then
	  return FALSE;
       end if;
   elsif I_cost_retail_ind = 'C' then
      if TAX_SQL.CALC_COST_TAX(O_error_message,
			       L_tax_calc_tbl) = FALSE then
	 return FALSE;
      end if;
   end if;

   if L_tax_calc_tbl.count > 0 and L_tax_calc_tbl is not null then
      if L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_exempt_ind = 'Y' then
	  O_tax_rate := NULL;
	  O_tax_code := NULL;
      else
	  O_tax_rate := L_tax_calc_tbl(L_tax_calc_tbl.count).O_cum_tax_pct;
	  if L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl is not null and L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl.count > 0 then
	     O_tax_code := L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl(1).tax_code;
	  else
	     O_tax_code := NULL;
	 end if;

      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    to_char(SQLCODE));
      return FALSE;
END GET_TAX_INFO;
-------------------------------------------------------------------------------
FUNCTION REAPPLY_CUSTOMER_COST (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
				I_order_no	    IN	     WF_ORDER_DETAIL.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program		     VARCHAR2(50)				   := 'WF_ORDER_SQL.REAPPLY_CUSTOMER_COST';
   L_exchange_rate	     WF_ORDER_EXP.EXCHANGE_RATE%TYPE;
   L_ord_currency	     FUTURE_COST.CURRENCY_CODE%TYPE;
   L_status		     WF_ORDER_HEAD.STATUS%TYPE;
   L_item		     ITEM_MASTER.ITEM%TYPE;
   L_cust_loc		     WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_source_loc_type	     WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_source_loc_id	     WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_templ_id		     WF_COST_RELATIONSHIP.TEMPL_ID%TYPE;
   L_templ_desc 	     WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE;
   L_margin_pct 	     WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE;
   L_calc_type		     WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE;
   L_cust_cost		     FUTURE_COST.PRICING_COST%TYPE;
   L_acqui_cost 	     FUTURE_COST.ACQUISITION_COST%TYPE;
   L_currency_code	     FUTURE_COST.CURRENCY_CODE%TYPE;
   L_cust_cost_order_curr    FUTURE_COST.PRICING_COST%TYPE	     := NULL;
   L_acqui_cost_order_curr   FUTURE_COST.ACQUISITION_COST%TYPE	     := NULL;

   L_table		     VARCHAR2(30);
   L_key		     VARCHAR2(100);
   RECORD_LOCKED	     EXCEPTION;
   PRAGMA		     EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_WF_ORD_HEAD IS
      select currency_code,
	     status
	from wf_order_head
       where wf_order_no = I_order_no
	 for update nowait;

   cursor C_GET_ORDER_DETAIL IS
      select wod.item item,
	     wod.customer_loc cust_loc,
	     wod.source_loc_type source_loc_type,
	     wod.source_loc_id source_loc_id
	from wf_order_detail wod
       where wf_order_no = I_order_no
	 for update nowait;

   cursor C_LOCK_WF_ORD_EXP IS
      select 'x'
	from wf_order_exp
       where wf_order_no = I_order_no
	 for update nowait;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_order_no',L_program,NULL);
      return FALSE;
   end if;

   L_table := 'WF_ORDER_HEAD';
   L_key   := 'I_ORDER_NO: ' || TO_CHAR(I_order_no);
   open C_LOCK_WF_ORD_HEAD;
   fetch C_LOCK_WF_ORD_HEAD into L_ord_currency, L_status;
   close C_LOCK_WF_ORD_HEAD;

   if L_status in ('I','R') then
      -- Getting the exchange rate of the order currency in primary currency
      if CURRENCY_SQL.GET_RATE(O_error_message,
			       L_exchange_rate,
			       L_ord_currency,
			       NULL,
			       NULL) = FALSE then
	 return FALSE;
      end if;

      update wf_order_head
	 set exchange_rate = L_exchange_rate
       where wf_order_no = I_order_no;

      L_table := 'WF_ORDER_EXP';
      L_key   := 'I_ORDER_NO';

      -- The expense have to recalculated for any change in template or acquisition cost.
      open C_LOCK_WF_ORD_EXP;
      close C_LOCK_WF_ORD_EXP;

      delete from wf_order_exp
       where wf_order_no = I_order_no;

      L_table := 'WF_ORDER_DETAIL';
      L_key   :=  'I_ORDER_NO :' || TO_CHAR(I_order_no);

      FOR rec_ord in C_GET_ORDER_DETAIL LOOP
	 L_item 	   := rec_ord.item;
	 L_cust_loc	   := rec_ord.cust_loc;
	 L_source_loc_type := rec_ord.source_loc_type;
	 L_source_loc_id   := rec_ord.source_loc_id;

	 if WF_ORDER_SQL.GET_COST_INFORMATION(O_error_message,
					      L_templ_id,
					      L_templ_desc,
					      L_margin_pct,
					      L_calc_type,
					      L_cust_cost,
					      L_acqui_cost,
					      L_currency_code,
					      L_item,
					      L_cust_loc,
					      L_source_loc_type,
					      L_source_loc_id,
					      'Y')= FALSE then
	    return FALSE;
	 end if;

	 -- Convert to order currency, the customer cost and acquisition cost
	 if CURRENCY_SQL.CONVERT(O_error_message,
				 L_cust_cost,
				 L_currency_code,
				 L_ord_currency,
				 L_cust_cost_order_curr,
				 'N',
				 NULL,
				 NULL,
				 NULL,
				 NULL) = FALSE then
	    return FALSE;
	 end if;

	 if CURRENCY_SQL.CONVERT(O_error_message,
				 L_acqui_cost,
				 L_currency_code,
				 L_ord_currency,
				 L_acqui_cost_order_curr,
				 'N',
				 NULL,
				 NULL,
				 NULL,
				 NULL) = FALSE then
	    return FALSE;
	 end if;

	 update wf_order_detail
	    set templ_id	     = L_templ_id,
		templ_desc	     = L_templ_desc,
		margin_pct	     = L_margin_pct,
		calc_type	     = L_calc_type,
		customer_cost	     = L_cust_cost_order_curr,
		acquisition_cost     = L_acqui_cost_order_curr,
		last_update_datetime = sysdate,
		last_update_id	     = get_user
	  where wf_order_no	= I_order_no
	    and item		= L_item
	    and source_loc_id	= L_source_loc_id
	    and source_loc_type = L_source_loc_type
	    and customer_loc	= L_cust_loc;

	 if WF_ORDER_SQL.GET_UPCHARGE_INFORMATION(O_error_message,
						  I_order_no,
						  L_ord_currency,
						  L_item,
						  L_source_loc_type,
						  L_source_loc_id,
						  L_cust_loc) = FALSE then
	    return FALSE;
	 end if;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WF_ORD_HEAD%ISOPEN then
	 close C_LOCK_WF_ORD_HEAD;
      end if;
      if C_LOCK_WF_ORD_EXP%ISOPEN then
	 close C_LOCK_WF_ORD_EXP;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
					     L_table,
					     L_key,
					     NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     to_char(SQLCODE));
      return FALSE;
END REAPPLY_CUSTOMER_COST;
------------------------------------------------------------------------------------------------
FUNCTION APPLY_SUPPLIER_COST_CHANGE(O_error_message	   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program		    VARCHAR2(64) := 'WF_ORDER_SQL.APPLY_SUPPLIER_COST_CHANGE';
   L_table		    VARCHAR2(50);

   L_f_ord_apply_cc_tbl     OBJ_F_ORD_APPLY_CC_TBL;
   L_consolidation_ind	    SYSTEM_OPTIONS.CONSOLIDATION_IND%TYPE;

   RECORD_LOCKED	    EXCEPTION;
   PRAGMA		    EXCEPTION_INIT(RECORD_LOCKED, -54);

   TYPE ROWID_TBL is TABLE of ROWID INDEX BY BINARY_INTEGER;
   L_woe_rowid_TBL ROWID_TBL;

   --This cursor will fetch the costing information for Franchise orders having the input item/loc
   --combination in Approved status for which the fixed_cost is not populated.
   cursor C_GET_FRAN_ORD_TO_UPDATE is
      select OBJ_F_ORD_APPLY_CC_REC(wf_order_no,
				    wf_order_line_no,
				    item,
				    source_loc_type,
				    source_loc_id,
				    customer_loc,
				    need_date,
				    approval_date,
				    acquisition_cost,
				    pricing_cost,
				    order_currency,
				    future_cost_curr,
				    templ_id,
				    templ_desc,
				    first_applied,
				    margin_pct)
	from (select fc_cost.wf_order_no,
		     fc_cost.wf_order_line_no,
		     fc_cost.item,
		     fc_cost.source_loc_type,
		     fc_cost.source_loc_id,
		     fc_cost.customer_loc,
		     fc_cost.need_date,
		     fc_cost.approval_date,
		     fc_cost.acquisition_cost * mcr.exchange_rate acquisition_cost,
		     fc_cost.pricing_cost * mcr.exchange_rate pricing_cost,
		     fc_cost.order_currency,
		     fc_cost.future_cost_curr,
		     templ_desc.templ_id,
		     templ_desc.templ_desc,
		     templ_desc.first_applied,
		     templ_desc.margin_pct,
		     row_number() over (partition by fc_cost.wf_order_no,
						     fc_cost.item,
						     fc_cost.source_loc_type,
						     fc_cost.source_loc_id,
						     fc_cost.customer_loc
					    order by mcr.effective_date desc) rown
		from (select fc.dept,
			     fc.class,
			     fc.subclass,
			     fc.acquisition_cost acquisition_cost,
			     fc.pricing_cost pricing_cost,
			     fc.currency_code future_cost_curr,
			     wod.wf_order_no,
			     wod.wf_order_line_no,
			     wod.item,
			     wod.source_loc_type,
			     wod.source_loc_id,
			     wod.customer_loc,
			     wod.need_date,
			     woh.approval_date,
			     woh.currency_code order_currency
			from future_cost fc,
			     cost_susp_sup_head cssh,
			     cost_susp_sup_detail cssd,
			     wf_order_head woh,
			     wf_order_detail wod
		       where cssh.active_date	   = LP_vdate + 1
			 and cssh.status	   = 'E'	 --pick the cost change that has been extracted('E')
			 and cssd.cost_change	   = cssh.cost_change
			 and cssd.recalc_ord_ind   = 'Y'
			 and fc.cost_change	   = cssh.cost_change
			 and cssd.item in (fc.item, fc.item_parent, fc.item_grandparent)
			 and fc.supplier	   = cssd.supplier
			 and fc.origin_country_id  = cssd.origin_country_id
			 and fc.active_date	   = cssh.active_date
			 and wod.item		   = fc.item
			 and wod.customer_loc	   = fc.location
			 and wod.fixed_cost is NULL	     --The records with fixed cost should not be updated
			 and wod.wf_order_no	   = woh.wf_order_no
			 and woh.status 	   = 'A'
			 and ((wod.source_loc_type = 'SU' and exists(select 'x'
								       from item_supp_country isc
								      where isc.item		    =  fc.item
									and isc.supplier	    =  fc.supplier
									and isc.origin_country_id   =  fc.origin_country_id
									and isc.supplier	    =  wod.source_loc_id
									and isc.primary_country_ind = 'Y'))
			      or (wod.source_loc_type <> 'SU' and fc.primary_supp_country_ind = 'Y'))
		      UNION ALL
		      select fc.dept,
			     fc.class,
			     fc.subclass,
			     fc.acquisition_cost,
			     fc.pricing_cost,
			     fc.currency_code future_cost_curr,
			     wod.wf_order_no,
			     wod.wf_order_line_no,
			     wod.item,
			     wod.source_loc_type,
			     wod.source_loc_id,
			     wod.customer_loc,
			     wod.need_date,
			     woh.approval_date,
			     woh.currency_code order_currency
			from future_cost fc,
			     cost_susp_sup_head cssh,
			     cost_susp_sup_detail_loc cssdl,
			     wf_order_head woh,
			     wf_order_detail wod
		       where cssh.active_date	   = LP_vdate + 1
			 and cssh.status	   = 'E'	 --pick the cost change that has been extracted('E')
			 and cssdl.cost_change	   = cssh.cost_change
			 and cssdl.recalc_ord_ind  = 'Y'
			 and fc.cost_change	   = cssh.cost_change
			 and fc.costing_loc	   = cssdl.loc
			 and cssdl.item in (fc.item, fc.item_parent, fc.item_grandparent)
			 and fc.supplier	   = cssdl.supplier
			 and fc.origin_country_id  = cssdl.origin_country_id
			 and fc.active_date	   = cssh.active_date
			 and wod.item		   = fc.item
			 and wod.customer_loc	   = fc.location
			 and wod.fixed_cost is NULL	     --The records with fixed cost should not be updated
			 and wod.wf_order_no	   = woh.wf_order_no
			 and woh.status 	   = 'A'
			 and ((wod.source_loc_type = 'SU' and exists(select 'x'
								       from item_supp_country isc
								      where isc.item		    =  fc.item
									and isc.supplier	    =  fc.supplier
									and isc.origin_country_id   =  fc.origin_country_id
									and isc.supplier	    =  wod.source_loc_id
									and isc.primary_country_ind = 'Y'))
			      or (wod.source_loc_type <> 'SU' and fc.primary_supp_country_ind = 'Y'))) fc_cost,
		     (select wcr.dept,
			     wcr.class,
			     wcr.subclass,
			     wcr.location,
			     wcr.item,
			     wcr.templ_id,
			     wcth.templ_desc,
			     wcth.first_applied,
			     wcth.margin_pct
			from wf_cost_relationship wcr,
			     wf_cost_buildup_tmpl_head wcth
		       where wcr.start_date <= LP_vdate
			 and wcr.end_date   >= LP_vdate
			 and wcr.templ_id   =  wcth.templ_id
			 and wcth.first_applied not in ('R', 'C')) templ_desc,
		     mv_currency_conversion_rates mcr
	       where fc_cost.dept	   = templ_desc.dept(+)
		 and fc_cost.class	   = templ_desc.class(+)
		 and fc_cost.subclass	   = templ_desc.subclass(+)
		 and fc_cost.customer_loc  = templ_desc.location(+)
		 and NVL(templ_desc.item, '-1') = '-1'
		 and mcr.from_currency	   = NVL(fc_cost.future_cost_curr, fc_cost.order_currency)
		 and mcr.to_currency	   = fc_cost.order_currency
		 and mcr.exchange_type	   = decode(L_consolidation_ind, 'Y', 'C', 'O')
		 and mcr.effective_date    <= NVL(fc_cost.approval_date, LP_vdate)) inner
	where inner.rown = 1;

   cursor C_LOCK_WF_ORDER_DETAIL is
      select 'x'
	from wf_order_detail wod,
	     TABLE(CAST(L_f_ord_apply_cc_tbl as OBJ_F_ORD_APPLY_CC_TBL)) cc_tbl
       where wod.wf_order_no	  =  cc_tbl.wf_order_no
	 and wod.item		  =  cc_tbl.item
	 and wod.source_loc_type  =  cc_tbl.source_loc_type
	 and wod.source_loc_id	  =  cc_tbl.source_loc_id
	 and wod.customer_loc	  =  cc_tbl.customer_loc
	 for update of wod.acquisition_cost nowait;

   cursor C_LOCK_WF_ORDER_EXP is
      select woe.rowid
	from TABLE(CAST(L_f_ord_apply_cc_tbl as OBJ_F_ORD_APPLY_CC_TBL)) cc_tbl,
	     wf_order_exp woe
       where woe.wf_order_no	  =  cc_tbl.wf_order_no
	 and woe.wf_order_line_no =  cc_tbl.wf_order_line_no
	 for update of woe.wf_order_no nowait;

BEGIN
   if SYSTEM_OPTIONS_SQL.CONSOLIDATION_IND (O_error_message,
					    L_consolidation_ind) = FALSE then
      return FALSE;
   end if;

   L_table := 'WF_ORDER_DETAIL, '||'WF_ORDER_EXP';

   SQL_LIB.SET_MARK('OPEN', 'C_GET_FRAN_ORD_TO_UPDATE', L_table, NULL);
   open C_GET_FRAN_ORD_TO_UPDATE;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_FRAN_ORD_TO_UPDATE', L_table, NULL);
   fetch C_GET_FRAN_ORD_TO_UPDATE BULK COLLECT INTO L_f_ord_apply_cc_tbl;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_FRAN_ORD_TO_UPDATE', L_table, NULL);
   close C_GET_FRAN_ORD_TO_UPDATE;

   if L_f_ord_apply_cc_tbl is NOT NULL and L_f_ord_apply_cc_tbl.COUNT > 0 then

      L_table := 'WF_ORDER_DETAIL';

      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_DETAIL', L_table, NULL);
      open C_LOCK_WF_ORDER_DETAIL;

      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_DETAIL', L_table, NULL);
      close C_LOCK_WF_ORDER_DETAIL;

      SQL_LIB.SET_MARK('UPDATE', NULL, L_table, NULL);
      FORALL i in L_f_ord_apply_cc_tbl.FIRST..L_f_ord_apply_cc_tbl.LAST
	 update wf_order_detail
	    set customer_cost	     =	  L_f_ord_apply_cc_tbl(i).pricing_cost,
		acquisition_cost     =	  L_f_ord_apply_cc_tbl(i).acquisition_cost,
		templ_id	     =	  L_f_ord_apply_cc_tbl(i).templ_id,
		templ_desc	     =	  L_f_ord_apply_cc_tbl(i).templ_desc,
		calc_type	     =	  L_f_ord_apply_cc_tbl(i).first_applied,
		margin_pct	     =	  L_f_ord_apply_cc_tbl(i).margin_pct,
		last_update_datetime =	  sysdate,
		last_update_id	     =	  get_user
	  where wf_order_no	     =	  L_f_ord_apply_cc_tbl(i).wf_order_no
	    and item		     =	  L_f_ord_apply_cc_tbl(i).item
	    and source_loc_type      =	  L_f_ord_apply_cc_tbl(i).source_loc_type
	    and source_loc_id	     =	  L_f_ord_apply_cc_tbl(i).source_loc_id
	    and customer_loc	     =	  L_f_ord_apply_cc_tbl(i).customer_loc;

      L_table := 'WF_ORDER_EXP';

      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_EXP', L_table, NULL);
      open C_LOCK_WF_ORDER_EXP;

      SQL_LIB.SET_MARK('FETCH', 'C_LOCK_WF_ORDER_EXP', L_table, NULL);
      fetch C_LOCK_WF_ORDER_EXP BULK COLLECT INTO L_woe_rowid_TBL;

      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_EXP', L_table, NULL);
      close C_LOCK_WF_ORDER_EXP;

      --Delete the expenses from wf_order_exp
      if L_woe_rowid_TBL is NOT NULL and L_woe_rowid_TBL.COUNT > 0 then
	 SQL_LIB.SET_MARK('DELETE', NULL, L_table, NULL);
	 FORALL i in L_woe_rowid_TBL.FIRST..L_woe_rowid_TBL.LAST
	    delete from wf_order_exp
	     where rowid = L_woe_rowid_TBL(i);
      end if;

      --The expenses should be recalculated to account for the change in Cost template.
      --Call get_upcharge_information to re-insert the expenses in wf_order_exp
      FOR i in L_f_ord_apply_cc_tbl.FIRST..L_f_ord_apply_cc_tbl.LAST LOOP
	 if WF_ORDER_SQL.GET_UPCHARGE_INFORMATION(O_error_message,
						  L_f_ord_apply_cc_tbl(i).wf_order_no,
						  L_f_ord_apply_cc_tbl(i).order_currency,
						  L_f_ord_apply_cc_tbl(i).item,
						  L_f_ord_apply_cc_tbl(i).source_loc_type,
						  L_f_ord_apply_cc_tbl(i).source_loc_id,
						  L_f_ord_apply_cc_tbl(i).customer_loc) = FALSE then
	    return FALSE;
	 end if;
      END LOOP;
   end if;  --end input collection NULL

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('BULK_TABLE_LOCK',
					     L_table,
					     NULL,
					     NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     to_char(SQLCODE));
      return FALSE;
END APPLY_SUPPLIER_COST_CHANGE;
-----------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DEP_ITEM_COSTING_LOC(O_error_message	   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
				    I_wf_order_no	   IN	    WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program		    VARCHAR2(64) := 'WF_ORDER_SQL.CHECK_DEP_ITEM_COSTING_LOC';
   L_item		    ITEM_MASTER.ITEM%TYPE;
   L_container_item	    ITEM_MASTER.ITEM%TYPE;
   L_customer_loc	    WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;

   cursor C_CHECK_COSTING_LOC is
      select wod.item,
	     im.container_item,
	     wod.customer_loc
	from item_master im,
	     item_loc il1,
	     item_loc il2,
	     wf_order_detail wod
       where wod.wf_order_no		   = I_wf_order_no
	 and wod.source_loc_type	   = 'SU'
	 and wod.item			   = im.item
	 and NVL(im.deposit_item_type,'x') = 'E'
	 and il1.item			   = wod.item
	 and il1.loc			   = wod.customer_loc
	 and il1.loc			   = il2.loc
	 and il2.item			   = im.container_item
	 and il1.costing_loc		  <> il2.costing_loc
	 and rownum = 1;


BEGIN
   open C_CHECK_COSTING_LOC;
   fetch C_CHECK_COSTING_LOC INTO L_item, L_container_item, L_customer_loc;
   close C_CHECK_COSTING_LOC;

   if L_item is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('WF_DEP_COST_LOC_DIFF',L_item,L_container_item,L_customer_loc);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					     SQLERRM,
					     L_program,
					     to_char(SQLCODE));
      return FALSE;
END CHECK_DEP_ITEM_COSTING_LOC;
-----------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
			O_valid_itemloc   IN OUT   BOOLEAN,
			I_wf_order_no	  IN	   WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program	     VARCHAR2(50) := 'WF_ORDER_SQL.CHECK_ITEM_LOC';
   L_exists	     VARCHAR2(1);
   L_wf_ord_exists   BOOLEAN;

   -- Query to check if there is atleast one item in the order that is not ranged to the customer location
   cursor C_CHECK_ITEM_LOC is
      select 'X'
	from wf_order_head woh,
	     wf_order_detail wod
       where woh.wf_order_no = wod.wf_order_no
	 and wod.wf_order_no = I_wf_order_no
	 and woh.order_type in ('M', 'E')
	 and not exists (select 1
			   from item_loc il
			  where il.loc	    = wod.customer_loc
			    and il.loc_type = 'S'
			    and il.item     = wod.item);

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
					    'I_wf_order_no',
					    L_program,
					    NULL);
      return FALSE;
   end if;

   if WF_ORDER_SQL.WF_ORDER_EXISTS(O_error_message,
				   L_wf_ord_exists,
				   I_wf_order_no) = FALSE then
      return FALSE;
   end if;

   if L_wf_ord_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',
					    I_wf_order_no,
					    NULL,
					    NULL);
      return FALSE;
   end if;

   open C_CHECK_ITEM_LOC;
   fetch C_CHECK_ITEM_LOC into L_exists;
   close C_CHECK_ITEM_LOC;

   if L_exists is NOT NULL then
      -- If cursor fetches 'X', there are invalid item-location combinations. So, return FALSE
      O_valid_itemloc := FALSE;
   else
      O_valid_itemloc := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    to_char(SQLCODE));
      return FALSE;

END CHECK_ITEM_LOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
			 I_wf_order_no	   IN	    WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program	  VARCHAR2(50) := 'WF_ORDER_SQL.CREATE_ITEM_LOC';

   -- Identify the items in the franchise order that are not already ranged to the customer location.
   cursor C_ITEMLOC_TO_RANGE is
      select wod.item		 item,
	     wod.customer_loc	 customer_loc
	from wf_order_detail wod,
	     wf_order_head   woh
       where woh.wf_order_no = I_wf_order_no
	 and woh.wf_order_no = wod.wf_order_no
	 and woh.order_type in ('M', 'E')
	 and not exists (select 1
			   from item_loc il
			  where il.loc	    = wod.customer_loc
			    and il.loc_type = 'S'
			    and il.item     = wod.item);

   TYPE ITEMLOC_RANGE_DATA is TABLE of C_ITEMLOC_TO_RANGE%ROWTYPE INDEX BY BINARY_INTEGER;
   L_itemloc_tbl   ITEMLOC_RANGE_DATA;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
					    'I_wf_order_no',
					    L_program,
					    NULL);
      return FALSE;
   end if;

   open C_ITEMLOC_TO_RANGE;
   fetch C_ITEMLOC_TO_RANGE BULK COLLECT into L_itemloc_tbl;
   close C_ITEMLOC_TO_RANGE;

   FOR i in 1..L_itemloc_tbl.COUNT LOOP
      -- Create new item location combinations for the identified items.
      if NEW_ITEM_LOC(O_error_message,
		      L_itemloc_tbl(i).item,
		      L_itemloc_tbl(i).customer_loc,
		      NULL,
		      NULL,
		      'S',
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      NULL,
		      'N',
		      NULL,
		      NULL,
		      NULL,
		      'Y') = FALSE then
	 return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    to_char(SQLCODE));
      return FALSE;

END CREATE_ITEM_LOC;
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_INVALID_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
				I_wf_order_no	  IN	   WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'WF_ORDER_SQL.DELETE_INVALID_ITEMLOC';

   -- Query fetches items in a order that are not ranged to the customer location.
   cursor C_GET_INVALID_ITEM is
      select wod.wf_order_line_no wf_order_line_no
	from wf_order_detail wod,
	     wf_order_head   woh,
	     wf_order_exp    woe
       where woh.wf_order_no = I_wf_order_no
	 and woh.wf_order_no = wod.wf_order_no
	 and woh.order_type in ('M', 'E')
	 and wod.wf_order_no = woe.wf_order_no (+)
	 and wod.wf_order_line_no = woe.wf_order_line_no (+)
	 and not exists (select 1
			   from item_loc il
			  where il.loc	    = wod.customer_loc
			    and il.loc_type = 'S'
			    and il.item     = wod.item)
	 for update of wod.wf_order_no, woe.wf_order_no nowait;

   TYPE itemloc_tbl is TABLE of C_GET_INVALID_ITEM%ROWTYPE   INDEX BY BINARY_INTEGER;

   L_itemloc_tbl   itemloc_tbl;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA	   EXCEPTION_INIT(Record_Locked,-54);
   L_table	   VARCHAR2(30) := 'WF_ORDER_DETAIL, WF_ORDER_EXP';

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
					    'I_wf_order_no',
					    L_program,
					    NULL);
      return FALSE;
   end if;

   open C_GET_INVALID_ITEM;
   fetch C_GET_INVALID_ITEM BULK COLLECT into L_itemloc_tbl;
   close C_GET_INVALID_ITEM;

   if L_itemloc_tbl is NOT NULL and L_itemloc_tbl.COUNT > 0 then
      FORALL i in 1..L_itemloc_tbl.COUNT
	 delete from wf_order_exp
	  where wf_order_no = I_wf_order_no
	    and wf_order_line_no = L_itemloc_tbl(i).wf_order_line_no;

      FORALL i in 1..L_itemloc_tbl.COUNT
	 delete from wf_order_detail
	  where wf_order_no = I_wf_order_no
	    and wf_order_line_no = L_itemloc_tbl(i).wf_order_line_no;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
					    L_table,
					    I_WF_ORDER_NO,
					    L_program);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
					    SQLERRM,
					    L_program,
					    to_char(SQLCODE));
      return FALSE;

END DELETE_INVALID_ITEMLOC;
------------------------------------------------------------------------------------------------
END WF_ORDER_SQL;
/