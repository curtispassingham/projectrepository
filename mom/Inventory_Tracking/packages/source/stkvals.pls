
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STKCNT_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--- Function Name:  STAKE_DATE_EXISTS
--- Purpose:        Checks if Stocktake Date is on stake_head.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION STAKE_DATE_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exist          IN OUT  BOOLEAN,
                           I_stake_date     IN      STAKE_HEAD.STOCKTAKE_DATE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  ITEM_LOC_EXISTS
--- Purpose:        Checks if cycle_count is on stake_sku_loc.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_ITEM_MASTER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION ITEM_LOC_EXISTS(I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                         I_count_type     IN      CODE_DETAIL.CODE%TYPE,
                         O_exists         IN OUT  BOOLEAN,
                         O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  ITEM_PROD_EXISTS
--- Purpose:        Checks if cycle_count is on stake_sku_loc or stake_product.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_ITEM_MASTER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION ITEM_PROD_EXISTS(I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE, 
                          O_exists         IN OUT  BOOLEAN,
                          O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  ITEM_PROD_EXISTS
--- Purpose:        Checks if cycle_count is on stake_prod_loc.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION PROD_LOC_EXISTS(I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                         O_exist          IN OUT  BOOLEAN,
                         O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  ITEM_QTY_EXISTS
--- Purpose:        Checks if cycle_count is on stake_qty.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_ITEM_MASTER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION ITEM_QTY_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_new            IN OUT  BOOLEAN,
                         O_exist          IN OUT  BOOLEAN,
                         I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  VALID_LOC
--- Purpose:        Checks if location exists on a specific cycle_count
---                 based on table stake_location and the cycle_count
---                 is scheduled to take place today (VDATE) or later.
-------------------------------------------------------------------------------
FUNCTION VALID_LOC(O_error_message  IN OUT  VARCHAR2,
                   O_valid          IN OUT  BOOLEAN,
                   I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                   I_location       IN      STAKE_LOCATION.LOCATION%TYPE,
                   I_loc_type       IN      STAKE_HEAD.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  UNIT_AND_DOLLAR_SKU
--- Purpose:        Checks if an Item's merchandise hierarchy matches
---                 the dept/class/subclass combination on a specified
---                 unit and dollar count based on table stake_product.
-------------------------------------------------------------------------------
FUNCTION UNIT_AND_DOLLAR_ITEM(O_error_message  IN OUT  VARCHAR2,
                              O_valid          IN OUT  BOOLEAN,
                              I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                              I_item           IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  VALID_STK_COUNT_VHWDIST
--- Purpose:        Checks, whether the passed in stock count is valid for VHWDIST. 

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION VALID_STK_COUNT_VWHDIST(O_error_message  IN OUT  VARCHAR2,
                                 O_exist          IN OUT  BOOLEAN,
                                 I_cycle_count    IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
                                 I_pwh            IN      WH.PHYSICAL_WH%TYPE default NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  PARTIAL_LOCATIONS
--- Purpose:        Checks if locations displayed for Cycle Count are partial view.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION PARTIAL_LOCATIONS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_partial        IN OUT  BOOLEAN,
                           I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  PARTIAL_DEPS
--- Purpose:        Checks if Dept/Class/Subclass displayed for Cycle Count are partial view.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION PARTIAL_DEPS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_partial        IN OUT  BOOLEAN,
                      I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                      I_loc_type       IN      STAKE_LOCATION.LOC_TYPE%TYPE,
                      I_location       IN      STAKE_LOCATION.LOCATION%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  STORE_COUNT_QTY_EXISTS
--- Purpose:        Checks, whether count quantity has been entered for the store. 
--- Calls:          None
--- Input Values:   I_cycle_count, I_store
--- Return Values:  O_error_message, O_exists
---------------------------------------------------------------------
   FUNCTION STORE_COUNT_QTY_EXISTS
            (O_error_message  IN OUT  VARCHAR2,
             O_exist          IN OUT  BOOLEAN,
             I_cycle_count    IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
             I_store          IN      STORE.STORE%TYPE default NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------
--- Function Name:  WH_COUNT_QTY_EXISTS
--- Purpose:        Checks, whether count quantity has been entered for the warehouse. 
--- Calls:          None
--- Input Values:   I_cycle_count, I_wh
--- Return Values:  O_error_message, O_exists
---------------------------------------------------------------------
   FUNCTION WH_COUNT_QTY_EXISTS
            (O_error_message  IN OUT  VARCHAR2,
             O_exist          IN OUT  BOOLEAN,
             I_cycle_count    IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
             I_wh             IN      WH.WH%TYPE default NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------
--- Function Name:  EXT_FINISHER_COUNT_QTY_EXISTS
--- Purpose:        Checks, whether count quantity has been entered for the external finisher. 
--- Calls:          None
--- Input Values:   I_cycle_count, I_external_finisher
--- Return Values:  O_error_message, O_exists
---------------------------------------------------------------------      
   FUNCTION EXT_FINISHER_COUNT_QTY_EXISTS
            (O_error_message     IN OUT  VARCHAR2,
             O_exist             IN OUT  BOOLEAN,
             I_cycle_count       IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE,
             I_external_finisher IN      STAKE_SKU_LOC.LOCATION%TYPE default NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------
--- Function Name:  VALIDATE_CYCLE_COUNT
--- Purpose:        This function is called from stkvarnc.fmb to validate
---                 the entered cycle count and fetch appropriate values.
---------------------------------------------------------------------
   FUNCTION VALIDATE_CYCLE_COUNT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_stocktake_date   IN OUT  STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                                 O_stocktake_type   IN OUT  STAKE_HEAD.STOCKTAKE_TYPE%TYPE,
                                 O_cycle_count_desc IN OUT  STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                                 O_cycle_recs_exist IN OUT  BOOLEAN,
                                 O_exists           IN OUT  BOOLEAN,
                                 I_cycle_count      IN      STAKE_SKU_LOC.CYCLE_COUNT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------
--- Function Name:  PARTIAL_PRODUCT_DEPS
--- Purpose:        Checks if Dept/Class/Subclass displayed for requested Cycle Count 
---                 Product Group is partial view.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION PARTIAL_PRODUCT_DEPS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_partial        IN OUT  BOOLEAN,
                              I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  STOCK_COUNT_LOC_EXISTS
--- Purpose:        Checks if location exists on a specific cycle_count
---                 based on table stake_location .
-------------------------------------------------------------------------------
FUNCTION STOCK_COUNT_LOC_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid          IN OUT  BOOLEAN,
                                I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                                I_location       IN      STAKE_LOCATION.LOCATION%TYPE,
                                I_loc_type       IN      STAKE_HEAD.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- Function Name:  STAKE_QTY_LOC_EXISTS
---       Purpose:  Check to see whether all the locations have been counted
---                 for the specific stock count.

FUNCTION STAKE_QTY_LOC_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist          IN OUT  BOOLEAN,
                              I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--- Function Name:  PARTIAL_STAKE_SCHEDULE
--- Purpose:        Checks if skulists displayed for stock schedule are partial view
---                 when stockstake_type is 'U'. Checks if dept/class/subclass for
---                 stock schedule are partial view when stockstake_type is 'B'.
-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--   V_SKULIST_HEAD V_DEPS V_CLASS V_SUBCLASS
-- which only returns data that the user has permission to access.  
------------------------------------------------------------------------------------
FUNCTION PARTIAL_STAKE_SCHEDULE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_partial           IN OUT   BOOLEAN,
                                I_location          IN       STAKE_SCHEDULE.LOCATION%TYPE,
                                I_loc_type          IN       STAKE_SCHEDULE.LOC_TYPE%TYPE,
                                I_stocktake_type    IN       STAKE_SCHEDULE.STOCKTAKE_TYPE%TYPE)    
RETURN BOOLEAN;                                       
-------------------------------------------------------------------------------------
END STKCNT_VALIDATE_SQL;
/
