CREATE OR REPLACE PACKAGE REF_RILU_UPDATES_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name:   UPDATE_NULL_LOC
--Purpose      :   Inserts location values for all items in
--                 REPL_ITEM_LOC_UPDATES table which have NULL (or -1) location
--                 values.
--------------------------------------------------------------------------------
FUNCTION UPDATE_NULL_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   DEL_NULL_LOCS
--Purpose      :   Delete all records with NULL (or -1) location values in
--                 REPL_ITEM_LOC_UPDATES table.
--------------------------------------------------------------------------------
FUNCTION DEL_NULL_LOCS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
END REF_RILU_UPDATES_SQL;
/