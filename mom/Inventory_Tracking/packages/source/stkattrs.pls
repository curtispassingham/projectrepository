CREATE OR REPLACE PACKAGE STKCNT_ATTRIB_SQL AUTHID CURRENT_USER AS
 
--------------------------------------------------------------------
--- Procedure Name: GET_DESC
--- Purpose:        Looks up description for cycle_count from stake_head table.
--- Calls:          None
--- Input Values:   I_stake
--- Return Values:  O_desc, O_error_message
--- Authored By:    Chad Whipple
---------------------------------------------------------------------
   FUNCTION GET_DESC(I_stake         IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                     O_desc          IN OUT  STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                     O_error_message IN OUT  VARCHAR2)
   RETURN BOOLEAN;

--------------------------------------------------------------------
--- Procedure Name: GET_TYPE
--- Purpose:        Looks up type of cycle_count from stake_head table.
--- Calls:          None
--- Input Values:   I_stake
--- Return Values:  O_type, O_error_message
--- Authored By:    Chad Whipple
---------------------------------------------------------------------
   FUNCTION GET_TYPE(I_stake         IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                     O_type          IN OUT  STAKE_HEAD.STOCKTAKE_TYPE%TYPE,
                     O_error_message IN OUT  VARCHAR2)
   RETURN BOOLEAN;

--------------------------------------------------------------------
--- Function Name:  PROD_LOC_PROCESSED
--- Purpose:        Checks if cycle_count is on stake_sku_loc.
--- Calls:          None
--- Input Values:   I_stake
--- Return Values:  O_proc, O_error_message
---------------------------------------------------------------------
   FUNCTION PROD_LOC_PROCESSED(I_stake          IN      STAKE_HEAD.CYCLE_COUNT%TYPE,
                               I_location       IN      STAKE_PROD_LOC.LOCATION%TYPE,
                               O_proc           IN OUT  STAKE_PROD_LOC.PROCESSED%TYPE,
                               O_error_message  IN OUT  VARCHAR2)
   RETURN BOOLEAN;

--------------------------------------------------------------------
--- Procedure Name: GET_INFO
--- Purpose:        Looks up all field information from stake_head table.
--- Calls:          None
--- Input Values:   I_stake
--- Return Values:  All other fields except cycle_count, O_error_message
---------------------------------------------------------------------
   FUNCTION GET_INFO(O_error_message     IN OUT VARCHAR2,
                     O_stake_desc        IN OUT STAKE_HEAD.CYCLE_COUNT_DESC%TYPE,
                     O_loc_type          IN OUT STAKE_HEAD.LOC_TYPE%TYPE,
                     O_stocktake_date    IN OUT STAKE_HEAD.STOCKTAKE_DATE%TYPE,
                     O_stocktake_type    IN OUT STAKE_HEAD.STOCKTAKE_TYPE%TYPE,
                     O_product_level_ind IN OUT STAKE_HEAD.PRODUCT_LEVEL_IND%TYPE,
                     O_delete_ind        IN OUT STAKE_HEAD.DELETE_IND%TYPE,
                     I_stake             IN     STAKE_HEAD.CYCLE_COUNT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
--- Procedure Name: STOCK_COUNT_PROCESSED
--- Purpose:        Determines if stock count has been processed.
--- Calls:          None
---------------------------------------------------------------------
   FUNCTION STOCK_COUNT_PROCESSED(O_error_message           IN OUT VARCHAR2,
                                  O_stock_count_processed   IN OUT BOOLEAN,
                                  O_cycle_count             IN OUT STAKE_HEAD.CYCLE_COUNT%TYPE,
                                  I_transaction_date        IN     DATE,
                                  I_item                    IN     ITEM_MASTER.ITEM%TYPE,
                                  I_loc_type                IN     STAKE_SKU_LOC.LOC_TYPE%TYPE,
                                  I_location                IN     STAKE_SKU_LOC.LOCATION%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------
--- Procedure Name: STOCK_COUNT_PROCESSED  (OVERLOADED FUNCTION)
--- Purpose:        Determines if stock count has been processed and
---                 returns shapshot_unit_cost and snapshot_unit_retail.
--- Calls:          None
---------------------------------------------------------------------
FUNCTION STOCK_COUNT_PROCESSED(O_error_message           IN OUT  VARCHAR2,
                               O_stock_count_processed   IN OUT  BOOLEAN,
                               O_cycle_count             IN OUT  STAKE_HEAD.CYCLE_COUNT%TYPE,
                               O_snapshot_unit_cost      IN OUT  STAKE_SKU_LOC.SNAPSHOT_UNIT_COST%TYPE,
                               O_snapshot_unit_retail    IN OUT  STAKE_SKU_LOC.SNAPSHOT_UNIT_RETAIL%TYPE,
                               I_transaction_date        IN      DATE,
                               I_item                    IN      ITEM_MASTER.ITEM%TYPE,
                               I_loc_type                IN      STAKE_SKU_LOC.LOC_TYPE%TYPE,
                               I_location                IN      STAKE_SKU_LOC.LOCATION%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
END STKCNT_ATTRIB_SQL;
/
