/******************************************************************************
* Service Name     : InventoryDetailService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/InventoryDetailService/v1
* Description      :
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE BODY InventoryDetailServiceProvider AS


/******************************************************************************
 *
 * Operation       : lookupInvAvailCriVo
 * Description     : Lookup available inventory for customer orderable items for all specified item at all specified customer orderable locations.
                        Quantities will be returned in standard unit of measure.
 *
 * Input           : "RIB_InvAvailCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvAvailCriVo/v1
 * Description     :  Inventory detail lookup criteria contains a collections of item and location for which inventory details has to be fetched.
 *
 * Output          : "RIB_InvAvailColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvAvailColDesc/v1
 * Description     :  InvAvailColDesc contains collection of item, location along with the inventory details.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
                    message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
                    "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.

 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE lookupInvAvailCriVo(I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                              I_businessObject          IN     "RIB_InvAvailCriVo_REC",
                              O_serviceOperationStatus  OUT    "RIB_ServiceOpStatus_REC",
                              O_businessObject          OUT    "RIB_InvAvailColDesc_REC")
IS
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status                  "RIB_SuccessStatus_REC"     := NULL;
   L_status_code             VARCHAR2(1)                 := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL"     := "RIB_SuccessStatus_TBL"();
   L_program                 VARCHAR2(100)               := 'InventoryDetailServiceProvider.lookupInvAvailCriVo';
  
BEGIN
   -- Validate the input service operation context and initialize the output service operation object.
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
         
   SVCPROV_INVAVAIL.GET_INV_DETAIL(O_serviceOperationStatus,
                                   O_businessObject,
                                   I_businessObject);                                   
     
   -- For any error in the call to SVCPROV_INVAVAIL, the FailStatus_TBL will be populated.
   -- If the FailStatus_TBL is null, populate output service object with success message.
   
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.COUNT = 0 then
      L_status := "RIB_SuccessStatus_REC"(0,'lookupInvAvailCriVo service call was successful.');
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   end if;
   
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);                                       
END lookupInvAvailCriVo;
/******************************************************************************/

END InventoryDetailServiceProvider;
/
