CREATE OR REPLACE PACKAGE SVCPROV_INVBACKORD AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
--------------------------------------------------------------------------------
-- Function Name: CREATE_BACKORDER
-- Purpose: This procedure will be called from web service implementation package 
--          for backorder. This package will load the inv backorder message into 
--          staging table and call the coresvc_invbackord package to process the message.
--          This will also cleanup the staging after successfull processing. 
--------------------------------------------------------------------------------
PROCEDURE CREATE_BACKORDER(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                           I_businessObject         IN     "RIB_InvBackOrdColDesc_REC");
--------------------------------------------------------------------------------
END SVCPROV_INVBACKORD;
/