CREATE OR REPLACE PACKAGE WF_CUSTOMER_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: WF_CUSTOMER_GROUP_ID_EXISTS
-- Purpose      : This FUNCTION will check if the given w/f customer group id exist IN WF_CUSTOMER_GROUP table.
--------------------------------------------------------------------------------------------------------------
FUNCTION WF_CUSTOMER_GROUP_ID_EXISTS (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists             IN OUT   BOOLEAN,
                                      I_wf_cust_group_id   IN       WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: WF_CUSTOMER_GROUP_INFO
-- Purpose      : This FUNCTION will get WF_CUSTOMER_GROUP_NAME from WF_CUSTOMER_GROUP table.
--------------------------------------------------------------------------------------------------------------
FUNCTION GET_WF_CUSTOMER_GROUP_INFO (O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_wf_customer_group_name   IN OUT   WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_NAME%TYPE,
                                     I_wf_customer_group_id     IN       WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: DELETE_WF_CUSTOMER_GROUP
-- Purpose      : This FUNCTION allows FOR the deletion of customer groups that have no customer locations
--                associated to them.
--------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_WF_CUSTOMER_GROUP (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_delete_group       IN OUT   VARCHAR2,
                                   I_wf_cust_group_id   IN       WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: WF_CUSTOMER_ID_EXISTS
-- Purpose      : This FUNCTION checks if the customer id exists IN WF_CUSTOMER table.
--------------------------------------------------------------------------------------------------------------
FUNCTION WF_CUSTOMER_ID_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT   BOOLEAN,
                                I_wf_customer_id   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: DELETE_WF_CUSTOMER
-- Purpose      : This FUNCTION allows FOR the deletion of customer records that no longer have locatios
--                associated to them IN the STORE table.
--------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_WF_CUSTOMER (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_delete_cust      IN OUT   VARCHAR2,
                             I_wf_customer_id   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: GET_WF_CUSTOMER_INFO
-- Purpose      : This FUNCTION retrives the entire row of the WF_CUSTOMER table given the customer id.
--------------------------------------------------------------------------------------------------------------
FUNCTION GET_WF_CUSTOMER_INFO (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_wf_customer      IN OUT   WF_CUSTOMER%ROWTYPE,
                               I_wf_customer_id   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: CHECK_WF_CUST_CREDIT
-- Purpose      : This FUNCTION retrives the credit indicator for the given customer id.
--------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_CUST_CREDIT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_credit_ind      IN OUT   WF_CUSTOMER.CREDIT_IND%TYPE,
                               I_wf_customer_id  IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE)
   RETURN BOOLEAN;   
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: CHECK_WF_CUST_LOC_CREDIT
-- Purpose      : This FUNCTION retrives the credit indicator for the given store.
--------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_CUST_LOC_CREDIT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_credit_ind      IN OUT   WF_CUSTOMER.CREDIT_IND%TYPE,
                                   I_store           IN       STORE.STORE%TYPE)
   RETURN BOOLEAN;   
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: GET_WF_STORE_CUSTOMER_ID
-- Purpose      : This FUNCTION retrives the wf customer id for the given store.
--------------------------------------------------------------------------------------------------------------
FUNCTION GET_WF_STORE_CUSTOMER_ID (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_wf_customer_id      IN OUT   WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
                                   I_store               IN       STORE.STORE%TYPE)
   RETURN BOOLEAN;   
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: CHECK_WF_CUSTOMER_LOC
-- Purpose      : This FUNCTION verifies whether the given location is a customer or company location.
--------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_CUSTOMER_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cust_loc        IN OUT   VARCHAR2,
                                I_loc             IN       STORE.STORE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- FUNCTION Name: WF_CUST_LOC_EXISTS
-- Purpose      : This FUNCTION verifies whether the given customer and store location combination exists.
--------------------------------------------------------------------------------------------------------------
FUNCTION WF_CUST_LOC_EXISTS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_cust_loc_exists   IN OUT   BOOLEAN,
                             I_wf_customer_id    IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
                             I_store             IN       V_STORE.STORE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
END WF_CUSTOMER_SQL;
/
