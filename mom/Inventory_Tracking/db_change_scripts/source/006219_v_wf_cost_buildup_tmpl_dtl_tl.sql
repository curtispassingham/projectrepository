CREATE OR REPLACE FORCE VIEW V_WF_COST_BUILDUP_TMPL_DTL_TL (TEMPL_ID, COST_COMP_ID, DESCRIPTION, LANG ) AS
SELECT  b.templ_id,
        b.cost_comp_id,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  WF_COST_BUILDUP_TMPL_DETAIL b,
        WF_COST_BUILDUP_TMPL_DTL_TL tl
 WHERE  b.templ_id = tl.templ_id (+)
   AND  b.cost_comp_id = tl.cost_comp_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_WF_COST_BUILDUP_TMPL_DTL_TL is 'This is the translation view for base table WF_COST_BUILDUP_TMPL_DETAIL. This view fetches data in user langauge either from translation table WF_COST_BUILDUP_TMPL_DTL_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_WF_COST_BUILDUP_TMPL_DTL_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_WF_COST_BUILDUP_TMPL_DTL_TL.TEMPL_ID is 'Unique cost buildup template ID number associated'
/

COMMENT ON COLUMN V_WF_COST_BUILDUP_TMPL_DTL_TL.COST_COMP_ID is 'Contains a unique specified code representing the component.'
/

COMMENT ON COLUMN V_WF_COST_BUILDUP_TMPL_DTL_TL.DESCRIPTION is 'Contains the name or description of the Component.'
/

