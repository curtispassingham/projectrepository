--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       DELIVERY_SLOT_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE DELIVERY_SLOT_TL(
LANG NUMBER(6) NOT NULL,
DELIVERY_SLOT_ID VARCHAR2(15) NOT NULL,
DELIVERY_SLOT_DESC VARCHAR2(240) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE DELIVERY_SLOT_TL is 'This is the translation table for DELIVERY_SLOT table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN DELIVERY_SLOT_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN DELIVERY_SLOT_TL.DELIVERY_SLOT_ID is 'This column specifies when the store requested quantity is needed at the store. For example, it defines whether the stock is designated for AM or PM delivery.'
/

COMMENT ON COLUMN DELIVERY_SLOT_TL.DELIVERY_SLOT_DESC is 'Description of the delivery slot. This could contain the time element for the delivery schedule (i.e. Afternoon Slot 3 PM).'
/

COMMENT ON COLUMN DELIVERY_SLOT_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN DELIVERY_SLOT_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN DELIVERY_SLOT_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN DELIVERY_SLOT_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE DELIVERY_SLOT_TL ADD CONSTRAINT PK_DELIVERY_SLOT_TL PRIMARY KEY (
LANG,
DELIVERY_SLOT_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE DELIVERY_SLOT_TL
 ADD CONSTRAINT DST_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE DELIVERY_SLOT_TL ADD CONSTRAINT DST_DS_FK FOREIGN KEY (
DELIVERY_SLOT_ID
) REFERENCES DELIVERY_SLOT (
DELIVERY_SLOT_ID
)
/

