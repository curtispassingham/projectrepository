--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW CREATED:          V_WF_RETURN_DATA
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT CREATING VIEW 'V_WF_RETURN_DATA';
CREATE OR REPLACE FORCE VIEW V_WF_RETURN_DATA
  ("RMA_NO", 
   "ITEM", 
   "ITEM_DESC", 
   "CUSTOMER_LOC", 
   "VAT_REGION", 
   "WF_ORDER_ID", 
   "ORDER_QTY", 
   "CUSTOMER_COST", 
   "CUST_ORD_REF_NO", 
   "RETURN_UNIT_COST", 
   "RETURNED_QTY", 
   "FINAL_RETURN_UNIT_COST", 
   "RETURN_REASON", 
   "RETURN_REASON_DESC", 
   "CANCEL_REASON", 
   "UNIT_RESTOCK_FEE", 
   "RESTOCK_TYPE", 
   "RESTOCK_TYPE_DESC", 
   "ROW_ID", 
   "PRIMARY_CURRENCY", 
   "RETURN_CURRENCY",
   "EXCHANGE_RATE", 
   "TAX_RATE", 
   "VAT", 
   "FINAL_RETURN_TOTAL_COST", 
   "RET_UNIT_COST_INCL_VAT", 
   "FINAL_RET_UNIT_COST_INVAT", 
   "FINAL_RET_TOTAL_COST_INVAT", 
   "CUSTOMER_COST_PRIM", 
   "RETURN_UNIT_COST_PRIM", 
   "FINAL_RETURN_UNIT_COST_PRIM", 
   "VAT_PRIM", 
   "UNIT_RESTOCK_FEE_PRIM", 
   "FINAL_RETURN_TOTAL_COST_PRIM", 
   "RET_UNIT_COST_INCL_VAT_PRIM", 
   "FINAL_RET_UNIT_COST_INVAT_PRIM", 
   "FIN_RET_TOTAL_COST_INVAT_PRIM", 
   "PRIMARY_REF_ITEM",
   "CANCEL_REASON_DESC",
   "PRIMARY_REF_ITEM_DESC",
   "CUSTOMER_COST_IN_RETURN_CUR",
   "CUSTOMER_COST_IN_PRIMARY_CUR",
   "STANDARD_UOM",
   "DEPOSIT_ITEM_TYPE") AS 
  SELECT 
    qry.rma_no,
    qry.item ,
    qry.item_desc,
    qry.customer_loc,
    qry.vat_region,
    qry.wf_order_id,
    qry.order_qty,
    qry.customer_cost,
    qry.cust_ord_ref_no,
    qry.return_unit_cost,
    qry.returned_qty,
    qry.final_return_unit_cost,
    qry.return_reason,
    qry.return_reason_desc,
    qry.cancel_reason,
    qry.unit_restock_fee,
    qry.restock_type,
    qry.restock_type_desc,
    qry.row_id,
    qry.primary_currency,
    qry.return_currency,
    qry.exchange_rate,
    qry.tax_rate,
    ((qry.final_return_unit_cost       * (1+ qry.tax_rate/100)) - qry.final_return_unit_cost) vat,
    qry.final_return_unit_cost   * qry.returned_qty final_return_total_cost,
    (qry.return_unit_cost        * (1+ qry.tax_rate/100)) ret_unit_cost_incl_vat,
    (qry.final_return_unit_cost  * (1+ qry.tax_rate/100)) final_ret_unit_cost_invat,
    ((qry.final_return_unit_cost * (1+ qry.tax_rate/100)) * returned_qty) final_ret_total_cost_invat,
    qry.customer_cost            * qry.exchange_rate customer_cost_prim,
    qry.return_unit_cost         * qry.exchange_rate return_unit_cost_prim,
    qry.final_return_unit_cost   * qry.exchange_rate final_return_unit_cost_prim,
    ((qry.final_return_unit_cost * (1+ qry.tax_rate/100)) - qry.final_return_unit_cost) * qry.exchange_rate vat_prim,
    qry.unit_restock_fee         * qry.exchange_rate unit_restock_fee_prim,
    (qry.final_return_unit_cost  * qry.returned_qty * qry.exchange_rate) final_return_total_cost_prim,
    (qry.return_unit_cost        * (1+ qry.tax_rate/100) * qry.exchange_rate) ret_unit_cost_incl_vat_prim,
    (qry.final_return_unit_cost  * (1+ qry.tax_rate/100) * qry.exchange_rate) final_ret_unit_cost_invat_prim,
    ((qry.final_return_unit_cost * (1+ qry.tax_rate/100)) * qry.returned_qty * qry.exchange_rate) fin_ret_total_cost_invat_prim,
    (SELECT im.item
    FROM item_master im
    WHERE item_parent        = qry.item
    AND primary_ref_item_ind = 'Y'
    ) primary_ref_item,
    cancel_reason_desc,
    (SELECT im1.item_desc
    FROM item_master im1
    WHERE item_parent        = qry.item
    AND primary_ref_item_ind = 'Y'
    ) primary_ref_item_desc,
    qry.customer_cost_in_return_cur,
    qry.customer_cost_in_return_cur * qry.exchange_rate customer_cost_in_primary_cur,
    qry.standard_uom,
	deposit_item_type
  FROM
    (SELECT wf.*,
      mccr.*,
      nvl(WF_RETURN_SQL.GET_TAX_INFO(item,customer_loc,'S','R'),0) tax_rate,
      (SELECT customer_cost * inr.exchange_rate
         FROM (SELECT mc.*,
                      rank() over(partition by from_currency,to_currency,exchange_type ORDER BY effective_date DESC) rnk1
                FROM mv_currency_conversion_rates mc ) inr
        WHERE inr.from_currency  = wf.order_currency
          AND inr.to_currency    = wf.return_currency
          AND inr.exchange_type  = 'C'
          AND inr.effective_date <= get_vdate
          AND inr.rnk1           = 1) customer_cost_in_return_cur
    FROM
      (SELECT wrdt.rma_no rma_no,
        wrdt.item item,
        im.item_desc item_desc,
        wrh.customer_loc customer_loc,
        s.vat_region vat_region,
        wrdt.wf_order_no wf_order_id,
        SUM(wodt.requested_qty) order_qty,
        MAX(NVL(wodt.fixed_cost,NVL(wodt.customer_cost,0))) customer_cost,
        wohd.cust_ord_ref_no cust_ord_ref_no,
        wrdt.return_unit_cost return_unit_cost,
        wrdt.returned_qty returned_qty,
        wrdt.net_return_unit_cost final_return_unit_cost,
        wrdt.return_reason,
        vcd1.code_desc return_reason_desc,
        wrdt.cancel_reason cancel_reason,
        wrdt.unit_restock_fee unit_restock_fee,
        wrdt.restock_type restock_type,
        vcd.code_desc restock_type_desc,
        rowidtochar(wrdt.rowid) row_id,
        so.currency_code primary_currency,
        wrh.currency_code return_currency,
        wohd.currency_code order_currency,
        vcd2.code_desc cancel_reason_desc,
        im.standard_uom,
		NVL(im.deposit_item_type,'0') deposit_item_type
      FROM item_master im,
        wf_order_head wohd,
        wf_order_detail wodt,
        wf_return_detail wrdt,
        wf_return_head wrh,
        store s,
        system_config_options so,
        v_code_detail vcd,
        v_code_detail vcd1,
        v_code_detail vcd2
      WHERE wrdt.rma_no                  = wrh.rma_no
      AND wrdt.wf_order_no               = wodt.wf_order_no(+)
      AND wodt.wf_order_no               = wohd.wf_order_no(+)
      AND wrdt.item                      = wodt.item(+)
      AND wrdt.item                      = im.item
      AND s.store                        = wrh.customer_loc
      AND wrdt.restock_type              = vcd.code(+)
      AND vcd.code_type(+)               = 'WFRT'
      AND wrdt.return_reason             = vcd1.code
      AND vcd1.code_type                 = 'RTVR'
      AND wrdt.cancel_reason             = vcd2.code(+)
      AND vcd2.code_type(+)             = 'WFCR'
      GROUP BY wrdt.item,
        im.item_desc,
        wrh.customer_loc,
        s.vat_region,
        wrdt.wf_order_no,
        wohd.cust_ord_ref_no,
        wrdt.return_unit_cost,
        wrdt.returned_qty,
        wrdt.net_return_unit_cost,
        vcd.code_desc,
        wrdt.return_reason,
        wrdt.cancel_reason,
        wrdt.unit_restock_fee,
        vcd1.code_desc,
        wrdt.restock_type,
        rowidtochar(wrdt.rowid),
        so.currency_code,
        wrh.currency_code,
        wohd.currency_code,
        wrdt.rma_no,
        vcd2.code_desc,
        im.standard_uom,
		NVL(im.deposit_item_type,'0')
      ) wf,
      (SELECT mc.*,
        rank() over(partition BY from_currency,to_currency,exchange_type order by effective_date DESC) rnk
      FROM mv_currency_conversion_rates mc
      ) mccr
    WHERE mccr.from_currency = return_currency
    AND mccr.to_currency     = primary_currency
    AND mccr.exchange_type   = 'C'
    AND effective_date      <= get_vdate
    And Rnk                  = 1
    ) qry
    /
COMMENT ON TABLE V_WF_RETURN_DATA IS 'This view is used to display the franchise return information for the wfreturn screen.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RMA_NO IS 'Contains the return no.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.ITEM IS 'Contains return item id.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.ITEM_DESC IS 'Contains return item description.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.CUSTOMER_LOC IS 'Contains customer location of the return.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.VAT_REGION IS 'Contains vat region.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.WF_ORDER_ID IS 'Contains franchise order no against which item is being returned .'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.ORDER_QTY IS 'Contains franchise order quantity.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.CUSTOMER_COST IS 'Contains customer cost.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.CUST_ORD_REF_NO IS 'Contains reference number that would be provided by the franchisee for their tracking purposes.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RETURN_UNIT_COST IS 'Contains Cost at which the item is being returned.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RETURNED_QTY IS 'Contains Quantity of the item that is being returned.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.FINAL_RETURN_UNIT_COST IS 'Contains the difference between RETURN_UNIT_COST and UNIT_RESTOCK_FEE.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RETURN_REASON IS 'Contains reason code describing why the item is being returned.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RETURN_REASON_DESC IS 'Contains reason description describing why the item is being returned.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.CANCEL_REASON IS 'Contains the reason code the return was cancelled.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.UNIT_RESTOCK_FEE IS 'Contains restocking fee per unit level.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RESTOCK_TYPE IS 'Contains restocking fee type.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RESTOCK_TYPE_DESC IS 'Contains restocking fee type description.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.ROW_ID IS 'Contains wf_order_detail row id.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.PRIMARY_CURRENCY IS 'Contains primary currency'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RETURN_CURRENCY IS 'Contains return currency which may or may not be different from the primary currency in the system.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.EXCHANGE_RATE IS 'Contains exchange rate between the primary currency and the Franchise return currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.TAX_RATE IS 'Contains the tax rate.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.VAT IS 'Contains VAT amount of the frcnhiase return record.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.FINAL_RETURN_TOTAL_COST IS 'Contains the total amount that has to be returned to the franchisee'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RET_UNIT_COST_INCL_VAT IS 'Contains Cost inclusive of VAT at which the item is being returned.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.FINAL_RET_UNIT_COST_INVAT IS 'Contains the difference between RET_UNIT_COST_INCL_VAT and UNIT_RESTOCK_FEE'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.FINAL_RET_TOTAL_COST_INVAT IS 'Contains the total amount inclusive of VAT that has to be returned to the franchisee'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.CUSTOMER_COST_PRIM IS 'Contains customer cost in the primary currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RETURN_UNIT_COST_PRIM IS 'Contains unit cost that has to be returned in the primary currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.FINAL_RETURN_UNIT_COST_PRIM IS 'Contains the difference between RET_UNIT_COST_INCL_VAT and UNIT_RESTOCK_FEE in the primary currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.VAT_PRIM IS 'Conatins VAT in the primary currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.UNIT_RESTOCK_FEE_PRIM IS 'Contains restock fee in the primary currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.FINAL_RETURN_TOTAL_COST_PRIM IS 'Contains the total amount in the primary currency that has to be returned to the franchisee.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.RET_UNIT_COST_INCL_VAT_PRIM IS 'Contains unit cost inclusive of VAT that has to be returned.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.FINAL_RET_UNIT_COST_INVAT_PRIM IS 'Contains the difference between RET_UNIT_COST_INCL_VAT and UNIT_RESTOCK_FEE in the primary currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.FIN_RET_TOTAL_COST_INVAT_PRIM IS 'Contains the total amount inclusive of VAT in the primary currency that has to be returned to the franchisee.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.PRIMARY_REF_ITEM IS 'Contains the primary reference item.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.CANCEL_REASON_DESC IS 'Contains cancel description describing why the item is being cancelled.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.PRIMARY_REF_ITEM_DESC IS 'Contains the primary reference item description.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.CUSTOMER_COST_IN_RETURN_CUR IS 'Contains order cost in return currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.CUSTOMER_COST_IN_PRIMARY_CUR IS 'Contains order cost in primary currency.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.STANDARD_UOM IS 'Contains Item UOM.'
/
COMMENT ON COLUMN V_WF_RETURN_DATA.DEPOSIT_ITEM_TYPE IS 'Contains Deposit Item Type.'
/