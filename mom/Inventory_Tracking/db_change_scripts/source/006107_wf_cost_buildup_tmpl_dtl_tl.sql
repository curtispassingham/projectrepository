--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:		       WF_COST_BUILDUP_TMPL_DTL_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE WF_COST_BUILDUP_TMPL_DTL_TL(
LANG NUMBER(6) NOT NULL,
TEMPL_ID NUMBER(10) NOT NULL,
COST_COMP_ID VARCHAR2(10) NOT NULL,
DESCRIPTION VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE WF_COST_BUILDUP_TMPL_DTL_TL is 'This is the translation table for WF_COST_BUILDUP_TMPL_DETAIL table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN WF_COST_BUILDUP_TMPL_DTL_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN WF_COST_BUILDUP_TMPL_DTL_TL.TEMPL_ID is 'Unique cost buildup template ID number associated'
/

COMMENT ON COLUMN WF_COST_BUILDUP_TMPL_DTL_TL.COST_COMP_ID is 'Contains a unique specified code representing the component.'
/

COMMENT ON COLUMN WF_COST_BUILDUP_TMPL_DTL_TL.DESCRIPTION is 'Contains the name or description of the Component.'
/

COMMENT ON COLUMN WF_COST_BUILDUP_TMPL_DTL_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN WF_COST_BUILDUP_TMPL_DTL_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN WF_COST_BUILDUP_TMPL_DTL_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN WF_COST_BUILDUP_TMPL_DTL_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE WF_COST_BUILDUP_TMPL_DTL_TL ADD CONSTRAINT PK_WF_COST_BUILDUP_TMPL_DTL_TL PRIMARY KEY (
LANG,
TEMPL_ID,
COST_COMP_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE WF_COST_BUILDUP_TMPL_DTL_TL
 ADD CONSTRAINT WCBTDT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE WF_COST_BUILDUP_TMPL_DTL_TL ADD CONSTRAINT WCBTDT_WCBTD_FK FOREIGN KEY (
TEMPL_ID,
COST_COMP_ID
) REFERENCES WF_COST_BUILDUP_TMPL_DETAIL (
TEMPL_ID,
COST_COMP_ID
)
/

