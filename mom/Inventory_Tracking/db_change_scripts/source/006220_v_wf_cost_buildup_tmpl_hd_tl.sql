CREATE OR REPLACE FORCE VIEW V_WF_COST_BUILDUP_TMPL_HD_TL (TEMPL_ID, TEMPL_DESC, LANG ) AS
SELECT  b.templ_id,
        case when tl.lang is not null then tl.templ_desc else b.templ_desc end templ_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  WF_COST_BUILDUP_TMPL_HEAD b,
        WF_COST_BUILDUP_TMPL_HD_TL tl
 WHERE  b.templ_id = tl.templ_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_WF_COST_BUILDUP_TMPL_HD_TL is 'This is the translation view for base table WF_COST_BUILDUP_TMPL_HEAD. This view fetches data in user langauge either from translation table WF_COST_BUILDUP_TMPL_HD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_WF_COST_BUILDUP_TMPL_HD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_WF_COST_BUILDUP_TMPL_HD_TL.TEMPL_ID is 'Unique cost buildup template ID number, generated from sequence.'
/

COMMENT ON COLUMN V_WF_COST_BUILDUP_TMPL_HD_TL.TEMPL_DESC is 'Description of the cost buildup template.'
/

