CREATE OR REPLACE FORCE VIEW V_WF_CUSTOMER_TL (WF_CUSTOMER_ID, WF_CUSTOMER_NAME, LANG ) AS
SELECT  b.wf_customer_id,
        case when tl.lang is not null then tl.wf_customer_name else b.wf_customer_name end wf_customer_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  WF_CUSTOMER b,
        WF_CUSTOMER_TL tl
 WHERE  b.wf_customer_id = tl.wf_customer_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_WF_CUSTOMER_TL is 'This is the translation view for base table WF_CUSTOMER. This view fetches data in user langauge either from translation table WF_CUSTOMER_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_WF_CUSTOMER_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_WF_CUSTOMER_TL.WF_CUSTOMER_NAME is 'This field will hold the customer description.'
/

COMMENT ON COLUMN V_WF_CUSTOMER_TL.WF_CUSTOMER_ID is 'This field will hold the unique identifier for the customer.'
/

