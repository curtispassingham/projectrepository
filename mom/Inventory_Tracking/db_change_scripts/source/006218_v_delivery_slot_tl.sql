CREATE OR REPLACE FORCE VIEW V_DELIVERY_SLOT_TL (DELIVERY_SLOT_ID, DELIVERY_SLOT_DESC, LANG ) AS
SELECT  b.delivery_slot_id,
        case when tl.lang is not null then tl.delivery_slot_desc else b.delivery_slot_desc end delivery_slot_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DELIVERY_SLOT b,
        DELIVERY_SLOT_TL tl
 WHERE  b.delivery_slot_id = tl.delivery_slot_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DELIVERY_SLOT_TL is 'This is the translation view for base table DELIVERY_SLOT. This view fetches data in user langauge either from translation table DELIVERY_SLOT_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DELIVERY_SLOT_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DELIVERY_SLOT_TL.DELIVERY_SLOT_ID is 'This column specifies when the store requested quantity is needed at the store. For example, it defines whether the stock is designated for AM or PM delivery.'
/

COMMENT ON COLUMN V_DELIVERY_SLOT_TL.DELIVERY_SLOT_DESC is 'Description of the delivery slot. This could contain the time element for the delivery schedule (i.e. Afternoon Slot 3 PM).'
/

