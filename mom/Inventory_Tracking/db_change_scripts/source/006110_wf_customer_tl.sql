--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       WF_CUSTOMER_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE WF_CUSTOMER_TL(
LANG NUMBER(6) NOT NULL,
WF_CUSTOMER_NAME VARCHAR2(120) NOT NULL,
WF_CUSTOMER_ID NUMBER(10) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE WF_CUSTOMER_TL is 'This is the translation table for WF_CUSTOMER table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN WF_CUSTOMER_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN WF_CUSTOMER_TL.WF_CUSTOMER_NAME is 'This field will hold the customer description.'
/

COMMENT ON COLUMN WF_CUSTOMER_TL.WF_CUSTOMER_ID is 'This field will hold the unique identifier for the customer.'
/

COMMENT ON COLUMN WF_CUSTOMER_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN WF_CUSTOMER_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN WF_CUSTOMER_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN WF_CUSTOMER_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE WF_CUSTOMER_TL ADD CONSTRAINT PK_WF_CUSTOMER_TL UNIQUE (
LANG,
WF_CUSTOMER_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE WF_CUSTOMER_TL
 ADD CONSTRAINT WCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE WF_CUSTOMER_TL ADD CONSTRAINT WCT_WC_FK FOREIGN KEY (
WF_CUSTOMER_ID
) REFERENCES WF_CUSTOMER (
WF_CUSTOMER_ID
)
/

