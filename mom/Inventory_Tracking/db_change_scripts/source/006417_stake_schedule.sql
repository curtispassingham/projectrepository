--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------------
--       Modifying Table:	STAKE_SCHEDULE               
-------------------------------------------------

PROMPT Dropping CONSTRAINT 'CHK_STAKE_SCH_LOC_TYPE'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_STAKE_SCH_LOC_TYPE'
     AND CONSTRAINT_TYPE = 'C';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE STAKE_SCHEDULE DROP CONSTRAINT CHK_STAKE_SCH_LOC_TYPE';
  end if;
end;
/

PROMPT ADDING CONSTRAINT 'CHK_STAKE_SCH_LOC_TYPE'
ALTER TABLE STAKE_SCHEDULE ADD CONSTRAINT
 CHK_STAKE_SCH_LOC_TYPE CHECK ( LOC_TYPE IN ('S','W','L','E'))
/
