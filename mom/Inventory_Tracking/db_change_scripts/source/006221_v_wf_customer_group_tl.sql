CREATE OR REPLACE FORCE VIEW V_WF_CUSTOMER_GROUP_TL (WF_CUSTOMER_GROUP_ID, WF_CUSTOMER_GROUP_NAME, LANG ) AS
SELECT  b.wf_customer_group_id,
        case when tl.lang is not null then tl.wf_customer_group_name else b.wf_customer_group_name end wf_customer_group_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  WF_CUSTOMER_GROUP b,
        WF_CUSTOMER_GROUP_TL tl
 WHERE  b.wf_customer_group_id = tl.wf_customer_group_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_WF_CUSTOMER_GROUP_TL is 'This is the translation view for base table WF_CUSTOMER_GROUP. This view fetches data in user langauge either from translation table WF_CUSTOMER_GROUP_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_WF_CUSTOMER_GROUP_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_WF_CUSTOMER_GROUP_TL.WF_CUSTOMER_GROUP_ID is 'This field will hold the unique identifier for the customer group.'
/

COMMENT ON COLUMN V_WF_CUSTOMER_GROUP_TL.WF_CUSTOMER_GROUP_NAME is 'This field will hold the customer group description'
/

