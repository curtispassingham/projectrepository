CREATE OR REPLACE FUNCTION REFRESH_MV_SIZE_PROFILE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   return BOOLEAN IS
   
   ------------------------------------------------------------------------------
   --- Function Name:  REFRESH_MV_SIZE_PROFILE
   --- Purpose:        This Function will perform a complete refresh of the materialized view
   ---                 MV_SIZE_PROFILE
------------------------------------------------------------------------------
 
   L_program               VARCHAR2(64) := 'REFRESH_MV_SIZE_PROFILE';
 
BEGIN

  DBMS_MVIEW.REFRESH('mv_size_profile_1a','f');

  DBMS_MVIEW.REFRESH('mv_size_profile_1b','f');

  DBMS_MVIEW.REFRESH(list=>'mv_size_profile', parallelism=>4, atomic_refresh=>FALSE);
   
   return TRUE;
 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('FUNCTION_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
 
END REFRESH_MV_SIZE_PROFILE;
/
