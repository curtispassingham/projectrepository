
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------------
-- Name:    NEXT_STAKE_SCHEDULE_ID
-- Purpose: This procedure will fetch the next available schedule ID.
----------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_STAKE_SCHEDULE_ID(O_schedule_id IN OUT NUMBER,
                                                   O_return_code   IN OUT VARCHAR2,
                                                   O_error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_schedule_id            NUMBER(8)   := 0;
   L_sched_number           NUMBER(8)   := 0;
   L_wrap_sequence_number   NUMBER(8)   := 0;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1) := NULL;

   cursor C_EXIST_SCHEDULE(cc_number_param NUMBER) IS
      select 'x'
        from stake_schedule
       where schedule_id = cc_number_param;

BEGIN
    O_return_code := 'TRUE';
    O_error_message := NULL;

    LOOP
        select stake_schedule_sequence.NEXTVAL
           into L_schedule_id
           from sys.dual;

        if (L_first_time = 'Yes') then
           L_wrap_sequence_number := L_schedule_id;
           L_first_time := 'No';
        elsif (L_schedule_id = L_wrap_sequence_number) then
           O_error_message := 'There are no available Stock Count Schedule numbers.';
           O_return_code := 'FALSE';
           EXIT;
        end if;

        L_sched_number := L_schedule_id;

        open  C_EXIST_SCHEDULE(L_sched_number);
        fetch C_EXIST_SCHEDULE into L_dummy;
        if(C_EXIST_SCHEDULE%NOTFOUND) then
           O_return_code := 'TRUE';
           close C_EXIST_SCHEDULE;
	     O_schedule_id := L_schedule_id;
           EXIT;
        end if;
        close C_EXIST_SCHEDULE;

   END LOOP;

EXCEPTION
    when OTHERS then
        O_error_message := SQLERRM || ' from NEXT_STAKE_SCHEDULE_ID proc.';
        O_return_code := 'FALSE';
--------------------------------------------------------------------------------------
END NEXT_STAKE_SCHEDULE_ID;
/


