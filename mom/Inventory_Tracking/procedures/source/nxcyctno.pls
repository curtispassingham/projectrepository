
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------------
-- Name:    NEXT_CYCLE_COUNT_NUMBER
-- Purpose: This Cycle Count number sequence generator will return to the calling
--          program/procedure.  Upon success (TRUE) a Cycle Count number.  
--          Upon failure (FALSE) an appropriate error message for display purposes by
--          the calling program/procedure.
----------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_CYCLE_COUNT_NUMBER(
                                              cc_number     IN OUT NUMBER,
                                              return_code   IN OUT VARCHAR2,
                                              error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_cycle_count            NUMBER(8)   := 0;
   L_wrap_sequence_number   NUMBER(8)   := 0;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1) := NULL;

   CURSOR c_exist_stake_head(cc_number_param NUMBER) IS
      SELECT 'x'
        FROM stake_head
       WHERE stake_head.cycle_count = cc_number_param;

BEGIN
    return_code := 'TRUE';
    error_message := NULL;

    LOOP
        SELECT cycle_count_sequence.NEXTVAL
          INTO L_cycle_count
          FROM sys.dual;

        IF (L_first_time = 'Yes') THEN
            L_wrap_sequence_number := L_cycle_count;
            L_first_time := 'No';
        ELSIF (L_cycle_count = L_wrap_sequence_number) THEN
            error_message := 'There are no available Cycle Count numbers.';
            return_code := 'FALSE';
            EXIT;
        END IF;

        cc_number := L_cycle_count;

        OPEN  c_exist_stake_head(cc_number);
        FETCH c_exist_stake_head into L_dummy;
        IF (c_exist_stake_head%notfound) THEN
           return_code := 'TRUE';
           CLOSE c_exist_stake_head;
           EXIT;
        END IF;
        CLOSE c_exist_stake_head;

   END LOOP;

EXCEPTION
    WHEN OTHERS THEN
        error_message := SQLERRM || ' from NEXT_CYCLE_COUNT_NUMBER proc.';
        return_code := 'FALSE';
END NEXT_CYCLE_COUNT_NUMBER;
/


