PROMPT Creating Trigger 'RMS_TABLE_WCR_AIUDR'
CREATE OR REPLACE TRIGGER RMS_TABLE_WCR_AIUDR
 AFTER DELETE OR INSERT OR UPDATE 
 ON WF_COST_RELATIONSHIP
 FOR EACH ROW
DECLARE

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_status          VARCHAR(1);
   L_dept             WF_COST_RELATIONSHIP.DEPT%TYPE;
   L_class            WF_COST_RELATIONSHIP.CLASS%TYPE;
   L_subclass         WF_COST_RELATIONSHIP.SUBCLASS%TYPE;
   L_location         WF_COST_RELATIONSHIP.LOCATION%TYPE;
   L_templ_id         WF_COST_RELATIONSHIP.TEMPL_ID%TYPE;
   L_old_start_date   WF_COST_RELATIONSHIP.START_DATE%TYPE;
   L_old_end_date     WF_COST_RELATIONSHIP.END_DATE%TYPE;
   L_new_start_date   WF_COST_RELATIONSHIP.START_DATE%TYPE;
   L_new_end_date     WF_COST_RELATIONSHIP.END_DATE%TYPE;
   L_item             WF_COST_RELATIONSHIP.ITEM%TYPE;
   L_action           COST_EVENT.ACTION%TYPE;
   L_rowid            ROWID;
BEGIN 
if DELETING then
   L_dept            := :old.dept;
   L_class           := :old.class;
   L_subclass        := :old.subclass;
   L_location        := :old.location;
   L_templ_id        := :old.templ_id;
   L_old_start_date  := :old.start_date;
   L_old_end_date    := :old.end_date;
   L_new_start_date  := :old.start_date;
   L_new_end_date    := :old.end_date;
   L_action          := 'REM';
   L_rowid           := :old.rowid;
   L_item            := :old.item;
elsif UPDATING then
   L_dept            := :new.dept;
   L_class           := :new.class;
   L_subclass        := :new.subclass;
   L_location        := :new.location;
   L_templ_id        := :new.templ_id;
   L_old_start_date  := :old.start_date;
   L_old_end_date    := :old.end_date;
   L_new_start_date  := :new.start_date;
   L_new_end_date    := :new.end_date;
   L_rowid           := :new.rowid;
   L_action          := 'MOD';
   L_item            := :new.item;
else
   L_dept            := :new.dept;
   L_class           := :new.class;
   L_subclass        := :new.subclass;
   L_location        := :new.location;
   L_templ_id        := :new.templ_id;
   L_old_start_date  := :new.start_date;
   L_old_end_date    := :new.end_date;
   L_new_start_date  := :new.start_date;
   L_new_end_date    := :new.end_date;
   L_action          := 'ADD';
   L_rowid           := :new.rowid;
   L_item            := :new.item;
end if;
---
merge into gtt_wf_cost_relationship gtt
     using dual
        on ((gtt.dept = L_dept and
             gtt.class = L_class and
             gtt.subclass = L_subclass and 
             gtt.location = L_location and
             gtt.old_start_date = L_old_start_date and
             gtt.old_end_date = L_old_end_date and
             gtt.item = L_item) or
            (gtt.wf_rowid = L_rowid))
      when matched then
    update
       set new_start_date = l_new_start_date,
           new_end_date = L_new_end_date,
           action = decode(L_action,'REM',decode(action,'ADD','ADD','MOD','REM',action),'ADD',decode(action,'REM','MOD',action),action)
    delete 
     where action ='ADD'  and 
           L_action = 'REM'
      when not matched then
    insert (gtt.dept,
            gtt.class,
            gtt.subclass,
            gtt.location,
            gtt.old_start_date,
            gtt.old_end_date,
            gtt.new_start_date,
            gtt.new_end_date,
            gtt.templ_id,
            gtt.action,
            gtt.wf_rowid,
            gtt.item)
    values (L_dept,
            L_class,
            L_subclass,
            L_location,
            L_old_start_date,
            L_old_end_date,
            decode(L_action,'REM',L_old_start_date,L_new_start_date),
            decode(L_action,'REM',L_old_end_date,L_new_end_date),
            L_templ_id,
            L_action,
            L_rowid,
            L_item);
---             
update gtt_wf_cost_relationship gtt
   set old_start_date = L_new_start_date,
       old_end_date = L_new_end_date
 where gtt.dept = L_dept 
   and gtt.class = L_class
   and gtt.subclass = L_subclass 
   and gtt.location = L_location
   and gtt.item     = L_item
   and gtt.old_start_date = L_old_start_date 
   and gtt.old_end_date = L_old_end_date 
   and action = 'ADD';
---       
delete from gtt_wf_cost_relationship
      where action = 'MOD' 
        and new_start_date = old_start_date 
        and new_end_date = old_end_date;
EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMS_TABLE_WCR_AIUDR');
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END RMS_TABLE_WCR_AIUDR;
/