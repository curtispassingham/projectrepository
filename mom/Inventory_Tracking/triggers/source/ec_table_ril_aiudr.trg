PROMPT Creating Trigger 'EC_TABLE_RIL_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_RIL_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF REPL_METHOD
, NEXT_DELIVERY_DATE
, REJECT_STORE_ORD_IND
, PRIMARY_REPL_SUPPLIER
, MULT_RUNS_PER_DAY_IND
 ON REPL_ITEM_LOC
 FOR EACH ROW
 WHEN (new.loc_type = 'S' OR old.loc_type = 'S')
DECLARE


   L_record        ITEM_LOC%ROWTYPE := NULL;
   L_status        VARCHAR2(1) := NULL;
   L_error_message VARCHAR2(255) := NULL;
   L_message_type  ITEMLOC_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;

   L_prim_repl_supplier   REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE := NULL;
   L_repl_method          REPL_ITEM_LOC.REPL_METHOD%TYPE := NULL;
   L_reject_store_ord_ind REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE := NULL;
   L_next_delivery_date   REPL_ITEM_LOC.NEXT_DELIVERY_DATE%TYPE := NULL;
   L_mult_runs_per_day_ind  REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE;

   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING then
      L_record.item := :old.item;
      L_record.loc  := :old.location;
      L_record.loc_type := :old.loc_type;
      L_mult_runs_per_day_ind  := :old.mult_runs_per_day_ind;
   else
           if UPDATING
            and ((    (NVL(:old.primary_repl_supplier, 0)  = NVL(:new.primary_repl_supplier, 0))
                 and (:old.repl_method                    = :new.repl_method)
                 and (NVL(:old.reject_store_ord_ind, 'x') = NVL(:new.reject_store_ord_ind, 'x'))
                 and (NVL(:old.next_delivery_date, to_date('18000101','YYYYMMDD'))  = NVL(:new.next_delivery_date, to_date('18000101','YYYYMMDD')))
                 and (:old.mult_runs_per_day_ind  = :new.mult_runs_per_day_ind))
             or (    (NVL(:old.next_delivery_date, to_date('18000101','YYYYMMDD')) <> NVL(:new.next_delivery_date, to_date('18000101','YYYYMMDD')))
                 and (:new.repl_method                   <> 'SO')
                 and (:old.repl_method                    = :new.repl_method)
                 and (NVL(:old.reject_store_ord_ind,'x')  = NVL(:new.reject_store_ord_ind,'x'))
                 and (NVL(:old.primary_repl_supplier, 0)  = NVL(:new.primary_repl_supplier, 0)))) then
         return;
      end if;

      L_record.item      := :new.item;
      L_record.loc       := :new.location;
      L_record.loc_type  := :new.loc_type;
      ---
      L_prim_repl_supplier   := :new.primary_repl_supplier;
      L_repl_method          := :new.repl_method;
      L_reject_store_ord_ind := :new.reject_store_ord_ind;
      L_next_delivery_date   := :new.next_delivery_date;
      L_mult_runs_per_day_ind  := :new.mult_runs_per_day_ind;

   end if;

   L_message_type := RMSMFM_ITEMLOC.REPL_UPD;

   if RMSMFM_ITEMLOC.ADDTOQ(L_error_message,
                            L_message_type,
                            L_record,
                            NULL,
                            L_prim_repl_supplier,
                            L_repl_method,
                            L_reject_store_ord_ind,
                            L_next_delivery_date,
                            L_mult_runs_per_day_ind) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         API_LIBRARY.HANDLE_ERRORS(L_status,
                                   L_error_message,
                                   API_LIBRARY.FATAL_ERROR,
                                   'EC_TABLE_RIL_AIUDR');
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/
