PROMPT Creating Trigger 'EC_TABLE_DLVY_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_DLVY_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON DELIVERY_SLOT
 FOR EACH ROW
DECLARE

   L_record          DELIVERY_SLOT%ROWTYPE               :=  NULL;
   L_action_type     VARCHAR2(1)                         :=  NULL;
   L_status          VARCHAR2(1)                         :=  NULL;
   L_error_msg       RTK_ERRORS.RTK_TEXT%TYPE            :=  NULL;
   L_message_type    DELIVERY_SLOT_MFQUEUE.MESSAGE_TYPE%TYPE     :=  NULL;

   PROGRAM_ERROR     EXCEPTION;
BEGIN

   if DELETING then

      L_action_type             := 'D';
      L_message_type            := RMSMFM_DLVYSLT.SLT_DEL;
      L_record.delivery_slot_id := :old.delivery_slot_id;
      L_record.delivery_slot_desc  := :old.delivery_slot_desc;
      L_record.delivery_slot_sequence := :old.delivery_slot_sequence;

   else
      if INSERTING then

         L_action_type := 'A';
         L_message_type := RMSMFM_DLVYSLT.SLT_ADD;

      else

         L_action_type := 'M';
         L_message_type := RMSMFM_DLVYSLT.SLT_UPD;

         if ((:old.delivery_slot_sequence     = :new.delivery_slot_sequence) and
             (:old.delivery_slot_desc   = :new.delivery_slot_desc)) then

            return;

         end if;
      end if;

      L_record.delivery_slot_id    := :new.delivery_slot_id;
      L_record.delivery_slot_desc  := :new.delivery_slot_desc;
      L_record.delivery_slot_sequence := :new.delivery_slot_sequence;

   end if;

   if not RMSMFM_DLVYSLT.ADDTOQ(L_error_msg,
                                L_status,
                                L_message_type,
                                L_record.delivery_slot_id,
                                L_record.delivery_slot_desc,
                                L_record.delivery_slot_sequence) then
      raise PROGRAM_ERROR;
   end if;

   if L_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_DLVY_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);

END EC_TABLE_DLVY_AIUDR;
/
