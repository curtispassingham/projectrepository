drop type "OBJ_FM_NFE_ENTITY_REC" force;
/
 
create or replace
TYPE "OBJ_FM_NFE_ENTITY_REC" AS OBJECT (
          COD_PFJ           VARCHAR2(10),
          RAZAO_SOCIAL      VARCHAR2(240),
          ENDERECO          VARCHAR2(250),
          FANTASIA          VARCHAR2(150),
          BAIRRO            VARCHAR2(250),
          COD_PAIS          VARCHAR2(250),
          CNPJ              VARCHAR2(250),
          CPF               VARCHAR2(250),
          INSCR_ESTADUAL    VARCHAR2(250),
          INSCR_MUNICIPAL   VARCHAR2(250),
          COD_MUN           VARCHAR2(250),
          NUM               VARCHAR2(250),
          COMPLEMENTO       VARCHAR2(250),
          ESTADO            VARCHAR2(250),
          IBG_ESTADO	    VARCHAR2(2),
          CEP               VARCHAR2(250),
          SUFRAMA           VARCHAR2(250),
          CIDADE            VARCHAR2(120),
          FONE              VARCHAR2(20),
          EMAIL             VARCHAR2(100),
          SISCOMEX_ID       VARCHAR2(120), 
          COD_CNAE          VARCHAR2(250),
          COD_REG_TRIB      NUMBER(1),
          INSCR_ESTADUAL_ST VARCHAR2(250),
constructor function "OBJ_FM_NFE_ENTITY_REC"
(
          COD_PFJ           VARCHAR2     DEFAULT NULL,
          RAZAO_SOCIAL      VARCHAR2     DEFAULT NULL,
          ENDERECO          VARCHAR2     DEFAULT NULL,
          FANTASIA          VARCHAR2     DEFAULT NULL,
          BAIRRO            VARCHAR2     DEFAULT NULL,
          COD_PAIS          VARCHAR2     DEFAULT NULL,
          CNPJ              VARCHAR2     DEFAULT NULL,
          CPF               VARCHAR2     DEFAULT NULL,
          INSCR_ESTADUAL    VARCHAR2     DEFAULT NULL,
          INSCR_MUNICIPAL   VARCHAR2     DEFAULT NULL,
          COD_MUN           VARCHAR2     DEFAULT NULL,
          NUM               VARCHAR2     DEFAULT NULL,
          COMPLEMENTO       VARCHAR2     DEFAULT NULL,
          ESTADO            VARCHAR2     DEFAULT NULL,
          IBG_ESTADO	    VARCHAR2	 DEFAULT NULL,
          CEP               VARCHAR2     DEFAULT NULL,
          SUFRAMA           VARCHAR2     DEFAULT NULL,
          CIDADE            VARCHAR2     DEFAULT NULL,
          FONE              VARCHAR2     DEFAULT NULL,
          EMAIL             VARCHAR2     DEFAULT NULL,
          SISCOMEX_ID       VARCHAR2     DEFAULT NULL,
          COD_CNAE          VARCHAR2     DEFAULT NULL,
          COD_REG_TRIB      NUMBER       DEFAULT NULL,
          INSCR_ESTADUAL_ST VARCHAR2     DEFAULT NULL
) return self as result
)
/
 
create or replace type body "OBJ_FM_NFE_ENTITY_REC"   is
constructor function "OBJ_FM_NFE_ENTITY_REC"
(
        COD_PFJ           VARCHAR2     DEFAULT NULL,
        RAZAO_SOCIAL      VARCHAR2     DEFAULT NULL,
        ENDERECO          VARCHAR2     DEFAULT NULL,
        FANTASIA          VARCHAR2     DEFAULT NULL,
        BAIRRO            VARCHAR2     DEFAULT NULL,
        COD_PAIS          VARCHAR2     DEFAULT NULL,
        CNPJ              VARCHAR2     DEFAULT NULL,
        CPF               VARCHAR2     DEFAULT NULL,
        INSCR_ESTADUAL    VARCHAR2     DEFAULT NULL,
        INSCR_MUNICIPAL   VARCHAR2     DEFAULT NULL,
        COD_MUN           VARCHAR2     DEFAULT NULL,
        NUM               VARCHAR2     DEFAULT NULL,
        COMPLEMENTO       VARCHAR2     DEFAULT NULL,
        ESTADO            VARCHAR2     DEFAULT NULL,
        IBG_ESTADO        VARCHAR2     DEFAULT NULL,
        CEP               VARCHAR2     DEFAULT NULL,
        SUFRAMA           VARCHAR2     DEFAULT NULL,
        CIDADE            VARCHAR2     DEFAULT NULL,
        FONE              VARCHAR2     DEFAULT NULL,
        EMAIL             VARCHAR2     DEFAULT NULL,
        SISCOMEX_ID       VARCHAR2     DEFAULT NULL,
        COD_CNAE          VARCHAR2     DEFAULT NULL,
        COD_REG_TRIB      NUMBER       DEFAULT NULL,
        INSCR_ESTADUAL_ST VARCHAR2     DEFAULT NULL
) return self as result is
begin
        self.cod_pfj           := cod_pfj;
        self.razao_social      := razao_social;
        self.endereco          := endereco;
        self.fantasia          := fantasia;
        self.bairro            := bairro;
        self.cod_pais          := cod_pais;
        self.cnpj              := cnpj;
        self.cpf               := cpf;
        self.inscr_estadual    := inscr_estadual;
        self.inscr_municipal   := inscr_municipal;
        self.cod_mun           := cod_mun;
        self.num               := num;
        self.complemento       := complemento;
        self.estado            := estado;
        self.ibg_estado        := ibg_estado;
        self.cep               := cep;
        self.suframa           := suframa;
        self.cidade            := cidade;
        self.fone              := fone;
        self.email             := email;
        self.siscomex_id       := siscomex_id;
        self.cod_cnae          := cod_cnae;
        self.cod_reg_trib      := cod_reg_trib;
        self.inscr_estadual_st := inscr_estadual_st;
  return;
end;
end;
/
 
drop type "OBJ_FM_NFE_DOCDTL_TBL" force;
/
 
drop type "OBJ_FM_NFE_DOCDTL_REC" force;
/
 
create or replace
TYPE "OBJ_FM_NFE_DOCDTL_REC" AS OBJECT(
         LINE_NUMBER              NUMBER(6),      
         COD_PRODUTO              VARCHAR2(25),
         DESCRICAO                VARCHAR2(250),
         QUANTIDADE               NUMBER(12,4),
         PRECO_UNITARIO           NUMBER(20,4),
         VLR_TOTAL_ITEM           NUMBER(20,4),
         COD_CFOP                 VARCHAR2(12),
         COD_NCM                  VARCHAR2(15),
         TP_ORIGEM_ITEM           VARCHAR2(250),
         COD_SIT_TRIB_EST         VARCHAR2(3),
         VLR_BASE_COFINS          NUMBER(20,4),
         VLR_TOTAL_COFINS         NUMBER(20,4),
         VLR_ALIQ_COFINS          NUMBER(20,4),
         VLR_BASE_ICMS            NUMBER(20,4),
         VLR_TOTAL_ICMS           NUMBER(20,4),
         VLR_ALIQ_ICMS            NUMBER(20,4),
         CHAVE_ACESSO_NFE_REFER   VARCHAR2(44),
         VLR_FRETE                NUMBER(20,4), 
         VLR_SEGURO               NUMBER(20,4),
         VLR_OUTRAS               NUMBER(20,4),
         VLR_DESCONTO             NUMBER(20,4),
         VLR_BASE_ISS             NUMBER(20,4),
         VLR_ALIQ_ISS             NUMBER(20,4),
         VLR_TOTAL_ISS            NUMBER(20,4),
         MOD_BC_ICMS              NUMBER(1),
         MOD_BC_ICMS_ST           NUMBER(1),
         VLR_BASE_ICMS_ST         NUMBER(20,4),
         PERC_MVA                 NUMBER(20,4),
         VLR_ALIQ_ICMS_ST         NUMBER(20,4),
         VLR_TOTAL_ICMS_ST        NUMBER(20,4),
         VLR_BASE_REDU_ICMS_ST    NUMBER(20,4),
         VLR_TOTAL_REDU_ICMS_ST   NUMBER(20,4),
         VLR_BASE_REDU_ICMS       NUMBER(20,4),
         IND_TOT                  NUMBER(1),
         COD_EAN                  VARCHAR2(25),
         EX_IPI                   VARCHAR2(250),
         NUM_PEDIDO               NUMBER(10),
         COD_MEDIDA               VARCHAR2(6),
         CST_PIS                  VARCHAR2(3),
         VLR_BASE_PIS             NUMBER(20,4),
         VLR_ALIQ_PIS             NUMBER(20,4),
         VLR_TOTAL_PIS            NUMBER(20,4),
         CST_COFINS               VARCHAR2(3),
         COD_LST                  VARCHAR2(250),
         VLR_TOTAL_PIS_SERV       NUMBER(20,4),
         VLR_TOTAL_COFINS_SERV    NUMBER(20,4),
         CHAVE_RURAL_PROD_REFER   NUMBER(15),
         IBG_ESTADO_REFER         VARCHAR2(250),
         DATA_EMISSAO_REFER       DATE,
         CNPJ_REFER               VARCHAR2(14),
         CPF_REFER                VARCHAR2(14),
         INSCR_ESTADUAL_REFER     VARCHAR2(250),
         NFE_DOC_TYPE_REFER       NUMBER(8),
         NUM_DOCFIS_REFER         NUMBER(15),
         SERIE_DOCFIS_REFER       VARCHAR2(20),
constructor function "OBJ_FM_NFE_DOCDTL_REC"
(       
         LINE_NUMBER              NUMBER          DEFAULT NULL,      
         COD_PRODUTO              VARCHAR2        DEFAULT NULL,
         DESCRICAO                VARCHAR2        DEFAULT NULL,
         QUANTIDADE               NUMBER          DEFAULT NULL,
         PRECO_UNITARIO           NUMBER          DEFAULT NULL,
         VLR_TOTAL_ITEM           NUMBER          DEFAULT NULL,
         COD_CFOP                 VARCHAR2        DEFAULT NULL,
         COD_NCM                  VARCHAR2        DEFAULT NULL,
         TP_ORIGEM_ITEM           VARCHAR2        DEFAULT NULL,
         COD_SIT_TRIB_EST         VARCHAR2        DEFAULT NULL,
         VLR_BASE_COFINS          NUMBER          DEFAULT NULL,
         VLR_TOTAL_COFINS         NUMBER          DEFAULT NULL,
         VLR_ALIQ_COFINS          NUMBER          DEFAULT NULL,
         VLR_BASE_ICMS            NUMBER          DEFAULT NULL,
         VLR_TOTAL_ICMS           NUMBER          DEFAULT NULL,
         VLR_ALIQ_ICMS            NUMBER          DEFAULT NULL,
         CHAVE_ACESSO_NFE_REFER   VARCHAR2        DEFAULT NULL,
         VLR_FRETE                NUMBER          DEFAULT NULL, 
         VLR_SEGURO               NUMBER          DEFAULT NULL,
         VLR_OUTRAS               NUMBER          DEFAULT NULL,
         VLR_DESCONTO             NUMBER          DEFAULT NULL,
         VLR_BASE_ISS             NUMBER          DEFAULT NULL,
         VLR_ALIQ_ISS             NUMBER          DEFAULT NULL,
         VLR_TOTAL_ISS            NUMBER          DEFAULT NULL,
         MOD_BC_ICMS              NUMBER          DEFAULT NULL,
         MOD_BC_ICMS_ST           NUMBER          DEFAULT NULL,
         VLR_BASE_ICMS_ST         NUMBER          DEFAULT NULL,
         PERC_MVA                 NUMBER          DEFAULT NULL,
         VLR_ALIQ_ICMS_ST         NUMBER          DEFAULT NULL,
         VLR_TOTAL_ICMS_ST        NUMBER          DEFAULT NULL,
         VLR_BASE_REDU_ICMS_ST    NUMBER          DEFAULT NULL,
         VLR_TOTAL_REDU_ICMS_ST   NUMBER          DEFAULT NULL,
         VLR_BASE_REDU_ICMS       NUMBER          DEFAULT NULL,
         IND_TOT                  NUMBER          DEFAULT NULL,
         COD_EAN                  VARCHAR2        DEFAULT NULL,
         EX_IPI                   VARCHAR2        DEFAULT NULL,
         NUM_PEDIDO               NUMBER          DEFAULT NULL,
         COD_MEDIDA               VARCHAR2        DEFAULT NULL,
         CST_PIS                  VARCHAR2        DEFAULT NULL,
         VLR_BASE_PIS             NUMBER          DEFAULT NULL,
         VLR_ALIQ_PIS             NUMBER          DEFAULT NULL,
         VLR_TOTAL_PIS            NUMBER          DEFAULT NULL,
         CST_COFINS               VARCHAR2        DEFAULT NULL,
         COD_LST                  VARCHAR2        DEFAULT NULL,
         VLR_TOTAL_PIS_SERV       NUMBER          DEFAULT NULL,
         VLR_TOTAL_COFINS_SERV    NUMBER          DEFAULT NULL,
         CHAVE_RURAL_PROD_REFER   NUMBER          DEFAULT NULL,
         IBG_ESTADO_REFER         VARCHAR2        DEFAULT NULL,
         DATA_EMISSAO_REFER       DATE            DEFAULT NULL,
         CNPJ_REFER               VARCHAR2        DEFAULT NULL,
         CPF_REFER                VARCHAR2        DEFAULT NULL,
         INSCR_ESTADUAL_REFER     VARCHAR2        DEFAULT NULL,
         NFE_DOC_TYPE_REFER       NUMBER          DEFAULT NULL,
         NUM_DOCFIS_REFER         NUMBER          DEFAULT NULL,
         SERIE_DOCFIS_REFER       VARCHAR2        DEFAULT NULL
) return self as result
)
/
 
create or replace type body "OBJ_FM_NFE_DOCDTL_REC" is
constructor function "OBJ_FM_NFE_DOCDTL_REC"
(
         LINE_NUMBER              NUMBER          DEFAULT NULL,      
         COD_PRODUTO              VARCHAR2        DEFAULT NULL,
         DESCRICAO                VARCHAR2        DEFAULT NULL,
         QUANTIDADE               NUMBER          DEFAULT NULL,
         PRECO_UNITARIO           NUMBER          DEFAULT NULL,
         VLR_TOTAL_ITEM           NUMBER          DEFAULT NULL,
         COD_CFOP                 VARCHAR2        DEFAULT NULL,
         COD_NCM                  VARCHAR2        DEFAULT NULL,
         TP_ORIGEM_ITEM           VARCHAR2        DEFAULT NULL,
         COD_SIT_TRIB_EST         VARCHAR2        DEFAULT NULL,
         VLR_BASE_COFINS          NUMBER          DEFAULT NULL,
         VLR_TOTAL_COFINS         NUMBER          DEFAULT NULL,
         VLR_ALIQ_COFINS          NUMBER          DEFAULT NULL,
         VLR_BASE_ICMS            NUMBER          DEFAULT NULL,
         VLR_TOTAL_ICMS           NUMBER          DEFAULT NULL,
         VLR_ALIQ_ICMS            NUMBER          DEFAULT NULL,
         CHAVE_ACESSO_NFE_REFER   VARCHAR2        DEFAULT NULL,
         VLR_FRETE                NUMBER          DEFAULT NULL, 
         VLR_SEGURO               NUMBER          DEFAULT NULL,
         VLR_OUTRAS               NUMBER          DEFAULT NULL,
         VLR_DESCONTO             NUMBER          DEFAULT NULL,
         VLR_BASE_ISS             NUMBER          DEFAULT NULL,
         VLR_ALIQ_ISS             NUMBER          DEFAULT NULL,
         VLR_TOTAL_ISS            NUMBER          DEFAULT NULL,
         MOD_BC_ICMS              NUMBER          DEFAULT NULL,
         MOD_BC_ICMS_ST           NUMBER          DEFAULT NULL,
         VLR_BASE_ICMS_ST         NUMBER          DEFAULT NULL,
         PERC_MVA                 NUMBER          DEFAULT NULL,
         VLR_ALIQ_ICMS_ST         NUMBER          DEFAULT NULL,
         VLR_TOTAL_ICMS_ST        NUMBER          DEFAULT NULL,
         VLR_BASE_REDU_ICMS_ST    NUMBER          DEFAULT NULL,
         VLR_TOTAL_REDU_ICMS_ST   NUMBER          DEFAULT NULL,
         VLR_BASE_REDU_ICMS       NUMBER          DEFAULT NULL,
         IND_TOT                  NUMBER          DEFAULT NULL,
         COD_EAN                  VARCHAR2        DEFAULT NULL,
         EX_IPI                   VARCHAR2        DEFAULT NULL,
         NUM_PEDIDO               NUMBER          DEFAULT NULL,
         COD_MEDIDA               VARCHAR2        DEFAULT NULL,
         CST_PIS                  VARCHAR2        DEFAULT NULL,
         VLR_BASE_PIS             NUMBER          DEFAULT NULL,
         VLR_ALIQ_PIS             NUMBER          DEFAULT NULL,
         VLR_TOTAL_PIS            NUMBER          DEFAULT NULL,
         CST_COFINS               VARCHAR2        DEFAULT NULL,
         COD_LST                  VARCHAR2        DEFAULT NULL,
         VLR_TOTAL_PIS_SERV       NUMBER          DEFAULT NULL,
         VLR_TOTAL_COFINS_SERV    NUMBER          DEFAULT NULL,
         CHAVE_RURAL_PROD_REFER   NUMBER          DEFAULT NULL,
         IBG_ESTADO_REFER         VARCHAR2        DEFAULT NULL,
         DATA_EMISSAO_REFER       DATE            DEFAULT NULL,
         CNPJ_REFER               VARCHAR2        DEFAULT NULL,
         CPF_REFER                VARCHAR2        DEFAULT NULL,
         INSCR_ESTADUAL_REFER     VARCHAR2        DEFAULT NULL,
         NFE_DOC_TYPE_REFER       NUMBER          DEFAULT NULL,
         NUM_DOCFIS_REFER         NUMBER          DEFAULT NULL,
         SERIE_DOCFIS_REFER       VARCHAR2        DEFAULT NULL
) return self as result is
begin
        self.line_number             := line_number;
        self.cod_produto             := cod_produto;
        self.descricao               := descricao;
        self.quantidade              := quantidade;
        self.preco_unitario          := preco_unitario;
        self.vlr_total_item          := vlr_total_item;
        self.cod_cfop                := cod_cfop;
        self.cod_ncm                 := cod_ncm;
        self.tp_origem_item          := tp_origem_item;
        self.cod_sit_trib_est        := cod_sit_trib_est;
        self.vlr_base_cofins         := vlr_base_cofins;
        self.vlr_total_cofins        := vlr_total_cofins;
        self.vlr_aliq_cofins         := vlr_aliq_cofins;
        self.vlr_base_icms           := vlr_base_icms;
        self.vlr_total_icms          := vlr_total_icms;
        self.vlr_aliq_icms           := vlr_aliq_icms;
        self.chave_acesso_nfe_refer  := chave_acesso_nfe_refer;
        self.vlr_frete               := vlr_frete;
        self.vlr_seguro              := vlr_seguro;
        self.vlr_outras              := vlr_outras;
        self.vlr_desconto            := vlr_desconto;
        self.vlr_base_iss            := vlr_base_iss;
        self.vlr_aliq_iss            := vlr_aliq_iss;
        self.vlr_total_iss           := vlr_total_iss;
        self.mod_bc_icms             := mod_bc_icms;
        self.mod_bc_icms_st          := mod_bc_icms_st;
        self.vlr_base_icms_st        := vlr_base_icms_st;
        self.perc_mva                := perc_mva;
        self.vlr_aliq_icms_st        := vlr_aliq_icms_st;
        self.vlr_total_icms_st       := vlr_total_icms_st;
        self.vlr_base_redu_icms_st   := vlr_base_redu_icms_st;
        self.vlr_total_redu_icms_st  := vlr_total_redu_icms_st;
        self.vlr_base_redu_icms      := vlr_base_redu_icms;
        self.ind_tot                 := ind_tot;
        self.cod_ean                 := cod_ean;
        self.ex_ipi                  := ex_ipi;
        self.num_pedido              := num_pedido;
        self.cod_medida              := cod_medida;
        self.cst_pis                 := cst_pis;
        self.vlr_base_pis            := vlr_base_pis;
        self.vlr_aliq_pis            := vlr_aliq_pis;
        self.vlr_total_pis           := vlr_total_pis;
        self.cst_cofins              := cst_cofins;
        self.cod_lst                 := cod_lst;        
        self.vlr_total_pis_serv      := vlr_total_pis_serv;
        self.vlr_total_cofins_serv   := vlr_total_cofins_serv;
        self.chave_rural_prod_refer  := chave_rural_prod_refer;
        self.ibg_estado_refer        := ibg_estado_refer;
        self.data_emissao_refer      := data_emissao_refer;
        self.cnpj_refer              := cnpj_refer;
        self.cpf_refer               := cpf_refer;
        self.inscr_estadual_refer    := inscr_estadual_refer;
        self.nfe_doc_type_refer      := nfe_doc_type_refer;
        self.num_docfis_refer        := num_docfis_refer;
        self.serie_docfis_refer      := serie_docfis_refer;
  return;
end;
end;
/
 
create or replace type "OBJ_FM_NFE_DOCDTL_TBL" as table of "OBJ_FM_NFE_DOCDTL_REC"
/
 
---
 
drop type "OBJ_FM_NFE_DOCHDR_REC" force;
/
 
create or replace TYPE "OBJ_FM_NFE_DOCHDR_REC" AS OBJECT (
             NUM_DOCFIS           NUMBER(15),
             DATA_EMISSAO         DATE,
             DT_RECEBIMENTO       DATE,
             VLR_FRETE            NUMBER(20,4),
             VLR_SEGURO           NUMBER(20,4),
             VLR_OUTRAS           NUMBER(20,4),
             PLACA_VEICULO        VARCHAR2(40),
             VLR_TOTAL_PIS        NUMBER(20,4),
             VLR_TOTAL_COFINS     NUMBER(20,4),
             VLR_TOTAL_ISS        NUMBER(20,4),
             VLR_DESCONTO         NUMBER(20,4),
             VLR_TOTAL_ICMS       NUMBER(20,4),
             VLR_BASE_ICMS        NUMBER(20,4),
             VLR_BASE_ISS         NUMBER(20,4),
             VLR_BASE_ICMS_ST     NUMBER(20,4),
             VLR_TOTAL_ICMS_ST    NUMBER(20,4),
             VLR_TOT_NOTA         NUMBER(20,4),
             COD_NATUREZA_OP      VARCHAR2(250),
             QTD_VOL              NUMBER(12,4),
             PESO_BRT             NUMBER(12,4),
             PESO_LIQ             NUMBER(12,4),
             VLR_PRODUTO          NUMBER(20,4),
             VL_SERV              NUMBER(20,4),
             IND_PGTO             VARCHAR2(1),
             IND_PROC             VARCHAR2(1),
             NUM_PROC             VARCHAR2(250),
             TXT                  VARCHAR2(255),
             HORA_ENT_SAI         TIMESTAMP(6),
             TIP_EMISS_NFE        NUMBER(1),
             FIN_NFE              NUMBER(1),
             PROC_EMISS_NFE       NUMBER(1),
             IND_FRT              NUMBER(1),
             TIPO_PEDIDO          VARCHAR2(6),
             COD_MEDIDA           VARCHAR2(6),
             UTILIZATION_MODE     NUMBER(1),
             ALEATORY_NUMBER      VARCHAR2(8),
             NFE_DOC_TYPE         NUMBER(8),
             SERIE_DOCFIS         VARCHAR2(20),
             nfe_detail_tbl       "OBJ_FM_NFE_DOCDTL_TBL",
             nfe_issuer_rec       "OBJ_FM_NFE_ENTITY_REC",
             nfe_addressee_rec    "OBJ_FM_NFE_ENTITY_REC",
             nfe_transporter_rec  "OBJ_FM_NFE_ENTITY_REC",
constructor function "OBJ_FM_NFE_DOCHDR_REC"
(
             NUM_DOCFIS           NUMBER     DEFAULT NULL,
             DATA_EMISSAO         DATE       DEFAULT NULL,
             DT_RECEBIMENTO       DATE       DEFAULT NULL,
             VLR_FRETE            NUMBER     DEFAULT NULL,
             VLR_SEGURO           NUMBER     DEFAULT NULL,
             VLR_OUTRAS           NUMBER     DEFAULT NULL,
             PLACA_VEICULO        VARCHAR2   DEFAULT NULL,
             VLR_TOTAL_PIS        NUMBER     DEFAULT NULL,
             VLR_TOTAL_COFINS     NUMBER     DEFAULT NULL,
             VLR_TOTAL_ISS        NUMBER     DEFAULT NULL,
             VLR_DESCONTO         NUMBER     DEFAULT NULL,
             VLR_TOTAL_ICMS       NUMBER     DEFAULT NULL,
             VLR_BASE_ICMS        NUMBER     DEFAULT NULL,
             VLR_BASE_ISS         NUMBER     DEFAULT NULL,
             VLR_BASE_ICMS_ST     NUMBER     DEFAULT NULL,
             VLR_TOTAL_ICMS_ST    NUMBER     DEFAULT NULL,
             VLR_TOT_NOTA         NUMBER     DEFAULT NULL,
             COD_NATUREZA_OP      VARCHAR2   DEFAULT NULL,
             QTD_VOL              NUMBER     DEFAULT NULL,
             PESO_BRT             NUMBER     DEFAULT NULL,
             PESO_LIQ             NUMBER     DEFAULT NULL,
             VLR_PRODUTO          NUMBER     DEFAULT NULL,
             VL_SERV              NUMBER     DEFAULT NULL,
             IND_PGTO             VARCHAR2   DEFAULT NULL,
             IND_PROC             VARCHAR2   DEFAULT NULL,
             NUM_PROC             VARCHAR2   DEFAULT NULL,
             TXT                  VARCHAR2   DEFAULT NULL,
             HORA_ENT_SAI         TIMESTAMP  DEFAULT NULL,
             TIP_EMISS_NFE        NUMBER     DEFAULT NULL,
             FIN_NFE              NUMBER     DEFAULT NULL,
             PROC_EMISS_NFE       NUMBER     DEFAULT NULL,
             IND_FRT              NUMBER     DEFAULT NULL,
             TIPO_PEDIDO          VARCHAR2   DEFAULT NULL,
             COD_MEDIDA           VARCHAR2   DEFAULT NULL,
             UTILIZATION_MODE     NUMBER     DEFAULT NULL,
             ALEATORY_NUMBER      VARCHAR2   DEFAULT NULL,
             NFE_DOC_TYPE         NUMBER     DEFAULT NULL,
             SERIE_DOCFIS         VARCHAR2   DEFAULT NULL,
             nfe_detail_tbl       "OBJ_FM_NFE_DOCDTL_TBL"  DEFAULT NULL,
             nfe_issuer_rec       "OBJ_FM_NFE_ENTITY_REC"  DEFAULT NULL,
             nfe_addressee_rec    "OBJ_FM_NFE_ENTITY_REC"  DEFAULT NULL,
             nfe_transporter_rec  "OBJ_FM_NFE_ENTITY_REC"  DEFAULT NULL
) return self as result
)
/
 
create or replace type body "OBJ_FM_NFE_DOCHDR_REC" is
constructor function "OBJ_FM_NFE_DOCHDR_REC"
(
        NUM_DOCFIS             NUMBER     DEFAULT NULL,
        DATA_EMISSAO           DATE       DEFAULT NULL,
        DT_RECEBIMENTO         DATE       DEFAULT NULL,
        VLR_FRETE              NUMBER     DEFAULT NULL,
        VLR_SEGURO             NUMBER     DEFAULT NULL,
        VLR_OUTRAS             NUMBER     DEFAULT NULL,
        PLACA_VEICULO          VARCHAR2   DEFAULT NULL,
        VLR_TOTAL_PIS          NUMBER     DEFAULT NULL,
        VLR_TOTAL_COFINS       NUMBER     DEFAULT NULL,
        VLR_TOTAL_ISS          NUMBER     DEFAULT NULL,
        VLR_DESCONTO           NUMBER     DEFAULT NULL,
        VLR_TOTAL_ICMS         NUMBER     DEFAULT NULL,
        VLR_BASE_ICMS          NUMBER     DEFAULT NULL,
        VLR_BASE_ISS           NUMBER     DEFAULT NULL,
        VLR_BASE_ICMS_ST       NUMBER     DEFAULT NULL,
        VLR_TOTAL_ICMS_ST      NUMBER     DEFAULT NULL,
        VLR_TOT_NOTA           NUMBER     DEFAULT NULL,
        COD_NATUREZA_OP        VARCHAR2   DEFAULT NULL,
        QTD_VOL                NUMBER     DEFAULT NULL,
        PESO_BRT               NUMBER     DEFAULT NULL,
        PESO_LIQ               NUMBER     DEFAULT NULL,
        VLR_PRODUTO            NUMBER     DEFAULT NULL,
        VL_SERV                NUMBER     DEFAULT NULL,
        IND_PGTO               VARCHAR2   DEFAULT NULL,
        IND_PROC               VARCHAR2   DEFAULT NULL,
        NUM_PROC               VARCHAR2   DEFAULT NULL,
        TXT                    VARCHAR2   DEFAULT NULL,
        HORA_ENT_SAI           TIMESTAMP  DEFAULT NULL,
        TIP_EMISS_NFE          NUMBER     DEFAULT NULL,
        FIN_NFE                NUMBER     DEFAULT NULL,
        PROC_EMISS_NFE         NUMBER     DEFAULT NULL,
        IND_FRT                NUMBER     DEFAULT NULL,
        TIPO_PEDIDO            VARCHAR2   DEFAULT NULL,
        COD_MEDIDA             VARCHAR2   DEFAULT NULL,
        UTILIZATION_MODE       NUMBER     DEFAULT NULL,
        ALEATORY_NUMBER        VARCHAR2   DEFAULT NULL,
        NFE_DOC_TYPE           NUMBER     DEFAULT NULL,
        SERIE_DOCFIS           VARCHAR2   DEFAULT NULL,
        nfe_detail_tbl         "OBJ_FM_NFE_DOCDTL_TBL"  DEFAULT NULL,
        nfe_issuer_rec         "OBJ_FM_NFE_ENTITY_REC"  DEFAULT NULL,
        nfe_addressee_rec      "OBJ_FM_NFE_ENTITY_REC"  DEFAULT NULL,
        nfe_transporter_rec    "OBJ_FM_NFE_ENTITY_REC"  DEFAULT NULL
) return self as result is
begin
        self.num_docfis              := num_docfis;
        self.data_emissao            := data_emissao;
        self.dt_recebimento          := dt_recebimento;
        self.vlr_frete               := vlr_frete;
        self.vlr_seguro              := vlr_seguro;
        self.vlr_outras              := vlr_outras;
        self.placa_veiculo           := placa_veiculo;
        self.vlr_total_pis           := vlr_total_pis;
        self.vlr_total_cofins        := vlr_total_cofins;
        self.vlr_total_iss           := vlr_total_iss;
        self.vlr_desconto            := vlr_desconto;
        self.vlr_total_icms          := vlr_total_icms;
        self.vlr_base_icms           := vlr_base_icms;
        self.vlr_base_iss            := vlr_base_iss;
        self.vlr_base_icms_st        := vlr_base_icms_st;
        self.vlr_total_icms_st       := vlr_total_icms_st;
        self.vlr_tot_nota            := vlr_tot_nota;
        self.cod_natureza_op         := cod_natureza_op;
        self.qtd_vol                 := qtd_vol;
        self.peso_brt                := peso_brt;
        self.peso_liq                := peso_liq;
        self.vlr_produto             := vlr_produto;
        self.vl_serv                 := vl_serv;
        self.ind_pgto                := ind_pgto;
        self.ind_proc                := ind_proc;
        self.num_proc                := num_proc;
        self.txt                     := txt;
        self.hora_ent_sai            := hora_ent_sai;
        self.tip_emiss_nfe           := tip_emiss_nfe;
        self.fin_nfe                 := fin_nfe;
        self.proc_emiss_nfe          := proc_emiss_nfe;
        self.ind_frt                 := ind_frt;
        self.tipo_pedido             := tipo_pedido;
        self.cod_medida              := cod_medida;
        self.utilization_mode        := utilization_mode;
        self.aleatory_number         := aleatory_number;
        self.nfe_doc_type            := nfe_doc_type;
        self.serie_docfis            := serie_docfis;
        self.nfe_detail_tbl          := nfe_detail_tbl;
        self.nfe_issuer_rec          := nfe_issuer_rec;
        self.nfe_addressee_rec       := nfe_addressee_rec;
        self.nfe_transporter_rec     := nfe_transporter_rec;
  return;
end;
end;
/