DROP Type WAC_UPDATE_TBL FORCE
/
DROP Type WAC_UPDATE_REC FORCE
/
CREATE OR REPLACE TYPE WAC_UPDATE_REC AS OBJECT
(
    item              VARCHAR2(25),
    location          NUMBER(10),
    loc_type          VARCHAR2(1),
    icms_unit_amt     NUMBER(20,4),
    icmsst_unit_amt   NUMBER(20,4),
    icmsste_unit_amt  NUMBER(20,4),
    new_av_cost       NUMBER(20,4)
)
/
CREATE OR REPLACE TYPE WAC_UPDATE_TBL AS TABLE OF WAC_UPDATE_REC

/