drop type "OBJ_MS_RFM_ErrorDtl_TBL" force;
/

drop type "OBJ_MS_RFM_ErrorDtl_REC" force;
/

create or replace 
TYPE  "OBJ_MS_RFM_ErrorDtl_REC" AS OBJECT(
        ERROR_ID                NUMBER(4),
        ERROR_DESC              VARCHAR2(1000),
        TRANSACTION_DATE        TIMESTAMP,
constructor function "OBJ_MS_RFM_ErrorDtl_REC"
(
        ERROR_ID                NUMBER         DEFAULT NULL,
        ERROR_DESC              VARCHAR2       DEFAULT NULL,
        TRANSACTION_DATE        TIMESTAMP      DEFAULT NULL
) return self as result
)
/

create or replace type body "OBJ_MS_RFM_ErrorDtl_REC" is
constructor function "OBJ_MS_RFM_ErrorDtl_REC"
(
        ERROR_ID                NUMBER         DEFAULT NULL,
        ERROR_DESC              VARCHAR2       DEFAULT NULL,
        TRANSACTION_DATE        TIMESTAMP      DEFAULT NULL
) return self as result is
begin
        self.error_id          := error_id;
        self.error_desc        := error_desc;
        self.transaction_date  := transaction_date;
return;
end;
end;
/

create or replace TYPE "OBJ_MS_RFM_ErrorDtl_TBL" AS TABLE OF "OBJ_MS_RFM_ErrorDtl_REC";
/

drop type "OBJ_MS_RFM_NFE_REC" force;
/

create or replace
TYPE "OBJ_MS_RFM_NFE_REC" AS OBJECT (
        FISCAL_DOC_ID           NUMBER(10),
        NFE_ACCESS_KEY          VARCHAR2(44),
        NFE_PROTOCOL            NUMBER(15),
        NFE_DANFE_URL           VARCHAR2(1000),
        STATUS                  VARCHAR2(6),
        errorDtl_tbl            "OBJ_MS_RFM_ErrorDtl_TBL",
constructor function "OBJ_MS_RFM_NFE_REC"
(
        FISCAL_DOC_ID           NUMBER         DEFAULT NULL,
        NFE_ACCESS_KEY          VARCHAR2       DEFAULT NULL,
        NFE_PROTOCOL            NUMBER         DEFAULT NULL,
        NFE_DANFE_URL           VARCHAR2       DEFAULT NULL,
        STATUS                  VARCHAR2       DEFAULT NULL,
        errorDtl_tbl            "OBJ_MS_RFM_ErrorDtl_TBL"  DEFAULT NULL 
) return self as result
)
/

create or replace type body "OBJ_MS_RFM_NFE_REC" is
constructor function "OBJ_MS_RFM_NFE_REC"
(
        FISCAL_DOC_ID           NUMBER         DEFAULT NULL,
        NFE_ACCESS_KEY          VARCHAR2       DEFAULT NULL,
        NFE_PROTOCOL            NUMBER         DEFAULT NULL,
        NFE_DANFE_URL           VARCHAR2       DEFAULT NULL,
        STATUS                  VARCHAR2       DEFAULT NULL,
        errorDtl_tbl            "OBJ_MS_RFM_ErrorDtl_TBL"  DEFAULT NULL
) return self as result is
begin
        self.fiscal_doc_id         := fiscal_doc_id;
        self.nfe_access_key        := nfe_access_key;
        self.nfe_protocol          := nfe_protocol;
        self.nfe_danfe_url         := nfe_danfe_url;
        self.status                := status;
        self.errorDtl_tbl          := errorDtl_tbl;
return;
end;
end;
/

---
