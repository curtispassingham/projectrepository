DROP TYPE "OBJ_RMAItem_REC" FORCE ;

CREATE OR REPLACE TYPE "OBJ_RMAItem_REC" AS OBJECT (
  RMA_ID                NUMBER(10),
  RMA_DETAIL_ID         NUMBER(4),
  QTY                   NUMBER(12),
  ITEM                  VARCHAR2(25),
  RMA_REASON            VARCHAR2(10),
  ORIGIN_DOCUMENT       NUMBER(10),
  FROM_DISPOSITION      VARCHAR2(4),
  TO_DISPOSITION        VARCHAR2(4),
  UNIT_COST             NUMBER(20,4),
constructor function "OBJ_RMAItem_REC"
( 
  RMA_ID                NUMBER    DEFAULT NULL
  ,RMA_DETAIL_ID        NUMBER    DEFAULT NULL
  ,QTY                  NUMBER    DEFAULT NULL
  ,ITEM                 VARCHAR2  DEFAULT NULL
  ,RMA_REASON           VARCHAR2  DEFAULT NULL
  ,ORIGIN_DOCUMENT      NUMBER    DEFAULT NULL
  ,FROM_DISPOSITION     VARCHAR2  DEFAULT NULL
  ,TO_DISPOSITION       VARCHAR2  DEFAULT NULL
  ,UNIT_COST            NUMBER    DEFAULT NULL
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "OBJ_RMAItem_REC" is
constructor function "OBJ_RMAItem_REC"
(
  RMA_ID                NUMBER    DEFAULT NULL
  ,RMA_DETAIL_ID        NUMBER    DEFAULT NULL
  ,QTY                  NUMBER    DEFAULT NULL
  ,ITEM                 VARCHAR2  DEFAULT NULL
  ,RMA_REASON           VARCHAR2  DEFAULT NULL
  ,ORIGIN_DOCUMENT      NUMBER    DEFAULT NULL
  ,FROM_DISPOSITION     VARCHAR2  DEFAULT NULL
  ,TO_DISPOSITION       VARCHAR2  DEFAULT NULL
  ,UNIT_COST            NUMBER    DEFAULT NULL
) return self as result is
begin
  self.rma_id                  := rma_id;
  self.rma_detail_id           := rma_detail_id;
  self.qty                     := qty;
  self.item                    := item;
  self.rma_reason              := rma_reason;
  self.origin_document         := origin_document;
  self.from_disposition        := from_disposition;
  self.to_disposition          := to_disposition;
  self.unit_cost               := unit_cost;
  return;
end;
end;
/
 
DROP TYPE "OBJ_RMAItem_TBL" FORCE;


CREATE OR REPLACE TYPE "OBJ_RMAItem_TBL" AS TABLE OF "OBJ_RMAItem_REC"
/

DROP TYPE "OBJ_RMAITEMDesc_REC" FORCE;
/

CREATE OR REPLACE TYPE "OBJ_RMAITEMDesc_REC" as object (
  RMA_ID                     NUMBER(10),
  LOC_TYPE                   VARCHAR2(1),
  LOCATION                   NUMBER(10),
  CUST_ID                    VARCHAR2(10),
  CUST_NAME                  VARCHAR2(120),
  RMA_DATE                   DATE,
  AUTH_BY                    VARCHAR2(50),
  RMA_STATUS                 VARCHAR2(1),
  FISCAL_DOC_ID              NUMBER(10),
  CLOSE_DATE                 DATE,
  PRO_NBR                    VARCHAR2(18),
  CNPJ                       VARCHAR2(14),
  CPF                        VARCHAR2(11),
  ADDR_1                     VARCHAR2(240),
  ADDR_2                     VARCHAR2(240),
  ADDR_3                     VARCHAR2(240),
  NEIGHBORHOOD               VARCHAR2(120),
  CITY                       NUMBER(7),
  STATE                      VARCHAR2(3),
  POSTAL_CODE                VARCHAR2(30),
  COUNTRY_ID                 VARCHAR2(3),
  SUFRAMA                    VARCHAR2(250),
  IE                         VARCHAR2(250),
  RMADtl_TBL                "OBJ_RMAItem_TBL",
constructor function "OBJ_RMAITEMDesc_REC" 
(
  RMA_ID                     NUMBER    DEFAULT NULL
  ,LOC_TYPE                  VARCHAR2  DEFAULT NULL
  ,LOCATION                  NUMBER    DEFAULT NULL
  ,CUST_ID                   VARCHAR2  DEFAULT NULL
  ,CUST_NAME                 VARCHAR2  DEFAULT NULL
  ,RMA_DATE                  DATE      DEFAULT NULL
  ,AUTH_BY                   VARCHAR2  DEFAULT NULL
  ,RMA_STATUS                VARCHAR2  DEFAULT NULL
  ,FISCAL_DOC_ID             NUMBER    DEFAULT NULL
  ,CLOSE_DATE                DATE      DEFAULT NULL
  ,PRO_NBR                   VARCHAR2  DEFAULT NULL
  ,CNPJ                      VARCHAR2  DEFAULT NULL
  ,CPF                       VARCHAR2  DEFAULT NULL
  ,ADDR_1                    VARCHAR2  DEFAULT NULL
  ,ADDR_2                    VARCHAR2  DEFAULT NULL
  ,ADDR_3                    VARCHAR2  DEFAULT NULL
  ,NEIGHBORHOOD              VARCHAR2  DEFAULT NULL
  ,CITY                      NUMBER    DEFAULT NULL
  ,STATE                     VARCHAR2  DEFAULT NULL
  ,POSTAL_CODE               VARCHAR2  DEFAULT NULL
  ,COUNTRY_ID                VARCHAR2  DEFAULT NULL
  ,SUFRAMA                   VARCHAR2  DEFAULT NULL
  ,IE                        VARCHAR2  DEFAULT NULL
  ,RMADtl_TBL                "OBJ_RMAItem_TBL" DEFAULT NULL
) return self as result
)
/
CREATE OR REPLACE TYPE BODY "OBJ_RMAITEMDesc_REC" is
constructor function "OBJ_RMAITEMDesc_REC"
(
   
  RMA_ID                     NUMBER    DEFAULT NULL
  ,LOC_TYPE                  VARCHAR2  DEFAULT NULL
  ,LOCATION                  NUMBER    DEFAULT NULL
  ,CUST_ID                   VARCHAR2  DEFAULT NULL
  ,CUST_NAME                 VARCHAR2  DEFAULT NULL
  ,RMA_DATE                  DATE      DEFAULT NULL
  ,AUTH_BY                   VARCHAR2  DEFAULT NULL
  ,RMA_STATUS                VARCHAR2  DEFAULT NULL  
  ,FISCAL_DOC_ID             NUMBER    DEFAULT NULL
  ,CLOSE_DATE                DATE      DEFAULT NULL
  ,PRO_NBR                   VARCHAR2  DEFAULT NULL
  ,CNPJ                      VARCHAR2  DEFAULT NULL
  ,CPF                       VARCHAR2  DEFAULT NULL
  ,ADDR_1                    VARCHAR2  DEFAULT NULL
  ,ADDR_2                    VARCHAR2  DEFAULT NULL
  ,ADDR_3                    VARCHAR2  DEFAULT NULL
  ,NEIGHBORHOOD              VARCHAR2  DEFAULT NULL
  ,CITY                      NUMBER    DEFAULT NULL
  ,STATE                     VARCHAR2  DEFAULT NULL
  ,POSTAL_CODE               VARCHAR2  DEFAULT NULL
  ,COUNTRY_ID                VARCHAR2  DEFAULT NULL
  ,SUFRAMA                   VARCHAR2  DEFAULT NULL
  ,IE                        VARCHAR2  DEFAULT NULL
  ,RMADtl_TBL                "OBJ_RMAItem_TBL" DEFAULT NULL
) return self as result is
begin
  self.rma_id             := rma_id;
  self.loc_type           := loc_type;
  self.location           := location;
  self.cust_id            := cust_id;
  self.cust_name          := cust_name;
  self.rma_date           := rma_date;
  self.auth_by            := auth_by;
  self.rma_status         := rma_status;
  self.fiscal_doc_id      := fiscal_doc_id;
  self.close_date         := close_date;
  self.pro_nbr            := pro_nbr;
  self.cnpj               := cnpj;
  self.cpf                := cpf;
  self.addr_1             := addr_1;
  self.addr_2             := addr_2;
  self.addr_3             := addr_3;
  self.neighborhood       := neighborhood;
  self.city               := city;
  self.state              := state;
  self.postal_code        := postal_code;
  self.country_id         := country_id;
  self.suframa            := suframa;
  self.ie                 := ie;
  self.rmadtl_tbl         := rmadtl_tbl;
  return;
end;
end;
/
 
