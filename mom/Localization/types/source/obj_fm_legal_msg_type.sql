drop type fm_legal_msg_type force
/

create or replace TYPE fm_legal_msg_type
        as table of VARCHAR2(1000)
/