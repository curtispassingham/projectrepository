#!/usr/bin/ksh
###############################################################################
# Purpose: import_SPED.ksh is developed to make a call to the package         #
#          FM_SPED_SQL.GET_SPED_INFO to insert records into the newly created #
#          tables - FM_SPED_FISCAL_DOC_HEADER and FM_SPED_FISCAL_DOC_DETAIL.  #
#          It is recommended to run this batch job on a daily basis due to    #
#          performance impacts. This batch has a pre-dependency on the RFM    #
#          Financial postings batch - fm_trandatapost.pc, so once Financial   #
#          postings batch program completes and sets the Nota Fiscal status to#
#          �C�ompleted only after that SPED insert batch should be triggered  #
#          to fetch all such �C�ompleted NFs from the RFM tables.             #
# Author : SHOBANA RAM                                                        #
# Created: 18-MAY-2010                                                        #
###############################################################################

export OK=0
export FATAL=1
export RC=$OK
export USER_PASS=$1

SCRIPT_HOME=$MMHOME/oracle/proc/bin
TMP_FILE=/tmp/batch_spedinsrt_`date +'%b_%d-%H:%M:%S'`.tmp.$$
LOG_FILE=$MMHOME/log/batch_spedinsrt_`date +'%b_%d-%H:%M:%S'`.log
SYS_DATE=$(echo `date +'%d-%b-%y'`)


# Local Function implementation #
function write_log
{
   MSG=$1
   NOW=`date "+%Y%m%d-%H:%M:%S"`
   if [[ ${OPT_T} = 1 ]]
   then
      echo "${NOW} - ${MSG}" | tee -a ${LOG_FILE}
   else
      echo "${NOW} - ${MSG}" >> ${LOG_FILE}
   fi
}

function clean_exit
{
   if [[ $# < 1 ]]
   then
      MSG="Usage: clean_exit 'ret_code' ['message']"
      write_log ${MSG}
      exit ${FATAL};
   fi

   RC=$1
   MSG=$2

   rm -f ${TMP_FILE}

   if [[ ! -z ${MSG} ]]
   then
      write_log "${MSG}"
   fi
   write_log "Program terminating with return code = ${RC}"
   exit $RC
}

### Parse the command line
while getopts ":th" CMD 
do
   if [[ $CMD = t ]]
   then
      OPT_T=1
   fi
   if [[ $CMD = h ]]
   then
      echo "Usage: $0 [-t|h] <user/passwd>"
      echo "-t: this option shows all messages being written"
      echo "    to the log also on the terminal output"
      echo "-h: shows this information"
      clean_exit ${OK}
   fi
done

### Test for the number of input arguments
if [[ $# < 1 ]]
then
   echo "Usage: $0 [-t] <user/passwd>."
   clean_exit $FATAL "`date "+%H:%M:%S"` - Usage: $0 [-t] <user/passwd>."
fi


# Writes the starting date/time
write_log "Program Starting..."

#runs the selected process
${ORACLE_HOME}/bin/sqlplus -s ${USER_PASS} >${TMP_FILE} <<EOF
set pause off
set verify off
set pagesize 0
set linesize 2000
set timing off
set heading off
set feedback off
set serveroutput on
set termout on

DECLARE
   L_error_message VARCHAR2(255);
BEGIN
   if (FM_SPED_SQL.GET_SPED_INFO(L_error_message,
                                   '`date +'%d-%b-%y-%H:%M:%S'`') = FALSE)
   then
      DBMS_OUTPUT.PUT_LINE(L_error_message);
      ROLLBACK;
   else
 COMMIT;
   end if;

END;
/
EOF
RC=$?

if (( ${RC} != ${OK} ))
then
    write_log "Error found running the SPED insertion process for the date : ${SYS_DATE}"
    if [[ -f ${TMP_FILE} ]]
    then
       cat ${TMP_FILE} >>${LOG_FILE}
    fi
    clean_exit ${FATAL}
fi

# Return code was OK, but some error message was written to the output file
if (( `grep "^ORA" ${TMP_FILE} | wc -l` > 0 || `grep "^@" ${TMP_FILE} | wc -l` > 0 ))
then
   write_log "Error found running the SPED insertion process for the date : ${SYS_DATE}"
   cat ${TMP_FILE} >>${LOG_FILE}
   clean_exit ${FATAL}
fi

clean_exit ${OK}

