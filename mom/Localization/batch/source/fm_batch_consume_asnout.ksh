#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  fm_batch_consume_asnout.ksh
#
#  Desc:  UNIX shell script to perform RMS's asnout consume logic 
#         for all ASNOUT messages staged during the night batch by RFM.
#-------------------------------------------------------------------------
pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
ERRINDFILE=err.ind

# Initialize number of parallel threads
SLOTS=0

CURR_THREADS=1
OK=0
FATAL=255

USAGE="Usage: `basename $0` [-p <# parallel threads>] <connect> \n
<# parallel threads> is the number of fm_batch_consume_asnout threads to run in parallel.\n
The default is the value on RESTART_CONTROL.NUM_THREADS.\n"

#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the  messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`
    
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function Name: BATCH_CONSUME_THREAD
# Purpose      : Calls the package FM_ASNOUT_CONSUME_SQL.BATCH_CONSUME_THREAD.
#-------------------------------------------------------------------------

function BATCH_CONSUME_THREAD
{
   threadVal=$1
   sqlTxt="
      DECLARE
         L_str_error_tst   VARCHAR2(1) := NULL;

         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT FM_ASNOUT_CONSUME_SQL.BATCH_CONSUME_THREAD(:GV_script_error,
                                                           ${threadVal},
                                                           ${SLOTS})then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "BATCH_CONSUME_THREAD Thread: $threadVal Failed" >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "Thread ${threadVal} - Successfully Completed"
      return ${OK}
   fi
}

#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------


while getopts :p: CMD
   do
      case $CMD in
         p)  SLOTS=$OPTARG;;
         *)  echo $0: Unknown option $OPTARG
             echo $USAGE
             exit 1;;
      esac
   done

shift $OPTIND-1

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1


# Test for the number of threads
# If the user does not designate the number of parallel threads, then default to RESTART_CONTROL.NUM_THREADS
if [ $SLOTS -eq 0 -o $SLOTS -lt 1 ]
   then
      SLOTS=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
      set pause off
      set echo off
      set heading off
      set feedback off
      set verify off
      set pages 0
      select num_threads
        from restart_control
       where UPPER(program_name) = UPPER('fm_batch_consume_asnout');
EOF`
fi


USER=${CONNECT%/*}

LOG_MESSAGE "Started by ${USER}"

#Calls to the function based on the thread count
while [ $CURR_THREADS -le $SLOTS ]
do
BATCH_CONSUME_THREAD $CURR_THREADS &  
CURR_THREADS=`expr $CURR_THREADS + 1 `
done

#Wait until all the threads are completed
wait

exit 0
