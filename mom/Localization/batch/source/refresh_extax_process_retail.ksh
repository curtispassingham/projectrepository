#! /bin/ksh
#-------------------------------------------------------------------------
#  Desc:  UNIX shell script to call the tax engine for seed and refresh in a 
#  multi-threaded manner.
#-------------------------------------------------------------------------

############################################################

pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
ERRINDFILE=err.ind

OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the  messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code   := 0;
      EXEC :GV_script_error  := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}

function PROCESS
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT L10N_BR_EXTAX_MAINT_SQL.PROCESS(:GV_script_error, ${proc_id}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}


   if [[ $? -ne ${OK} ]]; then
      echo "L10N_BR_EXTAX_MAINT_SQL.PROCESS Thread: $proc_id Failed" >>${ERRORFILE}
      return ${FATAL}
   else
      LOG_MESSAGE "Thread ${proc_id} - Successfully Completed"
      return ${OK}
   fi
}

############################################################

# Initialize number of parallel threads
SLOTS=0
DEFAULT_SLOTS=4

USAGE="Usage: `basename $0` [-p <# parallel threads>] <connect> \n
<# parallel threads> is the number of threads to run in parallel.\n
The default is 1.\n
"

# Parse the command line
while getopts :p: CMD
   do
      case $CMD in
         p)  SLOTS=$OPTARG;;
         *)  echo $0: Unknown option $OPTARG
             echo $USAGE
             exit 1;;
      esac
   done

shift $OPTIND-1

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1

USER=${CONNECT%/*}

# Test for the number of threads
# If the user does not designate the number of parallel threads, then use the default
if [ $SLOTS -eq 0 -o $SLOTS -lt 1 ]
   then
      SLOTS=$DEFAULT_SLOTS
fi

# Set filename to contain this runs process_id list
PROC_IDS=tax_procs

# Set filename to flag any failed executions

failed=refresh_extax_process_retail.$$
[ -f $failed ] && rm $failed

# If this script is killed, cleanup
trap "kill -15 0; rm -f $failed -f $PROC_IDS; exit 15" 1 2 3 15

if [ ! -a tax_procs ]; then
# Get the list of process_ids to be processed
$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$PROC_IDS
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
COLUMN reccount noprint
select process_id, count(*) reccount from l10n_br_extax_stg_retail
  group by process_id
  order by reccount desc;
EOF
fi

# Check for any Oracle errors from the SQLPLUS process
[ `grep "^ORA-" $PROC_IDS | wc -l` -gt 0 ] && exit 1

LOG_MESSAGE "Executing file refresh_extax_process_retail.ksh"
# Run the rplext program in parallel each processing a dept
cat $PROC_IDS | while read proc_id
do
   if [ `jobs | wc -l` -lt $SLOTS ]
   then
      (
      LOG_MESSAGE "Thread ${proc_id} - Started by ${USER}"
      PROCESS
      ) &
   else
      # Loop until a thread becomes available
      while [ `jobs | wc -l` -ge $SLOTS ]
      do
         : # Null command
      done
      (
      LOG_MESSAGE "Thread ${proc_id} - Started by ${USER}"
      PROCESS
      ) &
   fi
done

# Wait for all of the threads to complete
wait

# Remove the list of depts file
rm $PROC_IDS

# Check for the existence of a failed file from any of the threads
# and determine exit status
if [ -f $failed ]
then
   rm $failed
   exit 1
else
   LOG_MESSAGE "Completed refresh_extax_process_retail.ksh"
   exit 0
fi
