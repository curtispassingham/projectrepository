#--- Object file groupings.

L10NBRFISDNLD_PC_OBJS = l10nbrfisdnld.o

L10NBRFRECLSPRG_PC_OBJS = l10nbrfreclsprg.o

FMTRANDATA_PC_OBJS = fmtrandata.o

FMFINPOST_PC_OBJS = fmfinpost.o

FMPURGE_PC_OBJS = fmpurge.o

FMEDINF_PC_OBJS = fmedinf.o

RMS_C_SRCS = 

RMS_PC_SRCS = \
	$(L10NBRFISDNLD_PC_OBJS:.o=.pc) \
	$(L10NBRFRECLSPRG_PC_OBJS:.o=.pc) \
	$(FMTRANDATA_PC_OBJS:.o=.pc) \
	$(FMFINPOST_PC_OBJS:.o=.pc) \
	$(FMPURGE_PC_OBJS:.o=.pc) \
      $(FMEDINF_PC_OBJS:.o=.pc)


#--- Target groupings.

RMS_GEN_EXECUTABLES = \
	l10nbrfisdnld \
	l10nbrfreclsprg \
	fmtrandata \
	fmfinpost \
	fmpurge \
      fmedinf 

RMS_EXECUTABLES= \
	$(RMS_GEN_EXECUTABLES)

RMS_LIBS = 

RMS_INCLUDES = 

#--- External systems

RDM_RMS_C_SRCS = 

RDM_RMS_PC_SRCS =

RDM_RMS_GEN_EXECUTABLES =

RDM_RMS_EXECUTABLES = \
	$(RDM_RMS_GEN_EXECUTABLES)

RDM_RMS_LIBS = 

RDM_RMS_INCLUDES = 

#--- Put list of all targets for l10n_br_rms.mk here

RMS_TARGETS = \
	$(RMS_EXECUTABLES) \
	$(RMS_EXECUTABLES:=.lint) \
	$(RMS_C_SRCS:.c=.o) \
	$(RMS_PC_SRCS:.pc=.o) \
	$(RMS_PC_SRCS:.pc=.c) \
	$(RMS_LIBS) \
	$(RMS_INCLUDES) \
	$(RDM_RMS_EXECUTABLES) \
	$(RDM_RMS_EXECUTABLES:=.lint) \
	$(RDM_RMS_SRCS:.c=.o) \
	$(RDM_RMS_PC_SRCS:.pc=.o) \
	$(RDM_RMS_PC_SRCS:.pc=.c) \
	$(RDM_RMS_LIBS) \
	$(RDM_RMS_INCLUDES) \
	rms-all \
	rms \
	rms-ALL \
	rms-libs \
	rms-includes \
	rms-clobber \
	rms-libchange \
	rms-clean \
	rms-install \
	rms-lint \
	rms-depend \
	rdm-rms-all \
	rdm-rms \
	rdm-rms-ALL \
	rdm-rms-libs \
	rdm-rms-includes \
	rdm-rms-clobber \
	rdm-rms-libchange \
	rdm-rms-clean \
	rdm-rms-install \
	rdm-rms-lint \
	rdm-rms-depend \
	rms-rdm-all \
	rms-rdm \
	rms-rdm-ALL \
	rms-rdm-libs \
	rms-rdm-includes \
	rms-rdm-clobber \
	rms-rdm-libchange \
	rms-rdm-clean \
	rms-rdm-install \
	rms-rdm-lint \
	rms-rdm-depend