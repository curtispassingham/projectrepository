include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS =
PRODUCT_LDFLAGS =
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/lib/src/l10n_rmslib_include.mk

#--- Standard targets.

all: l10nrmslib-all

libs: l10nrmslib-libs

includes: l10nrmslib-includes

clean: l10nrmslib-clean FORCE

clobber: l10nrmslib-clobber FORCE

install: l10nrmslib-install FORCE

lint: l10nrmslib-lint FORCE

depend: l10nrmslib-depend


l10nrmslib-all: l10nrmslib-libs $(L10NRMSLIB_EXECUTABLES)

l10nrmslib-libs: l10nrmslib-includes $(L10NRMSLIB_LIBS)

l10nrmslib-includes: $(L10NRMSLIB_INCLUDES)

l10nrmslib-clean: FORCE
	rm -f $(L10NRMSLIB_C_SRCS:.c=.o)
	rm -f $(L10NRMSLIB_C_SRCS:.c=.ln)
	rm -f $(L10NRMSLIB_PC_SRCS:.pc=.o)
	rm -f $(L10NRMSLIB_PC_SRCS:.pc=.c)
	rm -f $(L10NRMSLIB_PC_SRCS:.pc=.lis)
	rm -f $(L10NRMSLIB_PC_SRCS:.pc=.ln)
	for f in $(L10NRMSLIB_LIBS); \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.lint; \
		rm -f $$n; \
	done

l10nrmslib-clobber: l10nrmslib-clean FORCE
	rm -f $(L10NRMSLIB_EXECUTABLES)
	rm -f $(L10NRMSLIB_LIBS)
	for f in $(L10NRMSLIB_LIBS); \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

l10nrmslib-install: FORCE
	-@L10NRMSLIB_INCLUDES="$(L10NRMSLIB_INCLUDES)"; \
	for f in $$L10NRMSLIB_INCLUDES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_SRC)/$$f ] || [ $$f -nt $(RETEK_LIB_SRC)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_SRC)/$$f"; \
				cp $$f $(RETEK_LIB_SRC)/$$f; \
				chmod $(MASK) $(RETEK_LIB_SRC)/$$f; \
			fi; \
		fi; \
	done; \
	L10NRMSLIB_EXECUTABLES="$(L10NRMSLIB_EXECUTABLES)"; \
	for f in $$L10NRMSLIB_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$f ] || [ $$f -nt $(RETEK_LIB_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_BIN)/$$f"; \
				cp $$f $(RETEK_LIB_BIN)/$$f; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$f; \
			fi; \
		fi; \
	done; \
	for f in $(L10NRMSLIB_LIBS); \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$f ] || [ $$f -nt $(RETEK_LIB_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_BIN)/$$f"; \
				mv $$f $(RETEK_LIB_BIN)/$$f; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$f; \
			fi; \
		fi; \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		if [ -x $$n ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$n ] || [ $$f -nt $(RETEK_LIB_BIN)/$$n ]; \
			then \
				echo "Copying $$n to $(RETEK_LIB_BIN)/$$n"; \
				cp $$n $(RETEK_LIB_BIN)/$$n; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$n; \
			fi; \
		fi; \
	done;

l10nrmslib-lint: FORCE
	-@for l in $(L10NRMSLIB_LIBS); \
	do \
		n=`expr $$l : 'lib\(.*\)\.a' \| $$l : 'lib\(.*\)\.$(SHARED_SUFFIX)'`; \
		$(MAKE) -f ${MMHOME}/oracle/lib/src/l10n_rmslib.mk llib-l$$n.ln; \
	done

l10nrmslib-depend: l10nrmslib.d

l10nrmslib.d: $(L10NRMSLIB_C_SRCS) $(L10NRMSLIB_PC_SRCS) ${MMHOME}/oracle/lib/src/oracle.mk
	@echo "Making dependencies l10nrmslib.d..."
	@$(MAKEDEPEND) $(L10NRMSLIB_C_SRCS) $(L10NRMSLIB_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

${MMHOME}/oracle/lib/src/oracle.mk: ${MMHOME}/oracle/lib/src/oramake FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/retek.mk $@

FORCE:

#--- Executable targets.

#--- Executable lint targets.

#--- Library targets by product.

l10n: libbatchl10n.a libl10n.$(SHARED_SUFFIX)

#--- Library targets.

libbatchl10n.a: $(LIBBATCHL10N_C_OBJS) $(LIBBATCHL10N_PC_OBJS)
	rm -f `basename $@`
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBBATCHL10N_C_OBJS) $(LIBBATCHL10N_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

libl10n.$(SHARED_SUFFIX): demo_rdbms.mk $(LIBCLNTSH) $(LIBL10N_C_OBJS) $(LIBL10N_PC_OBJS)
	$(MAKE) -f rmslib_rdbms.mk extproc_rmslib SHARED_LIBNAME=`basename $@` OBJS="`echo $(LIBL10N_C_OBJS) $(LIBL10N_PC_OBJS) | xargs -n1 basename | tr '\012' ' '`" GENCLNTSH=: GENCLNTSH64=:
	chmod $(MASK) `basename $@`

#--- Library lint targets.

llib-lbatchl10n.ln: $(LIBBATCHL10N_C_OBJS:.o=.c) $(LIBBATCHL10N_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o batchl10n `echo $(LIBBATCHL10N_C_OBJS:.o=.c) $(LIBBATCHL10N_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-ll10n.ln: $(LIBL10N_C_OBJS:.o=.c) $(LIBL10N_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o l10n `echo $(LIBL10N_C_OBJS:.o=.c) $(LIBL10N_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

#--- Oracle library targets that need to be checked.

$(LIBCLNTSH): FORCE
	@if [ ! -r $(LIBCLNTSH) ]; then \
		echo "$(LIBCLNTSH) does not exist."; \
		echo "This must be made by executing $(GENCLNTSH) while logged in as oracle."; \
		if [ -d ${ORACLE_HOME}/lib64 ]; then \
			echo "The 64 bit version of $(LIBCLNTSH) may also need to be built."; \
			echo "This can be done by executing $(GENCLNTSH64) while logged in as oracle."; \
		fi; \
		false; \
	else \
		true; \
	fi

#--- Object dependencies.

include l10nrmslib.d
