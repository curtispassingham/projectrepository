#--- Object file groupings.

LIBL10N_PC_OBJS = \
	l10nlib.o \
	l10nbrfinlib.o \

LIBBATCHL10N_PC_OBJS = \
	$(LIBL10N_PC_OBJS)

L10NRMSLIB_C_OBJS =

L10NRMSLIB_PC_OBJS =

L10NRMSLIB_C_SRCS =

L10NRMSLIB_PC_SRCS = \
	$(L10NRMSLIB_PC_OBJS:.o=.pc)

#--- Target groupings.

L10NRMSLIB_EXECUTABLES =

L10NRMSLIB_LIBS = \
	libbatchl10n.a \
	libl10n.$(SHARED_SUFFIX)

L10NRMSLIB_INCLUDES = \
	l10nlibobj.h \
	l10nlib.h \
	l10nfinlib.h

L10NRMSLIB_LN = \
	llib-lbatchl10n.ln \
	llib-ll10n.ln

#--- Put list of all targets for rmslib.mk here

L10NRMSLIB_TARGETS = \
	$(L10NRMSLIB_EXECUTABLES) \
	$(L10NRMSLIB_EXECUTABLES:=.lint) \
	$(L10NRMSLIB_C_SRCS:.c=.o) \
	$(L10NRMSLIB_PC_SRCS:.pc=.o) \
	$(L10NRMSLIB_PC_SRCS:.pc=.c) \
	$(L10NRMSLIB_LIBS) \
	$(L10NRMSLIB_INCLUDES) \
	$(L10NRMSLIB_LN) \
	l10nrmslib-all \
	l10nrmslib-libs \
	l10nrmslib-includes \
	l10nrmslib-clobber \
	l10nrmslib-clean \
	l10nrmslib-install \
	l10nrmslib-lint \
	l10nrmslib-depend \
	l10nrms \
	l10n
