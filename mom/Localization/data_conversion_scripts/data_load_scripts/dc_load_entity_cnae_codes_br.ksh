#!/bin/ksh

# Common functions library and Configurations
. ./dc_load.cfg
. ./dc_load.lib

# Script variables
scriptName=$0
logFile=${logDir}/$tDay.log


# Table Create scripts
dbcScript1=dbc_create_entity_cnae_codes_br_tab.sql

# Data files used by <function1>
load_entity_cnae_codes_br_data1=dc_entity_cnae_codes_br.dat

# Function list: For each load function defined (with PL/SQL block) list here
#                according to sequence. Enclose the list with " " and should 
#                be separated by newline.
funcLst="load_entity_cnae_codes_br"
         
# Function Declarations
function load_entity_cnae_codes_br
{
   echo "set feedback off
         set heading off
         set serveroutput on size 1000000

         VARIABLE GV_return_code    NUMBER;
         VARIABLE GV_script_error   CHAR(255);

         EXEC :GV_return_code := 0;

         WHENEVER SQLERROR EXIT 1

      DECLARE
         L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

         ERROR_IN_FUNCTION             EXCEPTION;


      BEGIN


         INSERT INTO l10n_br_entity_cnae_codes(key_value_1,
                                               key_value_2,
                                               module,
                                               country_id,
                                               cnae_code,
                                               primary_ind)
                                        SELECT dc.key_value_1,
                                               dc.key_value_2,
                                               dc.module,
                                               dc.country_id,
                                               dc.cnae_code,
                                               dc.primary_ind
                                          FROM dc_entity_cnae_codes_br dc,
                                              (select partner_id key_value_1,
                                                      partner_type key_value_2,
                                                      'PTNR' module
                                                 from partner
                                              union all
                                              select to_char(store) key_value_1,
                                                     'S' key_value_2,
                                                     'LOC' module
                                                from store
                                              union all
                                              select to_char(wh) key_value_1,
                                                     'W' key_value_2,
                                                     'LOC' module
                                                from wh
                                              union all
                                              select to_char(supplier) key_value_1,
                                                     'S' key_value_2,
                                                     'SUPP' module
                                                from sups
                                              union all
                                              select to_char(outloc_id) key_value_1,
                                                     outloc_type key_value_2,
                                                    'OLOC' module
                                                from outloc
                                              union all
                                              select to_char(company) key_value_1,
                                                     'C' key_value_2,
                                                     'COMP' module
                                                from comphead
                                              ) loc
                                        WHERE dc.key_value_1 = loc.key_value_1
                                          AND dc.key_value_2 = loc.key_value_2
                                          AND dc.module      = loc.module;
 #For inserting the CNAE CODES for all the virtual wh whose physical wh are already present in the l10n_br_entity_cnae_codes table.
         insert into l10n_br_entity_cnae_codes
              select *
                from (select wh key_value_1,
                             key_value_2,
                             module,
                             country_id,
                             cnae_code,
                             primary_ind
                        from l10n_br_entity_cnae_codes a,
                             wh b
                       where a.key_value_1 = b.physical_wh
                         and a.key_value_2 ='W'
                         and a.module = 'LOC'
                         and b.wh<>b.physical_wh
                         and not exists (select 'X'
                                           from l10n_br_entity_cnae_codes c
                                          where b.wh = c.key_value_1
                                            and a.key_value_2 = c.key_value_2
                                            and a.module = c.module));

      EXCEPTION
         when ERROR_IN_FUNCTION then
            rollback;
            :GV_return_code := 1;
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /
      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}

function execLoad
{
   loadFunc=$1
   statusFile=${loadFunc}.${statusExt}

   badFileLst=""
   dscFileLst=""

   #Check the status file skip loading if exists
   if [[ $(checkFile -f ${statusFile}) -eq 0 ]]
   then
      echo "Function ${loadFunc} started previously. Skipped loading process." >> ${logFile}
      return 1
   fi

   #Define the bad and discard
   eval dataFile='$'${loadFunc}_data1
   cnt=1
   while [[ -n ${dataFile} ]];
   do
     fileChk=$(checkFile -f $dataFile)
     # Check if data file is valid
     if [[ ${fileChk} -eq 0 ]]; then
        eval "badFile${cnt}=${dataFile%.*}.${badExt}"
        eval "dscFile${cnt}=${dataFile%.*}.${dscExt}"
     elif [[ ${fileChk} -eq 2 ]]; then
        missingFileLst=${missingFileLst:+"${missingFileLst}, "}${dataFile}
     elif [[ ${fileChk} -eq 3 ]]; then
        missingFileLst=${missingFileLst:+"${missingFileLst}, "}${dataFile}" (no data)"
     elif [[ ${fileChk} -eq 4 ]]; then
        missingFileLst=${missingFileLst:+"${missingFileLst}, "}${dataFile}" (no read access)"
     fi
     (( cnt += 1 ))

     dataFile=""
     eval dataFile='$'${loadFunc}_data$cnt
   done

   # Skip Load (exit function) if there are missing files
   if [[ -n ${missingFileLst} ]]
   then
      echo "Function ${loadFunc} requires the following data file/s to load: ${missingFileLst}" >> ${logFile}
      missingFileLst=""
      return 1
   fi

   # Create the status file
   touch ${statusDir}/${statusFile}
   
   # Execute the Load function
   ${loadFunc}
   retCode=$?
   checkError -e $retCode -m "Loading ${loadFunc} failed."
   if [[ ${retCode} -ne 0 ]]; then
      return 1
   fi

   # Check for bad/discard files
   badFile=$badFile1
   dscFile=$dscFile1
   cnt=1
   while [[ -n $badFile || -n $dscFile ]];
   do

     # Check if bad and discard files
     fileChk=$(checkFile -f $badFile)
     if [[ ${fileChk} -eq 0 ]]; then
       badFileLst=${badFileLst:+"${badFileLst}, "}${badFile}
     fi

     fileChk=$(checkFile -f $dscFile)
     if [[ ${fileChk} -eq 0 ]]; then
       dscFileLst=${dscFileLst:+"${dscFileLst}, "}${dscFile}
     fi

     (( cnt += 1 ))

     badFile=""
     dscFile=""

     eval badFile='$'badFile$cnt
     eval dscFile='$'dscFile$cnt
     eval badFile$cnt=""
     eval dscFile$cnt=""

   done

   if [[ -n $badFileLst || -n $dscFileLst ]]; then
      echo "Function ${loadFunc} completed with the following "${badFileLst:+"bad file/s: ${badFileLst} "}${dscFileLst:+"discard file/s: ${dscFileLst}"} | tee -a ${statusDir}/${statusFile} >> ${logFile}
      badFile=""
      dscFile=""
   else
      echo "Function ${loadFunc} loaded successfully." >> ${logFile}
   fi

   return $retCode   
}

###############################################################################
#                                     MAIN                                    #
###############################################################################

# Check configuration file
checkCfg
if [[ $? -eq 1 ]]; then
   exit 1
fi

# Get the options from the command line
while getopts c option
do
   case ${option} in
      c) createTbl=1;;
      \?) print "Invalid argument.";
          exit 1;;
   esac
done

# if user inputs -c in the command line run the table create scripts
if [[ $createTbl -eq 1 ]]; then

  dbc=$dbcScript1
  cnt=1
  while [[ -n $dbc ]];
  do
     execSql $dbc ${orclDataDir} ${orclLogDir}
     (( cnt += 1 ))
     
     dbc=""
     eval dbc='$'dbcScript$cnt
  done
fi

# Check if all required files are valid before proceeding with load
reqDat=$reqDataFile1
cnt=1
while [[ -n $reqDat ]];
do
   tmp=$(checkFile -f $reqDat)
   (( cnt += 1))
   
   reqDat=""
   eval reqDat='$'reqDataFile$cnt

done

if [[ -n $missingFileLst ]]; then
   echo "Script ${scriptName} cannot load due to missing/no data files: $missingFileLst" >> ${logDir}/${logFile}
   missingFileLst=""
   exit 1
fi

# Load all the scripts defined in the funcLst
for funcName in ${funcLst}
do
   execLoad ${funcName}
done

# Create Processed directory
mkdir ${dataCompDir} > /dev/null 2>&1

# Move the data files to the processed directory
for funcName in ${funcLst}
do
   eval dataFile='$'${funcName}_data1
   cnt=1
   while [[ -n ${dataFile} ]];
   do
     mv -f ${dataDir}/${dataFile} ${dataCompDir}/. > /dev/null 2>&1
     (( cnt += 1 ))

     dataFile=""
     eval dataFile='$'${funcName}_data$cnt
   done
done

exit 0
