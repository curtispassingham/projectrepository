LOAD DATA
APPEND
INTO TABLE stg_store_add_l10n_ext_br 
  FIELDS TERMINATED BY '|'
  OPTIONALLY ENCLOSED BY '"'
  TRAILING NULLCOLS
  (
     process_id "L10N_BR_DATA_MIGRATION_SQL.GET_PROCESS_SEQ_VAL('STORE_ADD_L10N_EXT')",
     store,
     taxpayer_type,
     addr_1,
     addr_2,
     addr_3,
     neighborhood,
     jurisdiction_code,
     state,
     country,
     postal_code,
     cpf,
     cnpj,
     nit,
     suframa,
     im,
     ie,
     iss_contrib_ind,
     rural_prod_ind,
     ipi_ind,
     icms_contrib_ind,
     match_opr_type,
     control_rec_st_ind,
     pis_contrib_ind,
     cofins_contrib_ind
  )