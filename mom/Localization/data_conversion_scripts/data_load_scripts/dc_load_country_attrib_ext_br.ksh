#!/bin/ksh

# Common functions library and Configurations
. ./dc_load.cfg
. ./dc_load.lib

# Script variables
scriptName=$0
logFile=${logDir}/$tDay.log


# Table Create scripts
dbcScript1=dbc_create_country_attrib_ext_br_tab.sql

# Data files used by <function1>
load_country_attrib_ext_data1=dc_country_attrib_ext_br.dat

# Function list: For each load function defined (with PL/SQL block) list here
#                according to sequence. Enclose the list with " " and should
#                be separated by newline.
funcLst="load_country_attrib_ext"

# Function Declarations
function load_country_attrib_ext
{
   echo "set feedback off
         set heading off
         set serveroutput on size 1000000

         VARIABLE GV_return_code    NUMBER;
         VARIABLE GV_script_error   CHAR(255);

         EXEC :GV_return_code := 0;

         WHENEVER SQLERROR EXIT 1

      DECLARE
         L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
         L_default_loc                 COUNTRY_ATTRIB.DEFAULT_LOC%TYPE;
         L_default_loc_type            COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE;
         ERROR_IN_FUNCTION             EXCEPTION;
         
         cursor GET_COUNTRY_ATTRIB
             is select default_loc,
                       default_loc_type
                  from dc_country_attrib_ext_br;



      BEGIN
         OPEN GET_COUNTRY_ATTRIB;
         FETCH GET_COUNTRY_ATTRIB into L_default_loc,
                                       L_default_loc_type;
         CLOSE GET_COUNTRY_ATTRIB;
         
         update country_attrib set default_loc = L_default_loc,
                                   default_loc_type = L_default_loc_type
                                where country_id='BR';
                             
         COMMIT;

      EXCEPTION
         when ERROR_IN_FUNCTION then
            rollback;
            :GV_return_code := 1;
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /
      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}

function execLoad
{
   loadFunc=$1
   statusFile=${loadFunc}.${statusExt}

   badFileLst=""
   dscFileLst=""

   #Check the status file skip loading if exists
   if [[ $(checkFile -f ${statusFile}) -eq 0 ]]
   then
      echo "Function ${loadFunc} started previously. Skipped loading process." >> ${logFile}
      return 1
   fi

   #Define the bad and discard
   eval dataFile='$'${loadFunc}_data1
   cnt=1
   while [[ -n ${dataFile} ]];
   do
     fileChk=$(checkFile -f $dataFile)
     # Check if data file is valid
     if [[ ${fileChk} -eq 0 ]]; then
        eval "badFile${cnt}=${dataFile%.*}.${badExt}"
        eval "dscFile${cnt}=${dataFile%.*}.${dscExt}"
     elif [[ ${fileChk} -eq 2 ]]; then
        missingFileLst=${missingFileLst:+"${missingFileLst}, "}${dataFile}
     elif [[ ${fileChk} -eq 3 ]]; then
        missingFileLst=${missingFileLst:+"${missingFileLst}, "}${dataFile}" (no data)"
     elif [[ ${fileChk} -eq 4 ]]; then
        missingFileLst=${missingFileLst:+"${missingFileLst}, "}${dataFile}" (no read access)"
     fi
     (( cnt += 1 ))

     dataFile=""
     eval dataFile='$'${loadFunc}_data$cnt
   done

   # Skip Load (exit function) if there are missing files
   if [[ -n ${missingFileLst} ]]
   then
      echo "Function ${loadFunc} requires the following data file/s to load: ${missingFileLst}" >> ${logFile}
      missingFileLst=""
      return 1
   fi

   # Create the status file
   touch ${statusDir}/${statusFile}

   # Execute the Load function
   ${loadFunc}
   retCode=$?
   checkError -e $retCode -m "Loading ${loadFunc} failed."
   if [[ ${retCode} -ne 0 ]]; then
      return 1
   fi

   # Check for bad/discard files
   badFile=$badFile1
   dscFile=$dscFile1
   cnt=1
   while [[ -n $badFile || -n $dscFile ]];
   do

     # Check if bad and discard files
     fileChk=$(checkFile -f $badFile)
     if [[ ${fileChk} -eq 0 ]]; then
       badFileLst=${badFileLst:+"${badFileLst}, "}${badFile}
     fi

     fileChk=$(checkFile -f $dscFile)
     if [[ ${fileChk} -eq 0 ]]; then
       dscFileLst=${dscFileLst:+"${dscFileLst}, "}${dscFile}
     fi

     (( cnt += 1 ))

     badFile=""
     dscFile=""

     eval badFile='$'badFile$cnt
     eval dscFile='$'dscFile$cnt
     eval badFile$cnt=""
     eval dscFile$cnt=""

   done

   if [[ -n $badFileLst || -n $dscFileLst ]]; then
      echo "Function ${loadFunc} completed with the following "${badFileLst:+"bad file/s: ${badFileLst} "}${dscFileLst:+"discard file/s: ${dscFileLst}"} | tee -a ${statusDir}/${statusFile} >> ${logFile}
      badFile=""
      dscFile=""
   else
      echo "Function ${loadFunc} loaded successfully." >> ${logFile}
   fi

   return $retCode
}

###############################################################################
#                                     MAIN                                    #
###############################################################################

# Check configuration file
checkCfg
if [[ $? -eq 1 ]]; then
   exit 1
fi

# Get the options from the command line
while getopts c option
do
   case ${option} in
      c) createTbl=1;;
      \?) print "Invalid argument.";
          exit 1;;
   esac
done

# if user inputs -c in the command line run the table create scripts
if [[ $createTbl -eq 1 ]]; then

  dbc=$dbcScript1
  cnt=1
  while [[ -n $dbc ]];
  do
     execSql $dbc ${orclDataDir} ${orclLogDir}
     (( cnt += 1 ))

     dbc=""
     eval dbc='$'dbcScript$cnt
  done
fi

# Check if all required files are valid before proceeding with load
reqDat=$reqDataFile1
cnt=1
while [[ -n $reqDat ]];
do
   tmp=$(checkFile -f $reqDat)
   (( cnt += 1))

   reqDat=""
   eval reqDat='$'reqDataFile$cnt

done

if [[ -n $missingFileLst ]]; then
   echo "Script ${scriptName} cannot load due to missing/no data files: $missingFileLst" >> ${logDir}/${logFile}
   missingFileLst=""
   exit 1
fi

# Load all the scripts defined in the funcLst
for funcName in ${funcLst}
do
   execLoad ${funcName}
done

# Create Processed directory
mkdir ${dataCompDir} > /dev/null 2>&1

# Move the data files to the processed directory
for funcName in ${funcLst}
do
   eval dataFile='$'${funcName}_data1
   cnt=1
   while [[ -n ${dataFile} ]];
   do
     mv -f ${dataDir}/${dataFile} ${dataCompDir}/. > /dev/null 2>&1
     (( cnt += 1 ))

     dataFile=""
     eval dataFile='$'${funcName}_data$cnt
   done
done

exit 0
