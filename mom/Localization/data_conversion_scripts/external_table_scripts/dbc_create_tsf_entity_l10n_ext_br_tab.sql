set verify off

prompt Dropping table DC_TSF_ENTITY_L10N_EXT_BR

DROP TABLE DC_TSF_ENTITY_L10N_EXT_BR
/
WHENEVER SQLERROR EXIT 1

define dc_data_dir=&1   -- passed as a parameter from the shell script
define dc_log_dir=&2    -- passed as a parameter from the shell script

prompt Creating external table DC_TSF_ENTITY_L10N_EXT_BR
CREATE TABLE DC_TSF_ENTITY_L10N_EXT_BR
(
  TSF_ENTITY_ID                                      NUMBER(10),
  IPI_IND                                            VARCHAR2(1),
  TAXPAYER_TYPE                                      VARCHAR2(1),
  ADDR_1                                             VARCHAR2(240),
  ADDR_2                                             VARCHAR2(240),
  ADDR_3                                             VARCHAR2(240),
  NEIGHBORHOOD                                       VARCHAR2(240),
  JURISDICTION_CODE                                  VARCHAR2(10),
  STATE                                              VARCHAR2(5),
  COUNTRY                                            VARCHAR2(3),
  POSTAL_CODE                                        VARCHAR2(30),
  CPF                                                VARCHAR2(11),
  CNPJ                                               VARCHAR2(14),
  NIT                                                VARCHAR2(250),
  SUFRAMA                                            VARCHAR2(250),
  IM                                                 VARCHAR2(250),
  IE                                                 VARCHAR2(250)
)
ORGANIZATION external
(
   TYPE oracle_loader
   DEFAULT DIRECTORY &&dc_data_dir
   ACCESS PARAMETERS
   (
      RECORDS DELIMITED BY NEWLINE CHARACTERSET UTF8
      LOAD WHEN (
                 tsf_entity_id != BLANKS
                )
      BADFILE &&dc_log_dir:'dc_tsf_entity_l10n_ext_br.bad'
      DISCARDFILE &&dc_log_dir:'dc_tsf_entity_l10n_ext_br.dsc'
      LOGFILE &&dc_log_dir:'dc_tsf_entity_l10n_ext_br.log'
      FIELDS TERMINATED BY '|'
      MISSING FIELD VALUES ARE NULL
      (
         tsf_entity_id,
         ipi_ind,
         taxpayer_type,
         addr_1,
         addr_2,
         addr_3,
         neighborhood,
         jurisdiction_code,
         state,
         country,
         postal_code,
         cpf,
         cnpj,
         nit,
         suframa,
         im,
         ie
      )
   )
   location
   (
      'dc_tsf_entity_l10n_ext_br.dat'
   )
)
reject limit unlimited
/
prompt DC_TSF_ENTITY_L10N_EXT_BR created successfully

exit;
