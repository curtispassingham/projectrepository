set verify off

prompt Dropping table DC_COUNTRY_ATTRIB_EXT_BR

DROP TABLE DC_COUNTRY_ATTRIB_EXT_BR
/
WHENEVER SQLERROR EXIT 1

define dc_data_dir=&1   -- passed as a parameter from the shell script
define dc_log_dir=&2    -- passed as a parameter from the shell script

prompt Creating external table DC_COUNTRY_ATTRIB_EXT_BR
CREATE TABLE DC_COUNTRY_ATTRIB_EXT_BR
(
   DEFAULT_LOC        NUMBER(10),
   DEFAULT_LOC_TYPE   VARCHAR2(1)
)
ORGANIZATION external
(
   TYPE oracle_loader
   DEFAULT DIRECTORY &&dc_data_dir
   ACCESS PARAMETERS
   (
      RECORDS DELIMITED BY NEWLINE CHARACTERSET UTF8
      LOAD WHEN (
                 DEFAULT_LOC          != BLANKS and
                 DEFAULT_LOC_TYPE     != BLANKS 
                )
      BADFILE &&dc_log_dir:'dc_country_attrib_ext_br.bad'
      DISCARDFILE &&dc_log_dir:'dc_country_atrrib_ext_br.dsc'
      LOGFILE &&dc_log_dir:'dc_country_attrib_ext_br.log'
      FIELDS TERMINATED BY '|'
   )
   location
   (
      'dc_country_attrib_ext_br.dat'
   )
)
reject limit unlimited
/
prompt DC_COUNTRY_ATTRIB_EXT_BR created successfully

exit;
