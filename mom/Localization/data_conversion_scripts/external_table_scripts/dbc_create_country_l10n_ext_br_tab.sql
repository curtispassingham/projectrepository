set verify off

prompt Dropping table DC_COUNTRY_L10N_EXT_BR

DROP TABLE DC_COUNTRY_L10N_EXT_BR
/
WHENEVER SQLERROR EXIT 1

define dc_data_dir=&1   -- passed as a parameter from the shell script
define dc_log_dir=&2    -- passed as a parameter from the shell script

prompt Creating external table DC_COUNTRY_L10N_EXT_BR
CREATE TABLE DC_COUNTRY_L10N_EXT_BR
(
   COUNTRY_ID            VARCHAR2(3),
   FISCAL_COUNTRY_ID     VARCHAR2(3),
   FISCAL_CODE           VARCHAR2(120)
)
ORGANIZATION external
(
   TYPE oracle_loader
   DEFAULT DIRECTORY &&dc_data_dir
   ACCESS PARAMETERS
   (
      RECORDS DELIMITED BY NEWLINE CHARACTERSET UTF8
      LOAD WHEN (
                   country_id != BLANKS and
                   fiscal_country_id != BLANKS and
                   fiscal_code != BLANKS
                 )
      BADFILE &&dc_log_dir:'dc_country_l10n_ext_br.bad'
      DISCARDFILE &&dc_log_dir:'dc_country_l10n_ext_br.dsc'
      LOGFILE &&dc_log_dir:'dc_country_l10n_ext_br.log'
      FIELDS TERMINATED BY '|'
      MISSING FIELD VALUES ARE NULL
      (
         country_id,
         fiscal_country_id,
         fiscal_code
      )
   )
   location
   (
      'dc_country_l10n_ext_br.dat'
   )
)
reject limit unlimited
/
prompt DC_COUNTRY_L10N_EXT_BR created successfully

exit;
