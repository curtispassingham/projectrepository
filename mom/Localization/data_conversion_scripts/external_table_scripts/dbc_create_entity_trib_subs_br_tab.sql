set verify off

prompt Dropping table DC_ENTITY_TRIB_SUBS_BR

DROP TABLE DC_ENTITY_TRIB_SUBS_BR
/
WHENEVER SQLERROR EXIT 1

define dc_data_dir=&1   -- passed as a parameter from the shell script
define dc_log_dir=&2    -- passed as a parameter from the shell script

prompt Creating external table DC_ENTITY_TRIB_SUBS_BR
CREATE TABLE DC_ENTITY_TRIB_SUBS_BR
(
   ENTITY                VARCHAR2(10),
   ENTITY_TYPE           VARCHAR2(4),
   COUNTRY_ID            VARCHAR2(3),
   STATE                 VARCHAR2(3),
   IE_SUBS               VARCHAR2(250)
)
ORGANIZATION external
(
   TYPE oracle_loader
   DEFAULT DIRECTORY &&dc_data_dir
   ACCESS PARAMETERS
   (
      RECORDS DELIMITED BY NEWLINE CHARACTERSET UTF8
      LOAD WHEN (
                 ENTITY          != BLANKS and
                 ENTITY_TYPE     != BLANKS and
                 COUNTRY_ID      != BLANKS and
                 STATE           != BLANKS and
                 IE_SUBS         != BLANKS
                )
      BADFILE &&dc_log_dir:'dc_entity_trib_subs_br.bad'
      DISCARDFILE &&dc_log_dir:'dc_entity_trib_subs_br.dsc'
      LOGFILE &&dc_log_dir:'dc_entity_trib_subs_br.log'
      FIELDS TERMINATED BY '|'
   )
   location
   (
      'dc_entity_trib_subs_br.dat'
   )
)
reject limit unlimited
/
prompt DC_ENTITY_TRIB_SUBS_BR created successfully

exit;
