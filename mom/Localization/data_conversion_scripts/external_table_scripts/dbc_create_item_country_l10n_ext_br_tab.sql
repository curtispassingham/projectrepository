set verify off

prompt Dropping table DC_ITEM_COUNTRY_L10N_EXT_BR

DROP TABLE DC_ITEM_COUNTRY_L10N_EXT_BR
/
WHENEVER SQLERROR EXIT 1

define dc_data_dir=&1   -- passed as a parameter from the shell script
define dc_log_dir=&2    -- passed as a parameter from the shell script

prompt Creating external table DC_ITEM_COUNTRY_L10N_EXT_BR
CREATE TABLE DC_ITEM_COUNTRY_L10N_EXT_BR
(
   ITEM                                  VARCHAR2(25),
   COUNTRY_ID                            VARCHAR2(3),
   SERVICE_IND                           VARCHAR2(1),
   ORIGIN_CODE                           VARCHAR2(6),
   CLASSIFICATION_ID                     VARCHAR2(25),
   NCM_CHAR_CODE                         VARCHAR2(25),
   EX_IPI                                VARCHAR2(25),
   PAUTA_CODE                            VARCHAR2(25),
   SERVICE_CODE                          VARCHAR2(25),
   FEDERAL_SERVICE                       VARCHAR2(25),
   STATE_OF_MANUFACTURE                  VARCHAR2(3),
   PHARMA_LIST_TYPE                      VARCHAR2(6)
)
ORGANIZATION external
(
   TYPE oracle_loader
   DEFAULT DIRECTORY &&dc_data_dir
   ACCESS PARAMETERS
   (
      RECORDS DELIMITED BY NEWLINE CHARACTERSET UTF8
      LOAD WHEN (
                   item != BLANKS and
                   country_id != BLANKS)
      BADFILE &&dc_log_dir:'dc_item_country_l10n_ext_br.bad'
      DISCARDFILE &&dc_log_dir:'dc_item_country_l10n_ext_br.dsc'
      LOGFILE &&dc_log_dir:'dc_item_country_l10n_ext_br.log'
      FIELDS TERMINATED BY '|'
      MISSING FIELD VALUES ARE NULL
      (
         item,
         country_id,
         service_ind,
         origin_code,
         classification_id,
         ncm_char_code,
         ex_ipi,
         pauta_code,
         service_code,
         federal_service,
         state_of_manufacture,
         pharma_list_type
      )
   )
   location
   (
      'dc_item_country_l10n_ext_br.dat'
   )
)
reject limit unlimited
/
prompt DC_ITEM_COUNTRY_L10N_EXT_BR created successfully

exit;
