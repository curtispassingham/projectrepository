set verify off

prompt Dropping table DC_ENTITY_CNAE_CODES_BR

DROP TABLE DC_ENTITY_CNAE_CODES_BR
/
WHENEVER SQLERROR EXIT 1

define dc_data_dir=&1   -- passed as a parameter from the shell script
define dc_log_dir=&2    -- passed as a parameter from the shell script

prompt Creating external table DC_ENTITY_CNAE_CODES_BR
CREATE TABLE DC_ENTITY_CNAE_CODES_BR
(
   KEY_VALUE_1       VARCHAR2(10),
   KEY_VALUE_2       VARCHAR2(6),
   MODULE            VARCHAR2(4),
   COUNTRY_ID        VARCHAR2(3),
   CNAE_CODE         VARCHAR2(9),
   PRIMARY_IND       VARCHAR2(1)
)
ORGANIZATION external
(
   TYPE oracle_loader
   DEFAULT DIRECTORY &&dc_data_dir
   ACCESS PARAMETERS
   (
      RECORDS DELIMITED BY NEWLINE CHARACTERSET UTF8
      LOAD WHEN (
                 KEY_VALUE_1     != BLANKS and
                 KEY_VALUE_2     != BLANKS and
                 MODULE          != BLANKS and
                 COUNTRY_ID      != BLANKS and
                 CNAE_CODE       != BLANKS and
                 PRIMARY_IND     != BLANKS
                )
      BADFILE &&dc_log_dir:'dc_entity_cnae_codes_br.bad'
      DISCARDFILE &&dc_log_dir:'dc_entity_cnae_codes_br.dsc'
      LOGFILE &&dc_log_dir:'dc_entity_cnae_codes_br.log'
      FIELDS TERMINATED BY '|'
   )
   location
   (
      'dc_entity_cnae_codes_br.dat'
   )
)
reject limit unlimited
/
prompt DC_ENTITY_CNAE_CODES_BR created successfully

exit;
