set verify off

prompt Dropping table DC_WH_L10N_EXT_BR

DROP TABLE DC_WH_L10N_EXT_BR
/
WHENEVER SQLERROR EXIT 1

define dc_data_dir=&1   -- passed as a parameter from the shell script
define dc_log_dir=&2    -- passed as a parameter from the shell script

prompt Creating external table DC_WH_L10N_EXT_BR
CREATE TABLE DC_WH_L10N_EXT_BR
(
   WH                                     NUMBER(10),
   TAXPAYER_TYPE                          VARCHAR2(1),
   ADDR_1                                 VARCHAR2(240),
   ADDR_2                                 VARCHAR2(240),
   ADDR_3                                 VARCHAR2(240),
   NEIGHBORHOOD                           VARCHAR2(240),
   JURISDICTION_CODE                      VARCHAR2(10),
   STATE                                  VARCHAR2(5),
   COUNTRY                                VARCHAR2(3),
   POSTAL_CODE                            VARCHAR2(30),
   CPF                                    VARCHAR2(11),
   CNPJ                                   VARCHAR2(14),
   NIT                                    VARCHAR2(250),
   SUFRAMA                                VARCHAR2(250),
   IM                                     VARCHAR2(250),
   IE                                     VARCHAR2(250),
   ISS_CONTRIB_IND                        VARCHAR2(1),
   RURAL_PROD_IND                         VARCHAR2(1),
   IPI_IND                                VARCHAR2(3),
   ICMS_CONTRIB_IND                       VARCHAR2(1),
   PIS_CONTRIB_IND                        VARCHAR2(1),
   COFINS_CONTRIB_IND                     VARCHAR2(1),
   MATCH_OPR_TYPE                         VARCHAR2(6),
   CONTROL_REC_ST_IND                     VARCHAR2(1)
)
ORGANIZATION external
(
   TYPE oracle_loader
   DEFAULT DIRECTORY &&dc_data_dir
   ACCESS PARAMETERS
   (
      RECORDS DELIMITED BY NEWLINE CHARACTERSET UTF8
      LOAD WHEN (
                 wh != BLANKS
                )
      BADFILE &&dc_log_dir:'dc_wh_l10n_ext_br.bad'
      DISCARDFILE &&dc_log_dir:'dc_wh_l10n_ext_br.dsc'
      LOGFILE &&dc_log_dir:'dc_wh_l10n_ext_br.log'
      FIELDS TERMINATED BY '|'
      MISSING FIELD VALUES ARE NULL
      (
         wh,
         taxpayer_type,
         addr_1,
         addr_2,
         addr_3,
         neighborhood,
         jurisdiction_code,
         state,
         country,
         postal_code,
         cpf,
         cnpj,
         nit,
         suframa,
         im,
         ie,
         iss_contrib_ind,
         rural_prod_ind,
         ipi_ind,
         icms_contrib_ind,
         pis_contrib_ind,
         cofins_contrib_ind,
         match_opr_type,
         control_rec_st_ind
      )
   )
   location
   (
      'dc_wh_l10n_ext_br.dat'
   )
)
reject limit unlimited
/
prompt DC_WH_L10N_EXT_BR created successfully

exit;
