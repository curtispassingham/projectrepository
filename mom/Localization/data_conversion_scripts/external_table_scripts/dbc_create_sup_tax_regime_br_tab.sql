set verify off

prompt Dropping table DC_SUP_TAX_REGIME_BR

DROP TABLE DC_SUP_TAX_REGIME_BR
/
WHENEVER SQLERROR EXIT 1

define dc_data_dir=&1   -- passed as a parameter from the shell script
define dc_log_dir=&2    -- passed as a parameter from the shell script

prompt Creating external table DC_SUP_TAX_REGIME_BR
CREATE TABLE DC_SUP_TAX_REGIME_BR
(
   SUPPLIER          NUMBER(10),
   TAX_REGIME        VARCHAR2(6)
)
ORGANIZATION external
(
   TYPE oracle_loader
   DEFAULT DIRECTORY &&dc_data_dir
   ACCESS PARAMETERS
   (
      RECORDS DELIMITED BY NEWLINE CHARACTERSET UTF8
      LOAD WHEN (
                 SUPPLIER        != BLANKS and
                 TAX_REGIME      != BLANKS
                )
      BADFILE &&dc_log_dir:'dc_sup_tax_regime_br.bad'
      DISCARDFILE &&dc_log_dir:'dc_sup_tax_regime_br.dsc'
      LOGFILE &&dc_log_dir:'dc_sup_tax_regime_br.log'
      FIELDS TERMINATED BY '|'
   )
   location
   (
      'dc_sup_tax_regime_br.dat'
   )
)
reject limit unlimited
/
prompt DC_SUP_TAX_REGIME_BR created successfully

exit;
