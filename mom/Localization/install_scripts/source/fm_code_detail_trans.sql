---
set feedback off
set define off

delete from code_detail_trans where code_type in ('FMFT','FMPS','FMTT','SUPP','FMDR','VRTP','FMTL','FMFE','FMVP','COMP','FMRA','FMNS','FMWE','FMMO','FMSD','FMET','FMOC','FMRT','RQRD','LFCT','FIMO','FIMD','FMRC','FMCD','ORME','FRPI','INDP','FMRD') and lang = 1;

Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMFT','CIF','CIF','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMFT','FOB','FOB','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMPS','E','Error','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMPS','J','Rejected','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMTT','VALUE','Amount Adjustment','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('SUPP','S','Supplier','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('COMP','C','Company','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','D','In-Discrepancy','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','S','Submitted','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','A','Approved','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','C','Completed','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRA','PO','PO','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRA','NF','Nota Fiscal','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','P','Printed','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','R','Printed, Pending for Receiving','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','F','Received','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','RV','Receipt-Verified','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','Q','Val.Received','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','NFE','Nfe Pending','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','V','Validated','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMNS','M','Matched','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMPS','P','Processed','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMPS','R','Received','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMTT','PERC','Percentual Adjustment','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMDR','A','ALL','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRA','SYS','SYSTEM','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','TSF','Transfer','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','RTV','Return to Vendor','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','RMA','Return Merchandise Authorization','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','RNF','Return NF','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','STOCK','Stock Out','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('VRTP','CHAR','Varchar','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('VRTP','DATE','Date','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('VRTP','NUMBER','Number','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMTL','COST','Line Cost','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMTL','QTY','Line Quantity','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMFE','COMP','System','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMFE','SUPP','Supplier','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMVP','P','Percentage','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMVP','V','Value','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('RQRD','REITED','Rejection Description','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','RPO','PO for Rural Producer','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','IC','Intercompany Transfer','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','REP','Repairing','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMMO','ENT','Entry','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMMO','EXIT','Exit','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','W','Worksheet','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','V','Validated','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','E','Error','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','S','Subm. for Recv.','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','D','In Discrepancy','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','A','Approved','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','P','Pending for Receiving','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','I','Inactive','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','R','Received','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','NFE_P','NFe Pending','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','F','Post Sefaz Approve Error','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','RV','Receipt-Verified','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','CN','Cancelled','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','C','Completed','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','N','Nullified by SEFAZ','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','H','Hold','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMSD','FP','Financials Posted',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMET','PO','Purchase Order','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMET','RMA','Return Merchandise Authorization','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMET','TSF','Transfer','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMET','RTV','Return to Vendor','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMET','STOCK','Stock Out','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMET','EDI','EDI Doc.','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMET','IC','Entre Emprs.','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMET','REP','Reparando','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMOC','O','Open','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMOC','C','Closed','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMWE','W','Warning','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMWE','E','Error','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRT','PO','Purchase Order','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMDR','R','Resolved','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMDR','U','Unresolved','1');
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('LFCT','I','Product',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('LFCT','S','Service',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMO','LOC','Location',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMO','OLOC','Outside Location',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMO','PTNR','Partner',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMO','SUPP','Supplier',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMO','CUST','Customer',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMCD','CL','Discrepancy Report',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMCD','RNF','Return NF',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMCD','MWN','Merchandise without NF',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMCD','CNFT','Complementary NF for taxes',1);


--Added for Discrepancy Resolution
insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values('FMRC','WOFF','Write Off',1);
insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values('FMRC','DA','Damaged',1);

--Added for Fiscal Reclassification
insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('ORME', '0', 'National', 1);
insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('ORME', '1', 'Foreign Item', 1);
insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('ORME', '2', 'Foreign Item bought from Brazil vendor', 1);
insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FRPI', 'N', 'New', 1);
insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FRPI', 'C', 'Complete', 1);
insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FRPI', 'E', 'Error', 1);

--Added for Supplier Site
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMD','LOC','Location',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMD','OLOC','Outside Location',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMD','PTNR','Partner',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMD','SUPP','Supplier Site',1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FIMD','CUST','Customer',1);

--Added for SPED
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('INDP','0', 'Sefaz (State Tax Authority)', 1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('INDP','1', 'Federal Justice', 1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('INDP','2', 'State Justice', 1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('INDP','3', 'Secex/SRF - Importation Tax Authority', 1);
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('INDP','9', 'Others', 1);


--Added for Reason code
Insert into code_detail_trans (CODE_TYPE,CODE,CODE_DESC,LANG) values ('FMRD','REITED','Reason Description',1);

commit;
set feedback on
set define on
PROMPT Done.

/
