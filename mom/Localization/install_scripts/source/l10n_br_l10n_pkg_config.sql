PROMPT deleting L10N_PKG_CONFIG

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'FISCAL_ATTRIB_EXISTS' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'DEL_ENT_TRIB_SUBS' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'COPY_DOWN_ATTRIB' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'CHECK_UTIL_CODE_EXISTS' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'CREATE_TRAN_UTIL_CODE' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'LOAD_TAX_OBJECT' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'CONSUME_RECEIPT'and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'CONSUME_SHIPMENT' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'CONSUME_INVADJ' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'CONSUME_RTV' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'UPDATE_AV_COST' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'GET_LOC_AV_COST' and country_id = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'GET_CURRENT_SOH_WAC' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'COPY_TRAN_UTIL_CODE' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'CHECK_IF_BRAZIL_LOCALIZED' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'LOAD_ORDER_TAX_OBJECT' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'DEFAULT_FISCAL_ATTRIB' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'VALIDATE_RIB_L10N_ATTRIB' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'PERSIST_RIB_L10N_ATTRIB' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'DEAL_ORDER_DISCOUNT' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'POST_VAT' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'RECOVERABLE_TAX_POSTING' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'CREATE_ORDCUST_EXT' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'UPDATE_ORDCUST_EXT' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'UPDATE_TRAN_UTIL_CODE' and COUNTRY_ID = 'BR';

delete from L10N_PKG_CONFIG where PROCEDURE_KEY = 'ADDR_UPDATE_L10N_EXT' and COUNTRY_ID = 'BR';

PROMPT inserting into L10N_PKG_CONFIG


insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'FISCAL_ATTRIB_EXISTS',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_FND_SQL',
                                    'FISCAL_ATTRIB_EXISTS',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'DEL_ENT_TRIB_SUBS',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_FND_SQL',
                                    'DEL_ENT_TRIB_SUBS',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'COPY_DOWN_ATTRIB',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_ITEM_SQL',
                                    'COPY_DOWN_ATTRIB',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'CHECK_UTIL_CODE_EXISTS',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_PO_SQL',
                                    'CHECK_UTIL_CODE_EXISTS',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'CREATE_TRAN_UTIL_CODE',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_PO_SQL',
                                    'CREATE_TRAN_UTIL_CODE',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'LOAD_TAX_OBJECT',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_INT_SQL',
                                    'LOAD_TAX_OBJECT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (select 'CONSUME_RECEIPT',
                                    'BR',
                                    SOP.TABLE_OWNER,
                                    'L10N_BR_FIN_SQL',
                                    'CONSUME_RECEIPT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (select 'CONSUME_SHIPMENT',
                                    'BR',
                                     SOP.TABLE_OWNER,
                                    'L10N_BR_FIN_SQL',
                                    'CONSUME_SHIPMENT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (select 'CONSUME_INVADJ',
                                    'BR',
                                     SOP.TABLE_OWNER,
                                    'L10N_BR_FIN_SQL',
                                    'CONSUME_INVADJ',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (select 'CONSUME_RTV',
                                    'BR',
                                     SOP.TABLE_OWNER,
                                    'L10N_BR_FIN_SQL',
                                    'CONSUME_RTV',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (select 'UPDATE_AV_COST',
                                    'BR',
                                    SOP.TABLE_OWNER,
                                    'L10N_BR_FIN_SQL',
                                    'UPDATE_AV_COST',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (select 'GET_LOC_AV_COST',
                                    'BR',
                                    SOP.TABLE_OWNER,
                                    'L10N_BR_FIN_SQL',
                                    'GET_LOC_AV_COST',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (select 'GET_CURRENT_SOH_WAC',
                                    'BR',
                                    SOP.TABLE_OWNER,
                                    'L10N_BR_FIN_SQL',
                                    'GET_CURRENT_SOH_WAC',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'COPY_TRAN_UTIL_CODE',
                                    'BR',
                                     SOP.TABLE_OWNER,
                                    'L10N_BR_PO_SQL',
                                    'COPY_TRAN_UTIL_CODE',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'CHECK_IF_BRAZIL_LOCALIZED',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_PO_SQL',
                                    'CHECK_IF_BRAZIL_LOCALIZED',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'LOAD_ORDER_TAX_OBJECT',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_INT_SQL',
                                    'LOAD_ORDER_TAX_OBJECT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'DEFAULT_FISCAL_ATTRIB',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_FND_SQL',
                                    'DEFAULT_FISCAL_ATTRIB',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'VALIDATE_RIB_L10N_ATTRIB',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_FLEX_API_SQL',
                                    'VALIDATE_RIB_L10N_ATTRIB',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'PERSIST_RIB_L10N_ATTRIB',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_FLEX_API_SQL',
                                    'PERSIST_RIB_L10N_ATTRIB',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'DEAL_ORDER_DISCOUNT',
                                    'BR',
                                    so.table_owner,
                                    'L10N_BR_INT_SQL',
                                    'DEAL_ORDER_DISCOUNT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'POST_VAT',
                                    'BR',
                                    so.table_owner,
                                    'L10N_BR_FIN_SQL',
                                    'POST_VAT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'RECOVERABLE_TAX_POSTING',
                                    'BR',
                                     SOP.TABLE_OWNER,
                                    'L10N_BR_FIN_SQL',
                                    'RECOVERABLE_TAX_POSTING',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'CREATE_ORDCUST_EXT',
                                    'BR',
                                     SOP.TABLE_OWNER,
                                    'L10N_BR_PO_SQL',
                                    'CREATE_ORDCUST_EXT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'UPDATE_ORDCUST_EXT',
                                    'BR',
                                     SOP.TABLE_OWNER,
                                    'L10N_BR_PO_SQL',
                                    'UPDATE_ORDCUST_EXT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'UPDATE_TRAN_UTIL_CODE',
                                    'BR',
                                     SOP.TABLE_OWNER,
                                    'L10N_BR_PO_SQL',
                                    'UPDATE_TRAN_UTIL_CODE',
                                    USER,
                                    SYSDATE,
                                    'Y'
                               from SYSTEM_OPTIONS SOP);

insert into L10N_PKG_CONFIG (PROCEDURE_KEY,
                             COUNTRY_ID,
                             SCHEMA_NAME,
                             PACKAGE_NAME,
                             FUNCTION_NAME,
                             USER_ID,
                             LAST_UPDATE_DATETIME,
                             BASE_IND)
                            (SELECT 'ADDR_UPDATE_L10N_EXT',
                                    'BR',
                                     so.table_owner,
                                    'L10N_BR_FND_SQL',
                                    'ADDR_UPDATE_L10N_EXT',
                                    USER,
                                    SYSDATE,
                                    'Y'
                             from SYSTEM_OPTIONS so);

 COMMIT;
