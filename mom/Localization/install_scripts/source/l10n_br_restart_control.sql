-- fisdnld

  DELETE FROM restart_bookmark
   WHERE restart_name = 'fisdnld';

  DELETE FROM restart_program_status
   WHERE restart_name = 'fisdnld';

  DELETE FROM restart_control
   WHERE program_name = 'fisdnld';

-- l10nbrfisdnld

delete from restart_bookmark where restart_name = 'l10nbrfisdnld';

delete from restart_program_status where restart_name = 'l10nbrfisdnld';

delete from restart_control where program_name = 'l10nbrfisdnld';

insert into restart_control values ('l10nbrfisdnld',null,'NONE','1','Y','T',1000);

insert into restart_program_status values ('l10nbrfisdnld',1,null,'l10nbrfisdnld','ready for start',null,null,null,null,null,null,null,null);

-- freclsprg

  DELETE FROM restart_bookmark
   WHERE restart_name = 'freclsprg';

  DELETE FROM restart_program_status
   WHERE restart_name = 'freclsprg';

   DELETE FROM restart_control
    WHERE program_name = 'freclsprg';

-- freclass

  DELETE FROM restart_bookmark
   WHERE restart_name = 'freclass';

  DELETE FROM restart_program_status
   WHERE restart_name = 'freclass';

   DELETE FROM restart_control
    WHERE program_name = 'freclass';

--l10nbrfreclsprg

delete from restart_bookmark where restart_name = 'l10nbrfreclsprg';

delete from restart_program_status where restart_name = 'l10nbrfreclsprg';

delete from restart_control where program_name = 'l10nbrfreclsprg';

insert into restart_control values ('l10nbrfreclsprg',null,'NONE','1','Y','T',1000);

insert into restart_program_status values ('l10nbrfreclsprg',1,null,'l10nbrfreclsprg','ready for start',null,null,null,null,null,null,null,null);



commit;