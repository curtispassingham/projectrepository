--------------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
-- TABLE AFFECTED: rtk_reports.
-- This is required for ORFM 13.2 Brazil reports.
--------------------------------------------------------------------------------

DELETE FROM RTK_REPORTS
      WHERE MODULE = 'ORFM';

--------------------------------------------------------------------------------
INSERT INTO RTK_REPORTS (MODULE, 
                         REPORT_NAME, 
                         REPORT_DESC, 
                         PARAMETER_IND, 
                         PRINTFRM_IND, 
                         SELECT_IND)
                   VALUES
                        ('ORFM',
                         'receiving',
                         'Receiving report will be used by WMS/SIM users',
                         'N',
                         'N',
                          NULL);

--------------------------------------------------------------------------------
INSERT INTO RTK_REPORTS (MODULE, 
                         REPORT_NAME, 
                         REPORT_DESC, 
                         PARAMETER_IND, 
                         PRINTFRM_IND, 
                         SELECT_IND)
                   VALUES
                        ('ORFM',
                         'fm_fiscal_document',
                         'Nota Fiscal document used for all type of transactions in Brazil',
                         'N',
                         'N',
                          NULL);
--------------------------------------------------------------------------------
INSERT INTO RTK_REPORTS (MODULE, 
                         REPORT_NAME, 
                         REPORT_DESC, 
                         PARAMETER_IND, 
                         PRINTFRM_IND, 
                         SELECT_IND)
                   VALUES
                        ('ORFM',
                         'correctionletter',
                         'Correction letter report - incase qty/cost/tax is wrong in the issued NF',
                         'N',
                         'N',
                          NULL);
--------------------------------------------------------------------------------
INSERT INTO RTK_REPORTS (MODULE, 
                         REPORT_NAME, 
                         REPORT_DESC, 
                         PARAMETER_IND, 
                         PRINTFRM_IND, 
                         SELECT_IND)
                   VALUES
                        ('ORFM',
                         'complementary_nf_tax',
                         'Complementary NF for taxes is system taxes is higher than the informed tax',
                         'N',
                         'N',
                          NULL);
--------------------------------------------------------------------------------
INSERT INTO RTK_REPORTS (MODULE, 
                         REPORT_NAME, 
                         REPORT_DESC, 
                         PARAMETER_IND, 
                         PRINTFRM_IND, 
                         SELECT_IND)
                   VALUES
                        ('ORFM',
                         'autortnf',
                         'Automatic return NF report',
                         'N',
                         'N',
                          NULL);
--------------------------------------------------------------------------------
INSERT INTO RTK_REPORTS (MODULE, 
                         REPORT_NAME, 
                         REPORT_DESC, 
                         PARAMETER_IND, 
                         PRINTFRM_IND, 
                         SELECT_IND)
                   VALUES
                        ('ORFM',
                         'prodwonf',
                         'Product without NF report - the items which doesnot support any NF documents found during receiving',
                         'N',
                         'N',
                          NULL);
--------------------------------------------------------------------------------
COMMIT;
/