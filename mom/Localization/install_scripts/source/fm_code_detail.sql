---
set feedback off
set define off

delete from code_detail_trans where code_type in ('FMFT','FMPS','FMTT','SUPP','FMDR','VRTP','FMTL','FMFE','FMVP','COMP','FMRA','FMNS','FMWE','FMMO','FMSD','FMET','FMOC','FMRT','RQRD','LFCT','FIMO','FIMD','FMRC','FMCD','ORME','FRPI','INDP','FMRD','FMCC','FMDA') and lang in ('1','12');
delete from code_detail where code_type in ('FMFT','FMPS','FMTT','SUPP','FMDR','VRTP','FMTL','FMFE','FMVP','COMP','FMRA','FMNS','FMWE','FMMO','FMSD','FMET','FMOC','FMRT','RQRD','LFCT','FIMO','FIMD','FMRC','FMCD','ORME','FRPI','INDP','FMRD','FMCC','FMDA');

Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMFT','CIF','CIF','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMFT','FOB','FOB','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMPS','E','Error','Y','3');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMPS','J','Rejected','Y','4');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMTT','VALUE','Amount Adjustment','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('SUPP','S','Supplier','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('COMP','C','Company','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','D','In-Discrepancy','Y','3');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','S','Submitted','Y','4');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','A','Approved','Y','5');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','C','Completed','Y','6');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','P','Printed','Y','7');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','R','Printed, Pending for Receiving','Y','8');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','F','Received','Y','9');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','RV','Receipt-Verified','Y','10');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','Q','Val.Received','Y','11');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','NFE','Nfe Pending','Y','12');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','V','Validated','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMNS','M','Matched','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMPS','P','Processed','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMPS','R','Received','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMTT','PERC','Percentual Adjustment','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRA','NF','Nota Fiscal','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRA','SYS','SYSTEM','Y','3');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','TSF','Transfer','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','RTV','Return to Vendor','Y','3');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','RMA','Return Merchandise Authorization','Y','4');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','STOCK','Stock Out','Y','5');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','RNF','Return NF','Y','10');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('VRTP','CHAR','Varchar','Y','3');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('VRTP','DATE','Date','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('VRTP','NUMBER','Number','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMTL','COST','Line Cost','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMTL','QTY','Line Quantity','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMFE','COMP','System','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMFE','SUPP','Supplier','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMVP','P','Percentage','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMVP','V','Value','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('RQRD','REITED','Rejection Description','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','RPO','PO for Rural Producer','Y','7');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','IC','InterCompany Transfers','Y','8');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','REP','Repairing','N','9');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMMO','ENT','Entry','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMMO','EXIT','Exit','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','W','Worksheet','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','V','Validated','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','E','Error','Y','3');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','S','Subm. for Recv.','Y','4');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','D','In Discrepancy','Y','5');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','A','Approved','Y','6');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','P','Pending for Receiving','Y','7');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','I','Inactive','Y','8');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','R','Received','Y','9');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','NFE_P','NFe Pending','Y','10');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','F','Post Sefaz Approve Error','Y','15');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','RV','Receipt-Verified','Y','11');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','CN','Cancelled','Y',12);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','N','Nullified by SEFAZ','Y',13);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','C','Completed','Y',14);
Insert into CODE_DETAIL (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','FP','Financials Posted','Y',16);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMSD','H','Hold','Y',17);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMET','PO','Purchase Order','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMET','RMA','Return Merchandise Authorization','Y','3');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMET','TSF','Transfer','Y','4');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMET','RTV','Return to Vendor','Y','5');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMET','STOCK','Stock Out','Y','6');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMET','EDI','EDI Doc.','Y','7');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMET','IC','Entre Emprs.','Y','8');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMET','REP','Reparando','N','9');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMOC','O','Open','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMOC','C','Closed','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMWE','W','Warning','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMWE','E','Error','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRT','PO','Purchase Order','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDR','R','Resolved','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDR','U','Unresolved','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('LFCT','I','Product','Y',1);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('LFCT','S','Service','Y',2);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMO','LOC','Location','Y',3);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMO','OLOC','Outside Location','Y',4);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMO','PTNR','Partner','Y',2);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMO','SUPP','Supplier','Y',1);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMO','CUST','Customer','Y',6);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMCD','CL','Discrepancy Report','Y',1);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMCD','RNF','Return NF','Y',2);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMCD','MWN','Merchandise without NF','Y',3);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMCD','CNFT','Complementary NF for taxes','Y',4);


--Added for Discrepancy Resolution
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values('FMRC','WOFF','Write Off','Y','1');
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values('FMRC','DA','Damaged','Y','2');

--Added for Fiscal Reclassifiction
insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('ORME','0','National','Y',1);
insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('ORME','1','Foreign Item','Y',2);
insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('ORME','2','Foreign Item bought from Brazil vendor','Y',3);
insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FRPI','N','New','Y',1);
insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FRPI','C','Complete','Y',2);
insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FRPI','E','Error','Y',3);
--Added for Supplier Site
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMD','LOC','Location','Y',3);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMD','OLOC','Outside Location','Y',4);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMD','PTNR','Partner','Y',2);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMD','SUPP','Supplier Site','Y',1);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FIMD','CUST','Customer','Y',6);

--Added for SPED
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('INDP','0', 'Sefaz (State Tax Authority)','Y',1);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('INDP','1', 'Federal Justice','Y',2);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('INDP','2', 'State Justice','Y',3);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('INDP','3', 'Secex/SRF - Importation Tax Authority','Y',4);
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('INDP','9', 'Others','Y',5);

--Added for Multiple cnae code
delete from code_detail where code_type = 'LABL' and code = 'COMP';
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('LABL','COMP','Company','Y','1999');


--Added for Reason code  
Insert into CODE_DETAIL (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMRD','REITED','Reason Description','Y',1);


--Added for cost components
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMCC','F','Freight','Y','1');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMCC','I','Insurance','Y','2');
Insert into code_detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMCC','O','Other Expenses','Y','3');

Insert into code_Detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDA','NA','Natural Account','Y',1);
Insert into code_Detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDA','COMP','Company','Y',2);
Insert into code_Detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDA','DEPT','Department','Y',3);
Insert into code_Detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDA','CLASS','Class','Y',4);
Insert into code_Detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDA','SUBC','Subclass','Y',5);
Insert into code_Detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDA','LOCN','Location','Y',6);
Insert into code_Detail (CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values ('FMDA','TSFENT','Transfer Entity','Y',7);


commit;
set feedback on
set define on
PROMPT Done.

/
