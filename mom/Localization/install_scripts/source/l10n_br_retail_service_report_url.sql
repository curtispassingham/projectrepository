-----------------------------------------------------------------------------------------------
-- This script will popualte the data into the RETAIL_SERVICE_REPORT_URL table.
-- During Install, this script populates only RS_CODE, RS_NAME and RS_TYPE details,
-- later this script needs to be modified by Customer to load URLs details when they are ready.
-----------------------------------------------------------------------------------------------

REM -------------------------------------------------
REM
REM  Refreshes RETAIL_SERVICE_REPORT_URL table.
REM
REM -------------------------------------------------

REM -------------------------------------------------

delete from RETAIL_SERVICE_REPORT_URL where RS_CODE='RTIL';

REM ****************************************************************
PROMPT inserting into RETAIL_SERVICE_REPORT_URL
REM ****************************************************************

INSERT INTO RETAIL_SERVICE_REPORT_URL ( RS_CODE, RS_NAME, RS_TYPE, URL, SERVER, PORT, SYS_ACCOUNT ) 
VALUES ('RTIL', 'Retail Tax Integration Layer', 'S', NULL, NULL, NULL, NULL);

commit;