PROMPT deleting L10N_BATCH_CONFIG


delete from l10n_batch_config
   where function_key = 'EXEC_INV_ADJ'
     and function_name = 'EXEC_INV_ADJ'
     and country_id = 'BR'
/

PROMPT inserting into L10N_BATCH_CONFIG

insert into l10n_batch_config (function_key,
                               country_id,
                               function_name,
                               user_id,
                               last_update_datetime,
                               base_ind)
                       values ('EXEC_INV_ADJ',
                               'BR',
                               'EXEC_INV_ADJ',
                               user,
                               sysdate,
                               'Y')
/

commit
/
