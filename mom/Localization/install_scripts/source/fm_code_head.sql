---

set feedback off
set define off


delete from code_detail_trans where code_type in ('FMFT','FMPS','FMTT','SUPP','FMDR','VRTP','FMTL','FMFE','FMVP','COMP','FMRA','FMNS','FMWE','FMMO','FMSD','FMET','FMOC','FMRT','RQRD','LFCT','FIMO','FIMD','FMRC','FMCD','ORME','FRPI','INDP','FMRD','FMCC','FMDA') ;

delete from code_detail where code_type in ('FMFT','FMPS','FMTT','SUPP','FMDR','VRTP','FMTL','FMFE','FMVP','COMP','FMRA','FMNS','FMWE','FMMO','FMSD','FMET','FMOC','FMRT','RQRD','LFCT','FIMO','FIMD','FMRC','FMCD','ORME','FRPI','INDP','FMRD','FMCC','FMDA');

delete from code_head where code_type in ('FMFT','FMPS','FMTT','SUPP','FMDR','VRTP','FMTL','FMFE','FMVP','COMP','FMRA','FMNS','FMWE','FMMO','FMSD','FMET','FMOC','FMRT','RQRD','LFCT','FIMO','FIMD','FMRC','FMCD','ORME','FRPI','INDP','FMRD','FMCC','FMDA');



-- INSERTING into code_head
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMFT','ORFM: Freight Type');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMPS','ORFM: Process Status');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMTT','ORFM: Tolerance Type');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('SUPP','ORFM: Supplier');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMDR','ORFM: Discrepancy Status');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('VRTP','ORFM: Variable Type');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMTL','ORFM: Tolerance Level');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMFE','ORFM: Fiscal Entity Modules');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMVP','ORFM: Tributary Substitution');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('COMP','ORFM: Company');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMRA','ORFM: Discrepancy Resolution Actions');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMNS','ORFM: NF Doc status for Dis. Resolution Filter ');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('RQRD','ORFM: Received Qty Desc');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMWE','ORFM: Error Type');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMMO','ORFM: Document Type');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMSD','ORFM: Fiscal Document Status');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMET','ORFM: Type of Document of the Error');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMOC','ORFM: Error Status');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMRT','ORFM: Requisition Type');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('LFCT','ORFM: Classification Type');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FIMO','ORFM: Fiscal Entity Modules');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FIMD','ORFM: Fiscal Entity Modules for Supplier Site');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMCD','ORFM: Correction Document');

--Added for Discrepancy Resolution
insert into code_head (CODE_TYPE, CODE_TYPE_DESC) values ('FMRC','ORFM: Reason Code');

--Added for Fiscal Reclassification
insert into code_head (CODE_TYPE, CODE_TYPE_DESC) values ('ORME', 'Merchandise Origin Code');
insert into code_head (CODE_TYPE, CODE_TYPE_DESC) values ('FRPI', 'Fiscal Reclassification Processed Indicator');

--Added for SPED
Insert into code_head (CODE_TYPE, CODE_TYPE_DESC) values ('INDP','Attributes to identify the origin of the process required for SPED');
--Added for Received qty form
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMRD','ORFM: Reason Code Description');
--Added for cost components
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMCC','ORFM: Cost Components');
Insert into code_head (CODE_TYPE,CODE_TYPE_DESC) values ('FMDA','ORFM: Entity Types');

commit;
set feedback on
set define on
PROMPT Done.

/