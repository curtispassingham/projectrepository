REM
REM DELETE FROM THE FLEXFIELD CONFIGURATION TABLES
REM

DELETE FROM EXT_ENTITY_KEY_DESCS WHERE LANG = 1;
DELETE FROM L10N_CODE_DETAIL_DESCS WHERE LANG = 1;
DELETE FROM L10N_REC_GROUP_DESCS WHERE LANG = 1;
DELETE FROM L10N_ATTRIB_GROUP_DESCS WHERE LANG = 1;
DELETE FROM L10N_ATTRIB_DESCS WHERE LANG = 1;
DELETE FROM L10N_MENU_DESCS WHERE LANG = 1;



REM
REM INSERTING into EXT_ENTITY_KEY_DESCS
REM

PROMPT INSERTING into EXT_ENTITY_KEY_DESCS

Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('TSF_ENTITY','TSF_ENTITY_ID',1,'Y','Transfer Entity');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('WH','WH',1,'Y','Warehouse');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('STORE','STORE',1,'Y','Store');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('OUTLOC','OUTLOC_TYPE',1,'Y','Location Type');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('OUTLOC','OUTLOC_ID',1,'Y','Location');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('SUPS','SUPPLIER',1,'Y',decode((select supplier_sites_ind from system_options), 'Y', 'Supplier Site', 'N', 'Supplier'));
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('PARTNER','PARTNER_TYPE',1,'Y','Partner Type');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('PARTNER','PARTNER_ID',1,'Y','Partner');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('ITEM_COUNTRY','ITEM',1,'Y','Item');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('ITEM_COUNTRY','COUNTRY_ID',1,'Y','Country Code');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('ORDHEAD','ORDER_NO',1,'Y','Order No.');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('TSFHEAD','TSF_NO',1,'Y','Transfer');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('MRT','MRT_NO',1,'Y','MRT No.');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('COUNTRY','COUNTRY_ID',1,'Y','Country Code');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('STORE_ADD','STORE',1,'Y','Store');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('FIF_GL_SETUP','SET_OF_BOOKS_ID',1,'Y','Set of Books ID');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('ORDCUST','CUST_ID',1,'Y','Customer ID');
Insert into EXT_ENTITY_KEY_DESCS (BASE_RMS_TABLE,KEY_COL,LANG,DEFAULT_LANG_IND,KEY_DESC) values ('ORDCUST','ORDCUST_SEQ_NO',1,'Y','Customer Sequence No.');


REM
REM INSERTING into L10N_CODE_DETAIL_DESCS
REM

PROMPT INSERTING into L10N_CODE_DETAIL_DESCS
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('ORME','0',1,'Y','National',1);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('ORME','1',1,'Y','Foreign Item',2);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('ORME','2',1,'Y','Foreign Item Bought from Brazil Vendor',3);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('ENTY','J',1,'Y','Corporate Taxpayer',1);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('ENTY','F',1,'Y','Individual Taxpayer',2);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('MOPT','C',1,'Y','Centralized',1);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('MOPT','D',1,'Y','Decentralized',2);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('TXCP','J',1,'Y','Corporate Taxpayer',1);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('PHRM','NEG',1,'Y','Negative',1);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('PHRM','POS',1,'Y','Positive',2);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('PHRM','NEU',1,'Y','Neutral',3);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('DITR','PPBBR',1,'Y','Incentive program for technology and research.',1);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('DITR','PRTGO',1,'Y','Reduction of base of ICMS for internal operations in Goias state.',2);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('DITR','RESARJ',1,'Y','Reduction of base of ICMS for agribusiness in Rio de Janeiro state.',3);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('DITR','TAEBA',1,'Y','Reduction of base of ICMS for suppliers in Bahia state.',4);
Insert into L10N_CODE_DETAIL_DESCS (L10N_CODE_TYPE,L10N_CODE,LANG,DEFAULT_LANG_IND,L10N_CODE_DESC,SEQ_NO) values ('DITR','TTERJ',1,'Y','Reduction of base of ICMS for chemical manufacturers in Rio de Janeiro state.',5);


REM
REM INSERTING into L10N_REC_GROUP_DESCS
REM

PROMPT INSERTING into L10N_REC_GROUP_DESCS

Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (1,1,'Y','List of Countries','Description','Country');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (2,1,'Y','List of Fiscal Classifications','Description','Fiscal Classification');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (3,1,'Y','List of NCM Characteristic Codes','Description','NCM Characteristic Code');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (4,1,'Y','List of EX IPI Codes','Description','EX IPI Code');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (5,1,'Y','List of Pauta Codes','Description','Pauta Code');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (6,1,'Y','List of Mastersaf Service Codes','Description','Mastersaf Service Code');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (7,1,'Y','List of Cities','Description','City');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (8,1,'Y','List of States','Description','State');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (10,1,'Y','List of Federal Service Codes','Decsription',' Federal Service Code');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (11,1,'Y','List of Util codes for PO','Description','Utilization Code');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (12,1,'Y','List of Util codes for TSF/MRT','Description','Utilization Code');
Insert into L10N_REC_GROUP_DESCS (L10N_REC_GROUP_ID,LANG,DEFAULT_LANG_IND,LOV_TITLE,LOV_COL1_HEADER,LOV_COL2_HEADER) values (13,1,'Y','List of State of Manufacture','Description','State');


REM
REM INSERTING into L10N_ATTRIB_GROUP_DESCS
REM

PROMPT INSERTING into L10N_ATTRIB_GROUP_DESCS

Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (1,1,'Y','Country Attributes');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (2,1,'Y','Item Fiscal Attributes');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (4,1,'Y','Transfer Entity Fiscal Address');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (5,1,'Y','Transfer Entity Registration Numbers');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (6,1,'Y','Transfer Entity Fiscal Classifications');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (7,1,'Y','Warehouse Fiscal Address');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (8,1,'Y','Warehouse Registration Numbers');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (9,1,'Y','Warehouse Fiscal Classifications');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (10,1,'Y','Warehouse Control Flags');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (11,1,'Y','Store Fiscal Address');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (12,1,'Y','Store Registration Numbers');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (13,1,'Y','Store Fiscal Classifications');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (14,1,'Y','Store Control Flags');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (15,1,'Y','Outside Location Fiscal Address');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (16,1,'Y','Outside Location Registration Numbers');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (17,1,'Y','Outside Location Fiscal Classifications');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (18,1,'Y','Supplier Fiscal Address');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (19,1,'Y','Supplier Registration Numbers');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (20,1,'Y','Supplier Fiscal Classifications');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (21,1,'Y','Partner Fiscal Address');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (22,1,'Y','Partner Registration Numbers');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (23,1,'Y','Partner Fiscal Classifications');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (24,1,'Y','Transfer Utilization codes');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (25,1,'Y','Purchase Order  Utilization codes');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (26,1,'Y','MRT Utilization codes');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (27,1,'Y','Store Fiscal Address');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (28,1,'Y','Store Registration Numbers');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (29,1,'Y','Store Fiscal Classifications');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (30,1,'Y','Store Control Flags');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (31,1,'Y','Set of Books Fiscal Address');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (32,1,'Y','Set of Books Registration Numbers');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (33,1,'Y','Set of Books Fiscal Classifications');
Insert into L10N_ATTRIB_GROUP_DESCS (GROUP_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (34,1,'Y','Customer Fiscal Attributes');

REM
REM INSERTING into L10N_ATTRIB_DESCS
REM

PROMPT INSERTING into L10N_ATTRIB_DESCS

Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (1,1,'Y','Fiscal Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (2,1,'Y','Fiscal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (3,1,'Y','Service Item');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (4,1,'Y','Merchandise Origin');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (5,1,'Y','NCM');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (6,1,'Y','NCM Characteristic');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (7,1,'Y','EX IPI');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (8,1,'Y','Pauta');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (9,1,'Y','Service Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (10,1,'Y','Federal Service');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (14,1,'Y','Taxpayer Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (15,1,'Y','Address');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (18,1,'Y','Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (19,1,'Y','City');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (20,1,'Y','State');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (21,1,'Y','Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (22,1,'Y','Postal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (23,1,'Y','CPF');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (24,1,'Y','CNPJ');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (26,1,'Y','NIT');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (27,1,'Y','SUFRAMA');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (28,1,'Y','City Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (29,1,'Y','State Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (30,1,'Y','IPI Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (31,1,'Y','Taxpayer Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (32,1,'Y','Address');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (35,1,'Y','Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (36,1,'Y','City');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (37,1,'Y','State');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (38,1,'Y','Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (39,1,'Y','Postal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (40,1,'Y','CPF');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (41,1,'Y','CNPJ');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (43,1,'Y','NIT');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (44,1,'Y','SUFRAMA');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (45,1,'Y','City Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (46,1,'Y','State Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (47,1,'Y','ISS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (48,1,'Y','Rural Producer');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (49,1,'Y','IPI Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (50,1,'Y','Matching Operation Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (51,1,'Y','Control Recovery of ST');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (52,1,'Y','Taxpayer Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (53,1,'Y','Address');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (56,1,'Y','Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (57,1,'Y','City');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (58,1,'Y','State');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (59,1,'Y','Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (60,1,'Y','Postal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (61,1,'Y','CPF');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (62,1,'Y','CNPJ');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (64,1,'Y','NIT');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (65,1,'Y','SUFRAMA');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (66,1,'Y','City Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (67,1,'Y','State Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (68,1,'Y','ISS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (69,1,'Y','Rural Producer');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (70,1,'Y','IPI Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (71,1,'Y','Matching Operation Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (72,1,'Y','Control Recovery of ST');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (73,1,'Y','Taxpayer Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (74,1,'Y','Address');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (77,1,'Y','Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (78,1,'Y','City');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (79,1,'Y','State');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (80,1,'Y','Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (81,1,'Y','Postal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (82,1,'Y','CPF');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (83,1,'Y','CNPJ');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (85,1,'Y','NIT');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (86,1,'Y','SUFRAMA');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (87,1,'Y','City Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (88,1,'Y','State Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (89,1,'Y','IPI Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (90,1,'Y','Taxpayer Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (91,1,'Y','Address');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (94,1,'Y','Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (95,1,'Y','City');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (96,1,'Y','State');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (97,1,'Y','Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (98,1,'Y','Postal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (99,1,'Y','CPF');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (100,1,'Y','CNPJ');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (102,1,'Y','NIT');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (103,1,'Y','SUFRAMA');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (104,1,'Y','City Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (105,1,'Y','State Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (106,1,'Y','ISS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (107,1,'Y','SIMPLES Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (108,1,'Y','ST Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (109,1,'Y','Rural Producer');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (110,1,'Y','IPI Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (111,1,'Y','Taxpayer Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (112,1,'Y','Address');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (115,1,'Y','Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (116,1,'Y','City');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (117,1,'Y','State');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (118,1,'Y','Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (119,1,'Y','Postal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (120,1,'Y','CPF');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (121,1,'Y','CNPJ');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (123,1,'Y','NIT');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (124,1,'Y','SUFRAMA');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (125,1,'Y','City Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (126,1,'Y','State Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (127,1,'Y','IPI Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (128,1,'Y','Utilization Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (129,1,'Y','Utilization Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (130,1,'Y','Utilization Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (131,1,'Y','Taxpayer Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (132,1,'Y','Address');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (135,1,'Y','Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (136,1,'Y','City');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (137,1,'Y','State');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (138,1,'Y','Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (139,1,'Y','Postal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (140,1,'Y','CPF');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (141,1,'Y','CNPJ');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (143,1,'Y','NIT');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (144,1,'Y','SUFRAMA');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (145,1,'Y','City Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (146,1,'Y','State Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (147,1,'Y','ISS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (148,1,'Y','Rural Producer');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (149,1,'Y','IPI Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (150,1,'Y','Matching Operation Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (151,1,'Y','Control Recovery of ST');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (152,1,'Y','ICMS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (153,1,'Y','ICMS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (154,1,'Y','ICMS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (155,1,'Y','ICMS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (156,1,'Y','ICMS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (158,1,'Y','PIS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (159,1,'Y','PIS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (160,1,'Y','PIS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (161,1,'Y','PIS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (162,1,'Y','PIS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (163,1,'Y','COFINS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (164,1,'Y','COFINS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (165,1,'Y','COFINS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (166,1,'Y','COFINS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (167,1,'Y','COFINS Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (168,1,'Y','Taxpayer Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (169,1,'Y','Address');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (172,1,'Y','Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (173,1,'Y','City');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (174,1,'Y','State');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (175,1,'Y','Country');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (176,1,'Y','Postal Code');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (177,1,'Y','CPF');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (178,1,'Y','CNPJ');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (179,1,'Y','NIT');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (180,1,'Y','SUFRAMA');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (181,1,'Y','City Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (182,1,'Y','State Inscription');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (183,1,'Y','IPI Contributor');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (184,1,'Y','State of Manufacture');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (185,1,'Y','Pharmaceutical List Type');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (186,1,'Y','Is income range eligible');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (187,1,'Y','Is distributor a manufacturer');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (188,1,'Y','ICMS Simples rate');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (189,1,'Y','Customer Neighborhood');
Insert into L10N_ATTRIB_DESCS (ATTRIB_ID,LANG,DEFAULT_LANG_IND,DESCRIPTION) values (190,1,'Y','Customer CPF');


insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (1, 1,'Tributary Substitution','Y');
insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (2, 1,'Tributary Substitution','Y');
insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (3, 1,'Tributary Substitution','Y');
insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (4, 1, 'CNAE Codes', 'Y');
insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (5, 1, 'CNAE Codes', 'Y');
insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (6, 1, 'CNAE Codes', 'Y');
insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (8, 1, 'CNAE Codes', 'Y');
insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (9, 1, 'CNAE Codes', 'Y');
insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (10, 1, 'CNAE Codes', 'Y');
Insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) VALUES (11, 1, 'Supplier Tax Regime', 'Y');
Insert into L10N_MENU_DESCS (L10N_MENU_ID,LANG,DESCRIPTION,DEFAULT_LANG_IND) values (12,1,'Tributary Substitution','Y');

COMMIT;
