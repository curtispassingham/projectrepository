PROMPT Deleting from Navigate tables

delete nav_element_mode_role 
 where element = 'l10nbrfiscalclass';

delete nav_element_mode 
 where element = 'l10nbrfiscalclass';

delete nav_folder
 where folder = 'FISCAL_CLASSIFICATION';

delete nav_element_mode_base
 where element = 'l10nbrfiscalclass';

delete nav_element 
 where element = 'l10nbrfiscalclass';

delete from nav_folder_base
 where folder = 'FISCAL_CLASSIFICATION';

delete nav_element_mode_role
 where element = 'l10nbrfreclass';

delete nav_element_mode
 where element = 'l10nbrfreclass';

delete nav_element_mode_role
 where element = 'l10nbrfreclassv';

delete nav_element_mode
 where element = 'l10nbrfreclassv';

delete nav_element_mode_base
 where element = 'l10nbrfreclassv';

delete nav_element
 where element = 'l10nbrfreclassv';

delete nav_folder
 where folder = 'FRECLASS';

delete nav_element_mode_base
 where element = 'l10nbrfreclass';

delete nav_element
 where element = 'l10nbrfreclass';

delete from nav_folder_base
 where folder = 'FRECLASS';

PROMPT Inserting into NAV_FOLDER_BASE

insert into nav_folder_base(folder, 
                            folder_name,
                            parent_folder,
                            user_id,
                            allow_access_during_batch) 
                      values('FISCAL_CLASSIFICATION', 
                             'Fiscal Classification',
                             'ITEM_RELATED',
                             NULL,
                             'N');

insert into nav_folder_base(folder,
                            folder_name,
                            parent_folder,
                            user_id,
                            allow_access_during_batch)
                     values('FRECLASS',
                            'Fiscal Reclassification',
                            'RECLASSIFICATION',
                            NULL,
                            'N');

PROMPT Inserting into NAV_ELEMENT

insert into nav_element(element,
                        element_type,
                        component)
                 values('l10nbrfiscalclass',
                        'F',
                        'RMS');

insert into nav_element(element,
                        element_type,
                        component)
                 values('l10nbrfreclass',
                        'F',
                        'RMS');

insert into nav_element(element,
                        element_type,
                        component)
                 values('l10nbrfreclassv',
                        'F',
                        'RMS');

PROMPT Inserting into NAV_ELEMENT_MODE_BASE

insert into nav_element_mode_base(element,
                                  nav_mode,
                                  folder,
                                  element_mode_name,
                                  allow_access_during_batch)
                            values('l10nbrfiscalclass',
                                   'VIEW',
                                   'FISCAL_CLASSIFICATION',
                                   'View',
                                   'N');

insert into nav_element_mode_base(element,
                                  nav_mode,
                                  folder,
                                  element_mode_name,
                                  allow_access_during_batch)
                            values('l10nbrfiscalclass',
                                   'EDIT',
                                   'FISCAL_CLASSIFICATION',
                                   'Edit',
                                   'N');

insert into nav_element_mode_base(element,
                                  nav_mode,
                                  folder,
                                  element_mode_name,
                                  allow_access_during_batch)
                            values('l10nbrfreclass',
                                   'NEW',
                                   'FRECLASS',
                                   'New',
                                   'N');

insert into nav_element_mode_base(element,
                                  nav_mode,
                                  folder,
                                  element_mode_name,
                                  allow_access_during_batch)
                            values('l10nbrfreclassv',
                                   '--DEFAULT--',
                                   'FRECLASS',
                                   'View/Maintain',
                                   'N');

PROMPT Inserting into NAV_FOLDER

insert into nav_folder(folder,
                       folder_name,
                       parent_folder,
                       user_id,
                       allow_access_during_batch)
                values('FISCAL_CLASSIFICATION',
                       'Fiscal Classification',
                       'ITEM_RELATED',
                       NULL,
                       'N');

insert into nav_folder(folder,
                       folder_name,
                       parent_folder,
                       user_id,
                       allow_access_during_batch)
                values('FRECLASS',
                       'Fiscal Reclassification',
                       'RECLASSIFICATION',
                       NULL,
                       'N');


PROMPT Inserting into NAV_ELEMENT_MODE

insert into nav_element_mode(element,
                             nav_mode,
                             folder,
                             element_mode_name,
                             user_id,
                             allow_access_during_batch)
                      values('l10nbrfiscalclass',
                             'VIEW',
                             'FISCAL_CLASSIFICATION',
                             'View',
                             NULL,
                             'N');

insert into nav_element_mode(element,
                             nav_mode,
                             folder,
                             element_mode_name,
                             user_id,
                             allow_access_during_batch)
                      values('l10nbrfiscalclass',
                             'EDIT',
                             'FISCAL_CLASSIFICATION',
                             'Edit',
                             NULL,
                             'N');

insert into nav_element_mode(element,
                             nav_mode,
                             folder,
                             element_mode_name,
                             user_id,
                             allow_access_during_batch)
                      values('l10nbrfreclass',
                             'NEW',
                             'FRECLASS',
                             'New',
                             NULL,
                             'N');

insert into nav_element_mode(element,
                             nav_mode,
                             folder,
                             element_mode_name,
                             user_id,
                             allow_access_during_batch)
                      values('l10nbrfreclassv',
                             '--DEFAULT--',
                             'FRECLASS',
                             'View/Maintain',
                             NULL,
                             'N');


PROMPT Inserting into NAV_ELEMENT_MODE_ROLE

insert into nav_element_mode_role(element,
                                  nav_mode,
                                  folder,
                                  role)
                           values('l10nbrfiscalclass',
                                  'VIEW',
                                  'FISCAL_CLASSIFICATION',
                                  'DEVELOPER');

insert into nav_element_mode_role(element,
                                  nav_mode,
                                  folder,
                                  role)
                           values('l10nbrfiscalclass',
                                  'EDIT',
                                  'FISCAL_CLASSIFICATION',
                                  'DEVELOPER');

insert into nav_element_mode_role(element,
                                  nav_mode,
                                  folder,
                                  role)
                           values('l10nbrfreclass',
                                  'NEW',
                                  'FRECLASS',
                                  'DEVELOPER');

insert into nav_element_mode_role(element,
                                  nav_mode,
                                  folder,
                                  role)
                           values('l10nbrfreclassv',
                                  '--DEFAULT--',
                                  'FRECLASS',
                                  'DEVELOPER');

COMMIT;
/
