---
set feedback off
set define off

delete from fm_sped_last_run_date;

Insert into FM_SPED_LAST_RUN_DATE (LAST_RUN_DATE) values (SYSDATE);

commit;
set feedback on
set define on
PROMPT Done.

/