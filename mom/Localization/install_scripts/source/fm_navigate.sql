prompt Remove Existing Fiscal Management Folder

prompt deleting NAV_ELEMENT_MODE_ROLE...

delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_loc_login' and NAV_MODE = '--DEFAULT--' and FOLDER = 'Fiscal Management';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_loc_login' and NAV_MODE = '--DEFAULT--' and FOLDER = 'FISCAL_MANAGEMENT';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_loc_login' and NAV_MODE = '--DEFAULT--' and FOLDER = 'FISCAL MANAGEMENT';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_error_log' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_ERROR_LOG';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_fiscal_doc_type' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_FIS_DOC_TYPE';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_fiscal_doc_type' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_FIS_DOC_TYPE';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_utilization' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_UTILIZATION';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_utilization' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_UTILIZATION';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_schedule' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_FISCAL_RECEIVING';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_schedule' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_FISCAL_RECEIVING';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_tolerances' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_TOLERANCES';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_tolerances' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_TOLERANCES';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_system_options' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_SYSTEM_OPTIONS';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_system_options' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_SYSTEM_OPTIONS';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_nfe_config_loc' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_NFE_CONFIGURATION';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_nfe_config_loc' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_NFE_CONFIGURATION';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_fiscal_find' and NAV_MODE = '--DEFAULT--' and FOLDER = 'ORFM_FISCAL_DOC';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_drm_header' and NAV_MODE = '--DEFAULT--' and FOLDER = 'ORFM_DISCREP_RESOLUTION';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_drm_header' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_DISCREP_RESOLUTION';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_drm_header' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_DISCREP_RESOLUTION';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_int_errors' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_INT_ERRORS';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_int_errors' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_INT_ERRORS';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_loc_fiscal_number' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_LOC_FISCAL_NUMBER';
delete from NAV_ELEMENT_MODE_ROLE where ELEMENT = 'orfm_loc_fiscal_number' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_LOC_FISCAL_NUMBER';

prompt deleting NAV_ELEMENT_MODE...

delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_loc_login' and NAV_MODE = '--DEFAULT--' and FOLDER = 'Fiscal Management';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_loc_login' and NAV_MODE = '--DEFAULT--' and FOLDER = 'FISCAL_MANAGEMENT';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_loc_login' and NAV_MODE = '--DEFAULT--' and FOLDER = 'FISCAL MANAGEMENT';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_fiscal_find' and NAV_MODE = '--DEFAULT--' and FOLDER = 'ORFM_FISCAL_DOC';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_error_log' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_ERROR_LOG';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_fiscal_doc_type' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_FIS_DOC_TYPE';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_fiscal_doc_type' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_FIS_DOC_TYPE';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_utilization' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_UTILIZATION';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_utilization' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_UTILIZATION';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_schedule' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_FISCAL_RECEIVING';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_schedule' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_FISCAL_RECEIVING';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_tolerances' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_TOLERANCES';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_tolerances' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_TOLERANCES';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_system_options' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_SYSTEM_OPTIONS';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_system_options' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_SYSTEM_OPTIONS';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_nfe_config_loc' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_NFE_CONFIGURATION';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_nfe_config_loc' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_NFE_CONFIGURATION';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_drm_header' and NAV_MODE = '--DEFAULT--' and FOLDER = 'ORFM_DISCREP_RESOLUTION';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_drm_header' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_DISCREP_RESOLUTION';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_drm_header' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_DISCREP_RESOLUTION';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_int_errors' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_INT_ERRORS';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_int_errors' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_INT_ERRORS';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_loc_fiscal_number' and NAV_MODE = 'EDIT' and FOLDER = 'ORFM_LOC_FISCAL_NUMBER';
delete from NAV_ELEMENT_MODE where ELEMENT = 'orfm_loc_fiscal_number' and NAV_MODE = 'VIEW' and FOLDER = 'ORFM_LOC_FISCAL_NUMBER';

prompt deleting NAV_ELEMENT...

delete from  NAV_ELEMENT where element = 'orfm_loc_login';
delete from  NAV_ELEMENT where element = 'orfm_error_log';
delete from  NAV_ELEMENT where element = 'orfm_fiscal_find';
delete from  NAV_ELEMENT where element = 'orfm_fiscal_doc_type';
delete from  NAV_ELEMENT where element = 'orfm_utilization';
delete from  NAV_ELEMENT where element = 'orfm_schedule';
delete from  NAV_ELEMENT where element = 'orfm_tolerances';
delete from  NAV_ELEMENT where element = 'orfm_nfe_config_loc';
delete from  NAV_ELEMENT where element = 'orfm_system_options';
delete from  NAV_ELEMENT where element = 'orfm_drm_header';
delete from  NAV_ELEMENT where element = 'orfm_int_errors';
delete from  NAV_ELEMENT where element = 'orfm_loc_fiscal_number';

prompt deleting NAV_FOLDER...

prompt  Disable Constraints

ALTER TABLE nav_folder DISABLE CONSTRAINT NFO_NFO_FK;

delete from NAV_FOLDER where folder = 'ORFM_FISCAL_RECEIVING';
delete from NAV_FOLDER where folder = 'ORFM_DISCREP_RESOLUTION';
delete from NAV_FOLDER where folder = 'ORFM_ERROR_LOG';
delete from NAV_FOLDER where folder = 'ORFM_UTILIZATION';
delete from NAV_FOLDER where folder = 'ORFM_FIS_DOC_TYPE';
delete from NAV_FOLDER where folder = 'ORFM_NFE_CONFIGURATION';
delete from NAV_FOLDER where folder = 'ORFM_TOLERANCES';
delete from NAV_FOLDER where folder = 'ORFM_SYSTEM_OPTIONS';
delete from NAV_FOLDER where folder = 'ORFM_FISCAL_DOC';
delete from NAV_FOLDER where folder = 'ORFM_INT_ERRORS';
delete from NAV_FOLDER where folder = 'ORFM_FISCAL_CONFIGURATION';
delete from NAV_FOLDER where folder = 'ORFM_LOC_FISCAL_NUMBER';
delete from NAV_FOLDER where folder = 'ORFM_SYSTEM_SETUP';
delete from NAV_FOLDER where folder = 'Fiscal Management';
delete from NAV_FOLDER where folder = 'FISCAL MANAGEMENT';
delete from NAV_FOLDER where folder = 'FISCAL_MANAGEMENT';

prompt deleting nav_component...

delete from nav_component where component = 'FISCAL_MANAGEMENT';
delete from nav_component where component = 'FISCAL MANAGEMENT';
delete from nav_component where component = 'Fiscal Management';

prompt Loading nav_component...

insert into nav_component values ('FISCAL_MANAGEMENT','--NONE--');  

prompt Loading NAV_FOLDER...


insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('FISCAL_MANAGEMENT', 'Fiscal Management',  null, null, 'Y');

insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_SYSTEM_SETUP', 'System Setup', 'FISCAL_MANAGEMENT', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_FISCAL_CONFIGURATION', 'Fiscal Configuration', 'FISCAL_MANAGEMENT', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_FISCAL_RECEIVING', 'Receiving / Issue', 'FISCAL_MANAGEMENT', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_DISCREP_RESOLUTION', 'Discrepancy Resolution', 'FISCAL_MANAGEMENT', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_ERROR_LOG', 'Errors Log', 'FISCAL_MANAGEMENT', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_FISCAL_DOC', 'Fiscal Documents', 'FISCAL_MANAGEMENT', null, 'Y');

insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_UTILIZATION', 'Fiscal Utilization', 'ORFM_FISCAL_CONFIGURATION', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_FIS_DOC_TYPE', 'Fiscal Document Types', 'ORFM_FISCAL_CONFIGURATION', null, 'Y');

insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_NFE_CONFIGURATION', 'NFe Configuration', 'ORFM_SYSTEM_SETUP', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_TOLERANCES', 'Tolerances', 'ORFM_SYSTEM_SETUP', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('ORFM_SYSTEM_OPTIONS', 'System Options', 'ORFM_SYSTEM_SETUP', null, 'Y');
insert into NAV_FOLDER (FOLDER, FOLDER_NAME, PARENT_FOLDER, USER_ID, ALLOW_ACCESS_DURING_BATCH) 
values ('ORFM_LOC_FISCAL_NUMBER', 'Fiscal Numbers', 'ORFM_SYSTEM_SETUP', null, 'Y');


prompt Loading NAV_ELEMENT...
insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_loc_login', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_error_log', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_fiscal_find', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_fiscal_doc_type', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_utilization', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_schedule', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_tolerances', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_nfe_config_loc', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_system_options', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_drm_header', 'F', 'FISCAL_MANAGEMENT');

insert into NAV_ELEMENT (ELEMENT, ELEMENT_TYPE, COMPONENT)
values ('orfm_loc_fiscal_number', 'F', 'FISCAL_MANAGEMENT');

prompt Loading NAV_ELEMENT_MODE...

insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_drm_header', 'VIEW', 'ORFM_DISCREP_RESOLUTION', 'View', null, 'Y');

insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_drm_header', 'EDIT', 'ORFM_DISCREP_RESOLUTION', 'Edit', null, 'Y');

insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_loc_login', '--DEFAULT--', 'FISCAL_MANAGEMENT', 'Login of Location', null, 'Y');

insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_error_log', 'VIEW', 'ORFM_ERROR_LOG', 'View', null, 'Y');


insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_fiscal_doc_type', 'EDIT', 'ORFM_FIS_DOC_TYPE', 'Edit', null, 'Y');
insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_fiscal_doc_type', 'VIEW', 'ORFM_FIS_DOC_TYPE', 'View', null, 'Y');

insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_utilization', 'EDIT', 'ORFM_UTILIZATION', 'Edit', null, 'Y');
insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_utilization', 'VIEW', 'ORFM_UTILIZATION', 'View', null, 'Y');


insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_schedule', 'EDIT', 'ORFM_FISCAL_RECEIVING', 'Edit', null, 'Y');
insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_schedule', 'VIEW', 'ORFM_FISCAL_RECEIVING', 'View', null, 'Y');


insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_tolerances', 'EDIT', 'ORFM_TOLERANCES', 'Edit', null, 'Y');
insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_tolerances', 'VIEW', 'ORFM_TOLERANCES', 'View', null, 'Y');

insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_system_options', 'EDIT', 'ORFM_SYSTEM_OPTIONS', 'Edit', null, 'Y');
insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_system_options', 'VIEW', 'ORFM_SYSTEM_OPTIONS', 'View', null, 'Y');


insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_nfe_config_loc', 'EDIT', 'ORFM_NFE_CONFIGURATION', 'Edit', null, 'Y');
insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_nfe_config_loc', 'VIEW', 'ORFM_NFE_CONFIGURATION', 'View', null, 'Y');


insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_fiscal_find', '--DEFAULT--', 'ORFM_FISCAL_DOC', 'Fiscal Documents', null, 'Y');

insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_loc_fiscal_number', 'EDIT', 'ORFM_LOC_FISCAL_NUMBER', 'Edit', null, 'Y');
insert into NAV_ELEMENT_MODE (ELEMENT, NAV_MODE, FOLDER, ELEMENT_MODE_NAME, USER_ID, ALLOW_ACCESS_DURING_BATCH)
values ('orfm_loc_fiscal_number', 'VIEW', 'ORFM_LOC_FISCAL_NUMBER', 'View', null, 'Y');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_loc_login', '--DEFAULT--', 'FISCAL_MANAGEMENT', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_error_log', 'VIEW', 'ORFM_ERROR_LOG', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_fiscal_doc_type', 'EDIT', 'ORFM_FIS_DOC_TYPE', 'DEVELOPER');
insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_fiscal_doc_type', 'VIEW', 'ORFM_FIS_DOC_TYPE', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_utilization', 'EDIT', 'ORFM_UTILIZATION', 'DEVELOPER');
insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_utilization', 'VIEW', 'ORFM_UTILIZATION', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_schedule', 'EDIT', 'ORFM_FISCAL_RECEIVING', 'DEVELOPER');
insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_schedule', 'VIEW', 'ORFM_FISCAL_RECEIVING', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_tolerances', 'EDIT', 'ORFM_TOLERANCES', 'DEVELOPER');
insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_tolerances', 'VIEW', 'ORFM_TOLERANCES', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_system_options', 'EDIT', 'ORFM_SYSTEM_OPTIONS', 'DEVELOPER');
insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_system_options', 'VIEW', 'ORFM_SYSTEM_OPTIONS', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_nfe_config_loc', 'EDIT', 'ORFM_NFE_CONFIGURATION', 'DEVELOPER');
insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_nfe_config_loc', 'VIEW', 'ORFM_NFE_CONFIGURATION', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_fiscal_find', '--DEFAULT--', 'ORFM_FISCAL_DOC', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_drm_header', 'VIEW', 'ORFM_DISCREP_RESOLUTION', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_drm_header', 'EDIT', 'ORFM_DISCREP_RESOLUTION', 'DEVELOPER');

insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_loc_fiscal_number', 'EDIT', 'ORFM_LOC_FISCAL_NUMBER', 'DEVELOPER');
insert into NAV_ELEMENT_MODE_ROLE (ELEMENT, NAV_MODE, FOLDER, ROLE)
values ('orfm_loc_fiscal_number', 'VIEW', 'ORFM_LOC_FISCAL_NUMBER', 'DEVELOPER');

commit;

prompt  Enable  Constraints

ALTER TABLE nav_folder ENABLE CONSTRAINT NFO_NFO_FK;
/




