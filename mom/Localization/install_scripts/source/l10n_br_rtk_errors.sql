set feedback off
set define off

REM -------------------------------------------------
REM
REM  Refreshes rtk_errors table for L10NBR errors.
REM
REM -------------------------------------------------

REM -------------------------------------------------


delete from rtk_errors where rtk_user = 'L10NBR' and rtk_lang = 1;

REM ****************************************************************
PROMPT inserting into rtk_errors
REM ****************************************************************

---
INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL', '?MERGE_ATTRIB', 1, 'Some of the attribute values are not selected. Do you want the values to be added?','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) 
                VALUES('BL', 'CANNOT_DEL_PRIMARY_CNAE', 1, 'Cannot delete primary cnae code.', 'L10NBR', 'Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','CUST_ATTR_INVALID',1,'Not a valid custom attribute. Base table: %s1, attribute: %s2.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','CUST_ATTRIB_EXISTS', 1, 'This attribute has been entered on the reclassification','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','ENTER_ATTRIB', 1, 'Please select an attribute','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','ENTER_NCM_CODE',1,'Please enter a NCM code for %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','ENTER_NCMCHAR_CODE',1,'Please enter a NCM characteristics code for %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','EXT_ENTITY_KEY_INVALID',1,'Extension entity key not valid for base table. Base table: %s1, key: %s2','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','EXT_ENTITY_NOT_FOUND',1,'Extension entity not defined for base table %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) 
                VALUES('BL','FED_SERV_REQ',1,'A Federal Service value is required.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_EXT_ENTITY_KEY',1,'Not a valid list of extension entity keys in the input.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INVALID_ATTRIB', 1, 'This attribute is not defined','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','L10N_NOP_NOT_FOUND',1,'The nature of operation for the purchase tax call was not found. Tax service id: %s1','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','L10N_ROUTE_INFO_NOT_FOUND',1,'The routing information for tax service id %s1 was not found.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','L10N_RTIL_URL_NOT_FOUND',1,'The RTIL service URL was not found in the %s1 table.','L10NBR','Y');
               
INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_CLASS_ID',1,'The Classification ID must be 7 characters in length when Classification type is Service','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_CITY_COUNTRY',1,'This city/country relationship does not exist for %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_CITY_STATE',1,'This city/state relationship does not exist for %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_CNAE_CODE',1,'Invalid CNAE code entered: %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_FEDSERV_CODE',1,'Invalid federal service code entered: %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_IPI_CODE',1,'Invalid ex ipi code entered: %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_NCM_CODE',1,'Invalid NCM code entered: %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_NCMCHAR_CODE',1,'Invalid NCM characteristics code entered: %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_PAUTA_CODE',1,'Invalid pauta code entered: %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_SERVICE_CODE',1,'Invalid service code entered: %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','INV_UTIL_CODE',1,'Invalid utilisation code entered: %s1.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE, RTK_KEY, RTK_LANG, RTK_TEXT, RTK_USER, RTK_APPROVED)
                VALUES('BL','MISSING ATTRIBUTES', 1, 'Custom attributes need to be entered','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','NCM_CHAR_PAUTA',1,'NCM_CHAR_CODE must be defined when PAUTA_CODE is defined.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) 
                VALUES('BL','NCM_REQ',1,'An NCM value is required.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','NO_ENTITY',1,'Entities associated with the tax service id do not have fiscal attributes defined. Tax service id: %s1','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) 
                VALUES('BL','NO_FISCAL_ATTRIB_EXIST',1,'No fiscal attributes exist.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) 
                VALUES('BL', 'PRIMARY_CNAE_REQ', 1, 'One primary cnae code has to be present.', 'L10NBR', 'Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) 
                VALUES('BL','SERV_CODE_REQ',1,'A Service Code value is required.','L10NBR','Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) 
                VALUES('BL','SERVICE_IND_CHECK',1,'This is a Service Item. The NCM code, NCM Characteristic, EX IPI, Pauta, State of Manufacture, and Pharma List Type should be NULL.', 'L10NBR', 'Y');

INSERT INTO RTK_ERRORS(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED)
                VALUES('BL','SERVICE_IND_REQ',1,'A service item designation is required.', 'L10NBR', 'Y');

---
commit;
set feedback on
set define on
