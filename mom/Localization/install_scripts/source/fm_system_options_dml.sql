prompt Deleting FM_SYSTEM_OPTIONS...

delete from FM_SYSTEM_OPTIONS
where
variable in 
('HOUR_FORMAT',
'PURGE_DAYS',
'ORIGIN_CST_IND',
'DOC_TYPE_PO',
'PERCENT_FORMAT',
'NUMBER_FORMAT',
'DEFAULT_PO_TYPE',
'DEFAULT_SALES_TYPE',
'DEFAULT_CURRENCY',
'TOLERANCES_MANDATORY',
'DEFAULT_DOCUMENT_TYPE',
'DEFAULT_FREIGHT_TYPE',
'QTY_FORMAT',
'RFC_REASON',
'ORFMI_MIN_DAYS',
'ORFMO_MIN_DAYS',
'ORFMO_MAX_DAYS',
'ORFMI_MAX_DAYS',
'EDI_DEF_PTNR_ID',
'EDI_DEF_PTNR_TYPE',
'GET_ORIGIN_DOC_TAXES',
'CHECK_APPROVED_DOCS',
'DEFAULT_COUNTRY',
'CALC_TOL_TYPE',
'CALC_TOL_VALUE',
'QTY_RESOLUTION_RULE',
'COST_RESOLUTION_RULE',
'TAX_RESOLUTION_RULE',
'DEFAULT_RMA_UTIL_ID',
'DEFAULT_RMA_DOC_TYPE',
'DEFAULT_RTV_DOC_TYPE',
'DEFAULT_RTV_UTIL_ID',
'EDI_RTV',
'DEFAULT_OUTBOUND_TSF_DOC_TYPE',
'DEFAULT_OUTBOUND_TSF_UTIL_ID',
'DEFAULT_INBOUND_TSF_DOC_TYPE',
'DEFAULT_INBOUND_TSF_UTIL_ID',
'EDI_TSF',
'DEFAULT_STOCK_DOC_TYPE',
'EDI_STOCK',
'TOLERANCES_PRIORITY',
'DEFAULT_RNF_UTILIZATION_ID',
'HISTORY_DAYS_COMPLETED_NON_POs',
'HISTORY_DAYS_COMPLETED_PO',
'HISTORY_DAYS_DELETED_DOCS',
'HISTORY_DAYS_HIST_TABLES',
'HISTORY_DAYS_WORKSHEET_STATUS',
'DEFAULT_NFE_DOC_TYPE',
'RECOVERABLE_TAX_CST',
'DEFAULT_INBOUND_IC_DOC_TYPE',
'DEFAULT_INBOUND_IC_UTIL_ID',
'DEFAULT_OUTBOUND_IC_DOC_TYPE',
'DEFAULT_OUTBOUND_IC_UTIL_ID',
'DEFAULT_RURAL_PROD_UTILIZATION',
'DEFAULT_RURAL_PROD_DOC_TYPE',
'EDI_REP',
'DEFAULT_OUTBOUND_REP_UTIL_ID',
'DEFAULT_OUTBOUND_REP_DOC_TYPE',
'DEFAULT_INBOUND_REP_DOC_TYPE',
'DEFAULT_INBOUND_REP_UTIL_ID',
'DEFAULT_OUTBOUND_CUST_UTIL_ID',
'DEFAULT_NOP');

commit;
prompt Loading FM_SYSTEM_OPTIONS...
insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('HOUR_FORMAT', 'CHAR', 'Format to represent fields of hour. Must be ''HH24:MI'' or ''HH:MI AM''.', null, 'HH24:MI', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('PURGE_DAYS', 'NUMBER', 'Days before records are purged from the tables', 0, null, null, sysdate,user, sysdate,user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('ORIGIN_CST_IND', 'CHAR', 'Default Origin Code and CST Type for ICMS tax situation', null, '000', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DOC_TYPE_PO', 'NUMBER', 'Default fiscal doc type code (e.g. 1 - Nota Fiscal - Modelo 1, 1A) ', 1, null, null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('PERCENT_FORMAT', 'CHAR', 'Percentage Field Format', null, 'FM9G999G999G999G999G990D9000"%"', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('NUMBER_FORMAT', 'CHAR', 'Value Field Format', null, 'FM9G999G999G999G990D90PR', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_PO_TYPE', 'CHAR', 'Default PO Type when null in ORMS (fiscal utilization code in ORFM)', null, '1000', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_SALES_TYPE', 'CHAR', 'Default SALES Type when null in ORMS (fiscal utilization code in ORFM)', null, 'SALE', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_COUNTRY', 'CHAR', 'Default supplier country', null, 'BR', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_CURRENCY', 'CHAR', 'Default currency code', null, 'BRL', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_DOCUMENT_TYPE', 'NUMBER', 'Default document type used by the process to create an EDI document based on external systems.', 1, null, null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('TOLERANCES_MANDATORY', 'CHAR', 'Tolerances are applied or not', null, 'Y', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_FREIGHT_TYPE', 'CHAR', 'Default freight type used by the process to create an EDI document based on external systems.', null, 'CIF', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('QTY_FORMAT', 'CHAR', 'Quantity Field Format', null, 'FM9G999G999G999G999G999G990D0', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('RFC_REASON', 'NUMBER', 'Return from customer reason code.', 0, null, null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('ORFMI_MIN_DAYS', 'NUMBER', 'Number of days minus the ORFMi date', 0, null, null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('ORFMO_MIN_DAYS', 'NUMBER', 'Number of days minus the ORFMo date', 0, null, null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('ORFMO_MAX_DAYS', 'NUMBER', 'Number of days minus the ORFMo date', 0, null, null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('ORFMI_MAX_DAYS', 'NUMBER', 'Number of days minus the ORFMi date', 0, null, null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('EDI_DEF_PTNR_ID', 'CHAR', 'Indicates the default partner id when importing EDI documents', null, '55', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('EDI_DEF_PTNR_TYPE', 'CHAR', 'Indicates the default partner type when importing EDI documents', null, 'E', null, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('GET_ORIGIN_DOC_TAXES', 'CHAR', 'Requisition type for use of parameters of taxes of the origin fiscal document.', null, 'RTV, RMA', null, sysdate, user, sysdate, user);

insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('CHECK_APPROVED_DOCS', 'CHAR',  'Variable to check whether it is mandatory to approve all fiscal documents before approving the schedule.', NULL, 'Y', NULL, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('CALC_TOL_TYPE', 'CHAR', 'The calculation tolerance type (P/V).', NULL, 'P', null, sysdate, user, sysdate, user);

insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('CALC_TOL_VALUE', 'NUMBER',  'The calculation tolerance value.', 1, NULL, NULL, sysdate, user, sysdate, user);

insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('QTY_RESOLUTION_RULE', 'CHAR',  'Default Resolution Action for Quantity Discrepancies', NULL, 'SYS', NULL, sysdate, user, sysdate, user);

insert into FM_SYSTEM_OPTIONS (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('COST_RESOLUTION_RULE', 'CHAR', 'Default Resolution Action for Cost Discrepancies', NULL, 'SYS', null, sysdate, user, sysdate, user);

insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('TAX_RESOLUTION_RULE', 'CHAR',  'Default Resolution Action for Tax Discrepancies', NULL, 'SYS', NULL, sysdate, user, sysdate, user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_RMA_UTIL_ID', 'CHAR', 'Default Utilization ID for RMA', NULL,'100',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_RMA_DOC_TYPE','NUMBER','Default Document Type for RMA',5,null,null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_RTV_DOC_TYPE','NUMBER','Default Document Type for RTVs.',2,null,null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_RTV_UTIL_ID','CHAR','Default Utilization ID for RTVs.',null,'81',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('EDI_RTV','CHAR','Indicates whether the requisition type for RTV is ''ENTRY'' or ''EXIT''',null,'EXIT',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_OUTBOUND_TSF_DOC_TYPE','NUMBER','Default Document Type for Outbound Transfers.',11,null,null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_OUTBOUND_TSF_UTIL_ID','CHAR','Default Utilization ID for Outbound Transfers.',null,'83',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID) 
values ('DEFAULT_INBOUND_TSF_DOC_TYPE','NUMBER','Default Document Type for Inbound Transfers.',15,null,null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_INBOUND_TSF_UTIL_ID','CHAR','Default Utilization ID for Inbound Transfers.',null,'84',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID) 
values ('EDI_TSF','CHAR','Indicates whether the requisition type for TSF is ''ENTRY'' or ''EXIT''',null,'EXIT',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID) 
values ('DEFAULT_STOCK_DOC_TYPE','NUMBER','Default Document Type for Inventory Adjustments.',14,null,null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('EDI_STOCK','CHAR','Indicates whether the requisition type for STOCK is ''ENTRY'' or ''EXIT''',null,'EXIT',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('TOLERANCES_PRIORITY','CHAR','Tolerances Priority is applied: All, Supplier, Location, Company',null,'A',null,sysdate,user,sysdate,user);

insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values('DEFAULT_RNF_UTILIZATION_ID', 'CHAR' , 'Default Utilization Id for Return NF', null,'81',null,sysdate,user,sysdate,user);

insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values('HISTORY_DAYS_COMPLETED_NON_POs', 'NUMBER' , 'Indicates the number of days for completed docs other than POs', 180,null,null,sysdate,user,sysdate,user);

insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values('HISTORY_DAYS_COMPLETED_PO', 'NUMBER' , 'Indicates the number of days for the completed POs', 360,null,null,sysdate,user,sysdate,user);

insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values('HISTORY_DAYS_DELETED_DOCS', 'NUMBER' , 'Indicates the number of days  for the Deleted NF docs', 30,null,null,sysdate,user,sysdate,user);

insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values('HISTORY_DAYS_HIST_TABLES', 'NUMBER' , 'Indicates the number of days the history table can hold the records', 180,null,null,sysdate,user,sysdate,user);

insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values('HISTORY_DAYS_WORKSHEET_STATUS', 'NUMBER' , 'Indicates the number of days the NF can be worksheet status', 60,null,null,sysdate,user,sysdate,user);

insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values('DEFAULT_NFE_DOC_TYPE', 'NUMBER' , 'Default Document Type for Nfe', 55,null,null,sysdate,user,sysdate,user);

insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values('RECOVERABLE_TAX_CST','CHAR','Value related to the selected CST',null,'001,002,003,004,005,006,007,008,009,010',null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values ('DEFAULT_INBOUND_IC_DOC_TYPE','NUMBER','Default Document Type for Inbound IC Transfers.',16,null,null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('DEFAULT_INBOUND_IC_UTIL_ID','CHAR','Default Utilization ID for Inbound IC Transfers.',null,'87',null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('DEFAULT_OUTBOUND_IC_DOC_TYPE','NUMBER','Default Document Type for Outbound IC Transfers.',12,null,null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('DEFAULT_OUTBOUND_IC_UTIL_ID','CHAR','Default Utilization ID for Outbound IC Transfers.',null,'86',null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('DEFAULT_RURAL_PROD_UTILIZATION','CHAR','Default Utilization Id for Rural Producer NF',null,'82',null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('DEFAULT_RURAL_PROD_DOC_TYPE','NUMBER','Default Document Type for Rural Producer',9,null,null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values ('DEFAULT_INBOUND_REP_DOC_TYPE','NUMBER','Default Document Type for Inbound Repairing Transfers.',40,null,null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('DEFAULT_INBOUND_REP_UTIL_ID','CHAR','Default Utilization ID for Inbound Repairing Transfers.',null,'66',null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('DEFAULT_OUTBOUND_REP_DOC_TYPE','NUMBER','Default Document Type for Outbound Repairing Transfers.',41,null,null,sysdate,user,sysdate,user);

Insert into FM_SYSTEM_OPTIONS (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('DEFAULT_OUTBOUND_REP_UTIL_ID','CHAR','Default Utilization ID for Outbound Repairing Transfers.',null,'67',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('EDI_REP','CHAR','Indicates whether the requisition type for REP is ''ENTRY'' or ''EXIT''',null,'EXIT',null,sysdate,user,sysdate,user);

Insert into fm_system_options (VARIABLE, VARIABLE_TYPE, DESCRIPTION, NUMBER_VALUE, STRING_VALUE, DATE_VALUE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('DEFAULT_OUTBOUND_CUST_UTIL_ID','CHAR','Default Utilization ID for outbound customer order transfers.',NULL,'83',NULL,SYSDATE,USER,SYSDATE,USER);

Insert into fm_system_options (VARIABLE,VARIABLE_TYPE,DESCRIPTION,NUMBER_VALUE,STRING_VALUE,DATE_VALUE,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
values ('DEFAULT_NOP','CHAR','Default Nature of Operation',null,'001',null,sysdate,user,sysdate,user);

commit;
/