spool rfmseeddata.log;

--- Startall ---
prompt 'Starting File: l10n_br_state.sql';
@@l10n_br_state.sql
prompt 'Starting File: fm_code_head.sql';
@@fm_code_head.sql;
prompt 'Starting File: fm_code_detail.sql';
@@fm_code_detail.sql;
prompt 'Starting File: fm_code_detail_trans.sql';
@@fm_code_detail_trans.sql;
prompt 'Starting File: fm_form_metadata.sql';
@@fm_form_metadata.sql;
prompt 'Starting File: fm_multiview_saved_45.sql';
@@fm_multiview_saved_45.sql;
prompt 'Starting File: fm_navigate.sql';
@@fm_navigate.sql;
prompt 'Starting File: fm_restart_control.sql';
@@fm_restart_control.sql;
prompt 'Starting File: fm_rtk_errors.sql';
@@fm_rtk_errors.sql;
prompt 'Starting File: fm_rtk_reports.sql';
@@fm_rtk_reports.sql;
prompt 'Starting File: fm_sped_last_run_date_dml.sql';
@@fm_sped_last_run_date_dml.sql;
prompt 'Starting File: fm_system_options_dml.sql';
@@fm_system_options_dml.sql;
prompt 'Starting File: fm_tran_codes.sql';
@@fm_tran_codes.sql;
prompt 'Starting File: l10n_br_cntrytxjur.sql';
@@l10n_br_cntrytxjur.sql;
prompt 'Starting File: l10n_br_flex_setup.sql';
@@l10n_br_flex_setup.sql;
prompt 'Starting File: l10n_br_flex_tran_setup.sql';
@@l10n_br_flex_tran_setup.sql;
prompt 'Starting File: l10n_br_form_metadata.sql';
@@l10n_br_form_metadata.sql;
prompt 'Starting File: l10n_br_l10n_batch_config.sql';
@@l10n_br_l10n_batch_config.sql;
prompt 'Starting File: l10n_br_l10n_pkg_config.sql';
@@l10n_br_l10n_pkg_config.sql;
prompt 'Starting File: l10n_br_navigate.sql';
@@l10n_br_navigate.sql;
prompt 'Starting File: l10n_br_restart_control.sql';
@@l10n_br_restart_control.sql;
prompt 'Starting File: l10n_br_retail_service_report_url.sql';
@@l10n_br_retail_service_report_url.sql;
prompt 'Starting File: l10n_br_rtk_errors.sql';
@@l10n_br_rtk_errors.sql;

prompt 'Starting File: base_form_menu_elements.sql';
@@form_menu_elements/base_form_menu_elements.sql;

spool off

