set feedback off
set define off

ALTER TABLE FM_TRAN_DATA DISABLE CONSTRAINT FTD_FTC_FK
/

ALTER TABLE FM_GL_CROSS_REF DISABLE CONSTRAINT FGCR_FMTC_FK
/

delete from fm_tran_codes ;

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (100,'Total merchandise cost exclusive of taxes','U',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (101,'Total non recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (102,'Total recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (103,'Total non-merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (104,'Variance merchandise Cost',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (105,'Variance recoverable  taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (106,'Variance non-recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (107,'Calculation Tolerance Cost',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (108,'Calculation Tolerance recoverable tax','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (109,'Calculation Tolerance non- recoverable tax','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (110,'Return NF- total merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (111,'Return NF- total non recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (112,'Return NF- total recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (113,'Return NF- total non-merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (114,'Correction NF- total merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (115,'Correction NF- total non recoverable taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (116,'Correction NF- total recoverable taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (117,'Correction NF- total non-merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (118,'Complementary NF- total merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (119,'Complementary NF- total non recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (120,'Complementary NF total non-merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (121,'Complementary NF non-merchandise taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (122,'RTV NF- total merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (123,'RTV NF- total non recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (124,'RTV NF- total recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (125,'Complementary NF Request- total merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (126,'Complementary NF Request- total non recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (127,'Complementary NF Request- total recoverable taxes','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (128,'Complementary NF Request- total non-merchandise cost exclusive of taxes',null,null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (129,'Non- recoverable Taxes for transfers for source location','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (130,'Recoverable Taxes for transfers for source location','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (131,'Non- recoverable Taxes for transfers for destination location','T',null,null,null,null,sysdate,user,sysdate,user);

Insert into fm_tran_codes (TRAN_CODE,DESCRIPTION,REF_NO_1,REF_NO_2,REF_NO_3,REF_NO_4,REF_NO_5,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values (132,'Recoverable Taxes for transfers for destination location','T',null,null,null,null,sysdate,user,sysdate,user);

commit;

ALTER TABLE FM_TRAN_DATA ENABLE CONSTRAINT FTD_FTC_FK;

ALTER TABLE FM_GL_CROSS_REF ENABLE CONSTRAINT FGCR_FMTC_FK;


set feedback on
set define on
PROMPT Done.

/
