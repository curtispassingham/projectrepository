PROMPT deleting from restart tables

DELETE FROM restart_bookmark where restart_name in ('fmtrandata','fmpurge','fmfinpost','fm_batch_consume_rtv','fmedinf','fm_batch_consume_asnout','fm_batch_consume_po_rcv','fm_batch_consume_tsf_rcv','fm_batch_consume_invadj');
DELETE FROM restart_program_status where restart_name in ('fmtrandata','fmpurge','fmfinpost','fm_batch_consume_rtv','fmedinf','fm_batch_consume_asnout','fm_batch_consume_po_rcv','fm_batch_consume_tsf_rcv','fm_batch_consume_invadj');
DELETE FROM restart_control where program_name in ('fmtrandata','fmpurge','fmfinpost','fm_batch_consume_rtv','fmedinf','fm_batch_consume_asnout','fm_batch_consume_po_rcv','fm_batch_consume_tsf_rcv','fm_batch_consume_invadj');
DELETE FROM restart_program_history where program_name in ('fmtrandata','fmpurge','fmfinpost','fm_batch_consume_rtv','fmedinf','fm_batch_consume_asnout','fm_batch_consume_po_rcv','fm_batch_consume_tsf_rcv','fm_batch_consume_invadj');

PROMPT inserting into restart tables 

INSERT INTO restart_control VALUES ('fmtrandata',null,'ALL_LOCATIONS',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fmtrandata',1,null,'fmtrandata','ready for start',null,null,null,null,null,null,null,null);

INSERT INTO restart_control VALUES ('fmpurge',null,'ALL_LOCATIONS',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fmpurge',1,null,'fmpurge','ready for start',null,null,null,null,null,null,null,null);

INSERT INTO restart_control VALUES ('fmfinpost',null,'NONE',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fmfinpost',1,null,'fmfinpost','ready for start',null,null,null,null,null,null,null,null);

INSERT INTO restart_control VALUES ('fm_batch_consume_rtv',null,'NONE',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fm_batch_consume_rtv',1,null,'fm_batch_consume_rtv','ready for start',null,null,null,null,null,null,null,null);

INSERT INTO restart_control VALUES ('fmedinf',null,'SUPPLIER',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fmedinf',1,null,'fmedinf','ready for start',null,null,null,null,null,null,null,null);

INSERT INTO restart_control VALUES ('fm_batch_consume_asnout',null,'NONE',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fm_batch_consume_asnout',1,null,'fm_batch_consume_asnout','ready for start',null,null,null,null,null,null,null,null);

INSERT INTO restart_control VALUES ('fm_batch_consume_po_rcv',null,'NONE',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fm_batch_consume_po_rcv',1,null,'fm_batch_consume_po_rcv','ready for start',null,null,null,null,null,null,null,null);

INSERT INTO restart_control VALUES ('fm_batch_consume_tsf_rcv',null,'NONE',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fm_batch_consume_tsf_rcv',1,null,'fm_batch_consume_tsf_rcv','ready for start',null,null,null,null,null,null,null,null);

INSERT INTO restart_control VALUES ('fm_batch_consume_invadj',null,'NONE',1,'Y','T',1);
INSERT INTO restart_program_status VALUES ('fm_batch_consume_invadj',1,null,'fm_batch_consume_invadj','ready for start',null,null,null,null,null,null,null,null);

COMMIT;
/
