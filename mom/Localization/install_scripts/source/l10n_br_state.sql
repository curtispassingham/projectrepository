
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
------------------------------------------------------------------------------
--
-- Name: l10n_br_state.sql
--
-- Description: This script is used to insert states into the STATE
-- table for the country Brazil.
--                           
------------------------------------------------------------------------------
DECLARE 

-- Define a record type to store all of the values that need to be passed out to the report

   TYPE state_type_rectype IS RECORD(state          VARCHAR2(2),
                                     description    VARCHAR2(120),
                                     country        VARCHAR2(3));

-- Define a table type based upon the report info record type defined above.  

   TYPE state_type_tabletype IS TABLE OF state_type_rectype
      INDEX BY BINARY_INTEGER;

   state_type_list   state_type_tabletype;

BEGIN

-- Removing all the data from the STATE table for the country 'BR'azil.

   DELETE FROM state
         WHERE country_id = 'BR';

/* Fill the table.  If you need to add a new state, add a new record to the table below*/

   state_type_list(1).state := 'AC';
   state_type_list(1).description := 'Acre';   
   state_type_list(1).country := 'BR';               

   state_type_list(2).state := 'AL';
   state_type_list(2).description := 'Alagoas';
   state_type_list(2).country := 'BR';               

   state_type_list(3).state := 'AM';
   state_type_list(3).description := 'Amazonas'; 
   state_type_list(3).country := 'BR';                                

   state_type_list(4).state := 'AP';
   state_type_list(4).description := 'Amapá';  
   state_type_list(4).country := 'BR';                              

   state_type_list(5).state := 'BA';
   state_type_list(5).description := 'Bahia'; 
   state_type_list(5).country := 'BR';                             

   state_type_list(6).state := 'CE';
   state_type_list(6).description := 'Ceará';                 
   state_type_list(6).country := 'BR';               

   state_type_list(7).state := 'DF';
   state_type_list(7).description := 'Distrito Federal';
   state_type_list(7).country := 'BR';                            

   state_type_list(8).state := 'ES';
   state_type_list(8).description := 'Espírito Santo';
   state_type_list(8).country := 'BR';               

   state_type_list(9).state := 'GO';
   state_type_list(9).description := 'Goiás';
   state_type_list(9).country := 'BR';                               

   state_type_list(10).state := 'MA';
   state_type_list(10).description := 'Maranhão';
   state_type_list(10).country := 'BR';                              

   state_type_list(11).state := 'MG';
   state_type_list(11).description := 'Minas Gerais';
   state_type_list(11).country := 'BR';                             

   state_type_list(12).state := 'MS';
   state_type_list(12).description := 'Mato Grosso do Sul';
   state_type_list(12).country := 'BR';                                

   state_type_list(13).state := 'MT';
   state_type_list(13).description := 'Mato Grosso';
   state_type_list(13).country := 'BR';                              

   state_type_list(14).state := 'PA';
   state_type_list(14).description := 'Pará';
   state_type_list(14).country := 'BR';                              

   state_type_list(15).state := 'PB';
   state_type_list(15).description := 'Paraíba'; 
   state_type_list(15).country := 'BR';                              

   state_type_list(16).state := 'PE';
   state_type_list(16).description := 'Pernambuco';
   state_type_list(16).country := 'BR';                                  

   state_type_list(17).state := 'PI';
   state_type_list(17).description := 'Piauí';
   state_type_list(17).country := 'BR';                                

   state_type_list(18).state := 'PR';
   state_type_list(18).description := 'Paraná';  
   state_type_list(18).country := 'BR';                            

   state_type_list(19).state := 'RJ';
   state_type_list(19).description := 'Rio de Janeiro'; 
   state_type_list(19).country := 'BR';                            

   state_type_list(20).state := 'RN';
   state_type_list(20).description := 'Rio Grande do Norte'; 
   state_type_list(20).country := 'BR';                                

   state_type_list(21).state := 'RO';
   state_type_list(21).description := 'Rondônia'; 
   state_type_list(21).country := 'BR';              

   state_type_list(22).state := 'RR';
   state_type_list(22).description := 'Roraima';
   state_type_list(22).country := 'BR';           

   state_type_list(23).state := 'RS';
   state_type_list(23).description := 'Rio Grande do Sul'; 
   state_type_list(23).country := 'BR';             

   state_type_list(24).state := 'SC';
   state_type_list(24).description := 'Santa Catarina'; 
   state_type_list(24).country := 'BR';            

   state_type_list(25).state := 'SE';
   state_type_list(25).description := 'Sergipe'; 
   state_type_list(25).country := 'BR';          

   state_type_list(26).state := 'SP';
   state_type_list(26).description := 'São Paulo'; 
   state_type_list(26).country := 'BR';             

   state_type_list(27).state := 'TO';
   state_type_list(27).description := 'Tocantins';
   state_type_list(27).country := 'BR';               

   FOR i in 1..state_type_list.COUNT LOOP
         
      insert into state values (state_type_list(i).state,
                                state_type_list(i).description,
                                state_type_list(i).country);
 
   END LOOP;

   commit;
----------------------------------------------------------------------------------------------- 
END;
/
