set feedback off
set define off

PROMPT DELETING from rtk_errors

delete from rtk_errors where rtk_user = 'ORFM' and rtk_lang = 1;


PROMPT INSERTING into rtk_errors

Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_SUPPLIERS','1','Purchase order supplier does not equal fiscal document supplier.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_DATES','1','Receiving date must be between purchase order dates.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ITEM_NOT_EXIST_PO','1','The item %s1 does not exist in purchase order %s2.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ITEM_NOT_AGGREG_PO','1','The item %s1 does not exist as an aggregated item in purchase order %s2.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PACK_NOT_DIST_FRAME','1','The pack %s1 does not exist in pack distribution frame.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_QTY_ITEMS','1','The quantity in fiscal document is greater than quantity ordered in purchase order.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_UNIT_COST_ITEM','1','The unit cost in fiscal document is not equal to unit cost ordered in purchase order.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_QTY_PACK','1','The quantity in packs distribution frame is greater than quantity ordered in purchase order.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_UNIT_COST_PACK','1','The unit cost in pack distribution frame is not equal to unit cost ordered in purchase order.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ITEM_EDI_NO_EXIST_NF','1','All the items in source document must be in the fiscal document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','DUE_DATE_WRONG','1','Due date cannot be before start date or today''s date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_EDI_STATUS','1','Invalid EDI document status.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_BEGIN_END_DATE','1','The due date must be earlier than or equal to the end date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ITEM_NOT_EXIST_EDI','1','Item does not exist in the source document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_QTY_ITEMS_EDI','1','The quantity in fiscal document must be equal to the quantity in the source document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','?ORFM_APPROVE_WITH_ERRORS','1','There are errors on this document. Do you want to force approve this document ?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_COMPL_DOC','1','Complementary fiscal document must have at least one document related.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_PO_PACKS_ADDED','1','No PO and Pack have been added to Pack Distribution list.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_NUMBER','1','Invalid Number entered. Correct mask is %s1','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_LINE_TOLERANCE_CALC','1','The difference between the total value informed of the line and the total value calculated of the line is out of tolerance''s limit. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','SET_OF_BOOKS_NOT_FOUND','1','Set of books not found.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NUM_MAX_DOC_LINE','1','Maximum number of lines of the fiscal document (%s1) has been reached.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_TOLERANCE_CALC','1','The value calculated of document is out of tolerance''s limits. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_LINE_VALUE_INF','1','The total value informed of the line does not correspond with the unit cost * quantity - discount.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TOT_MERC_VAL_INF','1','The total value informed of merchandises of the document does not correspond to the sum of merchandises in the detail of document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOT_SERV_TOLERA_INF','1','The difference between the total value informed of services of the document and the sum of services in the detail of document is out of tolerance''s limit. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TOT_SERV_VAL_INF','1','The total value informed of services of the document does not correspond to the sum of services in the detail of document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_TOLERA_INF','1','The difference between the informed total value of the document and the sum of merchandises and services in the detail of document is out of tolerance''s limits. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TOTAL_VAL_INF','1','The total value informed of the document does not correspond to the sum of merchandises and services in the detail of document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_LINE_VALUE_CALC','1','The total value informed of the line does not correspond with the total value calculated.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOT_MERCH_TOLERA_CAL','1','The difference between the total value informed of merchandises of the document and the total value calculated of merchandises in the detail of document is out of tolerance''s limits. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TOT_MERC_VAL_CAL','1','The total value informed of merchandises of the document does not correspond to the total value calculated of merchandises in the detail of document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOT_SERV_TOLERA_CAL','1','The difference between the total value informed of services of the document and the total value of services calculated in the detail of document is out of tolerance''s limits. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TOT_SERV_VAL_CAL','1','The total value informed of services of the document does not correspond to the total value of services calculated in the detail of document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_TOLERA_CALC','1','The difference between the total value informed of the document and the total value of merchandises and services calculated in the detail of document is out of tolerance''s limits. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TOTAL_VAL_CALC','1','The total value informed of the document does not correspond to the total value of merchandises and services calculated in the detail of document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_LINE_TAX_CALC','1','The tax value informed  does not correspond with the tax value calculated.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOT_TAX_TOLERA_CALC','1','The difference between the total value of tax informed and the total value tax calculated is out of tolerance''s limit. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TOTAL_TAX_CALC','1','The total tax value informed does not correspond with the total tax value calculated.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NOT_EXIST_DETAIL','1','No items exist for the fiscal document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NOT_REQ_TYPE','1','The requisition schedule document does not allow this operation.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_RMA_NOT_EXIST','1','The informed RMA document does not exist','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_CITY','1','Invalid City','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_FISCAL_CLASSIF','1','Invalid Fiscal Classification','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_VARIABLE','1','Invalid variable.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PROG_NOT_EXIST','1','Program does not exist.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DOC_NO_NOT_NULL','1','The Fiscal Document number cannot be null.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_BEGIN_DUE_DATE','1','The start date must be earlier than to the due date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_AFTER_START_DATE','1','The due date must be older than to the start date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_SERIES_NO_NOT_NULL','1','The Fiscal Document Series number cannot be null.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_AUTOMATIC_DOC_NO','1','The Fiscal Document Number will be generated automatically after approved.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NOT_FISCAL_DOCS','1','Fiscal documents for the schedule do not exist.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORG_ID_NOT_FOUND','1','Oraganization unit not found.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FIS_DOC_GER','1','This procedure will generate a new fiscal document number. Do you want to continue?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_MUST_ENT_KEY_VALUE_1','1','The name of Sender/Receiver is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ISSUE_DATE_REQUIRED','1','The Issue Date is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ENT_EXIT_DATE_REQ','1','The Entry/Exit Date is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PARTNER_TYPE_REQ','1','The Transportation Type is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PARTNER_REQ','1','Transportation name is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_SERV_REQ','1','The Total Service is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_MERCH_REQ','1','The Total Merchandise is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_DOC_REQ','1','The Total Value is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','INV_FISCAL_ATTR','1','Fiscal attributes were not found for the given entity.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_3','1','The parameter must have 3 characters.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_MODULE_REQUIRED','1','The Sender/Receiver type is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_REQ_TYPE_REQUIRED','1','The Requisition Type cannot be null.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NOFOP_REQUIRED','1','The Nature of Operation cannot be null.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_MODE_REQUIRED','1','The Mode cannot be null.','ORFM','Y');

Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_DOC_GREAT_ZERO','1','The Total Value must be greater than zero.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_MER_GREAT_ZERO','1','The Total Merchandise must be greater than zero.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_SER_GREAT_ZERO','1','The Total Service must be greater than zero.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PRINT_DOC_SAVE','1','Printing the document will save any changes that have been made. Do you want to continue ?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DUP_TAX','1','Tax %s1 is duplicated.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_REC_IN_EXIST','1','This record already exists or is inactive.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_DOC','1','Invalid Fiscal Document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','FISCAL_TOLERANCE_TYPE','1','A fiscal tolerance type must be entered.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ITEM_REF_NULL','1','Item reference must be null.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ITEM_REF_IN_MAIN_NF','1','Item reference must belong to the main Fiscal Document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','FISCAL_TOLERANCE_VALUE','1','A fiscal tolerance value must be entered.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TAX_HEADER_NOT_MAT','1','Total tax value does not match with base calc.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ITEM_REF_NOT_NULL','1','Item reference cannot be null.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','?ORFM_COPY_FISCAL_DOC','1','Would you like to create a new fiscal document based on selected document ?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ORIGIN_CODE','1','Origin code is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_EXP_DATE','1','You cannot remove records with expired dates.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_SCHED_NOT_EXIST','1','Schedule Number does not exist.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_UNIT_COST_REF','1','The unit cost in fiscal document is not equal to unit cost of fiscal document referenced.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PAYMENT_DATE','1','The payment date must be great or equal then issue date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_REC_EXISTS','1','This PO/Pack combination already exists for this schedule.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_CLASS_FISC_REQ','1','Fiscal Classification is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_CST','1','Cst cannot be changed because the date period is over.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_DOC_REF','1','Invalid fiscal document referenced.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_ORDER_SUPP','1','This order is invalid because the order''s supplier does not match with the fiscal document''s sender/receiver.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_ITEM_REF','1','Invalid item referenced.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_MATCH_SCH_LEVEL','1','This fiscal document belongs to a schedule with packs. In this case the match process must be done at schedule level.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_SCHED_STATUS','1','Invalid Schedule status.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_AFTER_DUE_DATE','1','The end date must be greater than or equal to the due date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_WIN_EXISTS','1','There is another chain of windows open.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_TOLERANCE','1','Total cost exceeds tolerance. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_ITEMS_NF','1','The NF unit cost is different from the PO cost.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','?ORFM_DEL_RECV_NO','1','There are Fiscal Documents associated to this receipt. Do you want to delete?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_APPR_VALID','1','There are unapproved NFs in this schedule.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TAX_EXISTS','1','Tax already exists.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_COMPANY_ID_REQUIRED','1','Please enter a Company ID value.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_LOC_FIS_NO_NOT_EXIST','1','There isn''t a fiscal sequence created for the Location %s1.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DOC_PAYMENT_EXISTS','1','Payment date already exists.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_VAR_NOT_EXIST','1','Variable does not exist.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_SCHED','1','Invalid Schedule number.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ERR_EDI','1','Process Error.Please verify the error log for information.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DECIMAL','1','This field accepts only %s1 numbers and %s2 decimals.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_LI_MODULE_REQUIRED','1','A value to indicates the level that tolerances are going to be applied must be entered.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_KEY_VALUE_1_REQUIRED','1','Please enter a Company, Supplier or Location ID value.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_OUT_TOTAL_TOLERANCE','1','The value calculated of document is out of tolerance''s limits. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','SUPP_ADDR_NOT_FOUND','1','Supplier address not found.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_CALC_BASE','1','Calculated base must be greater than zero or equal to zero.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_PERC_RATE','1','Percentage rate must be greater than zero or equal to zero.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_SUBSERIES_NULL','1','For this type of fiscal document the sub serial number must be NULL.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_QTY_DIST_NF','1','The quantities in packs distribution frame is different than quantity in fiscal documents.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_ISSUE_DATE2','1','Invalid fiscal document issue date. The issue date must be newer than the last generated fiscal document number.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_PAYMENTS','1','Total Payments does not match with total document.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FISCAL_DOC_NOT_EXIST','1','The fiscal document %s1 does not exist.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_ISSUE_DATE','1','Issue date must be less or equal than Entry/Exit date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_UTIL','1','Invalid Fiscal Utilization.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_EX_TYPE_ID','1','Fiscal Document type already exists.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ENTRY_OR_EXIT_DATE',1,'Entry/Exit date must be greater or equal than Issue date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FIS_DOC_TYPE_REQ','1','The Fiscal Document type is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FIS_DOC_TYPE_DSC_REQ','1','The Fiscal Document type description is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TYPE_ID','1','Invalid Fiscal Document type.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_CITY_DEST','1','City destination must be different than the origin city.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_UTIL_EXIST','1','Fiscal Utilization already exists.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_?CHANGE_REFERS','1','One or more items are linked in another table. Deleting this item will change all of the Active statuses to Inactive. Do you want to continue?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_UTIL_REQ','1','Fiscal Utilization is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TRIB_IND','1','Subst. Tax is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TRIB_VALUE','1','Value of the Subst. Tax is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_BASE_IND','1','Base of the change is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_BASE_VALUE','1','Value of the base of the change is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_CST_IND','1','Situation Tax is required.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DOC_DETAIL_NOTFOUND','1','The fiscal document cannot be validated until its details have been informed.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_TAX','1','Tax does not exist.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DATE_BEFORE_GL','1','The Date must be before the GL Date.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_DOC_STATUS','1','Invalid status change.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DEL_RECORD','1','Child items exist for this record. Do you want to delete this record?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DEL_CHILD','1','Child items exist. Cannot delete this record.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_BASE_IND','1','Only a register can be Base.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_LINE_TOLERANCE_INF','1','The difference between the total value informed of the line and the unit cost * quantity - discount is out of tolerance''s limit. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOT_MERCH_TOLERA_INF','1','The difference between the informed total value of merchandises of the document and the sum of merchandises in the detail of document is out of tolerance''s limits. Tolerance Type %s1, Value %s2','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_FIS_DOC_STATUS','1','Invalid fiscal document status.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_PO_STATUS','1','Invalid purchase order status.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_PO_TYPE','1','Purchase order type not equal utilization code.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_DISC_TYPE',1,'Discount type must be P or V.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_CNPJ_CPF','1','Invalid CNPJ/CPF.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_ENTITY_TYPE','1','Invalid entity type.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_ORIGIN_CODE','1','Invalid ORIGIN_CODE.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_PHARMA_LIST_TYPE','1','Invalid PHARMA_LIST_TYPE.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_CLASS_ID','1','Invalid CLASSIFICATION_ID.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_NCM_CHAR_CODE','1','Invalid NCM_CHAR_CODE.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_EX_IPI','1','Invalid EX_IPI.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_PAUTA_CODE','1','Invalid PAUTA_CODE.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_SERVICE_CODE','1','Invalid SERVICE_CODE.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_FEDERAL_SERVICE','1','Invalid FEDERAL_SERVICE.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_STATE_OF_MANU','1','Invalid STATE_OF_MANUFACTURE.','ORFM','Y');

Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','?ORFM_DEL_SCHD_VME','1','One or more NFs in the schedule are in validated, matched or error status. Do you want to delete?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_HEADER_TAX','1','The header level taxes must be entered.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_MAN_HEAD','1','Please provide mandatory information for NF header.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_MAN_SOURCE','1','Please provide mandatory information for NF Source: %s1.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_MAN_DEST','1','Please provide mandatory information for NF destination: %s1.','ORFM','Y');

Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_RESO_RULE_REQUIRED','1','Resolution Rule cannot be null.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_FISCAL_DOC_NO','1','Invalid Fiscal Document number.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_SHIPMENT_NO','1','Invalid Shipment number.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_UTIL_PARAM','1','The Utilization Parameter Configuration is incorrect.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','UTIL_REASON_REC_EXIST','1','The reason code is already attached to other Utilization ID.','ORFM','Y');

Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FISCAL_NO_NOT_EXIST','1','Fiscal No. does not exist.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_RMA_NO_NOT_EXISTS','1','RMA Id does not exist','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_RNF_FAILED','1','The return NF creation failed.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FISCAL_INFO_REQ','1','Please provide mandatory header and detail NF information.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_UTIL_DOC_TYPE','1','Invalid Fiscal Utilization and Document Type combination.','ORFM','Y');
---
insert into rtk_errors(rtk_type, rtk_key, rtk_lang,rtk_text, rtk_user,rtk_approved) values ('BL','?CONFIRM_DIS_RES_ACT','1','Selected Resolution Action will be applied to all the discrepant records.  Are you sure you want to continue?','ORFM','Y');
insert into rtk_errors(rtk_type, rtk_key, rtk_lang,rtk_text, rtk_user,rtk_approved) values ('BL','?CONFIRM_DIS_RES_ALL','1','All the discrepancies will be resolved for the selected NF.  Are you sure you want to continue?','ORFM','Y');
insert into rtk_errors(rtk_type, rtk_key, rtk_lang,rtk_text, rtk_user,rtk_approved) values ('BL','?CONFIRM_DIS_RES','1','Selected Resolution Action will be applied to all the discrepant records.  Are you sure you want to continue?','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','NO_TAX_RULE','1','Tax Resolution rule not defined. Please define Tax Rule.','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','NO_COST_RULE','1','Cost Resolution rule not defined. Please define Tax Rule.','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','NO_QTY_RULE','1','Quantity Resolution rule not defined. Please define Tax Rule.','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','APPOR_NOTEQUAL_INFORMED','1','Total of Apportioned NF value should be equal to Informed NF value.','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','INV_SCHEUDLE_NO','1','Invalid Schedule number.','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','INV_FISCAL_DOC_NO','1','Invalid Fiscal Document number.','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','INV_PO_NO','1','Invalid PO number.','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','PACK_RESOLVE_TYPE','1','For Pack Items all the Resolution type should be same. ','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','PARTIAL_PACK_RESOLVE','1','Partial resolution of a pack item has occurred. Select resolution %s1 from the Resolution Action field to complete the resolution.','ORFM','Y');
insert into rtk_errors(RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','NO_RESOLUTION_BY','1','Please select Resolution Action.','ORFM','Y');

---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PACK_COMP_NOT_MATCH','1','Please select all the component items for pack item %s1.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PKCMP_FACT_NOT_MATCH','1','Component item''s quantity must be factor of pack item %s1.','ORFM','Y');
---
-- rtk_errors for system option form
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_RESO_VALUE','1','Resolution Rule value entered in is not correct. It should be SYS/NF/RECONCILE.','ORFM','Y');
---

--rtk_errors added by sid for triangulation and ST recovery.
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','NO_COMP_DOC','1','The complementary document for this NF with triangulation PO has not been linked.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','NO_TRIANG_MATCH','1','The costs at the header level does not match with the complementary NF.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_FISCAL_DOC_ID','1','Fiscal document id is required for the calculation of taxes.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NF_NOT_FOR_HIST','1','The Nota Fiscal document is not applicable for history update.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','NO_COMP_REF_LINE','1','The reference line for this item detail does not exist in the Complementary NF.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','NO_TRIANG_LINE_MATCH','1','The unit cost or quantity or total cost for this item detail does not match with the Complementary NF.','ORFM','Y');
-- rtk_errors for receiving
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_REC_DOC','1','No submitted fiscal document is available for receipt processing.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ENTER_DETAIL_INFO','1','Please enter all the values in Detail Info.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','PLATE_STATE_INVALID','1','The selected plate state is invalid.  Please select another one.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_CFOP_REQ','1','NF CFOP must be entered.','ORFM','Y');
---
--  rtk_errors for AQUEUE/DEQUEUE
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_OVERAGE_NO_DETAIL','1','Overage receipt contains no item details (receiptoveragedtl).','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_RECEIPT_GT_ZERO','1','Quantities for overage receipts must be greater than zero.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_REC_ERR_PROC',1,'%s1 number of records are erroneous during reprocessing.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_UTIL_SETUP',1,'Invalid Document type (%s1) and Fiscal Utilization (%s2) combination.','ORFM','Y');
---

Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_COMPL_DOC_CNT',1,'There cannot be more than one complementary NF for a Triangulation NF.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_COMPL_DOC_SCHED',1,'All the NFs for Triangulation should be attached to the same schedule.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TRIANG_SCHED_STATUS',1,'Both the NFs attached to the schedule for Triangulation should be in the same status.','ORFM','Y');
---
-- rtk_errors for Complementary NF for Non merchandise cost
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TOTAL_DOC_COST_COMPO',1,'The Total Document Cost is not equal to sum of all the Non merchandise cost and any taxes that are not part of NIC.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_COMPL_MAIN_NF',1,'The complementary Nota Fiscal is not linked any of the main Nota Fiscal.Do you want to continue?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_UTIL_CHK','1','Complementary Triangulation NF indicator and Complemetary Fright NF indicator cannot both be selected.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_COMPL_SCHEDULE','1','Complementary Schedule for Non Merchandise cost should have only one NF attached.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NON_MER_GREAT_ZERO','1','The Total Non Merchandise cost must be greater than zero.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_COMPL_TOLERANCE_CALC','1','The difference between the total document value at header and the sum of all the Non merchandise cost and any taxes at header is out of tolerance limit. Tolerance Type %s1, Value %s2','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_NO_ACCT_EXISTS','1','There is no proper Account setup.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_SUB_UNEXPC_ITEM','1','The NF cannot be submitted for receiving as it contains only unexpected items with no PO information.','ORFM','Y');

insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DIFF_HEAD_DETAIL_TAX',1,'The difference between the header taxes and the total detail taxes is out of tolerance limit. Tolerance Type %s1, Value %s2, Tax Code %s3','ORFM','Y');

insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values('BL','ORFM_TAX_MISMATCH',1,'There is a missing/extra tax code between header and detail taxes entered.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_MAX_DOC_NO_NOT_NULL',1,'The Maximum Fiscal Document number cannot be null.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ENT_PAYMENT_DATE',1,'Payment Date must be entered.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ENT_PAYMENT_VAL',1,'Payment value must be entered.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','MULTI_PACK_COMP_DOC_REF',1,'Components of same pack cannot be associated with different Reference Nota Fiscals. Pack_no: %s1.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_GREATER_EQUL_ZERO',1,'This value must be greater than or equal to zero.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ENTER_TYPE_ID',1,'Enter a Fiscal Document type ID.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ENTER_UTIL_ID',1,'Enter a Utilization ID.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ENTER_UTIL_DESC',1,'Enter a Utilization Description.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ENTER_VARIABLE',1,'Enter a value in the Variable field.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ENTER_DESCRIPTION',1,'Enter a value in the Description field.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ENTER_VARIABLE_TYPE',1,'Select a value in the Type field.','ORFM','Y');
---
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_CFOP',1,'Invalid NF CFOP.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_PAY_DET_REQ',1,'Payment Details are required for PO NF.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_REQ_TYPE',1,'Invalid requisition type.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_TOTAL_COST',1,'Invalid Total Cost.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_REQ_NO',1,'Invalid requisition number.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_TRIANG_PO_COND',1,'For processing NFs related to triangulation PO, both Main and complementary NF should exist in EDI stage tables','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_PO_ITMLOC',1,'Invalid order/location/item combination','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','?ORFM_SCHED_FISCAL_DELINK',1,'Would you like to delink the fiscal document and the schedule ?','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FRTNF_NO_ITMDTL',1,'Complementary freight NF can not have item level details','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FRTNF_NO_TAXDTL',1,'Complementary freight NF can not have item level tax details','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_FRTNF_PRCS_COND',1,'Before processing complementary freight NF, its corresponding Main NF needs to be processed first.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_PACK_IND',1,'Invalid pack indicator has been provided for given record.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_DETAIL_INFO_REQ',1,'NF Detail information is required for PO NF.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_SELECT_ONE_NF',1,'At least one NF needs to be selected.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_UNIT_COST_NOT_ZERO',1,'One or more items have unit cost as zero after applying header and detail discounts. Please remove those item(s) from the list','ORFM','Y');
---
-- rtk_errors code for Support for VPN functionality
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_VPN_NOT_UNIQUE',1,'VPN is not unique.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_INV_ITEM_VPN',1,'Please provide VPN for transaction level item or below transaction level item.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_ITEM_VPN_NULL',1,'Both item and vpn cannot be null.','ORFM','Y');
---
-- rtk_errors code for Location Sequencing functionality
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_MAX_LIMIT_REACHED',1,'Maximum Limit reached for NF number. Please create a new entry for the same combination.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_MIN_LESS_THAN_MAX',1,'The minimum fiscal no must be less than or equal to maximum fiscal no for location %s1 : %s2 and document type : %s3.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','ORFM_LOC_SEQ_EXISTS',1,'Entry for this location : %s1 ,Document Type : %s2 and Series No : %s3 combination already exists. Please try another combination.','ORFM','Y');
Insert into rtk_errors (RTK_TYPE,RTK_KEY,RTK_LANG,RTK_TEXT,RTK_USER,RTK_APPROVED) values ('BL','?CONFIRM_DEL_LOC_DOC',1,'Do you want to delete this record for Location : %s1 ,Document Type : %s2 and Series No : %s3   ?','ORFM','Y');
---
commit;
set feedback on
set define on
PROMPT Done.

/

