whenever sqlerror exit failure

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_FM_JLBR_ENTS_LEGAIS'
CREATE OR REPLACE FORCE VIEW V_FM_JLBR_ENTS_LEGAIS
 (COD_LOCATION
 ,RAZAO_SOCIAL
 ,MODULE
 ,INSCR_ESTADUAL
 ,INSCR_MUNICIPAL
 ,CNPJ
 ,CPF
 ,BAIRRO
 ,ESTADO
 ,CEP
 ,COD_PAIS
 ,COD_MUN
 ,SUFRAMA
 ,FANTASIA
 ,ENDERECO
 ,NUM
 ,COMPLEMENTO
 ,FONE
 ,FAX
 ,EMAIL
 ,CIDADE
 ,SISCOMEX_ID
 ,COD_CNAE
 ,COD_REG_TRIB)
 AS SELECT te.tsf_entity_id COD_LOCATION,
          te.tsf_entity_desc  RAZAO_SOCIAL,
          vfa.module MODULE,
          vfa.ie INSCR_ESTADUAL,
          vfa.im INSCR_MUNICIPAL,
          NVL(vfa.cnpj,vfa.cpf) CNPJ,
          NVL(vfa.cpf,vfa.cnpj) CPF,
          vfa.neighborhood BAIRRO,
          vfa.state ESTADO,
          vfa.postal_code CEP,
          vfa.country_id COD_PAIS,
          vfa.city COD_MUN,
          vfa.suframa SUFRAMA,
          te.secondary_desc FANTASIA,
          vfa.addr_1 ENDERECO,
          vfa.addr_2 NUM,
          vfa.addr_3 COMPLEMENTO,
          NULL FONE,
          NULL FAX,
          NULL EMAIL,
          c.Jurisdiction_desc CIDADE,
          co.country_desc SISCOMEX_ID,
          NULL COD_CNAE,
          decode(vfa.simples_ind,'Y',1,'N',3) COD_REG_TRIB
     FROM v_fiscal_attributes vfa,
          tsf_entity te,
          country_tax_jurisdiction c,
          country co,
          system_options so
    WHERE vfa.module='TSFENT'
      AND vfa.key_value_1=te.tsf_entity_id
      AND c.Jurisdiction_code=vfa.city
      AND c.state = vfa.state
      AND c.country_id = co.country_id
      AND co.country_id=vfa.country_id
      AND co.country_id = 'BR'
      AND NVL(so.intercompany_transfer_basis,'X') = 'T'
UNION ALL
   select fgls.set_of_books_id COD_LOCATION,
          fgls.set_of_books_desc  RAZAO_SOCIAL,
          vfa.module MODULE,
          vfa.ie INSCR_ESTADUAL,
          vfa.im INSCR_MUNICIPAL,
          NVL(vfa.cnpj,vfa.cpf) CNPJ,
          NVL(vfa.cpf,vfa.cnpj) CPF,
          vfa.neighborhood BAIRRO,
          vfa.state ESTADO,
          vfa.postal_code CEP,
          vfa.country_id COD_PAIS,
          vfa.city COD_MUN,
          vfa.suframa SUFRAMA,
          NULL FANTASIA,
          vfa.addr_1 ENDERECO,
          vfa.addr_2 NUM,
          vfa.addr_3 COMPLEMENTO,
          NULL FONE,
          NULL FAX,
          NULL EMAIL,
          c.Jurisdiction_desc CIDADE,
          co.country_desc SISCOMEX_ID,
          NULL  COD_CNAE,
          decode(vfa.simples_ind,'Y',1,'N',3) COD_REG_TRIB
     FROM v_fiscal_attributes vfa,
          fif_gl_setup fgls,
          country_tax_jurisdiction c,
          country co,
          system_options so
    WHERE vfa.module='SOB'
      AND vfa.key_value_1=fgls.set_of_books_id
      AND c.Jurisdiction_code=vfa.city
      AND c.state = vfa.state
      AND c.country_id = co.country_id
      AND co.country_id=vfa.country_id
      AND co.country_id = 'BR'
      AND NVL(so.intercompany_transfer_basis,'X') = 'B'
UNION ALL
   select wh.wh  COD_LOCATION,
          wh.wh_name RAZAO_SOCIAL,
          vfa.module MODULE,
          vfa.ie INSCR_ESTADUAL,
          vfa.im INSCR_MUNICIPAL,
          NVL(vfa.cnpj, vfa.cpf) CNPJ,
          NVL(vfa.cpf, vfa.cnpj) CPF,
          vfa.neighborhood BAIRRO,
          vfa.state ESTADO,
          vfa.postal_code CEP,
          vfa.country_id PAIS,
          vfa.city COD_MUN,
          vfa.suframa SUFRAMA,
          wh.wh_name_secondary FANTASIA,
          vfa.addr_1 ENDERECO,
          vfa.addr_2 NUM,
          vfa.addr_3 COMPLEMENTO,
          NULL FONE,
          NULL FAX,
          wh.email EMAIL,
          c.Jurisdiction_desc CIDADE,
          co.country_desc SISCOMEX_ID,
          ecc.cnae_code  COD_CNAE,
          decode(vfa.simples_ind,'Y',1,'N',3) COD_REG_TRIB
     FROM v_fiscal_attributes vfa,
          wh wh,
          country_tax_jurisdiction c,
          country co,
          l10n_br_entity_cnae_codes ecc
    WHERE vfa.module='LOC'
      AND vfa.key_value_1=wh.wh
      AND c.Jurisdiction_code=vfa.city
      AND c.state = vfa.state
      AND c.country_id = co.country_id
      AND co.country_id=vfa.country_id
      AND co.country_id = 'BR'
      AND wh.PRIMARY_VWH is not null
      AND ecc.key_value_1 = vfa.key_value_1
      AND ecc.module = 'LOC'
      AND ecc.country_id = co.country_id
      AND ecc.primary_ind = 'Y'
UNION ALL
   SELECT s.store COD_LOCATION,
          s.store_name RAZAO_SOCIAL,
          vfa.module MODULE,
          vfa.ie INSCR_ESTADUAL,
          vfa.im INSCR_MUNICIPAL,
          NVL(vfa.cnpj, vfa.cpf) CNPJ,
          NVL(vfa.cpf, vfa.cnpj) CPF,
          vfa.neighborhood BAIRRO,
          vfa.state ESTADO,
          vfa.postal_code CEP,
          vfa.country_id PAIS,
          vfa.city COD_MUN,
          vfa.suframa SUFRAMA,
          s.store_name_secondary FANTASIA,
          vfa.addr_1 ENDERECO,
          vfa.addr_2 NUM,
          vfa.addr_3 COMPLEMENTO,
          s.phone_number FONE,
          s.fax_number FAX,
          s.email EMAIL,
          c.Jurisdiction_desc CIDADE,
          co.country_desc SISCOMEX_ID,
          ecc.cnae_code  COD_CNAE,
          decode(vfa.simples_ind,'Y',1,'N',3) COD_REG_TRIB
     FROM v_fiscal_attributes vfa,
          store s,
          country_tax_jurisdiction c,
          country co,
          l10n_br_entity_cnae_codes ecc
    WHERE vfa.module='LOC'
      AND vfa.key_value_1=s.store
      AND c.Jurisdiction_code=vfa.city
      AND c.state = vfa.state
      AND c.country_id = co.country_id
      AND co.country_id=vfa.country_id
      AND co.country_id = 'BR'
      AND ecc.key_value_1 = vfa.key_value_1
      AND ecc.module = 'LOC'
      AND ecc.country_id = co.country_id
      AND ecc.primary_ind = 'Y'
/