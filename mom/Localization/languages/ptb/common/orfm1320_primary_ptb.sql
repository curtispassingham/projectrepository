REM This file loads translations for orfm 13.2.0.0,
REM to be used only if ptb is the primary language of the system.

spool orfm1320_primary_ptb.log;

--- Startall ---
prompt 'Starting File: code_detail_ptb.sql';
@@code_detail_ptb.sql;
prompt 'Starting File: code_detail_trans_ptb.sql';
@@code_detail_trans_ptb.sql;
prompt 'Starting File: code_head_ptb.sql';
@@code_head_ptb.sql;
prompt 'Starting File: ext_entity_key_descs_ptb.sql';
@@ext_entity_key_descs_ptb.sql;
prompt 'Starting File: form_elements_langs_ptb.sql';
@@form_elements_langs_ptb.sql;
prompt 'Starting File: l10n_attrib_descs_ptb.sql';
@@l10n_attrib_descs_ptb.sql;
prompt 'Starting File: l10n_attrib_group_descs_ptb.sql';
@@l10n_attrib_group_descs_ptb.sql;
prompt 'Starting File: l10n_code_detail_descs_ptb.sql';
@@l10n_code_detail_descs_ptb.sql;
prompt 'Starting File: l10n_menu_descs_ptb.sql';
@@l10n_menu_descs_ptb.sql;
prompt 'Starting File: l10n_rec_group_descs_ptb.sql';
@@l10n_rec_group_descs_ptb.sql;
prompt 'Starting File: menu_elements_langs_ptb.sql';
@@menu_elements_langs_ptb.sql;
prompt 'Starting File: nav_element_mode_ptb.sql';
@@nav_element_mode_ptb.sql;
prompt 'Starting File: nav_folder_ptb.sql';
@@nav_folder_ptb.sql;
prompt 'Starting File: rtk_errors_ptb.sql';
@@rtk_errors_ptb.sql;
prompt 'Starting File: rtk_reports_ptb.sql';
@@rtk_reports_ptb.sql;

spool off;

