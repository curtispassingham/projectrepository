SET DEFINE OFF;
-- Secondary Table hence use DELETE & INSERT statement
-- RFM 13.2.1 DELTA STARTS HERE ----------------
delete from form_elements_langs where fm_name = 'FM_ORFM_DOC_COMPLEMENT' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'COMPL_FISCAL_DOC_ID','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'TI_COMPL_FISCAL_NO','NONE',12,'Complemento'||chr(10)||'N°', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'TI_UTILIZATION','NONE',12,'Uso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'TI_UTILIZATION_DESC','NONE',12,'Descrição do'||chr(10)||'Uso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'TI_CFOP','NONE',12,'CFOP NF', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'TI_TYPE','NONE',12,'Tipo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'TI_STATUS','NONE',12,'Status', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'TI_ISSUE_DATE','NONE',12,'Dt.Emissão', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'TI_ENTRY_EXIT_DATE','NONE',12,'Dt.Entr./'||chr(10)||'Saída', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'FISCAL_DOC_ID','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'MODULE','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'KEY_VALUE_1','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'LAST_UPDATE_DATETIME','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'CREATE_DATETIME','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'LAST_UPDATE_ID','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_COMPLEMENT_DOC', 'CREATE_ID','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_ACTION', 'PB_ADD','NONE',12,'Adic.','A', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_ACTION', 'PB_ITEMS_TO_COMP_NF','NONE',12,'Adicionar Item','I', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_ACTION', 'PB_DELETE','NONE',12,'Deletar','D', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','B_ACTION', 'PB_CLOSE','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','NONE',12,'Lista Docs Fiscais Complemts.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','1',12,'Nota Fiscal Complementar',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','2',12,'Nº de Série',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','3',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','4',12,'Módulo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','5',12,'Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','6',12,'Número',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','7',12,'Dt.Emissão',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'LOV_MULT_FISCAL_DOC_ID','8',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_DOC_COMPLEMENT','NONE', 'W_ORFM_COMPL_DOC', 'NONE',12,'Doc.Fiscal Complementar (doc_complement)', NULL, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_UTILIZATION' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_FILTER', 'TI_UTILIZATION','NONE',12,'Uso', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_FILTER', 'TI_UTILIZATION_DESC','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_FILTER', 'PB_FIND','NONE',12,'Localizar','F', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_FM_FISCAL_UTILIZATION', 'UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_FM_FISCAL_UTILIZATION', 'UTILIZATION_DESC','NONE',12,'Descrição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_FM_FISCAL_UTILIZATION', 'LI_REQUISITION_TYPE','NONE',12,'Tipo Requisição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_FM_FISCAL_UTILIZATION', 'LI_NOP','NONE',12,'Natureza Operação', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_FM_FISCAL_UTILIZATION', 'MODE_TYPE','NONE',12,'Modo', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_ACTION', 'PB_ADD','NONE',12,'Adic.','d', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_ACTION', 'PB_DELETE','NONE',12,'Deletar','l', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','B_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','NONE', 'LOV_B_FILTER_TI_UTILIZATION','NONE',12,'Lista de Usos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','NONE', 'LOV_B_FILTER_TI_UTILIZATION','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','NONE', 'LOV_B_FILTER_TI_UTILIZATION','2',12,'Id',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTILIZATION','NONE', 'W_UTILIZATION', 'NONE',12,'Configuração Uso Fiscal (uso)', NULL, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_UTIL_REASON' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','B_HEAD_UTIL_REASON', 'TI_UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','B_HEAD_UTIL_REASON', 'TI_UTILIZATION_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','B_FM_UTIL_REASON', 'REASON','NONE',12,'Cód.Motivo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','B_FM_UTIL_REASON', 'TI_REASON_DESC','NONE',12,'Descrição Cód.Motivo', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','B_ACTION', 'PB_OK_UTIL_REASON','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','B_ACTION', 'PB_ADD_UTIL_REASON','NONE',12,'Adic.','d', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','B_ACTION', 'PB_DELETE_UTIL_REASON','NONE',12,'Deletar','l', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','B_ACTION', 'PB_CANCEL_UTIL_REASON','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','NONE', 'LOV_B_FM_UTIL_REASON_REASON','NONE',12,'Lista Códigos Motivo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','NONE', 'LOV_B_FM_UTIL_REASON_REASON','1',12,'Motivo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','NONE', 'LOV_B_FM_UTIL_REASON_REASON','2',12,'Reason_Desc',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_REASON','NONE', 'W_UTIL_REASON', 'NONE',12,'Códigos do Motivo de Uso Fiscal (util_reason)', NULL, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_UTIL_DOC_TYPE' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_HEAD_DOC_TYPE', 'TI_UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_HEAD_DOC_TYPE', 'TI_UTILIZATION_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_FM_DOC_TYPE', 'TI_TYPE_ID','NONE',12,'Id Tipo Doc.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_FM_DOC_TYPE', 'TI_TYPE_DESC','NONE',12,'Descrição', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_ACTION', 'PB_OK_DOC','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_ACTION', 'PB_ADD_DOC','NONE',12,'Adic.','d', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_ACTION', 'PB_DELETE_DOC','NONE',12,'Deletar','l', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_ACTION', 'PB_CANCEL_DOC','NONE',12,'Cancelar','C', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_EDITOR', 'TI_EDITOR','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_EDITOR', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','B_EDITOR', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','NONE', 'LOV_B_FM_DOC_TYPE_ID','NONE',12,'Lista Tipos Nota Fiscal',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','NONE', 'LOV_B_FM_DOC_TYPE_ID','1',12,'Id_Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','NONE', 'LOV_B_FM_DOC_TYPE_ID','2',12,'Descr_Tipo',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','NONE', 'W_DOC_TYPE', 'NONE',12,'Tipo de Documento de Uso Fiscal (util_doc_type)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_DOC_TYPE','NONE', 'W_EDITOR', 'NONE',12,'',null, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_UTIL_ATTRIB' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_HEAD_UTIL_ATTRIB', 'TI_UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_HEAD_UTIL_ATTRIB', 'TI_UTILIZATION_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_FM_UTIL_ATTRIB', 'COMP_NF_IND','NONE',12,'NF Complementar para Triangulação', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_FM_UTIL_ATTRIB', 'COMP_FREIGHT_NF_IND','NONE',12,'NF Complementar para Frete', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_FM_UTIL_ATTRIB', 'ICMSST_RECOVERY_IND','NONE',12,'Recup.CMS-ST', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_FM_UTIL_ATTRIB', 'ALLOW_RECEIVING_IND','NONE',12,'Permitir Receb.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_FM_UTIL_ATTRIB', 'AUTO_APPROVE_IND','NONE',12,'Aprovação Autom.NF', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_FM_UTIL_ATTRIB', 'CHOOSE_DOC_IND','NONE',12,'Esclhr NF', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_FM_UTIL_ATTRIB', 'UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_ACTION', 'PB_OK_PARAMETERS','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','B_ACTION', 'PB_CANCEL_PARAMETERS','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_UTIL_ATTRIB','NONE', 'W_PARAMETERS', 'NONE',12,'Atributos de Uso Fiscal (util_attrib)', NULL, 'Y' );

delete from form_elements_langs where fm_name = 'FM_SCHEDULE' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_BOILER_PLATE', 'DI_FILTER','NONE',12,'Filtro', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FILTER', 'LI_MODE','NONE',12,'Modo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FILTER', 'TI_SCHEDULE_NO','NONE',12,'Agndmto', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FILTER', 'LI_STATUS','NONE',12,'Status', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FILTER', 'TI_START_DATE','NONE',12,'Data Inicial', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FILTER', 'TI_END_DATE','NONE',12,'Data Final', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FILTER', 'LI_LOC_TYPE','NONE',12,'Tipo de Local', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FILTER', 'TI_LOCATION','NONE',12,'Localização', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FILTER', 'TI_LOC_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FMI_SCHEDULE', 'SCHEDULE_NO','NONE',12,'Nº', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FMI_SCHEDULE', 'SCHEDULE_DATE','NONE',12,'Data', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FMI_SCHEDULE', 'ACCOUNTING_DATE','NONE',12,'Data GL', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FMI_SCHEDULE', 'STATUS','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FMI_SCHEDULE', 'TI_STATUS','NONE',12,'Status', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_FMI_SCHEDULE', 'LAST_UPDATE_DATETIME','NONE',12,'Últ.Atualz.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_ACTION', 'PB_FISCAL_DOC','NONE',12,'Fiscal','F', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_ACTION', 'PB_OK','NONE',12,'Ok','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_ACTION', 'PB_ADD','NONE',12,'Adic.','A', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_ACTION', 'PB_DELETE','NONE',12,'Deletar','D', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','B_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','NONE', 'LOV_SCHEDULE_NO','NONE',12,'Lista de N° agendmto.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','NONE', 'LOV_SCHEDULE_NO','1',12,'Nº Agendmto',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','NONE', 'W_DATE', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_SCHEDULE','NONE', 'W_SCHEDULE', 'NONE',12,'Programação (programação)', NULL, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_PRINT_FISCAL_DOC' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_BOILER_PLATE', 'TI_FILTER','NONE',12,'Filtro', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_BOILER_PLATE', 'PB_RECV_SORT','NONE',12,'Agndmto','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_BOILER_PLATE', 'PB_FISCAL_SORT','NONE',12,'Nº do Documento','', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_HEAD', 'TI_FISCAL_DOC_NO','NONE',12,'Nº do Documento', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_HEAD', 'TI_TYPE_ID','NONE',12,'Tipo Doc.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_HEAD', 'TI_TYPE_DESC','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_HEAD', 'TI_SCHEDULE_NO','NONE',12,'Agndmto', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_FM_FISCAL_DOC_HEADER', 'SCHEDULE_NO','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_FM_FISCAL_DOC_HEADER', 'FISCAL_DOC_NO','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_FM_FISCAL_DOC_HEADER', 'TI_TYPE','NONE',12,'Tipo Doc.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_ACTION', 'PB_PRINT','NONE',12,'Imprimir','P', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','B_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'LOV_B_HEAD_FISCAL_DOCUMENT','NONE',12,'Lista de Notas Fiscais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'LOV_B_HEAD_FISCAL_DOCUMENT','1',12,'Nº do Documento',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'LOV_B_HEAD_FISCAL_DOCUMENT','2',12,'ID do Documento Fiscal',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'LOV_TYPE_ID','NONE',12,'Lista Tipos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'LOV_TYPE_ID','1',12,'ID Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'LOV_TYPE_ID','2',12,'Descrição Tipo',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'W_EDITOR', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'W_PRINT_DOCUMENT', 'NONE',12,'Imprimir Docs.Fiscais (print_fiscal_doc)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'W_COMMENTS', 'NONE',12,'Reimprimir Comentários (print_fiscal_doc)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_PRINT_FISCAL_DOC','NONE', 'W_DATE', 'NONE',12,'',null, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_NFE_CONFIG_LOC' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_BOILERPLATE', 'DI_NFE_PARAMETERIZATION','NONE',12,'Parametrização Nf-e', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_BOILERPLATE', 'DI_FILTER','NONE',12,'Filtro', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FILTER', 'LI_LOCATION_TYPE','NONE',12,'Tipo de Local', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FILTER', 'TI_LOCATION','NONE',12,'Localização', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FILTER', 'TI_LOCATION_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FILTER', 'TI_UTILIZATION','NONE',12,'Uso', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FILTER', 'TI_UTILIZATION_DESC','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FM_NFE_CONFIG_LOC', 'LI_LOCATION_TYPE','NONE',12,'Tipo de Local', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FM_NFE_CONFIG_LOC', 'LOCATION','NONE',12,'Localização', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FM_NFE_CONFIG_LOC', 'LOCATION_DESC','NONE',12,'Descrição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FM_NFE_CONFIG_LOC', 'UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_FM_NFE_CONFIG_LOC', 'UTILIZATION_DESC','NONE',12,'Descrição', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_ACTION', 'PB_ADD','NONE',12,'Adic.','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_ACTION', 'PB_DELETE','NONE',12,'Deletar','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','B_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_NFE_UTILIZATION','NONE',12,'Lista de Usos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_NFE_UTILIZATION','1',12,'ID do Uso',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_NFE_UTILIZATION','2',12,'Descrição do Uso',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_LOC','NONE',12,'Lista Locais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_LOC','1',12,'Nome do Local',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_LOC','2',12,'Descrição do Local',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_NFE_LOC','NONE',12,'Lista Locais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_NFE_LOC','1',12,'Nome do Local',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_NFE_LOC','2',12,'Descrição do Local',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_UTILIZATION','NONE',12,'Lista de Usos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_UTILIZATION','1',12,'ID do Uso',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'LOV_UTILIZATION','2',12,'Descrição do Uso',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_NFE_CONFIG_LOC','NONE', 'W_NFE_CONFIG_LOC', 'NONE',12,'Configuração NFe (nfe_config_loc)', NULL, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_LOC_LOGIN' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','B_MAIN', 'TI_LABEL','NONE',12,'Tipo de Local', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','B_MAIN', 'TI_LOCATION','NONE',12,'Localização', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','B_MAIN', 'TI_LOC_DESC','NONE',12,'',null, 'Y' );
-- radio buttons
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','B_MAIN', 'RG_LOC','RB_STORE',12,'Loja','', 'Y' );
-- radio buttons
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','B_MAIN', 'RG_LOC','RB_WH',12,'Depósito','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','B_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','B_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','NONE', 'LOV_STORE','NONE',12,'Lista Lojas',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','NONE', 'LOV_STORE','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','NONE', 'LOV_STORE','2',12,'Loja',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','NONE', 'LOV_WH','NONE',12,'Lista de Depósitos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','NONE', 'LOV_WH','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','NONE', 'LOV_WH','2',12,'Depósito',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_LOGIN','NONE', 'W_MAIN', 'NONE',12,'Login do Local (loc_login)', NULL, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_LOC_FISCAL_NUMBER' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FILTER', 'LI_LOCATION_TYPE','NONE',12,'Tipo de Local', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FILTER', 'TI_LOCATION','NONE',12,'Localização', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FILTER', 'TI_LOCATION_DESC','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FM_LOC_FISCAL_NUMBER', 'LI_LOCATION_TYPE','NONE',12,'Tipo de Local', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FM_LOC_FISCAL_NUMBER', 'LOCATION','NONE',12,'Localização', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FM_LOC_FISCAL_NUMBER', 'SERIES_NO','NONE',12,'N° de Série', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FM_LOC_FISCAL_NUMBER', 'SUBSERIES_NO','NONE',12,'N° de Subsérie', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FM_LOC_FISCAL_NUMBER', 'FISCAL_DOC_NO','NONE',12,'Número Fiscal'||chr(10)||'Mínimo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_FM_LOC_FISCAL_NUMBER', 'FISCAL_DOC_NO_MAX','NONE',12,'Número Fiscal'||chr(10)||' Máximo', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_ACTION', 'PB_OK','NONE',12,'OK','K', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_ACTION', 'PB_ADD','NONE',12,'Adic.','d', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_ACTION', 'PB_DELETE','NONE',12,'Deletar','I', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','B_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','NONE', 'LOV_FILTER_LOC','NONE',12,'Lista Locais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','NONE', 'LOV_FILTER_LOC','1',12,'Nome do Local',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','NONE', 'LOV_FILTER_LOC','2',12,'Descrição do Local',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','NONE', 'LOV_LOC','NONE',12,'Lista Locais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','NONE', 'LOV_LOC','1',12,'Nome do Local',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','NONE', 'LOV_LOC','2',12,'Descrição do Local',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_LOC_FISCAL_NUMBER','NONE', 'W_LOC_FISCAL_NUMBER', 'NONE',12,'Números Fiscais do Local (loc_fiscal_number)', NULL, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_ORFM_FISCAL_HEADER' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'FISCAL_DOC_NO','NONE',12,'N° Fiscal', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'LI_REQUISITION_TYPE','NONE',12,'Tipo Requisição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_UTILIZATION_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_NF_CFOP','NONE',12,'CFOP NF', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_ENTRY_CFOP','NONE',12,'CFOP entrd', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'SERIES_NO','NONE',12,'Série', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'NFE_ACCESSKEY','NONE',12,'Chave Acesso NFe', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_SCHEDULE_NO','NONE',12,'NºAgendmto', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_STATUS','NONE',12,'Status', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TYPE_ID','NONE',12,'Tipo Doc.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_TYPE_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'SUBSERIES_NO','NONE',12,'Subserial', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_MODE_TYPE','NONE',12,'Tipo Modo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'LI_MODULE','NONE',12,'Tipo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'LI_KEY_VALUE_2','NONE',12,'Subtipo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'KEY_VALUE_1','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_KEY_VALUE_1_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_ADDR_1','NONE',12,'Endereço', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_POSTAL_CODE','NONE',12,'CEP', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_CITY_DESC','NONE',12,'Cidade', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_DISTRICT','NONE',12,'Bairro', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_STATE','NONE',12,'Estado', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'LI_PARTNER_TYPE','NONE',12,'Tipo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'LI_FREIGHT_TYPE','NONE',12,'Tipo Frete', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'QUANTITY','NONE',12,'Qtd.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'UNIT_TYPE','NONE',12,'UDM', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'PARTNER_ID','NONE',12,'Nome', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_PARTNER_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_PLATE_STATE','NONE',12,'Estado Placa', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_CNPJCPF_PARTNER','NONE',12,'CNPJ/CPF', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'NET_WEIGHT','NONE',12,'Peso Líquido', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TOTAL_WEIGHT','NONE',12,'Peso Total', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_VEHICLE_PLATE','NONE',12,'Placa Veículo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_POSTAL_CODE_PARTNER','NONE',12,'CEP', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_CITY_DESC_PARTNER','NONE',12,'Cidade', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_DISTRICT_PARTNER','NONE',12,'Bairro', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_STATE_PARTNER','NONE',12,'Estado', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TOTAL_SERV_VALUE','NONE',12,'Serviço', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TOTAL_ITEM_VALUE','NONE',12,'Mercadoria', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TOTAL_DOC_VALUE','NONE',12,'Total', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TOTAL_SERV_CALC_VALUE','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TOTAL_ITEM_CALC_VALUE','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TOTAL_DOC_CALC_VALUE','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'FREIGHT_COST','NONE',12,'Frete', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'COST_DISP','NONE',12,'Custo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'QTY_DISP','NONE',12,'Quantidade', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TAX_DISP','NONE',12,'Imposto', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'INSURANCE_COST','NONE',12,'Seguro', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'OTHER_EXPENSES_COST','NONE',12,'Outras Despesas', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'LI_DISCOUNT_TYPE','NONE',12,'Tipo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TOTAL_DISCOUNT_VALUE','NONE',12,'Valor', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'ISSUE_DATE','NONE',12,'Emissão', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'ENTRY_OR_EXIT_DATE','NONE',12,'Entr/Saída', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'EXIT_HOUR','NONE',12,'Hora Saída', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_COMP_FISCAL_DOC_NO','NONE',12,'N° comp', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_CNPJCPF','NONE',12,'CNPJ/CPF', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'PROCESS_ORIGIN','NONE',12,'Origem', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'PROCESS_VALUE','NONE',12,'Valor', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'TI_PLATE_STATE_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_HEADER', 'FISCAL_DOC_ID','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_HEAD', 'VAT_CODE','NONE',12,'Criar Data/Hora', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_HEAD', 'LI_VAT_CODE','NONE',12,'Cód.Imp.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_HEAD', 'TI_CALC_BASE_AUX','NONE',12,'Base Tributvl', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_HEAD', 'TOTAL_VALUE','NONE',12,'Valor Total Imp', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION_TAX', 'PB_ADD','NONE',12,'Adic.','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION_TAX', 'PB_DELETE','NONE',12,'Deletar','', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_PAYMENTS', 'PAYMENT_DATE','NONE',12,'Data', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_PAYMENTS', 'VALUE','NONE',12,'Valor', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION_PAY', 'PB_ADD','NONE',12,'Adic.','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION_PAY', 'PB_DELETE','NONE',12,'Deletar','', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'REQUISITION_NO','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'UNEXPECTED_ITEM','NONE',12,'Item '||chr(10)||'Inesperado', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'ITEM','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'TI_ITEM_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'TI_NF_CFOP','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'TI_ENTRY_CFOP','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'PACK_IND','NONE',12,'Ind.'||chr(10)||'Pack', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'PACK_NO','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'CST_PIS','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'CLASSIFICATION_ID','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'CST_COFINS','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'TI_ORIGIN_CST','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'RECOVERABLE_BASE','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'RECOVERABLE_VALUE','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'TI_STANDARD_UOM','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'QUANTITY','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'UNIT_COST','NONE',12,'0', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'DISCOUNT_TYPE','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'TI_DISCOUNT_VALUE_AUX','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'TOTAL_COST','NONE',12,'0', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'TI_FISCAL_DOC_NO_REF','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'OTHER_EXPENSES_COST','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'INSURANCE_COST','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_DETAIL', 'FREIGHT_COST','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_PACK_COMP', 'SEQ_NO','NONE',12,'Nº Seq.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_PACK_COMP', 'PACK_NO','NONE',12,'Item Pack', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_PACK_COMP', 'ITEM','NONE',12,'Item Compon.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_PACK_COMP', 'TI_ITEM_DESC','NONE',12,'Desc.Item Compon.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_PACK_COMP', 'TI_COMP_TOT_QTY','NONE',12,'Qtd.Total'||chr(10)||' Comp', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_PACK_COMP', 'TI_COMP_TOT_COST','NONE',12,'Cst.Total'||chr(10)||' Comp', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_PACK_COMP', 'PB_COMP_CLOSE','NONE',12,'Fechar','C', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_DETAIL', 'VAT_CODE','NONE',12,'Id Doc.Fiscal', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_DETAIL', 'LI_VAT_CODE','NONE',12,'Tipo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_DETAIL', 'TAX_BASE_IND','NONE',12,'Base Imp', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_DETAIL', 'TAX_BASIS','NONE',12,'Base Imp.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_DETAIL', 'PERCENTAGE_RATE','NONE',12,'Alíq. Imp.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_DETAIL', 'TOTAL_VALUE','NONE',12,'Valor Total', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_FM_FISCAL_DOC_TAX_DETAIL', 'LEGAL_MESSAGE_TEXT','NONE',12,'Msg.Legal', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION_DETAIL_TAX', 'PB_ADD','NONE',12,'Adic.','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION_DETAIL_TAX', 'PB_DELETE','NONE',12,'Deletar','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_REFRESH','NONE',12,'Atualizar','E', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_COMPLEMENT','NONE',12,'Complemento','M', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_REQ_DOC','NONE',12,'Doc.Obrig.','R', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_SHOW_COMPONENTS','NONE',12,'Exib.Compons.','S', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_EXPAND','NONE',12,'Expldr.','E', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_EXPAND_ALL','NONE',12,'Explodir Td','X', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_ADD','NONE',12,'Adic.','A', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_DELETE_DETAIL','NONE',12,'Deletar','D', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_DELETE','NONE',12,'Deletar','D', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_ACTION', 'PB_CLOSE','NONE',12,'Fechar','C', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'PB_DOC','NONE',12,'Doc.','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'PB_ITEM','NONE',12,'Item','', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_ITEM_TAXES','NONE',12,'Impostos', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_LINES','NONE',12,'Linhas', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_COMP','NONE',12,'Compons.Pack', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_CALCULATED_TOTALS','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_ADDITIONAL_COSTS','NONE',12,'Custos Adicionais', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_TAXES','NONE',12,'Impostos', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_SUPPLIER_PAYMENTS','NONE',12,'Pagamentos Fornecedor', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_ADDITIONAL_INFORMATION','NONE',12,'Custos Adicionais', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_COMPLEMENT','NONE',12,'Complemento', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_ADDITIONAL_INFO_IND_PROC','NONE',12,'Processar', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_DISCOUNT','NONE',12,'Descontos', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_KEY_VAL_SUPPLER_SITE','NONE',12,'Local Fornec.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_KEY_VAL_SUPPLIER','NONE',12,'Fornecedor', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_FISCAL_DOC','NONE',12,'Documento Fiscal', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_TRANSPORTATION','NONE',12,'Transporte', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_DATES','NONE',12,'Datas', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_TOTAL','NONE',12,'Totais', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_SENDER_RECEIVER','NONE',12,'Remet/Destinatário', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_ITEM_DESC','NONE',12,'Descrição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_STANDARD_UOM','NONE',12,'Unid.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_QUANTITY','NONE',12,'Quantidade', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_UNIT_COST','NONE',12,'Cst.'||chr(10)||'Unit.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_DISCOUNT_TYPE','NONE',12,'Tipo de'||chr(10)||' desconto', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_DISCOUNT_VALUE','NONE',12,'Desconto', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_TOTAL_COST','NONE',12,'Cst.'||chr(10)||'Total', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_NF_CFOP','NONE',12,'CFOP NF', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_ENTRY_CFOP','NONE',12,'CFOP entrd', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_FREIGHT_COST','NONE',12,'Frete', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_INSURANCE_COST','NONE',12,'Cst.'||chr(10)||'Seguro', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_OTHER_EXPENSES','NONE',12,'Cst.Otrs.'||chr(10)||'Desps', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_REC_BASE','NONE',12,'Base'||chr(10)||' Rec', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_REC_VALUE','NONE',12,'Valor '||chr(10)||'Rec', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_PIS','NONE',12,'PIS', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_COFINS','NONE',12,'COFINS', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_CLASSIFICATION','NONE',12,'Classificação', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_CST','NONE',12,'CST', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_FISCAL_REF','NONE',12,'Ref.Nota Fiscal', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_ITEM_REF','NONE',12,'Ref Item', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_ITEM_DESC_REF','NONE',12,'Descrição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_INFORMED_SUM','NONE',12,'Informd.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_CALCULATED_SUM','NONE',12,'Calculado', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_DISCREPANCY','NONE',12,'Discrep.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','B_BOILERPLATE', 'DI_KEY_VAL_OTHER','NONE',12,'Nome', NULL, 'Y' );
--Items outside of blocks
-- Tab Pages
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'HEADER_INFO','NONE',12,'Info.Cabeç.',NULL, 'Y' );
-- Tab Pages
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'HEADER_TAX','NONE',12,'Imps.Cabeç.',NULL, 'Y' );
-- Tab Pages
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'ADDITIONAL_INFO','NONE',12,'Info. Adicional',NULL, 'Y' );
-- Tab Pages
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'TRANSPORTATION_DETAILS','NONE',12,'Transportation Details',NULL, 'Y' );
-- Tab Pages
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'DETAIL_INFO','NONE',12,'Info.Detlh.',NULL, 'Y' );
-- Tab Pages
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'DETAIL_TAXES','NONE',12,'Impostos nos Detalhes',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CHILD_ITEMS','NONE',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CHILD_ITEMS_2ND','NONE',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_VPN_ITEMS','NONE',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_VPN_ITEMS_2ND','NONE',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TYPE_ID','NONE',12,'Lista Tipos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TYPE_ID','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TYPE_ID','2',12,'Tipo',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_SUPPLIER','NONE',12,'Lista Fornecedores',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_SUPPLIER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_SUPPLIER','2',12,'Fornecedor',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_SUPPLIER','3',12,'CPF/CNPJ',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_PARTNER','NONE',12,'Lista Parceiros',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_PARTNER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_PARTNER','2',12,'Parceiro',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_PARTNER','3',12,'CPF/CNPJ',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_LOC','NONE',12,'Lista Locais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_LOC','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_LOC','2',12,'Parceiro',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_LOC','3',12,'CPF/CNPJ',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_OUTLOC','NONE',12,'Lista Locais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_OUTLOC','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_OUTLOC','2',12,'Parceiro',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_OUTLOC','3',12,'CPF/CNPJ',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CUSTOMER','NONE',12,'Lista Clientes',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CUSTOMER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CUSTOMER','2',12,'Cliente',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CUSTOMER','3',12,'CPF/CNPJ',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMPANY','NONE',12,'Lista Empresas',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMPANY','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMPANY','2',12,'Empresa',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMPANY','3',12,'CPF/CNPJ',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF','NONE',12,'Lista CNPJ/CPF',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF','1',12,'CNPJ/CPF',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF','2',12,'',null, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF','3',12,'Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF','4',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF','5',12,'Descrição',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TRANSPORTATION','NONE',12,'Lista Parceiros',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TRANSPORTATION','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TRANSPORTATION','2',12,'Parceiro',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TRANSPORTATION','3',12,'CPF/CNPJ',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF_PARTNER','NONE',12,'Lista CNPJ/CPF',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF_PARTNER','1',12,'CNPJ/CPF',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF_PARTNER','2',12,'',null, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF_PARTNER','3',12,'Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF_PARTNER','4',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_CNPJCPF_PARTNER','5',12,'Descrição',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO','NONE',12,'Lista de Notas Fiscais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO','1',12,'Nota Fiscal',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO','2',12,'Nº Série',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO','3',12,'Módulo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO','4',12,'Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO','5',12,'Número',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO','6',12,'Dt.Emissão',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO','7',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMP_FISCAL_DOC_NO','NONE',12,'Lista Notas Fiscais Complementares',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMP_FISCAL_DOC_NO','1',12,'Nota Fiscal Complementar',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMP_FISCAL_DOC_NO','2',12,'Nº Série',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMP_FISCAL_DOC_NO','3',12,'Módulo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMP_FISCAL_DOC_NO','4',12,'Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMP_FISCAL_DOC_NO','5',12,'Número',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMP_FISCAL_DOC_NO','6',12,'Dt.Emissão',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_COMP_FISCAL_DOC_NO','7',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ORDER_NO_FILTER','NONE',12,'Lista OCs',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ORDER_NO_FILTER','1',12,'OC',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ORDER_NO','NONE',12,'Lista OCs',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ORDER_NO','1',12,'OC',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TSF_NO_FILTER','NONE',12,'Lista Transf.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TSF_NO_FILTER','1',12,'Transf.',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TSF_NO','NONE',12,'Lista Transf.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_TSF_NO','1',12,'Transf.',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_RTV_NO_FILTER','NONE',12,'Lista RTV''s',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_RTV_NO_FILTER','1',12,'RTV',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_RTV_NO','NONE',12,'Lista RTV''s',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_RTV_NO','1',12,'RTV',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_STOCK_NO_FILTER','NONE',12,'Lista Saídas Estq.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_STOCK_NO_FILTER','1',12,'Saída Estq.',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_STOCK_NO','NONE',12,'Lista Saídas Estq.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_STOCK_NO','1',12,'Saída Estq.',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_RMA_NO_FILTER','NONE',12,'Lista RMAs',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_RMA_NO_FILTER','1',12,'RMA',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_RMA_NO','NONE',12,'Lista RMAs',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_RMA_NO','1',12,'RMA',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ITEM','NONE',12,'Lista Itens',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ITEM','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ITEM','2',12,'Item',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ITEM','3',12,'Quantidade',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ITEM','4',12,'Cst.Unit.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ITEM','5',12,'Ind.Pack',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_ITEM','6',12,'Item Pack',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','NONE',12,'Lista de Notas Fiscais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','1',12,'Nota Fiscal',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','2',12,'Nº Série',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','3',12,'Módulo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','4',12,'Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','5',12,'Número',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','6',12,'Dt.Emissão',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','7',12,'',null, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','8',12,'Quantidade',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','9',12,'Custo_Unitário',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF_RTV','10',12,'Data_Recebimento',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','NONE',12,'Lista de Notas Fiscais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','1',12,'Nota Fiscal',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','2',12,'Nº de Série',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','3',12,'Módulo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','4',12,'Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','5',12,'Número',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','6',12,'Dt.Emissão',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','7',12,'Item',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','8',12,'Quantidade',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','9',12,'Cst.Unit.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','10',12,'',null, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_FISCAL_DOC_NO_REF','11',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_DISCOUNT_TYPE','NONE',12,'Lista Valores',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_DISCOUNT_TYPE','1',12,'Valor',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_DISCOUNT_TYPE','2',12,'Desc.',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_UTILIZATION','NONE',12,'Lista de Usos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_UTILIZATION','1',12,'Uso',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_UTILIZATION','2',12,'Descrição',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_PLATESTATE','NONE',12,'Lista Estados',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_PLATESTATE','1',12,'Estado',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'LOV_PLATESTATE','2',12,'Descrição Estados',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'W_CUSTOM_VIEW', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'W_MULTIVIEW_CHANGE_HEADER', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'W_OG_ITEM_NUMBER_LIST', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'W_OG_ITEM_NUMBER_FIND', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'W_EDITOR', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'W_DATE', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'W_ORFM_FISCAL_HEADER', 'NONE',12,'Lista de Notas Fiscais (fiscal_header)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_ORFM_FISCAL_HEADER','NONE', 'W_SHOW_COMP', 'NONE',12,'Componentes Pack (fiscal_header)', NULL, 'Y' );

--delete from main tables
delete from form_elements_langs where fm_name = 'FM_FISCAL_FIND' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_HEAD', 'LI_ACTION','NONE',12,'Ação', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_FISCAL_DOC_NO','NONE',12,'N°', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_SCHEDULE_NO','NONE',12,'NºAgendmto', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'LI_MODE_TYPE','NONE',12,'Tipo Modo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_TYPE_ID','NONE',12,'Tipo'||chr(10)||'Doc.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_TYPE_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'LI_STATUS','NONE',12,'Status', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_CFOP','NONE',12,'CFOP', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_NFE_ACCESS_KEY','NONE',12,'Chave Acesso NFe', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_UTILIZATION_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'LI_MODULE','NONE',12,'Tipo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'LI_KEY_VALUE_2','NONE',12,'Subtipo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_KEY_VALUE_1','NONE',12,'Nome', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_KEY_VALUE_1_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_CNPJCPF','NONE',12,'CNPJ/CPF', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_ISSUE_DATE','NONE',12,'Emissão', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_ENTRY_OR_EXIT_DATE','NONE',12,'Entr/Saída', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_EXIT_HOUR','NONE',12,'Hora Saída', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'LI_REQ_TYPE','NONE',12,'Tipo '||chr(10)||'Requisição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_REQ_NO','NONE',12,'Doc.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_ITEM','NONE',12,'Item', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_MAIN', 'TI_ITEM_DESC','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_ACTION', 'PB_RMA','NONE',12,'RMA','M', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_ACTION', 'PB_EDI','NONE',12,'EDI','E', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_ACTION', 'PB_ADD','NONE',12,'Adic.','A', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_ACTION', 'PB_FIND','NONE',12,'Localizar','F', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_ACTION', 'PB_REFRESH','NONE',12,'Atualizar','R', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_ACTION', 'PB_CLOSE','NONE',12,'Fechar','C', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'CB_RMA_SELECT','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'CB_EDI_SELECT','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'DI_EDI_FILTER','NONE',12,'Filtro', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'DI_FILTER','NONE',12,'Filtro', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'DI_EDI_DOCUMENTS','NONE',12,'Docs.EDI', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'DI_RMA_DOCUMENTS','NONE',12,'Docs.RMA', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'DI_FISCAL_DOC','NONE',12,'Documento Fiscal', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'DI_DOC_DETAIL','NONE',12,'Detlh.Doc', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'DI_DATES','NONE',12,'Datas', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_BOILERPLATE', 'DI_SENDER_RECEIVER','NONE',12,'Remet/Destinatário', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','FM_EDI_DOC_HEADER', 'EDI_DOC_ID','NONE',12,'Id Doc.Edi', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','FM_EDI_DOC_HEADER', 'LOCATION_ID','NONE',12,'Id Local', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','FM_EDI_DOC_HEADER', 'KEY_VALUE_1','NONE',12,'Valor-Chave 1', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','FM_EDI_DOC_HEADER', 'ISSUE_DATE','NONE',12,'Dt.Emissão', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','FM_EDI_DOC_HEADER', 'ATTRIBUTE1','NONE',12,'Atributo1', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'TI_MODE_TYPE','NONE',12,'Tipo'||chr(10)||' Modo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'SCHEDULE_NO','NONE',12,'Agndmto', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'FISCAL_DOC_NO','NONE',12,'N° '||chr(10)||'Documento', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'TI_UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'TI_UTILIZATION_DESC','NONE',12,'Descrição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'TI_CFOP','NONE',12,'CFOP', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'TI_STATUS_DESC','NONE',12,'Status', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'TYPE_ID','NONE',12,'Tipo '||chr(10)||'Doc.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'TI_TYPE_DESC','NONE',12,'Descrição', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_FISCAL_DOC_HEADER', 'TOTAL_DOC_VALUE','NONE',12,'Total', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_EDI_FILTER', 'TI_FISCAL_DOC_NO','NONE',12,'N° Fiscal', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_EDI_FILTER', 'TI_NFE_ACCESS_KEY','NONE',12,'Chave Acesso NFe', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_EDI_FILTER', 'TI_SUPPLIER','NONE',12,'Fornecedor', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_EDI_FILTER', 'TI_SUPPLIER_DESC','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_RMA_FILTER', 'TI_RMA_ID','NONE',12,'N° RMA', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_RMA_FILTER', 'TI_RMA_DATE','NONE',12,'Data RMA', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_RMA_FILTER', 'TI_CUSTOMER','NONE',12,'Cliente', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_RMA_FILTER', 'TI_CUSTOMER_DESC','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_RMA_NF_ACTION', 'PB_ADD_TO_SCHEDULE','NONE',12,'Adc. agendamto','D', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_RMA_NF_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_RMA_NF_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_EDI_NF_ACTION', 'PB_ADD_TO_SCHEDULE','NONE',12,'Adc. agendamto','D', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_EDI_NF_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_EDI_NF_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_EDI_NF_CREATION', 'FISCAL_DOC_NO','NONE',12,'N° Fiscal', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_EDI_NF_CREATION', 'TI_SUPPLIER','NONE',12,'Fornecedor', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_EDI_NF_CREATION', 'ISSUE_DATE','NONE',12,'Dt.Emissão', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_EDI_NF_CREATION', 'NFE_ACCESS_KEY','NONE',12,'Chave Acesso', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_EDI_NF_CREATION', 'CB_SELECT','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_RMA_NF_CREATION', 'CB_SELECT','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_RMA_NF_CREATION', 'RMA_ID','NONE',12,'N° RMA', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_RMA_NF_CREATION', 'CUST_ID','NONE',12,'Cliente', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_RMA_NF_CREATION', 'CUST_DESC','NONE',12,'Nome Cliente', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','B_FM_RMA_NF_CREATION', 'RMA_DATE','NONE',12,'Data RMA', NULL, 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_MAIN_RMA_NO','NONE',12,'Lista RMAs',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_MAIN_RMA_NO','1',12,'Nº da RMA',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_UTILIZATION','NONE',12,'Lista de Usos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_UTILIZATION','1',12,'Uso',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_UTILIZATION','2',12,'Descrição',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_NFE_ACCESS_KEY','NONE',12,'Lista Chaves Acesso NFe',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_NFE_ACCESS_KEY','1',12,'Chave de Acesso da NFe',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_FISCAL_DOC_NO','NONE',12,'Lista N° Doc.Fiscal',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_FISCAL_DOC_NO','1',12,'N° Doc.Fiscal',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CHILD_ITEMS','NONE',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CHILD_ITEMS_2ND','NONE',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_VPN_ITEMS','NONE',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_VPN_ITEMS_2ND','NONE',12,'',null, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_TYPE_ID','NONE',12,'Lista Tipos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_TYPE_ID','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_TYPE_ID','2',12,'Tipo',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_SUPPLIER','NONE',12,'Lista Fornecedores',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_SUPPLIER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_SUPPLIER','2',12,'Fornecedor',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_EDI_SUPPLIER','NONE',12,'Lista Fornecedores',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_EDI_SUPPLIER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_EDI_SUPPLIER','2',12,'Fornecedor',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_PARTNER','NONE',12,'Lista Parceiros',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_PARTNER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_PARTNER','2',12,'Parceiro',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_LOC','NONE',12,'Lista Locais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_LOC','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_LOC','2',12,'Localização',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_OUTLOC','NONE',12,'Lista Locais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_OUTLOC','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_OUTLOC','2',12,'Localização',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CUSTOMER','NONE',12,'Lista Clientes',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CUSTOMER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CUSTOMER','2',12,'Cliente',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_MAIN_CUSTOMER','NONE',12,'Lista Clientes',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_MAIN_CUSTOMER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_MAIN_CUSTOMER','2',12,'Cliente',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_COMPANY','NONE',12,'Lista Empresas',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_COMPANY','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_COMPANY','2',12,'Empresa',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CNPJCPF','NONE',12,'Lista CNPJ/CPF',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CNPJCPF','1',12,'CNPJ/CPF',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CNPJCPF','2',12,'',null, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CNPJCPF','3',12,'Tipo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CNPJCPF','4',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_CNPJCPF','5',12,'Descrição',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_ORDER_NO','NONE',12,'Lista OCs',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_ORDER_NO','1',12,'OC',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_TSF_NO','NONE',12,'Lista Transf.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_TSF_NO','1',12,'Transf.',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_RTV_NO','NONE',12,'Lista RTV''s',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_RTV_NO','1',12,'RTV',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_STOCK_NO','NONE',12,'Lista Saídas Estq.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_STOCK_NO','1',12,'Saída Estq.',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_DOC_NO','NONE',12,'Lista Docs.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_DOC_NO','1',12,'N° Doc',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_RMA_NO','NONE',12,'Lista RMAs',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_RMA_NO','1',12,'Id da RMA',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_ITEM','NONE',12,'Lista Itens',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_ITEM','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_ITEM','2',12,'Item',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_SCHEDULE_NO','NONE',12,'Lista de N° agendmto.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_SCHEDULE_NO','1',12,'Nº Agendmto',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_SCHEDULE_NO','2',12,'Tipo Modo',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'LOV_SCHEDULE_NO','3',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'W_EDI_NF_CREATION', 'NONE',12,'Criação NF EDI (fiscal_find)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'W_OG_ITEM_NUMBER_LIST', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'W_OG_ITEM_NUMBER_FIND', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'W_EDITOR', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'W_DATE', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'W_RMA_NF_CREATION', 'NONE',12,'Criação NF RMA (fiscal_find)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'W_MULTISEARCH', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_FIND','NONE', 'W_FISCAL_FIND', 'NONE',12,'Localz.Docs.Fiscais (fiscal_find)', NULL, 'Y' );


--delete from main tables
delete from form_elements_langs where fm_name = 'FM_FISCAL_DOC_TYPE' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_HEAD', 'TI_LABEL','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_HEAD', 'TI_TYPE_ID','NONE',12,'Tipo Doc.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_HEAD', 'TI_TYPE_ID_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_FM_FISCAL_DOC_TYPE', 'TYPE_ID','NONE',12,'Id Tipo '||chr(10)||'Doc.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_FM_FISCAL_DOC_TYPE', 'TYPE_DESC','NONE',12,'Descrição', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_FM_FISCAL_DOC_TYPE', 'SUBSERIES_NO_IND','NONE',12,'Sub'||chr(10)||'serial', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_HEAD_DOC_TYPE', 'TI_TYPE_ID','NONE',12,'Tipo Nota Fiscal', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_HEAD_DOC_TYPE', 'TI_TYPE_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_FM_DOC_TYPE_UTIL', 'UTILIZATION_ID','NONE',12,'Uso', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_FM_DOC_TYPE_UTIL', 'TI_UTILIZATION_DESC','NONE',12,'Descrição', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_UTILIZATION','NONE',12,'Uso','U', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_ADD','NONE',12,'Adic.','d', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_DELETE','NONE',12,'Deletar','l', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_FIND','NONE',12,'Localizar','F', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_OK_UTIL','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_ADD_UTIL','NONE',12,'Adic.','d', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_DELETE_UTIL','NONE',12,'Deletar','l', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','B_ACTION', 'PB_CANCEL_UTIL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','NONE', 'LOV_B_HEAD_TYPE_ID','NONE',12,'Lista de Tipos de Documento Fiscal',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','NONE', 'LOV_B_HEAD_TYPE_ID','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','NONE', 'LOV_B_HEAD_TYPE_ID','2',12,'Tipo Nota',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','NONE', 'LOV_B_FM_DOC_TYPE_UTIL','NONE',12,'Lista de Uso',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','NONE', 'LOV_B_FM_DOC_TYPE_UTIL','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','NONE', 'LOV_B_FM_DOC_TYPE_UTIL','2',12,'Uso',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','NONE', 'W_DOC_TYPE', 'NONE',12,'Tipo de Documento Fiscal (fiscal_doc_type)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCAL_DOC_TYPE','NONE', 'W_EDITOR', 'NONE',12,'',null, 'Y' );


--delete from main tables
delete from form_elements_langs where fm_name = 'FM_DRM_HEADER' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'LI_LOCATION_TYPE','NONE',12,'Tipo de Local', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_LOCATION','NONE',12,'Localização', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_LOCATION_NAME','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_SUPPLIER','NONE',12,'Local Fornec.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_SUPPLIER_NAME','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_PO_NUMBER','NONE',12,'N° OC', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_SCHEDULE_NO','NONE',12,'NºAgendmto', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_FISCAL_DOC_NO','NONE',12,'Nº do Documento', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'LI_STATUS','NONE',12,'Status Doc.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_FROM_RECEIPT_DATE','NONE',12,'Da Dt.Recebmto', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_TO_RECEIPT_DATE','NONE',12,'Até Dt.Recebmt', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_FROM_ISSUE_DATE','NONE',12,'Da Dt.Emissão', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'TI_TO_ISSUE_DATE','NONE',12,'Até Dt.Emis.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'LI_DISCREPANCY_STATUS','NONE',12,'Status Discrep.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'CB_COST_DISC','NONE',12,'Discrepância Cst', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'CB_QTY_DISC','NONE',12,'Discrepância de Qtd.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'CB_TAX_DISC','NONE',12,'Discrep.Imp.', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'PB_SEARCH','NONE',12,'Pesquisar','h', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_HEAD', 'PB_CLEAR','NONE',12,'Atualizar','e', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'SUPPLIER','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'SUP_NAME','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'SCHEDULE_NO','NONE',12,'NºAgendmto', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'FISCAL_DOC_NO','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'PO_NUMBER','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'ISSUE_DATE','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'ENTRY_OR_EXIT_DATE','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'LOCATION_TYPE_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'LOCATION_ID','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'LOCATION_NAME','NONE',12,'',null, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'NF_STATUS_DESC','NONE',12,'',null, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'COST_DISC','NONE',12,'Custo', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'QTY_DISC','NONE',12,'Qtd.', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_FM_DIS_RESOLUTION', 'CB_TAX','NONE',12,'Imposto', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_SUPPLIER_ID','NONE',12,'Local do Fornecedor','S', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_SUPPLIER_NAME','NONE',12,'Nome/Local do Fornecedor','U', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_NF_ISSUE_DATE','NONE',12,'Dt.Emissão','N', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_RECEIPT_DATE','NONE',12,'Dt.Recebmto','R', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_SCHEDULE_NO','NONE',12,'NºAgendmto','D', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_FISCAL_DOC_NO','NONE',12,'N° Doc','F', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_PO_NUMBER','NONE',12,'Nº OC.','', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_LOCATION_TYPE','NONE',12,'Tipo','E', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_LOCATION_ID','NONE',12,'Localização','L', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_LOCATION_NAME','NONE',12,'Nome do Loc.','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_SORT_NF_STATUS','NONE',12,'Status','u', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_RESOLVE_ALL','NONE',12,'Resolv.Td.','A', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_RESOLVE_COST','NONE',12,'Resolv.Cst.','L', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_RESOLVE_QTY','NONE',12,'Resolv.Qtd.','Q', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_RESOLVE_TAX','NONE',12,'Resolv.Imp.','T', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'PB_CLOSE','NONE',12,'Fechar','s', 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','B_ACTION', 'DI_TEXT','NONE',12,'Discrepâncias', NULL, 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_ALL','NONE',12,'Lista Lojas/Depósitos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_ALL','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_ALL','2',12,'Loja/Depósito',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_PO_NO','NONE',12,'N° OC',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_PO_NO','1',12,'N° OC',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_STORE','NONE',12,'Lista Lojas',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_STORE','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_STORE','2',12,'Loja',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_WH','NONE',12,'Lista de Depósitos',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_WH','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_WH','2',12,'Depósito',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_SUPPLIER','NONE',12,'Lista Fornecedores',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_SUPPLIER','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_SUPPLIER','2',12,'Fornecedor',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_SUP_SITE','NONE',12,'Lista Fornecedores',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_SUP_SITE','1',12,'Nome',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_SUP_SITE','2',12,'Fornecedor',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_SCHEDULE_NO','NONE',12,'Lista de N° agendmto.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_SCHEDULE_NO','1',12,'Nº Agendmto',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_FISCAL_DOC_NO','NONE',12,'Lista Número Documento Fiscal',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'LOV_B_HEAD_FISCAL_DOC_NO','1',12,'Documento Fiscal',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'W_MULTISEARCH', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'W_CUSTOM_VIEW', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'W_MULTIVIEW_CHANGE_HEADER', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'W_DIS_RESOLUTION', 'NONE',12,'Resolução Discrepância (drm_header)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'W_DATE', 'NONE',12,'',null, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'W_DESC_ERROR_LOG', 'NONE',12,'Detalhes Log Erro (orfm_error_log)', NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_DRM_HEADER','NONE', 'W_EDITOR', 'NONE',12,'',null, 'Y' );

--- 13.2.1 DELTA ENDS HERE ----------

COMMIT;
