set define off;
--Retain any customizations of form elements that may have arisen from client customizations of source code--
delete from form_elements_langs_temp where fm_name = 'FM_FISCALCLASS' and lang = 12;
insert into form_elements_langs_temp (select fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind from form_elements_langs where base_ind = 'N' and fm_name = 'FM_FISCALCLASS');
--delete from main tables
delete from form_elements_langs where fm_name = 'FM_FISCALCLASS' and lang = 12;
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_FILTER', 'TI_COUNTRY_ID','NONE',12,'País', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_FILTER', 'TI_FISCAL_CLASSIFICATION_ID','NONE',12,'Fiscal '||chr(10)||'Classification', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_FILTER', 'LI_FISCAL_CLASSIFICATION_TYPE','NONE',12,'Tipo', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_FISCAL_CLASSIFICATION', 'COUNTRY_ID','NONE',12,'País', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_FISCAL_CLASSIFICATION', 'FISCAL_CLASSIFICATION_ID','NONE',12,'Fiscal '||chr(10)||'Classification', NULL, 'Y' );
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_FISCAL_CLASSIFICATION', 'FISCAL_CLASSIFICATION_DESC','NONE',12,'Descrição', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
-- text items, check boxes, etc
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_FISCAL_CLASSIFICATION', 'FISCAL_CLASSIFICATION_TYPE','NONE',12,'Tipo', NULL, 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_ACTION', 'PB_OK','NONE',12,'OK','O', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_ACTION', 'PB_ADD','NONE',12,'Adic.','d', 'Y' );
---Push Buttons are seperated because they use access keys...
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','B_ACTION', 'PB_CANCEL','NONE',12,'Cancelar','C', 'Y' );
--Items outside of blocks
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','NONE', 'LOV_CLASSIFICATION','NONE',12,'Lista Classificações Fiscais',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','NONE', 'LOV_CLASSIFICATION','1',12,'Desc.Nom.',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','NONE', 'LOV_CLASSIFICATION','2',12,'Ncm_Code',NULL, 'Y' );
-- lov
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','NONE', 'LOV_COUNTRY','NONE',12,'Lista de Países',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','NONE', 'LOV_COUNTRY','1',12,'Descrição',NULL, 'Y' );
-- lov col mapping
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','NONE', 'LOV_COUNTRY','2',12,'ID País',NULL, 'Y' );
--Window
insert into form_elements_langs( fm_name, block_name, item_name, sub_item_name, lang, lang_label_prompt, lang_access_key, base_ind) VALUES (  'FM_FISCALCLASS','NONE', 'W_FISCAL_CLASS', 'NONE',12,'Classificação Fiscal               (fiscalclass)', NULL, 'Y' );
delete from form_elements_langs where lang_label_prompt is null and fm_name = 'FM_FISCALCLASS';
set define on;
--Commit
Commit;
/
