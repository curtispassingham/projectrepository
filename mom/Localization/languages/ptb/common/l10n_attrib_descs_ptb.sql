SET DEFINE OFF;
DELETE  FROM L10N_ATTRIB_DESCS WHERE LANG = 12;
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (1,'Y',12,'País Fiscal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (2,'Y',12,'Código Fiscal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (3,'Y',12,'Item de Serviço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (4,'Y',12,'Origem de Mercadoria');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (5,'Y',12,'NCM');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (6,'Y',12,'Característica de NCM');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (7,'Y',12,'EX IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (8,'Y',12,'Pauta');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (9,'Y',12,'Código de Serviço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (10,'Y',12,'Serviço Federal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (31,'Y',12,'Tipo de Contribuinte');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (32,'Y',12,'Endereço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (35,'Y',12,'Bairro');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (36,'Y',12,'Cidade');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (37,'Y',12,'Estado');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (38,'Y',12,'País');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (39,'Y',12,'CEP');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (40,'Y',12,'CPF');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (41,'Y',12,'CNPJ');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (43,'Y',12,'NIT');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (44,'Y',12,'SUFRAMA');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (45,'Y',12,'Inscrição Municipal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (46,'Y',12,'Inscrição Estadual');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (47,'Y',12,'Contribuinte de ISS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (48,'Y',12,'Produtor Rural');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (49,'Y',12,'Contribuinte de IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (50,'Y',12,'Tipo de Operação Correspondente');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (51,'Y',12,'Recuperação de Controle de ST');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (52,'Y',12,'Tipo de Contribuinte');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (53,'Y',12,'Endereço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (56,'Y',12,'Bairro');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (57,'Y',12,'Cidade');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (58,'Y',12,'Estado');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (59,'Y',12,'País');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (60,'Y',12,'CEP');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (61,'Y',12,'CPF');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (62,'Y',12,'CNPJ');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (64,'Y',12,'NIT');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (65,'Y',12,'SUFRAMA');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (66,'Y',12,'Inscrição Municipal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (67,'Y',12,'Inscrição Estadual');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (68,'Y',12,'Contribuinte de ISS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (69,'Y',12,'Produtor Rural');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (70,'Y',12,'Contribuinte de IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (71,'Y',12,'Tipo de Operação Correspondente');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (72,'Y',12,'Recuperação de Controle de ST');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (73,'Y',12,'Tipo de Contribuinte');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (74,'Y',12,'Endereço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (77,'Y',12,'Bairro');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (78,'Y',12,'Cidade');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (79,'Y',12,'Estado');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (80,'Y',12,'País');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (81,'Y',12,'CEP');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (82,'Y',12,'CPF');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (83,'Y',12,'CNPJ');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (85,'Y',12,'NIT');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (86,'Y',12,'SUFRAMA');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (87,'Y',12,'Inscrição Municipal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (88,'Y',12,'Inscrição Estadual');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (89,'Y',12,'Contribuinte de IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (90,'Y',12,'Tipo de Contribuinte');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (91,'Y',12,'Endereço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (94,'Y',12,'Bairro');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (95,'Y',12,'Cidade');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (96,'Y',12,'Estado');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (97,'Y',12,'País');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (98,'Y',12,'CEP');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (99,'Y',12,'CPF');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (100,'Y',12,'CNPJ');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (102,'Y',12,'NIT');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (103,'Y',12,'SUFRAMA');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (104,'Y',12,'Inscrição Municipal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (105,'Y',12,'Inscrição Estadual');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (106,'Y',12,'Contribuinte de ISS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (107,'Y',12,'Contribuinte de SIMPLES');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (108,'Y',12,'Contribuinte de ST');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (109,'Y',12,'Produtor Rural');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (110,'Y',12,'Contribuinte de IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (111,'Y',12,'Tipo de Contribuinte');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (112,'Y',12,'Endereço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (115,'Y',12,'Bairro');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (116,'Y',12,'Cidade');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (117,'Y',12,'Estado');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (118,'Y',12,'País');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (119,'Y',12,'CEP');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (120,'Y',12,'CPF');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (121,'Y',12,'CNPJ');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (123,'Y',12,'NIT');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (124,'Y',12,'SUFRAMA');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (125,'Y',12,'Inscrição Municipal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (126,'Y',12,'Inscrição Estadual');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (127,'Y',12,'Contribuinte de IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (128,'Y',12,'Código de Utilização');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (129,'Y',12,'Código de Utilização');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (130,'Y',12,'Código de Utilização');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (131,'Y',12,'Tipo de Contribuinte');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (132,'Y',12,'Endereço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (135,'Y',12,'Bairro');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (136,'Y',12,'Cidade');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (137,'Y',12,'Estado');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (138,'Y',12,'País');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (139,'Y',12,'CEP');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (140,'Y',12,'CPF');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (141,'Y',12,'CNPJ');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (143,'Y',12,'NIT');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (144,'Y',12,'SUFRAMA');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (145,'Y',12,'Inscrição Municipal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (146,'Y',12,'Inscrição Estadual');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (147,'Y',12,'Contribuinte de ISS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (148,'Y',12,'Produtor Rural');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (149,'Y',12,'Contribuinte de IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (150,'Y',12,'Tipo de Operação Correspondente');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (151,'Y',12,'Recuperação de Controle de ST');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (158,'Y',12,'Contribuinte de PIS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (159,'Y',12,'Contribuinte de PIS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (160,'Y',12,'Contribuinte de PIS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (161,'Y',12,'Contribuinte de PIS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (162,'Y',12,'Contribuinte de PIS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (163,'Y',12,'Contribuinte de COFINS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (164,'Y',12,'Contribuinte de COFINS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (165,'Y',12,'Contribuinte de COFINS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (166,'Y',12,'Contribuinte de COFINS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (167,'Y',12,'Contribuinte de COFINS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (152,'Y',12,'Contribuinte de ICMS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (153,'Y',12,'Contribuinte de ICMS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (154,'Y',12,'Contribuinte de ICMS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (155,'Y',12,'Contribuinte de ICMS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (156,'Y',12,'Contribuinte de ICMS');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (14,'Y',12,'Tipo de Contribuinte');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (15,'Y',12,'Endereço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (18,'Y',12,'Bairro');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (19,'Y',12,'Cidade');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (20,'Y',12,'Estado');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (21,'Y',12,'País');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (22,'Y',12,'CEP');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (23,'Y',12,'CPF');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (24,'Y',12,'CNPJ');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (26,'Y',12,'NIT');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (27,'Y',12,'SUFRAMA');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (28,'Y',12,'Inscrição Municipal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (29,'Y',12,'Inscrição Estadual');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (30,'Y',12,'Contribuinte de IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (168,'Y',12,'Tipo de Contribuinte');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (169,'Y',12,'Endereço');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (172,'Y',12,'Bairro');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (173,'Y',12,'Cidade');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (174,'Y',12,'Estado');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (175,'Y',12,'País');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (176,'Y',12,'CEP');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (177,'Y',12,'CPF');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (178,'Y',12,'CNPJ');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (179,'Y',12,'NIT');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (180,'Y',12,'SUFRAMA');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (181,'Y',12,'Inscrição Municipal');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (182,'Y',12,'Inscrição Estadual');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (183,'Y',12,'Contribuinte de IPI');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (184,'Y',12,'Estado de Fabricação');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (185,'Y',12,'Tipo de Lista Farmacêutica');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (186,'Y',12,'É elegível na faixa de renda');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (187,'Y',12,'É distribuidor de um fabricante');
INSERT INTO L10N_ATTRIB_DESCS (ATTRIB_ID,DEFAULT_LANG_IND,lang,DESCRIPTION ) VALUES (188,'Y',12,'Alíquota Simples de ICMS');
COMMIT;
