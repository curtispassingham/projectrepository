SET DEFINE OFF;
UPDATE RTK_REPORTS SET MODULE= 'ORFM', REPORT_DESC= 'Relat.receb.serão usados por usr.WMS/SIM' WHERE report_name='receiving';
UPDATE RTK_REPORTS SET MODULE= 'ORFM', REPORT_DESC= 'Doc.Nota Fiscal usado em todos os tipos de transações no Brasil' WHERE report_name='fm_fiscal_document';
UPDATE RTK_REPORTS SET MODULE= 'ORFM', REPORT_DESC= 'Relat.carta correção – caso qtd/CST/imposto esteja errados na NF emitida' WHERE report_name='correctionletter';
UPDATE RTK_REPORTS SET MODULE= 'ORFM', REPORT_DESC= 'NF complementar p/impostos: impostos sistema é maior que imposto informado' WHERE report_name='complementary_nf_tax';
UPDATE RTK_REPORTS SET MODULE= 'ORFM', REPORT_DESC= 'Relat.NF da devol.autom.' WHERE report_name='autortnf';
UPDATE RTK_REPORTS SET MODULE= 'ORFM', REPORT_DESC= 'Produto sem relatório NF – itens que não suportam documento NF encontrado no recebimento' WHERE report_name='prodwonf';
COMMIT;
