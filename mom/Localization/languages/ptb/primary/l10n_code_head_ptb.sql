SET DEFINE OFF;
UPDATE L10N_CODE_HEAD SET CODE_TYPE_DESC= 'Cód.Origem Mercadoria' WHERE L10N_CODE_TYPE='ORME' AND COUNTRY_ID='BR';
UPDATE L10N_CODE_HEAD SET CODE_TYPE_DESC= 'Tipo de Entidade' WHERE L10N_CODE_TYPE='ENTY' AND COUNTRY_ID='BR';
UPDATE L10N_CODE_HEAD SET CODE_TYPE_DESC= 'Tipo de Operação Correspondente' WHERE L10N_CODE_TYPE='MOPT' AND COUNTRY_ID='BR';
UPDATE L10N_CODE_HEAD SET CODE_TYPE_DESC= 'Tipo de Contribuinte' WHERE L10N_CODE_TYPE='TXCP' AND COUNTRY_ID='BR';
UPDATE L10N_CODE_HEAD SET CODE_TYPE_DESC= 'Tipo de Lista Farmacêutica' WHERE L10N_CODE_TYPE='PHRM' AND COUNTRY_ID='BR';
UPDATE L10N_CODE_HEAD SET CODE_TYPE_DESC= 'Regime Fiscal Diferenciado' WHERE L10N_CODE_TYPE='DITR' AND COUNTRY_ID='BR';
COMMIT;
