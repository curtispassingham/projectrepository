CREATE OR REPLACE PACKAGE FM_DISCREP_IDENTIFICATION_SQL is
----------------------------------------------------------------------------------
-- Function Name: PROCESS_DISCREP_IDEN
-- Purpose:       This function calls the function for identifying the quantity
--                , cost and tax discrepancies and then call the function for
--                apportioning the line level NF taxes if not mentioned
----------------------------------------------------------------------------------
FUNCTION PROCESS_DISCREP_IDEN(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,O_status         IN OUT INTEGER
                              ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                             )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_FOR_COMPL_TRIANGULATION
-- Purpose:       This function checks if the given NF needs to processed for Traingulation
----------------------------------------------------------------------------------
FUNCTION CHECK_FOR_COMPL_TRIANGULATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                ,O_triangulation       OUT ORDHEAD.TRIANGULATION_IND%TYPE
                                ,O_compl_fiscal_doc_id OUT FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE
                                 )
   return BOOLEAN;
   
   ----------------------------------------------------------------------------------
-- Function Name: CHECK_FOR_TRIANGULATION
-- Purpose:       This function checks if the given NF needs to processed for Traingulation
----------------------------------------------------------------------------------
FUNCTION CHECK_FOR_TRIANGULATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                ,I_fiscal_doc_id        IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                ,O_triangulation       OUT ORDHEAD.TRIANGULATION_IND%TYPE
                                ,O_compl_fiscal_doc_id OUT FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE
                                 )
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_DISREP_FOR_TRIANGULATION
-- Purpose:       This function updates the fm_fiscal_doc_detail table for discrepancy status
--                for triangulation.
----------------------------------------------------------------------------------
FUNCTION UPDATE_DISREP_FOR_TRIANG(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                 ,I_discrepancy          IN VARCHAR2
                                 ,I_compl_fiscal_doc_id  IN FM_FISCAL_DOC_COMPLEMENT.COMPL_FISCAL_DOC_ID%TYPE
                                 ,I_po_number            IN FM_FISCAL_DOC_DETAIL.REQUISITION_NO%TYPE
                                 ,I_item                 IN FM_FISCAL_DOC_DETAIL.ITEM%TYPE
                                        )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_QTY_DISCREP
-- Purpose:       This function identifies all the Quantity discrepany
--                and inserts quantity discrepany in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_QTY_DISCREP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                          )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_QTY_DISCREP_SI
-- Purpose:       This function identifies all the Quantity discrepancy for Special Items
--                and inserts quantity discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_QTY_DISCREP_SI(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                             ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                             )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_COST_DISCREP
-- Purpose:       This function identifies all the Cost discrepany
--                and inserts cost discrepany in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_COST_DISCREP(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                           ,I_fiscal_doc_id   IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                           )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_COST_DISCREP_SI
-- Purpose:       This function identifies all the Cost discrepancy for Special Items
--                and inserts Cost discrepancy in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_COST_DISCREP_SI(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_fiscal_doc_id   IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                               )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_ITEM_LEVEL_TAX_EXISTS
-- Purpose:       This function identifies if there are item level taxes available in fm_fiscal_doc_tax_detail
--                for fm_fiscal_doc_detail.
----------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LEVEL_TAX_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                    ,O_count          IN OUT NUMBER
                                    ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE
                                     )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_MISMATCH_TAX_DIS_HEAD
-- Purpose:       This function identifies all the tax codes which is extra in the NF tables
--                or missing from the NF tables and inserts Tax discrepany in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION GET_MISMATCH_TAX_DIS_HEAD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                  ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE )
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_NF_TAX_HEAD_DIS
-- Purpose:       This function identifies all the tax discrepent in
--                fm_fiscal_doc_tax_head and fm_fiscal_doc_tax_head_ext based on the matching indicator in fm_tax_codes
----------------------------------------------------------------------------------
FUNCTION GET_NF_TAX_HEAD_DIS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                            ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_NF_TAX_HEAD_DIS_SI
-- Purpose:       This function identifies all the tax discrepant in
--                fm_fiscal_doc_tax_head and fm_fiscal_doc_tax_head_ext based on the matching indicator in fm_tax_codes for unexpected items.
----------------------------------------------------------------------------------
FUNCTION GET_NF_TAX_HEAD_DIS_SI(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                               ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: GET_MISMATCH_TAX_DIS_DETAIL
-- Purpose:       This function identifies all the tax codes which is extra in the NF Detail tables
--                or missing from the NF Detail tables and inserts Tax discrepany in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION GET_MISMATCH_TAX_DIS_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                    ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_NF_TAX_DETAIL_DIS
-- Purpose:       This function identifies all the tax discrepent in
--                fm_fiscal_doc_tax_detail and fm_fiscal_doc_tax_detail_ext based on the matching indicator in fm_tax_codes
----------------------------------------------------------------------------------
FUNCTION GET_NF_TAX_DETAIL_DIS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                              ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: CHECK_TAX_DISCREP
-- Purpose:       This function identifies all the Tax discrepany
--                and inserts Tax discrepany in FM_RESOLUTION table.
----------------------------------------------------------------------------------
FUNCTION CHECK_TAX_DISCREP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_TOLERANCE
-- Purpose:       This function identifies if the nf value is withing the tolerance limit.
----------------------------------------------------------------------------------
FUNCTION GET_TOLERANCE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                      ,O_discripent     OUT VARCHAR2
                      ,I_module          IN FM_FISCAL_DOC_HEADER.MODULE%TYPE
                      ,I_key_value_1     IN FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE
                      ,I_key_value_2     IN FM_FISCAL_DOC_HEADER.KEY_VALUE_2%TYPE
                      ,I_tolerance_level IN FM_TOLERANCES.TOLERANCE_LEVEL%TYPE
                      ,I_nf_value        IN FM_FISCAL_DOC_HEADER.QUANTITY%TYPE
                      ,I_system_value    IN FM_FISCAL_DOC_HEADER.QUANTITY%TYPE)
   return BOOLEAN;
--
----------------------------------------------------------------------------------
-- Function Name: GET_TAX_TOLERANCE
-- Purpose:       This function identifies if the tax value is within the tolerance limit.
----------------------------------------------------------------------------------
FUNCTION GET_TAX_TOLERANCE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                          ,O_discripent      OUT VARCHAR2
                          ,I_total_value     IN FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE
                          ,I_tax_value       IN FM_FISCAL_DOC_TAX_DETAIL_EXT.TAX_VALUE%TYPE
                           )
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_DISCREP_STATUS_MATCHED
-- Purpose:       This function updates all the discrepany status (Qty/Cost/Tax) to Matched
--                if there are no discrepancy for a given NF .
----------------------------------------------------------------------------------
FUNCTION UPDATE_DISCREP_STATUS_MATCHED(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE
                                      ,I_fiscal_doc_id  IN FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CALC_LINE_TAXES
-- Purpose:       This function apportions the NF header level taxes to NF line level
--                taxes if NF line level taxes are not mentioned
----------------------------------------------------------------------------------
FUNCTION CALC_LINE_TAXES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_status        IN OUT INTEGER,
                         O_other_proc    IN OUT BOOLEAN,
                         I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UPD_LEGAL_MSG_REC_VAL
-- Purpose:       This function updates the legal message and the recoverable value
--                after fetching them from mastersaf
----------------------------------------------------------------------------------
FUNCTION UPD_LEGAL_MSG_REC_VAL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_status        IN OUT INTEGER,
                               O_other_proc    IN OUT BOOLEAN,
                               I_fiscal_doc_id IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
   return BOOLEAN;
--
END FM_DISCREP_IDENTIFICATION_SQL;
/