
CREATE OR REPLACE PACKAGE FM_EDI_NF_CREATION_SQL is
--------------------------------------------------------------------------------
FUNCTION SET_WHERE_CLAUSE(O_error_message   IN OUT VARCHAR2,
                          O_query           IN OUT VARCHAR2,
                          I_supplier        IN FM_EDI_DOC_HEADER.KEY_VALUE_1%TYPE,
                          I_fiscal_doc_no  IN FM_EDI_DOC_HEADER.FISCAL_DOC_NO%TYPE,
                          I_nfe_access_key  IN FM_EDI_DOC_HEADER.NFE_ACCESS_KEY%TYPE,
                          I_location_type  IN FM_EDI_DOC_HEADER.LOCATION_TYPE%TYPE,
                          I_location_id    IN FM_EDI_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN;

-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOV_FISCAL_DOC_NO(O_error_message  IN OUT VARCHAR2,
                           O_query          IN OUT VARCHAR2,
                           I_location_type  IN     FM_EDI_DOC_HEADER.LOCATION_TYPE%TYPE,
                           I_location_id    IN     FM_EDI_DOC_HEADER.LOCATION_ID%TYPE)
   return BOOLEAN;

-------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOV_NFE_ACCESS_KEY(O_error_message  IN OUT VARCHAR2,
                            O_query          IN OUT VARCHAR2,
                            I_location_type  IN     FM_EDI_DOC_HEADER.LOCATION_TYPE%TYPE,
                            I_location_id    IN     FM_EDI_DOC_HEADER.LOCATION_ID%TYPE)
return BOOLEAN;

-------------------------------------------------------------------------------------------------------------------------------------------------------
 FUNCTION EXISTS_NFE_ACCESS_KEY(O_error_message  IN OUT VARCHAR2,
                                O_exists         IN OUT BOOLEAN,
                                I_nfe_access_key IN     FM_EDI_DOC_HEADER.NFE_ACCESS_KEY%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------------------------------------------------------
end FM_EDI_NF_CREATION_SQL;
/
 
