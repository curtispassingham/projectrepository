CREATE OR REPLACE PACKAGE FM_WASTE_ADJ_SQL IS
----------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_seq_no          IN OUT   FM_STG_ADJUST_DESC.SEQ_NO%TYPE,
                 I_message         IN OUT   FM_STG_ADJUST_DTL%ROWTYPE,
                 I_location        IN       FM_STG_ADJUST_DESC.DC_DEST_ID%TYPE,
                 I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
END FM_WASTE_ADJ_SQL;
/
