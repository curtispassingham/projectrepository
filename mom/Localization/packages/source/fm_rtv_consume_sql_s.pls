CREATE OR REPLACE PACKAGE FM_RTV_CONSUME_SQL AS
---------------------------------------------------------------------------------------------
-- Function Name: CONSUME
-- Purpose      : Consume Message the FM_STG_RTV_DESC
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind       IN OUT   BOOLEAN,
                 I_seq_no          IN       FM_STG_RTV_DESC.SEQ_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: BATCH_CONSUME_THREAD
-- Purpose      : Consume thread of RTV message.
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_no       IN       NUMBER,
                              I_num_threads     IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END FM_RTV_CONSUME_SQL;
/