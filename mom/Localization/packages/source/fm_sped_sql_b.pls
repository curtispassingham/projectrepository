create or replace PACKAGE BODY FM_SPED_SQL IS
-------------------------------------------------------------------------------------------------
FUNCTION FM_LEGAL_MESSAGE( I_in IN fm_legal_msg_type)
 RETURN VARCHAR2 AS
   L_str VARCHAR2(4000);
   L_nbr NUMBER;
 BEGIN
   --
     FOR L_nbr IN 1..I_in.COUNT LOOP
         L_str := L_str
                  || CASE WHEN L_str IS NOT NULL
                          THEN ',' END
                  || I_in( L_nbr );
     END LOOP;
   --
return L_str;
END FM_LEGAL_MESSAGE;
-------------------------------------------------------------------------------------------------

FUNCTION FM_SPED_HEADER(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_last_update_datetime IN     VARCHAR2,
                        I_sped_last_run_date   IN     VARCHAR2)
return BOOLEAN AS

   CURSOR C_SPED_HEADER IS
 	SELECT ffdh.fiscal_doc_id INVOICE_ID,
               DECODE(fs.mode_type,'ENT',ffdh.key_value_1,'EXIT',ffdh.location_id) ISSUER,
               DECODE(fs.mode_type,'ENT',ffdh.MODULE,'EXIT','LOC') ISSUER_TYPE,
               fs.mode_type MOVTO_E_S,
               DECODE(ffdh.requisition_type, 'RMA','D','N') NORM_DEV,
               TO_CHAR(ffdh.type_id) COD_DOCTO,
               ffdh.fiscal_doc_no NUM_DOCFIS,
               ffdh.series_no SERIE_DOCFIS,
               ffdh.issue_date DATA_EMISSAO,
               ffdh.total_item_value VLR_PRODUTO,
               ffdh.total_doc_value VLR_TOT_NOTA,
               ffdh.freight_cost VLR_FRETE,
               ffdh.insurance_cost VLR_SEGURO,
               FFDH.other_expenses_cost VLR_OUTRAS,
               (SELECT SUM(ffdd.quantity * nvl(ffdd.unit_header_disc,0)) FROM fm_fiscal_doc_detail ffdd
               where ffdd.fiscal_doc_id=ffdh.fiscal_doc_id) VLR_DESCONTO,
               ffdh.freight_type GERA_FRETE,
               fs.accounting_date DATA_GL,
               ffdh.requisition_type TIPO_DOC,
               ffdh.entry_or_exit_date DT_RECEBIMENTO,
               ffdh.key_value_1 COD_PART,
               ffdh.quantity QTD_VOL,
               ffdh.total_weight PESO_BRT,
               ffdh.net_weight PESO_LIQ,
               ffdh.subseries_no SUB_SERIES,
               ffdh.total_serv_value VL_SERV,
               DECODE(ffdh.freight_type,'CIF',1,DECODE(ffdh.freight_type,'FOB',2,9)) IND_FRT,
               ffdh.plate_state UF_ID,
               ffdh.vehicle_plate PLACA_VEICULO,
               ffdh.nfe_accesskey CHV_NFE,
               ffdh.process_origin IND_PROC,
               ffdh.process_value NUM_PROC,
               NULL COD_INF,
               NULL CHV_CTE,
               NULL TP_CTE,
               NULL CHV_CTE_REF,
               ffdh.issue_date,
               ffdh.utilization_id,
               ffdh.key_value_1,
               DECODE(fs.mode_type,'ENT',ffdh.location_id,'EXIT',ffdh.key_value_1) ADDRESSEE,
               DECODE(fs.mode_type,'ENT','LOC','EXIT',ffdh.MODULE) ADDRESSEE_TYPE,
               ffdh.module,
               fs.status,
               ffdh.nf_cfop cod_cfo,
               ffdh.unit_type COD_MEDIDA,
               ffdh.exit_hour,
               ffdh.create_datetime DT_INI,
               ffdh.last_update_datetime DT_FIM
          FROM fm_fiscal_doc_header ffdh,
               fm_schedule fs
         WHERE ffdh.schedule_no = fs.schedule_no
           AND fs.status in ('FP')
           AND ffdh.status not in ('I')
           AND ffdh.last_update_datetime >to_date(I_sped_last_run_date,'DD-MON-RR-HH24:MI:SS')
           AND ffdh.last_update_datetime <=to_date(I_last_update_datetime,'DD-MON-RR-HH24:MI:SS') ;

   CURSOR C_TAX_VALUE(P_invoice_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) IS
        SELECT vat_code,
               total_value,
               nvl(modified_tax_basis,tax_basis) tax_basis
          FROM fm_fiscal_doc_tax_head
         WHERE fiscal_doc_id = P_invoice_id;

   CURSOR C_cod_natureza_op(P_utilization_id FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE) IS
        SELECT ffu.nop
          FROM fm_fiscal_utilization ffu
         WHERE ffu.utilization_id = P_utilization_id;

   CURSOR C_perc_reducao_icms(P_invoice_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) IS
        SELECT 100-(nvl(modified_tax_basis,tax_basis))
          FROM fm_fiscal_doc_tax_head
         WHERE fiscal_doc_id = P_invoice_id
           AND vat_code      = 'ICMSST'
           AND ((tax_basis < 100)
           OR  (modified_tax_basis < 100));

   CURSOR C_entity_id(P_key_value_1 FM_FISCAL_DOC_HEADER.KEY_VALUE_1%TYPE,P_module FM_FISCAL_DOC_HEADER.MODULE%TYPE) IS
        SELECT NVL(vfa.cnpj,vfa.cpf)
          FROM v_fiscal_attributes vfa
         WHERE vfa.key_value_1 = P_key_value_1 and vfa.module=P_module;

   CURSOR C_ind_pgto(P_invoice_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                     P_issue_date FM_FISCAL_DOC_HEADER.ISSUE_DATE%TYPE) IS
       WITH count_payment_date AS
  		(SELECT COUNT(ffdp.payment_date) cnt_payment_date
  			FROM fm_fiscal_doc_payments ffdp
  		 WHERE NVL(ffdp.fiscal_doc_id,P_invoice_id)=P_invoice_id
		 )
		SELECT DECODE(cnt_payment_date,0,9,1,(SELECT
    									CASE
									     WHEN P_issue_date = payment_date
									     THEN 0
     								           WHEN payment_date > P_issue_date
									     THEN 1
									     WHEN payment_date IS NULL
									     THEN 9
									END
									  FROM fm_fiscal_doc_payments
								  WHERE fiscal_doc_id = P_invoice_id),1)
		FROM count_payment_date;

   CURSOR C_txt(P_invoice_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) IS
        SELECT fm_legal_message(
                  CAST(
                   MULTISET(SELECT distinct legal_message_text
                              FROM fm_fiscal_doc_tax_detail fdtd,
                                   fm_fiscal_doc_detail ffdd
                             WHERE ffdd.fiscal_doc_id = P_invoice_id
                               AND fdtd.fiscal_doc_line_id = ffdd.fiscal_doc_line_id
                           ) AS FM_LEGAL_MSG_TYPE))
          FROM dual;

   CURSOR C_num_pedido(P_invoice_id FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE) IS
        SELECT DISTINCT ffdd.requisition_no
          FROM fm_fiscal_doc_detail ffdd
         WHERE ffdd.fiscal_doc_id = P_invoice_id;

   L_cod_natureza_op     fm_fiscal_utilization.nop%TYPE;
   L_vlr_total_icms_h      fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_ipi_h       fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_ir_h        fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_iss_h      fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_icms_st_h   fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_inss_h      fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_pis_h       fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_pis_st_h    fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_cofins_h    fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_total_cofins_st_h fm_fiscal_doc_tax_head.total_value%TYPE;
   L_vlr_base_icms_h       fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_ipi_h        fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_ir_h         fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_iss_h        fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_icms_st_h    fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_inss_h       fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_pis_h        fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_pis_st_h     fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_cofins_h     fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vlr_base_cofins_st_h  fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_perc_reducao_icms_h   fm_fiscal_doc_tax_head.tax_basis%TYPE;
   L_vl_nt               fm_fiscal_doc_tax_head.total_value%TYPE;
   L_entity_id           v_br_sups_reg_num.cnpj%TYPE;
   L_vendor_site_id      v_br_sups_reg_num.supplier%TYPE;
   L_num_pedido	         fm_fiscal_doc_detail.requisition_no%TYPE;
   L_txt                 VARCHAR2(4000);
   L_ind_pgto            NUMBER;
   L_program             VARCHAR2(4000) := 'FM_SPED_HEADER';

   BEGIN

     IF I_last_update_datetime IS NULL OR I_sped_last_run_date IS NULL THEN

            O_error_message := SQL_LIB.CREATE_MSG('Required Input Parameter is NULL',
                                                  I_last_update_datetime||' '||I_sped_last_run_date,
                                                  L_program,
                                                  to_char(SQLCODE));

            return FALSE;

     END IF;
     --
       SQL_LIB.SET_MARK('OPEN', 'C_SPED_HEADER', 'FM_FSCL_DOC_HDR', NULL);
       FOR L_sped_header IN c_sped_header LOOP


         SQL_LIB.SET_MARK('OPEN', 'C_COD_NATUREZA_OP', 'FM_FSCL_UTILIZATION', NULL);
         OPEN  C_cod_natureza_op(L_sped_header.utilization_id);
               SQL_LIB.SET_MARK('FETCH', 'C_COD_NATUREZA_OP', 'FM_FISCAL_UTILIZATION',NULL);
               FETCH C_cod_natureza_op INTO L_cod_natureza_op;
         SQL_LIB.SET_MARK('CLOSE', 'C_COD_NATUREZA_OP', 'FM_FISCAL_UTILIZATION',NULL);
         CLOSE C_cod_natureza_op;
       --
         SQL_LIB.SET_MARK('OPEN', 'C_TAX_VALUE', 'FM_FSCL_DOC_TX_HEAD', NULL);


         FOR L_tax_value IN C_TAX_VALUE(L_sped_header.invoice_id) LOOP
           --

             IF L_tax_value.vat_code = 'ICMS' THEN
                L_vlr_total_icms_h := L_tax_value.total_value;
                L_vlr_base_icms_h  := L_tax_value.tax_basis;
             ELSIF L_tax_value.vat_code = 'IPI' THEN
                   L_vlr_total_ipi_h := L_tax_value.total_value;
                   L_vlr_base_ipi_h  := L_tax_value.tax_basis;
             ELSIF L_tax_value.vat_code = 'IR' THEN
                  L_vlr_total_ir_h := L_tax_value.total_value;
                  L_vlr_base_ir_h  := L_tax_value.tax_basis;
             ELSIF L_tax_value.vat_code = 'ISS' THEN
                   L_vlr_total_iss_h := L_tax_value.total_value;
                   L_vlr_base_iss_h  := L_tax_value.tax_basis;
             ELSIF L_tax_value.vat_code = 'INSS' THEN
                   L_vlr_total_inss_h := L_tax_value.total_value;
                   L_vlr_base_inss_h  := L_tax_value.tax_basis;
             ELSIF L_tax_value.vat_code = 'ICMSST' THEN
                   L_vlr_total_icms_st_h := L_tax_value.total_value;
                   L_vlr_base_icms_st_h  := L_tax_value.tax_basis;
             ELSIF L_tax_value.vat_code = 'PIS' THEN
                   L_vlr_total_pis_h := L_tax_value.total_value;
                   L_vlr_base_pis_h  := L_tax_value.tax_basis;
             ELSIF L_tax_value.vat_code = 'PISST' THEN
                   L_vlr_total_pis_st_h := L_tax_value.total_value;
                   L_vlr_base_pis_st_h  := L_tax_value.tax_basis;
             ELSIF L_tax_value.vat_code = 'COFINS' THEN
                   L_vlr_total_cofins_h := L_tax_value.total_value;
                   L_vlr_base_cofins_h  := L_tax_value.tax_basis;
            ELSIF L_tax_value.vat_code = 'COFINSST' THEN
                   L_vlr_total_cofins_st_h := L_tax_value.total_value;
                   L_vlr_base_cofins_st_h  := L_tax_value.tax_basis;

             END IF;

           --
         SQL_LIB.SET_MARK('END LP', 'C_TAX_VALUE', 'FM_FSCL_DOC_TX_HEAD', NULL);



         END LOOP;

       --
         SQL_LIB.SET_MARK('OPEN', 'C_PERC_REDUCAO_ICMS', 'FM_FSCL_DOC_TX_HEAD', NULL);
         OPEN C_perc_reducao_icms(L_sped_header.invoice_id);
              SQL_LIB.SET_MARK('FETCH', 'C_PERC_REDUCAO_ICMS', 'FM_FSCL_DOC_TX_HEAD', NULL);
              FETCH C_perc_reducao_icms INTO L_perc_reducao_icms_h;
         SQL_LIB.SET_MARK('CLOSE', 'C_PERC_REDUCAO_ICMS', 'FM_FSCL_DOC_TX_HEAD', NULL);
         CLOSE C_perc_reducao_icms;
       --
         SQL_LIB.SET_MARK('OPEN', 'C_ENTITY_ID', 'V_BR_SUPS_REG_NUM', NULL);
         OPEN C_entity_id(L_sped_header.key_value_1,L_sped_header.module);
              SQL_LIB.SET_MARK('FETCH', 'C_ENTITY_ID', 'V_BR_SUPS_REG_NUM', NULL);
              FETCH C_entity_id INTO L_entity_id;
         SQL_LIB.SET_MARK('CLOSE', 'C_ENTITY_ID', 'V_BR_SUPS_REG_NUM', NULL);
         CLOSE C_entity_id;
       --
         L_vl_nt := nvl(L_sped_header.vlr_tot_nota,0) - nvl(L_vlr_total_icms_h,0) ;
       --
         SQL_LIB.SET_MARK('OPEN', 'C_IND_PGTO', 'FM_FSCL_DOC_PYMENT', NULL);
         OPEN C_ind_pgto(L_sped_header.invoice_id,
                         L_sped_header.issue_date);
              SQL_LIB.SET_MARK('FETCH', 'C_IND_PGTO', 'FM_FSCL_DOC_PYMENT', NULL);
              FETCH C_ind_pgto INTO L_ind_pgto;
         SQL_LIB.SET_MARK('CLOSE', 'C_IND_PGTO', 'FM_FSCL_DOC_PYMENT', NULL);
         CLOSE C_ind_pgto;
       --
         SQL_LIB.SET_MARK('OPEN', 'C_TXT', 'FM_LEGAL_MESSAGE', NULL);
         OPEN C_txt(L_sped_header.invoice_id);
              SQL_LIB.SET_MARK('FETCH', 'C_TXT', 'FM_LEGAL_MESSAGE', NULL);
              FETCH C_txt INTO L_txt;
         SQL_LIB.SET_MARK('CLOSE', 'C_TXT', 'FM_LEGAL_MESSAGE', NULL);
         CLOSE C_txt;
       --
          SQL_LIB.SET_MARK('OPEN', 'C_NUM_PEDIDO', 'FM_FSCL_DOC_DTL', NULL);
          OPEN C_num_pedido(L_sped_header.invoice_id);
               SQL_LIB.SET_MARK('FETCH', 'C_NUM_PEDIDO', 'FM_FSCL_DOC_DTL', NULL);
               FETCH C_num_pedido INTO L_num_pedido;
          SQL_LIB.SET_MARK('CLOSE', 'C_NUM_PEDIDO', 'FM_FSCL_DOC_DTL', NULL);
          CLOSE C_num_pedido;
       --


      INSERT INTO fm_sped_fiscal_doc_header(INVOICE_ID,
                                               --LOCATION_ID,
                                               ISSUER,
                                               ISSUER_TYPE,
                                               MOVTO_E_S,
                                               NORM_DEV,
                                               COD_DOCTO,
                                               NUM_DOCFIS,
                                               SERIE_DOCFIS,
                                               DATA_EMISSAO,
                                               COD_NATUREZA_OP,
                                               VLR_PRODUTO,
                                               VLR_TOT_NOTA,
                                               VLR_FRETE,
                                               VLR_SEGURO,
                                               VLR_OUTRAS,
                                               VLR_TOTAL_ICMS,
                                               VLR_TOTAL_IPI,
                                               VLR_TOTAL_IR,
                                               VLR_TOTAL_ISS,
                                               VLR_TOTAL_ICMS_ST,
                                               VLR_BASE_ICMS,
                                               VLR_BASE_IR,
                                               VLR_BASE_ISS,
                                               VLR_BASE_ICMS_ST,
                                               VLR_DESCONTO,
                                               VLR_BASE_INSS,
                                               VLR_TOTAL_INSS,
                                               PERC_REDUCAO_ICMS,
                                               GERA_FRETE,
                                               ENTITY_ID,
                                               --VENDOR_SITE_ID,
                                               ADDRESSEE,
                                               ADDRESSEE_TYPE,
                                               DATA_GL,
                                               TIPO_PEDIDO,
                                               DT_RECEBIMENTO,
                                               COD_PART,
                                               QTD_VOL,
                                               PESO_BRT,
                                               PESO_LIQ,
                                               SUB_SERIES,
                                               VL_SERV,
                                               VL_NT,
                                               IND_FRT,
                                               IND_PGTO,
                                               UF_ID,
                                               PLACA_VEICULO,
                                               CHV_NFE,
                                               IND_PROC,
                                               NUM_PROC,
                                               COD_INF,
                                               TXT,
                                               CHV_CTE,
                                               TP_CTE,
                                               CHV_CTE_REF,
                                               VLR_BASE_PIS,
                                               VLR_TOTAL_PIS,
                                               VLR_BASE_COFINS,
                                               VLR_TOTAL_COFINS,
                                               VLR_TOTAL_PIS_ST,
                                               VLR_TOTAL_COFINS_ST,
                                               STATUS_NF,
                                               COD_CFOP,
                                               COD_MEDIDA,
                                               NUM_PEDIDO,
                                               HORA_ENT_SAI,
                                               DT_INI,
                                               DT_FIM)
      	VALUES (L_sped_header.invoice_id,
                L_sped_header.issuer,
                L_sped_header.issuer_type,
                L_sped_header.movto_e_s,
                L_sped_header.norm_dev,
                L_sped_header.cod_docto,
                L_sped_header.num_docfis,
                L_sped_header.serie_docfis,
                L_sped_header.data_emissao,
                L_cod_natureza_op,
                L_sped_header.vlr_produto,
                L_sped_header.vlr_tot_nota,
                L_sped_header.vlr_frete,
                L_sped_header.vlr_seguro,
                L_sped_header.vlr_outras,
                L_vlr_total_icms_h,
                L_vlr_total_ipi_h,
                L_vlr_total_ir_h,
                L_vlr_total_iss_h,
                L_vlr_total_icms_st_h,
                L_vlr_base_icms_h,
                L_vlr_base_ir_h,
                L_vlr_base_iss_h,
                L_vlr_base_icms_st_h,
                L_sped_header.vlr_desconto,
                L_vlr_base_inss_h,
                L_vlr_total_inss_h,
                L_perc_reducao_icms_h,
                L_sped_header.gera_frete,
                L_entity_id,
                L_sped_header.addressee,
                L_sped_header.addressee_type,
                L_sped_header.data_gl,
                L_sped_header.tipo_doc,
                L_sped_header.dt_recebimento,
                L_sped_header.cod_part,
                L_sped_header.qtd_vol,
                L_sped_header.peso_brt,
                L_sped_header.peso_liq,
                L_sped_header.sub_series,
                L_sped_header.vl_serv,
                L_vl_nt,
                L_sped_header.ind_frt,
                L_ind_pgto,
                L_sped_header.uf_id,
                L_sped_header.placa_veiculo,
                L_sped_header.chv_nfe,
                L_sped_header.ind_proc,
                L_sped_header.num_proc,
                L_sped_header.cod_inf,
                L_txt,
                L_sped_header.chv_cte,
                L_sped_header.tp_cte,
                L_sped_header.chv_cte_ref,
                L_vlr_base_pis_h,
                L_vlr_total_pis_h,
                L_vlr_base_cofins_h,
                L_vlr_total_cofins_h,
                L_vlr_total_pis_st_h,
                L_vlr_total_cofins_st_h,
                L_sped_header.status,
                L_sped_header.cod_cfo,
                L_sped_header.cod_medida,
                L_num_pedido,
                L_sped_header.exit_hour,
                L_sped_header.dt_ini,
                L_sped_header.dt_fim);

         SQL_LIB.SET_MARK('END LP', 'C_SPED_HEADER', 'FM_FSCL_DOC_HDR', NULL);

                L_vlr_total_icms_h      := null;
                L_vlr_total_ipi_h       := null;
                L_vlr_base_ipi_h        := null;
                L_vlr_total_ir_h        := null;
                L_vlr_base_ir_h         := null;
                L_vlr_total_iss_h       := null;
                L_vlr_base_iss_h        := null;
                L_vlr_total_inss_h      := null;
                L_vlr_base_inss_h       := null;
                L_vlr_total_icms_st_h   := null;
                L_vlr_base_icms_st_h    := null;
                L_vlr_total_pis_h       := null;
                L_vlr_base_pis_h        := null;
                L_vlr_total_pis_st_h    := null;
                L_vlr_base_pis_st_h     := null;
                L_vlr_total_cofins_h    := null;
                L_vlr_base_cofins_h     := null;
                L_vlr_total_cofins_st_h := null;
                L_vlr_base_cofins_st_h  := null;

         END LOOP;
       --

       return TRUE;

   EXCEPTION
    WHEN OTHERS THEN
              O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                    SQLERRM,
                                                    L_program,
                                                    to_char(SQLCODE));
             return FALSE;
    END FM_SPED_HEADER;
------------------------------------------------------------------------------------------------
  FUNCTION FM_SPED_DETAIL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_last_update_datetime IN VARCHAR2,
                          I_sped_last_run_date   IN VARCHAR2)
  return BOOLEAN AS

        CURSOR C_SPED_DETAIL IS
               SELECT ffdd.fiscal_doc_id INVOICE_ID,
                      ffdd.fiscal_doc_line_id,
                      ffdd.line_no LINE_NUMBER,
                      TO_CHAR(ffdh.fiscal_doc_no) NUM_DOCFIS,
                      ffdh.issue_date DATA_EMISSAO,
                      ffdd.item COD_PRODUTO,
                      ffdd.quantity QUANTIDADE,
                      ffdd.unit_cost PRECO_UNITARIO,
                      ffdd.total_cost VLR_TOTAL_ITEM,
                      'N' VLR_INCLUI_IMPOSTO,
                      ffdd.nf_cfop COD_CFO,
                      SUBSTR(ffdd.classification_id,1,8) COD_NCM,
                      ffdh.utilization_id COD_UTIL_FISCAL,
                      (ffdd.quantity*ffdd.unit_item_disc) VLR_DESCONTO,
                      (CASE WHEN ffdd.total_cost IS NOT NULL THEN (ROUND(100*(NVL((ffdd.total_cost - (ffdd.unit_cost_with_disc * ffdd.quantity)),0)/ffdd.total_cost),2)) ELSE NULL END) PERC_DESCONTO,
                      '2' TP_TRIB_IPI,
                      'N' IPI_EMBUTIDO,
                      DECODE(ffdd.item,'NULL',1,0) IND_MOV,
                      1 IND_TOT,
                      ffdh.utilization_id,
                      ffdd.item,
                      fs.accounting_date,
                      ffdd.icms_cst COD_SIT_TRIB_EST,
                      ffdd.pis_cst CST_PIS,
                      ffdd.cofins_cst CST_COFINS,
                      fs.mode_type,
                      ffdh.create_datetime DT_INI,
                      ffdh.last_update_datetime DT_FIM
                 FROM fm_fiscal_doc_detail ffdd,
                      fm_fiscal_doc_header ffdh,
                      fm_schedule fs
                WHERE ffdh.fiscal_doc_id = ffdd.fiscal_doc_id
                  AND ffdh.schedule_no   = fs.schedule_no
                  AND fs.status in ('FP')
                  AND ffdh.status not in ('I')
                AND ffdh.last_update_datetime >to_date(I_sped_last_run_date,'DD-MON-RR-HH24:MI:SS')
           AND ffdh.last_update_datetime <=to_date(I_last_update_datetime,'DD-MON-RR-HH24:MI:SS') ;


        CURSOR C_cod_natureza_op(P_utilization_id fm_fiscal_utilization.utilization_id%TYPE) IS
               SELECT ffu.nop
                 FROM fm_fiscal_utilization ffu
                WHERE ffu.utilization_id = P_utilization_id;

        CURSOR C_tax_value(P_fiscal_doc_line_id fm_fiscal_doc_detail.fiscal_doc_line_id%TYPE) IS
               SELECT vat_code,
                      total_value,
                      nvl(modified_tax_basis,tax_basis) tax_basis,
                      DECODE(percentage_rate,0,appr_tax_rate,percentage_rate) percentage_rate
                 FROM fm_fiscal_doc_tax_detail
                WHERE fiscal_doc_line_id = P_fiscal_doc_line_id;

        CURSOR C_ITEM(P_item item_master.item%TYPE) IS
               SELECT DECODE (item_level,3,item_number_type, NULL),
                      item_desc,
                      standard_uom
                 FROM item_master
                WHERE item = P_item;

         CURSOR C_ORGIN_SERVICE_CODE(P_item item_master.item%TYPE) IS
             SELECT vifa.origin_code,
                    vifa.service_code
             FROM v_br_item_fiscal_attrib vifa
             WHERE vifa.item = P_item;

         CURSOR C_perc_mva(P_invoice_id fm_fiscal_doc_header.fiscal_doc_id%TYPE) IS
                SELECT tax_perc_mva
                  FROM fm_fiscal_doc_tax_detail_ext
                 WHERE fiscal_doc_id = P_invoice_id
                   AND tax_code      ='ICMSST';

         CURSOR C_vlr_base_redu_icms(P_fiscal_doc_line_id fm_fiscal_doc_detail.fiscal_doc_line_id%TYPE) IS
                SELECT ffdd.recoverable_base
                  FROM fm_fiscal_doc_tax_detail ffdtd,
                       fm_fiscal_doc_detail ffdd
                 WHERE ffdtd.fiscal_doc_line_id = ffdd.fiscal_doc_line_id
                   AND ffdtd.vat_code='ICMS'
                   AND ffdd.fiscal_doc_line_id = P_fiscal_doc_line_id;

         L_cod_natureza_op        fm_fiscal_utilization.nop%TYPE;
         L_vlr_total_pis_d        fm_fiscal_doc_tax_detail.total_value%TYPE;
         L_vlr_total_cofins_d     fm_fiscal_doc_tax_detail.total_value%TYPE;
         L_vlr_total_icms_d      fm_fiscal_doc_tax_detail.total_value%TYPE;
         L_vlr_total_icms_st_d    fm_fiscal_doc_tax_detail.total_value%TYPE;
         L_vlr_total_ipi_d        fm_fiscal_doc_tax_detail.total_value%TYPE;
         L_vlr_total_iss_d        fm_fiscal_doc_tax_detail.total_value%TYPE;
         L_vlr_base_pis_d         fm_fiscal_doc_tax_detail.tax_basis%TYPE;
         L_vlr_base_cofins_d      fm_fiscal_doc_tax_detail.tax_basis%TYPE;
         L_vlr_base_icms_d        fm_fiscal_doc_tax_detail.tax_basis%TYPE;
         L_vlr_base_icms_st_d     fm_fiscal_doc_tax_detail.tax_basis%TYPE;
         L_vlr_base_ipi_d         fm_fiscal_doc_tax_detail.tax_basis%TYPE;
         L_vlr_base_iss_d         fm_fiscal_doc_tax_detail.tax_basis%TYPE;
         L_vlr_aliq_pis_d         fm_fiscal_doc_tax_detail.percentage_rate%TYPE;
         L_vlr_aliq_cofins_d      fm_fiscal_doc_tax_detail.percentage_rate%TYPE;
         L_vlr_aliq_icms_d        fm_fiscal_doc_tax_detail.percentage_rate%TYPE;
         L_vlr_aliq_icms_st_d     fm_fiscal_doc_tax_detail.percentage_rate%TYPE;
         L_vlr_aliq_ipi_d         fm_fiscal_doc_tax_detail.percentage_rate%TYPE;
         L_vlr_aliq_iss_d         fm_fiscal_doc_tax_detail.percentage_rate%TYPE;
         L_perc_mva               fm_fiscal_doc_tax_detail_ext.tax_perc_mva%TYPE;
         L_vlr_base_redu_icmsst_d fm_fiscal_doc_detail.recoverable_base%TYPE;
         L_vlr_base_redu_icms_d   fm_fiscal_doc_detail.recoverable_base%TYPE;
         L_cod_ean              VARCHAR2(6);
         L_descricao_produto    VARCHAR2(250);
         L_cod_medida           VARCHAR2(4);
         L_tp_orgem_item        VARCHAR2(250);
         L_serv_code            VARCHAR2(250);
         L_tp_trib_icms         NUMBER(6);
         L_PROGRAM              VARCHAR2(4000) :='FM_SPED_DETAIL';

  BEGIN

     IF I_last_update_datetime IS NULL OR I_sped_last_run_date IS NULL THEN

                O_error_message := SQL_LIB.CREATE_MSG('Required Input Parameter is NULL',
                                                      I_last_update_datetime||' '||I_sped_last_run_date,
                                                      L_program,
                                                      to_char(SQLCODE));

                return FALSE;

     END IF;

     --
       SQL_LIB.SET_MARK('OPEN', 'C_SPED_DETAIL', 'FM_FSCL_DOC_DTL', NULL);
       FOR L_sped_detail IN c_sped_detail LOOP

        SQL_LIB.SET_MARK('OPEN', 'C_COD_NATUREZA_OP', 'FM_FSCL_UTILIZATION', NULL);
        OPEN C_cod_natureza_op(L_sped_detail.utilization_id);
             SQL_LIB.SET_MARK('FETCH', 'C_COD_NATUREZA_OP', 'FM_FSCL_UTILIZATION', NULL);
             FETCH C_cod_natureza_op INTO L_cod_natureza_op;
        SQL_LIB.SET_MARK('CLOSE', 'C_COD_NATUREZA_OP', 'FM_FSCL_UTILIZATION', NULL);
        CLOSE C_cod_natureza_op;
        --
          SQL_LIB.SET_MARK('OPEN', 'C_TAX_VALUE', 'FM_FSCL_DOC_TX_DTL', NULL);
          FOR L_tax_value IN C_TAX_VALUE(L_sped_detail.fiscal_doc_line_id) LOOP
            --
              IF L_tax_value.vat_code = 'ICMS' THEN
                 L_vlr_total_icms_d := L_tax_value.total_value;
                 L_vlr_base_icms_d:= L_tax_value.tax_basis;
                 L_vlr_aliq_icms_d  := L_tax_value.percentage_rate;
              ELSIF L_tax_value.vat_code = 'IPI' THEN
                    L_vlr_total_ipi_d := L_tax_value.total_value;
                    L_vlr_base_ipi_d  := L_tax_value.tax_basis;
                    L_vlr_aliq_ipi_d  := L_tax_value.percentage_rate;
              ELSIF L_tax_value.vat_code = 'ISS' THEN
                    L_vlr_total_iss_d := L_tax_value.total_value;
                    L_vlr_base_iss_d  := L_tax_value.tax_basis;
                    L_vlr_aliq_iss_d  := L_tax_value.percentage_rate;
              ELSIF L_tax_value.vat_code = 'ICMSST' THEN
                    L_vlr_total_icms_st_d := L_tax_value.total_value;
                    L_vlr_base_icms_st_d  := L_tax_value.tax_basis;
                    L_vlr_aliq_icms_st_d  := L_tax_value.percentage_rate;
              ELSIF L_tax_value.vat_code = 'PIS' THEN
                    L_vlr_total_pis_d := L_tax_value.total_value;
                    L_vlr_base_pis_d  := L_tax_value.tax_basis;
                    L_vlr_aliq_pis_d  := L_tax_value.percentage_rate;
              ELSIF L_tax_value.vat_code = 'COFINS' THEN
                    L_vlr_total_cofins_d := L_tax_value.total_value;
                    L_vlr_base_cofins_d  := L_tax_value.tax_basis;
                    L_vlr_aliq_cofins_d  := L_tax_value.percentage_rate;
              END IF;
            --
          SQL_LIB.SET_MARK('END LP', 'C_TAX_VALUE', 'FM_FSCL_DOC_TX_DTL', NULL);
          END LOOP;
        --
          SQL_LIB.SET_MARK('OPEN', 'C_ITEM', 'ITEM_MASTER', NULL);
          OPEN C_ITEM(L_sped_detail.item);
               SQL_LIB.SET_MARK('FETCH', 'C_ITEM', 'ITEM_MASTER', NULL);
               FETCH C_ITEM
                INTO L_cod_ean,
                     L_descricao_produto,
                     L_cod_medida;
          SQL_LIB.SET_MARK('CLOSE', 'C_ITEM', 'ITEM_MASTER', NULL);
          CLOSE C_ITEM;
        --

          SQL_LIB.SET_MARK('OPEN', 'C_ORGIN_SERVICE_CODE', 'V_BR_ITEM_FISCAL_ATTRIB', NULL);
          OPEN C_ORGIN_SERVICE_CODE(L_sped_detail.item);
               SQL_LIB.SET_MARK('FETCH', 'C_ORGIN_SERVICE_CODE', 'V_BR_ITEM_FISCAL_ATTRIB', NULL);
               FETCH C_ORGIN_SERVICE_CODE
                INTO L_tp_orgem_item,
                     L_serv_code;
          SQL_LIB.SET_MARK('CLOSE', 'C_ORGIN_SERVICE_CODE', 'V_BR_ITEM_FISCAL_ATTRIB', NULL);
          CLOSE C_ORGIN_SERVICE_CODE;
        --

          SQL_LIB.SET_MARK('OPEN', 'C_PERC_MVA', 'FM_FISCAL_DOC_TAX_DETAIL_EXT', NULL);
          OPEN C_perc_mva(L_sped_detail.invoice_id );
               SQL_LIB.SET_MARK('FETCH', 'C_PERC_MVA', 'FM_FISCAL_DOC_TAX_DETAIL_EXT', NULL);
               FETCH C_perc_mva
               INTO L_perc_mva;
          SQL_LIB.SET_MARK('CLOSE', 'C_PERC_MVA', 'FM_FISCAL_DOC_TAX_DETAIL_EXT', NULL);
          CLOSE C_perc_mva;
        --

          SQL_LIB.SET_MARK('OPEN', 'C_VLR_BASE_REDU_ICMS', 'fm_fiscal_doc_tax_detail fm_fiscal_doc_detail', NULL);
          OPEN C_vlr_base_redu_icms(L_sped_detail.fiscal_doc_line_id);
               SQL_LIB.SET_MARK('FETCH', 'C_VLR_BASE_REDU_ICMS', 'fm_fiscal_doc_tax_detail fm_fiscal_doc_detail', NULL);
               FETCH C_vlr_base_redu_icms INTO L_vlr_base_redu_icms_d;
          SQL_LIB.SET_MARK('CLOSE', 'C_VLR_BASE_REDU_ICMS', 'fm_fiscal_doc_tax_detail fm_fiscal_doc_detail', NULL);
          CLOSE C_vlr_base_redu_icms;
        --

          IF L_sped_detail.cod_sit_trib_est = '00' THEN
             L_tp_trib_icms := 1;
          ELSIF L_sped_detail.cod_sit_trib_est = '40' OR L_sped_detail.cod_sit_trib_est = '41' THEN
             L_tp_trib_icms := 2;
          ELSIF L_sped_detail.cod_sit_trib_est = '20' OR L_sped_detail.cod_sit_trib_est = '70' THEN
             L_tp_trib_icms := 4;
          ELSE
             L_tp_trib_icms := 3;
          END IF;

           INSERT INTO fm_sped_fiscal_doc_detail(INVOICE_ID,
                                                 FISCAL_DOC_LINE_ID,
                                                 LINE_NUMBER,
                                                 NUM_DOCFIS,
                                                 DATA_EMISSAO,
                                                 COD_PRODUTO,
                                                 QUANTIDADE,
                                                 PRECO_UNITARIO,
                                                 VLR_TOTAL_ITEM,
                                                 COD_MEDIDA,
                                                 VLR_INCLUI_IMPOSTO,
                                                 COD_CFOP,
                                                 COD_NCM,
                                                 COD_UTIL_FISCAL,
                                                 TP_ORIGEM_ITEM,
                                                 COD_SIT_TRIB_EST,
                                                 COD_NATUREZA_OP,
                                                 VLR_BASE_PIS,
                                                 VLR_TOTAL_PIS,
                                                 VLR_ALIQ_PIS,
                                                 VLR_BASE_COFINS,
                                                 VLR_TOTAL_COFINS,
                                                 VLR_ALIQ_COFINS,
                                                 VLR_BASE_ICMS,
                                                 VLR_TOTAL_ICMS,
                                                 VLR_ALIQ_ICMS,
                                                 VLR_BASE_ICMS_ST,
                                                 VLR_TOTAL_ICMS_ST,
                                                 VLR_ALIQ_ICMS_ST,
                                                 TP_TRIB_ICMS,
                                                 VLR_DESCONTO,
                                                 PERC_DESCONTO,
                                                 TP_TRIB_IPI,
                                                 IPI_EMBUTIDO,
                                                 VLR_ALIQ_IPI,
                                                 VLR_TOTAL_IPI,
                                                 VLR_BASE_IPI,
                                                 IND_MOV ,
                                                 CST_PIS,
                                                 CST_COFINS,
                                                 DESCRICAO_PRODUTO,
                                                 SERV_CODE,
                                                 VLR_BASE_ISS,
                                                 VLR_TOTAL_ISS,
                                                 VLR_ALIQ_ISS,
                                                 IND_TOT,
                                                 COD_EAN,
                                                 MOVTO_E_S,
                                                 PERC_MVA,
                                                 VLR_BASE_REDU_ICMSST,
                                                 VLR_BASE_REDU_ICMS,
                                                  DATA_GL,
                                                 DT_INI,
                                                 DT_FIM)
            VALUES (L_sped_detail.invoice_id,
                    L_sped_detail.fiscal_doc_line_id,
                    L_sped_detail.line_number,
                    L_sped_detail.num_docfis,
                    L_sped_detail.data_emissao,
                    L_sped_detail.cod_produto,
                    L_sped_detail.quantidade,
                    L_sped_detail.preco_unitario,
                    L_sped_detail.vlr_total_item,
                    L_cod_medida,
                    L_sped_detail.vlr_inclui_imposto,
                    L_sped_detail.cod_cfo,
                    L_sped_detail.cod_ncm,
                    L_sped_detail.cod_util_fiscal,
                    L_tp_orgem_item,
                    L_sped_detail.cod_sit_trib_est,
                    L_cod_natureza_op,
                    L_vlr_base_pis_d,
                    L_vlr_total_pis_d,
                    L_vlr_aliq_pis_d,
                    L_vlr_base_cofins_d,
                    L_vlr_total_cofins_d,
                    L_vlr_aliq_cofins_d,
                    L_vlr_base_icms_d,
                    L_vlr_total_icms_d,
                    L_vlr_aliq_icms_d,
                    L_vlr_base_icms_st_d,
                    L_vlr_total_icms_st_d,
                    L_vlr_aliq_icms_st_d,
                    L_tp_trib_icms,
                    L_sped_detail.vlr_desconto,
                    L_sped_detail.perc_desconto,
                    L_sped_detail.tp_trib_ipi,
                    L_sped_detail.ipi_embutido,
                    L_vlr_aliq_ipi_d,
                    L_vlr_total_ipi_d,
                    L_vlr_base_ipi_d,
                    L_sped_detail.ind_mov ,
                    L_sped_detail.cst_pis,
                    L_sped_detail.cst_cofins,
                    L_descricao_produto,
                    L_serv_code,
                    L_vlr_base_iss_d,
                    L_vlr_total_iss_d,
                    L_vlr_aliq_iss_d,
                    L_sped_detail.ind_tot,
                    L_cod_ean,
                    L_sped_detail.mode_type,
                    L_perc_mva,
                    L_vlr_base_redu_icmsst_d,
                    L_vlr_base_redu_icms_d,
                    L_sped_detail.accounting_date,
                    L_sped_detail.dt_ini,
                    L_sped_detail.dt_fim);

       SQL_LIB.SET_MARK('END LP', 'C_SPED_DETAIL', 'FM_FSCL_DOC_DTL', NULL);

                    L_vlr_total_icms_d      := null;
                    L_vlr_base_icms_d       := null;
                    L_vlr_aliq_icms_d       := null;
                    L_vlr_total_ipi_d       := null;
                    L_vlr_base_ipi_d        := null;
                    L_vlr_aliq_ipi_d        := null;
                    L_vlr_total_iss_d       := null;
                    L_vlr_base_iss_d        := null;
                    L_vlr_aliq_iss_d        := null;
                    L_vlr_total_icms_st_d   := null;
                    L_vlr_base_icms_st_d    := null;
                    L_vlr_aliq_icms_st_d    := null;
                    L_vlr_total_pis_d       := null;
                    L_vlr_base_pis_d        := null;
                    L_vlr_aliq_pis_d        := null;
                    L_vlr_total_cofins_d    := null;
                    L_vlr_base_cofins_d     := null;
                    L_vlr_aliq_cofins_d     := null;


       END LOOP;
     --
  return TRUE;

  EXCEPTION
     WHEN OTHERS THEN
          O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                 SQLERRM,
                                                 L_program,
                                                 to_char(SQLCODE));
          return FALSE;
  END FM_SPED_DETAIL;
-------------------------------------------------------------------------------------------------
  FUNCTION GET_SPED_INFO(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_last_update_datetime IN     VARCHAR2)
  return BOOLEAN AS

        CURSOR  C_SPED_LAST_RUN_DATE IS
             SELECT to_char(last_run_date,'DD-MON-RR-HH24:MI:SS')
               FROM fm_sped_last_run_date;

        L_sped_last_run_date VARCHAR2(50);
        L_program            VARCHAR2(4000)           := 'GET_SPED_INFO';
        L_key                DATE:= to_date(I_last_update_datetime,'DD-MON-RR-HH24:MI:SS');

  BEGIN


      IF I_last_update_datetime IS NULL THEN

         O_error_message := SQL_LIB.CREATE_MSG('Required Input Parameter is NULL',
                                               I_last_update_datetime,
                                               L_program,
                                               to_char(SQLCODE));

         return FALSE;

      END IF;

      SQL_LIB.SET_MARK('OPEN', 'C_SPED_LAST_RUN_DATE', 'FM_SPED_LAST_RUN_DATE', L_key);

      OPEN C_SPED_LAST_RUN_DATE;
           FETCH C_SPED_LAST_RUN_DATE INTO L_sped_last_run_date;
      CLOSE C_SPED_LAST_RUN_DATE;
    --

      IF FM_SPED_HEADER(O_error_message,
                        I_last_update_datetime,
                        L_sped_last_run_date) = FALSE THEN
         DBMS_OUTPUT.PUT_LINE ('Error Message :'||O_error_message);
         return FALSE;
      ELSE
         IF FM_SPED_DETAIL(O_error_message,
                           I_last_update_datetime,
                           L_sped_last_run_date) = FALSE THEN
            DBMS_OUTPUT.PUT_LINE ('Error Message :'||O_error_message);
            return FALSE;
         ELSE
            SQL_LIB.SET_MARK('UPDATE','FM_SPED_LAST_RUN_DATE','LAST_RUN_DATE', L_key);
            UPDATE fm_sped_last_run_date set last_run_date = SYSDATE;
            return TRUE;
         END IF;
      END IF;
    --

  EXCEPTION
     WHEN OTHERS THEN
          O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                 SQLERRM,
                                                 L_program,
                                                 to_char(SQLCODE));
          return FALSE;
  END GET_SPED_INFO;
--
END FM_SPED_SQL;
/