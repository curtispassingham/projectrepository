CREATE OR REPLACE PACKAGE BODY FM_RTV_CONSUME_SQL AS
---------------------------------------------------------------------------------------------
-- Function Name: BATCH_CONSUME_THREAD
-- Purpose      : Overloaded private function called for consuming RTV messages.
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_no       IN       NUMBER,
                              I_num_threads     IN       NUMBER,
                              I_status          IN       FM_STG_RTV_DESC.STATUS%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind       IN OUT   BOOLEAN,
                 I_seq_no          IN       FM_STG_RTV_DESC.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(100)       := 'FM_RTV_CONSUME_SQL.CONSUME';
   L_message_type    VARCHAR(20)         := RMSSUB_RTV.LP_cre_type;

   L_status_code     VARCHAR2(1)         := NULL;
   L_rtv_message     "RIB_RTVDesc_REC"   := NULL;

   cursor C_GET_RTV_DESC is
      select "RIB_RTVDesc_REC" (0,                                   -- rib_oid
                                r.dc_dest_id,                        -- dc_dest_id
                                r.rtv_id,                            -- rtv_id
                                r.rtn_auth_nbr,                      -- rtn_auth_nbr
                                r.vendor_nbr,                        -- vendor_nbr
                                r.ship_address1,                     -- ship_address1
                                r.ship_address2,                     -- ship_address2
                                r.ship_address3,                     -- ship_address3
                                r.state,                             -- state
                                r.city,                              -- city
                                r.shipto_zip,                        -- shipto_zip
                                r.country,                           -- country
                                r.creation_ts,                       -- creation_ts
                                r.comments,                          -- comments
                                r.rtv_order_no,                      -- rtv_order_no
                                CAST(MULTISET(select "RIB_RTVDtl_REC" (0,                    -- rib_oid
                                                                       d.item_id,            -- item_id
                                                                       d.unit_qty,           -- unit_qty
                                                                       d.container_qty,      -- container_qty
                                                                       d.from_disposition,   -- from_disposition
                                                                       d.to_disposition,     -- to_disposition
                                                                       d.ebc_cost,           -- unit_cost
                                                                       d.reason,             -- reason
                                                                       d.weight,             -- weight
                                                                       d.weight_uom,         -- weight_uom
                                                                       d.nic_cost)           -- gross_cost
                                                from fm_stg_rtv_dtl d
                                               where r.seq_no = d.desc_seq_no) as "RIB_RTVDtl_TBL"), --rtvdtl_tbl
                                case                                 -- status_ind
                                   when r.status in ('H', 'P') then
                                      'S'
                                   when r.status = 'A' then
                                      'A'
                                   else
                                      NULL
                                end)
        from fm_stg_rtv_desc r             
       where r.seq_no= I_seq_no
         and r.status in ('H','P','A');

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_RTV_DESC',
                    'fm_stg_rtv_desc, fm_stg_rtv_dtl',
                    NULL);
   open C_GET_RTV_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_RTV_DESC',
                    'fm_stg_rtv_desc, fm_stg_rtv_dtl',
                    NULL);
   fetch C_GET_RTV_DESC into L_rtv_message;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_RTV_DESC',
                    'fm_stg_rtv_desc, fm_stg_rtv_dtl',
                    NULL);
   close C_GET_RTV_DESC;

   -- Call procedure Consume
   RMSSUB_RTV.CONSUME_RTV(L_status_code,
                          O_error_message,
                          L_rtv_message,
                          L_message_type,
                          'N');   --I_check_l10n_ind

   if L_status_code = API_CODES.UNHANDLED_ERROR or O_error_message is NOT NULL then
      O_error_ind := TRUE;
      return FALSE;
   else
      O_error_ind := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return FALSE;
END CONSUME;
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_no       IN       NUMBER,
                              I_num_threads     IN       NUMBER)
RETURN BOOLEAN IS

   L_program         VARCHAR2(100)                 := 'FM_RTV_CONSUME_SQL.BATCH_CONSUME_THREAD';
   L_status          FM_STG_RTV_DESC.STATUS%TYPE   := NULL;

BEGIN

   -- Consume all RTV message in 'A'pproved status
   L_status := 'A';
   if BATCH_CONSUME_THREAD(O_error_message,
                           I_thread_no,
                           I_num_threads,
                           L_status) = FALSE then
      return FALSE;
   end if;

   -- Consume all RTV message in 'H'old status
   L_status := 'H';
   if BATCH_CONSUME_THREAD(O_error_message,
                           I_thread_no,
                           I_num_threads,
                           L_status) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END BATCH_CONSUME_THREAD;
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_no       IN       NUMBER,
                              I_num_threads     IN       NUMBER,
                              I_status          IN       FM_STG_RTV_DESC.STATUS%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(100)      := 'FM_RTV_CONSUME_SQL.BATCH_CONSUME_THREAD';
   L_table           VARCHAR2(50)       := NULL;
   L_rowid_dtl_tbl   ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_dsc_tbl   ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_dh_tbl    ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_sch_tbl   ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_seq_tbl         NUMBER_TBL         := NUMBER_TBL();
   L_fd_id_tbl       NUMBER_TBL         := NUMBER_TBL();
   L_error_ind       BOOLEAN;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   -- Staging Records
   cursor C_GET_STG_RTV is
   select seq_no,
          fiscal_doc_id
     from fm_stg_rtv_desc
    where status = I_status
      and mod(dc_dest_id, I_num_threads)+1 = I_thread_no
    order by seq_no;

   -- Locking cursors
   cursor C_LOCK_STG_RTV_DESC is
      select ROWIDTOCHAR(rowid)
        from fm_stg_rtv_desc
       where status = I_status
         and mod(dc_dest_id, I_num_threads)+1 = I_thread_no
         for update nowait;

   cursor C_LOCK_STG_RTV_DTL is
      select ROWIDTOCHAR(rowid)
        from fm_stg_rtv_dtl sd
       where exists (select 'x'
                       from TABLE(CAST(L_seq_tbl as NUMBER_TBL)) seq
                      where value(seq) = sd.desc_seq_no
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_FISCAL_DOC_HEADER is
      select ROWIDTOCHAR(rowid)
        from fm_fiscal_doc_header dh
       where dh.status = 'H'
         and not exists (select 'x'
                           from fm_stg_rtv_desc rd
                          where rd.fiscal_doc_id = dh.fiscal_doc_id
                            and rownum = 1)
         and exists (select 'x'
                       from TABLE(CAST(L_fd_id_tbl as NUMBER_TBL)) fd
                      where value(fd) = dh.fiscal_doc_id
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_SCHEDULE is
      select ROWIDTOCHAR(rowid)
        from fm_schedule s
       where s.status = 'H'
         and not exists (select 'x'
                           from fm_fiscal_doc_header dh
                          where dh.status != 'A'
                            and dh.schedule_no = s.schedule_no
                            and exists (select 'x'
                                          from TABLE(CAST(L_fd_id_tbl as NUMBER_TBL)) fd
                                         where value(fd) = dh.fiscal_doc_id
                                           and rownum = 1)
                            and rownum = 1)
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STG_RTV',
                    'fm_stg_rtv_desc',
                    NULL);
   open C_GET_STG_RTV;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STG_RTV',
                    'fm_stg_rtv_desc',
                    NULL);
   fetch C_GET_STG_RTV BULK COLLECT into L_seq_tbl,
                                         L_fd_id_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STG_RTV',
                    'fm_stg_rtv_desc',
                    NULL);
   close C_GET_STG_RTV;

   if L_seq_tbl is NULL or L_seq_tbl.count <= 0 then
      return TRUE;
   end if;

   -- Loop thru all the sequence numbers
   FOR rec in L_seq_tbl.FIRST..L_seq_tbl.LAST LOOP
      -- call consume for each seq_no
      if FM_RTV_CONSUME_SQL.CONSUME(O_error_message,
                                    L_error_ind,
                                    L_seq_tbl(rec)) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   -- Delete consumed RTVs
   L_table := 'FM_STG_RTV_DTL';
   open C_LOCK_STG_RTV_DTL;
   fetch C_LOCK_STG_RTV_DTL BULK COLLECT into L_rowid_dtl_tbl;
   close C_LOCK_STG_RTV_DTL;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'fm_stg_rtv_dtl',
                    NULL);
   delete from fm_stg_rtv_dtl sr
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_dtl_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = sr.rowid);

   L_table := 'FM_STG_RTV_DESC';
   open C_LOCK_STG_RTV_DESC;
   fetch C_LOCK_STG_RTV_DESC BULK COLLECT into L_rowid_dsc_tbl;
   close C_LOCK_STG_RTV_DESC;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'fm_stg_rtv_desc',
                    NULL);
   delete from fm_stg_rtv_desc sd
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_dsc_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = sd.rowid);

   -- Update status of fiscal doc to 'A'pproved
   L_table := 'FM_FISCAL_DOC_HEADER';
   open C_LOCK_FISCAL_DOC_HEADER;
   fetch C_LOCK_FISCAL_DOC_HEADER BULK COLLECT into L_rowid_dh_tbl;
   close C_LOCK_FISCAL_DOC_HEADER;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'fm_fiscal_doc_header',
                    NULL);
   update fm_fiscal_doc_header dh
      set dh.status = 'A'
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_dh_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = dh.rowid);

   -- Update status of schedule to 'A'pproved
   L_table := 'FM_SCHEDULE';
   open C_LOCK_SCHEDULE;
   fetch C_LOCK_SCHEDULE BULK COLLECT into L_rowid_sch_tbl;
   close C_LOCK_SCHEDULE;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'fm_schedule',
                    NULL);
   update fm_schedule s
      set s.status = 'A'
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_sch_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = s.rowid);
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             SQLERRM,
                                             L_table,
                                             TO_CHAR(SQLCODE));

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END BATCH_CONSUME_THREAD;
---------------------------------------------------------------------------------------------
END FM_RTV_CONSUME_SQL;
/