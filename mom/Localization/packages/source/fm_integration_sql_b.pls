CREATE OR REPLACE PACKAGE BODY FM_INTEGRATION_SQL is
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_STG_STATUS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_status             IN       VARCHAR2 DEFAULT NULL,
                           I_requisition_type   IN       FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE,
                           I_seq_no             IN       FM_STG_ASNOUT_DESC.SEQ_NO%TYPE,
                           I_fiscal_doc_id      IN       FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   ---
   L_program          VARCHAR2(100) := 'FM_INTEGRATION_SQL.UPDATE_STG_STATUS';
   L_key              VARCHAR2(100);
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_STG_ADJ_DESC is
      select 1
        from fm_stg_adjust_desc fsad
       where fsad.seq_no = I_seq_no
         for update nowait;
   ---
   cursor C_LOCK_STG_RTV_DESC is
      select 1
        from fm_stg_rtv_desc fsrd
       where fsrd.seq_no     = I_seq_no
         for update nowait;
   ---
   cursor C_LOCK_STG_ASNOUT_DESC is
      select 1
        from fm_stg_asnout_desc fsad
       where fsad.seq_no = I_seq_no
         for update nowait;
   ---
   cursor C_LOCK_STG_RECV_DESC is
      select 1
        from fm_receiving_header frh
       where frh.recv_no = I_seq_no
         for update nowait;

BEGIN

   if I_requisition_type = 'RECV' then
      L_key   := 'recv_no = '||I_seq_no;
      SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_RECV_DESC','FM_RECEIVING_HEADER',L_key);
      open C_LOCK_STG_RECV_DESC;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_RECV_DESC','FM_RECEIVING_HEADER',L_key);
      close C_LOCK_STG_RECV_DESC;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'FM_RECEIVING_HEADER',L_key);
      ---
      update fm_receiving_header
         set status = I_status,
             last_update_id = USER,
             last_update_datetime = SYSDATE
       where recv_no = I_seq_no;
   elsif I_requisition_type in ('TSF', 'IC', 'REP') then
      L_key   := 'seq_no = '||I_seq_no;
      SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ASNOUT_DESC','FM_STG_ASNOUT_DESC',L_key);
      open C_LOCK_STG_ASNOUT_DESC;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ASNOUT_DESC','FM_STG_ASNOUT_DESC',L_key);
      close C_LOCK_STG_ASNOUT_DESC;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'FM_STG_ASNOUT_DESC',L_key);
      ---
      update fm_stg_asnout_desc
         set status = NVL(I_status,status),
             fiscal_doc_id = NVL(I_fiscal_doc_id,fiscal_doc_id),
             last_update_id = USER,
             last_update_datetime = SYSDATE
       where seq_no = I_seq_no;
   elsif I_requisition_type ='RTV' then
      L_key   := 'seq_no = '||I_seq_no;
      SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_RTV_DESC','FM_STG_RTV_DESC',L_key);
      open C_LOCK_STG_RTV_DESC;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_RTV_DESC','FM_STG_RTV_DESC',L_key);
      close C_LOCK_STG_RTV_DESC;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'FM_STG_RTV_DESC',L_key);
      ---
      update fm_stg_rtv_desc
         set status = NVL(I_status,status),
             fiscal_doc_id = NVL(I_fiscal_doc_id,fiscal_doc_id),
             last_update_id = USER,
             last_update_datetime = SYSDATE
       where seq_no = I_seq_no;
      ---
   elsif I_requisition_type ='STOCK' then
      L_key   := 'seq_no = '||I_seq_no;
      SQL_LIB.SET_MARK('OPEN','C_LOCK_STG_ADJ_DESC','FM_STG_ADJUST_DESC',L_key);
      open C_LOCK_STG_ADJ_DESC;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_STG_ADJ_DESC','FM_STG_ADJUST_DESC',L_key);
      close C_LOCK_STG_ADJ_DESC;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'FM_STG_ADJUST_DESC',L_key);
      ---
      update fm_stg_adjust_desc
         set status = NVL(I_status,status),
             fiscal_doc_id = NVL(I_fiscal_doc_id,fiscal_doc_id),
             last_update_id = USER,
             last_update_datetime = SYSDATE
       where seq_no = I_seq_no;
    ---
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_STG_STATUS;
--------------------------------------------------------------------------------
FUNCTION CONSUME_RTV(O_status_code     IN OUT   VARCHAR2,
                     O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message         IN       RIB_OBJECT,
                     I_message_type    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(61) := 'FM_INTEGRATION_SQL.CONSUME_RTV';
   L_seq_no           FM_STG_RTV_DESC.SEQ_NO%TYPE := NULL;
   L_status_code      INTEGER(1);

BEGIN

   if FM_RTV_SQL.CONSUME(O_error_message,
                         L_seq_no,
                         I_message,
                         I_message_type) = FALSE then
      O_status_code := 'E';
      return FALSE;
   end if;

   if L_seq_no is NOT NULL then
      if FM_EXIT_NF_CREATION_SQL.PROCESS(O_error_message,
                                         L_status_code,
                                         'RTV',
                                         L_seq_no) = FALSE then
         O_status_code := 'E';
         return FALSE;
      end if;
   end if;

   O_status_code := 'S';
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status_code := 'E';
      return FALSE;
END CONSUME_RTV;
----------------------------------------------------------------------------------
FUNCTION CONSUME_SHIPMENT(O_status_code     IN OUT   VARCHAR2,
                          O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       RIB_OBJECT,
                          I_message_type    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'FM_INTEGRATION_SQL.CONSUME_SHIPMENT';
   L_seq_no         FM_STG_ASNOUT_DESC.SEQ_NO%TYPE := NULL;
   L_status_code    INTEGER(1);

BEGIN

   if FM_ASNOUT_SQL.CONSUME(O_error_message,
                            L_seq_no,
                            I_message,
                            I_message_type) = FALSE then
      O_status_code := 'E';
      return FALSE;
   end if;

   if L_seq_no is NOT NULL then
      if FM_EXIT_NF_CREATION_SQL.PROCESS(O_error_message,
                                         L_status_code,
                                         'TSF',
                                         L_seq_no) = FALSE then
         O_status_code := 'E';
         return FALSE;
      end if;
   end if;

   O_status_code := 'S';
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status_code := 'E';
      return FALSE;
END CONSUME_SHIPMENT;
----------------------------------------------------------------------------------
FUNCTION CONSUME_INVADJUST(O_status_code     IN OUT   VARCHAR2,
                           O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       RIB_OBJECT,
                           I_message_type    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'FM_INTEGRATION_SQL.CONSUME_INVADJUST';
   L_seq_no         FM_STG_ADJUST_DESC.SEQ_NO%TYPE := NULL;
   L_status_code    INTEGER(1);

BEGIN

   if FM_INVADJUST_SQL.CONSUME(O_error_message,
                               L_seq_no,
                               I_message,
                               I_message_type) = FALSE then
      O_status_code := 'E';
      return FALSE;
   end if;

   if L_seq_no is NOT NULL then
      if FM_EXIT_NF_CREATION_SQL.PROCESS(O_error_message,
                                         L_status_code,
                                         'STOCK',
                                         L_seq_no) = FALSE then
         O_status_code := 'E';
         return FALSE;
      end if;
   end if;

   O_status_code := 'S';
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status_code := 'E';
      return FALSE;
END CONSUME_INVADJUST;
----------------------------------------------------------------------------------
FUNCTION CONSUME_RECEIPT(O_status_code     IN OUT   VARCHAR2,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_OBJECT",
                         I_message_type    IN       VARCHAR2)

RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'FM_INTEGRATION_SQL.CONSUME_RECEIPT';
   L_schedule_no   FM_SCHEDULE.SCHEDULE_NO%TYPE := 0;

BEGIN

   if lower(I_message_type) = RECEIPT_ADD then
      if FM_SUB_RECEIPT_SQL.CONSUME(O_error_message,
                                    L_schedule_no,
                                    I_message,
                                    I_message_type) = FALSE then
         O_status_code := 'E';
         return FALSE;
      end if;
   end if;

   if FM_POST_RECEIVING_SQL.PROCESS(O_error_message,
                                    L_schedule_no) = FALSE then
      O_status_code := 'E';
      return FALSE;
   end if;

   O_status_code := 'S';
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_status_code := 'E';
      return FALSE;
END CONSUME_RECEIPT;
----------------------------------------------------------------------------------------
FUNCTION CONSUME_WASTE_ADJ(O_status_code     IN OUT   VARCHAR2,
                           O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN OUT   FM_STG_ADJUST_DTL%ROWTYPE,
                           I_location        IN       FM_STG_ADJUST_DESC.DC_DEST_ID%TYPE,
                           I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'FM_INTEGRATION_SQL.CONSUME_WASTE_ADJ';
   L_seq_no        FM_STG_ADJUST_DESC.SEQ_NO%TYPE := NULL;
   L_status_code   INTEGER(1);

BEGIN

   if FM_WASTE_ADJ_SQL.CONSUME(O_error_message,
                               L_seq_no,
                               I_message,
                               I_location,
                               I_inv_status) = FALSE then
      O_status_code := 'E';
      return FALSE;
   end if;

   if L_seq_no is NOT NULL then
      if FM_EXIT_NF_CREATION_SQL.PROCESS(O_error_message,
                                         L_status_code,
                                         'STOCK',
                                         L_seq_no,
                                         'Y') = FALSE then    --I_wasteadj_ind
         O_status_code := 'E';
         return FALSE;
      end if;
   end if;

   O_status_code := 'S';
   return FALSE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
     O_status_code := 'E';
     return FALSE;
END CONSUME_WASTE_ADJ;
----------------------------------------------------------------------------------------
END FM_INTEGRATION_SQL;
/