create or replace PACKAGE BODY FM_CALC_TOTALS_SQL is
----------------------------------------------------------------------------------
FUNCTION POPULATE_TAX_DETAIL (O_error_message  IN OUT VARCHAR2,
                              O_status         IN OUT INTEGER,
                              I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
 return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_CALC_TOTALS_SQL.POPULATE_TAX_DETAIL';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   ---
   L_fiscal_doc_id_ref         FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;
   L_fiscal_doc_line_id_ref    FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_fiscal_doc_line_id        FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE;
   L_RTV_quantity              FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_PO_quantity               FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_total_tax_value           FM_FISCAL_DOC_TAX_DETAIL.TOTAL_VALUE%TYPE;
   L_create_datetime           FM_FISCAL_DOC_HEADER.CREATE_DATETIME%TYPE := SYSDATE;
   L_create_id                 FM_FISCAL_DOC_HEADER.CREATE_ID%TYPE := USER;
   L_legal_msg                 FM_FISCAL_DOC_TAX_DETAIL.LEGAL_MESSAGE_TEXT%TYPE := NULL;
   L_nic_ind                   FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE;
   L_req_type                  FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE;
   L_line_qty                  FM_FISCAL_DOC_DETAIL.QUANTITY%TYPE;
   L_line_id_ref               FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID_REF%TYPE := 0;
   L_error_message             VARCHAR2(200);
   L_triang_ind                VARCHAR2(1) := 'N';
   L_cursor       VARCHAR2(100);
   L_table        VARCHAR2(100);
   L_key          VARCHAR2(100);
   L_count        NUMBER := 0;
   L_rma          CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'RMA';
   L_po           CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'PO';
   L_rep          CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'REP';
   L_tsf          CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'TSF';
   L_ic           CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'IC';
   L_rtv          CONSTANT FM_FISCAL_DOC_HEADER.REQUISITION_TYPE%TYPE := 'RTV';
   L_upd_ind      VARCHAR2(1)                                         := 'N';

   cursor C_GET_DOC_DETAIL is
      select fdd.fiscal_doc_id_ref,
             fdd.fiscal_doc_line_id_ref,
             fdd.fiscal_doc_line_id,
             fdd.item,
             fdd.quantity
        from fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdh.fiscal_doc_id = I_fiscal_doc_id
    order by fdd.fiscal_doc_line_id_ref;

   R_get_doc_detail    C_GET_DOC_DETAIL%ROWTYPE;

   TYPE T_fiscal_doc_tax_dtl_tbl IS TABLE OF FM_FISCAL_DOC_TAX_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_fiscal_doc_tax_dtl_tab   T_fiscal_doc_tax_dtl_tbl;

   j BINARY_INTEGER :=0;

   cursor C_GET_LINE_QTY (P_fiscal_doc_line_id     IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdd.quantity
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id      = I_fiscal_doc_id
         and fdd.fiscal_doc_line_id = P_fiscal_doc_line_id;

   cursor C_GET_REQ_TYPE is
      select fdh.requisition_type
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id;

   cursor C_EXT_NF_COUNT is
      select count(1)
        from fm_fiscal_doc_header fdh
       where fdh.module = 'PTNR'
         and fdh.key_value_2 = 'E'
         and fdh.fiscal_doc_id = I_fiscal_doc_id;

   cursor C_GET_PO_TAX_DTL(P_fiscal_doc_line_id_ref IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE,
                           P_fiscal_doc_line_id     IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdtw.tax_code as vat_code,
             fdtw.tax_rate as tax_rate,
             (fdtw.tax_basis / fdtw.rcvd_qty) * fdd.quantity as unit_tax_basis,
             (fdtw.modified_tax_basis / fdtw.rcvd_qty) * fdd.quantity as modified_tax_basis,
             fdtw.unit_rec_value,
             fdtw.unit_tax_value,
             'P' as tax_base_ind
        from fm_fiscal_doc_tax_detail_wac fdtw,
             fm_fiscal_doc_detail fdd
       where fdtw.fiscal_doc_line_id     = P_fiscal_doc_line_id_ref
         and fdd.fiscal_doc_line_id      = P_fiscal_doc_line_id
         and fdd.fiscal_doc_line_id_ref  = P_fiscal_doc_line_id_ref
      UNION
      select fdtw.tax_code as vat_code,
             fdtw.tax_rate as tax_rate,
             (fdtw.tax_basis / fdtw.rcvd_qty) * fdd.quantity as unit_tax_basis,
             (fdtw.modified_tax_basis / fdtw.rcvd_qty) * fdd.quantity as modified_tax_basis,
             fdtw.unit_rec_value,
             fdtw.unit_tax_value,
             'P' as tax_base_ind
        from fm_fiscal_doc_tax_detail_wac fdtw,
             fm_fiscal_doc_detail fdd
       where fdtw.fiscal_doc_line_id     in (select fdd.fiscal_doc_line_id
                                                    from fm_fiscal_doc_header fdh,
                                                         fm_fiscal_doc_detail fdd
                                                   where fdh.fiscal_doc_id          = fdd.fiscal_doc_id
                                                     and fdh.requisition_type       = L_po
                                                     and fdd.fiscal_doc_line_id_ref = P_fiscal_doc_line_id_ref)
         and fdd.fiscal_doc_line_id      = P_fiscal_doc_line_id
         and fdd.fiscal_doc_line_id_ref  = P_fiscal_doc_line_id_ref;

   TYPE T_po_tax_dtl_tab IS TABLE OF C_GET_PO_TAX_DTL%ROWTYPE
   INDEX BY BINARY_INTEGER;
   L_po_tax_dtl_tab T_po_tax_dtl_tab;

   i BINARY_INTEGER :=0;

   cursor C_GET_SYSTEM_TAX_DTL(P_fiscal_doc_line_id IN FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select fdte.tax_code,
             fdte.tax_rate,
             fdte.legal_msg,
             fdte.tax_basis,
             fdte.modified_tax_basis,             
             decode(fdte.tax_rate_type,'P','P','A','V') as tax_base_ind,
             fdte.tax_value,
             fdte.tax_rec_value,
             fdte.unit_tax_amt,
             fdte.unit_rec_value
        from fm_fiscal_doc_tax_detail_ext fdte
       where fdte.fiscal_doc_id      = I_fiscal_doc_id
         and fdte.fiscal_doc_line_id = P_fiscal_doc_line_id;

   TYPE T_system_tax_dtl_tab IS TABLE OF C_GET_SYSTEM_TAX_DTL%ROWTYPE
   INDEX BY BINARY_INTEGER;
   L_system_tax_dtl_tab T_system_tax_dtl_tab;

   k BINARY_INTEGER :=0;

   cursor C_GET_NIC_IND(P_vat_code IN FM_FISCAL_DOC_TAX_DETAIL.VAT_CODE%TYPE) is
      select incl_nic_ind
        from vat_codes
       where vat_code = P_vat_code;

   cursor C_GET_LINE_ID_REF (P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 1
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_line_id = P_fiscal_doc_line_id
         and fdd.fiscal_doc_id      = I_fiscal_doc_id
         and fdd.fiscal_doc_line_id_ref is not null;

   cursor C_LOCK_DOC_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_line_id = P_fiscal_doc_line_id
         for update nowait;

   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;

   BEGIN
   O_status := 1;
   ---
   if FM_ERROR_LOG_SQL.CLEAR_ERROR(O_error_message,
                                   I_fiscal_doc_id,
                                   L_program) = FALSE then
      return FALSE;
   end if;

   if I_fiscal_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fiscal_doc_id',
                                             L_program,
                                             NULL);
      O_status := 0;
      return FALSE;
   end if;

   OPEN C_GET_REQ_TYPE;
   FETCH C_GET_REQ_TYPE into L_req_type;
   CLOSE C_GET_REQ_TYPE;

   open C_EXT_NF_COUNT;
   fetch C_EXT_NF_COUNT into L_count;
   close C_EXT_NF_COUNT;

   open C_GET_DOC_DETAIL;
   loop
     fetch C_GET_DOC_DETAIL into R_get_doc_detail;
      exit when C_GET_DOC_DETAIL%NOTFOUND;

          if R_get_doc_detail.fiscal_doc_id_ref is not null and L_req_type <> L_rma then
            open C_GET_PO_TAX_DTL(R_get_doc_detail.fiscal_doc_line_id_ref, R_get_doc_detail.fiscal_doc_line_id);
            loop
            fetch C_GET_PO_TAX_DTL into L_po_tax_dtl_tab(i);
            exit when C_GET_PO_TAX_DTL%NOTFOUND;
            i := i + 1;
            end loop;
               if L_po_tax_dtl_tab.count > 0 then
               for i in L_po_tax_dtl_tab.first..L_po_tax_dtl_tab.last
                  loop
                    j := j+1;
                    L_fiscal_doc_tax_dtl_tab(j).fiscal_doc_line_id := R_get_doc_detail.fiscal_doc_line_id;
                    L_fiscal_doc_tax_dtl_tab(j).vat_code           := L_po_tax_dtl_tab(i).vat_code;
                    L_fiscal_doc_tax_dtl_tab(j).percentage_rate    := L_po_tax_dtl_tab(i).tax_rate;
                    L_fiscal_doc_tax_dtl_tab(j).unit_tax_amt       := L_po_tax_dtl_tab(i).unit_tax_value;
                    L_fiscal_doc_tax_dtl_tab(j).total_value        := (R_get_doc_detail.quantity) * (L_po_tax_dtl_tab(i).unit_tax_value);
                    L_fiscal_doc_tax_dtl_tab(j).tax_basis          := L_po_tax_dtl_tab(i).unit_tax_basis;
                    L_fiscal_doc_tax_dtl_tab(j).modified_tax_basis := L_po_tax_dtl_tab(i).modified_tax_basis;                    
                    L_fiscal_doc_tax_dtl_tab(j).tax_base_ind       := L_po_tax_dtl_tab(i).tax_base_ind;
                    L_fiscal_doc_tax_dtl_tab(j).unit_rec_value     := L_po_tax_dtl_tab(i).unit_rec_value;
                    L_fiscal_doc_tax_dtl_tab(j).rec_value          := (R_get_doc_detail.quantity) * (L_po_tax_dtl_tab(i).unit_rec_value);
                    L_fiscal_doc_tax_dtl_tab(j).legal_message_text := L_legal_msg;
                 end loop;
                 end if;
            close C_GET_PO_TAX_DTL;
            i := 0;
            L_po_tax_dtl_tab.DELETE;
          else
             open C_GET_SYSTEM_TAX_DTL(R_get_doc_detail.fiscal_doc_line_id);
               loop
               fetch C_GET_SYSTEM_TAX_DTL into L_system_tax_dtl_tab(k);
               exit when C_GET_SYSTEM_TAX_DTL%NOTFOUND;
               k := k + 1;
               end loop;
                if L_system_tax_dtl_tab.count > 0 then
                for k in L_system_tax_dtl_tab.first..L_system_tax_dtl_tab.last
                  loop
                    j:=j+1;
                    L_fiscal_doc_tax_dtl_tab(j).fiscal_doc_line_id := R_get_doc_detail.fiscal_doc_line_id;
                    L_fiscal_doc_tax_dtl_tab(j).vat_code           := L_system_tax_dtl_tab(k).tax_code;
                    L_fiscal_doc_tax_dtl_tab(j).percentage_rate    := L_system_tax_dtl_tab(k).tax_rate;
                    L_fiscal_doc_tax_dtl_tab(j).unit_tax_amt       := L_system_tax_dtl_tab(k).unit_tax_amt;
                    L_fiscal_doc_tax_dtl_tab(j).total_value        := L_system_tax_dtl_tab(k).tax_value;
                    L_fiscal_doc_tax_dtl_tab(j).tax_basis          := L_system_tax_dtl_tab(k).tax_basis;
                    L_fiscal_doc_tax_dtl_tab(j).modified_tax_basis := L_system_tax_dtl_tab(k).modified_tax_basis;                    
                    L_fiscal_doc_tax_dtl_tab(j).tax_base_ind       := L_system_tax_dtl_tab(k).tax_base_ind;
                    L_fiscal_doc_tax_dtl_tab(j).unit_rec_value     := L_system_tax_dtl_tab(k).unit_rec_value;
                    L_fiscal_doc_tax_dtl_tab(j).rec_value          := L_system_tax_dtl_tab(k).tax_rec_value;
                    L_fiscal_doc_tax_dtl_tab(j).legal_message_text := L_system_tax_dtl_tab(k).legal_msg;
                 end loop;
                end if;
             close C_GET_SYSTEM_TAX_DTL;
             k := 0;
             L_system_tax_dtl_tab.DELETE;
          end if;
       end loop;
   close C_GET_DOC_DETAIL;
            if L_fiscal_doc_tax_dtl_tab.count > 0 then
               for j in L_fiscal_doc_tax_dtl_tab.first..L_fiscal_doc_tax_dtl_tab.last
               loop
                ---
                L_line_id_ref := 0;
                open C_GET_LINE_ID_REF (L_fiscal_doc_tax_dtl_tab(j).fiscal_doc_line_id);
                fetch C_GET_LINE_ID_REF into L_line_id_ref;
                close C_GET_LINE_ID_REF;
                ---
                open C_GET_NIC_IND(L_fiscal_doc_tax_dtl_tab(j).vat_code);
               fetch C_GET_NIC_IND into L_nic_ind;
               if L_nic_ind='Y' or L_line_id_ref =0 then
                 insert into fm_fiscal_doc_tax_detail(vat_code,
                                                       fiscal_doc_line_id,
                                                       percentage_rate,
                                                       total_value,
                                                       unit_tax_amt,
                                                       unit_rec_value,
                                                       tax_basis,
                                                       modified_tax_basis,
                                                       tax_base_ind,
                                                       rec_value,
                                                       create_datetime,
                                                       create_id,
                                                       last_update_datetime,
                                                       last_update_id,
                                                       legal_message_text)
                                                values(L_fiscal_doc_tax_dtl_tab(j).vat_code,
                                                       L_fiscal_doc_tax_dtl_tab(j).fiscal_doc_line_id,
                                                       L_fiscal_doc_tax_dtl_tab(j).percentage_rate,
                                                       L_fiscal_doc_tax_dtl_tab(j).total_value,
                                                       L_fiscal_doc_tax_dtl_tab(j).unit_tax_amt,
                                                       L_fiscal_doc_tax_dtl_tab(j).unit_rec_value,
                                                       L_fiscal_doc_tax_dtl_tab(j).tax_basis,
                                                       L_fiscal_doc_tax_dtl_tab(j).modified_tax_basis,                                                       
                                                       L_fiscal_doc_tax_dtl_tab(j).tax_base_ind,
                                                       L_fiscal_doc_tax_dtl_tab(j).rec_value,
                                                       L_create_datetime,
                                                       L_create_id,
                                                       L_create_datetime,
                                                       L_create_id,
                                                       L_fiscal_doc_tax_dtl_tab(j).legal_message_text);
               else
                ---
                   L_cursor := 'C_LOCK_DOC_DETAIL';
                   L_table  := 'fm_fiscal_doc_detail';
                   L_key    := 'Fiscal_doc_line_id: '|| L_fiscal_doc_tax_dtl_tab(j).fiscal_doc_line_id;
                   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
                   open C_LOCK_DOC_DETAIL(L_fiscal_doc_tax_dtl_tab(j).fiscal_doc_line_id);
                   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
                   close C_LOCK_DOC_DETAIL;
                   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
                  ---
                  update fm_fiscal_doc_detail fdd
                     set fdd.unit_cost           = fdd.unit_cost + L_fiscal_doc_tax_dtl_tab(j).unit_tax_amt,
                         fdd.unit_cost_with_disc = fdd.unit_cost_with_disc + L_fiscal_doc_tax_dtl_tab(j).unit_tax_amt
                   where fdd.fiscal_doc_id       = I_fiscal_doc_id
                     and fdd.fiscal_doc_line_id  = L_fiscal_doc_tax_dtl_tab(j).fiscal_doc_line_id;
                  ---
                  open C_GET_LINE_QTY (L_fiscal_doc_tax_dtl_tab(j).fiscal_doc_line_id);
                  fetch C_GET_LINE_QTY into L_line_qty;
                  close C_GET_LINE_QTY;
                  ---
                   L_cursor := 'C_LOCK_DOC_HEADER';
                   L_table  := 'fm_fiscal_doc_header';
                   L_key    := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
                   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
                   open C_LOCK_DOC_HEADER;
                   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
                  close C_LOCK_DOC_HEADER;
                  ---
                  SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
                  ---
                  update fm_fiscal_doc_header fdh
                     set fdh.total_item_calc_value = fdh.total_item_calc_value + (L_fiscal_doc_tax_dtl_tab(j).unit_tax_amt * L_line_qty),
                         fdh.total_doc_calc_value  = fdh.total_doc_calc_value + (L_fiscal_doc_tax_dtl_tab(j).unit_tax_amt * L_line_qty)
                   where fdh.fiscal_doc_id         = I_fiscal_doc_id;

                  L_upd_ind := 'Y';
               ---
               end if;
               close C_GET_NIC_IND;
               end loop;
            else
               if L_req_type <> L_rep then
                  if L_req_type in (L_tsf,L_ic) and L_count = 0 then
                     O_status := 0;
                     if FM_ERROR_LOG_SQL.WRITE_ERROR(O_error_message,
                                                     I_fiscal_doc_id,
                                                     L_program,
                                                     L_program,
                                                     'E',
                                                     'NO TAXES FOUND',
                                                     O_error_message,
                                                    'Fiscal_doc_line_id: '||TO_CHAR(R_get_doc_detail.fiscal_doc_line_id)) = FALSE then
                        return FALSE;
                     end if;
                  end if;
               end if;
            end if;
            ---
            if L_req_type = L_rtv and L_upd_ind = 'Y' then

               L_table  := 'fm_fiscal_doc_header';
               L_key    := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);

               SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
               ---
               update fm_fiscal_doc_header fdh
                  set fdh.total_doc_calc_value  = nvl(fdh.total_item_calc_value,0) + nvl(fdh.total_serv_calc_value,0)
                where fdh.fiscal_doc_id         = I_fiscal_doc_id;

            end if;

   return true;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'FM_FISCAL_DOC_HEADER',
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_LOCK_DOC_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_HEADER', 'FM_FISCAL_DOC_HEADER', 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id));
         close C_LOCK_DOC_HEADER;
      end if;
      if C_LOCK_DOC_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DOC_DETAIL', 'FM_FISCAL_DOC_DETAIL', 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id));
         close C_LOCK_DOC_HEADER;
      end if;
      ---
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      O_status := 0;
---
   if C_GET_DOC_DETAIL%ISOPEN then
      close C_GET_DOC_DETAIL;
   end if;
---
   if C_GET_PO_TAX_DTL%ISOPEN then
      close C_GET_PO_TAX_DTL;
   end if;
---
   if C_GET_SYSTEM_TAX_DTL%ISOPEN then
      close C_GET_PO_TAX_DTL;
   end if;
   O_status := 0;
  return FALSE;

END POPULATE_TAX_DETAIL;
----------------------------------------------------------------------------------
FUNCTION POPULATE_TAX_HEAD(O_error_message  IN OUT VARCHAR2,
                           O_status         IN OUT INTEGER,
                           I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
 return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_CALC_TOTALS_SQL.POPULATE_TAX_HEAD';

   L_create_datetime  FM_FISCAL_DOC_HEADER.CREATE_DATETIME%TYPE := SYSDATE;
   L_create_id        FM_FISCAL_DOC_HEADER.CREATE_ID%TYPE := USER;


   cursor C_GET_TAX_HEAD is
      select fdtd.vat_code,
             SUM(fdtd.total_value) as total_value,
             SUM(fdtd.tax_basis) as tax_basis,
             SUM(fdtd.modified_tax_basis) as modified_tax_basis             
        from fm_fiscal_doc_tax_detail fdtd,
             fm_fiscal_doc_header fdh,
             fm_fiscal_doc_detail fdd
       where fdh.fiscal_doc_id      = I_fiscal_doc_id
         and fdd.fiscal_doc_id      = fdh.fiscal_doc_id
         and fdd.fiscal_doc_line_id = fdtd.fiscal_doc_line_id
    group by fdtd.vat_code;

   R_get_tax_head    C_GET_TAX_HEAD%ROWTYPE;

   begin
   O_status := 1;
      open C_GET_TAX_HEAD;
      loop
         fetch C_GET_TAX_HEAD into R_get_tax_head;
         exit when C_GET_TAX_HEAD%NOTFOUND;

         insert into fm_fiscal_doc_tax_head(vat_code,
                                            fiscal_doc_id,
                                            total_value,
                                            tax_basis,
                                            modified_tax_basis,                                            
                                            create_datetime,
                                            create_id,
                                            last_update_datetime,
                                            last_update_id)
                                     values(R_get_tax_head.vat_code,
                                            I_fiscal_doc_id,
                                            R_get_tax_head.total_value,
                                            R_get_tax_head.tax_basis,
                                            R_get_tax_head.modified_tax_basis,                                            
                                            L_create_datetime,
                                            L_create_id,
                                            L_create_datetime,
                                            L_create_id);

      end loop;
      close C_GET_TAX_HEAD;

    return true;

EXCEPTION
   when OTHERS then

     if C_GET_TAX_HEAD%ISOPEN then
      close C_GET_TAX_HEAD;
     end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      O_status := 0;
      return FALSE;
---
END POPULATE_TAX_HEAD;
----------------------------------------------------------------------------------
FUNCTION UPDATE_TOTALS(O_error_message  IN OUT VARCHAR2,
                       O_status         IN OUT INTEGER,
                       I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
 return BOOLEAN is
   ---
   L_program   VARCHAR2(100) := 'FM_CALC_TOTALS_SQL.UPDATE_TOTALS';

   C_ipi_code               FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE :='IPI';
   C_icmsst_code            FM_FISCAL_DOC_TAX_HEAD_EXT.TAX_CODE%TYPE  :='ICMSST';

   L_create_datetime        FM_FISCAL_DOC_HEADER.CREATE_DATETIME%TYPE := SYSDATE;
   L_create_id              FM_FISCAL_DOC_HEADER.CREATE_ID%TYPE := USER;
   L_total_serv_calc_cost   FM_FISCAL_DOC_HEADER.TOTAL_SERV_CALC_VALUE%TYPE :=0;
   L_total_item_calc_cost   FM_FISCAL_DOC_HEADER.TOTAL_ITEM_CALC_VALUE%TYPE :=0;
   L_total_doc_extra_costs  FM_FISCAL_DOC_HEADER.EXTRA_COSTS_CALC%TYPE :=0;
   L_unit_cost_with_disc    FM_FISCAL_DOC_DETAIL.UNIT_COST_WITH_DISC%TYPE :=0;
   L_total_calc_cost        FM_FISCAL_DOC_DETAIL.TOTAL_CALC_COST%TYPE :=0;
   L_total_ipi_cost_calc    FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE :=0;
   L_total_icmsst_cost_calc FM_FISCAL_DOC_TAX_HEAD.TOTAL_VALUE%TYPE :=0;
   L_total_doc_cost         FM_FISCAL_DOC_HEADER.TOTAL_DOC_CALC_VALUE%TYPE :=0;
   L_total_cost             FM_FISCAL_DOC_DETAIL.TOTAL_COST%TYPE :=0;
   L_rural_prod_ind         V_BR_SUPS_FISCAL_CLASS.RURAL_PROD_IND%TYPE := 'N';
   L_total_expenses         FM_FISCAL_DOC_HEADER.FREIGHT_COST%TYPE := 0;
   L_total_cost_calc_disc   FM_FISCAL_DOC_HEADER.TOTAL_DOC_VALUE_WITH_DISC%TYPE;
   L_cursor                 VARCHAR2(100);
   L_table                  VARCHAR2(100);
   L_key                    VARCHAR2(100);
   L_pack_ind_n               VARCHAR2(1)  := 'N';
   L_pack_ind_y               VARCHAR2(1)  := 'Y';
   L_requisition_type         VARCHAR2(10) := 'RPO';
   L_module                   VARCHAR2(10) := 'SUPP';

   L_total_discount_value   FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;
   L_final_total_discount_value FM_FISCAL_DOC_HEADER.TOTAL_DISCOUNT_VALUE%TYPE;

   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_TOTAL_SERV_CALC_VALUE is
      select NVL(DECODE(SUM(fdd.total_calc_cost), 0, 0,
                                        (SUM(DECODE(via.service_ind, 'Y', fdd.total_calc_cost, 0)))),0)
        from fm_fiscal_doc_detail fdd,
             v_br_item_fiscal_attrib via
       where via.item          = fdd.item
         and fdd.fiscal_doc_id = I_fiscal_doc_id
         and fdd.pack_ind      = L_pack_ind_n;

   cursor C_TOTAL_ITEM_CALC_VALUE is
      select DECODE(SUM(merch_total.item_total),0,0,(SUM(merch_total.item_total)))
        from
             (select NVL(DECODE(SUM(fdd.total_calc_cost), 0, 0, (SUM(DECODE(via.service_ind, 'N', fdd.total_calc_cost, 0)))),0) item_total
                from fm_fiscal_doc_detail fdd,
                     v_br_item_fiscal_attrib via
               where via.item          = fdd.item
                 and fdd.fiscal_doc_id = I_fiscal_doc_id
                 and fdd.pack_ind      = L_pack_ind_n
              union all
              select NVL(DECODE(SUM(fdd.total_calc_cost), 0, 0, ( SUM(fdd.total_calc_cost))),0) item_total
                from fm_fiscal_doc_detail fdd
               where fdd.fiscal_doc_id = I_fiscal_doc_id
                 and fdd.pack_ind      = L_pack_ind_y
             ) merch_total;

   cursor C_TOTAL_IPI_COST_CALC is
      select NVL(fdth.total_value,0)
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id
         and fdth.vat_code      = C_ipi_code;

     cursor C_TOTAL_ICMSST_COST_CALC is
      select NVL(fdth.total_value,0)
        from fm_fiscal_doc_tax_head fdth
       where fdth.fiscal_doc_id = I_fiscal_doc_id and
             fdth.vat_code      = C_icmsst_code;

   cursor C_GET_DOC_DETAIL is
      select fdd.fiscal_doc_line_id,
             fdd.unit_cost,
             fdd.quantity,
             fdd.fiscal_doc_line_id_ref
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id=I_fiscal_doc_id;

   R_get_doc_detail C_GET_DOC_DETAIL%ROWTYPE;

   cursor C_LOCK_DOC_HEADER is
      select 'X'
        from fm_fiscal_doc_header fdh
       where fdh.fiscal_doc_id = I_fiscal_doc_id
         for update nowait;

   cursor C_LOCK_DOC_DETAIL(P_fiscal_doc_line_id FM_FISCAL_DOC_DETAIL.FISCAL_DOC_LINE_ID%TYPE) is
      select 'X'
        from fm_fiscal_doc_detail fdd
       where fdd.fiscal_doc_id = P_fiscal_doc_line_id
         for update nowait;

   cursor C_CHK_RURAL_PROD_IND is
      select sfc.rural_prod_ind
       from  v_br_sups_fiscal_class sfc,
             fm_fiscal_doc_header fdh
      where fdh.fiscal_doc_id    = I_fiscal_doc_id
        and fdh.requisition_type = L_requisition_type
        and fdh.module           = L_module
        and fdh.key_value_1      = sfc.supplier;

   cursor C_GET_COST_COMPONENTS is
      select sum(fdd.freight_cost * fdd.quantity ) freight_cost
            ,sum(fdd.insurance_cost * fdd.quantity) insurance_cost
            ,sum(fdd.other_expenses_cost * fdd.quantity) other_expenses_cost
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id = fdh.fiscal_doc_id
         and fdd.fiscal_doc_id = I_fiscal_doc_id
    group by fdd.fiscal_doc_id;

   R_get_cost_components C_GET_COST_COMPONENTS%ROWTYPE;

   cursor C_TOTAL_HEADER_DISCOUNT is
      select SUM( NVL(fdd.unit_header_disc,0) * NVL(fdd.quantity,0) ) final_total_discount_value,
             decode(fdh.discount_type,'P',fdh.total_discount_value,SUM( NVL(fdd.UNIT_HEADER_DISC,0) * NVL(fdd.QUANTITY,0) ) ) total_discount_value
        from fm_fiscal_doc_detail fdd,
             fm_fiscal_doc_header fdh
       where fdd.fiscal_doc_id          = fdh.fiscal_doc_id
         and fdd.fiscal_doc_id = I_fiscal_doc_id
    GROUP BY fdd.fiscal_doc_id,
             fdh.total_discount_value,
             fdh.discount_type;
   BEGIN
   O_status := 1;
    open C_CHK_RURAL_PROD_IND;
   fetch C_CHK_RURAL_PROD_IND into L_rural_prod_ind;
   close C_CHK_RURAL_PROD_IND;
   if L_rural_prod_ind <> 'Y' then
      open C_GET_DOC_DETAIL;
         loop
            fetch C_GET_DOC_DETAIL into R_get_doc_detail;
            exit when C_GET_DOC_DETAIL%NOTFOUND;
               if R_get_doc_detail.fiscal_doc_line_id_ref is not null then
                  L_total_calc_cost := (R_get_doc_detail.unit_cost)*(R_get_doc_detail.quantity);
                  L_total_cost      := L_total_calc_cost;

                  L_cursor := 'C_LOCK_DOC_DETAIL';
                  L_table  := 'FM_FISCAL_DOC_DETAIL';
                  L_key    := 'Fiscal_doc_line_id: ' || TO_CHAR(R_get_doc_detail.fiscal_doc_line_id);
   ---
                  SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
                  open C_LOCK_DOC_DETAIL(R_get_doc_detail.fiscal_doc_line_id);
                  SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
                  close C_LOCK_DOC_DETAIL;
   ---
                  SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);

                  update fm_fiscal_doc_detail
                     set total_calc_cost     = L_total_calc_cost
                        ,total_cost          = L_total_cost
                  where fiscal_doc_line_id   = R_get_doc_detail.fiscal_doc_line_id;
               end if;
         end loop;
      close C_GET_DOC_DETAIL;
   end if;
     ---
   if L_rural_prod_ind = 'Y' then
      open C_TOTAL_SERV_CALC_VALUE;
     fetch C_TOTAL_SERV_CALC_VALUE into L_total_serv_calc_cost;
     close C_TOTAL_SERV_CALC_VALUE;
     ---
      open C_TOTAL_ITEM_CALC_VALUE;
     fetch C_TOTAL_ITEM_CALC_VALUE into L_total_item_calc_cost;
     close C_TOTAL_ITEM_CALC_VALUE;
     ---
      open C_TOTAL_IPI_COST_CALC;
     fetch C_TOTAL_IPI_COST_CALC into L_total_ipi_cost_calc;
     close C_TOTAL_IPI_COST_CALC;
     ---
      open C_TOTAL_ICMSST_COST_CALC;
     fetch C_TOTAL_ICMSST_COST_CALC into L_total_icmsst_cost_calc;
     close C_TOTAL_ICMSST_COST_CALC;
     ---
      open C_GET_COST_COMPONENTS;
     fetch C_GET_COST_COMPONENTS into R_get_cost_components;
     close C_GET_COST_COMPONENTS;
     ---
     open C_TOTAL_HEADER_DISCOUNT;
     fetch C_TOTAL_HEADER_DISCOUNT into L_final_total_discount_value,L_total_discount_value;
     close C_TOTAL_HEADER_DISCOUNT;

     L_total_expenses        := NVL(R_get_cost_components.freight_cost,0) + NVL(R_get_cost_components.insurance_cost,0) + NVL(R_get_cost_components.other_expenses_cost,0);
     L_total_doc_extra_costs := L_total_expenses + NVL(L_total_ipi_cost_calc,0) + NVL(L_total_icmsst_cost_calc,0);
     L_total_doc_cost        := NVL(L_total_serv_calc_cost,0) + NVL(L_total_item_calc_cost,0) + NVL(L_total_doc_extra_costs,0) - NVL(L_final_total_discount_value, 0);
     L_total_cost_calc_disc  := NVL(L_total_item_calc_cost,0) + NVL(L_total_serv_calc_cost,0) - NVL(L_final_total_discount_value, 0);

     L_cursor := 'C_LOCK_DOC_HEADER';
     L_table  := 'FM_FISCAL_DOC_HEADER';
     L_key    := 'Fiscal_doc_id: ' || TO_CHAR(I_fiscal_doc_id);
     ---
     SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
     open C_LOCK_DOC_HEADER;
     SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
     close C_LOCK_DOC_HEADER;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, L_table, L_key);
      ---
      update fm_fiscal_doc_header
         set freight_cost               = NVL(R_get_cost_components.freight_cost,0)
            ,insurance_cost             = NVL(R_get_cost_components.insurance_cost,0)
            ,other_expenses_cost        = NVL(R_get_cost_components.other_expenses_cost,0)
            ,total_serv_calc_value      = L_total_serv_calc_cost
            ,total_item_calc_value      = L_total_item_calc_cost
            ,extra_costs_calc           = L_total_doc_extra_costs
            ,total_doc_calc_value       = L_total_doc_cost
            ,total_doc_value_with_disc  = L_total_cost_calc_disc
            ,total_discount_value       = NVL(L_total_discount_value,0)
       where fiscal_doc_id              = I_fiscal_doc_id;
   end if;
    return TRUE;

EXCEPTION

  when RECORD_LOCKED then
     O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                           'C_LOCK_DOC_HEADER',
                                            L_key,
                                            TO_CHAR(SQLCODE));
      --
      if C_LOCK_DOC_HEADER%ISOPEN then
         close C_LOCK_DOC_HEADER;
      end if;
     O_status := 0;
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
     O_status := 0;
    return FALSE;

END UPDATE_TOTALS;
----------------------------------------------------------------------------------
FUNCTION POPULATE_VALUES(O_error_message  IN OUT VARCHAR2,
                         O_status         IN OUT INTEGER,
                         I_fiscal_doc_id  IN     FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE,
                         I_form_ind       IN     VARCHAR2)
 return BOOLEAN is

    L_program  VARCHAR2(100) := 'FM_CALC_TOTALS_SQL.POPULATE_VALUES';
    O_line_tax "RIB_TaxDetRBO_TBL"        :=  "RIB_TaxDetRBO_TBL"();

  begin

      O_status := 1;

      if I_fiscal_doc_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_fiscal_doc_id',
                                               L_program,
                                               NULL);
         O_status := 0;
         return FALSE;
      end if;
     if FM_EXT_TAXES_SQL.GET_EXT_TAX_VALUES(O_line_tax,O_error_message,O_status,I_fiscal_doc_id,null,null,null,I_form_ind)=FALSE THEN
        return (FALSE);
     end if;
     if O_status != 0 then
        if (POPULATE_TAX_DETAIL (O_error_message,
                                 O_status,
                                 I_fiscal_doc_id)= FALSE) then
           return (FALSE);
        end if;
        if O_status != 0 then
            if (POPULATE_TAX_HEAD (O_error_message,
                                   O_status,
                                   I_fiscal_doc_id)= FALSE) then
               return (FALSE);
            end if;
         end if;

        if (O_status = 0) then
           O_error_message := SQL_LIB.CREATE_MSG('NO TAXES FOUND',L_program,NULL,NULL);
        end if;
     end if;

    return true;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      O_status := 0;
      return FALSE;

END POPULATE_VALUES;
----------------------------------------------------------------------------------
END FM_CALC_TOTALS_SQL;
/