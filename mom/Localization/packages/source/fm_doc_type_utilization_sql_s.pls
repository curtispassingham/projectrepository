CREATE OR REPLACE PACKAGE FM_DOC_TYPE_UTILIZATION_SQL AS
---------------------------------------------------------------------------------
-- Function Name: EXISTS_UTILIZATION
-- Purpose:       This function validates the type document and utilization.
----------------------------------------------------------------------------------
FUNCTION EXISTS_UTILIZATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         IN OUT BOOLEAN,
                            I_type_id        IN     FM_DOC_TYPE_UTILIZATION.TYPE_ID%TYPE,
                            I_utilization_id IN     FM_DOC_TYPE_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: DELETE_DOC
-- Purpose:       This function delete the child records of FM_DOC_TYPE_UTILIZATION.
----------------------------------------------------------------------------------
FUNCTION DELETE_DOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_type_id        IN     FM_DOC_TYPE_UTILIZATION.TYPE_ID%TYPE)

   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: EXISTS_CHILD
-- Purpose:       This function will check if exists childs of table.
----------------------------------------------------------------------------------
FUNCTION EXIST_CHILD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists         IN OUT BOOLEAN,
                     I_type_id        IN     FM_DOC_TYPE_UTILIZATION.TYPE_ID%TYPE,
                     I_utilization_id IN     FM_DOC_TYPE_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: EXISTS_ANY_UTILIZATION
-- Purpose:       This function validates the utilization.
----------------------------------------------------------------------------------
FUNCTION EXISTS_ANY_UTILIZATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists         IN OUT BOOLEAN,
                                I_utilization_id IN     FM_DOC_TYPE_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------
END FM_DOC_TYPE_UTILIZATION_SQL;
/
