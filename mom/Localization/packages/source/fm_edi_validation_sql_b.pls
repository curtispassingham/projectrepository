CREATE OR REPLACE PACKAGE BODY FM_EDI_VALIDATION is
---------------------------------------------------------------------------------------
FUNCTION GET_TRIANG_INFO(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_status           IN OUT INTEGER,
                         O_triang_ind       IN OUT ORDHEAD.TRIANGULATION_IND%TYPE,
                         O_compl_edi_doc_id IN OUT FM_EDI_DOC_COMPLEMENT.COMPL_EDI_DOC_ID%TYPE,
                         I_edi_doc_id       IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_EXPLODE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_edi_doc_id     IN     FM_EDI_DOC_DETAIL.EDI_DOC_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_VALIDATION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_status        IN OUT INTEGER,
                            O_nf_type_ind   IN OUT VARCHAR2,
                            I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN is

   L_program          VARCHAR2(100) := 'FM_EDI_VALIDATION.PROCESS_VALIDATION';
   L_triang_ind       ORDHEAD.TRIANGULATION_IND%TYPE;
   L_compl_edi_doc_id FM_EDI_DOC_COMPLEMENT.COMPL_EDI_DOC_ID%TYPE;
   L_freight_nf_ind   FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE := 'N';
   
   cursor C_GET_FREIGHT_NF_IND is
      select fua.comp_freight_nf_ind
        from fm_edi_doc_header fedh,
             fm_utilization_attributes fua
       where fedh.utilization_id = fua.utilization_id
         and fedh.edi_doc_id     = I_edi_doc_id;

BEGIN
   O_status   := 1;
   O_nf_type_ind := 'M';
   
   if (FM_EDI_VALIDATION.GET_TRIANG_INFO(O_error_message, O_status, L_triang_ind, L_compl_edi_doc_id, I_edi_doc_id) = FALSE) then
      return (FALSE);
   end if;

   if L_triang_ind = 'Y' then
      O_nf_type_ind := 'B';
   end if;

   if (FM_EDI_VALIDATION.VALIDATE_HEADER(O_error_message, O_status, I_edi_doc_id) = FALSE) then
      return (FALSE);
   end if;
   
   SQL_LIB.SET_MARK('OPEN', 'C_GET_FREIGHT_NF_IND', 'FM_UTILIZATION_ATTRIBUTES', I_edi_doc_id);
   open C_GET_FREIGHT_NF_IND;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_FREIGHT_NF_IND', 'FM_UTILIZATION_ATTRIBUTES', I_edi_doc_id);
   fetch C_GET_FREIGHT_NF_IND into L_freight_nf_ind;
   ---
   
   if C_GET_FREIGHT_NF_IND%NOTFOUND then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_UTIL',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
   end if;

   ---   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_FREIGHT_NF_IND', 'FM_UTILIZATION_ATTRIBUTES', I_edi_doc_id);
   close C_GET_FREIGHT_NF_IND;
   ---

   if (FM_EDI_VALIDATION.VALIDATE_DETAILS(O_error_message, O_status, I_edi_doc_id, L_freight_nf_ind) = FALSE) then
      return (FALSE);
   end if;

   if (FM_EDI_VALIDATION.VALIDATE_PAYMENTS(O_error_message, O_status, I_edi_doc_id) = FALSE) then
      return (FALSE);
   end if;

   if (FM_EDI_VALIDATION.VALIDATE_COMPLEMENTS(O_error_message, O_status, I_edi_doc_id, L_freight_nf_ind) = FALSE) then
      return (FALSE);
   end if;

   if (FM_EDI_VALIDATION.VALIDATE_TAX_HEAD(O_error_message, O_status, I_edi_doc_id) = FALSE) then
      return (FALSE);
   end if;

   if (FM_EDI_VALIDATION.VALIDATE_TAX_DETAIL(O_error_message, O_status, I_edi_doc_id, L_freight_nf_ind) = FALSE) then
      return (FALSE);
   end if;

   --- Perform validations for complementary NFs, if the same exists
   if L_triang_ind = 'Y' then
      
      if (FM_EDI_VALIDATION.VALIDATE_HEADER(O_error_message, O_status, L_compl_edi_doc_id) = FALSE) then
         return (FALSE);
      end if;

      if (FM_EDI_VALIDATION.VALIDATE_DETAILS(O_error_message, O_status, L_compl_edi_doc_id, 'N') = FALSE) then
         return (FALSE);
      end if;
 
      if (FM_EDI_VALIDATION.VALIDATE_PAYMENTS(O_error_message, O_status, L_compl_edi_doc_id) = FALSE) then
         return (FALSE);
      end if;

      if (FM_EDI_VALIDATION.VALIDATE_COMPLEMENTS(O_error_message, O_status, L_compl_edi_doc_id, 'N') = FALSE) then
         return (FALSE);
      end if;

      if (FM_EDI_VALIDATION.VALIDATE_TAX_HEAD(O_error_message, O_status, L_compl_edi_doc_id) = FALSE) then
         return (FALSE);
      end if;

      if (FM_EDI_VALIDATION.VALIDATE_TAX_DETAIL(O_error_message, O_status, L_compl_edi_doc_id, 'N') = FALSE) then
         return (FALSE);
      end if;
      O_nf_type_ind := 'B';   
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_FREIGHT_NF_IND%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_FREIGHT_NF_IND', 'FM_UTILIZATION_ATTRIBUTES', I_edi_doc_id);
         close C_GET_FREIGHT_NF_IND;
      end if;
      
      return FALSE;
END PROCESS_VALIDATION;
--------------------------------------------------------------------------------
FUNCTION GET_TRIANG_INFO(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_status           IN OUT INTEGER,
                         O_triang_ind       IN OUT ORDHEAD.TRIANGULATION_IND%TYPE,
                         O_compl_edi_doc_id IN OUT FM_EDI_DOC_COMPLEMENT.COMPL_EDI_DOC_ID%TYPE,
                         I_edi_doc_id       IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN is
   L_program              VARCHAR2(100) := 'FM_EDI_VALIDATION.GET_TRIANG_INFO';
   L_compl_freight_ind    FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE;
   L_req_no               FM_EDI_DOC_DETAIL.REQUISITION_NO%TYPE;
   ---
   
   cursor C_GET_UTIL_ATTR is
      select comp_freight_nf_ind
        from fm_utilization_attributes fua,
             fm_edi_doc_header fedh
       where fedh.edi_doc_id = I_edi_doc_id
         and fua.utilization_id = fedh.utilization_id;

   cursor C_GET_REQ_NO is
      select distinct nvl(requisition_no,-999) requisition_no
        from fm_edi_doc_detail fedd
       where fedd.edi_doc_id     = I_edi_doc_id
    order by requisition_no desc;

   cursor C_GET_TRIANG_IND is
      select ord.triangulation_ind
        from ordhead ord
       where ord.order_no = L_req_no;
         
   cursor C_GET_COMPL_DOC_ID is
      select compl_edi_doc_id
        from fm_edi_doc_complement
       where edi_doc_id = I_edi_doc_id;
         
BEGIN
   O_status := C_SUCCESS;
   
   SQL_LIB.SET_MARK('OPEN', 'C_GET_UTIL_ATTR', 'FM_UTILIZATION_ATTRIBUTES', I_edi_doc_id);
   open C_GET_UTIL_ATTR;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_UTIL_ATTR', 'FM_UTILIZATION_ATTRIBUTES', I_edi_doc_id);
   fetch C_GET_UTIL_ATTR into L_compl_freight_ind;
   ---
   
   if C_GET_UTIL_ATTR%NOTFOUND then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_UTIL',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
   end if;

   ---   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_UTIL_ATTR', 'FM_UTILIZATION_ATTRIBUTES', I_edi_doc_id);
   close C_GET_UTIL_ATTR;
   ---
   
   if L_compl_freight_ind = 'N' then

      SQL_LIB.SET_MARK('OPEN', 'C_GET_REQ_NO', 'FM_EDI_DOC_DETAIL', I_edi_doc_id);
      open C_GET_REQ_NO;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_GET_REQ_NO', 'FM_EDI_DOC_DETAIL', I_edi_doc_id);
      fetch C_GET_REQ_NO into L_req_no;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_REQ_NO', 'FM_EDI_DOC_DETAIL', I_edi_doc_id);
      close C_GET_REQ_NO;
      ---   

      if L_req_no <> -999 then   
         SQL_LIB.SET_MARK('OPEN', 'C_GET_TRIANG_IND', 'ORDHEAD', L_req_no);
         open C_GET_TRIANG_IND;
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_GET_TRIANG_IND', 'ORDHEAD', L_req_no);
         fetch C_GET_TRIANG_IND into O_triang_ind;
         ---
   
         if C_GET_TRIANG_IND%NOTFOUND then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_REQ_NO',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  NULL);
            return FALSE;
         end if;

         ---   
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_TRIANG_IND', 'ORDHEAD', L_req_no);
         close C_GET_TRIANG_IND;
         ---

         if O_triang_ind = 'Y' then
            --- Get complement edi doc id, if triang_ind is 'Y'
            SQL_LIB.SET_MARK('OPEN', 'C_GET_COMPL_DOC_ID', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
            open C_GET_COMPL_DOC_ID;
            ---
            SQL_LIB.SET_MARK('FETCH', 'C_GET_COMPL_DOC_ID', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
            fetch C_GET_COMPL_DOC_ID into O_compl_edi_doc_id;
            ---
    
            if C_GET_COMPL_DOC_ID%NOTFOUND then
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('ORFM_TRIANG_PO_COND',
                                                     L_program,
                                                     'EDI Doc: '||I_edi_doc_id,
                                                     NULL);
               return FALSE;
            end if;

            ---
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_COMPL_DOC_ID', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
            close C_GET_COMPL_DOC_ID;
            ---
         end if;
      else
         O_triang_ind := 'N';
      end if;
   else
      O_triang_ind := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_UTIL_ATTR%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_UTIL_ATTR', 'FM_UTILIZATION_ATTRIBUTES', I_edi_doc_id);
         close C_GET_UTIL_ATTR;
      end if;
      
      if C_GET_REQ_NO%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_REQ_NO', 'FM_EDI_DOC_DETAIL', I_edi_doc_id);
         close C_GET_REQ_NO;
      end if;

      if C_GET_TRIANG_IND%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_TRIANG_IND', 'ORDHEAD', L_req_no);
         close C_GET_TRIANG_IND;
      end if;

      if C_GET_COMPL_DOC_ID%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_COMPL_DOC_ID', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
         close C_GET_COMPL_DOC_ID;
      end if;
      
      return FALSE;
END GET_TRIANG_INFO;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_HEADER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_status        IN OUT INTEGER,
                         I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN is
   L_program              VARCHAR2(100)                            := 'FM_EDI_VALIDATION.VALIDATE_HEADER';
   L_exists               BOOLEAN := TRUE;
   L_valid_module         NUMBER;
   L_valid_code_detail    NUMBER;
   L_subseries_ind        VARCHAR2(1);
   L_module               FM_EDI_DOC_HEADER.MODULE%TYPE            := 'SUPP';
   L_req_type             FM_EDI_DOC_HEADER.REQUISITION_TYPE%TYPE  := 'PO';
   L_key_value_2          FM_EDI_DOC_HEADER.KEY_VALUE_2%TYPE       := 'S';

   ---
   cursor C_GET_EDI_HEADER is
      select edi_doc_id
           , location_id
           , location_type
           , fiscal_doc_no
           , series_no
           , subseries_no
           , null schedule_no
           , 'W' status  -- always load the record in 'W'orksheet status
           , type_id
           , utilization_id
           , requisition_type
           , module
           , key_value_1
           , key_value_2
           , issue_date
           , entry_or_exit_date
           , exit_hour
           , partner_type
           , partner_id
           , quantity
           , unit_type
           , freight_type
           , net_weight
           , total_weight
           , total_serv_value
           , total_item_value
           , total_doc_value
           , freight_cost
           , insurance_cost
           , other_expenses_cost
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
           , process_status
           , import_date
           , process_date
           , nf_cfop
        from fm_edi_doc_header h
       where h.edi_doc_id = I_edi_doc_id;

   ---
   cursor C_GET_MODULE (P_key_value1 IN VARCHAR2) is
      select 1
        from v_br_sups_fiscal_addr v,
             sups s,
             system_options so
       where v.supplier = P_key_value1
         and s.supplier = v.supplier
         and s.supplier_parent is not null
         and s.sup_status = 'A'
         and so.supplier_sites_ind = 'Y'
      union all
      select 1
        from v_br_sups_fiscal_addr v,
             sups s,
             system_options so
       where v.supplier = P_key_value1
         and s.supplier = v.supplier
         and s.sup_status = 'A'
         and so.supplier_sites_ind = 'N';
   ---
   cursor C_GET_VALID_UTILIZATION_ID (P_utilization_id IN VARCHAR2,
                                      P_type_id        IN NUMBER) is
      select ffdt.subseries_no_ind
        from fm_fiscal_utilization ffu,
             fm_fiscal_doc_type ffdt,
             fm_doc_type_utilization fdtu
       where ffu.requisition_type = L_req_type
         and ffu.status = 'A'
         and ffu.utilization_id = fdtu.utilization_id
         and ffdt.type_id = fdtu.type_id
         and fdtu.utilization_id = P_utilization_id
         and fdtu.type_id = P_type_id
         and fdtu.status = 'A';
      
   L_edi_header  C_GET_EDI_HEADER%ROWTYPE;

BEGIN
   O_status := C_SUCCESS;
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_HEADER', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
   open C_GET_EDI_HEADER;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_HEADER', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
   fetch C_GET_EDI_HEADER into L_edi_header;

   ---validate the fiscal_Doc_no
   if (L_edi_header.fiscal_doc_no is null or L_edi_header.fiscal_doc_no < 0) then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_FISCAL_DOC_NO',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
      ---
   end if;

   ---validate the nf_cfop
   if (L_edi_header.requisition_type = L_req_type and L_edi_header.nf_cfop is null) then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_CFOP',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
      ---
   end if;

   ---validate the series_no
   if (L_edi_header.series_no is null) then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_SERIES_NO_NOT_NULL',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
      ---
   end if;

   ---validate the module
   if (L_edi_header.module <> L_module) then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('INV_MODULE',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
      ---
   end if;

   ---validate the key_value_2
   if (L_edi_header.key_value_2 <> L_key_value_2) then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('INV_VALUE',
                                            L_program,
                                            'Key Value 2: '||L_edi_header.key_value_2,
                                            'EDI Doc: '||I_edi_doc_id);
      return FALSE;
      ---
   end if;

   -- Verify the Sender/Receiver
   SQL_LIB.SET_MARK('OPEN', 'C_GET_MODULE', 'V_BR_SUPS_FISCAL_ADDR', I_edi_doc_id);
   open C_GET_MODULE (L_edi_header.key_value_1);
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_MODULE', 'V_BR_SUPS_FISCAL_ADDR', I_edi_doc_id);
   fetch C_GET_MODULE into L_valid_module;
   ---
   if (L_valid_module is null or C_GET_MODULE%NOTFOUND) then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
      ---
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_MODULE', 'V_BR_SUPS_FISCAL_ADDR', I_edi_doc_id);
   ---
   close C_GET_MODULE;

   --- Verify the partner
   if L_edi_header.partner_id is NOT NULL then
      if (PARTNER_SQL.PARTNER_EXISTS(O_error_message    ,
                                     L_exists           ,
                                     L_edi_header.partner_id,
                                     L_edi_header.partner_type) = FALSE) then
         return FALSE;
      end if;
      if (L_exists = FALSE) then
         O_status := C_ERROR;
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARTNER',
                                               L_program,
                                               'EDI Doc: '||I_edi_doc_id,
                                               NULL);
         return FALSE;
      end if;
   end if;

   --- Verify the location/location type
   if (L_edi_header.location_type = 'S') then
      if (STORE_VALIDATE_SQL.EXIST(O_error_message,
                                   L_edi_header.location_id,
                                   L_exists) = FALSE) then
         return FALSE;
      end if;
      if (L_exists = FALSE) then
         O_status := C_ERROR;
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',
                                               L_program,
                                               'EDI Doc: '||I_edi_doc_id,
                                               NULL);
         return FALSE;
         ---
      end if;
   else
      if (L_edi_header.location_type = 'W') then
         if (WAREHOUSE_VALIDATE_SQL.EXIST(O_error_message,
                                          L_edi_header.location_id,
                                          L_exists) = FALSE) then
            return FALSE;
         end if;
         if (L_exists = FALSE) then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  NULL);
            return FALSE;
            ---
         end if;
      else
         O_status := C_ERROR;
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',
                                               L_program,
                                               'EDI Doc: '||I_edi_doc_id,
                                               NULL);
         return FALSE;
         ---
      end if;
   end if;

   --- Verify the requisition type
   if (L_edi_header.requisition_type <> L_req_type) then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_REQ_TYPE',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
      -- 001 - End
      ---
   end if;
   ---

   --- Verify the Utilization id code
   SQL_LIB.SET_MARK('OPEN', 'C_GET_VALID_UTILIZATION_ID', 'FM_DOC_TYPE_UTILIZATION', L_edi_header.utilization_id);
   open C_GET_VALID_UTILIZATION_ID (L_edi_header.utilization_id,
                                    L_edi_header.type_id);
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_VALID_UTILIZATION_ID', 'FM_DOC_TYPE_UTILIZATION', L_edi_header.utilization_id);
   fetch C_GET_VALID_UTILIZATION_ID into L_subseries_ind;
   ---
   if (C_GET_VALID_UTILIZATION_ID%NOTFOUND) then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_UTIL_DOC_TYPE',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
      ---
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_VALID_UTILIZATION_ID', 'FM_DOC_TYPE_UTILIZATION', L_edi_header.utilization_id);
   close C_GET_VALID_UTILIZATION_ID;
   ---
   
   if L_subseries_ind = 'N' then
      if L_edi_header.subseries_no is not null then
         O_status := C_ERROR;
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_SUBSERIES_NULL',
                                               L_program,
                                               'EDI Doc: '||I_edi_doc_id,
                                               NULL);
         return FALSE;
      end if;
   end if;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_HEADER', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
   close C_GET_EDI_HEADER;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_EDI_HEADER%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_HEADER', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
         close C_GET_EDI_HEADER;
      end if;

      if C_GET_MODULE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_MODULE', 'V_BR_SUPS_FISCAL_ADDR', I_edi_doc_id);
         close C_GET_MODULE;
      end if;
      
      if C_GET_VALID_UTILIZATION_ID%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_VALID_UTILIZATION_ID', 'FM_DOC_TYPE_UTILIZATION', L_edi_header.utilization_id);
         close C_GET_VALID_UTILIZATION_ID;
      end if;
      
      return FALSE;
END VALIDATE_HEADER;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_DETAILS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_status         IN OUT INTEGER,
                          I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                          I_freight_nf_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE)
   return BOOLEAN is
   L_program       VARCHAR2(100) := 'FM_EDI_VALIDATION.VALIDATE_DETAILS';
   L_exists        BOOLEAN := TRUE;
   L_req_no        ORDHEAD.ORDER_NO%TYPE;
   L_item          ITEM_MASTER.ITEM%TYPE;
   L_item_level    ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level    ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_item_parent   ITEM_MASTER.ITEM_PARENT%TYPE;
   ---
   cursor C_GET_EDI_DETAILS is
      select d.edi_doc_line_id
           , d.edi_doc_id
           , d.location_id
           , d.location_type
           , d.line_no
           , d.requisition_no
           , d.item
           , d.classification_id
           , d.quantity
           , d.unit_cost
           , d.total_cost
           , d.freight_cost
           , d.net_cost
           , d.pack_ind
           , d.pack_no
           , d.create_datetime
           , d.create_id
           , d.last_update_datetime
           , d.last_update_id
           , d.vpn
           , h.key_value_1
        from fm_edi_doc_detail d,
             fm_edi_doc_header h
       where d.edi_doc_id = h.edi_doc_id
         and h.edi_doc_id = I_edi_doc_id;

   cursor C_GET_VAL_PO(P_req_no   FM_EDI_DOC_DETAIL.REQUISITION_NO%TYPE,
                       P_loc      FM_EDI_DOC_DETAIL.LOCATION_ID%TYPE,
                       P_loc_type FM_EDI_DOC_DETAIL.LOCATION_TYPE%TYPE,
                       P_item     FM_EDI_DOC_DETAIL.ITEM%TYPE,
                       P_pack_no  FM_EDI_DOC_DETAIL.PACK_NO%TYPE) is
      select order_no
        from ordloc
       where order_no  = P_req_no
         and ((P_loc_type = 'W'
               and loc_type = 'W'
               and location  in (select wh
                                   from wh
                                  where physical_wh = P_loc))
             or (P_loc_type = 'S'
                 and loc_type = 'S'
                 and location = P_loc))
         and item      = P_item
         and P_pack_no is null
      union all
      select ol.order_no
        from ordloc ol,
             packitem pi
       where ol.order_no  = P_req_no
         and ((P_loc_type = 'W'
               and ol.loc_type = 'W'
               and ol.location  in (select wh
                                      from wh
                                     where physical_wh = P_loc))
             or (P_loc_type = 'S'
                 and ol.loc_type = 'S'
                 and ol.location = P_loc))
         and ol.item      = pi.pack_no
         and pi.pack_no   = P_pack_no
         and pi.item      = P_item
         and P_pack_no is not null;

   ---
   cursor C_GET_ITEM_INFO(P_item ITEM_MASTER.ITEM%TYPE) is
      select pack_ind
        from item_master
       where item = P_item
         and status = 'A'
         and item_level = tran_level;
   
   TYPE VAL_EDI_DETAIL_TYPE IS TABLE OF C_GET_EDI_DETAILS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_edi_detail VAL_EDI_DETAIL_TYPE;
   L_pack_ind      ITEM_MASTER.PACK_IND%TYPE;
   ---
   cursor C_GET_ITEM(P_supplier ITEM_SUPPLIER.SUPPLIER%TYPE,
                     P_vpn      ITEM_SUPPLIER.VPN%TYPE) is
      select s.item,
             m.item_parent,
             m.item_level,
             m.tran_level,
             m.pack_ind
        from item_supplier s,
             item_master m
       where s.supplier = P_supplier
         and s.vpn = P_vpn
         and s.item = m.item
         and m.status = 'A';

BEGIN
   O_status := C_SUCCESS;
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_DETAILS', 'FM_EDI_DOC_DETAILS', I_edi_doc_id);
   open C_GET_EDI_DETAILS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_DETAILS', 'FM_EDI_DOC_DETAILS', I_edi_doc_id);
   fetch C_GET_EDI_DETAILS BULK COLLECT into L_edi_detail;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_DETAILS', 'FM_EDI_DOC_DETAILS', I_edi_doc_id);
   close C_GET_EDI_DETAILS;
   ---

   if L_edi_detail.COUNT > 0 then
      for i IN L_edi_detail.FIRST..L_edi_detail.LAST LOOP
         -- verify that item level records does not exists for complementary freight NF
         if I_freight_nf_ind = 'Y' then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('ORFM_FRTNF_NO_ITMDTL',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
            return FALSE;
         end if;
         ---
         -- Verify the location/location type
         if (L_edi_detail(i).location_type = 'S') then
            if (STORE_VALIDATE_SQL.EXIST(O_error_message,
                                         L_edi_detail(i).location_id,
                                         L_exists) = FALSE) then
               return FALSE;
            end if;
            if (L_exists = FALSE) then
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',
                                                     L_program,
                                                     'EDI Doc: '||I_edi_doc_id,
                                                     'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
               return FALSE;
               ---
            end if;
         else
            if (L_edi_detail(i).location_type = 'W') then
               if (WAREHOUSE_VALIDATE_SQL.EXIST(O_error_message,
                                                L_edi_detail(i).location_id,
                                                L_exists) = FALSE) then
                  return FALSE;
               end if;
               if (L_exists = FALSE) then
                  O_status := C_ERROR;
                  O_error_message := SQL_LIB.CREATE_MSG('INV_LOCATION',
                                                        L_program,
                                                        'EDI Doc: '||I_edi_doc_id,
                                                        'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
                  return FALSE;
                  ---
               end if;
            else
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',
                                                     L_program,
                                                     'EDI Doc: '||I_edi_doc_id,
                                                     'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
               return FALSE;
               ---
            end if;
         end if;

         -- Verify the item
         if L_edi_detail(i).item is not null then
            SQL_LIB.SET_MARK('OPEN', 'C_GET_ITEM_INFO', 'ITEM_MASTER', L_edi_detail(i).item);
            open C_GET_ITEM_INFO(L_edi_detail(i).item);
            ---
            SQL_LIB.SET_MARK('FETCH', 'C_GET_ITEM_INFO', 'ITEM_MASTER', L_edi_detail(i).item);
            fetch C_GET_ITEM_INFO into L_pack_ind;
    
            if C_GET_ITEM_INFO%NOTFOUND then
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                                     L_program,
                                                     'EDI Doc: '||I_edi_doc_id,
                                                     'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
               return FALSE;
            else
               if NVL(L_edi_detail(i).pack_ind,'N') <> L_pack_ind then
                  O_status := C_ERROR;
                  O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_PACK_IND',
                                                        L_program,
                                                        'EDI Doc: '||I_edi_doc_id,
                                                        'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
                  return FALSE;    
               end if;
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_ITEM_INFO', 'ITEM_MASTER', L_edi_detail(i).item);
            close C_GET_ITEM_INFO;
            ---
         else
            if L_edi_detail(i).vpn is not null then
               SQL_LIB.SET_MARK('OPEN', 'C_GET_ITEM', 'ITEM_SUPPLIER', I_edi_doc_id);
               open C_GET_ITEM(L_edi_detail(i).key_value_1,L_edi_detail(i).vpn);
               ---
               SQL_LIB.SET_MARK('FETCH', 'C_GET_ITEM', 'ITEM_SUPPLIER', I_edi_doc_id);
               fetch C_GET_ITEM into L_item,
                                     L_item_parent,
                                     L_item_level,
                                     L_tran_level,
                                     L_pack_ind;

               if C_GET_ITEM%NOTFOUND then
                  O_status := C_ERROR;
                  O_error_message := SQL_LIB.CREATE_MSG('INV_VPN',
                                                        L_program,
                                                        'EDI Doc: '||I_edi_doc_id,
                                                        'Vpn: '||L_edi_detail(i).vpn);
                  return FALSE;
               else
                  fetch C_GET_ITEM into L_item,
                                        L_item_parent,
                                        L_item_level,
                                        L_tran_level,
                                        L_pack_ind;
                  ---
                  if C_GET_ITEM%FOUND then
                     O_status := C_ERROR;
                     O_error_message := SQL_LIB.CREATE_MSG('ORFM_VPN_NOT_UNIQUE',
                                                            L_program,
                                                           'EDI Doc: '||I_edi_doc_id,
                                                           'Vpn: '||L_edi_detail(i).vpn);
                     return FALSE;
                  end if;
                  ---
                  if L_item_level < L_tran_level then
                     O_status := C_ERROR;
                     O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_ITEM_VPN',
                                                            L_program,
                                                            'EDI Doc: '||I_edi_doc_id,
                                                            'Item: '||L_edi_detail(i).item);
                     return FALSE;
                  elsif L_item_level > L_tran_level then 
                     L_edi_detail(i).item := L_item_parent;
                  else
                     L_edi_detail(i).item := L_item;
                  end if;
                  --- 
                  if NVL(L_edi_detail(i).pack_ind,'N') <> L_pack_ind then
                     O_status := C_ERROR;
                     O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_PACK_IND',
                                                           L_program,
                                                           'EDI Doc: '||I_edi_doc_id,
                                                           'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
                     return FALSE;
                  end if;

                  update fm_edi_doc_detail
                     set item = L_edi_detail(i).item
                   where edi_doc_id = I_edi_doc_id
                     and edi_doc_line_id = L_edi_detail(i).edi_doc_line_id;
                  ---
               end if;
               ---
               SQL_LIB.SET_MARK('CLOSE', 'C_GET_ITEM', 'ITEM_SUPPLIER', I_edi_doc_id);
               close C_GET_ITEM;
               --- 
            else
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('ORFM_ITEM_VPN_NULL',
                                                      L_program,
                                                      'EDI Doc: '||I_edi_doc_id,
                                                      'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
               return FALSE;
            end if;
            ---
         end if;

         -- Verify item quantity
         if (L_edi_detail(i).quantity <= 0) then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('MIN_QTY_GREATER_ZERO',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
            return FALSE;
            ---
         end if;
      
         -- Verify the item unit_cost
         if (L_edi_detail(i).unit_cost is NULL) then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('NO_UNIT_COST',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
            return FALSE;
            ---
         else
            if (L_edi_detail(i).unit_cost <= 0) then
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('UNIT_COST_GREATER_ZERO',
                                                     L_program,
                                                     'EDI Doc: '||I_edi_doc_id,
                                                     'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
               return FALSE;
               ---
            end if;
         end if;
   
         -- Verify the item total_cost
         if ((L_edi_detail(i).total_cost is NULL) or 
             (L_edi_detail(i).total_cost is not NULL and (L_edi_detail(i).total_cost <> (L_edi_detail(i).unit_cost * L_edi_detail(i).quantity)))) then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_TOTAL_COST',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
            return FALSE;
            ---
         end if;
       
         -- Verify the Fiscal Classification
         if NVL(L_edi_detail(i).pack_ind,'N') = 'N' then
            if (FM_FISCAL_CLASSIFICATION_SQL.EXISTS(O_error_message,
                                                    L_exists,
                                                    L_edi_detail(i).item,
                                                    L_edi_detail(i).classification_id) = FALSE) then
               return FALSE;
            end if;
            ---
            if (L_exists = FALSE) then
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_FISCAL_CLASSIF',
                                                     L_program,
                                                     'EDI Doc: '||I_edi_doc_id,
                                                     'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
               return FALSE;
               ---
            end if;
         end if;

         -- Verify requisition no
         if L_edi_detail(i).requisition_no is not NULL then
            SQL_LIB.SET_MARK('OPEN', 'C_GET_VAL_PO', 'ORDHEAD', L_edi_detail(i).requisition_no);
            open C_GET_VAL_PO(L_edi_detail(i).requisition_no,
                              L_edi_detail(i).location_id,
                              L_edi_detail(i).location_type,
                              L_edi_detail(i).item,
                              L_edi_detail(i).pack_no);
         
            SQL_LIB.SET_MARK('FETCH', 'C_GET_VAL_PO', 'ORDHEAD', L_edi_detail(i).requisition_no);
            fetch C_GET_VAL_PO into L_req_no;
    
            if C_GET_VAL_PO%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE', 'C_GET_VAL_PO', 'ORDHEAD', L_edi_detail(i).requisition_no);
               close C_GET_VAL_PO;
               --
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('ORFM_INV_PO_ITMLOC',
                                                     L_program,
                                                     'EDI Doc: '||I_edi_doc_id,
                                                     'EDI Doc Line: '||L_edi_detail(i).edi_doc_line_id);
               return FALSE;
            end if;

            SQL_LIB.SET_MARK('CLOSE', 'C_GET_VAL_PO', 'ORDHEAD', L_edi_detail(i).requisition_no);
            close C_GET_VAL_PO;
            ---
         end if;
      end loop;
   else
      if I_freight_nf_ind = 'N' then
         O_status := C_ERROR;
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_DETAIL_INFO_REQ',
                                               L_program,
                                               'EDI Doc: '||I_edi_doc_id,
                                               NULL);
         return FALSE;
      end if;
      ---
   end if;
   
   if FM_EDI_VALIDATION.VALIDATE_PACK_EXPLODE(O_error_message,
                                              I_edi_doc_id) = FALSE then
      O_status := C_ERROR;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_EDI_DETAILS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_DETAILS', 'FM_EDI_DOC_DETAILS', I_edi_doc_id);
         close C_GET_EDI_DETAILS;
      end if;
      
      if C_GET_ITEM_INFO%ISOPEN then
         close C_GET_ITEM_INFO;
      end if;
      
      if C_GET_VAL_PO%ISOPEN then
         close C_GET_VAL_PO;
      end if;

      if C_GET_ITEM%ISOPEN then
         close C_GET_ITEM;
      end if;
      
      return FALSE;
END VALIDATE_DETAILS;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_EXPLODE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_edi_doc_id     IN     FM_EDI_DOC_DETAIL.EDI_DOC_ID%TYPE)
   return BOOLEAN is
   ---
   L_program            VARCHAR2(100) := 'FM_EDI_VALIDATION.VALIDATE_PACK_EXPLODE';
   L_cursor             VARCHAR2(100);
   L_table              VARCHAR2(100) := 'FM_EDI_DOC_DETAIL';
   L_key                VARCHAR2(100) := 'EDI_doc_id: ' || TO_CHAR(I_edi_doc_id);
   L_pack_no            FM_EDI_DOC_DETAIL.PACK_NO%TYPE;
   L_req_no             FM_EDI_DOC_DETAIL.REQUISITION_NO%TYPE;
   L_chk_all_Pack_item  NUMBER;
   L_chk_pack_factor    NUMBER;
   L_quantity           FM_EDI_DOC_DETAIL.QUANTITY%TYPE;
   ---
   cursor C_EDI_DOC_PACK_NO is
      select distinct requisition_no, 
             pack_no
       from  fm_edi_doc_detail fdd
       where fdd.edi_doc_id = I_edi_doc_id
         and fdd.pack_no is not null;

   cursor C_VAL_ALL_PACK_COMP( P_pack_no  FM_EDI_DOC_DETAIL.PACK_NO%TYPE,
                               P_req_no   FM_EDI_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select count(1)
        from fm_edi_doc_detail fdd ,
             packitem pi
       where fdd.pack_no = pi.pack_no
         and fdd.pack_no is not null
         and fdd.requisition_no = P_req_no
         and fdd.edi_doc_id = I_edi_doc_id
         and pi.pack_no = P_pack_no
         and pi.item not in (select item 
                               from fm_edi_doc_detail
                              where edi_doc_id = I_edi_doc_id
                                and pack_no = P_pack_no 
                                and requisition_no = P_req_no);

   cursor C_VAL_PACK_COMP_FACTOR( P_pack_no  FM_EDI_DOC_DETAIL.PACK_NO%TYPE,
                                  P_req_no   FM_EDI_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select count (distinct (fdd.quantity / pi.pack_qty))
        from fm_edi_doc_detail fdd ,
             packitem pi
       where fdd.pack_no = pi.pack_no
         and fdd.pack_no is not null
         and fdd.item = pi.item
         and fdd.requisition_no = P_req_no
         and fdd.edi_doc_id = I_edi_doc_id
         and fdd.pack_no = P_pack_no;

   cursor C_VAL_PACK_COMP_FACTOR_CHK( P_pack_no  FM_EDI_DOC_DETAIL.PACK_NO%TYPE,
                                      P_req_no   FM_EDI_DOC_DETAIL.REQUISITION_NO%TYPE) is
      select distinct (fdd.quantity / pi.pack_qty)
        from fm_edi_doc_detail fdd ,
             packitem pi
       where fdd.pack_no = pi.pack_no
         and fdd.pack_no is not null
         and fdd.item = pi.item
         and fdd.requisition_no = P_req_no
         and fdd.edi_doc_id = I_edi_doc_id
         and fdd.pack_no = P_pack_no ;

   ---
BEGIN
   ---
   if I_edi_doc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_edi_doc_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_cursor := 'C_EDI_DOC_PACK_NO';

   SQL_LIB.SET_MARK('OPEN', L_cursor, L_table, L_key);
   open C_EDI_DOC_PACK_NO;
   ---
   LOOP
      SQL_LIB.SET_MARK('FETCH', L_cursor, L_table, L_key);
      fetch C_EDI_DOC_PACK_NO into L_req_no, L_pack_no;
      EXIT when C_EDI_DOC_PACK_NO%NOTFOUND;

      -- check if all comp of pack are available
      SQL_LIB.SET_MARK('OPEN', 'C_VAL_ALL_PACK_COMP', L_table, L_key);
      open C_VAL_ALL_PACK_COMP(L_pack_no, L_req_no);
     ---
      SQL_LIB.SET_MARK('FETCH', 'C_VAL_ALL_PACK_COMP', L_table, L_key);
      fetch C_VAL_ALL_PACK_COMP into L_chk_all_Pack_item;

      if L_chk_all_Pack_item > 0 then
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_PACK_COMP_NOT_MATCH',
                                               L_pack_no,
                                               'Raised By '||L_program,
                                               'for EDI Doc: '||I_edi_doc_id);
         SQL_LIB.SET_MARK('CLOSE', 'C_VAL_ALL_PACK_COMP', L_table, L_key);
         close C_VAL_ALL_PACK_COMP;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_VAL_ALL_PACK_COMP', L_table, L_key);
      close C_VAL_ALL_PACK_COMP;

      -- check if all comp of pack are with same factor

      SQL_LIB.SET_MARK('OPEN', 'C_VAL_PACK_COMP_FACTOR', L_table, L_key);
      open C_VAL_PACK_COMP_FACTOR(L_pack_no, L_req_no);
     ---
      SQL_LIB.SET_MARK('FETCH', 'C_VAL_PACK_COMP_FACTOR', L_table, L_key);
      fetch C_VAL_PACK_COMP_FACTOR into L_chk_pack_factor;

      if L_chk_pack_factor > 1 then
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_PKCMP_FACT_NOT_MATCH',
                                               L_pack_no,
                                               'Raised By '||L_program,
                                               'for EDI Doc: '||I_edi_doc_id);
         SQL_LIB.SET_MARK('CLOSE', 'C_VAL_PACK_COMP_FACTOR', L_table, L_key);
         close C_VAL_PACK_COMP_FACTOR;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_VAL_PACK_COMP_FACTOR', L_table, L_key);
      close C_VAL_PACK_COMP_FACTOR;

      -- check if all comp of pack are with same factor but no fraction

      SQL_LIB.SET_MARK('OPEN', 'C_VAL_PACK_COMP_FACTOR_CHK', L_table, L_key);
      open C_VAL_PACK_COMP_FACTOR_CHK(L_pack_no, L_req_no);
     ---
      SQL_LIB.SET_MARK('FETCH', 'C_VAL_PACK_COMP_FACTOR_CHK', L_table, L_key);
      fetch C_VAL_PACK_COMP_FACTOR_CHK into L_quantity;

      if L_quantity <> Round(L_quantity) then
         O_error_message := SQL_LIB.CREATE_MSG('ORFM_PKCMP_FACT_NOT_MATCH',
                                               L_pack_no,
                                               'Raised By '||L_program,
                                               'for EDI Doc: '||I_edi_doc_id);
         SQL_LIB.SET_MARK('CLOSE', 'C_VAL_PACK_COMP_FACTOR_CHK', L_table, L_key);
         close C_VAL_PACK_COMP_FACTOR_CHK;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_VAL_PACK_COMP_FACTOR_CHK', L_table, L_key);
      close C_VAL_PACK_COMP_FACTOR_CHK;

   END LOOP;
   ---
   SQL_LIB.SET_MARK('CLOSE', L_cursor, L_table, L_key);
   close C_EDI_DOC_PACK_NO;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SUBSTR(SQLERRM, 1,254),
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if C_EDI_DOC_PACK_NO%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_EDI_DOC_PACK_NO', L_table, L_key);
         close C_EDI_DOC_PACK_NO;
      end if;
      ---
      if C_VAL_ALL_PACK_COMP%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_VAL_ALL_PACK_COMP', L_table, L_key);
         close C_VAL_ALL_PACK_COMP;
      end if;
      ---
      if C_VAL_PACK_COMP_FACTOR%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_VAL_PACK_COMP_FACTOR', L_table, L_key);
         close C_VAL_PACK_COMP_FACTOR;
      end if;
      ---
      if C_VAL_PACK_COMP_FACTOR_CHK%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_VAL_PACK_COMP_FACTOR_CHK', L_table, L_key);
         close C_VAL_PACK_COMP_FACTOR_CHK;
      end if;
      ---
      return FALSE;
END VALIDATE_PACK_EXPLODE;
--------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PAYMENTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_status        IN OUT INTEGER,
                           I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN is

   L_program  VARCHAR2(100) := 'FM_EDI_VALIDATION.VALIDATE_PAYMENTS';
   L_cnt      NUMBER        := 0;
   L_req_type FM_EDI_DOC_HEADER.REQUISITION_TYPE%TYPE;
   ---

   cursor C_GET_EDI_HEADER is
      select requisition_type
        from fm_edi_doc_header h
       where h.edi_doc_id = I_edi_doc_id;
   ---
   cursor C_GET_EDI_PAYMENTS is
      select edi_doc_id
           , payment_date
           , value
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
        from fm_edi_doc_payment p
       where p.edi_doc_id = I_edi_doc_id;

   TYPE VAL_EDI_PAYMENT_TYPE IS TABLE OF C_GET_EDI_PAYMENTS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_edi_payment VAL_EDI_PAYMENT_TYPE;

BEGIN
   O_status := C_SUCCESS;
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_HEADER', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
   open C_GET_EDI_HEADER;
      
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_HEADER', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
   fetch C_GET_EDI_HEADER into L_req_type;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_HEADER', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
   close C_GET_EDI_HEADER;
   ---

   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_PAYMENTS', 'FM_EDI_DOC_PAYMENT', I_edi_doc_id);
   open C_GET_EDI_PAYMENTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_PAYMENTS', 'FM_EDI_DOC_PAYMENT', I_edi_doc_id);
   fetch C_GET_EDI_PAYMENTS BULK COLLECT into L_edi_payment;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_PAYMENTS', 'FM_EDI_DOC_PAYMENT', I_edi_doc_id);
   close C_GET_EDI_PAYMENTS;
   ---
   
   if L_edi_payment.COUNT > 0 then
      L_cnt := L_cnt + 1;
   end if;
      
   -- check if payment record exists for requisition_type 'PO'
   if L_cnt = 0 and L_req_type = 'PO' then
      O_status := C_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('ORFM_PAY_DET_REQ',
                                            L_program,
                                            'EDI Doc: '||I_edi_doc_id,
                                            NULL);
      return FALSE;
   end if;
   ---
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_EDI_PAYMENTS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_PAYMENTS', 'FM_EDI_DOC_PAYMENT', I_edi_doc_id);
         close C_GET_EDI_PAYMENTS;
      end if;
      
      return FALSE;
END VALIDATE_PAYMENTS;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_COMPLEMENTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_status         IN OUT INTEGER,
                              I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                              I_freight_nf_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(100) := 'FM_EDI_VALIDATION.VALIDATE_COMPLEMENTS';
   ---
   cursor C_GET_EDI_COMPLEMENTS is
      select compl_edi_doc_id
           , edi_doc_id
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
        from fm_edi_doc_complement c
       where c.edi_doc_id = I_edi_doc_id;
       
   cursor C_GET_FISCAL_DOC_ID(P_edi_doc_id FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE) is
      select NVL(fiscal_doc_id,-999)
        from fm_edi_doc_header
       where edi_doc_id = P_edi_doc_id;

   TYPE VAL_EDI_COMPL_TYPE IS TABLE OF C_GET_EDI_COMPLEMENTS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_edi_complement VAL_EDI_COMPL_TYPE;
   L_fiscal_doc_id  FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE;

BEGIN
   O_status := C_SUCCESS;
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_COMPLEMENTS', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
   open C_GET_EDI_COMPLEMENTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_COMPLEMENTS', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
   fetch C_GET_EDI_COMPLEMENTS BULK COLLECT into L_edi_complement;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_COMPLEMENTS', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
   close C_GET_EDI_COMPLEMENTS;
   ---
   
   if L_edi_complement.COUNT > 0 then
      for i IN L_edi_complement.FIRST..L_edi_complement.LAST LOOP
         if I_freight_nf_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN', 'C_GET_FISCAL_DOC_ID', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
            open C_GET_FISCAL_DOC_ID(L_edi_complement(i).compl_edi_doc_id);
            ---
            SQL_LIB.SET_MARK('FETCH', 'C_GET_FISCAL_DOC_ID', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
            fetch C_GET_FISCAL_DOC_ID into L_fiscal_doc_id;
            ---
            if L_fiscal_doc_id = -999 then
               O_status := C_ERROR;
               O_error_message := SQL_LIB.CREATE_MSG('ORFM_FRTNF_PRCS_COND',
                                                     L_program,
                                                     'EDI Doc: '||I_edi_doc_id,
                                                     NULL);
               return FALSE;
               ---
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ID', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
            close C_GET_FISCAL_DOC_ID;
         end if;      
      END LOOP;
   end if;
   ---

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_EDI_COMPLEMENTS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_COMPLEMENTS', 'FM_EDI_DOC_COMPLEMENT', I_edi_doc_id);
         close C_GET_EDI_COMPLEMENTS;
      end if;
      
      if C_GET_FISCAL_DOC_ID%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_FISCAL_DOC_ID', 'FM_EDI_DOC_HEADER', I_edi_doc_id);
         close C_GET_FISCAL_DOC_ID;
      end if;
      
      return FALSE;
END VALIDATE_COMPLEMENTS;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_TAX_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_status        IN OUT INTEGER,
                           I_edi_doc_id    IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE)
   return BOOLEAN is

   L_program       VARCHAR2(100) := 'FM_EDI_VALIDATION.VALIDATE_TAX_HEAD';
   L_exists        INTEGER := 1;
   ---
   cursor C_GET_EDI_TAX_HEAD is
      select vat_code
           , edi_doc_id
           , tax_basis
           , modified_tax_basis
           , total_value
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
        from fm_edi_doc_tax_head h
       where h.edi_doc_id = I_edi_doc_id;

   TYPE VAL_EDI_TAXHEAD_TYPE IS TABLE OF C_GET_EDI_TAX_HEAD%ROWTYPE INDEX BY BINARY_INTEGER;
   L_tax_head VAL_EDI_TAXHEAD_TYPE;
   ---

   cursor C_EXISTS_VAT_CODE (P_vat_code IN VAT_CODES.VAT_CODE%TYPE )is
      select 1
        from vat_codes v
       where v.vat_code = P_vat_code;

BEGIN
   O_status := C_SUCCESS;
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_TAX_HEAD', 'FM_EDI_DOC_TAX_HEAD', I_edi_doc_id);
   open C_GET_EDI_TAX_HEAD;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_TAX_HEAD', 'FM_EDI_DOC_TAX_HEAD', I_edi_doc_id);
   fetch C_GET_EDI_TAX_HEAD BULK COLLECT into L_tax_head;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_TAX_HEAD', 'FM_EDI_DOC_TAX_HEAD', I_edi_doc_id);
   close C_GET_EDI_TAX_HEAD;
   ---
   if L_tax_head.COUNT > 0 then
      for i IN L_tax_head.FIRST..L_tax_head.LAST LOOP
         -- Validate Vat Code
         SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_VAT_CODE', 'V_FM_VAT_CODES', L_tax_head(i).vat_code);
         open C_EXISTS_VAT_CODE (L_tax_head(i).vat_code);
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_VAT_CODE', 'V_FM_VAT_CODES', L_tax_head(i).vat_code);
         fetch C_EXISTS_VAT_CODE into L_exists;
         ---
         if (C_EXISTS_VAT_CODE%NOTFOUND) then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('INV_VAT_CODE',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  NULL);
            return FALSE;
            ---
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_VAT_CODE', 'V_FM_VAT_CODES', L_tax_head(i).vat_code);
         close C_EXISTS_VAT_CODE;
         ---
      END LOOP;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_EDI_TAX_HEAD%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_TAX_HEAD', 'FM_EDI_DOC_TAX_HEAD', I_edi_doc_id);
         close C_GET_EDI_TAX_HEAD;
      end if;
      
      if C_EXISTS_VAT_CODE%ISOPEN then
         close C_EXISTS_VAT_CODE;
      end if;
      
      return FALSE;
END VALIDATE_TAX_HEAD;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_TAX_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_status         IN OUT INTEGER,
                             I_edi_doc_id     IN     FM_EDI_DOC_HEADER.EDI_DOC_ID%TYPE,
                             I_freight_nf_ind IN     FM_UTILIZATION_ATTRIBUTES.COMP_FREIGHT_NF_IND%TYPE)
   return BOOLEAN is

   L_program       VARCHAR2(100) := 'FM_EDI_VALIDATION.VALIDATE_TAX_DETAIL';
   L_exists        INTEGER := 1;
   ---
   cursor C_GET_EDI_TAX_DETAIL is
      select vat_code
           , edi_doc_line_id
           , percentage_rate
           , total_value
           , create_datetime
           , create_id
           , last_update_datetime
           , last_update_id
           , tax_basis
           , modified_tax_basis
           , tax_base_ind
        from fm_edi_doc_tax_detail d
       where d.edi_doc_line_id IN (select edi_doc_line_id
                                     from fm_edi_doc_detail dd
                                    where dd.edi_doc_id = I_edi_doc_id);

   TYPE VAL_EDI_TAXDTL_TYPE IS TABLE OF C_GET_EDI_TAX_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_tax_detail VAL_EDI_TAXDTL_TYPE;
   ---

   cursor C_EXISTS_VAT_CODE (P_vat_code IN VAT_CODES.VAT_CODE%TYPE )is
      select 1
        from vat_codes v
       where v.vat_code = P_vat_code;
BEGIN
   O_status := C_SUCCESS;
   SQL_LIB.SET_MARK('OPEN', 'C_GET_EDI_TAX_DETAIL', 'FM_EDI_DOC_TAX_DETAIL', I_edi_doc_id);
   open C_GET_EDI_TAX_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_GET_EDI_TAX_DETAIL', 'FM_EDI_DOC_TAX_DETAIL', I_edi_doc_id);
   fetch C_GET_EDI_TAX_DETAIL BULK COLLECT into L_tax_detail;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_TAX_DETAIL', 'FM_EDI_DOC_TAX_DETAIL', I_edi_doc_id);
   close C_GET_EDI_TAX_DETAIL;
   ---
   if L_tax_detail.COUNT > 0 then
      for i IN L_tax_detail.FIRST..L_tax_detail.LAST LOOP
         -- verify that item level records does not exists for complementary freight NF
         if I_freight_nf_ind = 'Y' then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('ORFM_FRTNF_NO_TAXDTL',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  'EDI Doc Line: '||L_tax_detail(i).edi_doc_line_id);
            return FALSE;
         end if;
         ---
      
         -- Validate Vat Code
         SQL_LIB.SET_MARK('OPEN', 'C_EXISTS_VAT_CODE', 'V_FM_VAT_CODES', L_tax_detail(i).vat_code);
         open C_EXISTS_VAT_CODE (L_tax_detail(i).vat_code);
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_EXISTS_VAT_CODE', 'V_FM_VAT_CODES', L_tax_detail(i).vat_code);
         fetch C_EXISTS_VAT_CODE into L_exists;
         ---
         if (C_EXISTS_VAT_CODE%NOTFOUND) then
            O_status := C_ERROR;
            O_error_message := SQL_LIB.CREATE_MSG('INV_VAT_CODE',
                                                  L_program,
                                                  'EDI Doc: '||I_edi_doc_id,
                                                  'EDI Doc Line: '||L_tax_detail(i).edi_doc_line_id);
            return FALSE;
            ---
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS_VAT_CODE', 'V_FM_VAT_CODES', L_tax_detail(i).vat_code);
         close C_EXISTS_VAT_CODE;
         ---
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      if C_GET_EDI_TAX_DETAIL%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_EDI_TAX_DETAIL', 'FM_EDI_DOC_TAX_DETAIL', I_edi_doc_id);
         close C_GET_EDI_TAX_DETAIL;
      end if;
      
      if C_EXISTS_VAT_CODE%ISOPEN then
         close C_EXISTS_VAT_CODE;
      end if;
      
      return FALSE;
END VALIDATE_TAX_DETAIL;
---------------------------------------------------------------------------------------
END FM_EDI_VALIDATION;
/
 
