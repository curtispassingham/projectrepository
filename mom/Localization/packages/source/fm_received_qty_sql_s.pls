CREATE OR REPLACE PACKAGE FM_RECEIVED_QTY_SQL is
------------------------------------------------------------------------------------------------
--    Name: CREATE_FOR_CORRECTION_DOC
-- Purpose: This function Determines the Action type and Action Value and 
--          makes calls to CREATE_CORRECTION_DOC with appropriate values .
------------------------------------------------------------------------------------------------
FUNCTION CREATE_FOR_CORRECTION_DOC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_fiscal_doc_id   IN      FM_FISCAL_DOC_HEADER.FISCAL_DOC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--    Name: CREATE_CORRECTION_DOC
-- Purpose: This function insert's correction record's into FM_CORRECTION_DOC .
------------------------------------------------------------------------------------------------
/*FUNCTION CREATE_CORRECTION_DOC(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_v_fm_received_qty      IN       V_FM_RECEIVED_QTY%ROWTYPE,
                               I_action_type            IN       FM_CORRECTION_DOC.ACTION_TYPE%TYPE,
                               I_action_value           IN       FM_CORRECTION_DOC.ACTION_VALUE%TYPE )
RETURN BOOLEAN;*/
------------------------------------------------------------------------------------------------
END FM_RECEIVED_QTY_SQL;
/