CREATE OR REPLACE PACKAGE BODY FM_ASNOUT_CONSUME_SQL IS
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_ind       IN OUT   BOOLEAN,
                 I_seq_no          IN       FM_STG_ASNOUT_DESC.SEQ_NO%TYPE)
return BOOLEAN is

   L_program         VARCHAR2(100)   := 'FM_ASNOUT_CONSUME_SQL.CONSUME';
   L_status_code     VARCHAR2(1);
   L_message_type    VARCHAR(20)     := 'asnoutcre';
   L_count_distro    NUMBER;
   L_count_ctn       NUMBER;
   L_count_item      NUMBER;

   cursor C_GET_ASNOUT_DESC is
      select "RIB_ASNOutDesc_REC"(0,                     -- rib_oid
                                  NULL,                  -- schedule_nbr
                                  NULL,                  -- auto_receive
                                  d.to_location,         -- to_location
                                  d.from_location,       -- from_location
                                  d.asn_nbr,             -- asn_nbr
                                  d.asn_type,            -- asn_type
                                  d.container_qty,       -- container_qty
                                  d.bol_nbr,             -- bol_nbr
                                  d.shipment_date,       -- shipment_date
                                  d.est_arr_date,        -- est_arr_date
                                  d.ship_address1,       -- ship_address1
                                  d.ship_address2,       -- ship_address2
                                  d.ship_address3,       -- ship_address3
                                  d.ship_address4,       -- ship_address4
                                  d.ship_address5,       -- ship_address5
                                  d.ship_city,           -- ship_city
                                  d.ship_state,          -- ship_state
                                  d.ship_zip,            -- ship_zip
                                  d.ship_country_id,     -- ship_country_id
                                  d.trailer_nbr,         -- trailer_nbr
                                  d.seal_nbr,            -- seal_nbr
                                  d.carrier_code,        -- carrier_code
                                  d.transshipment_nbr,   -- transshipment_nbr
                                  -- ASNOutdistro_TBL
                                  CAST(MULTISET(select "RIB_ASNOutDistro_REC"(0,                       -- rib_oid
                                                                              ds.distro_nbr,           -- distro_nbr
                                                                              ds.distro_doc_type,      -- distro_doc_type
                                                                              ds.customer_order_nbr,   -- customer_order_nbr
                                                                              ds.consumer_direct,      -- consumer_direct
                                                                              -- ASNOutctn_TBL
                                                                              CAST(MULTISET(select "RIB_ASNOutCtn_REC"(0,                        -- rib_oid
                                                                                                                       dc.final_location,        -- final_location
                                                                                                                       dc.container_id,          -- container_id
                                                                                                                       dc.container_weight,      -- container_weight
                                                                                                                       dc.container_length,      -- container_length
                                                                                                                       dc.container_width,       -- container_width
                                                                                                                       dc.container_height,      -- container_height
                                                                                                                       dc.container_cube,        -- container_cube
                                                                                                                       dc.expedite_flag,         -- expedite_flag
                                                                                                                       dc.in_store_date,         -- in_store_date
                                                                                                                       dc.in_store_date,         -- rma_nbr
                                                                                                                       dc.tracking_nbr,          -- tracking_nbr
                                                                                                                       dc.freight_charge,        -- freight_charge
                                                                                                                       dc.master_container_id,   -- master_container_id
                                                                                                                       -- ASNOutItem_TBL
                                                                                                                       CAST(MULTISET(select "RIB_ASNOutItem_REC"(0,                            -- rib_oid
                                                                                                                                                                 di.item_id,                   -- item_id                  
                                                                                                                                                                 di.unit_qty,                  -- unit_qty                 
                                                                                                                                                                 di.nic_cost,                  -- gross_cost               
                                                                                                                                                                 di.priority_level,            -- priority_level           
                                                                                                                                                                 di.order_line_nbr,            -- order_line_nbr           
                                                                                                                                                                 di.lot_nbr,                   -- lot_nbr                  
                                                                                                                                                                 di.final_location,            -- final_location           
                                                                                                                                                                 di.from_disposition,          -- from_disposition         
                                                                                                                                                                 di.to_disposition,            -- to_disposition           
                                                                                                                                                                 di.voucher_number,            -- voucher_number           
                                                                                                                                                                 di.voucher_expiration_date,   -- voucher_expiration_date  
                                                                                                                                                                 di.container_qty,             -- container_qty            
                                                                                                                                                                 di.comments,                  -- comments                 
                                                                                                                                                                 di.ebc_cost,                  -- unit_cost                
                                                                                                                                                                 di.base_cost,                 -- base_cost                
                                                                                                                                                                 di.weight,                    -- weight                   
                                                                                                                                                                 di.weight_uom)                -- weight_uom               
                                                                                                                                       from fm_stg_asnout_item di
                                                                                                                                      where dc.seq_no = di.ctn_seq_no) as "RIB_ASNOutItem_TBL"),  -- RIB_ASNOutItem_TBL
                                                                                                                       dc.comments,              -- comments
                                                                                                                       dc.weight,                -- weight
                                                                                                                       dc.weight_uom)            -- WEIGHT_UOM
                                                                                              from fm_stg_asnout_ctn dc
                                                                                             where ds.seq_no = dc.distro_seq_no) as "RIB_ASNOutCtn_TBL"),  -- ASNOutdistro_TBL
                                                                              ds.comments)             -- comments
                                                  from fm_stg_asnout_distro ds
                                                 where ds.desc_seq_no = d.seq_no) as "RIB_ASNOutDistro_TBL"),  -- ASNOutdistro_TBL
                                  d.comments)            -- comments
        from fm_stg_asnout_desc d
       where d.seq_no = I_seq_no
         and d.status in ('H','P');

   L_asnout_message    "RIB_ASNOutDesc_REC";

BEGIN
   -- Populate Desc
   open C_GET_ASNOUT_DESC;
   fetch C_GET_ASNOUT_DESC into L_asnout_message;
   close C_GET_ASNOUT_DESC;

   L_status_code   := NULL;
   O_error_message := NULL;

   -- Call procedure Consume
   RMSSUB_ASNOUT.CONSUME_SHIPMENT(L_status_code,
                                  O_error_message,
                                  L_asnout_message,
                                  L_message_type,
                                  'N');

   if L_status_code = API_CODES.UNHANDLED_ERROR or O_error_message is NOT NULL then
      O_error_ind := TRUE;
      return FALSE;
   else
      O_error_ind := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CONSUME;
---------------------------------------------------------------------------------------------
FUNCTION BATCH_CONSUME_THREAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_no       IN       NUMBER,
                              I_num_threads     IN       NUMBER)
RETURN BOOLEAN IS

   L_program         VARCHAR2(100)      := 'FM_ASNOUT_CONSUME_SQL.BATCH_CONSUME_THREAD';
   L_table           VARCHAR2(50)       := NULL;
   L_rowid_dsc_tbl   ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_dst_tbl   ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_ctn_tbl   ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_itm_tbl   ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_dh_tbl    ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_rowid_sch_tbl   ROWID_CHAR_TBL     := ROWID_CHAR_TBL();
   L_seq_tbl         NUMBER_TBL         := NUMBER_TBL();
   L_fd_id_tbl       NUMBER_TBL         := NUMBER_TBL();
   L_error_ind       BOOLEAN;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   -- Staging Records
   cursor C_GET_STG_ASNOUT is
   select seq_no,
          fiscal_doc_id
     from fm_stg_asnout_desc
    where status = 'H'
      and mod(from_location, I_num_threads)+1 = I_thread_no
    order by seq_no;

   -- Locking cursors
   cursor C_LOCK_STG_ASN_DESC is
      select ROWIDTOCHAR(rowid)
        from fm_stg_asnout_desc
       where status = 'H'
         and mod(from_location, I_num_threads)+1 = I_thread_no
         for update nowait;

   cursor C_LOCK_STG_ASN_DISTRO is
      select ROWIDTOCHAR(rowid)
        from fm_stg_asnout_distro ad
       where exists (select 'x'
                       from TABLE(CAST(L_seq_tbl as NUMBER_TBL)) seq
                      where value(seq) = ad.desc_seq_no
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_STG_ASN_CTN is
      select ROWIDTOCHAR(rowid)
        from fm_stg_asnout_ctn ac
       where exists (select 'x'
                       from TABLE(CAST(L_seq_tbl as NUMBER_TBL)) seq
                      where value(seq) = ac.desc_seq_no
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_STG_ASN_ITEM is
      select ROWIDTOCHAR(rowid)
        from fm_stg_asnout_item ai
       where exists (select 'x'
                       from TABLE(CAST(L_seq_tbl as NUMBER_TBL)) seq
                      where value(seq) = ai.desc_seq_no
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_FISCAL_DOC_HEADER is
      select ROWIDTOCHAR(rowid)
        from fm_fiscal_doc_header dh
       where dh.status != 'A'            
         and not exists (select 'x'
                           from fm_stg_asnout_desc ad
                          where ad.fiscal_doc_id = dh.fiscal_doc_id
                            and rownum = 1)
         and exists (select 'x'
                       from TABLE(CAST(L_fd_id_tbl as NUMBER_TBL)) fd
                      where value(fd) = dh.fiscal_doc_id
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_SCHEDULE is
      select ROWIDTOCHAR(rowid)
        from fm_schedule s
       where s.status != 'A'
         and not exists (select 'x'
                           from fm_fiscal_doc_header dh
                          where dh.status != 'A'
                            and dh.schedule_no = s.schedule_no
                            and exists (select 'x'
                                          from TABLE(CAST(L_fd_id_tbl as NUMBER_TBL)) fd
                                         where value(fd) = dh.fiscal_doc_id
                                           and rownum = 1)
                            and rownum = 1)
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STG_ASNOUT',
                    'fm_stg_asnout_desc',
                    NULL);
   open C_GET_STG_ASNOUT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STG_ASNOUT',
                    'fm_stg_asnout_desc',
                    NULL);
   fetch C_GET_STG_ASNOUT BULK COLLECT into L_seq_tbl,
                                            L_fd_id_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STG_ASNOUT',
                    'fm_stg_asnout_desc',
                    NULL);
   close C_GET_STG_ASNOUT;

   if L_seq_tbl is NULL or L_seq_tbl.count <= 0 then
      return TRUE;
   end if;

   -- Loop thru all the sequence numbers
   FOR rec in L_seq_tbl.FIRST..L_seq_tbl.LAST LOOP
      -- call consume for each seq_no
      if FM_ASNOUT_CONSUME_SQL.CONSUME(O_error_message,
                                       L_error_ind,
                                       L_seq_tbl(rec)) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   -- Delete consumed records

   L_table := 'FM_STG_ASNOUT_ITEM';
   open C_LOCK_STG_ASN_ITEM;
   fetch C_LOCK_STG_ASN_ITEM BULK COLLECT into L_rowid_itm_tbl;
   close C_LOCK_STG_ASN_ITEM;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'fm_stg_asnout_item',
                    NULL);
   delete from fm_stg_asnout_item ai
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_itm_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = ai.rowid);

   L_table := 'FM_STG_ASNOUT_CTN';
   open C_LOCK_STG_ASN_CTN;
   fetch C_LOCK_STG_ASN_CTN BULK COLLECT into L_rowid_ctn_tbl;
   close C_LOCK_STG_ASN_CTN;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'FM_STG_ASNOUT_CTN',
                    NULL);
   delete from fm_stg_asnout_ctn ac
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_ctn_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = ac.rowid);

   L_table := 'FM_STG_ASNOUT_DISTRO';
   open C_LOCK_STG_ASN_DISTRO;
   fetch C_LOCK_STG_ASN_DISTRO BULK COLLECT into L_rowid_dst_tbl;
   close C_LOCK_STG_ASN_DISTRO;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'FM_STG_ASNOUT_DISTRO',
                    NULL);
   delete from fm_stg_asnout_distro ad
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_dst_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = ad.rowid);


   L_table := 'FM_STG_ASNOUT_DESC';
   open C_LOCK_STG_ASN_DESC;
   fetch C_LOCK_STG_ASN_DESC BULK COLLECT into L_rowid_dsc_tbl;
   close C_LOCK_STG_ASN_DESC;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'FM_STG_ASNOUT_DESC',
                    NULL);
   delete from fm_stg_asnout_desc ad
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_dsc_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = ad.rowid);

   -- Update status of fiscal doc to 'A'pproved
   L_table := 'FM_FISCAL_DOC_HEADER';
   open C_LOCK_FISCAL_DOC_HEADER;
   fetch C_LOCK_FISCAL_DOC_HEADER BULK COLLECT into L_rowid_dh_tbl;
   close C_LOCK_FISCAL_DOC_HEADER;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'fm_fiscal_doc_header',
                    NULL);
   update fm_fiscal_doc_header dh
      set dh.status = 'A'
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_dh_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = dh.rowid);

   -- Update status of schedule to 'A'pproved
   L_table := 'FM_SCHEDULE';
   open C_LOCK_SCHEDULE;
   fetch C_LOCK_SCHEDULE BULK COLLECT into L_rowid_sch_tbl;
   close C_LOCK_SCHEDULE;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'fm_schedule',
                    NULL);
   update fm_schedule s
      set s.status = 'A'
    where exists (select 'x'
                    from TABLE(CAST(L_rowid_sch_tbl as ROWID_CHAR_TBL)) rw
                   where value(rw) = s.rowid);
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             SQLERRM,
                                             L_table,
                                             TO_CHAR(SQLCODE));

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END BATCH_CONSUME_THREAD;
---------------------------------------------------------------------------------------------
END FM_ASNOUT_CONSUME_SQL;
/