CREATE OR REPLACE PACKAGE FM_RTV_SQL IS
----------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,           
                 O_seq_no          IN OUT  FM_STG_RTV_DESC.SEQ_NO%TYPE,        
                 I_message         IN      RIB_OBJECT,                         
                 I_message_type    IN      VARCHAR2)                           
RETURN BOOLEAN;
----------------------------------------------------------------------------------        
END FM_RTV_SQL;
/