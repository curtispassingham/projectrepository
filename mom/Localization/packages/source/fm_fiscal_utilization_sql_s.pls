CREATE OR REPLACE PACKAGE FM_FISCAL_UTILIZATION_SQL is
---------------------------------------------------------------------------------
-- Function Name: GET_DESC
-- Purpose:       This function gets the utilization description.
---------------------------------------------------------------------------------
FUNCTION GET_DESC (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_util_desc        IN OUT FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                   I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXISTS
-- Purpose:       This function will check if exists utilization on table fm_fiscal_utilization
---------------------------------------------------------------------------------
FUNCTION EXISTS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists           IN OUT BOOLEAN,
                 I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: UPDATE_STATUS
-- Purpose: This function will update the stauts of the utilization
---------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: GET_INFO
-- Purpose:       This function gets the all information of fm_fiscal_utilization.
---------------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_util_desc         IN OUT FM_FISCAL_UTILIZATION.UTILIZATION_DESC%TYPE,
                   O_status            IN OUT FM_FISCAL_UTILIZATION.STATUS%TYPE,
                   O_mode_type         IN OUT FM_FISCAL_UTILIZATION.MODE_TYPE%TYPE,
                   O_nop               IN OUT FM_FISCAL_UTILIZATION.NOP%TYPE,
                   O_requisition_type  IN OUT FM_FISCAL_UTILIZATION.REQUISITION_TYPE%TYPE,
                   I_utilization       IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXISTS_UTIL_REC
-- Purpose:       This function will check if exists NF records for utilization on
---               table fm_fiscal_utilization  and fm_fiscal_doc_header
---------------------------------------------------------------------------------
FUNCTION EXISTS_UTIL_REC (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT BOOLEAN,
                          I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: UTIL_REASON_EXISTS
-- Purpose:       This function will check if exists the utilization id reason code
--                combination in the table fm_util_invadj_reason
---------------------------------------------------------------------------------
FUNCTION UTIL_REASON_EXISTS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists_reason    IN OUT BOOLEAN,
                             I_utilization_id   IN     FM_UTIL_INVADJ_REASON.UTILIZATION_ID%TYPE,
                             I_reason           IN     FM_UTIL_INVADJ_REASON.REASON%TYPE )
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXISTS_DOC
-- Purpose:       This function will check if exists utilization_id on table
--                fm_fiscal_doc_header.
---------------------------------------------------------------------------------
FUNCTION EXISTS_DOC (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists           IN OUT BOOLEAN,
                     I_utilization_id   IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: UTIL_REASON_DUP
-- Purpose:       This function will check if the  reason code is attached to
--                any utilization id.
---------------------------------------------------------------------------------
FUNCTION UTIL_REASON_DUP (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists_reason    IN OUT BOOLEAN,
                          I_reason           IN     FM_UTIL_INVADJ_REASON.REASON%TYPE )
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXISTS_UTILIZATION_REQ_TYPE
-- Purpose:       This function validates the requisition Type and utilization.
----------------------------------------------------------------------------------
FUNCTION EXISTS_UTILIZATION_REQ_TYPE(O_error_message  IN OUT VARCHAR2,
                                     O_exists         IN OUT BOOLEAN,
                                     I_requisition_type  IN  FM_FISCAL_UTILIZATION.REQUISITION_TYPE%TYPE,
                                     I_utilization_id IN     FM_DOC_TYPE_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: EXISTS
-- Purpose:       This function will check if exists utilization is attached to PO/TSF/MRT
---------------------------------------------------------------------------------
FUNCTION RMS_UTIL_EXISTS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT BOOLEAN,
                          I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
FUNCTION DELETE_UTIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_utilization_id IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: NFE_UTIL_EXISTS
-- Purpose:       This function will check if exists utilization is attached to NFE LOCATION
---------------------------------------------------------------------------------
FUNCTION NFE_UTIL_EXISTS (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT BOOLEAN,
                          I_utilization      IN     FM_FISCAL_UTILIZATION.UTILIZATION_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------   
END FM_FISCAL_UTILIZATION_SQL;
/