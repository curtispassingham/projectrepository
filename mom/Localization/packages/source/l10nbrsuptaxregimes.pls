CREATE OR REPLACE PACKAGE L10N_BR_SUP_TAX_REGIME_SQL AS
---------------------------------------------------------------------
  -- Function: REGIME_EXISTS 
  -- Purpose: Duplicate check
---------------------------------------------------------------------
FUNCTION REGIME_EXISTS(O_error_message 	IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT     BOOLEAN,
                       I_supplier       IN      NUMBER,
                       I_tax_regime     IN      VARCHAR2)
                       
RETURN BOOLEAN;
---------------------------------------------------------------------
END L10N_BR_SUP_TAX_REGIME_SQL;
/